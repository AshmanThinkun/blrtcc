Parse.initialize(window.blrtpai, window.blrtpjk);
console.log('File login.js : Parse.initialize(window.blrtpai, window.blrtpjk)');
console.log(window.blrtpai);
console.log(window.blrtpjk);

var urlparts = window.location.pathname.split("/")
var conversationId = urlparts[urlparts.length - 1];
const numVer = '1.0.7'


listBlrtsOrReLogin(true)
var accessRequestDiv = '' +
    '<div id="blrt-web-player-cover-request-access-area content-black"> ' +
    '<img id="blrt-web-player-cover-request-access-area-img" alt="Private Blrt" src="//s3.amazonaws.com/blrt-webplayer/d/img/private-blrt-black.png"/> ' +
    '<p id="blrt-web-player-cover-request-access-area-text" class="bwp-cover-font" style="color: black">This is a private Blrt, but you can request access from the conversation creator</p>' +
    '<button type="button" data-loading-text="Loading..." onclick="requestAccess()" id="landing-user-sec-btn" class="mouse-cursor-clickable bwp-cover-font">Request access</button> ' +
    '</div>';
var userName;

$('#loginButton').click(function (e) {
    e.preventDefault();
    $('#loginButton').button('loading');
    var name = $("#email").val();
    var pass = $("#password").val();
    console.log("name: "+name+" pass: "+pass)
    Parse.User.logIn(name, pass, {
        success: function () {
            listBlrtsOrReLogin();
        },
        error: function () {
            listBlrtsOrReLogin()
        }
    })
})


function removeLogin()
{
    $('.UDP-play-btn').remove();
    $('#login').remove();
    $('.not-on-blrt').remove();
    $('#show').removeClass('show');
    $('#loginOrList').append($('<h4 class="loading">loading</h4>'))
}

function listBlrtsOrReLogin(first) {
    if(userName = Parse.User.current())
    {
        removeLogin()
    }
    Parse.Cloud.run("conversationRecentBlrt", {conversationId: conversationId}, {
        success: function (result) {
            if (result == "LOGIN" && first) {
                //$('#loginOrList').append($(loginDiv))
            }
            else if (result == "LOGIN") {
                $('#loginButton').button('reset')
                displayLoginError("Invalid login parameters")

            }
            else if (result == "NOACCESS") {
                removeLogin();
                $('.loading').remove();
                userName = Parse.User.current().get('username');
                $('#loginOrList').append($('<p id="blrt-web-player-login-page-subtitle" ' +
                    'style="margin-top: -25px;font-size: 25pt;font-weight: normal;">Request access to this conversation</p>'))
                $('#loginOrList').append($('<h5 style="margin-top: 10px;margin-bottom: 20px;">You are logged in as <strong>' +
                    userName + "</strong>.&nbsp;&nbsp;<a href='javascript:void(0)' onClick='logout()'>Sign out</a> </h5>"))
                $("#loginOrList").append($(accessRequestDiv))
            }
            else if (result) {
                userName = Parse.User.current().get('username');
                removeLogin();
                $('.loading').remove();
                $('#first-section-info').html('Blrts in this conversation are listed below. To view the full conversation,' +
                    'get the free Blrt app.')
                $('#landing-user-sec').removeClass('container-green');
                $('#landing-user-sec').addClass('container-grey');
                $('#loginOrList').append($('<p id="blrt-web-player-login-page-subtitle" ' +
                    'style="margin-top: -25px;font-size: 30pt;font-weight: normal;">Blrts in this conversation</p>'))
                $('#loginOrList').append($('<h5 style="margin-top: 10px;margin-bottom: 20px;">You are logged in as <strong>' +
                    userName + "</strong>.&nbsp;&nbsp;<a href='javascript:void(0)' onClick='logout()'>Sign out</a> </h5>"))
                result.forEach(function (blrt) {
                    var iframeHtml = getIFrameHTML(blrt.blrtId);
                    console.log(iframeHtml)
                    $('#loginOrList').append($(iframeHtml))
                })
//
                console.log(JSON.stringify(result))
            }
        }
    })

}


function logout() {
    Parse.User.logOut();
    location.reload()
}


function getIFrameHTML(blrtId) {
    var src = window.blrtes + "/embed/blrt/" + blrtId + "?player=0";
    return '<iframe height="139px" src="' + src + '" style="border: none;' +
        ' width: 450px; max-width: 100%;margin-left: auto; margin-right: auto;margin-top: 10px;display: block; "></iframe>';
}

function displayLoginError(message) {
    $('#loginError').html(message)
    $('#loginError').show();
}

$(".no-jump").on('click', function(event) {

    // Prevent default anchor click behavior
    event.preventDefault();
    // Store hash
    var hash = this.hash;
    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
        scrollTop: $(hash).offset().top
    }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
    });
});


$(".focus-register").on('click', function(event) {
    event.preventDefault();
    $('#landing-user-sec-email').focus();
})




//reuse the existing code
function requestAccess() {
    var btn=$("#landing-user-sec-btn");
    btn.button('loading');
    return callBlrtAPI("conversationRequestAccess", {
        conversationId: conversationId
    }).then(function (response) {

        console.log(response)
        if (response.error) {
            var alertPromise = null;
            switch (Number(response.error.code)) {
                case 100:
                    alert("Internet problem", "Check your internet connection or visit blrt.com for help.");
                    break;
                case 613:
                    alert("Conversation full", "This conversation has reached the user limit. You cannot request access at this time.");
                    break;
                case 612:
                    alert("Cannot request access", "You cannot request access to this conversation at this time.");
                    break;
                //case 611:
                //    alertPromise = alertUnexpectedError({API: "conversationRequestAccess", err: response.error, info: "code"});
                //    break;
            }
            return alertPromise.then(function () {
                return Parse.Promise.error(response.error);
            })
        } else if (response.success) {
            if (Number(response.status.code) == 556) {
                console.log("successfully request the access")
                $("#landing-user-sec-btn").remove()
                $("#loginOrList").append('<p> Request sent</p>')
            }
            return true;
        } else {
        }
    }, function (error) {

        return Parse.Promise.error(error);
    });
}


function isValidString(str, allowEmpty) {
    allowEmpty = ('undefined' === typeof allowEmpty) ? false : allowEmpty;
    return (('string' === typeof str) && (allowEmpty || str.length > 0));
}


function callCloudFunc(name, cloudFuncParams, options) {
    return Parse.Cloud.run(name, cloudFuncParams, options).then(
        function (response) {
            if (isValidString(response, true)) {
                try {
                    response = JSON.parse(response);
                } catch (e) {
                    return Parse.Promise.error(e);
                }
            }
            if (typeof response == 'object') {
                return response;
            }
            return Parse.Promise.error(e);
        },
        function (error) {
            return Parse.Promise.error(error);
        }
    );
}

function callBlrtAPI(name, params, options) {
    var os = null;
    var osVersion = null;
    var browser = null;
    var browserVersion = null;
    try {
        os = window.UAInfo.os.name;
    } catch (e) {
    }
    try {
        osVersion = window.UAInfo.os.version;
    } catch (e) {
    }
    try {
        browser = window.UAInfo.browser.name;
    } catch (e) {
    }
    try {
        browserVersion = window.UAInfo.browser.version;
    } catch (e) {
    }
    var cloudFuncParams = {
        api: {
            version: 1,
            endpoint: name
        },
        device: {
            version: numVer || "null",
            device: "unknown",
            os: os || "unknown",
            osVersion: osVersion || "unknown",
            appName: "ChromeWebPlayer",
            browser: browser,
            browserVersion: browserVersion
        },
        data: params
    };
    return callCloudFunc(name, cloudFuncParams, options)
        .then(function (response) {
            var errorHandlingPromise = Parse.Promise.as(true);
            if (response.error) {
                switch (Number(response.error.code)) {
                    case 300:
                        response.error.code = ErrorAlreadyHandled;
                        errorHandlingPromise = alertIncompatibleVersion("BlrtAPI", 1, "unknown", 0x6503);
                        break;
                    case 600:
                        response.error.code = ErrorAlreadyHandled;
                        errorHandlingPromise = alertUnderMaintenance();
                        break;
                }
            }
            return errorHandlingPromise.then(function () {
                return response;
            });
        });
}


function validateEmail(email) {
    var re = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b$/i;
    return re.test(email);
}


function forgetPassword() {
    var emailField= $('#email')
    if (validateEmail(emailField.val())) {
        //loadingMask.on();
        Parse.User.requestPasswordReset(emailField.val()).then(function() {
            //loadingMask.off();
            alert("A link to reset your password has been sent. You may need to check your spam folder.");
        }, function(err) {
            //loadingMask.off();
            switch (Number(err.code)) {
                case 100:
                    alert("Check your internet connection or visit blrt.com for help.");
                    break;
                case 205:
                    alert("No user found with that email address.");
                    break;
                default:
                    break;
            }
        });
    } else {
        alert("Please enter a valid email address.");
    }
}











