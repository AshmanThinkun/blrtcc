var stream=process.env.BLRT_STREAM||"d";
var _=require('underscore');

console.log('stream==='+stream);
console.log('BLRT_STREAM==='+process.env.BLRT_STREAM);

var serverConfig={
    fileKey:[
        "a79e8d42-08e4-42c5-bd92-568892f4d607",
        "8588d7f9-9724-4a34-8029-b0d0a3e8cf97",
        "eb54faca-0c91-4800-8480-0982693a7abb",
        "a79e8d42-08e4-42c5-bd92-568892f4d607"//test sp
    ],
    IOSPushPath:[
        __dirname+"/newDev.p12",
        __dirname+"/QA_APNS.p12",
        __dirname+"/LIVE_APNS.p12",
        __dirname+"/newDev.p12"//test sp
    ],
    pushBundleID:[
        "net.thinkun.blrt.dev",
        "net.thinkun.blrt.qa",
        "com.blrt.blrt",
        "net.thinkun.blrt.dev"//test sp
    ],
    androidSenderId:[
        "911760092001",
        "494185331879",
        "227338088772",
        "911760092001"//test sp
    ],
    androidApiKey:[
        "AIzaSyB9aCx5-ra_UXREyQdtxk3T701ROmRNC0U",
        "AIzaSyDnYE-5QvzEbzC68h52WJ0OTVTQs7LdzPk",
        "AIzaSyAdZQsDVOwq1HLdIOpCp82sfTetnRRnua8",
        "AIzaSyB9aCx5-ra_UXREyQdtxk3T701ROmRNC0U"//test sp
    ],
    s3AapaterBuket:[
        "blrt-dev-file-adapter",
        "blrt-qa-file-adapter",
        "blrt-live-file-adapter",
        "blrt-dev-file-adapter"//test sp
    ],
    serverUrl:[
        process.env.SERVER_URL||"https://d.blrt.co/parse",
        process.env.SERVER_URL||"https://q.blrt.co/parse",
        process.env.SERVER_URL||"https://m.blrt.co/parse",
        process.env.SERVER_URL||"https://sp.blrt.co/parse"//test sp
    ],
    siteAddress:[
        process.env.SITE_ADDRESS||"https://d.blrt.co",
        "https://q.blrt.co",
        (process.env.IS_STAGING == "yes") ? "https://s.blrt.co" : "https://m.blrt.co",
        process.env.SITE_ADDRESS||"https://d.blrt.co"//test sp
    ],
    masterKey:[
        "123",
        "123",
        "y7H!hf;E$Q5!}v4",
        "123" //test sp
    ],
    webappServerUrl:[
    	process.env.WEBAPP_SERVER_URL||"https://dweb.blrt.com",
        process.env.WEBAPP_SERVER_URL||"https://qweb.blrt.com",
        process.env.WEBAPP_SERVER_URL||"https://web.blrt.com", //send socket message to webapp
        process.env.WEBAPP_SERVER_URL||"https://spweb.blrt.com"//test sp
    ],
    appId:[
        "blrtDev",
        "blrtQa",
        "blrt",
        "blrtSpe"//test sp
    ],
    databaseURI:[
        process.env.PARSE_SERVER_DATABASE_URI||"mongodb://10.0.2.35/parse-dev",
        process.env.PARSE_SERVER_DATABASE_URI||"mongodb://10.0.2.35/parse-qa",
        "mongodb://10.0.2.97/parse",
        process.env.PARSE_SERVER_DATABASE_URI||"mongodb://10.0.2.35/parse-dev" //test sp
    ],
    logServerURI:[
        process.env.LOG_MONGO_URI||"mongodb://Guichi:123@10.0.2.35/log",
        process.env.LOG_MONGO_URI||"mongodb://Guichi:123@10.0.2.35/log",
        "mongodb://10.0.2.97/log",
        process.env.LOG_MONGO_URI||"mongodb://Guichi:123@10.0.2.35/log"//test sp
    ],
    videoConversationURL:[
        'http://ec2-34-200-97-255.compute-1.amazonaws.com',
        'http://ec2-34-200-97-255.compute-1.amazonaws.com',
        'http://ec2-52-55-48-231.compute-1.amazonaws.com',
        'http://ec2-34-200-97-255.compute-1.amazonaws.com' //test sp
        ]
}



// default one for localhost and dev ,
var configIndex=0;
if(stream=='q')
{
    configIndex=1;
}
else if(stream=='m')
{
    configIndex=2
}
else if(stream=='sp')
{
    configIndex=3
}


var result={};
_.each(serverConfig,function (value,key) {
    result[key]=value[configIndex]
    
    console.log('value[configIndex]=' + value[configIndex]);
    
})


 

module.exports=result;
