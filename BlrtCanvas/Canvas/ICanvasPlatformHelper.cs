using System;
using System.Drawing;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using System.IO;


namespace BlrtCanvas
{
	public enum ToolbarLayoutPolicy
	{
		Flexible,
		AlwaysRight
	};

	public interface ICanvasPlatformHelper
	{
		// PlatformHelper
		string Translate (string field);
		string Translate (string field, string context);
		void SetAutoDim (bool allow);
		Stream GetAssetFileStream (string filepath);
		int PDFPageCount (string path);
		ICanvasPage OpenPage (string objectId, int mediaPage, BlrtData.ILocalStorageParameter localStorage);
		/// <summary>
		/// Shows the alert async.
		/// </summary>
		/// <returns>-1: cancel; 0~n n th button in otherBtns is pressed</returns>
		/// <param name="title">Title.</param>
		/// <param name="message">Message.</param>
		/// <param name="cancelBtnTitle">Cancel button title.</param>
		/// <param name="otherBtns">Other button titles.</param>
		Task<int> ShowAlertAsync (string title, string message, string cancelBtnTitle, params string [] otherBtns);

		// BCPlatformManager
		Size GetImageSizeBeforeRotation (string filePath);
		TextureRotation GetImageRotation (string filePath);
		Task<bool> CreateImgFileThumbnail (string srcPath, string dstPath, Size thumbnailSize);
		Task<bool> CreatePDFThumbnail (string srcPDFPath, int mediaPage, string dstPath, Size thumbnailSize);
		Size GetPDFPageSize (string pdfPath, int mediaPage);
		Size GetImageAssetSize (string assetPath);
		float ScreenScale { get; }
		int ThumbnailLength { get; }

		// BCPlatformGLManager
		IBCTextureLoader GetImageTextureLoader (string path);
		IBCTextureLoader GetPDFTextureLoader (string path, int mediaPage);
		void ReleaseAll ();
		IBCTextureLoader GetImageAssetTextureLoader (string path);
		void CalulateMaxTextureSize ();
		IBCTextureLoader GetTiledImageAssetTextureLoader (string path);

		ToolbarLayoutPolicy PreferredToolbarLayout { get; }
		event EventHandler<ToolbarLayoutPolicy> PreferredToolbarLayoutChanged;
		bool TryValidateConversationName (string convoName);
	}
}

