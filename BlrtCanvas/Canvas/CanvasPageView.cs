﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using BlrtCanvas.GLCanvas;
using BlrtCanvas.Views;
using BlrtData.Contents;
using BlrtData;
using BlrtData.Media;
using BlrtCanvas.Pages;
using BlrtData.ExecutionPipeline;
using Acr.UserDialogs;
using System.Linq;

namespace BlrtCanvas
{
	public enum CanvasState {
		Loading,


		Playback_Paused,
		Playback_Playing,

		Record_PreRecording,
		Record_Recording,
		Record_PostRecording,

		Record_PlaybackPaused,
		Record_PlaybackPlaying,
	}

	[Flags]
	public enum ExcludedHidingView : int
	{
		None			= 0x0,
		VolumeControl	= 0x1,
		LineWidth		= 0x2,
		PageSelection	= 0x4
	}

	public class CanvasPageView : ContentView, ICanvasManager
	{
		static ICanvasPlatformHelper _platformHelper;
		public static ICanvasPlatformHelper PlatformHelper {
			get {
				if (_platformHelper == null) {
					_platformHelper = DependencyService.Get<ICanvasPlatformHelper> ();
				}
				return _platformHelper;
			}
		}

		//public event EventHandler PlayPausePressed {
		//	add {
		//		_bottomBar.PlayPausePressed += value;
		//	}
		//	remove {
		//		_bottomBar.PlayPausePressed -= value;
		//	}
		//}

		static readonly Xamarin.Forms.Color RecordBannerNormalColor = BlrtStyles.BlrtWhite;
		static readonly Xamarin.Forms.Color RecordBannerHighlightColor = BlrtStyles.BlrtBlackBlack;

		public BlrtData.BlrtBrush SelectedBrush		{ get { return _toolbar.SelectedBrush;	} }
		public BlrtData.BlrtBrushWidth BrushWidth	{ get { return _toolbar.BrushWidth;		} }
		public BlrtData.BlrtBrushColor BrushColor	{ get { return _toolbar.BrushColor;		} }

		public bool IsPlaying		{ get { return _currentState == CanvasState.Playback_Playing || _currentState == CanvasState.Record_PlaybackPlaying; } }
		public bool IsRecording		{ get { return _currentState == CanvasState.Record_Recording; } }
		public bool IsLoading		{ get { return _currentState == CanvasState.Loading; } }

		private float _lastPos;
		private int _nextNotifyPos;
		private bool _barsHidden;

		private MultiExecutionPreventor.SingleExecutionToken _multiExeToken = MultiExecutionPreventor.NewToken ();

		public float CurrentTime { 
			get {
				if (IsPlaying || IsRecording) {

					float pos = 0;

					if (IsPlaying) {
						pos = _audioPlayer.CurrentTime;
					} else {
						pos = _audioRecorder.Duration;
					}
					if(_lastPos != pos) {
						_lastPos = pos;
						Device.BeginInvokeOnMainThread (async () => {
							_bottomBar.SetCurrentTime (_lastPos);
							_topBar.PageSelectionView.CurrentPage = _canvasView.CurrentPage;

							if (IsRecording) {
								if ((_nextNotifyPos > 0) && ((BlrtPermissions.MaxBlrtDuration - _lastPos) < _nextNotifyPos)) {
									_flashView.FlashCountDown (_nextNotifyPos);
									--_nextNotifyPos;
								}
								if (_lastPos > BlrtPermissions.MaxBlrtDuration) {
									_supportRecordContinue = false;
									_bottomBar.CanContinueRecording = false;
									if (!IsLoading && IsRecording) {
										SetState(CanvasState.Record_PostRecording);
									}
									if (BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free) {
										var upgrade = await BlrtUpgradeHelper.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtDuration);
										if(!upgrade){
											TryShowHelpOverlay();
										}
									}
								}
							}
						});
					}
					return _lastPos;
				} else {
					return _bottomBar.CurrentTime;
				}
			}
		}

		IBlrtCanvasData _data;


		CanvasState _currentState;

		IBCAudioPlayer _audioPlayer;
		IBCAudioRecorder _audioRecorder;

		CanvasBarBottomView		_bottomBar;
		CanvasBarTopView		_topBar;
		CanvasGLView			_canvasView;
		ToolbarView				_toolbar;
		Label					_recBanner;

		bool _supportRecordContinue;

		bool _playAfterRelease;

		TouchPanelView _touchPanel;

		FlashNotificationView _flashView;

		Page _parentPage;

		const int RecBannerNotifyTimeForMoving = 1;
		int _recBannerRestNotifyForMoving;
		public CanvasPageView (Page parent)
		{
			_parentPage = parent;
			_barsHidden = false;
			_recBannerRestNotifyForMoving = RecBannerNotifyTimeForMoving;
			_canvasView				= new CanvasGLView (this) {

				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
			};
			_topBar = new CanvasBarTopView () {
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};

			_audioPlayer = DependencyService.Get<IBCAudioPlayer> ();
			_audioRecorder = DependencyService.Get<IBCAudioRecorder> ();
			_supportRecordContinue = _audioRecorder.CanContinueRecording;

			_bottomBar		= new CanvasBarBottomView () {
				CanContinueRecording = _supportRecordContinue,
				VerticalOptions = LayoutOptions.End
			};

			_touchPanel = new TouchPanelView () {
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
			};
			_toolbar = new ToolbarView () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Fill,
			};

			_toolbar.ToolsChanged += (sender, e) => {
				BlrtData.Helpers.BlrtTrack.CanvasSelectTool(_toolbar.SelectedBrush.ToString(), _toolbar.BrushColor.ToString());
			};

			_recBanner = new Label () {
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				BackgroundColor = BlrtStyles.BlrtAccentRedHighlight,
				TextColor = RecordBannerNormalColor,
				Text = PlatformHelper.Translate ("Press record to talk, point and draw", "canvas"),
				Opacity = 0,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				InputTransparent = true,
				TranslationY = 1,
			};

			_flashView = new FlashNotificationView ();

			// whole page layout
			var grid = new Grid () {
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				RowSpacing = 0,
				ColumnSpacing = 0,
				RowDefinitions = {
					new RowDefinition { Height = GridLength.Auto},
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					new RowDefinition { Height = new GridLength(30, GridUnitType.Absolute)},
					new RowDefinition { Height = GridLength.Auto},
				},

				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				}
			};

			grid.Children.Add (_canvasView,		0, 1, 0, 4);
			grid.Children.Add (_flashView, 		0, 1, 0, 4);
			grid.Children.Add (_touchPanel, 	0, 1, 0, 4);
			grid.Children.Add (_recBanner, 		0, 1, 2, 3);
			grid.Children.Add (_toolbar, 		0, 1, 1, 2);
			grid.Children.Add (_topBar, 		0, 1, 0, 1);
			grid.Children.Add (_bottomBar, 		0, 1, 3, 4);
			Content = grid;

			// event listeners binding
			_audioPlayer.Finished += OnAudioComplete;



			// playback control
			_bottomBar.PlayPausePressed += MultiExecutionPreventor.NewEventHandler (async (s, a) => {
				await TogglePlayback ();
				
				Task.Run (delegate {
					if (_data.IsDraft) {
						BlrtData.Helpers.BlrtTrack.CanvasRecordPlay ();
					} else {
						BlrtData.Helpers.BlrtTrack.CanvasPlay (_data.Conversation, _data.Blrt);
					}
				}).FireAndForget ();

				HidePopupViews (ExcludedHidingView.None);
			}, _multiExeToken);

			_bottomBar.RecordPausePressed += MultiExecutionPreventor.NewEventHandler (async (s, a) => {
				await ToggleRecord ();
				HidePopupViews (ExcludedHidingView.None);
			}, _multiExeToken);

			_topBar.CrossPressed += MultiExecutionPreventor.NewEventHandler (async (s, a) => {
				await OnCrossPressed ();
				HidePopupViews (ExcludedHidingView.None);
			}, _multiExeToken);

			_topBar.ReblrtPressed += MultiExecutionPreventor.NewEventHandler (async (s, a) => {
//				Reblrt();
				HidePopupViews (ExcludedHidingView.None);
				await SetResult (CanvasResult.BackPressed (true));
			}, _multiExeToken);

			_topBar.SavePressed += MultiExecutionPreventor.NewEventHandler (async (s, a) => {
				if (IsConversationReadonly) {
					Device.BeginInvokeOnMainThread (delegate {
						BlrtUtil.ShowConversationReadonlyAlert ();
					});
				} else {
					await SaveRecord ();
					HidePopupViews (ExcludedHidingView.None);
				}
			}, _multiExeToken);
			_bottomBar.SliderMoved += (sender, e) => {
				_canvasView.CameraMode = CanvasCameraMode.Auto;
				if (IsPlaying) {
					_playAfterRelease = true;
					Pause (false);
				}
				_topBar.PageSelectionView.CurrentPage = _canvasView.CurrentPage;
				_canvasView.Redraw();
				HidePopupViews (ExcludedHidingView.None);
			};
			_bottomBar.SliderReleased += (sender, e) => {
				_canvasView.CameraMode = _canvasView.CameraModeDefault;
				if (_playAfterRelease) {
					PlayFrom (_bottomBar.CurrentTime, false, false);
					_playAfterRelease = false;
				}
				_canvasView.Redraw ();
				HidePopupViews (ExcludedHidingView.None);
			};

			_touchPanel.TouchChanged += (sender, e) => {
				_canvasView.ReceivedTouch (sender, e);
			};

			_canvasView.GestureFinished += (sender, e) => {
				SetUndoRedoState ();
			};

			_canvasView.SingleTapped += OnCanvasSingleTapped;
			_canvasView.TouchStarted += (sender, e) => {
				HidePopupViews (e);
				if (_recBannerRestNotifyForMoving > 0) {
					--_recBannerRestNotifyForMoving;
					StartFlashRecordBanner ();
				}
			};

			_topBar.PageSelectionView.CurentPageChanged += PageSelectionChanged; 

			_toolbar.UndoPressed += (sender, e) => {
				_canvasView.Undo ();
				SetUndoRedoState ();
				HidePopupViews (ExcludedHidingView.None);
			};
			_toolbar.RedoPressed += (sender, e) => {
				_canvasView.Redo ();
				SetUndoRedoState ();
				HidePopupViews (ExcludedHidingView.None);
			};
			_toolbar.SelectionStarted += (sender, e) => {
				HidePopupViews (e);
				StartFlashRecordBanner ();
			};
			BlrtManager.App.MoveToBackground += (sender, e) => {
				OnMoveToBackground ();
			};
		}

		public async Task LoadNew(IBlrtCanvasData data) {
			// load blrt
			Result = null;
			await Load (data);
		}

		void StartVolumeTask ()
		{
			Task.Run (async () => {
				while (!_shouldVolTaskFinish) {
					var vol = _audioRecorder.CurrentVolume;
					Device.BeginInvokeOnMainThread (() => {
						_bottomBar.SetVolume (vol);
					});
					await Task.Delay (100);
				}
			});
		}

		public event EventHandler ResultSet;
		public ICanvasResult Result { get; protected set; }

		bool IsConversationReadonly { 
			get {
				var rootController = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
				var mailbox = rootController.Slave as BlrtiOS.Views.Mailbox.MailboxController;
				return mailbox.ConversationScreen.CurrentConversation.IsReadonly;
			} 
		}

		private async Task SetResult(ICanvasResult result) {
			using (UserDialogs.Instance.Loading (PlatformHelper.Translate ("Loading..."))) {
				await Release (CanvasReleaseParameter.All);
				Result = result;
				await Task.Run (() => {
					if (null != ResultSet) {
						ResultSet (this, EventArgs.Empty);
					}
					if (null != _data && _data.IsIndependent) {
						BlrtUtil.DeleteDecryptedFiles ();
					}
				});
			}

		}

		/// <summary>
		/// actions need to be done when the cross button is pressed.
		/// </summary>
		/// <returns><c>true</c>, if canvas need to be exited, <c>false</c> still need to stay in canvas.</returns>
		public async Task<bool> OnCrossPressed ()
		{
			switch (_currentState) {
				case CanvasState.Record_PreRecording:
					await SetResult (CanvasResult.BackPressed(true));
					break;
				case CanvasState.Record_PlaybackPlaying:
					Pause ();
					goto case CanvasState.Record_PostRecording;
				case CanvasState.Record_Recording:
					await ToggleRecord (true); // go through
					goto case CanvasState.Record_PostRecording;
				case CanvasState.Record_PostRecording:
				case CanvasState.Record_PlaybackPaused:
					var choice = await PlatformHelper.ShowAlertAsync (
						PlatformHelper.Translate ("Erase Blrt", "canvas cross button"),
						#if __IOS__
						PlatformHelper.Translate ("Would you like to erase your recording progress?", "canvas cross button"),
						#elif __ANDROID__
						PlatformHelper.Translate ("Erase your recording progress?", "canvas cross button"),
						#endif
						PlatformHelper.Translate ("Cancel", "canvas cross button"), 
						PlatformHelper.Translate ("Start again", "canvas cross button"), 
						PlatformHelper.Translate ("Edit pages", "canvas cross button"), 
						PlatformHelper.Translate ("Exit recording", "canvas cross button")
					);
					switch (choice) {
						case -1:
							TryShowHelpOverlay ();
							return false;
						case 0:
							Reblrt ();
							BlrtData.Helpers.BlrtTrack.CanvasClear ();
							return false;
						case 1:
							await SetResult (CanvasResult.BackPressed(true));
							break;
						case 2:
							await SetResult (CanvasResult.BackPressed(false));
							break;
						default:
							break;
					}
					break;
				default:
					// go back directly
					Pause ();
					await SetResult (CanvasResult.BackPressed (false));
					break;
			}
			return true;
		}

		void OnMoveToBackground ()
		{
			switch (_currentState) {
				case CanvasState.Record_Recording:
					SetState (CanvasState.Record_PostRecording);
					TryShowHelpOverlay ();
					break;
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					Pause ();
					break;
				default:
					break;
			}
		}

//		void ClearRecording () {
//			_audioRecorder.Reset ();
//			Load (_data);
//		}

		/// <summary>
		/// make reblrt without quitting the canvas.
		/// the media picker will not be opened, reblrt will use all the used media in current playback.
		/// </summary>
		void Reblrt () {
			if (_data.IsIndependent) {
				return;
			}
			_audioRecorder.Reset ();
			Load (_data.CreateDraft());
			if (_toolbar != null)
				_toolbar.ShowToolBar ();
		}

		void SetUndoRedoState() {
			_toolbar.EnableUndoButton(_canvasView.CanUndo, true);
			_toolbar.EnableRedoButton(_canvasView.CanRedo, true);
		}

		async Task PauseAudioRecorder ()
		{
			var t = await _audioRecorder.Pause ();
			Device.BeginInvokeOnMainThread (async () => {
				_bottomBar.SetCurrentTime (t);
			});
			_canvasView.OnRecorderStopped(t);
		}

		async Task StopAudioRecorder ()
		{
			using (UserDialogs.Instance.Loading ("Loading...")) {
				var t = await _audioRecorder.Stop ();
				Device.BeginInvokeOnMainThread (() => {
					_bottomBar.SetCurrentTime (t);
				});
				_canvasView.OnRecorderStopped(t);
			}
		}

		bool _shouldVolTaskFinish = true;
		async Task SetState (CanvasState state, bool showFlashNotification = true) {
			var preState = _currentState;
			if (_currentState != state)
			{
				switch (state) {
					case CanvasState.Record_Recording:
						_shouldVolTaskFinish = false;
						StartVolumeTask ();
						break;
					default:
						_shouldVolTaskFinish = true;
						break;
				}
			}
			_currentState = state;
			_topBar.SetState (state);
			_bottomBar.SetState (state);
			_canvasView.SetState (state);

			switch (state) {
				case CanvasState.Loading:
					PlatformHelper.SetAutoDim (true);
					_toolbar.IsVisible = false;
					break;
				case CanvasState.Playback_Paused:
					PlatformHelper.SetAutoDim (true);
					_toolbar.IsVisible = false;
					if (CanvasState.Playback_Playing == preState) {
						_audioPlayer.Pause ();
					}
					break;
				case CanvasState.Record_PlaybackPaused:
					PlatformHelper.SetAutoDim (true);
					_toolbar.IsVisible = false;
					if (CanvasState.Record_PlaybackPlaying == preState) {
						_audioPlayer.Pause ();
					}
					break;
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					PlatformHelper.SetAutoDim (false);
					_toolbar.IsVisible = false;
					_audioPlayer.Play ();
					_data.TryMarkRead ();
					break;

				case CanvasState.Record_PreRecording:
					PlatformHelper.SetAutoDim (true);
					_audioRecorder.Reset ();
					_toolbar.IsVisible = true;
					SetUndoRedoState ();
					break;

				case CanvasState.Record_PostRecording:
					_bottomBar.SetAudioLength ( _audioRecorder.Duration );
					_bottomBar.SetCurrentTime ( _audioRecorder.Duration );

					PlatformHelper.SetAutoDim (true);
					_toolbar.IsVisible = true;
					if (_supportRecordContinue) {
						await PauseAudioRecorder ();
					} else {
						await StopAudioRecorder ();
						await PreparePostRecordingPlayback ();
					}
					SetUndoRedoState ();
					break;

				case CanvasState.Record_Recording:
					_bottomBar.SetAudioLength (BlrtData.BlrtPermissions.MaxBlrtDuration);
					PlatformHelper.SetAutoDim (false);
					_toolbar.IsVisible = true;
					_audioRecorder.Record ();
					SetUndoRedoState ();
					break;
			}

			switch (state) {
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
				case CanvasState.Loading:
					_topBar.PageSelectionView.IsEnabled = false;
					break;
				default:
					_topBar.PageSelectionView.IsEnabled = true;
					break;

			}

			// flash notification
			if (showFlashNotification) {
				switch (state) {
					case CanvasState.Record_Recording:
						_flashView.FlashRecord ();
						break;
					case CanvasState.Playback_Playing:
					case CanvasState.Record_PlaybackPlaying:
						_flashView.FlashPlay ();
						break;
					case CanvasState.Playback_Paused:
					case CanvasState.Record_PlaybackPaused:
						_flashView.FlashPause ();
						HideBars (false, true);
						break;
					default:
						break;
				}
			}

			if (ShouldShowRecordbanner()) {
				if (0 == _recBanner.Opacity) {
					_recBanner.Opacity = 1;
					_recBannerRestNotifyForMoving = RecBannerNotifyTimeForMoving;
				}
			} else {
				if (1 == _recBanner.Opacity) {
					_recBanner.Opacity = 0;
				}
			}

		}

		bool ShouldShowRecordbanner ()
		{
			return (_currentState == CanvasState.Record_PreRecording || _currentState == CanvasState.Record_PostRecording);
		}

		async Task PreparePostRecordingPlayback ()
		{
			await _audioPlayer.LoadAudio(_audioRecorder.FilePath);
			var audioLength = _audioPlayer.Length;
			_bottomBar.SetAudioLength (audioLength, audioLength);

			SetState(CanvasState.Record_PlaybackPaused);
		}

		void PageSelectionChanged (object sender, EventArgs e)
		{
			_canvasView.CurrentPage = Math.Max(1, Math.Min(_topBar.PageSelectionView.TotalPages, _topBar.PageSelectionView.CurrentPage));
			HidePopupViews (ExcludedHidingView.None);
			StartFlashRecordBanner ();
			if (_currentState == CanvasState.Record_PreRecording) {
				_startRecordingPage = _topBar.PageSelectionView.CurrentPage;
			}
		}

		int _startRecordingPage = 1;
		async Task Load (IBlrtCanvasData data)
		{
			try {
				_data = data;
				if (_data.IsIndependent) {
					_topBar.CanReply = false;
				}
				_bottomBar.LoadingForRecord = _topBar.LoadingForRecord = _data.IsDraft;
				SetState (CanvasState.Loading);

				using (UserDialogs.Instance.Loading (PlatformHelper.Translate ("Loading..."))) {
					_topBar.PageSelectionView.TotalPages	= _data.PageCount;
					_topBar.PageSelectionView.CurrentPage 	= 1;
					_startRecordingPage 					= 1;
					if (!_data.IsDecypted()) {
						await _data.DecyptFiles ();
					}

					await _canvasView.LoadData(_data);
				}

				if (_data.IsDraft) {
					try {
						StartUploadingPages (_data);
					} catch (BlrtEncryptorManagerException enc_ex) {
						if (enc_ex.Code == BlrtEncryptorManagerException.ExceptionCodes.EncryptionFailed) {
							await _platformHelper.ShowAlertAsync (
								"Encryption failed",
								"Unable to encrypt file. Please contact Blrt Support.",
								"OK"
							);
						}
						LLogger.WriteEvent ("encrypt", "error", "encryption failed", "CanvasPageView: pages; error" + enc_ex.ToString ());
						throw enc_ex;
					}
					_bottomBar.SetAudioLength (BlrtData.BlrtPermissions.MaxBlrtDuration);
					_audioRecorder.SetFilePath (BlrtData.BlrtConstants.Directories.Default.GetDecryptedPath ("cache" + _audioRecorder.Extension));
					_supportRecordContinue = _audioRecorder.CanContinueRecording;
					_bottomBar.CanContinueRecording = _supportRecordContinue;
					_nextNotifyPos = 5;
					SetState (CanvasState.Record_PreRecording);

				} else {
					await _audioPlayer.LoadAudio (_data.DecyptedAudioPath);
					_bottomBar.SetAudioLength ( _audioPlayer.Length );

					SetState (CanvasState.Playback_Playing);

				Task.Run (delegate {
						BlrtData.Helpers.BlrtTrack.CanvasPlay (_data.Conversation, _data.Blrt);
					}).FireAndForget();

					NotifyPlayedOnce ();
				}

				_canvasView.Redraw ();

			} catch (Exception e) {
				SetResult (CanvasResult.BackPressed (false));
			}
		}

		public void StartUploadingPages (IBlrtCanvasData data) {
			for (int i = 1; i <= data.PageCount; i++) {
				var page = data.GetPage (i);
				page.StartUpload ();
			}
		}

		public async Task Release (CanvasReleaseParameter param)
		{
			Pause ();

			CanvasStateManager.Last.Save ();
			HideBars (false, false);

			_audioPlayer.ReleaseFile ();
			_audioRecorder.Release ();
			await _canvasView.Release (param);
			_platformHelper.ReleaseAll ();

			// this class's release
			_data = null;
			_currentState = CanvasState.Loading;
			_lastPos = 0f;
			_nextNotifyPos = 0;
			_playAfterRelease = false;
			_shouldVolTaskFinish = true;
			_startRecordingPage = 1;
			_hitEnd = true;

			// bottom bar reset
			_bottomBar.Release ();
			_supportRecordContinue = _audioRecorder.CanContinueRecording;
			_bottomBar.CanContinueRecording = _supportRecordContinue;
			_bottomBar.LoadingForRecord = false;

			// top bar reset
			_topBar.Release ();
			_topBar.PageSelectionView.CurrentPage = 1;
			_topBar.PageSelectionView.TotalPages = 1;

			// toolbar
			_toolbar.EnableUndoButton (false, false);
			_toolbar.EnableRedoButton (false, false);


			GC.Collect ();
		}

		void OnAudioComplete (object sender, EventArgs e)
		{
			_bottomBar.SetCurrentTime (_audioPlayer.Length);
			if (IsPlaying) {
				Pause ();
				_hitEnd = true;
			}
		}


		public async Task TogglePlayback()
		{
			switch (_currentState) {
				case CanvasState.Playback_Paused:
				case CanvasState.Record_PlaybackPaused:
					Play ();
					break;
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					Pause ();
					break;
				case CanvasState.Record_PostRecording:
					if (_supportRecordContinue) {
						bool playConfirmed = await _parentPage.DisplayAlert (
							PlatformHelper.Translate ("Play back Blrt?", "playback after record"), 
							PlatformHelper.Translate ("You won't be able to keep recording if you play this Blrt now.", "playback after record"),
							PlatformHelper.Translate ("Play", "playback after record"),
							PlatformHelper.Translate ("Cancel", "playback after record")
						);
						if (playConfirmed) {
							await StopAudioRecorder ();
							await PreparePostRecordingPlayback ();
							Play ();
						}
					} else {
						Play ();
					}
					break;
				case CanvasState.Record_PreRecording:
				case CanvasState.Record_Recording:
					throw new NotImplementedException ();
					break;
			}
		}

		async Task ToggleRecord (bool blockHelpOverlay = false)
		{
			switch (_currentState) {
				case CanvasState.Playback_Paused:
				case CanvasState.Record_PlaybackPaused:
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					throw new NotImplementedException ();
					break;
				case CanvasState.Record_PostRecording:
					if (_supportRecordContinue) {
						SetState (CanvasState.Record_Recording);
						BlrtData.Helpers.BlrtTrack.CanvasResumeRecording ();
					} else {
						throw new NotImplementedException ();
					}
					break;
				case CanvasState.Record_PreRecording:
					var granted = await _audioRecorder.RequestPermissions ();
					if (granted) {
						SetState (CanvasState.Record_Recording);
						BlrtData.Helpers.BlrtTrack.CanvasStartRecording ();
					} else {
						await BlrtUtil.ShowMicPermissionAlert ();
					}
					break;
				case CanvasState.Record_Recording:
					SetState (CanvasState.Record_PostRecording);
					BlrtData.Helpers.BlrtTrack.CanvasStopRecording ();
					if (!blockHelpOverlay) {
						TryShowHelpOverlay ();
					}
					break;
			}
		}

		void TryShowHelpOverlay() {
			try {
				if(!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayCanvasStartViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed])
				{
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasStartHelpOverlay ();
				}

				else if(!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayCanvasDrawViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed])
				{
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasDrawHelpOverlay ();
				}

				else if(!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayCanvasNavViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed])
				{
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasNavHelpOverlay ();
				}

				else if (!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayCanvasFinishViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasFinishViewed])
				{
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasFinishHelpOverlay ();
				} 

			} catch{}
		}

		public void Pause (bool showFlashNotification = true)
		{
			if (!IsLoading && IsPlaying) {
				SetState (_data.IsDraft ? CanvasState.Record_PlaybackPaused : CanvasState.Playback_Paused, showFlashNotification);
			}
		}

		public void Play () {
			// if the playing has reached the end of the blrt, play from the start
			PlayFrom( _bottomBar.CurrentTime );
		}

		bool _hitEnd = true;
		public void PlayFrom (float time, bool restartIfOverflow = true, bool showFlashNotification = true) {
			if (!IsLoading && !IsPlaying) {
				if (_hitEnd && time < 0.1f) {
					NotifyPlayedOnce ();
				}
				if (time + 0.1f > _audioPlayer.Length) {
					_hitEnd = true;
					if (restartIfOverflow) {
						PlayFrom (0, false, showFlashNotification);
						return;
					} else {
						SetState (_data.IsDraft ? CanvasState.Record_PlaybackPaused : CanvasState.Playback_Paused, true);
					}
				} else {
					_audioPlayer.Seek (time);
					SetState (_data.IsDraft ? CanvasState.Record_PlaybackPlaying : CanvasState.Playback_Playing, showFlashNotification);
				}
			}
		}

		void NotifyPlayedOnce ()
		{
			_hitEnd = false;
			MessagingCenter.Send<object> (this, BlrtConstants.FormsMessageId.BlrtPlayedOnce);
		}

		async Task SaveRecord () {
			if (_data.IsIndependent) {
				return;
			}
			using (UserDialogs.Instance.Loading (PlatformHelper.Translate ("Saving..."))) {
				Conversation conversation = null;
				var thumbnailSize = new System.Drawing.Size (256, 256);
				var thumbPage = _data.GetPage (_startRecordingPage) as IThumbnailable;
				if (null == thumbPage) {
					for (int i = 0; i < _data.PageCount; ++i) {
						var page = _data.GetPage (i + 1);

						if (page is IThumbnailable) {					
							thumbPage = page as IThumbnailable;
						}
					}
				}
				/*if (!_data.IsReply) {
					conversation = new Conversation () {
						Name = _data.ConversationName,
						CreationType = CreationType.StandardBlrt
					};

					conversation.LocalThumbnailName = string.Format ("{0}-thumb.jpg", conversation.ObjectId);

					if(_data.RequestArgs != null){
						conversation.AttachedMeta = _data.RequestArgs.AttachedMeta;
						conversation.RequestId = _data.RequestArgs.RequestId;
					}
					if (null != thumbPage) {
						await thumbPage.GenerateThumbnail (conversation.LocalThumbnailPath, thumbnailSize);	
					}

					conversation.ShouldUpload = true;
					conversation.LocalStorage.DataManager.AddUnsynced (conversation);
				} else {*/

				var rootController = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
				var mailbox = rootController.Slave as BlrtiOS.Views.Mailbox.MailboxController;

					var checkIfDataNull = _data;
					var dataConversation = _data.Conversation;

				conversation = BlrtManager.DataManagers.Default.GetConversation (_data.Conversation);
				conversation = mailbox.ConversationScreen.CurrentConversation;
				//}




				var tch = BlrtData.BlrtConstants.Directories.Default.GetDecryptedPath ("cache.tch");
				if (_supportRecordContinue) {
					await StopAudioRecorder ();
					await PreparePostRecordingPlayback ();
				}
				var mic = _audioRecorder.FilePath;



				_canvasView.WriteCanvasData  (tch);

				ConversationBlrtItem content = new ConversationReBlrtItem () { LocalThumbnailNames = new string [1], CloudThumbnailPaths = new Uri [1] };

				if (_data.IsReply) {
					//content = new ConversationReBlrtItem () { LocalThumbnailNames = new string[1], CloudThumbnailPaths = new Uri[1] };
					if (null != thumbPage) {
						content.LocalThumbnailNames[0] = string.Format ("{0}-thumb.jpg", content.ObjectId);
						await thumbPage.GenerateThumbnail (content.LocalThumbnailPath(0), thumbnailSize);
					}
				} else {
					content = new ConversationBlrtItem () {
						PublicBlrtName = _data.PublicBlrtName,
						LocalThumbnailNames = new string [1],
						CloudThumbnailPaths = new Uri [1]
					};
				}

				content.EncryptType		= BlrtEncryptorManager.RecommendedEncyption;
				content.ConversationId	= conversation.ObjectId;

				var blrtIndex			= _data.ToPageIndex ();

				if (conversation.LastMediaPrototype != null) {
					blrtIndex = BlrtMediaPagePrototype.GenerateUnused (
						blrtIndex,
						conversation.LastMediaPrototype
					);
				}
				conversation.ContentUpdatedAt = DateTime.Now;

				content.Media 				= _data.GetMediaAssets();
				content.MediaData 			= new BlrtRecordData (blrtIndex) {
					AudioLength				 	= (int)_audioPlayer.Length
				};

				_audioRecorder.Release ();

				content.LocalTouchDataName =  content.ObjectId + "-touch.tch";
				content.LocalAudioName = content.ObjectId + "-audio." + _audioRecorder.Extension;

				try {
					await BlrtEncryptorManager.Encrypt (
						tch,
						content.LocalTouchDataPath,
						content
					);
					await BlrtEncryptorManager.Encrypt (
						mic,
						content.LocalAudioPath,
						content
					);
				} catch (BlrtEncryptorManagerException enc_ex) {
					if (enc_ex.Code == BlrtEncryptorManagerException.ExceptionCodes.EncryptionFailed) {
						await _platformHelper.ShowAlertAsync (
							"Encryption failed",
							"Unable to encrypt file. Please contact Blrt Support.",
							"OK"
						);
					}
					LLogger.WriteEvent ("encrypt", "error", "encryption failed", "CanvasPageView: audio, video; error: " + enc_ex.ToString ());
					await SetResult (CanvasResult.BackPressed (false));
				}



				content.ShouldUpload = true;
				content.StringifyArgument ();
				conversation.LastMedia			= content.Media;
				conversation.LastMediaArgument	= content.Argument;

				conversation.LocalStorage.DataManager.AddUnsynced (content);
				conversation.RemoveMediaFromUnusedMediaList (content.Media.Where(m => !m.Contains (BlrtTemplate.TemplatePrefix)));

				if (!conversation.OnParse && !_data.IsReply) {
					//if blrt request need to make blrt public, we need to ask the user for public name
					if(_data.CreateWithPublicBlrt)
						await ConfirmPublicName (content);

					var uploader = BlrtManager.Upload.GetUploader (conversation);
					BlrtManager.Upload.RunAsync (uploader);

					await SetResult (CanvasResult.ConversationCreated(conversation.ObjectId, content.ObjectId));
				} else if (!conversation.OnParse && _data.IsReply) {
					//we might need to change this to await for covnersation upload
					var uploader = BlrtManager.Upload.GetUploader (content);
					BlrtManager.Upload.RunAsync (uploader);

					await SetResult (CanvasResult.BlrtCreated(_data, content.ObjectId));
				} else {
					var uploader = BlrtManager.Upload.GetUploader (content);
					BlrtManager.Upload.RunAsync (uploader);

					await SetResult (CanvasResult.BlrtCreated(_data, content.ObjectId));
				}
			}
		}

		async Task ConfirmPublicName (ConversationBlrtItem blrt)
		{
			var name = await GetPublicName (blrt.PublicBlrtName);
			blrt.PublicBlrtName = name;
		}

		async Task<string> GetPublicName (string defaultName)
		{
			#if __iOS__
			var tcs = new TaskCompletionSource<bool> ();
			var alert = new UIKit.UIAlertView (BlrtUtil.PlatformHelper.Translate ("Public Blrt name"),
				BlrtUtil.PlatformHelper.Translate ("Set a name for your public Blrt."),
				null,
				BlrtUtil.PlatformHelper.Translate ("OK")
			);

			alert.AlertViewStyle = UIKit.UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKit.UIKeyboardType.Default;
			alert.GetTextField (0).Placeholder = BlrtUtil.PlatformHelper.Translate ("public_name_placeholder", "conversation_screen");
			alert.GetTextField (0).Text = defaultName;
			alert.Clicked += (object sender, UIKit.UIButtonEventArgs e) => {
				tcs.TrySetResult (true);
			};

			alert.Show ();

			if (!await tcs.Task)
				return "";

			//check that the value given is usable
			var name = alert.GetTextField (0).Text;
			if (string.IsNullOrWhiteSpace (name) || name.Trim ().Length >= 56) {
				var invalidAlert = new UIKit.UIAlertView (BlrtUtil.PlatformHelper.Translate ("public_blrt_name_invalid_title", "preview"),
					BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_message", "preview"),
					null,
					"OK"
				);
				invalidAlert.Show ();
				await BlrtiOS.BlrtHelperiOS.AwaitAlert (invalidAlert);
				return await GetPublicName (name);
			} else {
				return name;
			}
			#else
			var result = await Acr.UserDialogs.UserDialogs.Instance.PromptAsync (new PromptConfig () {
				Title = BlrtUtil.PlatformHelper.Translate ("Public Blrt name"),
				Message = BlrtUtil.PlatformHelper.Translate ("Set a name for your public Blrt."),
				OkText = BlrtUtil.PlatformHelper.Translate ("OK"),
				CancelText = null,
				IsCancellable = false,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("public_name_placeholder", "conversation_screen"),
				Text = defaultName
			});
			if (result.Ok) {
				var name = result.Text.Trim ();
				if (string.IsNullOrWhiteSpace (name) || name.Length >= 56) {
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (
						BlrtUtil.PlatformHelper.Translate ("public_blrt_name_invalid_title", "preview"),
						BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_message", "preview"),
						BlrtUtil.PlatformHelper.Translate ("OK")
					);
					return await GetPublicName (name);
				}
				return name;
			}
			return defaultName;
			#endif
		}

		public void Redraw ()
		{
			_canvasView.Redraw();
		}

		void OnCanvasSingleTapped (object sender, EventArgs e) {
			switch (_currentState) {
				case CanvasState.Playback_Paused:
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPaused:
				case CanvasState.Record_PlaybackPlaying:
					ToggleBarsHidden (true);
					break;
			}
		}

		void ToggleBarsHidden (bool animated) {
			HideBars (!_barsHidden, animated);
		}

		void HideBars (bool shouldHide, bool animated)
		{
			if (shouldHide != _barsHidden) {
				_barsHidden = shouldHide;
				// transition
				var topTransitionYDestination = shouldHide ? -_topBar.Height : 0;
				var bottomTransitionYDestination = shouldHide ? _bottomBar.Height : 0;
				if (animated) {
					_topBar.TranslateTo (0, topTransitionYDestination, 250);
					_bottomBar.TranslateTo (0, bottomTransitionYDestination, 250);
				} else {
					_topBar.TranslationY = topTransitionYDestination;
					_bottomBar.TranslationY = bottomTransitionYDestination;
				}
				// opacity
//				var destinationOpacity = shouldHide ? 0 : 1;
//				if (animated) {
//					_topBar.FadeTo (destinationOpacity);
//					_bottomBar.FadeTo (destinationOpacity);
//				} else {
//					_topBar.Opacity = destinationOpacity;
//					_bottomBar.Opacity = destinationOpacity;
//				}
			}
		}

		void HidePopupViews (ExcludedHidingView excluded)
		{
			_toolbar.	HidePopupViews (excluded);
			_topBar.	HidePopupViews (excluded);
			_bottomBar.	HidePopupViews (excluded);
		}

		const int FlashTimes = 2;
		const string RecBannerAnimationName = "RecBanner_Flash";
		void StartFlashRecordBanner ()
		{
			if (ShouldShowRecordbanner ()) {
				_recBanner.AbortAnimation (RecBannerAnimationName);
				FlashRecordBannerAnimation (FlashTimes);
			}
		}

		void FlashRecordBannerAnimation (int restTimes)
		{
			_recBanner.Animate (
				RecBannerAnimationName,
				(percentage) => {
					var factor = 1 - Math.Abs (percentage - 0.5) / 0.5;
					_recBanner.TextColor = new Xamarin.Forms.Color (
						RecordBannerNormalColor.R + (RecordBannerHighlightColor.R - RecordBannerNormalColor.R) * factor,
						RecordBannerNormalColor.G + (RecordBannerHighlightColor.G - RecordBannerNormalColor.G) * factor,
						RecordBannerNormalColor.B + (RecordBannerHighlightColor.B - RecordBannerNormalColor.B) * factor,
						RecordBannerNormalColor.A + (RecordBannerHighlightColor.A - RecordBannerNormalColor.A) * factor
					);
				},
				16,
				500,
				null,
				(alwaysEqualsOne, animationCanceled) => {
					if (!animationCanceled) {
						--restTimes;
						if (restTimes > 0) {
							FlashRecordBannerAnimation (restTimes);
						}
					}
				}
			);
		}
	}
}

