﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlrtCanvas
{
	public class CanvasTouchCollection {

		public int Count { get { return _touches.Count; } }
	
		List<CanvasTouch> _touches;

		public CanvasTouchCollection(){
			_touches = new List<CanvasTouch> ();
		}

		/// <summary>
		/// Resets all of the touch data so that all of the last states are forgotten, and removes any dead touches
		/// </summary>
		public void ResetState () {

			foreach (var t in _touches) {
				if(t.State == CanvasTouch.CanvasTouchState.Released || t.State == CanvasTouch.CanvasTouchState.Invalid)
					t.Update(CanvasTouch.CanvasTouchState.Invalid, t.X, t.Y);
				else
					t.Update(CanvasTouch.CanvasTouchState.Idle, t.X, t.Y);

			}

			_touches.RemoveAll (t => t.State == CanvasTouch.CanvasTouchState.Invalid);
		}


		public void Update (int id, CanvasTouch.CanvasTouchState state, float x, float y)
		{
			foreach (var t in _touches) {
			
				if (t.Id == id) {

					if (state == CanvasTouch.CanvasTouchState.Idle && (t.X != x || t.Y != y))
						state = CanvasTouch.CanvasTouchState.Moved;

					t.Update(state, x, y);

					return;
				}
			}

			if (CanvasTouch.CanvasTouchState.Pressed == state) {
				var touch = new CanvasTouch (id);
				touch.Update(state, x, y);
				_touches.Add ( touch );
			}
		}

		public CanvasTouch Get (int i)
		{
			return _touches [i];
		}

		public CanvasTouch GetFromId (int id)
		{
			foreach (var t in _touches) {
				if (t.Id == id) {
					return t;
				}
			}

			return null;
		}

		public override string ToString ()
		{
			StringBuilder str = new StringBuilder ();


			foreach (var t in _touches) {

				if (str.Length > 0) {
					str.Append(", ");
				}
				str.Append(t.ToString ());
			}


			return string.Format ("{{\"CanvasTouchCollection\": [{0}]}}", str.ToString());
		}
	}



	public class CanvasTouch
	{
		public CanvasTouchState State { get; private set; }

		public int Id { get; private set; }

		public float X { get; private set; }
		public float Y { get; private set; }


		public float LastX { get; private set; }
		public float LastY { get; private set; }

		public float DeltaX { get { return X - LastX; } }
		public float DeltaY { get { return Y - LastY; } }

		public CanvasTouch (int id) {
			this.Id = id;
			this.State = CanvasTouchState.Invalid;
		}

		public void Update (CanvasTouch.CanvasTouchState state, float x, float y)
		{
			if (state == CanvasTouchState.Invalid || state == CanvasTouchState.Pressed) {
				this.LastX = x;
				this.LastY = y;
			} else {
				this.LastX = this.X;
				this.LastY = this.Y;
			}

			this.State = state;
			this.X = x;
			this.Y = y;
		}

		public override string ToString ()
		{
			return string.Format ("{{\"CanvasTouch\": {{\"State\":\"{0}\", \"Id\":{1}, \"X\":{2}, \"Y\":{3}, \"LastX\":{4}, \"LastY\":{5} }} }}", State, Id, X, Y, LastX, LastY);
		}

		public enum CanvasTouchState : int {
			Invalid,
			Pressed,
			Moved,
			Idle,
			Released
		}
	}
}

