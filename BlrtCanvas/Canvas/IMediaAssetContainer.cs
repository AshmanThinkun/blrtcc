using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using BlrtCanvas;
using BlrtData.Media;
using BlrtData;

namespace BlrtCanvas
{
	public interface IMediaAssetContainer
	{
		bool HasMediaAsset { get; }
		string Asset { get; }

		System.Threading.Tasks.Task CreateAsset ();
		bool SameMedia (IMediaAssetContainer asset);
		void SetMedia (IMediaAssetContainer asset);
	}

}

