﻿using System;
using OpenTK;
using OpenTK.Graphics.ES20;

namespace BlrtCanvas.GLCanvas.Shaders
{
	/// <summary>
	/// abstract base class for OpenGL vertex shader - fragment shader pairs
	/// </summary>
	public abstract class BCShader : IDisposable
	{
		public int FragmentShaderId { get; private set; }

		public int VertexShaderId { get; private set; }

		public int ProgramId { get; private set; }

		public BCShader ()
		{
			FragmentShaderId = VertexShaderId = -1;
		}

		public void Load() 
		{
			if(FragmentShaderId < 0) {
				string src = GetFragmentShaderSource ();
				FragmentShaderId = GLHelper.LoadShader (ShaderType.FragmentShader, src);
			}

			if(VertexShaderId < 0) {
				string src = GetVertexShaderSource ();
				VertexShaderId = GLHelper.LoadShader (ShaderType.VertexShader, src);
			}

		}

		public virtual void AttachShader()
		{
			ProgramId = GL.CreateProgram();
			if (ProgramId == 0)
				throw new InvalidOperationException ("Unable to create program");
			GL.AttachShader (ProgramId, VertexShaderId);
			GL.AttachShader (ProgramId, FragmentShaderId);
			GLHelper.LinkProgram (ProgramId);
		}

		public virtual void DetachShader()
		{
			GL.DetachShader (ProgramId, VertexShaderId);
			GL.DetachShader (ProgramId, FragmentShaderId);
		}

		public virtual void Activate() {
			GL.UseProgram (ProgramId);
		}

		protected abstract string GetFragmentShaderSource ();
		protected abstract string GetVertexShaderSource ();

		public abstract void SetProjectionMatrix (Matrix4 mt);

		/// <summary>
		/// Sets the vertex array. each vertex is represented by 3 float (x, y, z)
		/// </summary>
		/// <param name="vertices">Vertices coordinates.</param>
		public abstract void SetVertexArray3 (float[] vertices);

		/// <summary>
		/// Sets an attribute with a float array
		/// </summary>
		/// <param name="attr">Attribute name</param>
		/// <param name="data">the float array used to set the attribute</param>
		public abstract void SetAttribArray (string attr, float[] data);

		/// <summary>
		/// Draw with the specified mode and previously set vertex array.
		/// </summary>
		/// <param name="mode">Drawing mode.</param>
		/// <param name="count">number of vertices in the vertex array that should be rendered</param>
		public virtual void Draw(BeginMode mode, int count){
			GL.DrawArrays(mode, 0, count);
		}
			
		public interface IBCShaderColor {
			/// <summary>
			/// Sets the vertex color array. each color is represented by 4 float numbers (r, g, b, alpha)
			/// </summary>
			/// <param name="colors">the float array used to contain the color data.</param>
			void SetColorArray4 (float[] colors);
		}

		public interface IBCShaderTextureCoord {
			/// <summary>
			/// Sets the vertex texture coordinate. each coordinate is represented by 2 float numbers (x, y)
			/// </summary>
			/// <param name="coordinates">the float array containing the coordinate</param>
			void SetTextureCoordArray2 (float[] coordinates);
		}

		#region IDisposable implementation

		public void Dispose ()
		{
			DetachShader ();
			GL.DeleteProgram (ProgramId);
			GL.DeleteShader (FragmentShaderId);
			GL.DeleteShader (VertexShaderId);
		}

		#endregion
	}


}

