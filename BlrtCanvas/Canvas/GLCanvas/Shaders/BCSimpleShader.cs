﻿using System;
using OpenTK.Graphics.ES20;
using OpenTK;

namespace BlrtCanvas.GLCanvas.Shaders
{
	/// <summary>
	/// A shader pair which uses SimpleFragment.glsl and SimpleVertex.glsl as shader source
	/// </summary>
	public class BCSimpleShader : BCShader, BCShader.IBCShaderColor, BCShader.IBCShaderTextureCoord
	{
		private const string SHADER_SIMPLE_VERTEX = "SimpleVertex.glsl";
		private const string SHADER_SIMPLE_FRAGMENT = "SimpleFragment.glsl";

		int _positionAttrSlot;	// ID of the vertex position attribute in shader source
		int _colorAttrSlot;		// ID of the vertex color attribute in shader source
		int _texCoordInAttrSlot;// ID of the vertex texture coordinate attribute in shader source
		int _projectionUniform;
		int _modelviewUniform;
		int _texUniform;
		int _elementUniform;
		int _texEnabledUniform;

		public BCSimpleShader ()
		{
		}

		public void SetElement (int element)
		{
			GL.Uniform1 (_elementUniform, element);
		}

		protected override string GetFragmentShaderSource ()
		{
			return GLHelper.GetResourceFileText (SHADER_SIMPLE_FRAGMENT);
		}

		protected override string GetVertexShaderSource ()
		{
			return GLHelper.GetResourceFileText (SHADER_SIMPLE_VERTEX);
		}


		public override void AttachShader ()
		{
			base.AttachShader ();
			_positionAttrSlot 		= GL.GetAttribLocation(ProgramId, "Position");
			_colorAttrSlot 			= GL.GetAttribLocation(ProgramId, "SourceColor");
			_texCoordInAttrSlot		= GL.GetAttribLocation(ProgramId, "TexCoordIn");

			GL.EnableVertexAttribArray(_positionAttrSlot);
			GL.EnableVertexAttribArray(_colorAttrSlot);
			GL.EnableVertexAttribArray(_texCoordInAttrSlot);

			_projectionUniform 		= GL.GetUniformLocation (ProgramId, "Projection");
			_modelviewUniform 		= GL.GetUniformLocation (ProgramId, "Modelview");
			_elementUniform 		= GL.GetUniformLocation (ProgramId, "Element");
			_texUniform 			= GL.GetUniformLocation (ProgramId, "Texture");
			_texEnabledUniform 		= GL.GetUniformLocation (ProgramId, "TexEnabled");
		}

		public override void DetachShader ()
		{
			base.DetachShader ();
			GL.DisableVertexAttribArray(_positionAttrSlot);
			GL.DisableVertexAttribArray(_colorAttrSlot);
			GL.DisableVertexAttribArray(_texCoordInAttrSlot);
			_positionAttrSlot = _colorAttrSlot = _texCoordInAttrSlot = -1;
		}

		public override void SetAttribArray (string attr, float[] data)
		{
			throw new NotImplementedException ();
		}

		public override void SetVertexArray3 (float[] vertices)
		{
			GL.VertexAttribPointer (_positionAttrSlot, 3, VertexAttribPointerType.Float, false, 0, vertices);
		}
			
		public void SetColorArray4 (float[] colors)
		{
			GL.VertexAttribPointer (_colorAttrSlot, 4, VertexAttribPointerType.Float, false, 0, colors);
		}

		public void SetTextureCoordArray2 (float[] coordinates)
		{
			GL.VertexAttribPointer (_texCoordInAttrSlot, 2, VertexAttribPointerType.Float, false, 0, coordinates);
		}

		public override void SetProjectionMatrix (Matrix4 mt)
		{
			GL.UniformMatrix4 (_projectionUniform, false, ref mt);
		}

		public void SetWorldMatrix (ref Matrix4 mt)
		{
			GL.UniformMatrix4 (_modelviewUniform, false, ref mt);
		}

		public void ResetWorldMatrix ()
		{
			GL.UniformMatrix4 (_modelviewUniform, false, ref Matrix4.Identity);
		}

		public void EnableTexture (bool isEnabled)
		{
			if (isEnabled) {
				GL.ActiveTexture (TextureUnit.Texture0);
				GL.Uniform1 (_texUniform, 0);
				GL.Uniform1 (_texEnabledUniform, 1);
			} else {
				GL.Uniform1 (_texEnabledUniform, 0);
			}
		}
	}
}

