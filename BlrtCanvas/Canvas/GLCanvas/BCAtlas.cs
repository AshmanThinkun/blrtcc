﻿using System;
using System.Drawing;
using BlrtCanvas.GLCanvas.Shaders;

namespace BlrtCanvas.GLCanvas
{
	public class BCAtlas
	{
		private BCCanvas _canvas;
		private BCTextureManager _textureManager;

		public BCAtlas (BCCanvas canvas, BCTextureManager textureManager)
		{
			if (null == canvas) {
				throw new ArgumentNullException ("canvas");
			}
			_canvas = canvas;
			_textureManager = textureManager;
		}

		public float ViewportRatio {
			get {
				return _canvas.ViewPortRatio;
			}
		}

		public SizeF Viewport {
			get {
				return _canvas.Viewport;
			}
		}

		public SizeF CurrentPageViewSize {
			get {
				return _canvas.CurrentPageViewSize;
			}
		}

		public RectangleF CameraBounds { get { return _canvas.Camera.Bounds; } }

		public float CurrentTime
		{
			get {
				return _canvas.CurrentTime;
			}
		}

		public float DeltaTime{
			get {
				return _canvas.DeltaTime;
			}
		}

		public CanvasCameraMode CameraMode {
			get {
				return _canvas.CameraMode;
			}
		}


		public bool IsRecording { get { return _canvas.CanvasManager.IsRecording; } }
		public bool IsPlaying { get { return _canvas.CanvasManager.IsPlaying; } }




		public int CurrentPage
		{
			get {
				return _canvas.CurrentPage;
			}
		}

		public float CurrentZoom
		{
			get {
				return _canvas.CurrentZoom;
			}
		}

		public bool IsDirty {
			get {
				return _canvas.IsDirty;
			}
		}

		public void SetDirty()
		{
			_canvas.SetDirty ();
		}

		public void SelectTexture(int texName)
		{
			_canvas.SelectTexture (texName);
		}

		public BCShader GetShader(BCCanvas.ShaderType type)
		{
			return _canvas.GetShader(type);
		}

		public void LoadGLTexture (IBCTexture page)
		{
			// do nothing
			// because texture manager load textures itself based on user's current viewing page
		}

		public int GetGLTexture (IBCTexture page)
		{
			return _textureManager.GetTexture (page);
		}

		public IBCTexture GetLoadingTexture ()
		{
			return _textureManager.Placeholder;
		}

		public bool IsTextureLoaded (BCPageTexture page)
		{
			return _textureManager.IsTextureLoaded (page);
		}
	}
}

