﻿using System;
using BlrtCanvas.GLCanvas.Shapes.Elements.BlrtPages;

namespace BlrtCanvas.GLCanvas
{
	public class PageData
	{
		public int Width;
		public int Height;

		public PageData ()
		{
			Width = 0;
			Height = 0;
		}
	}

	public class BCPageTexture : IBCTexture, IDisposable
	{
		ICanvasPage _page;
		int _width;
		int _height;


		public BCPageTexture (ICanvasPage page, PageData pageData) {
			_page = page;

			if (pageData != null) {
				_width		= pageData.Width;
				_height		= pageData.Height;
			}
		}

		public IBCTextureLoader GetTextureLoader ()
		{
			return _page.GetTextureLoader ();
		}

		public int Width { get { return _width <= 0 ? _page.MediaWidth : _width; } }
		public int Height { get { return _height <= 0 ? _page.MediaHeight : _height; } }

		public TextureRotation Rotation { get { return _page.Rotation; } }

		bool IEquatable<IBCTexture>.Equals (IBCTexture other) {

			var otherPage = other as BCPageTexture;
			return (otherPage != null) && ((otherPage == this) || otherPage._page.Equals (_page));
		}

		public override int GetHashCode ()
		{
			return _page != null ? _page.GetHashCode () : base.GetHashCode();
		}


		public void Dispose () {

			if(_page is IDisposable){
				var d = _page as IDisposable;
				d.Dispose();
			}

			_page = null;
		}
	}
}

