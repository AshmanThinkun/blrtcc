using System;
using System.Collections.Generic;
using OpenTK.Graphics.ES20;
using System.Threading.Tasks;
using System.Threading;

namespace BlrtCanvas.GLCanvas
{
	public class BCTextureManager
	{
		const int BeforePages = 1;	// load how many more pages after current page
		const int AfterPages = 1;	// keep how many more pages before current page

		TaskCompletionSource<IBCTexture> _initTCS;

		Dictionary<IBCTexture, int> _textureList;

		public IBCTexture Placeholder { get; private set;}

		Task _loadingTask;
		object _loadingTaskLock = new object ();
		CancellationTokenSource _loadingTaskCTS;

		IBCTexture[] _pageSequence;

		object _crtIndexLock = new object ();
		int _crtIndex;

		protected BCCanvas Canvas { get; private set; }

		public BCTextureManager (BCCanvas canvas)
		{
			Canvas = canvas;

			Placeholder = new PlaceholderTexture ();

			_textureList = new Dictionary<IBCTexture, int> ();
			_crtIndex = -1;
			_loadingTaskCTS = null;
		}

		public void SetPageSequence (IBCTexture[] pages)
		{
			if (null != _loadingTaskCTS) {
				throw new InvalidOperationException ("cannot set a new page sequence when the old one is not released!");
			}
			if ((null != pages) && (pages.Length > 0)) {
				_pageSequence = pages;
				_loadingTaskCTS = new CancellationTokenSource ();
			} else {
				throw new ArgumentException ();
			}
		}

		public async Task<IBCTexture> InitLoading (int pageNum) 
		{
			if (_initTCS != null) {
				return await _initTCS.Task;
			}
			if (null == _pageSequence || null == _loadingTaskCTS) {
				throw new InvalidOperationException ("Set page sequence first");
			}
			var index = pageNum - 1;
			if (index < 0 || index >= _pageSequence.Length) {
				throw new ArgumentException ();
			}
			if (IsTextureLoaded (_pageSequence[index])) {
				SetCurrentIndex (index);
				return _pageSequence [index];
			} else {
				_initTCS = new TaskCompletionSource<IBCTexture> ();
				SetCurrentIndex (index);
				return await _initTCS.Task;
			}
		}

		public void NotifyCurrentPage (int pageNum)
		{
			if (null == _pageSequence || null == _loadingTaskCTS) {
				throw new InvalidOperationException ("Set page sequence first");
			}
			var index = pageNum - 1;
			if (index < 0 || index >= _pageSequence.Length) {
				throw new ArgumentException ();
			}
			SetCurrentIndex (index);
		}

		void SetCurrentIndex (int index)
		{
			bool changed = false;
			lock (_crtIndexLock) {
				if (_crtIndex != index) {
					_crtIndex = index;
					changed = true;
				}
			}
			if (!changed)
				return;
			lock (_loadingTaskLock) {
				if (_loadingTask == null || _loadingTask.Status == TaskStatus.Canceled || _loadingTask.Status == TaskStatus.Faulted || _loadingTask.Status == TaskStatus.RanToCompletion) {
					_loadingTask?.Dispose ();
					_loadingTask = CreateLoadTexturesTask (_loadingTaskCTS.Token);
				}
			}
		}

		public bool IsTextureLoaded (IBCTexture texture) {
			lock (_textureList) {
				return Unlocked_IsTextureLoaded (texture);
			}
		}

		private bool Unlocked_IsTextureLoaded (IBCTexture texture) {
			if (_textureList.ContainsKey (texture)) {
				return _textureList [texture] >= 0;
			}
			return false;
		}

		public int GetTexture (IBCTexture texture) {

			if (_textureList.ContainsKey (texture)) {
				return _textureList [texture];
			}

			if (Placeholder != null) {


				if(IsTextureLoaded(Placeholder))
					return _textureList [Placeholder];
			}

			return -1;
		}

		public async Task Release () {
			if (null == _pageSequence) {
				// no data yet; return directly
				return;
			}
			_loadingTaskCTS?.Cancel ();
			if (null != _loadingTask) {
				try {
					await _loadingTask;
				} catch {}
			}
			lock (_loadingTaskLock) {
				_loadingTask = null;
			}
			_loadingTaskCTS?.Dispose ();
			_loadingTaskCTS = null;
			var texNames = new List<int> ();
			lock (_textureList) {
				IBCTexture placeholderTx = null;
				int placeholderId = -1;
				foreach (var item in _textureList) {
					if (item.Key is PlaceholderTexture) {
						placeholderTx = item.Key;
						placeholderId = item.Value;
					} else {
						if (Unlocked_IsTextureLoaded (item.Key)) {
							texNames.Add(item.Value);
						}
					}
				}
				_textureList.Clear ();
				if (null != placeholderTx && placeholderId >= 0) {
					_textureList.Add (placeholderTx, placeholderId);
				}
			}
			if (texNames.Count > 0) {
				var tcs = new TaskCompletionSource<bool> ();
				Canvas.ActionQueue += () => {
					GL.DeleteTextures (texNames.Count, texNames.ToArray());
					tcs.SetResult (true);
					Canvas.CanvasManager.Redraw();
				};
				Canvas.CanvasManager.Redraw();
				await tcs.Task;
			}
			_initTCS = null;
			_pageSequence = null;
			_crtIndex = -1;
		}

		public void UnloadTexture (IBCTexture texture) {

			if (IsTextureLoaded (texture)) {
				GL.DeleteTexture (_textureList [texture]);
				Canvas.CanvasManager.Redraw();
				_textureList [texture] = -1;
			}

		}

		IBCTexture GetNextLoadingTexture()
		{
			lock (_textureList) {
				// placeholder first
				if (null != Placeholder) {
					if (!IsTextureLoaded (Placeholder)) {
						return Placeholder;
					}
				}
				// load pages arround current page
				lock (_crtIndexLock) {
					if (!IsTextureLoaded (_pageSequence[_crtIndex])) {
						return _pageSequence [_crtIndex];
					}
					for (int i = 1; i <= AfterPages && _crtIndex + i < _pageSequence.Length; ++i) {
						if (!IsTextureLoaded (_pageSequence[_crtIndex + i])) {
							return _pageSequence [_crtIndex + i];
						}
					}
					for (int i = 1; i <= BeforePages && _crtIndex - i >= 0; ++i) {
						if (!IsTextureLoaded (_pageSequence[_crtIndex - i])) {
							return _pageSequence [_crtIndex - i];
						}
					}
				}
				return null;
			}
		}

		IBCTexture GetNextUnloadingTexture ()
		{
			lock (_textureList) {
				int count = 0;
				foreach (var item in _textureList) {
					if (item.Value >= 0) {
						++count;
					}
				}
				if (count > BeforePages + AfterPages + 2) { // 2 means: 1 placeholder texture + 1 texture for current page
					int distance = 0;
					IBCTexture target = null;
					lock (_crtIndexLock) {
						foreach (var item in _textureList) {
							if ((item.Value >= 0) && (!item.Key.Equals (Placeholder))) {
								var disInSeq = _pageSequence.Length;
								for (int i = 0; i < _pageSequence.Length; i++) {
									if (_pageSequence[i].Equals (item.Key)) {
										var abs = Math.Abs (i - _crtIndex);
										if (abs < disInSeq) {
											disInSeq = abs;
										}
									}
								}
								if (disInSeq > distance) {
									distance = disInSeq;
									target = item.Key;
								}
							}
						}
					}
					return target;
				}
			}
			return null;
		}

		void CheckCancel (CancellationToken cancelToken, IDisposable obj = null) {
			if (cancelToken.IsCancellationRequested) {
				obj?.Dispose ();
				cancelToken.ThrowIfCancellationRequested ();
			}
		}

		Task CreateLoadTexturesTask (CancellationToken cancelToken) {
			return Task.Run (async () => {
				while (true) {
					CheckCancel (cancelToken);
					IBCTexture loadingTex = GetNextLoadingTexture();
					CheckCancel (cancelToken);
					if (null == loadingTex) {
						return;
					}

					// check if the texture is already loaded again
					if (!IsTextureLoaded (loadingTex)) {
						CheckCancel (cancelToken);
						// get a loader
						using (var loader = loadingTex.GetTextureLoader ()) {
							CheckCancel (cancelToken, loader);
							try {
								// load everything into memory that is needed, like the bitmap or the pdf page
								var result = await loader.Load ();
								CheckCancel (cancelToken, loader);
								if (result) {
									var tcsGlLoadTexture = new TaskCompletionSource<int> ();
									Canvas.ActionQueue += () => {
										try {
											//load it into memory
											tcsGlLoadTexture.SetResult (loader.LoadIntoOpenGL ());
										} catch (Exception e) {
											tcsGlLoadTexture.SetException (e);
										}
									};
									Canvas.CanvasManager.Redraw();
									var id = await tcsGlLoadTexture.Task;
									CheckCancel (cancelToken, loader);
									lock (_textureList) {
										if (_textureList.ContainsKey (loadingTex)) {
											_textureList [loadingTex] = id;
										} else {
											_textureList.Add (loadingTex, id);
										}
									}
									if ((null != _initTCS) && !(loadingTex is PlaceholderTexture)) {
										_initTCS.SetResult (loadingTex);
										_initTCS = null;
									}
									CheckCancel (cancelToken, loader);
									var unloadingTex = GetNextUnloadingTexture ();
									CheckCancel (cancelToken, loader);
									if (null != unloadingTex) {
										var tcsUnloadTexture = new TaskCompletionSource<int> ();
										Canvas.ActionQueue += () => {
											UnloadTexture (unloadingTex);
											tcsUnloadTexture.SetResult (0);
										};
										Canvas.CanvasManager.Redraw();
										await tcsUnloadTexture.Task;
									}
								}
							} catch (OperationCanceledException oce) {
								// if the task is canceled, throw this exception to notify
								throw oce;
							} catch (Exception e) {
								LLogger.WriteEvent ("TextureLoader", "LoadTexture", "Exception", e.ToString ());
							}
						}
					}
				}
			});
		}


		private class PlaceholderTexture : IBCTexture {


			public IBCTextureLoader GetTextureLoader ()
			{
				return CanvasPageView.PlatformHelper.GetTiledImageAssetTextureLoader ("texture_placeholder.png");
			}

			public int Width { get { return 16; } }
			public int Height { get { return 16; } }


			public bool Equals (IBCTexture other)
			{
				return other is PlaceholderTexture;



			}

			public TextureRotation Rotation { get { return TextureRotation.Angle_0; } }
		}
	}

	public enum TextureRotation : int // following the Exif orientation tag: http://sylvana.net/jpegcrop/exif_orientation.html
	{
		Angle_0 						= 1,
		Angle_0_Flipped_Horizontally	= 2,
		Angle_180 						= 3,
		Angle_180_Flipped_Horizontally	= 4,
		Angle_270_Flipped_Vertically	= 5,
		Angle_270 						= 6,
		Angle_90_Flipped_Vertically 	= 7,
		Angle_90 						= 8
	}

	public interface IBCTexture : IEquatable<IBCTexture> {
		int Width { get; }
		int Height { get; }
		IBCTextureLoader GetTextureLoader();
		TextureRotation Rotation { get; }
	}

	public interface IBCTextureLoader : IDisposable 
	{
		Task<bool> Load();
		int LoadIntoOpenGL();
	}

}

