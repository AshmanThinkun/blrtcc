﻿using System;
using System.Collections.Generic;
using System.Drawing;
using BlrtCanvas.Util.Serialization;
using BlrtData;

namespace BlrtCanvas.GLCanvas
{
	public class TouchGesturePoint
	{
		public float x, y, timestamp;

		public TouchGesturePoint (float x, float y, float timestamp)
		{
			this.x = x;
			this.y = y;
			this.timestamp = timestamp;
		}



		public static TouchGesturePoint Between (TouchGesturePoint p1, TouchGesturePoint p2, float timestamp){

			if (p1.timestamp > timestamp)
				return p1;
			if (p2.timestamp < timestamp)
				return p2;

			float t = (timestamp - p1.timestamp) / (p2.timestamp - p1.timestamp);

			if (float.IsNaN (t)) t = 0;

			return new TouchGesturePoint (
				p1.x + (p2.x - p1.x) * t,
				p1.y + (p2.y - p1.y) * t,
				timestamp
			);
		}
		public static bool IsPredictable (float scale, TouchGesturePoint p1, TouchGesturePoint p2, TouchGesturePoint point){

			var p = Between (p1, p2, point.timestamp);

			float s = (scale * scale);

			return ((p.x - point.x) * (p.x - point.x) + (p.y - point.y) * (p.y - point.y)) * s < 0.01f;
		}
	}

	public class TouchGesture
	{
		public int 	Thickness		{ get; set; }
		public float Scale			{ get; set; }
		public BlrtBrushColor Color	{ get; set; }
		public int Page				{ get; set; }

		public BlrtData.BlrtBrush Type	{ get; set; }

		List<float>					_displayToggle;
		List<TouchGesturePoint>	_points;

		bool _cancelled;

		bool	_isEditing;
		bool	_dirty;
		float	_startTime;
		float	_endTime;

		public bool IsEditing	{ get { return _isEditing; } set { _isEditing = value; _dirty = true; } }

		public int TotalPointCount	{	get { return _points.Count;	} }

		public float StartedAt	{	get { Clean (); return _startTime;	} }
		public float EndedAt	{	get { Clean (); return _endTime;	} }

		public bool Cancelled	{	get { return _cancelled;		} }

		public float LastChanged { get{ return _displayToggle.Count > 0 ? _displayToggle [_displayToggle.Count - 1] : EndedAt; } }

		public bool CanUndo { get; set; }

		public TouchGesture () {
			_dirty = true;
			_cancelled = false;
			_displayToggle = new List<float>();
			_points = new List<TouchGesturePoint> ();
		}

		void Clean ()
		{
			if (!_dirty)
				return;

			if(_cancelled){ 
				_startTime = _endTime = 0;

			} else if (_points.Count > 0) {

				lock (_points) {
					_startTime = _points [0].timestamp;

					if (IsEditing)
						_endTime = float.MaxValue;
					else
						_endTime = _points [_points.Count - 1].timestamp;
				}
			} else {
				_startTime = _endTime = float.MaxValue;
			}
			_dirty			= false;
		}

		public void Cancel() {
			_cancelled = true;
			_dirty = true;
		}

		public void AddPoint (PointF point, float timestamp)
		{
			AddPoint (new TouchGesturePoint(point.X, point.Y, timestamp));
		}

		public void Undo(float time) {
			if (_displayToggle.Count % 2 == 0 && (_displayToggle.Count == 0 || _displayToggle [_displayToggle.Count - 1] < time))
				_displayToggle.Add (time);
		}

		public void Redo(float time) {
			if (_displayToggle.Count % 2 == 1 && _displayToggle [_displayToggle.Count - 1] < time)
				_displayToggle.Add (time);
		}

		public void AddPoint (TouchGesturePoint point)
		{
			lock (_points) {
				if (_points.Count > 1) {
					var p1 = _points [_points.Count - 2];
					var p2 = _points [_points.Count - 1];
			
					if (TouchGesturePoint.IsPredictable (Scale, p1, point, p2))
						_points.RemoveAt (_points.Count - 1);
				}

				_points.Add (point);
				_dirty = true;
			}
		}

		public bool IsVisableAt(int page, float time){

			if (_cancelled 
				|| TotalPointCount == 0 
				|| page != Page 
				|| (!IsEditing && StartedAt > time)
			)
				return false;
		
			int counter = 0;

			for (int i = 0; i < _displayToggle.Count; ++i) {
				if (_displayToggle [i] <= time)
					++counter;
			}

			return counter % 2 == 0;
		}

		public TouchGesturePoint Get (int index)
		{
			lock (_points) {
				return _points [index];
			}
		}

		public TouchGesturePoint GetAt(float time){

			if (StartedAt > time || _points.Count < 2)
				return _points[0];

			int count = GetPointCount(time);

			lock (_points) {
				return TouchGesturePoint.Between (_points [count - 2], _points [count - 1], time);
			}
		}

		public int GetPointCount (float time)
		{
			if (StartedAt > time)
				return 0;

			lock (_points) {
				for (int i = 1; i < _points.Count; ++i) {
					if (_points [i].timestamp > time)
						return i + 1;
				}
			}

			return _points.Count;
		}

		public void ScalePointTime (float scale, float start)
		{
			if ((scale > 0) && (start >= 0)) {
				for (int i = 0; i < _points.Count; ++i) {
					_points[i].timestamp = scale * (_points [i].timestamp - start) + start;
				}
			}
		}

		public void ScaleDisplayToggle (float scale, float start, float end)
		{
			if ((scale > 0) && (start >= 0) && (end >= start)) {
				for (int i = 0; i < _displayToggle.Count; ++i) {
					if ((start <= _displayToggle[i]) && (_displayToggle[i] <= end)) {
						_displayToggle[i] = scale * (_displayToggle[i] - start) + start;
					}
				}
			}
		}

		public static BlrtUnpackedCanvasData.BlrtElementData PackData (TouchGesture data) {

			var output = new BlrtUnpackedCanvasData.BlrtElementData () {
				color				= (int)data.Color,
				page				= data.Page,
				thickness			= data.Thickness,
				brushType			= (int)data.Type,
				removedToggle		= data._displayToggle.ToArray()
			};

			output.points = new BlrtUnpackedCanvasData.BlrtElementDataPoint[data._points.Count];

			for (int i = 0; i < data._points.Count; ++i) {

				output.points[i] = new BlrtUnpackedCanvasData.BlrtElementDataPoint (){
					x			= data._points [i].x,
					y			= -data._points [i].y,
					timestamp	= data._points [i].timestamp,
					scale		= data.Scale
				};
			}
			return output;
		}


		public static TouchGesture UnpackData (BlrtUnpackedCanvasData.BlrtElementData data, bool invertedVertical) {

			var output = new TouchGesture () {
				Color				= (BlrtBrushColor)data.color,
				Page				= data.page,
				Scale				= data.points[0].scale,
				Thickness			= data.thickness,
				Type				= (BlrtBrush)data.brushType,
				_displayToggle		= new List<float>(data.removedToggle)
			};

			for (int i = 0; i < data.points.Length; ++i) {
				output.AddPoint (new TouchGesturePoint (
					data.points [i].x,
					invertedVertical ? -data.points [i].y : data.points [i].y,
					data.points [i].timestamp
				));
			}
			return output;
		}




		float _minX;
		float _minY;
		float _maxX;
		float _maxY;

		public void UpdateCameraOffset (PointF point)
		{
			_minX = Math.Min (point.X, _minX);
			_maxX = Math.Max (point.X, _maxX);

			_minY = Math.Min (point.Y, _minY);
			_maxY = Math.Max (point.Y, _maxY);
		}

		public PointF GetCameraMinOffset(){
			return new PointF (_minX, _minY);
		}
		public PointF GetCameraMaxOffset(){
			return new PointF (_maxX, _maxY);
		}
	}
}

