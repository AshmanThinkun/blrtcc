﻿using System;

namespace BlrtCanvas.GLCanvas.Shapes
{
	public interface IElement : IDisposable
	{
		bool Display { get; }


		int Layer	{ get; }
		int Page	{ get; }

		float StartAt { get; }

		void Update();
		void Draw();

		void SetAtlas(BCAtlas altas);
	}
}

