﻿using System;
using OpenTK.Graphics.ES20;
using BlrtCanvas.GLCanvas.Shaders;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public  class BCShape : BCBrush
	{
		public BCShape (TouchGesture guesture) : base(guesture)
		{
		}

		protected void DrawCurrentWithSimpleShader (float[] data) 
		{
			var verticesCount = data.Length / 3;
			var color = GLHelper.Vec4ToFloatArray (verticesCount, GLHelper.GetColor (Guesture.Color));
			var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;
			shader.EnableTexture (false);
			shader.ResetWorldMatrix ();
			shader.SetVertexArray3 (data);
			shader.SetColorArray4 (color);
			shader.Draw (BeginMode.TriangleStrip, verticesCount);
		}
	}
}

