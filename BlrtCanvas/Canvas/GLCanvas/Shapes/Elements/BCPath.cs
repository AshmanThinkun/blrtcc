﻿using System;
using BlrtCanvas.GLCanvas.Shapes.Helpers;
using BlrtCanvas.GLCanvas.Shaders;
using OpenTK.Graphics.ES20;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCPath : BCShape
	{
		float[] _path;
		TouchGesturePoint _position;

		public BCPath (TouchGesture guesture) : base(guesture)
		{
		}

		static float [] GetTexCoords (int arrayFloatCount)
		{
			var vertices = arrayFloatCount / 3;
			var coordArray = new float [2 * vertices];

			coordArray [0] = coordArray [(vertices - 2) * 2 + 0] = 1;
			coordArray [1] = coordArray [(vertices - 2) * 2 + 1] = 1;

			coordArray [2] = coordArray [(vertices - 1) * 2 + 0] = 1;
			coordArray [3] = coordArray [(vertices - 1) * 2 + 1] = -1;

			for (int i = 2; i < vertices - 2; i++) {
				coordArray [i * 2 + 0] = 0;
				coordArray [i * 2 + 1] = (i % 2 == 0) ? 1 : -1;
			}
			return coordArray;
		}

		public override void Update ()
		{
			base.Update ();

			if (Display) {

				var p = Guesture.GetAt (Atlas.CurrentTime);

				if (_path == null || (_position.x != p.x || _position.y != p.y)) {

					_path = null;
					_position = p;
					_path = BCPathHelper.CompilePath (Guesture, Width, Atlas.CurrentTime);
				}
			}
		}

		public override void Draw ()
		{
			base.Draw ();

			if (_path != null && _path.Length > 3) {
				var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;
				shader.SetElement (0);
				GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
				GL.Enable (EnableCap.Blend);
				var texCoords = GetTexCoords (_path.Length);
				shader.SetTextureCoordArray2 (texCoords);
				DrawCurrentWithSimpleShader (_path);
			}
		}
	}
}

