﻿using System;
using BlrtCanvas.GLCanvas;
using BlrtCanvas.GLCanvas.Shapes;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCBrush : IElement
	{

		protected const float ANTI_ALIASING_PADDING = 1.9f;
		protected BCAtlas Atlas { get; private set; }


		private float _lastTime;

		public virtual bool Display		{ get; set; }
		public virtual int Layer		{ get { return BCElementManager.SHAPE_LAYER; } }

		public int Page { get { return Guesture.Page; } }
		public float StartAt { get { return Guesture.StartedAt; } }

		public virtual float Width { get { return Guesture.Thickness * ANTI_ALIASING_PADDING / Guesture.Scale; } }

		public TouchGesture Guesture { get; private set; }

		public BCBrush (TouchGesture guesture)
		{
			Guesture = guesture;


		//	ColorRGBA		= data.Color;
		}

		public virtual void Update ()
		{
			bool display = ShouldDisplay();

			//because after end of time, we wont be updating the object
			float time = Math.Min (Atlas.CurrentTime, Guesture.EndedAt);

			if (display != Display || (display && _lastTime != time)){
				Atlas.SetDirty ();
				Display = display;
			}

			_lastTime = time;
		}

		public virtual void Draw ()
		{
		}

		protected virtual bool ShouldDisplay()
		{
			return Guesture.IsVisableAt(Atlas.CurrentPage, Atlas.CurrentTime);
		}

		public void SetAtlas (BCAtlas altas)
		{
			Atlas = altas;
		}

		public virtual void Dispose ()
		{
			if(Guesture != null){
				Guesture = null;
			}

			Atlas = null;
		}
	}
}

