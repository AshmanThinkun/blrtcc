﻿using System;
using OpenTK.Graphics.ES20;
using OpenTK;
using BlrtCanvas.GLCanvas.Shapes.Helpers;
using BlrtCanvas.GLCanvas.Shaders;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCPointer : BCBrush
	{
		//		readonly float _imgWidth;
		//		readonly float _imgHeight;
		//		readonly float _ratio;
		private float [] _data;
		private float [] _color;

		private static readonly float [] _texCoor;

		private const float POINTER_SIZE = 34f;
		private const int SEGMENT_COUNT = 64;

		/* Each segment has one vertex; center point is also a vertex.
		   To complete pointer first vertex needs to be redrawn
		*/
		private const int VERTEX_COUNT = (SEGMENT_COUNT + 1) + 1;

		static BCPointer ()
		{

			_texCoor = new float [2 * VERTEX_COUNT];
			for (int i = 0; i < _texCoor.Length; i++) {
				_texCoor [i] = 1.0f;
			}
			_texCoor [1] = 0.0f;
		}

		public BCPointer (TouchGesture guesture) : base (guesture)
		{
			_color = GLHelper.Vec4ToFloatArray (VERTEX_COUNT, GLHelper.GetColor (guesture.Color));
			_data = BCCircleHelper.GetCircle (0, 0, POINTER_SIZE, POINTER_SIZE * 1.2f, SEGMENT_COUNT);
		}

		public override void Dispose ()
		{
			_data = null;
			_color = null;
			base.Dispose ();
		}


		protected override bool ShouldDisplay ()
		{
			return base.ShouldDisplay () && (Guesture.IsEditing || Atlas.CurrentTime <= Guesture.EndedAt);
		}

		public override void Draw ()
		{
			base.Draw ();

			var p = Guesture.GetAt (Atlas.CurrentTime);

			var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;

			float scale;// = Math.Min(Atlas.CameraBounds.Width, Atlas.CameraBounds.Height) * POINTER_SIZE;
			var pageViewSize = Atlas.CurrentPageViewSize;
			float pageViewRatio = pageViewSize.Width / pageViewSize.Height;
			if (pageViewRatio >= Atlas.ViewportRatio) {
				scale = pageViewSize.Width * CanvasPageView.PlatformHelper.ScreenScale / Atlas.Viewport.Width / Atlas.CurrentZoom;
			} else {
				scale = pageViewSize.Height * CanvasPageView.PlatformHelper.ScreenScale / Atlas.Viewport.Height / Atlas.CurrentZoom;
			}
			var mvMatrix = Matrix4.Scale (scale) * Matrix4.CreateTranslation (p.x, p.y, 0);

			shader.SetWorldMatrix (ref mvMatrix);

			shader.EnableTexture (false);
			GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GL.Enable (EnableCap.Blend);
			shader.SetVertexArray3 (_data);
			shader.SetColorArray4 (_color);
			shader.SetElement (1);
			shader.SetTextureCoordArray2 (_texCoor);
			shader.Draw (BeginMode.TriangleFan, VERTEX_COUNT);
		}
	}
}