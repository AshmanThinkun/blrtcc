﻿using System;
using BlrtCanvas.GLCanvas.Shaders;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCSquare : BCShape
	{
		public BCSquare (TouchGesture guesture) : base(guesture)
		{
		}

		public override void Draw ()
		{
			base.Draw ();


			var p1 = Guesture.Get (0);
			var p2 = Guesture.GetAt (Atlas.CurrentTime);

			float halfWidth = Width / ANTI_ALIASING_PADDING * 0.5f;
			float right, top, left, bottom;
			if (p1.x > p2.x) {
				right = p1.x;
				left = p2.x;
			} else {
				left = p1.x;
				right = p2.x;
			}
			if (p1.y > p2.y) {
				top = p1.y;
				bottom = p2.y;
			} else {
				bottom = p1.y;
				top = p2.y;
			}
			var vertices = new float[] {
				right + halfWidth, top + halfWidth, 0f,
				right - halfWidth, top - halfWidth, 0f,
				right + halfWidth, bottom - halfWidth, 0f,
				right - halfWidth, bottom + halfWidth, 0f,
				left - halfWidth, bottom - halfWidth, 0f,
				left + halfWidth, bottom + halfWidth, 0f,
				left - halfWidth, top + halfWidth, 0f,
				left + halfWidth, top - halfWidth, 0f,
				right + halfWidth, top + halfWidth, 0f,
				right - halfWidth, top - halfWidth, 0f
			};

			var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;
			shader.SetElement (3);

			// Draw
			DrawCurrentWithSimpleShader (vertices);
		}
	}
}

