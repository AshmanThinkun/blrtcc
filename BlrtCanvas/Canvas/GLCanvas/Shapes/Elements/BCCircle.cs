﻿using System;
using BlrtCanvas.GLCanvas.Shapes.Helpers;
using BlrtCanvas.GLCanvas.Shaders;
using OpenTK.Graphics.ES20;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCCircle : BCShape
	{
		public BCCircle(TouchGesture guesture) : base(guesture)
		{
		}

		static float [] GetTexCoords (int arrayFloatCount)
		{
			var vertices = arrayFloatCount / 3;
			var coordArray = new float [2 * vertices];

			for (int i = 0; i < vertices; i++) {
				coordArray [i * 2 + 0] = 0;
				coordArray [i * 2 + 1] = (i % 2 == 0) ? 1 : -1;
			}
			return coordArray;
		}

		public override void Draw ()
		{
			base.Draw ();

			var p1 = Guesture.Get (0);
			var p2 = Guesture.GetAt (Atlas.CurrentTime);


			var radiusX = Math.Abs(p1.x - p2.x) * 0.5f;
			var radiusY = Math.Abs(p1.y - p2.y) * 0.5f;
			var centerX = (p1.x + p2.x) * 0.5f;
			var centerY = (p1.y + p2.y) * 0.5f;
			var data = BCCircleHelper.GetCircleLine (centerX, centerY, radiusX, radiusY, Width, 128);

			var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;
			var arrayFloatCount = data.Length;

			var texCoords = GetTexCoords (arrayFloatCount);
			shader.SetTextureCoordArray2 (texCoords);
			shader.SetElement (2);
			GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GL.Enable (EnableCap.Blend);
			DrawCurrentWithSimpleShader (data);
		}
	}
}

