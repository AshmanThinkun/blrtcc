﻿using System;
using System.IO;
using OpenTK;
using OpenTK.Graphics.ES20;
using BlrtCanvas.GLCanvas.Shaders;

namespace BlrtCanvas.GLCanvas.Shapes.Elements.BlrtPages
{
	public class BCPage : IElement
	{
		private BCAtlas _atlas;

		float[] _triangleVertices;
		float[] _triangleTexCoord;
		float[] _color;
		BCPageTexture _texture;

		bool _lastDisplay;

		int _pageNumber;


		public int Layer { get { return BCElementManager.PAGE_LAYER; } }

		public float StartAt { get { return 0; } }

		public float EndedTime { get { return float.MaxValue; } }

		public int Page { get { return _pageNumber; } }

		public int Width { get { return _texture.Width; } }

		public int Height { get { return _texture.Height; } }


		public bool Display {
			get {
				return (_atlas.CurrentPage == Page);
			}
		}

		public BCPage (BCAtlas atlas, int pageNumber, BCPageTexture page)
		{
			_lastDisplay = false;

			_atlas = atlas;
			_pageNumber = pageNumber;
			// load texture
			_texture = page;


			// prepare vertices data
			_triangleVertices = null;
			_triangleTexCoord = null;
			_color = null;
			if (null == _triangleVertices || null == _triangleTexCoord || null == _color) {
				_triangleTexCoord = GLHelper.GetTexCoordinate (_texture.Rotation);
				var halfWidth = Width * 0.5f;
				var halfHeight = Height * 0.5f;
				_triangleVertices = new float[] {
					-halfWidth, halfHeight, 0.0f,
					-halfWidth, -halfHeight, 0.0f,
					halfWidth, halfHeight, 0.0f,
					halfWidth, -halfHeight, 0.0f
				};
				_color = GLHelper.Vec4ToFloatArray (_triangleVertices.Length / 3, 1.0f, 1.0f, 1.0f, 1.0f);
			}
		}

		public void Update ()
		{
			if (null == _atlas) {
				throw new InvalidOperationException ("Helper has not been set");
			}
			if (_lastDisplay !=	Display) {
				_lastDisplay =	Display;

				if (Display)
					_atlas.LoadGLTexture (_texture);
	
				_atlas.SetDirty ();
			}
		}

		public void Draw ()
		{
			var shader = _atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;

			shader.EnableTexture (true);
			GL.Disable (EnableCap.Blend);
			shader.ResetWorldMatrix ();

			shader.SetVertexArray3 (_triangleVertices);
			shader.SetColorArray4 (_color);

			if (_atlas.IsTextureLoaded (_texture)) {
				int textureId = _atlas.GetGLTexture (_texture);
				_atlas.SelectTexture (textureId);

				shader.SetTextureCoordArray2 (_triangleTexCoord);
			} else {
				var texture = _atlas.GetLoadingTexture ();
				int textureId = _atlas.GetGLTexture (texture);
				_atlas.SelectTexture (textureId);		

				float w = _texture.Width / texture.Width * 0.4f;
				float h = _texture.Height / texture.Height * 0.4f;

				shader.SetTextureCoordArray2 (new float [] {
					0, 0,
					0, h,
					w, 0,
					w, h
				});
			}

			shader.Draw (BeginMode.TriangleStrip, _triangleVertices.Length / 3);
		}

		public void SetAtlas (BCAtlas altas)
		{
			_atlas = altas;
		}

		public virtual void Dispose ()
		{
			_texture.Dispose ();
			_texture = null;
			_triangleVertices = null;
			_triangleTexCoord = null;
			_color = null;
		}
	}
}

