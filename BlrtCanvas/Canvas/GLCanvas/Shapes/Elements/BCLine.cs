﻿using System;
using BlrtCanvas.GLCanvas.Shapes.Helpers;
using BlrtCanvas.GLCanvas.Shaders;
using OpenTK.Graphics.ES20;

namespace BlrtCanvas.GLCanvas.Shapes.Elements
{
	public class BCLine : BCShape
	{
		public BCLine (TouchGesture guesture) : base(guesture)
		{
		}

		public override void Draw ()
		{
			base.Draw ();


			var p1 = Guesture.Get (0);
			var p2 = Guesture.GetAt (Atlas.CurrentTime);


			float deltaX, deltaY;
			BCPathHelper.SquareParamFromLine (p1.x, p1.y, p2.x, p2.y, Width, out deltaX, out deltaY);
			var vertices = new float [] {
				p1.x - deltaX, p1.y + deltaY, 0.0f, //top-left
		        p1.x + deltaX, p1.y - deltaY, 0.0f, //bottom-left
		        p2.x - deltaX, p2.y + deltaY, 0.0f, //top-right
		        p2.x + deltaX, p2.y - deltaY, 0.0f  //bottom-right
			};

			// Draw
			var shader = Atlas.GetShader (BCCanvas.ShaderType.SimpleShader) as BCSimpleShader;
			shader.SetElement (4);
			GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GL.Enable (EnableCap.Blend);
			var coordArray = new float [2 * vertices.Length];

			coordArray [0] = -1;
			coordArray [1] = 1;

			coordArray [2] = -1;
			coordArray [3] = -1;

			coordArray [4] = 1;
			coordArray [5] = 1;

			coordArray [6] = 1;
			coordArray [7] = -1;

			shader.SetTextureCoordArray2 (coordArray);
			DrawCurrentWithSimpleShader (vertices);
		}
	}
}

