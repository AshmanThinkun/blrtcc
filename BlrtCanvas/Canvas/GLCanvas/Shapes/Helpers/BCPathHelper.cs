﻿using System;
using System.Collections.Generic;
using BlrtCanvas.GLCanvas.Shapes.Elements;

namespace BlrtCanvas.GLCanvas.Shapes.Helpers
{

	public static class BCPathHelper
	{
		/// <summary>
		/// Compiles the line data; calculate the vertices that should be rendered to show the line at the specified time; and return a GL float array which contains such data.
		/// </summary>
		/// <returns>GL float array</returns>
		/// <param name="data">line information</param>
		/// <param name="time">current playing position</param>
		public static float[] CompilePath(TouchGesture guesture, float width, float time) {
			if (guesture == null || width<= 0 || guesture.TotalPointCount <= 0) {
				throw new ArgumentException ();
			}
			int countAt = guesture.GetPointCount(time);
			var glPathData = new float[countAt * 6];


			for (int i = 1; i < countAt; i++) {


				TouchGesturePoint p1 = guesture.Get(i - 1);
				TouchGesturePoint p2 = guesture.Get(i);


				if (time < p2.timestamp) {
					// the vertex before the specified time has been rendered, but the vertex after the specified time should not be rendered
					// to make the animation smooth, we should create a temp vertex between the 2 vertices

					p2 = TouchGesturePoint.Between (p1, p2, time);
				}

				float x1 = p1.x, y1 = p1.y;
				float x2 = p2.x, y2 = p2.y;

				if (i > 1 && x1 == x2 && y1 == y2) {

					glPathData [i * 6 + 0] = glPathData [i * 6 - 6];
					glPathData [i * 6 + 1] = glPathData [i * 6 - 5];
					glPathData [i * 6 + 2] = glPathData [i * 6 - 4];
					glPathData [i * 6 + 3] = glPathData [i * 6 - 3];
					glPathData [i * 6 + 4] = glPathData [i * 6 - 2];
					glPathData [i * 6 + 5] = glPathData [i * 6 - 1];

					continue;
				}

				// create a rectangle for each line segment
				float deltaX, deltaY;
				SquareParamFromLine (x2, y2, x1, y1, width, out deltaX, out deltaY);

				if (1 == i) {
					glPathData[0 + 0] = x1 - deltaX;
					glPathData[0 + 1] = y1 + deltaY;
					glPathData[0 + 3] = x1 + deltaX;
					glPathData[0 + 4] = y1 - deltaY;
					glPathData[0 + 2] = glPathData[0 + 5] = 0;
				}

				glPathData[i * 6 + 0] = x2 - deltaX;
				glPathData[i * 6 + 1] = y2 + deltaY;
				glPathData[i * 6 + 3] = x2 + deltaX;
				glPathData[i * 6 + 4] = y2 - deltaY;
				glPathData[i * 6 + 2] = glPathData[i * 6 + 5] = (0);
			}
			return glPathData;
		}

		public static void SquareParamFromLine(float p0X, float p0Y, float p1X, float p1Y, float width, out float deltaX, out float deltaY)
		{
			var halfLineW = width * 0.5f;

			SquareParamFromLine(p1X - p0X, p1Y - p0Y, out deltaX, out deltaY);

			deltaX *= halfLineW;
			deltaY *= halfLineW;
		}

		public static void SquareParamFromLine(float pX, float pY, out float deltaX, out float deltaY)
		{
			if (pX == 0) {

				deltaX = Math.Sign(pY);
				deltaY = 0;

			} else if (pY == 0) {

				deltaX = 0;
				deltaY = Math.Sign(pX);

			} else {

				var tanOppoAngle	= pY / pX;
				var hypotenuse		= (float)Math.Sqrt (1 + tanOppoAngle * tanOppoAngle);
				deltaY				= Math.Sign(pX) / hypotenuse;
				deltaX				= tanOppoAngle * deltaY;

			}
		}
	}
}

