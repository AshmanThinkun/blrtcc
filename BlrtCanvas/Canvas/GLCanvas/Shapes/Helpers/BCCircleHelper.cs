﻿using System;
using System.Collections.Generic;

namespace BlrtCanvas.GLCanvas.Shapes.Helpers
{
	public static class BCCircleHelper
	{
		public static float[] GetCircleLine(float centerX, float centerY, float radius, float lineWidth, int segmentCount)
		{
			return GetCircleLine (centerX, centerY, radius, radius, lineWidth, segmentCount);
		}

		public static float[] GetCircleLine(float centerX, float centerY, float radiusX, float radiusY, float lineWidth, int segmentCount)
		{
			int vertexCount = (segmentCount + 1) * 2;
			int floatCount = vertexCount * 3;

			float[] array = new float[floatCount];
			float[] data = GetCircleData (segmentCount);
			float halfWidth = lineWidth * 0.5f;

			for (Int32 i = 0; i < segmentCount + 1; ++i) {
				/* 
					For info about algorithm, visit: 
						http://stackoverflow.com/questions/38986938/line-width-for-ellipse-is-not-constant/38987946?noredirect=1#comment65329670_38987946
				*/

				float sin = data [i * 2];
				float cos = data [i * 2 + 1];
				float perpX = radiusY * cos;
				float perpY = radiusX * sin;
				float magnitude = (float)Math.Sqrt (perpX * perpX + perpY * perpY);
				float scaledHalfWidth = halfWidth / magnitude;
				float scaledLenX = scaledHalfWidth * perpX;
				float scaledLenY = scaledHalfWidth * perpY;
				float X = centerX + radiusX * cos;
				float Y = centerY + radiusY * sin;
				array [i * 6 + 0] = X + scaledLenX; //outer X
				array [i * 6 + 1] = Y + scaledLenY; //outer Y
				array [i * 6 + 2] = 0;

				array [i * 6 + 3] = X - scaledLenX; //inner X
				array [i * 6 + 4] = Y - scaledLenY; //inner Y
				array [i * 6 + 5] = 0;
			}
			return array;
		}

		public static float[] GetCircle(float centerX, float centerY, float radiusX, float radiusY, int segmentCount)
		{
			int vertexCount = (segmentCount + 1) + 1;
			int floatCount = vertexCount * 3;

			float[] array = new float[floatCount];
			float[] data = GetCircleData (segmentCount);

			array [0] = centerX;
			array [1] = centerY;
			array [2] = 0.0f;

			for (int i = 0; i < segmentCount + 1; ++i) {

				float sin = data [i * 2];
				float cos = data [i * 2 + 1];

				array [(i + 1) * 3 + 0] = centerX + sin * (radiusX);
				array [(i + 1) * 3 + 1] = centerY + cos * (radiusY);
				array [(i + 1) * 3 + 2] = 0.0f;
			}
			return array;
		}

		private static Dictionary<int, float[]> _pregenerated;

		private static float[] GetCircleData(int segmentCount)
		{
			if (_pregenerated == null)
				_pregenerated = new Dictionary<int, float[]> ();

			lock (_pregenerated) {
				if (_pregenerated.ContainsKey (segmentCount))
					return _pregenerated [segmentCount];

				int floatCount = (segmentCount + 1) * 2;

				float segmentAngle = (float)(Math.PI * 2) / segmentCount;

				float[] array = new float[floatCount];

				for (int i = 0; i < segmentCount + 1; ++i) {

					array [i * 2 + 0] = (float)Math.Sin (segmentAngle * i);
					array [i * 2 + 1] = (float)Math.Cos (segmentAngle * i);
				}
				_pregenerated.Add (segmentCount, array);

				return array;
			}
		}
	}
}

