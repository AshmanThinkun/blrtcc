﻿using System;
using OpenTK.Graphics.ES20;
using OpenTK;
using System.Collections.Generic;
using System.Collections;
using BlrtCanvas.GLCanvas.Shapes;
using System.Drawing;
using BlrtCanvas.GLCanvas.Shaders;
using BlrtCanvas.Util.Serialization;
using BlrtCanvas.GLCanvas.Shapes.Elements.BlrtPages;
using System.IO;
using BlrtCanvas.Util;
using System.Threading.Tasks;

namespace BlrtCanvas.GLCanvas
{
	public enum CanvasCameraMode
	{
		Manual,
		Auto,
	}

	[Flags]
	public enum CanvasReleaseParameter : int
	{
		None			= 0x0,
		Elements		= 0x1,
		TexturesInUse	= 0x2,
		TexturesUnused	= 0x4,
		TexturesAll		= TexturesInUse | TexturesUnused,
		All				= Elements | TexturesAll
	}

	public class BCCanvas
	{
		public enum ShaderType : int
		{
			SimpleShader = 0
		}

		BCElementManager _elementManager;
		public BCCamera Camera { get; private set; }

		BCAtlas _atlas;

		public float DeltaTime { get; private set; }

		public bool IsSetup { get; private set; }

		public float ImageCanvasRatio {
			get {
				float w = _elementManager.CurrentPage.Width / (_viewport.Width);
				float h = _elementManager.CurrentPage.Height / (_viewport.Height);
				return w / h;
			}
		}

		/// <summary>
		///
		/// </summary>
		/// <value>The world scale.</value>
		public float LineScale {
			get {
				if (null != Camera) {
					return Math.Min (Viewport.Width / CanvasPageView.PlatformHelper.ScreenScale / Camera.CurrentPageViewWidth, Viewport.Height / CanvasPageView.PlatformHelper.ScreenScale / Camera.CurrentPageViewHeight);
				}
				return 1f;
			}
		}

		public SizeF CurrentPageViewSize {
			get {
				return new SizeF (Camera.CurrentPageViewWidth, Camera.CurrentPageViewHeight);
			}
		}

		float _previousTime;
		SizeF _viewport;

		Dictionary<ShaderType, BCShader>	_shadersList;
		BCShader							_currentShader;


		bool _viewportDirty;

		BCTextureManager _texManager;

		bool isReleased = false; // this is initial false because this can allow canvas to start init when the 1st time it starts

		TaskCompletionSource<bool> _setupObjectTCS;

		public ICanvasManager CanvasManager { get; private set; }

		public BCCanvas (ICanvasManager canvasManager)
		{
			CanvasManager = canvasManager;

			IsSetup = false;
			_setupObjectTCS = new TaskCompletionSource<bool> ();
			ResetTime ();
			IsDirty = true;
			_viewportDirty = false;
			CameraMode = CanvasCameraMode.Auto;
		}

		public CanvasCameraMode CameraMode { get; set; }

		public float ViewPortRatio {
			get {
				if (Viewport.Height == 0) {
					return 1;
				}
				return Viewport.Width / Viewport.Height;
			}
		}

		public void SetDirty()
		{
			IsDirty = true;
		}



		public bool IsDirty { get; private set; }
		public float CurrentTime { get; private set; }



		public int CurrentPage { get { return Camera.CurrentPage; } }
		public float CurrentZoom { get { return Camera.CurrentZoom; } }

		public SizeF Viewport {
			get {
				return _viewport;
			}
			set {
				if (value.Width != _viewport.Width || value.Height != _viewport.Height) {
					_viewport = value;
					_viewportDirty = true;
				}
			}
		}

		private void InitShaders()
		{
			_shadersList = new Dictionary<ShaderType, BCShader> () {
				{ ShaderType.SimpleShader, new BCSimpleShader () }
			};
			foreach (var shader in _shadersList) {
				shader.Value.Load ();
				shader.Value.AttachShader ();
			}
			_currentShader = _shadersList[ShaderType.SimpleShader];
			_currentShader.Activate ();
		}

		public async Task LoadArgument(IBlrtCanvasData arg)
		{
			if (null == arg) {
				throw new ArgumentNullException ("arg");
			}

			if (arg.PageCount <= 0) {
				throw new ArgumentException ("arg.PageCount <= 0");
			}

			if (null != Camera) {
				Camera.CurrentPageChanged -= OnCameraCurrentPageChanged;
			}

			isReleased = false;
			if (!IsSetup) {
				CanvasManager.Redraw ();
				await _setupObjectTCS.Task;
			}
			_elementManager.Release ();
			await _texManager.Release ();

			var pageSeq = new IBCTexture[arg.PageCount];
			if (!arg.IsDraft) {
				string json = null;
				using (StreamReader sr = new StreamReader (arg.DecyptedTouchDataPath)) {
					json = sr.ReadToEnd ();
				}
				var data = BlrtCanvasSerializator.Unpack (json);

				if (null == data.mediaData) {
					// if media data is not supplied (e.g. in v1 json), we will load it from the image
					data.mediaData = new BlrtUnpackedCanvasData.MediaData[arg.PageCount];
					for (int i = 0; i < data.mediaData.Length; i++) {
						data.mediaData [i] = new BlrtUnpackedCanvasData.MediaData ();
					}
				} else {
					if (arg.PageCount != data.mediaData.Length) {
						throw new ArgumentException ("image number and mediaData number can not match");
					}
				}

				for (int i = 0; i < arg.PageCount; ++i) {
					var page = arg.GetPage (i + 1);

					var pageTex = new BCPageTexture (page, new PageData () {
						Width = data.mediaData [i].mediaWidth,
						Height = data.mediaData [i].mediaHeight
					});
					_elementManager.AddPage (pageTex, i + 1);
					pageSeq [i] = pageTex;
					if (data.mediaData [i].mediaWidth <= 0 || data.mediaData [i].mediaHeight <= 0) {
						data.mediaData [i].mediaWidth = pageTex.Width;
						data.mediaData [i].mediaHeight = pageTex.Height;
					}
				}

				Camera.UnpackData (data);
				_elementManager.LoadElements (data);


			} else {
				for (int i = 0; i < arg.PageCount; ++i) {
					var page = arg.GetPage (i + 1);

					var pageTex = new BCPageTexture (page, new PageData ());
					_elementManager.AddPage (pageTex, i + 1);
					pageSeq [i] = pageTex;
				}

				Camera.LoadPages(BCMediaInfo.FromCanvasData(arg));
			}
			_texManager.SetPageSequence (pageSeq);
			ResetTime ();
			_sectionStarted = false;
		}

		public async Task WaitFirstTexture()
		{
			await _texManager.InitLoading (Camera.CurrentPage);
			Camera.CurrentPageChanged += OnCameraCurrentPageChanged;
		}

		void OnCameraCurrentPageChanged(object sender, int e)
		{
			_texManager.NotifyCurrentPage (e);
		}

		private BlrtUnpackedCanvasData PackCanvasData() {
		
			return new BlrtUnpackedCanvasData () {
				version = BlrtTouchSerializableV2.SerializableVersion,

				cameraData = Camera.PackData(),
				mediaData = _elementManager.PackMediaData(),
				touchData = _elementManager.PackElementData(),
			};
		}

		public void WriteCanvasData(string path) {

			var seriz = BlrtCanvasSerializator.Pack (
	            PackCanvasData ()
            );

			File.WriteAllText (
				path,
				seriz
			);
		}

		public async Task Release(CanvasReleaseParameter param)
		{
			isReleased = true;
			Camera.CurrentPageChanged -= OnCameraCurrentPageChanged;
			var tempTexInUse = false;
			var tempTexUnused = false;
			if (param.HasFlag (CanvasReleaseParameter.TexturesInUse)) {
				// TODO only release textures in use
				tempTexInUse = true;
			}
			if (param.HasFlag (CanvasReleaseParameter.TexturesUnused)) {
				// TODO only release unused textures
				tempTexUnused = true;
			}
			// TODO START
			if (tempTexInUse && tempTexUnused) {
				await _texManager.Release ();
			} else if (!tempTexInUse && !tempTexUnused) {
				// do nothing
			} else {
				throw new NotImplementedException ();
			}
			if (param.HasFlag(CanvasReleaseParameter.Elements)) {
				Camera.Release ();
				_elementManager.Release ();
			}
			DeltaTime = 0;
			ResetTime ();
			IsDirty = true;
			CameraMode = CanvasCameraMode.Auto;
			_sectionStarted = false;
			#if __ANDROID__
			// we don't need to do this in iOS
			IsSetup = false;
			// TODO disabled because this can cause problem on Samsung Galaxy S3
//			_currentShader = null;
//			foreach (var kvp in _shadersList) {
//				kvp.Value.Dispose ();
//			}
//			_shadersList.Clear ();
			#endif

		}

		bool _sectionStarted = false;
		public void StartNewRecordSection()
		{
			if (!_sectionStarted) {
				_sectionStarted = true;
				Camera.StartNewRecordSection ();
				_elementManager.StartNewRecordSection ();
			}
		}

		public void FinishCurrentRecordSection(float time)
		{
			if (_sectionStarted) {
				_sectionStarted = false;
				Camera.FinishCurrentRecordSection (time);
				_elementManager.FinishCurrentRecordSection (time);
			}
		}

		public void SelectTexture(int texName)
		{
			GL.BindTexture (TextureTarget.Texture2D, texName);
		}

		private void ResetTime()
		{
			CurrentTime = 0;
			_previousTime = -float.Epsilon;
		}

		bool _isObjectsInited = false;
		public void Setup()
		{
			if (isReleased) {
				return;
			}

			CanvasPageView.PlatformHelper.CalulateMaxTextureSize();

			InitShaders ();
			if (!_isObjectsInited) {
				_texManager = new BCTextureManager (this);
				_atlas = new BCAtlas (this, _texManager);
				_elementManager = new BCElementManager (_atlas);
				Camera = new BCCamera (_atlas);
				_isObjectsInited = true;
				_setupObjectTCS.TrySetResult (true);
			}
			IsSetup = true;
			SetDirty ();
		}

		public BCShader GetShader(ShaderType type)
		{
			if (_shadersList [type] != _currentShader) {
				_currentShader = _shadersList [type];
				_currentShader.Activate ();
				_currentShader.SetProjectionMatrix (Camera.GetProjectionMatrix());
			}
			return _shadersList[type];
		}

		private void ClearView()
		{
			GLHelper.Clear(0xCC / 255f, 0xCC / 255f, 0xCC / 255f);
		}

		public void AddGesture (TouchGesture gesture)
		{
			_elementManager.AddGesture (gesture);
		}

		public void NotifyGestureFinished (TouchGesture gesture)
		{
			_elementManager.NotifyGestureFinished (gesture);
		}

		public void NotifyGestureCancelled (TouchGesture gesture)
		{
			_elementManager.NotifyGestureCancelled (gesture);
		}

		public void Update(bool force) {

			if (!IsSetup)
				return;

			_previousTime = CurrentTime;
			CurrentTime = CanvasManager.CurrentTime;
			DeltaTime = force ? 1 : CurrentTime - _previousTime;

			if (_viewportDirty) {
				var size = new Size((int)_viewport.Width, (int)_viewport.Height);
				GL.Viewport (size);
				_viewportDirty = false;
				SetDirty ();
			}

			Camera.Update ();

			_elementManager.Update ();
		}

		public void Redraw () {

			ClearView ();

			if ((!IsSetup) || isReleased) return;

			_currentShader.SetProjectionMatrix (Camera.GetProjectionMatrix());
			_elementManager.Draw ();

			IsDirty = false;
		}

		public void SetPage(int page)
		{
			Camera.SetPage (page);
			_texManager.NotifyCurrentPage (page);
		}




		public void Undo() {
			int page = _elementManager.UndoLast (_atlas.CurrentTime);

			if(page >= 0 && page != Camera.CurrentPage)
				SetPage (page);
		}

		public void Redo() {
			int page = _elementManager.RedoLast (_atlas.CurrentTime);

			if(page >= 0 && page != Camera.CurrentPage)
				SetPage (page);
		}

		public bool CanUndo { get { return _elementManager.CanUndo; } }
		public bool CanRedo { get { return _elementManager.CanRedo; } }








		public event Action ActionQueue;

		public void DoActions ()
		{
			if (!IsSetup)
				return;

			var a = ActionQueue;
			if (a != null) {
				a.Invoke ();
				ActionQueue -= a;
			}
		}
	}
}

