﻿using System;
using System.Linq;
using OpenTK;
using BlrtCanvas.Util;
using BlrtCanvas.Util.Serialization;
using System.Drawing;
using System.Collections.Generic;

namespace BlrtCanvas.GLCanvas
{
	public class BCCamera
	{
		BCAtlas _atlas;
		List<BCCameraPosition> _camPos;
		BCMediaInfo _mediaInfo;

		public RectangleF Bounds { get; private set; }

		bool _locked;
		float _lerp;


		public bool Locked { get { return _locked; } set { _locked = value; _lerp = _locked ? 1 : 0; } }

		BCCameraPosition _currentPosition;

		float _lastViewportRatio;

		Matrix4 _projectionMatrix;

		public BCCamera (BCAtlas atlas) {

			_atlas = atlas;
			_projectionMatrix = Matrix4.Identity;

			SetPage (1);

			_camPos = new List<BCCameraPosition>();
		}

		public event EventHandler<int> CurrentPageChanged;


		public int CurrentPage		{ get { return _currentPosition.page; } }
		public float CurrentX		{ get { return _currentPosition.x; } }
		public float CurrentY		{ get { return _currentPosition.y; } }
		public float CurrentZoom	{ get { return _currentPosition.zoom; } }
		public float CurrentPageViewWidth { get; set; }
		public float CurrentPageViewHeight { get; set; }

		int _startLocation = 0;
		float _startTime = 0;
		public void StartNewRecordSection()
		{
			_startLocation = _camPos.Count;
			_startTime = _atlas.CurrentTime;
		}

		public void FinishCurrentRecordSection(float time)
		{
			if ((_camPos.Count > 0) && (_camPos.Count > _startLocation)) {
				var lastTime = _camPos [_camPos.Count - 1].timestamp;
				if (time < lastTime) {
					var calculatedLength = lastTime - _startTime;
					var realLength = time - _startTime;
					if ((calculatedLength > 0) && (realLength > 0)) {
						var scale = realLength / calculatedLength;
						for (int i = 0; i < _camPos.Count; ++i) {
							var temps = _camPos [i];
							temps.timestamp = scale * (_camPos [i].timestamp - _startTime) + _startTime;
							_camPos [i] = temps;
						}
					}
				}
			}
		}

		public void Release()
		{
			_camPos.Clear ();
			_mediaInfo = null;
			Bounds = RectangleF.Empty;
			_locked = false;
			_lerp = 0;
			_lastViewportRatio = 0;
			_currentPosition = new BCCameraPosition ();
			_projectionMatrix = Matrix4.Identity;
			SetPage (1);
		}

		public void UnpackData(BlrtUnpackedCanvasData data) {

			var camPoints = data.cameraData.points;
			var mediaData = data.mediaData;

			if (null == camPoints || camPoints.Length < 1) {
				throw new ArgumentException ("cameraData");
			}

			if (null == mediaData || mediaData.Length < 1) {
				throw new ArgumentException ("mediaData");
			}

			_camPos.Clear ();
			for (int i = 0; i < camPoints.Length; i++) {
				_camPos.Add(new BCCameraPosition () {
					x			= (data.invertedCameraPosition)									? -camPoints [i].x : camPoints [i].x,
					y			= (data.invertedYAxisPosition ^ data.invertedCameraPosition)	? -camPoints [i].y : camPoints [i].y,
					page		= camPoints [i].page,
					ratio		= camPoints [i].ratio,
					timestamp	= camPoints [i].timestamp,
					zoom		= camPoints [i].zoom
				});
			}
			_mediaInfo = BCMediaInfo.FromMediaData (data.mediaData);

			MoveTo (GetCurrentPosition());
		}

		public BlrtUnpackedCanvasData.CameraPositioning PackData() {

			var tmp = new BlrtUnpackedCanvasData.CameraPositioning ();

			tmp.points = (
				from c in _camPos
			    select new BlrtUnpackedCanvasData.CameraPositioningPoint () {
					page		= c.page,
					ratio		= c.ratio,
					timestamp	= c.timestamp,
					x			= -c.x,
					y			= c.y,
					zoom		= c.zoom,
				}
			).ToArray ();

			return tmp;
		}


		public void LoadPages(BCMediaInfo info) {
			_mediaInfo = info;
			_lastViewportRatio = 0;
			_camPos.Clear ();
			SetPage (1);
		}

		public void Update () {
			switch (_atlas.CameraMode) {
				case CanvasCameraMode.Auto:

					var pos = GetCurrentPosition ();
					if (!Locked) {
						float d = BCCameraPosition.DistanceSqr (pos, _currentPosition);

						if (d < 0.01f) {
							Locked = true;
						} else {
							_lerp += _atlas.DeltaTime * 0.5f;

							pos = BCCameraPosition.Lerp (_currentPosition, pos, _lerp);
						}
					}
					MoveTo (pos);

					break;
				case CanvasCameraMode.Manual:


					Locked = false;

					//make sure everything, like the viewport hasnt changed
					MoveTo (_currentPosition);

					if (_atlas.IsRecording) {
						_currentPosition.timestamp = _atlas.CurrentTime;
						Record ();
					}

					break;
			}
		}

		void MoveTo (BCCameraPosition position)
		{
			var vpr = _atlas.ViewportRatio;

			position.zoom = Math.Max (0.05f, Math.Min (50f, position.zoom));

			if (!BCCameraPosition.SamePosition (_currentPosition, position) || vpr != _lastViewportRatio) {

				if (_mediaInfo == null) {
					CurrentPageViewWidth = 1;
					CurrentPageViewHeight = 1;
					_projectionMatrix = CreateMatrix (0, 0, 1, 1, 1);
				} else {
					_lastViewportRatio = vpr;
					if (_currentPosition.page != position.page) {
						_currentPosition = position;
						if (null != CurrentPageChanged) {
							CurrentPageChanged (this, position.page);
						}
					} else {
						_currentPosition = position;
					}

					int page = Math.Max (1, _currentPosition.page);

					var viewAbsX = _currentPosition.x + _mediaInfo.GetXOffset (page) * 0.5f / _currentPosition.zoom;
					var viewAbsY = _currentPosition.y + _mediaInfo.GetYOffset (page) * 0.5f / _currentPosition.zoom;
					CurrentPageViewWidth = _mediaInfo.GetViewWidth (page);
					CurrentPageViewHeight = _mediaInfo.GetViewHeight (page);
					_projectionMatrix = CreateMatrix (viewAbsX, viewAbsY, _currentPosition.zoom, (int)CurrentPageViewWidth, (int)CurrentPageViewHeight);
				}
				_atlas.SetDirty ();
			}
		}

		public void Translate(float deltaX, float deltaY, float deltaZoom = 0)
		{
			MoveTo (new BCCameraPosition () {
				x			= CurrentX + deltaX,
				y			= CurrentY + deltaY,
				zoom		= CurrentZoom + deltaZoom,
				page		= CurrentPage,
				ratio		= 1,
				timestamp	= _atlas.CurrentTime
			});
		}

		public void SetPage(int page)
		{
			Locked = false;

			MoveTo (new BCCameraPosition(){
				x			= 0,
				y			= 0,
				zoom		= 1,
				page		= page,
				ratio		= 1,
				timestamp	= _atlas.CurrentTime
			});
		}

		public void SetToCurrentPosition(){
			Locked = true;
			MoveTo (GetCurrentPosition());
		}

		BCCameraPosition GetCurrentPosition()
		{
			if (_camPos.Count == 0)
				return _currentPosition;

			for (int i = _camPos.Count - 1; i >= 0; --i) 
				if (_atlas.CurrentTime >= _camPos[i].timestamp) 
					return _camPos [i];

			return _camPos [0];
		}




		public PointF ScreenToWorld (float x, float y)
		{
			var viewport = _atlas.Viewport;
			var screen = Bounds;

			return new PointF (
				(x * Bounds.Width / viewport.Width) + Bounds.X,
				((viewport.Height - y) * Bounds.Height / viewport.Height) + Bounds.Y
			);
		}

		public PointF ScreenToCamera (float x, float y)
		{
			var imgWidth = _mediaInfo.GetMediaWidth (_currentPosition.page);
			var imgHeight = _mediaInfo.GetMediaHeight (_currentPosition.page);


			float viewportRatio	= _atlas.ViewportRatio;
			float imgRatio = (float)imgWidth / imgHeight;
			float halfWidth, halfHeight;

			if (viewportRatio <= imgRatio) {
				halfWidth = imgWidth * 0.5f;
				halfHeight = halfWidth / viewportRatio;
			} else {
				halfHeight = imgHeight * 0.5f;
				halfWidth = halfHeight * viewportRatio;
			}




			var viewport = _atlas.Viewport;

			return new PointF (
				(x * halfWidth * 2 / viewport.Width) - halfWidth,
				((viewport.Height - y) * halfHeight * 2 / viewport.Height) - halfHeight
			);
		}

		public Matrix4 GetProjectionMatrix() {
			return _projectionMatrix;
		}

		private Matrix4 CreateMatrix(float centerX, float centerY, float zoom, int imgWidth, int imgHeight) {

			float viewportRatio	= _atlas.ViewportRatio;
			float imgRatio = (float)imgWidth / imgHeight;
			float halfWidth, halfHeight;

			if (viewportRatio <= imgRatio) {
				halfWidth = imgWidth / zoom * 0.5f;
				halfHeight = halfWidth / viewportRatio;
			} else {
				halfHeight = imgHeight / zoom * 0.5f;
				halfWidth = halfHeight * viewportRatio;
			}

			Bounds = new RectangleF (centerX - halfWidth, centerY - halfHeight, halfWidth * 2, halfHeight * 2);
			return Matrix4.CreateOrthographicOffCenter (Bounds.Left, Bounds.Right, Bounds.Top, Bounds.Bottom, -1000, 1000);
		}

		void Record ()
		{
			if (_camPos.Count >= 2) {
			
				var p1 = _camPos [_camPos.Count - 2];
				var p2 = _camPos [_camPos.Count - 1];

				if (BCCameraPosition.CanPredicite (p1, _currentPosition, p2))
					_camPos.RemoveAt (_camPos.Count - 1);

			}

			_camPos.Add (_currentPosition);
		}

		private struct BCCameraPosition : IEquatable<BCCameraPosition> {
			public float x, y, zoom, timestamp, ratio;
			public int page;

			public bool Equals (BCCameraPosition other)
			{
				return x			== other.x
					&& y			== other.y
					&& zoom			== other.zoom
					&& page			== other.page
					&& ratio		== other.ratio
					&& timestamp	== other.timestamp
				;
			}

			public static bool SamePosition (BCCameraPosition c1, BCCameraPosition c2)
			{
				return c1.x		== c2.x
					&& c1.y		== c2.y
					&& c1.zoom	== c2.zoom
					&& c1.page	== c2.page
					&& c1.ratio	== c2.ratio
					;
			}


			public static bool Equals(BCCameraPosition c1, BCCameraPosition c2){
				return c1.Equals (c2);
			}

			public static float DistanceSqr(BCCameraPosition c1, BCCameraPosition c2){

				if (c1.page != c2.page)
					return float.MaxValue;

				var zoom = 1 / Math.Max (c1.zoom, c2.zoom);

				var x = (c1.x - c2.x) * zoom;
				var y = (c1.y - c2.y) * zoom;

				var z = (c1.zoom - c2.zoom) * 8;
				var w = (c1.ratio - c2.ratio) * 16;

				return (x * x + y * y)
					+ z * z
					+ w * w;
			}

			public static BCCameraPosition Lerp(BCCameraPosition c1, BCCameraPosition c2, float lerp){

				lerp = Math.Min (1, Math.Max (0, lerp));

				if (c1.page != c2.page) {
					if (lerp == 1)
						return c2;
					else 
						return c1;
				}

				var x = c2.x - c1.x;
				var y = c2.y - c1.y;
				var zoom = c2.zoom - c1.zoom;
				var ratio = c2.ratio - c1.ratio;
				var t = c2.timestamp - c1.timestamp;

				return new BCCameraPosition () {
					x				= c1.x + x * lerp,
					y				= c1.y + y * lerp,
					zoom			= c1.zoom + zoom * lerp,
					ratio			= c1.ratio + ratio * lerp,
					timestamp		= c1.timestamp + t * lerp,
					page			= c1.page
				};
			}

			static BCCameraPosition Between (BCCameraPosition p1, BCCameraPosition p2, float timestamp)
			{
				return Lerp(p1, p2, (timestamp - p1.timestamp) / (p2.timestamp - p1.timestamp));
			}

			public static bool CanPredicite (BCCameraPosition p1, BCCameraPosition p2, BCCameraPosition other)
			{
				var p = Between (p1, p2, other.timestamp);
				return (DistanceSqr (p, other) < 0.01f);
			}
		};

	}
}

