﻿using System;
using System.Linq;
using BlrtCanvas.GLCanvas.Shapes;
using System.Collections.Generic;
using System.Drawing;
using BlrtCanvas.Util.Serialization;
using BlrtCanvas.GLCanvas.Shapes.Elements.BlrtPages;
using BlrtCanvas.GLCanvas.Shapes.Elements;
using BlrtData;

namespace BlrtCanvas.GLCanvas
{
	public class BCElementManager : IComparer<IElement>
	{
		public const int PAGE_LAYER = -1;
		public const int SHAPE_LAYER = 1;

		List<IElement> _elements;
		List<TouchGesture> _gestures;

		int _undoStack;

		private BCAtlas _atlas;

		public BCPage CurrentPage {
			get {
				lock (_elements)
					return _elements.FirstOrDefault (e => e.Display && e is BCPage) as BCPage;
			}
		}

		public BCElementManager (BCAtlas helper)
		{
			_atlas = helper;
			_elements = new List<IElement> ();
			_gestures = new List<TouchGesture> ();
		}

		public void Update ()
		{
			lock (_elements) {
				foreach (var element in _elements) {
					element.Update ();
				}
			}
		}

		public void Draw ()
		{
			lock (_elements) {
				foreach (var element in _elements) {
					if (element.Display) {
						element.Draw ();
					}
				}
			}
		}

		public void LoadElements (BlrtUnpackedCanvasData touchData)
		{
			if (null != touchData.touchData && touchData.touchData.Length > 0) {

				//if(touchData.ratioHandling == RatioHandling.AutoScaling)

				UnpackData (touchData);
			}
		}

		private void UnpackData (BlrtUnpackedCanvasData touchData)
		{
			foreach (var action in touchData.touchData) {
				var gesture = TouchGesture.UnpackData (action, touchData.invertedYAxisPosition);
				AddGesture (gesture);
			}

			lock (_elements) {
				_elements.Sort (this);
			}
		}


		public void AddPage(BCPageTexture page, int pageNumber)
		{
			var _bcPage = new BCPage(_atlas, pageNumber, page);
			_bcPage.SetAtlas (_atlas);

			lock (_elements) {
				_elements.Add (_bcPage);
			}
		}


		public BlrtUnpackedCanvasData.BlrtElementData[] PackElementData ()
		{
			lock (_gestures) {
				var output = from t in _gestures
						where !t.Cancelled && t.TotalPointCount > 0
					select TouchGesture.PackData (t);

				return output.ToArray ();
			}
		}

		public BlrtUnpackedCanvasData.MediaData[] PackMediaData ()
		{
			lock (_elements) {
				var page = from p in _elements
						where p is BCPage
						select p as BCPage;

				var output = new BlrtUnpackedCanvasData.MediaData[page.Max((BCPage arg) => arg.Page)];

				foreach (var p in page) {

					var min = GetCameraMinOffset (p.Page);
					var max = GetCameraMaxOffset (p.Page);



					output [p.Page - 1] = new BlrtUnpackedCanvasData.MediaData () {
					
						mediaHeight		= p.Height,
						mediaWidth		= p.Width,

						offsetMaxX		= max.X,
						offsetMaxY		= max.Y,
						offsetMinX		= min.X,
						offsetMinY		= min.Y,
					};
				}

				return output;
			}
		}



		public PointF GetCameraMinOffset(int page){

			var output = PointF.Empty;

			lock (_gestures) {
				foreach (var gest in _gestures) {
					if (page == gest.Page && !gest.Cancelled) {
					
						var p = gest.GetCameraMinOffset ();
						output.X = Math.Min(p.X, output.X);
						output.Y = Math.Min(p.Y, output.Y);
					}
				}
			}

			return output;
		}
		public PointF GetCameraMaxOffset(int page){

			var output = PointF.Empty;

			lock (_gestures) {
				foreach (var gest in _gestures) {
					if (page == gest.Page && !gest.Cancelled) {

						var p = gest.GetCameraMaxOffset ();
						output.X = Math.Max(p.X, output.X);
						output.Y = Math.Max(p.Y, output.Y);
					}
				}
			}

			return output;
		}



		TouchGesture _editingGesture = null;
		public void AddGesture (TouchGesture gesture) {
		
			BCBrush element = null;
			switch (gesture.Type) {

				case BlrtBrush.ShapeCircle:
					gesture.CanUndo = true;
					element = new BCCircle (gesture);
					break;

				case BlrtBrush.ShapeLine:
					gesture.CanUndo = true;
					element = new BCLine (gesture);
					break;

				case BlrtBrush.ShapeRectangle:
					gesture.CanUndo = true;
					element = new BCSquare (gesture);
					break;

				case BlrtBrush.Pointer:
					gesture.CanUndo = false;
					element = new BCPointer (gesture);
					break;

				case BlrtBrush.Pen:
				default:
					gesture.CanUndo = true;
					element = new BCPath (gesture);
					break;
			}
			element.SetAtlas (_atlas);

			if (gesture.IsEditing) {
				_editingGesture = gesture;
			} else {
				if (gesture.CanUndo && (!gesture.Cancelled)) {
					_undoStack = 0;
				}
			}
			if (gesture.CanUndo && (!gesture.IsEditing) && (!gesture.Cancelled)) {
				_undoStack = 0;
			}

			lock (_gestures) {
				_gestures.Add (gesture);
			}

			lock (_elements) {
				_elements.Add (element);
			}
		}

		public void NotifyGestureFinished (TouchGesture gesture)
		{
			if (gesture == _editingGesture) {
				_editingGesture = null;
				_undoStack = 0;
			}
		}

		public void NotifyGestureCancelled (TouchGesture gesture)
		{
			if (gesture == _editingGesture) {
				_editingGesture = null;
			}
		}

		float _sectionLastRedoUndo = -1;
		List<TouchGesture> _sectionRedoUndoGesture = new List<TouchGesture> ();

		void RegisterSectionUndoRecoGesture(float currentTime, TouchGesture gesture)
		{
			_sectionLastRedoUndo = currentTime;
			if (!_sectionRedoUndoGesture.Contains (gesture)) {
				_sectionRedoUndoGesture.Add (gesture);
			}
		}

		public int UndoLast(float currentTime) {


			var gesture = _gestures.OrderBy(g => g.LastChanged)
				.LastOrDefault (g => !g.IsEditing && g.IsVisableAt (g.Page, currentTime) && g.CanUndo);

			if (gesture != null) {
				RegisterSectionUndoRecoGesture (currentTime, gesture);
				gesture.Undo (currentTime);
				++_undoStack;
				return gesture.Page;
			}
			return -1;
		}

		public int RedoLast(float currentTime) {

			if (_undoStack > 0) {

				var gesture = _gestures.OrderBy(g => g.LastChanged)
					.LastOrDefault (g => !g.IsVisableAt (g.Page, currentTime) && g.CanUndo);

				if (gesture != null) {
					--_undoStack;
					RegisterSectionUndoRecoGesture (currentTime, gesture);
					gesture.Redo (currentTime);

					return gesture.Page;
				}
			}
			return -1;
		}

		public bool CanUndo { get { return (null != _gestures) && _gestures.Any(g => !g.IsEditing && g.IsVisableAt (g.Page, _atlas.CurrentTime) && g.CanUndo); } }
		public bool CanRedo { get { return _undoStack > 0; } }

		int _startLocation = 0;
		float _startTime = 0;

		public void StartNewRecordSection()
		{
			_startLocation = _gestures.Count;
			_startTime = _atlas.CurrentTime;
			_sectionRedoUndoGesture.Clear ();
			_sectionLastRedoUndo = -1;
		}

		public void FinishCurrentRecordSection(float time)
		{
			bool scalePoints = false;
			float gestureFinalTime = 0;
			if ((_gestures.Count > 0) && (_gestures.Count > _startLocation)) {
				scalePoints = true;
				gestureFinalTime = _gestures [_gestures.Count - 1].EndedAt;
			}

			bool scaleDisplayToggle = false;
			float toggleFinalTime = 0;
			if ((_sectionRedoUndoGesture.Count > 0) && (_sectionLastRedoUndo > 0)) {
				scaleDisplayToggle = true;
				toggleFinalTime = _sectionLastRedoUndo;
			}

			var lastTime = Math.Max (gestureFinalTime, toggleFinalTime);
			if (time < lastTime) {
				var calculatedLength = lastTime - _startTime;
				var realLength = time - _startTime;
				if ((calculatedLength > 0) && (realLength > 0)) {
					var scale = realLength / calculatedLength;
					if (scalePoints) {
						for (int i = 0; i < _gestures.Count; ++i) {
							_gestures [i].ScalePointTime (scale, _startTime);
						}
					}
					if (scaleDisplayToggle) {
						for (int i = 0; i < _sectionRedoUndoGesture.Count; ++i) {
							_sectionRedoUndoGesture [i].ScaleDisplayToggle (scale, _startTime, lastTime);
						}
					}
				}
			}
		}

		public void Release() {

			lock (_gestures) {
				_gestures.Clear ();
			}

			lock (_elements) {
				foreach (var element in _elements) {
					element.Dispose ();
				}

				_elements.Clear ();
			}

			_undoStack = 0;
			_editingGesture = null;
		}


		public int Compare (IElement x, IElement y)
		{
			var laycompare = x.Layer.CompareTo(y.Layer);
			if (0 == laycompare) {
				return x.StartAt.CompareTo(y.StartAt);
			} else {
				return laycompare;
			}
		}
	}
}

