﻿using System;
using OpenTK.Graphics.ES20;
using OpenTK;
using System.Reflection;
using System.IO;
using BlrtData;
using System.Linq;

namespace BlrtCanvas.GLCanvas
{
	public static class GLHelper
	{
		/// <summary>
		/// Clear current GL view with the specified color.
		/// </summary>
		/// <param name="r">The red component.</param>
		/// <param name="g">The green component.</param>
		/// <param name="b">The blue component.</param>
		public static void Clear(float r, float g, float b)
		{
			GL.ClearColor (r, g, b, 1);
			GL.Clear (ClearBufferMask.ColorBufferBit);
		}

		/// <summary>
		/// Loads a shader of the specified type with the specified source code.
		/// Then return the compiled shader id.
		/// </summary>
		/// <returns>The shader id.</returns>
		/// <param name="type">Shader Type.</param>
		/// <param name="source">Shader source code.</param>
		public static int LoadShader (ShaderType type, string source)
		{
			int shader = GL.CreateShader (type);
			if (shader == 0)
				throw new InvalidOperationException ("Unable to create shader");

			int length = 0;
			GL.ShaderSource (shader, 1, new string [] {source}, (int[])null);
			GL.CompileShader (shader);

			int compiled = 0;
			GL.GetShader (shader, ShaderParameter.CompileStatus, out compiled);
			if (compiled == 0) {
				length = 0;
				GL.GetShader (shader, ShaderParameter.InfoLogLength, out length);
				if (length > 0) {
					var log = GL.GetShaderInfoLog (shader);
					#if DEBUG
					Console.WriteLine ("GL2::Couldn't compile shader: " + log);
					#endif
					LLogger.WriteEvent ("OpenGL", "CompileShader", "Error", log);
				}
				GL.DeleteShader (shader);
			throw new InvalidOperationException ("Unable to compile shader of type : " + type.ToString ());
			}

			return shader;
		}

		/// <summary>
		/// Transfer a 4 dimension vector to a float array. the vector will be repeated for the specified count
		/// </summary>
		/// <returns>The transferred float array.</returns>
		/// <param name="count">repeat count.</param>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <param name="z">The z coordinate.</param>
		/// <param name="w">The w coordinate.</param>
		public static float[] Vec4ToFloatArray(int count, float x, float y, float z, float w)
		{
			if (count < 0) {
				throw new ArgumentException ("verticesNum");
			}
			var array = new float[count * 4];
			for (int i = 0; i < count; i++) {
				array [i * 4 + 0] = x;
				array [i * 4 + 1] = y;
				array [i * 4 + 2] = z;
				array [i * 4 + 3] = w;
			}
			return array;
		}


		//public static BlrtBrushColor GetColor (float[] color) {

		//	if (color.Length < 3)
		//		return BlrtBrushColor.Black;

		//	uint c = ((uint)Math.Round (color [0] * 255) << 8
		//		| (uint)Math.Round (color [1] * 255)) << 8
		//		| (uint)Math.Round (color [2] * 255);

		//	switch (c) {
		//		case 0xffffff:
		//			return BlrtBrushColor.White;
		//		case 0xff0000:
		//			return BlrtBrushColor.Red;
		//		case 0x0000ff:
		//			return BlrtBrushColor.Blue;
		//		case 0xffff00:
		//			return BlrtBrushColor.Yellow;
		//		case 0x00ff00:
		//			return BlrtBrushColor.Green;
		//	}

		//	return BlrtBrushColor.Black;
		//}

		static readonly float[] Color_White		= { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 1.0f };
		static readonly float[] Color_Black		= {   0.0f / 255.0f,   0.0f / 255.0f,   0.0f / 255.0f, 1.0f };
		static readonly float[] Color_Yellow	= { 255.0f / 255.0f, 176.0f / 255.0f,   0.0f / 255.0f, 1.0f };
		static readonly float[] Color_Blue		= {  79.0f / 255.0f, 153.0f / 255.0f, 200.0f / 255.0f, 1.0f };
		static readonly float[] Color_Green		= {   0.0f / 255.0f, 245.0f / 255.0f, 165.0f / 255.0f, 1.0f };
		static readonly float[] Color_Red		= { 226.0f / 255.0f,  26.0f / 255.0f,  69.0f / 255.0f, 1.0f };

		public static float[] GetColor (BlrtBrushColor color)
		{
			switch (color) {
				case BlrtBrushColor.Red:
					return Color_Red;
				case BlrtBrushColor.White:
					return Color_White;
				case BlrtBrushColor.Black:
					return Color_Black;
				case BlrtBrushColor.Blue:
					return Color_Blue;
				case BlrtBrushColor.Yellow:
					return Color_Yellow;
				case BlrtBrushColor.Green:
					return Color_Green;
				default:
					throw new NotImplementedException("color:[" + color + "]");
			}
		}

		public static float[] Vec4ToFloatArray(int count, float[] colorVec)
		{
			if (null == colorVec || 4 != colorVec.Length) {
				throw new ArgumentException ("colorVec");
			}
			return Vec4ToFloatArray (count, colorVec [0], colorVec [1], colorVec [2], colorVec [3]);
		}

		/// <summary>
		/// Links the shader program and check for any link error
		/// </summary>
		/// <param name="program">Program id.</param>
		public static void LinkProgram(int program)
		{
			GL.LinkProgram (program);

			int linked = 0;
			GL.GetProgram (program, ProgramParameter.LinkStatus, out linked);
			if (linked == 0) {
				// link failed
				int length = 0;
				GL.GetProgram (program, ProgramParameter.InfoLogLength, out length);
				if (length > 0) {
					var log = GL.GetProgramInfoLog (program);
					#if DEBUG
					Console.WriteLine ("GL2::Couldn't link program: " + log);
					#endif
					LLogger.WriteEvent ("OpenGL", "LinkProgram", "Error", log);
				}

				GL.DeleteProgram (program);
				throw new InvalidOperationException ("Unable to link program");
			}
		}

		/// <summary>
		/// Gets the resource file and return a stream of that file.
		/// </summary>
		/// <returns>The resource file's stream.</returns>
		/// <param name="resourceId">Resource identifier.</param>
		public static Stream GetResourceFileStream(string resourceId)
		{
			#if __IOS__
			const string RESOURCE_PREFIX = "BlrtiOS.Canvas.GLCanvas.GLShaders.";
			#elif __ANDROID__
			const string RESOURCE_PREFIX = "BlrtApp.Droid.Canvas.GLCanvas.GLShaders.";
			#endif 
			var assembly = typeof(BCAtlas).GetTypeInfo().Assembly;
			return assembly.GetManifestResourceStream(RESOURCE_PREFIX + resourceId);
		}

		public static int NextPowerOf2(int n) {
			--n;
			n |= n >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
			n |= n >> 2;   // and then or the results.
			n |= n >> 4;
			n |= n >> 8;
			n |= n >> 16;
			++n;
			return n;
		}

		/// <summary>
		/// Gets the resource file and return its content as string
		/// </summary>
		/// <returns>The resource file's text.</returns>
		/// <param name="resourceId">Resource id.</param>
		public static string GetResourceFileText(string resourceId)
		{
			using (var reader = new System.IO.StreamReader (GetResourceFileStream(resourceId))) {
				return reader.ReadToEnd ();
			}
		}

		public static float[] GetTexCoordinate(TextureRotation rotation = TextureRotation.Angle_0)
		{
			return CreateTexCoordinate (rotation);
		}

		private static float[] CreateTexCoordinate(TextureRotation rotation)
		{
			switch (rotation) {
				case TextureRotation.Angle_0_Flipped_Horizontally:
					return new float[] {
						1f, 0f,
						1f, 1f,
						0f, 0f,
						0f, 1f
					};
				case TextureRotation.Angle_180:
					return new float[] {
						1f, 1f,
						1f, 0f,
						0f, 1f,
						0f, 0f
					};
				case TextureRotation.Angle_180_Flipped_Horizontally:
					return new float[] {
						0f, 1f,
						0f, 0f,
						1f, 1f,
						1f, 0f
					};
				case TextureRotation.Angle_270_Flipped_Vertically:
					return new float[] {
						1f, 1f,
						0f, 1f,
						1f, 0f,
						0f, 0f
					};
				case TextureRotation.Angle_270:
					return new float[] {
						0f, 1f,
						1f, 1f,
						0f, 0f,
						1f, 0f
					};
				case TextureRotation.Angle_90_Flipped_Vertically:
					return new float[] {
						0f, 0f,
						1f, 0f,
						0f, 1f,
						1f, 1f
					};
				case TextureRotation.Angle_90:
					return new float[] {
						1f, 0f,
						0f, 0f,
						1f, 1f,
						0f, 1f
					};
				case TextureRotation.Angle_0:
				default:
					return new float[] {
						0f, 0f,
						0f, 1f,
						1f, 0f,
						1f, 1f
					};
			}
		}

		public static void PrintGLError()
		{
			#if DEBUG
			Console.WriteLine ("=============GL ERROR START===============");
			#endif
			ErrorCode error;
			do {
				error = GL.GetErrorCode ();
#if DEBUG
				Console.WriteLine (error);
#endif
			} while (error != ErrorCode.NoError);
			#if DEBUG
			Console.WriteLine ("=============GL ERROR END===============");
			#endif
		}

	}
}

