﻿#extension GL_OES_standard_derivatives : enable
precision mediump float;

varying lowp vec4 DestinationColor;

varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;
uniform int TexEnabled;

uniform int Element;

float edgeFactor();

void main() {
	  if (TexEnabled == 1) {
	    gl_FragColor = texture2D(Texture, TexCoordOut) * DestinationColor;
	  } else{
		gl_FragColor = vec4(DestinationColor.xyz, Element == 3 ? 1.0 : edgeFactor());
	  }
}

float edgeFactor() {

	if (Element == 1 || Element == 2 || Element == 4) { // Ellipse or pointer or line

        float alpha = 1.0 - abs(TexCoordOut.y);
        float d = fwidth(alpha);

	    if (alpha > 0.5 - d && alpha < 0.5 + d)
	    {
	        alpha = clamp(0.5 * (alpha - 0.5 + d) / d, 0.0, 1.0);
	    }
	    else
	    {
	        alpha = step(0.5, alpha);
	    }
	    return alpha;
	}

	// Line_segment or path 

	vec2 alpha = vec2(1.0) - abs(TexCoordOut);
	vec2 d = fwidth(alpha);

	// Inner implementation of smoothstep glsl function

    if (alpha.x > 0.5 - d.x && alpha.x < 0.5 + d.x)
    {
        alpha.x = clamp(0.5 * (alpha.x - 0.5 + d.x) / d.x, 0.0, 1.0);
    }
    else
    {
        alpha.x = step(0.5, alpha.x);
    }

    if (alpha.y > 0.5 - d.y && alpha.y < 0.5 + d.y)
    {
        alpha.y = clamp(0.5 * (alpha.y - 0.5 + d.y) / d.y, 0.0, 1.0);
    }
    else
    {
        alpha.y = step(0.5, alpha.y);
    }

    return min(alpha.x, alpha.y);
}