﻿using System;
using System.Threading.Tasks;

namespace BlrtCanvas
{
	public interface IBCAudioPlayer
	{
		float Length { get; }
		float CurrentTime { get; }

		Task LoadAudio (string audioFilePath);
		void Play ();
		void Pause ();
		void Seek (float seconds);
		void ReleaseFile ();
		event EventHandler Finished;
	}
}

