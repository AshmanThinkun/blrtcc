﻿using System;
using System.Threading.Tasks;

namespace BlrtCanvas
{
	public interface IBCAudioRecorder
	{
		bool CanContinueRecording { get; }
		bool IsRecording { get; }
		float Duration { get; }

		string Extension { get; }
		string FilePath { get; }

		void SetFilePath (string path);
		Task<bool> RequestPermissions ();
		void Record ();
		Task<float> Pause ();
		Task<float> Stop ();
		void Reset ();
		void Release ();

		float CurrentVolume { get; }
	}
}

