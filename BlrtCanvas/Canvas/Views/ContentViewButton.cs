﻿using System;
using Xamarin.Forms;

namespace BlrtCanvas.Views
{
	public class ContentViewButton : Grid
	{
		public event EventHandler Clicked
		{
			add {
				_btn.Clicked += value;
			}
			remove {
				_btn.Clicked -= value;
			}
		}

		Button _btn;
		View _content;
		public ContentViewButton ()
		{
			_btn = new Button {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
			};

			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
			};
			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
			};
			Children.Add (_btn, 0, 0);
		}

		public View ContentView
		{
			get {
				return _content;
			}

			set {
				if (value != _content) {
					if (null != _content) {
						Children.Remove (_content);
					}
					_content = value;
					Children.Add (_content, 0, 0);
					LowerChild (_content);
					InvalidateLayout ();
				}
			}
		}
	}
}

