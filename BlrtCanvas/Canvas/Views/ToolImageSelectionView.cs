﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class ToolImageSelectionView : ToolSelectionView
	{
		private readonly static ImageSource[] ToolIconSource = {
			ImageSource.FromFile("canvas_tool_width_thin.png"),
			ImageSource.FromFile("canvas_tool_width_medium.png"),
			ImageSource.FromFile("canvas_tool_width_thick.png"),
		};

		private readonly static ImageSource[] DrawerIconSource = {
			ImageSource.FromFile("canvas_tool_width_drawer_thin.png"),
			ImageSource.FromFile("canvas_tool_width_drawer_medium.png"),
			ImageSource.FromFile("canvas_tool_width_drawer_thick.png"),
		};

		public static readonly BindableProperty IconBackColorProperty 
			= BindableProperty.Create<ToolImageSelectionView, Color> (t => t.IconBackColor, Color.Transparent);

		public Color IconBackColor {
			get { return (Color)base.GetValue (IconBackColorProperty); }
			set { base.SetValue (IconBackColorProperty, value); }
		}

		public static readonly BindableProperty DrawerItemBackColorProperty 
			= BindableProperty.Create<ToolImageSelectionView, Color> (t => t.DrawerItemBackColor, Color.Transparent);

		public Color DrawerItemBackColor {
			get { return (Color)base.GetValue (DrawerItemBackColorProperty); }
			set { base.SetValue (DrawerItemBackColorProperty, value); }
		}

		Image _toolIcon;
		Grid[] _drawerItemIcon;

		public ToolImageSelectionView (ExcludedHidingView param = ExcludedHidingView.None) : base (param)
		{
		}

		#region implemented abstract members of ToolSelectionView
		protected override View GetToolIcon ()
		{
			_toolIcon = new Image {
				Source = ToolIconSource[SelectedIndex],
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			return _toolIcon;
		}


		protected override Xamarin.Forms.View GetDrawerItem (int index)
		{
			if (null == _drawerItemIcon) {
				_drawerItemIcon = new Grid[DrawerIconSource.Length];
			}

			if (null == _drawerItemIcon[index]) {
				var img = new Image {
					Source = DrawerIconSource[index],
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				}; 
				var background = new ExtendedImage () {
					BorderWidth = 0.5,
					BorderColor = BlrtStyles.BlrtCharcoal,
					BackgroundColor = DrawerItemBackColor
				};
				_drawerItemIcon[index] = new Grid {
					RowDefinitions = new RowDefinitionCollection {
						new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) }
					},
					ColumnDefinitions = new ColumnDefinitionCollection {
						new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
					},
					Children = { 
						{ background,	0, 0 },
						{ img, 			0, 0 } 
					},
				};
			}

			return _drawerItemIcon [index];
		}


		protected override int DrawerItemCount {
			get {
				return DrawerIconSource.Length;
			}
		}
		#endregion

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);
			if (propertyName.Equals(IconBackColorProperty.PropertyName)) {
				OnIconBackColorPropertyChanged ();
			} else if (propertyName.Equals(DrawerItemBackColorProperty.PropertyName)) {
				OnDrawerItemBackColorPropertyChanged ();
			}
		}

		void OnIconBackColorPropertyChanged ()
		{
			IconCurrentBackgroundColor = IconBackColor;
		}

		void OnDrawerItemBackColorPropertyChanged ()
		{
			if (null != _drawerItemIcon) {
				foreach (var item in _drawerItemIcon) {
					item.BackgroundColor = DrawerItemBackColor;
				}
				IndicatorColor = DrawerItemBackColor;
			}
		}

		protected override void OnSelect (int itemIndex)
		{
			base.OnSelect (itemIndex);
			_toolIcon.Source = ToolIconSource [itemIndex];
		}
	}
}

