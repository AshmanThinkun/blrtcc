﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class VolumeControlView : Grid
	{
		public const double ViewWidth = 50;
		public const double ButtonHeight = 43;
		public const double VolumeSliderHeight = 150;
		public const double VerticalPadding = 14;
		public const double HorizontalPadding = 10;
		public const int AnimationDuration = 150;

		const string FileNamePattern = "canvas_play_volume_{0}.png";
		const int TotalIconNum = 4;
		Button _btn;
		bool _drawerOpened;
		int _currentVolImgIndex;
		TintableImage[] _icons;
		ScrollView _sliderMask;
		ContentView _sliderDrawer;
		BoxView _background;
		public VolumeControlView ()
		{
			Padding = 0;
			RowSpacing = 0;
			ColumnSpacing = 0;
			_currentVolImgIndex = 3;
			_btn = new Button () {
				BackgroundColor = Color.Transparent,
				WidthRequest = ViewWidth,
				HeightRequest = ButtonHeight,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			_icons = new TintableImage[TotalIconNum];
			for (int i = 0; i < _icons.Length; i++) {
				_icons[i] = new TintableImage {
					BackgroundColor = Color.Transparent,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Source = ImageSource.FromFile (string.Format (FileNamePattern, i)),
					Aspect = Aspect.AspectFit,
					Opacity = 0
				};
			}
			_icons [_currentVolImgIndex].Opacity = 1;
			_background = new BoxView {
				BackgroundColor = Color.Transparent,
				WidthRequest = ViewWidth,
				HeightRequest = ButtonHeight,
				IsVisible = true,
				Opacity = 1,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill
			};
			// TODO we should use layout here, but layout.IsClippedToBounds only works in iOS
			// in Android, even layout.IsClippedToBounds is set to true, views outside the layout bound can still show
			// So we use sceoll view, whose IsClippedToBounds property works in both iOS and Android
			_sliderMask = new ScrollView () {
				WidthRequest = ViewWidth,
				HeightRequest = VolumeSliderHeight,
				IsClippedToBounds = true,
				IsEnabled = false
			};

			_sliderDrawer = new ContentView () {
				TranslationY = _sliderMask.HeightRequest
			};

			_sliderMask.Content = _sliderDrawer;

			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = GridLength.Auto }
			};
			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = GridLength.Auto },
				new RowDefinition { Height = GridLength.Auto }
			};

			Children.Add (_sliderMask, 0, 0);
			Children.Add (_background, 0, 1);
			for (int i = 0; i < _icons.Length; i++) {
				Children.Add (_icons[i], 0, 1);
			}
			Children.Add (_btn, 0, 1);

			_drawerOpened = false;
			_btn.Clicked += async (sender, e) => {
				if (_drawerOpened) {
					await OpenDrawer (false, true);
				} else {
					await OpenDrawer (true, true);
				}
			};
		}

		public event EventHandler<Color> TintColorChanged;

		public Color TintColor
		{
			get {
				return _icons[0].TintColor;
			}

			set {
				if (!_icons[0].TintColor.Equals (value)) {
					for (int i = 0; i < _icons.Length; i++) {
						_icons [i].TintColor = value;
					}
					if (null != TintColorChanged) {
						TintColorChanged (this, value);
					}
				}
			}
		}

		public Rectangle MaskBounds
		{
			get {
				return _sliderMask.Bounds;
			}
		}

		public ContentView DrawerView { get { return _sliderDrawer; }}

		public new Color BackgroundColor 
		{
			get {
				return _background.BackgroundColor;
			}

			set {
				Device.BeginInvokeOnMainThread (() => {
					_background.BackgroundColor = value;
					_sliderDrawer.BackgroundColor = value;
				});
			}
		}

		public bool DrawerOpened
		{
			get {
				return _drawerOpened;
			}
		}

		public async Task OpenDrawer (bool open = true, bool animated = true)
		{
			if (open != _drawerOpened) {
				_drawerOpened = open;
				if (open) {
					var targetTranslationY = 0;
					_sliderDrawer.IsVisible = true;
					if (animated) {
						await _sliderDrawer.TranslateTo (0, targetTranslationY, AnimationDuration);
					} else {
						_sliderDrawer.TranslationY = targetTranslationY;
					}
				} else {
					var targetTranslationY = _sliderDrawer.Height;
					if (animated) {
						bool finished = await _sliderDrawer.TranslateTo (0, targetTranslationY, AnimationDuration);
						if (finished) {
							_sliderDrawer.IsVisible = false;
						}
					} else {
						_sliderDrawer.TranslationY = targetTranslationY;
						_sliderDrawer.IsVisible = false;
					}
				}
			}
		}

		/// <summary>
		/// Notifies the volume has been changed.
		/// </summary>
		/// <param name="volume">Volume out of 1. e.g. 60% = 0.6f </param>
		public void NotifyVolumeChanged (float volume)
		{
			var newIdx = _currentVolImgIndex;
			if (volume < 0.01f) {
				newIdx = 0;
			} else if (volume < 0.40f) {
				newIdx = 1;
			} else if (volume < 0.80f) {
				newIdx = 2;
			} else {
				newIdx = 3;
			}
			if (newIdx != _currentVolImgIndex) {
				_icons [newIdx].Opacity = 1;
				_icons [_currentVolImgIndex].Opacity = 0;
				_currentVolImgIndex = newIdx;
			}
		}
	}
}

