﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public abstract class ToolSelectionView : ContentView
	{
		public event EventHandler ToolSelected;
		public event EventHandler SelectedIndexChanged;
		public event EventHandler<ExcludedHidingView> SeletionStarted;

		public Rectangle ToolIconBounds { get {return _toolIcon.Bounds;} }

		public bool OpenOnTapped { get; set; }
		public bool DrawerOpen { get; private set; }

		public static readonly BindableProperty SelectedIndexProperty 
		= BindableProperty.Create<ToolSelectionView, int> (t => t.SelectedIndex, 0);

		public int SelectedIndex {
			get { return (int)base.GetValue (SelectedIndexProperty); }
			set { base.SetValue (SelectedIndexProperty, value);}
		}

		public static readonly BindableProperty IconVerticallyFlippedProperty
		= BindableProperty.Create<ToolSelectionView, bool> (t => t.IconVerticallyFlipped, false);

		public bool IconVerticallyFlipped {
			get { return (bool)base.GetValue (IconVerticallyFlippedProperty); }
			set { base.SetValue (IconVerticallyFlippedProperty, value); }
		}

		public 		const 			double	IconToDrawerSpacing 		= 0.5;
		public 		static readonly	double	IconWidth 					= Device.Idiom == TargetIdiom.Phone ? 46 : 56;
		public 		static readonly	double 	DrawerItemWidth 			= Device.Idiom == TargetIdiom.Phone ? 40 : 48;
		public 		static readonly	double 	IconHeight 					= Device.Idiom == TargetIdiom.Phone ? 44 : 52;
		protected 	const 			double 	ColorIndicatorWidth 		= 3;
		public 		const 			double 	ActionArrowWidth			= 5;
		protected 	const 			double 	ActionArrowHeight			= 10;
		protected 	static readonly	double 	SelectedIconHeight 			= IconHeight + 14;
		public 		const 			uint 	AnimationDuration 			= 100; // ms
		protected 	static readonly	double	SelectedDrawIconHeightDelta	= (SelectedIconHeight - IconHeight) * 0.5d;

		protected abstract View GetToolIcon ();
		protected abstract int DrawerItemCount { get; }
		protected abstract View GetDrawerItem (int index);

		Grid _toolIcon;
		Grid _toolIconColorArea;
		Image _colorIndicator;
		TriangleView _triangle;
		View[] _colorIcons;
		StackLayout _drawer;
		View _drawerMask;
		int _currentSelected;

		public virtual bool Silent { get; set; }

		protected override void InvalidateMeasure ()
		{
//			if (!Silent) {
//				base.InvalidateMeasure ();
//			}
		}

		readonly ExcludedHidingView _selectionStartedParam;

		public ToolSelectionView (ExcludedHidingView selectionStartedParam = ExcludedHidingView.None) : base ()
		{
			_selectionStartedParam = selectionStartedParam;
			_currentSelected = (int)SelectedIndexProperty.DefaultValue;
			DrawerOpen = false;

			// sub views

			InitToolIcon ();

			_drawer = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Spacing = 0,
				InputTransparent = !DrawerOpen
			};

			var containerSize = InitItemIcons ();

			_drawer.TranslationX = DrawerOpen ? 0 : -containerSize.Width;

			_drawerMask = new ScrollView () {
				WidthRequest = containerSize.Width,
				HeightRequest = containerSize.Height,
				Padding = 0,
				Content = _drawer,
				BackgroundColor = Color.Transparent
			};

			var layout = new RelativeLayout {
				HorizontalOptions	= LayoutOptions.CenterAndExpand,
				VerticalOptions		= LayoutOptions.CenterAndExpand,
				HeightRequest		= IconHeight
			};

			layout.Children.Add (_toolIcon, Constraint.Constant(0));
			layout.Children.Add (_drawerMask, 
				Constraint.RelativeToView (_toolIcon, (parentLayout, view) => {
					return (view.X + view.Width + IconToDrawerSpacing - ActionArrowWidth);
				}),
				Constraint.RelativeToView (_toolIcon, (parentLayout, view) => {
					return (view.Y - SelectedDrawIconHeightDelta);
				})
			);
			Content = layout;
			Padding = new Thickness (0, 0);
		}

		TintableImage _flippableIcon = null;

		void InitToolIcon ()
		{
			var icon = GetToolIcon ();
			_flippableIcon = icon as TintableImage;

			_colorIndicator = new Image {
				WidthRequest = ColorIndicatorWidth,
				HeightRequest = IconHeight,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			_triangle = new TriangleView {
				WidthRequest = ActionArrowWidth,
				HeightRequest = ActionArrowHeight,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
//				TranslationX = ActionArrowWidth
			};

			_toolIconColorArea = new Grid {
				ColumnSpacing = 0,
				RowSpacing = 0,
				Padding = 0,

				ColumnDefinitions = {
					new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition(){ Width = GridLength.Auto },
				},

				RowDefinitions = {
					new RowDefinition(){ Height = new GridLength(1, GridUnitType.Star) },
				},


				Children = {
					{ icon,				0, 1, 0, 1 }, 
					{ _colorIndicator,	1, 2, 0, 1 },
				},

				WidthRequest = IconWidth,
				HeightRequest = IconHeight,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			_toolIcon = new Grid {
				ColumnSpacing = 0,
				RowSpacing = 0,
				Padding = 0,

				ColumnDefinitions = {
					new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition(){ Width = GridLength.Auto },
				},

				RowDefinitions = {
					new RowDefinition(){ Height = new GridLength(1, GridUnitType.Star) },
				},
					
				Children = {
					{ _toolIconColorArea,	0, 1, 0, 1 }, 
					{ _triangle,			1, 2, 0, 1 },
				},

				WidthRequest = IconWidth + ActionArrowWidth,
				HeightRequest = IconHeight,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

		}

		protected Color IconCurrentBackgroundColor
		{
			get {
				return _toolIconColorArea.BackgroundColor;
			}

			set {
				_toolIconColorArea.BackgroundColor = value;
			}
		}

		protected Color IndicatorColor
		{
			get {
				return _colorIndicator.BackgroundColor;
			}

			set {
				_colorIndicator.BackgroundColor = value;
				_triangle.Color = value;
			}
		}

		/// <summary>
		/// Inits the color items in the drawer.
		/// </summary>
		Size InitItemIcons ()
		{
			_colorIcons = new View[DrawerItemCount];
			for (int i = 0; i < _colorIcons.Length; i++) {
				_colorIcons [i] = GetDrawerItem (i);
				_colorIcons [i].WidthRequest = DrawerItemWidth;
				_colorIcons [i].HeightRequest = (i == _currentSelected) ? SelectedIconHeight : IconHeight;
				_colorIcons [i].VerticalOptions = LayoutOptions.CenterAndExpand;
				_drawer.Children.Add (_colorIcons [i]);
			}
			return new Size (_colorIcons.Length * DrawerItemWidth, SelectedIconHeight);
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);
			if (propertyName.Equals(SelectedIndexProperty.PropertyName)) {
				Select (SelectedIndex, DrawerOpen);
			} else if (propertyName.Equals (IconVerticallyFlippedProperty.PropertyName)) {
				if (null != _flippableIcon) {
					_flippableIcon.IsVerticallyFlipped = IconVerticallyFlipped;
				}
			}
		}

		int stateCounter = 0;

		/// <summary>
		/// Toggles the drawer visibility.
		/// </summary>
		/// <param name="show">If set to <c>true</c>, show the drawer.</param>
		/// <param name="animated">If set to <c>true</c>, animate the showing / hiding process.</param>
		public async Task SetDrawerOpen (bool show, bool animated = true)
		{
			if (show != DrawerOpen) {

				var counter = ++stateCounter;

				DrawerOpen = show;
				var toTranslateX = show ? 0 : -_drawer.Width;
				var triangleToOpacity = show ? 0 : 1;
				_drawer.InputTransparent = !show;
				if (animated) {
					_drawer.IsVisible = true;
					var t1 = _drawer.TranslateTo (toTranslateX, 0, AnimationDuration);
					var t2 = _triangle.FadeTo (triangleToOpacity, AnimationDuration);
					await t1;
					await t2;
				
					if (counter == stateCounter) {
						_drawer.IsVisible = show;
					}
				} else {
					_drawer.TranslationX = toTranslateX;
					_drawer.IsVisible = show;
					_triangle.Opacity = triangleToOpacity;
				}
			}
		}

		/// <summary>
		/// Select a color in the drawer
		/// </summary>
		/// <param name="itemIndex">color index.</param>
		/// <param name="animated">If set to <c>true</c>, animate the selection process</param>
		void Select (int itemIndex, bool animated = true)
		{
			if (_currentSelected != itemIndex) {
				ViewExtensions.CancelAnimations (_colorIcons [_currentSelected]);
				ViewExtensions.CancelAnimations (_colorIcons [itemIndex]);
				if (animated) {
					var topPadding = _drawer.Padding.Top;
					var selectedItemX = _colorIcons [_currentSelected].X;
					var newItemX = _colorIcons [itemIndex].X;
					_colorIcons [_currentSelected].LayoutTo (new Rectangle (selectedItemX, topPadding + SelectedDrawIconHeightDelta, DrawerItemWidth, IconHeight), AnimationDuration);
					_colorIcons [itemIndex].LayoutTo (new Rectangle (newItemX, topPadding, DrawerItemWidth, SelectedIconHeight), AnimationDuration);
				} else {
					_colorIcons [_currentSelected].HeightRequest = IconHeight;
					_colorIcons [itemIndex].HeightRequest = SelectedIconHeight;
					this.ForceLayout ();
				}
				_currentSelected = itemIndex;
				SelectedIndex = itemIndex;
				OnSelect (itemIndex);
			}
		}

		protected virtual void OnSelect (int itemIndex)
		{
			if (!this.IsEnabled)
				return;
			if (SelectedIndexChanged != null) {
				SelectedIndexChanged (this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// Called by the custom renderer when the user touch moves inside the view
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public void OnTouchMoveInside (float x, float y)
		{
			var xInDrawer = x - _drawerMask.X - _drawer.X;
			var yInDrawer = y - _drawerMask.Y - _drawer.Y;
			for (int i = 0; i < _colorIcons.Length; i++) {
				if ((xInDrawer > _colorIcons[i].X) && (yInDrawer > _colorIcons[i].Y) && (xInDrawer < _colorIcons[i].X + _colorIcons[i].Width) && (yInDrawer < _colorIcons[i].Y + _colorIcons[i].Height)) {
					Select(i);
					break;
				}
			}
		}

		/// <summary>
		/// Called by the custom renderer when the user touch down and up inside the view
		/// </summary>
		public void OnToolSelected ()
		{
			if (!this.IsEnabled)
				return;
			if (null != ToolSelected) {
				ToolSelected (this, EventArgs.Empty);
			}
		}

		public void NotifySelectionStarted ()
		{
			if (!this.IsEnabled)
				return;
			if (null != SeletionStarted) {
				SeletionStarted (this, _selectionStartedParam);
			}
		}
	}
}

