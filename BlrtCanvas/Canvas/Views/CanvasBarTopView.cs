﻿using System;
using Xamarin.Forms;
using BlrtCanvas.Views.SilentViews;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class CanvasBarTopView : Grid
	{
		public event EventHandler CrossPressed;
		public event EventHandler SavePressed;
		public event EventHandler ReblrtPressed;

		public PageSelectionView PageSelectionView { get; set; }

		BlrtImageButton _crossBtn;

		ContentViewButton _reblrtBtn;
		ContentViewButton _saveBtn;
		bool _canReply;
		CanvasState _currentState;
		public CanvasBarTopView ()
		{
			BackgroundColor = BlrtStyles.BlrtWhiteAlpha;
			Padding = new Thickness (0, 0, 10, 0);
			HeightRequest = 45;
			InputTransparent = false;
			_currentState = CanvasState.Loading;
			_canReply = true;
			LoadingForRecord = false;

			_crossBtn = new BlrtImageButton {
				ImageSource = new FileImageSource () { 
					File = "cross.png" // TODO black cross
				},
				BackgroundColor = Color.Transparent,
				Aspect = Aspect.AspectFit,
				WidthRequest = 50
			};

			PageSelectionView = new PageSelectionView () {
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			_reblrtBtn = new ContentViewButton {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,

				WidthRequest = 95,
				ContentView = new Frame {
					VerticalOptions = LayoutOptions.Fill,
					HorizontalOptions = LayoutOptions.Fill,
					Padding = 0,
					HasShadow = false,
					OutlineColor = BlrtStyles.BlrtGreen,
					BackgroundColor = BlrtStyles.BlrtGreen,
					Content = new StackLayout {
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Orientation = StackOrientation.Horizontal,
						Padding = 0,
						Spacing = 8,
						Children = {
							new Image () { 
								Source = "btn_logo.png",
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							new Label () {
								VerticalOptions = LayoutOptions.CenterAndExpand,
								FontSize = 15,
								Text = CanvasPageView.PlatformHelper.Translate ("Reply", "canvas playback"),
								TextColor = BlrtStyles.BlrtBlackBlack
							}
						}
					}
				}
			};

			_saveBtn = new ContentViewButton {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				WidthRequest = 45,
				ContentView = new Frame {
					Padding = 0,
					HasShadow = false,
					OutlineColor = BlrtStyles.BlrtGreen,
					BackgroundColor = BlrtStyles.BlrtGreen,
					Content = new Label {
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						XAlign = TextAlignment.Center,
						YAlign = TextAlignment.Center,
						FontSize = 15,
						Text = CanvasPageView.PlatformHelper.Translate ("Save", "canvas record"),
						TextColor = BlrtStyles.BlrtBlackBlack,
					}
				}
			};

			_saveBtn.IsVisible = false;

			RowSpacing = 0;
			ColumnSpacing = 0;

			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = new GridLength (7.5, GridUnitType.Absolute) },
				new RowDefinition { Height = new GridLength (1, GridUnitType.Star) },
				new RowDefinition { Height = new GridLength (10, GridUnitType.Absolute) },
			};
			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = GridLength.Auto },
				new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) },
				new ColumnDefinition { Width = GridLength.Auto }
			};
			Children.Add (_crossBtn, 0, 1, 0, 3);

			Children.Add (PageSelectionView, 1, 2, 1, 2);

			Children.Add (_saveBtn, 2, 3, 1, 2);
			Children.Add (_reblrtBtn, 2, 3, 1, 2);


			_crossBtn.Clicked += OnCrossPressed;

			_reblrtBtn.Clicked += OnReplyPressed;
			_saveBtn.Clicked += OnSavePressed;
		}

		public void Release ()
		{
			_currentState = CanvasState.Loading;
			_canReply = true;
		}

		public void HidePopupViews (ExcludedHidingView excluded)
		{
			if (!excluded.HasFlag (ExcludedHidingView.PageSelection)) {
				// TODO hide page selection drop down
			}
		}

		public bool LoadingForRecord { get; set; }

		public bool CanReply 
		{
			get {
				return _canReply;
			}

			set {
				if (value != _canReply) {
					_canReply = value;
					SetButtonsVisible ();
				}
			}
		}

		public void SetState (CanvasState state)
		{
			_currentState = state;

			// set colors
			switch (state) {
				case CanvasState.Loading:
					if (LoadingForRecord) {
						goto default;
					} else {
						goto case CanvasState.Playback_Paused;
					}
				case CanvasState.Playback_Paused:
				case CanvasState.Playback_Playing:
					BackgroundColor = ToolbarView.PlayGrayAlpha;
					_crossBtn.TintColor = PageSelectionView.EnabledTintColor = BlrtStyles.BlrtCharcoal;
					break;
				default:
					BackgroundColor = ToolbarView.RecordCharcoalAlpha;
					_crossBtn.TintColor = PageSelectionView.EnabledTintColor = BlrtStyles.BlrtWhite;
					break;
			}

			SetButtonsVisible ();

		}

		void SetButtonsVisible ()
		{
			if (CanReply) {
				switch (_currentState) {
					case CanvasState.Playback_Paused:
					case CanvasState.Playback_Playing:
						_saveBtn.IsVisible = false;
						_reblrtBtn.IsVisible = true;
						break;
					case CanvasState.Record_PlaybackPlaying:
					case CanvasState.Record_PlaybackPaused:
						_saveBtn.IsVisible = true;
						_reblrtBtn.IsVisible = false;
						break;
					case CanvasState.Record_PostRecording:
						_saveBtn.IsVisible = true;
						_reblrtBtn.IsVisible = false;
						break;
					default:
						_reblrtBtn.IsVisible = _saveBtn.IsVisible = false;
						break;
				}
			} else {
				_reblrtBtn.IsVisible = _saveBtn.IsVisible = false;
			}
		}

		void OnCrossPressed (object sender, EventArgs args)
		{
			var e = CrossPressed;
			if (e != null) e (this, args);
		}

		void OnReplyPressed (object sender, EventArgs args)
		{
			var e = ReblrtPressed;
			if (e != null) e (this, args);
		}

		void OnSavePressed (object sender, EventArgs args)
		{
			var e = SavePressed;
			if (e != null) e (this, args);
		}
	}
}

