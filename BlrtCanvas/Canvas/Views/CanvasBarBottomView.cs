﻿using System;
using Xamarin.Forms;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using BlrtCanvas.Views.SilentViews;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class CanvasBarBottomView : Grid
	{
		public event EventHandler RecordPausePressed;
		public event EventHandler PlayPausePressed;

		public event EventHandler SliderMoved;
		public event EventHandler SliderReleased;


		public float CurrentTime { get { return _audioLength > 0 ? (float)(_slider.Value * _audioLength) : 0; } }

		static readonly double DurationLabelLength = 45;

		bool _sliderDown;

		Grid _sameColorArea;
		BlrtSwitchImageButton _recordPauseButton;
		BlrtSwitchImageButton _playPauseButton;

		PlayProgressSlider _slider;

		VolumeControlView _pbVolumeControl;
		MicrophoneIndicatorView _recMicIndicator;
		SilentLabel _sliderLeftLbl;
		SilentLabel _sliderRightLbl;

		float _audioLength;

		const int RecordButtonSize = 40;
		public CanvasBarBottomView ()
		{
			BackgroundColor = Color.Transparent;
			InputTransparent = false;
			Padding = 0;
			ColumnSpacing = 0;
			RowSpacing = 0;
			LoadingForRecord = false;

			_recordPauseButton = new BlrtSwitchImageButton () {
				ImageSource = new ImageSource[] {
					new FileImageSource () { File = "canvas_record_start.png" },
					new FileImageSource () { File = "canvas_record_stop.png" },
				},
				IsVisible = false,
				WidthRequest = RecordButtonSize,
				VerticalOptions = LayoutOptions.Fill,
			};
			_playPauseButton = new BlrtSwitchImageButton () {
				ImageSource = new ImageSource[] {
					new FileImageSource () { File = "canvas_playback_play.png" },
					new FileImageSource () { File = "canvas_playback_pause.png" },
				},
				WidthRequest = RecordButtonSize,
				VerticalOptions = LayoutOptions.Fill,
			};


			_slider = new PlayProgressSlider () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			_pbVolumeControl = new VolumeControlView ();
			_recMicIndicator = new MicrophoneIndicatorView () {
				HeightRequest = 43,
				VerticalOptions = LayoutOptions.End
			};

			_sliderLeftLbl = new SilentLabel () {
				MinimumWidthRequest 	= DurationLabelLength,
				WidthRequest 			= DurationLabelLength,
				XAlign					= TextAlignment.Center,
				YAlign					= TextAlignment.Center,
				VerticalOptions 		= LayoutOptions.CenterAndExpand,
			};
			_sliderRightLbl = new SilentLabel () {
				MinimumWidthRequest 	= DurationLabelLength,
				WidthRequest 			= DurationLabelLength,
				XAlign					= TextAlignment.Center,
				YAlign					= TextAlignment.Center,
				VerticalOptions 		= LayoutOptions.CenterAndExpand,
			};
			SetAudioTime ();

			StackLayout leftStack;
			StackLayout rightStack;

			leftStack = new StackLayout () {
				Orientation		= StackOrientation.Horizontal,
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			leftStack.Children.Add (_recordPauseButton);
			leftStack.Children.Add (_playPauseButton);

			rightStack = new StackLayout () {
				Padding			= 0,
				Spacing			= 0,

				Orientation		= StackOrientation.Horizontal,
				BackgroundColor = Color.Transparent,
				Children = { _pbVolumeControl, _recMicIndicator }
			};

			_sameColorArea = new Grid {
				ColumnSpacing = 10,
				Padding = new Thickness (10, 0),
				HeightRequest = 43,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = new RowDefinitionCollection () {
					new RowDefinition(){ Height = GridLength.Auto },
				},
				ColumnDefinitions = new ColumnDefinitionCollection () {
					new ColumnDefinition(){ Width = GridLength.Auto },
					new ColumnDefinition(){ Width = GridLength.Auto },
					new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Star) },  
					new ColumnDefinition(){ Width = GridLength.Auto }
				},
				Children = {
					{ leftStack, 0, 0 },
					{ _sliderLeftLbl, 1, 0},
					{ _slider, 2, 0},
					{ _sliderRightLbl, 3, 0}
				}
			};

			ColumnDefinitions = new ColumnDefinitionCollection () {
				new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Star) },  
				new ColumnDefinition(){ Width = GridLength.Auto },  
			};

			RowDefinitions = new RowDefinitionCollection () {
				new RowDefinition(){ Height = new GridLength(1, GridUnitType.Star) },
				new RowDefinition(){ Height = GridLength.Auto },
			};
			Children.Add ( _sameColorArea,  0, 1);
			Children.Add ( rightStack,		1, 2, 0, 2);


			_recordPauseButton.Clicked	+= OnRecordPausePressed;
			_playPauseButton.Clicked	+= OnPlayPausePressed;

			_slider.TouchDown			+= OnSliderPressed;
			_slider.ProgressChanged		+= OnSliderMoved;
			_slider.TouchUp				+= OnSliderReleased;
		}

		public bool CanContinueRecording { get; set; } 

		public void HidePopupViews (ExcludedHidingView excluded)
		{
			if (!excluded.HasFlag (ExcludedHidingView.VolumeControl)) {
				_pbVolumeControl.OpenDrawer (false, true);
			}
		}

		public bool LoadingForRecord { get; set; }

		public void Release()
		{
			_pbVolumeControl.OpenDrawer (false, false);
			_sliderDown = false;
			_slider.Value = 0;
			SetAudioLength (0f);
			_currentSeconds = -1;
			_totalSeconds = -1;
		}

		public void SetState (CanvasState state)
		{
			switch (state) {
				case CanvasState.Loading:
					_recordPauseButton.IsEnabled = false;
					_playPauseButton.IsEnabled = false;
					_slider.IsEnabled = false;
					break;
				case CanvasState.Playback_Paused:
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPaused:
				case CanvasState.Record_PlaybackPlaying:
					_recordPauseButton.IsEnabled = false;
					_playPauseButton.IsEnabled = true;
					_slider.IsEnabled = true;
					_pbVolumeControl.IsEnabled = true;
					break;

				case CanvasState.Record_PostRecording:
					_recordPauseButton.IsEnabled = CanContinueRecording;
					_playPauseButton.IsEnabled = true;
					_pbVolumeControl.IsEnabled = true;
					_slider.IsEnabled = true;
					break;
				case CanvasState.Record_PreRecording:
				case CanvasState.Record_Recording:
					_recordPauseButton.IsEnabled = true;
					_playPauseButton.IsEnabled = false;
					_slider.IsEnabled = false;
					_pbVolumeControl.OpenDrawer (false, false);
					_pbVolumeControl.IsEnabled = false;
					break;
			}

			switch (state) {
				case CanvasState.Loading:
					if (LoadingForRecord) {
						goto case CanvasState.Record_PreRecording;
					} else {
						goto case CanvasState.Playback_Paused;
					}
				case CanvasState.Playback_Paused:
				case CanvasState.Record_PlaybackPaused:
					_recordPauseButton.IsVisible = false;
					_playPauseButton.IsVisible = true;
					_playPauseButton.Current = 0;
					_pbVolumeControl.IsVisible = true;
					break;
				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					_recordPauseButton.IsVisible = false;
					_playPauseButton.IsVisible = true;
					_playPauseButton.Current = 1;
					_pbVolumeControl.IsVisible = true;
					break;
				case CanvasState.Record_PostRecording:
					_recordPauseButton.IsVisible = true;
					_recordPauseButton.Current = 0;
					if (CanContinueRecording) {
						_playPauseButton.IsVisible = true;
						_playPauseButton.Current = 0;
					} else {
						_playPauseButton.IsVisible = false;
					}
					_pbVolumeControl.IsVisible = false;
					break;
				case CanvasState.Record_PreRecording:
					_recordPauseButton.IsVisible = true;
					_recordPauseButton.Current = 0;
					_playPauseButton.IsVisible = false;
					_pbVolumeControl.IsVisible = false;
					break;
				case CanvasState.Record_Recording:
					_recordPauseButton.IsVisible = true;
					_recordPauseButton.Current = 1;
					_playPauseButton.IsVisible = false;
					_pbVolumeControl.IsVisible = false;
					break;
			}


			switch (state) {
				case CanvasState.Loading:
					if (LoadingForRecord) {
						goto default;
					} else {
						goto case CanvasState.Playback_Paused;
					}
				case CanvasState.Playback_Paused:
				case CanvasState.Playback_Playing:
					_sameColorArea.BackgroundColor = ToolbarView.PlayGrayDeepAlpha;
					_pbVolumeControl.BackgroundColor = ToolbarView.PlayGrayAlpha;
					_recMicIndicator.BackgroundColor = ToolbarView.PlayGrayAlpha;
					_pbVolumeControl.TintColor = BlrtStyles.BlrtCharcoal;
					_sliderLeftLbl.TextColor = 
						_sliderRightLbl.TextColor = BlrtStyles.BlrtCharcoal;
					_slider.TintColor = BlrtStyles.BlrtGreen;
					_slider.TrackColor = BlrtStyles.BlrtCharcoal;
					break;
				default:
					_sameColorArea.BackgroundColor = ToolbarView.RecordCharcoalDeepAlpha;
					_pbVolumeControl.BackgroundColor = ToolbarView.RecordCharcoalAlpha;
					_recMicIndicator.BackgroundColor = ToolbarView.RecordCharcoalAlpha;
					_pbVolumeControl.TintColor = BlrtStyles.BlrtWhite;
					_sliderLeftLbl.TextColor = 
						_sliderRightLbl.TextColor = BlrtStyles.BlrtWhite;

					_slider.TintColor = BlrtStyles.BlrtGreen;
					_slider.TrackColor = BlrtStyles.BlrtGray2;
					break;
			}

			switch (state) {
				case CanvasState.Record_Recording:
					_recMicIndicator.NotifyRecordStarted ();
					break;
				default:
					_recMicIndicator.NotifyRecordStopped ();
					break;
			}

			_recMicIndicator.IsEnabled = !_pbVolumeControl.IsEnabled;
			_recMicIndicator.IsVisible = !_pbVolumeControl.IsVisible;
		}

		public void SetVolume (float volume)
		{
			_recMicIndicator.SetVolume (volume);
		}

		public void SetCurrentTime (float time, bool forced = false)
		{
			if (forced || (Math.Abs (_slider.Value * _audioLength - time) > 0.01f)) {
				_slider.Value = (0 == _audioLength) ? 0 : Math.Min (1, time / _audioLength);
				SetAudioTime (time);
			}
		}

		int _currentSeconds = -1;
		int _totalSeconds = -1;

		void SetAudioTime (float time = -1)
		{
			var c = (int)((time < 0) ? CurrentTime : time);
			if (_currentSeconds != c) {
				_currentSeconds = c;
				_sliderLeftLbl.Text = BlrtData.BlrtUtil.ToMinutes (CurrentTime);
			}

			var t = (int)_audioLength;
			if (_totalSeconds != t) {
				_totalSeconds = t;
				_sliderRightLbl.Text = BlrtData.BlrtUtil.ToMinutes (_audioLength);
			}
		}

		public void SetAudioLength (float length, float startTime = 0f)
		{
			_audioLength = length;
			SetCurrentTime (startTime, true);
		}

		void OnRecordPausePressed (object sender, EventArgs args)
		{
			var e = RecordPausePressed;
			if (e != null) e (this, args);
		}
			

		void OnPlayPausePressed (object sender, EventArgs args)
		{
			var e = PlayPausePressed;
			if (e != null) e (this, args);


		}


		void OnSliderPressed (object sender, EventArgs args)
		{
			_sliderDown = true;
			OnSliderMoved (this, args);
		}

		void OnSliderMoved (object sender, EventArgs args)
		{
			if (_sliderDown) {
				var e = SliderMoved;
				if (e != null)
					e (this, args);

				SetAudioTime ();
			}
		}

		void OnSliderReleased (object sender, EventArgs args)
		{
			_sliderDown = false;

			var e = SliderReleased;
			if (e != null) e (this, args);
		}
	}
}

