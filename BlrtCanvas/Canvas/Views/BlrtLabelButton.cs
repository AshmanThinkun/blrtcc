﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas
{
	public class BlrtLabelButton : Grid
	{
		public event EventHandler Clicked {
			add {
				_btn.Clicked += value;
			}
			remove {
				_btn.Clicked -= value;
			}
		}

		ExtendedButton _btn;

		Label _label;

		public BlrtLabelButton ()
		{
			_btn = new ExtendedButton {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
			};
			_label = new Label () {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
			};

			ColumnSpacing = 0;

			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) }
			};
			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = new GridLength (1, GridUnitType.Star) }
			};

			Children.Add (_label, 0, 0);
			Children.Add (_btn, 0, 0);
		}

		bool _isTouchFeedbackColorSet = false;
		Color _touchFeedbackColor;
		Color _normalColor;
		uint _touchFeedbackDuration;
		public void SetTouchFeedbackColor (Color highlightColor, Color normalColor, uint duration = 50)
		{
			_isTouchFeedbackColorSet = true;
			_touchFeedbackColor = highlightColor;
			_normalColor = normalColor;
			_touchFeedbackDuration = duration;
			_btn.Pressed += (sender, e) => {
				TouchFeedbackHighlight (true);
			};
			_btn.DraggedEnter += (sender, e) => {
				TouchFeedbackHighlight (true);
			};
			_btn.DraggedExit += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
			_btn.ReleasedInside += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
			_btn.ReleasedOutside += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
		}

		bool _isHighlighted = false;
		const string AnimationName_Highlight = "BlrtLabelButton_highlight";
		const string AnimationName_UnHighlight = "BlrtLabelButton_unhighlight";
		Color _startColor;
		void TouchFeedbackHighlight (bool isHighlight)
		{
			if (_isHighlighted != isHighlight) {
				_isHighlighted = isHighlight;
				if (isHighlight) {
					if (this.AnimationIsRunning (AnimationName_UnHighlight)) {
						this.AbortAnimation (AnimationName_UnHighlight);
					}
					_startColor = BackgroundColor;
					this.Animate (
						AnimationName_Highlight,
						(d) => {
							BackgroundColor = new Color (
								_startColor.R + (_touchFeedbackColor.R - _startColor.R) * d,
								_startColor.G + (_touchFeedbackColor.G - _startColor.G) * d,
								_startColor.B + (_touchFeedbackColor.B - _startColor.B) * d,
								_normalColor.A
							);
						},
						24,
						_touchFeedbackDuration
					);
				} else {
					if (this.AnimationIsRunning (AnimationName_Highlight)) {
						this.AbortAnimation (AnimationName_Highlight);
					}
					_startColor = BackgroundColor;
					this.Animate (
						AnimationName_UnHighlight,
						(d) => {
							BackgroundColor = new Color (
								_startColor.R + (_normalColor.R - _startColor.R) * d,
								_startColor.G + (_normalColor.G - _startColor.G) * d,
								_startColor.B + (_normalColor.B - _startColor.B) * d,
								_normalColor.A
							);
						},
						24,
						_touchFeedbackDuration
					);
				}
			}
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == IsEnabledProperty.PropertyName) {
				_btn.IsEnabled = IsEnabled;
				Opacity = IsEnabled ? 1 : 0.5f;
			}
		}

		public string Text { get { return _label.Text; } set { _label.Text = value; }}

		public Color TextColor { get { return _label.TextColor; } set { _label.TextColor = value; }}

		public double FontSize { get { return _label.FontSize; } set { _label.FontSize = value; }}
		public string FontFamily { get { return _label.FontFamily; } set { _label.FontFamily = value; }}
		public FontAttributes FontAttributes { get { return _label.FontAttributes; } set { _label.FontAttributes = value; }}

		public TextAlignment XAlign { get { return _label.XAlign; } set { _label.XAlign = value; }}

		public TextAlignment YAlign { get { return _label.YAlign; } set { _label.YAlign = value; }}

		public LineBreakMode LineBreakMode { get { return _label.LineBreakMode; } set { _label.LineBreakMode = value; }}
	}
}

