﻿using System;
using Xamarin.Forms;

namespace BlrtCanvas.Views
{
	public class PlayProgressSlider : Slider
	{
		public const int MAX_PROGRESS = 1;


		public static readonly BindableProperty TintColorProperty =
			BindableProperty.Create<PlayProgressSlider, Color>(cell => cell.TintColor, Color.Transparent);

		public Color TintColor { get { return (Color)GetValue(TintColorProperty); } set { SetValue (TintColorProperty, value); } }

		public static readonly BindableProperty TrackColorProperty =
			BindableProperty.Create<PlayProgressSlider, Color>(cell => cell.TrackColor, Color.Transparent);

		public Color TrackColor { get { return (Color)GetValue(TrackColorProperty); } set { SetValue (TrackColorProperty, value); } }




		public class ProgressChangedArgs : EventArgs
		{
			public ProgressChangedArgs(float progress)
			{
				Progress = progress;
			}

			public float Progress { get; private set; }
		}

		public PlayProgressSlider ()
		{
			Minimum = 0;
			Maximum = MAX_PROGRESS;
		}

		const int DeltaPixels = 1;
		float _deltaProgress = 0;
		protected override void OnSizeAllocated (double width, double height)
		{
			base.OnSizeAllocated (width, height);
			_deltaProgress = (float)(DeltaPixels / width);
		}

		public event EventHandler TouchDown;
		public event EventHandler TouchUp;
		public event EventHandler<ProgressChangedArgs> ProgressChanged;

		float _sliderValue;

		/// <summary>
		/// Gets or sets the current value. This hides the Sliders value.  Fixes the problem where the value is wrong while receiving touch.
		/// </summary>
		/// <value>A double.</value>
		/// <remarks></remarks>
		public new float Value {
			get {
				return _sliderValue;
			}
			set {
				_sliderValue = value;
				var d = base.Value - _sliderValue;
				if (d < -_deltaProgress || d > _deltaProgress)
				{
					base.Value = _sliderValue;
				}
			}
		}

		public void NotifyTouchDown()
		{
			if (null != TouchDown) {
				TouchDown (this, EventArgs.Empty);
			}
		}

		public void NotifyTouchUp()
		{
			if (null != TouchUp) {
				TouchUp (this, EventArgs.Empty);
			}
		}

		public void NotifyProgressChanged(ProgressChangedArgs arg)
		{
			_sliderValue = arg.Progress;
			if (null != ProgressChanged) {
				ProgressChanged (this, arg);
			}
		}
	}
}

