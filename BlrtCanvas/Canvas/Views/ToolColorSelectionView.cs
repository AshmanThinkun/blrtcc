﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class ToolColorSelectionView : ToolSelectionView
	{
		public static readonly Color[] Colors = {
			Color.FromRgb (226, 26, 69),
			Color.FromRgb (  0,245,165),
			Color.FromRgb ( 79,153,200),
			Color.FromRgb (255,176,  0),
			Color.FromRgb (255,255,255),
			Color.FromRgb (  0,  0,  0),
		};

		public static readonly BindableProperty HighlightedProperty 
			= BindableProperty.Create<ToolColorSelectionView, bool> (t => t.Highlighted, false);

		public bool Highlighted {
			get { return (bool)base.GetValue (HighlightedProperty); }
			set { base.SetValue (HighlightedProperty, value); }
		}

		public static readonly BindableProperty HighlightIconBackColorProperty 
			= BindableProperty.Create<ToolColorSelectionView, Color> (t => t.HighlightIconBackColor, Color.Transparent);

		public Color HighlightIconBackColor {
			get { return (Color)base.GetValue (HighlightIconBackColorProperty); }
			set { base.SetValue (HighlightIconBackColorProperty, value); }
		}

		public static readonly BindableProperty IconBackColorProperty 
			= BindableProperty.Create<ToolColorSelectionView, Color> (t => t.IconBackColor, Color.Transparent);

		public Color IconBackColor {
			get { return (Color)base.GetValue (IconBackColorProperty); }
			set { base.SetValue (IconBackColorProperty, value); }
		}

		public static readonly BindableProperty IconSourceProperty 
			= BindableProperty.Create<ToolColorSelectionView, ImageSource> (t => t.IconSource, null);

		public ImageSource IconSource {
			get { return (ImageSource)base.GetValue (IconSourceProperty); }
			set { base.SetValue (IconSourceProperty, value); }
		}

		public static readonly BindableProperty IconTintColorProperty 
			= BindableProperty.Create<ToolColorSelectionView, Color> (p => p.IconTintColor, Color.Default);

		public Color IconTintColor {
			get { return (Color)base.GetValue (IconTintColorProperty); }
			set { base.SetValue (IconTintColorProperty, value); }
		}

		public static readonly BindableProperty HighlightIconTintColorProperty
			= BindableProperty.Create<ToolColorSelectionView, Color> (p => p.HighlightIconTintColor, Color.Default);

		public Color HighlightIconTintColor {
			get { return (Color)base.GetValue (HighlightIconTintColorProperty); }
			set { base.SetValue (HighlightIconTintColorProperty, value); }
		}

		TintableImage _iconImage;

		public ToolColorSelectionView (ExcludedHidingView param = ExcludedHidingView.None) : base (param)
		{
			IndicatorColor = Colors [base.SelectedIndex];
		}

		#region implemented abstract members of ToolSelectionView

		protected override View GetToolIcon ()
		{
			_iconImage = new TintableImage {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Source = IconSource,
				TintColor = IconTintColor
			};

			return _iconImage;
		}

		protected override View GetDrawerItem (int index)
		{
			return new ExtendedImage {
				BackgroundColor = Colors[index],
				BorderWidth = 0.5,
				BorderColor = BlrtStyles.BlrtCharcoal
			};
		}

		protected override int DrawerItemCount {
			get {
				return Colors.Length;
			}
		}

		#endregion

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);
			if (		propertyName.Equals(HighlightedProperty.PropertyName)) {
				OnHighlightedPropertyChanged ();
			} else if (	propertyName.Equals(HighlightIconBackColorProperty.PropertyName)) {
				OnHighlightIconBackColorPropertyChanged ();
			} else if (	propertyName.Equals(IconBackColorProperty.PropertyName)) {
				OnIconBackColorPropertyChanged ();
			} else if (	propertyName.Equals(IconTintColorProperty.PropertyName)) {
				OnIconTintColorPropertyChanged ();
			} else if (	propertyName.Equals(HighlightIconTintColorProperty.PropertyName)) {
				OnHighlightIconTintColorPropertyChanged ();
			} else if (	propertyName.Equals(IconSourceProperty.PropertyName)) {
				OnIconSourcePropertyChanged ();
			}
		}

		/// <summary>
		/// Handle the change of Highlighted property
		/// </summary>
		void OnHighlightedPropertyChanged ()
		{
			if (Highlighted) {
				_iconImage.TintColor = HighlightIconTintColor;
				IconCurrentBackgroundColor = HighlightIconBackColor;
			} else {
				_iconImage.TintColor = IconTintColor;
				IconCurrentBackgroundColor = IconBackColor;
			}
		}

		/// <summary>
		/// Handle the change of IconTintColor property
		/// </summary>
		void OnIconTintColorPropertyChanged ()
		{
			if (!Highlighted) {
				_iconImage.TintColor = IconTintColor;
			}
		}

		/// <summary>
		/// Handle the change of HighlightIconTintColor property
		/// </summary>
		void OnHighlightIconTintColorPropertyChanged ()
		{
			if (Highlighted) {
				_iconImage.TintColor = HighlightIconTintColor;
			}
		}

		/// <summary>
		/// Handle the change of HighlightIconBackColor property
		/// </summary>
		void OnHighlightIconBackColorPropertyChanged ()
		{
			if (Highlighted) {
				IconCurrentBackgroundColor = HighlightIconBackColor;
			}
		}

		/// <summary>
		/// Handle the change of IconBackColor property
		/// </summary>
		void OnIconBackColorPropertyChanged ()
		{
			if (!Highlighted) {
				IconCurrentBackgroundColor = IconBackColor;
			}
		}

		/// <summary>
		/// Handle the change of IconSource property
		/// </summary>
		void OnIconSourcePropertyChanged ()
		{
			if (!Highlighted) {
				_iconImage.Source = IconSource;
			}
		}

		protected override void OnSelect (int itemIndex)
		{
			base.OnSelect (itemIndex);
			IndicatorColor = Colors [SelectedIndex];
		}
	}
}

