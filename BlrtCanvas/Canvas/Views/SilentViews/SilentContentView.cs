﻿using System;
using Xamarin.Forms;

namespace BlrtCanvas.Views.SilentViews
{
	public class SilentContentView : ContentView
	{
		public SilentContentView ()
		{
		}

		protected override void InvalidateMeasure ()
		{
			// Do nothing
		}
	}
}

