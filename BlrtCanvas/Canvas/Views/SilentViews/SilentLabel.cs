﻿using System;
using Xamarin.Forms;

namespace BlrtCanvas.Views.SilentViews
{
	public class SilentLabel : Label
	{
		public SilentLabel ()
		{
		}

		protected override void InvalidateMeasure ()
		{
			// Do nothing
		}
	}
}

