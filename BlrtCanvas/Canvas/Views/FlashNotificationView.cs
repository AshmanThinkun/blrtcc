﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtCanvas.Views.SilentViews;

namespace BlrtCanvas.Views
{
	public class FlashNotificationView : SilentGrid
	{
		const int TotalCounters = 5;
		const string CounterFileNameFormat = "{0}canvas_count_{1}_overlay.png";
		Image _play;
		Image _pause;
		Image _record;
		Image[] _counter;

		public FlashNotificationView ()
		{
			Padding = 0;
			RowSpacing = 0;
			ColumnSpacing = 0;
			HorizontalOptions = LayoutOptions.Fill;
			VerticalOptions = LayoutOptions.Fill;

			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
			};
			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) }
			};
			BackgroundColor = Color.Transparent;
			InputTransparent = true;
			Opacity = 0;

			string idiom = null;
			if (Device.Idiom == TargetIdiom.Phone) {
				idiom = "phone_";
			} else {
				idiom = "tablet_";
			}
			_play = new Image () {
				Source = ImageSource.FromFile (idiom + "canvas_play_overlay"),
				InputTransparent = true,
				Opacity = 0,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			_pause = new Image () {
				Source = ImageSource.FromFile (idiom + "canvas_pause_overlay"),
				InputTransparent = true,
				Opacity = 0,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			_record = new Image () {
				Source = ImageSource.FromFile (idiom + "canvas_record_overlay"),
				InputTransparent = true,
				Opacity = 0,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			Children.Add (_play, 0, 0);
			Children.Add (_pause, 0, 0);
			Children.Add (_record, 0, 0);

			_counter = new Image [TotalCounters];
			for (int i = 0; i < _counter.Length; i++) {
				_counter [i] = new Image {
					InputTransparent = true,
					Opacity = 0,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Source = ImageSource.FromFile (string.Format (CounterFileNameFormat, idiom, (i + 1)))
				};
				Children.Add (_counter [i], 0, 0);
			}
		}

		async Task FlashView (View view)
		{
			if (null == view) {
				throw new NotImplementedException ();
			}
			_play.Opacity = _pause.Opacity = _record.Opacity = this.Opacity = 0;
			for (int i = 0; i < _counter.Length; i++) {
				_counter [i].Opacity = 0;
			}
			view.Opacity = 1;

			this.Opacity = 0.8;
			await this.FadeTo (0, 400);
			this.Opacity = 0;
			view.Opacity = 0;
		}

		public void FlashCountDown (int number)
		{
			if (number >= 1 && number <= 5) {
				FlashView (_counter[number - 1]);
			}
		}

		public void FlashPlay() {
			FlashView (_play);
		}

		public void FlashPause() {
			FlashView (_pause);
		}

		public void FlashRecord() {
			FlashView (_record);
		}
	}
}

