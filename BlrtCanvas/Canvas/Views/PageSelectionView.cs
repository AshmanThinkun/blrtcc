using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtCanvas.Views.SilentViews;
using BlrtData.Views.CustomUI;

namespace BlrtCanvas.Views
{
	public class PageSelectionView : StackLayout
	{
		public event EventHandler CurentPageChanged;

		Color _enabledTintColor;
		Color _disabledTintColor;

		public Color EnabledTintColor {
			get { return _enabledTintColor; }
			set {
				_enabledTintColor = value;
				SetButtonColor ();
			}
		}

		public Color DisabledTintColor {
			get { return _disabledTintColor; }
			set {
				_disabledTintColor = value;
				SetButtonColor ();
			}
		}

		public int TotalPages {
			get { return _totalPages; }
			set {
				if (_totalPages != value) {
					_totalPages = value;
					Changed (false);
				}
			}
		}

		public int CurrentPage {
			get { return _currentPage; }
			set {
				if (_currentPage != value) {
					_currentPage = value;
					Changed (false);
				}
			}
		}

		public new bool IsEnabled {
			get { return _enabled; }
			set {
				if (_enabled != value) {
					_enabled = value;
					Changed (false);
				}
			}
		}


		int _totalPages;
		int _currentPage;
		bool _enabled;

		SilentLabel _lbl;
		BlrtImageButton _prevBtn;
		BlrtImageButton _nextBtn;

		public PageSelectionView ()
		{
			_enabledTintColor = BlrtStyles.BlrtWhite;
			_disabledTintColor = BlrtStyles.BlrtGray5.MultiplyAlpha (0.5f);
			Orientation = StackOrientation.Horizontal;
			Spacing = 8;

			_enabled = true;

			_lbl = new SilentLabel () {
				FontSize = 16,
				FontAttributes = FontAttributes.None,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Center,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				BackgroundColor = Color.Transparent,
				TextColor = Color.Black
			};

			_prevBtn = new BlrtImageButton {
				ImageSource = new FileImageSource {
					File = "canvas_page_prev.png"
				},
				TintColor = DisabledTintColor
			};

			_nextBtn = new BlrtImageButton {
				ImageSource = new FileImageSource {
					File = "canvas_page_next.png"
				},
				TintColor = DisabledTintColor
			};

			var c = new Frame () {
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				HeightRequest = 27,
				Padding = 0,
				HasShadow = false,
				OutlineColor = Color.Transparent,
				WidthRequest = 80,
				Content = _lbl,
			};

			Children.Add (_prevBtn);
			Children.Add (c);
			Children.Add (_nextBtn);

			_prevBtn.Clicked += (object sender, EventArgs e) => {
				_currentPage = Math.Max (1, Math.Min (TotalPages, _currentPage - 1));
				BlrtData.Helpers.BlrtTrack.CanvasChangePage("pre");
				Changed (true);
			};
			_nextBtn.Clicked += (object sender, EventArgs e) => {
				_currentPage = Math.Max (1, Math.Min (TotalPages, _currentPage + 1));
				BlrtData.Helpers.BlrtTrack.CanvasChangePage("next");
				Changed (true);
			};

			TotalPages = 1;
			CurrentPage = 1;

			Changed (false);
		}

		void Changed (bool fireEvent)
		{
			_lbl.Text = string.Format ("{0} / {1}", CurrentPage, TotalPages);

			_prevBtn.IsEnabled = IsEnabled && CurrentPage > 1;
			_nextBtn.IsEnabled = IsEnabled && CurrentPage < TotalPages;

			SetButtonColor ();

			if (fireEvent && CurentPageChanged != null) {
				CurentPageChanged (this, EventArgs.Empty);
			}
		}

		void SetButtonColor ()
		{
			_prevBtn.TintColor = _prevBtn.IsEnabled ? EnabledTintColor : DisabledTintColor;
			_nextBtn.TintColor = _nextBtn.IsEnabled ? EnabledTintColor : DisabledTintColor;
		}
	}
}

