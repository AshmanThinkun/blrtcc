﻿using System;
using Xamarin.Forms;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections.Generic;
using BlrtData;
using Parse;

namespace BlrtCanvas.Views
{
	public class CanvasGLView : ContentView
	{
		public CanvasCameraMode CameraMode { get { return _canvas.CameraMode; } set { _canvas.CameraMode = value; } }

		public int CurrentPage {
			get { return _canvas.CurrentPage; }
			set { 
				if (value != CurrentPage) {
					_canvas.SetPage (value); 
					Redraw ();
				}
			}
		}

		public CanvasCameraMode CameraModeDefault { get; private set; }

		bool _isRefreshForced;

		bool _active;

		public bool Active { get { return _canvas != null && _canvas.IsSetup && _active; } }

		BCCanvas _canvas;

		ICanvasManager _canvasManager;

		OpenGLView _child;

		bool _redrawRequested;

		bool _expectedRLState = false;

		private bool ExpectedRenderloopState {
			get {
				return _expectedRLState;
			}

			set {
				if (true == value) {
					_child.HasRenderLoop = true;
				}
				_expectedRLState = value;
			}
		}

		public event EventHandler GestureFinished;
		public event EventHandler SingleTapped;
		public event EventHandler<ExcludedHidingView> TouchStarted;

		public CanvasGLView (ICanvasManager canvasManager)
		{
			_canvasManager = canvasManager;
			_canvas = new BCCanvas (canvasManager);
			_redrawRequested = false;
			ExpectedRenderloopState = false;
			Content = _child = new OpenGLView () {
				HasRenderLoop = false
			};
			_child.OnDisplay += DisplayUpdate;


			BackgroundColor = Xamarin.Forms.Color.FromRgba (0xCC, 0xCC, 0xCC, 0xFF);
		}

		public void SetState (CanvasState state)
		{
			switch (state) {
				case CanvasState.Record_PostRecording:
					FinishCurrentGesture ();
					// fall through to the next case
					goto case CanvasState.Loading;
				case CanvasState.Loading:
				case CanvasState.Playback_Paused:
				case CanvasState.Record_PlaybackPaused:
				case CanvasState.Record_PreRecording:
					CameraModeDefault = CameraMode = CanvasCameraMode.Manual;
					ExpectedRenderloopState = false;
					break;

				case CanvasState.Playback_Playing:
				case CanvasState.Record_PlaybackPlaying:
					CameraModeDefault = CameraMode = CanvasCameraMode.Auto;
					ExpectedRenderloopState = true;
					_canvas.Camera.SetToCurrentPosition ();
					break;

				case CanvasState.Record_Recording:
					_canvas.StartNewRecordSection ();
					CameraModeDefault = CameraMode = CanvasCameraMode.Manual;
					ExpectedRenderloopState = true;
					break;
			}

			Redraw ();
		}
		
		public void OnRecorderStopped(float time)
		{
			_canvas.FinishCurrentRecordSection (time);
		}

		protected override SizeRequest OnSizeRequest (double widthConstraint, double heightConstraint)
		{
			if (ParentView != null)
				return new SizeRequest (new Xamarin.Forms.Size (ParentView.Width, ParentView.Height));

			return base.OnSizeRequest (widthConstraint, heightConstraint);
		}

		protected override void OnSizeAllocated (double width, double height)
		{
			base.OnSizeAllocated (width, height);
			var newVPWidth = width * CanvasPageView.PlatformHelper.ScreenScale;
			var newVPHeight = height * CanvasPageView.PlatformHelper.ScreenScale;
			if ((_canvas.Viewport.Width != newVPWidth) || (_canvas.Viewport.Height != newVPHeight)) {
				_canvas.Viewport = new SizeF ((float)newVPWidth, (float)newVPHeight);
			}
			Redraw ();
		}


		void DisplayUpdate (Xamarin.Forms.Rectangle frame)
		{
			_redrawRequested = false;

			if (!_canvas.IsSetup)
				_canvas.Setup ();

			if (_active) {
				var force = _isRefreshForced;

				_canvas.Update (force);
				if (force || _canvas.IsDirty || true) {

					_canvas.Redraw ();

					if (force)
						_isRefreshForced = false;
				}
				_canvas.DoActions ();

			}

			if (_redrawRequested) {
				_child.HasRenderLoop = true;
			} else {
				_child.HasRenderLoop = ExpectedRenderloopState;
			}
		}

		public void Redraw ()
		{
			if (Active) {
				_isRefreshForced = true;
				_redrawRequested = true;
				if (!_child.HasRenderLoop) {
					Device.BeginInvokeOnMainThread (() => {
						_child.Display ();
					});
				}
			}
		}

		public async Task LoadData (IBlrtCanvasData data)
		{
			_active = true;
			await _canvas.LoadArgument (data);
			await _canvas.WaitFirstTexture ();
		}

		public async Task Release (CanvasReleaseParameter parameters)
		{
			await _canvas.Release (parameters);
			_active = false;
			_child.HasRenderLoop = false;
			// this view's own release
			_isRefreshForced = false;
			_redrawRequested = false;
			ExpectedRenderloopState = false;
			CameraModeDefault = CanvasCameraMode.Manual;

			// guesture
			_currentGesture = null;
			id = 0;
			_camera = false;
			_singleTapStarted = false;
			_singleTapStartX = 0f;
			_singleTapStartY = 0f;
			_singleTapId = 0;
			_preCount = 0;
			_singleTapStartTime = DateTime.MinValue;
		}

		public void WriteCanvasData (string path)
		{
			_canvas.WriteCanvasData (path);
		}



		public void Undo ()
		{
			_canvas.Undo ();
		}

		public void Redo ()
		{
			_canvas.Redo ();
		}

		public bool CanUndo { get { return _canvas.CanUndo && _canvasManager.IsRecording; } }

		public bool CanRedo { get { return _canvas.CanRedo && _canvasManager.IsRecording; } }


		TouchGesture _currentGesture = null;
		int id = 0;

		bool _camera;

		const float SingleTapDistanceSquare = 10f * 10f;
		bool _singleTapStarted = false;
		float _singleTapStartX = 0f;
		float _singleTapStartY = 0f;
		int _singleTapId = 0;
		DateTime _singleTapStartTime;
		int _preCount = 0;

		public void ReceivedTouch (object sender, CanvasTouchCollection touches)
		{
			if (!Active) {
				Redraw ();
				return;
			}


			int count = 0;
			for (int i = 0; i < touches.Count; ++i) {
				var t = touches.Get (i);

				if (t.State != CanvasTouch.CanvasTouchState.Released)
					++count;
			}
			if (_preCount != count) {
				if (0 == _preCount && (count > 0)) {
					if (null != TouchStarted) {
						TouchStarted (this, ExcludedHidingView.None);
					}
				} 
//				else if (0 == count && (_preCount > 0)) {
//					release all
//				}
				_preCount = count;
			}
			if (_canvasManager.IsRecording) {
				if (count == 0) {
					_camera = false;
				} else if (_currentGesture == null || _canvasManager.CurrentTime - _currentGesture.StartedAt < 0.5f) { 
					if (count > 1 && !_camera) {
						_camera = true;

						if (_currentGesture != null) {
							_currentGesture.Cancel ();
							_canvas.NotifyGestureCancelled (_currentGesture);
							_currentGesture = null;
						}

						id = 0;

					} 
				} 

				if (_camera) {
					MoveCamera (touches);					
				} else {
					AddGesture (touches);						
				}
					
			} else {
				MoveCamera (touches);
			}

			if (touches.Count == 1) {
				var t = touches.Get (0);
				if (t.State == CanvasTouch.CanvasTouchState.Pressed) {
					_singleTapStartX = t.X;
					_singleTapStartY = t.Y;
					_singleTapId = t.Id;
					_singleTapStartTime = DateTime.UtcNow;
					_singleTapStarted = true;
				} else {
					if (_singleTapStarted && (t.State == CanvasTouch.CanvasTouchState.Released)) {
						if ((_singleTapId == t.Id) 
							&& (DistanceSquare (_singleTapStartX, _singleTapStartY, t.X, t.Y) <= SingleTapDistanceSquare)
							&& (DateTime.UtcNow - _singleTapStartTime < TimeSpan.FromMilliseconds (500))) {
							if (null != SingleTapped) {
								SingleTapped (this, null);
							}
						}
						_singleTapStarted = false;
						_singleTapStartX = 0;
						_singleTapStartY = 0;
						_singleTapId = 0;
					}
				}
			} else {
				_singleTapStarted = false;
				_singleTapStartX = 0;
				_singleTapStartY = 0;
				_singleTapId = 0;
			}
		}

		static float DistanceSquare (float x1, float y1, float x2, float y2)
		{
			var dx = x1 - x2;
			var dy = y1 - y2;
			return dx * dx + dy * dy;
		}

		static bool HasCheckedZoomed = false;
		static bool HasCheckedDrawn = false;
		const int MaxSendRetry = 5;

		void AddGesture (CanvasTouchCollection touches)
		{
			var touch = _currentGesture != null ? touches.GetFromId (id) : null;

			if (touch == null || _currentGesture == null) {

				if (_currentGesture != null) {
					_currentGesture.IsEditing = false;
					_currentGesture = null;
					id = 0;
				}

				for (int i = 0; i < touches.Count; ++i) {
					var t = touches.Get (i);

					if (t.State == CanvasTouch.CanvasTouchState.Pressed) {
						id = t.Id;
						_currentGesture = new TouchGesture () {
							Color = _canvasManager.BrushColor,
							Page = _canvas.CurrentPage,
							IsEditing	= true,
							Scale = _canvas.CurrentZoom * _canvas.LineScale,
							Type = _canvasManager.SelectedBrush,
							Thickness	= (int)_canvasManager.BrushWidth,
						};

						_canvas.AddGesture (_currentGesture);

						if ((!HasCheckedDrawn) && (_currentGesture.Type != BlrtBrush.Pointer)) {
							SetHasDrawn ();
						}
					}
				}
			}

			if (touch != null) {
				switch (touch.State) {
					case CanvasTouch.CanvasTouchState.Idle:
					case CanvasTouch.CanvasTouchState.Invalid:
						break;

					case CanvasTouch.CanvasTouchState.Released:

						_currentGesture.IsEditing = false;
						_currentGesture.AddPoint (
							_canvas.Camera.ScreenToWorld (touch.X, touch.Y), 
							_canvasManager.CurrentTime
						);
						_currentGesture.UpdateCameraOffset (
							_canvas.Camera.ScreenToCamera (touch.X, touch.Y)
						);
						_canvas.NotifyGestureFinished (_currentGesture);
						_currentGesture = null;
						id = 0;
						if (null != GestureFinished) {
							GestureFinished (this, null);
						}
						break;
					default:
						_currentGesture.AddPoint (
							_canvas.Camera.ScreenToWorld (touch.X, touch.Y), 
							_canvasManager.CurrentTime
						);
						_currentGesture.UpdateCameraOffset (
							_canvas.Camera.ScreenToCamera (touch.X, touch.Y)
						);
						break;
				}
			}
			Redraw ();
		}

		void FinishCurrentGesture ()
		{
			if ((_currentGesture != null) && _currentGesture.IsEditing) {
				_currentGesture.IsEditing = false;
				_canvas.NotifyGestureFinished (_currentGesture);
				_currentGesture = null;
				if (null != GestureFinished) {
					GestureFinished (this, null);
				}
			}
		}

		public void MoveCamera (CanvasTouchCollection touches)
		{
			int count = 0;

			for (int i = 0; i < touches.Count; ++i) {
				var t = touches.Get (i);
			
				if (t.State != CanvasTouch.CanvasTouchState.Released)
					++count;
			}

			if (count > 0) {
				CameraMode = CanvasCameraMode.Manual;

				var list = new List<TouchDelta> ();

				for (int i = 0; i < touches.Count; ++i) {

					var t = touches.Get (i);
					var p1 = _canvas.Camera.ScreenToWorld (t.X, t.Y);
					var p2 = _canvas.Camera.ScreenToWorld (t.LastX, t.LastY);

					list.Add (new TouchDelta (p2, new PointF (p1.X - p2.X, p1.Y - p2.Y)));
				}

				float x = 0;
				float y = 0;
				float z = 0;

				for (int i = 0; i < list.Count; ++i) {

					var p1 = list [i];
					x -= p1.n.X / list.Count;
					y -= p1.n.Y / list.Count;

					for (int j = i + 1; j < list.Count; ++j) {

						var p2 = list [j];

						float tmpX1 = p1.position.X - p2.position.X;
						float tmpY1 = p1.position.Y - p2.position.Y;

						float tmpX2 = p1.position.X - p2.position.X + (p1.n.X - p2.n.X);
						float tmpY2 = p1.position.Y - p2.position.Y + (p1.n.Y - p2.n.Y);

						float tmp = (float)(Math.Sqrt (tmpX2 * tmpX2 + tmpY2 * tmpY2) - Math.Sqrt (tmpX1 * tmpX1 + tmpY1 * tmpY1))
						            / list.Count * 2 * 2
						            / Math.Min (_canvas.Camera.Bounds.Width, _canvas.Camera.Bounds.Height); 


						if (!float.IsNaN (tmp) && !float.IsInfinity (tmp)) {

							//so when we zoom, we also must move to ensure the zoom is in the center of the two fingers, not the center of the canvas
							x += ((p1.position.X + p2.position.X) * 0.5f - _canvas.Camera.CurrentX) * tmp;
							y += ((p1.position.Y + p2.position.Y) * 0.5f - _canvas.Camera.CurrentY) * tmp;

							z += tmp * _canvas.Camera.CurrentZoom;

							if ((!HasCheckedZoomed) && (0 != tmp)) {
								SetHasZoomed ();
							}
						}
					}

				}

				_canvas.Camera.Translate (x, y, z);

			} else {

				CameraMode = CameraModeDefault;

			}


			Redraw ();
		}

		void SetHasDrawn ()
		{
			HasCheckedDrawn = true;
			Task.Run (async () => {
				var hasDrawn = false;
				try {
					hasDrawn = ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HAS_DRAWN_KEY, false);
				} catch {}
				if (!hasDrawn) {
					for (int ti = 0; ti < MaxSendRetry; ++ti) {
						try {
							ParseUser.CurrentUser[BlrtUserHelper.HAS_DRAWN_KEY] = true;
							await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
						} catch {
							continue;
						}
						break;
					}
				}
			});
		}

		void SetHasZoomed ()
		{
			HasCheckedZoomed = true;
			Task.Run (async () => {
				var hasZoomed = false;
				try {
					hasZoomed = ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HAS_ZOOOMED_KEY, false);
				} catch {}
				if (!hasZoomed) {
					for (int ti = 0; ti < MaxSendRetry; ++ti) {
						try {
							ParseUser.CurrentUser[BlrtUserHelper.HAS_ZOOOMED_KEY] = true;
							await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
						} catch {
							continue;
						}
						break;
					}
				}
			});
		}


		public class TouchDelta
		{
			public PointF position;
			public PointF n;

			public TouchDelta (PointF p, PointF n)
			{
				this.position = p;
				this.n = n;
			}
		}
	}
}

