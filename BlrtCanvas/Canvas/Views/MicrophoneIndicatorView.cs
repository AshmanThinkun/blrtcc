﻿using System;
using Xamarin.Forms;

namespace BlrtCanvas.Views
{
	public class MicrophoneIndicatorView : ContentView
	{
		Image _microphoneRecording;
		Image _microphoneIdle;
		Image[] _volume;

		const string microWsSound = "canvas_record_microphone_wsound.png";
		const string microNoSound = "canvas_record_microphone_nosound.png";

		const string lvl0 = "canvas_record_volume_{0}.png";
		const int TotalLevel = 6;

		bool _isRecording;
		int _showingVol;
		public MicrophoneIndicatorView () : base ()
		{
			_microphoneIdle = new Image () {
				Source = microNoSound,

				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Opacity = 1
			};

			_microphoneRecording = new Image () {
				Source = microWsSound,

				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Opacity = 0
			};

			_volume = new Image[TotalLevel];

			_isRecording = false;

			var grid = new Grid () {
				Padding = 10,
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},

				RowSpacing = 0,
				ColumnSpacing = 0,


				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { 
					{_microphoneIdle, 0, 0},
					{_microphoneRecording, 0, 0},
				}
			};

			for (int i = 0; i < _volume.Length; i++) {
				_volume[i] = new Image () {
					Source = string.Format(lvl0 , i),

					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Center,
					Opacity = 0
				};

				grid.Children.Add (_volume[i], 1, 0);
			}
			_showingVol = 0;
			_volume[_showingVol].Opacity = 1;
			Content = grid;
		}

		public void NotifyRecordStarted ()
		{
			if (!_isRecording) {
				_isRecording = true;
				_microphoneRecording.Opacity = 1;
				_microphoneIdle.Opacity = 0;
			}
		}

		public void NotifyRecordStopped ()
		{
			if (_isRecording)
			{
				_isRecording = false;
				_microphoneIdle.Opacity = 1;
				_microphoneRecording.Opacity = 0;
				_volume[0].Opacity = 1;
				for (int i = 1; i < _volume.Length; i++) {
					_volume [i].Opacity = 0;
				}
				_showingVol = 0;

			}
		}

		static float[] arr = new float[] { 0f, 0.3f, 0.46f, 0.6f, 0.7f, 0.9f };
		public void SetVolume(float volume)
		{
			if (_isRecording) {
				int vol = 0;
				for (vol = arr.Length - 1; vol > 0; --vol) {
					if (volume >= arr[vol]) {
						break;
					}
				}
				if (vol != _showingVol) {
					_volume[vol].Opacity = 1;
					_volume[_showingVol].Opacity = 0;
					_showingVol = vol;

				}
			}
		}
	}
}

