﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtData;
using BlrtData.Views.CustomUI;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BlrtCanvas.Views
{
	public class ToolbarView : ContentView
	{
		public static readonly Color RecordCharcoalAlpha = BlrtStyles.BlrtCharcoal.MultiplyAlpha (0.8);
		public static readonly Color RecordCharcoalDeepAlpha = BlrtStyles.BlrtCharcoal.MultiplyAlpha (0.96);

		public static readonly Color PlayGrayAlpha = BlrtStyles.CanvasPlayBarColor.MultiplyAlpha (0.8);
		public static readonly Color PlayGrayDeepAlpha = BlrtStyles.CanvasPlayBarColor.MultiplyAlpha (0.95);

		public event EventHandler ToolsChanged;
		public event EventHandler UndoPressed;
		public event EventHandler RedoPressed;
		public event EventHandler<ExcludedHidingView> SelectionStarted
		{
			add {
				_pen.			SeletionStarted += value;
				_pointer.		SeletionStarted += value; 
				_line.			SeletionStarted += value; 
				_circle.		SeletionStarted += value; 
				_rectangle.		SeletionStarted += value;
				_widthSelection.SeletionStarted += value;
			}

			remove {
				_pen.			SeletionStarted -= value;
				_pointer.		SeletionStarted -= value; 
				_line.			SeletionStarted -= value; 
				_circle.		SeletionStarted -= value; 
				_rectangle.		SeletionStarted -= value;
				_widthSelection.SeletionStarted -= value;
			}
		}

		public BlrtBrush SelectedBrush {
			get;
			private set;
		}

		public BlrtBrushWidth BrushWidth {
			get {
				switch (_widthSelection.SelectedIndex) {
					case 0:
						return BlrtBrushWidth.Thin;
					case 2:
						return BlrtBrushWidth.Thick;
					default:
						return BlrtBrushWidth.Meduim;
				}
			}
		}

		public BlrtBrushColor BrushColor {
			get { return GetColor (_selected.SelectedIndex); }
		}

		protected enum AbsLayoutElement
		{
			Undo,
			Pen,
			Pointer,
			Line,
			Circle,
			Rectangle,
			WidthSelection,
			Hide,
			Redo,
			LeftBackground,
			RightBackground
		};

		protected enum ToolbarLayout
		{
			Left,
			Split,
			Right,
			RightMinified
		};

		protected class LayoutParam
		{
			public Rectangle Bounds;
			public AbsoluteLayoutFlags Layout;
		}

		protected static readonly double ToolWidth = ToolSelectionView.IconWidth 
			+ ToolSelectionView.IconToDrawerSpacing 
			+ ToolSelectionView.DrawerItemWidth * ToolColorSelectionView.Colors.Length;

		const double SeparatorHeight = 1d;
		static readonly double IconHeightOcupation = ToolSelectionView.IconHeight + SeparatorHeight;

		protected static readonly IReadOnlyDictionary<AbsLayoutElement, LayoutParam> Param_Flexible_Normal = null;
		protected static readonly IReadOnlyDictionary<AbsLayoutElement, LayoutParam> Param_Flexible_Split = null;
		protected static readonly IReadOnlyDictionary<AbsLayoutElement, LayoutParam> Param_AlwaysRight_Normal = null;
		protected static readonly IReadOnlyDictionary<AbsLayoutElement, LayoutParam> Param_AlwaysRight_Minified = null;

		static ToolbarView ()
		{
			Param_Flexible_Normal = new ReadOnlyDictionary<AbsLayoutElement, LayoutParam> (new Dictionary<AbsLayoutElement, LayoutParam> {
				{
					AbsLayoutElement.Undo,
					new LayoutParam {
						Bounds = new Rectangle (0, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pen,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pointer,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Line,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Circle,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 3, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Rectangle,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.WidthSelection,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Hide,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 8 + IconHeightOcupation * 6, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Redo,
					new LayoutParam {
						Bounds = new Rectangle (1, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.LeftBackground,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 6 - SeparatorHeight),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.RightBackground,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 2),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				}
			});

			Param_Flexible_Split = new ReadOnlyDictionary<AbsLayoutElement, LayoutParam> (new Dictionary<AbsLayoutElement, LayoutParam> {
				{
					AbsLayoutElement.Undo,
					new LayoutParam {
						Bounds = new Rectangle (0, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pen,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pointer,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Line,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Circle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Rectangle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.WidthSelection,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Hide,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 8 + IconHeightOcupation * 3, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Redo,
					new LayoutParam {
						Bounds = new Rectangle (1, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.LeftBackground,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 2),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.RightBackground,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 3 - SeparatorHeight),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				}
			});

			Param_AlwaysRight_Normal = new ReadOnlyDictionary<AbsLayoutElement, LayoutParam> (new Dictionary<AbsLayoutElement, LayoutParam> {
				{
					AbsLayoutElement.Undo,
					new LayoutParam {
						Bounds = new Rectangle (0, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pen,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Pointer,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Line,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Circle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 3, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Rectangle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.WidthSelection,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Hide,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 8 + IconHeightOcupation * 6, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Redo,
					new LayoutParam {
						Bounds = new Rectangle (1, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.LeftBackground,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 2),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.RightBackground,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 6 - SeparatorHeight),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				}
			});

			Param_AlwaysRight_Minified = new ReadOnlyDictionary<AbsLayoutElement, LayoutParam> (new Dictionary<AbsLayoutElement, LayoutParam> {
				{
					AbsLayoutElement.Undo,
					new LayoutParam {
						Bounds = new Rectangle (0, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.Pen,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Pointer,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Line,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Circle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 3, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Rectangle,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4 + IconHeightOcupation * 4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Hide,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 8 + IconHeightOcupation * 5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.Redo,
					new LayoutParam {
						Bounds = new Rectangle (1, 0, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				},
				{
					AbsLayoutElement.LeftBackground,
					new LayoutParam {
						Bounds = new Rectangle (0, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 2),
						Layout = AbsoluteLayoutFlags.None
					}
				},
				{
					AbsLayoutElement.RightBackground,
					new LayoutParam {
						Bounds = new Rectangle (1, ToolSelectionView.IconHeight * 0.8f + 4, ToolSelectionView.IconWidth, IconHeightOcupation * 5 - SeparatorHeight),
						Layout = AbsoluteLayoutFlags.XProportional
					}
				}
			});
		}

		BlrtLabelButton	_undo;
		BlrtLabelButton	_redo;

		BlrtSwitchImageButton _hideView;
		ToolColorSelectionView _selected;

		ToolColorSelectionView[] _buttonList;
		View[] _groupLeft;
		View[] _groupRight;

		BoxView _backgroundLeft;
		BoxView _backgroundRight;

		ToolColorSelectionView _pen;
		ToolColorSelectionView _pointer; 
		ToolColorSelectionView _line; 
		ToolColorSelectionView _circle; 
		ToolColorSelectionView _rectangle;

		ToolImageSelectionView _widthSelection;

		double _fullHeight;
		ToolbarLayout _currentLayout;

		IReadOnlyDictionary<AbsLayoutElement, View> _absViewMapping;

		public ToolbarView ()
		{
			Padding = 0;
			_fullHeight = 0;
			_currentLayout = ToolbarLayout.Left;

			_backgroundLeft = new BoxView {
				BackgroundColor = BlrtStyles.ToolbarWidthDrawerColor,
				Opacity = 1,
				InputTransparent = true
			};

			_backgroundRight = new BoxView {
				BackgroundColor = BlrtStyles.ToolbarWidthDrawerColor,
				Opacity = 0,
				InputTransparent = true
			};

			_undo = new BlrtLabelButton () {
				HorizontalOptions	= LayoutOptions.Start,

				BackgroundColor 	= BlrtStyles.BlrtGreen,

				WidthRequest		= ToolSelectionView.IconWidth,
				HeightRequest		= ToolSelectionView.IconHeight * 0.8f,
				Text				= CanvasPageView.PlatformHelper.Translate ("undo", "canvas toolbar"),
				TextColor			= BlrtStyles.BlrtBlackBlack,
				FontSize 			= 12,
				XAlign				= TextAlignment.Center,
				YAlign				= TextAlignment.Center,
				LineBreakMode		= LineBreakMode.NoWrap,
				IsEnabled			= false,
				Opacity				= 0
			};
			_undo.SetTouchFeedbackColor (BlrtStyles.BlrtAccentRed, _undo.BackgroundColor);
			_fullHeight += _undo.HeightRequest;
			_redo = new BlrtLabelButton () {
				HorizontalOptions 	= LayoutOptions.End,

				BackgroundColor 	= BlrtStyles.BlrtGreen,
				WidthRequest		= ToolSelectionView.IconWidth,
				HeightRequest		= ToolSelectionView.IconHeight * 0.8f,
				Text				= CanvasPageView.PlatformHelper.Translate ("redo", "canvas toolbar"),
				TextColor			= BlrtStyles.BlrtBlackBlack,
				FontSize 			= 12,
				XAlign				= TextAlignment.Center,
				YAlign				= TextAlignment.Center,
				LineBreakMode		= LineBreakMode.NoWrap,
				IsEnabled			= false,
				Opacity				= 0
			};
			_redo.SetTouchFeedbackColor (BlrtStyles.BlrtAccentRed, _redo.BackgroundColor);
			_fullHeight += _redo.HeightRequest;


			_pen		= new ToolColorSelectionView () {
				HighlightIconBackColor	= BlrtStyles.BlrtWhite,
				IconBackColor			= BlrtStyles.BlrtCharcoal,

				IconSource				= ImageSource.FromFile("canvas_tool_pen.png"),

				IconTintColor 			= BlrtStyles.BlrtWhite,
				HighlightIconTintColor 	= BlrtStyles.BlrtCharcoal,

				HorizontalOptions = LayoutOptions.Start

			};
			_pointer	= new ToolColorSelectionView () {
				HighlightIconBackColor	= BlrtStyles.BlrtWhite,
				IconBackColor			= BlrtStyles.BlrtCharcoal,

				IconSource				= ImageSource.FromFile("canvas_tool_pointer.png"),

				IconTintColor 			= BlrtStyles.BlrtWhite,
				HighlightIconTintColor 	= BlrtStyles.BlrtCharcoal,

				HorizontalOptions = LayoutOptions.Start,

			};
			_line		= new ToolColorSelectionView () {
				HighlightIconBackColor	= BlrtStyles.BlrtWhite,
				IconBackColor			= BlrtStyles.BlrtCharcoal,

				IconSource				= ImageSource.FromFile("canvas_tool_line.png"),

				IconTintColor 			= BlrtStyles.BlrtWhite,
				HighlightIconTintColor 	= BlrtStyles.BlrtCharcoal,

				HorizontalOptions = LayoutOptions.Start,

			};
			_circle		= new ToolColorSelectionView () {
				HighlightIconBackColor	= BlrtStyles.BlrtWhite,
				IconBackColor			= BlrtStyles.BlrtCharcoal,

				IconSource				= ImageSource.FromFile("canvas_tool_circle.png"),

				IconTintColor 			= BlrtStyles.BlrtWhite,
				HighlightIconTintColor 	= BlrtStyles.BlrtCharcoal,

				HorizontalOptions = LayoutOptions.Start,

			};
			_rectangle	= new ToolColorSelectionView () {
				HighlightIconBackColor	= BlrtStyles.ToolbarIconHighlightColor,
				IconBackColor			= BlrtStyles.BlrtCharcoal,

				IconSource				= ImageSource.FromFile("canvas_tool_rectangle.png"),

				IconTintColor 			= BlrtStyles.BlrtWhite,
				HighlightIconTintColor 	= BlrtStyles.BlrtCharcoal,

				HorizontalOptions = LayoutOptions.Start,

			};

			_widthSelection		= new ToolImageSelectionView (ExcludedHidingView.LineWidth) {
				IconBackColor			= BlrtStyles.BlrtCharcoal,
				DrawerItemBackColor		= BlrtStyles.ToolbarWidthDrawerColor,

				HorizontalOptions		= LayoutOptions.Start,

				OpenOnTapped			= true,
			};

			_fullHeight += 6 * ToolSelectionView.IconHeight;

			_fullHeight += 8;
			_groupLeft = new View [] { _backgroundLeft, _pen, _pointer, _line };
			_groupRight = new View [] { _backgroundRight, _circle, _rectangle, _widthSelection };

			_buttonList = new [] {
				_pen,
				_pointer,
				_line,
				_circle,
				_rectangle,
			};

			_hideView = new BlrtSwitchImageButton () {
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
				WidthRequest = ToolSelectionView.IconWidth,
				HeightRequest = ToolSelectionView.IconHeight * 0.6f,
				BackgroundColor = RecordCharcoalAlpha,
				ImageSource = new ImageSource[] {
					new FileImageSource () { File = "canvas_hide.png" },
					new FileImageSource () { File = "canvas_hide_active.png" },
				},
			};
			_hideView.SetTouchFeedbackColor (BlrtStyles.BlrtAccentRed, _hideView.BackgroundColor);

			_fullHeight += _hideView.HeightRequest + 4;


			_absViewMapping = new ReadOnlyDictionary<AbsLayoutElement, View> (new Dictionary<AbsLayoutElement, View> {
				{ AbsLayoutElement.Undo,			_undo			 },
				{ AbsLayoutElement.Pen,				_pen			 },
				{ AbsLayoutElement.Pointer,			_pointer		 },
				{ AbsLayoutElement.Line,			_line			 },
				{ AbsLayoutElement.Circle,			_circle			 },
				{ AbsLayoutElement.Rectangle,		_rectangle		 },
				{ AbsLayoutElement.WidthSelection,	_widthSelection	 },
				{ AbsLayoutElement.Hide,			_hideView		 },
				{ AbsLayoutElement.Redo,			_redo			 },
				{ AbsLayoutElement.LeftBackground,	_backgroundLeft	 },
				{ AbsLayoutElement.RightBackground,	_backgroundRight }
			});

			var mainLayout = new AbsoluteLayout {
				Padding = 0,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = {
					_backgroundLeft,
					_backgroundRight,
					_undo,
					_pen, 
					_pointer, 
					_line,
					_circle, 
					_rectangle, 
					_widthSelection,
					_hideView,
					_redo
				}
			};

			FlexibleLayout_CombileTools (true);

			Content = mainLayout;

			_pen.SelectedIndex				= GetColorIndex (CanvasStateManager.Last.GetColor (BlrtBrush.Pen			) );
			_pointer.SelectedIndex			= GetColorIndex (CanvasStateManager.Last.GetColor (BlrtBrush.Pointer		) );
			_line.SelectedIndex				= GetColorIndex (CanvasStateManager.Last.GetColor (BlrtBrush.ShapeLine		) );
			_circle.SelectedIndex			= GetColorIndex (CanvasStateManager.Last.GetColor (BlrtBrush.ShapeCircle	) );
			_rectangle.SelectedIndex		= GetColorIndex (CanvasStateManager.Last.GetColor (BlrtBrush.ShapeRectangle	) );

			switch (CanvasStateManager.Last.width) {
				case BlrtBrushWidth.Thick:
					_widthSelection.SelectedIndex	= 2;
					break;
				case BlrtBrushWidth.Thin:
					_widthSelection.SelectedIndex	= 0;
					break;
				default:
					_widthSelection.SelectedIndex	= 1;
					break;
			}

			_pen.ToolSelected					+= (object sender, EventArgs e) => { SetBrush(BlrtBrush.Pen);			};
			_pointer.ToolSelected				+= (object sender, EventArgs e) => { SetBrush(BlrtBrush.Pointer);		};
			_line.ToolSelected					+= (object sender, EventArgs e) => { SetBrush(BlrtBrush.ShapeLine);		};
			_circle.ToolSelected				+= (object sender, EventArgs e) => { SetBrush(BlrtBrush.ShapeCircle);	};
			_rectangle.ToolSelected				+= (object sender, EventArgs e) => { SetBrush(BlrtBrush.ShapeRectangle);};

			_pen.SelectedIndexChanged			+= (object sender, EventArgs e) => { SetColor(BlrtBrush.Pen);			};
			_pointer.SelectedIndexChanged		+= (object sender, EventArgs e) => { SetColor(BlrtBrush.Pointer);		};
			_line.SelectedIndexChanged			+= (object sender, EventArgs e) => { SetColor(BlrtBrush.ShapeLine);		};
			_circle.SelectedIndexChanged		+= (object sender, EventArgs e) => { SetColor(BlrtBrush.ShapeCircle);	};
			_rectangle.SelectedIndexChanged		+= (object sender, EventArgs e) => { SetColor(BlrtBrush.ShapeRectangle);};

			_undo.Clicked += (sender, e) => {
				OnUndoClicked ();
			};
			_redo.Clicked += (sender, e) => {
				OnRedoClicked ();
			};
			_hideView.Clicked += (sender, e) => {
				OnHideClicked();
			};

			SelectedBrush = CanvasStateManager.Last.brush;

			CanvasPageView.PlatformHelper.PreferredToolbarLayoutChanged += OnPreferredToolbarLayoutChanged;

			ResetState ();
		}

		void OnPreferredToolbarLayoutChanged (object sender, BlrtCanvas.ToolbarLayoutPolicy e)
		{
			Device.BeginInvokeOnMainThread (() => {
				RepositionTools (_preHeight, CanvasPageView.PlatformHelper.PreferredToolbarLayout);
			});
		}


		protected override SizeRequest OnSizeRequest (double widthConstraint, double heightConstraint)
		{
			return new SizeRequest (new Size (widthConstraint, Math.Max (heightConstraint, _fullHeight)));
		}

		double _preHeight = -1;
		ToolbarLayoutPolicy? _preLayoutPolicy = null;
		protected override void OnSizeAllocated (double width, double height)
		{
			RepositionTools (height, CanvasPageView.PlatformHelper.PreferredToolbarLayout);
			base.OnSizeAllocated (width, height);
		}

		void RepositionTools (double height, ToolbarLayoutPolicy? layoutPolicy)
		{
			if ((height != _preHeight) || (layoutPolicy != _preLayoutPolicy)) {
				_preHeight = height;
				_preLayoutPolicy = layoutPolicy;
				var applyingPolicy = layoutPolicy.HasValue ? layoutPolicy.Value : ToolbarLayoutPolicy.Flexible;
				switch (applyingPolicy) {
					case ToolbarLayoutPolicy.AlwaysRight:
						if (height < _fullHeight) {
							AlwaysRightLayout_MinifiedTools ();
						} else {
							AlwaysRightLayout_FullTools ();
						}
						break;
					case ToolbarLayoutPolicy.Flexible:
					default:
						if (height < _fullHeight) {
							FlexibleLayout_SplitTools ();
						} else {
							FlexibleLayout_CombileTools ();
						}
						break;
				}
			}
		}

		void ApplyAbsLayout (IReadOnlyDictionary<AbsLayoutElement, LayoutParam> layoutParam)
		{
			foreach (var kvp in layoutParam)
			{
				var view = _absViewMapping [kvp.Key];
				var param = kvp.Value;
				AbsoluteLayout.SetLayoutFlags (view, param.Layout);
				AbsoluteLayout.SetLayoutBounds (view, param.Bounds);
			}
		}

		void Flip (View view, bool shouldFlip)
		{
			var flippableView = (view as ToolSelectionView);
			if (null != flippableView) {
				flippableView.IconVerticallyFlipped = shouldFlip;
			}
		}

		protected void FlexibleLayout_SplitTools (bool forced = false)
		{
			if ((ToolbarLayout.Split != _currentLayout) || forced) {
				_currentLayout = ToolbarLayout.Split;

				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = 0;

				ApplyAbsLayout (Param_Flexible_Split);

				for (int i = 0; i < _groupLeft.Length; i++) {
					_groupLeft [i].TranslationX = -_groupLeft [i].TranslationX;
					_groupLeft [i].HorizontalOptions = LayoutOptions.Start;
					_groupLeft [i].RotationY = 0;
					Flip (_groupLeft [i], false);
				}

				for (int i = 0; i < _groupRight.Length; i++) {
					_groupRight[i].TranslationX = -_groupRight[i].TranslationX;
					_groupRight[i].HorizontalOptions = LayoutOptions.End;
					_groupRight[i].RotationY = 180;
				}

				_widthSelection.Opacity = 1;
				_hideView.Current = _isHidden ? 1 : 0;
				_hideView.Opacity = 1;
				_backgroundLeft.Opacity = _isHidden ? 0 : 1;
				_backgroundRight.Opacity = _isHidden ? 0 : 1;
			}
		}

		protected void FlexibleLayout_CombileTools (bool forced = false)
		{
			if (ToolbarLayout.Left != _currentLayout || forced) {
				_currentLayout = ToolbarLayout.Left;

				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = 0;

				ApplyAbsLayout (Param_Flexible_Normal);

				for (int i = 0; i < _groupLeft.Length; i++) {
					_groupLeft [i].TranslationX = -_groupLeft [i].TranslationX;
					_groupLeft [i].HorizontalOptions = LayoutOptions.Start;
					_groupLeft [i].RotationY = 0;
					Flip (_groupLeft [i], false);
				}

				for (int j = 0; j < _groupRight.Length; j++) {
					_groupRight[j].TranslationX = -_groupRight[j].TranslationX;
					_groupRight[j].HorizontalOptions = LayoutOptions.Start;
					_groupRight[j].RotationY = 0;
				}

				_widthSelection.Opacity = 1;
				_hideView.Current = _isHidden ? 1 : 0;
				_hideView.Opacity = 1;
				_backgroundLeft.Opacity = _isHidden ? 0 : 1;
				_backgroundRight.Opacity = 0;
			}
		}

		protected void AlwaysRightLayout_FullTools (bool forced = false)
		{
			if (ToolbarLayout.Right != _currentLayout || forced) {
				_currentLayout = ToolbarLayout.Right;

				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = 0;

				ApplyAbsLayout (Param_AlwaysRight_Normal);

				for (int i = 0; i < _groupLeft.Length; i++) {
					_groupLeft [i].TranslationX = -_groupLeft [i].TranslationX;
					_groupLeft [i].HorizontalOptions = LayoutOptions.End;
					_groupLeft [i].RotationY = 180;
					Flip (_groupLeft [i], true);
				}

				for (int j = 0; j < _groupRight.Length; j++) {
					_groupRight [j].TranslationX = -_groupRight [j].TranslationX;
					_groupRight [j].HorizontalOptions = LayoutOptions.End;
					_groupRight [j].RotationY = 180;
				}

				_widthSelection.Opacity = 1;
				_hideView.Current = _isHidden ? 0 : 1;
				_hideView.Opacity = 1;
				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = _isHidden ? 0 : 1;
			}
		}

		protected void AlwaysRightLayout_MinifiedTools (bool forced = false)
		{
			if (ToolbarLayout.RightMinified != _currentLayout || forced) {
				_currentLayout = ToolbarLayout.RightMinified;

				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = 0;

				ApplyAbsLayout (Param_AlwaysRight_Minified);

				for (int i = 0; i < _groupLeft.Length; i++) {
					_groupLeft [i].TranslationX = -_groupLeft [i].TranslationX;
					_groupLeft [i].HorizontalOptions = LayoutOptions.End;
					_groupLeft [i].RotationY = 180;
					Flip (_groupLeft [i], true);
				}

				for (int j = 0; j < _groupRight.Length; j++) {
					_groupRight [j].TranslationX = -_groupRight [j].TranslationX;
					_groupRight [j].HorizontalOptions = LayoutOptions.End;
					_groupRight [j].RotationY = 180;
				}

				_widthSelection.Opacity = 0;
				_hideView.Current = _isHidden ? 0 : 1;
				_hideView.Opacity = 0;
				_backgroundLeft.Opacity = 0;
				_backgroundRight.Opacity = _isHidden ? 0 : 1;
				ShowToolBar ();
			}
		}

		public async Task EnableUndoButton (bool enabled, bool animated)
		{
			await AnimatedEnableView (enabled, animated, _undo);
		}

		public async Task EnableRedoButton (bool enabled, bool animated)
		{
			await AnimatedEnableView (enabled, animated, _redo);
		}

		static async Task AnimatedEnableView (bool enabled, bool animated, View view)
		{
			if (view.IsEnabled != enabled) {
				view.IsEnabled = enabled;
				var fromOpacity = view.Opacity;
				ViewExtensions.CancelAnimations (view);
				var toOpacity = enabled ? 1 : 0;
				if (animated) {
					view.Opacity = fromOpacity;
					await view.FadeTo (toOpacity, ToolSelectionView.AnimationDuration);
				} else {
					view.Opacity = toOpacity;
				}
			}
		}

		public void HidePopupViews (ExcludedHidingView excluded)
		{
			if (!excluded.HasFlag (ExcludedHidingView.LineWidth)) {
				_widthSelection.SetDrawerOpen (false, true);
			}
		}

		void SetColor (BlrtBrush brush)
		{
			BlrtBrushColor color = BlrtBrushColor.Red;
			switch (brush) {
				case BlrtBrush.Pen:
					color =  GetColor(_pen.SelectedIndex);
					break;
				case BlrtBrush.Pointer:
					color =  GetColor(_pointer.SelectedIndex);
					break;
				case BlrtBrush.ShapeLine:
					color =  GetColor(_line.SelectedIndex);
					break;
				case BlrtBrush.ShapeCircle:
					color =  GetColor(_circle.SelectedIndex);
					break;
				case BlrtBrush.ShapeRectangle:
					color =  GetColor(_rectangle.SelectedIndex);
					break;
			}

			CanvasStateManager.Last.SetColor(brush, color);
		}

		BlrtBrushColor GetColor (int index)
		{
			switch (index) {
				case 0:
					return BlrtBrushColor.Red;
				case 1:
					return BlrtBrushColor.Green;
				case 2:
					return BlrtBrushColor.Blue;
				case 3:
					return BlrtBrushColor.Yellow;
				case 4:
					return BlrtBrushColor.White;
				case 5:
					return BlrtBrushColor.Black;
				default:
					return BlrtBrushColor.Red;
			}
		}

		int GetColorIndex (BlrtBrushColor index)
		{
			switch (index) {
				case BlrtBrushColor.Red:
					return 0;
				case BlrtBrushColor.Green:
					return 1;
				case BlrtBrushColor.Blue:
					return 2;
				case BlrtBrushColor.Yellow:
					return 3;
				case BlrtBrushColor.White:
					return 4;
				case BlrtBrushColor.Black:
					return 5;
				default:
					return 0;
			}
		}

		void SetBrush (BlrtBrush brush)
		{
			if (SelectedBrush != brush) {
				SelectedBrush = brush;
				ResetState ();

				Changed ();
			}
		}

		void Changed () {
			if (ToolsChanged != null)
				ToolsChanged (this, EventArgs.Empty);
		}

		void ResetState () {
			CanvasStateManager.Last.brush = SelectedBrush;

			switch (SelectedBrush) {
				case BlrtBrush.Pen:
					SelectButton (_pen);
					break;
				case BlrtBrush.ShapeLine:
					SelectButton (_line);
					break;
				case BlrtBrush.ShapeCircle:
					SelectButton (_circle);
					break;
				case BlrtBrush.ShapeRectangle:
					SelectButton (_rectangle);
					break;
				default:
					SelectButton (_pointer);
					break;
			}
		}

		void SelectButton (ToolColorSelectionView button)
		{
			_selected = button;

			for (int i = 0; i < _buttonList.Length; ++i) {
				_buttonList [i].Highlighted = (button == _buttonList [i]);
			}
		}

		void OnUndoClicked ()
		{
			if (null != UndoPressed) {
				UndoPressed (this, EventArgs.Empty);
			}
		}

		void OnRedoClicked ()
		{
			if (null != RedoPressed) {
				RedoPressed (this, EventArgs.Empty);
			}
		}

		public void ShowToolBar()
		{
			if (_isHidden) {
				Device.BeginInvokeOnMainThread (() => {
					OnHideClicked ();
				});
			}
		}

		bool _isHidden = false;
		static readonly double TranslateValue = ToolSelectionView.IconWidth + ToolSelectionView.ActionArrowWidth;
		void OnHideClicked ()
		{
			_isHidden = !_isHidden;
			if (_isHidden) {
				// hide tool bar
				var leftTranslate = TranslateValue;
				var rightTranslate = TranslateValue;
				var targetHideButtonIndex = 1;
				switch (_currentLayout) {
					case ToolbarLayout.Split:
						leftTranslate = -TranslateValue;
						rightTranslate = TranslateValue;
						targetHideButtonIndex = 1;
						break;
					case ToolbarLayout.Right:
						leftTranslate = TranslateValue;
						rightTranslate = TranslateValue;
						targetHideButtonIndex = 0;
						break;
					case ToolbarLayout.RightMinified:
						leftTranslate = TranslateValue;
						rightTranslate = TranslateValue;
						targetHideButtonIndex = 0;
						break;
					case ToolbarLayout.Left:
					default:
						leftTranslate = -TranslateValue;
						rightTranslate = -TranslateValue;
						targetHideButtonIndex = 1;
						break;
				}
				foreach (var item in _groupLeft) {
					TranslateAndHide (item, leftTranslate);
				}
				foreach (var item in _groupRight) {
					TranslateAndHide (item, rightTranslate);
				}
				_hideView.Current = targetHideButtonIndex;
			} else {
				// show tool bar
				foreach (var item in _groupLeft) {
					if (((ToolbarLayout.Right == _currentLayout) || (ToolbarLayout.RightMinified == _currentLayout)) && (item == _backgroundLeft)) {
						item.Opacity = 0;
					} else {
						item.Opacity = 1;
					}
					item.TranslateTo (0, item.TranslationY, ToolSelectionView.AnimationDuration);
				}
				foreach (var item in _groupRight) {
					if ((ToolbarLayout.Left == _currentLayout) && (item == _backgroundRight)) {
						item.Opacity = 0;
					} else {
						item.Opacity = 1;
					}
					item.TranslateTo (0, item.TranslationY, ToolSelectionView.AnimationDuration);
				}
				var targetHideButtonIndex = 1;
				switch (_currentLayout) {
				case ToolbarLayout.Split:
					targetHideButtonIndex = 0;
					_widthSelection.Opacity = 1;
					break;
				case ToolbarLayout.Right:
					targetHideButtonIndex = 1;
					_widthSelection.Opacity = 1;
					break;
				case ToolbarLayout.RightMinified:
					targetHideButtonIndex = 1;
					_widthSelection.Opacity = 0;
					break;
				case ToolbarLayout.Left:
				default:
					targetHideButtonIndex = 0;
					_widthSelection.Opacity = 1;
					break;
				}
				_hideView.Current = targetHideButtonIndex;
			}
		}

		void TranslateAndHide (View item, double translationX)
		{
			item.TranslateTo (translationX, item.TranslationY, ToolSelectionView.AnimationDuration).ContinueWith ((t) => {
				if (!t.Result) {
					Device.BeginInvokeOnMainThread (() => {
						item.Opacity = 0;
					});
				}
			});
		}
	}
}

