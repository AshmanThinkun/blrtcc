﻿using System;
using Xamarin.Forms;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using System.Drawing;
using BlrtCanvas.Views.SilentViews;

namespace BlrtCanvas.Views
{
	public class TouchPanelView : BoxView
	{
		public event EventHandler<CanvasTouchCollection> TouchChanged;


		public TouchPanelView () : base()
		{
		}

		public bool OnTouchEvent (CanvasTouchCollection touchCollection)
		{
			if (TouchChanged != null)
				TouchChanged (this, touchCollection);

			return TouchChanged != null;
		}
	}
}

