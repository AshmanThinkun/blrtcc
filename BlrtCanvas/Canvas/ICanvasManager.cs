using System;
using Xamarin.Forms;

namespace BlrtCanvas
{
	public interface ICanvasManager
	{
		float CurrentTime { get; }

		bool IsPlaying		{ get; }
		bool IsRecording	{ get; }


		BlrtData.BlrtBrush SelectedBrush	{ get; }
		BlrtData.BlrtBrushWidth BrushWidth	{ get; }
		BlrtData.BlrtBrushColor BrushColor	{ get; }

		void Redraw ();
	}

}

