﻿using System;
using BlrtData;
using System.Drawing;
using BlrtCanvas.GLCanvas;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using BlrtiOS.Util;

namespace BlrtCanvas.Pages
{
	[Serializable]
	public class BlrtPDFPage : ICanvasPage, IMediaAssetContainer, IThumbnailable
	{
		MediaAsset _asset;

		[NonSerialized]
		string _localThumbnailPath;

		[NonSerialized]
		string _path;

		[NonSerialized]
		long? _mediaFileSize = null;

		int _mediaPage;

		Size _imgSize;
		bool _sizeUnknown;


		public string ObjectId			{ get { return _asset != null ? _asset.ObjectId : ""; } }
		public string DecyptedPath		{ get { 
				if(string.IsNullOrEmpty(_path)){
					_path = _asset.DecyptedPath;
				}
				return _path;
			} 
		}

		public BlrtPDFPage (MediaAsset mediaAsset, int myPageNumber, string name)
		{
			_asset = mediaAsset;
			_mediaPage = myPageNumber;
			_path = _asset.DecyptedPath;

			_sizeUnknown = true;
		}
		public BlrtPDFPage (string path, int myPageNumber, string name)
		{
			_asset = null;
			_mediaPage = myPageNumber;
			_path = path;

			_sizeUnknown = true;
		}

		public async Task<BlrtData.IO.BlrtParseFileHelper> StartUpload ()
		{
			if (null == _asset) {
				await CreateAsset ();
			}
			var uploader = _asset.Uploader;
			if (null == uploader && !_asset.OnParse) {
				uploader = _asset.TryUploadAsset ();
			}
			return uploader;
		}

		public async Task DecyptData ()
		{
			await _asset.DecyptData ();
		}

		public BlrtCanvas.GLCanvas.IBCTextureLoader	GetTextureLoader ()
		{
			return CanvasPageView.PlatformHelper.GetPDFTextureLoader (DecyptedPath, _mediaPage);
		}

		public BlrtMediaFormat Format {
			get {
				return BlrtMediaFormat.PDF;
			}
		}

		public int MediaPage {
			get {
				return _mediaPage;
			}
		}

		void LoadSize ()
		{
			if (_sizeUnknown) {
				try{
					_imgSize = CanvasPageView.PlatformHelper.GetPDFPageSize (DecyptedPath, _mediaPage);
					_sizeUnknown = false;
				} catch { }
			}
		}

		public int MediaWidth { get { LoadSize (); return _imgSize.Width;} }
		public int MediaHeight { get { LoadSize (); return _imgSize.Height;} }

		public long MediaFileSizeByte {
			get {
				if (null != _asset && _asset.FileSizeByte > 0) {
					return _asset.FileSizeByte;
				} else if (!string.IsNullOrWhiteSpace (_path)) {
					if (!_mediaFileSize.HasValue) {
						try {
							var fileInfo = new FileInfo (_path);
							var flength = fileInfo.Length;
							if (flength < 0) {
								_mediaFileSize = null;
							} else {
								_mediaFileSize = flength;
							}
						} catch {}
					}
					return _mediaFileSize.HasValue ? _mediaFileSize.Value : 0;
				}
				return 0;
			}
		}

		public TextureRotation Rotation { get { return TextureRotation.Angle_0; } }

		public bool IsDecypted {
			get {
				return BlrtUtil.CanUseFile (DecyptedPath, false);
			}
		}

		public bool Equals (ICanvasPage other)
		{
			var otherPDFPage = other as BlrtPDFPage;
			return (other.Format == this.Format) 
				&& (other.MediaPage == this.MediaPage) 
				&& (null != otherPDFPage) 
				&& (otherPDFPage.DecyptedPath.Equals(this.DecyptedPath))
				&& (otherPDFPage.ObjectId.Equals(this.ObjectId));
		}

		public override int GetHashCode ()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 29 + Format.GetHashCode();
				hash = hash * 29 + MediaPage.GetHashCode();
				hash = hash * 29 + ObjectId.GetHashCode();
				return hash;
			}
		}

		public bool SameMedia (ICanvasPage other)
		{
			if (this == other)
				return true;


			var otherPage = other as BlrtPDFPage;

			return otherPage != null
				&& otherPage.Format == Format
				&& otherPage.DecyptedPath == DecyptedPath;
		}

		public async Task<string> LocalThumbnailPath ()
		{
			if (_asset == null) // asset would be null for newly created pages. In that case we need to create thumbnail.
			{
				var thumbnailSize = BlrtUtil.GetStandardThumbnailSize ();
				_localThumbnailPath = await GenerateThumbnail (new Size (thumbnailSize, thumbnailSize));
				return _localThumbnailPath;
			}
			return _asset.LocalThumbnailPath;
		}

		public Uri CloudThumbnailPath {
			get {
				return _asset.CloudThumbnailPath;
			}
		}

		#region IMediaAssetContainer implementation
		public async Task CreateAsset ()
		{
			var media = await BlrtUtil.CreateAsset (_path, BlrtMediaFormat.PDF);
			media.Name = "media";
			media.PageArgs = BlrtUtil.GetRangeArray (1, CanvasPageView.PlatformHelper.PDFPageCount (DecyptedPath));

			string src = media.DecyptedPath;

			await BlrtEncryptorManager.Encrypt (
				src,
				media.LocalMediaPath,
				media
			);

			_asset = media;
			_path				= src;

			_sizeUnknown = true;

			await  CreateAssetThumbnail ();
			media.LocalStorage.DataManager.AddUnsynced (media);
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		async Task CreateAssetThumbnail ()
		{
			_asset.LocalThumbnailName = string.Format ("{0}-thumb.jpg", _asset.ObjectId);

			if (File.Exists (_localThumbnailPath)) {
				// just copy the same file
				using (var stream = CanvasPageView.PlatformHelper.GetAssetFileStream (_localThumbnailPath)) {
					await BlrtUtil.StreamToFileAsync (_asset.LocalThumbnailPath, stream);
				}

				try {
					File.Delete (_localThumbnailPath);
					_localThumbnailPath = string.Empty;
				} catch { }

			} else {
				var thumbnailSize = BlrtUtil.GetStandardThumbnailSize ();
				await BlrtUtilities.CreatePDFThumbnail (_path,
													1,
													_asset.LocalThumbnailPath, // TODO: need to consider pages wth pdf as well.
													new Size (thumbnailSize, thumbnailSize));
			}
		}

		public bool SameMedia (IMediaAssetContainer asset)
		{
			if (asset is BlrtPDFPage) {
				var image = asset as BlrtPDFPage;
				return image.DecyptedPath == DecyptedPath;
			}

			return false;
		}

		public void SetMedia (IMediaAssetContainer asset)
		{
			if (asset is BlrtPDFPage) {
				var image = asset as BlrtPDFPage;
				_asset				= image._asset;
				_path				= _asset.DecyptedPath;

				_sizeUnknown = true;
				return;
			}
		}

		public bool HasMediaAsset {
			get { return !string.IsNullOrEmpty (Asset); }
		}

		public string Asset {
			get { return ObjectId; }
		}
		#endregion
		#region IThumbnailable implementation


		public async Task<string> GenerateThumbnail (System.Drawing.Size size)
		{
			var path = BlrtConstants.Directories.Default.GetDecryptedPath (string.Format("{0}-{1}-{2}x{3}.jpeg", BlrtUtil.GetFileSafeString(Path.GetFileName (DecyptedPath)), MediaPage, size.Width, size.Height));

			if (!BlrtUtil.CanUseFile (path))
				await GenerateThumbnail (path, size);

			return path;
		}


		public async Task GenerateThumbnail (string path, System.Drawing.Size thumbnailSize)
		{
			if (!IsDecypted)
				await DecyptData ();
			await CanvasPageView.PlatformHelper.CreatePDFThumbnail (DecyptedPath, MediaPage, path, thumbnailSize);
		}

		#endregion
	}
}

