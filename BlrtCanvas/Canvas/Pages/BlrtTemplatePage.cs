﻿using System;
using System.Collections.Generic;
using BlrtData.Media;
using System.Drawing;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using BlrtData;

namespace BlrtCanvas.Pages
{
	[Serializable]
	public class BlrtTemplatePage : ICanvasPage
	{
		readonly int _templateId;

		public string ObjectId { get { return BlrtTemplate.AsTempleteId(_templateId); } }

		public BlrtTemplatePage (int templateId)
		{
			_templateId = templateId;

			_sizeUnknown = true;
		}

		public async Task<BlrtData.IO.BlrtParseFileHelper> StartUpload ()
		{
			return null;
		}

		public Task DecyptData ()
		{
			return Task.Delay(0);
		}

		public IBCTextureLoader	GetTextureLoader ()
		{
			return CanvasPageView.PlatformHelper.GetImageAssetTextureLoader (BlrtTemplate.GetImagePath(_templateId));
		}

		public void FreeMemory ()
		{

		}

		public BlrtMediaFormat Format {
			get {
				return BlrtMediaFormat.Template;
			}
		}

		public int MediaPage { get { return 0; } }

		void LoadSize () {
			if (_sizeUnknown) {
				try{
					_imgSize = CanvasPageView.PlatformHelper.GetImageAssetSize (BlrtTemplate.GetImagePath(_templateId));
					_sizeUnknown = false;
				} catch { }
			}
		}

		Size _imgSize;
		bool _sizeUnknown;

		public int MediaWidth { get { LoadSize (); return _imgSize.Width;;} }
		public int MediaHeight { get { LoadSize (); return _imgSize.Height;} }
		public TextureRotation Rotation { get { return TextureRotation.Angle_0; } }

		public long MediaFileSizeByte {
			get {
				return 0;
			}
		}

		public bool IsDecypted {
			get {
				return true;
			}
		}

		public bool Equals (ICanvasPage other)
		{
			var otherTemplatePage = other as BlrtTemplatePage;
			return (other.Format == this.Format)
				&& (other.MediaPage == this.MediaPage)
				&& (null != otherTemplatePage)
				&& (otherTemplatePage._templateId == this._templateId);
		}



		public bool SameMedia (ICanvasPage other)
		{
			if (this == other)
				return true;


			var otherPage = other as BlrtTemplatePage;

			return otherPage != null
				&& otherPage.Format == Format
				&& otherPage._templateId == _templateId;
		}


		public override int GetHashCode ()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 29 + Format.GetHashCode();
				hash = hash * 29 + MediaPage.GetHashCode();
				hash = hash * 29 + _templateId;
				return hash;
			}
		}

		public async Task<string> LocalThumbnailPath ()
		{
			var thumbnailSize = BlrtUtil.GetStandardThumbnailSize ();
			return await GenerateThumbnail (new Size (thumbnailSize, thumbnailSize));
		}

		public Uri CloudThumbnailPath {
			get {
				return null; // thumbnail for template is not suppose to be on cloud
			}
		}
		#region IThumbnailable implementation

		public async Task<string> GenerateThumbnail (Size size)
		{
			var path = BlrtConstants.Directories.Default.GetDecryptedPath (string.Format("{0}-{1}-{2}x{3}.jpeg", BlrtUtil.GetFileSafeString(ObjectId), MediaPage, size.Width, size.Height));

			if (!BlrtUtil.CanUseFile (path))
				await GenerateThumbnail (path, size);

			return path;
		}


		public async Task GenerateThumbnail (string path, System.Drawing.Size thumbnailSize)
		{
			if (!IsDecypted)
				await DecyptData ();

			using (var stream = CanvasPageView.PlatformHelper.GetAssetFileStream (BlrtTemplate.GetThumbnailPath(_templateId))) {
				await BlrtUtil.StreamToFileAsync (path, stream);
			}
			
//			await BCPlatformManager.CreateImgFileThumbnail (BlrtTemplate.GetImagePath(_templateId), path, thumbnailSize);
		}

		#endregion

		public string TemplateName { get { return BlrtTemplate.GetTemplateName (_templateId); } }
		public string TemplateImagePath { get { return BlrtTemplate.GetImagePath (_templateId); } }
	}
}

