﻿using System;
using BlrtData;
using System.Drawing;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using System.IO;
using BlrtiOS.Util;
using System.Runtime.CompilerServices;

namespace BlrtCanvas.Pages
{
	[Serializable]
	public class BlrtImagePage : ICanvasPage, IMediaAssetContainer, IThumbnailable
	{
		MediaAsset _asset;

		[NonSerialized]
		string _localThumbnailPath;

		[NonSerialized]
		string _path;

		[NonSerialized]
		long? _mediaFileSize = null;

		Size _imgSize;
		bool _sizeUnknown;

		TextureRotation _imgRotation;
		bool _rotationUnknown;


		public string ObjectId			{ get { return _asset != null ? _asset.ObjectId : ""; } }
		public string DecyptedPath		{ get { 
				if(string.IsNullOrEmpty(_path)){
					_path = _asset.DecyptedPath;
				}
				return _path;
			} 
		}

		public BlrtImagePage (MediaAsset mediaAsset)
		{
			_asset				= mediaAsset;
			_path				= _asset.DecyptedPath;
			_sizeUnknown		= true;
			_rotationUnknown	= true;
		}

		public BlrtImagePage (string path)
		{
			_asset = null;
			_path = path;
			_sizeUnknown = true;
			_rotationUnknown = true;
		}

		public async Task<BlrtData.IO.BlrtParseFileHelper> StartUpload ()
		{
			if (null == _asset) {
				await CreateAsset ();
			}
			var uploader = _asset.Uploader;
			if (null == uploader && !_asset.OnParse) {
				uploader = _asset.TryUploadAsset ();
			}
			return uploader;
		}

		public async Task DecyptData ()
		{
			await _asset.DecyptData ();
		}

		public IBCTextureLoader	GetTextureLoader ()
		{
			return CanvasPageView.PlatformHelper.GetImageTextureLoader (DecyptedPath);
		}

		public BlrtMediaFormat Format {
			get { return BlrtMediaFormat.Image; }
		}

		public int MediaPage {
			get { return 0; }
		}

		void LoadSize ()
		{
			if (_sizeUnknown) {
				try{
					_imgSize = CanvasPageView.PlatformHelper.GetImageSizeBeforeRotation (DecyptedPath);
					switch (Rotation) {
						case TextureRotation.Angle_90:
						case TextureRotation.Angle_90_Flipped_Vertically:
						case TextureRotation.Angle_270:
						case TextureRotation.Angle_270_Flipped_Vertically:
							var temp = _imgSize.Width;
							_imgSize.Width = _imgSize.Height;
							_imgSize.Height = temp;
							break;
					}
					_sizeUnknown = false;
				} catch { }
			}
		}

		void LoadRotation ()
		{
			if (_rotationUnknown) {
				try {
					_imgRotation = CanvasPageView.PlatformHelper.GetImageRotation (DecyptedPath);
					_rotationUnknown = false;
				} catch {}
			}
		}

		public bool SameMedia (ICanvasPage other)
		{
			if (this == other)
				return true;


			var otherPage = other as BlrtImagePage;

			return otherPage != null
				&& otherPage.Format == Format
				&& otherPage.DecyptedPath == DecyptedPath;
		}

		public int MediaWidth { get { LoadSize (); return _imgSize.Width;} }
		public int MediaHeight { get { LoadSize (); return _imgSize.Height;} }

		public TextureRotation Rotation { get { LoadRotation (); return _imgRotation; } }

		public bool IsDecypted {
			get {
				return BlrtUtil.CanUseFile (DecyptedPath, false);
			}
		}

		public bool Equals (ICanvasPage other)
		{
			var otherImgPage = other as BlrtImagePage;
			return (other.Format == this.Format) 
				&& (other.MediaPage == this.MediaPage) 
				&& (null != otherImgPage) 
				&& (otherImgPage.DecyptedPath == this.DecyptedPath) 
				&& (otherImgPage.ObjectId.Equals(this.ObjectId));
		}

		public override int GetHashCode ()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 29 + Format.GetHashCode();
				hash = hash * 29 + MediaPage.GetHashCode();
				hash = hash * 29 + ObjectId.GetHashCode();
				return hash;
			}
		}

		public async Task<string> LocalThumbnailPath()
		{
			if (_asset == null) // asset would be null for newly created pages. In that case we need to create thumbnail.
			{
				var thumbnailSize = BlrtUtil.GetStandardThumbnailSize ();
				_localThumbnailPath = await GenerateThumbnail (new Size (thumbnailSize, thumbnailSize));
				return _localThumbnailPath;
			}
			return _asset.LocalThumbnailPath;
		}

		public Uri CloudThumbnailPath {
			get {
				return _asset.CloudThumbnailPath;
			}
		}

		#region IMediaAssetContainer implementation
		public async Task CreateAsset ()
		{
			if (_asset != null) {
				// asset is not required to be created as it has already been assigned
				return;
			}
			var media = await BlrtUtil.CreateAsset (_path, BlrtMediaFormat.Image);
			media.Name = "media";
			media.PageArgs = new int [] { 1 };

			string src = media.DecyptedPath;

			await BlrtEncryptorManager.Encrypt (
				src,
				media.LocalMediaPath,
				media
			);

			_asset = media;
			_path				= src;

			_sizeUnknown = true;
			_rotationUnknown = true;

			await CreateAssetThumbnail ();

			media.LocalStorage.DataManager.AddUnsynced (media);
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		async Task CreateAssetThumbnail ()
		{
			_asset.LocalThumbnailName = string.Format ("{0}-thumb.jpg", _asset.ObjectId);

			if (File.Exists (_localThumbnailPath))
			{
				// just copy the same file
				using (var stream = CanvasPageView.PlatformHelper.GetAssetFileStream (_localThumbnailPath)) {
					await BlrtUtil.StreamToFileAsync (_asset.LocalThumbnailPath, stream);
				}

				try {
					File.Delete (_localThumbnailPath);
					_localThumbnailPath = string.Empty;
				} catch {}

			} else {
				var thumbnailSize = BlrtUtil.GetStandardThumbnailSize ();
				await BlrtUtilities.CreateImgFileThumbnail (_path,
														_asset.LocalThumbnailPath,
														new Size (thumbnailSize, thumbnailSize));
			}
		}

		public bool SameMedia (IMediaAssetContainer asset)
		{
			if (asset is BlrtImagePage) {
				var image = asset as BlrtImagePage;
				return image.DecyptedPath == DecyptedPath;
			}

			return false;
		}

		public void SetMedia (IMediaAssetContainer asset)
		{
			if (asset is BlrtImagePage) {
				var image = asset as BlrtImagePage;
				_asset				= image._asset;
				_path				= _asset.DecyptedPath;

				_sizeUnknown = true;
				_rotationUnknown = true;
				return;
			}
		}

		public bool HasMediaAsset {
			get { return !string.IsNullOrEmpty (Asset); }
		}

		public string Asset {
			get { return ObjectId; }
		}

		public long MediaFileSizeByte {
			get {
				if (null != _asset && _asset.FileSizeByte > 0) {
					return _asset.FileSizeByte;
				} else if (!string.IsNullOrWhiteSpace (_path)) {
					if (!_mediaFileSize.HasValue) {
						try {
							var fileInfo = new FileInfo (_path);
							var flength = fileInfo.Length;
							if (flength < 0) {
								_mediaFileSize = null;
							} else {
								_mediaFileSize = flength;
							}
						} catch { }
					}
					return _mediaFileSize.HasValue ? _mediaFileSize.Value : 0;
				}
				return 0;
			}
		}

		#endregion
		#region IThumbnailable implementation


		public async Task<string> GenerateThumbnail (Size size)
		{
			var path = BlrtConstants.Directories.Default.GetDecryptedPath (string.Format("{0}-{1}-{2}x{3}.jpeg", BlrtUtil.GetFileSafeString(Path.GetFileName (DecyptedPath)), MediaPage, size.Width, size.Height));

			if (!BlrtUtil.CanUseFile (path))
				await GenerateThumbnail (path, size);

			return path;
		}


		public async Task GenerateThumbnail (string path, Size thumbnailSize)
		{
			if (!IsDecypted)
				await DecyptData ();
			await CanvasPageView.PlatformHelper.CreateImgFileThumbnail (DecyptedPath, path, thumbnailSize);
		}

		#endregion
	}
}

