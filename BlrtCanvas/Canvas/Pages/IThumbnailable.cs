﻿using System;
using System.Threading.Tasks;

namespace BlrtCanvas.Pages
{
	public interface IThumbnailable
	{
		System.Threading.Tasks.Task<string> GenerateThumbnail (System.Drawing.Size thumbnailSize);
		System.Threading.Tasks.Task GenerateThumbnail (string path, System.Drawing.Size thumbnailSize);

		Task<string> LocalThumbnailPath ();
		Uri CloudThumbnailPath { get; }
	}
}

