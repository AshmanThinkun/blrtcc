﻿using System;

namespace BlrtCanvas
{



	public class CanvasResult : BlrtData.ExecutionPipeline.ICanvasResult {

		string _conversationId;
		string _contentId;
		bool _creation;
		bool _showMediaPicker;

		public string CreatedConversationId {
			get { return _conversationId; }
		}

		public string CreatedContentId {
			get { return _contentId; }
		}

		public bool CreatedContent {
			get { return _creation; }
		}

		public bool ShowMediaPicker {
			get { return _showMediaPicker; }
		}


		public static CanvasResult ConversationCreated(string conversationId, string contentId){
			return new CanvasResult () {
				_creation = true,
				_conversationId = conversationId,
				_contentId = contentId,
			};
		}

		public static CanvasResult BlrtCreated(IBlrtCanvasData data, string contentId){
			return new CanvasResult () {
				_creation = true,
				_contentId = contentId,
			};
		}

		public static CanvasResult BackPressed(bool showMediaPicker){
			return new CanvasResult () {
				_creation = false,
				_showMediaPicker = showMediaPicker
			};
		}
	}
}

