using System;
using System.Linq;
using System.Threading.Tasks;
using BlrtData;
using BlrtData.Contents;
using BlrtData.Media;
using BlrtCanvas.Pages;
using BlrtData.ExecutionPipeline;

namespace BlrtCanvas
{
	public interface IBlrtCanvasData
	{

		IBlrtCanvasData Parent { get; }

		IRequestArgs RequestArgs { get; }
		bool IsIndependent { get; }
		bool IsDraft { get; }
		bool IsReply { get; }

		string Conversation { get; }
		string Blrt { get; }

		string DecyptedTouchDataPath { get; }
		string DecyptedAudioPath { get; }

		int PageCount { get; }
		int HiddenPageCount { get; }

		string ConversationName { get; set; }

		bool CreateWithPublicBlrt { get; set; }
		string PublicBlrtName { get; set; }

		IBlrtCanvasData CreateDraft ();
		IBlrtCanvasData CreateDraft (BlrtMediaPagePrototype result, IRequestArgs requestArgs = null);


		ICanvasPage GetPage (int page);
		ICanvasPage GetHiddenPage (int page);

		string [] GetMediaAssets ();

		BlrtMediaPagePrototype ToPageIndex ();

		bool IsDecypted ();
		Task DecyptFiles ();

		bool TryMarkRead ();

	}


	public interface ICanvasPage : IEquatable<ICanvasPage>, IThumbnailable
	{

		string ObjectId { get; }

		BlrtMediaFormat Format { get; }
		int MediaPage { get; }

		int MediaWidth { get; }
		int MediaHeight { get; }

		long MediaFileSizeByte { get; }

		bool IsDecypted { get; }

		Task DecyptData ();

		GLCanvas.IBCTextureLoader GetTextureLoader ();
		GLCanvas.TextureRotation Rotation { get; }

		bool SameMedia (ICanvasPage page);

		Task<BlrtData.IO.BlrtParseFileHelper> StartUpload ();
	}



	[Serializable]
	public class BlrtCanvasData : IBlrtCanvasData
	{
		ConversationBlrtItem _item;

		ICanvasPage [] _pages;
		ICanvasPage [] _hidden;

		BlrtMediaPagePrototype _pageIndex;

		[NonSerialized]
		IRequestArgs _requestArgs;

		string _convoName;
		string _conversationId;
		bool _isIndependent = false;

		public IBlrtCanvasData Parent { get; private set; }

		public IRequestArgs RequestArgs { get { return _requestArgs ?? (Parent != null ? Parent.RequestArgs : null); } set { _requestArgs = value; } }

		public bool IsIndependent { get { return _isIndependent; } }

		public bool IsDraft { get { return _item == null; } }
		public bool IsReply { get { return string.IsNullOrEmpty (Conversation); } }

		public string Conversation { get { return _conversationId ?? (_item != null ? _item.ConversationId : (Parent != null ? Parent.Conversation : null)); } }

		public string DecyptedTouchDataPath { get { return _item != null ? _item.DecryptedTouchDataPath : ""; } }
		public string DecyptedAudioPath { get { return _item != null ? _item.DecryptedAudioPath : ""; } }

		public int PageCount { get { return _pages.Length; } }
		public int HiddenPageCount { get { return _hidden.Length; } }

		public string ConversationName {
			get { return _convoName ?? (Parent != null ? Parent.ConversationName : ""); }
			set { if (IsDraft && !IsReply) _convoName = value; }
		}

		public bool CreateWithPublicBlrt { get; set; }
		public string PublicBlrtName { get; set; }

		public string Blrt => _item?.ObjectId ?? string.Empty;

		private BlrtCanvasData (ConversationBlrtItem item)
		{
			_item = item;
			_isIndependent = item == null ? false : item.IsIndependent;
			var convo = item.LocalStorage.DataManager.GetConversation (item?.ConversationId);
			_convoName = (null == convo) ? "" : convo.Name;

			_pageIndex = _item.MediaData;

			_pages = new ICanvasPage [_pageIndex.TotalPages];
			_hidden = new ICanvasPage [_pageIndex.TotalHiddenPages];

			try {
				for (var i = 0; i < _pageIndex.TotalPages; ++i) {
					var p = _pageIndex.UncompressedItems [i];
					_pages [i] = GetPage (p.objectId, p.mediaPage);
				}

				for (var i = 0; i < _pageIndex.TotalHiddenPages; ++i) {
					var p = _pageIndex.UncompressedHiddenItems [i];
					_hidden [i] = GetPage (p.objectId, p.mediaPage);
				}
			} catch (Exception ex) {
				LLogger.WriteEvent ("BlrtCanvasData", "Contructor", "Exception", ex.ToString ());
			}
		}

		private BlrtCanvasData (Conversation conversation)
		{

			_convoName = conversation.Name;

			_conversationId = conversation.ObjectId;
			_pageIndex = conversation.LastMediaPrototype;

			_pages = new ICanvasPage [_pageIndex.TotalPages];
			_hidden = new ICanvasPage [_pageIndex.TotalHiddenPages];

			for (var i = 0; i < _pageIndex.TotalPages; ++i) {
				var p = _pageIndex.UncompressedItems [i];
				_pages [i] = GetPage (p.objectId, p.mediaPage);
			}

			for (var i = 0; i < _pageIndex.TotalHiddenPages; ++i) {
				var p = _pageIndex.UncompressedHiddenItems [i];
				_hidden [i] = GetPage (p.objectId, p.mediaPage);
			}
		}

		private BlrtCanvasData (IRequestArgs args) : this (new BlrtMediaPagePrototype (), args)
		{

		}

		private BlrtCanvasData (BlrtMediaPagePrototype pageIndex, IRequestArgs requestArgs = null)
		{
			_item = null;
			Parent = null;

			_pageIndex = pageIndex;
			_pages = new ICanvasPage [_pageIndex.TotalPages];
			_hidden = new ICanvasPage [_pageIndex.TotalHiddenPages];

			for (var i = 0; i < _pageIndex.TotalPages; ++i) {
				var p = _pageIndex.UncompressedItems [i];
				_pages [i] = GetPage (p.objectId, p.mediaPage);
			}

			for (var i = 0; i < _pageIndex.TotalHiddenPages; ++i) {
				var p = _pageIndex.UncompressedHiddenItems [i];
				_hidden [i] = GetPage (p.objectId, p.mediaPage);
			}

			_requestArgs = requestArgs;

			if (_requestArgs != null) {
				this.ConversationName = _requestArgs.BlrtName;
				this.CreateWithPublicBlrt = _requestArgs.MakeBlrtPublic;
				if (this.CreateWithPublicBlrt) {
					this.PublicBlrtName = this.ConversationName;
				}
			}
		}

		private ICanvasPage GetPage (string objectId, int mediaPage)
		{

			var page = _pages.Concat (_hidden).FirstOrDefault (p => p != null && p.ObjectId == objectId && p.MediaPage == mediaPage);
			if (page == null) {
				ILocalStorageParameter param = null;
				if (null == _item) {
					param = LocalStorageParameters.Default;
				} else {
					param = _item.LocalStorage;
				}
				return CanvasPageView.PlatformHelper.OpenPage (objectId, mediaPage, param);
			}
			return page;

		}


		private BlrtCanvasData (BlrtCanvasData parent, BlrtData.Media.BlrtMediaPagePrototype pageIndex, IRequestArgs requestArgs = null)
			: this (pageIndex, requestArgs)
		{

			Parent = parent;
		}

		public IBlrtCanvasData CreateDraft ()
		{
			return new BlrtCanvasData (this, this._pageIndex, this.RequestArgs);
		}
		public IBlrtCanvasData CreateDraft (BlrtData.Media.BlrtMediaPagePrototype result, IRequestArgs requestArgs = null)
		{
			return new BlrtCanvasData (this, result, requestArgs);
		}


		public ICanvasPage GetPage (int page)
		{
			return _pages [page - 1];
		}
		public ICanvasPage GetHiddenPage (int page)
		{
			return _hidden [page - 1];
		}

		public string [] GetMediaAssets ()
		{
			return (from m in _pages
					where !(string.IsNullOrWhiteSpace (m.ObjectId) && BlrtData.Media.BlrtTemplate.IsTempleteId (m.ObjectId))
					select m.ObjectId)
					.Distinct ().ToArray ();
		}

		public BlrtMediaPagePrototype ToPageIndex ()
		{
			return new BlrtMediaPagePrototype (_pageIndex);
		}

		public bool IsDecypted ()
		{

			foreach (var page in _pages) {
				if (null != page && !page.IsDecypted)
					return false;
			}

			return _item == null || (BlrtUtil.CanUseFile (DecyptedAudioPath) && BlrtUtil.CanUseFile (DecyptedTouchDataPath));
		}

		public async Task DecyptFiles ()
		{
			foreach (var page in _pages) {
				if (null != page && !page.IsDecypted)
					await page.DecyptData ();
			}

			if (!IsDecypted ()) {
				if (!BlrtUtil.CanUseFile (DecyptedAudioPath, false)) {
					await BlrtData.BlrtEncryptorManager.Decrypt (
						_item.LocalAudioPath,
						DecyptedAudioPath,
						_item
					);
				}

				if (!BlrtUtil.CanUseFile (DecyptedTouchDataPath, false)) {
					await BlrtData.BlrtEncryptorManager.Decrypt (
						_item.LocalTouchDataPath,
						DecyptedTouchDataPath,
						_item
					);
				}
			}
		}

		public bool TryMarkRead ()
		{
			if (_item != null) {
				return _item.TryMarkRead ();
			}
			return false;
		}

		public static BlrtCanvasData FromConversationItem (BlrtData.Contents.ConversationBlrtItem item)
		{
			return new BlrtCanvasData (item);
		}

		public static BlrtCanvasData FromConversation (BlrtData.Conversation conversation)
		{
			try {
				return new BlrtCanvasData (
					conversation.LocalStorage.DataManager.GetConversationItems (conversation)
						.OrderBy (item => item)
						.LastOrDefault (item => item is BlrtData.Contents.ConversationBlrtItem)
					as BlrtData.Contents.ConversationBlrtItem
				);
			} catch {
				return new BlrtCanvasData (
					conversation
				);
			}
		}

		public static BlrtCanvasData FromDraft (string name, BlrtData.Media.BlrtMediaPagePrototype result, IRequestArgs requestArgs = null)
		{
			return new BlrtCanvasData (result, requestArgs) {
				ConversationName = name,
			};
		}

		public static BlrtCanvasData FromRequest (IRequestArgs args)
		{
			return new BlrtCanvasData (args);
		}

		internal void SetRequestArgs (IRequestArgs args)
		{
			_requestArgs = args;
		}
	}




	public class IndependentBlrtData : IBlrtCanvasData
	{
		#region IBlrtCanvasData implementation

		public bool CreateWithPublicBlrt {
			get;
			set;
		}

		public string PublicBlrtName {
			get;
			set;
		}

		public IBlrtCanvasData CreateDraft ()
		{
			throw new InvalidOperationException ("cannot create reblrt based on independent blrt");
		}

		public IBlrtCanvasData CreateDraft (BlrtMediaPagePrototype result, IRequestArgs requestArgs = null)
		{
			throw new InvalidOperationException ("cannot create reblrt based on independent blrt");
		}

		public ICanvasPage GetPage (int page)
		{
			return _pages [page - 1];
		}

		public ICanvasPage GetHiddenPage (int page)
		{
			throw new InvalidOperationException ("no hidden page in independent blrt");
		}

		public string [] GetMediaAssets ()
		{
			throw new InvalidOperationException ("cannot create reblrt based on independent blrt");
		}

		public BlrtMediaPagePrototype ToPageIndex ()
		{
			throw new InvalidOperationException ("cannot create reblrt based on independent blrt");
		}

		public bool IsDecypted ()
		{
			return true;
		}

		public async Task DecyptFiles ()
		{
			// do nothing
			return;
		}

		public bool TryMarkRead ()
		{
			return true;
		}

		public IBlrtCanvasData Parent { get { return null; } }

		public IRequestArgs RequestArgs { get { return null; } }

		public bool IsIndependent { get { return true; } }

		public bool IsDraft { get { return false; } }

		public bool IsReply { get { return false; } }

		public string Conversation {
			get {
				throw new InvalidOperationException ("no conversation for independent blrt");
			}
		}

		public string DecyptedTouchDataPath { get { return _args.TouchPath; } }

		public string DecyptedAudioPath { get { return _args.AudioPath; } }

		public int PageCount { get { return _args.Pages.Length; } }

		public int HiddenPageCount { get { return 0; } }

		public string ConversationName {
			get {
				throw new InvalidOperationException ("no conversation for independent blrt");
			}
			set {
				throw new InvalidOperationException ("no conversation for independent blrt");
			}
		}

		public string Blrt => throw new NotImplementedException ();

		#endregion

		ILocalBlrtArgs _args;
		ICanvasPage [] _pages;
		public IndependentBlrtData (ILocalBlrtArgs args)
		{
			_args = args;
			_pages = new ICanvasPage [_args.Pages.Length];
			for (int i = 0; i < _args.Pages.Length; i++) {
				var pageInfo = _args.Pages [i];
				var media = _args.Media [pageInfo.MediaIndex];
				switch (media.Format) {
				case BlrtMediaFormat.Image:
					_pages [i] = new BlrtImagePage (media.Location);
					break;
				case BlrtMediaFormat.PDF:
					_pages [i] = new BlrtPDFPage (media.Location, pageInfo.PageNumInMedia, "");
					break;
				case BlrtMediaFormat.Template:
					_pages [i] = new BlrtTemplatePage (Int32.Parse (media.Location));
					break;
				default:
					break;
				}
			}
		}
	}
}

