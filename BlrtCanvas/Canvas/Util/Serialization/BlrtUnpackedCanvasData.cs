﻿using System;

namespace BlrtCanvas.Util.Serialization
{
	public class BlrtUnpackedCanvasData
	{
		public int version;

		public CameraPositioning cameraData;
		public BlrtElementData[] touchData;
		public MediaData[] mediaData;


		public RatioHandling ratioHandling;
		public bool invertedCameraPosition;
		public bool invertedYAxisPosition;


		//size of each page
		public class MediaData
		{
			public int mediaHeight, mediaWidth;
			public float offsetMaxX, offsetMinX, offsetMaxY, offsetMinY;
		}

		//the camera positioning during the blrt
		public class CameraPositioning
		{
			public CameraPositioningPoint[] points;
		}


		public class CameraPositioningPoint
		{
			public float x, y, zoom, timestamp, ratio;
			public int page;
		}


		public class BlrtElementData
		{
			public int page;
			public int brushType;
			public int color;
			public int thickness;

			public float[] removedToggle;

			public BlrtElementDataPoint[] points;
		}

		public class BlrtElementDataPoint
		{
			public float x, y, scale, timestamp;
		}
	}




	public enum RatioHandling
	{

		AutoScaling,

		CameraRatio,

		MediaPageOffset
	}
}

