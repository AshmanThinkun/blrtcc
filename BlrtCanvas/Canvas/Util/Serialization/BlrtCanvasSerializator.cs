﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BlrtCanvas.Util.Serialization
{
	public static class BlrtCanvasSerializator
	{
		public static BlrtUnpackedCanvasData Unpack(string json){
		
			var jsonObj = JObject.Parse(json);
		
			var version = jsonObj ["v"];
		
			int verisonInt = 0;

			if (version != null) {
				verisonInt = version.Value<int> ();
			}

			BlrtUnpackedCanvasData unpackedData = null;
		
			switch (verisonInt) {
				case 0: 
				case 1:
					{
						var seril = JsonConvert.DeserializeObject<BlrtTouchSerializableV1> (json);
						unpackedData = BlrtTouchSerializableV1.UnpackData (seril);
					}
					break;
				case 2:
					{
						var seril = JsonConvert.DeserializeObject<BlrtTouchSerializableV2> (json);
						unpackedData = BlrtTouchSerializableV2.UnpackData (seril);
					}
					break;
				default:
					throw new Exception ("Unknown Serialization version id!");
			}

			return unpackedData;
		}

		public static string Pack (BlrtUnpackedCanvasData unpacked)
		{
			var data = BlrtTouchSerializableV2.PackData (unpacked);
			return JsonConvert.SerializeObject (data, new JsonSerializerSettings(){
				NullValueHandling = NullValueHandling.Ignore,
			});
		}
	}
}

