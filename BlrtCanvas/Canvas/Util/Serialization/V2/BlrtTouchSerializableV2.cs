using System;
using System.Collections.Generic;
using System.Drawing;

using Newtonsoft.Json;

namespace BlrtCanvas.Util.Serialization
{
	public class BlrtTouchSerializableV2
	{
		public const int SerializableVersion = 2;

		public int v;

		public CameraData[] camera{ get; set;}
		public TouchData[] actions{ get; set;}
		public MediaData[] media{ get; set;}

		public BlrtTouchSerializableV2 ()	{ }


		public static BlrtTouchSerializableV2 PackData(BlrtUnpackedCanvasData unpackedData)
		{
			var s = new BlrtTouchSerializableV2 ();

			s.v 		= SerializableVersion;


			s.camera = new CameraData[unpackedData.cameraData.points.Length];
			for (int i = 0; i < unpackedData.cameraData.points.Length; ++i) {

				var v1 = i == 0 ? null : unpackedData.cameraData.points [i-1];
				var v2 = unpackedData.cameraData.points [i];

				var c = new CameraData () {
					page 		= (v1 == null || v1.page != v2.page)		 	? v2.page as int? : null,
					x 			= (v1 == null || v1.x != v2.x) 					? v2.x as float? : null,
					y 			= (v1 == null || v1.y != v2.y) 					? v2.y as float? : null,
					zoom 		= (v1 == null || v1.zoom != v2.zoom) 			? v2.zoom as float? : null,
					ratio 		= (v1 == null || v1.ratio != v2.ratio) 			? v2.ratio as float? : null,
					timestamp 	= (v1 == null || v1.timestamp != v2.timestamp) 	? v2.timestamp as float? : null,
				};
				s.camera [i] = c;
			}

			s.actions = new TouchData[unpackedData.touchData.Length];
			for (int i = 0; i < unpackedData.touchData.Length; ++i) {

				var touch = unpackedData.touchData [i];

				var t = new TouchData () {
					brush 			= touch.brushType,
					page 			= touch.page,
					color 			= touch.color,
					removeToggle 	= touch.removedToggle,
					scale			= touch.points[0].scale,
					width			= touch.thickness,
					points			= new BlrtTouchSavePoint[touch.points.Length]
				};



				for (int j = 0; j < touch.points.Length; ++j) {
					t.points [j] = new BlrtTouchSavePoint () {
						x = touch.points[j].x,
						y = touch.points[j].y,
						t = touch.points[j].timestamp
					};
				}

				s.actions [i] = t;
			}

			s.media = new MediaData[unpackedData.mediaData.Length];
			for (int i = 0; i < unpackedData.mediaData.Length; ++i) {

				s.media [i] = new MediaData(){
					width = unpackedData.mediaData[i].mediaWidth,
					height = unpackedData.mediaData[i].mediaHeight,

					offsetMaxX	= unpackedData.mediaData[i].offsetMaxX,
					offsetMaxY	= unpackedData.mediaData[i].offsetMaxY,
					offsetMinX	= unpackedData.mediaData[i].offsetMinX,
					offsetMinY	= unpackedData.mediaData[i].offsetMinY,
				};
			}

			return s;
		}


		public static BlrtUnpackedCanvasData UnpackData(BlrtTouchSerializableV2 serializable)
		{
			var d = new BlrtUnpackedCanvasData (){
				version 					= serializable.v,
				ratioHandling				= RatioHandling.CameraRatio,
				invertedCameraPosition	 	= true,
				invertedYAxisPosition	= true
			};


			d.cameraData = new BlrtUnpackedCanvasData.CameraPositioning ();

			d.cameraData.points = new BlrtUnpackedCanvasData.CameraPositioningPoint[serializable.camera.Length];
			for (int i = 0; i < serializable.camera.Length; ++i) {

				var last = i == 0 ? null : d.cameraData.points [i-1];

				var camera = serializable.camera [i];
				var c = new BlrtUnpackedCanvasData.CameraPositioningPoint (){
					x 			= camera.x ?? last.x, 
					y 			= camera.y ?? last.y,        
					zoom 		= camera.zoom ?? last.zoom,        
					timestamp 	= camera.timestamp ?? last.timestamp,       
					ratio		= camera.ratio ??  last.ratio,
					page		= camera.page ?? last.page
				};

				d.cameraData.points [i] = c;
			}


			d.touchData = new BlrtUnpackedCanvasData.BlrtElementData[serializable.actions.Length];
			for (int i = 0; i < serializable.actions.Length; ++i) {

				var action = serializable.actions[i];

				var t = new BlrtUnpackedCanvasData.BlrtElementData () {
					brushType 		= action.brush,
					page 			= action.page,
					color 			= action.color,
					removedToggle 	= action.removeToggle,
					thickness		= action.width,
					points			= new BlrtUnpackedCanvasData.BlrtElementDataPoint[action.points.Length]
				};

				for (int j = 0; j < action.points.Length; ++j) {
					t.points [j] = new BlrtUnpackedCanvasData.BlrtElementDataPoint () {
						x 			= action.points[j].x,
						y 			= action.points[j].y,
						timestamp 	= action.points[j].t,
						scale		= action.scale,
					};
				}

				d.touchData [i] = t;
			}




			d.mediaData = new BlrtUnpackedCanvasData.MediaData[serializable.media.Length];
			for (int i = 0; i < serializable.media.Length; ++i) {

				if (serializable.media [i].offsetMaxX != 0 && serializable.media [i].offsetMaxY != 0
					|| serializable.media [i].offsetMinX != 0 && serializable.media [i].offsetMinY != 0) {
					d.ratioHandling 		= RatioHandling.MediaPageOffset;
				}

				d.mediaData [i] = new BlrtUnpackedCanvasData.MediaData(){
					mediaWidth = serializable.media[i].width,
					mediaHeight = serializable.media[i].height,
					offsetMaxX	= serializable.media[i].offsetMaxX,
					offsetMaxY	= serializable.media[i].offsetMaxY,
					offsetMinX	= serializable.media[i].offsetMinX,
					offsetMinY	= serializable.media[i].offsetMinY,
				};
			}



			return d;
		}



		public class TouchData{
			[JsonProperty("p")]
			public int page;
			[JsonProperty("b")]
			public int brush;
			[JsonProperty("c")]
			public int color;
			[JsonProperty("w")]
			public int width;

			[JsonProperty("s")]
			public float scale;

			[JsonProperty("rt")]
			public float[] removeToggle;

			[JsonProperty("ps")]
			public BlrtTouchSavePoint[] points;
		}

		public class BlrtTouchSavePoint{
			public float x, y, t;
		}


		public class CameraData{
			public float? x;
			public float? y;

			[JsonProperty("z")]
			public float? zoom;
			[JsonProperty("r")]
			public float? ratio;
			[JsonProperty("t")]
			public float? timestamp;

			[JsonProperty("p")]
			public int? page;
		}



		public class MediaData {

			[JsonProperty("w")]
			public int width;
			[JsonProperty("h")]
			public int height;

			[JsonProperty("xa")]
			public float offsetMaxX;
			[JsonProperty("ya")]
			public float offsetMaxY;
			[JsonProperty("xi")]
			public float offsetMinX;
			[JsonProperty("yi")]
			public float offsetMinY;
		}
	}
}