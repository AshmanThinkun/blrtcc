using System;
using System.Drawing;

namespace BlrtCanvas.Util.Serialization
{
	public class BlrtTouchSerializableV1
	{
		public const int SerializableVersion = 1;

		public int v;

		public  BlrtCanvasCameraPoint[] CameraMoves{ get; set;}
		public  BlrtTouchSaveAction[] Actions{ get; set;}

		public BlrtTouchSerializableV1 ()	{ }


		public static BlrtTouchSerializableV1 PackData(BlrtUnpackedCanvasData unpackedData)
		{
			var s = new BlrtTouchSerializableV1 ();

			s.v 		= SerializableVersion;

			s.CameraMoves = new BlrtCanvasCameraPoint[unpackedData.cameraData.points.Length];
			for (int i = 0; i < unpackedData.cameraData.points.Length; ++i) {

				var camera = unpackedData.cameraData.points [i];

				var c = new BlrtCanvasCameraPoint (
					camera.x, camera.y,        
					camera.zoom,        
					camera.timestamp,   
					camera.page
				);

				s.CameraMoves [i] = c;
			}



			s.Actions = new BlrtTouchSaveAction[unpackedData.touchData.Length];
			for (int i = 0; i < unpackedData.touchData.Length; ++i) {

				var touch = unpackedData.touchData [i];

				var t = new BlrtTouchSaveAction () {
					Mode 			= touch.brushType,
					Page 			= touch.page,
					Color 			= touch.color,
					RemovedToggle 	= touch.removedToggle,
					Scale			= touch.points[0].scale,
					Width			= touch.thickness,
					Points			= new BlrtTouchSavePoint[touch.points.Length]
				};



				for (int j = 0; j < touch.points.Length; ++j) {
					t.Points [j] = new BlrtTouchSavePoint () {
						x = touch.points[j].x,
						y = touch.points[j].y,
						t = touch.points[j].timestamp
					};
				}

				s.Actions [i] = t;
			}

			return s;
		}


		public static BlrtUnpackedCanvasData UnpackData(BlrtTouchSerializableV1 serializable)
		{
			var d = new BlrtUnpackedCanvasData (){
				version 					= serializable.v,
				ratioHandling				= RatioHandling.AutoScaling,
				invertedCameraPosition 		= true,

				invertedYAxisPosition		= true
			};


			d.cameraData = new BlrtUnpackedCanvasData.CameraPositioning ();

			d.cameraData.points = new BlrtUnpackedCanvasData.CameraPositioningPoint[serializable.CameraMoves.Length];
			for (int i = 0; i < serializable.CameraMoves.Length; ++i) {

				var camera = serializable.CameraMoves [i];

				var c = new BlrtUnpackedCanvasData.CameraPositioningPoint (){
					x 			= camera.x, 
					y 			= camera.y,        
					zoom 		= camera.zoom,        
					timestamp 	= camera.timestamp,       
					ratio		= 1,
					page	= camera.page
				};

				d.cameraData.points [i] = c;
			}


			d.touchData = new BlrtUnpackedCanvasData.BlrtElementData[serializable.Actions.Length];
			for (int i = 0; i < serializable.Actions.Length; ++i) {

				var action = serializable.Actions[i];

				var t = new BlrtUnpackedCanvasData.BlrtElementData () {
					brushType 		= action.Mode,
					page 			= action.Page,
					color 			= action.Color,
					removedToggle 	= action.RemovedToggle,
					thickness		= action.Width,
					points			= new BlrtUnpackedCanvasData.BlrtElementDataPoint[action.Points.Length]
				};



				for (int j = 0; j < action.Points.Length; ++j) {
					t.points [j] = new BlrtUnpackedCanvasData.BlrtElementDataPoint () {
						x 			= action.Points[j].x,
						y 			= action.Points[j].y,
						timestamp 	= action.Points[j].t,
						scale		= action.Scale,
					};
				}

				d.touchData [i] = t;
			}

			d.mediaData = null;

			return d;
		}



		public class BlrtTouchSaveAction{
			public int Page;
			public int Mode;
			public int Color;
			public int Width;

			public float Scale = 1;

			public float[] RemovedToggle;

			public BlrtTouchSavePoint[] Points;
		}

		public class BlrtTouchSavePoint {
			public float x, y, t;
		}


		public class BlrtCanvasCameraPoint{


			public BlrtCanvasCameraPoint (float x, float y, float zoom, float timestamp, int page) {
				this.x 			= x;
				this.y 			= y;
				this.zoom 		= zoom;
				this.timestamp 	= timestamp;
				this.page 		= page;
			}

			public float x, y, zoom, timestamp, ratio;
			public int page;
		}
	}
}