﻿using System;
using BlrtCanvas.Util.Serialization;

namespace BlrtCanvas.Util
{
	public class BCMediaInfo {

		const float Padding = 0.0f;



		public static BCMediaInfo FromCanvasData (IBlrtCanvasData arg)
		{
			var p = new MediaPage[arg.PageCount];

			for (int i = 0; i < p.Length; ++i) {
			
				var page = arg.GetPage (i + 1);

				p [i] = new MediaPage (page.MediaWidth, page.MediaHeight);
			}

			return new BCMediaInfo (p);
		}

		public static BCMediaInfo FromMediaData (BlrtUnpackedCanvasData.MediaData[] mediaData)
		{
			if(mediaData == null)
				return null;

			var p = new MediaPage[mediaData.Length];

			for (int i = 0; i < p.Length; ++i)
				p [i] = new MediaPage (mediaData [i]);

			return new BCMediaInfo (p);
		} 




		private class MediaPage {
			int width, height;
			float offsetMaxX, offsetMaxY, offsetMinX, offsetMinY;

			float calulatedMinX, 
			calulatedMaxX,
			calulatedMinY,
			calulatedMaxY;

			public MediaPage (BlrtUnpackedCanvasData.MediaData media) 
				: this(media.mediaWidth, media.mediaHeight, media.offsetMaxX, media.offsetMaxY, media.offsetMinX, media.offsetMinY)
			{
			}

			public MediaPage(int width, int height) : this (width, height,0,0,0,0){
			}

			public MediaPage(int width, int height, float maxX, float maxY, float minX, float minY){
				this.width = width;
				this.height = height;
				this.offsetMaxX = maxX;
				this.offsetMaxY = maxY;
				this.offsetMinX = minX;
				this.offsetMinY = minY;

				Calulate();
			}

			public void Calulate(){

				calulatedMinX = Math.Min(offsetMinX - Padding * width, width * -0.5f); 
				calulatedMaxX = Math.Max(offsetMaxX + Padding * width, width * 0.5f);
				calulatedMinY = Math.Min(offsetMinY - Padding * height, height * -0.5f); 
				calulatedMaxY = Math.Max(offsetMaxY + Padding * height, height * 0.5f);
			}

			public float MediaWidth{ get { return width; } }
			public float MediaHeight{ get { return height; } }


			public float OffsetX{ get { return (calulatedMaxX + calulatedMinX); } }
			public float OffsetY{ get { return (calulatedMaxY + calulatedMinY); } }

			public float ViewWidth{ get { return (calulatedMaxX - calulatedMinX); } }
			public float ViewHeight { get { return (calulatedMaxY - calulatedMinY); } }
		}



		private MediaPage[] _pages;
		private BCMediaInfo(MediaPage[] pages){
			this._pages = pages;
		}

		public float GetMediaWidth(int page) {
			return _pages [page - 1].MediaWidth;
		}

		public float GetMediaHeight(int page) {
			return _pages [page - 1].MediaHeight;
		}

		public float GetXOffset(int page) {

			return _pages [page - 1].OffsetX;
		}

		public float GetYOffset(int page) {

			return _pages [page - 1].OffsetY;
		}


		public float GetViewWidth(int page) {

			return _pages [page - 1].ViewWidth;
		}

		public float GetViewHeight(int page) {

			return _pages [page - 1].ViewHeight;
		}
	}
}

