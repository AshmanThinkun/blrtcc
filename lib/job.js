var yaml = require('js-yaml');
var fs = require('fs');
var docObj = {
    version: 1,
    cron: []
}
var docYaml
var logger=require('./log')
const TARGET = process.env.npm_lifecycle_event;
console.log("TARGET :: "+TARGET)
module.exports = function (app) {
    return function job(name, cb, schedule) {
        // console.log(name+" is scheduled")
        // some jobs are never scheduled in legacy system
        if(!schedule)
        {
            return
        }
        else
        {
            console.log(name+" is scheduled")
            var task = {name: name, url: "/" + name, schedule: schedule}
            docObj.cron.push(task)
            //register url for scheduled job
            app.post('/' + name, function (req, res) {
                var start;
                var finish;
                var result={}
                req.start=function () {
                    start=new Date()
                }

                res.success=function (result) {
                    logger.info("The job "+name+" success")
                    if(start)
                    {
                        finish=new Date()
                        var diff=finish - start
                        result.duration=Number(diff)
                        logger.info(name+" success in "+result.duration+"ms",result)
                    }
                    res.end("The job success")
                }
                res.error=function (error) {
                    if(start)
                    {
                        finish=new Date()
                        var diff=finish - start
                        result.duration=Number(diff)
                        result.result=r
                        logger.info(name+ "fail !",result)
                    }
                    res.end("The job fail")
                }
                cb(req,res)
            })

            if(TARGET==="build"){
                try {
                    docYaml = yaml.safeDump(docObj)
                    fs.writeFileSync("./cron.yaml", docYaml)
                } catch (e) {
                }
            }


        }


    }
}