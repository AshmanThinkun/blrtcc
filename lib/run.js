var Client = require('node-rest-client').Client;
var serverConfig=require('../config/serverConfig')
var client = new Client();
module.exports = function run(fnName, payload, options) {
    return new Promise(function (resolve, reject) {
        var endpoint = serverConfig.serverUrl + "/functions/" + fnName;
        var headers = {"Content-Type": "application/json"}
        headers['X-Parse-Application-Id'] = serverConfig.appId;
        var sessionToken = null;
        var successCb = null;
        var errorCb = null;
        if (options) {
            sessionToken = options.sessionToken;
            successCb = options.success;
            errorCb = options.error;
        }
        if (sessionToken) {
            headers['X-Parse-Session-Token'] = sessionToken;
        }
        var args = {
            data: payload,
            headers: headers
        }

        client.post(endpoint, args, function (data, response) {
            var funcResult = data ? data.result : null;
            if(successCb){
                successCb(funcResult)
            }
            resolve(funcResult)
        }).on('error', function (err) {
            if(errorCb){
                errorCb(err)
            }
            reject(err)
        })
    })


}
