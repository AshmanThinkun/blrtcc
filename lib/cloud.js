'use strict'

var logger = require('./log')
module.exports = {
    define: function () {
        var defineCache = Parse.Cloud.define
        return function (name, cb) {
            defineCache(name, function (req, res) {
                var start;
                var finish;
                var result = {}
                req.start = function () {
                    start = new Date()
                    // result.params = req.params
                    // result.user = req.user
                }
                var successCache = res.success;
                var errorCache = res.error;
                res.success = function (r) {
                    if (start) {
                        finish = new Date()
                        var diff = finish - start
                        result.duration = Number(diff)
                        // result.result = r
                        // result.name=name
                        logger.info(name + " success in " + result.duration + "ms")
                    }
                    successCache.apply(this, arguments);
                }
                res.error = function (r,e) {
                    if (start) {
                        finish = new Date()
                        var diff = finish - start
                        result.duration = Number(diff)
                        result.result = r
                        result.name=name
                        logger.error(e)
                        logger.error(name + " fail !", result)
                    }
                    errorCache.apply(this, arguments);
                }
                cb(req, res);
            })
        }
    },
    afterSave:function () {
        var afterSaveCache = Parse.Cloud.afterSave
        return function (className, cb) {
            afterSaveCache(className, function (req, res) {
                var start;
                var finish;
                var result = {}
                req.start = function () {
                    start = new Date()
                    result.object = req.object
                    result.user = req.user
                }
                var successCache = res.success;
                var errorCache = res.error;
                res.success = function (r) {
                    if (start) {
                        finish = new Date()
                        var diff = finish - start
                        result.duration = Number(diff)
                        result.result = r
                        logger.info(className + "afterSave trigger success in " + result.duration + "ms", result)
                    }
                    successCache.apply(this, arguments);
                }
                res.error = function (e) {
                    if (start) {
                        finish = new Date()
                        var diff = finish - start
                        result.duration = Number(diff)
                        result.result = e.toString()
                        logger.error(className + "afterSave trigger fail !", result)
                    }
                    errorCache.apply(this, arguments);
                }
                cb(req, res);
            })
        }
    }
}



