var _ = require('underscore')
module.exports = function () {
    return function ckeckCookieAndAppendUser(req, res, next) {
        var id = req.signedCookies['parse.sess'];
        if (!id) {
            next();
        }
        else {
            var user=new Parse.User();
            user.id=id;
            req.user = user;
            next();
        }

    }
}