# some environment should be specified 
* PARSE_SERVER_DATABASE_URI (no fallback)
* SERVER_URL (no fallback)
* PARSE_SERVER_APPLICATION_ID {no fallback}
* PARSE_SERVER_MASTER_KEY (no fallback)
* BLRT_STREAM (fallback to 'd' if not specified)
* LOG_MONGO_URI (fallback to log file)
* WEBAPP_SERVER_URL (for posting real-time message ,If not provide ,just ignore it)
* SITE_ADDRESS (for some express route other than parse api)
* Always set PORT as 8081

# version
* use node v6.2.2 (same with elastic beanstalk version)

# how to start 
* npm start (we can enjoy pm2 with the command)
* npm build simply generate the cron.yaml file