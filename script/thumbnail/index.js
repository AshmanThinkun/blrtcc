const Parse = require('parse/node');
const fs = require('fs')
Parse.initialize('blrt');
Parse.serverURL = "http://m.blrt.co/parse";
// Parse.initialize('blrt');
// Parse.serverURL = "https://m.blrt.co/parse";
const gm = require('gm')
var util = require('util');
var logFile = fs.createWriteStream('log.txt', { flags: 'a' });
// Or 'w' to truncate the file every time the process starts.
var logStdout = process.stdout;

console.log = function () {
    logFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
}
console.error = console.log;

const exec = require('child_process').exec;
const http = require('http');
const downloadFn = require('download-file')


function base64_encode(file) {
    var bitmap = fs.readFileSync(file);
    return new Buffer(bitmap).toString('base64');
}

function getThumb(path) {
    return new Promise((resolve, reject) => {
        gm(`${path}[0]`)
            .write(`./tmp`, function (error) {
                // Callback function executed when finished
                if (!error) {
                    gm(`./tmp`)
                        .thumb(
                            256, // Width
                            256, // Height
                            './thumbnail.jpg', // Output file name
                            60, // Quality from 0 to 100
                            function (error) {
                                if (!error) {
                                    resolve(base64_encode('./thumbnail.jpg'))

                                } else {
                                    reject(error)
                                }
                            }
                        );
                } else {
                    reject(error)
                }
            });
    })
}

const download = 'download.jpg'
const decrypted = './decrypted.jpg'
function processObj(obj) {
    const {id, encryptValue, encryptType, url} = obj

    return new Promise((resolve, reject) => {
        downloadFn(url, {
            directory: "./",
            filename: download
        }, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(`${id} : download success`)
            }
        })
    })
        .then((r) => {
            //todo for debug
            // console.log(r)
            return new Promise((resolve, reject) => {
                exec(`./decrypt ${download} ${decrypted} ${encryptType} ${encryptValue}`, {}, (e,o) => {
                    if(e){
                        reject(e)
                    }
                    resolve(`${id} : decrypted success`)
                })
            })
        })
        .then((r) => {
            //todo for debug
            // console.log(r)
            return getThumb(decrypted)

        })
        .then((base64) => {
            return Parse.Promise.when(
                [new Parse.File('thumb.jpg', {base64}).save(),
                new Parse.Query('MediaAsset').get(id)])
        })
        .then(function(results) {
            const newFile=results[0];
            const mediaAsset=results[1]
            return mediaAsset.set('thumbnailFile', newFile).save()
        })
        .then((media)=>{
            return `SUCCESS ${id} : ${media.get('thumbnailFile').url()}`
        })
        .catch(function (err) {
            return `ERROR ${id} : ${JSON.stringify(err)}`

        });

}

function processFile(path) {
    const json = require(path);
    let counter=0
    const result = json.reduce((memo, obj) => {
        return memo.then((pre) => {
            console.log("%d. %s",counter++,pre)
            return processObj(obj)
        })
    }, new Parse.Promise.as(`start ${path} there are ${json.length} files inside ===\n`))

    return result.then((pre) => {
        console.log((counter++)+". "+pre) //the result of the last entry in reduce function goes here
        return `${path} complete, check logs to see if there is any error \n\n`
    })
}


function processApp(dir) {
    const a=[...Array(25).keys()]
    return a.reduce((memo,index)=>{
        return memo.then((pre)=>{
            console.log(pre)
            return processFile(`./${dir}/file${index}`)
        })
    },new Parse.Promise.as(`start to process ${dir} ======= `))
}


processApp('live').then(function () {
    console.log("All done")
})



// redirect stdout / stderr