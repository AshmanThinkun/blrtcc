console.log(process.version)
const Parse = require('parse/node');
const fs = require('fs');
Parse.initialize('blrt');
Parse.serverURL = "https://m.blrt.co/parse";


function getAllObjects(baseQuery) {
    var allObjects = [];
    let index = 0
    var next = function () {
        if (allObjects.length) {
            baseQuery.greaterThan('createdAt', allObjects[allObjects.length - 1].createdAt);
        }
        return baseQuery.find().then(function (r) {
            const data = (r.map(i => ({
                id: i.id,
                encryptValue: i.get('encryptValue'),
                encryptType: i.get('encryptType'),
                url: i.get('mediaFile').url()
            })))
            fs.writeFileSync(`./live/file${index++}.json`, JSON.stringify(data))
            allObjects = allObjects.concat(r);
            if (r.length === 0) {
                // return Promise.resolve(allObjects);
                return `done : ${allObjects.length}`
            } else {
                return next();
            }
        });
    }
    return next();
}
new Parse.Query('MediaAsset')
    .count()
    .then((r) => {
        console.log(`${r} in total`)
        const query = new Parse.Query('MediaAsset');
        query.select('createdAt', 'encryptValue', 'encryptType','mediaFile');
        query.ascending('createdAt');
        query.exists('encryptValue')
        query.doesNotExist('thumbnailFile')
        query.limit(100);

        return getAllObjects(query)

    })
    .then((result) => {
        console.log(result)
    })
    .fail((e)=>{
        console.log(e)
    })

