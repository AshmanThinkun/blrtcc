﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using BlrtApp.CustomViews;
using BlrtApp.iOS.CustomRenderer;

[assembly: ExportRenderer (typeof (PlayProgressSlider), typeof (PlayProgressSliderRenderer))]
namespace BlrtApp.iOS.CustomRenderer
{
	public class PlayProgressSliderRenderer : SliderRenderer
	{
		private PlayProgressSlider RealElement
		{ 
			get {
				var slider = Element as PlayProgressSlider;
				if (slider != null) {
					return slider;
				} else {
					throw new Exception("Render is not connected to PlayProgressSlider");
				}
			} 
		}

		public PlayProgressSliderRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Slider> eventarg)
		{
			base.OnElementChanged (eventarg);
			Element.ValueChanged += (sender, e) => {
				RealElement.NotifyProgressChanged(new PlayProgressSlider.ProgressChangedArgs((float)Element.Value));
			};
			Control.TouchDown += (sender, e) => {
				RealElement.NotifyTouchDown();
			};
			Control.TouchCancel += (sender, e) => {
				RealElement.NotifyTouchUp();
			};
			Control.TouchUpInside += (sender, e) => {
				RealElement.NotifyTouchUp();
			};
			Control.TouchUpOutside += (sender, e) => {
				RealElement.NotifyTouchUp();
			};
		}
	}
}

