var express = require('express');
var logger=require('./lib/log');
// console.log(process.env)
 console.log ("\n\n\n=====DB URL = "+process.env.PARSE_SERVER_DATABASE_URI);
//Population Parse global
var ParseServer = require('parse-server').ParseServer;
var S3Adapter = require('parse-server').S3Adapter;
var app = express();

const logger1=require('parse-server').logger

//Parse's share the same implementation will client side i.e. ajax making it very problematic
//use our owm implementation instead.

//We need 'config' job with app,it is easier to export here
var job=require("./lib/job");
module.exports.job=job(app)
process.on('uncaughtException',function (err) {
    //Thanks to aws elastic beanstalk ,the server can be restart automatically
    console.log("error happens")
    logger.error("uncaught error !!! ",err)
    console.log(err)
    process.exit(1)
})

//compatible to Parse
require('app-module-path').addPath(__dirname);
var serverConfig = require('config/serverConfig');
// const test={
//     validateAuthData: function validateAuthData() {
//         console.log("validateAuthData validateAuthData index.js")
//         return Promise.resolve();
//     },
//     validateAppId: function validateAppId() {
//         console.log("validateAppId validateAppId validateAppId")
//         return Promise.resolve();
//     }
// }
//console.log(serverConfig);

var api = new ParseServer({
    databaseURI: process.env.PARSE_SERVER_DATABASE_URI||serverConfig.databaseURI,
    appId: serverConfig.appId,
    masterKey: serverConfig.masterKey, //Add your master key here. Keep it secret!
    //This URL will be used when making requests to Parse Server from Cloud Code
    //using localhost can avoid external traffic, elastic beanstalk set port 8081,
    //we should follow this convention
    //https://github.com/ParsePlatform/parse-server/issues/2756
    serverURL:"http://localhost:8081/parse",
    // serverURL: process.env.SERVER_URL, // use the environment variable SERVER_URL
    fileKey: serverConfig.fileKey,
    cloud: __dirname + '/cloud/main.js',
    //todo try azure
    //the same AWS user have access of all the stream
    filesAdapter: new S3Adapter(
        "AKIAIFTPKGNBF22HDBRQ",
        "W66I7sbiD9ODtq7fhvy2MOBSq81SdiWp77IsfADb",
        serverConfig.s3AapaterBuket,
        {directAccess: true}
    ),

    // filesAdapter: new AzureStorageAdapter("blrtdevblobstorage", "firstprivate", options),
    // filesAdapter:filesAdapter,
    push:{
        ios:{
            pfx: serverConfig.IOSPushPath,
            bundleId: serverConfig.pushBundleID,
            production: process.env.BLRT_STREAM==='m'
        },
        android: {
            senderId: serverConfig.androidSenderId,
            apiKey: serverConfig.androidApiKey
        },
    },
    emailAdapter: {
        module: 'parse-server-mandrill-adapter',
        options: {
            //todo avoid a bug in sdk
            customUserAttributesMergeTags:[],
            // API key from Mandrill account
            apiKey: 'cKMAu25QxPorE-z6DLhHSw',
            // From email address
            fromEmail: 'no-reply@blrt.com',
            // Display name
            displayName: 'no-reply@blrt.com',
            // Reply-to email address
            replyTo: 'no-reply@blrt.com',
            // Verification email subject
            verificationSubject: 'Please verify your e-mail for *|appname|*',
            // Verification email body
            verificationBody: 'Hi *|username|*,\n\nYou are being asked to confirm the e-mail address *|email|* with *|appname|*\n\nClick here to confirm it:\n*|link|*',
            // Password reset email subject
            passwordResetSubject: 'Password Reset Request for *|appname|*',
            // Password reset email body
            passwordResetBody: 'Hi ,\n\nTo reset your password for Blrt, follow the link below.\nIf you \'ve remembered your password, or you didn \'t make this request, feel free to ignore this email.\nReset password here:\n*|link|*'
        }
    },
    customPages: {
        choosePassword: '/usermanagement/choose_password.html',
        passwordResetSuccess: '/usermanagement/password_updated.html'
    },
    publicServerURL: serverConfig.serverUrl,
    // Your apps name. This will appear in the subject and body of the emails that are sent.
    appName: serverConfig.appId,
    // oauth:{
    //     test:{
    //         module:test
    //     }
    // }
});

console.log("\n\n\n serverConfig.databaseURI = "+serverConfig.databaseURI);
console.log('appId appId appId: '+api.appId);

var ParseDashboard = require('parse-dashboard');
var dashboard = new ParseDashboard({
    "apps": [
        {
            "serverURL": serverConfig.serverUrl,
            "appId": serverConfig.appId,
            "masterKey":serverConfig.masterKey,
            "appName": serverConfig.appId
        }
    ],
    "users": [
        {
            "user":"blrt",
            "pass":"VgL4ut`f4$Hy)"
        }
    ],
});


//todo in AWS elasticbeanstalk we can config an ngnix/apache server to server the static put it here just for localhost
app.use(express.static('public'));


//our own rest API
//For backward computability, we mount this to root .So API endpoint should never be /parse or /dashboard
//todo it is better to specify a separate mount path rather than root
require('cloud/routes')(app)
//mount parse api to /parse
app.use('/parse',api);
//mount parse-dashboard application to /dashboard
app.use('/dashboard', dashboard);

var port = 8081;
if(process.env.npm_lifecycle_event!="build"){
    var httpServer = require('http').createServer(app);
    httpServer.listen(port,function() {
        console.log('Blrt server running on port ' + port + '.');
    });

}

Parse.Cloud.useMasterKey();

/******************************************************************************************************************
 ************************************************************************************************************************/


