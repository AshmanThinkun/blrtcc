using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Dropins.Chooser.iOS;
using Foundation;
using UIKit;
using Parse;
using BlrtData;
using Xamarin.Forms;
using Xamarin;
using Facebook.CoreKit;

#if QA || CE
using TestFlightBinding;
#endif

namespace BlrtiOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		UIWindow window;

		public override UIWindow Window {
			get {
				return window;
			}
			set {
				window = value;
			}
		}


		BlrtiOS.Views.Root.RootAppController nav;

		// This method is invoked when the application has loaded and is ready to run
		// You have 17 seconds to return from this method, or iOS will terminate your application
		public override bool FinishedLaunching (UIApplication uiApplication, NSDictionary launchOptions)
		{
			try {
				InitXamarinInsights ();
				Forms.Init ();
				LoadApplication (new App ());
				InitYozio ();
				//init Segment
				#if DEBUG
				Segment.Analytics.Initialize(BlrtConstants.SegmentKey/*, new Segment.Config().SetAsync(false)*/);
				#else
				Segment.Analytics.Initialize(BlrtConstants.SegmentKey);
				#endif

				// Initialise our dependencies and directories
				BlrtData.BlrtConstants.Setup ();
				MR.Gestures.iOS.Settings.LicenseKey = BlrtConstants.MrGesturesKey;
				UINavigationBar.Appearance.SetTitleTextAttributes (new UITextAttributes () {
					Font = UIFont.SystemFontOfSize (18)
				});

				//vernacular
				Vernacular.Catalog.Implementation = new Vernacular.ResourceCatalog {
					GetResourceById = id => {
						var resource = NSBundle.MainBundle.LocalizedString (id, null);
						return resource == id ? null : resource;
					}
				};

				// Initialise FaceBook stuff
				ParseFacebookUtils.Initialize (BlrtData.BlrtConstants.FacebookAppId);
				Profile.EnableUpdatesOnAccessTokenChange (true);
				Settings.AppID = BlrtData.BlrtConstants.FacebookAppId;
				Settings.DisplayName = BlrtData.BlrtConstants.FacebookAppName;
				FBManager.FetchDeferredAppLink();


				BlrtUserVoice.Initialize (BlrtData.BlrtConstants.UserVoiceSite, BlrtData.BlrtConstants.UserVoiceForumId);


				LLogger.AppVersion = BlrtConfig.APP_VERSION;
				LLogger.DeviceModel = BlrtHelperiOS.GetDeviceModel ();


				#if QA || CE
				TestFlight.TakeOffThreadSafe(BlrtData.BlrtConstants.TestFlightKey);
				#endif

				if (BlrtHelperiOS.IsIOS8Above) {

					UIApplication.SharedApplication.RegisterUserNotificationSettings (
						UIUserNotificationSettings.GetSettingsForTypes (
							UIUserNotificationType.Alert
							| UIUserNotificationType.Badge
							| UIUserNotificationType.Sound, 
							new NSSet ()
						)
					);
				
				} else {
					// Register for our Blrt push notifications
					UIApplication.SharedApplication.RegisterForRemoteNotificationTypes (
						UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound
					);
				}

				// Create a new window instance based on the screen size
				Window = new UIWindow (UIScreen.MainScreen.Bounds);

				// Set our own RootViewController
				if (Window.RootViewController == null) {
					nav = new BlrtiOS.Views.Root.RootAppController ();

					Window.RootViewController = nav;
					Window.TintColor = BlrtiOS.BlrtConfig.BLRT_ACTION;
				}


				// Make the window visible
				Window.MakeKeyAndVisible ();

				if (ParseUser.CurrentUser == null) {
					BlrtManager.Logout (false);
				} else {
					if (!BlrtManager.Login (ParseUser.CurrentUser, false, false)) {
						BlrtManager.Logout (false);
					}
				}

				// Handle any options
				//if (launchOptions != null) {
				//	if (launchOptions.ContainsKey(UIApplication.LaunchOptionsRemoteNotificationKey)) {
				//		OpenArgs (BlrtData.BlrtPushState.OpenFromPush, launchOptions [UIApplication.LaunchOptionsRemoteNotificationKey] as NSDictionary).FireAndForget ();
				//	}
				//}

				ParseAnalytics.TrackAppOpenedAsync ();
				BlrtData.Helpers.BlrtTrack.TrackInstall();
				// All good!
				// This method verifies if you have been logged into the app before, and keep you logged in after you reopen or kill your app.
				ApplicationDelegate.SharedInstance.FinishedLaunching (uiApplication, launchOptions);

				System.Threading.Tasks.Task.Run (delegate {
					PendingActionHandler.HandlePendingActions ().FireAndForget ();
				});

				return true;
			} catch (Exception e) {
				//if app failed to startup, we'll remove all local catch, and try start up again. Even the app failed to start up again, 
				//at least next time when the user want to start all app it should work
				if (_triedRelunch) {
					throw e;
				} else {
					_triedRelunch = true;
				}
				try {
					//send crash report
					SendCrashReport ("startup", e);
					//clean everthing
					BlrtConstants.DeleteEverything ();
					if (ParseUser.CurrentUser != null)
						ParseUser.LogOut ();
					BlrtManager.Logout (false);
					return FinishedLaunching (uiApplication, launchOptions);
				} catch (Exception es) {
					es.ToString ();
					return FinishedLaunching (uiApplication, launchOptions);
				}
			}
		}

		static bool _triedRelunch;

		void SendCrashReport (string from, Exception e)
		{
			Insights.Report (e, Insights.Severity.Error);
			var url = BlrtConstants.ServerUrl + "/internal/crashreport";
			try {
				using (WebClient client = new WebClient ()) {
					client.UploadValues (url, new System.Collections.Specialized.NameValueCollection () {
						{ "from", from },
						{ "version", NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"].ToString () },
						{ "exception", e.ToString () },
						{ "crashReportKey", BlrtConstants.CrashReprtKey }
					});
				}
			} catch {
			}
		}

		void InitXamarinInsights ()
		{
			var ds = NSUserDefaults.StandardUserDefaults;
			try {
				var disableValue = ds.ValueForKey (new NSString ("DisableCrashReport"));
				bool disable = false;
				if (disableValue != null) {
					disable = disableValue.ToString ().ToLower () == "true";
				}

				Xamarin.Insights.DisableCollection = disable;
			} catch (Exception e) {
				Xamarin.Insights.DisableCollection = false;
			}
		}

		void InitYozio ()
		{
			Yozio.Yozio.InitializeWithAppKey (BlrtConstants.YozioKey, BlrtConstants.YozioSecret, new Action<NSDictionary> ((metaData) => {
				var dic = metaData.ToDictionary (t => t.Key.ToString (), v => v.Value.ToString ());
				BlrtUserHelper.InstallMetaData = dic;
			}));
		}


		public override void DidReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			switch (application.ApplicationState) {
				case UIApplicationState.Inactive:
					OpenArgs (BlrtData.BlrtPushState.ResumeFromPush, userInfo).FireAndForget ();
					break;
				case UIApplicationState.Background:
					OpenArgs (BlrtData.BlrtPushState.Background, userInfo).FireAndForget ();
					break;
				case UIApplicationState.Active:
					OpenArgs (BlrtData.BlrtPushState.InApp, userInfo).FireAndForget ();
					break;
			}

			if (completionHandler != null)
				completionHandler (UIBackgroundFetchResult.NoData);

			return;
		}

		public override void OnResignActivation (UIApplication application)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
			nav.OnMoveToBackground ();
		}

		// This is run every time the app moves into the "active" state
		// For instance, when opening the app for the first time or switching from another app to this one
		public override void OnActivated (UIApplication application)
		{
			// We need to properly handle activation of the application with regards to SSO
			// (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
			Facebook.CoreKit.AppEvents.ActivateApp ();

			nav.AppDidResume ();
			nav.OnMoveToForeground ();

			BlrtData.Helpers.BlrtTrack.AppActivate ();
		}

		public override void DidRegisterUserNotificationSettings (UIApplication application, UIUserNotificationSettings notificationSettings)
		{
			application.RegisterForRemoteNotifications ();
		}


		// This is run when the user allows the app to use push notifications
		// iOS provides the app with a deviceToken which we need to give to Parse so it knows who to push to
		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			// Update the Parse Installation to use the device token provided by iOS
			BlrtData.BlrtParseInstallation.Current.SetDeviceToken (
				deviceToken.Description.Replace ("<", "").Replace (">", "").Replace (" ", "")
			);
		}

		// When any URLs of specific types (as registered in Info.plist) are opened on the iOS device

		// The two types of URLs to handle are Dropbox app URLs (specifically for Blrt's dropbox app)
		// and Blrt specific URLs (for instance when following a Blrt link from an email)
		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			System.Threading.Tasks.Task.Run (delegate {
				
			
					// sourceApplication is null when this app is the source application
					if (sourceApplication == null)
						sourceApplication = "";
					
					Yozio.Yozio.TrackDeeplink (url);
					if (ParseUser.CurrentUser != null) {
						BlrtData.Helpers.BlrtTrack.TrackURLClick (url.ToString ());
					}

					if (url.AbsoluteString.StartsWith (BlrtConstants.UrlProtocolWithSlashes, StringComparison.Ordinal)) {
						InvokeOnMainThread (delegate {
							 BlrtManager.ParseUrl (url.AbsoluteString, application.ApplicationState == UIApplicationState.Active);
						});
					}

					InvokeOnMainThread (delegate {
						// Can the DropBox SDK handle this URL? If so, go for it!
						if (DBChooser.DefaultChooser.HandleOpenUrl (url)) {
							return;
						}

						if (ApplicationDelegate.SharedInstance.OpenUrl (application, url, sourceApplication, annotation))
							return;

						BlrtData.BlrtManager.ParseUrl (url.AbsoluteString, application.ApplicationState == UIApplicationState.Active);
					});
			});
			return true;
		}

		private async System.Threading.Tasks.Task OpenArgs (BlrtData.BlrtPushState state, NSDictionary args)
		{
			if (args == null || args.Count == 0)
				return;

			if (state != BlrtPushState.InApp && state != BlrtPushState.Background) {
				await System.Threading.Tasks.Task.Delay (200);
			}
			var dic = new Dictionary<string, object> ();
			foreach (var kvp in args) {
				dic.Add (kvp.Key.ToString (), ToValue (kvp.Value));
			}

			if (state == BlrtPushState.ResumeFromPush) {
				// it has high priority because user tapped on the notification. Thus, it needs to be settled immediately.
				BlrtManager.App.SettleHighPriorityPushNotification (dic);
				return;
			}

			await BlrtData.BlrtManager.ParsePush (state, dic);
		}

		private object ToValue (NSObject obj)
		{

			if (obj is NSDictionary) {

				var dic = new Dictionary<string, object> ();

				foreach (var kvp in obj as NSDictionary) {
					dic.Add (kvp.Key.ToString (), ToValue (kvp.Value));
				}
				return dic;
			}
			if (obj is NSArray) {

				var list = new List<object> ();

				var arr = (obj as NSArray);

				for (nuint i = 0; i < arr.Count; ++i) {
					list.Add (ToValue (arr.GetItem<NSObject> (i)));
				}
				return list;
			}
			if (obj is NSNumber) {
				return (obj as NSNumber).Int32Value;
			}

			return obj.ToString ();
		}

		public override void ReceiveMemoryWarning (UIApplication application)
		{
			nav.OnSystemMemoryWarning ();
		}
	}
	public class App : global::Xamarin.Forms.Application
	{
	}
}

