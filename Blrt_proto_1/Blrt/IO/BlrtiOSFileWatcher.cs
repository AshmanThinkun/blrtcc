﻿using System;
using BlrtShared;
using BlrtData;
using System.IO;
using CoreFoundation;
using System.Collections.Generic;
using BlrtiOS;
using System.Threading;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency (typeof(BlrtiOSFileWatcherHelper))]
namespace BlrtiOS
{
	public class BlrtiOSFileWatcherHelper : IFileWatcherPlatformHelper
	{
		#region IFileWatcherPlatformHelper implementation
		public IFileWatcher CreateWatcher (string path)
		{
			if (string.IsNullOrWhiteSpace (path)) {
				throw new ArgumentNullException ("path");
			}
			return new BlrtiOSFileWatcher (path);
		}
		#endregion
		
	}

	public class BlrtiOSFileWatcher : IFileWatcher
	{
		string _path;
		bool _preFileExisted = false;
		public BlrtiOSFileWatcher (string path)
		{
			_path = path;
			_preFileExisted = File.Exists (_path);
			DirectoryMonitorManager.Subscribe (this);
		}

		// this is used to monitor a single file; we delete it because it can open too many file descriptors
		// check commit 118d9e1aca088f4d964e37471ca78cbdc59a6c3a for full usage code
//		private void MonitorFile ()
//		{
//			var queue = DispatchQueue.GetGlobalQueue (CoreFoundation.DispatchQueuePriority.Default);
//			_fileMonitor = new DispatchSource.VnodeMonitor(_path, CoreFoundation.VnodeMonitorKind.Delete | CoreFoundation.VnodeMonitorKind.Rename, queue);
//			_fileMonitor.SetEventHandler (() => {
//				if (!FileExists) {
//					if (null != FileExistanceChanged) {
//						Task.Run (() => {
//							FileExistanceChanged?.Invoke (this, false);
//						});
//					}
//					_fileMonitor?.Dispose ();
//					_fileMonitor = null;
//					MonitorDirectory ();
//				}
//			});
//			_fileMonitor.Resume ();
//		}

		private void OnDirectoryWritten()
		{
			var currentExistance = File.Exists (_path);
			if (_preFileExisted != currentExistance) {
				_preFileExisted = currentExistance;
				FileExistanceChanged?.Invoke (this, currentExistance);
			}
		}

		public bool FileExists {
			get {
				return File.Exists (_path);
			}
		}

		#region IFileWatcher implementation

		public event EventHandler<bool> FileExistanceChanged;

		public void Dispose ()
		{
			DirectoryMonitorManager.Unsubscribe (this);
		}
		#endregion

		static class DirectoryMonitorManager
		{
			class MonitorAdapter : IDisposable
			{
				DispatchSource.VnodeMonitor _monitor;
				List<BlrtiOSFileWatcher> _subscribers;
				ReaderWriterLockSlim _listLock = new ReaderWriterLockSlim ();

				public MonitorAdapter (string directory)
				{
					_subscribers = new List<BlrtiOSFileWatcher>();
					var queue = DispatchQueue.GetGlobalQueue (CoreFoundation.DispatchQueuePriority.Default);
					_monitor = new DispatchSource.VnodeMonitor(directory, CoreFoundation.VnodeMonitorKind.Write, queue);
					_monitor.SetEventHandler (OnAction);
					_monitor.Resume ();
				}

				public void Add (BlrtiOSFileWatcher watcher)
				{
					using (_listLock.DisposableWriteLock ()) {
						if (!_subscribers.Contains (watcher)) {
							_subscribers.Add (watcher);
						}
					}
				}

				public void Remove (BlrtiOSFileWatcher watcher)
				{
					using (_listLock.DisposableWriteLock ()) {
						if (_subscribers.Contains (watcher)) {
							_subscribers.Remove (watcher);
						}
					}
				}

				public bool IsEmpty {
					get { return (_subscribers.Count == 0); }
				}

				void OnAction ()
				{
					List<BlrtiOSFileWatcher> targetList = null;
					using (_listLock.DisposableReadLock ()) {
						targetList = new List<BlrtiOSFileWatcher> (_subscribers);
					}
					foreach (var item in targetList) {
						item.OnDirectoryWritten ();
					}
					targetList = null;
				}

				#region IDisposable implementation

				public void Dispose ()
				{
					_monitor?.Dispose ();
					_monitor = null;
					_subscribers.Clear ();
					_subscribers = null;
				}

				#endregion
			}

			static Dictionary<string, MonitorAdapter> _dirMonitors = new Dictionary<string, MonitorAdapter>();
			static MonitorAdapter Get (string filePath, bool createNew = true)
			{
				var directory = Path.GetDirectoryName (filePath);
				lock (_dirMonitors) {
					if (_dirMonitors.ContainsKey (directory)) {
						return _dirMonitors [directory];
					}

					if (!createNew) {
						return null;
					}

					if (!Directory.Exists (directory)) {
						try {
							Directory.CreateDirectory (directory);
						} catch {}
					}

					var monitor = new MonitorAdapter (directory);
					_dirMonitors.Add (directory, monitor);
					return monitor;
				}
			}

			public static void Subscribe (BlrtiOSFileWatcher subscriber)
			{
				var monitorSubscriber = Get (subscriber._path);
				monitorSubscriber.Add (subscriber);
			}

			public static void Unsubscribe (BlrtiOSFileWatcher subscriber)
			{
				var monitorSubscriber = Get (subscriber._path, false);
				if (null != monitorSubscriber) {
					monitorSubscriber.Remove (subscriber);
					if (monitorSubscriber.IsEmpty) {
						_dirMonitors.Remove (Path.GetDirectoryName (subscriber._path));
						monitorSubscriber.Dispose ();
						monitorSubscriber = null;
					}
				}
			}
		}

	}
}

