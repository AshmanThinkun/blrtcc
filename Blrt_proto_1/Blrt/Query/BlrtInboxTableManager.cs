using System;
using System.Linq;
using System.Collections.Generic;
using Parse;
using BlrtData;
using BlrtData.Query;
using Foundation;

namespace BlrtiOS.Query
{
	public class BlrtInboxTableManager {

		private class BlrtInboxTableManagerSection {
			public string name = null;
			public List<ConversationUserRelation> items;
			public bool displayName;
			public bool showFlagged;
		}



		List<BlrtInboxTableManagerSection> _list;

		//public bool BlrtAuthor {  	get; set; }
		public bool ShowCached {  	get; set; }
		//public bool SeperateNew { 	get; set; }
		//public bool ShowArchive { 	get; set; }

		public BlrtInboxQueryType Type{ get; private set; }


		public string TranslationPrefix { get; set; }

		public string[] _tags;
		public string[] _userIds;

		public int ItemCount {
			get{
				var i = 0;

				foreach (var kvp in _list)
					i += kvp.items.Count;


				return i;
			
			}
		}

		public EventHandler OnRefresh{ get; set; }

		public BlrtInboxTableManager(BlrtInboxQueryType type) {

			Type = type;
			_list = new List<BlrtInboxTableManagerSection>();

			ShowCached 	= false;
			TranslationPrefix = "";
		}

		public void Reload()
		{
			_list.Clear ();

			var flagged = new List<ConversationUserRelation> ();
			var normalList = new List<ConversationUserRelation> ();
			var unsent = new List<ConversationUserRelation> ();

			if (ParseUser.CurrentUser != null) {

				List<ConversationUserRelation> relations = null;

				if (_userIds == null || _userIds.Length == 0)
					relations = BlrtManager.DataManagers.Default.GetConversationUserRelations (ParseUser.CurrentUser.ObjectId).ToList ();
				else
					relations = GetRelationWithUser (_userIds);

				relations.RemoveAll (relation => relation.Conversation == null || relation.Deleted || relation.Conversation.Deleted);

				relations.Sort ((ConversationUserRelation x, ConversationUserRelation y) => { 
					var tmp = x.Flagged.CompareTo (y.Flagged);
					if (tmp != 0)
						return -tmp;

					return -x.Conversation.ContentUpdatedAt.CompareTo (y.Conversation.ContentUpdatedAt);
				});

				foreach (var cur in relations) {
					if (Contains (cur, true)) {
						if (cur.Flagged)
							flagged.Add (cur);
						else
							normalList.Add (cur);
					}
				}

				if (ShowCached) {
				
					var cachedConversations = BlrtManager.DataManagers.Default.GetUnsyncedConversations ();
				
					foreach (var c in cachedConversations) {
						var relation = ConversationUserRelation.CreatePlaceholder (c);
						unsent.Add (relation);
					}
				}



				if (unsent.Count > -1) {
					_list.Add (new BlrtInboxTableManagerSection () {
						//name = BlrtHelperiOS.Translate (string.Format ("{0}_section_cached", TranslationPrefix)), 
						items = unsent,
						displayName = false
					});
				}
				if (flagged.Count > -1) {
					_list.Add (new BlrtInboxTableManagerSection () {
						//name = BlrtHelperiOS.Translate (string.Format ("{0}_section_flagged", TranslationPrefix)), 
						items = flagged,
						displayName = false,
						showFlagged = true
					});
				}
				if (normalList.Count > -1) {
					_list.Add (new BlrtInboxTableManagerSection () {
						//name = BlrtHelperiOS.Translate (string.Format ("{0}_section_read", TranslationPrefix)), 
						items = normalList,
						displayName = false
					});
				}

			}


			if (OnRefresh != null)
				OnRefresh (this, EventArgs.Empty);
		}

		public bool HasConversation (Conversation conversation)
		{
			//null checks for rare cause there is no relation for the current user
			if(conversation != null){
				var relation = conversation.LocalStorage.DataManager.GetConversationUserRelation (conversation.ObjectId, ParseUser.CurrentUser.ObjectId);

				if(relation != null)
					return Contains (relation, false);
			}

			return false;
		}

		List<ConversationUserRelation> GetRelationWithUser (string[] userIds)
		{
			var cons = new List<string> ();
			var relations = new List<ConversationUserRelation> ();

			for (int i = 0; i < userIds.Length; ++i) {

				var tmpRelations = BlrtManager.DataManagers.Default.GetConversationUserRelations (userIds[i]);

				foreach(var rel in tmpRelations) {
					if (!cons.Contains (rel.ConversationId))
						cons.Add (rel.ConversationId);
				}
			}


			foreach(var c in cons) {
				var rel = BlrtManager.DataManagers.Default.GetConversationUserRelation (c, ParseUser.CurrentUser.ObjectId);

				if (rel != null)
					relations.Add (rel);
			}


			return relations;
		}

		public ConversationUserRelation GetFirstRelation ()
		{
			foreach (var l in _list) {
				foreach(var v in l.items)
					return v;
			}

			return null;
		}

		public void SetTagsFilter (string[] tags) {
			_tags = tags;
		}

		public void SetUserFilter(string[] userIds)	{
			_userIds = userIds;
		}

		public IEnumerable<string> GetAllTags ()
		{
			var tags = new List<string> ();


			foreach (var cur in  BlrtManager.DataManagers.Default.GetConversationUserRelations (ParseUser.CurrentUser.ObjectId)) {

				if (Contains (cur, false)) {
					foreach (var t in cur.Tags)
						if (!tags.Contains (t))
							tags.Add (t);
				}
			}

			return tags;
		}

		public int TotalNewCount()
		{
			int count = 0;

			foreach (var kvp in _list) {
				for (int i = 0; i < kvp.items.Count; ++i)
					count += kvp.items[i].EstimateUnread ();
			}
			return count;
		}

		public int GetSections()
		{
			return _list.Count;
		}

		public string NameOfSection(int section)
		{
			if(_list [section].displayName)
				return _list [section].name;
			return "";
		}

		public int GetRowsInSection(int section)
		{
			return _list [section].items.Count;
		}

		public NSIndexPath GetIndexPath(ConversationUserRelation relation)
		{
			foreach (var kvp in _list) {
				if (kvp.items.Contains (relation)) {

					int k = _list.IndexOf(kvp);

					return NSIndexPath.FromRowSection (
						kvp.items.IndexOf(relation),
						k
					);
				}			
			}

			return null;
		}

		public ConversationUserRelation GetRelation(NSIndexPath indexPath)
		{
			return GetRelation(indexPath.Section, indexPath.Row);
		}
		public ConversationUserRelation GetRelation(int section, int row)
		{
			return _list [section].items[row];
		}


		private bool Contains(ConversationUserRelation cur, bool applyFilters)
		{
			bool showArchive = BlrtInboxQueryType.Archive == Type;
			bool blrtAuthor = true;


			if (!cur.OnParse && !ShowCached)
				return false;

			//if there is no bond, and the current user didnt create it
			if ( cur.Conversation == null )
				return false;

			if ( cur.Archive != showArchive )
				return false;

			if (applyFilters) {

				if (_tags != null && _tags.Length > 0) {
					bool passed = false;

					foreach (var t in _tags) {
						if (cur.Tags.Contains (t)) {
							passed = true;
							break;
						}
					}

					if (!passed)
						return false;
				}
				// TODO: filter by people disabled
				/*
				if (_userIds != null && _userIds.Length > 0) {
					bool passed = false;

					foreach (var u in _userIds) {
						if (cur.UserIds.Contains (u)) {
							passed = true;
							break;
						}
					}

					if (!passed)
						return false;
				}
				*/
			}
			if (!showArchive) {
				if (blrtAuthor) {
					if (!cur.Conversation.CreatedByCurrentUser)
						return false;
				} else {
					if (cur.Conversation.CreatedByCurrentUser && !cur.Conversation.HasConversationBegan)
						return false;
				}
			}


			return true;
		}

		public BlrtData.Query.BlrtQueryItem GetQuery(int skip = 0)
		{
			return BlrtData.Query.BlrtInboxQuery.RunQuery(Type, skip);
		}

		public bool HasQuery ()
		{
			return BlrtData.Query.BlrtInboxQuery.HasQuery(Type, 0);
		}

		public bool ShowFlagged(int section)
		{
			return (_list [section].showFlagged);
		}
	}
}

