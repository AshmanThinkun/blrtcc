using System;
using CoreGraphics;
using UIKit;
using Foundation;
using System.Collections.Generic;

namespace BlrtiOS.Overlay.Tooltips
{
	public class TooltipSectionView : UIView, ITooltipSectionComposite
	{
		public const float DEFAULT_SPACING = 10f;
		public const float DEFAULT_HORIZONTAL_PADDING = 0f;
		public const float DEFAULT_VERTICAL_PADDING = 0f;
		public const SectionOrientation DEFAULT_ORIENTATION = SectionOrientation.Horizontal;
		public const SectionAlignment DEFAULT_ALIGNMENT = SectionAlignment.Center;
		public const float DEFAULT_SEPARATOR_THICKNESS = 1f;
		public readonly UIColor DEFAULT_SEPARATOR_COLOR = UIColor.FromRGB (123, 151, 173);
		private List<UIView> _sections;
		private List<UIView> _sectionBackground;
		private List<UIView> _separators;
		private SectionOrientation _orientation;
		private SectionAlignment _alignment;
		private float _spacing;
		private float _horizontalPadding;
		private float _verticalPadding;
		private TooltipSectionViewSeparatorMode _separatorMode;
		private UIColor _separatorColor;
		private float _separatorThickness;

		public TooltipSectionView ()
		{
			_sections = new List<UIView> ();
			_sectionBackground = new List<UIView> ();
			_separators = new List<UIView> ();
			_orientation = DEFAULT_ORIENTATION;
			_alignment = DEFAULT_ALIGNMENT;
			_spacing = DEFAULT_SPACING;
			_horizontalPadding = DEFAULT_HORIZONTAL_PADDING;
			_verticalPadding = DEFAULT_VERTICAL_PADDING;
			_separatorMode = TooltipSectionViewSeparatorMode.AUTO;
			_separatorThickness = DEFAULT_SEPARATOR_THICKNESS;
			_separatorColor = DEFAULT_SEPARATOR_COLOR;
			SetNeedsLayout ();
		}

		public SectionOrientation Orientation
		{
			get {
				return _orientation;
			}
			set {
				if (value != _orientation) {
					_orientation = value;
					SetNeedsLayout ();
				}
			}
		}

		public SectionAlignment Alignment
		{
			get {
				return _alignment;
			}
			set {
				if (value != _alignment) {
					_alignment = value;
					SetNeedsLayout ();
				}
			}
		}

		public float Spacing
		{
			get {
				return _spacing;
			}
			set {
				if (value != _spacing) {
					_spacing = value;
					SetNeedsLayout ();
				}
			}
		}

		public float HorizontalPadding
		{
			get {
				return _horizontalPadding;
			}
			set {
				if (value != _horizontalPadding) {
					_horizontalPadding = value;
					SetNeedsLayout ();
				}
			}
		}

		public float VerticalPadding
		{
			get {
				return _verticalPadding;
			}
			set {
				if (value != _verticalPadding) {
					_verticalPadding = value;
					SetNeedsLayout ();
				}
			}
		}

		public void AddSection (UIView section)
		{
			AddSectionWithoutLayout (section);
			SetNeedsLayout ();
		}

		private void AddSectionWithoutLayout (UIView section)
		{
			if (null == section) {
				throw new ArgumentNullException ("section");
			}
			if (section.Superview == this) {
				var existedIndex = _sections.IndexOf (section);
				if ((existedIndex >= 0) && (existedIndex < _sections.Count)) {
					_sections.RemoveAt (existedIndex);
				}
				_sections.Add (section);
			} else {
				_sections.Add (section);
				var background = new UIView ();
				_sectionBackground.Add (background);
				this.AddSubview (background);
				this.AddSubview (section);
			}
		}

		public void InsertSection (int index, UIView section)
		{
			if (null == section) {
				throw new ArgumentNullException ("section");
			}
			if (section.Superview == this) {
				var existedIndex = _sections.IndexOf (section);
				if ((index >= 0) && (index < _sections.Count)) {
					if (index < existedIndex) {
						_sections.RemoveAt (existedIndex);
						_sections.Insert (index, section);
					} else if (index > existedIndex) {
						_sections.Insert (index, section);
						_sections.RemoveAt (existedIndex);
					} else {
						// equal, do nothing
					}
				} else {
					_sections.Insert (index, section);
					var background = new UIView ();
					_sectionBackground.Insert (index, background);
					this.AddSubview (background);
				}
			} else {
				_sections.Insert (index, section);
				var background = new UIView ();
				_sectionBackground.Insert (index, background);
				this.AddSubview (background);
				this.AddSubview (section);
			}
			SetNeedsLayout ();
		}

		public void RemoveSection (UIView section)
		{
			if (null == section) {
				throw new ArgumentNullException ("section");
			}
			if (section.Superview == this) {
				section.RemoveFromSuperview ();
			}
			var index = _sections.IndexOf (section);
			_sectionBackground [index].RemoveFromSuperview ();
			_sectionBackground.RemoveAt (index);
			_sections.RemoveAt (index);
			SetNeedsLayout ();
		}

		public void RemoveAllSections ()
		{
			for (int i = 0; i < _sections.Count; i++) {
				_sectionBackground [i].RemoveFromSuperview ();
				_sections [i].RemoveFromSuperview ();
			}
			_sectionBackground.Clear ();
			_sections.Clear ();
			SetNeedsLayout ();
		}

		public TooltipSectionViewSeparatorMode SeparatorMode
		{
			get {
				return _separatorMode;
			}
			set {
				if (value != _separatorMode) {
					_separatorMode = value;
					SetNeedsLayout ();
				}
			}
		}

		public UIColor SeparatorColor
		{
			get {
				return _separatorColor;
			}
			set {
				if (value != _separatorColor) {
					_separatorColor = value;
					SetNeedsLayout ();
				}
			}
		}

		public float SeparatorThickness
		{
			get {
				return _separatorThickness;
			}
			set {
				if (value != _separatorThickness) {
					_separatorThickness = value;
					SetNeedsLayout ();
				}
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			return CalculateSubviews (false);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			CalculateSubviews (true);
		}

		private CGSize CalculateSubviews(bool setSubviews)
		{
			if (_sections.Count > 0) {
				switch (Orientation) {
				case SectionOrientation.Horizontal:
					return CalculateSubviewsHorizontally (setSubviews);
				case SectionOrientation.Vertical:
					return CalculateSubviewsVertically (setSubviews);
				default:
					break;
				}
			}
			return new CGSize (HorizontalPadding * 2, VerticalPadding * 2);
		}

		private CGSize CalculateSubviewsHorizontally(bool setSubviews)
		{
			nfloat maxHeight = 0;
			nfloat nextX = HorizontalPadding;
			nfloat nextBackgroundX = 0;
			var listX = new List<nfloat> (_sections.Count);
			var listBackgroundX = new List<nfloat> (_sectionBackground.Count);
			var listBackgroundWidth = new List<nfloat> (_sectionBackground.Count);
			var listHeight = new List<nfloat> (_sections.Count);
			var listSize = new List<CGSize> (_sections.Count);
			for (int i = 0; i < _sections.Count; i++) {
				var thisSectionSize = _sections [i].SizeThatFits (_sections [i].Bounds.Size);
				listX.Add (nextX);
				listBackgroundX.Add (nextBackgroundX);
				nextX += thisSectionSize.Width + Spacing;
				listBackgroundWidth.Add (nextX - Spacing * 0.5f - nextBackgroundX);
				nextBackgroundX = nextX - Spacing * 0.5f;
				if (thisSectionSize.Height > maxHeight) {
					maxHeight = thisSectionSize.Height;
				}
				listHeight.Add (thisSectionSize.Height);
				listSize.Add (thisSectionSize);
			}
			if (setSubviews) {
				ClearSeparators ();
				listBackgroundWidth [listBackgroundWidth.Count - 1] += Spacing * 0.5f;
				var listY = AlignLineSegments (maxHeight, listHeight, Alignment);
				for (int i = 0; i < _sections.Count; i++) {
					_sectionBackground[i].Frame = new CGRect (listBackgroundX [i], 0, listBackgroundWidth [i], maxHeight + VerticalPadding * 2);
					_sections [i].Frame = new CGRect (new CGPoint (listX [i], listY [i] + VerticalPadding), listSize[i]);
					_sectionBackground [i].BackgroundColor = _sections [i].BackgroundColor;
					if ((i > 0) && ((TooltipSectionViewSeparatorMode.ALWAYS == SeparatorMode) 
									|| ((TooltipSectionViewSeparatorMode.AUTO == SeparatorMode) && IsColorsRGBAEqual (_sectionBackground [i].BackgroundColor, _sections [i - 1].BackgroundColor)))) {
						var separator = new UIView (new CGRect (_sectionBackground [i].Frame.Location, new CGSize (SeparatorThickness, _sectionBackground [i].Frame.Height))) {
							BackgroundColor = SeparatorColor
						};
						_separators.Add (separator);
						AddSubview (separator);
					}
				}
			}
			return new CGSize (nextX - Spacing + HorizontalPadding, maxHeight + VerticalPadding * 2);
		}

		private CGSize CalculateSubviewsVertically(bool setSubviews)
		{
			nfloat maxWidth = 0;
			nfloat nextY = VerticalPadding;
			nfloat nextBackgroundY = 0;
			var listY = new List<nfloat> (_sections.Count);
			var listBackgroundY = new List<nfloat> (_sectionBackground.Count);
			var listBackgroundHeight = new List<nfloat> (_sectionBackground.Count);
			var listWidth = new List<nfloat> (_sections.Count);
			var listSize = new List<CGSize> (_sections.Count);
			for (int i = 0; i < _sections.Count; i++) {
				var thisSectionSize = _sections [i].SizeThatFits (_sections [i].Bounds.Size);
				listY.Add (nextY);
				listBackgroundY.Add (nextBackgroundY);
				nextY += thisSectionSize.Height + Spacing;
				listBackgroundHeight.Add (nextY - Spacing * 0.5f - nextBackgroundY);
				nextBackgroundY = nextY - Spacing * 0.5f;
				if (thisSectionSize.Width > maxWidth) {
					maxWidth = thisSectionSize.Width;
				}
				listWidth.Add (thisSectionSize.Width);
				listSize.Add (thisSectionSize);
			}
			if (setSubviews) {
				ClearSeparators ();
				listBackgroundHeight [listBackgroundHeight.Count - 1] += Spacing * 0.5f;
				var listX = AlignLineSegments (maxWidth, listWidth, Alignment);
				for (int i = 0; i < _sections.Count; i++) {
					_sectionBackground[i].Frame = new CGRect (0, listBackgroundY [i], maxWidth + HorizontalPadding * 2, listBackgroundHeight [i]);
					_sections [i].Frame = new CGRect (new CGPoint (listX [i] + HorizontalPadding, listY [i]), listSize[i]);
					_sectionBackground [i].BackgroundColor = _sections [i].BackgroundColor;
					if ((i > 0) && ((TooltipSectionViewSeparatorMode.ALWAYS == SeparatorMode) 
									|| ((TooltipSectionViewSeparatorMode.AUTO == SeparatorMode) && IsColorsRGBAEqual (_sectionBackground [i].BackgroundColor, _sections [i - 1].BackgroundColor)))) {
						var separator = new UIView (new CGRect (_sectionBackground [i].Frame.Location, new CGSize (_sectionBackground [i].Frame.Width, SeparatorThickness))) {
							BackgroundColor = SeparatorColor
						};
						_separators.Add (separator);
						AddSubview (separator);
					}
				}
			}
			return new CGSize (maxWidth + HorizontalPadding * 2, nextY - Spacing + VerticalPadding);
		}

		private void ClearSeparators()
		{
			foreach (var separator in _separators) {
				separator.RemoveFromSuperview ();
			}
			_separators.Clear ();
		}

		public UIView this [int index]
		{ 
			get {
				return _sections [index];
			}
		}

		public static List<nfloat> AlignLineSegments (nfloat maxLength, List<nfloat> lineLength, SectionAlignment alignment)
		{
			if ((null == lineLength) || (lineLength.Count < 1)) {
				return null;
			}
			var lintStartPoint = new List<nfloat> (lineLength.Count);
			switch (alignment) {
			case SectionAlignment.LeftOrUpperBorder:
				for (int i = 0; i < lineLength.Count; i++) {
					lintStartPoint.Add (0);
				}
				break;
			case SectionAlignment.Center:
				for (int i = 0; i < lineLength.Count; i++) {
					lintStartPoint.Add ((maxLength - lineLength [i]) * 0.5f);
				}
				break;
			case SectionAlignment.RightOrLowerBoarder:
				for (int i = 0; i < lineLength.Count; i++) {
					lintStartPoint.Add (maxLength - lineLength [i]);
				}
				break;
			default:
				return null;
			}
			return lintStartPoint;
		}

		public static bool IsColorsRGBAEqual (UIColor color1, UIColor color2)
		{
			if ((null == color1) || (null == color2)) {
				return (color1 == color2);
			} else {
				nfloat R1, G1, B1, A1, R2, G2, B2, A2;
				color1.GetRGBA (out R1, out G1, out B1, out A1);
				color2.GetRGBA (out R2, out G2, out B2, out A2);
				return ((R1 == R2) && (B1 == B2) && (G1 == G2) && (A1 == A2));
			}
		}
	}

	public enum TooltipSectionViewSeparatorMode
	{
		NEVER,
		AUTO,
		ALWAYS
	}
}

