using System;
using UIKit;

namespace BlrtiOS.Overlay.Tooltips
{

	/// <summary>
	/// Interface of a composite of tooltip sections.
	/// </summary>
	public interface ITooltipSectionComposite
	{
		/// <summary>
		/// Gets or sets the orientation of tooltip sections.
		/// </summary>
		/// <value>The orientation.</value>
		SectionOrientation Orientation { get; set; }

		/// <summary>
		/// Gets or sets the alignment of the tooltip sections.
		/// </summary>
		/// <value>The alignment.</value>
		SectionAlignment Alignment { get; set; }

		/// <summary>
		/// Adds new section to this composite.
		/// </summary>
		/// <param name="section">new section.</param>
		void AddSection (UIView section);

		/// <summary>
		/// Inserts a section to this composite.
		/// </summary>
		/// <param name="index">position to insert.</param>
		/// <param name="section">the section.</param>
		void InsertSection (int index, UIView section);

		/// <summary>
		/// Removes a section from the composite.
		/// </summary>
		/// <param name="section">Section.</param>
		void RemoveSection (UIView section);

		/// <summary>
		/// Removes all sections.
		/// </summary>
		void RemoveAllSections ();

		/// <summary>
		/// Gets the <see cref="OverLayController.ITooltipSectionComposite"/> at the specified index.
		/// </summary>
		/// <param name="index">Index.</param>
		UIView this [int index] { get; }
	}

	/// <summary>
	/// the enum represents orientation of tooltip sections.
	/// </summary>
	public enum SectionOrientation
	{
		Vertical,
		Horizontal
	}

	/// <summary>
	/// the enum represents alignment of all the sections in a composite.
	/// </summary>
	public enum SectionAlignment
	{
		LeftOrUpperBorder,
		Center,
		RightOrLowerBoarder
	}
}

