using System;
using CoreGraphics;
using UIKit;
using Foundation;

namespace BlrtiOS.Overlay.Tooltips
{
	/// <summary>
	/// A tooltip with a title, a subtitle, and an image. Any of them can be null
	/// </summary>
	public class ImageTitleTooltip : ITooltip
	{
		public const float DEFAULT_TITLE_FONT_SIZE = 28f;
		public const float DEFAULT_SUBTITLE_FONT_SIZE = 18f;
		public const float DEFAULT_MAX_WIDTH = float.MaxValue;
		public const float DEFAULT_MAX_HEIGHT = float.MaxValue;
		public const float DEFAULT_TITLE_SUBTITLE_SPACING = 5f;
		public const float DEFAULT_IMAGE_TEXT_SPACING = 10f;
		public const float DEFAULT_HORIZONTAL_PADDING = 40f;
		public const float DEFAULT_VERTICAL_PADDING = 26f;
		private Action<UIView, UIView, UIView> _layoutTooltip;
		private UIView _titleView;
		private UIView _subtitleView;
		private CGSize _tooltipSize;
		private CGSize? _contentSize;

		public ImageTitleTooltip (Action<UIView, UIView, UIView> layoutTooltipDelegate = null)
		{
			_layoutTooltip = layoutTooltipDelegate;
			_titleView = null;
			_subtitleView = null;
			ImageView = null;
			ImagePosition = ImageTitleTooltipImagePosition.LeftOfSubtitle;
			MaxWidth = DEFAULT_MAX_WIDTH;
			MaxHeight = DEFAULT_MAX_HEIGHT;
			TitleSubtitleSpacing = DEFAULT_TITLE_SUBTITLE_SPACING;
			ImageTextSpacing = DEFAULT_IMAGE_TEXT_SPACING;
			TitleStyle = new UIStringAttributes () {
			};
			SubtitleStyle = new UIStringAttributes () {
			};
			IsTouchReceiver = true;
			ScrollShadowEnabled = true;
			HorizontalPadding = DEFAULT_HORIZONTAL_PADDING;
			VerticalPadding = DEFAULT_VERTICAL_PADDING;
		}

		/// <summary>
		/// Gets or sets the background color of the tooltip.
		/// </summary>
		/// <value>The background color of the tooltip.</value>
		public UIColor BackgroundColor { get; set; }

		/// <summary>
		/// Gets the size of the tooltip.
		/// </summary>
		/// <value>The size of the tooltip.</value>
		public CGSize TooltipSize
		{
			get {
				return _tooltipSize;
			}
		}

		/// <summary>
		/// Gets the size of actual content in tooltip.
		/// </summary>
		/// <value>The size of the actual content in tooltip.</value>
		public CGSize? ContentSize
		{
			get {
				return _contentSize;
			}
		}

		public bool IsTouchReceiver { get; set; }

		public bool ScrollShadowEnabled { get; set; }

		public float HorizontalPadding { get; set; }

		public float VerticalPadding { get; set; }

		/// <summary>
		/// Gets or sets the image.
		/// </summary>
		/// <value>The image.</value>
		public UIImage Image
		{
			get {
				if (null == ImageView) {
					return null;
				} else {
					return ImageView.Image;
				}
			}

			set {
				if (null == ImageView) {
					ImageView = new UIImageView (value) {
						ContentMode = UIViewContentMode.Center
					};
				} else if (value != ImageView.Image) {
					if (null == value) {
						ImageView = null;
					} else {
						ImageView.Image = value;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the image tint color.
		/// </summary>
		/// <value>The image tint color.</value>
		public UIColor ImageTintColor
		{
			get {
				if (null == ImageView) {
					return null;
				} else {
					return ImageView.TintColor;
				}
			}

			set {
				if (null == ImageView) {
					throw new InvalidOperationException ("Can not set tint color if an image is not set!");
				} else {
					ImageView.TintColor = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the image view.
		/// </summary>
		/// <value>The image view.</value>
		public UIImageView ImageView { get; set; }

		/// <summary>
		/// Gets or sets the max width of the tooltip.
		/// </summary>
		/// <value>The max width of the tooltip.</value>
		public float MaxWidth { get; set; }

		/// <summary>
		/// Gets or sets the max height of the tooltip.
		/// </summary>
		/// <value>The max height of the tooltip.</value>
		public float MaxHeight { get; set; }

		/// <summary>
		/// Gets or sets the spacing between title and subtitle.
		/// </summary>
		/// <value>The spacing between title and subtitle.</value>
		public float TitleSubtitleSpacing { get; set; }

		/// <summary>
		/// Gets or sets the spacing between image and text.
		/// </summary>
		/// <value>The spacing between image and text.</value>
		public float ImageTextSpacing { get; set; }

		/// <summary>
		/// Gets or sets the image position relative to the title/subtitle.
		/// </summary>
		/// <value>The image position.</value>
		public ImageTitleTooltipImagePosition ImagePosition { get; set; }

		/// <summary>
		/// Gets or sets the title with plain text.
		/// </summary>
		/// <value>The plain text in title.</value>
		public string Title {
			get {
				return AttributedTitle.Value;
			}
			set {
				if (null == value) {
					AttributedTitle = null;
				} else {
					if (null == TitleStyle) {
						AttributedTitle = new NSMutableAttributedString (value); 
					} else {
						AttributedTitle = new NSMutableAttributedString (value, TitleStyle); 
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the subtitle with plain text.
		/// </summary>
		/// <value>The plain text in subtitle.</value>
		public string Subtitle { 
			get { 
				return AttributedSubtitle.Value;
			} 
			set {
				if (null == value) {
					AttributedSubtitle = null;
				} else {
					if (null == SubtitleStyle) {
						AttributedSubtitle = new NSMutableAttributedString (value); 
					} else {
						AttributedSubtitle = new NSMutableAttributedString (value, SubtitleStyle); 
					}
				}
			} 
		}

		/// <summary>
		/// Gets or sets the style of the title if it is set by plain text (<see cref="OverLayController.ImageTitleTooltip.Title"/>).
		/// </summary>
		/// <value>The style of the title.</value>
		public UIStringAttributes TitleStyle { get; set; }

		/// <summary>
		/// Gets or sets the style of the title if it is set by plain text (<see cref="OverLayController.ImageTitleTooltip.Subtitle"/>)..
		/// </summary>
		/// <value>The subtitle style.</value>
		public UIStringAttributes SubtitleStyle { get; set; }

		/// <summary>
		/// Gets or sets the attributed title text.
		/// </summary>
		/// <value>The attributed title text.</value>
		public NSMutableAttributedString AttributedTitle { get; set; }

		/// <summary>
		/// Gets or sets the attributed subtitle text.
		/// </summary>
		/// <value>The attributed subtitle text.</value>
		public NSMutableAttributedString AttributedSubtitle { get; set; }

		/// <summary>
		/// Layouts the tooltip inner views in the content view.
		/// delegate for the IOverlayManager
		/// </summary>
		/// <param name="contentView">Content view of a tooltip.</param>
		public void LayoutTooltip (UIView contentView) {
			if (null != contentView) {
				RebuildViews (contentView);
			}
			if (null != _layoutTooltip) {
				_layoutTooltip (contentView, _titleView, _subtitleView);
			}
		}

		/// <summary>
		/// Rebuilds the views of image, title and subtitle.
		/// </summary>
		/// <param name="contentView">Content view.</param>
		private void RebuildViews(UIView contentView)
		{
			if (null != _titleView && null != _titleView.Superview) {
				_titleView.RemoveFromSuperview ();
			}
			_titleView = null;
			if (null != _subtitleView && null != _subtitleView.Superview) {
				_subtitleView.RemoveFromSuperview ();
			}
			_subtitleView = null;
			switch (ImagePosition) {
			case ImageTitleTooltipImagePosition.LeftOfAll:
			case ImageTitleTooltipImagePosition.RightOfAll:
				RebuildViews_ImageForAll (contentView);
				break;
			case ImageTitleTooltipImagePosition.LeftOfTitle:
			case ImageTitleTooltipImagePosition.RightOfTitle:
				RebuildViews_ImageForTitle (contentView);
				break;
			case ImageTitleTooltipImagePosition.LeftOfSubtitle:
			case ImageTitleTooltipImagePosition.RightOfSubtitle:
				RebuildViews_ImageForSubtitle (contentView);
				break;
			default:
				break;
			}
		}

		/// <summary>
		/// called by RebuildViews if the position is LeftOfAll / RightOfAll
		/// </summary>
		/// <param name="contentView">Content view.</param>
		private void RebuildViews_ImageForAll(UIView contentView)
		{
			NSMutableAttributedString newAttrText = null;
			if (null != AttributedTitle && AttributedTitle.Length > 0) {
				newAttrText = new NSMutableAttributedString (AttributedTitle);
				if (null != AttributedSubtitle && AttributedSubtitle.Length > 0) {
					newAttrText.Append (new NSAttributedString (Environment.NewLine));
					var spacingAttr = new UIStringAttributes () {
						ParagraphStyle = new NSMutableParagraphStyle () {
							MinimumLineHeight = 0,
							MaximumLineHeight = (TitleSubtitleSpacing > 0) ? TitleSubtitleSpacing : float.Epsilon,
							LineSpacing = 0,
							ParagraphSpacing = 0,
							ParagraphSpacingBefore = 0
						}
					};
					newAttrText.Append (new NSAttributedString (Environment.NewLine, spacingAttr)); // empty line for the spacing between title and subtitle
					newAttrText.Append (AttributedSubtitle);
				}
			}
			// create one single ImagedLabelView with the image, title and subtitle in it
			// subtitle would be null
			_titleView = new ImagedLabelView () {
				ImageView = this.ImageView,
				AttributedText = newAttrText,
				ImagePosition = (ImageTitleTooltipImagePosition.LeftOfAll == ImagePosition) ? ImagedLabelViewImagePosition.LeftToText : ImagedLabelViewImagePosition.RightToText,
				Spacing = this.ImageTextSpacing
			};
			contentView.AddSubview (_titleView);
			var titleSize = _titleView.SizeThatFits (new CGSize (MaxWidth, MaxHeight));
			_titleView.Frame = new CGRect (0, 0, titleSize.Width, titleSize.Height);
			if (titleSize.Width > MaxWidth || titleSize.Height > MaxHeight) {
				_tooltipSize = new CGSize (NMath.Min (titleSize.Width, MaxWidth), NMath.Min (titleSize.Height, MaxHeight));
				_contentSize = titleSize;
			} else {
				_tooltipSize = titleSize;
				_contentSize = null;
			}
		}

		/// <summary>
		/// called by RebuildViews if the position is LeftOfTitle / RightOfTitle
		/// </summary>
		/// <param name="contentView">Content view.</param>
		private void RebuildViews_ImageForTitle(UIView contentView)
		{
			// title and image in one ImagedLabelView
			_titleView = new ImagedLabelView () {
				ImageView = this.ImageView,
				AttributedText = this.AttributedTitle,
				ImagePosition = (ImageTitleTooltipImagePosition.LeftOfTitle == ImagePosition) ? ImagedLabelViewImagePosition.LeftToText : ImagedLabelViewImagePosition.RightToText,
				Spacing = this.ImageTextSpacing
			};
			contentView.AddSubview (_titleView);
			var titleSize = _titleView.SizeThatFits (new CGSize (MaxWidth, MaxHeight));
			// subtitle in a UILable
			_subtitleView = new UILabel () {
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap
			};
			if (null != AttributedSubtitle) {
				(_subtitleView as UILabel).AttributedText = AttributedSubtitle;
			}
			var subtitleSize = _subtitleView.SizeThatFits (new CGSize (MaxWidth, MaxHeight - (0 != titleSize.Height ? TitleSubtitleSpacing + titleSize.Height : 0)));
			nfloat titleX, subtitleX;
			var entireWidth = ImagedLabelView.AlignLineSegments (titleSize.Width, subtitleSize.Width, ImagedLabelViewAlignment.Center, out titleX, out subtitleX);
			_titleView.Frame = new CGRect (new CGPoint (titleX, 0), titleSize);
			_subtitleView.Frame = new CGRect (new CGPoint (subtitleX, titleSize.Height + (0 != titleSize.Height ? TitleSubtitleSpacing : 0)), subtitleSize);
			contentView.AddSubview (_subtitleView);
			nfloat entireHeight = titleSize.Height + subtitleSize.Height + ((0 != titleSize.Height && 0 != subtitleSize.Height) ? TitleSubtitleSpacing : 0);
			if (entireWidth > MaxWidth || entireHeight > MaxHeight) {
				_tooltipSize = new CGSize (NMath.Min (entireWidth, MaxWidth), NMath.Min (entireHeight, MaxHeight));
				_contentSize = new CGSize (entireWidth, entireHeight);
			} else {
				_tooltipSize = new CGSize (entireWidth, entireHeight);
				_contentSize = null;
			}
		}

		/// <summary>
		/// called by RebuildViews if the position is LeftOfSubtitle / RightOfSubtitle
		/// </summary>
		/// <param name="contentView">Content view.</param>
		private void RebuildViews_ImageForSubtitle(UIView contentView)
		{
			// title in a UILable
			_titleView = new UILabel () {
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap
			};
			if (null != AttributedTitle) {
				(_titleView as UILabel).AttributedText = AttributedTitle;
			}
			contentView.AddSubview (_titleView);
			var titleSize = _titleView.SizeThatFits (new CGSize (MaxWidth, MaxHeight));
			// subtitle and image in a ImagedLabelView
			_subtitleView = new ImagedLabelView () {
				ImageView = this.ImageView,
				AttributedText = this.AttributedSubtitle,
				ImagePosition = (ImageTitleTooltipImagePosition.LeftOfSubtitle == ImagePosition) ? ImagedLabelViewImagePosition.LeftToText : ImagedLabelViewImagePosition.RightToText,
				Spacing = this.ImageTextSpacing
			};
			contentView.AddSubview (_subtitleView);
			var subtitleSize = _subtitleView.SizeThatFits (new CGSize (MaxWidth, MaxHeight - (0 != titleSize.Height ? TitleSubtitleSpacing + titleSize.Height : 0)));
			nfloat titleX, subtitleX;
			var entireWidth = ImagedLabelView.AlignLineSegments (titleSize.Width, subtitleSize.Width, ImagedLabelViewAlignment.Center, out titleX, out subtitleX);
			_titleView.Frame = new CGRect (new CGPoint (titleX, 0), titleSize);
			_subtitleView.Frame = new CGRect (new CGPoint (subtitleX, titleSize.Height + (0 != titleSize.Height ? TitleSubtitleSpacing : 0)), subtitleSize);
			nfloat entireHeight = subtitleSize.Height + titleSize.Height + ((0 != subtitleSize.Height && 0 != titleSize.Height) ? TitleSubtitleSpacing : 0);
			if (entireWidth > MaxWidth || entireHeight > MaxHeight) {
				_tooltipSize = new CGSize (NMath.Min (entireWidth, MaxWidth), NMath.Min (entireHeight, MaxHeight));
				_contentSize = new CGSize (entireWidth, entireHeight);
			} else {
				_tooltipSize = new CGSize (entireWidth, entireHeight);
				_contentSize = null;
			}
		}
	}

	public enum ImageTitleTooltipImagePosition
	{
		LeftOfAll,
		LeftOfTitle,
		LeftOfSubtitle,
		RightOfAll,
		RightOfTitle,
		RightOfSubtitle
	}
}

