using System;
using CoreGraphics;
using UIKit;
using Foundation;

namespace BlrtiOS.Overlay.Tooltips
{
	/// <summary>
	/// A tooltip that contains multiple tooltip sections
	/// </summary>
	public class MultiSectionTooltip : ITooltipSectionComposite, ITooltip
	{
		public const float DEFAULT_HORIZONTAL_PADDING = 25f;
		public const float DEFAULT_VERTICAL_PADDING = 25f;
		private TooltipSectionView _baseView;
		private Action<UIView, MultiSectionTooltip> _layoutTooltip;
		private CGSize _tooltipSize;
		private CGSize? _contentSize;

		public MultiSectionTooltip (Action<UIView, MultiSectionTooltip> layoutTooltipDelegate = null)
		{
			_layoutTooltip = layoutTooltipDelegate;
			_baseView = new TooltipSectionView ();
			MaxWidth = float.MaxValue;
			MaxHeight = float.MaxValue;
			IsTouchReceiver = true;
			ScrollShadowEnabled = true;
			HorizontalPadding = DEFAULT_HORIZONTAL_PADDING;
			VerticalPadding = DEFAULT_VERTICAL_PADDING;
		}

		/// <summary>
		/// Gets or sets the orientation of tooltip sections.
		/// </summary>
		/// <value>The orientation.</value>
		public SectionOrientation Orientation
		{
			get {
				return _baseView.Orientation;
			}

			set {
				_baseView.Orientation = value;
			}
		}

		/// <summary>
		/// Gets or sets the alignment of the tooltip sections.
		/// </summary>
		/// <value>The alignment.</value>
		public SectionAlignment Alignment
		{
			get {
				return _baseView.Alignment;
			}

			set {
				_baseView.Alignment = value;
			}
		}

		/// <summary>
		/// Adds new section to this tooltip.
		/// </summary>
		/// <param name="section">new section.</param>
		public void AddSection (UIView section)
		{
			_baseView.AddSection (section);
		}

		/// <summary>
		/// Inserts a section to this tooltip.
		/// </summary>
		/// <param name="index">position to insert.</param>
		/// <param name="section">the section.</param>
		public void InsertSection (int index, UIView section)
		{
			_baseView.InsertSection (index, section);
		}

		/// <summary>
		/// Removes a section from the tooltip.
		/// </summary>
		/// <param name="section">Section.</param>
		public void RemoveSection (UIView section)
		{
			_baseView.RemoveSection (section);
		}

		/// <summary>
		/// Removes all sections.
		/// </summary>
		public void RemoveAllSections ()
		{
			_baseView.RemoveAllSections ();
		}

		/// <summary>
		/// Gets the tooltip section at the specified index.
		/// </summary>
		/// <param name="index">Index.</param>
		public UIView this [int index]
		{
			get {
				return _baseView [index];
			}
		}

		/// <summary>
		/// Gets the TooltipSectionView.
		/// </summary>
		/// <value>The TooltipSectionView.</value>
		public TooltipSectionView SectionView
		{
			get {
				return _baseView;
			}
		}

		/// <summary>
		/// Gets or sets the max width of the tooltip.
		/// </summary>
		/// <value>The max width of the tooltip.</value>
		public float MaxWidth { get; set; }

		/// <summary>
		/// Gets or sets the max height of the tooltip.
		/// </summary>
		/// <value>The max height of the tooltip.</value>
		public float MaxHeight { get; set; }

		/// <summary>
		/// Gets or sets the spacing between 2 sections.
		/// </summary>
		/// <value>The spacing.</value>
		public float Spacing
		{
			get {
				return _baseView.Spacing;
			}

			set {
				_baseView.Spacing = value;
			}
		}

		/// <summary>
		/// Gets or sets the background color of the tooltip.
		/// </summary>
		/// <value>The background color of the tooltip.</value>
		public UIColor BackgroundColor { get; set; }

		/// <summary>
		/// Gets the size of the tooltip.
		/// </summary>
		/// <value>The size of the tooltip.</value>
		public CGSize TooltipSize
		{
			get {
				return _tooltipSize;
			}
		}

		/// <summary>
		/// Gets the size of actual content in tooltip.
		/// </summary>
		/// <value>The size of the actual content in tooltip.</value>
		public CGSize? ContentSize
		{
			get {
				return _contentSize;
			}
		}

		public bool IsTouchReceiver { get; set; }

		public bool ScrollShadowEnabled { get; set; }

		public float HorizontalPadding { get; set; }

		public float VerticalPadding { get; set; }

		/// <summary>
		/// Layouts the tooltip inner views in the content view.
		/// delegate for the IOverlayManager
		/// </summary>
		/// <param name="contentView">Content view of a tooltip.</param>
		public void LayoutTooltip (UIView contentView) 
		{
			if (null != contentView) {
				if (contentView != _baseView.Superview) {
					contentView.AddSubview (_baseView);
				}
				var baseViewSize = _baseView.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
				_baseView.Frame = new CGRect (CGPoint.Empty, baseViewSize);
				if ((baseViewSize.Width > MaxWidth) || (baseViewSize.Height > MaxHeight)) {
					_tooltipSize = new CGSize (NMath.Min (baseViewSize.Width, MaxWidth), NMath.Min (baseViewSize.Height, MaxHeight));
					_contentSize = baseViewSize;
				} else {
					_tooltipSize = baseViewSize;
					_contentSize = null;
				}
			}
			if (null != _layoutTooltip) {
				_layoutTooltip (contentView, this);
			}
		}
	}
}

