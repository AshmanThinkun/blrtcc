using System;
using CoreGraphics;
using UIKit;
using Foundation;

namespace BlrtiOS.Overlay.Tooltips
{
	/// <summary>
	/// A basic tooltip that contains only text
	/// </summary>
	public class TextTooltip : ITooltip
	{
		public const float DEFAULT_HORIZONTAL_PADDING = 10f;
		public const float DEFAULT_VERTICAL_PADDING = 15f;
		private Action<UIView, TextTooltip> _layoutTooltip;
		private CGSize _tooltipSize;
		private CGSize? _contentSize;
		private UILabel _labelView;

		public TextTooltip (Action<UIView, TextTooltip> layoutTooltipDelegate = null)
		{
			_layoutTooltip = layoutTooltipDelegate;
			_labelView = new UILabel () {
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0)	// transparent color
			};
			MaxWidth = 90;
			MaxHeight = float.MaxValue;
			PlainTextStyle = null;
			AttributedText = new NSMutableAttributedString ();
			IsTouchReceiver = true;
			ScrollShadowEnabled = true;
			HorizontalPadding = DEFAULT_HORIZONTAL_PADDING;
			VerticalPadding = DEFAULT_VERTICAL_PADDING;
		}

		public UIStringAttributes PlainTextStyle { get; set; }

		/// <summary>
		/// Gets or sets the text in the tooltip.
		/// The ContentSize will be changed to fit the text and the max width/height
		/// </summary>
		/// <value>The text.</value>
		public string Text { 
			get {
				return AttributedText.Value;
			}

			set {
				if (null == PlainTextStyle) {
					AttributedText = new NSMutableAttributedString (value);
				} else {
					AttributedText = new NSMutableAttributedString (value, PlainTextStyle);
				}
			}
		}

		/// <summary>
		/// Gets or sets the attributed text in the tooltip.
		/// The ContentSize will be changed to fit the text and the max width/height
		/// </summary>
		/// <value>The attributed text.</value>
		public NSMutableAttributedString AttributedText { get; set; }

		/// <summary>
		/// Gets or sets the max width of the tooltip.
		/// </summary>
		/// <value>The max width of the text.</value>
		public float MaxWidth { get; set; }

		/// <summary>
		/// Gets or sets the max height of the text.
		/// </summary>
		/// <value>The max height of the text.</value>
		public float MaxHeight { get; set; }

		/// <summary>
		/// Gets the UILabel that holds the text.
		/// </summary>
		/// <value>The UILabel.</value>
		public UILabel LabelView {
			get {
				return _labelView;
			}
		}

		public UITextAlignment TextAlignment {
			get {
				return _labelView.TextAlignment;
			}

			set {
				_labelView.TextAlignment = value;
			}
		}

		public virtual UIColor BackgroundColor { get; set; }

		public virtual CGSize TooltipSize { 
			get {
				return _tooltipSize;
			}
		}

		public virtual CGSize? ContentSize {
			get {
				return _contentSize;
			}
		}

		public virtual bool IsTouchReceiver { get; set; }

		public float HorizontalPadding { get; set; }

		public float VerticalPadding { get; set; }

		public bool ScrollShadowEnabled { get; set; }

		public virtual void LayoutTooltip (UIView contentView)
		{
			_labelView.AttributedText = AttributedText;
			var textFitSize = _labelView.SizeThatFits (new CGSize (MaxWidth, MaxHeight));
			_labelView.Frame = new CGRect (0, 0, textFitSize.Width, textFitSize.Height);
			if ((textFitSize.Width > MaxWidth) || (textFitSize.Height > MaxHeight)) {	// enable scroll when the width/height is bigger than max width/height
				_tooltipSize = new CGSize (NMath.Min (textFitSize.Width, MaxWidth), NMath.Min (textFitSize.Height, MaxHeight));
				_contentSize = textFitSize;
			} else {
				_tooltipSize = textFitSize;
				_contentSize = null;
			}
			if (contentView != _labelView.Superview) {
				if (null != _labelView.Superview) {
					_labelView.RemoveFromSuperview ();
				}
				if (null != contentView) {
					contentView.AddSubview (_labelView);
				}
			}
			if (null != _layoutTooltip) {
				_layoutTooltip (contentView, this);
			}
		}
	}
}

