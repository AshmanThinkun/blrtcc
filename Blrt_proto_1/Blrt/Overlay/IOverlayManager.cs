using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.Overlay
{
	/// <summary>
	/// interface of a manager that can handle multiple tooltips overlaid on a view
	/// </summary>
	public interface IOverlayManager
	{
		/// <summary>
		/// Shows a tooltip.
		/// </summary>
		/// <param name="view">target of the tooltip</param>
		/// <param name="tooltip">the tooltip.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		/// <param name="position">tooltip position relative to the target.</param>
		void ShowTooltip (UIView view, ITooltip tooltip, bool animated, TooltipPosition position = TooltipPosition.Auto);

		/// <summary>
		/// Hides the tooltip.
		/// </summary>
		/// <param name="tooltip">the Tooltip.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		void HideTooltip (ITooltip tooltip, bool animated);

		/// <summary>
		/// Hides all the tooltips.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		void HideAllTooltips (bool animated);


		void ShowImage (UIImage image, UIControlContentVerticalAlignment aligment, bool animated);
		void OnOverlaysDismiss(Action action);
	}

	/// <summary>
	/// interface of a tooltip that can be shown by IOverlayManager
	/// </summary>
	public interface ITooltip
	{
		/// <summary>
		/// Gets or sets the background color of the tooltip.
		/// </summary>
		/// <value>The background color of the tooltip.</value>
		UIColor BackgroundColor { get; set; }

		/// <summary>
		/// Gets the size of the tooltip.
		/// </summary>
		/// <value>The size of the tooltip.</value>
		CGSize TooltipSize { get; }

		/// <summary>
		/// Gets the size of actual content in tooltip.
		/// </summary>
		/// <value>The size of the actual content in tooltip.</value>
		CGSize? ContentSize { get; }

		bool IsTouchReceiver { get; }

		/// <summary>
		/// Gets or sets the padding on x direction.
		/// </summary>
		/// <value>The padding on x direction.</value>
		float HorizontalPadding { get; set; }

		/// <summary>
		/// Gets or sets the padding on y direction.
		/// </summary>
		/// <value>The padding on y direction.</value>
		float VerticalPadding { get; set; }

		/// <summary>
		/// Gets a value indicating whether the shadow is enabled when the tooltip can be scrolled.
		/// </summary>
		/// <value><c>true</c> if scroll shadow enabled; otherwise, <c>false</c>.</value>
		bool ScrollShadowEnabled { get; }

		/// <summary>
		/// Layouts the tooltip inner views in the content view.
		/// delegate for the IOverlayManager
		/// </summary>
		/// <param name="contentView">Content view of a tooltip.</param>
		void LayoutTooltip (UIView contentView);






	}

	/// <summary>
	/// enum represents the position of a tooltip relative to its target
	/// </summary>
	public enum TooltipPosition
	{
		AboveTarget = 0,
		RightToTarget,
		BelowTarget,
		LeftToTarget,
		AutoVertical,
		AutoHorizontal,
		Auto
	}
}


