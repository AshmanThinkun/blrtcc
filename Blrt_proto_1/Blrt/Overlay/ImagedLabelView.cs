using System;
using CoreGraphics;
using UIKit;
using Foundation;

namespace BlrtiOS.Overlay
{
	/// <summary>
	/// the view than contains a image and a text
	/// </summary>
	public class ImagedLabelView : UIView
	{
		public const float DEFAULT_SPACING = 10f;
		public const float DEFAULT_HORIZONTAL_PADDING = 0f;
		public const float DEFAULT_VERTICAL_PADDING = 0f;
		public const ImagedLabelViewImagePosition DEFAULT_IMAGE_POSITION = ImagedLabelViewImagePosition.LeftToText;
		public const ImagedLabelViewAlignment DEFAULT_ALIGNMENT = ImagedLabelViewAlignment.Center;
		private UIImageView _imgView;
		private UILabel _labelView;
		private ImagedLabelViewImagePosition _imgPosition;
		private ImagedLabelViewAlignment _alignment;
		private float _spacing;
		private float _horizontalPadding;
		private float _verticalPadding;

		public ImagedLabelView()
		{
			_spacing = DEFAULT_SPACING;
			_horizontalPadding = DEFAULT_HORIZONTAL_PADDING;
			_verticalPadding = DEFAULT_VERTICAL_PADDING;
			_imgView = null;
			_labelView = new UILabel () {
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap
			};
			_imgPosition = DEFAULT_IMAGE_POSITION;
			_alignment = DEFAULT_ALIGNMENT;
			this.AddSubview (_labelView);
			SetNeedsLayout ();
		}

		/// <summary>
		/// try to set the tint color of imgView; successful if _imgView is not null
		/// </summary>
		/// <returns><c>true</c>, if set image tint color was set successfully, <c>false</c> otherwise.</returns>
		/// <param name="imgTintColor">Image tint color.</param>
		private bool trySetImgTintColor (UIColor imgTintColor)
		{
			if ((null != _imgView) && (null != imgTintColor)) {
				_imgView.TintColor = imgTintColor;
				return true;
			}
			return false;
		}

		/// <summary>
		/// Repositions the image view and text view.
		/// </summary>
		private CGSize CalulateSubviews(CGSize maxSize, bool setSubviews)
		{
			var maxWidth = maxSize.Width - HorizontalPadding * 2;
			var maxHeight = maxSize.Height - VerticalPadding * 2;
			maxWidth = maxWidth > 0 ? maxWidth : nfloat.MaxValue;
			maxHeight = maxWidth > 0 ? maxWidth : nfloat.MaxValue;
			if (null != _imgView) {
				nfloat imgX, imgY, textX, textY, entireWidth, entireHeight; // entire width/height of the composite of image and text; not considering the padding
				CGSize textSize;
				switch (_imgPosition) {
				case ImagedLabelViewImagePosition.AboveText:
					textSize = _labelView.SizeThatFits (new CGSize (maxWidth, maxHeight - _spacing - _imgView.Image.Size.Height));
					imgY = 0;
					textY = _imgView.Image.Size.Height + _spacing;
					entireWidth = AlignLineSegments (_imgView.Image.Size.Width, textSize.Width, _alignment, out imgX, out textX);
					entireHeight = textY + textSize.Height;
					break;
				case ImagedLabelViewImagePosition.BelowText:
					textSize = _labelView.SizeThatFits (new CGSize (maxWidth, maxHeight - _spacing - _imgView.Image.Size.Height));
					textY = 0;
					imgY = textSize.Height + _spacing;
					entireWidth = AlignLineSegments (_imgView.Image.Size.Width, textSize.Width, _alignment, out imgX, out textX);
					entireHeight = imgY + _imgView.Image.Size.Height;
					break;
				case ImagedLabelViewImagePosition.LeftToText:
					textSize = _labelView.SizeThatFits (new CGSize (maxWidth - _spacing - _imgView.Image.Size.Width, maxHeight));
					imgX = 0;
					textX = _imgView.Image.Size.Width + _spacing;
					entireHeight = AlignLineSegments (_imgView.Image.Size.Height, textSize.Height, _alignment, out imgY, out textY);
					entireWidth = textX + textSize.Width;
					break;
				case ImagedLabelViewImagePosition.RightToText:
					textSize = _labelView.SizeThatFits (new CGSize (maxWidth - _spacing - _imgView.Image.Size.Width, maxHeight));
					textX = 0;
					imgX = textSize.Width + _spacing;
					entireHeight = AlignLineSegments (_imgView.Image.Size.Height, textSize.Height, _alignment, out imgY, out textY);
					entireWidth = imgX + _imgView.Image.Size.Width;
					break;
				default:
					// same to LeftToText
					textSize = _labelView.SizeThatFits (new CGSize (maxWidth - _spacing - _imgView.Image.Size.Width, maxHeight));
					imgX = 0;
					textX = _imgView.Image.Size.Width + _spacing;
					entireHeight = AlignLineSegments (_imgView.Image.Size.Height, textSize.Height, _alignment, out imgY, out textY);
					entireWidth = textX + textSize.Width;
					break;
				}

				if (setSubviews) {
					_labelView.Frame = new CGRect (textX + HorizontalPadding, textY + VerticalPadding, textSize.Width, textSize.Height);
					_imgView.Frame = new CGRect (new CGPoint (imgX + HorizontalPadding, imgY + VerticalPadding), _imgView.Image.Size);
				}

				return new CGSize (entireWidth + HorizontalPadding * 2, entireHeight + VerticalPadding * 2);

			} else {
				// the user didn't specify a image --> show the text only
				var textSize = _labelView.SizeThatFits (new CGSize (maxWidth, maxHeight));
				if (setSubviews) {
					_labelView.Frame = new CGRect (new CGPoint (HorizontalPadding, VerticalPadding), textSize);
				}
				return new CGSize (textSize.Width + HorizontalPadding * 2, textSize.Height + VerticalPadding * 2);
			}
		}

		/// <summary>
		/// get the start point of 2 line segments when aligning them
		/// </summary>
		/// <returns>the max width of the line segments</returns>
		/// <param name="line1Length">Line1 length.</param>
		/// <param name="line2Length">Line2 length.</param>
		/// <param name="alignment">Alignment.</param>
		/// <param name="line1Start">Line1 start point position.</param>
		/// <param name="line2Start">Line2 start point position.</param>
		public static nfloat AlignLineSegments (nfloat line1Length, nfloat line2Length, ImagedLabelViewAlignment alignment, out nfloat line1Start, out nfloat line2Start)
		{
			var maxLength = NMath.Max (line1Length, line2Length);
			switch (alignment) {
			case ImagedLabelViewAlignment.LeftOrUpperBorder:
				line1Start = 0;
				line2Start = 0;
				break;
				case ImagedLabelViewAlignment.RightOrLowerBoarder:
				line1Start = maxLength - line1Length;
				line2Start = maxLength - line2Length;
				break;
				case ImagedLabelViewAlignment.Center:
				line1Start = (maxLength - line1Length) * 0.5f;
				line2Start = (maxLength - line2Length) * 0.5f;
				break;
			default:
				// same to center
				line1Start = (maxLength - line1Length) * 0.5f;
				line2Start = (maxLength - line2Length) * 0.5f;
				break;
			}
			return maxLength;
		}

		/// <summary>
		/// Gets or sets the image view.
		/// </summary>
		/// <value>The image view.</value>
		public UIImageView ImageView
		{
			get {
				return _imgView;
			}

			set {
				if (value != _imgView) {
					if (null != _imgView) {
						_imgView.RemoveFromSuperview ();
					}
					_imgView = value;
					if (null != _imgView) {
						this.AddSubview (_imgView);
					}
					SetNeedsLayout ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the image.
		/// </summary>
		/// <value>The image.</value>
		public UIImage Image
		{
			get {
				return (null == _imgView) ? null : _imgView.Image;
			}

			set {
				if (null == _imgView) {
					_imgView = new UIImageView (value);
					this.AddSubview (_imgView);
					SetNeedsLayout ();
				} else if (value != _imgView.Image) {
					if (null == value) {
						_imgView.RemoveFromSuperview ();
						_imgView = null;
					} else {
						_imgView.Image = value;
					}
					SetNeedsLayout ();
				}
			}
		}


		/// <summary>
		/// Gets or sets the image tint color.
		/// </summary>
		/// <value>The image tint color.</value>
		public UIColor ImageTintColor
		{
			get {
				return (null == _imgView) ? null : _imgView.TintColor;
			}

			set {
				if (null == _imgView) {
					throw new InvalidOperationException ("Can not set tint color if an image is not set!");
				} else {
					_imgView.TintColor = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
		public string Text
		{
			get {
				return _labelView.Text;
			}

			set {
				_labelView.Text = value;
				SetNeedsLayout ();
			}
		}

		/// <summary>
		/// Gets or sets the attributed text.
		/// </summary>
		/// <value>The attributed text.</value>
		public NSAttributedString AttributedText { 
			get {
				return _labelView.AttributedText;
			}

			set {
				_labelView.AttributedText = value;
				SetNeedsLayout ();
			}
		}

		/// <summary>
		/// Gets or sets the spacing between the image and text.
		/// </summary>
		/// <value>The spacing between the image and text.</value>
		public float Spacing {
			get {
				return _spacing;
			}

			set {
				if (value != _spacing) {
					_spacing = value;
					SetNeedsLayout ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the x padding from the left/right border of this the <see cref="OverLayController.ImagedLabelView"/> to the left/right border of the composite of image and text.
		/// </summary>
		/// <value>The x padding.</value>
		public float HorizontalPadding
		{
			get {
				return _horizontalPadding;
			}
			set {
				if (value != _horizontalPadding) {
					_horizontalPadding = value;
					SetNeedsLayout ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the y padding from the upper/lower border of this the <see cref="OverLayController.ImagedLabelView"/> to the upper/lower border of the composite of image and text.
		/// </summary>
		/// <value>The padding y.</value>
		public float VerticalPadding
		{
			get {
				return _verticalPadding;
			}
			set {
				if (value != _verticalPadding) {
					_verticalPadding = value;
					SetNeedsLayout ();
				}
			}
		}

		public ImagedLabelViewImagePosition ImagePosition
		{
			get {
				return _imgPosition;
			}
			set {
				if (value != _imgPosition) {
					_imgPosition = value;
					SetNeedsLayout ();
				}
			}
		}
		public ImagedLabelViewAlignment Alignment
		{
			get {
				return _alignment;
			}
			set {
				if (value != _alignment) {
					_alignment = value;
					SetNeedsLayout ();
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			CalulateSubviews (Bounds.Size, true);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			return CalulateSubviews (size, false);
		}

	}


	public enum ImagedLabelViewImagePosition
	{
		LeftToText,
		RightToText,
		AboveText,
		BelowText
	}

	public enum ImagedLabelViewAlignment
	{
		LeftOrUpperBorder,
		Center,
		RightOrLowerBoarder
	}
}

