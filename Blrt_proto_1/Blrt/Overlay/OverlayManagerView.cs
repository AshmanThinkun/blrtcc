using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.Collections.Generic;

using BlrtiOS.UI;

namespace BlrtiOS.Overlay
{
	/// <summary>
	/// The view class for overlaying multiple tooltips
	/// </summary>
	public class OverlayManagerView : UIView
	{
		const float SHOW_ANIMATION		= 0.25f;
		const float HIDE_ANIMATION		= 0.25f;
		const int MIN_SHOW_TIME 		= 1000; // miliseconds

		private List<OverlayTooltipView> _tooltipPool;
		private int _waitingStartTick;



		private bool _overlayImageContainerHidden;
		private UIView _overlayImageContainer;
		private List<UIImageView> _overlayImageViews;

		private List<Action> _fireOnDismiss;

		private BlrtActionButton _dismissButton;






		public OverlayManagerView ()
		{
			_tooltipPool = new List<OverlayTooltipView> ();
			BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0.6f);
			AutoresizingMask = UIViewAutoresizing.All;
			Hidden = true;
			_shouldShow = false;
			Layer.ZPosition = nfloat.MaxValue;	// on top of all views
			Alpha = 0;
			_waitingStartTick = 0;

			_fireOnDismiss = new List<Action> ();


			_overlayImageContainer = new UIView (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};
			AddSubview(_overlayImageContainer);
			_overlayImageViews = new List<UIImageView> ();

			_dismissButton = new BlrtActionButton ("OK", BlrtConfig.BoldFont.SubTitle) {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin
			};
			_dismissButton.Click += Dismiss;
			_dismissButton.Frame = new CGRect ((int)((Bounds.Width - _dismissButton.Bounds.Width * 2.25f) * 0.5f), (Bounds.Height - _dismissButton.Bounds.Height * 1.2f - 16), _dismissButton.Bounds.Width *  2.25f, _dismissButton.Bounds.Height * 1.2f);

			_dismissButton.Hidden = !BlrtHelperiOS.IsPhone;

			_dismissButton.TintColor = BlrtConfig.BlrtGreen;
			_dismissButton.TextColor = BlrtConfig.BlrtBlack;


			AddSubview(_dismissButton);

			DismissOverlays(false);
		}

		/// <summary>
		/// Shows a tooltip.
		/// </summary>
		/// <param name="targetView">target of the tooltip</param>
		/// <param name="tooltipData">Tooltip data.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		/// <param name="position">tooltip position relative to the target.</param>
		public void ShowTooltip (UIView targetView, ITooltip tooltipData, bool animated, TooltipPosition position = TooltipPosition.Auto)
		{
			_waitingStartTick = Environment.TickCount;
			Frame = new CGRect (0, 0, Superview.Bounds.Width, Superview.Bounds.Height);
			OverlayTooltipView tooltipView = null;

			for (int i = 0; i < _tooltipPool.Count; i++) {
				// check if this tooltip has existed in tooltip pool
				if (tooltipData == _tooltipPool [i].Data) {
					tooltipView = _tooltipPool [i];
					break;
				}

				// find an unused tooltip instance in pool
				if (_tooltipPool [i].Usable) {
					tooltipView = _tooltipPool [i];
				}
			}

			if (null == tooltipView) {
				tooltipView = new OverlayTooltipView ();
				_tooltipPool.Add (tooltipView);
				AddSubview (tooltipView);
			}



			tooltipView.bindNew (targetView, tooltipData, position, animated);


			TryShowBody (animated);
		}

		/// <summary>
		/// Hides the tooltip.
		/// </summary>
		/// <param name="tooltipData">the hiding tooltip.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public void HideTooltip (ITooltip tooltipData, bool animated)
		{
			int foundTooltipIndex = -1;
			int usedTooltipCounter = 0;
			bool willHideCurtain = !Hidden;
			for (int i = 0; i < _tooltipPool.Count; i++) {
				if (null != _tooltipPool [i].Data) {
					// in pool find the tooltip which is requested to hide
					if ((-1 == foundTooltipIndex) && (tooltipData == _tooltipPool [i].Data)) {
						foundTooltipIndex = i;
					}

					// find if there are more than 1 tooltips are showing
					if (willHideCurtain) {
						usedTooltipCounter++;
						if (usedTooltipCounter > 1) {
							willHideCurtain = false;
						}
					}

					// exit the loop when both the 2 tasks above are finished
					if ((-1 != foundTooltipIndex) && (!willHideCurtain)) {
						break;
					}
				}
			}
			if (-1 != foundTooltipIndex) {	// the tooltip has been found
				var foundTooltip = _tooltipPool [foundTooltipIndex];

				foundTooltip.Disconnect (animated);

				if (null != TooltipDismissed) {
					TooltipDismissed(this, new TooltipDismissedEventArgs () {
						DismissedTooltips = new ITooltip[] {tooltipData}
					});
				}
			}


			TryHideBody (animated);
		}

		bool _shouldShow;

		void TryShowBody (bool animated)
		{
			if (!Hidden && _shouldShow) {
				return;
			}
			Frame = new CGRect (0, 0, Superview.Bounds.Width, Superview.Bounds.Height);

			bool show = !_overlayImageContainerHidden;

			foreach (var tooltip in _tooltipPool) {
			
					if (tooltip.Visable || show) {
					show = true;
					break;
				}
			}

			if (show) {

				_shouldShow = true;
				Hidden = false;

				if (animated) {
					UIView.Animate (SHOW_ANIMATION, () => {
						Alpha = 1;
					});

				} else {
					Alpha = 1;
				}			
			}
		}

		void TryHideBody (bool animated)
		{
			if (Hidden && !_shouldShow) {
				return;
			}

			if(!_overlayImageContainerHidden)
				return;


			foreach (var tooltip in _tooltipPool) {

				if (tooltip.Visable) {
					return;
				}
			}




			_shouldShow = false;

			if (animated) {
				UIView.Animate (SHOW_ANIMATION, () => {
					Alpha = 0;
				}, () => {
					if(!_shouldShow)
						Hidden = true;
				});
			
			} else {
				Alpha = 0;
				Hidden = true;
			}

			if (_fireOnDismiss.Count > 0) {

				var tmp = _fireOnDismiss.Count;
			
				for (int i = 0; i < tmp; ++i) {
					if(_fireOnDismiss[i] != null)
						_fireOnDismiss[i]();
				}

				_fireOnDismiss.RemoveRange(0, tmp);
			}
		}

		public void OnOverlaysDismiss(Action action){
		
			_fireOnDismiss.Add (action);
		
		}






		/// <summary>
		/// Hides all tooltips.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public void DismissOverlays (bool animated)
		{
			if(animated && _overlayImageContainer.Hidden){
				_overlayImageContainerHidden = true;
				UIView.Animate (0.4f, () => {

					for (int i = 0; i < _overlayImageViews.Count; ++i) {
						_overlayImageContainer.Alpha = 0;	
					}			
				}, ()=>{
					if(_overlayImageContainerHidden){
						_overlayImageContainer.Hidden = true;
						
						for (int i = 0; i < _overlayImageViews.Count; ++i) {
							_overlayImageViews [i].Image = null;
						}
					}
				});
			} else {

				_overlayImageContainerHidden = _overlayImageContainer.Hidden = true;
				_overlayImageContainer.Alpha = 0;

				for (int i = 0; i < _overlayImageViews.Count; ++i) {
					_overlayImageContainer.Alpha = 0;	
					_overlayImageViews [i].Image = null;
				}
			}




			var dismissedTooltipList = new List<ITooltip> ();

			foreach (var tooltip in _tooltipPool) {
				if (tooltip.Visable) {
					dismissedTooltipList.Add (tooltip.Data);
					tooltip.Disconnect (animated);
				}
			}
			if (null != TooltipDismissed) {
				TooltipDismissed(this, new TooltipDismissedEventArgs () {
					DismissedTooltips = dismissedTooltipList.ToArray ()
				});
			}

			TryHideBody (true);
		}

		public EventHandler TooltipDismissed;

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if (!Hidden) {
				foreach (var tooltip in _tooltipPool) {
					if (null != tooltip.Data) {
						tooltip.PositionTooltip ();
					}
				}
			}
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);

			if (!_dismissButton.Hidden)
				return;

			if (Hidden) {
				DismissOverlays (false);
			} else if (Environment.TickCount - _waitingStartTick > MIN_SHOW_TIME) {
				DismissOverlays (true);
			}
		}

		private void Dismiss(object sender, EventArgs args){

			if (Hidden) {
				DismissOverlays (false);
			} else {
				DismissOverlays (true);
			}
		}


		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			if (Hidden)
				return null;

			return base.HitTest (point, uievent);
		}








		public void ShowImage(UIImage image, UIControlContentVerticalAlignment aligment, bool animated)
		{
			float delta = 0;
			Frame = new CGRect (0, 0, Superview.Bounds.Width, Superview.Bounds.Height);

			switch (aligment) {
				case UIControlContentVerticalAlignment.Bottom:
					delta = 1;
					break;
				case UIControlContentVerticalAlignment.Center:
				case UIControlContentVerticalAlignment.Fill:
					delta = 0.5f;
					break;
				default:
					delta = 0;
					break;
			}

			UIImageView img = null;


			for (int i = 0; img == null; ++i) {
			
				if(i < _overlayImageViews.Count){
				
					if (_overlayImageViews [i].Image == null) {
						img = _overlayImageViews [i];
					}

				} else {

					img = new UIImageView () {
					};
					_overlayImageContainer.AddSubview(img);
					_overlayImageViews.Add(img);

				}
			}

			_waitingStartTick = Environment.TickCount;

			img.Image = image;
			img.Frame = new CGRect ((int)((Bounds.Width - image.Size.Width) * 0.5f), (int)((Bounds.Height - image.Size.Height) * delta), image.Size.Width, image.Size.Height);

			if(animated){
				_overlayImageContainer.Alpha = 1;
				_overlayImageContainer.Hidden = _overlayImageContainerHidden = false;
				UIView.Animate (0.4f, () => {
					img.Alpha = 1;
				});
			} else {

				_overlayImageContainer.Alpha = 1;
				_overlayImageContainerHidden = _overlayImageContainer.Hidden = false;
				img.Alpha = 1;
			}

			TryShowBody (animated);
		}

	}

	/// <summary>
	/// Tooltip dismissed event arguments.
	/// </summary>
	public class TooltipDismissedEventArgs : EventArgs
	{
		public ITooltip[] DismissedTooltips { get; set; }
	}
}

