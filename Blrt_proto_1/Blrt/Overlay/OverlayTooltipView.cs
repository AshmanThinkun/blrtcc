using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.Overlay
{
	/// <summary>
	/// View for each single tooltip
	/// </summary>
	public class OverlayTooltipView : UIView
	{
		const float SPACING_FROM_SUBJECT	= 16f;
		const float TRIANGLE_HEIGHT			= 14f;
		const float TRIANGLE_WIDTH			= 32f;
		const float EDGE_MARGIN				= 4f;
		const float CORNER_RADIUS			= 8f;
		const float STATUS_BAR_HEIGHT		= 22f;
		const float ARROW_BASE_CURVE		= 0.20f;
		const float ARROW_POINT_CURVE		= 0.10f;

		bool _animating;

		private UIView _targetView;
		private UIScrollView _scrollView;
		private UIView _contentView;
		private TooltipPosition _position;
		private ITooltip _tooltipData;

		public bool Visable { get { return !(_tooltipData == null || _targetView == null);} }
		public bool Usable { get { return !_animating && Visable;} }




		public OverlayTooltipView ()
		{


			_scrollView = new UIScrollView () {
				Delegate = new TooltipScrollViewDelegate (),
				ContentOffset = CGPoint.Empty
			};
			_contentView = new UIView ();
			_scrollView.AddSubview (_contentView);
			AddSubview (_scrollView);
		}

		/// <summary>
		/// Binds this instance to a new ITooltip.
		/// </summary>
		/// <param name="targetView">target of the tooltip.</param>
		/// <param name="parentView">Parent view of the tooltip.</param>
		/// <param name="tooltipData">Tooltip data.</param>
		/// <param name="position">tooltip's position relative to the target.</param>
		/// <param name="initAlpha">Init value of the tooltip's alpha.</param>
		public void bindNew (UIView targetView, ITooltip tooltipData, TooltipPosition position, bool animate)
		{
			if ((null == targetView) || (null == tooltipData)) {
				throw new ArgumentNullException ();
			}
			Disconnect (false);
			_position = position;
			_targetView = targetView;
			_tooltipData = tooltipData;
			BackgroundColor = _tooltipData.BackgroundColor ?? UIColor.White;
			_tooltipData.LayoutTooltip (_contentView);

			PositionTooltip ();

			Hidden = false;

			if (animate) {
				_animating = true;

				UIView.Animate (0.4f, () => {
					Alpha = 1;
				}, () => {
					_animating = false;
				});
			} else {

				_animating = false;
				Alpha = 1;
			}
		}

		/// <summary>
		/// Disconnect this instance from the tooltip data and its parent view.
		/// </summary>
		public void Disconnect (bool animate)
		{
			if (_tooltipData == null)
				return;

			_targetView = null;
			_tooltipData = null;

			if (animate) {
				_animating = true;

				UIView.Animate (0.4f, () => {
					Alpha = 0;
				}, () => {
					_animating = false;
					Disconnect(false);
				});
			} else {

				_animating = false;
				foreach (var subview in _contentView.Subviews) {
					subview.RemoveFromSuperview ();
				}
				Hidden = true;
				Alpha = 0;
			}
		}

		/// <summary>
		/// Gets the target view of the tooltip.
		/// </summary>
		/// <value>The target view.</value>
		public UIView Target {
			get {
				return _targetView;
			}
		}

		/// <summary>
		/// Gets the tooltip data.
		/// </summary>
		/// <value>The tooltip data.</value>
		public ITooltip Data {
			get {
				return _tooltipData;
			}
		}

		/// <summary>
		/// Gets the tooltip's position relative to the target.
		/// </summary>
		/// <value>the tooltip's position relative to the target.</value>
		public TooltipPosition Position {
			get {
				return _position;
			}
		}

		/// <summary>
		/// Positions the tooltip.
		/// </summary>
		public void PositionTooltip ()
		{
			if (Superview == null || _targetView == null || _tooltipData == null)
				return;




			CGRect targetFrame = CGRect.Empty;
			CGRect bounds = CGRect.Empty;
			if (Superview != null && _targetView != null) {
				targetFrame = GetFrameRelativeToSuper (Superview, _targetView);
				bounds = new CGRect (0, STATUS_BAR_HEIGHT, Superview.Bounds.Width, Superview.Bounds.Height - STATUS_BAR_HEIGHT);
			}
			CGRect tooltipFrame;
			TooltipPosition tooltipPosition = CalculateTooltipInfo (targetFrame, bounds, _tooltipData.TooltipSize, _tooltipData.HorizontalPadding, _tooltipData.VerticalPadding, _position, out tooltipFrame);
			Frame = tooltipFrame;
			nfloat arrowX, arrowY;

			// get the frames of the tooltip view and scroll view, and the position of the arrow
			switch (tooltipPosition) {
			case TooltipPosition.LeftToTarget:
				_scrollView.Frame = new CGRect (0, _tooltipData.VerticalPadding, _tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height);
				arrowX = Frame.Width;
				arrowY = targetFrame.Y + targetFrame.Height * 0.5f - tooltipFrame.Y;
				break;
			case TooltipPosition.BelowTarget:
				_scrollView.Frame = new CGRect (0, _tooltipData.VerticalPadding + TRIANGLE_HEIGHT, _tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height);
				arrowX = targetFrame.X + targetFrame.Width * 0.5f - tooltipFrame.X;
				arrowY = 0;
				break;
			case TooltipPosition.RightToTarget:
				_scrollView.Frame = new CGRect (0 + TRIANGLE_HEIGHT, _tooltipData.VerticalPadding, _tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height);
				arrowX = 0;
				arrowY = targetFrame.Y + targetFrame.Height * 0.5f - tooltipFrame.Y;
				break;
			case TooltipPosition.AboveTarget:
				_scrollView.Frame = new CGRect (0, _tooltipData.VerticalPadding, _tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height);
				arrowX = targetFrame.X + targetFrame.Width * 0.5f - tooltipFrame.X;
				arrowY = Frame.Height;
				break;
			default:
				LLogger.WriteLine ("ERROR!!! Wrong TooltipPosition!!!");
				_scrollView.Frame = new CGRect (0, _tooltipData.VerticalPadding, _tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height);
				arrowX = targetFrame.X + targetFrame.Width * 0.5f - tooltipFrame.X;
				arrowY = Frame.Height;
				break;
			}

			_contentView.Frame = new CGRect (new CGPoint (_tooltipData.HorizontalPadding, 0), _tooltipData.ContentSize ?? _tooltipData.TooltipSize);
			_scrollView.ContentSize = (null != _tooltipData.ContentSize) 
					? (new CGSize (NMath.Max (_tooltipData.ContentSize.Value.Width, _tooltipData.TooltipSize.Width) + _tooltipData.HorizontalPadding * 2, 
				NMath.Max (_tooltipData.ContentSize.Value.Height, _tooltipData.TooltipSize.Height))) 
					: (new CGSize (_tooltipData.TooltipSize.Width + _tooltipData.HorizontalPadding * 2, _tooltipData.TooltipSize.Height));

			// Draw the triangle
			Layer.Mask = CreateTooptipBoxMask (Bounds, new CGSize (TRIANGLE_WIDTH, TRIANGLE_HEIGHT), arrowX, arrowY, CORNER_RADIUS, tooltipPosition);

			if (null != _scrollView.Delegate) {
				_scrollView.Delegate.Scrolled (_scrollView);
			}
		}

		/// <summary>
		/// Calculates the tooltip position and size info.
		/// </summary>
		/// <returns>The tooltip's position relative to target.</returns>
		/// <param name="targetFrame">frame of the target view.</param>
		/// <param name="bounds">Bounds.</param>
		/// <param name="tooltipSize">Bounds to place the tooltip.</param>
		/// <param name="preferedPosition">user's prefered tooltip position.</param>
		/// <param name="tooltipFrame">Tooltip frame.</param>
		public static TooltipPosition CalculateTooltipInfo (CGRect targetFrame, CGRect bounds, CGSize tooltipSize, nfloat paddingX, nfloat paddingY, TooltipPosition preferedPosition, out CGRect tooltipFrame)
		{
			nfloat xCoordinate, yCoordinate, width, height;
			var tooltipPosition = preferedPosition;
			// get the (x, y) coordinate, width and height of the tooltip based on its position to the target
			switch (tooltipPosition) {
			case TooltipPosition.LeftToTarget:
				CalculateLeftTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				break;
			case TooltipPosition.BelowTarget:
				CalculateDownTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				break;
			case TooltipPosition.RightToTarget:
				CalculateRightTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				break;
			case TooltipPosition.AboveTarget:
				CalculateUpTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				break;
			case TooltipPosition.AutoVertical:
				if (CalculateUpTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height)) {
					tooltipPosition = TooltipPosition.AboveTarget;
				} else {
					tooltipPosition = TooltipPosition.BelowTarget;
					CalculateDownTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				}
				break;
			case TooltipPosition.AutoHorizontal:
				if (CalculateLeftTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height)) {
					tooltipPosition = TooltipPosition.LeftToTarget;
				} else {
					tooltipPosition = TooltipPosition.RightToTarget;
					CalculateRightTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				}
				break;
			default: // including TooltipPosition.Auto
				if (TooltipPosition.Auto != tooltipPosition) {
					LLogger.WriteLine ("ERROR!!! Wrong TooltipPosition!!!");
				}
				if (CalculateUpTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height)) {
					tooltipPosition = TooltipPosition.AboveTarget;
				} else if (CalculateDownTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height)) {
					tooltipPosition = TooltipPosition.BelowTarget;
				} else if (CalculateLeftTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height)) {
					tooltipPosition = TooltipPosition.LeftToTarget;
				} else {
					tooltipPosition = TooltipPosition.RightToTarget;
					CalculateRightTooltipInfo (targetFrame, bounds, tooltipSize, paddingX, paddingY, out xCoordinate, out yCoordinate, out width, out height);
				}
				break;
			}
			tooltipFrame = new CGRect (xCoordinate, yCoordinate, width, height);
			return tooltipPosition;
		}

		/// <summary>
		/// Calculates the tooltip position and size info if the tooltip is above the target
		/// </summary>
		/// <returns><c>true</c>, if the tooltip's upper board is in the bound, <c>false</c> otherwise.</returns>
		/// <param name="targetFrame">frame of the target view.</param>
		/// <param name="bounds">Bounds.</param>
		/// <param name="tooltipSize">Bounds to place the tooltip.</param>
		/// <param name="xCoordinate">output position - X coordinate.</param>
		/// <param name="yCoordinate">output position - Y coordinate.</param>
		/// <param name="width">output size - Width.</param>
		/// <param name="height">output size - Height.</param>
		public static bool CalculateUpTooltipInfo (CGRect targetFrame, CGRect bounds, CGSize tooltipSize, nfloat paddingX, nfloat paddingY, out nfloat xCoordinate, out nfloat yCoordinate, out nfloat width, out nfloat height)
		{
			xCoordinate = targetFrame.X + (targetFrame.Width - (tooltipSize.Width + paddingX * 2)) * 0.5f;
			yCoordinate = targetFrame.Y - (SPACING_FROM_SUBJECT + tooltipSize.Height + paddingY * 2);
			width = tooltipSize.Width + paddingX * 2;
			height = tooltipSize.Height + paddingY * 2 + TRIANGLE_HEIGHT;
			if (yCoordinate - EDGE_MARGIN >= bounds.Y) {
				AdjustHorizontally (bounds, width, ref xCoordinate);
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Calculates the tooltip position and size info if the tooltip is right to the target
		/// </summary>
		/// <returns><c>true</c>, if the tooltip's right board is in the bound, <c>false</c> otherwise.</returns>
		/// <param name="targetFrame">frame of the target view.</param>
		/// <param name="bounds">Bounds.</param>
		/// <param name="tooltipSize">Bounds to place the tooltip.</param>
		/// <param name="xCoordinate">output position - X coordinate.</param>
		/// <param name="yCoordinate">output position - Y coordinate.</param>
		/// <param name="width">output size - Width.</param>
		/// <param name="height">output size - Height.</param>
		public static bool CalculateRightTooltipInfo (CGRect targetFrame, CGRect bounds, CGSize tooltipSize, nfloat paddingX, nfloat paddingY, out nfloat xCoordinate, out nfloat yCoordinate, out nfloat width, out nfloat height)
		{
			xCoordinate = targetFrame.X + targetFrame.Width + SPACING_FROM_SUBJECT - TRIANGLE_HEIGHT;
			yCoordinate = targetFrame.Y + (targetFrame.Height - (tooltipSize.Height + paddingY * 2)) * 0.5f;
			width = tooltipSize.Width + paddingX * 2 + TRIANGLE_HEIGHT;
			height = tooltipSize.Height + paddingY * 2;
			if (xCoordinate + width + EDGE_MARGIN <= bounds.X + bounds.Width) {
				AdjustVertically (bounds, height, ref yCoordinate);
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Calculates the tooltip position and size info if the tooltip is below the target
		/// </summary>
		/// <returns><c>true</c>, if the tooltip's lower board is in the bound, <c>false</c> otherwise.</returns>
		/// <param name="targetFrame">frame of the target view.</param>
		/// <param name="bounds">Bounds.</param>
		/// <param name="tooltipSize">Bounds to place the tooltip.</param>
		/// <param name="xCoordinate">output position - X coordinate.</param>
		/// <param name="yCoordinate">output position - Y coordinate.</param>
		/// <param name="width">output size - Width.</param>
		/// <param name="height">output size - Height.</param>
		public static bool CalculateDownTooltipInfo (CGRect targetFrame, CGRect bounds, CGSize tooltipSize, nfloat paddingX, nfloat paddingY, out nfloat xCoordinate, out nfloat yCoordinate, out nfloat width, out nfloat height)
		{
			xCoordinate = targetFrame.X + (targetFrame.Width - (tooltipSize.Width + paddingX * 2)) * 0.5f;
			yCoordinate = targetFrame.Y + targetFrame.Height + SPACING_FROM_SUBJECT - TRIANGLE_HEIGHT;
			width = tooltipSize.Width + paddingX * 2;
			height = tooltipSize.Height + paddingY * 2 + TRIANGLE_HEIGHT;
			if (yCoordinate + height + EDGE_MARGIN <= bounds.Y + bounds.Height) {
				AdjustHorizontally (bounds, width, ref xCoordinate);
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Calculates the tooltip position and size info if the tooltip is left to the target
		/// </summary>
		/// <returns><c>true</c>, if the tooltip's left board is in the bound, <c>false</c> otherwise.</returns>
		/// <param name="targetFrame">frame of the target view.</param>
		/// <param name="bounds">Bounds.</param>
		/// <param name="tooltipSize">Bounds to place the tooltip.</param>
		/// <param name="xCoordinate">output position - X coordinate.</param>
		/// <param name="yCoordinate">output position - Y coordinate.</param>
		/// <param name="width">output size - Width.</param>
		/// <param name="height">output size - Height.</param>
		public static bool CalculateLeftTooltipInfo (CGRect targetFrame, CGRect bounds, CGSize tooltipSize, nfloat paddingX, nfloat paddingY, out nfloat xCoordinate, out nfloat yCoordinate, out nfloat width, out nfloat height)
		{
			xCoordinate = targetFrame.X - (SPACING_FROM_SUBJECT + tooltipSize.Width + paddingX * 2);
			yCoordinate = targetFrame.Y + (targetFrame.Height - (tooltipSize.Height + paddingY * 2)) * 0.5f;
			width = tooltipSize.Width + paddingX * 2 + TRIANGLE_HEIGHT;
			height = tooltipSize.Height + paddingY * 2;
			if (xCoordinate - EDGE_MARGIN >= bounds.X) {
				AdjustVertically (bounds, height, ref yCoordinate);
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Adjusts the tooltip horizontally so that most/all of the tooltip is in the bound.
		/// </summary>
		/// <param name="bounds">Bounds to place the tooltip.</param>
		/// <param name="width">Width of the tooltip.</param>
		/// <param name="xCoordinate">X coordinate of the tooltip.</param>
		public static void AdjustHorizontally (CGRect bounds, nfloat width, ref nfloat xCoordinate)
		{
			if ((width + 2 * EDGE_MARGIN > bounds.Width) || (xCoordinate - EDGE_MARGIN < bounds.X)) {
				xCoordinate = bounds.X + EDGE_MARGIN;
			} else if (xCoordinate + width + EDGE_MARGIN > bounds.X + bounds.Width) {
				xCoordinate = bounds.X + bounds.Width - EDGE_MARGIN - width;
			}
		}

		/// <summary>
		/// Adjusts the tooltip vertically so that most/all of the tooltip is in the bound.
		/// </summary>
		/// <param name="bounds">Bounds to place the tooltip.</param>
		/// <param name="height">Height of the tooltip.</param>
		/// <param name="yCoordinate">Y coordinate of the tooltip .</param>
		public static void AdjustVertically (CGRect bounds, nfloat height, ref nfloat yCoordinate)
		{
			if ((height + 2 * EDGE_MARGIN > bounds.Height) || (yCoordinate - EDGE_MARGIN < bounds.Y)) {
				yCoordinate = bounds.Y + EDGE_MARGIN;
			} else if (yCoordinate + height + EDGE_MARGIN > bounds.Y + bounds.Height) {
				yCoordinate = bounds.Y + bounds.Height - EDGE_MARGIN - height;
			}
		}

		/// <summary>
		/// Gets the frame relative to super.
		/// </summary>
		/// <returns>The frame relative to super.</returns>
		/// <param name="super">Super.</param>
		/// <param name="subject">Subject.</param>
		public static CGRect GetFrameRelativeToSuper (UIView super, UIView subject)
		{
			return new CGRect (
				super.ConvertPointFromView (CGPoint.Empty, subject),
				subject.Bounds.Size);
		}

		/// <summary>
		/// Creates the mask of the tooltip box.
		/// </summary>
		/// <returns>The tooptip box mask.</returns>
		/// <param name="frame">bound of the tooltip.</param>
		/// <param name="arrowSize">Arrow size.</param>
		/// <param name="arrowX">top point position x.</param>
		/// <param name="arrowY">top point position y.</param>
		/// <param name="cornerRadius">Corner radius of the rectangle part.</param>
		/// <param name="position">Position of the tooltip relative to the target.</param>
		protected static CoreAnimation.CAShapeLayer CreateTooptipBoxMask (CGRect frame, CGSize arrowSize, nfloat arrowX, nfloat arrowY, nfloat cornerRadius, TooltipPosition position)
		{
			UIBezierPath path = CreateTooptipBox (frame, arrowSize, arrowX, arrowY, cornerRadius, position);
			//roundedPath

			var maskLayer = new CoreAnimation.CAShapeLayer ();
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = path.CGPath;

			return maskLayer;
		}

		/// <summary>
		/// Creates the tooptip box.
		/// </summary>
		/// <returns>The tooptip box.</returns>
		/// <param name="frame">bound of the tooltip.</param>
		/// <param name="arrowSize">Arrow size.</param>
		/// <param name="arrowX">top point position x.</param>
		/// <param name="arrowY">top point position y.</param>
		/// <param name="cornerRadius">Corner radius of the rectangle part.</param>
		/// <param name="position">Position of the tooltip relative to the target.</param>
		protected static UIBezierPath CreateTooptipBox (CGRect frame, CGSize arrowSize, nfloat arrowX, nfloat arrowY, nfloat cornerRadius, TooltipPosition position)
		{
			UIBezierPath roundedPath = null;

			switch (position) {
			case TooltipPosition.LeftToTarget:

				roundedPath = UIBezierPath.FromRoundedRect (
					new CGRect (0, 0, frame.Width - arrowSize.Height, frame.Height), 
					UIRectCorner.AllCorners, new CGSize (cornerRadius, cornerRadius));
				roundedPath.UsesEvenOddFillRule = false;
				roundedPath.MoveTo (new CGPoint (arrowX - arrowSize.Height, arrowY - arrowSize.Width * 0.5f)); // to the up point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, arrowY),
					new CGPoint (arrowX - arrowSize.Height, arrowY - arrowSize.Width * (0.5f - ARROW_BASE_CURVE)),
					new CGPoint (arrowX, arrowY - arrowSize.Width * ARROW_POINT_CURVE)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX - arrowSize.Height, arrowY + arrowSize.Width * 0.5f),
					new CGPoint (arrowX, arrowY + arrowSize.Width * ARROW_POINT_CURVE),
					new CGPoint (arrowX - arrowSize.Height, arrowY + arrowSize.Width * (0.5f - ARROW_BASE_CURVE))
				);

				break;
			case TooltipPosition.BelowTarget:

				roundedPath = UIBezierPath.FromRoundedRect (
					new CGRect (0, arrowSize.Height, frame.Width, frame.Height - arrowSize.Height), 
					UIRectCorner.AllCorners, new CGSize (cornerRadius, cornerRadius));
				roundedPath.UsesEvenOddFillRule = false;
				roundedPath.MoveTo (new CGPoint (arrowX - arrowSize.Width * 0.5f, arrowY + arrowSize.Height)); // to the left point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, arrowY),
					new CGPoint (arrowX - arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowY + arrowSize.Height),
					new CGPoint (arrowX - arrowSize.Width * ARROW_POINT_CURVE, arrowY)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX + arrowSize.Width * 0.5f, arrowY + arrowSize.Height),
					new CGPoint (arrowX + arrowSize.Width * ARROW_POINT_CURVE, arrowY),
					new CGPoint (arrowX + arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowY + arrowSize.Height)
				);

				break;
			case TooltipPosition.RightToTarget:

				roundedPath = UIBezierPath.FromRoundedRect (
					new CGRect (arrowSize.Height, 0, frame.Width - arrowSize.Height, frame.Height), 
					UIRectCorner.AllCorners, new CGSize (cornerRadius, cornerRadius));
				roundedPath.UsesEvenOddFillRule = false;
				roundedPath.MoveTo (new CGPoint (arrowX + arrowSize.Height, arrowY + arrowSize.Width * 0.5f)); // to the down point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, arrowY),
					new CGPoint (arrowX + arrowSize.Height, arrowY + arrowSize.Width * (0.5f - ARROW_BASE_CURVE)),
					new CGPoint (arrowX, arrowY + arrowSize.Width * ARROW_POINT_CURVE)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX + arrowSize.Height, arrowY - arrowSize.Width * 0.5f),
					new CGPoint (arrowX, arrowY - arrowSize.Width * ARROW_POINT_CURVE),
					new CGPoint (arrowX + arrowSize.Height, arrowY - arrowSize.Width * (0.5f - ARROW_BASE_CURVE))
				);

				break;
			case TooltipPosition.AboveTarget:

				roundedPath = UIBezierPath.FromRoundedRect (
					new CGRect (0, 0, frame.Width, frame.Height - arrowSize.Height), 
					UIRectCorner.AllCorners, new CGSize (cornerRadius, cornerRadius));
				roundedPath.UsesEvenOddFillRule = false;
				roundedPath.MoveTo (new CGPoint (arrowX + arrowSize.Width * 0.5f, arrowY - arrowSize.Height)); // to the right point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, arrowY),
					new CGPoint (arrowX + arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowY - arrowSize.Height),
					new CGPoint (arrowX + arrowSize.Width * ARROW_POINT_CURVE, arrowY)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX - arrowSize.Width * 0.5f, arrowY - arrowSize.Height),
					new CGPoint (arrowX - arrowSize.Width * ARROW_POINT_CURVE, arrowY),
					new CGPoint (arrowX - arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowY - arrowSize.Height)
				);

				break;
			default:
				LLogger.WriteLine ("ERROR!!! Wrong TooltipPosition!!!");
				break;
			}
			roundedPath.ClosePath ();
			return roundedPath;
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			if (Hidden || null == _tooltipData || !_tooltipData.IsTouchReceiver)
				return null;

			return base.HitTest (point, uievent);
		}

		private class TooltipScrollViewDelegate : UIScrollViewDelegate
		{
			private const float DELTA = 4;
			UIView top;
			UIView bottom;
			UIScrollView target;

			public override void Scrolled (UIScrollView scrollView)
			{
				if (target == null) {
					target = scrollView;

					top = new UIView () {
						BackgroundColor = UIColor.Black
					};
					top.Layer.ShadowColor = UIColor.Black.CGColor;
					top.Layer.ShadowRadius = 8;
					top.Layer.ShadowOpacity = 1f;
					top.Layer.ZPosition = 100;
					scrollView.AddSubview (top);

					bottom = new UIView () {
						BackgroundColor = UIColor.Black
					};
					bottom.Layer.ShadowColor = UIColor.Black.CGColor;
					bottom.Layer.ShadowRadius = 8;
					bottom.Layer.ShadowOpacity = 1f;
					bottom.Layer.ZPosition = 100;
					scrollView.AddSubview (bottom);
				}

				if (target == scrollView) {

					top.Alpha = NMath.Min (1, scrollView.ContentOffset.Y / (scrollView.Bounds.Height * 0.2f));
					top.Frame = new CGRect (0, scrollView.ContentOffset.Y - DELTA, scrollView.Frame.Width, DELTA);

					bottom.Alpha = NMath.Min (1, (scrollView.ContentSize.Height - scrollView.ContentOffset.Y - scrollView.Bounds.Height) / (scrollView.Bounds.Height * 0.2f));
					bottom.Frame = new CGRect (0, scrollView.ContentOffset.Y + scrollView.Bounds.Height + DELTA - 1, scrollView.Frame.Width, DELTA);

				}
			}
		}
	}
}

