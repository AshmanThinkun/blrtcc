using System;
using Parse;
using Newtonsoft.Json;
using BlrtData;

namespace BlrtiOS
{
	public class BlrtParseFile
	{
		public string Name {
			get;
			set;
		}
		public Uri Url {
			get;
			set;
		}

		[JsonIgnore]
		public string FileLocation 				{ get { return BlrtConstants.Directories.Default.GetFilesPath (Name); } }
		[JsonIgnore]
		public bool IsFileLocalAndUsable 		{ get { return BlrtUtil.CanUseFile(FileLocation); } }

		public BlrtParseFile()
		{

		}

		public static BlrtParseFile FromParse(ParseFile parse)
		{
			var v = new BlrtParseFile ();

			v.Name 	= parse.Name;
			v.Url 	= parse.Url;

			return v;
		}
	}
}

