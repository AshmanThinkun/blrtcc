using System;
using UserVoiceSDKBinding;
using UIKit;
using Foundation;
using System.Collections.Generic;
using Parse;
using BlrtData;


namespace BlrtiOS
{
	/// <summary>
	/// Show UserVoice Interfaces to get user's feedback, show knowledge bases, and set custom fields for Blrt
	/// </summary>
	public static class BlrtUserVoice
	{
		private const int INVALID_FORUM_ID			= -1;

		private const string FN_USER_TRAITS_TYPE	= "type";

		private const string STR_TRUE 				= "true";
		private const string STR_FALSE 				= "false";

		private const string FN_USER_ID				= "user_id";
		private const string FN_CREATED_AT			= "user_created_at";
		private const string FN_EMAIL_VERIFIED		= "email_verified";
		private const string FN_ACCOUNT_TYPE		= "account_type";
		private const string FN_ORGANISATION		= "organisation";
		private const string FN_DEVICE_LANGUAGE		= "device_language";
		private const string FN_DEVICE_LOCALE		= "device_locale";
		private const string FN_GENDER				= "gender";
		private const string FN_INDUSTRY			= "industry";
		private const string FN_FB_SIGNUP			= "fb_signup";
		private const string FN_FB_CONNECTED		= "fb_connected";
		private const string FN_NUM_CONTACTS 		= "num_contacts";
		private const string FN_NUM_RECENT_CONTACTS	= "num_recent_contacts";
		private const string FN_NUM_CONV_INBOX		= "num_conv_inbox";
		private const string FN_NUM_CONV_MY			= "num_conv_my";
		private const string FN_NUM_CONV_ARCHIVE	= "num_conv_archive";
		private const string FN_NUM_CONV_FLAGGED	= "num_conv_flagged";
		private const string FN_NUM_TAGS			= "num_tags";

		private static UVConfig _config = null;
		//private static UVCustomInfo _customInfo;
		//private static bool _isCustomInfoDirty = false;
		private static string _userTraitsType = null;

		/// <summary>
		/// Initialize the with site and forumId.
		/// </summary>
		/// <param name="site">Site.</param>
		/// <param name="forumId">Forum Id.</param>
		public static void Initialize (string site, nint forumId)
		{
			if (string.IsNullOrWhiteSpace (site)) {
				throw new ArgumentNullException ("site");
			}
			_config = UVConfig.ConfigWithSite (site);
			_config.ForumId = forumId;
			UserVoice.Initialize (_config);
		}

		/// <summary>
		/// Gets the site.
		/// </summary>
		/// <value>The site.</value>
		public static string Site 
		{
			get {
				if (Initialized) {
					return _config.Site;
				} else {
					return null;
				}
			}
		}

		/// <summary>
		/// Gets the forum identifier.
		/// </summary>
		/// <value>The forum identifier.</value>
		public static nint ForumId
		{
			get {
				if (Initialized) {
					return _config.ForumId;
				} else {
					return INVALID_FORUM_ID;
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="UserVoiceTest.BlrtUserVoice"/> is initialized.
		/// </summary>
		/// <value><c>true</c> if initialized; otherwise, <c>false</c>.</value>
		public static bool Initialized
		{ 
			get
			{
				return (_config != null);
			}
		}

		/// <summary>
		/// Gets or sets the "type" value in user traits.
		/// </summary>
		/// <value>the "type" value in user traits.</value>
		public static string UserTraitsType
		{
			get {
				return _userTraitsType;
			}

			set {
				_userTraitsType = value;
			}
		}

		/*
		/// <summary>
		/// Gets or sets the custom fields data.
		/// </summary>
		/// <value>the custom fields data.</value>
		public static UVCustomInfo CustomInfo
		{
			get {
				return _customInfo;
			}

			set {
				_isCustomInfoDirty = true;
				_customInfo = value;
			}
		}
		*/

		/// <summary>
		/// Presents the UserVoice interface.
		/// </summary>
		/// <param name="parentViewController">Parent view controller.</param>
		public static void PresentUVInterface(UIViewController parentViewController)
		{
			if (!Initialized) {
				throw new NotInitializedException ();
			}
			setUserInfo ();
			UserVoice.PresentUserVoiceInterfaceForParentViewController (parentViewController);
		}

		public static void PresentFeedback (UIViewController parentViewController)
		{
			if (!Initialized) {
				throw new NotInitializedException ();
			}
			setUserInfo ();
			UserVoice.PresentUserVoiceNewIdeaFormForParentViewController (parentViewController);
		}

		public static void PresentSupport (UIViewController parentViewController, string extraText = null)
		{
			if (!Initialized) {
				throw new NotInitializedException ();
			}
			setUserInfo ();

			_config.ExtraTicketInfo = extraText ?? "";

			UserVoice.PresentUserVoiceContactUsFormForParentViewController (parentViewController);
		}

		/// <summary>
		/// Sets the user info to the UserVoice, includes the user traits and custom fields
		/// </summary>
		private static void setUserInfo ()
		{
			var user = ParseUser.CurrentUser;
			// user traits
			if (user == null) {
				_config.UserTraits = NSDictionary.FromObjectAndKey (new NSString (_userTraitsType ?? "non-user"), new NSString(FN_USER_TRAITS_TYPE));
				_config.IdentifyUserWithEmail ("", "", "");
			} else {
				_config.IdentifyUserWithEmail (user.Email, user.Get<string>(BlrtData.BlrtUserHelper.DisplayNameKey, ""), string.Format ("{0}-{1}", BlrtData.BlrtConstants.AppPrefix, user.ObjectId));


				var values = new List<NSObject>()
				{
					new NSString (Foundation.NSLocale.CurrentLocale.CountryCode),
					new NSString (Foundation.NSLocale.PreferredLanguages[0]),

					new NSString ((user.CreatedAt ?? new DateTime()).ToString ("r")),
					new NSString (boolToString(BlrtUserHelper.EmailVerified)),
					new NSString (user.Get<int>(BlrtUserHelper.UserGenderKey, 0).ToString()),
					new NSString (user.Get<string>(BlrtUserHelper.IndustryKey, "")),
					new NSString (user.Get<string>(BlrtUserHelper.ORGANISATION_NAME, "")),

					/*
					//TODO add this stuff
					new NSString (_customInfo.numContacts.ToString ()),
					new NSString (_customInfo.numRecentContacts.ToString ()),
					new NSString (_customInfo.numConvInbox.ToString ()),
					new NSString (_customInfo.numConvMy.ToString ()),
					new NSString (_customInfo.numConvArchive.ToString ()),
					new NSString (_customInfo.numConvFlagged.ToString ()),
					new NSString (_customInfo.numTags.ToString ())
					*/
				};
				// dictionary keys
				var keys = new List<NSObject>()
				{
					new NSString (FN_DEVICE_LOCALE),
					new NSString (FN_DEVICE_LANGUAGE),

					new NSString (FN_CREATED_AT),
					new NSString (FN_EMAIL_VERIFIED),
					new NSString (FN_GENDER),
					new NSString (FN_INDUSTRY),
					new NSString (FN_ORGANISATION),



					/*
					
					new NSString (FN_NUM_CONTACTS),
					new NSString (FN_NUM_RECENT_CONTACTS),
					new NSString (FN_NUM_CONV_INBOX),
					new NSString (FN_NUM_CONV_MY),
					new NSString (FN_NUM_CONV_ARCHIVE),
					new NSString (FN_NUM_CONV_FLAGGED),
					new NSString (FN_NUM_TAGS)
					*/
				};


				// set to config
				_config.CustomFields = NSDictionary.FromObjectsAndKeys (values.ToArray (), keys.ToArray ());

			}

			_config.ExtraTicketInfo = "";

			_config.ShowForum = false;
			_config.ShowPostIdea = false;
		}

		/// <summary>
		/// transfer Bools to string.
		/// </summary>
		/// <returns>The string of the bool.</returns>
		/// <param name="value">the bool value</param>
		private static string boolToString (bool value)
		{
			return (value ? STR_TRUE : STR_FALSE);
		}

		/// <summary>
		/// Represents the error that occurs when the user wants to show UserVoice interface or identify user before initialization
		/// </summary>
		public class NotInitializedException : InvalidOperationException
		{
			public NotInitializedException () : base ("BlrtUserVoice has to be initialized before doing this operation") {}
			public NotInitializedException (string message) : base (message) {}
		}
	}

	/// <summary>
	/// UserVoice custom fields for Blrt.
	/// </summary>
	public struct UVCustomInfo
	{
		public int numContacts;			// Number of contacts they have stored on their device - i.e. iPad/iPhone contacts
		public int numRecentContacts;	// Number of recent contacts they currently have
		public int numConvInbox;		// Number of conversations in the inbox
		public int numConvMy;			// Number of conversations in 'my blrts'
		public int numConvArchive;		// Number of archived conversations
		public int numConvFlagged;		// Number of flagged conversations
		public int numTags;				// Number of tags the user has created all up
	}
}

