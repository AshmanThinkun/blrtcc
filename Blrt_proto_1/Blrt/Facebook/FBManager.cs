using System;
using System.Collections.Generic;
using Foundation;
using Parse;
using System.Threading.Tasks;
using BlrtData;
using UIKit;
using System.Threading;
using Facebook.CoreKit;

namespace BlrtiOS
{
	public static partial class FBManager
	{
		public const string FB_FEED_NAME = "name";
		public const string FB_FEED_DESCRIPTION = "description";
		public const string FB_FEED_TO = "to";
		public const string FB_FEED_LINK = "link";
		public const string FB_FEED_CAPTION = "caption";


		public static bool IsLinked { get { return ParseUser.CurrentUser != null && ParseFacebookUtils.IsLinked (ParseUser.CurrentUser);} }


		public static bool FBConnected() {

			if (IsLinked) {
				var token = GetFacebookAuthInfo (ParseUser.CurrentUser);

				if (token != null
					&& AccessToken.CurrentAccessToken != null
					&& AccessToken.CurrentAccessToken.TokenString == token.Token
					&& AccessToken.CurrentAccessToken.ExpirationDate.ToDateTime() > DateTime.Now
				) {

					return true;
				}
			}

			return false;
		}

		static Facebook.LoginKit.LoginManager _loginManager;
		public static Facebook.LoginKit.LoginManager LoginManagerInstance{
			get{
				if(_loginManager==null){
					_loginManager = new Facebook.LoginKit.LoginManager ();
				}
				return _loginManager;
			}
		}

		public static void FetchDeferredAppLink()
		{
			Facebook.CoreKit.AppLinkUtility.FetchDeferredAppLink (new DeferredAppLinkHandler ((url, error) => {
				if(error!=null)
					return;
				if(url!=null){
					BlrtUserHelper.InstallMetaData = BlrtUtil.GetParameters(url.AbsoluteString);
				}
			}));
		}


		/// <summary>
		/// The user has logged in using parse
		/// </summary>
		public static async System.Threading.Tasks.Task LoginWithParse() {

			if (IsLinked) {

				if (FBConnected ())
					return;

				if (AccessToken.CurrentAccessToken != null) {
					LoginManagerInstance.LogOut ();
				}

				var authInfo = GetFacebookAuthInfo (ParseUser.CurrentUser);

				AccessToken.CurrentAccessToken = new AccessToken (
					authInfo.Token,
					BlrtConstants.FacebookExtendedPermissions,
					new string[]{},
					BlrtConstants.FacebookAppId,
					authInfo.ID,
					DateTime.Parse(authInfo.Expiration).ToNSDate(),
					null
				);
				var tcs = new TaskCompletionSource<bool> ();
				Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
					AccessToken.RefreshCurrentAccessToken (new GraphRequestHandler((connection, result, error) =>{
						if(error==null)
							tcs.TrySetResult(true);
						else
							tcs.TrySetResult(false);
					}));
				});
				await tcs.Task;
			}
		}

		public static async Task<string> FetchUserAvatar(){
			if (!IsLinked)
				return "blrt_error";
			else if(!FBConnected()){
				//connect
				await LoginWithParse ();
				int totalWait = 0;
				while(!FBConnected() && totalWait<5000){
					await System.Threading.Tasks.Task.Delay (500);
					totalWait += 500;
				}
				if(!FBConnected())
					return "blrt_error";
			}

			var tcs = new TaskCompletionSource<string> ();

			var graphPath = "me";
			var parameters = new NSMutableDictionary ();
			parameters.SetValueForKey(new NSString(string.Format("picture.width({0}).height({0}).redirect(false)",BlrtData.Contacts.Avatar.MaxSize.ToString ())), new NSString("fields"));

			var request = new GraphRequest (graphPath, parameters);
			request.Start(new GraphRequestHandler(((GraphRequestConnection connection, NSObject result, NSError error) => {
				if (error!=null) {
					// Handle error
					tcs.TrySetResult("blrt_error");
				}else {
					var picture = result.ValueForKey(new NSString("picture")) as NSDictionary;
					var data = picture.ValueForKey(new NSString("data"));
					NSNumber isSilhouette = data.ValueForKey(new NSString("is_silhouette")) as NSNumber;
					NSString url = data.ValueForKey(new NSString("url")) as NSString;

					if(isSilhouette == NSNumber.FromBoolean(true)){
						tcs.TrySetResult("");
					}else{
						string userImageURL = url.ToString();
						tcs.TrySetResult(userImageURL);
					}
				}
			})));

			return await tcs.Task;
		}

	
		public static async System.Threading.Tasks.Task DisconnectWithFacebook() {
			if (!IsLinked)
				throw new Exception ("User need to be logged in for to connect it to parse");

			if (BlrtUserHelper.ContactDetails == null || BlrtUserHelper.FacebookContact == null) {

				try {
					var query = BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ();
					var t2 = BlrtUtil.BetterFetchAsync (ParseUser.CurrentUser, new System.Threading.CancellationTokenSource (15000).Token);
					await System.Threading.Tasks.Task.WhenAll (query.WaitAsync (), t2);
				} catch {
					if (BlrtUserHelper.ContactDetails == null)
						throw;
				}
			}

			if (BlrtUserHelper.FacebookContact == null)
				throw new Exception ("No Facebook contact detail found for current user");

			var request = new BlrtAPI.APIUserContactDetailDelete (new BlrtAPI.APIUserContactDetailDelete.Parameters () {
				Id = BlrtUserHelper.FacebookContact.ObjectId
			});
			if (await request.Run (new System.Threading.CancellationTokenSource (15000).Token)) {
				if (request.Response.Success) {
					// By this point we know that the link to Facebook has been destroyed from CC
					// We'll just do our best effort to have the app realise that...
					ParseUser.CurrentUser["authData"] = new Dictionary<string, object> () { { "facebook", null } };

					try {
						var t1 = BlrtData.Query.BlrtSettingsQuery.RunQuery ().WaitAsync ();
						var t2 = ParseUser.CurrentUser.BetterSaveAsync (new CancellationTokenSource (15000).Token);
						await System.Threading.Tasks.Task.WhenAll (new System.Threading.Tasks.Task [] { t1, t2 });
					} catch {
					}
				} else if (request.Response.Error.Code != BlrtAPI.APIUserContactDetailDeleteError.RecordNotFound)
					throw new Exception ("Error occurred with request: " + request.Response.Error.Message);

				if (AccessToken.CurrentAccessToken != null){
					LoginManagerInstance.LogOut ();
				}
			} else
				throw request.Exception;

			IsLinked.ToString ();
		}


		public static void Logout ()
		{
			LoginManagerInstance.LogOut ();
		}



		public static AuthTokenInfo GetFacebookAuthInfo(ParseUser user)
		{
			try{
				var dic = user["authData"] as Dictionary<string, object>;
				var facebook = dic["facebook"] as Dictionary<string, object>;

				return  new AuthTokenInfo (
					facebook["id"].ToString(),
					facebook["access_token"].ToString(),
					Convert.ToDateTime(facebook["expiration_date"].ToString()),
					AuthTarget.Facebook
				);

			} catch {
			}
			//var data = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthData> (str);

			return null;// data.Facebook;
		}


		private static System.Threading.CancellationToken CancelToken(){
			return new System.Threading.CancellationTokenSource (1000 * 15).Token;
		}



		class ShareDel: Facebook.ShareKit.SharingDelegate
		{
			TaskCompletionSource<bool> _tcs;
			public ShareDel():base()
			{
				_tcs = new TaskCompletionSource<bool>();
			}
			#region implemented abstract members of SharingDelegate
			public override void DidCancel (Facebook.ShareKit.ISharing sharer)
			{
				_tcs.TrySetResult (true);
			}
			public override void DidComplete (Facebook.ShareKit.ISharing sharer, NSDictionary results)
			{
				_tcs.TrySetResult (true);
			}
			public override void DidFail (Facebook.ShareKit.ISharing sharer, NSError error)
			{
				_tcs.TrySetResult (false);
			}
			#endregion

			public Task<bool> WaitAsync(){
				return _tcs.Task;
			}
		}

		public static async void TrySendFBMessenger(NSUrl url, UIViewController controller){
			var urlContent = new Facebook.ShareKit.ShareLinkContent ();
			urlContent.SetContentUrl (url);
			var del = new ShareDel();
			Facebook.ShareKit.MessageDialog.Show (urlContent, del);

			var openned = await del.WaitAsync();
			if(!openned){// cannot present in messager

				if(BlrtHelperiOS.IsPhone){
					var alert = new UIAlertView (
						BlrtUtil.PlatformHelper.Translate("fb_messenger_install_title", "send_screen"),
						BlrtUtil.PlatformHelper.Translate("fb_messenger_install_message", "send_screen"),
						null,
						BlrtUtil.PlatformHelper.Translate("fb_messenger_install_cancel", "send_screen")
					);
					var installIndex = BlrtHelperiOS.IsPhone? alert.AddButton (BlrtUtil.PlatformHelper.Translate ("fb_messenger_install_accept", "send_screen")): -999;
					alert.Clicked += (object s, UIButtonEventArgs e) => {
						if(e.ButtonIndex == installIndex){
							if(UIApplication.SharedApplication.CanOpenUrl (new NSUrl("https://itunes.apple.com/en/app/id454638411?mt=8")))
								UIApplication.SharedApplication.OpenUrl (new NSUrl("https://itunes.apple.com/en/app/id454638411?mt=8"));																				
						}
					};
					alert.Show ();
				}else{
					var fbUrl = string.Format("https://www.facebook.com/dialog/send?app_id={0}&link={1}&redirect_uri=https://www.facebook.com/", 
						BlrtConstants.FacebookAppId, 
						System.Web.HttpUtility.UrlEncode(url.ToString()));
					UIApplication.SharedApplication.OpenUrl (NSUrl.FromString(fbUrl));
				}
			}
		}
	}
}

