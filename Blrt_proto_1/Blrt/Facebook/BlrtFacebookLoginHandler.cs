﻿using System;
using Acr.UserDialogs;
using BlrtData;
using Facebook.CoreKit;
using System.Threading.Tasks;
using Parse;
using System.Linq;
using System.Collections.Generic;
using BlrtAPI;
using System.Threading;
using Foundation;
using UIKit;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS
{
	public class BlrtFacebookLoginHandler
	{
		public void SystemCallback (Facebook.LoginKit.LoginManagerLoginResult result, Foundation.NSError error)
		{
			HandleSystemCallbackAsync (result, error).FireAndForget ();
		}

		async System.Threading.Tasks.Task HandleSystemCallbackAsync (Facebook.LoginKit.LoginManagerLoginResult result, Foundation.NSError error)
		{
			if (error != null) {
				#if DEBUG
				Console.WriteLine ("Error: " + error);
				#endif
			} else if (result != null && !result.IsCancelled) {
				if (ParseUser.CurrentUser == null) {
					PushLoading ();
					await Login (result);
				} else {
					//this is link user 
					await LinkFBUserAsync (result);
				}
			}
		}

		static async System.Threading.Tasks.Task LinkFBUserAsync (Facebook.LoginKit.LoginManagerLoginResult loginResult)
		{
			using (UserDialogs.Instance.Loading (BlrtUtil.PlatformHelper.Translate ("Loading..."))) {
				try {
					await ConnectWithFacebook (loginResult);
					BlrtData.Helpers.BlrtTrack.ProfileConnectService ("Facebook");
				} catch (System.Exception ex) {
					if (AccessToken.CurrentAccessToken != null) {
						FBManager.LoginManagerInstance.LogOut ();
					}

					bool alert = false;
					if (ex.Message == BlrtAPI.APIUserContactDetailCreateError.DetailInUse.ToString ()) {
						BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_user_exists_title", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_user_exists_message", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_user_exists_cancel", "profile")
						)).FireAndForget ();
						alert = true;
					}

					if (!alert) {
						BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_unknown_title", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_unknown_message", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_link_failed_unknown_cancel", "profile")
						)).FireAndForget ();

					}
				}
				await ReloadContactDetails (true);
			}

			Xamarin.Forms.MessagingCenter.Send<Object> ("", "FacebookLinkPage.UpdatePage");
		}

		public static async Task<bool> ReloadContactDetails (bool fetchUserObject = false)
		{
			if (null == ParseUser.CurrentUser) {
				return false;
			}
			if (fetchUserObject) {
				try {
					await ParseUser.CurrentUser.BetterFetchAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				} catch (ParseException) {
					return false;
				}
			}

			try {
				var query = BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ();
				await query.WaitAsync ();

				if (query.Failed) {
					if (!BlrtUserHelper.ContactDetails.Any ()) {
					}
					return false;
				}
			} catch {
			}

			return true;
		}

		public static async System.Threading.Tasks.Task ConnectWithFacebook (Facebook.LoginKit.LoginManagerLoginResult loginResult)
		{
			if (ParseUser.CurrentUser == null)
				throw new System.Exception ("User need to be logged in for to connect it to parse");

			var graphCallback = new GraphCallBack ();
			var parameters = new NSMutableDictionary ();
			parameters.SetValueForKey (new NSString ("id, first_name, last_name, email, gender, birthday, locale, link, name"), new NSString ("fields"));

			GraphRequest graphRequest = new GraphRequest ("me", parameters);
			graphRequest.Start (graphCallback.GetHandler ());

			var u = await graphCallback.GetResult ();

			var request = new BlrtAPI.APIUserContactDetailCreate (new BlrtAPI.APIUserContactDetailCreate.Parameters () {
				Token = new AuthTokenInfo (
					u ["id"],
					loginResult.Token.TokenString,
					loginResult.Token.ExpirationDate.ToDateTime (),
					AuthTarget.Facebook
				),
				Extra = new Dictionary<string, object> () {
					{ BlrtUserHelper.UserGenderKey, (int) BlrtUserHelper.GenderFromString (BlrtUtil.StringOrNull (u.ContainsKey("gender")? u["gender"]: null)) },
					{ BlrtUserHelper.FirstNameKey, BlrtUtil.StringOrNull (u.ContainsKey("first_name")? u["first_name"]: null) },
					{ BlrtUserHelper.LastNameKey, BlrtUtil.StringOrNull (u.ContainsKey("last_name")? u["last_name"]: null) },
					{ BlrtUserHelper.FacebookMetaId, BlrtUtil.StringOrNull (u["id"]) },
					{ BlrtUserHelper.FacebookMetaLink, BlrtUtil.StringOrNull (u.ContainsKey("link")? u["link"]: null) },
					{ BlrtUserHelper.FacebookMetaLocale, BlrtUtil.StringOrNull (u.ContainsKey("locale")? u["locale"]: null) },
					{ BlrtUserHelper.FacebookMetaName, BlrtUtil.StringOrNull (u.ContainsKey("name")? u["name"]: null) },
					{ BlrtUserHelper.FacebookMetaFirstName, BlrtUtil.StringOrNull (u.ContainsKey("first_name")? u["first_name"]: null) },
					{ BlrtUserHelper.FacebookMetaLastName, BlrtUtil.StringOrNull (u.ContainsKey("last_name")? u["last_name"]: null) }
				}
			});

			if (await request.Run (new System.Threading.CancellationTokenSource (15000).Token)) {
				if (request.Response.Success) {
					try {
						await BlrtData.Query.BlrtSettingsQuery.RunQuery ().WaitAsync ();
					} catch {
					}
				} else if (request.Response.Error.Code == BlrtAPI.APIUserContactDetailCreateError.DetailInUse)
					// TODO: Stop using dodgy code
					throw new System.Exception (BlrtAPI.APIUserContactDetailCreateError.DetailInUse.ToString ());
				else
					throw new System.Exception ("Error occurred with request: " + request.Response.Error.Message);
			} else
				throw request.Exception;
		}

		public class GraphCallBack
		{
			TaskCompletionSource<Dictionary<string, string>> _tcs;
			public GraphCallBack () : base ()
			{
				_tcs = new TaskCompletionSource<Dictionary<string, string>> ();
			}

			GraphRequestHandler _handler;
			public GraphRequestHandler GetHandler ()
			{
				if (_handler == null) {
					_handler = new GraphRequestHandler (((GraphRequestConnection connection, Foundation.NSObject result, Foundation.NSError error) => {
						if (error != null) {

						} else {
							var bFacebookData = GetFacebookData (result as NSDictionary);
							_tcs.TrySetResult (bFacebookData);
						}
					}));
				}

				return _handler;
			}


			public Task<Dictionary<string, string>> GetResult ()
			{
				return _tcs.Task;
			}

			private Dictionary<string, string> GetFacebookData (Foundation.NSDictionary result)
			{
				try {
					var bundle = new Dictionary<string, string> ();
					var id = result.ValueForKey (new NSString ("id")).ToString ();

					bundle.Add ("id", id);
					if (result.ContainsKey (new NSString ("first_name")))
						bundle.Add ("first_name", result.ValueForKey (new NSString ("first_name")).ToString ());
					if (result.ContainsKey (new NSString ("last_name")))
						bundle.Add ("last_name", result.ValueForKey (new NSString ("last_name")).ToString ());
					if (result.ContainsKey (new NSString ("email")))
						bundle.Add ("email", result.ValueForKey (new NSString ("email")).ToString ());
					if (result.ContainsKey (new NSString ("gender")))
						bundle.Add ("gender", result.ValueForKey (new NSString ("gender")).ToString ());
					if (result.ContainsKey (new NSString ("birthday")))
						bundle.Add ("birthday", result.ValueForKey (new NSString ("birthday")).ToString ());
					if (result.ContainsKey (new NSString ("locale")))
						bundle.Add ("locale", result.ValueForKey (new NSString ("locale")).ToString ());
					if (result.ContainsKey (new NSString ("link")))
						bundle.Add ("link", result.ValueForKey (new NSString ("link")).ToString ());
					if (result.ContainsKey (new NSString ("name")))
						bundle.Add ("name", result.ValueForKey (new NSString ("name")).ToString ());

					return bundle;
				} catch { }
				return new Dictionary<string, string> ();
			}

		}

		private async System.Threading.Tasks.Task Login (Facebook.LoginKit.LoginManagerLoginResult loginResult, string prefillEmail = null)
		{
			var graphCallback = new GraphCallBack ();
			var parameters = new NSMutableDictionary ();
			parameters.SetValueForKey (new NSString ("id, first_name, last_name, email, gender, birthday, locale, link, name"), new NSString ("fields"));
			GraphRequest graphRequest = new GraphRequest ("me", parameters);

			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				graphRequest.Start (graphCallback.GetHandler ()); // this method must be called in main thread
			});

			var u = await graphCallback.GetResult ();

			var metaName = BlrtUtil.StringOrNull (u.ContainsKey ("name") ? u ["name"] : null);
			var firstName = BlrtUtil.StringOrNull (u.ContainsKey ("first_name") ? u ["first_name"] : null);
			var lastName = BlrtUtil.StringOrNull (u.ContainsKey ("last_name") ? u ["last_name"] : null);
			var email = "";
			try {
				email = string.IsNullOrWhiteSpace (prefillEmail) ? BlrtUtil.StringOrNull (u.ContainsKey ("email") ? u ["email"] : null) : prefillEmail;
			} catch { }

			var _params = new APIUserRegister.Parameters () {
				Email = email,
				Name = metaName != null ? metaName : firstName + (lastName != null ? " " + lastName : ""),
				Token = new AuthTokenInfo (
					u ["id"],
					loginResult.Token.TokenString,
					loginResult.Token.ExpirationDate.ToDateTime (),
					AuthTarget.Facebook
				),
				Extra = new Dictionary<string, object> () { 
					{
						BlrtUserHelper.UserGenderKey,
						(int)BlrtUserHelper.GenderFromString (BlrtUtil.StringOrNull (BlrtUtil.StringOrNull (u.ContainsKey("gender")? u["gender"]: null)))
					},
					{ BlrtUserHelper.FirstNameKey, BlrtUtil.StringOrNull (u.ContainsKey("first_name")? u["first_name"]: null) },
					{ BlrtUserHelper.LastNameKey, BlrtUtil.StringOrNull (u.ContainsKey("last_name")? u["last_name"]: null) },
					{ BlrtUserHelper.FacebookMetaId, BlrtUtil.StringOrNull (u["id"]) },
					{ BlrtUserHelper.FacebookMetaLink, BlrtUtil.StringOrNull (u.ContainsKey("link")? u["link"]: null) },
					{ BlrtUserHelper.FacebookMetaLocale, BlrtUtil.StringOrNull (u.ContainsKey("locale")? u["locale"]: null) },
					{ BlrtUserHelper.FacebookMetaName, BlrtUtil.StringOrNull (u.ContainsKey("name")? u["name"]: null) },
					{ BlrtUserHelper.FacebookMetaFirstName, BlrtUtil.StringOrNull (u.ContainsKey("first_name")? u["first_name"]: null) },
					{ BlrtUserHelper.FacebookMetaLastName, BlrtUtil.StringOrNull (u.ContainsKey("last_name")? u["last_name"]: null) }
				}
			};

			var referer = BlrtData.Helpers.LoginHelper.GetRefererId ();
			if (!string.IsNullOrEmpty (referer)) {
				_params.Extra [BlrtUserHelper.ReferredByKey] = referer;
			}
			var signupUrl = BlrtData.Helpers.LoginHelper.GetSignupUrl ();
			if (!string.IsNullOrEmpty (signupUrl)) {
				_params.Extra ["signupUrl"] = signupUrl;
			}

			APIUserRegister request = new APIUserRegister (_params);

			try {
				if (!await request.Run (new CancellationTokenSource (15000).Token)) {
					throw new System.Exception ("UserRegister request failed to receive response: " + request.Exception.Message);
				}
				if (!request.Response.Success) {
					throw new System.Exception ("UserRegister failed: " + request.Response.Error.Message);
				}
				FBLoginFinish (loginResult, request.Response.Status.Code == APIUserRegisterStatus.AccountCreated, BlrtUserHelper.InstallMetaData?.ContainsKey ("sendToPhone") ?? false);
				BlrtUserHelper.InstallMetaData = null;
				BlrtPersistant.Properties ["HasSignedUpWithReferrerId"] = true;
				BlrtPersistant.Properties ["HasSignedUpWithSignupUrl"] = true;
				BlrtPersistant.SaveProperties ();

			} catch (System.Exception ex) {
				if (ParseUser.CurrentUser != null)
					ParseUser.LogOut ();

				PushLoading ("", true);

				// There's a chance that result.success == true (if the crash happened after creation)
				// That's why we need to check that result.error != null

				if (request.Response != null && request.Response.Error != null && request.Response.Error.Code == APIUserRegisterError.EmailInUse) {
					// email already in use
					var title = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_emailtaken_title", "login");
					var message = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_emailtaken_message", "login");
					var button = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_emailtaken_accept", "login");

					if (AccessToken.CurrentAccessToken != null)
						FBManager.LoginManagerInstance.LogOut ();

					BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (title, message, button));

					LLogger.WriteEvent ("login-fb", "failed", ex.ToString ());
					PopLoading ("", true);
				} else if (request.Response != null && request.Response.Error != null && request.Response.Error.Code == APIUserRegisterError.InvalidParameters) {
					// facebook register needs an email
					PopLoading ("", true);
					var useremail = await GetEmailAddressFromAlert ();
					if (!string.IsNullOrWhiteSpace (useremail)) {
						PushLoading ("", true);
						Login (loginResult, useremail);
					} else if (AccessToken.CurrentAccessToken != null) {
						//user canceled
						FBManager.LoginManagerInstance.LogOut ();
					}
				} else {
					// general fail retry
					var title = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_title", "login");
					var message = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_message", "login");
					var cancel = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_accept", "login");
					var accept = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_tryagain", "login");

					LLogger.WriteEvent ("login-fb", "failed", ex.ToString ());
					PopLoading ("", true);

					var tryagain = await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (title, message, cancel, accept));
					if (tryagain.Success && !tryagain.Result.Cancelled) {
						PushLoading ();
						Login (loginResult);
					} else if (AccessToken.CurrentAccessToken != null) {
						FBManager.LoginManagerInstance.LogOut ();
					}
				}
			}
		}


		async System.Threading.Tasks.Task FBLoginFinish (Facebook.LoginKit.LoginManagerLoginResult loginResult, bool isNew, bool signupWithSMSLink)
		{
			try {
				await ParseFacebookUtils.LogInAsync (
					loginResult.Token.UserId,
					loginResult.Token.TokenString,
					loginResult.Token.ExpirationDate.ToDateTime (),
					new CancellationTokenSource (15000).Token
				);
				PopLoading ();
			} catch {
				PopLoading ();
				string title;
				string message;
				string button;
				string tryagain;

				if (isNew) {
					title = BlrtUtil.PlatformHelper.Translate ("alert_register_then_login_failed_title", "login");
					message = BlrtUtil.PlatformHelper.Translate ("alert_register_then_login_failed_message", "login");
					button = BlrtUtil.PlatformHelper.Translate ("alert_register_then_login_failed_accept", "login");
					tryagain = BlrtUtil.PlatformHelper.Translate ("alert_register_then_login_failed_tryagain", "login");
				} else {
					title = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_title", "login");
					message = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_message", "login");
					button = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_accept", "login");
					tryagain = BlrtUtil.PlatformHelper.Translate ("alert_fb_failed_tryagain", "login");
				}

				var result = await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (title, message, button, tryagain));
				if (result.Success && !result.Result.Cancelled) {
					PushLoading ();
					FBLoginFinish (loginResult, isNew, signupWithSMSLink);
				} else if (AccessToken.CurrentAccessToken != null) {
					FBManager.LoginManagerInstance.LogOut ();
				}

				return;
			}

			if (BlrtData.BlrtManager.Login (ParseUser.CurrentUser, isNew, signupWithSMSLink))
				BlrtData.BlrtManager.Event.Fire<BlrtData.Events.BlrtLoginEvent> (new BlrtData.Events.BlrtLoginEvent (isNew ? BlrtData.Events.BlrtLoginEvent.LoginType.RegisterWithFacebook : BlrtData.Events.BlrtLoginEvent.LoginType.LoginWithFacebook));

			if (isNew)
				BlrtData.Helpers.BlrtTrack.AccountCreate ("Facebook");
			else
				BlrtData.Helpers.BlrtTrack.AccountSignIn ("Facebook");

			LLogger.WriteEvent ("login-fb", "finished");

		}

		void PushLoading (string cause = "", bool animate = true)
		{
			UserDialogs.Instance.ShowLoading ("Loading...");
		}

		void PopLoading (string cause = "", bool animate = true)
		{
			UserDialogs.Instance.HideLoading ();
		}


		async Task<string> GetEmailAddressFromAlert (string prefill = "")
		{
			var tcs = new TaskCompletionSource<string> ();
			var title = BlrtUtil.PlatformHelper.Translate ("enter_email_title", "login");
			var message = BlrtUtil.PlatformHelper.Translate ("enter_email_message", "login");
			var cancel = BlrtUtil.PlatformHelper.Translate ("enter_email_cancel", "login");
			var accept = BlrtUtil.PlatformHelper.Translate ("enter_email_accept", "login");

			var alert = new UIAlertView (title, message, null, cancel, accept);
			alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKeyboardType.EmailAddress;
			alert.GetTextField (0).Placeholder = BlrtUtil.PlatformHelper.Translate ("enter_email_placeholder", "login");
			alert.Clicked += (object sender, UIButtonEventArgs arg) => {
				if (arg.ButtonIndex != alert.CancelButtonIndex) {
					var useremail = alert.GetTextField (0).Text;
					// validate email
					if (!BlrtData.BlrtUtil.IsValidEmail (useremail)) {
						var invalidEmailAlert = new UIAlertView (
							BlrtHelperiOS.Translate ("alert_register_not_email_title", "login"),
							BlrtHelperiOS.Translate ("alert_register_not_email_message", "login"),
							null,
							BlrtHelperiOS.Translate ("alert_register_not_email_accept", "login")
						);

						invalidEmailAlert.Clicked += async (senders, se) => {
							tcs.TrySetResult (await GetEmailAddressFromAlert (useremail));
						};
						invalidEmailAlert.Show ();
					} else {
						//user enter valid email
						tcs.TrySetResult (useremail);
					}
				} else {
					//user cancel
					tcs.TrySetResult ("");
				}
			};
			alert.Show ();

			return await tcs.Task;
		}
	}
}

