using System;
using System.Linq;
using System.Collections.Generic;
using Foundation;
using Parse;
using System.Threading.Tasks;
using BlrtData.Contacts;
using Facebook.CoreKit;

namespace BlrtiOS
{
	public class FBFriend
	{
		public string fbId;
		public string name;
		public string email;

		public bool inviteOnly;


		public string parseObjectId;
		public string parseName;

		public DateTime UpdatedAt;



		public BlrtContact ToContact ()
		{
			var contact = new BlrtContact () {
				Name = name,
				State = BlrtContactForm.Facebook,
				NonBlrtUserUpdatedAt = UpdatedAt
			};

			contact.Add (new BlrtContactInfo (BlrtContactInfoType.FacebookId, fbId));

			if (!string.IsNullOrWhiteSpace (parseObjectId))
				contact.Add (new BlrtContactInfo (BlrtContactInfoType.BlrtUserId, parseObjectId));
			if (!string.IsNullOrWhiteSpace (email))
				contact.Add (new BlrtContactInfo (BlrtContactInfoType.Email, email));	
		
		
			return contact;
		}
	}



	public static partial class FBManager
	{

		private static Dictionary<string, BlrtContact> _deletedFBContact;
		public static void RemoveFacebookContacts ()
		{
			BlrtContactManager.SharedInstanced.RemoveForm (BlrtContactForm.Facebook);

			//remove contact type
			var type = BlrtContactInfoType.FacebookId;
			_deletedFBContact = new Dictionary<string, BlrtContact> ();
			foreach(var contact in BlrtContactManager.SharedInstanced.ToList(true)){

				for (int i = 0; i < contact.InfoList.Count; ++i) {
					if (contact.InfoList [i].ContactType == type) {
						_deletedFBContact [contact.InfoList [i].ContactValue] = contact;
						contact.Remove (contact.InfoList [i]);
						--i;
					}
				}
			}
		}

	

		public static async System.Threading.Tasks.Task AddFBToContacts ()
		{
			try {
				if (!FBConnected ()) {
					await LoginWithParse ();
					return;
				}

				RemoveFacebookContacts ();
				var friends = await GetFriends ();

				BlrtContactManager.SharedInstanced.Add (
					(from f in friends
					 select f.ToContact ())			
				);

				BlrtContactManager.SharedInstanced.Save ();


			} catch (Exception lne) {
				lne.ToString ();
			}
		}

		public static bool HasPermissions {
			get {
				return IsLinked && AccessToken.CurrentAccessToken.Permissions.Contains ("user_friends");
			}
		}

		

		public static async Task<FBFriend[]> GetFriends ()
		{
			FBFriend[] friends = new FBFriend[0];


			if (HasPermissions) {
				friends = await GetAllowedFriends (null);
			}


			return friends;
		}

		static TaskCompletionSource<NSObject> _tcs;

		static void OnFriendGot (GraphRequestConnection connection, NSObject result, NSError error)
		{
			_tcs?.TrySetResult (result);
		}

		private static async Task<FBFriend[]> GetAllowedFriends (string url = null)
		{
			if (!IsLinked)
				return null;
			_tcs = new TaskCompletionSource<NSObject> ();
			
			var request = new GraphRequest (url==null?"me/friends":url, new NSDictionary(new NSString("fields"), new NSString()));
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				request.Start(new GraphRequestHandler(OnFriendGot));
			});
			var result = await _tcs.Task as NSDictionary;

			if (null == result) {
				return new FBFriend[0];
			}

			var data = result.ObjectForKey (new NSString("data")) as NSArray;
			var paging = result.ObjectForKey (new NSString("paging")) as NSDictionary;


			if (data == null || data.Count <= 0)
				return new FBFriend[0];


			var friends = new List<FBFriend> ();

			for (nuint i = 0; i < data.Count; ++i) {
				var user = data.GetItem<NSObject> (i);

				var friend = new FBFriend () {
					fbId = user.ValueForKey(new NSString("id")).ToString(),
					name = user.ValueForKey(new NSString("name")).ToString(),
					inviteOnly = false
				};

				friends.Add (friend);
			}

			friends = await GetFriendsParseAction (friends);

			if(paging.ContainsKey(new NSString("next"))){
				var next = paging.ObjectForKey (new NSString("next")).ToString ();
				if (!string.IsNullOrWhiteSpace (next))
					friends.AddRange (await GetAllowedFriends (next));
			}

			return friends.ToArray ();
		}

	

		public static async Task<List<FBFriend>> GetFriendsParseAction (List<FBFriend> friends)
		{
			try {
				var dic = new List<Dictionary<string,string>> ();

				for (int i = 0; i < friends.Count; ++i) {
					if (_deletedFBContact != null) {
						BlrtContact del = null;
						if (_deletedFBContact.TryGetValue (friends [i].fbId, out del)
							&& del != null
							&& del.NonBlrtUserUpdatedAt.AddDays (1) >= DateTime.Now) 
						{ //if less than 1 day, load local data
							if (del.InfoList != null && del.InfoList.Where (info => info.ContactType == BlrtContactInfoType.BlrtUserId).Count () > 0) { //if blrt user
								friends [i].parseObjectId = del.InfoList.Where (info => info.ContactType == BlrtContactInfoType.BlrtUserId).First ().ContactValue;
							}
							friends [i].UpdatedAt = del.NonBlrtUserUpdatedAt;
							continue;
						}
					}
					dic.Add (new Dictionary<string, string> () {
						{ "value", friends [i].fbId },
						{ "type", "facebook" }
					});
					friends [i].UpdatedAt = DateTime.Now;
				}

				IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType> result = null;
				try {
					result = await BlrtData.Query.BlrtParseActions.FindUsersFromContacts (dic, new System.Threading.CancellationTokenSource (15000).Token);
				} catch {}

				if (result?.Count () > 0) {
					foreach (var user in result) {
						var friend = friends.FirstOrDefault (f => f.fbId == user.facebookId);
						if (friend != null) {
							friend.parseObjectId = user.id;
							friend.parseName = user.name;
							friend.email = user.email;
						}
					}
				}
			} catch {}

			return friends;
		}
	}
}

