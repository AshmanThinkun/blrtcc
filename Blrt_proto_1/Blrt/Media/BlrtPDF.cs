using System;
using CoreGraphics;
using System.IO;
using Foundation;
using System.Collections.Generic;
using System.Linq;


namespace BlrtiOS.Media
{
	public class BlrtPDF
	{
		public NSUrl Url { get; protected set; }

		public CGPDFDocument Document { get; protected set; }

		public int Pages {
			get {
				return (int)(Document.Pages);
			}
		}



		private BlrtPDF (NSUrl url)
		{
			Url = url;
			Document = CGPDFDocument.FromUrl (url.AbsoluteString);
		}

		public CGSize GetPageSize (int page)
		{
			var p = Document.GetPage (page);
			var output = p.GetBoxRect (CGPDFBox.Media);

			if ((p.RotationAngle % 180) == 90)
				output = new CGRect (0, 0, output.Height, output.Width);

			return output.Size;
		}


		public static BlrtPDF FromFile (string filename)
		{
			return FromUrl (NSUrl.FromFilename (filename));
		}

		static Dictionary<string, BlrtPDF> _pdfFiles;

		static Dictionary<string, BlrtPDF> PDFFiles {
			get {
				if (null == _pdfFiles) {
					_pdfFiles = new Dictionary<string, BlrtPDF> ();
				}
				return _pdfFiles;
			}
		}

		const int MaxHoldingPDF = 5;
		static List<string> _pdfFrequentUse;

		static List<string> PDFFrequentUse {
			get {
				if (null == _pdfFrequentUse) {
					_pdfFrequentUse = new List<string> ();
				}
				return _pdfFrequentUse;
			}
		}

		public static BlrtPDF FromUrl (NSUrl url)
		{
			string absUrlStr = url.AbsoluteString;
			BlrtPDF pdf;
			var canGet = PDFFiles.TryGetValue (absUrlStr, out pdf);
			if ((!canGet) || (canGet && ((null == pdf) || (null == pdf.Document)))) {
				pdf = new BlrtPDF (url);
				PDFFiles [absUrlStr] = pdf;
				if (PDFFrequentUse.Contains (absUrlStr)) {
					PDFFrequentUse.Remove (absUrlStr);
				}
				PDFFrequentUse.Insert (0, absUrlStr);
				if (PDFFrequentUse.Count > MaxHoldingPDF) {
					PDFFiles [PDFFrequentUse [PDFFrequentUse.Count - 1]].Release ();
				}
			}

			if (pdf.Document == null)
				return null;

			return pdf;
		}

		public static void ReleaseAll ()
		{
			if (PDFFiles.Count <= 0) {
				return;
			}
			var files = PDFFiles.Values.ToArray ();
			for (int i = 0; i < files.Length; i++) {
				files [i].Release ();
				files [i] = null;
			}
			PDFFiles.Clear ();
			files = null;
		}

		public System.IO.Stream AsStream ()
		{
			return new FileStream (Url.Path, FileMode.Open, FileAccess.Read, FileShare.Read);
		}

		public void Release ()
		{
			if (null != Document) {
				Document.Dispose ();
				Document = null;
			}
			if (null != Url) {
				PDFFiles.Remove (Url.AbsoluteString);
				PDFFrequentUse.Remove (Url.AbsoluteString);
				Url.Dispose ();
				Url = null;
			}
		}

	}
}

