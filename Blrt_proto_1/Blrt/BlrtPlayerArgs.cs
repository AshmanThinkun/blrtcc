//using System;
//using System.IO;
//
//
//using BlrtData;
//using BlrtData.Contents;
//using BlrtData.Media;
//
//namespace BlrtiOSDDelete
//{
////	public interface IBlrtPlayerArgs{
////
////
////		string LocalAudioPath		{ get; }
////		string LocalTouchDataPath	{ get; }
////
////
////		bool IsDraft 				{ get; }
////		bool IsEditable 			{ get; }
////
////
////		BlrtMediaWrapper MediaWrapper { get; }
////		string Name { get; }
////
////
////		string RecepientQueryId 	{ get; }
////		string[] DownloadMediaUrls 	{ get; }
////		string[] RecepientEmails 	{ get; }
////		string CaptureWebsite 		{ get; }
////
////		BlrtPlayerArgs Parent 		{ get; }
////
////		BlrtPlayerArgs CreateChild();
////		void MarkViewed ();
////
////
////
////
////
////	}
//
//	public class BlrtPlayerArgs// : IBlrtPlayerArgs
//	{
//		ConversationUserRelation	_relation;
//		Conversation 				_conversation;
//		ConversationBlrtItem 		_content;
//		BlrtMediaWrapper 			_media;
//
//		public bool Play {
//			get;
//			set;
//		}
//
//		string _name;
//
//		public Conversation Conversation {
//			get { 
//				if (_relation != null)
//					return _relation.Conversation;
//				return _conversation;
//			}
//		}
//		public ConversationBlrtItem 		Content{ get { return _content; } }
//
//		public bool IsDraft { get { return _relation == null; } }
//		public bool IsEditable { get{ return _content == null; } }
//
//
//
//		public string LocalAudioPath		{	get { return _content 	!= null ? _content.LocalAudioPath 		: ""; } }
//		public Uri	CloudAudioPath			{	get { return _content 	!= null ? _content.CloudAudioPath 		: null; } }
//
//		public string LocalTouchDataPath	{	get { return _content 	!= null ? _content.LocalTouchDataPath 	: ""; } }
//		public Uri	CloudTouchDataPath		{	get { return _content 	!= null ? _content.CloudTouchDataPath 	: null; } }
//
//
//
//		public BlrtMediaWrapper MediaWrapper {
//			get { return _media; }
//			set { if(IsEditable) _media = value; }
//		}
//		public string Name {
//			get{ 
//				if(Conversation != null)
//					return Conversation.Name;
//				return _name;
//			}
//			set{ if (IsDraft) _name = value; }
//		}
//
//
//		public string RecepientQueryId {
//			get;
//			set;
//		}
//		public string[] DownloadMediaUrls {
//			get;
//			set;
//		}
//		public string[] RecepientEmails {
//			get;
//			set;
//		}
//		public string CaptureWebsite {
//			get;
//			set;
//		}
//
//		public string AttachedMeta { get; set; }
//		public string RequestId { get; set; }
//
//
//		public event EventHandler LeaveCanvas;
//
//
//
//		public BlrtPlayerArgs Parent { get; private set; }
//
//		public BlrtPlayerArgs CreateChild(){
//			return new BlrtPlayerArgs (_relation, MediaWrapper){
//				Parent = this
//			};
//		}
//
//
//
//
//
//
//		public BlrtPlayerArgs (ConversationUserRelation relation, ConversationBlrtItem content)
//		{
//			_relation = relation;
//			_content = content;
//
//			Play = true;
//			
//			Name = Conversation.Name;
//
//
//
//			_media = BlrtMediaWrapper.FromContentItem (content);
//		}
//
//		public BlrtPlayerArgs (ConversationUserRelation relation) : this(relation, (BlrtMediaWrapper)null) {
//		}
//
//		public BlrtPlayerArgs (ConversationUserRelation relation, BlrtMediaWrapper media) {
//			_relation = relation;
//			_content = null;
//
//			Name = Conversation.Name;
//			_media = media;
//		}
//
//		private BlrtPlayerArgs () {
//			_relation = null;
//
//			_media = null;
//			_content = null;
//		}
//
//		public static BlrtPlayerArgs CreateDraft()
//		{
//			var b = new BlrtPlayerArgs (){
//			};
//
//			return b;
//		}
//
//		public void SetContent (Conversation conversation, ConversationBlrtItem content)
//		{
//			if (IsDraft) {
//
//				_conversation 	= conversation;
//				_content 		= content;
//			} else {
//				if (IsEditable)
//					_content = content;
//			}
//		}
//
//		public void OnLeaveCanvas ()
//		{
//			if (LeaveCanvas != null) {
//				LeaveCanvas (this, EventArgs.Empty);
//			}
//		}
//
//
//
//		public void MarkViewed ()
//		{
//			if (_relation != null && _content != null) {
//			
//				_relation.MarkViewed (_content);
//			}
//		}
//	}
//}
