using System;
using CoreGraphics;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Foundation;
using UIKit;
 
using BlrtData;
using System.Threading.Tasks;
using BlrtData.Instructions;
using BlrtiOS.ViewControllers;
using BlrtData.ExecutionPipeline;
using BlrtiOS.Views.Util;
using ObjCRuntime;

namespace BlrtiOS
{
	public static class BlrtHelperiOS
	{
		public static bool IsPhone{ get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; } }
		public static bool IsPhabletOrBigger { get { return NMath.Min (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) >= 414; } }
		public static bool IsIOS8Above{ get { return UIDevice.CurrentDevice.CheckSystemVersion(8, 0); } }


		public static string DateToString (DateTime date)
		{
			if (IsPhone)
				return date.ToString ("g");
			else
				return date.ToString ("f");
		}

		public static string TranslateDate (DateTime date)
		{
			if (DateTime.Today == date.Date || (DateTime.Now.AddHours (-8)).CompareTo (date) < 0) {
				return date.ToString (BlrtUtil.TIME_FORMAT).ToLower ();
			}
			return string.Format (BlrtUtil.GetDateString (date, BlrtUtil.LONG_DATE_FORMAT));
		}

		public static string TranslateDateLong (DateTime date)
		{

			var time = date.ToString (BlrtUtil.TIME_FORMAT).ToLower ();

			if (DateTime.Today == date.Date) {
				return string.Format ("{0}", time);
			}
			return string.Format ("{0}\n{1}", time, BlrtUtil.GetDateString (date, BlrtUtil.LONG_DATE_FORMAT));
		}

		public static UIBarButtonItem OpenDrawerBackButton (UIViewController controller, Action afterTriggered = null)
		{
			return OpenDrawerButton (controller, UIImage.FromBundle ("back-arrow.png"), afterTriggered);
		}

		public static UIBarButtonItem OpenDrawerMenuButton (UIViewController controller)
		{
			return OpenDrawerButton (controller, UIImage.FromBundle ("tabbar/menu_icon.png"));
		}

		public static UIBarButtonItem OpenDrawerButton (UIViewController controller, UIImage image, Action afterTriggered = null)
		{
			return new UIBarButtonItem (
				image, 
				UIBarButtonItemStyle.Plain, 
				(object sender, EventArgs e) => {
					BlrtHelperiOS.OpenSlideDrawer (controller, true);
					if (null != afterTriggered) {
						afterTriggered ();
					}
				}
			);
		}

		public class OpenDrawerWithUnread:UIBarButtonItem
		{
			UILabel unreadLbl;
			public OpenDrawerWithUnread(UIViewController controller): base(){
				var imgView = new UIButton(){
					Frame = new CoreGraphics.CGRect(0,0,27,16),
				};
				imgView.SetImage (UIImage.FromFile ("tabbar/menu_icon.png"), UIControlState.Normal);
				unreadLbl = new UILabel(){
					Frame = new CoreGraphics.CGRect(imgView.Frame.Right-4, imgView.Frame.Top -10, 16,16),
					ClipsToBounds = true,
					BackgroundColor = BlrtStyles.iOS.AccentRed,
					TextColor = UIColor.White,
					TextAlignment = UITextAlignment.Center,
					Text = BlrtUserHelper.UnreadUserMessageCount.ToString(),
					Font = BlrtStyles.iOS.CellDetailFont,
					Hidden = BlrtUserHelper.UnreadUserMessageCount == 0
				};
				unreadLbl.Layer.CornerRadius = 8;
				imgView.AddSubview (unreadLbl);

				imgView.TouchUpInside += (sender, e) => {
					BlrtHelperiOS.OpenSlideDrawer(controller,true);
				};

				Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "ContactChanged", UpdateUnread);
				Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "DisplaynameChanged", UpdateUnread);
				Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "PasswordChanged", UpdateUnread);
				this.CustomView = imgView;
			}

			public void UpdateUnread(object o){
				BeginInvokeOnMainThread (() => {
					unreadLbl.Text = BlrtUserHelper.UnreadUserMessageCount.ToString ();
					unreadLbl.Hidden = BlrtUserHelper.UnreadUserMessageCount == 0;
				});
			}
		}

		public static UIBarButtonItem OpenDrawerMenuButton (UIViewController controller, string title)
		{
			var btn = UIButtonWithImageAndText (UIImage.FromBundle ("tabbar/menu_icon.png"), title);
			btn.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			btn.Frame = new CGRect (0, 0, 100, 50);
			btn.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			btn.TouchUpInside += (object sender, EventArgs e) => {
				BlrtHelperiOS.OpenSlideDrawer (controller, true);
			};
			btn.SizeToFit ();
			return new UIBarButtonItem (btn);
		}


		public static UIButton UIButtonWithImageAndText (UIImage image, string title)
		{
			const int LeftBtnSpace = 10;

			var myBtn = new UIButton () {
			};
			myBtn.SetImage (image, UIControlState.Normal);
			myBtn.SetTitle (title, UIControlState.Normal);
			myBtn.SetTitleColor (UIColor.Black, UIControlState.Normal);
			myBtn.TitleEdgeInsets = new UIEdgeInsets (0, LeftBtnSpace, 0, 0);


			return myBtn;
		}


		public static void OpenSlideDrawer (this UIViewController controller, bool animated)
		{
		
			var c = controller;

			while (c.ParentViewController != null) {
			
				if (c.ParentViewController is Views.Util.SlideDrawerViewController) {
					var slider = c.ParentViewController as Views.Util.SlideDrawerViewController;
					slider.ShowSlider (true);
					return;
				}

				c = c.ParentViewController;
			}
		}



		public static NSMutableAttributedString AttributeString (string text, string prefix, UIStringAttributes attr)
		{
			return AttributeString (new NSMutableAttributedString (text), prefix, attr);
		}

		public static bool ScreenWidthLessThanOrEqualTo (int width)
		{
			return UIApplication.SharedApplication.KeyWindow.Frame.Width <= width;
		}


		public static NSMutableAttributedString AttributeString (NSMutableAttributedString text, string prefix, UIStringAttributes attr)
		{
			int start, end;
			//set attr between two prefix(or pos1 to the end of the string), and then remove these two prefixs before next loop
			while ((start = text.Value.IndexOf (prefix)) != -1) {
				text.DeleteRange (new NSRange (start, prefix.Length));
				end = text.Value.IndexOf (prefix, start);
				if (end == -1)
					end = text.Value.Length;
				else
					text.DeleteRange (new NSRange (end, prefix.Length));
				text.AddAttributes (attr, new NSRange (start, end - start));
			}

			return text;
		}


		public static CGSize GetSizeOfString (string text, UIStringAttributes stringAttr, nfloat maxWidth)
		{
			NSString tmpNSStr = null;

			if (text != null)
				tmpNSStr = new NSString (text);
			else
				tmpNSStr = new NSString ();


			return tmpNSStr.GetBoundingRect (
				new CGSize (maxWidth, nfloat.MaxValue),
				NSStringDrawingOptions.UsesLineFragmentOrigin,
				stringAttr,
				new NSStringDrawingContext ()
			).Size;
		}


		public static CGSize GetSizeOfString (NSAttributedString str, float maxWidth)
		{
			return str.GetBoundingRect (
				new CGSize (maxWidth, nfloat.MaxValue),
				NSStringDrawingOptions.UsesLineFragmentOrigin,
				new NSStringDrawingContext ()
			).Size;
		}


		public static async Task AwaitAlert (UIAlertView alert)
		{
			bool displaying = true;
			new NSObject ().InvokeOnMainThread (() => {
				alert.Clicked += (object sender, UIButtonEventArgs e) => {
					displaying = false;
				};
			});
				
			while (displaying)
				await Task.Delay (200);
		}


		public static DateTime ToDateTime (this NSDate date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime (new DateTime (2001, 1, 1, 0, 0, 0));
			return reference.AddSeconds (date.SecondsSinceReferenceDate);
		}

		public static NSDate ToNSDate (this DateTime date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime (new DateTime (2001, 1, 1, 0, 0, 0));
			return NSDate.FromTimeIntervalSinceReferenceDate ((date - reference).TotalSeconds);
		}


		public static bool TryValidateConversationName(string title)
		{
			if (string.IsNullOrWhiteSpace (title) || title.Trim().Length >= 56) {

				new UIAlertView (
					BlrtHelperiOS.Translate ("blrt_name_invalid_title", "preview"),
					BlrtHelperiOS.Translate ("blrt_name_invalid_message", "preview"),
					null,
					BlrtHelperiOS.Translate ("blrt_name_invalid_accept", "preview"))
					.Show ();
				return false;
			}

			return true;
		}


		public static string Translate(string key)
		{
			return BlrtUtil.PlatformHelper.Translate (key);
		}

		public static string Translate(string key, string table)
		{
			return BlrtUtil.PlatformHelper.Translate (key,table);
		}


		private static Dictionary<string, UIImage> _dic;
		public static UIImage FromBundleTemplate(string file)
		{
			if (_dic == null)
				_dic = new Dictionary<string, UIImage> ();

			if (_dic.ContainsKey (file))
				return _dic[file];

			_dic.Add (file, UIImage.FromBundle (file).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate));

			return _dic [file];
		}

		public static bool IsChild (UIView childView, UIView superView)
		{
			if (childView == null || superView == null)
				return false;

			for (int i = superView.Subviews.Length - 1; i > -1; --i) {

				if (superView.Subviews [i] == childView)
					return true;

				if (superView.Subviews [i].Subviews.Length > 0) {
					if (IsChild (childView, superView.Subviews [i]))
						return true;
				}
			}

			return false;
		}


		public static CGRect GetFrameInSuperView (UIView super, UIView subject)
		{
			return new CGRect (
				super.ConvertPointFromView (CGPoint.Empty, subject),
				subject.Bounds.Size);
		}

		public static CGSize SizeOfSubViews (UIView view)
		{
			CGSize output = new CGSize ();

			for (int i = 0; i < view.Subviews.Length; ++i) {

				var s = SizeOfSubViews (view.Subviews [i]);

				output.Width 	= NMath.Max (output.Width, 	view.Subviews [i].Frame.X + NMath.Max (view.Subviews [i].Bounds.Width,	s.Width));
				output.Height 	= NMath.Max (output.Height, view.Subviews [i].Frame.Y + NMath.Max (view.Subviews [i].Bounds.Height,	s.Height));
			}

			return output;
		}

		static string KBIntToMBString (this int i)
		{
			return (i / 1024f).ToString ("##.#") + " MB";
		}

		public static UIAlertView ShowUpgradeDialog (BlrtPermissions.PermissionEnum permission, long value = -1) {

			string valueStr 		= "";
			string currentLimit 	= "";
			string premiumLimit 	= "";
			string AccountHitLimitLabel = "";
			bool upgrade 	= true;

			string message = "permission_limit";


			LLogger.WriteEvent("Blrt Permission", "Limit Reached", null, permission.ToString());

			switch(permission){
				case BlrtPermissions.PermissionEnum.MaxBlrtDuration:
					message 		= "max_blrt_duration";
					valueStr	 	= (BlrtPermissions.MaxBlrtDuration / 60).ToString();
					currentLimit 	= (BlrtPermissions.MaxBlrtDuration / 60).ToString();
					premiumLimit	= (BlrtSettings.AdvertisedPremiumValues.MaxBlrtDuration / 60).ToString();
					AccountHitLimitLabel = "Blrt length";
					break;

				case BlrtPermissions.PermissionEnum.MaxBlrtPageCount:
					message 		= "max_pages";
					valueStr 		= value.ToString ();
					currentLimit 	= BlrtPermissions.MaxBlrtPageCount.ToString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxBlrtPageCount.ToString ();
					AccountHitLimitLabel = "page";
				break;

				case BlrtPermissions.PermissionEnum.MaxConvoSizeKB:
					message 		= "max_convo_size";
					valueStr 		= value.ToString ();
					currentLimit 	= BlrtPermissions.MaxConvoSizeKb.KBIntToMBString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxConvoSizeKB.KBIntToMBString ();
					break;

				case BlrtPermissions.PermissionEnum.MaxBlrtSizeKB:
					message 		= "max_blrt_size";
					valueStr 		= value.ToString ();
					currentLimit 	= BlrtPermissions.MaxBlrtSizeKb.KBIntToMBString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxBlrtSizeKB.KBIntToMBString ();
					AccountHitLimitLabel = "PDF";
					break;

				case BlrtPermissions.PermissionEnum.MaxPDFSizeKB:
					message 		= "max_pdf_size";
					valueStr 		= (value / 1024f).ToString ("F2");
					currentLimit 	= (BlrtPermissions.MaxPDFSizeKB / 1024f).ToString ("##.#") + " MB";
					premiumLimit 	= (BlrtSettings.AdvertisedPremiumValues.MaxPdfSizeKb / 1024f).ToString ("##.#") + " MB";
					AccountHitLimitLabel = "PDF";
					break;

				case BlrtPermissions.PermissionEnum.CanMediaAddOnReblrt:
					message 		= "can_add_on_reblrt";
					break;

				case BlrtPermissions.PermissionEnum.CanMediaReorder:
					message 		= "can_reorder";
					break;

				case BlrtPermissions.PermissionEnum.CanMediaShowHide:
					message 		= "can_showhide";
					break;

				case BlrtPermissions.PermissionEnum.MaxImageResolution:
					message 		= "max_resolution";
					valueStr		= value.ToString();
					currentLimit 	= BlrtPermissions.MaxImageResolution.ToString();
					premiumLimit	= BlrtSettings.AdvertisedPremiumValues.MaxImageResolution.ToString();
					upgrade = false;
					AccountHitLimitLabel = "MaxImageResolution";
					break;

				case BlrtPermissions.PermissionEnum.CanChangeAllowAdd:
					message 		= "can_allowAdd";
					break;

				case BlrtPermissions.PermissionEnum.CanChangeAllowPublic:
					message 		= "can_allowPublic";
					break;

				case BlrtPermissions.PermissionEnum.MaxConversationUsers:
				case BlrtPermissions.PermissionEnum.CanCreateConversation:
				case BlrtPermissions.PermissionEnum.CanCreateComment:
				case BlrtPermissions.PermissionEnum.CanCreateReblrt:
				case BlrtPermissions.PermissionEnum.CanAddUsers:
				case BlrtPermissions.PermissionEnum.CanUseWebViewer:
				case BlrtPermissions.PermissionEnum.CanUseDesktopViewer:
					return null;
			} 

			LLogger.WriteEvent ("Blrt Permissions", "Limit reached alert", null, permission.ToString());

			BlrtData.Helpers.BlrtTrack.AccountHitLimit (AccountHitLimitLabel);

			bool trial 		= !BlrtUserHelper.HasDoneFreeTrial;
			bool free 		= BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free;


			if (free) {
				message += "_free";
			}
			UIAlertView alert = null;
			new NSObject ().InvokeOnMainThread (() => {
				alert = new UIAlertView (
					string.Format (BlrtHelperiOS.Translate (message + "_title", 	"upgrade"), valueStr, currentLimit, premiumLimit),
					string.Format (BlrtHelperiOS.Translate (message + "_message", 	"upgrade"), valueStr, currentLimit, premiumLimit),
					null,
					BlrtHelperiOS.Translate (message + "_cancel", 	"upgrade")
				);

				if (upgrade && free) {
					alert.AddButton (BlrtHelperiOS.Translate (message + "_upgrade" + (trial ? "_trial" : ""), "upgrade"));
				}

				var tcs = new TaskCompletionSource<bool> ();
				var blocker = new SimpleBlocker (async () => {
					return await tcs.Task;
				});
				alert.Clicked += (object sender, UIButtonEventArgs e) => {
					if (e.ButtonIndex != alert.CancelButtonIndex)
						BlrtManager.App.OpenUpgradeAccount (BlrtData.ExecutionPipeline.Executor.System ());
					tcs.SetResult (e.ButtonIndex == alert.CancelButtonIndex);
				};
				alert.Dismissed += (sender, e) => {
					ExecutionPerformer.UnregisterBlocker (blocker);
				};
				ExecutionPerformer.RegisterBlocker (blocker);

				alert.Show ();
			});

			return alert;
		}

		public static Task<bool> ShowWaitingProgressAlertAsync (BlrtData.IO.BlrtDownloader[] downloaders, string title, string message, string cancel)
		{
			return ShowWaitingProgressAlertAsync (
				new BlrtData.IO.BlrtDownloaderList( downloaders ),
				title, message, cancel
			);
		}

		public static async Task<bool> ShowWaitingProgressAlertAsync (IProgress source, string title, string message, string cancel)
		{
			var alert = new BlrtiOS.UI.ProgressAwaitAlertView (
				source,
				title, 
				message,
				cancel
			);
			alert.Show ();

			try {
				await alert.WaitAsync();
			} finally {
				alert.DismissWithClickedButtonIndex (alert.CancelButtonIndex, true);
			}

			return alert.WaitAsync().Result;
		}

		public static async Task<bool> ShowWaitingAlertAsync (Task task, string title, string message, string cancel)
		{
			var alert = new BlrtiOS.UI.TaskAwaitAlertView (
		        title, 
				message,
	            cancel,
	            task
            );
			alert.Show ();

			try {
				await alert.WaitAsync();
			} finally {
				alert.DismissWithClickedButtonIndex (alert.CancelButtonIndex, true);
			}

			return alert.WaitAsync().Result;
		}

		//public static UIAlertView GenericAlert (string label, string table, params string[] formatStrings) {
		//	var alert =  new UIAlertView (String.Format (BlrtHelperiOS.Translate (label + "_title", table), formatStrings),
		//		String.Format (BlrtHelperiOS.Translate (label + "_message", table), formatStrings),
		//		null, 
		//		String.Format (BlrtHelperiOS.Translate (label + "_cancel", table), formatStrings)
		//	);
		//	var blocker = new SimpleBlocker (async () => {
		//		alert.DismissWithClickedButtonIndex (alert.CancelButtonIndex, true);
		//		return true;
		//	});
		//	alert.Dismissed += (sender, e) => {
		//		ExecutionPerformer.UnregisterBlocker (blocker);
		//	};
		//	ExecutionPerformer.RegisterBlocker (blocker);
		//	alert.Show ();
		//	return alert;
		//}

		public static Task GenericAlertAsync (string label, string table, params string [] formatStrings)
		{
			var alert = new UIAlertView (String.Format (BlrtHelperiOS.Translate (label + "_title", table), formatStrings),
				String.Format (BlrtHelperiOS.Translate (label + "_message", table), formatStrings),
				null,
				String.Format (BlrtHelperiOS.Translate (label + "_cancel", table), formatStrings)
			);
			var blocker = new SimpleBlocker (() => {
				alert.DismissWithClickedButtonIndex (alert.CancelButtonIndex, true);
				return Task.FromResult (true);
			});
			var tcs = new TaskCompletionSource<object> ();
			alert.Dismissed += (sender, e) => {
				ExecutionPerformer.UnregisterBlocker (blocker);
				tcs.TrySetResult (null);
			};
			ExecutionPerformer.RegisterBlocker (blocker);
			alert.Show ();
			return tcs.Task;
		}

		//public static UIAlertView GenericOptionAlert (string label, string table, bool leaveWhenCancel, Action<bool> callback = null, params object [] formatStrings)
		//{
		//	var alert = new UIAlertView (String.Format (BlrtHelperiOS.Translate (label + "_title", table), formatStrings),
		//		String.Format (BlrtHelperiOS.Translate (label + "_message", table), formatStrings),
		//		null,
		//		String.Format (BlrtHelperiOS.Translate (label + "_cancel", table), formatStrings)
		//	);

		//	SimpleBlocker blocker = null;
		//	if (callback != null) {
		//		var tcs = new TaskCompletionSource<bool> ();
		//		blocker = new SimpleBlocker (async () => {
		//			return await tcs.Task;
		//		});
		//		alert.AddButton (BlrtHelperiOS.Translate (label + "_action", table));
		//		alert.Clicked += (object sender, UIButtonEventArgs e) => {
		//			callback.DynamicInvoke (alert.CancelButtonIndex != e.ButtonIndex);
		//			tcs.SetResult (!(leaveWhenCancel ^ (alert.CancelButtonIndex == e.ButtonIndex)));
		//		};
		//	} else {
		//		blocker = new SimpleBlocker (() => {
		//			alert.DismissWithClickedButtonIndex (alert.CancelButtonIndex, true);
		//			return Task.FromResult (true);
		//		});
		//	}
		//	alert.Dismissed += (sender, e) => {
		//		ExecutionPerformer.UnregisterBlocker (blocker);
		//	};
		//	ExecutionPerformer.RegisterBlocker (blocker);
		//	alert.Show ();
		//	return alert;
		//}

		public static async Task<bool> GenericConfirmAsync (string label, string table, params object [] formatStrings)
		{
			return await SimpleConfirmAsync (
				string.Format (Translate (label + "_title", table),	formatStrings),
				string.Format (Translate (label + "_message", table),	formatStrings),
				string.Format (Translate (label + "_action", table),	formatStrings),
				true, 
				string.Format (Translate (label + "_cancel", table),	formatStrings)
			);
		}

		public static Task<bool> SimpleConfirmAsync (
			string title,
			string message,
			string actionTitle,
			bool showCancel,
			string cancelTitle = "",
			bool isBlocker = true )
		{
			TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool> ();
			var alert = UIAlertController.Create (title, message, UIAlertControllerStyle.Alert);

			if (showCancel) {
				alert.AddAction (UIAlertAction.Create (cancelTitle, UIAlertActionStyle.Cancel, delegate {
					tcs.SetResult (false);
				}));
			}

			alert.AddAction (UIAlertAction.Create (actionTitle, UIAlertActionStyle.Default, delegate {
					tcs.SetResult (true);
			}));

			Action onDismissal = null;
			if (isBlocker) {
				var blocker = new SimpleBlocker (async () => {
					return !(await tcs.Task);
				});
				ExecutionPerformer.RegisterBlocker (blocker);

				onDismissal = new Action (delegate {
					ExecutionPerformer.UnregisterBlocker (blocker);
				});
			}
				


			UIApplication.SharedApplication?.KeyWindow?.RootViewController?.PresentViewController (alert, true, onDismissal);
			return tcs.Task;
		}


		[DllImport(Constants.SystemLibrary)]
		static private extern int sysctlbyname([MarshalAs(UnmanagedType.LPStr)] string property, IntPtr output, IntPtr oldLen, IntPtr newp, uint newlen);

		public static string GetDeviceModel ()
		{
			var pLen = Marshal.AllocHGlobal(sizeof(int));
			sysctlbyname("hw.machine", IntPtr.Zero, pLen, IntPtr.Zero, 0);

			var length = Marshal.ReadInt32(pLen);
			if (length == 0) {
				Marshal.FreeHGlobal(pLen);
				return "unknown";
			}

			var pStr = Marshal.AllocHGlobal(length);
			sysctlbyname("hw.machine", pStr, pLen, IntPtr.Zero, 0);

			var platform = Marshal.PtrToStringAnsi(pStr);

			Marshal.FreeHGlobal(pLen);
			Marshal.FreeHGlobal(pStr);

			if (platform .Equals(@"iPhone1,1"))	return @"iPhone 1G";
			if (platform .Equals(@"iPhone1,2"))	return @"iPhone 3G";
			if (platform .Equals(@"iPhone2,1"))	return @"iPhone 3GS";
			if (platform .Equals(@"iPhone3,1"))	return @"iPhone 4";
			if (platform .Equals(@"iPhone3,3"))	return @"Verizon iPhone 4";
			if (platform .Equals(@"iPhone4,1"))	return @"iPhone 4S";
			if (platform .Equals(@"iPhone5,1"))	return @"iPhone 5 (GSM)";
			if (platform .Equals(@"iPhone5,2"))	return @"iPhone 5 (GSM+CDMA)";
			if (platform .Equals(@"iPhone5,3"))	return @"iPhone 5c (GSM)";
			if (platform .Equals(@"iPhone5,4"))	return @"iPhone 5c (GSM+CDMA)";
			if (platform .Equals(@"iPhone6,1"))	return @"iPhone 5s (GSM)";
			if (platform .Equals(@"iPhone6,2"))	return @"iPhone 5s (GSM+CDMA)";  
			if (platform .Equals(@"iPhone7,2"))	return @"iPhone 6";
			if (platform .Equals(@"iPhone7,1"))	return @"iPhone 6 Plus";
			if (platform .Equals(@"iPod1,1"))	return @"iPod Touch 1G";
			if (platform .Equals(@"iPod2,1"))	return @"iPod Touch 2G";
			if (platform .Equals(@"iPod3,1"))	return @"iPod Touch 3G";
			if (platform .Equals(@"iPod4,1"))	return @"iPod Touch 4G";
			if (platform .Equals(@"iPod5,1"))	return @"iPod Touch 5G";
			if (platform .Equals(@"iPad1,1"))	return @"iPad";
			if (platform .Equals(@"iPad2,1"))	return @"iPad 2 (WiFi)";
			if (platform .Equals(@"iPad2,2"))	return @"iPad 2 (GSM)";
			if (platform .Equals(@"iPad2,3"))	return @"iPad 2 (CDMA)";
			if (platform .Equals(@"iPad2,4"))	return @"iPad 2 (WiFi)";
			if (platform .Equals(@"iPad2,5"))	return @"iPad Mini (WiFi)";
			if (platform .Equals(@"iPad2,6"))	return @"iPad Mini (GSM)";
			if (platform .Equals(@"iPad2,7"))	return @"iPad Mini (GSM+CDMA)";
			if (platform .Equals(@"iPad3,1"))	return @"iPad 3 (WiFi)";
			if (platform .Equals(@"iPad3,2"))	return @"iPad 3 (GSM+CDMA)";
			if (platform .Equals(@"iPad3,3"))	return @"iPad 3 (GSM)";
			if (platform .Equals(@"iPad3,4"))	return @"iPad 4 (WiFi)";
			if (platform .Equals(@"iPad3,5"))	return @"iPad 4 (GSM)";
			if (platform .Equals(@"iPad3,6"))	return @"iPad 4 (GSM+CDMA)";
			if (platform .Equals(@"iPad4,1"))	return @"iPad Air (WiFi)";
			if (platform .Equals(@"iPad4,2"))	return @"iPad Air (Cellular)";
			if (platform .Equals(@"iPad4,4"))	return @"iPad mini 2G (WiFi)";
			if (platform .Equals(@"iPad4,5"))	return @"iPad mini 2G (Cellular)";
			if (platform .Equals(@"i386"))		return @"Simulator";
			if (platform .Equals(@"x86_64"))	return @"Simulator";
			return platform;
		}

		public static void EvenlyHoziontalSpaceSubViews (UIView view, int padding)
		{
			EvenlyHoziontalSpaceSubViews (view, padding, padding);
		}

		public static void EvenlyHoziontalSpaceSubViews (UIView view, int leftPadding, int rightPadding)
		{

			nfloat whiteSpace = view.Bounds.Width - (leftPadding + rightPadding);

			for (int i = 0; i < view.Subviews.Length; ++i) {
				whiteSpace -= view.Subviews [i].Bounds.Width;
			}

			whiteSpace = whiteSpace / (view.Subviews.Length - 1);

			nfloat spacing = leftPadding;

			for (int i = 0; i < view.Subviews.Length; ++i) {
				view.Subviews [i].Frame = new CGRect (
					(int)spacing,
					(int)((view.Bounds.Height - view.Subviews [i].Bounds.Height) * 0.5f),
					view.Subviews [i].Bounds.Width,
					view.Subviews [i].Bounds.Height
				);
				spacing = view.Subviews [i].Frame.Right + whiteSpace;
			}
		}

		public static UIViewController TopPresentedViewController (this UIViewController parent)
		{
			if (null == parent) {
				return null;
			}
			while (parent.PresentedViewController != null)
				parent = parent.PresentedViewController;
			return parent;
		}

		public static Version DeviceVersion {
			get {
				return new Version (UIDevice.CurrentDevice.SystemVersion);
			}
		}
	}
}

