﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlrtData;
using Foundation;
using Parse;
using UIKit;

namespace BlrtiOS.Views.Root
{
	public partial class RootAppController
	{
		NSObject _applicationDidBecomeActiveObserverObj;
		IDictionary<string, HashSet<IDictionary<string, object>>> PendingPushNotifications;

		#region overloads

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			_applicationDidBecomeActiveObserverObj = UIApplication.Notifications.ObserveDidBecomeActive (ApplicationDidBecomeActive);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			NSNotificationCenter.DefaultCenter.RemoveObserver (_applicationDidBecomeActiveObserverObj);
		}

		#endregion

		/// <summary>
		/// Called everytime application becomes active.
		/// </summary>
		/// <returns>The did become active.</returns>
		/// <param name="arg">Argument.</param>
		void ApplicationDidBecomeActive (object sender, NSNotificationEventArgs e)
		{
			SettlePendingPushNotifications ();
		}

		#region helpers

		/// <summary>
		/// Gets or sets a value indicating whether high priority push notification is being settled
		/// </summary>
		/// <value><c>true</c> if settling high priority push notification; otherwise, <c>false</c>.</value>
		bool SettlingHighPriorityPushNotification { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether a user cancelled the conversation loading process
		/// </summary>
		/// <value><c>true</c> if conversation loading cancelled by user; otherwise, <c>false</c>.</value>
		bool ConversationLoadingCancelledByUser { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether push notifications are currently being settled or not.
		/// <value><c>true</c> if settling push notifications; otherwise, <c>false</c>.</value>
		bool SettlingPushNotifications { get; set; }

		/// <summary>
		/// Gets the current conversation (the conversation that is currently open) identifier.
		/// </summary>
		/// <value>The current conversation identifier.</value>
		string CurrentConversationId {
			get {
				return CurrentConversationScreen?.CurrentConversation?.ObjectId ?? string.Empty;
			}
		}

		/// <summary>
		/// Gets the current conversation screen.
		/// </summary>
		/// <value>The current conversation screen.</value>
		BlrtData.ExecutionPipeline.IConversationScreen CurrentConversationScreen {
			get {
				return (Slave as Mailbox.MailboxController)?.ConversationScreen;
			}
		}

		/// <summary>
		/// Gets the value for key.
		/// </summary>
		/// <returns>The value for key.</returns>
		/// <param name="dic">Dic.</param>
		/// <param name="key">Key.</param>
		static object GetValueForKey (IDictionary<string, object> dic, string key)
		{
			object value = string.Empty;
			dic.TryGetValue (key, out value);

			return value;
		}

		/// <summary>
		/// Gets the conversation with the given id.
		/// </summary>
		/// <returns>The conversation model</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		BlrtData.Conversation GetConversation (string conversationId)
		{
			return LocalStorageParameters.Default?.DataManager?.GetConversation (conversationId);
		}

		/// <summary>
		/// Gets the type of a given push notification.
		/// </summary>
		/// <returns>The push notification type.</returns>
		/// <param name="notification">Notification.</param>
		PushHandler.BlrtPushType GetPushNotificationType (IDictionary<string, object> notification)
		{
			var type = PushHandler.BlrtPushType.Message;

			if (notification.ContainsKey (BlrtUtil.PushNotificationConstants.NOTIFICATION_TYPE)) {
				int value = Convert.ToInt32 (notification [BlrtUtil.PushNotificationConstants.NOTIFICATION_TYPE]);

				if (Enum.IsDefined (typeof (PushHandler.BlrtPushType), value))
					type = (PushHandler.BlrtPushType)value;
			}

			return type;
		}

		/// <summary>
		/// Gets the conversation item count in a given conversation.
		/// </summary>
		/// <returns>The conversation item count.</returns>
		/// <param name="conversation">Conversation.</param>
		int GetConversationItemCount (BlrtData.Conversation conversation)
		{
			return conversation?.GeneralCount ?? 0;
		}

		/// <summary>
		/// Checks whether the Id of the currently open conversation macthes the provided conversation id
		/// </summary>
		/// <returns><c>true</c>, if conversation identifier matches that of currently open conversation, <c>false</c> otherwise.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		bool CurrentConversationIdMatches (string conversationId)
		{
			return CurrentConversationId?.Equals (conversationId) ?? false;
		}

		#endregion

		#region notification registration

		/// <summary>
		/// Adds the push notification to the pending notifications list.
		/// </summary>
		/// <param name="pushData">Push data.</param>
		public void AddPushNotification (IDictionary<string, object> pushData)
		{
			if (PendingPushNotifications == null) {
				PendingPushNotifications = new Dictionary<string, HashSet<IDictionary<string, object>>> ();
			}

			RemovePendingPushNotificationIfExists (pushData);
			AddPushNotificationToPendingList (pushData);

			if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active) {
				SettlePendingPushNotifications ();
			}
		}

		/// <summary>
		/// Adds a push notification to the pending notifications list.
		/// </summary>
		/// <param name="pushData">Push data.</param>
		void AddPushNotificationToPendingList (IDictionary<string, object> pushData)
		{
			lock (PendingPushNotifications) {

				var key = GetKeyForPendingNotificationsList (pushData);

				HashSet<IDictionary<string, object>> notifications;
				if (PendingPushNotifications.TryGetValue (key, out notifications)) {
					notifications.Add (pushData);
				} else {
					notifications = new HashSet<IDictionary<string, object>> (new NotificationsDictionaryComparer());
					notifications.Add (pushData);
					PendingPushNotifications.Add (key, notifications);
				}
			}
		}

		/// <summary>
		/// Gets the key for pending notifications list.
		/// </summary>
		/// <returns>The key for pending notifications list.</returns>
		/// <param name="pushData">Push data.</param>
		public static string GetKeyForPendingNotificationsList (IDictionary<string, object> pushData)
		{
			var key = BlrtUtil.PushNotificationConstants.CONVERSATION_ID;
			var type = GetValueForKey (pushData, BlrtUtil.PushNotificationConstants.NOTIFICATION_TYPE);

			if (PendingActionHandler.IsPendingActionType (Convert.ToInt32 (type))){
				key = BlrtUtil.PushNotificationConstants.PENDING_ACTION_ID;
			}

			return GetValueForKey (pushData, key) as string;
		}

		/// <summary>
		/// Removes a pending push notifications (if it exists) from the pending notifications list.
		/// </summary>
		/// <param name="dic">Dic.</param>
		public void RemovePendingPushNotificationIfExists (IDictionary<string, object> dic)
		{
			if (PendingPushNotifications == null) {
				return;
			}

			// block multiple threads from accessing it at a time
			lock (PendingPushNotifications) {
				var key = GetKeyForPendingNotificationsList (dic);

				HashSet<IDictionary<string, object>> notifications;

				// check whether any notification for this conversation exists
				if (PendingPushNotifications.TryGetValue (key, out notifications)) {
					notifications.Remove (dic);

					// remove notification if all of it's sub notifications have been settled
					if (notifications.Count == 0) {
						PendingPushNotifications.Remove (key);
					}
				}
			}
		}

		#endregion

		#region notification settlement

		/// <summary>
		/// Settles the high priority push notification. It is used only when a user taps on a notification. 
		/// Since tapping on a notification, needs immediate action, that notification is considered of the highest priority.
		/// </summary>
		/// <param name="dic">Dic.</param>
		public void SettleHighPriorityPushNotification (Dictionary<string, object> dic)
		{
			SettlingHighPriorityPushNotification = true;
			RemovePendingPushNotificationIfExists (dic);

			Task.Run (async delegate {
				try {
					await SettlePushNotification (BlrtPushState.ResumeFromPush, dic);
				} finally {
					SettlingHighPriorityPushNotification = false;

					SettlePendingPushNotifications (); // continue settling pending notifications
					DownloadContentForPendingItems (); // continue downloading content for conversation items
				}
			});
		}

		/// <summary>
		/// Settles the pending push notifications.
		/// </summary>
		void SettlePendingPushNotifications ()
		{
			if (SettlingPushNotifications) {
				return;
			}

			SettlingPushNotifications = true;

			Task.Run (async delegate {
				try {
					IDictionary<string, object> pendingNotification = null;
					while ((pendingNotification = GetNextPendingPushNotification ()) != null) {
						await SettlePushNotification (BlrtPushState.ResumeFromBackground, pendingNotification);
					}

				} finally {
					SettlingPushNotifications = false;
				}
			});
		}

		/// <summary>
		/// Gets the next pending notification.
		/// </summary>
		/// <returns>The next pending notification.</returns>
		IDictionary<string, object> GetNextPendingPushNotification ()
		{
			if (PendingPushNotifications == null) {
				return null;
			}
			if (SettlingHighPriorityPushNotification) {
				return null;
			}

			var notificationsId = CurrentConversationId;

			lock (PendingPushNotifications) {

				HashSet<IDictionary<string, object>> notifications = null;
				IDictionary<string, object> notification = null;

				if (PendingPushNotifications.TryGetValue (notificationsId, out notifications)) {
					// given notifications id found in the pending list
					notification = notifications.FirstOrDefault ();
				} else {

					/*
					 * KeyValuePair <notificationsId, notifications>
					*/
					var pair = PendingPushNotifications?.FirstOrDefault ();

					notificationsId = pair?.Key;
					notifications = pair?.Value;
					notification = notifications?.FirstOrDefault ();
				}

				notifications?.Remove (notification);// remove convo notification from the set

				if (notifications?.Count == 0) {
					// remove convo id from the dictionary
					PendingPushNotifications?.Remove (notificationsId);
				}

				return notification;

			}
		}

		/// <summary>
		/// Settles a push notification.
		/// </summary>
		/// <param name="notification">Notification.</param>
		async Task SettlePushNotification (BlrtPushState state, IDictionary<string, object> notification)
		{
			if (GetPushNotificationType (notification) == PushHandler.BlrtPushType.BlrtRefresh) {
				await SettleRefreshPushNotification (notification, state);
			} else {
				try {
					await PushHandler.ParsePush (state, notification);
				} catch { }
			}
		}

		/// <summary>
		/// Settles the refresh push notification.
		/// </summary>
		/// <returns>The refresh push notification.</returns>
		/// <param name="notification">Notification.</param>
		async Task SettleRefreshPushNotification (IDictionary<string, object> notification, BlrtPushState state)
		{
			var conversationId = (string)GetValueForKey (notification, BlrtUtil.PushNotificationConstants.CONVERSATION_ID);
			var conversation = GetConversation (conversationId);

			var isConversationLocal = conversation != null;
			var conversationCountBeforeRefresh = GetConversationItemCount (conversation);

			// fetch any new conversation user relations
			FetchConversationUserRelation (conversationId);

			IEnumerable<ParseObject> results = null;
			var newConversationFetched = false;

			if (isConversationLocal) {
				OpenConversationIfNeeded (state, conversationId);
				results = await RefreshConversation (conversation);
			} else {
				var task = DownloadConversationContentFromCloud (conversationId, LocalStorageParameters.Default);
				ConversationLoadingCancelledByUser = false;
				ShowConversationLoadingAlertIfNeeded (task, state);
				results = await task;

				if (!ConversationLoadingCancelledByUser) {
					OpenConversationIfNeeded (state, conversationId);
				}

				newConversationFetched = true;
			}

			RefreshDidComplete (conversationId, conversationCountBeforeRefresh, results, newConversationFetched);
		}

		/// <summary>
		/// Shows the conversation loading alert if needed.
		/// </summary>
		/// <param name="conversationLoadingTask">Task.</param>
		/// <param name="state">State.</param>
		void ShowConversationLoadingAlertIfNeeded (Task conversationLoadingTask, BlrtPushState state)
		{
			if (state != BlrtPushState.ResumeFromPush) {
				return;
			}

			InvokeOnMainThread (async delegate {
				var taskCompleted = await BlrtUtil.PlatformHelper.ShowWaitAlert (
					conversationLoadingTask,
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_title"),
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_message"),
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_cancel")
				);

				if (!taskCompleted) {
					ConversationLoadingCancelledByUser = true;
				}
			});
		}

		/// <summary>
		/// Opens a conversation if needed.
		/// </summary>
		/// <param name="state">State.</param>
		/// <param name="conversationId">Conversation identifier.</param>
		void OpenConversationIfNeeded (BlrtPushState state, string conversationId)
		{
			if (state == BlrtPushState.ResumeFromPush) {
				InvokeOnMainThread (delegate {
					BlrtUtil.OpenConversation (conversationId).FireAndForget ();
				});
			}
		}

		/// <summary>
		/// Refreshes a local conversation.
		/// </summary>
		/// <returns>The conversation.</returns>
		/// <param name="conversation">Conversation.</param>
		async Task<IEnumerable<Parse.ParseObject>> RefreshConversation (BlrtData.Conversation conversation)
		{
			IEnumerable<Parse.ParseObject> results = null;

			if (conversation.OnParse) {
				try {
					await SetConversationRefreshing (conversation, true);
					results = await DownloadConversationContentFromCloud (conversation.ObjectId, conversation.LocalStorage);
				} finally {
					await SetConversationRefreshing (conversation, false);
				}
			}

			return results;
		}

		/// <summary>
		/// Sets a conversation refreshing to indicate whether a conversation is refreshing. 
		/// It is used to decide whether to show loading indicator in a conversation when a conversation is open.
		/// </summary>
		/// <returns>The conversation refreshing.</returns>
		/// <param name="conversation">Conversation.</param>
		/// <param name="refreshing">If set to <c>true</c> refreshing.</param>
		async Task SetConversationRefreshing (BlrtData.Conversation conversation, bool refreshing)
		{
			conversation.Refreshing = refreshing;
			await conversation.LocalStorage.DataManager.SaveLocal ();
		}

		#endregion

		#region cloud

		/// <summary>
		/// Downloads the conversation content from cloud.
		/// </summary>
		/// <returns>The conversation content from cloud.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="localStorage">Local storage.</param>
		async Task<IEnumerable<ParseObject>> DownloadConversationContentFromCloud (string conversationId, ILocalStorageParameter localStorage)
		{
			try {
				var query = BlrtData.Query.BlrtConversationQuery.RunQuery (conversationId, -1, 0, localStorage);
				await query.WaitAsync ();
				return query.Results;
			} catch {
				return null;
			}
		}

		/// <summary>
		///  Fetches conversation user relations for a given conversation.
		/// </summary>
		/// <param name="convoId">Convo identifier.</param>
		static void FetchConversationUserRelation (string convoId)
		{
			if (null == ParseUser.CurrentUser) {
				return;
			}
			var rel = BlrtManager.DataManagers.Default.GetConversationUserRelation (
				convoId,
				ParseUser.CurrentUser.ObjectId
			);
			try {
				BlrtData.Query.BlrtConversationMetaQuery.RunQuery (rel);
			} catch { }
		}

		#endregion
	}

	#region notifications dictionary comparer

	/// <summary>
	/// Notifications dictionary comparer.
	/// </summary>
	class NotificationsDictionaryComparer : EqualityComparer<IDictionary<string, object>>
	{
		public override bool Equals (IDictionary<string, object> x, IDictionary<string, object> y)
		{
			var id1 = RootAppController.GetKeyForPendingNotificationsList (x);
			var id2 = RootAppController.GetKeyForPendingNotificationsList (y);

			return id1?.Equals (id2) ?? false;
		}

		public override int GetHashCode (IDictionary<string, object> obj)
		{
			return base.GetHashCode ();
		}
	}

	#endregion
}