using System;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using BlrtData.ExecutionPipeline;
using BlrtiOS.Views.AppMenu;
using BlrtiOS.Views.Util;
using BlrtData;
using Xamarin.Forms;
using BlrtCanvas;
using BlrtiOS.Views.Login;
using CoreGraphics;
using BlrtiOS.Views.Settings;
using BlrtData.Models.Mailbox;
using BlrtData.Contents;
using System.Threading;
using BlrtiOS.Views.Profile;
using BlrtData.Views.AddPhone;
using Parse;
using System.Collections.Generic;
using BlrtiOS.Views.MediaPicker;

namespace BlrtiOS.Views.Root
{
	using HiddenParameterFor = BlrtMediaPageListController.PagePermissions.HiddenParameterFor;
	public partial class RootAppController
		: BaseSlideDrawerViewController, IAppController, IExecutor, IBlrtResponser, IBlrtViewController
	{

		Lockout.LockoutViewController _lockoutController;

		AppMenuController _appMenu;
		CustomUI.BlrtLoadingOverlayView _loader;

		UINavigationController _tosNav;

		bool IsLoggedOut { get { return ParseUser.CurrentUser == null; } }

		public LoginNavigationController LoginNav { get; private set; }

		bool _shouldHideStatusBar = true;
		public bool ShouldHideStatusBar {
			get { return _shouldHideStatusBar; }
			set {
				_shouldHideStatusBar = value;
				SetNeedsStatusBarAppearanceUpdate ();
			}
		}

		public async Task RefreshMailbox ()
		{
			if (Slave is Mailbox.MailboxController) {
				var con = Slave as Mailbox.MailboxController;
				await con.Refresh (this);
			}
		}

		public override bool PrefersStatusBarHidden ()
		{
			return ShouldHideStatusBar;
		}

		public RootAppController () : base ()
		{
			this.DividerColor = BlrtStyles.iOS.Black;
		}

		private UIView _loading;
		private UILabel _newUserLabel;

		public override void TraitCollectionDidChange (UITraitCollection previousTraitCollection)
		{
			base.TraitCollectionDidChange (previousTraitCollection);
			if (UIDevice.CurrentDevice.CheckSystemVersion (9, 0)) {
				ViewControllers.Canvas.CanvasPlatformHelper.Support3DTouch
							   = (TraitCollection.ForceTouchCapability == UIForceTouchCapability.Available);
			}
		}

		public override void LoadView ()
		{
			BlrtManager.Initialize (this);
			BlrtiOSIAPManager.Setup ();

			base.LoadView ();

			_loading = new UIView (View.Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				BackgroundColor = BlrtConfig.BlrtGreen,
				Layer = {
					ZPosition = 9999999
				}
			};

			var logo = new UIImageView (UIImage.FromBundle ("logo.png")) {
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			if (BlrtHelperiOS.IsPhone) {
				logo.Frame = new CGRect (0, 0, 320, 80);
			}
			logo.Center = View.Center;

			_loading.AddSubview (logo);


			_newUserLabel = new UILabel (new CGRect (20, View.Center.Y - 32, View.Bounds.Width - 40, 64)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Text = BlrtHelperiOS.Translate ("loading_creating_account"),//  "Time travel is like a coconut of banana phones eating sandwiches on a sunday afternoon.",
				Lines = 0,
				TextAlignment = UITextAlignment.Center,
				Hidden = true
			};
			_loading.AddSubview (_newUserLabel);

			View.AddSubview (_loading);
			ShowLoading (true);
		}


		public void HideLoading (bool animated)
		{
			if (!_loading.Hidden) {
				UIView.Animate (
					animated ? 1 : 0,
					() => {
						_loading.Alpha = 0;
					},
					() => {
						_loading.Hidden = true;
						_newUserLabel.Hidden = true;
					}
				);
			}
		}

		public void ShowLoading (bool animated)
		{
			if (_loading.Hidden) {
				UIView.Animate (
					animated ? 1 : 0,
					() => {
						_loading.Hidden = false;
						_loading.Alpha = 1;
					},
					() => {
					}
				);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			MasterWidth = NMath.Min (NMath.Min (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) * 0.8f, 320);
			SlaveWidth = nfloat.MaxValue;

			_appMenu = new AppMenuController () {
			};
			Master = _appMenu;

			_loader = new BlrtiOS.Views.CustomUI.BlrtLoadingOverlayView ();

			View.AddSubview (_loader);
		}

		public override void PushLoading (string cause, bool animated)
		{
			_loader.Push (cause, animated);
			View.BringSubviewToFront (_loader);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loader.Pop (cause, animated);
		}

		#region IAppController implementation

		public event EventHandler SystemMemoryWarning;
		public event EventHandler MoveToBackground;
		public event EventHandler MoveToForeground;

		public void OnSystemMemoryWarning ()
		{
			if (SystemMemoryWarning != null) {
				SystemMemoryWarning (this, EventArgs.Empty);
			}
		}

		public void OnMoveToBackground ()
		{
			if (MoveToBackground != null) {
				MoveToBackground (this, EventArgs.Empty);
			}
		}

		public void OnMoveToForeground ()
		{
			if (MoveToForeground != null) {
				MoveToForeground (this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// Tries the let user create new conversation.
		/// </summary>
		/// <returns>The let create new conversation.</returns>
		public async Task<bool> TryLetCreateNewConversation ()
		{
			if (Slave is Mailbox.MailboxController) {
				var mailboxController = Slave as Mailbox.MailboxController;
				await mailboxController.CreateConversationThroughProfile (true);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Creates the conversation with contacts.
		/// </summary>
		/// <returns>The conversation with contacts.</returns>
		/// <param name="executor">Executor.</param>
		/// <param name="args">Arguments.</param>
		public async Task<bool> CreateConversationWithContacts (IExecutor executor, IRequestArgs args)
		{
			if (Slave is Mailbox.MailboxController) {
				var mailboxController = Slave as Mailbox.MailboxController;
				var conversationCreated = await TryLetCreateNewConversation ();

				return conversationCreated;
			}
			return false;
		}

		public async Task CreateConversationToCreateNewConvoItemsUsingExternalMedia (IExecutor executor,
																					 IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia)
		{
			if (Slave is Mailbox.MailboxController) {
				var mailboxController = Slave as Mailbox.MailboxController;
				var conversationCreated = await TryLetCreateNewConversation ();

				if (!conversationCreated) {
					return;
				}

				mailboxController
					.ConversationScreen
					.CreateConversationItemsUsingExternalMedia (argsToCreateNewConvoItemsUsingExternalMedia);
			}
		}

		public async Task<ExecutionResult<IMailboxScreen>> OpenMailboxToCreateNewConvoItemsUsingExternalMedia (IExecutor executor,
																												 MailboxType type,
																												 IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia)
		{
			return OpenMailbox (executor, type, false, true, argsToCreateNewConvoItemsUsingExternalMedia);
		}

		public Task<ExecutionResult<IMailboxScreen>> OpenMailbox (IExecutor e,
																  MailboxType type)
		{
			return OpenMailbox (e, type, false);
		}

		public async Task<ExecutionResult<IMailboxScreen>> OpenMailbox (IExecutor e, MailboxType type, bool refresh)
		{
			return OpenMailbox (e, type, refresh, false, null);
		}

		ExecutionResult<IMailboxScreen> OpenMailbox (IExecutor e,
													 MailboxType type,
													 bool refresh,
													 bool inSeparateContainer,
													 IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia)
		{
			if (IsLoggedOut || InMaintanceMode) {
				return new ExecutionResult<IMailboxScreen> (new Exception ("Cannot open mailbox: In maintance mode, or user is logged out"));
			}

			_appMenu.SetMailbox (type);

			//SetMailbox (type, false, null, refresh);
			var mailbox = GetMailbox (type,
						  SlaveIsMailboxInRequestedContainer (inSeparateContainer: inSeparateContainer),
						  inSeparateContainer);
			SetMailbox (mailbox, inSeparateContainer, argsToCreateNewConvoItemsUsingExternalMedia);

			if (refresh) {
				mailbox?.Refresh (e).FireAndForget ();
			}

			HideSlider (true);

			return new ExecutionResult<IMailboxScreen> (Slave as IMailboxScreen);
		}

		/// <summary>
		/// Gets the mailbox with either mailbox in separate controller or in the Detail pane of conversation controller.
		/// </summary>
		/// <returns>The mailbox.</returns>
		/// <param name="type">Type.</param>
		/// <param name="slaveIsMailboxInRequestedContainer">If set to <c>true</c> slave is mailbox in requested container.</param>
		/// <param name="inSeparateContainer">If set to <c>true</c> in separate container.</param>
		Mailbox.MailboxController GetMailbox (MailboxType type, bool slaveIsMailboxInRequestedContainer, bool inSeparateContainer)
		{
			var mailbox = slaveIsMailboxInRequestedContainer
				? Slave as Mailbox.MailboxController : new Mailbox.MailboxController (inSeparateContainer);
			mailbox?.SetType (type);

			return mailbox;
		}

		/// <summary>
		/// Sets the mailbox and args necessary media args to make blrt or send media.
		/// </summary>
		/// <param name="mailbox">Mailbox.</param>
		/// <param name="inSeparateContainer">If set to <c>true</c> in separate container.</param>
		/// <param name="argsToCreateNewConvoItemsUsingExternalMedia">Arguments to create new convo items using external media.</param>
		void SetMailbox (Mailbox.MailboxController mailbox, bool inSeparateContainer, IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia)
		{
			var slaveIsMailboxInRequestedContainer = SlaveIsMailboxInRequestedContainer (inSeparateContainer: inSeparateContainer);

			if (!slaveIsMailboxInRequestedContainer) {
				Slave = mailbox;
			}

			if (inSeparateContainer) {
				mailbox.RequestArgsForExternalMedia = argsToCreateNewConvoItemsUsingExternalMedia;
			}
		}

		/// <summary>
		/// Checks whether Slave is mailbox and in the requested container. 
		/// Container could either be a Master or a separate controller on top of slit view controller.
		/// </summary>
		/// <returns><c>true</c>, if Slave is mailbox and in the requested container <c>false</c> otherwise.</returns>
		/// <param name="inSeparateContainer">If set to <c>true</c> separate container.</param>
		bool SlaveIsMailboxInRequestedContainer (bool inSeparateContainer)
		{
			return Slave is Mailbox.MailboxController && (Slave as Mailbox.MailboxController).MailboxListInSeparateContainer == inSeparateContainer;
		}

		public async Task<ExecutionResult<ISignUpScreen>> OpenSignUp (IExecutor e)
		{
			if (IsLoggedOut) {
				if (LoginNav == null) {
					LoginNav = new BlrtiOS.Views.Login.LoginNavigationController ();
					PresentViewController (LoginNav, true, null);
				}
				return new ExecutionResult<ISignUpScreen> (LoginNav);
			} else {
				return new ExecutionResult<ISignUpScreen> (new InvalidOperationException ("cannot open sign up page when the user is still logged in"));
			}
		}

		public Task<ExecutionResult<IHelpScreen>> OpenSupport (IExecutor e)
		{
			var help = new Help.HelpNavController ();
			Slave = help;
			HideSlider (true);
			_appMenu.LastSelection = CellType.Help;
			return Task.Run (() => {
				return new ExecutionResult<IHelpScreen> (help);
			});

		}

		public async Task<ExecutionResult<object>> OpenSendSupportMsg (IExecutor e, string additionalMsg = null)
		{
			if (InMaintanceMode) {
				return new ExecutionResult<object> (new Exception ("Cannot open support message page: In maintance mode"));
			}
			UIViewController parent = null;
			if (e is UIViewController) {
				parent = e as UIViewController;
			} else if (e is Page) {
				parent = (e as Page).SharedViewController ();
			} else {
				parent = this.TopPresentedViewController ();
			}
			Help.HelpNavController.OpenMessageSupport (parent, additionalMsg).FireAndForget ();
			return new ExecutionResult<object> (new object ());
		}

		public async Task<ExecutionResult<IUpgradeScreen>> OpenUpgradeAccount (IExecutor e)
		{
			if (IsLoggedOut || InMaintanceMode) {
				return new ExecutionResult<IUpgradeScreen> (new Exception ("Cannot display upgrade page: In maintance mode, or user is logged out"));
			}

			await BlrtManager.ApplyPermission (new UserPermissionHandler ());

			try {

				var nav = new Upgrade.UpgradeNavController ();
				UIViewController view = this.TopPresentedViewController ();

				view.PresentViewController (nav, true, null);
				//async wait finish
				Task.Factory.StartNew (async () => {
					await nav.WaitAsyncCompletion ();
					BeginInvokeOnMainThread (() => {
						_appMenu.QuietSelect (_appMenu.LastSelection, true);
					});
				},
					CancellationToken.None,
					TaskCreationOptions.None,
					TaskScheduler.Default
				  ).FireAndForget ();

				return new ExecutionResult<IUpgradeScreen> (nav);

			} catch (Exception exp) {
				return new ExecutionResult<IUpgradeScreen> (new Exception ("Cannot display upgrade page: Settings incorrect"));
			}
		}

		public Task<ExecutionResult<object>> OpenShare (IExecutor e)
		{
			var share = new Share.ShareNavController ();
			Slave = share;
			_appMenu.LastSelection = CellType.Share;
			HideSlider (true);

			return Task.Run (() => {
				return new ExecutionResult<object> (new object ());
			});
		}

		public Task<ExecutionResult<object>> OpenContactsPage (IExecutor e)
		{
			UINavigationController nav = new BlrtiOS.Views.Contacts.ContactsNavController () {
				ModalPresentationStyle = UIModalPresentationStyle.FormSheet
			};
			HideSlider (true);
			UIViewController view = this.TopPresentedViewController ();
			view.PresentViewController (nav, true, null);

			return Task.Run (() => {
				return new ExecutionResult<object> (nav);
			});
		}

		public Task<ExecutionResult<object>> OpenSettings (IExecutor e)
		{
			Slave = new SettingController ();
			HideSlider (true);
			_appMenu.LastSelection = CellType.Settings;
			return Task.Run (() => {
				return new ExecutionResult<object> (Slave);
			});
		}

		public Task<ExecutionResult<object>> OpenNews (IExecutor e)
		{

			Slave = new News.NewsNavController ();
			HideSlider (true);
			_appMenu.LastSelection = CellType.News;
			return Task.Run (() => {
				return new ExecutionResult<object> (new object ());
			});
		}

		public async Task<ExecutionResult<object>> OpenOrganisationList (IExecutor e, string ocListId)
		{
			if (string.IsNullOrWhiteSpace (ocListId)) {
				throw new ArgumentNullException ("ocListId");
			}
			HideSlider (true);
			var con = new Organisation.OrgnisationListController (ocListId);
			UINavigationController nav = new ExecutionNavController (con);
			await this.PresentViewControllerAsync (nav, true);
			return new ExecutionResult<object> (new object ());
		}

		public Task<ExecutionResult<object>> OpenProfile (IExecutor e, BlrtUser blrtUser = null)
		{
			UINavigationController nav;
			if (blrtUser == null || blrtUser.ObjectId == ParseUser.CurrentUser?.ObjectId) {
				nav = new ProfileNavigationPage ();
			} else {
				nav = new OtherProfileNavigationPage (blrtUser);
			}

			HideSlider (true);
			UIViewController view = this.TopPresentedViewController ();
			view.PresentViewController (nav, true, null);

			return Task.Run (() => {
				return new ExecutionResult<object> (nav);
			});
		}

		public Task<ExecutionResult<object>> OpenProfile (IExecutor e, BlrtData.Contacts.BlrtContact blrtContact)
		{
			var nav = new OtherProfileNavigationPage (blrtContact);

			HideSlider (true);
			UIViewController view = this.TopPresentedViewController ();
			view.PresentViewController (nav, true, null);

			return Task.Run (() => {
				return new ExecutionResult<object> (nav);
			});
		}

		public async Task<ExecutionResult<object>> OpenAddPhone (IExecutor executor, OpenFrom openfrom)
		{
			var nav = new AddPhoneNavPage (openfrom);
			var topController = this.TopPresentedViewController ();
			topController.PresentViewController (nav, true, null);
			await nav.WaitResult;
			return new ExecutionResult<object> (nav.WaitResult.Result);
		}

		public bool BlrtCreationFailed { get; set; }

		public async Task<ExecutionResult<ICanvasResult>> OpenBlrtCreation (IExecutor e, IRequestArgs args)
		{
			if (IsLoggedOut || InMaintanceMode) {
				return new ExecutionResult<ICanvasResult> (new Exception ("Cannot display: In maintance mode, or user is logged out"));
			}

			IBlrtCanvasData data = null;

			if (args != null) {
				data = BlrtCanvasData.FromRequest (args);
			} else {
				data = BlrtCanvasData.FromDraft ("", new BlrtData.Media.BlrtMediaPagePrototype ());
			}

			return await OpenBlrtCreation (e, data, false);
		}

		public async Task<ExecutionResult<ICanvasResult>> OpenBlrtCreation (IExecutor e,
																			object args,
																			bool mediaHidden,
																			Tuple<IEnumerable<MediaAsset>, HashSet<string>> blrtThisMediaModel = null)
		{
			if (IsLoggedOut || InMaintanceMode) {
				return new ExecutionResult<ICanvasResult> (new Exception ("Cannot display: In maintance mode, or user is logged out"));
			}

			if (!(args is IBlrtCanvasData))
				return new ExecutionResult<ICanvasResult> (new Exception ("args needs to implement IBlrtCanvasData"));

			var data = args as IBlrtCanvasData;

			var conversationScreen = (Slave as Mailbox.MailboxController).ConversationScreen;

			var blrtTheseMediaAssets = blrtThisMediaModel?.Item1;
			var mediaIdsToSetHiddenParamFor = blrtThisMediaModel?.Item2;

			var preview = new BlrtMediaMasterController (conversationScreen, blrtTheseMediaAssets);

			/* 
			 * If new media is being added, "hidden" is considered for new media and "!hidden" for last blrt media.
             * Note: If new media is being added, "mediaIdsToSetHiddenParamFor" will always be null and thus
             * 		 hidden will be applied to all the media of last last blrt
			 * */
			preview.SetData (data, mediaHidden, mediaIdsToSetHiddenParamFor);

			await preview.SetBlrtThisAssets (mediaHidden);

			while (true) {
				preview.ActivateWaiting ();

				PresentViewController (preview, true, null/*, async delegate { await preview.SetBlrtThisAssets (); }*/);
				ShouldHideStatusBar = true;
				var result = await preview.WaitResult ();
				preview.DeactivateWaiting ();

				if (preview.PresentingViewController == this) {
					BlrtCreationFailed = !result.Successful;
					await preview.DismissViewControllerAsync (true);
				}

				if (result.Successful) {

					var canvasResult = await OpenCanvas (e, result.Data);
					if (canvasResult.Result.CreatedContent) {
						BlrtData.Helpers.BlrtTrack.TrackCreatedBlrt ();
					}
					if (canvasResult.Result.ShowMediaPicker) {
						continue;
					} else {
						Views.MediaPicker.BlrtMediaMasterController.LocalCanvasData = null;
						return canvasResult;
					}
				} else {
					Views.MediaPicker.BlrtMediaMasterController.LocalCanvasData = null;
				}

				return new ExecutionResult<ICanvasResult> (new Exception ());
			}
		}

		public async Task<ExecutionResult<ICanvasResult>> OpenCanvas (IExecutor e, object args)
		{
			if (InMaintanceMode) {
				return new ExecutionResult<ICanvasResult> (new Exception ("Cannot display: In maintance mode"));
			}
			if (null == args) {
				return new ExecutionResult<ICanvasResult> (new ArgumentNullException ("args"));
			}
			IBlrtCanvasData data = null;
			if (args is IBlrtCanvasData) {
				data = args as IBlrtCanvasData;
			} else if (args is ConversationBlrtItem) {
				data = BlrtCanvasData.FromConversationItem (args as ConversationBlrtItem);
			} else {
				return new ExecutionResult<ICanvasResult> (new Exception ("args needs to implement IBlrtCanvasData"));
			}

			try {
				UIViewController page = this.TopPresentedViewController ();

				var result = await ViewControllers.Canvas.CanvasPage.Open (page, data, true);
				return new ExecutionResult<ICanvasResult> (result);
			} catch (Exception ex) {
				return new ExecutionResult<ICanvasResult> (ex);
			}

		}

		public async Task<ExecutionResult<ICanvasResult>> OpenCanvas (IExecutor e, ILocalBlrtArgs args)
		{
			if (InMaintanceMode) {
				return new ExecutionResult<ICanvasResult> (new Exception ("Cannot display: In maintance mode"));
			}

			if (null == args) {
				return new ExecutionResult<ICanvasResult> (new ArgumentNullException ("ILocalBlrtArgs args"));
			}

			var data = new IndependentBlrtData (args);

			if (null == data) {
				return new ExecutionResult<ICanvasResult> (new InvalidOperationException ("cannot generate canvas data from local data"));
			}

			try {
				UIViewController page = this.TopPresentedViewController ();
				var result = await ViewControllers.Canvas.CanvasPage.Open (page, data, true);
				return new ExecutionResult<ICanvasResult> (result);
			} catch (Exception ex) {
				return new ExecutionResult<ICanvasResult> (ex);
			}
		}

		public async Task<ExecutionResult<IAlert>> ShowAlert (IExecutor e, IAlert alert)
		{
			var tcs = new TaskCompletionSource<bool> ();
			InvokeOnMainThread (async delegate {
				var alertView = new UIAlertView (
				alert.Title,
				alert.Message,
				null,
				alert.Cancel
				);

				nint startIndex = -1;

				if (alert.OptionCount > 0) {
					for (int i = 0; i < alert.OptionCount; ++i) {
						if (startIndex < 0)
							startIndex = alertView.AddButton (alert.GetOption (i));
						else
							alertView.AddButton (alert.GetOption (i));
					}
				}

				alertView.Clicked += (object sender, UIButtonEventArgs args) => {
					if (args.ButtonIndex == alertView.CancelButtonIndex)
						alert.SetResult (-1);
					else
						alert.SetResult ((int)(args.ButtonIndex - startIndex));
				};
				alertView.Show ();

				await BlrtHelperiOS.AwaitAlert (alertView);
				tcs.SetResult (true);
			});

			await tcs.Task;
			return new ExecutionResult<IAlert> (alert);
		}

		public async Task<ExecutionResult<IProgressAlert>> ShowProgressAlert (IExecutor e, IProgressAlert alert)
		{
			try {
				var result = await BlrtHelperiOS.ShowWaitingProgressAlertAsync (alert.Source, alert.Title, alert.Message, alert.Cancel);
				alert.SetResult (result);
				return new ExecutionResult<IProgressAlert> (alert);
			} catch (Exception ex) {
				return new ExecutionResult<IProgressAlert> (ex);
			}
		}

		public object GetCurrentSlave ()
		{
			return Slave;
		}

		#endregion

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}

		#endregion

		#region IBlrtResponser implementation

		public void ActivateLockout (BlrtData.BlrtLockoutMode mode, bool animated)
		{
			BeginInvokeOnMainThread (() => {
				if (_lockoutController == null) {
					_lockoutController = new BlrtiOS.Views.Lockout.LockoutViewController ();
				}
				_lockoutController.Activite (mode, animated);

				if (_lockoutController.Mode != BlrtData.BlrtLockoutMode.None && _lockoutController.PresentingViewController == null) {


					UIViewController v = this.TopPresentedViewController ();

					v.PresentViewController (_lockoutController, animated, null);

				}
			});
		}

		public void DeactivateLockout (BlrtData.BlrtLockoutMode mode, bool animated)
		{
			BeginInvokeOnMainThread (() => {
				if (_lockoutController != null) {
					_lockoutController.Deactivite (mode, animated);
					if (_lockoutController.Mode == BlrtData.BlrtLockoutMode.None && _lockoutController.PresentingViewController == null) {

						_lockoutController.PresentingViewController.DismissViewController (false, () => {
							PresentViewController (_lockoutController, false, null);
						});
					}
				}
			});

		}

		public void ShowNotification (BlrtData.IBlrtNotification notification, bool animated)
		{

		}

		public void ShowFeedbackRatingDialog ()
		{

		}

		public bool CanOpenEvent (BlrtData.BlrtEventType eventType)
		{
			return true;
		}

		public void PushLoading (bool animated)
		{
			PushLoading ("_responder", animated);
		}

		public void PopLoading (bool animated)
		{
			PopLoading ("_responder", animated);
		}

		public void Logout (bool animated)
		{
			FBManager.Logout ();
			BeginInvokeOnMainThread (() => {
				if (LoginNav == null) {
					LoginNav = new BlrtiOS.Views.Login.LoginNavigationController ();
				}
				LoginNav.PopToRootViewController (false);
				ShowLoading (true);
				if (!InMaintanceMode)
					PresentViewController (LoginNav, true, InitPhoneLib);
			});
		}

		public void Login (bool isNewUser, bool animated, bool signupWithSMSLink)
		{
			if (LoginNav != null) {

				if (LoginNav.PresentingViewController == this) {
					DismissViewController (true, async () => {
						AfterLoginAction (isNewUser, animated, signupWithSMSLink);
					});
				} else {
					AfterLoginAction (isNewUser, animated, signupWithSMSLink);
				}
				BlrtiOS.Views.Mailbox.MailboxListController.FirstTimeLogin = true;
			} else {
				AfterLoginAction (isNewUser, animated, signupWithSMSLink);
			}
			FBManager.LoginWithParse ();
			BlrtUserHelper.GetLatestAvatar ();
			OpenMailbox (this, MailboxType.Inbox);
			HideLoading (true);
			InitPhoneLib ();
		}

		void Deeplink ()
		{
			/*************/
			//check meta data from Yozio to deferred deep link
			string new_url = BlrtConstants.UrlProtocolWithSlashes + "yozio.com/";
			//Acquire meta data from Yozio link
			Dictionary<string, string> dic = BlrtUserHelper.InstallMetaData;

			if (dic == null) {
				return;
			}

			if (dic.ContainsKey ("__blrtaction")) {
				new_url += dic ["__blrtaction"];
			} else if (dic.ContainsKey (BlrtUtil.URLConstants.Paths.BLRT)) {
				new_url += BlrtUtil.URLConstants.Paths.BLRT;
			} else if (dic.ContainsKey ("convo")) {
				new_url += BlrtUtil.URLConstants.Paths.CONVERSATION;
			} else if (dic.ContainsKey ("req")) {
				new_url += "make/blrtrequest";
			} else if (dic.ContainsKey ("oclist")) {
				new_url += "oclist";
			}
			string queryString = "?";
			foreach (var kv in dic) {
				queryString += (System.Web.HttpUtility.UrlEncode (kv.Key) + "=" + System.Web.HttpUtility.UrlEncode (kv.Value != null ? kv.Value.ToString () : "true") + "&");
			}
			if (queryString.EndsWith ("&", StringComparison.Ordinal)) {
				queryString = queryString.Substring (0, queryString.Length - 1);
			}
			//check the probability. Only deeplink when the probability is at least 85%.
			if (dic.ContainsKey ("yozio_probability")) {
				double prob = Convert.ToDouble (dic ["yozio_probability"]);
				if (prob >= 0.85) {
					new_url += queryString;
				} else {
					return;
				}
			}
			BlrtData.BlrtManager.ParseUrl (new_url, true);


			/************/
		}

		async void AfterLoginAction (bool isNewUser, bool animated, bool signupWithSMSLink)
		{
			//force user to have display name
			if (BlrtUserHelper.HasnotSetDisplayname) {
				await ForceAddDisplayName ();
			} else {
				/* [HACK] this is to allow the main thread to finish showing the mailbox overlay. Otherwise "viewWillDisappear" is triggered before the ovelay has been created in the "viewWillAppear". 
					Without doing this, overlay is created after "viewWillDisappear" has been triggered, effectively failing to remove overlay from the screen */
				await Task.Delay (1);
			}
			//ask user for phone number
			if (isNewUser || (!ParseUser.CurrentUser.Get ("hasShownAddPhoneScreen", false) && string.IsNullOrEmpty (BlrtUserHelper.PhoneNumber))) {
				var openfrom = signupWithSMSLink ? OpenFrom.SingupWithSMS : OpenFrom.Signup;
				await OpenAddPhone (BlrtData.ExecutionPipeline.Executor.System (), openfrom);
				ParseUser.CurrentUser ["hasShownAddPhoneScreen"] = true;
				await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				//deep-linking
				Deeplink ();

			}
		}

		public async Task ForceAddDisplayName ()
		{
			var name = await GetDisplayName ();
			ParseUser.CurrentUser [BlrtUserHelper.DisplayNameKey] = name.Trim ();
			ParseUser.CurrentUser [BlrtUserHelper.HasnotSetDisplaynameKey] = false;
			BlrtData.Helpers.BlrtTrack.ProfileEditDisplayname (name);
			ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
			Xamarin.Forms.MessagingCenter.Send<object> (this, "DisplaynameChanged");
		}
		async Task<string> GetDisplayName ()
		{
			var tcs = new TaskCompletionSource<bool> ();
			var alert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("signup_displayname_alert_title", "profile"),
				null,
				null,
				BlrtUtil.PlatformHelper.Translate ("OK")
			);

			alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKeyboardType.Default;
			alert.GetTextField (0).Placeholder = BlrtUtil.PlatformHelper.Translate ("edit_profile_display_name_field_new", "profile");

			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				tcs.TrySetResult (true);
			};

			alert.Show ();

			if (!await tcs.Task)
				return "";

			//check that the value given is usable
			var name = alert.GetTextField (0).Text;
			if (string.IsNullOrWhiteSpace (name)) {
				var invalidAlert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_message", "profile"),
					null,
					"OK"
				);
				invalidAlert.Show ();
				await BlrtHelperiOS.AwaitAlert (invalidAlert);
				return await GetDisplayName ();
			} else {
				return name;
			}
		}


		void InitPhoneLib ()
		{
			//only call this function after all ui displayed. This function may cause the deplay of showing any ui
			InvokeInBackground (() => {
				//init phone lib
				BlrtUtil.GetPhoneCountryPrefix (BlrtUtil.PlatformHelper.CountryCode);
			});
		}

		public void OpenMediaFromUrl (string url)
		{
			OpenMediaFromUrl (new string [] { url });
		}

		public void OpenMediaFromUrl (string [] urls)
		{
			//use create new Blrt performer, fix bugs: https://thinkun.atlassian.net/browse/BLRT-1345
			//old code doesn't consider the case to open mailbox conversation, then send dialog after creation
			var mediaUrlHandler = new ExternalMediaUrlHandler (Executor.System (), new BlrtRequestArgs () {
				DownloadUrls = urls
			});
			mediaUrlHandler.RunAction ();
		}

		public void RecalulateAppBadge ()
		{
			int total = BlrtManager.DataManagers.Default.TotalUnreadCount ();

			InvokeOnMainThread (() => {

				if (_appMenu != null)
					_appMenu.SetInboxBadge (total);

				if (Slave is Mailbox.MailboxController)
					(Slave as Mailbox.MailboxController).ReloadMailbox ();

				UIApplication.SharedApplication.ApplicationIconBadgeNumber = total;
				BlrtParseInstallation.Current.Update ();

				ShowTos ();
			});
		}



		public async void ShowTos ()
		{
			try {
				if (ParseUser.CurrentUser != null && !ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HasAgreedToTOSKey, false)) {

					await BlrtData.Query.BlrtSettingsQuery.RunQuery ().WaitAsync ();

					if (!ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HasAgreedToTOSKey, false) && BlrtManager.Query.ValidateSettings) {

						InvokeOnMainThread (() => {

							if (InMaintanceMode)
								return;

							if (_tosNav == null || PresentedViewController != _tosNav) {

								TermsController webView = new TermsController ();

								webView.OnPageLoadFailed += delegate {
									_tosNav.DismissViewController (true, null);
									BlrtManager.Logout (true);
									_tosNav = null;
								};

								webView.OnAccept += delegate {
									ParseUser.CurrentUser [BlrtUserHelper.HasAgreedToTOSKey] = true;
									BlrtManager.DataManagers.SaveAll ();
									_tosNav.DismissViewController (true, null);
									_tosNav = null;
								};

								webView.OnCancel += delegate {
									_tosNav.DismissViewController (true, null);
									BlrtManager.Logout (true);
									_tosNav = null;
								};

								// TODO: Replace "asd" with tag manager stuff (search all uses of TermOfServiceUrl and handle appropriately)
								webView.Url = String.Format (BlrtData.BlrtSettings.TermOfServiceUrl, "asd");

								_tosNav = new UINavigationController (webView);
								_tosNav.ModalPresentationStyle = webView.ModalPresentationStyle;

								PresentViewController (_tosNav, true, null);
							}
						});
					}
				}
			} catch (Exception ex) {
				LLogger.WriteEvent ("RootAppController", "ShowTOS", "Exception", ex.ToString ());
			}
		}

		public void OpenWebUrl (string str, bool openInApp)
		{
			if (openInApp) {
				var webController = new WebViewController () {
					ModalPresentationStyle = UIModalPresentationStyle.FullScreen
				};
				webController.Url = str;


				var _webNavController = new UINavigationController (webController);
				_webNavController.ModalPresentationStyle = webController.ModalPresentationStyle;


				webController.OnDismiss += (s1, e1) => {
					DismissViewController (true, null);
				};

				ShowViewController (_webNavController, this);
			} else {
				UIApplication.SharedApplication.OpenUrl (new NSUrl (str));
			}
		}

		public bool ShowUpgradeModal (BlrtData.BlrtPermissions.BlrtAccountThreshold threshold)
		{
			throw new NotImplementedException ();
		}

		public bool InMaintanceMode {
			get { return _lockoutController != null && _lockoutController.Mode != BlrtData.BlrtLockoutMode.None; }
		}

		public string AppVersion { get { return BlrtConfig.APP_VERSION; } }

		public string AppName { get { return BlrtConfig.APP_NAME; } }

		public string OS { get { return "iOS"; } }

		BlrtData.DeviceDetails _device;

		public BlrtData.DeviceDetails DeviceDetails {
			get {
				if (_device == null) {
					_device = new BlrtData.DeviceDetails (
						AppVersion,
						UIKit.UIDevice.CurrentDevice.Model,
						UIKit.UIDevice.CurrentDevice.SystemName,
						UIKit.UIDevice.CurrentDevice.SystemVersion,
						Foundation.NSLocale.PreferredLanguages,
						Foundation.NSLocale.CurrentLocale.CountryCode
					);
				}

				return _device;
			}
		}

		#endregion

	}
}

