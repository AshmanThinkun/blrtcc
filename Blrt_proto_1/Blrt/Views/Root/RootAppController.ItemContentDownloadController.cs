﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlrtData;
using Parse;

namespace BlrtiOS.Views.Root
{
	public partial class RootAppController
	{
		static int CONVERSATION_ITEM_DOWNLOAD_PRIORITY = 1000;
		static int CURRENT_CONVERSATION_ITEM_DOWNLOAD_PRIORITY = CONVERSATION_ITEM_DOWNLOAD_PRIORITY + 10;

		/// <summary>
		/// The number of itmes to download content for in newly fetched conversation
		/// </summary>
		static byte ITMES_TO_DOWNLOAD_CONTENT_FOR_IN_NEWLY_FETCHED_CONVERSATION = 1;

		/// <summary>
		/// Contains all the items whose content need to be downloaded.
		/// Key = Conversation id,
		/// Value = Set of conversation items ids
		/// </summary>
		Dictionary<string, HashSet<string>> PendingItemsToDownloadContentFor;

		/// <summary>
		/// Gets or sets a value indicating whether content is being downloaded for an item
		/// </summary>
		/// <value><c>true</c> if downloading content for pending items; otherwise, <c>false</c>.</value>
		bool DownloadingContentForPendingItems { get; set; }

		/// <summary>
		/// Invoked after a refresh on a given conversation has completed.
		/// </summary>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="conversationItemCountBeforeRefresh">Conversation item count before refresh.</param>
		/// <param name="refreshResults">Results.</param>
		/// <param name="newConversationFetched">If set to <c>true</c> new conversation fetched.</param>
		void RefreshDidComplete (string conversationId, 
		                         int conversationItemCountBeforeRefresh, 
		                         IEnumerable<ParseObject> refreshResults,
		                         bool newConversationFetched)
		{

			/* 
			 * Note: the following statement assumes that the "results" enumerable is in descending order 
			 * with the most recent item being the first item in the list and the oldest item being the last.
			 * This assumption is based on a fact that the parse query to query items from parse backend requests 
			 * the items in descending order.
			 */
			IEnumerable<string> itemsToDownloadContentFor = new List<string> ();

			if (newConversationFetched) {
				// download content for only certain number of items
				itemsToDownloadContentFor = itemsToDownloadContentFor
					.Take (ITMES_TO_DOWNLOAD_CONTENT_FOR_IN_NEWLY_FETCHED_CONVERSATION);
			} else {
				itemsToDownloadContentFor = GetItemsToDownloadContentFor (refreshResults, conversationItemCountBeforeRefresh);
			}

			AddItemsToDownloadContentFor (conversationId, itemsToDownloadContentFor);
		}

		/// <summary>
		/// Gets the items to download content for.
		/// </summary>
		/// <returns>The items to download content for.</returns>
		/// <param name="refreshResults">Refresh results.</param>
		/// <param name="conversationItemCountBeforeRefresh">Conversation item count before refresh.</param>
		List<string> GetItemsToDownloadContentFor (IEnumerable<ParseObject> refreshResults, int conversationItemCountBeforeRefresh)
		{
			var itemsToDownloadContentFor = new List<string> ();

			foreach (var result in refreshResults) {
				int index = -1;

				// check index
				if (result.TryGetValue (ConversationItem.INDEX_KEY, out index)) {
					if (index < conversationItemCountBeforeRefresh) {
						// if the index of the item is less than that of the last item before refresh, break the loop
						break;
					}	
				}

				int type = -1;

				// check whether the result has any media content to download
				if (result.TryGetValue (ConversationItem.TYPE_KEY, out type)) {
					var contentType = (BlrtContentType)type;

					// only blrts, images, PDFs and audio items have media content
					if (contentType == BlrtContentType.Blrt		 ||
					    contentType == BlrtContentType.BlrtReply ||
					    contentType == BlrtContentType.Audio 	 ||
					    contentType == BlrtContentType.Media) 
					{
						itemsToDownloadContentFor.Add (result.ObjectId);
					}
				}
			}

			return itemsToDownloadContentFor;
		}

		/// <summary>
		/// Adds the items to download content for.
		/// </summary>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="itemsToDownloadContentFor">Items to download content for.</param>
		void AddItemsToDownloadContentFor (string conversationId, IEnumerable<string> itemsToDownloadContentFor)
		{
			if (PendingItemsToDownloadContentFor == null) {
				PendingItemsToDownloadContentFor = new Dictionary<string, HashSet<string>> ();
			}

			// block multiple threads from accessing it at a time
			lock (PendingItemsToDownloadContentFor) {

				HashSet<string> itemsToDownloadContentForInPendinList;
				if (PendingItemsToDownloadContentFor.TryGetValue (conversationId, out itemsToDownloadContentForInPendinList)) {
					foreach (var item in itemsToDownloadContentFor) {
						itemsToDownloadContentForInPendinList.Add (item);
					}
				} else {
					PendingItemsToDownloadContentFor.Add (conversationId, itemsToDownloadContentFor.ToHashSet ());
				}
			}

			DownloadContentForPendingItems ();
		}

		/// <summary>
		/// Downloads the content for pending items.
		/// </summary>
		void DownloadContentForPendingItems ()
		{
			if (DownloadingContentForPendingItems) {
				return;
			}

			DownloadingContentForPendingItems = true;

			Task.Run (delegate {
				try {
					string pendingConversationItemId = string.Empty;
					bool itemForCurrentConversation = false;
					while (!string.IsNullOrWhiteSpace((pendingConversationItemId = GetNextPendingConversationItemId (out itemForCurrentConversation)))) {
						DownloadPendingConversationItemContent (pendingConversationItemId, GetItemDownloadPriority(itemForCurrentConversation));
					}
				} finally {
					DownloadingContentForPendingItems = false;
				}
			});
		}

		/// <summary>
		/// Gets the item download priority.
		/// </summary>
		/// <returns>The item download priority.</returns>
		/// <param name="itemForCurrentConversation">If set to <c>true</c> item for current conversation.</param>
		int GetItemDownloadPriority (bool itemForCurrentConversation)
		{
			return itemForCurrentConversation ? CURRENT_CONVERSATION_ITEM_DOWNLOAD_PRIORITY : CONVERSATION_ITEM_DOWNLOAD_PRIORITY;
		}

		/// <summary>
		/// Gets the next pending conversation item identifier.
		/// </summary>
		/// <returns>The next pending conversation item identifier.</returns>
		string GetNextPendingConversationItemId (out bool itemForCurrentConversation) 
		{
			if (SettlingHighPriorityPushNotification) {
				itemForCurrentConversation = false;
				return null;
			}

			var conversationId = CurrentConversationId;

			lock(PendingItemsToDownloadContentFor) {
				
				HashSet<string> convoItemIds = null;
				string convoItemId = null;

				if (PendingItemsToDownloadContentFor.TryGetValue (conversationId, out convoItemIds)) {
					// given convo id found in the pending list
					convoItemId = convoItemIds.FirstOrDefault ();
					itemForCurrentConversation = true;
				} else {
					// given convo id NOT found in the pending list

					/*
					 * KeyValuePair <convoId, set of convo item ids>
					*/
					var pair = PendingItemsToDownloadContentFor?.FirstOrDefault ();

					conversationId = pair?.Key;
					convoItemIds = pair?.Value;
					convoItemId = convoItemIds?.FirstOrDefault ();
					itemForCurrentConversation = false;
				}

				convoItemIds?.Remove (convoItemId); // remove convo item id from the set

				if (convoItemIds?.Count == 0) {
					// remove convo id from the dictionary
					PendingItemsToDownloadContentFor?.Remove (conversationId);
				}

				return convoItemId;

			}
		}

		/// <summary>
		/// Downloads the content of the pending conversation item.
		/// </summary>
		/// <returns>The pending conversation item content.</returns>
		/// <param name="pendingConversationItemId">Pending conversation item identifier.</param>
		void DownloadPendingConversationItemContent (string pendingConversationItemId, int downloadPriority)
		{
			var convoItem = GetConversationItemFromLocalStorage (pendingConversationItemId);

			if (convoItem == null) {
				return;
			}

			Task.WaitAll (convoItem.DownloadContent (downloadPriority).Select (a => a.WaitAsync ()).ToArray ());
			Task.WaitAll (convoItem.DownloadMedia (downloadPriority).Select (a => a.WaitAsync ()).ToArray ());
		}

		/// <summary>
		/// Gets the conversation item.
		/// </summary>
		/// <returns>The conversation item.</returns>
		/// <param name="pendingConversationItemId">Pending conversation item identifier.</param>
		ConversationItem GetConversationItemFromLocalStorage (string pendingConversationItemId)
		{
			return LocalStorageParameters.Default?.DataManager?.GetConversationItem (pendingConversationItemId);
		}
	}
}
