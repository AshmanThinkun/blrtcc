using System;
using System.Collections;
using CoreGraphics;
using Foundation;
using UIKit;
using Social;
using MessageUI;
using System.Collections.Generic;
using System.Timers;
using BlrtData;

namespace BlrtiOS.Views.Mailbox
{
	public class TipBannerViewCell : UIView
	{
		public static readonly UIFont BannerFont = UIFont.SystemFontOfSize (13.5f);

		private UIButton _preBtn, _nextBtn;

		private CopyView _copyView;

		public EventHandler PreBtnClicked, NextBtnClicked, CopyBtnClicked;

		public TipBannerViewCell ():base()
		{
			_preBtn = new UIButton (){

			};
			_preBtn.SetImage (UIImage.FromBundle ("canvas_page_prev.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal);
			_preBtn.TouchUpInside += (sender, e) => {
				if(null != PreBtnClicked)
					PreBtnClicked(this,EventArgs.Empty);
			};

			_nextBtn = new UIButton (){
			};
			_nextBtn.SetImage (UIImage.FromBundle ("canvas_page_next.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal);
			_nextBtn.TouchUpInside += (sender, e) => {
				if(null != NextBtnClicked)
					NextBtnClicked(this,EventArgs.Empty);
			};

			_copyView = new CopyView ();
			_copyView.TouchUpInside += (sender, e) => {
				if(null != CopyBtnClicked)
					CopyBtnClicked(this,EventArgs.Empty);
			};

			var leftSwipe = new UISwipeGestureRecognizer (() => {
				if(NextBtnClicked != null)
					NextBtnClicked(this,EventArgs.Empty);
			});
			leftSwipe.Direction = UISwipeGestureRecognizerDirection.Left;
			leftSwipe.ShouldRecognizeSimultaneously = (gestureRecognizer, otherGestureRecognizer) => true;
			AddGestureRecognizer (leftSwipe);

			var rightSwipe = new UISwipeGestureRecognizer (() => {
				if(PreBtnClicked != null)
					PreBtnClicked(this,EventArgs.Empty);
			});
			rightSwipe.Direction = UISwipeGestureRecognizerDirection.Right;
			rightSwipe.ShouldRecognizeSimultaneously = (gestureRecognizer, otherGestureRecognizer) => true;
			AddGestureRecognizer (rightSwipe);


			AddSubview (_preBtn);
			AddSubview (_nextBtn);
			AddSubview (_copyView);
		}

		public void SetContent(TipBanner currentBannerObj){
			switch(currentBannerObj.ColorScheme){
				case 1:
					BackgroundColor = BlrtStyles.iOS.Gray2;
					_preBtn.TintColor = BlrtStyles.iOS.Gray5;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray5;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray7;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.AccentBlue;
					break;
				case 2:
					BackgroundColor = BlrtStyles.iOS.Gray2;
					_preBtn.TintColor = BlrtStyles.iOS.Gray5;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray5;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray7;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.AccentRed;
					break;
				case 3:
					BackgroundColor = BlrtStyles.iOS.Gray2;
					_preBtn.TintColor = BlrtStyles.iOS.Gray5;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray5;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray7;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.Green;
					break;
				case 4:
					BackgroundColor = BlrtStyles.iOS.Charcoal;
					_preBtn.TintColor = BlrtStyles.iOS.Gray2;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray2;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray2;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.AccentBlue;
					break;
				case 5:
					BackgroundColor = BlrtStyles.iOS.Charcoal;
					_preBtn.TintColor = BlrtStyles.iOS.Gray2;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray2;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray2;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.AccentRed;
					break;
				case 6:
					BackgroundColor = BlrtStyles.iOS.Charcoal;
					_preBtn.TintColor = BlrtStyles.iOS.Gray2;
					_nextBtn.TintColor = BlrtStyles.iOS.Gray2;
					_copyView.TitleLbl.TextColor = BlrtStyles.iOS.Gray2;
					_copyView.ActionLbl.TextColor = BlrtStyles.iOS.Green;
					break;
			default:
				break;
			}



			_copyView.TitleLbl.Text = currentBannerObj.Title;
			_copyView.ActionLbl.Text = currentBannerObj.ActionText;

			SetNeedsLayout ();
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			const float ButtonWidth = 32;
			const float ButtonHeight = 48;
			const float Spacer = 13;
			const float Padding = 14;

			_preBtn.Frame = new CGRect (Padding, (Bounds.Height - ButtonHeight) * 0.5f, ButtonWidth, ButtonHeight);
			_nextBtn.Frame = new CGRect (_preBtn.Frame.Right + Spacer, (Bounds.Height - ButtonHeight) * 0.5f, ButtonWidth, ButtonHeight);

			nfloat width = Bounds.Width - (_nextBtn.Frame.Right + Spacer + Padding);

			var titleSize = _copyView.SizeThatFits (new CGSize (width, Bounds.Height));

			_copyView.Frame = new CGRect (_nextBtn.Frame.Right + Spacer, (Bounds.Height - titleSize.Height) * 0.5f, titleSize.Width, titleSize.Height);
		}

		class CopyView : UIButton
		{
			const int LabelSpace = 0;

			public UILabel TitleLbl { get; private set; }
			public UILabel ActionLbl { get; private set; }


			public CopyView(){
				TitleLbl = new UILabel(){
					Lines = 0,
					Font = BannerFont,
				};
				ActionLbl = new UILabel(){
					Lines = 0,
					Font = BannerFont,
				};

				AddSubviews (new []{
					TitleLbl,
					ActionLbl,
				});
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var s2 = ActionLbl.SizeThatFits (size);

				size.Height = size.Height - s2.Height - LabelSpace;
				var s1 = TitleLbl.SizeThatFits (size);

				return new CGSize (NMath.Min (NMath.Max (s1.Width, s2.Width), size.Width), NMath.Min (s1.Height + s2.Height + LabelSpace, size.Height));
			}


			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var s1 = TitleLbl.SizeThatFits (new CGSize(Bounds.Width, nfloat.MaxValue));
				var s2 = ActionLbl.SizeThatFits (new CGSize(Bounds.Width, nfloat.MaxValue));

				if(s1.Height+s2.Height + LabelSpace > Bounds.Height){
					s1.Height = Bounds.Height - s2.Height- LabelSpace;
				}
				TitleLbl.Frame = new CGRect (0, 0, Bounds.Width, s1.Height);
				ActionLbl.Frame = new CGRect (0, TitleLbl.Frame.Bottom + LabelSpace, Bounds.Width, s2.Height);

			}
		}
	}
}

