using System;
using System.Collections;
using CoreGraphics;
using Foundation;
using UIKit;
using Social;
using MessageUI;
using System.Collections.Generic;
using System.Timers;
using BlrtData;
using Newtonsoft.Json;
using BlrtiOS.Views.Util;

namespace BlrtiOS.Views.Mailbox
{
	public class TipBannerView : UIView
	{
		public const int space = 5;

		public float BannerHeight { get { return 82; } }

		public bool HasData {
			get { return TipBanners.Count > 0; }
		}

		public List<TipBanner> TipBanners = new List<TipBanner> ();
		public EventHandler DataFetched;

		private int _index;
		private Timer _timer;
		private UIViewController _parent;

		private TipBannerViewCell _tipBannerCell1, _tipBannerCell2, _activeCell;

		public TipBannerView (UIViewController parent):base()
		{
			Frame = new CGRect (0, 0, 10, 10);
			_parent = parent;
			getTipBanners ();
			_tipBannerCell1 = new TipBannerViewCell (){
				Frame = Bounds,
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};
			_tipBannerCell2 = new TipBannerViewCell (){
				Frame = Bounds,
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};

			AddSubview (_tipBannerCell1);
			AddSubview (_tipBannerCell2);


			_tipBannerCell1.PreBtnClicked += (sender, e) => {
				changeBanner (-1);
			};
			_tipBannerCell1.NextBtnClicked += (sender, e) => {
				changeBanner (1);
			};
			_tipBannerCell2.PreBtnClicked += (sender, e) => {
				changeBanner (-1);
			};
			_tipBannerCell2.NextBtnClicked += (sender, e) => {
				changeBanner (1);
			};

			_tipBannerCell1.CopyBtnClicked += (sender, e) => {
				processAction();
			};
			_tipBannerCell2.CopyBtnClicked += (sender, e) => {
				processAction();
			};


			_timer = new Timer ();
			_timer.AutoReset = false;
			_timer.Elapsed += (sender, e) => {
				InvokeOnMainThread (() => {
					changeBanner (1);
				});
			};
			_index = -1;
		}


		//indexShift = 1 ? -1
		private void changeBanner(int indexShift){

			try {
				if (indexShift == 0)
					return;
				
				_index = (_index + indexShift) % TipBanners.Count;
				var bannerObj = TipBanners [(_index + TipBanners.Count) % TipBanners.Count];
				
				_timer.Stop ();
				
				var other = _activeCell != _tipBannerCell1 ? _tipBannerCell1 : _tipBannerCell2;
				
				if (_activeCell != null) {
					if (indexShift > 0)
						other.Frame = new CGRect (Bounds.Width, 0, Bounds.Width, Bounds.Height);
					else
						other.Frame = new CGRect (-Bounds.Width, 0, Bounds.Width, Bounds.Height);
					other.SetContent (bannerObj);
				
					UIView.Animate (0.4f, () => {
						if (indexShift > 0)
							_activeCell.Frame = new CGRect (0 - Bounds.Width, 0, Bounds.Width, Bounds.Height);
						else
							_activeCell.Frame = new CGRect (Bounds.Width, 0, Bounds.Width, Bounds.Height);
						other.Frame = Bounds;
					}, () => {
						_activeCell = other;
					});
				} else {
					BeginInvokeOnMainThread (() => {
						try {
							_tipBannerCell2.Frame = new CGRect (0 - Bounds.Width, 0, Bounds.Width, Bounds.Height);
							other.SetContent (bannerObj);
							other.Frame = Bounds;
							_activeCell = other;
						} catch (Exception ex1) {
							LLogger.WriteEvent ("TipBanner", "ChangeBanner-MainThread", "Exception", ex1.ToString ());
						}
					});
				}
				
				_timer.Interval = bannerObj.TimeToStay * 1000;
				_timer.Start ();
			} catch (Exception ex2) {
				LLogger.WriteEvent ("TipBanner", "ChangeBanner", "Exception", ex2.ToString ());
			}
		}
			

		private void getTipBanners(){
			try {
				BlrtData.Query.BlrtUserGetTipBannersQuery.RunQuery ();
				BlrtData.TipBannerModel.ListChanged = (sender, e) => {
					TipBanners = BlrtData.TipBannerModel.BannerList;
					BeginInvokeOnMainThread(()=>{
						if (_index == -1) {
							changeBanner (1);
							this.Hidden = false;
						}
						if (null != DataFetched)
							DataFetched (this, EventArgs.Empty);
					});
				};
			} catch (Exception ex) {
				LLogger.WriteEvent ("TipBanner", "GetTipBanners", "Exception", ex.ToString ());
			}
		}



		public class BannerAction{
			[JsonProperty("action")]
			public string Action{get;set;}
			[JsonProperty("content")]
			public string Content{ get; set;}
		}

		private void processAction(){
			var bannerObj = TipBanners [(_index + TipBanners.Count) % TipBanners.Count];
			BannerAction action =null;
			try{
				action = JsonConvert.DeserializeObject <BannerAction>(bannerObj.Action);
			}catch{}
			if(null==action)
				return;
			if(action.Action=="upgrade"){
				BlrtData.BlrtManager.App.OpenUpgradeAccount (new BlrtData.ExecutionPipeline.Executor(BlrtData.ExecutionPipeline.ExecutionSource.InApp));
			}else if(action.Action=="url"){
				WebViewController webView = new WebViewController (){Url = action.Content};
				var nav = new UINavigationController (webView);
				_parent.PresentViewController (nav,true,null);

				var dic = new Dictionary<string,object> ();
				dic.Add ("url", action.Content);
				BlrtData.Helpers.BlrtTrack.TrackScreenView ("TipView");
				webView.OnDismiss += (sender, e) => {
					_parent.DismissViewController (true,null);
				};
			}else if(action.Action == "support"){
				BlrtData.BlrtManager.App.OpenSupport (BlrtData.ExecutionPipeline.Executor.System ());
			}
		}
	}
}

