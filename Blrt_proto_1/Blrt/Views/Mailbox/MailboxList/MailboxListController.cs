using System;
using UIKit;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using CoreGraphics;
using Xamarin.Forms;
using BlrtData;
using BlrtData.Models.Mailbox;
using BlrtiOS.UI;
using Parse;
using BlrtData.Views.Mailbox.UserFilter;
using Foundation;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxListController : UIViewController, IExecutor
	{
		const float RefreshHeight = 60;

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}


		#endregion

		IMailboxScreen _parent;

		MailboxListTableView _table;
		UIRefreshControl _refresh;

		MailboxType _type;
		TipBannerView _infoBanner;

		IMailboxFilter _filter;
		MailboxActionBuilder _actionBuilder;

		BlrtInboxWarning _warningView;
		int _refreshCounter;

		/*
		 * continer used to display mailbox in if it is required. At the moment it is only used when app is 
		 * launched via sharing to create blrt or share media
		 */
		UINavigationController _separateContainerForMailbox;
		bool _mailboxInSeperateContainer;

		public string SelectedConversation {get;set;}

		public object RequestArgsForExternalMedia { get; set; }

		public MailboxListController (IMailboxScreen parent, bool mailboxInSeperateContainer) : base ()
		{
			_mailboxInSeperateContainer = mailboxInSeperateContainer;
			_parent = parent;

			_actionBuilder = new MailboxActionBuilder (this);

			_table = new MailboxListTableView () {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			_refresh = new UIRefreshControl () {
				BackgroundColor = BlrtStyles.iOS.Green,
				Frame = new CGRect (0, 0, _table.Frame.Width, RefreshHeight)
			};

			_table.FilterButtonPressed += OpenFilterScreen;
			_table.KeywordChanged += SearchChanged;

			_infoBanner = new TipBannerView (this) {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleWidth, 
			};

			_warningView = new BlrtInboxWarning () {
				AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleWidth,
			};

			Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "DisplaynameChanged", async (o) => {
				if(_table!=null){
					await Refresh();
					BeginInvokeOnMainThread(()=>{
						_table.ReloadData();
					});
				}
			});

			Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "ReloadMailBox", (obj) => {
				if(_filter!=null){
					_filter.ReloadList();
				}	
			});
		}

		async Task<string> GetPrimaryContact ()
		{
			var currentUser = ParseUser.CurrentUser;

			var primaryContact = BlrtUserHelper.PrimaryEmail;

			if (primaryContact == null) {
				try {
					await BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ().WaitAsync ();
				} catch (Exception) {
					return null;
				}
			} else
				return primaryContact.ObjectId;

			primaryContact = BlrtUserHelper.PrimaryEmail;

			if (primaryContact == null)
				return null;

			return primaryContact.ObjectId;
		}

		/// <summary>
		/// Applies the readonly conversation filter and reloads data to see filtered data
		/// </summary>
		/// <returns>The readonly conversation filter.</returns>
		/// <param name="readonlyConversationHidden">If set to <c>true</c> readonly conversation hidden.</param>
		async Task ApplyReadonlyConversationFilter (bool readonlyConversationHidden)
		{
			_table.ListenToChanges = false;

			try {
				SetReadonlyConversationFilter (readonlyConversationHidden);
				await _filter.ReloadList ();			
			} catch {
			}

			_table.ListenToChanges = true;
			_table.ReloadData ();
		}

		/// <summary>
		/// Sets the readonly conversation filter BUT doesn't apply it. After setting filter, MailboxListTableview and 
		/// IMailboxFilter both need to be reloaded to see the filtered data.
		/// </summary>
		/// <param name="readonlyConversationHidden">If set to <c>true</c> readonly conversation hidden.</param>
		void SetReadonlyConversationFilter (bool readonlyConversationHidden)
		{
			_filter.ApplyReadonlyConversationHiddenFilter (readonlyConversationHidden);
		}

		async void SearchChanged (object sender, EventArgs e)
		{
			_table.ListenToChanges = false;

			try {
				_filter.ApplyKeywordFilter (_table.Keyword);
				await _filter.ReloadList ();			
			} catch {
			}

			_table.ListenToChanges = true;
			_table.ReloadData ();
		}

		public void OpenFilterScreen (object sender, EventArgs e)
		{
			var page = new FilterPage (this._filter);
			var controller = page.CreateViewController ();
			controller.Title = page.Title;
			controller.EdgesForExtendedLayout = UIRectEdge.None;
			controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (
				BlrtUtil.PlatformHelper.Translate ("clear_all", "conversation_option"), 
				UIBarButtonItemStyle.Plain, 
				(abc, def) => {
					page.ClearAll (this, EventArgs.Empty);
				}
			);

			if (_separateContainerForMailbox == null) {
				controller.View.Frame = new CGRect (0, 85, 100, 200);
				NavigationController.PushViewController (controller, true);
			} else {
				controller.View.Frame = new CGRect (0, 85 + 100, 100, 200);
				_separateContainerForMailbox.PushViewController (controller, true);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_infoBanner.DataFetched += (sender, e) => {
				RepositionTable (true);
			};
			_refresh.ValueChanged += (object sender, EventArgs e) => {
				Refresh ();
			};
			_table.AddSubview (_refresh);

			_table.Scrolled += TableScrolled;
			_warningView.SizeChanged += (sender, e) => {
				RepositionTable(true);
			};

			//			View.AddSubview (_warningView);
			if (_mailboxInSeperateContainer) 
			{
				SetMailboxInSeparateController ();
				SetReadonlyConversationFilter (true);
			} 
			else {
				View.AddSubview (_table);
				_table.SetFooterTextHidden (false);
			}

			View.AddSubview (_infoBanner);
			_table.ConversationSelected += ConversationSelected;


			if (_filter != null && _filter.ShouldRefresh)
				 Refresh ();


			BlrtUserHelper.InitilizeUser ();
		}

		/// <summary>
		/// Sets the mailbox in separate controller.
		/// </summary>
		void SetMailboxInSeparateController ()
		{
			var blrtImportController = new ContainerForBlrtImportController (_table, "Select a conversation", true);
			blrtImportController.OnCancelButtonPressed += delegate {
				DismissViewControllerIfMailboxInSeparateContainer (DisposeMailboxSeparateController);
			};
			_separateContainerForMailbox = new UINavigationController (blrtImportController);

			_table.SwipeDisabled = true;
			_table.SetFooterTextHidden (true);
			_separateContainerForMailbox.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;

			PresentViewController (_separateContainerForMailbox, true, null);
		}

		UITapGestureRecognizer _hideKeyboardGesture;

		/// <summary>
		/// this is the way to fix spinner animation bug
		/// </summary>
		/// <returns>The refresh spinner.</returns>
		async Task AnimateRefreshSpinner() {
			//if (_refresh.Refreshing) {
				BeginInvokeOnMainThread (() => {
					_refresh.RemoveFromSuperview();
					_refresh.EndRefreshing();
					_refresh.Enabled = false;
					_table.AddSubview(_refresh);
					_refresh.Enabled = true;
					_refresh.SetNeedsLayout();
					_refresh.SetNeedsDisplay();
					_refresh.TintColor = BlrtStyles.iOS.Charcoal;
					_refresh.BeginRefreshing();
					_table.SetContentOffset (new CGPoint (0, - RefreshHeight), false);
				});
			//}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

                                                                                                                                                                                                                                                                                           
			var firstTimeAppear = FirstTimeLogin && !MailboxOpenned;
			if (ParseUser.CurrentUser != null) {
				FirstTimeLogin = false;
				MailboxOpenned = true;
			}
			if (_refresh.Refreshing) {
				BeginInvokeOnMainThread (() => {
					if(firstTimeAppear){
						AnimateRefreshSpinner();
					}
					_refresh.BackgroundColor = BlrtStyles.iOS.Green;
				//	_table.SetContentOffset (new CGPoint (0, - RefreshHeight), false);
				});
			}

			//hack because the navigation bar needs to receive this event
			if (_hideKeyboardGesture == null) {
				_hideKeyboardGesture = new UITapGestureRecognizer (() => {
					View.EndEditing (true);
				});
				_hideKeyboardGesture.ShouldBegin += (UIGestureRecognizer recognizer) => {
					return _table.SearchFirstResponder;
				};
				_hideKeyboardGesture.CancelsTouchesInView = true;

				var v = View;

				while (v.Superview != null)
					v = v.Superview;

				v.AddGestureRecognizer (_hideKeyboardGesture);
			}

			try{
				if(Device.Idiom == TargetIdiom.Tablet || ParseUser.CurrentUser==null){

				}else if (!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayMailboxViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayMailboxViewed])
				{
					if(_helpOverlay==null)
						_helpOverlay = (UIView)BlrtData.Views.Helpers.HelpOverlayHelper.AddMailboxHelpOverlay();
					else{
						var overlay = (_helpOverlay as BlrtiOS.UI.HelpOverlay);
						if(overlay != null){
							overlay.ClearEventHandler();
							overlay.Open(false);
						}
					}
				}
			}catch(Exception e){

			}
		}

		public UIView _helpOverlay;
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			if (_helpOverlay != null && _helpOverlay.Superview!=null)
				_helpOverlay.RemoveFromSuperview ();
		}

		public static bool FirstTimeLogin;
		static bool MailboxOpenned;
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			var tt = _table.SelectedCell;
			_table.ReloadData ();
			if (_filter != null)
				_filter.ReloadList ();
		}

		void RepositionTable (bool animate)
		{
			if (_mailboxInSeperateContainer) {
				return;
			}

			if (_warningView.WarningList.Count==0) {
				_warningView.Frame = new CGRect (0, 0, View.Bounds.Width, 0);
				_warningView.Hidden = true;
			} else {
				_warningView.Frame = new CGRect (0, 0, View.Bounds.Width, _warningView.HeightRequest);
				_warningView.Hidden = false;
			}

		
			if (_infoBanner.HasData) {
				_table.Frame = new CGRect (0, _warningView.Frame.Bottom, View.Bounds.Width, View.Bounds.Height - _infoBanner.BannerHeight - _warningView.Frame.Height);
				_infoBanner.Frame = new CGRect (0, _table.Frame.Bottom, View.Bounds.Width, _infoBanner.BannerHeight);
				_infoBanner.Hidden = false;

			} else {
				_table.Frame = new CGRect (0, _warningView.Frame.Bottom, View.Bounds.Width, View.Bounds.Height - _warningView.Frame.Height);
				_infoBanner.Hidden = true;
			}

		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			RepositionTable (false); // TODO: reason fro layout issue
		}


		/// <summary>
		/// This Method checks if the table has been scrolled to the last display item. If found <see langword="true"/> 
		/// then it loads more items.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void TableScrolled (object sender, EventArgs e)
		{
			var currentOffset = _table.ContentOffset.Y;
			var maximumOffset = _table.ContentSize.Height - _table.Frame.Size.Height;

			// 10.0 to adjust the distance from bottom
			if (maximumOffset - currentOffset <= 10.0)
			{
				LoadNextPage ().FireAndForget ();
			}

		}

		void ConversationSelected (object sender, MailboxCellModel e)
		{
			SelectedConversation = e.ConversationId;
			var conversation = _parent.OpenConversation (this, e.ConversationId).GetAwaiter ().GetResult ().Result;

			DismissViewControllerIfMailboxInSeparateContainer (delegate {
				conversation.CreateConversationItemsUsingExternalMedia (RequestArgsForExternalMedia);
				DisposeMailboxSeparateController ();
			});
		}

		/// <summary>
		/// Dismisses the view controller if mailbox is in separate container. If only happens during media import to Blrt.
		/// </summary>
		/// <param name="onViewControllerDismissed">On view controller dismissed.</param>
		void DismissViewControllerIfMailboxInSeparateContainer (Action onViewControllerDismissed)
		{
			if (_mailboxInSeperateContainer) {

				_table.SetFooterTextHidden (false);

				DismissViewController (true, onViewControllerDismissed);

				_table.ContentInset = UIEdgeInsets.Zero;

				_mailboxInSeperateContainer = false;
				_parent.MailboxListInSeparateContainer = false;

				_table.ContentInset = UIEdgeInsets.Zero;
				_table.SwipeDisabled = false;


				InvokeOnMainThread (delegate {
					ApplyReadonlyConversationFilter (false).FireAndForget ();
					//_table.ReloadData ();
					View.AddSubview (_table);
				});
			}
		}


		/// <summary>
		/// Disposes the mailbox separate controller.
		/// </summary>
		void DisposeMailboxSeparateController()
		{
			_separateContainerForMailbox.Dispose ();
			_separateContainerForMailbox = null;
		}

		public async Task<IConversationScreen> OpenConversation (IExecutor e, string conversationId)
		{
			var convo = await _parent.OpenConversation (e, conversationId);
			return convo.Result;
		}

		public static Task CreateNewBlrt ()
		{
			return new ExternalMediaUrlHandler (new Executor (ExecutionSource.System)).RunAction ();
		}

		public async Task LoadNextPage ()
		{

			int counter = ++_refreshCounter;

			if (!_refresh.Refreshing)
				_refresh.BeginRefreshing ();
			_table.Footer.StartLoading ();
			if (_filter != null) {
				try {
					await _filter.LoadNextPage ();
				} catch {
				}
			} else {
			}

			Reload (SelectedConversation);
			_table.Footer.StopLoading ();
			if (_refresh.Refreshing && counter == _refreshCounter)
				_refresh.EndRefreshing ();
		}

		public async Task Refresh ()
		{
			InvokeInBackground (async delegate {
				int counter = ++_refreshCounter;

				InvokeOnMainThread (() => {
					_refresh.BackgroundColor = BlrtStyles.iOS.Green;
					if (!_refresh.Refreshing) {
						_refresh.BeginRefreshing ();
						_table.SetContentOffset (new CGPoint (0, -RefreshHeight), true);
					}
				});

				if (_filter != null) {
					try {
						await _filter.RefreshList (0);
					} catch {
					}
				} else {
				}

				InvokeOnMainThread (delegate {
					Reload (SelectedConversation);
				});

				Xamarin.Forms.MessagingCenter.Send<Object> (this, "ContactChanged");


					InvokeOnMainThread (() => {
						if (_refresh.Refreshing && counter == _refreshCounter)
							_refresh.EndRefreshing ();
					});
			});
		}

		public void Reload (string selectedConversation)
		{
			if (_filter != null) {
				_filter.ReloadList ();
				SelectCell (selectedConversation);
			}
		}

		protected override void Dispose (bool disposing)
		{
			Xamarin.Forms.MessagingCenter.Unsubscribe<Object> (this, "DisplaynameChanged");
			base.Dispose (disposing);
		}

		public void SelectCell (string conversationId)
		{
			if (_filter != null) {
				var cell = _filter.GetCell (conversationId);
				if (cell != null)
					_table.QuietSelect (cell, true);
			}
		}

		static MailboxType _lastType = MailboxType.Inbox;

		public void SetType (MailboxType type)
		{
			//try{
			//	if(ParseUser.CurrentUser==null){

			//	}else if (!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayMailboxViewed)
			//		|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayMailboxViewed])
			//	{
			//		_helpOverlay = (UIView) BlrtData.Views.Helpers.HelpOverlayHelper.AddMailboxHelpOverlay ();
			//	}
			//}catch{}

			type = type == MailboxType.Any ? _lastType : type;

			if ((_type != type && type != MailboxType.Any) || (null != _filter && !_filter.CurrentUser)) {
				_type = type;
				_lastType = type;
				switch (type) {
					case MailboxType.Inbox:
						_filter = new MailboxInboxFilter ();
						Title = BlrtHelperiOS.Translate ("inbox_screen", "screen_titles");
						break;
					case MailboxType.Archive:
						_filter = new MailboxArchiveFilter ();
						Title = BlrtHelperiOS.Translate ("archive_screen", "screen_titles");
						break;
				}

				_filter.ActionBuilder = _actionBuilder;
				_filter.OnReloadList += UpdateFilterCount;

				_filter.ReloadList ();
				_table.List = _filter.GetList ();



				if (_refresh != null) {
					InvokeOnMainThread (() => {
						_refresh.BackgroundColor = BlrtStyles.iOS.Green;
					});

					if (_filter.ShouldRefresh)
						Refresh ();
					else if (_refresh.Refreshing)
						InvokeOnMainThread (() => {
							_refresh.EndRefreshing ();
						});
				}


			} else {
				_table.List = _filter.GetList ();
				Reload (SelectedConversation);
			}

			if (_filter != null) {
				_table.Keyword = _filter.Keyword;
				_table.EndEditing (true);
			}

			var ViewName = "Mailbox_Inbox";
			switch (type) {
				case MailboxType.Inbox:
					ViewName = "Mailbox_Inbox";
					break;
				case MailboxType.Archive:
					ViewName = "Mailbox_Archive";
					break;
			}
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}

		public void UndoSwiped (bool animated)
		{
			_table.UndoSwiped (animated);
		}

		int _filterItemCount;
		void UpdateFilterCount(object sender, EventArgs e){
			if (_filter != null) {
				var userFilterCount = _filter.UserFilters != null ? _filter.UserFilters.Length : 0;
				var tagFilterCount = _filter.TagFilters != null ? _filter.TagFilters.Length : 0;
				var count = userFilterCount + tagFilterCount;

				if (_filterItemCount == count)
					return;

				Device.BeginInvokeOnMainThread (() => {
					_filterItemCount = count;
					_table.FilterBadge = count;
				});
			}
		}

	}
}

