using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtiOS.Views.CustomUI;
using BlrtiOS.Views.Util;
using BlrtData;
using BlrtData.Models.Mailbox;
using BlrtData.Views.Helpers;



namespace BlrtiOS.Views.Mailbox
{

	public class MailboxCell : SwipableCellView, IBindable<MailboxCellModel>, ITableInfoRequester {

		public const string Indentifer = MailboxCellModel.MailboxCellIndentifer;
		public const int CellHeight = 84;

		private CloudStateView _accessoryView;

		AsyncImageView _imageView;

		private UILabel _titleLbl;
		private UILabel _authorLbl;
		private UILabel _dateLbl;
		private BlrtsCounterView _counterView;

		MailboxCellMetaFlagsView _metaFlags;

		MailboxCellModel _model;
		ITableInfo _infoSrc = null;
		int _section = -1;
		int _row = -1;

		public MailboxCell (IntPtr handle) 
			: base(handle)
		{
			BackgroundColor = MailboxListTableView.CellBackgroundColor;
			SelectionStyle = UITableViewCellSelectionStyle.None;

			_imageView = new AsyncImageView(){
				ContentMode = UIViewContentMode.ScaleAspectFit
			};


			_titleLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				Lines = 2,	
				TextColor = BlrtStyles.iOS.Black
			};
			_authorLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellDetailFont,
				Lines = 1,			
				TextColor= BlrtStyles.iOS.Gray6
			};
			_dateLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellDetailBoldFont,
				Lines = 1,		
				TextColor = BlrtStyles.iOS.Gray6
			};
			_metaFlags = new MailboxCellMetaFlagsView ();


			_accessoryView = new CloudStateView () {
				Frame = new CGRect(0,0,50, CellHeight) 
			};

			_counterView = new BlrtsCounterView ();

			ContentView.AddSubviews (new UIView[] {
				_imageView,

				_titleLbl,
				_authorLbl,
				_dateLbl,

				_metaFlags,

				_accessoryView,
				_counterView
			});


			const float Width = 77;

			var _moreBtn = new CellButton () {
				Frame = new CGRect(0,0, Width, CellHeight),
				BackgroundColor = BlrtStyles.iOS.Gray4,
				AutoresizingMask = UIViewAutoresizing.FlexibleHeight,
			};
			_moreBtn.SetTitle("More", UIControlState.Normal);
			_moreBtn.SetImage(UIImage.FromBundle("inbox_ellipsis.png"), UIControlState.Normal);

			var tagBtn = new CellButton () {
				Frame = new CGRect(Width,0, Width, CellHeight),
				BackgroundColor = BlrtStyles.iOS.AccentBlue,
				AutoresizingMask = UIViewAutoresizing.FlexibleHeight,
			};
			tagBtn.SetTitle("Tags", UIControlState.Normal);
			tagBtn.SetImage(UIImage.FromBundle("tag_filter_filled.png"), UIControlState.Normal);


			this.SwipeStarted += Swiped;

			_moreBtn.TouchUpInside += MorePressed;
			tagBtn.TouchUpInside += TagPressed;


			RightView = new UIView(new CGRect(0,0,Width * 2, CellHeight)){
				_moreBtn,
				tagBtn
			};
		}

		#region ITableInfoRequester implementation

		public void SetInfoSource (ITableInfo src, int section, int row)
		{
			_infoSrc = src;
			_section = section;
			_row = row;
		}

		#endregion

		void MorePressed (object sender, EventArgs e)
		{
			if (_model != null && _model.MoreAction != null)
				_model.MoreAction.Action (sender);
		}

		void TagPressed (object sender, EventArgs e)
		{
			if (_model != null && _model.TagAction != null)
				_model.TagAction.Action (sender);
		}

		void Swiped (object sender, EventArgs e)
		{
			if (_model != null && _model.SwipeAction != null)
				_model.SwipeAction.Action (sender);
		}

		public void Bind (MailboxCellModel model) {	
			
			_model = model;
			_model.StartFileMonitoring ();
			_titleLbl.Text = _model.Title;
			_authorLbl.Text = string.Format(BlrtHelperiOS.Translate("inbox_blrt_cell_sub1"), _model.Author.DisplayName);
			_dateLbl.Text = BlrtHelperiOS.TranslateDate (_model.LastUpdated);

			_imageView.SetAsyncSource (_model.CloudPathThumbnail, _model.PathThumbnail);

			_metaFlags.NewHidden = _model.Unread == 0;
			_metaFlags.FlaggedHidden = !_model.Flagged;

			_accessoryView.CloudState = _model.CloudState;

			_counterView.SetState (_model.ContentCount, _model.Unread);
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetColors ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetColors ();
		}

		void SetColors ()
		{
			if (Highlighted)
				BackgroundColor = ContentView.BackgroundColor = BlrtStyles.iOS.Gray2;
			else if (Selected)
				BackgroundColor = ContentView.BackgroundColor = BlrtStyles.iOS.Gray3;
			else
				BackgroundColor = ContentView.BackgroundColor = MailboxListTableView.CellBackgroundColor;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			//the gap between the image and the copy
			const int Spacer = 21;

			var sep = 1 / UIScreen.MainScreen.Scale;

			ContentView.Frame = new CGRect (Pan,  ContentView.Frame.Y, Bounds.Width, Bounds.Height - sep);
			_imageView.Frame = new CGRect (0, 0, ContentView.Bounds.Height, ContentView.Bounds.Height);
			_accessoryView.Frame = new CGRect (
				Bounds.Width - _accessoryView.Frame.Width, 
				(Bounds.Height -_accessoryView.Frame.Height) * 0.5f, 
				_accessoryView.Frame.Width, 
				_accessoryView.Frame.Height
			);
			_metaFlags.Frame = new CGRect (_imageView.Frame.Right, 0, Spacer, ContentView.Bounds.Height);

			var width = ContentView.Frame.Width - (_accessoryView.Frame.Width + _imageView.Frame.Width + Spacer + 4);

			var titleSize = _titleLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));
			var authorSize = _authorLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));
			var dateSize = _dateLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));


			RightView.Frame = new CGRect (RightView.Frame.X, 0, RightView.Frame.Width, ContentView.Frame.Height);


			_titleLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				8, 
				width, 
				titleSize.Height
			);
			_authorLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				48, 
				width, 
				authorSize.Height
			);
			_dateLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				62, 
				width, 
				dateSize.Height
			);


			var countBounds = _counterView.Bounds;
			_counterView.Frame = new CGRect (
				_imageView.Frame.Left,
				_imageView.Frame.Bottom - countBounds.Height,
				countBounds.Width,
				countBounds.Height
			);
		}

		public override void PrepareForReuse ()
		{
			try {
				if ((null == _infoSrc) || (!_infoSrc.IsCellVisible (_section, _row))) {
					_imageView.ReleaseImage ();
				}
				base.PrepareForReuse ();
			} catch (Exception eerr) {
#if DEBUG
				Console.WriteLine (eerr.ToString ());
#endif
			}
		}
	}
}

