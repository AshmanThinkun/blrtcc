using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.CustomUI;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxCellMetaFlagsView : UIView
	{
		public bool NewHidden { get{ return _newIcon.Hidden; } 
			set { 
				if (value != _newIcon.Hidden) {
					_newIcon.Hidden = value;
					SetNeedsLayout ();
				} 
			} 
		}
		public bool FlaggedHidden { get{ return _flaggedIcon.Hidden; } 
			set { 
				if (value != _flaggedIcon.Hidden) {
					_flaggedIcon.Hidden = value;
					SetNeedsLayout ();
				} 
			} 
		}

		private UIView _newIcon;
		private UIView _flaggedIcon;

		public MailboxCellMetaFlagsView ()
		{
			_newIcon = new UIView () {
				BackgroundColor = BlrtStyles.iOS.AccentRed,
				Frame = new CoreGraphics.CGRect(0, 0, 8, 8),
				Layer = {
					CornerRadius = 4
				},
			};
			_flaggedIcon = new UIImageView (UIImage.FromBundle("flagged.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)) {
				TintColor = BlrtStyles.iOS.AccentRed,

			};


			AddSubviews (new [] {
				_newIcon,
				_flaggedIcon,
			});
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			//set the postion of the unread flag
			_newIcon.Frame = new CoreGraphics.CGRect (
				(Bounds.Width - _newIcon.Bounds.Width) * 0.5f, 
				13, 
				_newIcon.Bounds.Width, 
				_newIcon.Bounds.Height
			);

			//set the postion of the Flagged flag.  Set the position based on if the unread flag is visible or not
			if (_newIcon.Hidden) {
				_flaggedIcon.Frame = new CoreGraphics.CGRect (
					(Bounds.Width - _flaggedIcon.Bounds.Width) * 0.5f, 
					_newIcon.Frame.Y, 
					_flaggedIcon.Bounds.Width, 
					_flaggedIcon.Bounds.Height
				);
			} else {
				_flaggedIcon.Frame = new CoreGraphics.CGRect (
					(Bounds.Width - _flaggedIcon.Bounds.Width) * 0.5f, 
					_newIcon.Frame.Bottom + 5, 
					_flaggedIcon.Bounds.Width, 
					_flaggedIcon.Bounds.Height
				);
			}
		}
	}
}

