using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtiOS.Views.CustomUI;
using BlrtData.Models.Mailbox;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxListTableView : TableModelView<MailboxCellModel>
	{
		public static readonly UIColor TableBackgroundColor = BlrtStyles.iOS.White;
		public static readonly UIColor CellBackgroundColor = BlrtStyles.iOS.White;

		public bool SwipeDisabled { get; set; }

		public event EventHandler<MailboxCellModel> ConversationSelected;

		public event EventHandler KeywordChanged{
			add { _header.KeywordChanged += value; }
			remove { _header.KeywordChanged -= value; }
		}

		public event EventHandler FilterButtonPressed {
			add { _header.FilterButtonPressed += value; }
			remove { _header.FilterButtonPressed -= value; }
		}

		public string Keyword {
			get {return _header.Keyword; }
			set {_header.Keyword = value; }
		}

		public int FilterBadge {
			get{
				return _header.FilterBadge;
			}
			set{
				_header.FilterBadge = value;
			}
		}

		public bool SearchFirstResponder {
			get { return _header.SearchFirstResponder; }
		}

		MailboxFooterView _footer;
		MailboxSearchBar _header;

		public MailboxListTableView ()
			: base()
		{
			RegisterClassForCellReuse (typeof(MailboxCell), new NSString (MailboxCell.Indentifer));


			this.BackgroundColor		= TableBackgroundColor;
			this.Bounces				= true;

			this.SeparatorInset			= new UIEdgeInsets(0,MailboxCell.CellHeight + 8,0,0);
			this.SeparatorStyle			= UITableViewCellSeparatorStyle.SingleLine;

			this.RowHeight				= MailboxCell.CellHeight;
			this.SectionHeaderHeight	= 0;
			this.SectionFooterHeight	= 0;

			_header = new MailboxSearchBar () {
				Frame = new CGRect(0,0,Frame.Width,50),
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth
			};


			_footer = new MailboxFooterView ();
			this.TableHeaderView = _header;
			this.TableFooterView = _footer;
		}

		public MailboxFooterView Footer{
			get{
				return _footer;
			}
		}

		static byte FOOTER_HEIGHT_REGULAR = 32; // used when text in footer is hidden
		static byte FOOTER_HEIGHT_EXTENDED = 96; // used texet in footer is visible

		/// <summary>
		/// Sets the footer text hidden.
		/// </summary>
		/// <param name="hidden">If set to <c>true</c> hidden.</param>
		internal void SetFooterTextHidden (bool hidden)
		{
			Footer.SetFooterTextHidden (hidden);
			Footer.Frame = new CGRect (0, 0, Frame.Width, hidden ? FOOTER_HEIGHT_REGULAR : FOOTER_HEIGHT_EXTENDED);
		}

		public override UITableViewCell DequeueReusableCell (NSString reuseIdentifier, NSIndexPath indexPath)
		{
			var cell = base.DequeueReusableCell (reuseIdentifier, indexPath);

			if (cell is SwipableCellView) {
				var slider = cell as SwipableCellView;
				slider.SwipeDisabled = SwipeDisabled;
				slider.SwipeStarted += SwipeStarted;
			}

			return cell;
		}

		void SwipeStarted (object sender, EventArgs e)
		{
			UndoSwiped(sender as SwipableCellView, true);
		}

		public override void CellSelected (MailboxCellModel cell)
		{
			base.CellSelected (cell);

			if (ConversationSelected != null)
				ConversationSelected (this, cell);
		}

		public void UndoSwiped(bool animated) {
			UndoSwiped (null, animated);
		}

		void UndoSwiped(SwipableCellView cell, bool animated){

			foreach (var c in VisibleCells) {

				if (c != cell && c is SwipableCellView) {
					var slider = c as SwipableCellView;

					if(slider.Pan != 0)
						slider.SetPan (0, animated);
				}
			}
		}

}
}

