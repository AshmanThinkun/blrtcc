using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.Views.CustomUI;
using BlrtData;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxSearchBar: UIView
	{
		const int padding = 5;
		UISearchBar _search;
		FilterButton _filterBtn;
		UIView _separator;

		public string Keyword{
			get{return _search.Text;}
			set{_search.Text = value;}
		}

		public int FilterBadge{
			get{
				return _filterBtn.Badge;
			}
			set{
				if(_filterBtn.Badge != value)
					_filterBtn.SetState (value) ;
			}
		}


		public bool SearchFirstResponder {
			get { return _search.IsFirstResponder; }
		}

		UIEdgeInsets _padding;


		public event EventHandler KeywordChanged;

		public event EventHandler FilterButtonPressed{
			add { _filterBtn.TouchUpInside += value; }
			remove { _filterBtn.TouchUpInside -= value; }
		}
		public MailboxSearchBar ():base()
		{
			_padding = new UIEdgeInsets (10,0,10,0);

			Frame = new CGRect (0, 0, Frame.Width, 50);
			AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

			_search = new UISearchBar () {
				SearchBarStyle = UISearchBarStyle.Minimal,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("Search")
			};
			_search.TextChanged += SearchTextChanged;

			_filterBtn = new FilterButton () {
			};

			_separator = new UIView () {
				BackgroundColor = BlrtStyles.iOS.Gray4
			};

			AddSubview (_search);
			AddSubview (_filterBtn);
			AddSubview (_separator);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			_filterBtn.Frame = new CGRect (Bounds.Width - _filterBtn.Bounds.Width - padding, (int)((Bounds.Height - _filterBtn.Bounds.Height) * 0.5f), _filterBtn.Bounds.Width, _filterBtn.Bounds.Height);

			var rect = _padding.InsetRect (Bounds);
			rect.Width -= _filterBtn.Bounds.Width + padding;

			_search.Frame = rect;

			var h = 1 / UIScreen.MainScreen.Scale;

			_separator.Frame = new CGRect (0, Bounds.Height - h, Bounds.Width, h	);
		}

		void SearchTextChanged (object sender, UISearchBarTextChangedEventArgs e)
		{
			if (KeywordChanged != null)
				KeywordChanged (this, EventArgs.Empty);
		}
	}
}

