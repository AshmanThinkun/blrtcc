using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.CustomUI;
using CoreGraphics;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxFooterView : UIView
	{
		private UILabel _lbl;
		UIActivityIndicatorView _loader;

		public MailboxFooterView ()
		{
			Frame = new CGRect (0, 0, 120, 96);
			BackgroundColor = BlrtStyles.iOS.White;

			this.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

			_lbl = new UILabel () {
				Font = BlrtStyles.iOS.CellTitleFont,
				TextColor = BlrtStyles.iOS.Gray5,
				Lines = 0,
				Text = BlrtHelperiOS.Translate("mailbox_footer"),
				TextAlignment = UITextAlignment.Center,
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};

			_loader = new UIActivityIndicatorView (){
				Color = BlrtStyles.iOS.Charcoal,
				Hidden = true
			};
			_loader.SizeToFit ();

			AddSubview (_lbl);
			AddSubview (_loader);
		}

		/// <summary>
		/// Sets the footer text hidden.
		/// </summary>
		/// <param name="hidden">If set to <c>true</c> hidden.</param>
		public void SetFooterTextHidden(bool hidden)
		{
			_lbl.Hidden = hidden;
		}

		public void StartLoading(){
			InvokeOnMainThread (() => {
				_loader.Hidden = false;
				_loader.StartAnimating ();
			});
		}

		public void StopLoading(){
			InvokeOnMainThread (() => {
				_loader.Hidden = true;
				_loader.StopAnimating ();
			});
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (!_lbl.Hidden) {
				_lbl.Frame = new CGRect (20, 0, Bounds.Width - 40, Bounds.Height);
			}
			_loader.Center = new CGPoint (Bounds.Width * 0.5f, Bounds.Height - 20);
		}
	}
}

