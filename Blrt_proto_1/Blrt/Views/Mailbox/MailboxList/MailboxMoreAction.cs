using System;
using UIKit;
using BlrtData;
using MessageUI;
using BlrtData.Models.Mailbox;
using BlrtData.Views.Helpers;
using BlrtData.Views.Mailbox;
using Xamarin.Forms;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using Parse;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxActionBuilder : IMailboxActionBuilder{


		MailboxListController _controller;

		public MailboxActionBuilder(MailboxListController controller){
			_controller = controller;
		}

		public ICellAction MoreAction (MailboxCellModel model)
		{
			return new MailboxMoreAction (model, _controller);
		}

		public ICellAction TagAction (MailboxCellModel model)
		{
			return new MailboxTagsAction (model, _controller);
		}

		public ICellAction SwipeAction (MailboxCellModel model)
		{
			return new MailboxSwipeAction (model, _controller);
		}
	}

	public class MailboxSwipeAction: ICellAction, IExecutor
	{
		MailboxCellModel _model;
		MailboxListController _controller;

		public MailboxSwipeAction (MailboxCellModel model, MailboxListController controller)
		{
			_controller = controller;
			_model = model;
		}

		public void Action (object sender)
		{
			if(!BlrtHelperiOS.IsPhone)
				_controller.OpenConversation (this, _model.ConversationId);
		}

		#region IExecutor implementation

		ExecutionSource IExecutor.Cause {
			get {
				return ExecutionSource.System; 
			}
		}

		#endregion
	}

	public class MailboxTagsAction : ICellAction,IExecutor
	{
		MailboxCellModel _model;
		MailboxListController _controller;

		public MailboxTagsAction (MailboxCellModel model, MailboxListController controller)
		{
			_controller = controller;
			_model = model;
		}

		public void Action (object sender)
		{

			try{
				if(!_model.OnParse){
					throw new Exception("item not on parse");
				}
				var filter = new ApplyTagsPage ();
				filter.SetRelation (_model.ConversationId, BlrtBaseHelpers.GetAllKnownTags());
				_controller.NavigationController.PushViewController (
					new Util.KeyboardController(filter.CreateViewController ()){
						Title = "Tags"
					}, true
				);
				_controller.UndoSwiped(true);
			}catch{
				new UIAlertView (
					BlrtUtil.PlatformHelper.Translate("can_not_open_tags_title"),
					BlrtUtil.PlatformHelper.Translate("can_not_open_tags_message"),
					null,
					BlrtUtil.PlatformHelper.Translate("can_not_open_tags_cancel")
				).Show ();
				return;
			}

		}

		#region IExecutor implementation

		ExecutionSource IExecutor.Cause {
			get {
				return ExecutionSource.System; 
			}
		}

		#endregion
	}


	public class MailboxMoreAction : ICellAction, IExecutor
	{
		MailboxCellModel _model;
		MailboxListController _controller;

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}

		#endregion

		public MailboxMoreAction (MailboxCellModel model, MailboxListController controller)
		{
			_controller = controller;
			_model = model;
		}

		string _url_convo_sms, _url_convo_email, _url_convo_other = "";
		public void PrepareUrl(){
			Task.Factory.StartNew (() => {
				GenerateLink ("convo_sms");
				GenerateLink ("convo_email");
				GenerateLink ("convo_other");
			});
		}

		async Task GenerateLink(string urlType){
			var rel = BlrtData.BlrtManager.DataManagers.Default.GetConversationUserRelation (
				_model.ConversationId,
				ParseUser.CurrentUser.ObjectId
			);
			var content = rel.Conversation;
			switch(urlType){
				case "convo_sms":
					if (string.IsNullOrEmpty (_url_convo_sms) || !_url_convo_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_sms = await BlrtUtil.GenerateYozioUrl (content.ObjectId, urlType);
					}
					break;
				case "convo_email":
					if (string.IsNullOrEmpty (_url_convo_email) || !_url_convo_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_email = await BlrtUtil.GenerateYozioUrl (content.ObjectId, urlType);
					}
					break;
				case "convo_other":
					if (string.IsNullOrEmpty (_url_convo_other) || !_url_convo_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_other = await BlrtUtil.GenerateYozioUrl (content.ObjectId, urlType);
					}
					break;
			}
		}

		public void Action (object sender)
		{
			nint copy		= -999;
			nint email		= -999;
			nint message 		= -999;

			nint flag		= -999;
			nint archive	= -999;
			nint upload		= -999;

			nint read		= -999;

			nint options 	= -999;

			var actionSheet = new UIActionSheet ();


			if (!_model.OnParse || BlrtData.BlrtManager.DataManagers.Default.GetUnsyncedConversationItemCount(_model.ConversationId) > 0) {
				upload = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_upload"));
			}


			if (_model.OnParse) {
				copy = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_copy"));

				email = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_email"));

				message = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_message"));

				if (!_model.Flagged) {
					flag = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_flag"));
				} else {
					flag = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_unflag"));
				}

				if (!_model.Archived) {
					archive = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_archive"));
				} else {
					archive = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_unarchive"));
				}

				if (_model.Unread > 0) {
					read = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate("more_options_read"));
				}

				options = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("Options"));
			}

			actionSheet.CancelButtonIndex = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("more_options_cancel"));

			actionSheet.Clicked += async (sender1, e) => {

				var rel = BlrtData.BlrtManager.DataManagers.Default.GetConversationUserRelation (
					_model.ConversationId,
					ParseUser.CurrentUser.ObjectId
				);

				if(e.ButtonIndex == actionSheet.CancelButtonIndex)
					return;

				else if(e.ButtonIndex == copy){
					await GenerateLink("convo_other");
					UIPasteboard.General.String = _url_convo_other;
					new UIAlertView (
						BlrtHelperiOS.Translate ("alert_copy_url_title",	"conversation_screen"),
						BlrtHelperiOS.Translate ("alert_copy_url_message",	"conversation_screen"),
						null,
						BlrtHelperiOS.Translate ("alert_copy_url_accept",	"conversation_screen")
					).Show ();
				}

				else if(e.ButtonIndex == email){
					await GenerateLink("convo_email");
					ShowEmailViewController (rel.Conversation.Name, _url_convo_email);
				}

				else if (e.ButtonIndex == message) {
					await GenerateLink("convo_sms");
					ShowSMSViewController (rel.Conversation.Name, _url_convo_sms);
				}

				else if(e.ButtonIndex == flag){
					rel.Flagged = !_model.Flagged;
					rel.Dirty = true;
					BlrtManager.DataManagers.Default.Save();

					BlrtData.Helpers.BlrtTrack.ConvoFlag(_model.ConversationId, rel.Flagged);
				}
				else if(e.ButtonIndex == archive){
					rel.Archive = !_model.Archived;
					rel.Dirty = true;
					BlrtManager.DataManagers.Default.Save();
					BlrtData.Helpers.BlrtTrack.ConvoArchive(_model.ConversationId, rel.Archive);
				}
				else if(e.ButtonIndex == upload){
					BlrtManager.Upload.UploadAll(_model.ConversationId, LocalStorageParameters.Default);
				}
				else if(e.ButtonIndex == read){
					rel.MarkAllRead();
				}

				else if(e.ButtonIndex == options){
					ShowConvoOption();
				}
				_controller.UndoSwiped(true);
				_controller.Reload(_controller.SelectedConversation);
			};



			if (sender is UIView) {

				var view = sender as UIView;
				var rect = view.Bounds;
				rect.Inflate (10240, 0);


				actionSheet.ShowFrom (
					rect,
					view,
					true
				);
			} else {
				actionSheet.ShowInView (_controller.View);
			}
		}

		async void ShowConvoOption()
		{
			var convo = await _controller.OpenConversation (this, _model.ConversationId);
			convo.OpenConversationOption (this);
		}

		async void ShowEmailViewController (string conversationTitle, string url)
		{
			if (MFMailComposeViewController.CanSendMail) {
				var m = new MFMailComposeViewController ();
				var impression  = BlrtUtil.GetImpressionHtml("convo_email");
				impression += BlrtUtil.GetImpressionHtml("share_email");
				var shareLink = await BlrtUtil.GenerateYozioShareUrl("share_email", true);
				var messageString = string.Format(BlrtUtil.PlatformHelper.Translate("email_convo_url_message_body_html"), url, conversationTitle, impression, shareLink);

				m.SetSubject (string.Format (BlrtHelperiOS.Translate("email_convo_url_subject"), conversationTitle));
				m.SetMessageBody (messageString, true);
				m.Finished += (sender, e) => {
					m.BeginInvokeOnMainThread (() => {
						m.DismissViewController (true, null);
					});
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				_controller.PresentViewController (m, true, null);



				new UIAlertView (
					BlrtHelperiOS.Translate ("alert_before_send_url_title",	"conversation_screen"),
					BlrtHelperiOS.Translate ("alert_before_send_url_message",	"conversation_screen"),
					null,
					BlrtHelperiOS.Translate ("alert_before_send_url_accept",	"conversation_screen")
				).Show ();
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert(
						platforHelper.Translate ("cannot_send_email_error_title"),
						platforHelper.Translate ("cannot_send_email_error_message"),
						platforHelper.Translate ("cannot_send_email_error_cancel")
					)
				);
			}
		}

		void ShowSMSViewController (string conversationTitle, string url)
		{
			if (MFMessageComposeViewController.CanSendText) {

				var m = new MFMessageComposeViewController () {
					Body = string.Format (BlrtHelperiOS.Translate("message_convo_url_sms_body"), conversationTitle, url)
				};
				m.Finished += (object s, MFMessageComposeResultEventArgs e) => {
					m.BeginInvokeOnMainThread (() => {
						m.DismissViewController (true, null);
					});
					if(e.Result == MessageComposeResult.Sent){
						BlrtUtil.TrackImpression("convo_sms");
					}
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				_controller.PresentViewController (m, true, null);
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert(
						platforHelper.Translate ("alert_enable_messages_title", 		"share"),
						platforHelper.Translate ("alert_enable_messages_message", 		"share"),
						platforHelper.Translate ("alert_enable_messages_cancel", 		"share")
					)
				);
			}
		}
	}
}

