using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.Mailbox
{
	class ContainerForBlrtImportController : UIViewController
	{
		public static readonly byte HEADER_HEIGHT = 30;

		UILabel _header;
		UIView _primaryView;
		readonly bool _showCancelButton;

		public event EventHandler OnCancelButtonPressed;

		public ContainerForBlrtImportController (UIView primaryView, string headerTitle, bool showCancelButton)
		{
			_showCancelButton = showCancelButton;
			_primaryView = primaryView;

			_header = new UILabel {
				BackgroundColor = BlrtStyles.iOS.CharcoalAlpha,
				Font = BlrtStyles.iOS.CellTitleFont,
				TextColor = BlrtStyles.iOS.White,
				TextAlignment = UITextAlignment.Center,
				Text = headerTitle
			};
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			Title = "Import file";
			View.AddSubview (_primaryView);
			View.AddSubview (_header);

			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (BlrtData.BlrtUtil.PlatformHelper.Translate ("import_to_blrt_cancel_btn_title", "import_to_blrt"),
			                                                        UIBarButtonItemStyle.Done, (sender, args) => {
				OnCancelButtonPressed?.Invoke (sender, null);
			});
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			var navbarFrame = NavigationController.NavigationBar.Frame;
			_header.Frame = new CGRect (navbarFrame.X, navbarFrame.Bottom, navbarFrame.Width, HEADER_HEIGHT);

			if (_primaryView != null) {
				var headerBottom = _header.Frame.Bottom;

				if (_primaryView is UIScrollView) {
					_primaryView.Frame = View.Bounds;
					(_primaryView as UIScrollView).ContentInset = new UIEdgeInsets (headerBottom, 0, 0, 0);
				} else {
					_primaryView.Frame = new CGRect (View.Bounds.X, 
					                                      headerBottom, 
					                                      View.Bounds.Width, 
					                                      View.Bounds.Bottom - headerBottom);
				}
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			if (_primaryView is UIScrollView) {
				(_primaryView as UIScrollView).SetContentOffset (new CGPoint (0, -_header.Frame.Bottom), false);
			}
		}
	}
}