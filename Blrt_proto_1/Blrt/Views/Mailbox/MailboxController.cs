using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using BlrtData;
using BlrtData.Models.Mailbox;
using BlrtAPI;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxController : SlideDrawerViewController, IMailboxScreen, IExecutor
	{
		Conversation.ConversationController _slave;
		UINavigationController _slaveNavigation;





		MailboxListController _mailbox;
		UINavigationController _mailboxNavigation;
		public bool MailboxListInSeparateContainer { get; set; }

		/// <summary>
		/// Gets or sets the request arguments for external media.
		/// </summary>
		/// <value>The request arguments for external media.</value>
		public object RequestArgsForExternalMedia {
			get {
				return _mailbox.RequestArgsForExternalMedia;
			}
			set {
				_mailbox.RequestArgsForExternalMedia = value;
			}
		}

		public MailboxController (bool mailboxListInSeparateContainer) : base ()
		{
			MailboxListInSeparateContainer = mailboxListInSeparateContainer;
			_mailbox = new MailboxListController (this, mailboxListInSeparateContainer);
			_mailboxNavigation = new Util.BaseUINavigationController (_mailbox);
			_mailbox.NavigationItem.LeftBarButtonItem = new BlrtHelperiOS.OpenDrawerWithUnread (this);
			_mailbox.NavigationItem.RightBarButtonItem = new UIBarButtonItem (NewConversationButton);
			Master = _mailboxNavigation;

			initializeSlave ();

			if (!_slave.ConversationOpen)
				ShowSlider (false);
			else
				HideSlider (false);

			this.DividerColor = BlrtStyles.iOS.Gray4;
			Profile.ProfileNavigationPage.LogoutClicked += (object o, int e) => {

				initializeSlave ();
				View.LayoutIfNeeded ();
			};
		}

		void initializeSlave ()
		{
			_slave = new Conversation.ConversationController ();
			_slaveNavigation = new Util.BaseUINavigationController (_slave);
			Slave = _slaveNavigation;
		}

		UIButton NewConversationButton {
			get {
				var image = UIImage.FromBundle ("inbox_add.png");
				var btn = new UIButton ();
				btn.SetImage (image, UIControlState.Normal);
				btn.SizeToFit ();
				btn.TouchUpInside += async delegate { await TryLetCreateNewConversation (false); };
				return btn;
			}
		}

		public async Task CreateConversationThroughProfile (bool presentConversationSetupViewInSeparateController)
		{
			await TryLetCreateNewConversation (presentConversationSetupViewInSeparateController);
			await _slave.OpenReBlrtCreation (_slave, null);
		}

		public async Task TryLetCreateNewConversation (bool presentConversationSetupViewInSeparateController)
		{

			await OnNewConversationButtonPressed (await ShouldCreateConversation (presentConversationSetupViewInSeparateController));
		}

		async Task<Tuple<bool, string>> ShouldCreateConversation (bool presentConversationSetupViewInSeparateController)
		{
			/*var executor = new Executor (ExecutionSource.System);
			var mailbox = await BlrtData.BlrtManager.App.OpenMailbox (executor, BlrtData.Models.Mailbox.MailboxType.Inbox);
			if (!mailbox.Success)
				return new Tuple<bool, string> (false, string.Empty);

			return await mailbox.Result.CreateNewConversation (presentConversationSetupViewInSeparateController);*/
			return await CreateNewConversation (presentConversationSetupViewInSeparateController);
		}


		public event EventHandler conversationCreated;

		async Task OnNewConversationButtonPressed (Tuple<bool, string> userResponse)
		{
			var shouldCreateConversation = userResponse.Item1;
			var newConversationName = userResponse.Item2;

			if (shouldCreateConversation) {
				var conversation = BlrtUtil.CreateConversation (newConversationName);
				_mailbox.SelectedConversation = conversation.ObjectId;

				Task.Run (async delegate {
					await BlrtUtil.UploadItem (conversation, true);
					InvokeOnMainThread (async delegate {
						await _mailbox.Refresh ();
						BlrtData.Helpers.BlrtTrack.ConvoCreate (conversation.ObjectId, newConversationName);
					});
				}).FireAndForget ();

				await BlrtUtil.OpenConversation (conversation.ObjectId);
			} else if (!string.IsNullOrWhiteSpace (_mailbox.SelectedConversation)) {
				await BlrtUtil.OpenConversation (_mailbox.SelectedConversation);
			}
		}






		#region Layout stuff

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			//			_mailbox.NavigationItem.LeftBarButtonItem = BlrtHelperiOS.OpenDrawerMenuButtonWithUnread (this);

		}

		public override void ViewDidLayoutSubviews ()
		{
			var isPhalet = NMath.Min (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) > 400;
			var isTablet = NMath.Min (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) > 640;

			if (isPhalet
			   && (this.InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft
			   || this.InterfaceOrientation == UIInterfaceOrientation.LandscapeRight)) {

				MasterWidth = 320;
				SlaveWidth = NMath.Max (320, View.Bounds.Width - (MasterWidth + DividerWidth));

			} else {
				MasterWidth = isTablet ? 320 : nfloat.MaxValue;
				SlaveWidth = isTablet ? 320 : nfloat.MaxValue;
			}

			base.ViewDidLayoutSubviews ();

			if (SlaveWidth > View.Bounds.Width) {
				_slave.NavigationItem.SetLeftBarButtonItem (BlrtHelperiOS.OpenDrawerBackButton (_slave, delegate {
					BlrtUtil.DeleteDecryptedFiles ();
					_mailbox.SelectedConversation = string.Empty;
					_slave.SetConversation (null, LocalStorageType.Default);
				}), true);
			} else {
				_slave.NavigationItem.SetLeftBarButtonItem (null, true);
			}
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);

			if (!_slave.ConversationOpen)
				ShowSlider (true);
			else
				HideSlider (true);
		}

		#endregion


		public void SetType (MailboxType type)
		{
			_slaveNavigation.PopToRootViewController (true);
			//_slave.SetConversation (null, LocalStorageType.Default);

			_mailboxNavigation.PopToRootViewController (true);
			_mailbox.SetType (type);
		}



		public void ReloadMailbox ()
		{
			_mailbox.Reload (_slave.ConversationId);
		}

		#region IMailboxScreen implementation

		public IConversationScreen ConversationScreen {
			get {
				return _slave;
			}
		}

		public async Task Refresh (IExecutor e)
		{
			await _mailbox.Refresh ();
		}

		public void Reload (IExecutor e)
		{
			_mailbox.Reload (null);
		}

		public void CloseConversation (IExecutor e)
		{

			InvokeOnMainThread (delegate {
				_slaveNavigation.PopToRootViewController (true);
				_slave.SetConversation (null, LocalStorageType.Default);
				ShowSlider (true);
			});
		}

		async Task<Tuple<bool, string>> CreateNewConversation (bool presentConversationSetupViewInSeparateController)
		{
			_slaveNavigation.PopToRootViewController (true);
			HideSlider (true);
			var userResponse = await _slave.ConfirmNewConvoCreation (presentConversationSetupViewInSeparateController);
			BlrtHelperiOS.OpenSlideDrawer (_slave, true);
			BlrtUtil.DeleteDecryptedFiles ();
			return userResponse;
		}

		public async Task<ExecutionResult<IConversationScreen>> OpenConversation (IExecutor e,
																				  string conversationId,
																				  bool openWithSMSLink = false,
																				  bool IsNewConversation = false)
		{
			var localStorageType = LocalStorageType.Default;

			UIViewController currentViewController = new UIViewController ();
			 currentViewController = this.TopPresentedViewController ();


			if(!(currentViewController is MailboxController) && !MailboxListInSeparateContainer ){

				//currentViewController.NavigationController.PopToRootViewController (true);
				currentViewController.DismissViewController (true, null);

			}


			var c = this.TopPresentedViewController ();


			if (!await GetConversation (conversationId, localStorageType, openWithSMSLink))
				return new ExecutionResult<IConversationScreen> (new Exception ("Conversation not found"));

			try {
				_slaveNavigation.PopToRootViewController (true);
				_slave.SetConversation (conversationId, localStorageType, IsNewConversation).FireAndForget ();
				_mailbox.SelectCell (conversationId);
				HideSlider (true);

				View.SetNeedsLayout ();
			} catch (Exception exp) {
				BlrtUtil.Debug.ReportException (exp, null, Xamarin.Insights.Severity.Error);

				try {
					_slave.SetConversation (null, LocalStorageType.Default).FireAndForget ();
				} catch { }
				var localStorage = LocalStorageParameters.Get (localStorageType);
				//clear local data and try again
				var conversation = localStorage.DataManager.GetConversation (conversationId);
				if (conversation == null)
					return new ExecutionResult<IConversationScreen> (exp);
				localStorage.DataManager.Remove (conversation);

				var relations = localStorage.DataManager.GetConversationUserRelationsFromConversation (conversationId);
				if (relations != null)
					localStorage.DataManager.Remove (relations);

				var items = localStorage.DataManager.GetConversationItems (conversationId);
				if (items != null) {
					var media = new List<MediaAsset> ();
					foreach (var item in items) {
						media.AddRange (localStorage.DataManager.GetMediaAssets (item));
					}
					foreach (var m in media) {
						m.DeleteData ();
					}
					localStorage.DataManager.Remove (media);
					localStorage.DataManager.Remove (items);
				}
				await localStorage.DataManager.SaveLocal ();
				return await OpenConversation (e, conversationId);
			}

			return new ExecutionResult<IConversationScreen> (_slave);
		}

		private async Task<bool> GetConversation (string conversationId, LocalStorageType localStorageType, bool openWithSMSLink = false)
		{
			bool isReadOnly = false;

			if (String.IsNullOrWhiteSpace (conversationId))
				return false;
			var localStorage = LocalStorageParameters.Get (localStorageType);
			var conversation = localStorage.DataManager.GetConversation (conversationId);

			if (conversation != null) {
				return true;
			} else {
				try {

					//can't find the conv from local
					var tcs = new TaskCompletionSource<bool> ();
					Task.Factory.StartNew (async () => {
						var apiCall = new APIUrlInfo (new APIUrlInfo.Parameters () {
							url = BlrtConstants.ServerUrl + "/conv/" + BlrtEncode.EncodeId (conversationId)
						});
						if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
							if (apiCall.Response.Data != null && !string.IsNullOrWhiteSpace (apiCall.Response.Data.ConversationId) && !apiCall.Response.Data.Accessible) {
								tcs.TrySetResult (false);
								return;
							}
							if (!apiCall.Response.Success && apiCall.Response.Error.Code == APIUrlInfoError.invalidUrl) {
								tcs.TrySetResult (true); //conversation deleted, it will go to failed to find convo alert
								return;
							}
							if (!apiCall.Response.Success && apiCall.Response.Error.Code == APIUrlInfoError.readOnly) {
								isReadOnly = true;
								tcs.TrySetResult (true);//conversation is readonly, cannot request access
								return;
							}

						}

						await BlrtData.Query.BlrtConversationQuery.RunQuery (conversationId).WaitAsync ();
						tcs.TrySetResult (true);
						return;
					});

					var worked = await BlrtUtil.PlatformHelper.ShowWaitAlert (
						tcs.Task,
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_title"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_message"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_cancel")
					);


					if (!worked)
						return false;
					if (!tcs.Task.Result) {
						var alertTcs = new TaskCompletionSource<bool> ();
						Device.BeginInvokeOnMainThread (async () => {
							if (openWithSMSLink && string.IsNullOrEmpty (BlrtUserHelper.PhoneNumber)) {
								var alertResult = await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
									BlrtUtil.PlatformHelper.Translate ("add_phone_alert_title", "profile"),
									BlrtUtil.PlatformHelper.Translate ("add_phone_alert_message", "profile"),
									BlrtUtil.PlatformHelper.Translate ("Cancel"),
									BlrtUtil.PlatformHelper.Translate ("OK")
								));
								if (alertResult.Success && !alertResult.Result.Cancelled) {
									var addPhoneResult = await BlrtManager.App.OpenAddPhone (Executor.System (), BlrtData.Views.AddPhone.OpenFrom.Profile);
									if (addPhoneResult.Success && (bool)(addPhoneResult.Result)) {
										var result = await GetConversation (conversationId, localStorageType, false);
										alertTcs.TrySetResult (result);
										return;
									}
								}
							}
							var tt = BlrtManager.DataManagers.Default.GetConversation (conversationId);
							OpenConversationPerformer.RequestAccess (BlrtEncode.EncodeId (conversationId), this);
							alertTcs.TrySetResult (false);
						});
						return await alertTcs.Task;
					}

					conversation = localStorage.DataManager.GetConversation (conversationId);

					if (conversation != null)
						return true;
				} catch {
				}


				//can't find the conv from server
				FailedToFindConversation (isReadOnly);

				return false;
			}
		}

		private async void FailedToFindConversation (bool isReadOnly)
		{
			if (isReadOnly) {
				await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("request_access_title", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("request_access_message_readonly", "screen_titles"),
					BlrtUtil.PlatformHelper.Translate ("request_access_cancel", "conversation_screen")
				));
			} else {
				var alertResult = await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_title"),
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_message"),
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_cancel"),
					BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_support")
				));
				if (alertResult.Success && !alertResult.Result.Cancelled) {
					var addPhoneResult = await BlrtManager.App.OpenAddPhone (Executor.System (), BlrtData.Views.AddPhone.OpenFrom.Profile);
					if (addPhoneResult.Success && (bool)(addPhoneResult.Result)) {
						var help = await BlrtManager.App.OpenSupport (this);
						help.Result.OpenMessageSupport ();
						return;
					}
				}
			}
		}

		#endregion

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}

		#endregion
	}

}

