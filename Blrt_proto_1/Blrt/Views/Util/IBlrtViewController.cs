using System;
using UIKit;

namespace BlrtiOS.Views.Util
{
	///this interface is used to check if the current viewcontroller is too busy to proform and action from outside the app, such as opening a url in blrt	
	public interface IBlrtViewController
	{
		void AppDidResume();
		void AppReceivedPush();

		void PushLoading(string cause, bool animated);
		void PopLoading(string cause, bool animated);
	}
}

