using System;
using System.Linq;
using Foundation;
using UIKit;
using System.ComponentModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.Helpers;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;


namespace BlrtiOS.Views.Util
{
	/// <summary>
	/// This table automatically releazes when something inside of it has changed.  
	/// </summary>
	public class TableModelView<T>  : UITableView, ITableInfo
		where T : class, ITableModelCell
	{
		public new event EventHandler Scrolled;


		bool _quietSelection;

		ObservableCollection<TableModelSection<T>> _list;
		List<TableModelSection<T>> _trackingSection;

		public ObservableCollection<TableModelSection<T>> List {
			get {
				return _list;
			}
			set {
				if (_list != value) {

					if (_list != null)
						_list.CollectionChanged -= SectionListChanged;

					_list = value;
					_executionList.Clear ();
					ResetTracking ();

					if (_list != null)
						_list.CollectionChanged += SectionListChanged;

					this.ReloadData ();
				}
			}
		}

		public bool ListenToChanges { get; set; }

		List<RowExecution> _executionList;



		public TableModelView () : this (CoreGraphics.CGRect.Empty)
		{
			ListenToChanges = true;
		}

		public TableModelView (CoreGraphics.CGRect frame) : base (frame)
		{
			_trackingSection = new List<TableModelSection<T>> ();

			this.Source = new TableModelListSource ();
			this.Delegate = new TableModelListDelegate ();

			this.SectionIndexMinimumDisplayRowCount = 1;

			_executionList = new List<RowExecution> ();
		}

		public bool IsCellVisible (int section, int row)
		{
			var ipfv = IndexPathsForVisibleRows;
			foreach (var path in ipfv) {
				if (path.Section == section && path.Row == row) {
					return true;
				}
			}
			return false;
		}

		void StartSectionTracking (IList list)
		{
			foreach (var c in list)
				if (c is TableModelSection<T>)
					StartSectionTracking (c as TableModelSection<T>);
		}

		void EndSectionTracking (IList list)
		{
			foreach (var c in list)
				if (c is TableModelSection<T>)
					EndSectionTracking (c as TableModelSection<T>);
		}


		void StartSectionTracking (TableModelSection<T> list)
		{
			lock (_trackingSection) {
				if (!_trackingSection.Contains (list)) {
					list.CollectionChanged += CellListChanged;
					_trackingSection.Add (list);

					StartCellTracking (list);
				}
			}
		}

		void EndSectionTracking (TableModelSection<T> list)
		{
			lock (_trackingSection) {
				if (_trackingSection.Contains (list)) {
					list.CollectionChanged -= CellListChanged;
					_trackingSection.Remove (list);

					EndCellTracking (list);
				}
			}
		}


		void CleanSectionTracking ()
		{
			lock (_trackingSection) {
				for (int i = 0; i < _trackingSection.Count; ++i) {
					if (!_list.Contains (_trackingSection [i])) {
						EndSectionTracking (_trackingSection [i]);
						--i;
					}
				}
			}
		}

		void ResetTracking ()
		{
			while (_trackingSection.Count != 0) {
				EndSectionTracking (_trackingSection [0]);
			}

			for (int i = 0; _list != null && i < _list.Count; ++i) {
				StartSectionTracking (_list [i]);
			}
		}

		void StartCellTracking (IList cells)
		{
			foreach (var c in cells)
				if (c is T)
					StartCellTracking (c as T);
		}

		void EndCellTracking (IList cells)
		{
			foreach (var c in cells)
				if (c is T)
					EndCellTracking (c as T);
		}

		void StartCellTracking (TableModelSection<T> cells)
		{
			foreach (var c in cells)
				StartCellTracking (c);
		}

		void EndCellTracking (TableModelSection<T> cells)
		{
			foreach (var c in cells)
				EndCellTracking (c);
		}

		void StartCellTracking (T cell)
		{
			if (cell != null)
				cell.PropertyChanged += CellChanged;
		}

		void EndCellTracking (T cell)
		{
			if (cell != null)
				cell.PropertyChanged -= CellChanged;
		}



		void SectionListChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (sender is ObservableCollection<TableModelSection<T>>) {

				InvokeOnMainThread (() => {

					this.ReloadData ();

					var selected = SelectedCell;

					switch (e.Action) {
						case NotifyCollectionChangedAction.Add:
							StartSectionTracking (e.NewItems);
							break;
						case NotifyCollectionChangedAction.Move:
							break;
						case NotifyCollectionChangedAction.Remove:
							StartSectionTracking (e.OldItems);
							break;
						case NotifyCollectionChangedAction.Replace:
							EndSectionTracking (e.OldItems);
							StartSectionTracking (e.NewItems);
							break;
						case NotifyCollectionChangedAction.Reset:
							ResetTracking ();
							break;
					}

					if (SelectedCell != selected)
						QuietSelect (selected, false);

				});
			}
		}

		void CellListChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (sender is TableModelSection<T>) {

				var section = sender as TableModelSection<T>;

				if (section == null) {
					CleanSectionTracking ();
					return;
				}

				var index = _list.IndexOf (section);


				switch (e.Action) {
					case NotifyCollectionChangedAction.Add:
						for (int i = 0; i < e.NewItems.Count; ++i) {
							InsertExecutionRow (index, i + e.NewStartingIndex);
						}
						StartCellTracking (e.NewItems);
						break;
					case NotifyCollectionChangedAction.Move:
						for (int i = 0; i < e.OldItems.Count; ++i) {
							RemoveExecutionRow (index, i + e.OldStartingIndex);
						}
						for (int i = 0; i < e.NewItems.Count; ++i) {
							InsertExecutionRow (index, i + e.NewStartingIndex);
						}
						break;
					case NotifyCollectionChangedAction.Remove:
						for (int i = 0; i < e.OldItems.Count; ++i) {
							RemoveExecutionRow (index, i + e.OldStartingIndex);
						}
						EndCellTracking (e.OldItems);
						break;
					case NotifyCollectionChangedAction.Replace:
						for (int i = 0; i < e.NewItems.Count; ++i) {
							ReloadExecutionRow (index, i + e.NewStartingIndex);						
						}
						StartCellTracking (e.NewItems);
						EndCellTracking (e.OldItems);

						break;
					case NotifyCollectionChangedAction.Reset:
						ReloadData ();
						StartSectionTracking (section);
						EndSectionTracking (section);
						break;
				}

				ExecutionChanges ();
			}
		}

		void CellChanged (object sender, PropertyChangedEventArgs args)
		{
			if (!ListenToChanges)
				return;

			if (sender is T) {
				var model = sender as T;
				var index = GetIndexPath (model);

				if (index == null) {
					EndCellTracking (model);
					return;
				}

				ReloadExecutionRow (index.Section, index.Row);
				ExecutionChanges ();
			}
		}

		public override void ReloadData ()
		{
			base.ReloadData ();
		
			lock (_executionList)
				_executionList.Clear ();
		}

		private void InsertExecutionRow (int section, int row)
		{
			if (!ListenToChanges)
				return;


			lock (_executionList) {
				var count = _executionList.Count (cell => cell.Type == RowExecutionType.Insert && cell.Section == section && cell.Row < row);
				_executionList.Add (new RowExecution (RowExecutionType.Insert, section, row - count));
			}
		}

		private void RemoveExecutionRow (int section, int row)
		{
			if (!ListenToChanges)
				return;

			lock (_executionList) {
				var count = _executionList.Count (cell => cell.Type == RowExecutionType.Insert && cell.Section == section && cell.Row < row);
				_executionList.Add (new RowExecution (RowExecutionType.Remove, section, row - count));
			}
		}

		private void ReloadExecutionRow (int section, int row)
		{
			if (!ListenToChanges)
				return;

			lock (_executionList) {
				var any = _executionList.Any (cell => cell.Type == RowExecutionType.Reload && cell.Section == section && cell.Row == row);
				if (!any)
					_executionList.Add (new RowExecution (RowExecutionType.Reload, section, row));
			}
		}


		public void ExecutionChanges ()
		{
			if (!NSThread.IsMain) {
				InvokeOnMainThread (ExecutionChanges);
				return;
			}

			if (Window == null) {
				SetNeedsLayout ();
				this.ReloadData ();
				return;
			}

			lock (_executionList) {
				if (_executionList.Count == 0)
					return;

				var insert = from cell in _executionList
				             where cell.Type == RowExecutionType.Insert
				             select NSIndexPath.FromRowSection (cell.Row, cell.Section);
				var remove = from cell in _executionList
				             where cell.Type == RowExecutionType.Remove
				             select NSIndexPath.FromRowSection (cell.Row, cell.Section);
				var reload = from cell in _executionList
				             where cell.Type == RowExecutionType.Reload
				             select NSIndexPath.FromRowSection (cell.Row, cell.Section);


				BeginUpdates ();

				if (insert.Any ())
					InsertRows (insert.ToArray (), UITableViewRowAnimation.Automatic);

				if (remove.Any ())
					DeleteRows (remove.ToArray (), UITableViewRowAnimation.Automatic);

				if (reload.Any ())
					ReloadRows (reload.ToArray (), UITableViewRowAnimation.Automatic);

				EndUpdates ();

				_executionList.Clear ();
			}
		}



		public virtual nfloat GetHeightForRow (T cell)
		{
			return RowHeight;
		}

		public virtual nfloat GetHeightForHeader (TableModelSection<T> header)
		{
			return SectionHeaderHeight;
		}

		public virtual nfloat GetHeightForFooter (TableModelSection<T> footer)
		{
			return SectionFooterHeight;
		}

		public T SelectedCell {
			get {
				if (IndexPathForSelectedRow != null)
					return GetCell (IndexPathForSelectedRow);
				return null;
			}
		}

		public void SelectCell (T cell, bool animated)
		{
			var index = GetIndexPath (cell);

			if (index != null)
				SelectRow (index, animated, UITableViewScrollPosition.None);
		}

		public void DeselectCell (T cell, bool animated)
		{
			var index = GetIndexPath (cell);

			if (index != null)
				DeselectRow (index, animated);
		}

		public void QuietSelect (T cell, bool animated)
		{
			try {
				_quietSelection = true;
				SelectCell (cell, animated);

			} finally {
				_quietSelection = false;
			}
		}

		public virtual void CellSelected (T cell)
		{
			if (cell.Action != null)
				cell.Action.Action (CellAt (cell));
		}

		protected NSIndexPath GetIndexPath (T cell)
		{
			if (_list == null)
				return null;


			for (int i = 0; i < _list.Count; ++i) {

				int j = _list [i].IndexOf (cell);

				if (j >= 0)
					return NSIndexPath.FromRowSection (j, i);
			}
			return null;
		}

		protected T GetCell (NSIndexPath indexPath)
		{
			if (_list.Count > indexPath.Section && _list [indexPath.Section].Count > indexPath.Row)
				return _list [indexPath.Section] [indexPath.Row];

			return null;
		}


		public virtual void ScrollTo (T cell, UITableViewScrollPosition position, bool animated)
		{
			this.ScrollToRow (GetIndexPath (cell), position, animated);
		}

		public virtual UITableViewCell CellAt (T cell)
		{
			return this.CellAt (GetIndexPath (cell));
		}

		public virtual T GetCellForRow (UITableViewCell cell)
		{

			var ns = this.IndexPathForCell (cell);
			return GetCell (ns);
		}

		protected virtual void DidScroll ()
		{
			if (Scrolled != null)
				Scrolled (this, EventArgs.Empty);
		}


		protected class TableModelListSource : UITableViewSource
		{
			public TableModelListSource () : base ()
			{
			}

			public override nint NumberOfSections (UITableView tableView)
			{

				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return 0;

				return t._list.Count;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				var t = tableview as TableModelView<T>;
				if (t == null || t._list == null)
					return 0;

				return t._list [(int)section].Count;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return null;
				var cell = t._list [indexPath.Section] [indexPath.Row];
				var view = tableView.DequeueReusableCell (new NSString (cell.CellIdentifer), indexPath);
				if (view is ITableInfoRequester) {
					(view as ITableInfoRequester).SetInfoSource (t, indexPath.Section, indexPath.Row);
				}
				if (view is IBindable<T>) {
					(view as IBindable<T>).Bind (cell);
				}
				return view;
			}
		}

		protected class TableModelListDelegate : UITableViewDelegate
		{
			public TableModelListDelegate () : base ()
			{

			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return;

				if (!t._quietSelection) {

					var cell = t.GetCell (indexPath);
					t.CellSelected (cell);

				}
			}

			public override UIView GetViewForHeader (UITableView tableView, nint section)
			{

				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null || GetHeightForHeader (tableView, section) == 0)
					return null;

				var cell = t._list [(int)section];
				if (string.IsNullOrEmpty (cell.HeaderIdentifer))
					return null;

				var view = tableView.DequeueReusableHeaderFooterView (new NSString (cell.HeaderIdentifer));

				if (view is IBindable<TableModelSection<T>>)
					(view as IBindable<TableModelSection<T>>).Bind (cell);

				return view;
			}

			public override UIView GetViewForFooter (UITableView tableView, nint section)
			{

				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null || GetHeightForFooter (tableView, section) == 0)
					return null;

				var cell = t._list [(int)section];
				if (string.IsNullOrEmpty (cell.FooterIdentifer))
					return null;

				var view = tableView.DequeueReusableHeaderFooterView (new NSString (cell.FooterIdentifer));

				if (view is IBindable<TableModelSection<T>>)
					(view as IBindable<TableModelSection<T>>).Bind (cell);

				return view;
			}

			public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{

				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return 0;

				var cell = t.GetCell (indexPath);
				return t.GetHeightForRow (cell);
			}

			public override nfloat GetHeightForHeader (UITableView tableView, nint section)
			{

				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return 0;

				var cell = t._list [(int)section];
				return t.GetHeightForHeader (cell);
			}

			public override nfloat GetHeightForFooter (UITableView tableView, nint section)
			{
				var t = tableView as TableModelView<T>;
				if (t == null || t._list == null)
					return 0;

				var cell = t._list [(int)section];
				return t.GetHeightForFooter (cell);
			}




			public override void Scrolled (UIScrollView scrollView)
			{
				if (scrollView is TableModelView<T>) {
					(scrollView as TableModelView<T>).DidScroll ();
				}
			}
		}



		private struct RowExecution
		{
			public int Row, Section;
			public RowExecutionType Type;

			public RowExecution (RowExecutionType type, int section, int row)
			{
				this.Type = type;
				this.Section	= section;
				this.Row = row;
			}
		}

		private enum RowExecutionType
		{
			Reload,
			Insert,
			Remove
		}
	}

	public interface ITableInfo {
		bool IsCellVisible (int section, int row);
	}

	public interface ITableInfoRequester {
		void SetInfoSource (ITableInfo src, int section, int row);
	}
}

