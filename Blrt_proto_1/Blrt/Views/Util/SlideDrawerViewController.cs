using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace BlrtiOS.Views.Util
{
	public class SlideDrawerViewController : UIViewController
	{
		UIViewController _master, _slave;
		UIView _divider;

		public UIViewController Master {
			get { return _master; }
			set {
				if (_master != value)
					SetMaster (value);
			}
		}

		public UIViewController Slave {
			get { return _slave; }
			set {
				if (_slave != value)
					SetSlave (value);
			}
		}

		protected nfloat SliderPosition { get { return _slave != null ? _slave.View.Frame.X - DividerWidth - CappedMasterWidth : 0; } }

		public bool SliderOpen { get; private set; }

		public bool SlaveOpen { get; private set; }



		nfloat _masterScrollSpeed = 0.25f,
			_masterWidth = 240,
			_slaveWidth = nfloat.MaxValue,
			_dividerWidth = 1;

		CGSize ScreenBounds {
			get {
				var s = UIScreen.MainScreen.Bounds.Size;

				if (!BlrtHelperiOS.IsIOS8Above) {

					if (InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft
					    || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight)
						return  new CGSize (s.Height, s.Width);

				}
				return s;
			
			}

		}

		public nfloat MasterScrollSpeed {
			get{ return _masterScrollSpeed; }
			set {
				if (value != _masterScrollSpeed) {
					_masterScrollSpeed = value;
					View.SetNeedsLayout ();
				}
			}
		}

		public nfloat MasterWidth {
			get{ return _masterWidth; }
			set {
				if (value != _masterWidth) {
					_masterWidth = value;
					View.SetNeedsLayout ();
				}
			}
		}

		public nfloat SlaveWidth {
			get{ return _slaveWidth; }
			set {
				if (value != _slaveWidth) {
					_slaveWidth = value;
					View.SetNeedsLayout ();
				}
			}
		}

		public nfloat DividerWidth {
			get{ return _dividerWidth; }
			set {
				if (value != _dividerWidth) {
					_dividerWidth = value;
					View.SetNeedsLayout ();
				}
			}
		}

		/// <summary>
		/// Used for the pan guesture to rememeber where the pan started.
		/// </summary>
		private nfloat _startingPoint;

		SlideDrawPanGestureRecognizer _pan;

		protected nfloat CappedMasterWidth { get { return NMath.Min (ScreenBounds.Width, MasterWidth); } }

		protected nfloat CappedSlaveWidth { get { return NMath.Min (ScreenBounds.Width, SlaveWidth); } }


		int _stackChangeCounter;
		bool _sliding;
		bool _animating;

		UIView _blockerView;

		public UIColor DividerColor { get { return _divider.BackgroundColor; } set { _divider.BackgroundColor = View.BackgroundColor = value; } }

		public SlideDrawerViewController ()
			: base ()
		{
			View.ClipsToBounds = true;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.AddSubview (_divider = new UIView () {
				Layer = {
					ZPosition = 99
				},
				BackgroundColor = UIColor.Black
			});
			View.AddSubview (_blockerView = new UIView () {
				Layer = {
					ZPosition = 99
				},
				UserInteractionEnabled = true
			});


			View.AddGestureRecognizer (_pan = new SlideDrawPanGestureRecognizer (this));
			View.AddGestureRecognizer (new SlideDrawTapGestureRecognizer (this));

			DividerColor = UIColor.Black;
		}

		void SetMaster (UIViewController master)
		{

			if (master == null)
				throw new ArgumentNullException ();


			if (_master != null && _master.ParentViewController == this) {
				_master.RemoveFromParentViewController ();
				_master.View.RemoveFromSuperview ();
			}

			_master = master;

			View.BringSubviewToFront (_divider);
			Reposition (false);
		}

		void SetSlave (UIViewController slave)
		{
			if (slave == null)
				throw new ArgumentNullException ();


			if (_slave != null && _slave.ParentViewController == this) {
				_slave.RemoveFromParentViewController ();
				_slave.View.RemoveFromSuperview ();
			}

			_slave = slave;

			View.BringSubviewToFront (_divider);
			Reposition (false);
		}

		void EnsureVisable (bool onlyAdd)
		{


			if (Master != null) {
				if (Master.ParentViewController != this) {
					if (!(Slave != null && Slave.View.Frame == View.Bounds)) {
						AddChildViewController (Master);
						View.InsertSubview (Master.View, 0);
						Master.View.Hidden = false;
					}
				} else if (!onlyAdd) {
					if (Slave != null && Slave.View.Frame == View.Bounds) {
						Master.RemoveFromParentViewController ();
						Master.View.RemoveFromSuperview ();
					}
				}
			}

			if (Slave != null) {
				if (Slave.ParentViewController != this) {
					if (View.Bounds.IntersectsWith (Slave.View.Frame)) {
						AddChildViewController (Slave);
						View.AddSubview (Slave.View);


						View.BringSubviewToFront (_blockerView);
						View.BringSubviewToFront (_divider);
					}
				} else if (!onlyAdd) {
					if (!View.Bounds.IntersectsWith (Slave.View.Frame)) {
						Slave.RemoveFromParentViewController ();
						Slave.View.RemoveFromSuperview ();
					}
				}
			}

			if (!onlyAdd && Slave != null && Master != null) {

				if (View.Bounds.Width < Master.View.Bounds.Width + Slave.View.Bounds.Width + DividerWidth
				    && (View.Bounds.Width > Master.View.Bounds.Width || View.Bounds.Width > Slave.View.Bounds.Width)) {

					_blockerView.Hidden = false;

					if (SliderOpen) {
						_blockerView.Frame = new CGRect (Master.View.Bounds.Width, 0, View.Bounds.Width - Master.View.Bounds.Width, View.Bounds.Height);
					} else {
						_blockerView.Frame = new CGRect (0, 0, Slave.View.Frame.X, View.Bounds.Height);
					}

				}

			} else {
				_blockerView.Hidden = true;
			}
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);

			HideSlider (true);
		}


		void Reposition (bool animated)
		{

			if (SliderOpen)
				ShowSlider (animated);
			else
				HideSlider (animated);
		}

		public override void ViewDidLayoutSubviews ()
		{
			//need to prevent this from being called when panning.  This will occur when adding a view
			if (!(_sliding || _animating)) {
				Reposition (true);
			}

			base.ViewDidLayoutSubviews ();
		}

		void SetSildePosition (nfloat xSlide)
		{
			var rect = View.Bounds;
			var screen = ScreenBounds;


			var mw = CappedMasterWidth;
			var sw = CappedSlaveWidth;

			xSlide = NMath.Max (xSlide, rect.Width - (DividerWidth + mw + sw));

			if (xSlide > 0) {

				//creates a nice max over bounce distance.
				if (_sliding)
					xSlide = BlrtData.BlrtUtil.SmoothClamp (xSlide, 100);
				else
					xSlide = 0;
			}

			if (Master != null && Slave != null) {

				if (screen.Width > mw + sw + DividerWidth) {
					Master.View.Frame = new CGRect (0, 0, mw, rect.Height);
					Slave.View.Frame = new CGRect (mw + DividerWidth, 0, rect.Width - (mw + DividerWidth), rect.Height);
				} else {
					Master.View.Frame = new CGRect ((int)(xSlide * MasterScrollSpeed), 0, mw, rect.Height);
					Slave.View.Frame = new CGRect ((int)(xSlide) + DividerWidth + mw, 0, sw, rect.Height);
				}

				_divider.Frame = new CGRect (Slave.View.Frame.Left - DividerWidth, 0, DividerWidth, rect.Height);
				EnsureVisable (true);
			}
			SliderOpen = SliderPosition >= 0;
			SlaveOpen = Slave != null && Slave.View.Frame.Right == View.Bounds.Width;

			_pan.CancelsTouchesInView = SliderOpen;
		}

		public void ToggleSlider (bool animate)
		{
			if (SliderOpen) {
				HideSlider (animate);
			} else {
				ShowSlider (animate);			
			}
		}


		const float AnimationLength = 0.4f;

		public virtual void ShowSlider (bool animate)
		{

			if (_animating && SliderOpen)
				return;

			++_stackChangeCounter;

			if (animate) {
				int stack = _stackChangeCounter;
				_animating = true;

				UIView.Animate (AnimationLength, () => {
					SetSildePosition (0);	
				}, () => {
					_animating = false;
					if (stack == _stackChangeCounter)
						EnsureVisable (false);		
				});
			} else {
				SetSildePosition (0);
				EnsureVisable (false);				
			}
		}

		public virtual void HideSlider (bool animate)
		{

			if (_animating && !SliderOpen)
				return;

			_stackChangeCounter++;

			if (animate) {
				int stack = _stackChangeCounter;
				_animating = true;

				UIView.Animate (
					AnimationLength, 
					() => {
						SetSildePosition (-(CappedMasterWidth + DividerWidth));
					}, 
					() => {
						_animating = false;
						if (stack == _stackChangeCounter)
							EnsureVisable (false);			
					}
				);
			} else {
				SetSildePosition (-(CappedMasterWidth + DividerWidth));	
				EnsureVisable (false);			
			}
		}


		void PanResponder (UIPanGestureRecognizer guesture)
		{

			switch (guesture.State) {
				case UIGestureRecognizerState.Began:
					_sliding = true;
					_startingPoint = SliderPosition;
					break;

				case UIGestureRecognizerState.Changed:
					_stackChangeCounter++;
					SetSildePosition (guesture.TranslationInView (View).X + _startingPoint);
					break;
				case UIGestureRecognizerState.Ended:
					_sliding = false;
					if (SliderPosition + guesture.VelocityInView (View).X > -24) {
						ShowSlider (true);
					} else {
						HideSlider (true);
					}
					break;
			}
		}

		void TapResponder (UITapGestureRecognizer guesture)
		{
			ToggleSlider (true);
		}

		private class SlideDrawPanGestureRecognizer : UIPanGestureRecognizer
		{

			public SlideDrawPanGestureRecognizer (SlideDrawerViewController sliderController) : base (sliderController.PanResponder)
			{
				CancelsTouchesInView = true;

				this.ShouldBegin += (UIGestureRecognizer recognizer) => {
					var point = recognizer.LocationInView (sliderController.View);

					if (!sliderController.SliderOpen) {
						return point.X < sliderController.SliderPosition + sliderController.CappedMasterWidth + 64;
					} else {
						var vel = this.TranslationInView (sliderController.View);
						return vel.X < 0 && point.X > sliderController.SliderPosition + sliderController.CappedMasterWidth;
					} 
				};


				this.ShouldRecognizeSimultaneously += (gestureRecognizer, otherGestureRecognizer) => {

					if (gestureRecognizer is SlideDrawPanGestureRecognizer && otherGestureRecognizer is SlideDrawPanGestureRecognizer) {

						var pan = gestureRecognizer as SlideDrawPanGestureRecognizer;

						var vel = pan.TranslationInView (sliderController.View);

						return sliderController.SliderOpen && vel.X < 0;
					}
					return false;
				};
			}
		}

		private class SlideDrawTapGestureRecognizer : UITapGestureRecognizer
		{

			public SlideDrawTapGestureRecognizer (SlideDrawerViewController sliderController) : base (sliderController.TapResponder)
			{
				CancelsTouchesInView = false;
				this.ShouldBegin += (UIGestureRecognizer recognizer) => {
					var point = recognizer.LocationInView (sliderController.View);
					return sliderController.Slave != null && (!sliderController.SliderOpen ^ sliderController.Slave.View.Frame.Contains (point));
				};

				this.ShouldRecognizeSimultaneously += (gestureRecognizer, otherGestureRecognizer) => {				
//					return (gestureRecognizer is SlideDrawTapGestureRecognizer && otherGestureRecognizer is SlideDrawTapGestureRecognizer);		
					return true;
				};
			}
		}
	}
}

