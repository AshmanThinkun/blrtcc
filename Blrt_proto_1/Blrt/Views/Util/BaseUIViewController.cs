using System;
using UIKit;
using MonoTouch.Dialog;

using BlrtData;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Util
{
	public abstract class BaseUIViewController : UIViewController, IBlrtViewController
	{
		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			if(BlrtHelperiOS.IsPhabletOrBigger)
				return base.GetSupportedInterfaceOrientations ();
		
			return UIInterfaceOrientationMask.Portrait;
		}


		public abstract string ViewName { get; }

		public BaseUIViewController () : base()
		{
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageView ();
		}

		public virtual void TrackPageView()
		{
			if(!string.IsNullOrWhiteSpace(ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}


		public virtual void AppDidResume () { }
		public virtual void AppReceivedPush () { }


		public virtual void PushLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PushLoading (cause, animated);
		}
		public virtual void PopLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PopLoading (cause, animated);
		}
	}


	public abstract class BaseUITableViewController : UITableViewController, IBlrtViewController
	{
		public abstract string ViewName { get; }

		public BaseUITableViewController () : base(){}
		public BaseUITableViewController (UITableViewStyle style) : base(style){}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageview ();
		}

		public virtual void TrackPageview()
		{
			if(!string.IsNullOrWhiteSpace(ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}

		public virtual void AppDidResume () { }
		public virtual void AppReceivedPush () { }

		public virtual void PushLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PushLoading (cause, animated);
		}
		public virtual void PopLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PopLoading (cause, animated);
		}
	}


	public abstract class BaseDialogViewController : DialogViewController, IBlrtViewController
	{
		public abstract string ViewName { get; }

		public BaseDialogViewController ( RootElement root ) : base( root ) { }

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageview ();
		}

		public virtual void TrackPageview()
		{
			if(!string.IsNullOrWhiteSpace(ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}

		public virtual void AppDidResume () { }
		public virtual void AppReceivedPush () { }


		public virtual void PushLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PushLoading (cause, animated);
		}
		public virtual void PopLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PopLoading (cause, animated);
		}
	}


	public class BaseUINavigationController : UINavigationController, IBlrtViewController
	{

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			if(BlrtHelperiOS.IsPhabletOrBigger)
				return base.GetSupportedInterfaceOrientations ();

			return UIInterfaceOrientationMask.Portrait;
		}

		public virtual string ViewName { get { return null; } }

		public BaseUINavigationController ( UIViewController root ) : base( root ) { }
		public BaseUINavigationController ( ) : base( ) { }
		public UIColor OriginalBarColor{ get; private set;}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			OriginalBarColor = NavigationBar.BarTintColor;
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			NavigationBar.Opaque = false;
			NavigationBar.Translucent = false;
		}
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageview ();
		}

		public virtual void TrackPageview()
		{
			if(!string.IsNullOrWhiteSpace(ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}

		public virtual void AppDidResume () {
			if (VisibleViewController is IBlrtViewController) {
				(VisibleViewController as IBlrtViewController).AppDidResume();
			}
		}
		public virtual void AppReceivedPush () {
			if (VisibleViewController is IBlrtViewController) {
				(VisibleViewController as IBlrtViewController).AppDidResume();
			}
		}

		public virtual void PushLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PushLoading (cause, animated);
		}
		public virtual void PopLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PopLoading (cause, animated);
		}
	}


	public abstract class BaseSlideDrawerViewController : SlideDrawerViewController, IBlrtViewController
	{

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			if(BlrtHelperiOS.IsPhabletOrBigger)
				return base.GetSupportedInterfaceOrientations ();

			return UIInterfaceOrientationMask.Portrait;
		}

		public virtual string ViewName { get { return null; } }

		public BaseSlideDrawerViewController ( ) : base( ) { }

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageview ();
		}

		public virtual void TrackPageview()
		{
			if(!string.IsNullOrWhiteSpace(ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}

		public virtual void AppDidResume () {


			if (Master is IBlrtViewController) {
				(Master as IBlrtViewController).AppDidResume();
			}
			if (Slave is IBlrtViewController) {
				(Slave as IBlrtViewController).AppDidResume();
			}
		}
		public virtual void AppReceivedPush () {

			if (Master is IBlrtViewController) {
				(Master as IBlrtViewController).AppReceivedPush();
			}
			if (Slave is IBlrtViewController) {
				(Slave as IBlrtViewController).AppReceivedPush();
			}
		}

		public virtual void PushLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PushLoading (cause, animated);
		}
		public virtual void PopLoading (string cause, bool animated) {
			if(ParentViewController is IBlrtViewController)
				(ParentViewController as IBlrtViewController).PopLoading (cause, animated);
		}
	}





}

