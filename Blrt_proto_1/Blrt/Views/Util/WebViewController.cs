using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Diagnostics;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using System.Text;


namespace BlrtiOS.Views.Util
{
	public class WebViewController : UIViewController, IExecutionBlocker
	{
		public string Url { get; set; }
		public EventHandler OnPageLoadFailed { get; set; }
		public EventHandler OnDismiss { get; set; }
		public bool ScalesPageToFit{ get; set;}
//		public string UserAgent{ 
//			get{ 
//				return NSUserDefaults.StandardUserDefaults.StringForKey ("UserAgent");
//			} set{
//				var dic = NSDictionary.FromObjectAndKey (new NSString (value), new NSString ("UserAgent"));
////				var dic = NSDictionary.FromObjectAndKey (new NSString ("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0"), new NSString ("UserAgent"));
//				NSUserDefaults.StandardUserDefaults.RegisterDefaults (dic);
//			}
//		}


		private UIWebView _webView;
		private UI.BlrtLoaderCoverView _loadingOverlay;
		private UIBarButtonItem backBtn;
		private UIBarButtonItem clostBtn;

		public WebViewController() : base()
		{
			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			// TODO change this so it is supplied from outside the class
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_loadingOverlay = new UI.BlrtLoaderCoverView (View.Bounds);
			_loadingOverlay.Layer.ZPosition = 100;
			View.Add (_loadingOverlay);

			clostBtn = new UIBarButtonItem (UIBarButtonSystemItem.Done);
			clostBtn.Clicked += (object sender, EventArgs e) => {
				if(OnDismiss != null)
					OnDismiss(this, EventArgs.Empty);
			};

			backBtn = new UIBarButtonItem ("Back", UIBarButtonItemStyle.Plain, null);
			backBtn.Enabled = false;
			backBtn.Clicked += (object sender, EventArgs e) => {
				_webView.GoBack();
			};

			NavigationController.NavigationBar.Translucent = true;
			NavigationItem.SetRightBarButtonItem (clostBtn, false);
			NavigationItem.SetLeftBarButtonItem (backBtn, false);


			EdgesForExtendedLayout = UIRectEdge.None;
		}

		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				// enter navigation stack
				ExecutionPerformer.RegisterBlocker (this);
			}

			base.ViewWillAppear (animated);

			if (Url != null) {

				if (_webView != null) {
					_webView.RemoveFromSuperview ();
					_webView.Dispose ();
					_webView = null;
				}

				_webView = new UIWebView (View.Bounds) {
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight,
					ScalesPageToFit = ScalesPageToFit
				};
				_webView.LoadError += WebViewError;
				_webView.LoadFinished += WebViewLoaded;

				View.Add (_webView);

				_webView.LoadRequest (new NSUrlRequest (new NSUrl (Url)));

				_loadingOverlay.Show (false);
			}
		}
			

		private void WebViewLoaded(object sender, EventArgs e) {

			if (_webView == null || _webView != sender)
				return;

			var Title = _webView.EvaluateJavascript("document.title");
			NavigationItem.Title= Title;
			//hide overlay mask
			if(_loadingOverlay!=null)
				_loadingOverlay.Hide(true);

			//goback button
			if(_webView.CanGoBack)
				backBtn.Enabled = true;
			else{
				backBtn.Enabled = false;
			}
		}


		private void WebViewError(object sender, UIWebErrorArgs e) {

			if (_webView == null || _webView != sender)
				return;

			if (e.Error.Code != -999 && OnPageLoadFailed != null) 
				OnPageLoadFailed (this, EventArgs.Empty);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);


			//we should stop the web view imediately, otherwise yozio might bring us to app store.
			NSUrlCache.SharedCache.DiskCapacity = 0;

			NSUrlCache.SharedCache.MemoryCapacity = 0;

			_webView.LoadHtmlString("",null);

			_webView.EvaluateJavascript("var body=document.getElementsByTagName('body')[0];body.style.backgroundColor=(body.style.backgroundColor=='')?'white':'';");

			_webView.EvaluateJavascript("document.open();document.close()");

			_webView.StopLoading();

			_webView.Delegate = null;

			_webView.RemoveFromSuperview();

			_webView.Dispose();

			_webView = null;
		}

		#region IExecutionBlocker implementation
		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;
		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			if(OnDismiss != null) {
				OnDismiss(this, EventArgs.Empty);
			}
			await _tcsLeaveThisScreen.Task;
			return true;
		}
		#endregion

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}
	}
}

