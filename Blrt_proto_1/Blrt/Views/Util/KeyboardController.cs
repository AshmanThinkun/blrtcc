using System;
using UIKit;
using MonoTouch.Dialog;
using Foundation;
using CoreGraphics;
using System.Collections.Generic;

namespace BlrtiOS.Views.Util
{
	public class KeyboardController : BaseUIViewController, IBlrtViewController
	{
		#region implemented abstract members of BaseUIViewController

		public override string ViewName {
			get { return null; }
		}

		#endregion

		NSObject _hideKeyboardObj;
		NSObject _showWillKeyboardObj;
		NSObject _showDidKeyboardObj;

		CGRect _keyboardOverflow;

		UITapGestureRecognizer _tap;

		public bool TapDismissKeyobard { get; set; }
		public UIView KeyboardView { get; private set;}
		public bool KeyboardDisplaying { get { return _keyboardOverflow.Width > 0 || _keyboardOverflow.Height > 0;} }


		private List<Type> _disabledKeyboardTapGestureList;





		public KeyboardController (UIViewController controller) : this()
		{
			AddChildViewController (controller);
			View.SetNeedsLayout ();
			KeyboardView.AddSubview (controller.View);

		}

		public KeyboardController () : base()
		{
			TapDismissKeyobard = true;
			_disabledKeyboardTapGestureList = new List<Type>();
			_disabledKeyboardTapGestureList.Add (typeof(UITextView));
		}

		public void AddDisabledKeyboardTap(Type type)
		{
			_disabledKeyboardTapGestureList.Add (type);
		}

		public void RemoveDisabledKeyboardTap(Type type)
		{
			_disabledKeyboardTapGestureList.Remove (type);
		}

		public override void LoadView ()
		{
			base.LoadView ();

			KeyboardView = new UIView (View.Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			View.AddSubview (KeyboardView);

			_tap = new UITapGestureRecognizer (TapAction);
			_tap.ShouldRecognizeSimultaneously += (gestureRecognizer, otherGestureRecognizer) =>{
				return false;
			};
			_tap.ShouldBegin += (UIGestureRecognizer recognizer) => KeyboardDisplaying;
			_tap.CancelsTouchesInView = true;
			View.AddGestureRecognizer (_tap);


			_tap.ShouldReceiveTouch += (recognizer, touch) => {
				if( null != _disabledKeyboardTapGestureList ) {

					var point = touch.LocationInView(View);
					var view = View.HitTest(point, new UIEvent());

					while(view != this.View && view != null){
						if( _disabledKeyboardTapGestureList.Contains(view.GetType()) )
							return false;

						view = view.Superview;
					}
				}
				return true;
			};

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		
			RemoveKeyboardObservers ();

			_hideKeyboardObj 		= NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, HandleKeyboardWillHide);
			_showWillKeyboardObj 	= NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, HandleKeyboardWillShow);  
			_showDidKeyboardObj 	= NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, HandleKeyboardWillShow);  
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
            RemoveKeyboardObservers ();
		}

		void TapAction(UITapGestureRecognizer tap){
			if (TapDismissKeyobard)
				View.EndEditing (false);
		
		}
			

		void RemoveKeyboardObservers(){

			if (_hideKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_hideKeyboardObj);
				_hideKeyboardObj = null;
			}
			if (_showWillKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_showWillKeyboardObj);
				_showWillKeyboardObj = null;
			}
			if (_showDidKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_showDidKeyboardObj);
				_showDidKeyboardObj = null;
			}
		}

		private void HandleKeyboardWillShow(NSNotification notification)
		{
			if (View.Window == null) {
				return;
			}

			CGRect keyboardBounds = UIKeyboard.FrameEndFromNotification(notification);
			var rect = View.ConvertRectFromView (keyboardBounds,  View.Window);
			_keyboardOverflow = View.ConvertRectToView (rect,  View);

			RecalulateView ();
		}

		private void HandleKeyboardWillHide(NSNotification notification)
		{
			if (View.Window == null) {
				return;
			}
			_keyboardOverflow = CGRect.Empty;

			RecalulateView ();
		}

		void RecalulateView ()
		{
			var vargule = CGRect.Intersect (View.Bounds, _keyboardOverflow);
			if(KeyboardView!=null)
				KeyboardView.Frame = new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height -  vargule.Height);

			View.LayoutIfNeeded ();
		}
	}
}