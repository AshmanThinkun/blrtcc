﻿using System;
using UIKit;
using BlrtiOS.Views.Settings;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using Xamarin.Forms;
using BlrtData.Views.Contacts;
using BlrtData.Contacts;

namespace BlrtiOS.Views.Contacts
{
	public class ContactsNavController : GenericNavigationController, IExecutionBlocker
	{

		#region IExecutionBlocker implementation

		async Task<bool> IExecutionBlocker.Resolve (ExecutionPerformer e)
		{
			var tcs = new TaskCompletionSource<bool> ();
			if (this.PresentingViewController != null) {
				this.DismissViewController (true, () => {
					tcs.TrySetResult (true);
					ExecutionPerformer.UnregisterBlocker (this);
				});
			}
			return await tcs.Task;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ExecutionPerformer.RegisterBlocker (this);
			this.NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			this.OnDismiss += (sender, e) => {
				if (this.PresentingViewController != null) {
					this.DismissViewController (true, () => {
						ExecutionPerformer.UnregisterBlocker (this);
					});
				}
			};
		}

		private event EventHandler OnDismiss;
		ContactsPage MainPage;
		public ContactsNavController ()
		{
			var page = MainPage = new ContactsPage ();
			var controller = page.CreateViewController ();
			controller.Title = page.Title;
			this.PushViewController (controller, false);

			controller.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem ("Close", UIBarButtonItemStyle.Plain, (s, ev) => {
				OnDismiss?.Invoke (this, EventArgs.Empty);
			}), false);


			page.ContactSelected += async (sender, e) => {
				var result = await BlrtData.BlrtManager.App.OpenProfile (Executor.System (), e.Contact);
				var nav = result.Result as BlrtData.Views.Profile.IProfileNav;
				nav.OnDismissed += (sendera, ea) => {
					page.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
				};
			};


			if (BlrtContactManager.SharedInstanced.ShouldLoadContacts) {
				RefreshContact ();
			}
			page.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());

			BlrtContactManager.SharedInstanced.Changed += (sender, e) => {
				page?.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
			};
		}

		void RefreshContact ()
		{
			ContactManagerIOS.RefreshContacts ();
		}
	}
}

