using System;
using Xamarin.Forms;
using BlrtCanvas;
using System.Threading.Tasks;
using UIKit;
using BlrtData.ExecutionPipeline;
using BlrtData;
using System.Collections.Generic;
using Parse;

namespace BlrtiOS.ViewControllers.Canvas
{
	public class CanvasPage : ContentPage, IExecutionBlocker
	{
		TaskCompletionSource<ICanvasResult> _tcs;
		CanvasPageView _canvasView;
		bool _navigated;
		TaskCompletionSource<bool> _waitNavigation;
		static MultiExecutionPreventor.SingleExecutionToken _multiExeToken = MultiExecutionPreventor.NewToken ();
		static CanvasPage _canvas;
		static StatusBarHideController _canvasUIController;

		public static async Task<ICanvasResult> Open (UIViewController parent, IBlrtCanvasData data, bool animated = true)
		{
			using (var locker = _multiExeToken.Lock ()) {
				if (null == locker) {
					throw new InvalidOperationException ("canvas has already been opened");
				}
				if (null == _canvasUIController) {
					_canvas = new CanvasPage ();
					_canvasUIController = new StatusBarHideController (_canvas.CreateViewController ());
					_canvasUIController.ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve;
				}
				_canvas.StartNew (data);
				ExecutionPerformer.RegisterBlocker (_canvas);
				_canvasUIController.Appearing = true;
				parent.PresentViewController (_canvasUIController, true, null);

				//helpoverlay
				try {
					if (ParseUser.CurrentUser == null || !data.IsDraft) {

					} 
					else if(!BlrtPersistant.Properties.ContainsKey(BlrtConstants.HelpOverlayCanvasStartViewed)
							|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed])
					{
						BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasStartHelpOverlay ();
					}
				} catch { }

				//tracking
				if (data.IsDraft) {
					var dic = new Dictionary<string, object> ();
					dic.Add ("numberOfPages", data.PageCount);
					BlrtData.Helpers.BlrtTrack.TrackScreenView ("Canvas_Recording", false, dic);
				}

				var result = await _canvas._tcs.Task;

				_canvasUIController.Appearing = false;
                _canvasUIController.SetNeedsStatusBarAppearanceUpdate ();

				if (!_canvas._navigated) {
					await _canvasUIController.DismissViewControllerAsync (true);
				}
				_canvas._waitNavigation.SetResult (true);
				ExecutionPerformer.UnregisterBlocker (_canvas);
				parent = null;
				return result;
			}
		}

		private CanvasPage ()
		{
			_canvasView = new CanvasPageView (this);
			
			Content = _canvasView;

			_canvasView.ResultSet += (sender, e) => {
				var result = _canvasView.Result;
				if (null != result) {
					SetResult (result);
				}
			};
		}

		private void StartNew (IBlrtCanvasData data)
		{
			_tcs = new TaskCompletionSource<ICanvasResult> ();
			_waitNavigation = new TaskCompletionSource<bool> ();
			_canvasView.LoadNew (data);
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			_navigated = false;

			CanvasPageView.PlatformHelper.SetAutoDim (true);
			//_canvasView.PlayPausePressed += OnPlayPausePressed;
		}

		//protected virtual void OnPlayPausePressed (object sender, EventArgs e)
		//{
		//	Task.Run (delegate {
		//		BlrtData.Helpers.BlrtTrack.CanvasRecordPlay (data);

		//	});
		//}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();

			CanvasPageView.PlatformHelper.SetAutoDim (true);

			_navigated = true;

			//_canvasView.PlayPausePressed -= OnPlayPausePressed;
		}

		void SetResult (ICanvasResult result)
		{
			_tcs.TrySetResult (result);
		}

		#region IExecutionBlocker implementation
		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			if (_navigated) {
				return true;
			}
			bool exitCanvas = false;
			try {
				exitCanvas = await _canvasView.OnCrossPressed ();
			} catch (Exception ex) {
				LLogger.WriteEvent ("Canvas", "ResolveExecutionBlocker", "Exception", ex.ToString ());
			}
			if (!exitCanvas) {
				return false;
			} else {
				var nav = _navigated || await _waitNavigation.Task;
				return nav && (!_tcs.Task.Result.ShowMediaPicker);
			}
		}
		#endregion

		//status bar hider
		class StatusBarHideController : UIViewController
		{
			public StatusBarHideController (UIViewController root) : base ()
			{
				AddChildViewController (root);
				View.AddSubview (root.View);
			}

			public bool Appearing { get; set;}

			//public override void ViewDidDisappear (bool animated)
			//{
			//	base.ViewDidDisappear (animated);
			//	Appearing = false;
			//	SetNeedsStatusBarAppearanceUpdate ();
			//}

			//public override void ViewWillAppear (bool animated)
			//{
			//	base.ViewWillAppear (animated);
			//	Appearing = true;
   //             SetNeedsStatusBarAppearanceUpdate ();
			//}

			public override bool PrefersStatusBarHidden () {
				return Appearing;
			}

			public override void TraitCollectionDidChange (UITraitCollection previousTraitCollection)
			{
				base.TraitCollectionDidChange (previousTraitCollection);
				if (UIDevice.CurrentDevice.CheckSystemVersion (9, 0)) {
					ViewControllers.Canvas.CanvasPlatformHelper.Support3DTouch
								   = (TraitCollection.ForceTouchCapability == UIForceTouchCapability.Available);
				}
			}
		}
	}
}

