using System;
using UIKit;
using CoreGraphics;
using OpenTK.Graphics.ES20;
using System.Runtime.InteropServices;
using Foundation;

namespace BlrtiOS.ViewControllers.Canvas
{
	public static class OpenGLHelper
	{
		public static int MaxTextureSize = 2048;

		public static void CalulateMaxTextureSize ()
		{
			var maxTex = new int[1];
			GL.GetInteger (GetPName.MaxTextureSize, maxTex);
			MaxTextureSize = maxTex [0];
			var mem = NSProcessInfo.ProcessInfo.PhysicalMemory;
			if (mem <= 192ul * 1024ul * 1024ul) {
				MaxTextureSize = Math.Min (1024, maxTex [0]);
			} else if (mem <= 768ul * 1024ul * 1024ul) {
				MaxTextureSize = Math.Min (2048, maxTex [0]);
			}

			//TODO: please remove this code, its for the special event
			MaxTextureSize = Math.Min ((int)(MaxTextureSize*0.5), 2048);
		}

		public static TextureData GetImageRGBAForTex (UIImage image)
		{
			using (var spriteImage = image.CGImage) {
				int width = (int)spriteImage.Width;
				int height = (int)spriteImage.Height;
				if (width > height) {
					if (width > MaxTextureSize) {
						height = (int)(height * (float)MaxTextureSize / width);
						width = MaxTextureSize;
					}
				} else {
					if (height > MaxTextureSize) {
						width = (int)(width * (float)MaxTextureSize / height);
						height = MaxTextureSize;
					}
				}
				var data = new TextureData (width, height);
				using (var spriteContext = new CGBitmapContext (data.Data, width, height, 8, width * 4, 
					                           CGColorSpace.CreateDeviceRGB (), CGImageAlphaInfo.PremultipliedLast)) {
					spriteContext.DrawImage (new CGRect (0, 0, width, height), spriteImage);
					return data;
				}
			}
		}

		public static int LoadTexture (TextureData data, bool wrap = false)
		{
			var texId = GL.GenTexture ();
			GL.BindTexture (TextureTarget.Texture2D, texId);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)(wrap ? TextureWrapMode.Repeat : TextureWrapMode.ClampToEdge));
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)(wrap ? TextureWrapMode.Repeat : TextureWrapMode.ClampToEdge));
			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, data.Data);
			return texId;
		}

		public static byte[] Checker (UIColor color1, UIColor color2, int size)
		{
			byte[] checker = new byte[size * size * 4];

			byte r1, g1, b1, a1;
			byte r2, g2, b2, a2;

			color1.GetRGBAByte (
				out r1,
				out g1,
				out b1,
				out a1
			);
			color2.GetRGBAByte (
				out r2,
				out g2,
				out b2,
				out a2
			);

			for (int i = 0; i < size * size; ++i) {
				if (((i % size) > size * 0.5f) ^ ((i / size) > size * 0.5f)) {
					checker [i * 4 + 0] = r1;
					checker [i * 4 + 1] = g1;
					checker [i * 4 + 2] = b1;
					checker [i * 4 + 3] = a1;
				} else {
					checker [i * 4 + 0] = r2;
					checker [i * 4 + 1] = g2;
					checker [i * 4 + 2] = b2;
					checker [i * 4 + 3] = a2;
				}
			}
			return checker;
		}

		public static void GetRGBAByte (this UIColor color, out byte r, out byte g, out byte b, out byte a)
		{
			nfloat fr, fg, fb, fa;
			color.GetRGBA (
				out fr,
				out fg,
				out fb,
				out fa
			);
			r = Convert.ToByte (fr * 255);
			g = Convert.ToByte (fg * 255);
			b = Convert.ToByte (fb * 255);
			a = Convert.ToByte (fa * 255);
		}
	}

	public class TextureData : IDisposable
	{
		public IntPtr Data { get { return _data; } }

		public int Width { get { return _width; } }

		public int Height { get { return _height; } }

		private IntPtr _data;
		private int _width;
		private int _height;

		public TextureData (int width, int height)
		{
			if (width <= 0 || height <= 0) {
				throw new ArgumentException ("width and height must be bigger than 0");
			}
			if (width > OpenGLHelper.MaxTextureSize || height > OpenGLHelper.MaxTextureSize) {
				throw new ArgumentException ("Size is larger than OpenGL max texture size");
			}
			_width = width;
			_height = height;
			_data = Marshal.AllocHGlobal (width * height * 4);
		}

		public TextureData (byte[] bytes, int width, int height)
		{
			if (width <= 0 || height <= 0) {
				throw new ArgumentException ("width and height must be bigger than 0");
			}
			if (width > OpenGLHelper.MaxTextureSize || height > OpenGLHelper.MaxTextureSize) {
				throw new ArgumentException ("Size is larger than OpenGL max texture size");
			}
			if (bytes.Length != width * height * 4) {
				throw new ArgumentException ("width and height cannot match array size");
			}
			_width = width;
			_height = height;
			_data = Marshal.AllocHGlobal (width * height * 4);
			Marshal.Copy (bytes, 0, _data, bytes.Length);
			bytes = null;
		}

		public void Dispose ()
		{
			Marshal.FreeHGlobal (_data);
			_data = IntPtr.Zero;
			_width = 0;
			_height = 0;
		}
	}
}

