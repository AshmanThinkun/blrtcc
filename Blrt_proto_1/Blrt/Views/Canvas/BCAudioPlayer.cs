using System;
using BlrtCanvas;
using BlrtiOS.ViewControllers.Canvas;
using BlrtiOS.Util;
using AVFoundation;
using Foundation;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency (typeof (BCAudioPlayer))]
namespace BlrtiOS.ViewControllers.Canvas
{
	public class BCAudioPlayer : IBCAudioPlayer
	{
		private AVAudioPlayer _audioPlayer;
		private NSError _audioPlayerError;
		private string _audioPath;
		private bool _audioPlaying = false;

		public BCAudioPlayer ()
		{
			_audioPlayerError = new NSError (new NSString ("Blrt_proto_1"), 1);
			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out _audioPlayerError);
			AVAudioSession.SharedInstance ().SetActive (true, out _audioPlayerError);
		}

		#region IBCAudioPlayer implementation

		public event EventHandler Finished;

		public async Task LoadAudio (string audioFilePath)
		{
			if (_audioPlayer != null) {
				_audioPlayer.Dispose ();
				_audioPlayer = null;
			}

			// Changed follow the JIRA commit
			//_audioPath = DRAFT_FILE;
			_audioPath = audioFilePath;

			OpenAudioPlayer ();

			if (_audioPlayerError != null) {
				throw new Exception ("Fail to open audio file");
			}

		}

		public void Play ()
		{
			PlayFrom (null);
		}

		public void Pause ()
		{
			InternalPlaybackStop (false);
		}

		public void Seek (float seconds)
		{
			PlayFrom (seconds);
		}

		public void ReleaseFile ()
		{
			if (_audioPlayer != null) {
				_audioPlayer.Dispose ();
				_audioPlayer = null;
			}
		}

		public float Length {
			get {
				if (null != _audioPlayer) {
					return (float)_audioPlayer.Duration;
				}
				return 0;
			}
		}

		private double _lastTime = 0;

		public float CurrentTime {
			get {
				if (_audioPlayer != null) {
					_lastTime = Math.Max(_lastTime, _audioPlayer.CurrentTime);
					return (float)_lastTime;
				}
				return 0;
			}
		}

		#endregion

		private bool OpenAudioPlayer ()
		{

			if (_audioPlayer != null)
				return true;

			if (_audioPlayer == null) {

				try{

					_audioPlayer = AVAudioPlayer.FromUrl (NSUrl.FromFilename (_audioPath), out _audioPlayerError);
				} catch (Exception e){
					throw e;
				}
				if (_audioPlayer == null) {
					return false;
				}
			}

			double dt = 0;

			for (int i = 0; i < 16 && dt != _audioPlayer.Duration; ++i) {
				_audioPlayer.CurrentTime = dt = _audioPlayer.Duration;
				_audioPlayer.PrepareToPlay ();
			}

			_audioPlayer.CurrentTime = 0;


			_audioPlayer.FinishedPlaying += delegate {
				InternalPlaybackStop (true);
			};

			return true;
		}

		private void InternalPlaybackStop (bool fireEvent)
		{
			if (_audioPlayer != null && (_audioPlayer.Playing || _audioPlaying)) {
				_audioPlaying = false;
				_audioPlayer.Pause ();

				if (fireEvent && Finished != null)
					Finished (this, EventArgs.Empty);
			}

		}

		private void PlayFrom (double? time)
		{
			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayback, out _audioPlayerError);

			if (_audioPlayer == null) {

				OpenAudioPlayer ();

				if (_audioPlayer == null)
					throw new NullReferenceException ("Fail to open audio file");
			}

			if (time != null)
				_audioPlayer.CurrentTime = (time ?? 0);

			_audioPlaying = true;

			_audioPlayer.PrepareToPlay ();
			_audioPlayer.Play ();

			_lastTime = time ?? 0;
		}
	}
}

