using System;
using Xamarin.Forms.Platform.iOS;
using BlrtCanvas.Views;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using Xamarin.Forms;
using CoreGraphics;
using UIKit;

[assembly: ExportRenderer (typeof (CanvasBarBottomView), typeof (CanvasBarBottomViewRenderer))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class CanvasBarBottomViewRenderer : ViewRenderer
	{
		public CanvasBarBottomViewRenderer ()
		{
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			if (Hidden || Alpha <= 0)
				return null;

			for (int i = Subviews.Length - 1; i >= 0; i--) 
			{
				var itrView = Subviews [i];
				var ptX = point.X - itrView.Frame.X;
				var ptY = point.Y - itrView.Frame.Y;
				for (int j = itrView.Subviews.Length - 1; j >= 0; j--) {
					var hit = itrView.Subviews[j].HitTest (new CGPoint (ptX - itrView.Subviews[j].Frame.X, ptY - itrView.Subviews[j].Frame.Y), uievent);
					if (hit != null)
						return hit;
				}
			}

			return null;
		}
	}
}

