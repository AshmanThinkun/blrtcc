using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using BlrtCanvas.Views;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using UIKit;
using MediaPlayer;
using CoreGraphics;
using Foundation;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (VolumeControlView), typeof (VolumeControlViewRenderer))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class VolumeControlViewRenderer : ViewRenderer
	{
		MPVolumeView _slider;
		UIView _sliderDrawer;
		public VolumeControlViewRenderer ()
		{
			_sliderDrawer = null;
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			if (null != _slider) {
				_slider.RemoveFromSuperview ();
			}
			var oldEle = e.OldElement as VolumeControlView;
			if (null != oldEle) {
				oldEle.TintColorChanged -= OnTintColorChanged;
			}
			base.OnElementChanged (e);
			var newEle = e.NewElement as VolumeControlView;
			if (null != newEle) {
				newEle.TintColorChanged += OnTintColorChanged;
			}
			FindSliderContainer ();
			if (_slider == null) {
				_slider = new MPVolumeView () {
					AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleWidth,
					ShowsRouteButton = false
				};
				_slider.SetVolumeThumbImage (BlrtHelperiOS.FromBundleTemplate ("player/volume/volume-toggle.png"), UIControlState.Normal);
				_slider.SizeToFit ();
				_slider.Transform = CGAffineTransform.Rotate (_slider.Transform, (-0.5f * NMath.PI));

				// find the slider
				foreach (var view in _slider.Subviews) {
					if (view is UIControl) {
						var mpSlider = view as UIControl;
						mpSlider.ValueChanged += OnSliderValueChanged;
					}
				}
			}
			_sliderDrawer.AddSubview (_slider);
			return;
		}

		void OnTintColorChanged (object sender, Xamarin.Forms.Color e)
		{
			if (_slider != null) {
				_slider.TintColor = e.ToUIColor ();
			}
		}

		void OnSliderValueChanged (object sender, EventArgs e) 
		{
			var ele = this.Element as VolumeControlView;
			var realSlider = sender as UISlider;
			if (null != ele && null != realSlider) {
				ele.NotifyVolumeChanged (realSlider.Value / (realSlider.MaxValue - realSlider.MinValue));
			}
		}

		void FindSliderContainer()
		{
			if (this.Subviews.Length > 0) {
				UIView mask = null;
				foreach (var item in this.Subviews) {
					if (item is BlrtImageButtonRenderer) {
						continue;
					}
					mask = item;
					break;
				}
				if (null != mask && mask.Subviews.Length > 0) {
					_sliderDrawer = mask.Subviews [0];
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			var drawerFrame = _sliderDrawer.Frame;
			_slider.Frame = new CGRect (
				(nfloat)VolumeControlView.HorizontalPadding + 7, 
				(nfloat)VolumeControlView.VerticalPadding, 
				(nfloat)(drawerFrame.Width - VolumeControlView.HorizontalPadding * 2), 
				(nfloat)(drawerFrame.Height - VolumeControlView.VerticalPadding * 2)
			);
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			var ele = this.Element as VolumeControlView;
			if (null != ele) {
				if (ele.IsVisible && ele.IsEnabled && Bounds.Contains (point)) {
					if (ele.DrawerOpened) {
						return base.HitTest (point, uievent);
					} else {
						if ((point.X >= 0) && (point.Y > ele.MaskBounds.Height)) {
							return base.HitTest (point, uievent);
						}
					}
				}
			}
			return null;
		}
	}
}

