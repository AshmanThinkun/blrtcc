using System;
using Xamarin.Forms;
using BlrtCanvas.Views;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Threading.Tasks;

[assembly: ExportRenderer (typeof (ToolSelectionView), typeof (ToolSelectionViewRenderer))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class ToolSelectionViewRenderer : ViewRenderer<ToolSelectionView, UIView>
	{
		private int WaitingDuration = 150; // ms
		bool _touchDown = false;

		public ToolSelectionViewRenderer ()
		{
			this.MultipleTouchEnabled = false;
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			var tmp = evt.TouchesForView (this);
			if (tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);

			var tmp = evt.TouchesForView (this);
			if (tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);

			var tmp = evt.TouchesForView (this);
			if (tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);

			var tmp = evt.TouchesForView (this);
			if (tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			var ele = this.Element as ToolSelectionView;
			if (null != ele) {
				if (180 == ele.RotationY) {
					point = new CGPoint (((nfloat)ele.Width - point.X), point.Y);
				}
				bool hover = Bounds.Contains (point);
				if (ele.DrawerOpen) {
					return hover ? this : null;
				} else {
					var hovenIcon = ele.DrawerOpen || hover && (point.X < (ele.ToolIconBounds.Width + ele.ToolIconBounds.Y));
					if (hovenIcon) {
						return this;
					} else {
						return null;
					}
				}
			}
			return null;
		}

		void HandleTouch (UITouch[] touches)
		{
			if (1 != touches.Length) {
				return;
			}
			var ele = this.Element as ToolSelectionView;
			if (null == ele) {
				return;
			}
			var touch = touches [0];
			var loc = touch.LocationInView (this);
			bool hover = Bounds.Contains (loc);
			switch (touch.Phase) 
			{
				case UITouchPhase.Began:
					var hoverIcon = hover && (loc.X <= (ele.ToolIconBounds.X + ele.ToolIconBounds.Width)) && (loc.X >= ele.ToolIconBounds.X);
					if (!hoverIcon) {
						if (hover) {
							goto case UITouchPhase.Moved;
						} else {
							return;
						}
					}
					_touchDown = true;
					DelayedShowDrawer (ele);
					ele.NotifySelectionStarted ();
					goto case UITouchPhase.Moved;
				case UITouchPhase.Moved:
					if (hover) {
						ele.OnTouchMoveInside ((float)loc.X, (float)loc.Y);
					}
					break;
				case UITouchPhase.Ended:
				case UITouchPhase.Cancelled:
					_touchDown = false;
					if (ele.OpenOnTapped && !ele.DrawerOpen) {
						ele.SetDrawerOpen (true);
					} else {
						ele.SetDrawerOpen (false);
					}

					if (hover) {
						ele.OnToolSelected ();
					}
					break;
			}
		}

		async Task DelayedShowDrawer (ToolSelectionView ele)
		{
			await Task.Delay (WaitingDuration);

			if (_touchDown) {
				Device.BeginInvokeOnMainThread (() => {
					ele.SetDrawerOpen (true, true);
				});
			}
		}
	}
}

