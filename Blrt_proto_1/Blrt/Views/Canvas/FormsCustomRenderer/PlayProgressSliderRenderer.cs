using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using BlrtCanvas.Views;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using CoreGraphics;
using UIKit;

[assembly: ExportRenderer (typeof (PlayProgressSlider), typeof (PlayProgressSliderRenderer))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class PlayProgressSliderRenderer : SliderRenderer
	{

		private PlayProgressSlider RealElement
		{ 
			get {
				var slider = Element as PlayProgressSlider;
				if (slider != null) {
					return slider;
				} else {
					throw new Exception("Render is not connected to PlayProgressSlider");
				}
			} 
		}

		public PlayProgressSliderRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Slider> eventarg)
		{
			base.OnElementChanged (eventarg);
			Control.MaxValue = PlayProgressSlider.MAX_PROGRESS;
			Control.ValueChanged += (sender, e) => {
				RealElement.NotifyProgressChanged(new PlayProgressSlider.ProgressChangedArgs(Control.Value));
			};
			Control.TouchDown += (sender, e) => {
				RealElement.NotifyTouchDown();
			};
			Control.TouchCancel += (sender, e) => {
				RealElement.NotifyTouchUp();
			};
			Control.TouchUpInside += (sender, e) => {
				RealElement.NotifyTouchUp();
			};
			Control.TouchUpOutside += (sender, e) => {
				RealElement.NotifyTouchUp();
			};

			SetTrackMin ();
			SetTrackMax ();
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == PlayProgressSlider.TintColorProperty.PropertyName)
				SetTrackMin ();

			if (e.PropertyName == PlayProgressSlider.TrackColorProperty.PropertyName) {
				SetTrackMax ();

			}
		}

		void SetTrackMin ()
		{
			var rect = new CGRect(0, 0, 2, 2);
			UIGraphics.BeginImageContextWithOptions(rect.Size, false, 1);
			RealElement.TintColor.ToUIColor ().SetFill ();
			UIGraphics.RectFill(rect);
			UIImage image = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			Control.SetMinTrackImage (image, UIControlState.Normal);
		}

		void SetTrackMax ()
		{
			var rect = new CGRect(0, 0, 2, 2);
			UIGraphics.BeginImageContextWithOptions(rect.Size, false, 1);
			RealElement.TrackColor.ToUIColor ().SetFill ();
			UIGraphics.RectFill(rect);
			UIImage image = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			Control.SetMaxTrackImage (image, UIControlState.Normal);
		}
	}
}

