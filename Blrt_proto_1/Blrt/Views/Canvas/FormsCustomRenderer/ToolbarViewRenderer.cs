using System;
using Xamarin.Forms;
using BlrtCanvas.Views;
using Xamarin.Forms.Platform.iOS;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer (typeof (ToolbarView), typeof (ToolbarViewRenderer))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class ToolbarViewRenderer : ViewRenderer<ToolbarView, UIView>
	{
		public ToolbarViewRenderer ()
		{
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			var v = HitDeep (this, point, uievent, 1);
			return v;
		}

		UIView HitDeep (UIView view, CGPoint point, UIEvent uievent, int delve) {
		
			foreach (var subv in view.Subviews) {

				var p = new CGPoint (point.X - subv.Frame.X, point.Y - subv.Frame.Y);

				if (delve > 0) {
					var hit = HitDeep (subv, p, uievent, delve - 1);

					if (hit != null)
						return hit;
				} else {
					var hit = subv.HitTest (p, uievent);
				
					if (hit != null)
						return hit;
				}
			}

			return null;
		}
	}
}

