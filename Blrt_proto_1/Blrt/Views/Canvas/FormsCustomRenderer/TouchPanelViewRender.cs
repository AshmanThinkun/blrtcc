using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using BlrtCanvas.Views;
using BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer;
using BlrtCanvas;
using Foundation;
using UIKit;

[assembly: ExportRenderer (typeof (TouchPanelView), typeof (TouchPanelViewRender))]
namespace BlrtiOS.ViewControllers.Canvas.FormsCustomRenderer
{
	public class TouchPanelViewRender : BoxRenderer
	{
		CanvasTouchCollection _collection;

		private TouchPanelView TouchPanel {
			get {
				return Element as TouchPanelView;
			}
		}

		nfloat _scale;

		public TouchPanelViewRender ()
		{
			this.MultipleTouchEnabled = true;
			_collection = new CanvasTouchCollection ();
			_scale = UIScreen.MainScreen.Scale;
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			var ele = TouchPanel;
			if (null == ele || ele.InputTransparent) {
				return;
			}
			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			var ele = TouchPanel;
			if (null == ele || ele.InputTransparent) {
				return;
			}
			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var ele = TouchPanel;
			if (null == ele || ele.InputTransparent) {
				return;
			}
			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			var ele = TouchPanel;
			if (null == ele || ele.InputTransparent) {
				return;
			}
			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				HandleTouch (tmp.ToArray<UITouch> ());
			else 
				HandleTouch (touches.ToArray<UITouch> ());
		}

		void HandleTouch (UITouch[] touches)
		{
			_collection.ResetState ();
			for (int i = 0; i < touches.Length; i++) {
				var state = CanvasTouch.CanvasTouchState.Idle;
				var touch = touches [i];
				switch (touch.Phase) {
					case UITouchPhase.Began:
						state = CanvasTouch.CanvasTouchState.Pressed;
						break;
					case UITouchPhase.Moved:
						state = CanvasTouch.CanvasTouchState.Moved;
						break;
					case UITouchPhase.Stationary:
						state = CanvasTouch.CanvasTouchState.Idle;
						break;
					case UITouchPhase.Cancelled:
					case UITouchPhase.Ended:
						state = CanvasTouch.CanvasTouchState.Released;
						break;
					default:
						break;
				}
				var location = touch.LocationInView (this);
				_collection.Update (touch.Handle.ToInt32 (), state, (float)(location.X * _scale), (float)(location.Y * _scale));
			}
			TouchPanel.OnTouchEvent (_collection);
		}
	}
}

