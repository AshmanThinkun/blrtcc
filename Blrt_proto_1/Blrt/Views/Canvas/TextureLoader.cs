using System;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using UIKit;
using BlrtiOS.Media;
using BlrtiOS.Util;
using Xamarin.Forms;

namespace BlrtiOS.ViewControllers.Canvas
{
	public class TextureLoader : IBCTextureLoader
	{
		string _path;
		int _pdfPageNum = -1;
		TextureData _data;
		bool _asset;
		bool _wrap;

		private TextureLoader (string imgPath, bool isAsset = false, bool wrapTexture = false)
		{
			_path = imgPath;
			_pdfPageNum = -1;
			_asset = isAsset;
			_wrap = wrapTexture;
			_data = null;
		}

		private TextureLoader (string pdfPath, int pageNum, bool isAsset = false)
		{
			_path = pdfPath;
			_pdfPageNum = pageNum;
			_asset = isAsset;
			_wrap = false;
			_data = null;
		}

		public TextureLoader (TextureData data, bool wrapTexture = false)
		{
			_path = null;
			_pdfPageNum = -1;
			_asset = false;
			_wrap = wrapTexture;
			_data = data;
		}

		public Task<bool> Load ()
		{
			return Task<bool>.Run (async () => {
				if (_data == null) {
					if (_pdfPageNum > 0) {
						var pdf = BlrtPDF.FromFile(_path);
						var size = pdf.GetPageSize (_pdfPageNum);
						int width, height;
						if (size.Width > size.Height) {
							width = OpenGLHelper.MaxTextureSize;
							height = (int)((float)(width / size.Width) * size.Height);
						} else {
							height = OpenGLHelper.MaxTextureSize;
							width = (int)((float)(height / size.Height) * size.Width);
						}
						using (var img = BlrtUtilities.ImageFromPDF(pdf, _pdfPageNum, width, height)) {
							_data = OpenGLHelper.GetImageRGBAForTex(img);
						}
					} else {
						using (var img = _asset ? (await LoadAsset(_path)) : UIImage.FromFile(_path)) {
							_data = OpenGLHelper.GetImageRGBAForTex(img);
						}
					}
				}
				return (null != _data);
			});
		}

		static Task<UIImage> LoadAsset(string path) {
			var tcs = new TaskCompletionSource<UIImage> ();
			Device.BeginInvokeOnMainThread (() => {
				tcs.TrySetResult(UIImage.FromBundle(path));
			});
			return tcs.Task;
		}

		public int LoadIntoOpenGL ()
		{
			if (null == _data) {
				throw new InvalidOperationException ("Load() task has not finished successfully");
			}
			return OpenGLHelper.LoadTexture (_data, _wrap);
		}

			
		public void Dispose ()
		{
			if (null != _data) {
				_path = null;
				_data.Dispose ();
				_data = null;
				_asset = false;
				GC.Collect ();
			}
		}

		public static TextureLoader ForImageFile (string path, bool wrapTexture = false)
		{
			return new TextureLoader (path, false, wrapTexture);
		}

		public static TextureLoader ForImageAsset (string path, bool wrapTexture = false)
		{
			return new TextureLoader (path, true, wrapTexture);
		}

		public static TextureLoader ForPDF (string path, int pageNum)
		{
			return new TextureLoader (path, pageNum, false);
		}

		public static TextureLoader ForTextureData (TextureData data, bool wrapTexture = false)
		{
			return new TextureLoader (data, wrapTexture);
		}
	}
}

