using System;
using BlrtiOS.ViewControllers.Canvas;
using BlrtCanvas;
using System.IO;
using UIKit;
using Foundation;
using BlrtiOS.Media;
using BlrtData.Media;
using BlrtCanvas.Pages;
using BlrtData;
using BlrtCanvas.GLCanvas;
using System.Threading.Tasks;
using BlrtiOS.Util;

[assembly: Xamarin.Forms.Dependency (typeof (CanvasPlatformHelper))]
namespace BlrtiOS.ViewControllers.Canvas
{
	public class CanvasPlatformHelper : ICanvasPlatformHelper
	{
		public CanvasPlatformHelper ()
		{
			Support3DTouchChanged += (sender, e) => {
				PreferredToolbarLayoutChanged?.Invoke (this, PreferredToolbarLayout);
			};
		}

		#region ICanvasPlatformHelper implementation

		static bool _support3DTouch = false;

		static event EventHandler<bool> Support3DTouchChanged;

		static public bool Support3DTouch {
			get {
				return _support3DTouch;
			}
			set {
				if (_support3DTouch != value) {
					_support3DTouch = value;
					Support3DTouchChanged?.Invoke (null, _support3DTouch);
				}
			}
		}

		public event EventHandler<ToolbarLayoutPolicy> PreferredToolbarLayoutChanged;

		public ToolbarLayoutPolicy PreferredToolbarLayout { 
			get {
				//return ToolbarLayoutPolicy.Flexible;
				return _support3DTouch ? ToolbarLayoutPolicy.AlwaysRight : ToolbarLayoutPolicy.Flexible;
			}
		}


		public string Translate (string field)
		{
			return field;
		}

		public string Translate (string field, string context)
		{
			return field;
		}

		public void SetAutoDim (bool allow)
		{
			UIApplication.SharedApplication.IdleTimerDisabled = !allow;
		}

		public Stream GetAssetFileStream (string filepath)
		{
			return new FileStream (filepath, FileMode.Open, FileAccess.Read, FileShare.Read);
		}

		public int PDFPageCount (string path)
		{
			return BlrtPDF.FromFile (path).Pages;
		}

		public ICanvasPage OpenPage (string objectId, int mediaPage, ILocalStorageParameter localStorage)
		{
			if (BlrtTemplate.IsTempleteId (objectId)) {

				return new BlrtTemplatePage(BlrtTemplate.GetTempleteId (objectId));

			} else {

				var mediaAsset = localStorage.DataManager.GetMediaAsset (objectId);
				if (null == mediaAsset) {
					return null;
				}
				switch (mediaAsset.Format) {
					case BlrtMediaFormat.Image:
						return new BlrtImagePage(mediaAsset);
					case BlrtMediaFormat.PDF:
						return new BlrtPDFPage (mediaAsset, mediaPage, mediaAsset.Name); // mediaPage starts from 1
				}
				return null;

			}
		}

		public async Task<int> ShowAlertAsync (string title, string message, string cancelBtnTitle, params string[] otherBtns)
		{
			var tcs = new TaskCompletionSource<int> ();
			var alert = new UIAlertView (title, message, null, cancelBtnTitle);
			var ids = new nint[otherBtns.Length];
			for (int i = 0; i < otherBtns.Length; i++) {
				ids[i] = alert.AddButton (otherBtns [i]);
			}
			alert.Clicked += (sender, e) => {
				if (e.ButtonIndex == alert.CancelButtonIndex) {
					tcs.SetResult(-1);
					return;
				} else {
					for (int i = 0; i < ids.Length; i++) {
						if (e.ButtonIndex == ids[i]) {
							tcs.SetResult(i);
							return;
						}
					}
				}
			};
			alert.Show ();
			return await tcs.Task;
		}

		public System.Drawing.Size GetImageSizeBeforeRotation (string filePath)
		{
			using(var img = UIImage.FromFile (filePath)) {
				var size = img.Size;
				switch (img.Orientation) {
					case UIImageOrientation.Left:
					case UIImageOrientation.LeftMirrored:
					case UIImageOrientation.Right:
					case UIImageOrientation.RightMirrored:
						var temp = size.Width;
						size.Width = size.Height;
						size.Height = temp;
						break;
					default:
						break;
				}
				return new System.Drawing.Size ((int)size.Width, (int)size.Height);
			}
		}

		public TextureRotation GetImageRotation (string filePath)
		{
			using(var img = UIImage.FromFile (filePath)) {
				switch (img.Orientation) {
					case UIImageOrientation.Up:
						return TextureRotation.Angle_0;
					case UIImageOrientation.UpMirrored:
						return TextureRotation.Angle_0_Flipped_Horizontally;
					case UIImageOrientation.Down:
						return TextureRotation.Angle_180;
					case UIImageOrientation.DownMirrored:
						return TextureRotation.Angle_180_Flipped_Horizontally;
					case UIImageOrientation.Left:
						return TextureRotation.Angle_90;
					case UIImageOrientation.LeftMirrored:
						return TextureRotation.Angle_270_Flipped_Vertically;
					case UIImageOrientation.Right:
						return TextureRotation.Angle_270;
					case UIImageOrientation.RightMirrored:
						return TextureRotation.Angle_90_Flipped_Vertically;
					default:
						return TextureRotation.Angle_0;
				}
			}
		}

		public Task<bool> CreateImgFileThumbnail (string srcPath, string dstPath, System.Drawing.Size thumbnailSize)
		{
			return BlrtUtilities.CreateImgFileThumbnail (srcPath, dstPath, thumbnailSize);
		}

		public Task<bool> CreatePDFThumbnail (string srcPDFPath, int mediaPage, string dstPath, System.Drawing.Size thumbnailSize)
		{
			return BlrtUtilities.CreatePDFThumbnail (srcPDFPath, mediaPage, dstPath, thumbnailSize);
		}

		public System.Drawing.Size GetPDFPageSize (string pdfPath, int mediaPage)
		{
			var size = BlrtPDF.FromFile (pdfPath).GetPageSize (mediaPage);

			return new System.Drawing.Size ((int)size.Width, (int)size.Height);
		}

		public System.Drawing.Size GetImageAssetSize (string assetPath)
		{
			using (var img = UIImage.FromBundle(assetPath)) {
				var size = img.Size;
				return new System.Drawing.Size ((int)size.Width, (int)size.Height);
			}
		}

		public IBCTextureLoader GetImageTextureLoader (string path)
		{
			return TextureLoader.ForImageFile (path);
		}

		public IBCTextureLoader GetPDFTextureLoader (string path, int mediaPage)
		{
			return TextureLoader.ForPDF (path, mediaPage);
		}

		public void ReleaseAll ()
		{
			BlrtPDF.ReleaseAll ();
		}

		public IBCTextureLoader GetImageAssetTextureLoader (string path)
		{
			return TextureLoader.ForImageAsset (path, false);
		}

		public void CalulateMaxTextureSize ()
		{
			OpenGLHelper.CalulateMaxTextureSize ();
		}

		const int Size = 16;
		public IBCTextureLoader GetTiledImageAssetTextureLoader (string path)
		{
			if (path.Equals ("texture_placeholder.png")) {
				return TextureLoader.ForTextureData (new TextureData (OpenGLHelper.Checker (UIColor.Gray, UIColor.White, Size), Size, Size), true);
			} else {
				return TextureLoader.ForImageAsset (path, true);
			}

		}

		public bool TryValidateConversationName (string convoName)
		{
			return BlrtiOS.BlrtHelperiOS.TryValidateConversationName (convoName);
		}

		public float ScreenScale {
			get {
				return (float)UIScreen.MainScreen.Scale;
			}
		}

		public int ThumbnailLength {
			get {
				var length = 0;

				if (BlrtHelperiOS.IsPhone) length = BlrtUtil.THUMBNAIL_SIZE_SMALL;
				if (BlrtHelperiOS.IsPhabletOrBigger) length = BlrtUtil.THUMBNAIL_SIZE_LARGE;

				return length;
			}
		}

		#endregion
	}
}

