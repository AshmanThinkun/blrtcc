using System;
using BlrtCanvas;
using BlrtiOS.ViewControllers.Canvas;
using AVFoundation;
using Foundation;
using System.Threading.Tasks;
using BlrtiOS.Util;

[assembly: Xamarin.Forms.Dependency (typeof (BCAudioRecorder))]
namespace BlrtiOS.ViewControllers.Canvas
{
	public class BCAudioRecorder : IBCAudioRecorder
	{
		private float _duration;
		private AVAudioRecorder _audioRecorder;
		private NSError _audioRecorderError;
		private AudioSettings _audioSettings;
		private string _audioPath;
		private bool _audioRecording = false;

		public BCAudioRecorder ()
		{
			_audioRecorderError = new NSError (new NSString ("Blrt_proto_1"), 1);
			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out _audioRecorderError);
			AVAudioSession.SharedInstance ().SetActive (true, out _audioRecorderError);

			NSDictionary values = new NSDictionary (
				AVAudioSettings.AVSampleRateKey,			NSNumber.FromFloat (44100f / 4), //Sample Rate
				AVAudioSettings.AVFormatIDKey,				NSNumber.FromInt32 ((int)AudioToolbox.AudioFormatType.MPEG4AAC),
				AVAudioSettings.AVNumberOfChannelsKey,		NSNumber.FromInt32 (1),  //Channels
				AVAudioSettings.AVEncoderAudioQualityKey,	NSNumber.FromInt32 ((int)AudioToolbox.AudioConverterQuality.Max)  //Channels
			);

			_audioSettings = new AudioSettings (values);
		}

		#region IBCAudioRecorder implementation

		public bool CanContinueRecording { get { return _duration < BlrtData.BlrtPermissions.MaxBlrtDuration - 0.2f; } }

		public void SetFilePath (string path)
		{
			_audioPath = path;

		}

		public async Task<bool> RequestPermissions ()
		{
			var permission = BlrtUtilities.AudioRecordPermission;

			if (permission != null) {
				return permission.Value;
			}

			return await BlrtUtilities.RequestAudioRecordPermission ();
		}

		public void Record ()
		{
			if ((null != _audioRecorder) && _audioRecording && (!_audioRecorder.Recording)) {
				_audioRecorder.Record ();
				return;
			}

			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out _audioRecorderError);

			if (_audioRecorder == null) {
				//				_audioPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), DRAFT_FILE);
				_audioRecorder = AVAudioRecorder.Create (NSUrl.FromFilename (_audioPath), _audioSettings, out _audioRecorderError);

				_audioRecorder.BeginInterruption += (object sender, EventArgs e) => {
					InternalRecordStop (true);
				};
				_audioRecorder.FinishedRecording += (object sender, AVStatusEventArgs e) => {
					InternalRecordStop (true);
				};

			}

			// extra 0.1 to push it over into the next second
			float maxLength = 100000000000;//BlrtAccountSettings.MaxBlrtDuration + 0.15f;

			_audioRecorder.PrepareToRecord ();
			_audioRecorder.MeteringEnabled = true;
			_audioRecorder.RecordFor (_audioRecording ? maxLength - _audioRecorder.currentTime : maxLength);
			_audioRecording = true;
		}

		public async Task<float> Pause ()
		{
			var duration = Duration;
			if ((null != _audioRecorder) && _audioRecording && (_audioRecorder.Recording)) {
				_audioRecorder.Pause ();
			}
			return duration;
		}

		public async Task<float> Stop ()
		{
			var duration = Duration;
			InternalRecordStop (false);
			return duration;
		}

		public void Reset ()
		{
			if (_audioRecorder != null) {
				_audioRecorder.Dispose ();
				_audioRecorder = null;
			}
		}

		public void Release ()
		{
			// do nothing
		}

		public bool IsRecording {
			get {
				return _audioRecording;
			}
		}

		public float Duration {
			get {
				if (_audioRecorder != null && _audioRecorder.Recording) {
					return (float)_audioRecorder.currentTime;
				}
				return _duration;
			}
		}

		public const string EXTENSION = ".m4a";

		public string Extension {
			get {
				return EXTENSION;
			}
		}

		public string FilePath {
			get {
				return _audioPath;
			}
		}
		#endregion

		private void InternalRecordStop (bool fireEvent)
		{
			if (_audioRecorder != null && (_audioRecorder.Recording || _audioRecording)) {
				_audioRecording = false;
				_duration = (float)_audioRecorder.currentTime;
				_audioRecorder.Stop ();
			}

		}

		float _avg = 0;
		public float CurrentVolume {
			get {
				uint channel = 0;

				if (_audioRecorder != null && _audioRecorder.Recording) {
					_audioRecorder.UpdateMeters ();


					const float Alpha = 0.9f;
					const float Beta = 0.25f;

					var value = _audioRecorder.PeakPower (channel) * Beta + _audioRecorder.AveragePower (channel) * (1 - Beta);
					//var value = _audioRecorder.AveragePower (channel);

					_avg = (float)(Alpha * value + (1.0 - Alpha) * _avg);

					//double peakPowerForChannel = Math.Pow(10, (0.05 * value));
					//_avg = (float)(ALPHA * peakPowerForChannel + (1.0 - ALPHA) * _avg);
				} else {
					_avg = -50;
				}

				return (_avg + 50f) / 50f;
			}
		}
	}
}

