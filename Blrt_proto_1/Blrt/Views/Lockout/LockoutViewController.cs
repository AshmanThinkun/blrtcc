using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;
using BlrtData;
using BlrtData.Notification;
using Newtonsoft.Json;
using BlrtiOS.UI;

namespace BlrtiOS.Views.Lockout
{
	public class LockoutViewController : Login.GenericLoginController
	{
		public override string ViewName { get { return "BSOD_" + Mode.ToString (); } }

		public override float ContentWidth { get { return  BlrtHelperiOS.IsPhone ? 280 : 480; } }

		public BlrtLockoutMode Mode { get; private set; }

		private UIImageView _image;
		private UIView _imageContainer;
		private UILabel _textLbl;
		private UILabel _textDetailLbl;

		private UIView _buttonContainer;
		private List<BlrtActionButton> _buttons;



		private UILabel _versionLbl;


		BlrtCloudNotification currentMessage;

		private bool _animate;
		private int _switchCounter;

		public LockoutViewController ()
		{

			_buttons = new List<BlrtActionButton> ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			_logo.Hidden = true;

			_image = new UIImageView (UIImage.FromBundle ("logo.png")) {
				ContentMode = UIViewContentMode.ScaleAspectFit,
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleTopMargin,
				Transform = CoreGraphics.CGAffineTransform.MakeRotation (NMath.PI)
			};
			if (!BlrtHelperiOS.IsPhone) {
				_image.Frame = new CGRect (0, 0, ContentWidth, 128);
			} else {
				_image.Frame = new CGRect (0, 0, ContentWidth, 96);
			}

			_imageContainer = new UIView (_image.Bounds);
			_imageContainer.Center = new CGPoint (ContentWidth * 0.5f, BUTTON_MARGIN + _logo.Frame.Height * 0.5f);
			ContentView.Add (_imageContainer);
			_imageContainer.Add (_image);

			_textLbl = new UILabel () {
				Lines = 0,
				Font = BlrtConfig.LightFont.Title,
				TextColor = BlrtConfig.BlrtBlack,
				TextAlignment = UITextAlignment.Center
			};
			ContentView.Add (_textLbl);

			_textDetailLbl = new UILabel () {
				Lines = 0,
				Font = BlrtConfig.LightFont.SubTitle,
				TextColor = BlrtConfig.BlrtBlack,
				TextAlignment = UITextAlignment.Center
			};
			ContentView.Add (_textDetailLbl);


			_buttonContainer = new UIView () {
			};
			ContentView.Add (_buttonContainer);

			_versionLbl = new UILabel (new CGRect (10, 20, View.Frame.Width - 20, 16)) {

				Lines 			= 0,
				Font 			= BlrtConfig.NormalFont.Normal,
				TextColor 		= BlrtConfig.BlrtBlack.ColorWithAlpha (0.1f),
				TextAlignment 	= UITextAlignment.Right,
				Text 			= BlrtConfig.APP_VERSION
			};
			ScrollView.Add (_versionLbl);


			LayoutController ();
		}

		public override void KeyboardHasChanged ()
		{
		}


		private void OnTimer (NSTimer nsTimer = null)
		{
		
			if (!_animate)
				return;


			InvokeOnMainThread (() => {

				float timer = 5;

				switch (_switchCounter) {
					case 0:
						_image.Transform = CoreGraphics.CGAffineTransform.MakeRotation (NMath.PI);
						break;
					case 1:
					case 3:
					case 5:
					case 7:
						_image.Transform = CoreGraphics.CGAffineTransform.MakeRotation (NMath.PI + 0.2f);
						timer = 0.1f;
						break;
					default:
						_image.Transform = CoreGraphics.CGAffineTransform.MakeRotation (NMath.PI);
						timer = 0.1f;
						break;
				}

				_switchCounter = (_switchCounter + 1) % 10;
				NSTimer.CreateScheduledTimer (timer, OnTimer);
			});
		}






		public void Activite (BlrtLockoutMode mode, bool animate)
		{
			Mode = (Mode | mode);
			LayoutController ();
		}

		public void Deactivite (BlrtLockoutMode mode, bool animate)
		{
	
			Mode = (Mode | mode) ^ mode;
		
			if (Mode != BlrtLockoutMode.None) {
				LayoutController ();
			} else {
				_animate = false;
			}
		}

		BlrtLockoutMode GetHighestPriorityMode ()
		{
			var values = Enum.GetValues (typeof(BlrtLockoutMode));

			Array.Sort (values);
			Array.Reverse (values);


			foreach (var v in values) {

				var e = (BlrtLockoutMode)v;

				if (e != BlrtLockoutMode.None && (Mode & e) == e) {
					return e;
				}			
			}

			return BlrtLockoutMode.None;
		}

		private void LayoutController ()
		{
		
			if (!IsViewLoaded)
				return;

			var mode = GetHighestPriorityMode ();


			switch (mode) {
				case BlrtLockoutMode.Maintance:
					ShowMessage (BlrtSettings.MaintenanceMessage);
					break;
				case BlrtLockoutMode.InvalidVersion:
					ShowMessage (BlrtSettings.InvalidVersionMessage);
					break;
			}


			if (!_animate) {
				_animate = true;
				OnTimer ();
			}

			ScrollView.SetNeedsLayout ();
		}

		void ShowMessage (string message)
		{
			try {
				currentMessage = JsonConvert.DeserializeObject<BlrtCloudNotification> (message);
			} catch {
				currentMessage = new BlrtCloudNotification ();
			}

			nfloat SpacingBetween = BlrtHelperiOS.IsPhone ? 20 : 44;


			_textLbl.Text = currentMessage.Title;
			_textDetailLbl.Text = currentMessage.Message;

			nfloat bottom = _imageContainer.Frame.Bottom;

			_textLbl.Hidden = string.IsNullOrWhiteSpace (_textLbl.Text);
			_textDetailLbl.Hidden = string.IsNullOrWhiteSpace (_textDetailLbl.Text);

			if (!_textLbl.Hidden) {
				var size = _textLbl.SizeThatFits (new CGSize (ContentView.Frame.Width * 0.8f, nfloat.MaxValue));
				_textLbl.Frame = new CGRect ((ContentView.Frame.Width - size.Width) * 0.5f, bottom + SpacingBetween, size.Width, size.Height);
				bottom = _textLbl.Frame.Bottom;
			}

			if (!_textDetailLbl.Hidden) {
				var size = _textDetailLbl.SizeThatFits (new CGSize (ContentView.Frame.Width, nfloat.MaxValue));
				_textDetailLbl.Frame = new CGRect ((ContentView.Frame.Width - size.Width) * 0.5f, bottom + SpacingBetween, size.Width, size.Height);
				bottom = _textDetailLbl.Frame.Bottom;
			}

			for (int i = 0; (currentMessage.Options != null && i < currentMessage.Options.Length) || i < _buttons.Count; ++i) {
				var btn = i < _buttons.Count ? _buttons [i] : null;
				var opt = i < currentMessage.Options.Length ? currentMessage.Options [i] : null;

				if (btn == null) {

					btn = new BlrtActionButton ("cake", OptionPressed) {
					};

					_buttons.Add (btn);
					_buttonContainer.Add (btn);
				}

				opt = BlrtNotificationHandler.GetValid (opt);

				if (opt == null) {
					btn.Hidden = true;
				} else {
					btn.Hidden = false;
					btn.Text = opt.label;
					btn.SizeToFit ();
				}
			}

			const float Margin = 10;

			nfloat h = 0;
			nfloat width = -Margin;
			int start = 0;

			for (int i = 0; currentMessage.Options != null && i <= currentMessage.Options.Length; ++i) {

				var btn = i < _buttons.Count ? _buttons [i] : null;


				if (i == currentMessage.Options.Length || (btn != null && !btn.Hidden)) {

					if (i == currentMessage.Options.Length || btn.Frame.Width + Margin + width > ContentWidth) {

						nfloat currenShift = (ContentWidth - width) * 0.5f;
						UIView last = null;

						for (int j = start; j < i; ++j) {

							var booten = j < _buttons.Count ? _buttons [j] : null;

							if (booten != null && !booten.Hidden) {							
								booten.Frame = new CGRect (
									currenShift, h, 
									booten.Frame.Width, booten.Frame.Height
								);

								currenShift += booten.Frame.Width + Margin;							
							}

							last = booten;
						}

						if (last != null)
							h += last.Frame.Height + 5;

						if (i != currentMessage.Options.Length) {

							start = i;
							width = -Margin;
						}
					}

					if (i != currentMessage.Options.Length) {
						width += btn.Frame.Width + Margin;
					}
				}
			}

			if (h > 0) {
				_buttonContainer.Frame = new CGRect (0, bottom + SpacingBetween, ContentWidth, h - 5);
				bottom = _buttonContainer.Frame.Bottom;
			}

			ContentView.Frame = new CGRect (0, 0, ContentWidth, bottom + SpacingBetween);
		}

		public void OptionPressed (object sender, EventArgs args)
		{
			if (currentMessage == null || currentMessage.Options == null)
				return;

			for (int i = 0; i < currentMessage.Options.Length; ++i) {
				if (_buttons [i] == sender)
					BlrtNotificationHandler.ProformAction (currentMessage.Options [i]);
			
			}
		}
	}
}

