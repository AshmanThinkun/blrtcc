using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using BlrtData.Contacts;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contact list controler. It represents the contact table view displayed inside the view hierarchy of "ContactsViewController"
	/// </summary>
	public partial class ContactListControler : UITableViewController
	{
		List<ContactCellViewModel> _viewModels; // all view models received
		List<ContactCellViewModel> _filteredViewModels; // includes non-hidden, valid and flagged models
		Dictionary<char, List<ContactCellViewModel>> _indexedViewModels; // indexed based on char
		List<char> _keys;
		public EventHandler<ConversationNonExistingContact> ContactSelected { get; set; }
		BlrtContactForm _contactForm = BlrtContactForm.None;

		public readonly string ConversationName;
		public readonly string ConversationId;

		public ContactListControler (string conversationName, string conversationId)
		{
			ConversationId = conversationId;
			ConversationName = conversationName;
			_viewModels = new List<ContactCellViewModel> ();

			ReloadWithSelectForm(_contactForm);
			TableView.Source = new ContactListControllerSource (this);
			TableView.TableFooterView = new UIView (new CoreGraphics.CGRect (0, 0, 0, 0));
			TableView.SeparatorColor = BlrtStyles.iOS.Gray3;
			TableView.SectionIndexBackgroundColor = UIColor.Clear;
			TableView.SectionIndexColor = BlrtStyles.iOS.GreenShadow;
		}


		public void AssignModels(List<ContactCellViewModel> viewModels)
		{
			_viewModels = viewModels;
			ReloadWithSelectForm (_contactForm);
		}

		public void ReloadWithSelectForm(BlrtContactForm form)
		{
			_contactForm = form;

			_filteredViewModels = new List<ContactCellViewModel> ();
			for (int i = 0; i < _viewModels.Count; ++i) {
				var model = _viewModels [i];
				var contact = model.Contact;
				if (contact.Hide)
					continue;
				if (contact.State.HasFlag(form) && !contact.Invalid) {
					_filteredViewModels.Add (model);
				}
			}
			ReloadIndexedTableItems ();

			InvokeOnMainThread (delegate{
				TableView.ReloadData ();
			});
		}

		public void ReloadWithSelectForm()
		{
			ReloadWithSelectForm (_contactForm);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.View.BackgroundColor = BlrtStyles.iOS.Gray2;
		}

		private void ReloadIndexedTableItems()
		{
			_indexedViewModels = new Dictionary<char, List<ContactCellViewModel>>();

			foreach (var t in _filteredViewModels) {
				var indexKey = t.UsableName.ToUpper() [0];
				if (!char.IsLetter(indexKey)) {
					indexKey = '#';
				}
				if (_indexedViewModels.ContainsKey (indexKey)) {
					_indexedViewModels[indexKey].Add(t);
				} else {
					_indexedViewModels.Add (indexKey, new List<ContactCellViewModel> {t});
				}
			}

			foreach (var item in _indexedViewModels) {
				item.Value.Sort ();
			}

			_keys = _indexedViewModels.Keys.ToList ();
			_keys.Sort (delegate(char x, char y)
				{
					if(x=='#' || y=='#')
						return y.CompareTo(x);
					return x.CompareTo(y);
				});
		}


		public int OrderPerson(BlrtContact a, BlrtContact b)
		{
			return a.UsableName.CompareTo (b.UsableName);
		}
	}
}

