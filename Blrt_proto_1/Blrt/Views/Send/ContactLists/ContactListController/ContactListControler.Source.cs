using System;
using UIKit;
using System.Linq;
using System.Collections.Generic;
using BlrtData.Contacts;
using CoreGraphics;
using BlrtData;
using System.IO;
using Foundation;
using System.Threading.Tasks;
using MessageUI;

namespace BlrtiOS.Views.Send
{
	public partial class ContactListControler: UITableViewController
	{
		class ContactListControllerSource : UITableViewSource
		{
			public const int ContactRowHeight = 60;
			ContactListControler _parent;
			UIActionSheet _emailSelections;
			UIImage _emptyImg;

			public ContactListControllerSource (ContactListControler parent)
			{
				_parent = parent;
				_emptyImg = UIImage.FromBundle("transparent_50.png");
			}

			public override nfloat GetHeightForRow (UITableView tableView, Foundation.NSIndexPath indexPath)
			{
				return ContactRowHeight;
			}

			public override nint NumberOfSections (UITableView tableView)
			{
				return _parent._keys.Count;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				var keys = _parent._keys;
				return _parent._indexedViewModels [keys [(int)section]].Count;
			}

			public override String[] SectionIndexTitles (UITableView tableView)
			{
				return _parent._keys.Select (c => c.ToString ()).ToArray ();
			}

			public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
			{
				var cell = tableView.DequeueReusableCell (ContactTableViewCell.CellIdentifier) as ContactTableViewCell;
				// if there are no cells to reuse, create a new one
				if (null == cell) {
					cell = new ContactTableViewCell (ContactTableViewCell.CellIdentifier);
					var accessoryView = new ContactCellAccessoryView ();
					accessoryView.ResendNotificationButtonTapped += async delegate { await OnResendNotificationButtonTapped (cell.IndexPath);};
					cell.AccessoryView = accessoryView;
					cell.BackgroundColor = BlrtStyles.iOS.Gray2;
				}

				var model = GetModelFor (indexPath);

				cell.TextLabel.Text = model.UsableName;
				cell.IndexPath = indexPath;
				cell.DetailTextLabel.Text = model.ContactInfoString;
				cell.ImageView.Image = _emptyImg;

				var isAdded = model.IsAdded;
				cell.AlreadyAddedTextHidden = !isAdded;

				if (isAdded) {
					cell.AlreadyAddedText = model.AlreadyAddedText;
				}

				var contact = model.Contact;
				if(contact.Avatar!=null && File.Exists(contact.Avatar.Path)){
					cell.ThumbnailLabel.Text = string.Empty;
					cell.ThumbnailLabel.ImagePath = contact.Avatar.Path;
				}else{
					cell.ThumbnailLabel.Text = BlrtUtil.GetInitials (model.UsableName);
					cell.ThumbnailLabel.ImagePath = string.Empty;
				}

				if (cell.AccessoryView is ContactCellAccessoryView) {
					var accessoryView = cell.AccessoryView as ContactCellAccessoryView;
					accessoryView?.UpdateView (model.IsSelected,
					                           model.ContactTypes,
					                           model.SendNotificationButtonHidden, 
					                           model.SendNotificationButtonTitle);
				}
				return cell;
			}

			/// <summary>
			/// Triggered when the resend notification button is tapped.
			/// </summary>
			/// <param name="sender">Sender.</param>
			/// <param name="e">E.</param>
			async Task OnResendNotificationButtonTapped (NSIndexPath indexPath)
			{
				try {
					Acr.UserDialogs.UserDialogs.Instance.ShowLoading (string.Empty, Acr.UserDialogs.MaskType.Black);
					var model = GetModelFor (indexPath);

					UIViewController notificationSenderController = null;

					switch (model.ContactInfoType) {
						case BlrtContactInfoType.Email:
							notificationSenderController = await GetEmailViewController (model.RecipientId);
							break;
						case BlrtContactInfoType.PhoneNumber:
							notificationSenderController = GetSmsViewController (model.RecipientId);
							break;
						case BlrtContactInfoType.BlrtUserId:
						case BlrtContactInfoType.FacebookId:
						case BlrtContactInfoType.Invalid: break;
					}

					if (notificationSenderController != null) {
						notificationSenderController.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;
						_parent.PresentViewController (notificationSenderController, 
						                               true,
						                               Acr.UserDialogs.UserDialogs.Instance.HideLoading);
					}
				} catch {
					Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
				}
			}

			/// <summary>
			/// Gets the email view controller.
			/// </summary>
			/// <returns>The email view controller.</returns>
			/// <param name="recipientId">Recipient identifier.</param>
			async Task<MFMailComposeViewController> GetEmailViewController (string recipientId)
			{
				var recipients = new string [] { recipientId };
				var convoName = _parent.ConversationName;
				var subject = string.Format (BlrtHelperiOS.Translate ("email_convo_url_subject", "send_screen"), convoName);
				var url = BlrtUtil.GenerateUrl (_parent.ConversationId);

				var body = await BlrtUtil.GetEmailBodyUsing (bodyTemplate: BlrtHelperiOS.Translate ("email_convo_url_message_body_html", "send_screen"),
														  	itemUrl: url,
				                                            itemName: convoName,
															forPublicItem: false,
															forEmbedCode: false);
				
				var sendEmailController = BlrtiOS.Util.BlrtUtilities.TryGetSendEmailViewController (recipients, subject, body, true);

				if (sendEmailController != null) {
					sendEmailController.Finished += delegate { _parent.DismissViewController (true, null); };
				}

				return sendEmailController;
			}

			/// <summary>
			/// Shows the sms view controller.
			/// </summary>
			/// <returns>The sms view controller.</returns>
			/// <param name="recipientId">Recipient identifier.</param>
			MFMessageComposeViewController GetSmsViewController (string recipientId)
			{
				var recipients = new string [] { recipientId };
				var convoName = _parent.ConversationName;
				var url = BlrtUtil.GenerateUrl (_parent.ConversationId);

				var bodyTemplate = BlrtHelperiOS.Translate ("message_convo_url_sms_body", "send_screen");
				var sendSmsController = BlrtiOS.Util.BlrtUtilities.TryGetSendSmsViewController (bodyTemplate, convoName, url);

				if (sendSmsController == null) {
					return null;
				}

				sendSmsController.Recipients = recipients;
				sendSmsController.Finished += delegate {
					sendSmsController.DismissViewController (true, null);
				};

				return sendSmsController;
			}

			public override bool ShouldHighlightRow (UITableView tableView, NSIndexPath rowIndexPath)
			{
				var person = GetModelFor (rowIndexPath);
				return person.IsSelectable;
			}

			public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
			{
				_parent.TableView.DeselectRow (indexPath, true);

				var person = GetModelFor (indexPath);

				var weight = -1;

				var contactInfos = new List<BlrtContactInfo> ();
				var contact = person.Contact;

				foreach (var item in contact.InfoList) {

					int tmpWeight = item.GetTypeWeight ();
					if (item.Implemented && weight <= tmpWeight) {
					
						if (weight < tmpWeight) {
							contactInfos.Clear ();
							weight = tmpWeight;
						}

						contactInfos.Add (item);
					}
				}


				if (contactInfos.Count > 1) {

					_emailSelections = new UIActionSheet (BlrtHelperiOS.Translate ("choose_contact_method", "send_screen"));
					foreach (var e in contactInfos) {
						_emailSelections.AddButton (e.ToString (contact));
					}
					_emailSelections.Add (BlrtHelperiOS.Translate ("choose_contact_method_cancel", "send_screen"));
					_emailSelections.CancelButtonIndex = contactInfos.Count;
					_emailSelections.Clicked += (object sender, UIButtonEventArgs e) => {
						var index = e.ButtonIndex;
						if (index != contactInfos.Count && index != -1) {
							var selected = contactInfos [(int)e.ButtonIndex];
							if (_parent.ContactSelected != null)
								_parent.ContactSelected (this, new ConversationNonExistingContact (selected, contact));
						}
					};

					_emailSelections.WillPresent += ChangeEmailSelections;

					_emailSelections.ShowInView (_parent.TableView);
				} else {
					if (_parent.ContactSelected != null && contactInfos.Count > 0)
						_parent.ContactSelected (this, new ConversationNonExistingContact (contactInfos [0], contact));
				}
			}

			/// <summary>
			/// Gets the model for a given indexPath.
			/// </summary>
			/// <returns>The <see cref="T:BlrtiOS.Views.Send.ContactCellViewModel"/>.</returns>
			/// <param name="indexPath">Index path.</param>
			ContactCellViewModel GetModelFor (NSIndexPath indexPath)
			{
				var keys = _parent._keys;
				return _parent._indexedViewModels [keys [indexPath.Section]].ElementAt (indexPath.Row);
			}

			private void ChangeEmailSelections (object sender, EventArgs e)
			{
				foreach (UIView subview in _emailSelections.Subviews) {
					if (subview is UIButton) {
						UIButton button = subview as UIButton;
						button.SetTitleColor (BlrtConfig.BLRT_RED, UIControlState.Normal);

						if (button.TitleLabel.Text == BlrtHelperiOS.Translate ("Cancle")) {
							button.SetTitleColor (BlrtConfig.BlrtTextGray, UIControlState.Normal);
							button.Font = BlrtConfig.NormalFont.Large;
						}
					} else if (subview is UILabel) {
						var label = subview as UILabel;
						label.Font = BlrtConfig.BoldFont.Large;
						label.TextColor = UIColor.Black;

						var labelFrame = label.Frame;
						labelFrame.Height = label.Frame.Height + 20;
						label.Frame = labelFrame;
					}
				}
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				var keys = _parent._keys;
				return keys [(int)section].ToString ();
			}
		}
	}
}

