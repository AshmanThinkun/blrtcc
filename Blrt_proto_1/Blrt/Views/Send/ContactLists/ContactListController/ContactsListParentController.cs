using System;
using System.Collections.Generic;
using AddressBook;
using UIKit;
using BlrtData.Contacts;
using CoreGraphics;
using BlrtShared;
using BlrtData;
using System.Linq;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contacts view controller. It contains the tableview to show contatcs, the search bar and filter buttons
	/// </summary>
	public class ContactsListParentController : UIViewController
	{
		//private BlrtSendToController _parent;
		private CategoryScrollView _optionSelect;
		private ContactListControler _contactPickerController;
		private UITextField _searchTextField;
		private ContactFilterListController _filterListController;
		private UIView _searchBackground;
		private UILabel _searchDescription;

		private UIView _contactsAccessDeniedView;
		private UIView _facebookAccessDeniedView;

		private const float SearchFieldMargin = 10;
		private readonly string [] CONTACT_LIST = new string [] {
			"filter_all",
			"filter_recent",
			"filter_contacts"
		};

		public event EventHandler<ConversationNonExistingContact> ContactSelected;

		List<BlrtContact> _contactList;

		AddPeopleViewController _parent; 
		public ContactsListParentController (AddPeopleViewController parent)
		{
			_parent = parent;
			EdgesForExtendedLayout = UIRectEdge.None;
			Title = BlrtHelperiOS.Translate ("select_contact_title", "send_screen");
			View.BackgroundColor = BlrtStyles.iOS.Gray2;
		}

		public void AssignContacts (List<BlrtContact> contacts)
		{
			_contactList = contacts;

			if (_contactPickerController != null && _contactList != null) {
				_contactPickerController.AssignModels (GetViewModelsForContactPicker());
			}
			if (_filterListController != null && _contactList != null) {
				_filterListController.AssignContacts (_contactList);
			}
		}

		/// <summary>
		/// Returns the view models for the contact picker.
		/// </summary>
		/// <returns>The view models for contact picker.</returns>
		List<ContactCellViewModel> GetViewModelsForContactPicker ()
		{
			var models = new List<ContactCellViewModel> ();
			var listCount = _contactList.Count;

			for (int i = 0; i < listCount; ++i) {
				var contact = _contactList [i];
				var count = contact.InfoList.Count;

				var isConnectedToBlrt = contact.IsConnectedToBlrt;

				for (int index = 0; index < count; index++) {
					var info = contact.InfoList [index];

					if (info.Implemented) {
						//var detail = contact.ContactInfoToString ();

						var isSelected = _parent.IsContactSelected (info.ContactValue);
						var isAdded = false;

						if (!isSelected) {
							isAdded = _parent.IsContactAdded (info.ContactValue);
						}

						var isEmail = info.ContactType == BlrtContactInfoType.Email;
						var isBlrtEmail = isEmail && isConnectedToBlrt;

						models.Add (new ContactCellViewModel (contact, isBlrtEmail ? BlrtContactInfoType.BlrtUserId : info.ContactType, index, isAdded, isSelected));
					}
				}
			}
			return models;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			_filterListController.View.Hidden = true;
			_contactPickerController.View.Hidden = false;
			_searchTextField.Text = "";
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			_searchTextField.ResignFirstResponder ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			var _refreshButton = new UIBarButtonItem (UIBarButtonSystemItem.Refresh);
			_refreshButton.TintColor = BlrtConfig.BLRT_RED;
			NavigationItem.RightBarButtonItem = _refreshButton;
			_refreshButton.Clicked += SafeEventHandler.FromAction (ReAssignContacts);

			_optionSelect = new CategoryScrollView (CONTACT_LIST) {
				Frame = new CGRect (0, 0, View.Bounds.Width, CategoryScrollView.SCROLLVIEW_HEIGHT),
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				ShowsHorizontalScrollIndicator = false
			};
			_optionSelect.OnOptionSelected += SafeEventHandler.FromAction (OnFilterChanged);
			View.Add (_optionSelect);



			_searchBackground = new UIView () {
				Frame = new CGRect (0, _optionSelect.Frame.Bottom, View.Bounds.Width, CategoryScrollView.SCROLLVIEW_HEIGHT * 2 + SearchFieldMargin * 2),
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				BackgroundColor = BlrtStyles.iOS.Green
			};
			View.Add (_searchBackground);

			_searchDescription = new UILabel () {
				Frame = new CGRect (SearchFieldMargin, _optionSelect.Frame.Bottom + SearchFieldMargin, View.Bounds.Width - SearchFieldMargin * 2, 20),
				Text = "Facebook friends on Blrt",
				Hidden = true,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth
			};
			View.Add (_searchDescription);

			_searchTextField = new UITextField () {
				Placeholder = BlrtHelperiOS.Translate ("select_contact_search_placeholder", "send_screen"),
				TextAlignment = UITextAlignment.Center,
				BackgroundColor = UIColor.White,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
			};


			_searchTextField.Layer.CornerRadius = 8;
			_searchTextField.Frame = new CGRect (SearchFieldMargin, _optionSelect.Frame.Bottom + SearchFieldMargin, View.Bounds.Width - SearchFieldMargin * 2, CategoryScrollView.SCROLLVIEW_HEIGHT);
			_searchTextField.EditingChanged += SafeEventHandler.FromAction (OnSearchFieldChanged);
			View.Add (_searchTextField);

			//table view
			var headerHeight = _searchTextField.Frame.Bottom + SearchFieldMargin;

			_contactPickerController = new ContactListControler (_parent.ConversationName, _parent.ConversationId);
			_contactPickerController.ContactSelected += SafeEventHandler.FromAction<ConversationNonExistingContact> (OnContactSelected);
			_contactPickerController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
			_contactPickerController.View.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			View.Add (_contactPickerController.View);
			AddChildViewController (_contactPickerController);


			_filterListController = new ContactFilterListController (_parent);
			_filterListController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
			_filterListController.View.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			_filterListController.ContactSelected += SafeEventHandler.FromAction<ConversationNonExistingContact> (OnContactSelected);
			_filterListController.View.Hidden = true;
			View.Add (_filterListController.View);
			AddChildViewController (_filterListController);

			//access denied
			const float WIDTH = 280;


			_facebookAccessDeniedView = new UIView () {
				Frame = new CGRect (0, _optionSelect.Frame.Bottom + 1, View.Bounds.Width, View.Bounds.Height - (_optionSelect.Frame.Bottom + 1)),
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Hidden = true,
				BackgroundColor = BlrtConfig.BLRT_MID_GRAY
			};
			View.Add (_facebookAccessDeniedView);



			var fbdeniedTitle = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleLeftMargin,
				Text = BlrtHelperiOS.Translate ("fb_access_denied_title", "send_screen"),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				TextAlignment = UITextAlignment.Center,
				Font = BlrtConfig.NormalFont.SubTitle,
			};
			_facebookAccessDeniedView.Add (fbdeniedTitle);

			var size = fbdeniedTitle.SizeThatFits (new CGSize (WIDTH, nfloat.MaxValue));
			fbdeniedTitle.Frame = new CGRect ((View.Frame.Width - WIDTH) * 0.5f, 40, WIDTH, size.Height);


			var fbdeniedDetails = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleLeftMargin,
				Text = BlrtHelperiOS.Translate ("fb_access_denied_detail", "send_screen"),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				TextAlignment = UITextAlignment.Center,
				Font = BlrtConfig.NormalFont.Normal,
			};
			_facebookAccessDeniedView.Add (fbdeniedDetails);

			size = fbdeniedDetails.SizeThatFits (new CGSize (WIDTH, nfloat.MaxValue));
			fbdeniedDetails.Frame = new CGRect ((View.Frame.Width - WIDTH) * 0.5f, fbdeniedTitle.Frame.Bottom + 10, WIDTH, size.Height);



			_contactsAccessDeniedView = new UIView () {
				Frame = new CGRect (0, _optionSelect.Frame.Bottom + 1, View.Bounds.Width, View.Bounds.Height - (_optionSelect.Frame.Bottom + 1)),
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Hidden = true,
				BackgroundColor = BlrtConfig.BLRT_MID_GRAY
			};
			View.Add (_contactsAccessDeniedView);

			var deniedTitle = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleLeftMargin,
				Text = BlrtHelperiOS.Translate ("contact_access_denied_title", "send_screen"),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				TextAlignment = UITextAlignment.Center,
				Font = BlrtConfig.NormalFont.SubTitle,
			};
			_contactsAccessDeniedView.Add (deniedTitle);

			size = deniedTitle.SizeThatFits (new CGSize (WIDTH, nfloat.MaxValue));
			deniedTitle.Frame = new CGRect ((View.Frame.Width - WIDTH) * 0.5f, 40, WIDTH, size.Height);


			var deniedDetails = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleLeftMargin,
				Text = BlrtHelperiOS.Translate ("contact_access_denied_detail", "send_screen"),
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap,
				TextAlignment = UITextAlignment.Center,
				Font = BlrtConfig.NormalFont.Normal,
			};
			_contactsAccessDeniedView.Add (deniedDetails);

			size = deniedDetails.SizeThatFits (new CGSize (WIDTH, nfloat.MaxValue));
			deniedDetails.Frame = new CGRect ((View.Frame.Width - WIDTH) * 0.5f, deniedTitle.Frame.Bottom + 10, WIDTH, size.Height);


			//AssignContacts (_contactList);
			ReAssignContacts ();


			UIGestureRecognizer tap = new UITapGestureRecognizer ();
			tap.AddTarget (() => {
				var v = tap.View.HitTest (tap.LocationInView (tap.View), new UIEvent ());
				if (v is UI.BlrtActionButton || v is UIButton) {
				} else {
					View.EndEditing (true);
				}
			});
			tap.CancelsTouchesInView = false;
			View.AddGestureRecognizer (tap);

		}

		/// <summary>
		/// Reassign contacts when reload is required.
		/// </summary>
		public void ReAssignContacts ()
		{
			_parent.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
		}

		void OnContactSelected (object sender, ConversationNonExistingContact e)
		{
			ContactSelected?.Invoke (this, e);
		}

		async void OnSearchFieldChanged (object sender, EventArgs e)
		{
			if (await _filterListController.Search (_searchTextField.Text)) {
				_filterListController.View.Hidden = false;
				_contactPickerController.View.Hidden = true;
				_contactsAccessDeniedView.Hidden = true;
			} else {
				_filterListController.View.Hidden = true;
				_contactPickerController.View.Hidden = false;
			}
		}

		async void OnFilterChanged (object sender, EventArgs e)
		{
			_contactsAccessDeniedView.Hidden = true;
			_facebookAccessDeniedView.Hidden = true;
			_contactPickerController.View.Hidden = !string.IsNullOrWhiteSpace (_searchTextField.Text);

			switch (_optionSelect.Selected) {
				case 0:
					_contactPickerController.ReloadWithSelectForm (BlrtContactForm.None);
					await _filterListController.ApplyFilter (BlrtContactForm.None);
					_searchDescription.Hidden = true;
					break;
				case 1:
					_contactPickerController.ReloadWithSelectForm (BlrtContactForm.Recent);
					await _filterListController.ApplyFilter (BlrtContactForm.Recent);
					_searchDescription.Hidden = true;
					break;
				case 2:
					await _filterListController.ApplyFilter (BlrtContactForm.Contacts);
					if (ABAddressBook.GetAuthorizationStatus () != ABAuthorizationStatus.Authorized) {
						_contactsAccessDeniedView.Hidden = false;
						_contactPickerController.View.Hidden = true;
					} else {
						_contactPickerController.ReloadWithSelectForm (BlrtContactForm.Contacts);
					}
					_searchDescription.Hidden = true;
					break;
				default:
					break;
			}

			// reset search section
			if (_searchDescription.Hidden) {
				_searchTextField.Frame = new CGRect (SearchFieldMargin, _optionSelect.Frame.Bottom + SearchFieldMargin, View.Bounds.Width - SearchFieldMargin * 2, CategoryScrollView.SCROLLVIEW_HEIGHT);
				var headerHeight = CategoryScrollView.SCROLLVIEW_HEIGHT * 2 + SearchFieldMargin * 2;
				_filterListController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
				_contactPickerController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
			} else {
				var lblHeight = _searchDescription.Frame.Height + SearchFieldMargin;
				_searchTextField.Frame = new CGRect (SearchFieldMargin, _optionSelect.Frame.Bottom + SearchFieldMargin + lblHeight, View.Bounds.Width - SearchFieldMargin * 2, CategoryScrollView.SCROLLVIEW_HEIGHT);
				var headerHeight = _searchTextField.Frame.Bottom + SearchFieldMargin; ;
				_filterListController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
				_contactPickerController.View.Frame = new CGRect (0, headerHeight, View.Bounds.Width, View.Bounds.Height - headerHeight);
			}
		}


		public class CategoryScrollView : UIScrollView
		{
			public const float SCROLLVIEW_HEIGHT = 30;
			public readonly float PADDING = BlrtHelperiOS.IsPhone ? 30 : 40;
			private string [] _options;
			private UIButton [] _btns;

			public event EventHandler OnOptionSelected;

			public int Selected { get; private set; }


			public CategoryScrollView (string [] options)
			{
				_options = new string [options.Length];

				for (int i = 0; i < options.Length; ++i) {
					_options [i] = BlrtHelperiOS.Translate (options [i], "send_screen");
				}

				_btns = new UIButton [options.Length];

				nfloat x = 0;
				for (int i = 0; i < options.Length; ++i) {
					var btn = new UIButton ();
					btn.SetTitle (_options [i], UIControlState.Normal);
					btn.SetTitleColor (BlrtConfig.BlrtBlack, UIControlState.Normal);
					btn.Font = BlrtStyles.iOS.CellTitleFont;

					btn.TouchUpInside += SafeEventHandler.FromAction (OnButtonPressed);


					var width = BlrtHelperiOS.GetSizeOfString (_options [i], new UIStringAttributes () { Font = btn.Font }, 200).Width;
					btn.Frame = new CGRect (x, 0, (int)(width + PADDING), SCROLLVIEW_HEIGHT);
					x = btn.Frame.Right;
					this.Add (btn);
					_btns [i] = btn;
				}
				ContentSize = new CGSize (x, SCROLLVIEW_HEIGHT);

				if (_options.Length > 0) {
					_btns [0].SetTitleColor (BlrtConfig.BLRT_RED, UIControlState.Normal);
				}
			}

			void OnButtonPressed (object sender, EventArgs e)
			{
				for (int i = 0; i < _btns.Length; ++i) {
					if (_btns [i] != sender) {
						_btns [i].SetTitleColor (BlrtConfig.BlrtBlack, UIControlState.Normal);
					} else {
						_btns [i].SetTitleColor (BlrtConfig.BLRT_RED, UIControlState.Normal);
						Selected = i;
					}
				}
				OnOptionSelected?.Invoke (this, EventArgs.Empty);
				// await System.Threading.Tasks.Task.Delay (500); why?
			}
		}
	}
}

