using System;
using System.Collections.Generic;
using System.Linq;
using BlrtData;
using BlrtData.Contacts;

namespace BlrtiOS.Views.Send
{
	public struct ContactCellViewModel : IComparable<ContactCellViewModel>
	{
		public readonly BlrtContact Contact;
		public readonly bool IsSelected;
		public readonly bool IsAdded;
		public readonly bool SendNotificationButtonHidden;
		public readonly string SendNotificationButtonTitle;
		public readonly BlrtContactInfoType ContactInfoType;
		public readonly string RecipientId;

		// text to indicate that this the contact represented by this cell has already been added to conversation
		public readonly string AlreadyAddedText;

		public bool IsSelectable {
			get {
				return !IsAdded && !IsSelected;
			}
		}

		public string UsableName {
			get {
				return Contact.UsableName;
			}
		}

		public string ContactInfoString {
			get {
				return ContactInfo.ToString ();
			}
		}

		public ConversationNonExistingContact SelectedContact {
			get {
				return new ConversationNonExistingContact (ContactInfo, Contact);
			}
		}

		readonly BlrtContactInfoType ContactType;
		public HashSet<BlrtContactInfoType> ContactTypes {
			get {
				return new HashSet<BlrtContactInfoType> {ContactType}; //BlrtUtil.GetAllContactTypesForInfoList (new List<BlrtContactInfo> {ContactInfo});
			}
		}

		readonly int blrtContactInfoIndex;

		BlrtContactInfo ContactInfo {
			get {
				try {
					return Contact.InfoList [blrtContactInfoIndex];
				} catch {
					return Contact.InfoList [blrtContactInfoIndex - 1];
				}
			}
		}

		public ContactCellViewModel (BlrtContact contact, BlrtContactInfoType contactType, int blrtContactInfoIndex, bool isAdded, bool isSelected)
		{
			ContactType = contactType;
			this.blrtContactInfoIndex = blrtContactInfoIndex;
			Contact = contact;
			IsAdded = isAdded;
			IsSelected = isSelected;
			AlreadyAddedText = "Already added";

			var contactInfo = Contact.InfoList [blrtContactInfoIndex];
			SendNotificationButtonHidden = !IsAdded || contactInfo.ContactType == BlrtContactInfoType.BlrtUserId;
				//|| Contact.InfoList.Any (info => contactInfo.ContactType == BlrtContactInfoType.BlrtUserId);

			if (SendNotificationButtonHidden) {
				SendNotificationButtonTitle = null;
				ContactInfoType = BlrtContactInfoType.Invalid;
				RecipientId = null;
			} else {
				var data = GetSendNotificationData (contactInfo);
				ContactInfoType  = data.Item1;
				RecipientId = data.Item2;
				SendNotificationButtonTitle = data.Item3;
			}
		}

		public int CompareTo (ContactCellViewModel other)
		{
			return UsableName.CompareTo (other.UsableName);
		}

		/// <summary>
		/// Gets the send notification data.
		/// </summary>
		/// <returns>The send notification data.</returns>
		/// <param name="contact">Contact.</param>
		static new Tuple<BlrtContactInfoType, string, string> GetSendNotificationData (BlrtContactInfo contactInfo)
		{
			var msgTemplate = "Send reminder {0}";
			string msg = null;
			BlrtContactInfoType? type = null;
			string recipientId = null;

			recipientId = contactInfo.ContactValue;

			var contactType = contactInfo.ContactType;

			if (contactType == BlrtContactInfoType.Email) {
				if (BlrtUtil.IsValidEmail (recipientId)) {
					msg = string.Format (msgTemplate, "email");
					type = BlrtContactInfoType.Email;
				}
			}
			else if (contactType == BlrtContactInfoType.PhoneNumber) {
				if (BlrtUtil.IsValidPhoneNumber (recipientId)) 
				{
					msg = string.Format (msgTemplate, "sms");
					type = BlrtContactInfoType.PhoneNumber;
				}
			}

			if (msg == null) {
				return null;
			}

			return new Tuple<BlrtContactInfoType, string, string> (type.Value, recipientId, msg);
		}
	}
}