﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace BlrtiOS.Views.Send
{
	public partial class ContactFilterListController
	{
		class ContactFilterListCell : UITableViewCell
		{
			private const float SUBTITLE_FONT_SIZE = 12f;
			private const float LEFT_PADDING = 16f;
			private const float RIGHT_PADDING = 5f;
			private const float TXT_IMG_SPACING = 10f;
			private const float TITLE_SUBTITLE_SPACING = 4f;
			private string _name;
			private string _value;

			private string _keyword;
			bool _keywordChanged;
			ContactCellAccessoryView _accessoryView; // it includes the logos for facebook, twitter, google and blrt

			// text to indicate that this the contact represented by this cell has already been added to conversation
			UILabel _alreadyAddedTextLabel;

			private static readonly UIStringAttributes BoldSubtitleAttr = new UIStringAttributes () {
				Font = UIFont.BoldSystemFontOfSize (SUBTITLE_FONT_SIZE),
				ForegroundColor = BlrtConfig.BlrtTextGray,
			};
			private static readonly UIStringAttributes HighlightAttr = new UIStringAttributes () {
				ForegroundColor = UIColor.Black
			};

			public ContactFilterListCell (String cellId) : base (UITableViewCellStyle.Subtitle, cellId)
			{
				_name = null;
				_value = null;
				_accessoryView = new ContactCellAccessoryView ();

				TextLabel.Font = BlrtConfig.BoldFont.Large;
				TextLabel.TextColor = BlrtConfig.BlrtTextGray;
				TextLabel.BackgroundColor = UIColor.Clear;
				TextLabel.Lines = 1;

				DetailTextLabel.Font = UIFont.SystemFontOfSize (SUBTITLE_FONT_SIZE);
				DetailTextLabel.TextColor = BlrtConfig.BlrtTextGray;
				DetailTextLabel.BackgroundColor = UIColor.Clear;
				DetailTextLabel.Lines = 1;

				AccessoryView = _accessoryView;
			}

			public string Name {
				get {
					return _name;
				}

				set {
					if (value != _name) {
						_name = value;
						SetNeedsLayout ();
					}
				}
			}

			/// <summary>
			/// Gets or sets the already added text. Message shown to indicate contact has already been added.
			/// </summary>
			/// <value>The already added text.</value>
			public string AlreadyAddedText { 
				get {
					return _alreadyAddedTextLabel?.Text ?? string.Empty;
				} 
				set {
					if (_alreadyAddedTextLabel != null && !AlreadyAddedText.Equals (value)) {
						_alreadyAddedTextLabel.Text = value;
						_alreadyAddedTextLabel.SizeToFit ();
					}
				}
			}

			/// <summary>
			/// Gets or sets a value indicating whether this
			/// <see cref="T:BlrtiOS.Views.Send.ContactFilterListController.ContactFilterListCell"/> already added text hidden.
			/// </summary>
			/// <value><c>true</c> if already added text hidden; otherwise, <c>false</c>.</value>
			public bool AlreadyAddedTextHidden { 
				get {
					return _alreadyAddedTextLabel?.Hidden ?? true;
				}
				set {
					if (AlreadyAddedTextHidden != value) {
						if (_alreadyAddedTextLabel == null) {
							_alreadyAddedTextLabel = new UILabel {
								Lines = 1,
								Font = BlrtStyles.iOS.CellDetailBoldFont,
								TextColor = BlrtStyles.iOS.GreenShadow,
								BackgroundColor = UIColor.Clear
							};
			                AddSubview (_alreadyAddedTextLabel);
						}
						_alreadyAddedTextLabel.Hidden = value;
                        SetNeedsLayout ();
					}
				}
			}

			public string Value {
				get {
					return _value;
				}

				set {
					if (value != _value) {
						_value = value;
						SetNeedsLayout ();
					}
				}
			}

			public string Keyword {
				get {
					return _keyword;
				}

				set {
					if (value != _keyword) {
						_keyword = value;
						_keywordChanged = true;
						SetNeedsLayout ();
					}
				}
			}

			public override void LayoutSubviews ()
			{
                UpdateTextLabels ();

				base.LayoutSubviews ();

				nfloat x = LayoutMargins.Left;
				nfloat width = Bounds.Width - LayoutMargins.Left - LayoutMargins.Right - AccessoryView.Frame.Width;
				nfloat boundsHeight = Bounds.Height;
				nfloat subviewsHeight = 0.0f;

				subviewsHeight += TextLabel.Frame.Height;
				subviewsHeight += DetailTextLabel.Frame.Height;

				if (!AlreadyAddedTextHidden) {
					subviewsHeight += _alreadyAddedTextLabel.Frame.Height;
				}

				var topPadding = (boundsHeight - subviewsHeight) * 0.5f;
				var y = topPadding;

				// TextLabel
				var newFrame = new CGRect (x, y, width, TextLabel.Frame.Height);

				if (!TextLabel.Frame.Equals (newFrame)) {
					TextLabel.Frame = newFrame;
				}

				// DetailTextLabel
				newFrame = new CGRect (x, TextLabel.Frame.Bottom, width, DetailTextLabel.Frame.Height);

				if (!DetailTextLabel.Frame.Equals (newFrame)) {
					DetailTextLabel.Frame = newFrame;
				}

				// alreadyAddedTextLabel
				if (!AlreadyAddedTextHidden) {
					newFrame = new CGRect (x, DetailTextLabel.Frame.Bottom, width, _alreadyAddedTextLabel.Frame.Height);
					
					if (!_alreadyAddedTextLabel.Frame.Equals (newFrame)) {
						_alreadyAddedTextLabel.Frame = newFrame;
					}
				}
			}

			/// <summary>
			/// Updates the text labels.
			/// </summary>
			void UpdateTextLabels ()
			{
				if (!Name.Equals (TextLabel.Text) || _keywordChanged) {
					_keywordChanged = false;
					NSMutableAttributedString nameStr = new NSMutableAttributedString (Name);
					if (null != Name && _keyword != null) {

						int keywordStart = Name.IndexOf (_keyword, StringComparison.CurrentCultureIgnoreCase);

						nameStr = new NSMutableAttributedString (Name);

						if (keywordStart > -1) {
							nameStr.AddAttributes (
								HighlightAttr,
								new NSRange (keywordStart, _keyword.Length)
							);
						}
					}
					TextLabel.AttributedText = nameStr;
					TextLabel.SizeToFit ();
				}

				if (!_value.Equals (DetailTextLabel.Text)) {
					NSMutableAttributedString emailStr = new NSMutableAttributedString (_value);
					if (null != _value && _keyword != null) {

						int keywordStart = _value.IndexOf (_keyword, StringComparison.CurrentCultureIgnoreCase);

						if (keywordStart > -1) {
							emailStr.AddAttributes (
								HighlightAttr,
								new NSRange (keywordStart, _keyword.Length)
							);
						}
					}
					DetailTextLabel.AttributedText = emailStr;
					DetailTextLabel.SizeToFit ();
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				if (AlreadyAddedTextHidden) {
					return base.SizeThatFits (size);
				}

				size = base.SizeThatFits (size);
				var labelHeight = _alreadyAddedTextLabel?.Frame.Height ?? 0;

				return new CGSize (size.Width, size.Height + labelHeight);
			}
		}
	}
}