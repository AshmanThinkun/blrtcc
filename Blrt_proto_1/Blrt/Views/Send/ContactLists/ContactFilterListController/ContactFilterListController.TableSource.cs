using System;
using System.Collections.Generic;
using UIKit;
using Foundation;

using BlrtData.Contacts;
using BlrtData;

namespace BlrtiOS.Views.Send
{
	public partial class ContactFilterListController
	{
		class ContactFilterListSource : UITableViewSource
		{
			const string CellIdentifier = "FilterListCell";

			List<ContactCellViewModel> _foundPersonList;
			ContactFilterListController _parent;

			public ContactFilterListSource (ContactFilterListController parent)
			{
				_parent = parent;
				_foundPersonList = null;
			}

			public List<ContactCellViewModel> Data {
				get {
					return _foundPersonList;
				}

				set {
					_foundPersonList = value;
				}
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				// NOTE: Don't call the base implementation on a Model class
				// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
				if (null != _foundPersonList) {
					return _foundPersonList.Count;
				} else {
					return 0;
				}
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				// NOTE: Don't call the base implementation on a Model class
				// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
				ContactFilterListCell cell = tableView.DequeueReusableCell (CellIdentifier) as ContactFilterListCell;
				// if there are no cells to reuse, create a new one
				if (null == cell) {
					cell = new ContactFilterListCell (CellIdentifier);
				}

				cell.ContentView.BackgroundColor = indexPath.Row != 0 ? UIColor.Clear : UIColor.White;

				if (_foundPersonList.Count == 0) {
					return cell;
				}
				var model = _foundPersonList [indexPath.Row];

				var accessoryView = cell.AccessoryView as ContactCellAccessoryView;
				accessoryView?.UpdateView (model.IsSelected, model.ContactTypes, true, null);

				cell.Name = model.UsableName;
				cell.Value = model.ContactInfoString;
				cell.Keyword = _parent._keyword;

				var isAdded = model.IsAdded;
				cell.AlreadyAddedTextHidden = !isAdded;

				if (isAdded) {
					cell.AlreadyAddedText = model.AlreadyAddedText;
				}

				cell.SetNeedsLayout ();

				return cell;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				tableView.DeselectRow (indexPath, true);
				var selection = _foundPersonList [indexPath.Row];

				if (selection.IsSelectable) {
					var selectedContact = selection.SelectedContact;
					if (_parent.ContactSelected != null) {
						_parent.ContactSelected (_parent, selectedContact);
					}
				}
			}
		}
	}
}