using System;
using System.Collections;
using System.Collections.Generic;
using AddressBook;
using System.Linq;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Text;

using BlrtData.Contacts;
using BlrtData;
using BlrtShared;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlrtiOS.Views.Send
{
	public partial class ContactFilterListController : UITableViewController
	{
		private ContactFilterListSource _source;
		public EventHandler<ConversationNonExistingContact> ContactSelected { get; set; }
		private string _keyword;

		BlrtContactForm _form;

		public List<BlrtContact> _contactList;
		AddPeopleViewController _contactHandler;

		public ContactFilterListController (AddPeopleViewController contactSelectionHandler)
		{
			_source = new ContactFilterListSource (this);
			_contactHandler = contactSelectionHandler;
			//Title = BlrtHelperiOS.Translate("autofill_title",  "send_screen");
			_contactList = new List<BlrtContact> ();
		}

		public void AssignContacts (List<BlrtContact> contacts)
		{
			_form = BlrtContactForm.None;
			_contactList = contacts;
			//Search (_keyword);
		}


		public async Task ApplyFilter (BlrtContactForm form)
		{
			_form = form;
			await Search (_keyword);
		}

		List<ContactCellViewModel> _foundPersonList;
		public List<ConversationNonExistingContact> FoundPersonList {
			get {
				return _foundPersonList.Select (t => t.SelectedContact).ToList ();
			}
		}

		/// <summary>
		/// Gets the contacts matching search keyword. Sorting precedence is as follows:
		/// 1) First name
		/// 2) Last name
		/// 3) Other
		/// </summary>
		/// <returns>The contacts matching search keyword.</returns>
		/// 
		IEnumerable<BlrtContact> GetContactsMatchingSearchKeyword ()
		{
			return _contactList
				.GroupBy (contact => {
					if (contact.Invalid || (_form != BlrtContactForm.None && !contact.State.HasFlag (_form))) {
						return 0;
					}

					var name = contact.UsableName.Trim ().ToLower ();

					if (name.IndexOf (_keyword, 0, Math.Min (name.Length, _keyword.Length), StringComparison.Ordinal) != -1) {
						return 1; // firstname
					}

					var names = name.Split (null, 2);
					if (names.Length > 1 && names [1].IndexOf (_keyword, 0, Math.Min (names [1].Length, _keyword.Length), StringComparison.Ordinal) != -1) {
						return 2; // lastname
					}

					// info list
					if (contact.InfoList != null) {
						int count = contact.InfoList.Count;
						for (int i = 0; i < count; i++) {
							if (contact.InfoList [i].ContactValue.IndexOf (_keyword, 0, Math.Min (contact.InfoList [i].ContactValue.Length, _keyword.Length), StringComparison.Ordinal) != -1) {
								return 3;
							}
						}
					}

					return 0;
				})
				.Where (grouping => grouping.Key != 0)
				.OrderBy (grouping => grouping.Key)
				.SelectMany (grouping => {
					//for the group of the last name, order the list based on the 
					//first character of the names in alphabetical order
					if (grouping.Key == 2) {
						return grouping.OrderBy (x => {
							return x.UsableName.Trim ().ToLower () [0];
						});
					}

					if (grouping.Key == 3) {
						return grouping.OrderBy (x => {
							return x.InfoList [0].ContactValue.Trim ().ToLower () [0];
						});
					}

					//for the group of first name, order the list based on the
					//character right after the keyword in alphabetical order
					return grouping.OrderBy (x => {
						var name = x.UsableName.Trim ().ToLower ();
						if (_keyword.Length >= name.Length)
							return '#';
						return name [_keyword.Length];
					});
				});
		}

		TaskCompletionSource<bool> task;
		string LastKeyword;
		public async Task <bool> Search (string keyword, bool explict = false)
		{
			_keyword = keyword?.Trim ()?.ToLower ();

			if (task != null) {
				return await task.Task;
			}

			LastKeyword = string.Empty;
			task = new TaskCompletionSource<bool> ();

			if ((null != _keyword) && (_keyword.Length > 0) && (_contactList.Count > 0)) {
				if (_foundPersonList == null)
					_foundPersonList = new List<ContactCellViewModel> ();
				else {
					_foundPersonList.Clear ();
				}

				if (!string.IsNullOrWhiteSpace (_keyword)) {

					InvokeInBackground (delegate {
						while (LastKeyword != _keyword) {
							LastKeyword = _keyword;
							_foundPersonList.Clear ();

							if (string.IsNullOrWhiteSpace(LastKeyword)) {
								break;
							}

							var contactsMatchingSearchKeyword = GetContactsMatchingSearchKeyword ();

							if (LastKeyword != _keyword) continue;

							PopulateDataSource (contactsMatchingSearchKeyword, explict);
						}
						task.SetResult (_foundPersonList.Count > 0);
					});
				} else {
					task.SetResult (false);
				}
			} else {
				task.SetResult (false);
			}

			var result = await task.Task;

			_source.Data = _foundPersonList;
			TableView.ReloadData ();

			task = null;
			return result;
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		void PopulateDataSource (IEnumerable<BlrtContact> contactsMatchingSearchKeyword, bool explict)
		{
			foreach (var contact in contactsMatchingSearchKeyword) {

				if (LastKeyword != _keyword) break;

				var nameHighLight = explict ? -1 : contact.UsableName.ToLower ().IndexOf (LastKeyword, StringComparison.Ordinal);

				var isConnectedToBlrt = contact.IsConnectedToBlrt;
				var count = contact.InfoList.Count;

				for (int index = 0; index < count; index++) {
					if (LastKeyword != _keyword) break;

					var info = contact.InfoList [index];

					if (info.Implemented) {
						var detail = contact.InfoList [index].ToString ().ToLower();

						var detailHightLight = explict ? 
							((LastKeyword == detail) ? 0 : -1) : detail.IndexOf (LastKeyword, StringComparison.Ordinal);
						
						if (nameHighLight > -1 || detailHightLight > -1) {
							var isSelected = _contactHandler.IsContactSelected (info.ContactValue);
							var isAdded = false;

							if (!isSelected) {
								isAdded = _contactHandler.IsContactAdded (info.ContactValue);
							}

							var isEmail = info.ContactType == BlrtContactInfoType.Email;
							var isBlrtEmail = isEmail && isConnectedToBlrt;

							if (LastKeyword != _keyword) break;

							_foundPersonList.Add (new ContactCellViewModel (contact, isBlrtEmail ? BlrtContactInfoType.BlrtUserId : info.ContactType, index, isAdded, isSelected));
						}
					}
				}
			}
		}

		public ConversationNonExistingContact GetContact (int row)
		{
			if (_source.Data.Count > row)
				return _source.Data [row].SelectedContact;

			return null;
		}

		// test contact generation
		private static Random _random = new Random ();
		private static Array _enumValues = Enum.GetValues (typeof (BlrtContactInfoType));
		private static BlrtContactInfoType RandomContactFrom ()
		{
			return (BlrtContactInfoType)_enumValues.GetValue (_random.Next (_enumValues.Length));
		}
		// test contact generation

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			TableView.Source = _source;
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 44;
		}
	}
}
