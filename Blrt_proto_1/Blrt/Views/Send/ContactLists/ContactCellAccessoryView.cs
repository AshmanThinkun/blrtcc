﻿using System;
using UIKit;
using System.Linq;
using System.Collections.Generic;
using BlrtData.Contacts;
using CoreGraphics;
using BlrtData;
using System.IO;
using Foundation;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Send
{
	public class ContactCellAccessoryView : UIView
	{
		static byte MARGIN = 4;

		MultiExecutionPreventor.SingleExecutionToken _multiExeToken;

		UIImageView _facebookLogo;
		UIImageView _blrtLogo;
		UIImageView _selectionTick;
		UIButton _resendNotificationButton;

		public event EventHandler ResendNotificationButtonTapped;

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.ContactCellAccessoryView"/> facebook
		/// logo hidden.
		/// </summary>
		/// <value><c>true</c> if facebook logo hidden; otherwise, <c>false</c>.</value>
		bool FacebookLogoHidden {
			get {
				return _facebookLogo?.Hidden ?? true;
			}
			set {
				if (value != FacebookLogoHidden) {
					if (_facebookLogo == null) {

						if (value) {
							return; // don't create object until hidden == false
						}

						_facebookLogo = new UIImageView (UIImage.FromBundle ("sendto_fb.png"));
						AddSubview (_facebookLogo);
					}
					_facebookLogo.Hidden = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.ContactCellAccessoryView"/> blrt logo hidden.
		/// </summary>
		/// <value><c>true</c> if blrt logo hidden; otherwise, <c>false</c>.</value>
		bool BlrtLogoHidden {
			get {
				return _blrtLogo?.Hidden ?? true;
			}
			set {
				if (value != BlrtLogoHidden) {
					if (_blrtLogo == null) {

						if (value) {
							return; // don't create object until hidden == false
						}

						_blrtLogo = new UIImageView (UIImage.FromBundle ("sendto_blrt.png"));
						AddSubview (_blrtLogo);
					}
					_blrtLogo.Hidden = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.ContactCellAccessoryView"/> selection
		/// tick hidden. It is shown when user has already selected the contact represented by the cell.
		/// </summary>
		/// <value><c>true</c> if selection tick hidden; otherwise, <c>false</c>.</value>
		bool SelectionTickHidden {
			get {
				return _selectionTick?.Hidden ?? true;
			}
			set {
				if (value != SelectionTickHidden) {
					if (_selectionTick == null) {

						if (value) {
							return; // don't create object until hidden == false
						}

						_selectionTick = new UIImageView (UIImage.FromBundle ("img_select_sm.png"));
						AddSubview (_selectionTick);
					}
					_selectionTick.Hidden = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether re-send button is hidden
		/// </summary>
		/// <value><c>true</c> if resend notification button hidden; otherwise, <c>false</c>.</value>
		bool ResendNotificationButtonHidden {
			get {
				return _resendNotificationButton?.Hidden ?? true;
			}
			set {
				if (value != ResendNotificationButtonHidden) {
					if (_resendNotificationButton == null) {

						if (value) {
							return; // don't create object until hidden == false
						}
						_resendNotificationButton = new UIButton {
							Font = BlrtStyles.iOS.CellContentFont.WithSize (12)
						};
						_resendNotificationButton.TouchUpInside += MultiExecutionPreventor.NewEventHandler(OnResendNotificationButtonTapped, _multiExeToken);
                        AddSubview (_resendNotificationButton);
					}
					_resendNotificationButton.Hidden = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the resend notification button text.
		/// </summary>
		/// <value>The resend notification button text.</value>
		string ResendNotificationButtonText {
			get {
				return _resendNotificationButton?.GetAttributedTitle (UIControlState.Normal)?.Value ?? string.Empty;
			}
			set {
				if (value != ResendNotificationButtonText) {
					if (_resendNotificationButton != null) {
						var title = new NSMutableAttributedString (value);
						title.AddAttribute (UIStringAttributeKey.UnderlineStyle, 
						                    NSNumber.FromInt32((int)NSUnderlineStyle.Single), 
						                    new NSRange (0, value.Length));
						title.AddAttribute (UIStringAttributeKey.ForegroundColor,
											BlrtStyles.iOS.AccentBlue,
											new NSRange (0, value.Length));
						_resendNotificationButton.SetAttributedTitle (title, UIControlState.Normal);
						_resendNotificationButton.SizeToFit ();
					}
				}
			}
		}

		public ContactCellAccessoryView()
		{
			_multiExeToken = MultiExecutionPreventor.NewToken ();
		}

		internal void UpdateView (bool isSelected, 
		                          HashSet<BlrtContactInfoType> contactTypes, 
		                          bool sendNotificationButtonHidden, 
		                          string sendNotificationButtonTitle)
		{
			if (isSelected) {
				SelectionTickHidden = !(ResendNotificationButtonHidden = BlrtLogoHidden = FacebookLogoHidden = true);
				return;
			}

			if (!sendNotificationButtonHidden) {
				ResendNotificationButtonHidden = !(SelectionTickHidden = BlrtLogoHidden = FacebookLogoHidden = true);
				ResendNotificationButtonText = sendNotificationButtonTitle;
				return;
			}

			SelectionTickHidden = ResendNotificationButtonHidden = true;
			bool isFacebookUser = contactTypes.Contains (BlrtContactInfoType.FacebookId);
			bool isBlrtUser = contactTypes.Contains (BlrtContactInfoType.BlrtUserId);

			FacebookLogoHidden = !isFacebookUser;
			BlrtLogoHidden = !isBlrtUser;
		}

		/// <summary>
		/// Triggered when the resend notification button is tapped.
		/// </summary>
		/// <returns>The resend notification button tapped.</returns>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async Task OnResendNotificationButtonTapped (object sender, EventArgs e)
		{
			ResendNotificationButtonTapped?.Invoke (sender, e);
		}

		/// <summary>
		/// Sets the notification receipient identifier data.
		/// </summary>
		/// <param name="recepientId">Recepient identifier.</param>
		void SetNotificationReceipientIdData (string recepientId)
		{
			BlrtContactInfoType notificationReceipientIdType;

			if (BlrtUtil.IsValidPhoneNumber (recepientId)) {
				notificationReceipientIdType = BlrtContactInfoType.PhoneNumber;
			}

			if (BlrtUtil.IsValidEmail (recepientId)) {
				notificationReceipientIdType = BlrtContactInfoType.Email;
			} else {
				notificationReceipientIdType = BlrtContactInfoType.Invalid;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			nfloat halfWidth = 0;
			nfloat halfHeight = Bounds.GetMidY ();
			nfloat right = Bounds.Right;

			if (!BlrtLogoHidden) {
				var width = _blrtLogo.Frame.Width;
				halfWidth = width * 0.5f;

				_blrtLogo.Center = new CGPoint (right - halfWidth, halfHeight);
				right -= width + MARGIN;
			}

			if (!FacebookLogoHidden) {
				halfWidth = _facebookLogo.Frame.Width * 0.5f;
				_facebookLogo.Center = new CGPoint (right - halfWidth, halfHeight);
			}

			if (!SelectionTickHidden) {
				halfWidth = _selectionTick.Frame.Width * 0.5f;
				_selectionTick.Center = new CGPoint (right - halfWidth, halfHeight);
			}

			if (!ResendNotificationButtonHidden) {
				halfWidth = _resendNotificationButton.Frame.Width * 0.5f;
				_resendNotificationButton.Center = new CGPoint (right - halfWidth, halfHeight);
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			nfloat height = 0.0f;
			nfloat width = 0.0f;

			if (!FacebookLogoHidden) {
				height = _facebookLogo.Frame.Height;
				width += _facebookLogo.Frame.Width;
			}

			if (!BlrtLogoHidden) {
				height = (nfloat)Math.Max (_blrtLogo.Frame.Height, height);

				/* 
				 * add margin if facebook button is visible. We need margin (to add spacing between the logos) only if  
				 * both the logos are visible at the same time 
				*/
				if (width > 0) {
					width += MARGIN;
				}

				width += _blrtLogo.Frame.Width;
			}

			if (!SelectionTickHidden) {
				height = _selectionTick.Frame.Height;
				width = _selectionTick.Frame.Width;
			}

			if (!ResendNotificationButtonHidden) {
				height = _resendNotificationButton.Frame.Height;
				width = _resendNotificationButton.Frame.Width;
			}

			return new CGSize (width, height);
		}
	}
}