﻿using System;
using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.Send
{
	public class EmailAddressEntry : UITextField {
		private float MIN_LENGTH = 44;
		private float PaddingRight = 10;

		public UIButton AddButton;
		public event EventHandler<int> Completed;

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.EmailAddressEntry"/> add button enabled.
		/// </summary>
		/// <value><c>true</c> if add button enabled; otherwise, <c>false</c>.</value>
		public bool AddButtonEnabled {
			get {
				return AddButton.Enabled;
			}
			set {
				if (AddButton.Enabled != value) {
					AddButton.Enabled = value;
				}
			}
		}

		public EmailAddressEntry() {

			AddButton = new UIButton (UIButtonType.ContactAdd) {
				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleBottomMargin,
				Frame = new CGRect(0 ,0, MIN_LENGTH, MIN_LENGTH),
				Enabled = false
			};

			AddButton.TintColor = BlrtConfig.BLRT_ACTION;

			AddButton.TouchUpInside += delegate {
				if (!string.IsNullOrWhiteSpace (Text)) {
					Completed?.Invoke (this, 1);
				}
			};

			AddSubview (AddButton);
		}


		public override CGRect EditingRect (CGRect forBounds)
		{
			return InsetRect(forBounds, new UIEdgeInsets(5,10,5,MIN_LENGTH + PaddingRight));
		}

		public override CGRect TextRect (CGRect forBounds)
		{
			return InsetRect(forBounds, new UIEdgeInsets(5,10,5,MIN_LENGTH + PaddingRight));
		}

		public static CGRect InsetRect(CGRect rect, UIEdgeInsets insets)
		{
			return new CGRect (rect.X + insets.Left,
				rect.Y + insets.Top,
				rect.Width - insets.Left - insets.Right,
				rect.Height - insets.Top - insets.Bottom);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var newCenter = new CGPoint (Bounds.Right - AddButton.Frame.Width * 0.5f, Bounds.GetMidY ());
			if (!newCenter.Equals (AddButton.Center)) {
				AddButton.Center = newCenter;
			}
		}

		public class EmailTextFieldDelegate: UITextFieldDelegate
		{
			public event EventHandler<bool> TextFieldEmpty;
			public event EventHandler ReturnPressed;

			public bool InterfaceOrientationChanging { get; set; }

			public override bool ShouldChangeCharacters (UITextField textField, NSRange range, string replacementString)
			{
				InvokeTextFieldEmptyEvent (textField.Text.Length, range.Length, string.IsNullOrWhiteSpace (replacementString));

				if (textField.Text.Length == 0) {
					if ((replacementString == " " || replacementString == ",")) {
						return false;
					}
				} 

				if (!string.IsNullOrEmpty(replacementString) && (replacementString == " " && textField.Text.ToCharArray().Last()==' ' || replacementString =="\n" || replacementString =="," )){
					textField.EndEditing(false);
					return false;
				}

				return true;
			}

			void InvokeTextFieldEmptyEvent (int textFieldTextLength, nint rangeLength, bool isReplacementStringEmpty)
			{
				if (!isReplacementStringEmpty || textFieldTextLength != rangeLength) {
					TextFieldEmpty?.Invoke (null, false);
				} else {
					TextFieldEmpty?.Invoke (null, true);
				}
			}

			public override bool ShouldEndEditing (UITextField textField)
			{
				var shouldEndEditing = !InterfaceOrientationChanging;

				if (shouldEndEditing) {
					TextFieldEmpty?.Invoke (textField, true);
				}

				return shouldEndEditing;
			}

			public override void EditingStarted (UITextField textField)
			{
				if (!string.IsNullOrWhiteSpace (textField.Text) && !InterfaceOrientationChanging) {
					TextFieldEmpty?.Invoke (textField, false);
				}
			}

			public override bool ShouldReturn (UITextField textField)
			{
				ReturnPressed?.Invoke (textField, null);
				return false;
			}
		}
	}
}

