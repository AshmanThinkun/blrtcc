﻿using System;
using CoreGraphics;
using UIKit;
using BlrtData;
using CoreAnimation;

namespace BlrtiOS.Views.Send
{
	public partial class AddPeopleViewController
	{
		public class ContactAddView : UIView
		{
			readonly byte PADDING = 16;
			readonly byte EMAIL_FIELD_MARGIN = 12;
			readonly byte ADD_CONTACTS_BUTTON_MARGIN = 2;
			readonly byte ERROR_VIEW_BOTTOM_MARGIN = 4;

			readonly byte EMAIL_EDIT_FIELD_HEIGHT = 42;
			readonly byte EMAIL_EDIT_FIELD_BORDER_WIDTH = 2;
			readonly byte EMAIL_EDIT_FIELD_CORNER_RADIUS = 2;

			EmailAddressEntry _emailEditField;
			UILabel _userInfoLabel;
			UIButton _viewContactsButton;
			UILabel _errorLbl;

			/// <summary>
			/// Gets the user info label text to infor user about different ways to add people
			/// </summary>
			/// <value>The user info label text.</value>
			string UserInfoLabelText { 
				get {
					return BlrtUtil.PlatformHelper.Translate ("add_people_field_info", "send_screen");
				} 
			}

			/// <summary>
			/// Gets the view contacts button title.
			/// </summary>
			/// <value>The view contacts button title.</value>
			string ViewContactsButtonTitle { 
				get {
					return BlrtUtil.PlatformHelper.Translate ("add_people_view_contacts", "send_screen");
				}  
			}

			CAShapeLayer EmailTextFieldOutline { get; set; }

			/// <summary>
			/// Occurs when editing changed in the textfield
			/// </summary>
			public event EventHandler EditingChanged {
				add {
					_emailEditField.EditingChanged += value;
				}
				remove {
					_emailEditField.EditingChanged -= value;
				}
			}

			/// <summary>
			/// Occurs when editing ended.
			/// </summary>
			public event EventHandler<int> EditingEnded;/*{
				add {
					_emailEditField.Completed += value;
				}
				remove {
					_emailEditField.Completed -= value;
				}
			}*/

			/// <summary>
			/// Gets or sets the email edit field delegate.
			/// </summary>
			/// <value>The email edit field delegate.</value>
			EmailAddressEntry.EmailTextFieldDelegate EmailEditFieldDelegate { get; set; }

			/// <summary>
			/// Gets or sets a value indicating whether the interface orientation changed.
			/// </summary>
			/// <value><c>true</c> if interface orientation changed; otherwise, <c>false</c>.</value>
			public bool InterfaceOrientationChanging { 
				get {
					return EmailEditFieldDelegate.InterfaceOrientationChanging;
				}
				set {
					if (EmailEditFieldDelegate.InterfaceOrientationChanging != value) {
						EmailEditFieldDelegate.InterfaceOrientationChanging = value;
					}
				}
			}

			/// <summary>
			/// Occurs when add contact button is pressed.
			/// </summary>
			public event EventHandler ViewContactsButtonPressed {
				add {
					_viewContactsButton.TouchUpInside += value;
				}
				remove {
					_viewContactsButton.TouchUpInside -= value;
				}
			}

			/// <summary>
			/// Gets or sets a value indicating whether this
			/// <see cref="T:BlrtiOS.Views.Send.AddPeopleViewController.ContactAddView"/> error view hidden.
			/// </summary>
			/// <value><c>true</c> if error view hidden; otherwise, <c>false</c>.</value>
			public bool ErrorViewHidden { 
				get {
					return _errorLbl.Hidden;
				} 
				internal set {
					if (_errorLbl.Hidden != value) {
						_errorLbl.Hidden = value;
						if (EmailTextFieldOutline != null) {
							EmailTextFieldOutline.StrokeColor = GetEmailFieldOutlineColor (value);
						}
						SetNeedsLayout ();
					}
				} 
			}

			/// <summary>
			/// Gets the text in the email text field.
			/// </summary>
			/// <value>The text.</value>
			public string Text { 
				get {
					return _emailEditField.Text;
				}
				internal set {
					if (!value.Equals (_emailEditField.Text)) {
						_emailEditField.Text = value;
					}
				}
			}

			public ContactAddView ()
			{
				AddEmailEditField ();
				AddUserInfoLabel ();
				AddErrorView ();
				AddViewContactButton ();
			}

			/// <summary>
			/// Adds the email edit field.
			/// </summary>
			void AddEmailEditField ()
			{
				_emailEditField = new EmailAddressEntry ()
				{
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
									AutocorrectionType = UITextAutocorrectionType.No,
									AutocapitalizationType = UITextAutocapitalizationType.None,
									Placeholder = BlrtHelperiOS.Translate ("send_to_placeholder", "send_screen"),
									Font = BlrtStyles.iOS.CellTitleFont,
									BackgroundColor = TextFieldColor,
									KeyboardType = UIKeyboardType.EmailAddress
				};

				_emailEditField.Layer.CornerRadius = EMAIL_EDIT_FIELD_CORNER_RADIUS;
				_emailEditField.Layer.AddSublayer (EmailTextFieldOutline = GetEmailTextFieldOutline ());
				_emailEditField.Delegate = EmailEditFieldDelegate = new EmailAddressEntry.EmailTextFieldDelegate ();

				_emailEditField.Completed += (sender, e) => {
					EditingEnded?.Invoke (sender, e);
				};

				EmailEditFieldDelegate.TextFieldEmpty += (sender, empty) => {
					SetContactAddButtonEnabled (!empty);
				};

				EmailEditFieldDelegate.ReturnPressed += (sender, e) => {
					EditingEnded?.Invoke (sender, 1);
				};

				AddSubview (_emailEditField);
			}

			/// <summary>
			/// Sets the contact add button enabled.
			/// </summary>
			/// <param name="enabled">If set to <c>true</c> enabled.</param>
			public void SetContactAddButtonEnabled (bool enabled)
			{
				_emailEditField.AddButtonEnabled = enabled;
			}

			/// <summary>
			/// Adds the user info label.
			/// </summary>
			void AddUserInfoLabel ()
			{
				_userInfoLabel = new UILabel {
					Font = BlrtStyles.iOS.CellTitleBoldFont,
					TextAlignment = UITextAlignment.Center,
					TextColor = BlrtStyles.iOS.Black,
					Text = UserInfoLabelText,
					Lines = 2
				};

                AddSubview (_userInfoLabel);
			}

			/// <summary>
			/// Adds the error view. It is displayed when invalid email/phone number is added.
			/// </summary>
			void AddErrorView ()
			{
				_errorLbl = new UILabel ()
				{
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
					Text = BlrtHelperiOS.Translate ("error_invalid_contact_info", "send_screen"),
					TextColor = BlrtConfig.BLRT_RED,
					Font = BlrtConfig.NormalFont.Normal,
					Hidden = true
				};
				_errorLbl.SizeToFit ();

				AddSubview (_errorLbl);
			}

			/// <summary>
			/// Adds the view contact button.
			/// </summary>
			void AddViewContactButton ()
			{
				_viewContactsButton = new UIButton (UIButtonType.System)
				{
					TintColor = BlrtStyles.iOS.AccentBlue,
					Font = BlrtStyles.iOS.CellTitleFont
				};
				_viewContactsButton.SizeToFit ();
								var size = _viewContactsButton.Frame.Size;
				var newSize = size;

				if (size.Width< 44) {
					newSize.Width = 44;
				} else if (size.Height< 44) {
					newSize.Width = 44;
				}

				if (!newSize.Equals (size)) {
					_viewContactsButton.Frame = new CGRect (_viewContactsButton.Frame.X,
															_viewContactsButton.Frame.Y,
															newSize.Width,
															newSize.Height);
				}

				_viewContactsButton.SetTitle (ViewContactsButtonTitle, UIControlState.Normal);
				_viewContactsButton.SizeToFit ();

				AddSubview (_viewContactsButton);
			}

			/// <summary>
			/// Gets the outline for email text field.
			/// </summary>
			/// <returns>The outline for email text field.</returns>
			/// <param name="frame">Frame.</param>
			CAShapeLayer GetEmailTextFieldOutline ()
			{
				return new CoreAnimation.CAShapeLayer {
					StrokeColor = GetEmailFieldOutlineColor(true),
					FillColor = null,
					LineWidth = EMAIL_EDIT_FIELD_BORDER_WIDTH,
					LineDashPattern = new Foundation.NSNumber [] { 4, 4 }
				};
			}

			/// <summary>
			/// Gets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.AddPeopleViewController.ContactAddView"/> is editing.
			/// </summary>
			/// <value><c>true</c> if is editing; otherwise, <c>false</c>.</value>
			public bool IsEditing { 
				get {
					return _emailEditField.IsEditing;
				}
			}

			/// <summary>
			/// Gets the color of the email field outline.
			/// </summary>
			/// <returns>The email field outline color.</returns>
			/// <param name="errorViewHidden">If set to <c>true</c> error view hidden.</param>
			CGColor GetEmailFieldOutlineColor (bool errorViewHidden)
			{
				return errorViewHidden ? BlrtStyles.iOS.Green.CGColor : BlrtConfig.BLRT_RED.CGColor;
			}

			/// <summary>
			/// Frame height excluding label.
			/// </summary>
			/// <returns>The height excluding label.</returns>
			internal nfloat FrameHeightExcludingLabel ()
			{
				return Bounds.Height - PADDING - _userInfoLabel.Bounds.Height;
			}

			/// <summary>
			/// Becomes the first responder.
			/// </summary>
			/// <returns><c>true</c>, if first responder was become, <c>false</c> otherwise.</returns>
			public override bool BecomeFirstResponder ()
			{
				return _emailEditField.BecomeFirstResponder ();
			}

			/// <summary>
			/// Ends the editing in email text field.
			/// </summary>
			/// <returns><c>true</c>, if editing was ended, <c>false</c> otherwise.</returns>
			/// <param name="force">If set to <c>true</c> force.</param>
			public bool EndEditing(bool force) {
				return _emailEditField.EndEditing (force);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var bounds = Bounds.Inset (PADDING, PADDING);

				// info label
				var newSize = _userInfoLabel.SizeThatFits (bounds.Size);
				var newFrame = new CGRect (bounds.X, 
				                           bounds.Y, 
				                           bounds.Width, 
				                           newSize.Height);
				
				if (!newFrame.Equals (_userInfoLabel.Frame)) {
					_userInfoLabel.Frame = newFrame;
				}


				// text field
				newFrame = new CGRect (bounds.X, 
			                           _userInfoLabel.Frame.Bottom + EMAIL_FIELD_MARGIN, 
			                           bounds.Width, EMAIL_EDIT_FIELD_HEIGHT);
				
				if (!newFrame.Equals (_emailEditField.Frame)) {
					_emailEditField.Frame = newFrame;
					var newBounds = _emailEditField.Bounds;
					EmailTextFieldOutline.Frame = newBounds;
					EmailTextFieldOutline.Path = UIBezierPath.FromRect (newBounds).CGPath;
				}

				var y = _emailEditField.Frame.Bottom + EMAIL_FIELD_MARGIN;

				//error label
				if (!ErrorViewHidden) {
					var height = _errorLbl.Frame.Height;
					newFrame = new CGRect (bounds.X, y, _errorLbl.Frame.Width, height);

					if (!_errorLbl.Frame.Equals (newFrame)) {
						_errorLbl.Frame = newFrame;
					}

					y += height + ERROR_VIEW_BOTTOM_MARGIN;
				}

				// view contacts button
				var newCenter = new CGPoint (bounds.GetMidX (),
				                             y
				                             + ADD_CONTACTS_BUTTON_MARGIN
				                             + _viewContactsButton.Frame.Height * 0.5f);

				if (!newCenter.Equals (_viewContactsButton.Center)) {
					_viewContactsButton.Center = newCenter;
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var height = _emailEditField.SizeThatFits (size).Height;
				height += _userInfoLabel.SizeThatFits (size).Height;
				height += _viewContactsButton.SizeThatFits (size).Height;
				height += PADDING * 2;
				height += EMAIL_FIELD_MARGIN * 2;
				height += ADD_CONTACTS_BUTTON_MARGIN;

				if (!ErrorViewHidden) {
					height += _errorLbl.Frame.Height;
				}

				return new CGSize(size.Width, height);
			}
		}
	}
}
