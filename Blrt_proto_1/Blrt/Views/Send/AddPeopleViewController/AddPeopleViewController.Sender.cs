﻿using System;
using System.Collections.Generic;
using BlrtData.Contacts;
using BlrtData;
using System.Threading.Tasks;
using System.Linq;
using BlrtAPI;
using BlrtData.Query;
using System.Threading;
using MessageUI;
using BlrtiOS.Util;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using UIKit;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contains all the methods related to sending
	/// </summary>
	public partial class AddPeopleViewController
	{
		public static bool isThereANewPhoneNumber;
		bool _sendingPopupHidden = true;
		UIAlertController _sendingPopup;
		UIActivityIndicatorView _indicator;

	



		public bool SendingPopupHidden {
			set {
				if (_sendingPopupHidden != value) {
					_sendingPopupHidden = value;

					if (_sendingPopup == null) {

						_sendingPopup = UIAlertController.Create (BlrtUtil.PlatformHelper.Translate ("send_progress_popup_title", "send_screen"), " ", UIAlertControllerStyle.Alert);
						_sendingPopup.AddAction (UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("Cancel"), 
						                                               UIAlertActionStyle.Default, delegate {
							_sendingPopupHidden = true;
							_indicator.StopAnimating ();
						}));

						//create an activity indicator
						_indicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
						_indicator.TranslatesAutoresizingMaskIntoConstraints = false;
						_sendingPopup.View.AddSubview (_indicator);
						_indicator.SizeToFit ();
						_indicator.BottomAnchor.ConstraintEqualTo (_sendingPopup.View.BottomAnchor, -55).Active = true;
						_indicator.LeadingAnchor.ConstraintEqualTo (_sendingPopup.View.LeadingAnchor).Active = true;
						_indicator.TrailingAnchor.ConstraintEqualTo (_sendingPopup.View.TrailingAnchor).Active = true;


						_indicator.UserInteractionEnabled = false; // required otherwise if there buttons in the UIAlertController you will not be able to press them
					}

					if (_sendingPopupHidden) {
						_indicator?.StopAnimating ();
						_sendingPopup?.DismissViewController (true, delegate { });
						_sendingPopup?.Dispose ();
						_sendingPopup = null;
						_indicator = null;
					} else {
						var topMostPresentedController = UIApplication.SharedApplication.KeyWindow.RootViewController.TopPresentedViewController ();

						_indicator.StartAnimating ();
						topMostPresentedController.
						                          PresentViewController (_sendingPopup, true, delegate {});
					}

					//BlrtData.BlrtManager.App.ShowAlert (
					//	this,
					//	new SimpleAlert (string.Format ("Remove {0}?", _name), "You can follow the subscription link to add this channel to Blrt again.", "Cancel", "Remove")
					//);
					//if (result.Success) {
					//	if ((!result.Result.Cancelled) && (0 == result.Result.Result)) {
					//		using (UserDialogs.Instance.Loading ("Unsubscribing...")) {
					//			try {
					//				var query = BlrtUnsubscribeOCListQuery.RunQuery (_id);
					//				await query.WaitAsync ();
					//			} catch { }
					//		}
					//	}
					//}
				}
			}
		}

		public static bool hasPhoneNumber;

		/// <summary>
		/// Tries to send conversation.
		/// </summary>
		/// <param name="sender">S.</param>
		/// <param name="eventArgs">E.</param>
		async void TrySendConversation (IEnumerable<ConversationNonExistingContact> contacts, 
		                                [CallerMemberName] string callingMethod = "",
										[CallerFilePath] string callingFilePath = "",
										[CallerLineNumber] int callingFileLineNumber = 0)
		{
			LLogger.WriteLine ("==ADPSL Step 6/14, TrySendConversation called: callingMethod {0}, callingFilePath = {1}, callingFileLineNumber = {2}, time = {3}=======", callingMethod, callingFilePath, callingFileLineNumber, DateTime.Now);
			if (_sendingConversation) {
				LLogger.WriteLine ("==ADPSL _sendingConversation = true, time = {0}=======", DateTime.Now);
				return;
			}

			var isContactsValid = await ValidateContacts (contacts);
			LLogger.WriteLine ("==ADPSL Step 7/14 ValidateContacts, time = {0}=======", DateTime.Now);
			_sendingConversation = true;
			contacts = contacts.Where (c => c.IsValid ());

			  hasPhoneNumber = contacts.Any (c => c.SelectedInfo.ContactType == BlrtContactInfoType.PhoneNumber);

			APIConversationUserRelationCreateResponse response;
			try {
				SendingPopupHidden = !hasPhoneNumber;

				BlrtUtil.AddSendingContactsToConversation (this, contacts, _conversation);
				LLogger.WriteLine ("==ADPSL Step 8/14 BlrtUtil.AddSendingContactsToConversation returned, time = {0}=======", DateTime.Now);

				var isConnectedToInternet = await BlrtUtilities.IsConnectedToInternet ();
				LLogger.WriteLine ("==ADPSL Step 9/14 isConnectedToInternet check, time = {0}=======", DateTime.Now);
				if (!isConnectedToInternet.Item1) {
					LLogger.WriteLine ("==ADPSL isConnectedToInternet = false, time = {0}=======", DateTime.Now);
					switch (isConnectedToInternet.Item2.Status) {
						case System.Net.WebExceptionStatus.NameResolutionFailure :
							LLogger.WriteLine ("==ADPSL BlrtUtilities.NoInternetSourceErrorMessage, time = {0}=======", DateTime.Now);
							throw new Exception (BlrtUtilities.NoInternetSourceErrorMessage);
						case System.Net.WebExceptionStatus.Timeout :
							LLogger.WriteLine ("==ADPSL BlrtUtilities.NoInternetErrorMessage, time = {0}=======", DateTime.Now);
							throw new Exception (BlrtUtilities.NoInternetErrorMessage);
						default : throw isConnectedToInternet.Item2;
					}
				}

				var isServerReachable = await BlrtUtilities.IsServerReachable ();
				LLogger.WriteLine ("==ADPSL Step 10/14, isServerReachable check, time = {0}=======", DateTime.Now);
				if (!isServerReachable.Item1) {
					LLogger.WriteLine ("==ADPSL isServerReachable = false, time = {0}=======", DateTime.Now);
					throw new Exception (BlrtUtilities.ServerUnreachableErrorMessage);
				}

				response = await BlrtUtil.TryCreateUserRelation (_conversation.ObjectId, 
				                                             _noteViewController.NoteText, 
				                                             _noteViewController.AddNoteInConvo, 
				                                             contacts);
				LLogger.WriteLine ("==ADPSL Step 11/14, BlrtUtil.TryCreateUserRelation returned, time = {0}=======", DateTime.Now);

			} catch (Exception e) {
				SendingPopupHidden = true;
				OnAPIConversationUserRelationCreateFailed (e, contacts);
				return;
			}

			try {
				//HACK, need conversation to set to dirty
				var responseObjectCount = response.Objects.Count ();
				BlrtUtil.UpdateConversationBondCount (_conversation, responseObjectCount);
				LLogger.WriteLine ("==ADPSL Step 12/14 BlrtUtil.UpdateConversationBondCount returned, time = {0}=======", DateTime.Now);

				var newObjects = await BlrtUtil.FetchNewObjectsFromParse (response);
				_conversation.LocalStorage.DataManager.AddParse (newObjects);

				if (newObjects==null || newObjects?.Count() != responseObjectCount)
				{
					throw new Exception ("Fetch Objects incomplete!");
				}

			} catch (Exception e) {
				LLogger.WriteEvent ("BlrtParseActions.SendConversationToContactInfo.FetchObjects", "failed", e.ToString ());
				Xamarin.Insights.Report (e);
				_refreshFailed = true;
			}

			_sendingConversation = false;
			SendingPopupHidden = true;
			await Task.Delay (600);
			UpdateContactsSendingStatus (contacts, response);
			LLogger.WriteLine ("==ADPSL Step 13/14 UpdateContactsSendingStatus returned, time = {0}=======", DateTime.Now);

			//BlrtManager.Query.TryRunQuery ();//what's this? TODO: insvestigate wht is it for
		}

		/// <summary>
		/// Validates all the contacts to send conversation to.
		/// </summary>
		/// <returns>The contacts.</returns>
		/// <param name="contacts">Contacts.</param>
		async Task<bool> ValidateContacts (IEnumerable<ConversationNonExistingContact> contacts)
		{
			var isContactInTextFieldValid = ValidateContactInTextField ();

			if (isContactInTextFieldValid) {

				OnUserEndEditing (this, 1);
			} else {
				_contactAddView.EndEditing (true);
			}

			var isSelectedContactsValid = ValidateSelectedContacts (contacts);

			if (!isSelectedContactsValid) {
				bool send = await AskUserToSendIfContactsInvalid ();

				if (!send) {
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Asks the user to send if contacts are invalid. Use may cancel or continue sending.
		/// </summary>
		/// <returns>The user to send if contacts invalid.</returns>
		async Task<bool> AskUserToSendIfContactsInvalid ()
		{
			return await BlrtHelperiOS.SimpleConfirmAsync (BlrtHelperiOS.Translate ("alert_send_unknown_contact_info_title", "send_screen"),
		                                 					 	BlrtHelperiOS.Translate ("alert_send_unknown_contact_info_message", "send_screen"), 
		                                                     	"Send", 
		                                                     	true,
				                                               "Cancel");
		}

		/// <summary>
		/// Validates the contact in text field. When user presses the send button contact data in text filed is also considered for validated for adding
		/// </summary>
		/// <returns><c>true</c>, if contact in text field was validated, <c>false</c> otherwise.</returns>
		bool ValidateContactInTextField ()
		{
			var contactString = _contactAddView.Text.Trim ();
			return BlrtUtil.IsValidEmail (contactString) || BlrtUtil.IsValidPhoneNumber (contactString);
		}

		/// <summary>
		/// Validates the selected contacts.
		/// </summary>
		/// <returns><c>true</c>, if selected contacts were validated, <c>false</c> otherwise.</returns>
		bool ValidateSelectedContacts (IEnumerable<ConversationNonExistingContact> selectedContacts)
		{
			foreach (var contact in selectedContacts) {
				if (!contact.IsValid ()) {
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Checks whether the contact already exists.
		/// </summary>
		/// <returns><c>true</c>, if same was ised, <c>false</c> otherwise.</returns>
		/// <param name="contacts">Contacts.</param>
		/// <param name="contact">Contact.</param>
		bool IsContactAlreadyAdded (IEnumerable<BlrtUtil.IConversationContact> contacts, BlrtUtil.IConversationContact contact)
		{
			if (contacts.Any (c => c.Equals (contact))) {
				BlrtHelperiOS.SimpleConfirmAsync (BlrtHelperiOS.Translate ("failed_already_added_title", "send_screen"),
												 BlrtHelperiOS.Translate ("failed_already_added_message", "send_screen"),
												 BlrtHelperiOS.Translate ("failed_too_many_recipents_accept", "send_screen"),
												 false);
				return true;
			}
			return false;
		}

		int retries; // number of times tried to send a conversation

		/// <summary>
		/// Triggered the API conversation user relation create failed.
		/// </summary>
		/// 
		void OnAPIConversationUserRelationCreateFailed (Exception e, IEnumerable<ConversationNonExistingContact> contacts)
		{
			LLogger.WriteLine ("==ADPSL OnAPIConversationUserRelationCreateFailed called, time = {0}=======", DateTime.Now);
			_sendingConversation = false;

			var retry = !(e.Message == APIConversationUserRelationCreateError.disabledByCreator.ToString () // disabled by user
						  || e.Message == BlrtUtilities.NoInternetSourceErrorMessage); // not connected to internet

			if (retry) {
				LLogger.WriteLine ("==ADPSL OnAPIConversationUserRelationCreateFailed called, retries = {0}, time = {1}=======", retries, DateTime.Now);
				if (retries++ < BlrtUtilities.MAX_API_CALL_RETRIES) {
					TrySendConversation (contacts);
					return;
				}
				retries = 0;
			}

			LLogger.WriteLine ("==ADPSL OnAPIConversationUserRelationCreateFailed called, retry = false, time = {0}=======", DateTime.Now);

			LLogger.WriteEvent ("==ADPSL BlrtParseActions.SendConversationToContactInfo", "failed", e.ToString ());
			Xamarin.Insights.Report (e);
			UpdateSendingContactsStatusOnUserRelationCreateFailed (contacts);
			LLogger.WriteLine ("==ADPSL UpdateSendingContactsStatusOnUserRelationCreateFailed returned, time = {0}=======", DateTime.Now);
			_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Failed;
			_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();

			//update conversation screen
			Xamarin.Forms.MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), _refreshFailed);

			InvokeOnMainThread (async delegate {
				BlrtData.ExecutionPipeline.ExecutionResult<BlrtData.ExecutionPipeline.IAlert> result = null;

				if (e.Message == APIConversationUserRelationCreateError.disabledByCreator.ToString ()) {
					result = await BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("conversation_lock_title", "conversation_screen"),
						BlrtUtil.PlatformHelper.Translate ("conversation_lock_msg", "conversation_screen"),
						BlrtUtil.PlatformHelper.Translate ("conversation_lock_accept", "conversation_screen")
					));

				} else if (e.Message == BlrtUtilities.NoInternetSourceErrorMessage) {
					result = await BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
						BlrtUtilities.NoInternetSourceErrorTitle,
						e.Message,
						BlrtUtil.PlatformHelper.Translate ("failed_to_send_accept", "send_screen")
					));
				} else if (e.Message == BlrtUtilities.NoInternetErrorMessage) {
					result = await BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
						BlrtUtilities.NoInternetErrorTitle,
						e.Message,
						BlrtUtil.PlatformHelper.Translate ("failed_to_send_accept", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("request_get_failed_retry")
					));
				} else {
					result = await BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("server_unreachable_title", "internet"),
						e.Message,
						BlrtUtil.PlatformHelper.Translate ("failed_to_send_accept", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("request_get_failed_retry")
					));
				}

				if (result != null && result.Success && !result.Result.Cancelled) {
					TrySendConversation (contacts);
				}
			});
		}

		/// <summary>
		/// Updates the sending contacts status after relation creation failed.
		/// </summary>
		void UpdateSendingContactsStatusOnUserRelationCreateFailed (IEnumerable<ConversationNonExistingContact> contacts)
		{
			var now = DateTime.Now;
			foreach (var contact in contacts) {
				var match = _conversation.SendingContacts.FirstOrDefault (x => x.SelectedInfo.Equals (contact.SelectedInfo));
				if (match != null) {
					match.Status = ContactState.FailedToSend;
					match.UpdateAt = now;
				} else {
					_conversation.SendingContacts.Add (new ContactStatus 
					{
						Status = ContactState.FailedToSend,
												UpdateAt = now,
												SelectedInfo = contact.SelectedInfo
					});
				}
			}
		}

		/// <summary>
		/// Updates the contacts sending status after attempting to send invitation.
		/// </summary>
		/// <param name="contacts">Contacts.</param>
		/// <param name="results">Results.</param>
		public void UpdateContactsSendingStatus (IEnumerable<ConversationNonExistingContact> contacts, BlrtAPI.APIConversationUserRelationCreateResponse results)
		{
			LLogger.WriteLine ("==ADPSL UpdateContactsSendingStatus called, time = {0}=======", DateTime.Now);
			//show result on send root page
			var list = new List<BlrtContact> ();
			var dictionary = new Dictionary<BlrtContactInfo, string> ();
			var nonUserPhoneNumbers = new List<BlrtContactInfo> ();

			int i = -1;
			var now = DateTime.Now;
			foreach (var contact in contacts) {
				i++;
				var result = results.RelationStatuses [i];

				if (contact.Contact.Name != null && !string.IsNullOrWhiteSpace (result.Name))
					contact.Contact.Name = result.Name;

				ContactState state;

				switch (result.Result) {
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentReachable:
					state = ContactState.ConversationSentReachable;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUnreachable:
					state = ContactState.ConversationSentUnreachable;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUser:
					state = ContactState.ConversationSentUser;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.AlreadySent:
					state = ContactState.ConversationAlreadySent;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.BlockedByUser:
					state = ContactState.Blocked;
					break;
				default:
					state = ContactState.InvalidContact;
					break;
				}

				var match = _conversation.SendingContacts.FirstOrDefault (x => x.SelectedInfo.Equals (contact.SelectedInfo));
				if (match != null) {
					match.Status = state;
					match.UpdateAt = now;
				} else {
					_conversation.SendingContacts.Add (match = new ContactStatus () {
						Status = state,
						UpdateAt = now,
						SelectedInfo = contact.SelectedInfo
					});
				}

				contact.Contact.LastUsed = now;

				list.Add (contact.Contact);
				dictionary.Add (match.SelectedInfo, state.ToString ());

				if (match.SelectedInfo.ContactType == BlrtContactInfoType.PhoneNumber && 
				    state != ContactState.Blocked &&
				    state != ContactState.InvalidContact) 
				{
					nonUserPhoneNumbers.Add (match.SelectedInfo);
				}
			}

			BlrtContactManager.SharedInstanced.Add (list);

			if (results.RelationStatuses.Any (
				x => x.Result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.BlockedByUser
				|| x.Result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.Invalid
			)) {
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Failed;
			} else {
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
			}

			BlrtData.Helpers.BlrtTrack.AddPeopleSend (dictionary);
			//update conversation screen
			Xamarin.Forms.MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), _refreshFailed);

			if (nonUserPhoneNumbers.Count > 0) {
				SendSmsToNonUsers (nonUserPhoneNumbers);
			}
		}

		/// <summary>
		/// Sends an sms to non-users.
		/// </summary>
		/// <returns>The sms to non users.</returns>
		/// <param name="recepients">Recepients.</param>
		public void SendSmsToNonUsers (List<BlrtContactInfo> recepients)
		{
			LLogger.WriteLine ("==ADPSL Step 14/14 SendSmsToNonUsers called, recipients = {0}, time = {1}=======", recepients[0], DateTime.Now);
			try {
				Acr.UserDialogs.UserDialogs.Instance.ShowLoading (string.Empty, Acr.UserDialogs.MaskType.Black);
			    
				var notificationSenderController = GetSmsViewController (recepients.Select(r => r.ContactValue).ToArray());

				if (notificationSenderController != null) {
					notificationSenderController.ModalPresentationStyle = UIKit.UIModalPresentationStyle.PageSheet;

					var topMostPresentedController = UIApplication.SharedApplication.KeyWindow.RootViewController.TopPresentedViewController();

					topMostPresentedController
						.PresentViewController (notificationSenderController, 
						                        true, 
						                        Acr.UserDialogs.UserDialogs.Instance.HideLoading);
				}
			} catch (Exception e) {
				LLogger.WriteLine ("==ADPSL SendSmsToNonUsers called, exception = {0}, time = {1}=======", e.Message, DateTime.Now);
				Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
			}
		}

		/// <summary>
		/// Gets the sms view controller.
		/// </summary>
		/// <returns>The sms view controller.</returns>
		/// <param name="recepients">Recepients.</param>
		MFMessageComposeViewController GetSmsViewController (string[] recepients)
		{
			var convoName = ConversationName;
			var url = BlrtUtil.GenerateUrl (ConversationId);

			var bodyTemplate = BlrtHelperiOS.Translate ("message_convo_url_sms_body", "send_screen");

			var sendSmsController = BlrtiOS.Util.BlrtUtilities.TryGetSendSmsViewController (bodyTemplate, convoName, url);

			if (sendSmsController == null) {
				return null;
			}

			sendSmsController.Recipients = recepients;
			sendSmsController.Finished += delegate {
				sendSmsController.DismissViewController (true, null);
			};

			return sendSmsController;
		}

		/// <summary>
		/// Fetchs the users from cloud.
		/// </summary>
		/// <param name="userIds">User identifiers.</param>
		async void FetchUsersFromCloud (List<string> userIds)
		{
			if (_fetchedUsersFromCloud != null && _fetchedUsersFromCloud.SequenceEqual (userIds)) {
				return;
			}

			_fetchedUsersFromCloud = userIds;

			try {
				await BlrtParseActions.FindUsersQuery (userIds, new CancellationTokenSource (5000).Token);
			} catch {
			}
			AddExistingContacts ();

			if (_fetchedUsersFromCloud.SequenceEqual (userIds)) {
				_fetchedUsersFromCloud = null;
			}
		}
	}
}