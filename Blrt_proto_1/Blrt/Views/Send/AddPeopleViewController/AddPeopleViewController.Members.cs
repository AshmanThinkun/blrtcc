using CoreGraphics;
using UIKit;
using System.Collections.Generic;
using BlrtData;
using BlrtiOS.View.CustomUI;
using System.Linq;
using BlrtData.Views.Send;
using Foundation;
using System;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contains all the fields and properties
	/// </summary>
	public partial class AddPeopleViewController
	{
		#region Readonly

		public static readonly UIColor TextFieldColor = BlrtStyles.iOS.White;

		#endregion

		#region Fields

		UIBarButtonItem _nextButton;
		ContactsListParentController _contactsViewController; // controller responsible to show phone and social media contacts
		ContactFilterListController _filterListview; // view showing the filtered contacts
		UINavigationController _filterListviewContainerForIPad; // for iPads ONLY
		CGRect _keyboardFrame; // used to layout other views in presense/absense of the keyboard

		UIScrollView _scrollView; // child view of this controller's View
		BlrtStackPanel _stackPanel; // child view of scroll view

		/*
		 * Following are the child views of stack panel
		*/
		AddContactTableView _tableView; // view containing contacts
		ContactAddView _contactAddView; // contains text field and other views
		UIView _topPadding; // padding at the top of the stackpanel

		List<string> _fetchedUsersFromCloud; // if user is not available locally, it can be found in this list after fetching it from the cloud.
		BlrtData.Conversation _conversation;
		ConversationContactListModel _contactsModel; // represents the data for contacts to be displyed in the tableview

		NoteViewController _noteViewController; // controller responsible to show note screen

		bool _sendingConversation; // used to track whether conversation is being sent
		bool _refreshFailed; // used to track whether refresh failed after sending conversation

		/* Following are for keyboard notifications */
		NSObject _hideKeyboardObj;
		NSObject _showWillKeyboardObj;
		NSObject _showDidKeyboardObj;

		HashSet<string> _addedContactInfoList; // info list of all the contact that have been added to conversation
		HashSet<string> _selectedContactInfoList; // info list of all the contacts that have been selected by the user to send conversation to but haven't been sent yet.

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether un-send
		/// contacts saving is required.
		/// </summary>
		/// <value><c>true</c> if un send contacts saving required; otherwise, <c>false</c>.</value>
		bool UnSendContactsSavingRequired { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether sending
		/// failed contacts saving is required.
		/// </summary>
		/// <value><c>true</c> if sending failed contacts saving required; otherwise, <c>false</c>.</value>
		bool SendingFailedContactsSavingRequired { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the contact list (shown when user taps "view contacts" button) 
		/// needs reload
		/// </summary>
		/// <value><c>true</c> if contact list reload required; otherwise, <c>false</c>.</value>
		bool ContactListReloadRequired { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether user can press next button or not
		/// </summary>
		/// <value><c>true</c> if next enabled; otherwise, <c>false</c>.</value>
		bool NextEnabled { 
			get {
				return _nextButton.Enabled;
			}
			set {
				if (_nextButton.Enabled != value) {
					_nextButton.Enabled = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.AddPeopleViewController"/> scroll to bottom.
		/// </summary>
		/// <value><c>true</c> if scroll to bottom; otherwise, <c>false</c>.</value>
		bool ScrollToBottom { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether orientation changed. 
		/// It is used to make the decision whether to animate frame change or not
		/// </summary>
		/// <value><c>true</c> if orientation changed; otherwise, <c>false</c>.</value>
		bool InterfaceOrientationChanging {
			get {
				return _contactAddView.InterfaceOrientationChanging;
			}
			set {
				if (_contactAddView.InterfaceOrientationChanging != value) {
					_contactAddView.InterfaceOrientationChanging = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.AddPeopleViewController"/> search
		/// popover hidden.
		/// </summary>
		/// <value><c>true</c> if search popover hidden; otherwise, <c>false</c>.</value>
		bool SearchPopoverHidden {
			get {
				return _filterListview?.View.Hidden ?? true;
			}
			set {
				if (_filterListview != null && value != _filterListview.View.Hidden) {
					_filterListview.View.Hidden = value;
				}
			}
		}

		/// <summary>
		/// Gets the contacts that have been added to conversation successfully..
		/// </summary>
		/// <value>The contacts.</value>
		public List<BlrtUtil.IConversationContact> ExistingContacts {
			get {
				return _contactsModel.ExistingContacts.Select (c => c as BlrtUtil.IConversationContact).ToList ();
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.AddPeopleViewController"/> is last
		/// recalulate scroll view for error view visible. It is used to determine whetehr recalculate is required or not. Edditing after 
		/// error view visible will call this method to recalculate but in that case recalculate is not required as only erro view
		/// is set to hidden and nothing else changes.
		/// </summary>
		/// <param name = "force"> force the relayout</param>
		/// <value><c>true</c> if is last recalulate scroll view for error view visible; otherwise, <c>false</c>.</value>
		bool IsLastRecalculateScrollViewForErrorViewVisible { get; set; }

		/// <summary>
		/// Gets a value indicating whether filter list is empty.
		/// </summary>
		/// <value><c>true</c> if filter list empty; otherwise, <c>false</c>.</value>
		async Task<bool> FilterListEmpty () {
			if (_filterListview == null) {
				return true;
			}
			return !await _filterListview.Search (_contactAddView?.Text?.Trim ());
		}

		/// <summary>
		/// Gets or sets a value indicating whether contact is being validated. During validation, alert might be shown. 
		/// Alert dismisses keyboard and as a result scrollview is recalculated. 
		/// To prevent scrollview recalculation, this bool is set accordingly.
		/// </summary>
		/// <value><c>true</c> if validating contact; otherwise, <c>false</c>.</value>
		bool ValidatingContact { get; set; }

		/// <summary>
		/// Gets the conversation identifier.
		/// </summary>
		/// <value>The conversation identifier.</value>
		public string ConversationId {
			get {
				return _conversation?.ObjectId ?? string.Empty;
			}
		}

		/// <summary>
		/// Gets the name of the conversation.
		/// </summary>
		/// <value>The name of the conversation.</value>
		public string ConversationName {
			get {
				return _conversation?.Name ?? string.Empty;
			}
		}
		#endregion
	}
}