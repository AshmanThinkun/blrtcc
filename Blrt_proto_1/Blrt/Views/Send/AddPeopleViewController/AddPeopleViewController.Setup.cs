using System;
using CoreGraphics;
using UIKit;
using Foundation;
using BlrtData.Contacts;
using BlrtData;
using BlrtShared;
using BlrtiOS.View.CustomUI;
using System.Linq;
using BlrtData.Views.Send;
using System.Collections.Generic;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Initial setup methods and constructors.
	/// </summary>
	public partial class AddPeopleViewController
	{
        AddPeopleViewController () { } // block empty constructor

		public AddPeopleViewController (BlrtData.Conversation conversation)
		{
			_conversation = conversation;

			if (_conversation != null && !_conversation.OnParse) {
				_refreshFailed = true;
			}

			_contactsModel = new ConversationContactListModel ();
		}

		/// <summary>
		/// Adds the existing contacts to the list
		/// </summary>
		void AddExistingContacts ()
		{
			try {
				var result = BlrtUtil.CreateConversationExistingContactsForConversation (_conversation.ObjectId);
				var existingContacts = result.Item1;
				var unfetchedUsers = result.Item2;

				_contactsModel.ExistingContacts = existingContacts;
				ReloadTableView ();

				if (unfetchedUsers.Count > 0) {
					FetchUsersFromCloud (unfetchedUsers);
				}
			} catch { }
		}

		/// <summary>
		/// Loads the failed contacts - the ones that have not been not added successfully
		/// </summary>
		void LoadFailedContacts ()
		{
			BlrtUtil.LoadFailedContactsFromConversation (_conversation, AddContact);
		}

		/// <summary>
		/// Loads the un send contacts -- Contacts that were selected were were not sent.
		/// </summary>
		void LoadUnSendContacts ()
		{
			BlrtUtil.LoadUnsendContactsToConversation (_conversation);
			var unsendContacts = _conversation.UnSendContacts;
			if (unsendContacts != null) {
				BlrtUtil.CreateContactsFromString (unsendContacts, AddContact);
			}
		}

		/// <summary>
		/// Sets the next button in nav bar
		/// </summary>
		void SetNextButton ()
		{
			_nextButton = new UIBarButtonItem (BlrtHelperiOS.Translate ("next_button", "send_screen"), UIBarButtonItemStyle.Plain, null);
			_nextButton.TintColor = UIColor.Black;
			_nextButton.Enabled = false;

			NavigationItem.RightBarButtonItem = _nextButton;

			_nextButton.Clicked += SafeEventHandler.FromAction (OnNextButtonPressed);
		}

		/// <summary>
		/// Sets the scroll view.
		/// </summary>
		void AddScrollView ()
		{
			_scrollView = new UIScrollView (new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};
			View.Add (_scrollView);
			AddScrollViewSubViews ();
		}

		/// <summary>
		/// Sets the scroll view's sub views.
		/// </summary>
		void AddScrollViewSubViews ()
		{
			_stackPanel = new BlrtStackPanel (new CGRect (0, 0, View.Frame.Width, View.Frame.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};

			_stackPanel.ContentSizeChanged += SafeEventHandler.FromAction (new Action (delegate {
				RecalculateScrollView ();
			}));

			_scrollView.Add (_stackPanel);

			AddStackpanelSubViews ();
			AddSearchPickerPopover ();
		}

		/// <summary>
		/// Adds the search picker popover.
		/// </summary>
		void AddSearchPickerPopover ()
		{
			_filterListview = new ContactFilterListController (this);
			_filterListview.ContactSelected += SafeEventHandler.FromAction<ConversationNonExistingContact> (OnFilterListEntrySelected);
			_filterListview.View.Hidden = true;

			if (BlrtHelperiOS.IsPhone) {
				_scrollView.AddSubview (_filterListview.View);
			} else {

				var viewContactsButton = new UIButton (UIButtonType.System) {
					Font = BlrtStyles.iOS.CellContentFont,
					TintColor = BlrtStyles.iOS.AccentBlue,
					HorizontalAlignment = UIControlContentHorizontalAlignment.Right
				};
				viewContactsButton.SetTitle ("View contacts", UIControlState.Normal);
				viewContactsButton.SizeToFit ();
				viewContactsButton.TouchUpInside += SafeEventHandler.FromAction (OnViewContactsButtonPressed);
				_filterListview.NavigationItem.RightBarButtonItem = new UIBarButtonItem (viewContactsButton);

				var leftBarButton = new UILabel () {
					UserInteractionEnabled = false,
					Text = BlrtHelperiOS.Translate ("autofill_title", "send_screen"),
					Font = BlrtStyles.iOS.CellContentFont,
					TextColor = BlrtStyles.iOS.Black,
					LayoutMargins = new UIEdgeInsets (0, 0, 0, 0)
				};
				leftBarButton.SizeToFit ();
				_filterListview.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (leftBarButton);
			}
		}

		/// <summary>
		/// Sets the stackpanel's sub views.
		/// </summary>
		void AddStackpanelSubViews ()
		{
			_topPadding = new UIView (new CGRect (0, 0, 10, 25));
			_stackPanel.Add (_topPadding);
			AddContactListTableView ();
			AddContactAddView ();
		}

		/// <summary>
		/// Adds the contact list table view.
		/// </summary>
		void AddContactListTableView ()
		{
			_tableView = new AddContactTableView ("Pending", true) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				ScrollEnabled = false,
				Sections = 2
			};

			_tableView.CellDeleteButtonClicked += SafeEventHandler.FromAction<NSIndexPath> (OnCellDeleteButtonClicked);
			_stackPanel.Add (_tableView);
		}

		/// <summary>
		/// Adds the contact text field.
		/// </summary>
		void AddContactAddView ()
		{
			_contactAddView = new ContactAddView ();
			var size = _contactAddView.SizeThatFits (new CGSize (_stackPanel.Bounds.Width, nfloat.MaxValue));
			_contactAddView.Frame = new CGRect (0, 0, size.Width, size.Height);

			_contactAddView.EditingChanged += SafeEventHandler.FromAction (OnEditingChanged);
			_contactAddView.EditingEnded += SafeEventHandler.FromAction<int> (OnUserEndEditing);
			_contactAddView.ViewContactsButtonPressed += SafeEventHandler.FromAction (OnViewContactsButtonPressed);
			_stackPanel.Add (_contactAddView);
		}
	}
}