﻿using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Foundation;
using BlrtData.Contacts;
using System.Threading.Tasks;
using BlrtData;
using System;
using System.Runtime.CompilerServices;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contains all the helper methods and the methods that are called more than once depending upon the state.
	/// </summary>
	public partial class AddPeopleViewController : Util.BaseUIViewController
	{
		void ResetSearchPickerViewFrame ()
		{
			if (_filterListview == null || _filterListview.View.Hidden) {
				return;
			}

			if (BlrtHelperiOS.IsPhone) {
				var offset = (_contactAddView == null ? 0 : _contactAddView.Frame.Bottom) + 5;

				var height = View.Bounds.Height - _keyboardFrame.Height - _contactAddView.FrameHeightExcludingLabel ();
				var newFrame = new CGRect (0, offset, View.Frame.Width, height);

				if (!_filterListview.View.Frame.Equals (newFrame)) {
					_filterListview.View.Frame = newFrame;
				}
			}
		}

		void RemoveKeyboardObservers ()
		{
			if (_hideKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_hideKeyboardObj);
				_hideKeyboardObj = null;
			}
			if (_showWillKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_showWillKeyboardObj);
				_showWillKeyboardObj = null;
			}
			if (_showDidKeyboardObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_showDidKeyboardObj);
				_showDidKeyboardObj = null;
			}

			_keyboardFrame = CGRect.Empty;
		}

		public async void RecalculateScrollView (bool force = false)
		{
			// for error view no animation and recalcuate is not required as nothing is added to tableview
			if (_contactAddView != null && !_contactAddView.ErrorViewHidden && !force) {
				IsLastRecalculateScrollViewForErrorViewVisible = true;
				return;
			}

			// when errorview is set to hidden after user starts editing, recalcuate is not required as nothing is added to tableview; only errorview is removed.
			if (IsLastRecalculateScrollViewForErrorViewVisible && !force) {
				IsLastRecalculateScrollViewForErrorViewVisible = false;
				return;
			}

			var size = _stackPanel.ContentSize;

			_stackPanel.Frame = new CGRect (0, 0, _stackPanel.Frame.Width, _stackPanel.ContentSize.Height);
			_scrollView.ContentSize = size;
			ResetSearchPickerViewFrame ();

			if (_contactAddView != null) {
				if (_contactAddView.IsEditing) {

					// to show newly added cell.
					var y = _tableView.Frame.Bottom - _tableView.SectionFooterHeight - AddContactTableView.ContactRowHeight - AddContactTableView.TableFooterHeight;
					if (!InterfaceOrientationChanging) {
						ScrollViewToPoint (false, new CGPoint (0, y));

						await Task.Delay (1000);
						MoveScrollViewToTop ();
					}
				} else {
					DismissPopovers ();
					MoveScrollViewToBottom ();
				}
			}
		}

		/// <summary>
		/// Checks the currently added contacts and adds the contact.
		/// </summary>
		/// <returns><c>true</c>, if contact was added, <c>false</c> otherwise.</returns>
		/// <param name="e">E.</param>
		async Task<bool> AddContact (ConversationNonExistingContact contact, bool validate = true)
		{
			var shouldAddContact = true;

			if (validate) {
				ValidatingContact = true;
				shouldAddContact = await ShouldAddContact (contact);
				ValidatingContact = false;
			}

			if (!shouldAddContact) {
				return false;
			}
			_contactsModel.AddContact (contact);

			if (_selectedContactInfoList == null) {
				_selectedContactInfoList = new HashSet<string> ();
			}

			_selectedContactInfoList.Add (contact.SelectedInfo.ContactValue);

			ReloadTableView ();
			LLogger.WriteLine ("==ADPSL Step 3/14, Contact added, time = {0}=======", DateTime.Now);
			return true;
		}

		/// <summary>
		/// Determines whether the contact should be added to the contact list 
		/// so that the conversation can be sent to him/her
		/// </summary>
		/// <returns><c>true</c>, if valid contact was ised, <c>false</c> otherwise.</returns>
		/// <param name="contact">Contact.</param>
		async Task<bool> ShouldAddContact (ConversationNonExistingContact contact)
		{
			var contacts = _contactsModel.NonExistingContacts;
			return await BlrtUtil.CheckAddPeoplePermission (_conversation, true, contacts.Count ())
									   && !IsContactAlreadyAdded (contacts, contact);
		}

		public void clearAddressField ()
		{
			SetErrorViewHidden (true);
			_contactAddView.Text = string.Empty;
			_contactAddView.SetContactAddButtonEnabled (false);
		}

		/// <summary>
		/// Sets the error view hidden.
		/// </summary>
		void SetErrorViewHidden (bool hidden)
		{
			if (_contactAddView.ErrorViewHidden != hidden) {
				_contactAddView.ErrorViewHidden = hidden;
				_contactAddView.LayoutIfNeeded ();
				_stackPanel.SetNeedsLayout ();
				_stackPanel.LayoutIfNeeded ();
			}
		}

		void ReloadTableView ()
		{
			var allContacts = _contactsModel.AllContacts;
			_tableView.Data = allContacts;
			_tableView.ReloadData ();
			ContactsChanged ();
			var frame = _tableView.Frame;

			//reset table view frame
			int allContactsCount = 0;

			foreach (var list in allContacts) {
				allContactsCount += list.Count;
			}

			if (allContactsCount > 0)
				_tableView.Frame = new CGRect (0, frame.Y, _stackPanel.Bounds.Width, AddContactTableView.ContactRowHeight * allContactsCount + _tableView.SectionFooterHeight);
			else
				_tableView.Frame = new CGRect (0, frame.Y, 0, 0);
		}

		void ContactsChanged ()
		{
			InvokeOnMainThread (delegate {
				NextEnabled = _contactsModel.NonExistingContacts.Count > 0;
			});
		}

		public ConversationNonExistingContact GetSearchResults (int row)
		{
			if (!_filterListview.View.Hidden) {
				return _filterListview.GetContact (row);
			}

			return null;
		}

		public void DismissPopovers ()
		{
			SearchPopoverHidden = true;

			if (!BlrtHelperiOS.IsPhone) {
				DismissViewController (true, null);
				_filterListviewContainerForIPad?.Dispose ();
				_filterListviewContainerForIPad = null;
				TrackPageView ();
			}
		}

		public void AssignContacts (List<BlrtContact> contacts)
		{
			_filterListview?.AssignContacts (contacts);
			_contactsViewController?.AssignContacts (contacts);
		}

		/// <summary>
		/// Saves the un send contacts to local conversation for later use.
		/// </summary>
		void SaveUnSendContacts ()
		{
			BlrtUtil.SaveUnSendContactsToLocalConversation (_conversation, _contactsModel.NonExistingContacts.Where (c => c.SendingStatus != ContactState.FailedToSend));
		}

		/// <summary>
		/// Moves the scroll view to bottom.
		/// </summary>
		void MoveScrollViewToBottom (bool scrollIfContentSizeSmallerThanScrollView = true)
		{
			if (!ScrollToBottom) {
				return;
			}

			if (_contactAddView.Frame.Bottom > _scrollView.Frame.Bottom) {
				var bottomOffset = new CGPoint (0, _scrollView.ContentSize.Height - _scrollView.Bounds.Height);
				ScrollViewToPoint (true, bottomOffset);
			} else if (scrollIfContentSizeSmallerThanScrollView) {
				ScrollViewToPoint (true, new CGPoint (0, 0));
			}
		}

		/// <summary>
		/// Moves the scroll view to top.
		/// </summary>
		void MoveScrollViewToTop (bool animate = true)
		{
			var y = _scrollView.ContentSize.Height - _contactAddView.FrameHeightExcludingLabel ();
			ScrollViewToPoint (animate, new CGPoint (0, y));
		}

		/// <summary>
		/// Scrolls the view to point.
		/// </summary>
		/// <param name="animate">If set to <c>true</c> animate.</param>
		void ScrollViewToPoint (bool animate, CGPoint point)
		{
			if (!_scrollView.ContentOffset.Equals (point)) {
				_scrollView.SetContentOffset (point, animate);
			}
		}

		/// <summary>
		/// Determines whether contact is already added to conversation. If contact is already added, "Already added" label is not displayed.
		/// </summary>
		/// <returns><c>true</c>, if contact selectable was ised, <c>false</c> otherwise.</returns>
		/// <param name="contact">Contact.</param>
		public bool IsContactAdded (string contactString)
		{
			if (_addedContactInfoList == null) {
				_addedContactInfoList = BlrtUtil.GetConversationExistingContactsAsContactInfo (_conversation);
			}
			/*
			if (contactInfoIndex == -1) {
				var paramContactInfoList = contact.InfoList.Select (info => info.ContactValue);
				return _addedContactInfoList.Intersect (paramContactInfoList).Any ();
			}
*/
			return _addedContactInfoList.Contains (contactString);
		}

		/// <summary>
		/// Determines whether the contact has been selected. If contact is already selected, "tick" is displayed.
		/// </summary>
		/// <returns><c>true</c>, if contact selected was ised, <c>false</c> otherwise.</returns>
		/// <param name="contact">Contact.</param>
		public bool IsContactSelected (string contactString)
		{
			if (_selectedContactInfoList == null || _selectedContactInfoList.Count == 0) {
				return false;
			}

			/*if (contactInfoIndex == -1) {
				var paramContactInfoList = contact.InfoList.Select (info => info.ContactValue);
				return _selectedContactInfoList.Intersect (paramContactInfoList).Any ();
			}*/

			return _selectedContactInfoList.Contains (contactString);
		}
	}
}