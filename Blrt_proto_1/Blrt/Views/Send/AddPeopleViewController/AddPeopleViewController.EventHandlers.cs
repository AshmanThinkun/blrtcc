﻿using System;
using CoreGraphics;
using UIKit;
using Foundation;
using BlrtData.Contacts;
using BlrtData;
using BlrtShared;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using static BlrtiOS.Views.Send.ContactSendingActivityViewControllerNav;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Initial setup methods
	/// </summary>
	public partial class AddPeopleViewController
	{
		void OnViewContactsButtonPressed (object sender, EventArgs e)
		{
			_contactAddView.EndEditing (true);
			DismissPopovers ();

			BlrtData.Helpers.BlrtTrack.TrackScreenView (this.ViewName + "_Contacts", false, BlrtData.Helpers.BlrtTrack.LastConvoDic);

			if (ContactListReloadRequired) {
				ContactListReloadRequired = false;
				_contactsViewController.ReAssignContacts ();
			}

			NavigationController.PushViewController (_contactsViewController, true);
		}

		/// <summary>
		/// Handles the keyboard will show.
		/// </summary>
		/// <returns>The keyboard will show.</returns>
		/// <param name="notification">Notification.</param>
		async Task KeyboardWillShow (NSNotification notification)
		{
			if (View.Window == null || InterfaceOrientationChanging || ValidatingContact) {
				return;
			}

			CGRect keyboardBounds = UIKeyboard.FrameEndFromNotification (notification);
			_keyboardFrame = View.ConvertRectFromView (keyboardBounds, View.Window);
			_keyboardFrame = View.ConvertRectToView (_keyboardFrame, View);

			await Task.Delay (100);

			MoveScrollViewToTop ();
			if (!await FilterListEmpty()) {
				SearchPopoverHidden = false;
				ResetSearchPickerViewFrame ();
			}
			_scrollView.ScrollEnabled = false;
		}

		/// <summary>
		/// Handles the keyboard will hide.
		/// </summary>
		/// <returns>The keyboard will hide.</returns>
		/// <param name="notification">Notification.</param>
		async Task KeyboardWillHide (NSNotification notification)
		{
			if (View.Window == null || InterfaceOrientationChanging || ValidatingContact) {
				return;
			}

			_keyboardFrame = CGRect.Empty;

			await Task.Delay (100);
			_scrollView.ScrollEnabled = true;
			RecalculateScrollView (true);        
		}

		/// <summary>
		/// Called when delete button inside tableview cell is clicked.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="indexPath">Index path.</param>
		void OnCellDeleteButtonClicked (object sender, NSIndexPath indexPath)
		{
			var contact = _contactsModel.NonExistingContacts [indexPath.Row];

			switch (contact.SendingStatus) {
			case ContactState.FailedToSend :
				BlrtUtil.RemoveSendingFailedContacts (false,
				                                      _conversation, 
				                                      new List<BlrtContactInfo> { contact.SelectedInfo });
				SendingFailedContactsSavingRequired = true;
				break;
			case ContactState.NotSend : 
				BlrtUtil.RemoveUnSendContactsFromConversation (_conversation, 
				                                               new System
				                                               .Collections
				                                               .Generic
				                                               .List<ConversationNonExistingContact> { contact });
				UnSendContactsSavingRequired = true;
				break;
			}

			_contactsModel.DeleteAtIndex (indexPath.Row);
			_selectedContactInfoList.Remove (contact.SelectedInfo.ContactValue);
			ContactListReloadRequired = true;

			ReloadTableView ();
		}

		/// <summary>
		/// Called when filter list entry is selected.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async void OnFilterListEntrySelected (object sender, ConversationNonExistingContact e)
		{
			if (await AddContact (e)) {
				ContactListReloadRequired = UnSendContactsSavingRequired = true;

				if (sender == _filterListview) {
					clearAddressField ();
				}
				DismissPopovers ();
			}
		}

		/// <summary>
		/// Called when user end editing.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async void OnUserEndEditing (object sender, int e)
		{
			//return;
			if (string.IsNullOrWhiteSpace (_contactAddView.Text))
				return;

			DismissPopovers ();

			if (e == 1 && !(BlrtUtil.IsValidEmail (_contactAddView.Text.Trim ()) || BlrtUtil.IsValidPhoneNumber (_contactAddView.Text.Trim ()))) {
				//user confirmed with press "Enter" on keyboard, in this case if we can found a result from the suggesstions, we should use it
				ConversationNonExistingContact se;
				var email = _contactAddView.Text.Trim ();
				if (await _filterListview.Search (email, false)) {
					_contactAddView.Text = string.Empty;
					se = _filterListview.FoundPersonList.First ();
					AddContact (se).ContinueWith ((isAdded) => {
						if (isAdded.Result) {
							ContactListReloadRequired = UnSendContactsSavingRequired = true;
						}
					}).FireAndForget ();
					_contactAddView.BecomeFirstResponder ();
					return;
				}
			}

			//the text filed unfocused
			if (!BlrtUtil.IsValidEmail (_contactAddView.Text.Trim ()) && !BlrtUtil.IsValidPhoneNumber (_contactAddView.Text.Trim ())) {
				//if the string is not a valid email or phone number, we should say invalid
				SetErrorViewHidden (false);
			} else {
				ConversationNonExistingContact se;
				var emailOrPhone = _contactAddView.Text.Trim ();
				_contactAddView.Text = string.Empty;

				//if this is a contact has exactly the same value, we should use it, otherwise, we should contruct new contact from the string.
				if (await _filterListview.Search (emailOrPhone, true)) {
					se = _filterListview.FoundPersonList.First ();
				} else {
					se = ConversationNonExistingContact.FromString (emailOrPhone);
				};
                AddContact (se).ContinueWith ((isAdded) => {
					if (isAdded.Result) {
						ContactListReloadRequired = UnSendContactsSavingRequired = true;
						InvokeOnMainThread (delegate {
							_contactAddView.SetContactAddButtonEnabled (false);
						});
					}
				}).FireAndForget ();
			}
		}

		/// <summary>
		/// Called when the text changed in the text field.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async void OnEditingChanged (object sender, EventArgs e)
		{
			SetErrorViewHidden (true);
			if (!await FilterListEmpty()) {
				SearchPopoverHidden = false;

				if (BlrtHelperiOS.IsPhone) {
					ResetSearchPickerViewFrame ();
				} else {

					_filterListviewContainerForIPad = new UINavigationController (_filterListview);
					_filterListviewContainerForIPad.ModalPresentationStyle = UIModalPresentationStyle.Popover;
					PresentViewController (_filterListviewContainerForIPad, true, null);

					var popoverPresentationController = _filterListviewContainerForIPad.PopoverPresentationController;
					if (popoverPresentationController != null) {
						var s = sender as UIView;
						popoverPresentationController.SourceView = s;
						popoverPresentationController.SourceRect = new CGRect (0, 0, s.Frame.Width, s.Frame.Height);
						popoverPresentationController.PermittedArrowDirections = UIPopoverArrowDirection.Up;
					}
				}
			} else {
				DismissPopovers ();
			}

			ContactsChanged ();
		}

		/// <summary>
		/// Called when contacts changed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="args">Arguments.</param>
		void OnContactsChanged (object sender, EventArgs args)
		{
			try {
				ContactsChanged ();
				Task.Run (delegate {
                    AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
				});
			} catch { }
		}

		/// <summary>
		/// Called when the next button pressed after adding people to send conversation to.
		/// </summary>
		/// <param name="sender>sender</param>
		/// <param name="eventArgs">event args</param>
		void OnNextButtonPressed (object sender, EventArgs eventArgs)
		{
			if (!BlrtiOS.Util.BlrtUtilities.IsConnectedToInternetSource) {
				BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
					BlrtiOS.Util.BlrtUtilities.NoInternetSourceErrorTitle,
					BlrtiOS.Util.BlrtUtilities.NoInternetSourceErrorMessage,
					BlrtUtil.PlatformHelper.Translate ("failed_to_send_accept", "send_screen")
				)).FireAndForget ();
			} else {
				SaveUnSendContacts ();

				if (_noteViewController == null) {
					_noteViewController = new NoteViewController ();
					_noteViewController.SendPressed += SafeEventHandler.FromAction (OnSendButtonClicked);
				}
				NavigationController.PushViewController (_noteViewController, true);
			}
			LLogger.WriteLine ("==ADPSL Step 4/14, OnNextButtonPressed, time = {0}=======", DateTime.Now);
		}

		ContactSendingActivityViewControllerNav _contactSendingActivityViewControllerNav;

		/// <summary>
		/// Called when the send button clicked to send the conversation to contacts
		/// </summary>
		void OnSendButtonClicked ()
		{
			LLogger.WriteLine ("==ADPSL Step 5/14, OnSendButtonClicked, time = {0}=======", DateTime.Now);
			_contactSendingActivityViewControllerNav = new ContactSendingActivityViewControllerNav (ExistingContacts, _conversation);
			_contactSendingActivityViewControllerNav.RetrySendContacts += OnRetrySendContacts;
			_contactSendingActivityViewControllerNav.RemindSmsPromptEvent += OnRemindPromptEvent;
			PresentViewController (_contactSendingActivityViewControllerNav, true, OnActivityViewDidAppear);
		}

		protected virtual void OnRemindPromptEvent( object sender, EventArgs e )
		{
			TrySendConversation (_contactsModel.NonExistingContacts.AsEnumerable ());
		}

		/// <summary>
		/// Called when retry of contact-sending is required.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="contacts">Contacts.</param>
		void OnRetrySendContacts (object sender, List<ConversationNonExistingContact> contacts)
		{
			TrySendConversation (contacts);
		}

		/// <summary>
		/// Called when activity view has appeared.
		/// </summary>
		void OnActivityViewDidAppear ()
		{
			BlrtUtil.RemoveUnSendContactsFromConversation (_conversation, _contactsModel.NonExistingContacts);
			NavigationController.PopToRootViewController (false);
            TrySendConversation (_contactsModel.NonExistingContacts.AsEnumerable ());
		}
	}
}