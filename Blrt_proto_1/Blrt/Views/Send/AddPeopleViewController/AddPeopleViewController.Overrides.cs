﻿using CoreGraphics;
using UIKit;
using Foundation;
using BlrtData.Contacts;
using BlrtShared;
using System.Threading.Tasks;
using System;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Overrides the UIVIewController methods
	/// </summary>
	public partial class AddPeopleViewController
	{
		public override string ViewName { get { return "Conversation_Send_Add"; } }

		public override void TrackPageView ()
		{
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, BlrtData.Helpers.BlrtTrack.LastConvoDic);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = BlrtStyles.iOS.Gray2;

			// to prevent scrollview animation during initial setup
			ScrollToBottom = false;

			EdgesForExtendedLayout = UIRectEdge.None;
			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			Title = BlrtHelperiOS.Translate ("send_people_title", "send_screen");

			View.AddGestureRecognizer (GetTapGestureRecognizer());
			View.AddGestureRecognizer (GetSwipeGestureRecognizer());

			SetNextButton ();
			AddScrollView ();

			_contactsViewController = new ContactsListParentController (this);
			_contactsViewController.ContactSelected += SafeEventHandler.FromAction<ConversationNonExistingContact> ((sender, e) => {
				OnFilterListEntrySelected (sender, e);
				NavigationController.PopViewController (true);
			});

            AddExistingContacts ();
			LoadFailedContacts ();
			LoadUnSendContacts ();

			ContactListReloadRequired = true; // for the first time contact list needs to be loaded. For subsequent time it will only be loaded if something changes in the app.

			Task.Run (delegate {
				if (BlrtContactManager.SharedInstanced.ShouldLoadContacts) {
					ContactManagerIOS.RefreshContacts (delegate {
                        AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
					});
				} else {
                    AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
				}
			});
		}

		/// <summary>
		/// Gets the tap gesture recognizer.
		/// </summary>
		/// <returns>The tap gesture recognizer.</returns>
		UIGestureRecognizer GetTapGestureRecognizer ()
		{
			UIGestureRecognizer tap = new UITapGestureRecognizer {
				CancelsTouchesInView = false
			};

			tap.AddTarget (new Action (delegate { 
				OnGestureRecognized (tap); 
			}));

			return tap;
		}

		/// <summary>
		/// Gets the swipe gesture recognizer.
		/// </summary>
		/// <returns>The swipe gesture recognizer.</returns>
		UIGestureRecognizer GetSwipeGestureRecognizer ()
		{
			UIGestureRecognizer swipe = new UISwipeGestureRecognizer {
				Direction = UISwipeGestureRecognizerDirection.Down,
				CancelsTouchesInView = false
			};

			swipe.AddTarget (new Action (delegate { 
				OnGestureRecognized (swipe); 
			}));

			return swipe;
		}

		/// <summary>
		/// Triggered when either swipe or tap gesture is recognized.
		/// </summary>
		/// <param name="gesture">Gesture.</param>
		void OnGestureRecognized (UIGestureRecognizer gesture)
		{
			var point = gesture.LocationInView (gesture.View);
			var view = gesture.View.HitTest (point, null);

			if (view is UIButton || view.IsDescendantOfView (_filterListview.View)) {
				// button will be either delete button or add-contact button
				// descendent of filter list
				//do nothing
			} else {
				_contactAddView.EndEditing (false);
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			RemoveKeyboardObservers ();
			_hideKeyboardObj = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, SafeEventHandler.Actionize<NSNotification> (KeyboardWillHide));
			_showWillKeyboardObj = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, SafeEventHandler.Actionize<NSNotification> (KeyboardWillShow));

			BlrtContactManager.SharedInstanced.Changed += OnContactsChanged;

			/*Task.Run (delegate {
                AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
			});*/
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			RemoveKeyboardObservers ();

			BlrtContactManager.SharedInstanced.Changed -= OnContactsChanged;

			if (UnSendContactsSavingRequired) {
				UnSendContactsSavingRequired = false;
				SaveUnSendContacts ();
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			if (!ScrollToBottom) {
				ScrollToBottom = true;
				// if add view is outside of view, move it in view
				MoveScrollViewToBottom (false);
			}

			LLogger.WriteLine ("==ADPSL Step 2/14, Add-people screen did appear, time = {0}=======", DateTime.Now);
		}

		public override void ViewWillTransitionToSize (CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			base.ViewWillTransitionToSize (toSize, coordinator);

			InterfaceOrientationChanging = true;

			var tempUnSendContactsSavingRequired = UnSendContactsSavingRequired;
			UnSendContactsSavingRequired = false;

			var tempSendingFailedContactsSavingRequired = SendingFailedContactsSavingRequired;
			SendingFailedContactsSavingRequired = false;

			coordinator.AnimateAlongsideTransition (delegate { }, delegate {
				InterfaceOrientationChanging = false;
				UnSendContactsSavingRequired = tempUnSendContactsSavingRequired;
				SendingFailedContactsSavingRequired = tempSendingFailedContactsSavingRequired;

				var popoverPresentationController = _filterListviewContainerForIPad?.PopoverPresentationController;
				if (popoverPresentationController != null) {
					var s = popoverPresentationController?.SourceView;
					if (s != null) {
						popoverPresentationController.SourceRect = new CGRect (0, 0, s.Frame.Width, s.Frame.Height);
					}
				}
			});
		}
	}
}