using BlrtData.Contacts;
using BlrtiOS.Views.Conversation.Cells;
using System;
using UIKit;
using CoreGraphics;
using BlrtData;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contact table view cell.
	/// </summary>
	public class ContactTableViewCell : UITableViewCell
	{
		public static readonly string CellIdentifier = "ContactTableViewCell";

		public const int ThumbnailHeight = 50;

		// text to indicate that this the contact represented by this cell has already been added to conversation
		UILabel _alreadyAddedTextLabel;

		public UserThumbnailView ThumbnailLabel {
			get {
				return _userThumb;
			}
		}

		/// <summary>
		/// Gets or sets the index.
		/// </summary>
		/// <value>The index.</value>
		public Foundation.NSIndexPath IndexPath { get; internal set; }

		public object Model { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this
		/// <see cref="T:BlrtiOS.Views.Send.ContactFilterListController.ContactFilterListCell"/> already added text hidden.
		/// </summary>
		/// <value><c>true</c> if already added text hidden; otherwise, <c>false</c>.</value>
		public bool AlreadyAddedTextHidden {
			get {
				return _alreadyAddedTextLabel?.Hidden ?? true;
			}
			set {
				if (AlreadyAddedTextHidden != value) {
					if (_alreadyAddedTextLabel == null) {
						_alreadyAddedTextLabel = new UILabel {
							Lines = 1,
							Font = BlrtStyles.iOS.CellDetailBoldFont,
							TextColor = BlrtStyles.iOS.GreenShadow,
							BackgroundColor = UIColor.Clear
						};
						AddSubview (_alreadyAddedTextLabel);
					}
					_alreadyAddedTextLabel.Hidden = value;
					SetNeedsLayout ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the already added text. Message shown to indicate contact has already been added.
		/// </summary>
		/// <value>The already added text.</value>
		public string AlreadyAddedText {
			get {
				return _alreadyAddedTextLabel?.Text ?? string.Empty;
			}
			set {
				if (_alreadyAddedTextLabel != null && !AlreadyAddedText.Equals (value)) {
					_alreadyAddedTextLabel.Text = value;
					_alreadyAddedTextLabel.SizeToFit ();
				}
			}
		}

		UserThumbnailView _userThumb;
		public ContactTableViewCell (string CellIdentifier) : base (UITableViewCellStyle.Subtitle, CellIdentifier)
		{
			_userThumb = new UserThumbnailView () {
				Frame = new CGRect (20, (AddContactTableView.ContactRowHeight - ThumbnailHeight) * 0.5f, ThumbnailHeight, ThumbnailHeight),
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin,
				BackgroundColor = BlrtStyles.iOS.Green,
				Font = BlrtStyles.iOS.CellTitleFont
			};
			AddSubview (_userThumb);

			DetailTextLabel.TextColor = BlrtStyles.iOS.Gray5;
			var detailFrame = DetailTextLabel.Frame;

			_userThumb.Clicked += OpenProfile;

			ImageView.Image = UIImage.FromFile ("transparent_50.png");
		}

		public void OpenProfile (object sender, EventArgs e)
		{
			if (Model != null) {
				if (Model is BlrtContact) {
					BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), Model as BlrtContact);
				} else if (Model is BlrtUser) {
					BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), Model as BlrtUser);
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			var margins = LayoutMargins;

			var accessoryViewSize = AccessoryView.SizeThatFits (new CGSize (nfloat.MaxValue, AddContactTableView.ContactRowHeight));
			var accessoryViewFrame = new CGRect (Bounds.Width - accessoryViewSize.Width - margins.Right,
			                                     (AddContactTableView.ContactRowHeight - accessoryViewSize.Height) * 0.5f,
			                                     accessoryViewSize.Width,
			                                     accessoryViewSize.Height);
			
			if (!accessoryViewFrame.Equals (AccessoryView.Frame)) {
				AccessoryView.Frame = accessoryViewFrame;
			}

			nfloat x = TextLabel.Frame.Left;
			nfloat width = Bounds.Width - x - LayoutMargins.Right - AccessoryView.Frame.Width;
			nfloat boundsHeight = Bounds.Height;
			nfloat subviewsHeight = 0.0f;

			subviewsHeight += TextLabel.Frame.Height;
			subviewsHeight += DetailTextLabel.Frame.Height;

			if (!AlreadyAddedTextHidden) {
				subviewsHeight += _alreadyAddedTextLabel.Frame.Height;
			}

			var topPadding = (boundsHeight - subviewsHeight) * 0.5f;
			var y = topPadding;

			// TextLabel
			var newFrame = new CGRect (x, y, width, TextLabel.Frame.Height);

			if (!TextLabel.Frame.Equals (newFrame)) {
				TextLabel.Frame = newFrame;
			}

			// DetailTextLabel
			newFrame = new CGRect (x, TextLabel.Frame.Bottom, width, DetailTextLabel.Frame.Height);

			if (!DetailTextLabel.Frame.Equals (newFrame)) {
				DetailTextLabel.Frame = newFrame;
			}

			// alreadyAddedTextLabel
			if (!AlreadyAddedTextHidden) {
				newFrame = new CGRect (x, DetailTextLabel.Frame.Bottom, width, _alreadyAddedTextLabel.Frame.Height);
				
				if (!_alreadyAddedTextLabel.Frame.Equals (newFrame)) {
					_alreadyAddedTextLabel.Frame = newFrame;
				}
			}
		}
	}
}