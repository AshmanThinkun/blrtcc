using System;
using UIKit;
using CoreGraphics;
using BlrtData;

namespace BlrtiOS.Views.Send
{
	public partial class AddContactTableView
	{
		/// <summary>
		/// Contact accessory view. It shows either cross button or user type or activity indicator
		/// </summary>
		public class AddContactAccessoryView : UIView
		{
			readonly byte TYPE_LABEL_LEFT_MARGIN = 12;
			readonly byte DELETE_BTN_SIZE = 50;

			UIButton _deleteBtn; // to remove a contact
			UITextView _contactStatusLabel; // to how user type
			UIActivityIndicatorView _sendingIndicatorView;

			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.ContactCell.ContactAccessoryView"/>
			/// delete button hidden.
			/// </summary>
			/// <value><c>true</c> if delete button hidden; otherwise, <c>false</c>.</value>
			bool DeleteBtnHidden {
				get {
					return _deleteBtn.Hidden;
				}
				set {
					if (_deleteBtn.Hidden != value) {
						_deleteBtn.Enabled = !(_deleteBtn.Hidden = value);
					}
				}
			}

			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="T:BlrtiOS.Views.Send.ContactCell.ContactAccessoryView"/>
			/// user type label hidden.
			/// </summary>
			/// <value><c>true</c> if user type label hidden; otherwise, <c>false</c>.</value>
			bool ContactStatusLabelHidden {
				get {
					return _contactStatusLabel.Hidden;
				}
				set {
					if (_contactStatusLabel.Hidden != value) {
						_contactStatusLabel.Hidden = value;
					}
				}
			}

			/// <summary>
			/// Gets or sets a value indicating whether contact sending is in progress
			/// </summary>
			/// <value><c>true</c> if sending indicator view hidden; otherwise, <c>false</c>.</value>
			bool SendingIndicatorViewHidden {
				get {
					return _sendingIndicatorView.Hidden;
				}
				set {
					if (_sendingIndicatorView.Hidden != value) {
						_sendingIndicatorView.Hidden = value;
						if (value) {
							_sendingIndicatorView.StopAnimating ();
						} else {
							_sendingIndicatorView.StartAnimating ();
						}
					}
				}
			}

			/// <summary>
			/// Gets or sets the user type label text.
			/// </summary>
			/// <value>The user type label text.</value>
			string ContactStausText {
				get {
					return _contactStatusLabel.Text;
				}
				set {
					if (_contactStatusLabel.Text == null || !_contactStatusLabel.Text.Equals (value)) {
						_contactStatusLabel.Text = value;
					}
				}
			}

			AddContactAccessoryView ()
			{
				_deleteBtn = new UIButton () {
					Frame = new CGRect (0, 0, DELETE_BTN_SIZE, DELETE_BTN_SIZE),
					AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
				};
				_deleteBtn.SetImage (UIImage.FromFile ("cross.png"), UIControlState.Normal);

				_contactStatusLabel = new UITextView () {
					Font = BlrtStyles.iOS.CellDetailFont.WithSize (12),
					TextContainerInset = new UIEdgeInsets (3, 3, 3, 3),
					TextAlignment = UITextAlignment.Center,
					BackgroundColor = BlrtStyles.iOS.Gray2,
					UserInteractionEnabled = false
				};
				_contactStatusLabel.Layer.BorderWidth = 1;
				_contactStatusLabel.Layer.CornerRadius = 2;

				_sendingIndicatorView = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
					Hidden = true
				};

				AddSubview (_deleteBtn);
				AddSubview (_contactStatusLabel);
				AddSubview (_sendingIndicatorView);
			}

			public AddContactAccessoryView (EventHandler deleteButtonHandler) : this ()
			{
				_deleteBtn.TouchUpInside += deleteButtonHandler;
			}

			public void UpdateView (bool isCellRemovable, Tuple<bool, Tuple<string, UIColor>> viewModel)
			{
				DeleteBtnHidden = !isCellRemovable;

				var isSending = viewModel.Item1;

				if (isSending) {
					SendingIndicatorViewHidden = false;
					ContactStatusLabelHidden = true;
					return;
				}

				SendingIndicatorViewHidden = true;

				var contactStatus = viewModel.Item2.Item1;
				ContactStausText = contactStatus;
				ContactStatusLabelHidden = string.IsNullOrWhiteSpace (ContactStausText);

				var contactStatusTint = viewModel.Item2.Item2;
				SetContactStatusLabelTint (contactStatusTint);
			}

			/// <summary>
			/// Updates the user type label tint.
			/// </summary>
			/// <param name="contactType">Contact type.</param>
			void SetContactStatusLabelTint (UIColor contactStatusLabelTint)
			{
				_contactStatusLabel.Layer.BorderColor = contactStatusLabelTint.CGColor;
				_contactStatusLabel.TextColor = contactStatusLabelTint;
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();


				/*
				 * delete button has a clear padding around it. It makes it seem as if the delete button and the other views are not 
				 * aligned properly to the right edge of the parent view. To make all the views seem to have same padding,
				 * delete button is moved some value towards right. The value with which to move is the padding 
				 * between imageview's (inside delete button) edge and the delete button's edge.
				*/
				nfloat offset = 0;
				var midY = Bounds.GetMidY ();

				if (!DeleteBtnHidden) {
					offset = _deleteBtn.Bounds.Right - _deleteBtn.ImageView.Frame.Right;

					var newCenter = new CGPoint (Bounds.Right + offset - _deleteBtn.Frame.Width * 0.5f, midY);
					if (!newCenter.Equals (_deleteBtn.Center)) {
						_deleteBtn.Center = newCenter;
					}
				}

				if (!ContactStatusLabelHidden) {
					var size = _contactStatusLabel.SizeThatFits (Bounds.Size);
					var newFrame = new CGRect (Bounds.X + offset + TYPE_LABEL_LEFT_MARGIN,
											   midY - size.Height * 0.5f,
											   size.Width,
											   size.Height);

					if (!newFrame.Equals (_contactStatusLabel.Frame)) {
						_contactStatusLabel.Frame = newFrame;
					}
				}

				if (!SendingIndicatorViewHidden) {
					var newCenter = new CGPoint (Bounds.GetMidX (), Bounds.GetMidY ());
					if (!newCenter.Equals (_sendingIndicatorView.Center)) {
						_sendingIndicatorView.Center = newCenter;
					}
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				nfloat totalWidth = 0;

				if (!DeleteBtnHidden) {
					totalWidth += DELETE_BTN_SIZE;
				}

				if (!ContactStatusLabelHidden) {
					var conSize = _contactStatusLabel.SizeThatFits (size);
					totalWidth += conSize.Width;
				}

				return new CGSize (totalWidth + TYPE_LABEL_LEFT_MARGIN, size.Height);
			}
		}
	}
}
