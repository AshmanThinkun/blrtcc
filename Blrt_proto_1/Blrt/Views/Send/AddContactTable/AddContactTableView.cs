﻿using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using BlrtData;
using Foundation;

namespace BlrtiOS.Views.Send
{
	/// <summary>
	/// Contact table view. Displays all the contacts.
	/// </summary>
	public partial class AddContactTableView: UITableView
	{
		public static readonly int ContactRowHeight = 60;

		public static readonly int TableFooterHeight = 21;

		/// <summary>
		/// Gets or sets the tableview sections.
		/// </summary>
		/// <value>The sections.</value>
		public byte Sections { get; internal set;}

		/// <summary>
		/// Occurs when retry button is clicked.
		/// </summary>
		public event EventHandler ClearButtonClicked {
			add {
				_source.ClearButtonClicked += value;
			}
			remove {
				_source.ClearButtonClicked -= value;
			}
		}

		public List<List<BlrtUtil.IConversationContact>> Data
		{
			get {
				return _source.Data;
			}

			set {
				_source.Data = value;
			}
		}

		public override nint NumberOfSections ()
		{
			return Sections;
		}

		public event EventHandler<NSIndexPath> CellDeleteButtonClicked;
		public event EventHandler<NSIndexPath> RowSelected;

		ContactTableSource _source;

		public AddContactTableView (string sectionTitle, bool clearBtnHidden)
		{
			BackgroundColor = BlrtStyles.iOS.Gray2;
			Source = _source = new ContactTableSource (this,sectionTitle, clearBtnHidden);
			TableFooterView = new UIView (new CGRect(0, 0, 0, TableFooterHeight));
			SectionFooterHeight = 0.3f;
		}

		void OnCellDeleteButtonClicked(NSIndexPath indexPath)
		{
			CellDeleteButtonClicked?.Invoke (this, indexPath);
		}

		public void OnRowSelected (NSIndexPath indexPath) 
		{
			RowSelected?.Invoke (this, indexPath);
		}
	}
}

