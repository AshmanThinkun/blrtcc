using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using BlrtData;

namespace BlrtiOS.Views.Send
{
	public partial class AddContactTableView : UITableView
	{
		class ContactTableSource : UITableViewSource
		{
			readonly string SectionTitle;
			readonly bool ClearBtnHidden;

			List<List<BlrtUtil.IConversationContact>> _contactList;
			AddContactTableView _parent;

			/// <summary>
			/// The sending activity header view. It separates the existing contacts from contacts whom invitation has not been sent to successfully.
			/// </summary>
			SendingActivityHeaderView _sendingActivityHeader;

			public event EventHandler ClearButtonClicked;

			SendingActivityHeaderView SendingActivityHeader { 
				get {
					if (_sendingActivityHeader == null) {
						_sendingActivityHeader = new SendingActivityHeaderView (SectionTitle, ClearBtnHidden);
						_sendingActivityHeader.ClearButtonClicked += OnClearButtonClicked;
					}
					return _sendingActivityHeader;
				}
			}

			/// <summary>
			/// Triggered when retry button is clicked.
			/// </summary>
			/// <param name="sender">Sender.</param>
			/// <param name="e">E.</param>
			void OnClearButtonClicked (object sender, EventArgs e)
			{
				ClearButtonClicked?.Invoke (sender, e);
			}

			public ContactTableSource (AddContactTableView parent, string sectionTitle, bool clearBtnHidden)
			{
				SectionTitle = sectionTitle;
				ClearBtnHidden = clearBtnHidden;

				_contactList = null;
				_parent = parent;
			}

			public List<List<BlrtUtil.IConversationContact>> Data {
				get {
					return _contactList;
				}

				set {
					_contactList = value;
				}
			}

			public override UIView GetViewForHeader (UITableView tableView, nint section)
			{
				if (section != 1 || tableView.NumberOfRowsInSection (section) == 0) {
					return null;
				}

				return SendingActivityHeader;
			}

			public override nfloat GetHeightForHeader (UITableView tableView, nint section)
			{
				if (section != 1 || (_contactList?[(int)section]?.Count ?? 0) == 0) {
					return 0;
				}

				return SendingActivityHeader
					.SizeThatFits (new CoreGraphics.CGSize (tableView.Bounds.Width, nfloat.MaxValue))
					.Height;
			}

			public override nint NumberOfSections (UITableView tableView)
			{
				return tableView.NumberOfSections ();
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				if (null != _contactList) {
					return _contactList [(int)section].Count;
				}

				return 0;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				var cell = tableView.DequeueReusableCell (ContactTableViewCell.CellIdentifier) as ContactTableViewCell;
				// if there are no cells to reuse, create a new one
				if (null == cell) {
					cell = new ContactTableViewCell (ContactTableViewCell.CellIdentifier) {
						SelectionStyle = UITableViewCellSelectionStyle.None
					};
					cell.AccessoryView = new AddContactAccessoryView ( delegate {
						_parent.OnCellDeleteButtonClicked (cell.IndexPath);
					});

				}
				cell.IndexPath = indexPath;

				var cellInfo = _contactList [indexPath.Section][indexPath.Row];
				cell.Model = cellInfo.Model;
				cell.TextLabel.Text = cellInfo.Name;
				cell.DetailTextLabel.AttributedText = GetDetalText(cellInfo.Detail, cellInfo.AccountRemoved);
				cell.ThumbnailLabel.Text = cellInfo.Initials;
				cell.ThumbnailLabel.ImagePath = cellInfo.Avatar;
				cell.BackgroundColor = BlrtStyles.iOS.Gray2;

				var accessoryViewModel = GetContactStatusData (cellInfo);
				(cell.AccessoryView as AddContactAccessoryView)?.UpdateView (cellInfo.IsRemovable, accessoryViewModel);

				return cell;
			}

			NSAttributedString GetDetalText (string detail, bool accountRemoved)
			{
				return new NSAttributedString (detail, new UIStringAttributes {
					Font = BlrtStyles.iOS.CellDetailBoldFont,
					ForegroundColor = accountRemoved ? BlrtStyles.iOS.AccentRed : BlrtStyles.iOS.Gray5,
					Obliqueness = accountRemoved ? 0.25f : 0.0f
				});
			}

			/// <summary>
			/// Gets the contact status data. 
			/// </summary>
			/// <returns>The contact status data.
			/// Tuple<bool, Tuple<string, UIColor>> :
			///     bool: whether contact is being sent
			/// 	string: contact status string (it can either be a contact type or sending status)
			/// 	UIColor: tint color for "string"
			/// </returns>
			/// <param name="cellInfo">Cell info.</param>
			Tuple<bool, Tuple<string, UIColor>> GetContactStatusData (BlrtUtil.IConversationContact cellInfo)
			{
				string contactStatus;
				UIColor contactStatusTint;
				var state = cellInfo.SendingStatus;
				var isSending = state == ContactState.Sending;

				if (isSending) {
					return new Tuple<bool, Tuple<string, UIColor>> (true, null);
				}

				var relationType = cellInfo.RelationType;

				// relation status will be none for only non existing contacts (contacts that haven't been successfully added)
				if (relationType != BlrtData.Models.ConversationScreen.ConversationRelationStatus.RoleEnum.None) 
				{
					contactStatus = BlrtUtil.GetStringForConversationRelationType (relationType);

					var isCreator = BlrtData.Models.ConversationScreen.ConversationRelationStatus.RoleEnum.Creator == relationType;
					contactStatusTint = isCreator ? BlrtStyles.iOS.GreenShadow : BlrtStyles.iOS.BlueGray;
				} 
				else 
				{
					// contact that hasn't been added will have sending status
					contactStatus = BlrtUtil.GetStringForContactSendingState (state);

					var sendingFailed = state == ContactState.FailedToSend;
					contactStatusTint = sendingFailed ? BlrtStyles.iOS.AccentRed : BlrtStyles.iOS.BlueGray;
				}

				var statusData = new Tuple<string, UIColor> (contactStatus, contactStatusTint);
				return new Tuple<bool, Tuple<string, UIColor>> (false, statusData);
			}

			public override UIView GetViewForFooter (UITableView tableView, nint section)
			{
				return null;
			}

			public override nfloat GetHeightForFooter (UITableView tableView, nint section)
			{
				return tableView.SectionFooterHeight;
			}

			public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				return ContactRowHeight;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				tableView.DeselectRow (indexPath, true);
				_parent.OnRowSelected (indexPath);
			}
		}
	}
}
