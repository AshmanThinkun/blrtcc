
using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using BlrtData;
using CoreGraphics;

namespace BlrtiOS.Views.Send
{
	public partial class AddContactTableView
	{
		public class SendingActivityHeaderView : UIView
		{
			readonly string Title;
			readonly UIEdgeInsets Insets = new UIEdgeInsets (5, 22, 5, 16);

			/// <summary>
			/// Occurs when retry button is clicked.
			/// </summary>
			public event EventHandler ClearButtonClicked {
				add {
					if (_clearButton != null) {
						_clearButton.TouchUpInside += value;
					}
				}
				remove {
					if (_clearButton != null) {
						_clearButton.TouchUpInside -= value;
					}
				}
			}

			/// <summary>
			/// Gets the retry button title.
			/// </summary>
			/// <value>The retry button title.</value>
			string RetryButtonTitle {
				get {
					return "Clear";
				}
			}

			UILabel _title;
			UIButton _clearButton;

			UIView _bottomPadding;

			public SendingActivityHeaderView (string headerTitle, bool clearBtnHidden)
			{
				Title = headerTitle;
				BackgroundColor = BlrtStyles.iOS.Green;

				_title = new UILabel {
					Text = Title,
					Font = BlrtStyles.iOS.CellTitleBoldFont,
					TextColor = BlrtStyles.iOS.Black,
					UserInteractionEnabled = false
				};
				_title.SizeToFit ();
                AddSubview (_title);

				if (!clearBtnHidden) {
					_clearButton = new UIButton (new CoreGraphics.CGRect (0, 0, 44, 44));
					_clearButton.SetTitleColor (BlrtStyles.iOS.AccentBlue, UIControlState.Normal);
					_clearButton.SetTitle (RetryButtonTitle, UIControlState.Normal);


					AddSubview (_clearButton);
				}
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var width = _title.Frame.Width;
				var midY = Bounds.Height * 0.5f;

				var newCenter = new CoreGraphics.CGPoint (Insets.Left + width * 0.5f,
														  midY);

				if (!newCenter.Equals (_title.Center)) {
					_title.Center = newCenter;
				}

				if (_clearButton != null && !_clearButton.Hidden) {
					width = _clearButton.Frame.Width;

					newCenter = new CoreGraphics.CGPoint (Bounds.Right - Insets.Right - width * 0.5f,
														  midY);

					if (!newCenter.Equals (_clearButton.Center)) {
						_clearButton.Center = newCenter;
					}
				}
			}

			public override CoreGraphics.CGSize SizeThatFits (CoreGraphics.CGSize size)
			{
				var titleHeight = _title.Frame.Height;
				var retryBtnHeight = _clearButton?.Frame.Height ?? 0;

				var height = Math.Max (titleHeight, retryBtnHeight);
				height += Insets.Top + Insets.Bottom;

				return new CoreGraphics.CGSize (size.Width, /*height*/44);
			}
		}
	}
}
