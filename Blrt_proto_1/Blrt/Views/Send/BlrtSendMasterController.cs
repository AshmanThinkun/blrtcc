using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Foundation;
using BlrtData.Contacts;
using BlrtiOS.UI;
using System.Threading;
using System.Threading.Tasks;
using Parse;
using BlrtData.ExecutionPipeline;
using MessageUI;
using Xamarin.Forms;
using BlrtiOS.Views.Util;
using BlrtiOS.Views.Send;
using BlrtiOS;
using BlrtiOS.Views.CustomUI;
using BlrtShared;
using BlrtiOS.Util;

namespace BlrtData.Views.Send
{
	public partial class BlrtSendMasterController : BaseUINavigationController, IExecutionBlocker
	{
		private AddPeopleViewController _sender;

		private bool _isSending;

		private BlrtData.Conversation _conversation;

		private BlrtLoaderCoverView _overlay;
		private TaskCompletionSource<bool> _tcs;


		public override bool DisablesAutomaticKeyboardDismissal {
			get { return false; }
		}

		SendRootPage _sendRootPage;

		public BlrtSendMasterController ()
		{
			_tcs = new TaskCompletionSource<bool> ();

			_sender = new AddPeopleViewController (null);
			//_sender.NextButtonPressed += SafeEventHandler.FromAction (SendConversation);
			//_sender.CancelPressed += SafeEventHandler.PreventMulti (OnDoneButtonClick);
			//_sender.ContactRemoved += SafeEventHandler.FromAction<ConversationNonExistingContact> (OnSenderContactRemoved);


			_sendRootPage = new SendRootPage ();
			_sendRootPage.OnRetrySend += SafeEventHandler.FromAction (OnRootPageRetrySend);

			var sendRootController = _sendRootPage.CreateViewController ();
			sendRootController.Title = _sendRootPage.Title;
			//right done button
			var doneBtn = new UIBarButtonItem (BlrtHelperiOS.Translate ("Done"), UIBarButtonItemStyle.Plain, null);
			doneBtn.TintColor = UIColor.Black;
			doneBtn.Clicked += SafeEventHandler.PreventMulti (OnDoneButtonClick);
			sendRootController.NavigationItem.RightBarButtonItem = doneBtn;

			//Openpage
			_sendRootPage.AddClicked += SafeEventHandler.FromAction (OpenSendPage);

			PushViewController (sendRootController, false);

			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

			_overlay = new BlrtLoaderCoverView (View.Bounds);
			_overlay.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			View.Add (_overlay);

			Reset ();
		}

		IRequestArgs _requestArgs;

		public void SetRequestArgs (IRequestArgs args)
		{
			_requestArgs = args;
			if (_requestArgs != null && _requestArgs.Emails != null) {
				this.AddContactString (_requestArgs.Emails, !string.IsNullOrEmpty (_requestArgs.BlrtName));

				if (_requestArgs.MakeBlrtPublic) {
					//_sender.ConversationNameHidden = true;
					_sendRootPage.ConversatonVisible = false;
					_sendRootPage.ForceHideConversationName = true;
				} else {
					//_sender.ConversationNameHidden = false;
				}
			}
		}


		#region IExecutionBlocker implementation

		TaskCompletionSource<bool> _tcsAbandonSendingConfirm = null;
		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;
		bool _confirmDlgShown = false;

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsAbandonSendingConfirm = new TaskCompletionSource<bool> ();
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			if (!_confirmDlgShown) {
				await CancelAsync ();
			}
			bool abandonSending = await _tcsAbandonSendingConfirm.Task;
			_tcsAbandonSendingConfirm = null;
			if (abandonSending) {
				await _tcsLeaveThisScreen.Task;
				return true;
			} else {
				_tcsLeaveThisScreen.SetCanceled ();
				_tcsLeaveThisScreen = null;
				return false;
			}
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.NavigationBar.BarTintColor = BlrtStyles.iOS.NavigationBarGray;

			_loader = new BlrtiOS.Views.CustomUI.BlrtLoadingOverlayView ();
			View.AddSubview (_loader);
		}

		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				// enter navigation stack
				ExecutionPerformer.RegisterBlocker (this);
				/*if (_sender.GetContacts ().Count () > 0) {
					OpenSendPage (this, EventArgs.Empty);
				}*/
			}
			base.ViewWillAppear (animated);


			BlrtContactManager.SharedInstanced.Changed += SafeEventHandler.FromAction (ContactsChanged);

			if (BlrtContactManager.SharedInstanced.ShouldLoadContacts) {
				ContactManagerIOS.RefreshContacts ();
			}
			_sender.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());

		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}

		public Task<bool> WaitResult ()
		{
			return _tcs.Task;
		}

		async void OpenSendPage (object sendera, EventArgs ea)
		{
			if (!await SaveConversationName ()) {
				return;
			}

			//check permission
			if (!CheckAddPeoplePermission ()) {
				return;
			}

			//open
			if (_senderNav == null) {
				_senderNav = new BaseUINavigationController (_sender);
				_senderNav.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				_senderNav.NavigationBar.BarTintColor = BlrtStyles.iOS.NavigationBarGray;
			}
			this.PresentViewController (_senderNav, true, null);
		}

		BaseUINavigationController _senderNav;

		bool _refreshFailed;

		async Task OnDoneButtonClick (object sender, EventArgs e)
		{
			if (this.PresentedViewController != null) {
				if (sender == _sender && !await SaveConversationName ()) {
					return;
				}
				if (sender == _sender) {
					//_sender.ConversationNameHidden = true;
				}
				this.PresentedViewController.DismissViewController (true, null);
				_sendRootPage.RecalculateViewAppear ();
			} else {
				if (!await SaveConversationName ()) {
					return;
				}

				//if no user has been sent, show alert
				if (_conversation.OnParse && _conversation.BondCount <= 1 && (_conversation.SendingContacts == null || _conversation.SendingContacts.Count (x => x.Status == ContactState.Sending) < 1)) {
					_sendRootPage.UnFocusNameField ();
					var result = false;/*await BlrtHelperiOS.SimpleConfirmAsync (
			            BlrtUtil.PlatformHelper.Translate ("add_people_alert_title", "send_screen"),
						string.Empty,//BlrtUtil.PlatformHelper.Translate ("add_people_alert_message", "send_screen"),
			            BlrtUtil.PlatformHelper.Translate ("add_people_alert_cancel", "send_screen"),
			            BlrtUtil.PlatformHelper.Translate ("add_people_alert_accept", "send_screen"),
			            false
		            );*/
					if (!result) {
						await Task.Delay (500); // wait for the keyboard animation
						CloseSendMaster ();
					} else {
						OpenSendPage (this, EventArgs.Empty);
					}
				} else {
					CloseSendMaster ();
				}
			}
		}

		private void ContactsChanged (object sender, EventArgs args)
		{
			InvokeOnMainThread (() => {
				if (PresentingViewController != null) {
					_sender.AssignContacts (BlrtContactManager.SharedInstanced.ToList ());
				}
			});
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			BlrtContactManager.SharedInstanced.Save ();
		}


		public async Task<bool> SaveConversationName ()
		{
			/*if (!_sender.ConversationNameHidden) {
				_sendRootPage.ConversationName = _sender.ConversationName;
			}*/

			if (!_sendRootPage.ConversatonVisible)
				return true;

			var newName = _sendRootPage.ConversationName.Trim ();
			if (!BlrtCanvas.CanvasPageView.PlatformHelper.TryValidateConversationName (newName)) {
				return false;
			}

			var status = await BlrtUtil.SaveConversationName (_conversation, newName);
			switch (status) {
				case BlrtAPI.APIConversationRenameRequestStatus.Success :
					return true;
				case BlrtAPI.APIConversationRenameRequestStatus.Cancelled :
				case BlrtAPI.APIConversationRenameRequestStatus.Failed :
					return false;
				default :
					return false;
			}
		}

		public void AddContactString (string[] contactString, bool forrequest = false)
		{
			int deletStringCount = 0;

			// should not add current user to list
			for (int i = 0; contactString != null && i < contactString.Length; ++i) {
				var str = contactString [i].Trim ();
				if (str == ParseUser.CurrentUser.Email) {
					deletStringCount++;
					continue;
				}
				AddContactString (str);
			}
			if (forrequest && (contactString.Length - deletStringCount) > 0) {
			}
		}

		/// <summary>
		/// Adds the contact from string.
		/// </summary>
		/// <param name="contactString">Contact string. should be email address</param>
		public void AddContactString (string contactString)
		{
			var str = contactString.Trim ();

			var contactInfo = new BlrtContactInfo (BlrtUtil.IsValidEmail (str) ? BlrtContactInfoType.Email : BlrtContactInfoType.PhoneNumber, str);
			var newcontact = new BlrtContact () {
				Name = null,
			};
			newcontact.Add (contactInfo);
			var match = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
			if (match == null) {
				match = newcontact;
			}
			var selectInfo = contactInfo;
			foreach (var info in match.InfoList) {
				if (info.Equals (contactInfo)) {
					selectInfo = info;
					break;
				}
			}
			//_sender.AddContactToField (new ConversationNonExistingContact (selectInfo, match));
		}

		public void Reset ()
		{

			//_sender.ClearSendContacts ();


			//_sender.DescriptionFieldText = "";

			HideProgressIndicator (false);
			SetProgress (0.0f, false);

			PopToRootViewController (false);
			SetConversation (null).FireAndForget ();
		}


		public async Task SetConversation (BlrtData.Conversation conversation)
		{
			_conversation = conversation;
			//_sender.SendEnabled = (conversation != null);
			if (conversation != null) {
				_sendRootPage.ConversationName = _conversation.Name;
			//	_sender.ConversationName = _conversation.Name;
				_sendRootPage.SetConversation (conversation);
				LoadUnSendContacts ();
			}


			if (conversation != null && !conversation.OnParse) {
				//_sender.SendEnabled = false;
				ShowProgressIndicator (true);
				var uploader = BlrtManager.Upload.GetUploader (_conversation, _conversation.EstimatePagesCount () <= 0);
				uploader.ProgressChanged += SafeEventHandler.FromAction (
					(sender, e) => {
						InvokeOnMainThread (() => {
							SetProgress (uploader.Progress, true);
						});
					}
				);

				SetProgress (uploader.Progress, false);
				await BlrtManager.Upload.RunAsync (uploader);

				if (!uploader.Finished && View.Superview != null) {
					_confirmDlgShown = true;
					var retry = false;/*await BlrtHelperiOS.SimpleConfirmAsync (
			            BlrtUtil.PlatformHelper.Translate ("alert_blrt_upload_failed_title"),
			            BlrtUtil.PlatformHelper.Translate ("alert_blrt_upload_failed_message"),
			            BlrtUtil.PlatformHelper.Translate ("alert_blrt_upload_failed_cancel"),
			            BlrtUtil.PlatformHelper.Translate ("alert_blrt_upload_failed_accept"),
			            false
		            );*/

					if (retry) {
						SetConversation (conversation);
					} else {
						DismissViewController (true, null);
					}
					_confirmDlgShown = false;
				} else {
					_conversation.Dirty = true;
					await SaveConversationName ();
					_conversation.LocalStorage.DataManager.Save ();
					//_sender.SendEnabled = true;
				}
				_refreshFailed = true;
			}
			PrepareUrl ();
		}

		public void ShowProgressIndicator (bool animated)
		{
			//_sender.ShowProgress (animated);
			_sendRootPage.IsProgressVisible = true;
		}

		public void HideProgressIndicator (bool animated)
		{
			//_sender.HideProgress (animated);
			_sendRootPage.IsProgressVisible = false;
		}

		public void SetProgress (float progress, bool animated)
		{
			//_sender.SetProgress (progress, animated);
			_sendRootPage.SetProgress (progress, animated);
		}


		public void ShowLoading (bool animate)
		{
			if (_overlay != null) {
				_overlay.Show (animate);
			}
		}

		public void HideLoading (bool animate)
		{
			if (_overlay != null) {
				_overlay.Hide (animate);
			}
		}

		private async Task CancelAsync ()
		{
			//var sendLater = _sender.SendEnabled;
			/*if (!sendLater) {
				_confirmDlgShown = true;
				var result = await BlrtHelperiOS.SimpleConfirmAsync (
		            BlrtUtil.PlatformHelper.Translate ("canvas_cancel_send_title"),
		            BlrtUtil.PlatformHelper.Translate ("canvas_cancel_send_message"),
		            BlrtUtil.PlatformHelper.Translate ("canvas_cancel_send_cancel"),
		            BlrtUtil.PlatformHelper.Translate ("canvas_cancel_send_accept"),
		            false
	            );
				if (result) {
					sendLater = true;
				}
			}
			if (null != _tcsAbandonSendingConfirm) {
				_tcsAbandonSendingConfirm.SetResult (sendLater);
			}
			if (sendLater) {
				CloseSendMaster ();
			}*/
			_confirmDlgShown = false;
		}

		async void SendConversation (object sender, EventArgs args)
		{

			if (_isSending || _conversation == null || !_conversation.OnParse)
				return;

			//var contacts = _sender.GetContacts ();

			_isSending = true;

			/*if (contacts.Count () > BlrtPermissions.MaxConversationUsers - _conversation.BondCount
			    || (_conversation.CreatedByCurrentUser && contacts.Count () > BlrtPermissions.MaxConversationUsers - _conversation.BondCount)) {

				new UIAlertView (
					BlrtHelperiOS.Translate ("failed_too_many_recipents_title", "send_screen"),
					BlrtHelperiOS.Translate ("failed_too_many_recipents_message", "send_screen"),
					null,
					BlrtHelperiOS.Translate ("failed_too_many_recipents_accept", "send_screen")
				).Show ();

				_isSending = false;
				return;
			}

			if (contacts.Count () > 10) {

				new UIAlertView (
					BlrtHelperiOS.Translate ("failed_too_many_recipents_single_send_title", "send_screen"),
					BlrtHelperiOS.Translate ("failed_too_many_recipents_single_send_message", "send_screen"),
					null,
					BlrtHelperiOS.Translate ("failed_too_many_recipents_single_send_accept", "send_screen")
				).Show ();

				_isSending = false;*/
				return;
			//}

			//convo name
			/*if (!_sender.ConversationNameHidden) {
				if (!await SaveConversationName ()) {
					_isSending = false;
					return;
				}
			}*/

			/*if (_sendRootPage.ConversatonVisible) {
				ParseAction (contacts.Where (c => c.IsValid ).Select (c => c as ConversationNonExistingContact).ToArray (), BlrtConversation.Name).FireAndForget ();
			} else {
     			ParseAction (contacts.Where (c => c.IsValid ).Select (c => c as ConversationNonExistingContact).ToArray ()).FireAndForget ();
			}

            this.PresentedViewController.DismissViewController (true, null);
            ResetSenderPage ();*/
		}

		/// <summary>
		/// Resets all the fields on the sender page
		/// </summary>
		void ResetSenderPage ()
		{
			//_sender.ClearSendContacts ();
			//_sender.DescriptionFieldText = "";
			//_sender.ConversationNameHidden = true;
			_sender.clearAddressField ();
		}

		async Task<string> GetSmsAccessToken (string[] numbers)
		{
			if (null != numbers || numbers.Length > 0) {
				var param = new Dictionary<string, object> () {
					{ "convoId", _conversation.ObjectId },
					{ "phoneNumbers", numbers }
				};
				try {
					var json = await BlrtUtil.BetterParseCallFunctionAsync<string> ("getPhoneUserAccessToken", param, new CancellationTokenSource (15000).Token);
					var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>> (json);
					object succeededObj = null;
					if (dic.TryGetValue ("success", out succeededObj)) {
						bool succeeded = (bool)succeededObj;
						if (succeeded) {
							object tokenObj = null;
							if (dic.TryGetValue ("token", out tokenObj)) {
								return (string)tokenObj;
							}
						}
					}
					return null;
				} catch {
					return null;
				}
			}
			return null;
		}

		BlrtLoadingOverlayView _loader;

		public override void PushLoading (string cause, bool animated)
		{
			_loader.Push (cause, animated);
			View.BringSubviewToFront (_loader);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loader.Pop (cause, animated);
		}
	}
}

