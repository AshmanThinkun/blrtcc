﻿using System;
using System.Threading.Tasks;
using BlrtiOS.UI;
using BlrtiOS.Views.Send;
using BlrtiOS.Views.Util;
using CoreGraphics;
using UIKit;

namespace BlrtiOS
{
	/// <summary>
	/// Note view controller. It is shown when user is adding a user to a conversation.
	/// </summary>
	public class NoteViewController : BaseUIViewController
	{
		static readonly UIColor viewBackgroundColor = BlrtStyles.iOS.Gray2;
		static readonly UIColor subViewBackgroundColor = BlrtStyles.iOS.White;

		readonly byte NOTE_TEXT_AREA_MARGIN_X = 10;
		readonly byte NOTE_TEXT_AREA_MARGIN_Y = 20;
		readonly byte NOTE_TEXT_AREA_HEIGHT = 110;
		readonly byte NOTE_TEXT_AREA_CORNER_RADIUS = 2;
		readonly CGColor NOTE_TEXT_AREA_OUTLINE_COLOR = BlrtConfig.BLRT_DARK_GRAY.CGColor;

		readonly byte VIEW_MARGIN = 10;
		readonly string TITLE = "Add note";

		BlrtTextView _noteTextArea; // area to type in note
		NoteToConvoSwitchView _noteToConvoSwitchView; // switch to show/hide note in convo
		UIBarButtonItem _sendButton;

		/// <summary>
		/// Occurs when send is pressed.
		/// </summary>
		public event EventHandler SendPressed {
			add {
				_sendButton.Clicked += value;
			}
			remove {
				_sendButton.Clicked -= value;
			}
		}

        public NoteViewController () 
		{
			_sendButton = new UIBarButtonItem (BlrtHelperiOS.Translate ("send_button_send", "send_screen"), 
			                                   UIBarButtonItemStyle.Plain, null);
		}

		/// <summary>
		/// Gets the note text.
		/// </summary>
		/// <value>The note text.</value>
		public string NoteText {
			get {
				return _noteTextArea.Text;
			}
		}

		/// <summary>
		/// Gets a value indicating whether note should be added to conversation
		/// <value><c>true</c> if add note in convo; otherwise, <c>false</c>.</value>
		public bool AddNoteInConvo {
			get {
				return _noteToConvoSwitchView.AddNoteInConvo;
			}
		}

		public override string ViewName {
			get {
				return "NoteViewController";
			}
		}

		public override void TrackPageView ()
		{
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, BlrtData.Helpers.BlrtTrack.LastConvoDic);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = viewBackgroundColor;
			EdgesForExtendedLayout = UIRectEdge.None;
			Title = TITLE;

			NavigationItem.RightBarButtonItem = _sendButton;

			_noteTextArea = new BlrtTextView
			{
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				Font = BlrtStyles.iOS.CellTitleFont,
				PlaceHolder = BlrtHelperiOS.Translate ("send_description_placeholder", "send_screen"),
				LeftPadding = 6f,
				BackgroundColor = subViewBackgroundColor
			};
			_noteTextArea.Layer.CornerRadius = NOTE_TEXT_AREA_CORNER_RADIUS;
			_noteTextArea.Changed += BlrtShared.SafeEventHandler.FromAction ((sender, e) => {
					_noteToConvoSwitchView.SetEnabled (!string.IsNullOrWhiteSpace (_noteTextArea.Text));
				}
			);
			
			_noteToConvoSwitchView = new NoteToConvoSwitchView {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				BackgroundColor = subViewBackgroundColor
			};

			 /* tapping on textarea will not let the event to propagate to the parent view, 
			    thus preventing this gesture recognizer to get triggered */
			View.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				_noteTextArea.ResignFirstResponder ();
			}));

			View.AddSubview (_noteTextArea);
			View.AddSubview (_noteToConvoSwitchView);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			var newFrame = new CGRect(NOTE_TEXT_AREA_MARGIN_X, 
			                          NOTE_TEXT_AREA_MARGIN_Y, 
			                          View.Bounds.Width - NOTE_TEXT_AREA_MARGIN_X * 2, 
			                          NOTE_TEXT_AREA_HEIGHT);

			if (newFrame.Equals (_noteTextArea.Frame)) {
				return; // no need to set layout as it is already set
			}

			_noteTextArea.Frame = newFrame;
			SetNoteTextAreaOutline ();

			var size = _noteToConvoSwitchView.SizeThatFits (View.Bounds.Size);
			_noteToConvoSwitchView.Frame = new CGRect (new CGPoint(View.Bounds.X, _noteTextArea.Frame.Bottom + VIEW_MARGIN), size);
		}

		/// <summary>
		/// Sets the note text area outline.
		/// </summary>
		void SetNoteTextAreaOutline ()
		{
			var outline = new CoreAnimation.CAShapeLayer {
				StrokeColor = NOTE_TEXT_AREA_OUTLINE_COLOR,
				FillColor = null,
				LineWidth = 3,
				LineDashPattern = new Foundation.NSNumber[] {4, 4},
				Frame = _noteTextArea.Bounds,
				Path = UIBezierPath.FromRect (_noteTextArea.Bounds).CGPath,
			};
			_noteTextArea.Layer.AddSublayer (outline);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			_noteTextArea.BecomeFirstResponder ();
		}

		/// <summary>
		/// Note to convo switch view. Contains switch to show/hide note in convo
		/// </summary>
		class NoteToConvoSwitchView : UIView
		{
			readonly byte MARGIN_X = 5; // margin between label and switch
			readonly byte inset = 13; // entire view padding

			UISwitch _noteToConvoSwitch; // switch to show/hide note in convo
			UILabel _switchLabel; // label to let user know what this switch will do.

			/// <summary>
			/// Gets a value indicating whether note should be added to conversation
			/// <value><c>true</c> if add note in convo; otherwise, <c>false</c>.</value>
			public bool AddNoteInConvo {
				get {
					return _noteToConvoSwitch.On;
				}
			}

			public NoteToConvoSwitchView()
			{ 
				_noteToConvoSwitch = new UISwitch
				{
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
					On = true,
					Enabled = false
				};

				_switchLabel = new UILabel {
					Text = BlrtHelperiOS.Translate ("add_note_in_convo", "send_screen"),
					TextColor = BlrtStyles.iOS.Black,
					TextAlignment = UITextAlignment.Left,
					Font = BlrtConfig.NormalFont.Normal,
					LineBreakMode = UILineBreakMode.TailTruncation,
					Enabled = false
				};

				_noteToConvoSwitch.ValueChanged += BlrtShared.SafeEventHandler.FromAction ((sender, e) => {
						if (_noteToConvoSwitch.On) {
						_switchLabel.Text = BlrtHelperiOS.Translate ("add_note_in_convo", "send_screen");
						} else {
							_switchLabel.Text = BlrtHelperiOS.Translate ("add_note_in_convo_off", "send_screen");
						}
					}
				);

				AddSubview (_noteToConvoSwitch);
				AddSubview (_switchLabel);
			}

			/// <summary>
			/// Enables or disables the switch and label
			/// </summary>
			/// <param name="enabled">If set to <c>true</c> enabled.</param>
			public void SetEnabled(bool enabled)
			{
				if (_noteToConvoSwitch.Enabled != enabled && _switchLabel.Enabled != enabled) {
					_noteToConvoSwitch.Enabled = _switchLabel.Enabled = enabled;
				}
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var size = _noteToConvoSwitch.SizeThatFits (Bounds.Size);
				var newCenter = new CGPoint (Bounds.Right - size.Width * 0.5f - inset, Bounds.Height * 0.5f);

				if (!newCenter.Equals (_noteToConvoSwitch.Center)) {
					_noteToConvoSwitch.Center = newCenter;
				} else {
					return; // layout need not to be set
				}

				size = new CGSize (Bounds.Width - 
				                   inset * 2 - /* left, right insets */
				                   size.Width -
				                   MARGIN_X, /* margin between switch and label */
				                   Bounds.Height);
				
				_switchLabel.Frame = new CGRect (Bounds.X + inset, 
				                                 Bounds.GetMidY () - size.Height * 0.5f,
				                                 size.Width,
				                                 size.Height);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				return new CGSize (size.Width, _noteToConvoSwitch.SizeThatFits (size).Height + inset * 2);
			}
		}
	}
}
