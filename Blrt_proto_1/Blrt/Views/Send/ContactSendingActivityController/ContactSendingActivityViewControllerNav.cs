using System;
using System.Collections.Generic;
using System.Linq;
using BlrtData;
using BlrtData.Contacts;
using BlrtiOS.Views.Util;
using Foundation;
using UIKit;
using Xamarin.Forms;

namespace BlrtiOS.Views.Send
{
	public class ContactSendingActivityViewControllerNav : BaseUINavigationController
	{
		ContactSendingActivityViewController _childController;

		public event EventHandler<List<ConversationNonExistingContact>> RetrySendContacts {
			add {
				_childController.RetrySendContacts += value;
			}
			remove {
				_childController.RetrySendContacts -= value;
			}
		}
		public event EventHandler RemindSmsPromptEvent
		{

			add {
				_childController.RemindSmsPromptEvent += value;
			}
			remove {
				_childController.RemindSmsPromptEvent -= value;
			}
		}

		ContactSendingActivityViewControllerNav () {}
		public ContactSendingActivityViewControllerNav (List<BlrtUtil.IConversationContact> contacts, BlrtData.Conversation conversation)
		{
            AddChildViewController (_childController = new ContactSendingActivityViewController(contacts, conversation));
		}

		class ContactSendingActivityViewController : UITableViewController
		{
			readonly string TITLE = "Activity";

			AddContactTableView _contactTableView;
			List<List<BlrtUtil.IConversationContact>> _contacts;
			public UIBarButtonItem _doneButton;


			BlrtData.Conversation Conversation { get; set; }
			List<ContactStatus> SendingContacts { get; set; }

			public event EventHandler<List<ConversationNonExistingContact>> RetrySendContacts;
			public event EventHandler RemindSmsPromptEvent;

			ContactSendingActivityViewController () { }

			public ContactSendingActivityViewController (List<BlrtUtil.IConversationContact> contacts, BlrtData.Conversation conversation)
			{
				_contacts = new List<List<BlrtUtil.IConversationContact>> () { 
					contacts, 
					new List<BlrtUtil.IConversationContact> ()
				};

				Conversation = conversation;
				MessagingCenter.Subscribe<Object, bool> (this, 
				                                         string.Format ("{0}.RelationChanged", 
                                                         Conversation.LocalGuid),
				                                         (o, b) => {
					UpdateContactsStatus ();
				});
			}

			void UpdateContactsStatus ()
			{
				SendingContacts = BlrtManager.DataManagers.Default.GetConversation (Conversation.LocalGuid).SendingContacts;

				if (SendingContacts == null) {
					return;
				}

				SetConversationStatusToSuccessIfRelationExists ();
				SetConversationStatusToFailedIfNeeded ();
				SetConversationStatusIfNeeded ();
			}

			/// <summary>
			/// Sets the conversation status if relation exists. 
			/// </summary>
			void SetConversationStatusToSuccessIfRelationExists ()
			{
				var relations = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (Conversation.LocalGuid);
				/*if(_conversation.SendingContacts==null){
					_conversation.SendingContacts = new List<ContactStatus> ();
				}*/

				//if relation already exist and we haven't got a response after 20 seconds, we should just say success.
				foreach(var relation in relations){
					var match = SendingContacts.FirstOrDefault (x => x.SelectedInfo.ContactValue.ToLower () == relation.QueryValue);
					if(match!=null && (match.Status== ContactState.Sending)&& match.UpdateAt<DateTime.Now.AddSeconds(-20)){
						match.UpdateAt = DateTime.Now;
						if(!string.IsNullOrEmpty (relation.UserId)){
							match.Status = ContactState.ConversationSentUser;
						}else{
							match.Status = ContactState.ConversationSentReachable;
						}
					}
				}
			}

			void SetConversationStatusToFailedIfNeeded ()
			{
				//set other timeout status as send failure
				foreach(var sta in SendingContacts){
					if(sta.Status== ContactState.Sending && sta.UpdateAt<DateTime.Now.AddSeconds(-20)){
						sta.Status = ContactState.FailedToSend;
						sta.UpdateAt = DateTime.Now;
					}
				}
			}

			void SetConversationStatusIfNeeded ()
			{
				var animateSectionReload = false;
				var sendingContacts = _contacts [1];
				var infoStatuses = SendingContacts.OrderByDescending (t => t.UpdateAt);

				foreach (var infoStatus in infoStatuses) {
					var contactInfo = infoStatus.SelectedInfo;
					var newcontact = new BlrtContact () {
						Name = null,
					};
					newcontact.Add (contactInfo);
					var match = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
					if (match == null) {
						match = newcontact;
					}
					var selectInfo = contactInfo;
					foreach (var info in match.InfoList) {
						if (info.Equals (contactInfo)) {
							selectInfo = info;
							break;
						}
					}

					var existingContact = sendingContacts
						.Where (c => (c as ConversationNonExistingContact)
						        .SelectedInfo
						        .Equals (selectInfo))
						.FirstOrDefault ();

					if (existingContact != null) {
						existingContact.SendingStatus = infoStatus.Status;
					} else {
						animateSectionReload = true;
						var contact = new ConversationNonExistingContact (selectInfo, match);
						contact.SendingStatus = infoStatus.Status;
						sendingContacts.Add (contact as BlrtUtil.IConversationContact);
					}
				}

				InvokeOnMainThread (delegate {
					//_tableView.Data = _contacts;
					//TableView.ReloadData ();
					TableView.ReloadSections (new NSIndexSet (1), 
					                          animateSectionReload
					                          ? UITableViewRowAnimation.Automatic : UITableViewRowAnimation.None);
				});
			}

			public override void ViewDidLoad ()
			{
				base.ViewDidLoad ();


				Title = TITLE;

				NavigationController.NavigationBar.BarTintColor = BlrtStyles.iOS.Green;

				_doneButton = new UIBarButtonItem (BlrtHelperiOS.Translate ("result_done", "send_screen"), 
				                                   UIBarButtonItemStyle.Plain, null);
				_doneButton.Clicked += OnDoneButtonClicked;

				NavigationItem.RightBarButtonItem = _doneButton;

				TableView = _contactTableView = new AddContactTableView ("Activity", false) {
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
					Data = _contacts,
					Sections = 2,
					ContentInset = new UIEdgeInsets (15, 0, 0, 0)
				};

				_contactTableView.ClearButtonClicked += OnClearButtonClicked;
				_contactTableView.CellDeleteButtonClicked += OnCellDeleteButtonClicked;
				_contactTableView.RowSelected += OnRowSelected;
			}

			bool isRemindAlertShownOnce;

			protected virtual void OnDoneButtonClicked(object sender, EventArgs e)
			{
			
				if(AddPeopleViewController.hasPhoneNumber && !isRemindAlertShownOnce)
				{
					isRemindAlertShownOnce = true;
					//Create Alert
					var RemindSmsAlert = UIAlertController.Create ("Have you sent an SMS?", "Tap Yes to go ahead or No to send SMS to the phone numbers who do not have a blrt account.", UIAlertControllerStyle.Alert);

					//Add Actions
					RemindSmsAlert.AddAction (UIAlertAction.Create ("Yes", UIAlertActionStyle.Default, alert => DismissViewController (true, null)));
					RemindSmsAlert.AddAction (UIAlertAction.Create ("No", UIAlertActionStyle.Cancel, alert => RemindSmsPromptEvent?.Invoke (null, null)));

					//Present Alert
					PresentViewController (RemindSmsAlert, true, null);

				} else
				{
					DismissViewController (true, null);
				}
			}


			/// <summary>
			/// On the retry button clicked.
			/// </summary>
			/// <param name="sender">Sender.</param>
			/// <param name="e">E.</param>
			void OnClearButtonClicked (object sender, EventArgs e)
			{
				_contacts [1].Clear ();
				ReloadSection (1);
				BlrtUtil.RemoveSendingFailedContacts (true, Conversation);
			}

			void ReloadSection (int section)
			{
				_contacts [section].TrimExcess ();
				TableView.ReloadSections (new NSIndexSet ((uint) section), UITableViewRowAnimation.Automatic);
			}

			void OnCellDeleteButtonClicked (object sender, NSIndexPath path)
			{
				var sec = _contacts [path.Section];
				var contact = sec[path.Row] as ConversationNonExistingContact;
				BlrtUtil.RemoveSendingFailedContacts (true, Conversation, new List<BlrtData.Contacts.BlrtContactInfo> {contact.SelectedInfo});
				sec.RemoveAt (path.Row);
                ReloadSection (path.Section);
			}

			/// <summary>
			/// On the row selected.
			/// </summary>
			/// <param name="sender">Sender.</param>
			/// <param name="e">E.</param>
			void OnRowSelected (object sender, NSIndexPath path)
			{
				var contact = _contacts [path.Section] [path.Row];
				var failedToSend = contact.SendingStatus == ContactState.FailedToSend;

				if (failedToSend) {
					RetrySendContacts?.Invoke (sender, new List<ConversationNonExistingContact> { contact as ConversationNonExistingContact });
				}
			}

			public override void ViewDidAppear (bool animated)
			{
				base.ViewDidAppear (animated);
				TableView.ScrollRectToVisible (TableView.TableFooterView.Frame, true);
			}
		}
	}
}
