﻿//using System;
//using Xamarin.Forms.Platform.iOS;
//using UIKit;
//using Xamarin.Forms;
//using BlrtiOS.Views.CustomRenderers;
//using BlrtData.Views.Send;
//
//[assembly: ExportRenderer (typeof (ShareRootGrid), typeof (ShareRootGridRenderer))]
//namespace BlrtiOS.Views.CustomRenderers
//{
//	public class ShareRootGridRenderer: ViewRenderer
//	{
//		UIView _nativeView;
//		ShareRootGrid _formsView;
//
//
//		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
//		{
//			base.OnElementChanged (e);
//			if (e.OldElement == null) {   // perform initial setup
//				_nativeView =  this.NativeView;
//				_formsView = (ShareRootGrid)(e.NewElement);
//				_nativeView.BackgroundColor = _formsView.BackgroundColor.ToUIColor();
//				_formsView.BackgroundColorChanged -= FormsBackgroundColorChanged;
//				_formsView.BackgroundColorChanged += FormsBackgroundColorChanged;
//			}
//		}
//
//		void FormsBackgroundColorChanged(object sender, EventArgs e ){
//			if(_nativeView!=null){
//				_nativeView.BackgroundColor = _formsView.BackgroundColor.ToUIColor();
//			}
//		}
//
//		public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
//		{
//			base.TouchesBegan (touches, evt);
//			if (_formsView.IsEnabled && _nativeView.BackgroundColor != BlrtStyles.iOS.Green) {
//				UIView.Animate (0.2f, () => {
//					_nativeView.BackgroundColor = BlrtStyles.iOS.Green;
//				});
//			}
//		}
//
//		public override void TouchesCancelled (Foundation.NSSet touches, UIEvent evt)
//		{
//			base.TouchesCancelled (touches, evt);
//			if(_nativeView.BackgroundColor != _formsView.BackgroundColor.ToUIColor()){
//				UIView.Animate (0.2f, () => {
//					_nativeView.BackgroundColor = _formsView.BackgroundColor.ToUIColor();
//				});
//			}
//		}
//
//		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
//		{
//			base.TouchesEnded (touches, evt);
//			if(_nativeView.BackgroundColor != _formsView.BackgroundColor.ToUIColor()){
//				UIView.Animate (0.2f, () => {
//					_nativeView.BackgroundColor = _formsView.BackgroundColor.ToUIColor();
//				});
//			}
//
//			if(_formsView.IsEnabled){
//				_formsView.FireUserTapped ();
//			}
//		}
//	}
//}
//
