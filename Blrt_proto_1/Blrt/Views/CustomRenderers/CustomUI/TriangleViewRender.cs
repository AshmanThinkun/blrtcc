﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using BlrtData;
using CoreAnimation;

[assembly: ExportRendererAttribute (typeof(TriangleView), typeof(TriangleViewRender))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class TriangleViewRender : ViewRenderer<TriangleView,UIView>
	{
		UIView _childView;

		protected override void OnElementChanged (ElementChangedEventArgs<TriangleView> e)
		{
			base.OnElementChanged (e);
			var formsView = e.NewElement as TriangleView;
			if (null != formsView) {
				if (null != _childView) {
					_childView.RemoveFromSuperview ();
				} else {
					_childView = new UIView ();
				}
				_childView.BackgroundColor = BlrtUtil.CombineColor (formsView.Color, formsView.BackgroundColor).ToUIColor ();
				_childView.AutoresizingMask = UIViewAutoresizing.All;
				if (formsView.Width > 0 && formsView.Height > 0) {
					_childView.Layer.Mask = new CAShapeLayer () {
						Path = GetPath (formsView).CGPath
					};
				}

				SetNativeControl (_childView);
			}
		}

		static UIBezierPath GetPath (TriangleView triangle)
		{
			var path = new UIBezierPath ();
			var pts = triangle.GetTrianglePoints ();
			path.MoveTo 	(new CGPoint((nfloat)pts[0].X, (nfloat)pts[0].Y));
			path.AddLineTo 	(new CGPoint((nfloat)pts[1].X, (nfloat)pts[1].Y));
			path.AddLineTo 	(new CGPoint((nfloat)pts[2].X, (nfloat)pts[2].Y));
			path.ClosePath 	();
			return path;
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (null != _childView) {
				if (		e.PropertyName == TriangleView.OrientationProperty.PropertyName 
					|| 		e.PropertyName == BoxView.WidthProperty.PropertyName 
					|| 		e.PropertyName == BoxView.HeightProperty.PropertyName) 
				{
					UpdateShape ();
				}
				else if (	e.PropertyName == BoxView.ColorProperty.PropertyName 
					||		e.PropertyName == BoxView.BackgroundColorProperty.PropertyName)
				{
					UpdateColor ();
				}
			}
		}

		void UpdateShape ()
		{
			var formsView = this.Element as TriangleView;
			if (null != formsView && formsView.Width > 0 && formsView.Height > 0) {
				_childView.Layer.Mask = new CAShapeLayer () {
					Path = GetPath (formsView).CGPath
				};
			}
		}

		void UpdateColor ()
		{
			var formsView = this.Element as TriangleView;
			if (null != formsView) {
				_childView.BackgroundColor = BlrtUtil.CombineColor (formsView.Color, formsView.BackgroundColor).ToUIColor ();
			}
		}
	}
}

