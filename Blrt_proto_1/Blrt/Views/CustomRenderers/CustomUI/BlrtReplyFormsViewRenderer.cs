using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtiOS.Views.CustomUI;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (BlrtReplyFormsView), typeof (BlrtReplyFormsViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class BlrtReplyFormsViewRenderer : BoxRenderer
	{
		private UIView _nativeView;
		private BlrtReplyFormsView _formsView;
		private UIButton _iosView;

		public BlrtReplyFormsViewRenderer (): base()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.BoxView> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {

				_nativeView =  this.NativeView as UIView;
				_formsView = e.NewElement as BlrtReplyFormsView;


				if (_iosView == null) {
					_iosView = NewBlrtButton.CreateReply (() => {
						if(_formsView.Clicked != null)
							_formsView.Clicked(this,EventArgs.Empty);
					});
					_iosView.Center = new CoreGraphics.CGPoint (Bounds.Width * 0.5f, Bounds.Height * 0.5f);
					_nativeView.AddSubview (_iosView);
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_iosView.Center = new CoreGraphics.CGPoint (Bounds.Width * 0.5f, Bounds.Height * 0.5f);
		}
	}
}

