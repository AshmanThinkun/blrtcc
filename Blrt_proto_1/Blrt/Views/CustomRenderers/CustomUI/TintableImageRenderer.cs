using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using CoreGraphics;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using BlrtData.Views.CustomUI;

[assembly: ExportRenderer (typeof (TintableImage), typeof (TintableImageRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class TintableImageRenderer : ImageRenderer
	{
		public TintableImageRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == TintableImage.TintColorProperty.PropertyName
				|| e.PropertyName == Image.SourceProperty.PropertyName) {
				
			}
			SetColor ();
		}

		void SetColor () {
			if (this.Element is TintableImage) {
				var ti = this.Element as TintableImage;

				if (ti.TintColor.A != 0) {
					if (Control.Image != null && Control.Image.RenderingMode != UIImageRenderingMode.AlwaysTemplate)
						Control.Image = Control.Image.ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate);
					Control.TintColor = ti.TintColor.ToUIColor ();
				} else {
					if (Control.Image != null && Control.Image.RenderingMode != UIImageRenderingMode.AlwaysOriginal)
						Control.Image = Control.Image.ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
					Control.TintColor = null;
				}
				Control.Transform = CGAffineTransform.MakeScale (ti.IsVerticallyFlipped ? -1 : 1, 1);
			}
		}
	}
}

