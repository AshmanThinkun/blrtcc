using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsEntry), typeof (FormsEntryRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsEntryRenderer:EntryRenderer
	{
		UITextField _nativeEntry;
		FormsEntry _entryForm;
		Thickness Padding{ get; set; }

		FormsTextField _newEntry;

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Entry> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {   // perform initial setup
				_nativeEntry =  Control as UITextField;
				_entryForm = e.NewElement as FormsEntry;

				if(_entryForm.ReturnKey == ReturnKeyType.Done){
					_nativeEntry.ReturnKeyType = UIReturnKeyType.Done;
				}else if(_entryForm.ReturnKey == ReturnKeyType.Next){
					_nativeEntry.ReturnKeyType = UIReturnKeyType.Next;
				}

				//select all
				_entryForm.SelectAllAction = new Action (() => {
					BeginInvokeOnMainThread(()=>{
//						_nativeEntry.SelectedTextRange = _nativeEntry.GetTextRange(_nativeEntry.BeginningOfDocument, _nativeEntry.EndOfDocument);
						_nativeEntry.SelectAll(null);
					});
				});

				//corner
				if(_entryForm.CornerRadious!=-999){
					_nativeEntry.Layer.CornerRadius = _entryForm.CornerRadious;
					_nativeEntry.BorderStyle = UITextBorderStyle.None;
				}

				if(_entryForm.DisableAutoCap){
					_nativeEntry.AutocapitalizationType = UITextAutocapitalizationType.None;
				}

				if(_entryForm.FontSize!=-999){
					_nativeEntry.Font = UIFont.SystemFontOfSize (_entryForm.FontSize);
				}

				//TODO: remove this part in the future
				Padding = _entryForm.ContentEdgeInsets;
				if (Padding.Left == -999)
					return;
				if (_newEntry == null) {
					_newEntry = new FormsTextField () {
						Padding = _entryForm.ContentEdgeInsets,
						Placeholder = _nativeEntry.Placeholder,
						Text = _nativeEntry.Text,
						BackgroundColor = _nativeEntry.BackgroundColor,
						Frame = _nativeEntry.Frame,
						SecureTextEntry = _entryForm.IsPassword,
						KeyboardType = _nativeEntry.KeyboardType,
						KeyboardAppearance = _nativeEntry.KeyboardAppearance,
						AutocapitalizationType = _nativeEntry.AutocapitalizationType
					};
					_newEntry.EditingChanged += (sender, ex) => {
						_nativeEntry.Text = _newEntry.Text;
						_entryForm.Text = _newEntry.Text;
					};
					AddSubview (_newEntry);
					_nativeEntry.Hidden = true;
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if(_newEntry!=null)
				_newEntry.Frame = _nativeEntry.Frame;
		}

		class FormsTextField : UITextField
		{
			public Thickness Padding{ get; set; }

			public override CGRect TextRect (CGRect forBounds)
			{
				var frame = base.TextRect (forBounds);
				return new CGRect ((nfloat)(frame.X + Padding.Left), (nfloat)(frame.Y + Padding.Top), (nfloat)(frame.Width - Padding.Left - Padding.Right), (nfloat)(frame.Height - Padding.Top - Padding.Bottom));

			}

			public override CGRect EditingRect (CGRect forBounds)
			{
				var frame = base.EditingRect (forBounds);
				return new CGRect ((nfloat)(frame.X + Padding.Left), (nfloat)(frame.Y + Padding.Top), (nfloat)(frame.Width - Padding.Left - Padding.Right), (nfloat)(frame.Height - Padding.Top - Padding.Bottom));

			}
		}
	}
}

