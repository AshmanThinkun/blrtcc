﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using BlrtData.Views.AddPhone;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsPicker), typeof (FormsPickerRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsPickerRenderer: PickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged (e);
			this.Control.TextColor = this.TintColor;
			this.Control.BorderStyle = UITextBorderStyle.None;
			this.Control.Font = UIFont.SystemFontOfSize (34 * 0.5f);
		}
	}
}

