using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using System.ComponentModel;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using UIKit;

[assembly: ExportRenderer (typeof(ExtendedScrollView), typeof(ExtendedScrollViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class ExtendedScrollViewRenderer : ScrollViewRenderer
	{
		CGPoint _targetPoint;

		protected override void OnElementChanged (VisualElementChangedEventArgs e)
		{
			base.OnElementChanged (e);

			this.Scrolled += (sender, ev) => {
				ExtendedScrollView sv = (ExtendedScrollView)Element;
				sv.UpdateBounds (this.Bounds.ToRectangle ());
			};

			if (e.OldElement != null)
				e.OldElement.PropertyChanged -= OnElementPropertyChanged;
			e.NewElement.PropertyChanged += OnElementPropertyChanged;
			ShowsVerticalScrollIndicator = false;
			ShowsHorizontalScrollIndicator = false;
			DecelerationRate = UIScrollView.DecelerationRateFast;
			Bounces = false;
			AlwaysBounceVertical = false;
			AlwaysBounceHorizontal = false;
//			this.AutoresizingMask = UIViewAutoresizing.None;
			_targetPoint = new CGPoint (0f, 0f);
			DecelerationStarted += (sender, ev) => {
				this.SetContentOffset (_targetPoint, true);
			};
			var newEle = e.NewElement as ExtendedScrollView;
			if (null != newEle) {
				this.DraggingStarted += (sender, ev) => {
					newEle.NotifyDraggingStarted ();
				};
				this.DraggingEnded += (sender, ev) => {
					newEle.NotifyDraggingEnded ();
				};
			}
		}

		double EPSILON = 0.1;

		protected void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == ExtendedScrollView.PositionProperty.PropertyName) {
				ExtendedScrollView sv = (ExtendedScrollView)Element;
				Xamarin.Forms.Point pt = sv.Position;
				if (Math.Abs (this.Bounds.Location.Y - pt.Y) < EPSILON
					&& Math.Abs (this.Bounds.Location.X - pt.X) < EPSILON) {
					return;
				}
				_targetPoint = new CGPoint ((nfloat)pt.X, (nfloat)pt.Y);
				this.ScrollRectToVisible (
					new CGRect ((nfloat)pt.X, (nfloat)pt.Y, Bounds.Width, Bounds.Height), sv.AnimateScroll);
			}
		}
	}
}

