using System;
using Xamarin.Forms.Platform.iOS;
using BlrtData.Views.CustomUI;
using UIKit;
using Xamarin.Forms;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (BlrtSwitchImageButton), typeof (BlrtSwitchImageButtonRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class BlrtSwitchImageButtonRenderer : ViewRenderer
	{
		UIButton _btn;

		public BlrtSwitchImageButtonRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			if (null != _btn) {
				_btn.TouchDown -= ShowTouched;
				_btn.TouchDragEnter -= ShowTouched;
				_btn.TouchCancel -= ShowUnTouched;
				_btn.TouchDragExit -= ShowUnTouched;
				_btn.TouchUpInside -= ShowUnTouched;
				_btn.TouchUpOutside -= ShowUnTouched;
			}
			base.OnElementChanged (e);
			_btn = FindButton (this);
			if (null != _btn) {
				_btn.TouchDown += ShowTouched;
				_btn.TouchDragEnter += ShowTouched;
				_btn.TouchCancel += ShowUnTouched;
				_btn.TouchDragExit += ShowUnTouched;
				_btn.TouchUpInside += ShowUnTouched;
				_btn.TouchUpOutside += ShowUnTouched;
			}
		}

		void ShowTouched (object sender, EventArgs e)
		{
			this.Alpha = 0.5f;
		}

		void ShowUnTouched (object sender, EventArgs e)
		{
			this.Alpha = 1f;
		}

		UIButton FindButton (UIView parent)
		{
			if (null != parent && null != parent.Subviews) {
				foreach (var item in parent.Subviews) {
					if (item is UIButton) {
						return item as UIButton;
					}
					if (null != item.Subviews && (item.Subviews.Length > 0)) {
						var foundBtn = FindButton (item);
						if (null != foundBtn) {
							return foundBtn;
						}
					}
				}
			}
			return null;
		}
	}
}

