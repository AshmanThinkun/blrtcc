﻿using System;
using CoreGraphics;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsSlider), typeof (FormsSliderRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsSliderRenderer:SliderRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Slider> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {  
				var nativeView = Control as UISlider;
				nativeView.MinimumTrackTintColor = BlrtStyles.iOS.Green;
			}
		}
	}
}

