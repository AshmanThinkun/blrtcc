using System;
using CoreGraphics;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsListView), typeof (FormsListViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsListViewRenderer : ListViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.ListView> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {  
				var formsView = e.NewElement as FormsListView;
				var separatorInset = formsView.SeparatorInset;

				var nativeListView =  Control as UITableView;
				nativeListView.SeparatorInset = new UIEdgeInsets ((nfloat)separatorInset.Top, (nfloat)separatorInset.Left, (nfloat)separatorInset.Bottom, (nfloat)separatorInset.Right);
				nativeListView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
				SetFooterView (null);

				if(formsView.DisableSelectionStyle){
					nativeListView.AllowsSelection = false;
				}
				if(formsView.DisableScrolling){
					nativeListView.ScrollEnabled = false;

				}
				nativeListView.SectionIndexBackgroundColor = UIColor.Clear;
			}
		}

		void SetFooterView(Xamarin.Forms.View view){

			var nativeListView =  Control as UITableView;
			if(view == null)
				nativeListView.TableFooterView = new UIView (new CGRect (0, 0, 1, 0));
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			foreach(UIView view in this.Subviews[0])
			{
				if(view.Description.IndexOf("UITableViewIndex")!=-1)
				{
					view.TintColor = BlrtStyles.iOS.Green;
				}
			}
		}
	}
}

