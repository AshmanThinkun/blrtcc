﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using System.Threading.Tasks;

[assembly: ExportRenderer (typeof (RoundedImageView), typeof (RoundedImageViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class RoundedImageViewRenderer:ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {   // perform initial setup
				var formImage = e.NewElement as RoundedImageView;
				this.Control.Layer.CornerRadius = (nfloat)(formImage.CornerRadius);
				this.Control.ClipsToBounds = true;
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (e.PropertyName == RoundedImageView.CornerRadiusProperty.PropertyName && this.Element != null && this.Control != null) {
				var formImage = this.Element as RoundedImageView;
				this.Control.Layer.CornerRadius = (nfloat)(formImage.CornerRadius);
				this.Control.ClipsToBounds = true;
			}
		}
	}
}

