﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Foundation;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (BlrtProgressBar), typeof (BlrtProgressBarRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class BlrtProgressBarRenderer: ProgressBarRenderer
	{
		public BlrtProgressBarRenderer ():base()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ProgressBar> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {   // perform initial setup
				var native =  Control as UIProgressView;
				native.Style = UIProgressViewStyle.Default;
				native.ProgressTintColor = native.TintColor = BlrtStyles.iOS.Green;
				native.TrackTintColor = BlrtStyles.iOS.Charcoal;
			}
		}
	}
}

