﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRendererAttribute (typeof(ExtendedImage), typeof(ExtendedImageRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class ExtendedImageRenderer : ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);
			var formsView = e.NewElement as ExtendedImage;
			if (null != formsView && null != Control) {
				Control.Layer.BorderColor = formsView.BorderColor.ToCGColor ();
				Control.Layer.BorderWidth = (nfloat)formsView.BorderWidth;
			}
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
			if (e.PropertyName == ExtendedImage.BorderColorProperty.PropertyName) {
				var formsView = this.Element as ExtendedImage;
				if (null != formsView && null != Control) {
					Control.Layer.BorderColor = formsView.BorderColor.ToCGColor ();
				}
			} else if (e.PropertyName == ExtendedImage.BorderWidthProperty.PropertyName) {
				var formsView = this.Element as ExtendedImage;
				if (null != formsView && null != Control) {
					Control.Layer.BorderWidth = (nfloat)formsView.BorderWidth;
				}
			}
		}
	}
}
