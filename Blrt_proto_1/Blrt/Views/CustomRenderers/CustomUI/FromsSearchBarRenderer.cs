﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using Foundation;
using BlrtData.Views.AddPhone;

[assembly: ExportRenderer (typeof (FromsSearchBar), typeof (FromsSearchBarRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FromsSearchBarRenderer:SearchBarRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<SearchBar> e)
		{
			base.OnElementChanged (e);


			UITextField searchField = null;
			foreach(var view in this.Control.Subviews[0].Subviews){
				if(view is UITextField) { //conform?
					searchField = view as UITextField;
				}
			}
			if(searchField != null) {
				searchField.BackgroundColor = BlrtStyles.iOS.Gray3;
			}

			var searchBar = this.Control;

			searchBar.Layer.BorderWidth = 1;
			searchBar.Layer.BorderColor = BlrtStyles.iOS.White.CGColor;
		}
	}
}

