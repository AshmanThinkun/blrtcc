using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using Foundation;

[assembly: ExportRenderer (typeof (FormsBlrtButton), typeof (BlrtButtonRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class BlrtButtonRenderer : ButtonRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {   // perform initial setup
				var nativeBtn =  Control as UIButton;
				var btnForm = e.NewElement as FormsBlrtButton;
				nativeBtn.ContentEdgeInsets = new UIEdgeInsets ((nfloat) btnForm.Padding.Top, (nfloat)btnForm.Padding.Left, (nfloat)btnForm.Padding.Bottom, (nfloat)btnForm.Padding.Right);

				var attrString = new NSMutableAttributedString(nativeBtn.CurrentTitle??"");
				if (btnForm.IsUnderline)
				{
					attrString.AddAttribute(UIStringAttributeKey.UnderlineStyle, NSNumber.FromInt32((int)NSUnderlineStyle.Single), new NSRange(0, attrString.Length));
					attrString.AddAttribute (UIStringAttributeKey.ForegroundColor, btnForm.TextColor.ToUIColor(), new NSRange (0, attrString.Length));
					nativeBtn.SetAttributedTitle (attrString, UIControlState.Normal);
				}
				nativeBtn.SizeToFit ();
			}
		}
	}
}

