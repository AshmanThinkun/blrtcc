﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using System.ComponentModel;
using CoreGraphics;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;
using UIKit;

[assembly: ExportRenderer (typeof(ExtendedButton), typeof(ExtendedButtonRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class ExtendedButtonRenderer : ButtonRenderer
	{
		ExtendedButton _ele;

		public ExtendedButtonRenderer ()
		{
			_ele = null;
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{
			if (null != e.OldElement && null != Control) {
				Control.TouchDown -= NotifyTouchDown;
				Control.TouchDragEnter -= NotifyTouchDragEnter;
				Control.TouchDragExit -= NotifyTouchDragExit;
				Control.TouchUpInside -= NotifyTouchUpInside;
				Control.TouchUpOutside -= NotifyTouchUpOutside;
				Control.TouchCancel -= NotifyTouchUpOutside;
			}
			base.OnElementChanged (e);
			if (null != Control) {
				_ele = e.NewElement as ExtendedButton;
				Control.TouchDown += NotifyTouchDown;
				Control.TouchDragEnter += NotifyTouchDragEnter;
				Control.TouchDragExit += NotifyTouchDragExit;
				Control.TouchUpInside += NotifyTouchUpInside;
				Control.TouchUpOutside += NotifyTouchUpOutside;
				Control.TouchCancel += NotifyTouchUpOutside;
			}
		}

		void NotifyTouchDown (object sender, EventArgs e)
		{
			if (null != _ele) {
				_ele.NotifyPressed ();
			}
		}

		void NotifyTouchDragEnter (object sender, EventArgs e)
		{
			if (null != _ele) {
				_ele.NotifyDraggedEnter ();
			}
		}

		void NotifyTouchDragExit (object sender, EventArgs e)
		{
			if (null != _ele) {
				_ele.NotifyDraggedExit ();
			}
		}

		void NotifyTouchUpInside (object sender, EventArgs e)
		{
			if (null != _ele) {
				_ele.NotifyReleasedInside ();
			}
		}

		void NotifyTouchUpOutside (object sender, EventArgs e)
		{
			if (null != _ele) {
				_ele.NotifyReleasedOutside ();
			}
		}
	}
}

