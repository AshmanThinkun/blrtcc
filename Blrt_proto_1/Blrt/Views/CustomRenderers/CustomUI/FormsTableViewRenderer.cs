using System;
using CoreGraphics;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsTableView), typeof (FormsTableViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsTableViewRenderer : TableViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.TableView> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {   // perform initial setup
				SetFooterView (null);

				var nativeListView =  Control as UITableView;
				var formsView = e.NewElement as FormsTableView;
				if(formsView.DisableSelectionStyle){
					nativeListView.AllowsSelection = false;
				}
			}
		}

		void SetFooterView(Xamarin.Forms.View view){

			var nativeListView =  Control as UITableView;

			if(view == null)
				nativeListView.TableFooterView = new UIView (new CGRect (0, 0, 1, 0));

		}
	}
}

