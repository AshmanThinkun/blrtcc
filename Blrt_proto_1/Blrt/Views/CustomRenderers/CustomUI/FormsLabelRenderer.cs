using System;
using CoreGraphics;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.CustomRenderers.CustomUI;

[assembly: ExportRenderer (typeof (FormsLabel), typeof (FormsLabelRenderer))]
namespace BlrtiOS.Views.CustomRenderers.CustomUI
{
	public class FormsLabelRenderer: LabelRenderer
	{
		public FormsLabelRenderer ():base()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);
			if (e.OldElement == null) {  
				var nativeView = Control as UILabel;
				nativeView.Lines = 0;

				nativeView.ClipsToBounds = true;
				var formsView = e.NewElement as FormsLabel;
				nativeView.Layer.CornerRadius = formsView.CornerRadius;
			}
		}
	}
}

