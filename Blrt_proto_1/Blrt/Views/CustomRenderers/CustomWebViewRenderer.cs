﻿using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using BlrtiOS.Views.CustomRenderers;
using BlrtData.Views.Login;
using UIKit;

[assembly: ExportRenderer (typeof (CustomWebView), typeof (CustomWebViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers
{
	public class CustomWebViewRenderer : WebViewRenderer
	{
		protected override void OnElementChanged (VisualElementChangedEventArgs e)
		{
			base.OnElementChanged (e);
			if(e.OldElement==null){
				this.AutosizesSubviews = true;
				this.AutoresizingMask = UIKit.UIViewAutoresizing.FlexibleDimensions;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if (this.NativeView != null) {
				ResizeSubviewsToParent (); //this is due to xamarin forms bug, please remove it in the future
			}
		}

		private void ResizeSubviewsToParent()
		{
			foreach (UIView subview in Subviews)
			{
				foreach (UIView sub in subview.Subviews)
				{
					if ((sub is UIImageView) == false)
					{
						sub.Frame = new CoreGraphics.CGRect(sub.Frame.X, sub.Frame.Y, subview.Frame.Width, sub.Frame.Height);
					}
				}
			}
		}
	}
}