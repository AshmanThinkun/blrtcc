﻿
using System;
using BlrtData.Views.CustomUI;
using BlrtiOS.Views.Conversation;
using BlrtiOS.Views.CustomRenderers;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (EditConvoNameView), typeof (EditConvoNameViewRenderer))]
namespace BlrtiOS.Views.CustomRenderers
{
	public class EditConvoNameViewRenderer : ViewRenderer
	{
		ConversationNameSetupViewParent _newConversationSetupView;

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged (e);

			if (Control == null) 
            {
				// first time binding occurs
				var element = ((EditConvoNameView)Element);
				var convoName = element.CurrentConvoName;

				_newConversationSetupView = new ConversationNameSetupViewParent (true);

				_newConversationSetupView.ConvoSaveBtnPressed += delegate {
					element.CurrentConvoName = _newConversationSetupView.ConversationName;
					element.OnConvoSaveBtnPressed (null, new ConvoSaveBtnEventArgs (element.CurrentConvoName));
				};

				_newConversationSetupView.CancelBtnPressed += element.OnConvoCancelBtnPressed;

               	SetNativeControl (_newConversationSetupView);
				_newConversationSetupView.SetInitialConvoName (convoName);
            }
		}
	}
}