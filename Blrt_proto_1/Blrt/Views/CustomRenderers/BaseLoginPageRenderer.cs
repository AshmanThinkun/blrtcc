﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtiOS.Views.CustomRenderers;
using BlrtData.Views.Login;
using Foundation;

[assembly: ExportRenderer (typeof (BaseLoginPage), typeof (BaseLoginPageRenderer))]
namespace BlrtiOS.Views.CustomRenderers
{
	public class BaseLoginPageRenderer: PageRenderer
	{
		NSObject _show, _hide;

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			//keyboard tracking
			_show = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification,KeyBoardUpNotification);
			_hide = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification,KeyBoardDownNotification);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			NSNotificationCenter.DefaultCenter.RemoveObserver (_show);
			NSNotificationCenter.DefaultCenter.RemoveObserver (_hide);
		}

		private void KeyBoardUpNotification(NSNotification notification)
		{
			// get the keyboard size
			var r = UIKeyboard.BoundsFromNotification (notification);

			if(this.Element is BaseLoginPage){
				var login = this.Element as BaseLoginPage;
				login.KeyboardAppearing = true;
				if(login.KeyboardAppeared!=null){
					login.KeyboardAppeared (this, r.Height);
				}
			}

		}

		private void KeyBoardDownNotification(NSNotification notification)
		{
			if(this.Element is BaseLoginPage){
				var login = this.Element as BaseLoginPage;
				login.KeyboardAppearing = false;
			}
		}
	}
}

