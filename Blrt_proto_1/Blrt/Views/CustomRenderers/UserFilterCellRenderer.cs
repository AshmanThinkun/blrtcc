using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BlrtiOS.Views.CustomRenderers;
using BlrtData.Views.Mailbox.UserFilter;

[assembly: ExportRenderer (typeof (UserFilterCell), typeof (UserFilterCellRenderer))]
namespace BlrtiOS.Views.CustomRenderers
{
	public class UserFilterCellRenderer: ViewCellRenderer
	{
		public UserFilterCellRenderer ():base()
		{
		}

		// TODO-FORMS-130
		// WAS: public override MonoTouch.UIKit.UITableViewCell GetCell (Cell item, MonoTouch.UIKit.UITableView tv)
		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var iosCell = base.GetCell (item, reusableCell, tv);

			iosCell.SelectionStyle = UITableViewCellSelectionStyle.None;

			return iosCell;
		}
	}
}

