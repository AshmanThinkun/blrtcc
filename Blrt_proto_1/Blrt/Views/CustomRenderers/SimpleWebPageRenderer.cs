﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using BlrtData.Views.Login;
using BlrtiOS.Views.CustomRenderers;

[assembly: ExportRenderer (typeof (SimpleWebPage), typeof (SimpleWebPageRenderer))]
namespace BlrtiOS.Views.CustomRenderers
{
	public class SimpleWebPageRenderer : PageRenderer
	{
		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			if(BlrtHelperiOS.IsPhone){
				return UIInterfaceOrientationMask.Portrait;
			}
			return base.GetSupportedInterfaceOrientations ();
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return base.ShouldAutorotateToInterfaceOrientation (toInterfaceOrientation);
		}
	}
}

