using System;
using UIKit;
using BlrtData;
using System.Threading.Tasks;
using Parse;
using System.Threading;
using BlrtiOS.Views.Util;
using BlrtiOS.Views.CustomUI;
using BlrtData.Views.Intro;
using Xamarin.Forms;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.Login
{
	public class LoginNavigationController : BaseUINavigationController, ISignUpScreen
	{
		private const string LoadingCause = "loginLoading";

		private UIViewController _intro;
		private LoginMenuController _login;

		BlrtLoadingOverlayView _loadingView;

		public LoginNavigationController () : base ()
		{
			var introPage = new IntroPage ();
			introPage.StartClicked += (sender, e) => {
				OpenSignUpMenu (Executor.System (), true);
			};
			_intro = new IntroPageContainer (introPage.CreateViewController ());
			PushViewController (_intro, false);
			_login = new LoginMenuController ();

			_loadingView = new BlrtLoadingOverlayView ();
			View.AddSubview (_loadingView);
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			try {
				BlrtData.Query.BlrtParseActions.ForceGetSettings (new CancellationTokenSource(15000).Token);
			} catch {}
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			return BlrtHelperiOS.IsPhone ? UIInterfaceOrientationMask.Portrait : UIInterfaceOrientationMask.All;
		}

		public override void PushLoading (string cause, bool animated)
		{
			_loadingView.Push (cause, animated);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loadingView.Pop (cause, animated);
		}

		public void PushToMenu (bool animated)
		{
			if (ViewControllers.Length == 1)
				PushViewController (_login, animated);
		}


		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			if (ParseUser.CurrentUser != null && !ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HasAgreedToTOSKey, false)) {

				var _webView = new TermsController ();
				_webView.OnPageLoadFailed += delegate {
					if (!BlrtHelperiOS.IsPhone)
						DismissViewController (true, null);
					else
						NavigationController.PopViewController (true);
				};
				_webView.OnAccept += delegate {
					if (BlrtHelperiOS.IsPhone)
						NavigationController.PopViewController (true);
					else
						DismissViewController (true, null);

					ParseUser.CurrentUser [BlrtUserHelper.HasAgreedToTOSKey] = true;
				};
				_webView.OnCancel += delegate {
					if (!BlrtHelperiOS.IsPhone)
						DismissViewController (true, null);
					else
						NavigationController.PopViewController (true);


					BlrtManager.Logout (true);
				};

				_webView.Url = String.Format (BlrtData.BlrtSettings.TermOfServiceUrl, 0);
				var _webViewNav = new UINavigationController (_webView);
				_webViewNav.ModalPresentationStyle = _webView.ModalPresentationStyle;
				PresentViewController (_webViewNav, true, null);
			}
		}

		#region ISignUpScreen implementation

		public Task<ExecutionResult<object>> OpenSignUpMenu (IExecutor e, bool animated)
		{
			BeginInvokeOnMainThread (() => {
				PushToMenu (animated);
			});
			return Task<ExecutionResult<object>>.Run (() => {
				return new ExecutionResult<Object> (new Object ());
			});
		}

		#endregion
			

		class IntroPageContainer : UIViewController
		{
			public IntroPageContainer (UIViewController root)
			{
				AddChildViewController (root);
				View.AddSubview (root.View);
			}

			public override void ViewWillAppear (bool animated)
			{
				NavigationController.NavigationBar.SetBackgroundImage (new UIImage (), UIBarMetrics.Default);
				NavigationController.NavigationBar.ShadowImage = new UIImage ();
				base.ViewWillAppear (animated);
			}

			public override void ViewDidDisappear (bool animated)
			{
				base.ViewDidDisappear (animated);
				NavigationController.NavigationBar.SetBackgroundImage (null, UIBarMetrics.Default);
				NavigationController.NavigationBar.ShadowImage = null;
			}
		}
	}
}
