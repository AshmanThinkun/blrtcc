using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.UI;
using Foundation;

namespace BlrtiOS.Views.Login
{
	public abstract class GenericLoginController : Util.BaseUIViewController
	{
		public const float BUTTON_HEIGHT = 48;
		public static readonly float FieldHeight = BlrtHelperiOS.IsPhone ? 36 : 48;
		public static readonly float FieldMargin = BlrtHelperiOS.IsPhone ? 6 : 10;
		public static readonly float ButtonMargin = FieldMargin + 5;
		protected const float BUTTON_MARGIN = 32;


		public static readonly float ButtonWidth = BlrtHelperiOS.IsPhone ? 280 : 320;




		public virtual float ContentWidth { get { return ButtonWidth; } }


		private LoginScrollView _scrollView;
		protected UIImageView _logo;
		private nfloat _keyboardHeight;

		public UIScrollView ScrollView { get { return _scrollView; } }

		public UIView ContentView { get { return _scrollView.ContentView; } }

		NSObject _keyboardHide;
		NSObject _keyboardWillShow;
		NSObject _keyboardDidShow;

		public GenericLoginController () : base ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = BlrtConfig.BlrtGreen;


			_scrollView = new LoginScrollView (View.Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			_scrollView.ContentView.Frame = new CGRect (0, 0, ContentWidth, View.Frame.Height);
			View.Add (_scrollView);

			_logo = new UIImageView (UIImage.FromBundle ("blrt_tpd.png")) {
				ContentMode = UIViewContentMode.Center,
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleTopMargin
			};
			if (!BlrtHelperiOS.IsPhone) {
				_logo.Frame = new CGRect (0, 0, ContentWidth, ContentWidth * _logo.Image.Size.Height / _logo.Image.Size.Width);
				_logo.Center = new CGPoint (ContentWidth * 0.5f, BUTTON_MARGIN + _logo.Frame.Height * 0.5f);
			} else {
				nfloat width = ContentWidth - 150;
				if (UIScreen.MainScreen.Bounds.Height > 500)
					width = ContentWidth - 100;
				_logo.Frame = new CGRect (0, 0, width, width * _logo.Image.Size.Height / _logo.Image.Size.Width);
				_logo.Center = new CGPoint (ContentWidth * 0.5f, _logo.Frame.Height * 0.5f);
			}
			ContentView.Add (_logo);
		}


		public override void LoadView ()
		{
			base.LoadView ();

			UIGestureRecognizer tap = new UITapGestureRecognizer ();
			tap.AddTarget (() => {

				var v = tap.View.HitTest (tap.LocationInView (tap.View), new UIEvent ());

				if (v is BlrtActionButton || v is UIButton) {
				} else {
					tap.View.EndEditing (false);
				}
			});
			tap.CancelsTouchesInView = false;
			View.AddGestureRecognizer (tap);
		}


		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (NavigationController != null) {
				NavigationController.SetNavigationBarHidden (false, animated);
				NavigationController.NavigationBar.BackgroundColor = BlrtConfig.BlrtGreen;
			}

			NSNotificationCenter center = NSNotificationCenter.DefaultCenter;
			_keyboardHide 		= center.AddObserver (UIKeyboard.WillHideNotification, keyboardHide);
			_keyboardWillShow	= center.AddObserver (UIKeyboard.WillShowNotification, keyboardShow);
			_keyboardDidShow 	= center.AddObserver (UIKeyboard.DidShowNotification, keyboardShow);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);


			NSNotificationCenter center = NSNotificationCenter.DefaultCenter;
			center.RemoveObserver (_keyboardHide);
			center.RemoveObserver (_keyboardWillShow);
			center.RemoveObserver (_keyboardDidShow);
		}

		public void ScrollToView (UIView subject)
		{
			var frame = BlrtHelperiOS.GetFrameInSuperView (_scrollView, subject);
			frame.Y -= BUTTON_HEIGHT;
			frame.Height += BUTTON_HEIGHT * 2;

			ScrollView.ScrollRectToVisible (frame, true);
		}

		private void keyboardHide (NSNotification notification)
		{
			var time = notification.UserInfo [UIKeyboard.AnimationDurationUserInfoKey] as NSNumber;
			_keyboardHeight = 0;



			if (time != null) {
				UIView.Animate (time.DoubleValue, () => {
					_scrollView.Frame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - _keyboardHeight);
				}, KeyboardHasChanged);
			} else {
				_scrollView.Frame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - _keyboardHeight);
				KeyboardHasChanged ();
			}
		}

		private void keyboardShow (NSNotification notification)
		{

			var time = notification.UserInfo [UIKeyboard.AnimationDurationUserInfoKey] as NSNumber;
			CGRect keyboardBounds = UIKeyboard.FrameEndFromNotification (notification);
			CGRect keyboardFrameConverted = View.ConvertRectFromView (keyboardBounds, UIApplication.SharedApplication.Windows [0]);

			_keyboardHeight = keyboardFrameConverted.Height;



			if (time != null) {
				UIView.Animate (time.DoubleValue, () => {
					_scrollView.Frame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - _keyboardHeight);
				}, KeyboardHasChanged);
			} else {
				_scrollView.Frame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - _keyboardHeight);
				KeyboardHasChanged ();
			}
		}

		public abstract void KeyboardHasChanged ();

		protected class LoginScrollView : UIScrollView
		{
			public UIView ContainerView { get; private set; }

			public UIView ContentView { get; private set; }

			public LoginScrollView (CGRect frame) : base (frame)
			{

				ContentView = new UIView (new CGRect (0, 0, frame.Width, frame.Height)) {
					//AutoresizingMask = UIViewAutoresizing.FlexibleMargins
				};

				ContainerView = new UIView (new CGRect (0, 0, frame.Width, frame.Height)) {
					//AutoresizingMask = UIViewAutoresizing.FlexibleMargins
				};


				Add (ContainerView);
				ContainerView.Add (ContentView);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				ContainerView.Frame = new CGRect (
					0,
					0, 
					NMath.Max (ContentView.Bounds.Width, Bounds.Width - ContentInset.Left - ContentInset.Right),
					NMath.Max (ContentView.Bounds.Height, Bounds.Height - ContentInset.Bottom - ContentInset.Top)
				);
				ContentSize = ContainerView.Frame.Size;

				ContentView.Frame = new CGRect (
					(ContainerView.Bounds.Width - ContentView.Bounds.Width) * 0.5f, 
					(ContainerView.Bounds.Height - ContentView.Bounds.Height) * 0.5f, 
					ContentView.Bounds.Width, 
					ContentView.Bounds.Height
				);
			}
		}
	}
}

