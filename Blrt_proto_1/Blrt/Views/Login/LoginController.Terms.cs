using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Diagnostics;
using BlrtiOS;
using BlrtiOS.UI;
using BlrtiOS.Views.Util;


namespace BlrtiOS.Views.Login
{
	public class TermsController : Util.BaseUIViewController
	{
		public override string ViewName { get { return "Term"; } }


		public string Url { get; set; }
		public EventHandler OnPageLoadFailed { get; set; }
		public EventHandler OnAccept { get; set; }
		public EventHandler OnCancel { get; set; }


		private UIWebView _webView;
		private BlrtLoaderCoverView _loadingOverlay;

		public TermsController() : base()
		{
			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationController.NavigationBar.Translucent = false;
			View.BackgroundColor = UIColor.White;



			if (_webView != null) {
				_webView.RemoveFromSuperview ();
				_webView.Dispose ();
				_webView = null;
			}

			_webView = new UIWebView (new CGRect(0,0,View.Bounds.Width,View.Bounds.Height-44)){
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight,
			};
			var webDelegate = new WebViewDelegate ();
			_webView.Delegate = webDelegate;

			webDelegate.OnPageLoadFailed += (object sender, EventArgs args)=>{ 
				if(OnPageLoadFailed != null)
					OnPageLoadFailed(sender, args);
			};


			webDelegate.OnHyperLinkClicked += (object sender, EventArgs e) => {
				var url = sender as string;
				OnHyperLinkClicked(url);
			};

			View.Add (_webView);


			var _toolbar = new UIToolbar (){
				Frame = new CGRect (0, View.Bounds.Height - 44, View.Bounds.Width, 44),
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleTopMargin,
				BackgroundColor = UIColor.White,
				Translucent = false	
			};

			var _btnCancel = UIButton.FromType (UIButtonType.RoundedRect);
			_btnCancel.Frame = new CGRect (20, (_toolbar.Frame.Height-30)*0.5f, 80, 30);
			_btnCancel.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
			_btnCancel.Font = BlrtConfig.BUTTON_FONT;
			_btnCancel.SetTitle ("Cancel", UIControlState.Normal);
			_btnCancel.SetTitleColor(UIColor.Black,UIControlState.Normal);
			_btnCancel.TouchUpInside += delegate {
				if (OnCancel != null)
					OnCancel (this,EventArgs.Empty);
			};

			var _btnAccept = new BlrtActionButton ("Accept", BlrtConfig.BUTTON_FONT){
				Frame = new CGRect (View.Bounds.Width-100, (_toolbar.Frame.Height-30)*0.5f, 80, 30),
				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin,
				TintColor = BlrtConfig.BlrtBlack	
			};
			_btnAccept.Click += delegate {
				if(OnAccept!=null)
					OnAccept(this,EventArgs.Empty);
			};

			View.Add (_toolbar);
			_toolbar.LayoutIfNeeded ();
			_toolbar.Add (_btnCancel);
			_toolbar.Add (_btnAccept);

			webDelegate.OnLoadFinish += delegate {
				NavigationItem.Title = _webView.EvaluateJavascript ("document.title");
				if(!_loadingOverlay.Hidden)
					_loadingOverlay.Hide(true);
			};


			_loadingOverlay = new BlrtLoaderCoverView (View.Bounds);
			View.Add (_loadingOverlay);

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			_loadingOverlay.Show (true);

			if (Url != null) {

				_webView.LoadHtmlString ("", null);
				_webView.LoadRequest (new NSUrlRequest (new NSUrl (Url)));


			}
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			_webView.StopLoading ();
		}

		public void OnHyperLinkClicked(string url)
		{
			if (!BlrtHelperiOS.IsPhone) {
				var linkwebView = new WebViewController();
				linkwebView.OnPageLoadFailed +=delegate {
					UIAlertView alert = new UIAlertView ("Error", BlrtHelperiOS.Translate ("internet_connection_error", 	"login"), null, "OK", null);
					alert.Clicked += (s, b) => {
						DismissViewController(true,null);
					};
					alert.Show();
				};
				linkwebView.OnDismiss += delegate {
					DismissViewController(true,null);
				};
				linkwebView.Url = url;
				var linkwebViewNav = new UINavigationController(linkwebView);
				linkwebViewNav.ModalPresentationStyle = linkwebView.ModalPresentationStyle;

				PresentViewController(linkwebViewNav,true,null);
			}
			else
				UIApplication.SharedApplication.OpenUrl (new NSUrl (url));
		}

	}

	class WebViewDelegate : UIWebViewDelegate
	{
		public EventHandler OnPageLoadFailed { get; set; }
		public EventHandler OnHyperLinkClicked{ get; set; }
		public EventHandler OnLoadFinish{ get; set; }


		public override bool ShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
		{
			if(navigationType == UIWebViewNavigationType.Other)
				return true;

			if (OnHyperLinkClicked != null)
				OnHyperLinkClicked (request.Url.ToString (), EventArgs.Empty);

			return false;
		}

		public override void LoadFailed (UIWebView webView, NSError error)
		{
			if (error.Code != -999 && OnPageLoadFailed != null) {
				OnPageLoadFailed (this, EventArgs.Empty);
				LLogger.WriteLine ("Tos failed because: {{Code: \"{0}\", Message: \"{1}\"}}", error.Code, error.LocalizedDescription);
			}
		}

		public override void LoadingFinished (UIWebView webView)
		{
			var answer = webView.EvaluateJavascript ("blrt_check == \"blrt\"");


			if (answer == "true") {
				if (OnLoadFinish != null)
					OnLoadFinish (webView, EventArgs.Empty);
			} else {

				if (OnPageLoadFailed != null) {
					OnPageLoadFailed (this, EventArgs.Empty);
					LLogger.WriteLine ("Tos failed because: JS didn't validate.");
				}
			}

		}

	}
}

