using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.UI;
using BlrtData.Views.Login;
using Xamarin.Forms;
using Foundation;
using System.Drawing;

namespace BlrtiOS.Views.Login
{
	public class LoginMenuController : GenericLoginController
	{

		public override string ViewName {
			get { return "Login_GetStarted"; }
		}

		public LoginMenuController () : base ()
		{
			Title = BlrtHelperiOS.Translate ("splash_title", "login");
		}

		public override void KeyboardHasChanged ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			FBManager.LoginManagerInstance.LogOut ();
			var _fbLogin = BlrtActionButton.FacebookButton ("Log in with Facebook");
			_fbLogin.Frame = new CGRect (0, _logo.Frame.Bottom + BUTTON_MARGIN, ContentWidth, BUTTON_HEIGHT);
			_fbLogin.Click += delegate {
				FBManager.LoginManagerInstance.LogInWithReadPermissions(BlrtData.BlrtConstants.FacebookExtendedPermissions, new BlrtFacebookLoginHandler().SystemCallback);
			};

			BlrtActionButton btnEmail = new BlrtActionButton (BlrtHelperiOS.Translate ("splash_login_button", "login"), BlrtConfig.BUTTON_FONT) {
				Frame = new CGRect (0, _fbLogin.Frame.Bottom + BUTTON_MARGIN, ContentWidth, BUTTON_HEIGHT),
				TintColor = BlrtConfig.BlrtBlack
			};
			btnEmail.Click += delegate {
				var page = new HomeLoginPage();
				var controller = page.CreateViewController();
				NavigationController.PushViewController(controller,true);
			};

			ContentView.Add (_fbLogin);
			ContentView.Add (btnEmail);
			ContentView.Frame = new CGRect (0, 0, ContentWidth, btnEmail.Frame.Bottom + BUTTON_MARGIN);
		}

		public void ShowLoading (object sender, EventArgs args)
		{
			PushLoading ("facebookLogin", true);
		}

		public void HideLoading (object sender, EventArgs args)
		{
			PopLoading ("facebookLogin", true);
		}
	}
}

