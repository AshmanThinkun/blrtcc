﻿using System;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.Models.Organisation;
using Foundation;
using CoreGraphics;
using BlrtData;
using System.IO;
using MessageUI;
using Xamarin.Forms;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using BlrtShared;

namespace BlrtiOS.Views.Organisation
{
	public class OrgnisationListController : UIViewController
	{
		TableModelView<OrganisationListItemCellModel> _table;
		OrganisationListFilter _filter;

		public OrgnisationListController (string ocListId) : base ()
		{
			if (string.IsNullOrWhiteSpace (ocListId)) {
				throw new ArgumentNullException ("ocListId");
			}
			_filter = new OrganisationListFilter (ocListId);

			_table = new TableModelView<OrganisationListItemCellModel> ();
			_table.Scrolled += SafeEventHandler.FromTaskFunc (OnTableScrolled);
		}


		UIButton _closeButton;
		UIImageView _topBanner;
		UIActivityIndicatorView _loader;
		UIBarButtonItem _rightBarItem;
		BottomBannerView _bottomBanner;
		public override void ViewDidLoad ()
		{
			_rightBarItem = new UIBarButtonItem (
				UIImage.FromBundle ("share_navbtn.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate), 
				UIBarButtonItemStyle.Plain,
				ShareButtonClicked)
			{
				TintColor = BlrtStyles.iOS.Black
			};

			this.NavigationItem.SetRightBarButtonItem (_rightBarItem, true);

			_closeButton = new UIButton (new CGRect (0, 0, 80, 70)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleBottomMargin,
				Hidden = true
			};
			_closeButton.SetTitle ("Close", UIControlState.Normal);
			_closeButton.SetTitleColor (BlrtStyles.iOS.Black, UIControlState.Normal);
			_closeButton.TouchUpInside += SafeEventHandler.FromAction (
				(sender, e) => {
					(this.NavigationController as ExecutionNavController)?.DismissNav ();
				}
			);
			_topBanner = new UIImageView (new CGRect (0, 0, View.Bounds.Width, 0)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleDimensions,
				BackgroundColor = BlrtStyles.iOS.Black,
				ContentMode = UIViewContentMode.ScaleAspectFill,
				UserInteractionEnabled = true
			};
			_topBanner.AddGestureRecognizer (new UITapGestureRecognizer ((o) => {
				TopBannerClicked ();
			}));

			_bottomBanner = new BottomBannerView () {
				Frame = new CGRect (0, 0, View.Bounds.Width, 0),
				Hidden = true
			};
			_bottomBanner.ButtonClicked += SafeEventHandler.PreventMulti (OnBottomButtonClicked);


			_table.Frame = new CGRect (0, _topBanner.Frame.Bottom, View.Frame.Width, View.Frame.Height - _topBanner.Frame.Bottom);
			_table.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions | UIViewAutoresizing.FlexibleTopMargin;
			_table.List = _filter.List;

			_table.RegisterClassForCellReuse (typeof (OrganisationListItemCell), new NSString (OrganisationListItemCell.Indentifer));

			_table.Bounces = true;

			_table.SeparatorInset = new UIEdgeInsets (0, OrganisationListItemCell.CellHeight + 8, 0, 0);
			_table.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;

			_table.RowHeight = OrganisationListItemCell.CellHeight;
			_table.SectionHeaderHeight = 0;
			_table.SectionFooterHeight = 0;

			_table.TableFooterView = _loader = new UIActivityIndicatorView (new CGRect (0, 0, View.Bounds.Width, 70)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				Color = BlrtStyles.iOS.Charcoal,
				Hidden = true
			};

			_filter.ReloadList ().FireAndForget ();
			OnFilterPageModelChanged (this, EventArgs.Empty).FireAndForget ();

			_table.TableHeaderView = _topBanner;
			View.AddSubviews (_table, _bottomBanner);

			_filter.PageModelChanged += SafeEventHandler.FromTaskFunc (OnFilterPageModelChanged);

			UpdateList ().FireAndForget ();
		}

		async Task UpdateList ()
		{
			var showLoader = true;
			if (showLoader) {
				StartLoading ();
			}
			await _filter.RefreshList ();
			if (showLoader)
			{
				StopLoading ();
			}
		}

		async Task OnBottomButtonClicked (object sender, EventArgs e)
		{
			if (_filter.PageModel != null
				&& _filter.PageModel.BottomBanner != null
				&& !string.IsNullOrEmpty (_filter.PageModel.BottomBanner.ButtonUri)
			) {
				await BlrtData.BlrtManager.OpenHyperlink (_filter.PageModel.BottomBanner.ButtonUri);
			}
		}

		#region share
		void ShareButtonClicked (object sender, EventArgs e)
		{
			if (_filter.PageModel == null)
				return;

			_filter.PrepareUrl ();

			//display options
			nint copyLink, shareLink, emailLink, messageLink, fbLink;
			var acsheet = new UIActionSheet ();

			copyLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_copy_link", "conversation_screen"));
			shareLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_share_link", "conversation_screen"));
			emailLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_email_link", "conversation_screen"));
			messageLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_message_link", "conversation_screen"));
			fbLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_fb_link", "conversation_screen"));
			acsheet.CancelButtonIndex = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_cancel", "conversation_screen"));

			acsheet.Clicked += SafeEventHandler.PreventMulti<UIButtonEventArgs> (
				async (senders, es) => {
					if (es.ButtonIndex == copyLink) {
						await CopyShareLinkAsync ();
					} else if (es.ButtonIndex == shareLink) {
						await ShareShareLinkAsync (sender);
					} else if (es.ButtonIndex == emailLink) {
						await EmailShareLinkAsync ();
					} else if (es.ButtonIndex == messageLink) {
						await MessageShareLinkAsync ();
					} else if (es.ButtonIndex == fbLink) {
						await FbMessageShareLinkAsync ();
					}
				}
			);

			if (sender is UIBarButtonItem) {

				var view = sender as UIBarButtonItem;

				acsheet.ShowFrom (view, true);
			}
		}


		async Task CopyShareLinkAsync ()
		{
			PushLoading ("", true);
			await _filter.GenerateLink ("oclist_other");
			PopLoading ("", true);
			var url = _filter.url_oclist_other;

			UIPasteboard.General.String = url;
		}

		public async Task EmailShareLinkAsync ()
		{
			PushLoading ("", true);
			await _filter.GenerateLink ("oclist_email");
			PopLoading ("", true);
			var url = _filter.url_oclist_email;
			var impression = BlrtUtil.GetImpressionHtml ("oclist_email");
			impression += BlrtUtil.GetImpressionHtml ("share_email");
			await BlrtUtil.GenerateYozioShareUrl ("share_email", true);

			if (!string.IsNullOrEmpty (_filter.PageModel.ShareCustomText)) {
				url = string.Format (_filter.PageModel.ShareCustomText, url);
			}

			ShowEmailViewController (
				"",
				url,
				false,
				false
			);
		}

		public async Task MessageShareLinkAsync ()
		{
			PushLoading ("", true);
			await _filter.GenerateLink ("oclist_sms");
			PopLoading ("", true);
			var url = _filter.url_oclist_sms;

			if (!string.IsNullOrEmpty (_filter.PageModel.ShareCustomText)) {
				url = string.Format (_filter.PageModel.ShareCustomText, url);
			}

			ShowSMSViewController (url);
		}

		public async Task ShareShareLinkAsync (object sender)
		{
			PushLoading ("", true);
			await _filter.GenerateLink ("oclist_other");
			PopLoading ("", true);
			var url = _filter.url_oclist_other;

			var activeVc = new UIActivityViewController (new NSObject [] { NSUrl.FromString (url) }, null);

			if (BlrtUtil.PlatformHelper.IsPhone) {
				this.PresentViewController (activeVc, true, null);
			} else {
				var popup = new UIPopoverController (activeVc);
				Device.BeginInvokeOnMainThread (() => {
					popup.PresentFromBarButtonItem (sender as UIBarButtonItem, UIPopoverArrowDirection.Any, true);
				});
			}
		}

		public async Task FbMessageShareLinkAsync ()
		{
			PushLoading ("", true);
			await _filter.GenerateLink ("oclist_facebook");
			PopLoading ("", true);
			var url = _filter.url_oclist_facebook;

			var nsurl = new NSUrl (url);
			FBManager.TrySendFBMessenger (nsurl, this);
		}

		void PushLoading (string casue = "", bool animate = true)
		{
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading ();
		}

		void PopLoading (string casue = "", bool animate = true)
		{
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
		}

		void ShowSMSViewController (string body)
		{
			if (MFMessageComposeViewController.CanSendText) {

				var m = new MFMessageComposeViewController {
					Body = body
				};
				m.Finished += SafeEventHandler.FromTaskFunc (
					async (object s, MFMessageComposeResultEventArgs e) => {
						m.BeginInvokeOnMainThread (() => {
							m.DismissViewController (true, null);
						});

						if (e.Result == MessageComposeResult.Sent) {
							await BlrtUtil.TrackImpression ("oclist_sms");
						}
					}
				);
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				this.BeginInvokeOnMainThread (() => {
					this.PresentViewController (m, true, null);
				});
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (),
					new SimpleAlert (
						platforHelper.Translate ("alert_enable_messages_title", "share"),
						platforHelper.Translate ("alert_enable_messages_message", "share"),
						platforHelper.Translate ("alert_enable_messages_cancel", "share")
					)
				);
			}
		}

		UIViewController ShowEmailViewController (string subject, string body, bool isHtml, bool showAlert = false, bool presentController = true)
		{
			try {
				if (MFMailComposeViewController.CanSendMail) {
					var m = new MFMailComposeViewController ();
					m.SetSubject (subject);
					m.SetMessageBody (body, isHtml);
					m.Finished += SafeEventHandler.FromAction(
						(object sender, MFComposeResultEventArgs e) => {
							m.BeginInvokeOnMainThread (() => {
								m.DismissViewController (true, null);
							});
						}
					);
					m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

					if (presentController) {
						this.BeginInvokeOnMainThread (() => {
							if (this.PresentedViewController != null && Device.Idiom == TargetIdiom.Phone) {
								this.DismissViewController (false, () => {
									this.PresentViewController (m, true, null);
								});
							} else {
								this.PresentViewController (m, true, null);
							}
						});
					}

					if (showAlert) {
						new UIAlertView (
							BlrtHelperiOS.Translate ("alert_before_send_filter.url_title", "conversation_screen"),
							BlrtHelperiOS.Translate ("alert_before_send_filter.url_message", "conversation_screen"),
							null,
							BlrtHelperiOS.Translate ("alert_before_send_filter.url_accept", "conversation_screen")
						).Show ();
					}
					return m;
				} else {
					var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
					BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (),
						new SimpleAlert (
							platforHelper.Translate ("cannot_send_email_error_title"),
							platforHelper.Translate ("cannot_send_email_error_message"),
							platforHelper.Translate ("cannot_send_email_error_cancel")
						)
					);
				}
			} catch {
			}
			return null;
		}



		#endregion

		bool _ignoreScroll;
		async Task OnTableScrolled (object sender, EventArgs e)
		{
			// if scroll to the last cell of the page, then load next page
			if (_filter != null
				&& _filter.HasMoreContent
				&& !_ignoreScroll
				&& _table.ContentOffset.Y > (_filter.List [0].Count * _table.RowHeight - _table.Bounds.Height) /*make sure it scrolls to the last item*/) {
				_ignoreScroll = true;
				StartLoading ();
				await _filter.LoadNext ();
				StopLoading ();
				_ignoreScroll = false;
			}
		}

		public void StartLoading ()
		{
			InvokeOnMainThread (() => {
				_loader.Hidden = false;
				_loader.StartAnimating ();
			});
		}

		public void StopLoading ()
		{
			InvokeOnMainThread (() => {
				_loader.Hidden = true;
				_loader.StopAnimating ();
			});
		}


		void TopBannerClicked ()
		{
			if (_filter.PageModel != null && !string.IsNullOrEmpty (_filter.PageModel.TopBannerTargetUrl)) {
				//open webpage
				WebViewController webView = new WebViewController () { Url = _filter.PageModel.TopBannerTargetUrl };
				var nav = new UINavigationController (webView);
				this.PresentViewController (nav, true, null);
				webView.OnDismiss += SafeEventHandler.FromAction (
					(sender, e) => {
						this.DismissViewController (true, null);
					}
				);
			}
		}

		async Task OnFilterPageModelChanged (object sender, EventArgs e)
		{
			if (_filter.PageModel == null) {
				return;
			}
			this.Title = _filter.PageModel.Name;

			//load bottom banner
			if (_filter.PageModel.BottomBanner != null) {
				_bottomBanner.Hidden = false;
				_bottomBanner.Title = _filter.PageModel.BottomBanner.Title;
				_bottomBanner.Detail = _filter.PageModel.BottomBanner.Detail;
				_bottomBanner.ButtonTitle = _filter.PageModel.BottomBanner.ButtonTitle;
			} else {
				_bottomBanner.Hidden = true;
			}

			// load top banner
			if (string.IsNullOrEmpty (_filter.PageModel.CloudTopBannerImagePath)) {
			} else if (!File.Exists (_filter.PageModel.LocalTopBannerImagePath)) {
				await _filter.DownLoadFile (_filter.PageModel.CloudTopBannerImagePath, _filter.PageModel.LocalTopBannerImagePath);
			} else {
				_topBanner.Image = UIImage.FromFile (_filter.PageModel.LocalTopBannerImagePath);
			}
			this.View.SetNeedsLayout ();
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();

			var image = _topBanner.Image;
			if (image != null) {
				var height = image.Size.Height * View.Bounds.Width / image.Size.Width;
				_topBanner.Frame = new CGRect (0, 0, View.Bounds.Width, height);
			} else {
				_topBanner.Frame = new CGRect (0, 0, View.Bounds.Width, 0);
			}
			_table.TableHeaderView = _topBanner;

			if (_filter.PageModel != null && _filter.PageModel.BottomBanner != null) {
				_bottomBanner.Frame = new CGRect (0, View.Bounds.Height - 82, View.Bounds.Width, 82);
			} else {
				_bottomBanner.Frame = new CGRect (0, 0, View.Bounds.Width, 0);
			}

			_table.Frame = new CGRect (0, 0, View.Frame.Width, View.Frame.Height - _bottomBanner.Frame.Height);
		}
	}
}

