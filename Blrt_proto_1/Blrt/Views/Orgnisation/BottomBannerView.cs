﻿using System;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtiOS.Views.AppMenu;
using BlrtData.Models.Organisation;
using BlrtiOS.Views.CustomUI;
using BlrtData.Views.Helpers;
using CoreGraphics;

namespace BlrtiOS.Views.Organisation
{
	public class BottomBannerView: UIView
	{

		UILabel _titleLbl;
		UILabel _detailLbl;
		UIButton _actionButton;

		const int ButtonMargin = 20;
		const int ButtonMinWidth = 100;

		public string Title{
			get{
				return _titleLbl.Text;
			}set{
				if(_titleLbl.Text!=value){
					_titleLbl.Text = value;
					SetNeedsLayout ();
				}
			}
		}

		public string Detail{
			get{
				return _detailLbl.Text;
			}set{
				if(_detailLbl.Text!=value){
					_detailLbl.Text = value;
					SetNeedsLayout ();
				}
			}
		}

		public string ButtonTitle{
			get{
				return _actionButton.Title(UIControlState.Normal);
			}set{
				if(_actionButton.Title(UIControlState.Normal)!=value){
					_actionButton.SetTitle (value, UIControlState.Normal);
				}
			}
		}

		public event EventHandler ButtonClicked;
		public BottomBannerView ():base()
		{
			this.BackgroundColor = BlrtStyles.iOS.Gray2;

			_titleLbl = new UILabel () {
				Font = UIFont.SystemFontOfSize (13.5f),
				TextColor = BlrtStyles.iOS.Gray7,
				Lines = 0,
			};

			_detailLbl = new UILabel () {
				Font = UIFont.SystemFontOfSize (13.5f),
				TextColor = BlrtStyles.iOS.Gray7,
				Lines = 0,
			};

			_actionButton = new UIButton () {
				BackgroundColor = BlrtStyles.iOS.Green,
			};
			_actionButton.Font = UIFont.BoldSystemFontOfSize (14);
			_actionButton.SetTitleColor (BlrtStyles.iOS.Charcoal, UIControlState.Normal);
			_actionButton.Layer.CornerRadius = 2;
			_actionButton.Layer.ShadowRadius = 0;
			_actionButton.Layer.ShadowOffset = new CoreGraphics.CGSize (2, 2);
			_actionButton.Layer.ShadowOpacity = 1;
			_actionButton.Layer.ShadowColor = BlrtStyles.iOS.GreenShadow.CGColor;

			_actionButton.SetImage (UIImage.FromBundle ("btn_logo.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal);




			_actionButton.TouchUpInside += (object sender, EventArgs e) => {
				ButtonClicked?.Invoke(sender,e);
			};

			this.AddSubviews (_titleLbl, _detailLbl, _actionButton);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			_actionButton.Frame = new CGRect (Bounds.Width - ButtonMinWidth - ButtonMargin, (Bounds.Height - 45) * 0.5f, ButtonMinWidth, 45);
			// the space between the image and text
			nfloat spacing = -3;

			// lower the text and push it left so it appears centered 
			//  below the image
			CGSize imageSize = _actionButton.ImageView.Frame.Size;
			_actionButton.TitleEdgeInsets = new UIEdgeInsets(
				8, - imageSize.Width, - (imageSize.Height + spacing), 0);

			// raise the image and push it right so it appears centered
			//  above the text
			CGSize title1Size = _actionButton.TitleLabel.Frame.Size;
			_actionButton.ImageEdgeInsets = new UIEdgeInsets(
				- (title1Size.Height + spacing), 0, 0, - title1Size.Width);



			var textWidth = Bounds.Width - 3 * ButtonMargin - ButtonMinWidth;

			var titleSize = _titleLbl.SizeThatFits (new CGSize (textWidth, nfloat.MaxValue));

			var detailSize = _detailLbl.SizeThatFits (new CGSize (textWidth, nfloat.MaxValue));

			if(titleSize.Height +detailSize.Height > Bounds.Height){
				detailSize.Height = Bounds.Height - titleSize.Height;
			}

			_titleLbl.Frame = new CGRect (ButtonMargin, (Bounds.Height - titleSize.Height - detailSize.Height) * 0.5f, titleSize.Width, titleSize.Height);
			_detailLbl.Frame = new CGRect (ButtonMargin, _titleLbl.Frame.Bottom, detailSize.Width, detailSize.Height);
		}
	}
}

