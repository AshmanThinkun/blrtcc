﻿using System;
using System.Linq;
using BlrtData.ExecutionPipeline;

using System.ComponentModel;
using System.Threading.Tasks;
using BlrtData;
using System.Collections.Generic;
using BlrtData.Views.Helpers;
using System.Collections.ObjectModel;
using System.IO;
using UIKit;
using BlrtData.Contents;
using MessageUI;
using BlrtiOS;
using Foundation;
using Xamarin.Forms;
using System.Web;

namespace BlrtiOS.Views.Organisation
{
	class ShareAction: ICellAction
	{
		string _id;
		ConversationBlrtItem _content;
		UIViewController _controller;
		public ShareAction(string id)
		{
			_id = id;
			_content = BlrtManager.DataManagers.Default.GetConversationItem (id) as ConversationBlrtItem;
			_controller = (BlrtManager.App as UIViewController).TopPresentedViewController ();
		}

		#region ICellAction implementation
		UIView _sender;
		public void Action (object sender)
		{
			BlrtLinkClicked (sender);
		}
		#endregion


		void BlrtLinkClicked (object sender)
		{
			PrepareUrl ();
			nint copyLink, shareLink, emailLink, messageLink, fbLink;
			var acsheet = new UIActionSheet ();

			copyLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_copy_link", "conversation_screen"));
			shareLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_share_link", "conversation_screen"));
			emailLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_email_link", "conversation_screen"));
			messageLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_message_link", "conversation_screen"));
			fbLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_fb_link", "conversation_screen"));
			acsheet.CancelButtonIndex = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_cancel", "conversation_screen"));

			acsheet.Clicked += (senders, es) => {
				if (es.ButtonIndex == copyLink) {
					CopyShareLinkPressed ();
					BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (_content.ConversationId, _content.ObjectId, _content.PublicBlrtName);
				} else if (es.ButtonIndex == shareLink) {
					ShareShareLinkPressed ();
					BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (_content.ConversationId, _content.ObjectId, _content.PublicBlrtName);
				} else if (es.ButtonIndex == emailLink) {
					EmailShareLinkPressed ();
				} else if (es.ButtonIndex == messageLink) {
					MessageShareLinkPressed ();
				} else if (es.ButtonIndex == fbLink) {
					FbMessageShareLinkPressed ();
					BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (_content.ConversationId, _content.ObjectId, _content.PublicBlrtName);
				}
			};

			if (sender is UIView) {

				var view = _sender = sender as UIView;
				var rect = view.Bounds;
				rect.Inflate (10240, 0);

				acsheet.ShowFrom (
					rect,
					view,
					true
				);
			}
		}

		async void CopyShareLinkPressed ()
		{
			PushLoading ("", true);
			await GenerateLink ("blrt_other");
			PopLoading ("", true);
			var url = _url_blrt_other;

			UIPasteboard.General.String = url;
			new UIAlertView (
				BlrtUtil.PlatformHelper.Translate ("alert_copy_url_title",	"conversation_screen"),
				"",
				null,
				BlrtUtil.PlatformHelper.Translate ("alert_copy_url_accept",	"conversation_screen")
			).Show ();
		}

		public async void EmailShareLinkPressed ()
		{
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_id);
			PushLoading ("", true);
			await GenerateLink ("blrt_email");
			PopLoading ("", true);
			var url = _url_blrt_email;
			var impression = BlrtUtil.GetImpressionHtml ("blrt_email");
			impression += BlrtUtil.GetImpressionHtml ("share_email");
			var shareLink = await BlrtUtil.GenerateYozioShareUrl ("share_email", true);

			var conversation = BlrtManager.DataManagers.Default.GetConversation (content.ConversationId);

			if (_content.IsPublicBlrt) {
				ShowEmailViewController (
					string.Format (BlrtUtil.PlatformHelper.Translate ("email_public_blrt_url_subject", "conversation_screen"), _content.PublicBlrtName),
					string.Format (BlrtUtil.PlatformHelper.Translate ("email_public_blrt_url_message_body_html", "conversation_screen"), url, _content.PublicBlrtName, 
						HttpUtility.UrlEncode (_content.PublicBlrtName), HttpUtility.UrlEncode (url), impression,
						shareLink
					),
					true,
					!_content.IsPublicBlrt
				);
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
			} else {
				ShowEmailViewController (
					string.Format (BlrtUtil.PlatformHelper.Translate ("email_blrt_url_subject", "conversation_screen"), conversation.Name),
					string.Format (BlrtUtil.PlatformHelper.Translate ("email_blrt_url_message_body_html", "conversation_screen"), url, conversation.Name, impression,
						shareLink
					),
					true,
					!_content.IsPublicBlrt
				);
			}
		}

		public async void MessageShareLinkPressed ()
		{
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_id);
			PushLoading ("", true);
			await GenerateLink ("blrt_sms");
			PopLoading ("", true);
			var url = _url_blrt_sms;
			string blrtName = null;
			if (_content.IsPublicBlrt) {
				blrtName = _content.PublicBlrtName;
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
			} else {
				var conversation = BlrtManager.DataManagers.Default.GetConversation (content.ConversationId);
				blrtName = conversation.Name;
			}
			ShowSMSViewController (blrtName, url);
		}

		public async void ShareShareLinkPressed(){
			PushLoading ("", true);
			await GenerateLink ("blrt_other");
			PopLoading ("", true);
			var url = _url_blrt_other;

			var activeVc = new UIActivityViewController (new NSObject[]{NSUrl.FromString(url)}, null);

			if (BlrtUtil.PlatformHelper.IsPhone) {
				_controller.PresentViewController (activeVc, true, null);
			}else{
				var popup = new UIPopoverController(activeVc);
				if(_sender!=null){
					var rect = _sender.Bounds;
					rect.Inflate (10240, 0);
					Device.BeginInvokeOnMainThread(()=>{
						popup.PresentFromRect(rect, _sender, UIPopoverArrowDirection.Any, false);

					});
				}else{
					Device.BeginInvokeOnMainThread(()=>{
						popup.PresentFromRect (new CoreGraphics.CGRect(235,225,10,10), _controller.View, UIPopoverArrowDirection.Any, false);
					});
				}
			}
		}

		public async void FbMessageShareLinkPressed(){
			PushLoading ("", true);
			await GenerateLink ("blrt_facebook");
			PopLoading ("", true);
			var url = _url_blrt_facebook;

			var nsurl = new NSUrl (url);
			FBManager.TrySendFBMessenger (nsurl, _controller);
		}

		void PushLoading(string casue = "", bool animate =true){
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading ();
		}

		void PopLoading(string casue = "", bool animate =true){
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
		}

		void ShowSMSViewController (string blrtName, string url)
		{
			if (MFMessageComposeViewController.CanSendText) {

				var m = new MFMessageComposeViewController () {
					Body = string.Format (BlrtUtil.PlatformHelper.Translate("message_blrt_url_sms_body", "conversation_screen"), blrtName, url)
				};
				m.Finished += (object s, MFMessageComposeResultEventArgs e) => {
					m.BeginInvokeOnMainThread (() => {
						m.DismissViewController (true, null);
					});

					if(e.Result == MessageComposeResult.Sent){
						BlrtUtil.TrackImpression("blrt_sms");
					}
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				_controller.BeginInvokeOnMainThread (() => {
					_controller.PresentViewController (m, true, null);
				});
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert(
						platforHelper.Translate ("alert_enable_messages_title", 		"share"),
						platforHelper.Translate ("alert_enable_messages_message", 		"share"),
						platforHelper.Translate ("alert_enable_messages_cancel", 		"share")
					)
				);
			}
		}

		UIViewController ShowEmailViewController (string subject, string body, bool isHtml, bool showAlert = false, bool presentController = true)
		{
			try {
				if (MFMailComposeViewController.CanSendMail) {
					var m = new MFMailComposeViewController ();
					m.SetSubject (subject);
					m.SetMessageBody (body, isHtml);
					m.Finished += (sender, e) => {
						m.BeginInvokeOnMainThread (() => {
							m.DismissViewController (true, null);
						});
					};
					m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

					if(presentController){
						_controller.BeginInvokeOnMainThread(()=>{
							if(_controller.PresentedViewController!=null && Device.Idiom == TargetIdiom.Phone){
								_controller.DismissViewController(false,()=>{
									_controller.PresentViewController (m, true, null);
								});
							}else{
								_controller.PresentViewController (m, true, null);
							}
						});
					}

					if (showAlert) {
						new UIAlertView (
							BlrtHelperiOS.Translate ("alert_before_send_url_title",	"conversation_screen"),
							BlrtHelperiOS.Translate ("alert_before_send_url_message",	"conversation_screen"),
							null,
							BlrtHelperiOS.Translate ("alert_before_send_url_accept",	"conversation_screen")
						).Show ();
					}
					return m;
				} else {
					var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
					BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), 
						new SimpleAlert (
							platforHelper.Translate ("cannot_send_email_error_title"),
							platforHelper.Translate ("cannot_send_email_error_message"),
							platforHelper.Translate ("cannot_send_email_error_cancel")
						)
					);
				}
			} catch {
			}
			return null;
		}



		string _url_blrt_sms, _url_blrt_facebook, _url_blrt_twitter, _url_blrt_email, _url_blrt_other = "";

		public void PrepareUrl(){  //we can hook this with the bubble on appearing method as well
			Task.Factory.StartNew (() => {
				GenerateLink ("blrt_sms");
				GenerateLink ("blrt_facebook");
				GenerateLink ("blrt_twitter");
				GenerateLink ("blrt_email");
				GenerateLink ("blrt_other");
			});
		}

		public void InvalidateUrl(){
			_url_blrt_sms = _url_blrt_facebook =_url_blrt_twitter = _url_blrt_email = _url_blrt_other = "";
		}

		async Task GenerateLink(string urlType){
			switch(urlType){
				case "blrt_sms":
					if (string.IsNullOrEmpty (_url_blrt_sms) || !_url_blrt_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_sms = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_facebook":
					if (string.IsNullOrEmpty (_url_blrt_facebook) || !_url_blrt_facebook.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_facebook = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_twitter":
					if (string.IsNullOrEmpty (_url_blrt_twitter) || !_url_blrt_twitter.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_twitter = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_email":
					if (string.IsNullOrEmpty (_url_blrt_email) || !_url_blrt_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_email = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_other":
					if (string.IsNullOrEmpty (_url_blrt_other) || !_url_blrt_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_other = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
			}
		}

	}
}

