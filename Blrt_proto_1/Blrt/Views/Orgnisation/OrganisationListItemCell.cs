﻿using System;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtiOS.Views.AppMenu;
using BlrtData.Models.Organisation;
using BlrtiOS.Views.CustomUI;
using BlrtData.Views.Helpers;
using CoreGraphics;

namespace BlrtiOS.Views.Organisation
{
	public class OrganisationListItemCell: UITableViewCell, IBindable<OrganisationListItemCellModel>
	{
		#region IBindable implementation
		public void Bind (OrganisationListItemCellModel data)
		{
			_model = data;
			_titleLbl.Text = _model.Title;
			_authorLbl.Text = "Public Blrt by " + _model.AuthorName;
			_dateLbl.Text = BlrtHelperiOS.TranslateDate (_model.CreatedAt);
			if (!string.IsNullOrEmpty (_model.Author.LocalAvatarPath)) {
				_imageView.SetAsyncSource (null, _model.Author.LocalAvatarPath);
			} else if (!string.IsNullOrEmpty (_model.Author.AvatarName)) {
				_imageView.SetAsyncSource (new Uri(_model.Author.AvatarCloudPath), BlrtData.BlrtConstants.Directories.Default.GetAvatarPath (_model.Author.AvatarName));
			} else {
				_imageView.SetAsyncSource (_model.CloudPathThumbnail, _model.PathThumbnail);
			}
		}
		#endregion

		public const string Indentifer = OrganisationListItemCellModel.OrganisationListItemCellIdentifer;
		public const int CellHeight = 84;




		AsyncImageView _imageView;

		private UILabel _titleLbl;
		private UILabel _authorLbl;
		private UILabel _dateLbl;
		private UIButton _shareBtn;

		OrganisationListItemCellModel _model;

		public OrganisationListItemCell(IntPtr handle): base(handle)
		{
			BackgroundColor = BlrtStyles.iOS.White;
			SelectionStyle = UITableViewCellSelectionStyle.None;

			_imageView = new AsyncImageView(){
				ContentMode = UIViewContentMode.ScaleAspectFit
			};


			_titleLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				Lines = 2,	
				TextColor = BlrtStyles.iOS.Black
			};
			_authorLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellDetailFont,
				Lines = 1,			
				TextColor= BlrtStyles.iOS.Gray6
			};
			_dateLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellDetailBoldFont,
				Lines = 1,		
				TextColor = BlrtStyles.iOS.Gray6
			};

			_shareBtn = new UIButton ();
			_shareBtn.SetImage (UIImage.FromBundle ("menu_share.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal);
			_shareBtn.ImageView.TintColor = BlrtStyles.iOS.Black;

			_shareBtn.TouchUpInside += SharePressed;

			ContentView.AddSubviews (new UIView[] {
				_imageView,

				_titleLbl,
				_authorLbl,
				_dateLbl,
				_shareBtn
			});

		}

		void SharePressed (object sender, EventArgs e)
		{
			_model.ShareAction.Action (sender);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			//the gap between the image and the copy
			const int Spacer = 21;

			var sep = 1 / UIScreen.MainScreen.Scale;

			ContentView.Frame = new CGRect (0,  ContentView.Frame.Y, Bounds.Width, Bounds.Height - sep);
			_imageView.Frame = new CGRect (0, 0, ContentView.Bounds.Height, ContentView.Bounds.Height);

			_shareBtn.Frame = new CGRect (Bounds.Width - 30 - Spacer*1.5f, 0, 30 + Spacer*1.5f, Bounds.Height);

			var width = ContentView.Frame.Width - (_imageView.Frame.Width + Spacer + 4 + 30 + Spacer);

			var titleSize = _titleLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));
			var authorSize = _authorLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));
			var dateSize = _dateLbl.SizeThatFits(new CGSize(width, nfloat.MaxValue));


			_titleLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				8, 
				width, 
				titleSize.Height
			);
			_authorLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				48, 
				width, 
				authorSize.Height
			);
			_dateLbl.Frame = new CGRect (
				_imageView.Frame.Width + Spacer, 
				62, 
				width, 
				dateSize.Height
			);
		}
	}
}

