﻿using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.Settings;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;

namespace BlrtiOS.Views
{
	public class ExecutionNavController: GenericNavigationController, IExecutionBlocker
	{
		#region IExecutionBlocker implementation

		async Task<bool> IExecutionBlocker.Resolve (ExecutionPerformer e)
		{
			if(e is OpenIndependentBlrtPerformer){
				return true;
			}

			var tcs = new TaskCompletionSource<bool> ();
			if(this.PresentingViewController!=null){
				this.DismissViewController(true,()=>{
					tcs.TrySetResult(true);
					ExecutionPerformer.UnregisterBlocker(this);
				});
			}
			return await tcs.Task;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ExecutionPerformer.RegisterBlocker (this);
			this.NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			this.OnDismiss += (sender, e) => {
				if(this.PresentingViewController!=null){
					this.DismissViewController(true,()=>{
						ExecutionPerformer.UnregisterBlocker(this);
					});
				}
			};
		}

		private event EventHandler OnDismiss;
		public ExecutionNavController (UIViewController rootController):base( rootController)
		{
			rootController.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem("Close", UIBarButtonItemStyle.Plain, (s,ev)=>{
				OnDismiss?.Invoke(this,EventArgs.Empty);
			}),false);
			this.ModalPresentationCapturesStatusBarAppearance = true;

			this.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
		}

		public void DismissNav()
		{
			OnDismiss?.Invoke(this,EventArgs.Empty);
		}
	}
}

