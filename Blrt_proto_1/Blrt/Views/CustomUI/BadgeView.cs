using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class BadgeView : UIView
	{
		private const int HPadding = 2;
		private const int VPadding = 1;
		private UILabel _lbl;

		public BadgeView () : base ()
		{
			Frame = new CoreGraphics.CGRect (0, 0, HPadding * 2, VPadding * 2);
			_lbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Center = this.Center,
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize (10),
				TextColor = BlrtStyles.iOS.Gray3
			};
			AddSubview (_lbl);

			BackgroundColor = BlrtStyles.iOS.AccentRed;
		}


		public void SetState (int badge)
		{

			_lbl.Text = Math.Max (0, badge).ToString ("N0");

			var size = _lbl.SizeThatFits (new CoreGraphics.CGSize (128, 128));

			var width = NMath.Max (size.Width, size.Height);

			Bounds = new CoreGraphics.CGRect (0, 0, width + HPadding * 2, size.Height + VPadding * 2);
			Layer.CornerRadius = Bounds.Height * 0.5f;
		}
	}
}

