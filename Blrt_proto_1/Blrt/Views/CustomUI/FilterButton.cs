using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace BlrtiOS.Views.CustomUI
{
	public class FilterButton : BlrtButton
	{
		private UILabel _lbl;
		private UIImageView _filter;

		private UIEdgeInsets _padding;

		int _badge;

		public int Badge { get { return _badge; } }

		public FilterButton () : base ()
		{
			_lbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Center = this.Center,
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize (14),
				TextColor = BlrtStyles.iOS.Charcoal
			};
			_filter = new UIImageView (UIImage.FromBundle ("filter_icon")) {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin,
				TintColor = BlrtStyles.iOS.Charcoal
			};
			AddSubview (_lbl);
			AddSubview (_filter);

			_padding = new UIEdgeInsets (1, 2, 1, 2);

			Layer.CornerRadius = 4;

			SetState (-1);
		}


		public void SetState (int badge)
		{
			if (_badge != badge) {

				_badge = badge;

				if (badge > 0) {
					_lbl.Hidden = false;
					_lbl.Text = badge.ToString ();
					BackgroundColor = BlrtStyles.iOS.Green;			
				} else {
					_lbl.Hidden = true;
					BackgroundColor = BlrtStyles.iOS.Gray3;
				}

				Resize ();
			}
		}

		void Resize ()
		{
			var s1 = _filter.SizeThatFits (new CGSize (100, 100));
			var s2 = _lbl.SizeThatFits (new CGSize (100, 100));

			s2.Width += 3;

			if (_lbl.Hidden)
				s2 = CGSize.Empty;

			Bounds = new CGRect (0, 0, 
				s1.Width + s2.Width + _padding.Left + _padding.Right,
				NMath.Max (s1.Height, s2.Height) + _padding.Top + _padding.Bottom);

			_filter.Frame = new CGRect (_padding.Left, (int)(Bounds.Height - s1.Height) / 2, s1.Width, s1.Height);
			_lbl.Frame = new CGRect (_filter.Frame.Right, (int)(Bounds.Height - s2.Height) / 2, s2.Width, s2.Height);
		}
	}
}

