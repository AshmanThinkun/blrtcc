using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace BlrtiOS.Views.CustomUI
{
	public class BlrtsCounterView:UIView
	{
		private const int Padding = 4;
		private const int whiteSecHPadding = 4;
		private const int whiteSecHeight = 17;
		private const int whiteSecMinWidth = 18;


		private BadgeView _unreadBadgeView;
		private UILabel _lbl;

		public BlrtsCounterView () : base ()
		{
			_lbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Center = this.Center,
				TextAlignment = UITextAlignment.Center,
				Font = UIFont.SystemFontOfSize (13),
				Opaque = false,
				BackgroundColor = BlrtStyles.iOS.WhiteAlpha,
				ClipsToBounds = true
			};

			_lbl.Layer.CornerRadius = 3;
			Bounds = new CGRect (0, 0, Padding * 2 + whiteSecHPadding * 2, Padding * 2 + whiteSecHeight * 2);
			BackgroundColor = UIColor.Clear;


			_unreadBadgeView = new BadgeView ();

			Add (_lbl);
			Add (_unreadBadgeView);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			int offset = 2;

			var size = _lbl.SizeThatFits (new CGSize (128, 128));

			_lbl.Frame = new CGRect (Padding, Bounds.Height - Padding - whiteSecHeight, NMath.Max (whiteSecMinWidth, size.Width) + whiteSecHPadding, whiteSecHeight);

			_unreadBadgeView.Center = new CGPoint (_lbl.Frame.Right + offset, _lbl.Frame.Top + offset);

		}

		public void SetState (int cotentCount, int badge = 0)
		{
			_lbl.Text = Math.Max (0, cotentCount).ToString ("N0");
			var size = _lbl.SizeThatFits (new CGSize (128, 128));

			Bounds = new CGRect (0, 0, Padding * 2 + whiteSecHPadding * 2 + NMath.Max (whiteSecMinWidth, size.Width) * 2, Padding * 2 + whiteSecHeight * 2);

			_unreadBadgeView.Hidden = badge <= 0;
			_unreadBadgeView.SetState (badge);
			SetNeedsLayout ();
		}
	}
}

