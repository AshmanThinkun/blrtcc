using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class NewBlrtButton :BlrtButton
	{
		const float TitleOffset = 2;

		public enum ColorScheme
		{
			BlackWithGreen,
			GreenWithBlack
		}
		 
		ColorScheme _scheme;

		private NewBlrtButton (ColorScheme scheme, string logo = "btn_logo.png")
			: base()
		{
			_scheme = scheme;

			SetImage (UIImage.FromBundle (logo).ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal);
			ContentEdgeInsets = new UIEdgeInsets (4, 10, 4, 10);
			TitleEdgeInsets = new UIEdgeInsets(TitleOffset, 0, -TitleOffset, 0);

			Font = UIFont.BoldSystemFontOfSize (18);

			AdjustsImageWhenHighlighted = false;

			Layer.CornerRadius = 2;

			StateChanged (BlrtButtonState.None);
		}

		public override void TintColorDidChange ()
		{
			base.TintColorDidChange ();
			TintAdjustmentMode = UIViewTintAdjustmentMode.Normal;
		}

		public override void StateChanged(BlrtButtonState state)
		{
			switch(_scheme){
				case ColorScheme.BlackWithGreen:
					if (state == BlrtButtonState.Hover)
						BackgroundColor = BlrtStyles.iOS.CharcoalShadow;
					else
						BackgroundColor = BlrtStyles.iOS.Charcoal;
					TintColor = BlrtStyles.iOS.Green;
					SetTitleColor (BlrtStyles.iOS.Green, UIControlState.Normal);
					break;
				case ColorScheme.GreenWithBlack:
					if (state == BlrtButtonState.Hover)
						BackgroundColor = BlrtStyles.iOS.GreenHighlight;
					else
						BackgroundColor = BlrtStyles.iOS.Green;
					TintColor = BlrtStyles.iOS.Charcoal;
					SetTitleColor (BlrtStyles.iOS.Charcoal, UIControlState.Normal);

					break;
			}
		}



		public static UIBarButtonItem CreateBarBlrt(Action action){

			return new UIBarButtonItem (CreateBlrt (action));
		}
		public static UIBarButtonItem CreateBarNewBlrt(Action action){

			return new UIBarButtonItem (CreateNewBlrt (action));
		}


		public static UIButton CreateBlrt(Action action){
			var btn = new NewBlrtButton (ColorScheme.BlackWithGreen);
			btn.SizeToFit ();

			btn.TouchUpInside += (object sender, EventArgs e) => {
				if(action != null)
					action.Invoke();
			};

			return btn;
		}

		public static UIButton CreateNewBlrt(Action action, 
		                                     ColorScheme colorScheme = ColorScheme.BlackWithGreen,
		                                     bool ShouldShowShadows = false)
		{
			var btn = new NewBlrtButton (colorScheme);
			btn.SetTitle ("New ", UIControlState.Normal);

			btn.SizeToFit ();
			btn.TitleEdgeInsets = new UIEdgeInsets(0, -btn.ImageView.Frame.Width, -0, btn.ImageView.Frame.Width);
			btn.ImageEdgeInsets = new UIEdgeInsets(0, btn.TitleLabel.Frame.Width, 0, -btn.TitleLabel.Frame.Width);

			if (ShouldShowShadows) {
				SetShadowForBtn (btn);
			}

			btn.TouchUpInside += (object sender, EventArgs e) => {
				if(action != null)
					action.Invoke();
			};

			return btn;
		}

		static void SetShadowForBtn (UIButton btn)
		{
			btn.Layer.ShadowRadius = 0;
			btn.Layer.ShadowOffset = new CoreGraphics.CGSize (2, 2);
			btn.Layer.ShadowOpacity = 1;
			btn.Layer.ShadowColor = BlrtStyles.iOS.GreenShadow.CGColor;
		}

		public static UIButton CreateReply(Action action){
			var btn = CreateBlrtBtnForPostText (action);
			btn.SetTitle (" Reply", UIControlState.Normal);
			return btn;
		}

		static UIButton CreateBlrtBtnForPostText (Action action)
		{
			var btn = new NewBlrtButton (ColorScheme.GreenWithBlack);

			btn.SizeToFit ();
			var ori = btn.TitleEdgeInsets;
			ori.Top -= 2;  //fix "Reply" position
			btn.TitleEdgeInsets = ori;

			SetShadowForBtn (btn);

			btn.TouchUpInside += (object sender, EventArgs e) => {
				if (action != null)
					action.Invoke ();
			};

			return btn;
		}

		public static UIButton CreateBlrtThisBtn (Action action)
		{
			var btn = CreateBlrtBtnForPostText(action);
			btn.SetTitle (" this", UIControlState.Normal);
			return btn;
		}

		public static UIButton SendButton(Action action){
			var btn = new NewBlrtButton (ColorScheme.GreenWithBlack, "add_user.png");
			btn.SetTitle ("Add", UIControlState.Normal);

			btn.Frame = new CoreGraphics.CGRect (0, 0, 107, 30);
			var ori = btn.TitleEdgeInsets;
			ori.Top -= 2;  //fix "Add" position
			ori.Left += 2;
			btn.TitleEdgeInsets = ori;

			var imageInset = btn.ImageEdgeInsets;
			imageInset.Right += 3;
			btn.ImageEdgeInsets = imageInset;

			btn.Layer.ShadowRadius = 0;
			btn.Layer.ShadowOffset = new CoreGraphics.CGSize (2, 2);
			btn.Layer.ShadowOpacity = 1;
			btn.Layer.ShadowColor = BlrtStyles.iOS.GreenShadow.CGColor;



			btn.TouchUpInside += (object sender, EventArgs e) => {
				if(action != null)
					action.Invoke();
			};

			return btn;
		}
	}
}

