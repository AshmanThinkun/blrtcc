using System;
using Foundation;
using UIKit;
using BlrtData;

namespace BlrtiOS.Views.CustomUI
{
	public class CloudStateView : BlrtButton
	{
		private UIImageView _image;
		private UIActivityIndicatorView _activity;

		CloudState _state;

		public CloudState CloudState { get { return _state; } set { SetState(value); } }



		public CloudStateView ()
			: base()
		{
			Frame = new CoreGraphics.CGRect (0, 0, 48, 48);
			_image = new UIImageView () {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				ContentMode = UIViewContentMode.Center,
				Frame = this.Bounds
			};
			AddSubview (_image);

			_activity = new UIActivityIndicatorView ( UIActivityIndicatorViewStyle.Gray ) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Frame = this.Bounds,
				HidesWhenStopped = false
			};
			AddSubview (_activity);

			SetState (_state);
		}

		public void TryShowingLoadingIndicator ()
		{
			if (_state == CloudState.ChildrenNotOnCloud || 
			    _state == CloudState.NotOnCloud || 
			    _state == CloudState.UploadingFailed) {
				SetState (CloudState.UploadingContent);
			}
		}

		void SetState(CloudState state)
		{
			_state = state;

			switch (_state) {
				case CloudState.ChildrenNotOnCloud:
				case CloudState.NotOnCloud:
					_activity.Hidden = true;
					_image.Hidden = false;
					_image.Image = UIImage.FromBundle ("cloud_inbox_alert.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
					_image.TintColor = BlrtStyles.iOS.AccentRed;

					break;
				case CloudState.UploadingFailed:
					_activity.Hidden = true;
					_image.Hidden = false;
					_image.Image = UIImage.FromBundle ("large_send_fail.png");

					break;
				case CloudState.ContentMissing:
					_activity.Hidden = true;
					_image.Hidden = false;
					_image.Image = UIImage.FromBundle ("cloud_inbox_half.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate);
					_image.TintColor = BlrtStyles.iOS.Gray4;

					break;
				case CloudState.Synced:
					_activity.Hidden = true;
					_image.Hidden = false;
					_image.Image = null;
					_image.TintColor = BlrtStyles.iOS.BlueGray;

					break;
				case CloudState.UploadingContent:
				case CloudState.DownloadingContent:
					_activity.Hidden = false;
					_image.Hidden = true;
					_activity.StartAnimating ();

					break;
			}
		}
	}
}

