using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class CellButton : BlrtButton
	{
		public CellButton ()
			: base()
		{
			TitleLabel.Font = BlrtStyles.iOS.CellTitleFont;
			TitleLabel.TextAlignment = UITextAlignment.Center;

			SetTitleColor(BlrtStyles.iOS.Charcoal, UIControlState.Normal);
			TintColor = BlrtStyles.iOS.Charcoal;
		}

		public override void SetImage (UIImage image, UIControlState forState)
		{
			if(image != null)
				base.SetImage (image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState);
			else
				base.SetImage (image, forState);
		}

		public override void StateChanged (BlrtButtonState state)
		{
			base.StateChanged (state);

			UIView.Animate (0.2f, () => {
				switch (state) {
				case BlrtButtonState.Hover:
					Alpha = 0.6f;
					break;
				default:
					Alpha = 1;
					break;
				}
			});
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			const float LayoutMargins = 4;

			var size = TitleLabel.SizeThatFits (new CoreGraphics.CGSize (Bounds.Width, Bounds.Height));

			TitleLabel.Frame = new CoreGraphics.CGRect (
				0,
				Bounds.Height * 0.5f + LayoutMargins,
				Bounds.Width,
				size.Height
			);
			if (ImageView != null) {
				ImageView.Center = new CoreGraphics.CGPoint (
					Bounds.Width * 0.5f,
					Bounds.Height - TitleLabel.Center.Y
				);
			}
		}
	}
}

