using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class BlrtCommentView : UIView, IUITextViewDelegate
	{
		UIButton SendBtn { get; set; }
		CommentFieldView CommentArea { get; set; }

		UIImage SendButtonImage {
			get {
				return UIImage.FromBundle ("audio_send_up.png");
			}
		}

		public bool Enabled {
			set {
				SendBtn.Enabled = CommentArea.UserInteractionEnabled = value;
			}
		} 

		public UIEdgeInsets Padding { get; set; }
		public float Gap { get; set; }

		public event EventHandler TextChanged;
		public event EventHandler TextStarted;
		public event EventHandler TextEnded;

		public event EventHandler SendPressed {
			add { SendBtn.TouchUpInside += value; }
			remove { SendBtn.TouchUpInside -= value; }
		}

		public string Comment { 
			get { return CommentArea.Text.Trim (); }
			set {
				if (CommentArea.Text != value) {
					CommentArea.Text = value;
					OnTextChange (this, EventArgs.Empty);
				}
			}
		}

		public BlrtCommentView ()
		{
			Padding = new UIEdgeInsets (12, 8, 12, 8);
			Gap = 8;

			Layer.CornerRadius = 4;
			Layer.BorderWidth = 1;
			Layer.BorderColor = BlrtStyles.iOS.Gray5.CGColor;

			//			TextContainerInset = new UIEdgeInsets(4,5,4,5);
			CommentArea = new CommentFieldView ();
			CommentArea.Changed += OnTextChange;
			CommentArea.Started += (object sender, EventArgs e) => { 
				Layer.BorderColor = BlrtStyles.iOS.Green.CGColor;
				if (TextStarted != null) {
					TextStarted (sender, e);
				}
			};
			CommentArea.Ended += (object sender, EventArgs e) => { 
				Layer.BorderColor = BlrtStyles.iOS.Gray5.CGColor;
				if (TextEnded != null) {
					TextEnded (sender, e);
				}
			};

			AddSubview (CommentArea);

			SendBtn = new UIButton ();

			SendBtn.SetImage (SendButtonImage, UIControlState.Normal);
			SendBtn.Hidden = true;
			SendBtn.Enabled = false;
			AddSubview (SendBtn);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var w = size.Width;

			size.Width -= Padding.Right + Padding.Left;
			size.Height -= Padding.Top + Padding.Bottom;

			var s1 = SendBtn.SizeThatFits (size);
			size.Width -= s1.Width + Gap;
			int maxHeight = BlrtHelperiOS.IsPhone ? 94 * 2 : 94 * 4;
			return new CGSize (
				w,
				NMath.Min (NMath.Max (s1.Height, CommentArea.ContentSize.Height <= 0 ? 41: CommentArea.ContentSize.Height), maxHeight) //max height for this element
			);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var edge = new UIEdgeInsets (
				Padding.Top,
				Padding.Left,
				Padding.Bottom,
				Padding.Right + Gap + SendBtn.Bounds.Width
			);

			if (edge.Top != CommentArea.TextContainerInset.Top
				|| edge.Left != CommentArea.TextContainerInset.Left
				|| edge.Bottom != CommentArea.TextContainerInset.Bottom
				|| edge.Right != CommentArea.TextContainerInset.Right) {
				CommentArea.TextContainerInset = edge;
			}

			if (CommentArea.Frame != Bounds) {
				CommentArea.Frame = Bounds;

				// value 4 is to cover entire height. Not sure where this height of extra 4 is coming from.
				var newRect = new CGRect (Bounds.Right - (SendBtn.ImageView.Image?.Size.Width ?? 0) - Padding.Top,
				                          Bounds.Bottom - (SendBtn.ImageView.Image?.Size.Height ?? 0) - Padding.Top - 4,
				                          (SendBtn.ImageView.Image?.Size.Width ?? 0) + Padding.Top,
				                          (SendBtn.ImageView.Image?.Size.Height ?? 0) + Padding.Top + 4);

				if (SendBtn.Frame != newRect) {
					SendBtn.Frame = newRect;
				}
			}

		}

		void OnTextChange (object sender, EventArgs args)
		{
			if (string.IsNullOrWhiteSpace (CommentArea.Text)) {
				SendBtn.Hidden = true;
				SendBtn.Enabled = false;
				CommentArea.ContentSize = CommentArea.SizeThatFits (new CGSize (CommentArea.Frame.Width, int.MaxValue));
				SetNeedsLayout ();
			} else {
				SendBtn.Hidden = false;
				SendBtn.Enabled = true;
			}

			if (TextChanged != null)
				TextChanged (this, args);
			
			CommentArea.ContentOffset = new CGPoint (0, NMath.Min (CommentArea.ContentOffset.Y, CommentArea.ContentSize.Height - CommentArea.Frame.Height));
		}

		class CommentFieldView : UITextView
		{
			protected UILabel placeHolderLbl;

			public CommentFieldView ()
			{
				placeHolderLbl = new UILabel (Bounds) {
					Lines = 1,
					UserInteractionEnabled = false,
					Enabled = false
				};

				AddSubview (placeHolderLbl);

				PlaceHolder = BlrtHelperiOS.Translate ("comment_placeholder", "conversation_screen");
				BackgroundColor = BackgroundColor;
				                 
				PlaceHolderColor = BlrtConfig.BLRT_DARK_GRAY;
				Editable = true;

				Font = BlrtConfig.BASE_TEXT_FONT;

				Changed += OnTextChange;
			}

			void OnTextChange (object sender, EventArgs args)
			{
				placeHolderLbl.Hidden = !string.IsNullOrEmpty (base.Text);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var rect = TextContainerInset.InsetRect (Bounds);
				var size = placeHolderLbl.SizeThatFits (new CGSize (rect.Width, nfloat.MaxValue));
				placeHolderLbl.Frame = new CGRect (rect.X + 4, (Bounds.Height - size.Height) * 0.5f, NMath.Max (rect.Width - 4, 0), size.Height);
				//			placeHolderLbl.Frame = new RectangleF(rect.X+4, rect.Y, Math.Max (rect.Width-4,0), size.Height);

			}

			public string PlaceHolder {
				get {
					return placeHolderLbl.Text;
				}
				set {
					if (placeHolderLbl.Text != value) {
						placeHolderLbl.Text = value;
						SetNeedsLayout ();
					}
				}
			}

			public UIColor PlaceHolderColor {
				get {
					return placeHolderLbl.TextColor;
				}
				set {
					placeHolderLbl.TextColor = value;
				}
			}

			public override UIFont Font {
				get {
					return base.Font;
				}
				set {
					base.Font = placeHolderLbl.Font = value;
					SetNeedsLayout ();
				}
			}

			public override string Text {
				get {
					return base.Text;
				}
				set {
					base.Text = value;
					placeHolderLbl.Hidden = !string.IsNullOrEmpty (value);
				}
			}
		}
	}
}

