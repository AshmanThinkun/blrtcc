﻿using System;
using BlrtiOS.Views.Conversation;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class BlrtAudioRecordingView : UIView
	{
		const int DEGREE_OF_VOLUME_GRANULARITY = 5;
		const string ERROR_MESSAGE = "Hold to record";
		const string DURING_RECORDING_MESSAGE = "Recording";

		internal static readonly float MINIMUN_LONG_PRESS_DURATION = 0.09f;

		UILabel Instructions { get; set; }
		UIButton MicBtn { get; set; }
		UIButton DuringRecordMicBtn { get; set; }
		UILabel Duration { get; set; }
		UIButton CancelBtn { get; set; }
		UIButton DoneBtn { get; set; }
		UIButton VolumeView { get; set; }

		event EventHandler _startAudioRecording;

		public event EventHandler MicBtnTouchDown {
			add { MicBtn.TouchDown += value; }
			remove { MicBtn.TouchDown -= value; }
		}

		public event EventHandler MicBtnTouchUp {
			add { 
				MicBtn.TouchUpInside += value; 
				MicBtn.TouchUpOutside += value;
			}
			remove {
				MicBtn.TouchUpInside -= value;
				MicBtn.TouchUpOutside -= value;
			}
		}

		public event EventHandler AudioRecordingStarted {
			add { _startAudioRecording += value; }
			remove { _startAudioRecording -= value; }
		}

		public event EventHandler DonePressed {
			add { DoneBtn.TouchUpInside += value; }
			remove { DoneBtn.TouchUpInside -= value; }
		}

		public event EventHandler CancelPressed {
			add { CancelBtn.TouchUpInside += value; }
			remove { CancelBtn.TouchUpInside -= value; }
		}

		public void HandleLongPressGestureBegan(object obj)
		{
			ShowRecordingViews ();

			if (_startAudioRecording != null) {
				_startAudioRecording (obj, null);
			}
		}

		public void HandleLongPressGestureEnd (object obj)
		{
			if (_startAudioRecording != null) {
				_startAudioRecording (obj, null);
			}
		}

		UILongPressGestureRecognizer MicBtnLongPressGestureRecognizer {
			get {
				return new UILongPressGestureRecognizer ((obj) => {
					
					if (UIGestureRecognizerState.Began == obj.State) {
						HandleLongPressGestureBegan (obj);
					}

					if (UIGestureRecognizerState.Ended == obj.State || 
					    UIGestureRecognizerState.Cancelled == obj.State) 
					{
						HandleLongPressGestureEnd (obj);
					}

				}) { MinimumPressDuration = MINIMUN_LONG_PRESS_DURATION };
			}
		}

		UIImage CancelBtnImage {
			get {
				return UIImage.FromBundle ("audio_cross.png");
			}
		}

		UIImage MicBtnImage {
			get {
				return UIImage.FromBundle ("media_audio_light.png");
			}
		}

		UIImage DoneBtnImage {
			get {
				return UIImage.FromBundle ("audio_send_up.png");
			}
		}

		UIImage DuringRecordMicBtnImg {
			get {
				return UIImage.FromBundle ("audio_mic_record_red.png");
			}
		}

		UIImage PostRecordMicBtnImg {
			get {
				return UIImage.FromBundle ("audio_mic_record_dark.png");
			}
		}

		UIImage VolumeZeroImage {
			get {
				return UIImage.FromBundle ("audio_volume_0.png");
			}
		}

		UIImage VolumeOneImage {
			get {
				return UIImage.FromBundle ("audio_volume_1.png");
			}
		}

		UIImage VolumeTwoImage {
			get {
				return UIImage.FromBundle ("audio_volume_2.png");
			}
		}

		UIImage VolumeThreeImage {
			get {
				return UIImage.FromBundle ("audio_volume_3.png");
			}
		}

		UIImage VolumeFourImage {
			get {
				return UIImage.FromBundle ("audio_volume_4.png");
			}
		}

		UIColor PrimaryColor {
			get {
				return BlrtStyles.iOS.WhiteAlpha.ColorWithAlpha (0.0f);
			}
		}

		UIColor SecondaryColor {
			get {
				return BlrtStyles.iOS.Gray5;
			}
		}

		CoreAnimation.CAGradientLayer ColorGradient {
			get {
				return new CoreAnimation.CAGradientLayer () {
					Colors = new CoreGraphics.CGColor [] { PrimaryColor.CGColor, SecondaryColor.CGColor },
					StartPoint = new CoreGraphics.CGPoint (1.0f, 0.0f),
					EndPoint = new CoreGraphics.CGPoint (1.0f, 1.0f)
				};
			}
		}

		public BlrtAudioRecordingView ()
		{
			BackgroundColor = PrimaryColor; // Gradient requires transparent background color
			Frame = new CGRect (0,0,0,0);
			Layer.BorderWidth = 0;
			Layer.InsertSublayer (ColorGradient, 0);

			VolumeView = new UIButton () {
				Hidden = true,
				UserInteractionEnabled = false		
			};
			VolumeView.SetImage (VolumeZeroImage, UIControlState.Normal);

			Instructions = new UILabel () {
				Text = ERROR_MESSAGE,
				Font = BlrtStyles.iOS.CellTitleFont,
				TextColor = BlrtStyles.iOS.AccentRedShadow,
				Enabled = true,
				UserInteractionEnabled = false,
				Lines = 1
			};

			Duration = new UILabel () {
				Text = "00:00",
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				TextColor = BlrtStyles.iOS.Gray7,
				Enabled = false,
				Hidden = true,
				UserInteractionEnabled = false,
				Lines = 1
			};

			MicBtn = new UIButton () {
				Enabled = true,
				UserInteractionEnabled = true
			};

			CancelBtn = new UIButton () {
				Enabled = true,
				UserInteractionEnabled = true,
				Hidden = true,
			};

			CancelBtn.TouchUpInside += ResetDefaultViews;

			DoneBtn = new UIButton () {
				Enabled = true,
				UserInteractionEnabled = true,
				Hidden = true
			};

			DoneBtn.TouchUpInside += ResetDefaultViews;

			DuringRecordMicBtn = new UIButton () {
				Enabled = false,
				Hidden = true,
				UserInteractionEnabled = false
			};

			CancelBtn.SetImage (CancelBtnImage, UIControlState.Normal);
			DoneBtn.SetImage (DoneBtnImage, UIControlState.Normal);
			MicBtn.SetImage (MicBtnImage, UIControlState.Normal);
			DuringRecordMicBtn.SetImage (DuringRecordMicBtnImg, UIControlState.Normal);

			MicBtn.AddGestureRecognizer (MicBtnLongPressGestureRecognizer);

			AddSubviews (new UIView [] {
				Instructions,
				MicBtn,
				DuringRecordMicBtn,
				Duration,
				CancelBtn,
				DoneBtn,
				VolumeView
			});
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			// Frame for gradient layer
			var newRect = Bounds;
			if(!newRect.Equals(Layer.Sublayers [0].Frame))
				Layer.Sublayers [0].Frame = newRect;

			// Instructions
			var size = Instructions.SizeThatFits (Bounds.Size);
			var paddingHorizontal = (Bounds.Width - size.Width) * 0.5;
			var paddingVertical = (Bounds.Height - size.Height) * 0.5;

			newRect = new CGRect (paddingHorizontal,
											 paddingVertical,
											 size.Width,
											 size.Height);

			if (!newRect.Equals (Instructions.Frame))
				Instructions.Frame = newRect;

			// Mic button
			var Width = ConversationFooterView.BUTTON_HEIGHT;
			newRect = new CGRect (Bounds.Right - Width - 5,
			                      Bounds.Top,
									   Width, //MicBtn.ImageView.Image.Size.Width + 20,
									   Bounds.Height);

			if (!newRect.Equals (MicBtn.Frame))
				MicBtn.Frame = newRect;

			// Done Button
			newRect = new CGRect (Bounds.Right - Width - 4,
									   Bounds.Top,
									   Width, //MicBtn.ImageView.Image.Size.Width + 20,
									   Bounds.Height);

			if (!newRect.Equals (DoneBtn.Frame))
				DoneBtn.Frame = newRect;

			// Cancel button
			size = CancelBtn.SizeThatFits (Bounds.Size);

			newRect = new CGRect (Bounds.GetMidX () - size.Width * 0.5,
			                      	   Bounds.Top,
			                      	   size.Width,
									   Bounds.Height);

			if (!newRect.Equals (CancelBtn.Frame))
				CancelBtn.Frame = newRect;


			// DuringRecordMicBtn button
			newRect = new CGRect (Bounds.Left - 2,
									   Bounds.Top,
									   Width,
									   Bounds.Height);

			if (!newRect.Equals (DuringRecordMicBtn.Frame))
				DuringRecordMicBtn.Frame = newRect;

			// VolumeView
			newRect = new CGRect (DuringRecordMicBtn.ImageView.Frame.GetMidX(),
									   Bounds.Top,
									   Width,
									   Bounds.Height);

			if (!newRect.Equals (VolumeView.Frame))
				VolumeView.Frame = newRect;


			// Duration
			size = Duration.SizeThatFits (Bounds.Size);
			paddingVertical = (Bounds.Height - size.Height) * 0.5;
			newRect = new CGRect (VolumeView.Hidden ? DuringRecordMicBtn.Frame.Right : VolumeView.Frame.Right,
								 paddingVertical,
								 size.Width + 15,
								 size.Height);
			
			if (!newRect.Equals (Duration.Frame))
				Duration.Frame = newRect;

		}

		public void SetDuration (float duration)
		{
			var seconds = Math.Floor (duration);
			var minString = ((int)Math.Floor (seconds / 60)).ToString ("D2");
			var secondsString = ((int)(seconds % 60)).ToString ("D2");

			Duration.Text = string.Format ("{0}:{1}", minString, secondsString);
		}

		public void SetVolume (float volume)
		{
			var step = 1.0f / DEGREE_OF_VOLUME_GRANULARITY;

			if (Math.Abs (volume) < float.Epsilon) {
				VolumeView.SetImage (VolumeZeroImage, UIControlState.Normal);
			}
			else if (volume <= step) {
				VolumeView.SetImage (VolumeOneImage, UIControlState.Normal);
			}
			else if (volume <= step * 2) {
				VolumeView.SetImage (VolumeTwoImage, UIControlState.Normal);
			}
			else if (volume <= step * 3) {
				VolumeView.SetImage (VolumeThreeImage, UIControlState.Normal);
			}
			else {
				VolumeView.SetImage (VolumeFourImage, UIControlState.Normal);
			}
		}

		public void ResetDefaultViews (object s, EventArgs e)
		{
			Instructions.Text = ERROR_MESSAGE;
			Instructions.Hidden = false;
			DuringRecordMicBtn.Hidden = true;
			Duration.Hidden = true;
			Duration.Text = "00:00";
			CancelBtn.Hidden = true;
			DoneBtn.Hidden = true;
			MicBtn.Hidden = false;
			VolumeView.Hidden = true;
			SetNeedsLayout ();
		}

		void ShowRecordingViews ()
		{
			Instructions.Text = DURING_RECORDING_MESSAGE;
			DuringRecordMicBtn.SetImage (DuringRecordMicBtnImg, UIControlState.Normal);
			DuringRecordMicBtn.Hidden = false;
			Duration.Hidden = false;
			VolumeView.Hidden = false;
			SetNeedsLayout ();
		}

		public void ShowPostRecordingViews ()
		{
			Instructions.Hidden = true;
			MicBtn.Hidden = true;
			CancelBtn.Hidden = false;
			DoneBtn.Hidden = false;
			VolumeView.Hidden = true;
			DuringRecordMicBtn.SetImage (PostRecordMicBtnImg, UIControlState.Normal);
			SetNeedsLayout ();
		}
	}
}
