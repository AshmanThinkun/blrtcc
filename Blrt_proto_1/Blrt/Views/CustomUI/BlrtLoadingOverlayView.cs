using System;
using CoreGraphics;

using Foundation;
using UIKit;
using System.Collections.Generic;

namespace BlrtiOS.Views.CustomUI
{
	public class BlrtLoadingOverlayView : UIView{
		
		UIActivityIndicatorView _progress;

		private bool _visable;
		private bool _animating;
		private bool _matchParentSize;


		public CGPoint _offset;

		private List<string> _counter;

		public float AnimationLength { get; set; }

		public override bool Hidden {
			get {
				return !_visable;
			}
			set {
				if (!value != _visable) {
					ChangeVisible (!value, false);
				}
			}
		}


		public BlrtLoadingOverlayView()
			: base() 
		{
			UserInteractionEnabled = true;
			_matchParentSize = true;
			BackgroundColor = new UIColor (0, 0, 0, 0.5f);

			AnimationLength = 0.4f;
			Layer.ZPosition = 100;

			AutoresizingMask = UIViewAutoresizing.All;

			_progress = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.White) {
			};
			this.AddSubview (_progress);
			base.Alpha = 0;

			_counter = new List<string> ();
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (Superview != null && _matchParentSize) {
				Frame = Superview.Bounds;
			}
			_progress.Center = new CGPoint ((int)(Bounds.Width * 0.5f + _offset.X), (int)(Bounds.Height * 0.5f + _offset.Y));
		}


		private void ChangeVisible(bool state, bool animate){
			if (_visable != state) {

				_visable = state;

				if (animate) {
					_animating = true;
					Animate (AnimationLength, 
						() => {
							if (!_visable) {
								Alpha = 0;
							} else {
								_progress.StartAnimating ();
								Alpha = 1;
							}
						}, () => {
						_animating = false;
					});
				} else {
					Alpha = _visable ? 1 : 0;

					if(_visable)
						_progress.StartAnimating ();
					else
						_progress.StopAnimating ();
				}
			}
			SetNeedsLayout ();
		}

		public override bool PointInside (CGPoint point, UIEvent uievent)
		{
			if (!_visable || _animating)
				return false;

			return base.PointInside (point, uievent);
		}

		public void Hide (bool animated)
		{
			Pop ("_selfCaused", animated);
		}

		public void Show (bool animated)
		{
			Push ("_selfCaused", animated);
		}

		public void Show (bool animated, CGPoint offset)
		{
			_offset = offset;
			Push ("_selfCaused", animated);
		}


		public virtual void Push (string cause, bool animated) {

			if (!_counter.Contains (cause))
				_counter.Add (cause);
			ChangeVisible (_counter.Count > 0, animated);
		}
		public virtual void Pop (string cause, bool animated) {

			if (_counter.Contains (cause))
				_counter.Remove (cause);
			ChangeVisible (_counter.Count > 0, animated);
		}


		public static BlrtLoadingOverlayView CreateMicroLoader (CGRect bounds)
		{
			var v = new BlrtLoadingOverlayView (){
				Frame = new CGRect ((bounds.Width - 128) * 0.5f, (bounds.Height - 128) * 0.5f, 128, 128),
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Layer = {
					CornerRadius = 16
				},
				_matchParentSize = false,
			};

			return v;
		}
	}
}

