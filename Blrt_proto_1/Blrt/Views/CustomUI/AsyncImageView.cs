using System;
using System.IO;
using System.Threading.Tasks;
using BlrtData;
using CoreGraphics;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class AsyncImageView : UIImageView
	{
		/// <summary>
		/// The error margin. It is between error image and label.
		/// </summary>
		readonly byte ERROR_MARGIN = 10;

		/// <summary>
		/// Gets or sets the error label. It is only shown if the thubnail data is corrupt.
		/// </summary>
		/// <value>The error label.</value>
		UILabel ErrorLabel { get; set; }

		/// <summary>
		/// Gets the error label text.
		/// </summary>
		/// <value>The error label text.</value>
		string ErrorLabelText { 
			get {
				return "Can't display this item";
			} 
		}

		/// <summary>
		/// Gets or sets the error image view. It is only shows when error needs to be shown.
		/// </summary>
		/// <value>The error image view.</value>
		UIImageView ErrorImageView { get; set; }

		/// <summary>
		/// Gets the error image source.
		/// </summary>
		/// <value>The error image source.</value>
		UIImage ErrorImageSrc {
			get {
				return UIImage.FromFile ("error_sad.png");
			} 
		}

		/// <summary>
		/// Gets the color of the error view background.
		/// </summary>
		/// <value>The color of the error view background.</value>
		UIColor ErrorViewBackgroundColor { 
			get {
				return BlrtStyles.iOS.Charcoal;
			}
		}

		public override UIImage Image {
			get {
				return base.Image;
			}
			set {
                this.ContentMode = UIViewContentMode.ScaleAspectFill;
				if (null != base.Image) {
					var releasingImg = base.Image;
					base.Image = value;
					releasingImg.Dispose ();
					releasingImg = null;
				} else {
					base.Image = value;
				}
			}
		}
		int _currentImgSrcId = 0;
		bool _isReleased = true;

		public AsyncImageView ()
		{
			this.ClipsToBounds = true;
			ErrorLabel = new UILabel {
				BackgroundColor = ErrorViewBackgroundColor,
				Font = BlrtStyles.iOS.CellContentFont, 
				TextColor = BlrtStyles.iOS.White,
				TextAlignment = UITextAlignment.Center,
				Hidden = true
			};

			ErrorImageView = new UIImageView () {
				BackgroundColor = ErrorViewBackgroundColor,
				Hidden = true
			};

			AddSubview (ErrorLabel);
            AddSubview (ErrorImageView);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (!ErrorLabel.Hidden && !ErrorImageView.Hidden) {
				SetErrorImageSrc ();
				SetErrorText ();

				var totalErrorViewHeight = ErrorImageView.Frame.Height;
				totalErrorViewHeight += ErrorLabel.Frame.Height;
				totalErrorViewHeight += ERROR_MARGIN;

				var y = (Frame.Height - totalErrorViewHeight) * 0.5f;

				var width = ErrorImageView.Frame.Width;
				var boundsMidX = Bounds.GetMidX ();

				var errorImageViewFrame = new CGRect (boundsMidX - width * 0.5f, y, width, ErrorImageView.Frame.Height);

				if (!ErrorImageView.Frame.Equals (errorImageViewFrame)) {
					ErrorImageView.Frame = errorImageViewFrame;

					width = ErrorLabel.Frame.Width;
					ErrorLabel.Frame = new CGRect (boundsMidX - width * 0.5f,
					                               ErrorImageView.Frame.Bottom + ERROR_MARGIN, 
					                               ErrorLabel.Frame.Width, 
					                               ErrorLabel.Frame.Height);
				}
			}
		}

		/// <summary>
		/// Sets the error label text.
		/// </summary>
		void SetErrorText ()
		{
			if (string.IsNullOrEmpty (ErrorLabel.Text)) {
				ErrorLabel.Text = ErrorLabelText;
				ErrorLabel.SizeToFit ();
			}
		}

		/// <summary>
		/// Sets the error image source.
		/// </summary>
		void SetErrorImageSrc ()
		{
			if (ErrorImageView.Image == null) {
				ErrorImageView.Image = ErrorImageSrc;
				ErrorImageView.SizeToFit ();
			}
		}

		public void SetAsyncSource (Uri cloudPath, string path)
		{
			_isReleased = false;
			++_currentImgSrcId;
			UIImage targetImg = null;

			ErrorLabel.Hidden = ErrorImageView.Hidden = true;
			if (File.Exists (path)) {
				targetImg = UIImage.FromFile (path);
			} else {
				targetImg = null;
			}

			if (null == targetImg) {
				if (cloudPath != null) {
					Image = UIImage.FromBundle ("logo-placeholder.png");
					ErrorImageView.Image?.Dispose ();
					ErrorImageView.Image = null;
					DownloadFile (cloudPath, path, _currentImgSrcId).FireAndForget ();
				} else {
					Image?.Dispose ();
					Image = null;
					ErrorLabel.Hidden = ErrorImageView.Hidden = false;
					BackgroundColor = ErrorViewBackgroundColor;
					SetNeedsLayout ();


				}
			} else {
				Image = targetImg;
			}

			targetImg = null;
		}

		public void ReleaseImage ()
		{
			_isReleased = true;
			Image = null; // real releasing code already in the setter
		}

		async Task DownloadFile (Uri cloudPath, string path, int srcId)
		{
			var downloader = BlrtData.IO.BlrtDownloader.Download (4, cloudPath, path);
			try {
				await downloader.WaitAsync ();
				if (_currentImgSrcId == srcId) {
					InvokeOnMainThread (() => {
						if (!_isReleased && (_currentImgSrcId == srcId)) {
							Image = UIImage.FromFile (path);
						}
					});
				}
			} catch (System.Net.WebException ex) {
				// TODO: throw when download failed
			} catch (Exception other) {
				LLogger.WriteEvent ("AsyncImageView", "DownloadFile", "Exception", other.ToString ());
			}
		}
	}
}

