using System;
using CoreGraphics;
using System.Collections.Generic;


using UIKit;
using Foundation;

namespace BlrtiOS.View.CustomUI
{
	public class BlrtStackPanel : UIView
	{
		private const string BOUNDS = "bounds";
		private const string HIDDEN = "hidden";

		private ViewObserver observer;
		private bool _resizing;

		public event EventHandler ContentSizeChanged;

		public CGSize ContentSize { get; private set; }

		public BlrtStackPanel (CGRect frame) : base (frame)
		{
			observer = new ViewObserver (this);
		}

		public BlrtStackPanel ()
		{
			observer = new ViewObserver (this);
		}

		public override void AddSubview (UIView view)
		{
			base.AddSubview (view);
			RepositionViews ();

			view.AddObserver (observer, new NSString (BOUNDS), (NSKeyValueObservingOptions)0, IntPtr.Zero);
			view.AddObserver (observer, new NSString (HIDDEN), (NSKeyValueObservingOptions)0, IntPtr.Zero);
		}

		public override void WillRemoveSubview (UIView uiview)
		{
			base.WillRemoveSubview (uiview);
			uiview.RemoveObserver (observer, new NSString (BOUNDS));
			uiview.RemoveObserver (observer, new NSString (HIDDEN));

			RepositionViews ();
		}


		private void RepositionViews ()
		{

			if (_resizing)
				return;
			_resizing = true;

			nfloat delta = 0;

			for (int i = 0; i < Subviews.Length; ++i) {

				var subview = Subviews [i];
				if (!subview.Hidden) {
					
					var newSize = subview.SizeThatFits (new CGSize(Bounds.Width, nfloat.MaxValue));

					// Because of the bug in iOS 11, we need to calculate the height manually.
					if (subview is UITableView) {
						var table = subview as UITableView;
						var rowsInSecondSection = table.NumberOfRowsInSection (1);


						var totalHeight = Views.Send.AddContactTableView.ContactRowHeight * (table.NumberOfRowsInSection (0) + rowsInSecondSection);

						if (rowsInSecondSection > 0) {
							totalHeight += 44;
						}
						 
						newSize.Height = totalHeight;
					}

					var newFrame = new CGRect (new CGPoint(subview.Frame.X, delta), newSize);

					if (!subview.Frame.Equals (newFrame)) {
						subview.Frame = newFrame;
					}

					delta = subview.Frame.Bottom;
				}
			}

			var size = new CGSize (Bounds.Width, delta);
			_resizing = false;

			if (ContentSize.Width != size.Width || ContentSize.Height != size.Height) {
				ContentSize = size;

				if (ContentSizeChanged != null)
					ContentSizeChanged (this, EventArgs.Empty);
			}
		}



		public override void LayoutSubviews ()
		{
			RepositionViews ();
		}



		private class ViewObserver : NSObject
		{

			BlrtStackPanel _parent;

			public ViewObserver (BlrtStackPanel parent)
			{
				_parent = parent;
			}

			public override void ObserveValue (NSString keyPath, NSObject ofObject, NSDictionary change, IntPtr context)
			{
				var str = keyPath.ToString ();

				if (BOUNDS == str || HIDDEN == str) {
					for (int i = 0; i < _parent.Subviews.Length; ++i) {
						if (_parent.Subviews [i] == ofObject)
							_parent.RepositionViews ();
					}
				}
			}
		}
	}
}

