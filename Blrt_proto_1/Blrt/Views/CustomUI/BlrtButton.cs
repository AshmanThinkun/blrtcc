using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class BlrtButton : UIButton
	{
		public enum BlrtButtonState
		{
			None,
			Down,
			Hover
		}


		public BlrtButtonState ButtonState {
			get;
			private set;
		}

		public BlrtButton ()
			: base()
		{
			ButtonState = BlrtButtonState.None;

			TouchDown			+= GotoHover;
			TouchDragExit		+= GotoDown;
			TouchDragEnter		+= GotoHover;
			TouchUpInside		+= GotoNone;
			TouchUpOutside		+= GotoNone;
			TouchCancel			+= GotoNone;
		}

		void GotoDown (object sender, EventArgs e) {
			SetState (BlrtButtonState.Down);
		}

		void GotoHover (object sender, EventArgs e) {
			SetState (BlrtButtonState.Hover);
		}

		void GotoNone (object sender, EventArgs e) {
			SetState (BlrtButtonState.None);
		}

		void SetState (BlrtButtonState state)
		{
			if (state != ButtonState) {
				ButtonState = state;
				StateChanged (ButtonState);
			}
		}

		public virtual void StateChanged(BlrtButtonState state)
		{

		}
	}
}

