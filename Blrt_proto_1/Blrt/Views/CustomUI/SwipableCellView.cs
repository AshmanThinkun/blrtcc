using System;
using System.Linq;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class SwipableCellView : UITableViewCell
	{
		public event EventHandler SwipeStarted;


		UIView _right;
		UIView _left;

		UIPanGestureRecognizer _pan;

		private nfloat _startX;

		public UIView RightView { get { return _right; } set { SetRightView(value); } }
		public UIView LeftView { get { return _left; } set {SetLeftView(value); } }

		public nfloat Pan { get; set; }

		public bool SwipeDisabled { get; set; }

		public SwipableCellView (UITableViewCellStyle style, string reuseIdentifer)
			:base(style, reuseIdentifer)
		{
			InitView ();
		}

		public SwipableCellView (IntPtr handler)
			:base(handler)
		{
			InitView ();
		}

		void InitView ()
		{
			_pan = new UIPanGestureRecognizer (Panned) {
			};
			_pan.ShouldBegin += (UIGestureRecognizer recognizer) => {
				
				if(!SwipeDisabled && recognizer is UIPanGestureRecognizer){
					var vel = (recognizer as UIPanGestureRecognizer).VelocityInView(this);
					var pos = (recognizer as UIPanGestureRecognizer).LocationInView(this);

					if(Pan == 0 && _left == null && vel.X > 0)
						return false;
					if(Pan == 0 && _right == null && vel.X < 0)
						return false;

					if(pos.X < Pan || pos.X > Pan + Bounds.Width)
						return false;

					if(NMath.Abs(vel.X) > NMath.Abs(vel.Y)){
						return true;
					}
				}
				return false;
			};
			this.AddGestureRecognizer (_pan);
		}

		void Panned(UIPanGestureRecognizer gesture) {
			switch (gesture.State) {
				case UIGestureRecognizerState.Began:
					_startX = gesture.TranslationInView (this).X - Pan;
					if (SwipeStarted != null)
						SwipeStarted.Invoke (this, EventArgs.Empty);


				break;

			case UIGestureRecognizerState.Changed:
				SetPan (gesture.TranslationInView (this).X - _startX, false);
				break;
			case UIGestureRecognizerState.Cancelled:
			case UIGestureRecognizerState.Ended:

				var vel = gesture.VelocityInView (this);

				if (_left != null && Pan + vel.X > _left.Frame.Width)
					SetPan (_left.Frame.Width, true);
				else if (_right != null && Pan + vel.X < -_right.Frame.Width)
					SetPan (-_right.Frame.Width, true);
				else
					SetPan (0, true);
				break;
			}

		}

		public override void PrepareForReuse () {
			base.PrepareForReuse ();
			SetPan (0, false);
		}


		void SetRightView (UIView value)
		{
			if (value != _right) {
			
				if (_right != null)
					_right.RemoveFromSuperview ();

				_right = value;

				if (_right != null)
					AddSubview (_right);
			
				Pan = 0;
				LayoutSubviews ();
			}
		}

		void SetLeftView (UIView value)
		{
			if (value != _left) {

				if (_left != null)
					_left.RemoveFromSuperview ();

				_left = value;

				if (_left != null)
					AddSubview (_left);

				Pan = 0;
				LayoutSubviews ();
			}
		}

		public override UIView HitTest (CoreGraphics.CGPoint point, UIEvent uievent)
		{
			var view = base.HitTest (point, uievent);
		
			if (view != null && ContentView.Frame.Contains (point))
				return this;

			return view;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if(_left != null)
				_left.Frame = new CoreGraphics.CGRect (0, (Bounds.Height - _left.Bounds.Height) * 0.5f, _left.Bounds.Width, _left.Bounds.Height);
			if(_right != null)
				_right.Frame = new CoreGraphics.CGRect (Bounds.Width - _right.Bounds.Width, (Bounds.Height - _right.Bounds.Height) * 0.5f, _right.Bounds.Width, _right.Bounds.Height);

			ContentView.Layer.ZPosition = 10;
			SetPan (Pan, false);
		}

		public void SetPan (nfloat pan, bool animated)
		{
			this.Pan = pan;

			var calPan = pan;

			if (_left != null) {

				if (calPan > _left.Frame.Width) {
					var tmp = (calPan - _left.Frame.Width);
					calPan = _left.Frame.Width + tmp / (1 + NMath.Abs(tmp) * 0.02f);
				}

			} else {
				if(this.Pan > 0)
				calPan = 0;
			}
			if (_right != null) {

				if (calPan < -_right.Frame.Width) {
					var tmp = (calPan + _right.Frame.Width);
					calPan = -_right.Frame.Width + tmp / (1 + NMath.Abs(tmp) * 0.02f);				
				}

			} else {
				if(this.Pan < 0)
					calPan = 0;
			}

			if (animated) {
				UIView.Animate (0.4f, () => {
					ContentView.Frame = new CoreGraphics.CGRect (calPan, ContentView.Frame.Y, ContentView.Frame.Width, ContentView.Frame.Height);
				});
			} else {

				ContentView.Frame = new CoreGraphics.CGRect (calPan, ContentView.Frame.Y, ContentView.Frame.Width, ContentView.Frame.Height);
			}
		}
	}
}

