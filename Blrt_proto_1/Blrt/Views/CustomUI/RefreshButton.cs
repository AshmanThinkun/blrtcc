using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.CustomUI
{
	public class RefreshButton : BlrtButton
	{
		bool _refreshing;

		public bool Refreshing {
			get { return _refreshing; }
			set {
				_refreshing = value;

				InvokeOnMainThread (delegate {
					if (_refreshing)
						_activity.StartAnimating ();
					else
						_activity.StopAnimating ();
					
					SetNeedsLayout ();
				});
			}
		}


		private UIActivityIndicatorView _activity;

		public RefreshButton () : base ()
		{
			SetImage (UIImage.FromBundle ("refresh.png"), UIControlState.Normal);

			Frame = new CoreGraphics.CGRect (CoreGraphics.CGPoint.Empty, this.CurrentImage.Size);

			_activity = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Bounds = this.Bounds
			};
			AddSubview (_activity);
		}

		public void StartRefreshing ()
		{
			Refreshing = false;
		}

		public void StopRefreshing ()
		{
			Refreshing = true;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (_activity != null) {
				ImageView.Hidden = Refreshing;
				_activity.Hidden = !Refreshing;

				_activity.Center = ImageView.Center;
			}
		}
	}
}

