﻿using System.Threading;
using Social;
using Foundation;
using MessageUI;
using BlrtData.ExecutionPipeline;
using BlrtiOS.Views.Settings;
using BlrtData;
using System;
using UIKit;

namespace BlrtiOS.Views.Profile
{
	public partial class ProfileNavigationPage: GenericNavigationController
	{
		private async void FacebookSharePressed(object sender, string args)
		{
			PushLoading ("facebookShare", true);

			var slComposer = SLComposeViewController.FromService (SLServiceType.Facebook);

			slComposer.AddUrl (NSUrl.FromString (args));
			slComposer.SetInitialText (BlrtUtil.PlatformHelper.Translate ("message_facebook", "share"));


			PresentViewController (slComposer, true, ()=>{
				PopLoading("facebookShare",true);
			});
		}

		private async void TwitterSharePressed(object sender, string args)
		{
			PushLoading ("twitterShare", true);

			var slComposer = SLComposeViewController.FromService (SLServiceType.Twitter);

			slComposer.AddUrl (NSUrl.FromString (args));
			slComposer.SetInitialText (BlrtUtil.PlatformHelper.Translate ("message_twitter", "share"));

			PresentViewController (slComposer, true, ()=>{
				PopLoading("twitterShare",true);
			});
		}

		private async void EmailSharePressed(object sender, string args)
		{
			if (MFMailComposeViewController.CanSendMail) {
				PushLoading ("emailShare", true);

				var name = BlrtUserHelper.CurrentUserName;

				var m = new MFMailComposeViewController () {
				};
				m.SetSubject (String.Format (BlrtUtil.PlatformHelper.Translate ("message_email_subject", "share"), name));
				m.SetMessageBody (string.Format (BlrtUtil.PlatformHelper.Translate ("message_email", "share"), name, args, BlrtUtil.GetImpressionHtml("share_email")), true);

				m.Finished += (object s, MFComposeResultEventArgs e) => {
					DismissViewController (true, null);
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				PresentViewController (m, true, ()=>{
					PopLoading("emailShare",true);
				});
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert(
						platforHelper.Translate ("cannot_send_email_error_title"),
						platforHelper.Translate ("cannot_send_email_error_message"),
						platforHelper.Translate ("cannot_send_email_error_cancel")
					)
				);
			}
		}

		private async void SmsSharePressed(object sender, string args)
		{
			if (MFMessageComposeViewController.CanSendText) {

				PushLoading ("smsShare", true);


				var m = new MFMessageComposeViewController () {
					Body = string.Format (BlrtUtil.PlatformHelper.Translate ("message_sms", "share"), args),
				};
				m.Finished += (object s, MFMessageComposeResultEventArgs e) => {
					DismissViewController (true, null);
					if(e.Result == MessageComposeResult.Sent){
						BlrtUtil.TrackImpression("share_sms");
					}
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				PresentViewController (m, true, ()=>{
					PopLoading("smsShare",true);
				});
			} else {

				var alert = new UIAlertView(
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_title", 		"share"), 
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_message", 		"share"),
					null,
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_cancel", 		"share")); 



				alert.Show ();
			}
		}

		private void BigSharseBtn(object sender, string args)
		{
			var url =new NSUrl(args);
			var content = new Facebook.ShareKit.AppInviteContent();
			content.AppLinkURL = url;
			content.PreviewImageURL = new NSUrl ("https://s3.amazonaws.com/blrt-cc-images/share/blrt_share_fbinvite.jpg");

			var aDelegate = new BlrtiOS.Views.Share.ShareNavController.InviteDel();

			Facebook.ShareKit.AppInviteDialog.Show(content,aDelegate);
		}
	}
}

