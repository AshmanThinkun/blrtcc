﻿using BlrtiOS.Views.Settings;
using BlrtData.Views.Settings.Profile;
using BlrtData;
using UIKit;
using Xamarin.Forms;
using System;
using BlrtData.Views.AddPhone;
using System.Linq;
using Parse;

namespace BlrtiOS.Views.Profile
{
	public partial class ProfileNavigationPage: GenericNavigationController
	{
		#region eidtProfile

		ProfileMainPage _profilePage;
		UIViewController profileController;
		void OpenEditProfile (bool animate = true)
		{
			_profilePage = new ProfileMainPage ();
		   profileController = _profilePage.CreateViewController ();
			profileController.Title = _profilePage.Title;

			_profilePage.RowSelected += (sender, e) => {
				UIViewController controller = null;

				var profile = _profilePage.Profile;
				var navText = (e.SelectedItem as CellItem).NavText;

				if (navText == ProfileMainPage.NavText.Email.ToString ()) {
					OpenEmailPage ();
				}else if(navText == ProfileMainPage.NavText.Phone.ToString()){
					PhoneRowSelected()	;
				}else {
					controller = ShowEditPage (navText, profile);
				}

				if (controller != null)
					PushViewController (controller, true);
			};

			this.PushViewController (profileController, animate);
		}


		public UIViewController ShowEditPage (string navText, ProfileMainPage.UserProfile profile)
		{
			if (profile == null)
				profile = _profilePage.Profile;
			ProfileMainPage.NavText textEnum = (ProfileMainPage.NavText)Enum.Parse (typeof(ProfileMainPage.NavText), navText);
			ContentPage editPage = null;
			UIViewController controller = null;
			string title;
			switch (textEnum) {
			case ProfileMainPage.NavText.DisplayName: 
				title = BlrtUtil.PlatformHelper.Translate ("profile_profile_name_field", "profile");
				editPage = new EdittingPage (title, title, profile.DisplayName);
				break;
			case ProfileMainPage.NavText.Organization:
				title = BlrtUtil.PlatformHelper.Translate ("profile_profile_organisation_field", "profile");
				editPage = new EdittingPage (title, title, profile.Organization);
				break;
			case ProfileMainPage.NavText.Gender:
				editPage = new GenderPage ();
				break;
			case ProfileMainPage.NavText.Industry:
				editPage = new IndustryPage ();
				break;
			case ProfileMainPage.NavText.Password:
				editPage = new PasswordPage ();
				break;
			default:
				return null;
			}

			controller = editPage.CreateViewController ();
			controller.Title = editPage.Title;
			controller.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Cancel, ( s, args) => {
				PopViewController (true);
			});

			var editPageSt = editPage as EdittingPage;

			switch (textEnum) {
			case ProfileMainPage.NavText.DisplayName: 
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, ( s, args) => {
					saveProfile (BlrtUserHelper.DisplayNameKey, editPageSt.EdittingString,
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_message", "profile")
					);
					Xamarin.Forms.MessagingCenter.Send<Object> (s, "DisplaynameChanged");
				});
				break;
			case ProfileMainPage.NavText.FirstName:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, ( s, args) => {
					saveProfile (BlrtUserHelper.FirstNameKey, editPageSt.EdittingString,
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_message", "profile")
					);
					BlrtData.Helpers.BlrtTrack.ProfileEditOther(textEnum.ToString());
				});
				break;
			case ProfileMainPage.NavText.LastName:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, ( s, args) => {
					saveProfile (BlrtUserHelper.LastNameKey, editPageSt.EdittingString,
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_message", "profile")
					);
					BlrtData.Helpers.BlrtTrack.ProfileEditOther(textEnum.ToString());
				});
				break;
			case ProfileMainPage.NavText.Organization:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, ( s, args) => {
					saveProfile (BlrtUserHelper.ORGANISATION_NAME, editPageSt.EdittingString,
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("edit_profile_required_fail_message", "profile")
					);
						BlrtData.Helpers.BlrtTrack.ProfileEditOther(textEnum.ToString());
				});
				break;
			case ProfileMainPage.NavText.Gender:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, async ( s, args) => {
					var myPage = editPage as GenderPage;
					if (await myPage.SavePressed (s, args)) {
						_profilePage.RefreshItemSource ();
						PopViewController (true);
							BlrtData.Helpers.BlrtTrack.ProfileEditOther(textEnum.ToString());
					}
				});
				break;
			case ProfileMainPage.NavText.Industry:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, async ( s, args) => {
					var myPage = editPage as IndustryPage;
					if (await myPage.SaveIndustryAsync ()) {
						_profilePage.RefreshItemSource ();
						PopViewController (true);
							BlrtData.Helpers.BlrtTrack.ProfileEditOther(textEnum.ToString());
					}
				});
				break;
			case ProfileMainPage.NavText.Password:
				controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, async ( s, args) => {
					var myPage = editPage as PasswordPage;
					InvokeOnMainThread (() => {
						PushLoading ("password", true);
					});
					if (await myPage.SavePasswordAsync ()) {
						_profilePage.RefreshItemSource ();
						PopViewController (true);
						Xamarin.Forms.MessagingCenter.Send<Object> (s, "PasswordChanged");
						BlrtData.Helpers.BlrtTrack.ProfileEditPassword();
					}
					InvokeOnMainThread (() => {
						PopLoading ("password", true);
					});
				});
				break;
			}
			return controller;
		}


		void saveProfile (string field, string value, string alertTitle, string alertMessage)
		{
			var currentUser = ParseUser.CurrentUser;
			if (field != BlrtUserHelper.ORGANISATION_NAME && string.IsNullOrWhiteSpace (value)) {
				var alert = new UIAlertView (alertTitle, alertMessage, null, "OK", null);
				alert.Show ();
			} else {
				currentUser [field] = value.Trim ();
				if (field == BlrtUserHelper.DisplayNameKey) {
					currentUser [BlrtUserHelper.HasnotSetDisplaynameKey] = false;
					BlrtData.Helpers.BlrtTrack.ProfileEditDisplayname (value);
				}
				currentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				_profilePage.RefreshItemSource ();
				PopViewController (true);
			}
		}

		public void SelectPage (string navText)
		{
			var con = ShowEditPage (navText, _profilePage.Profile);
			this.PushViewController (con, true);
		}


		public async void PhoneRowSelected(){
			if (string.IsNullOrEmpty (BlrtUserHelper.PhoneNumber)) {
				BlrtManager.App.OpenAddPhone (BlrtData.ExecutionPipeline.Executor.System (), OpenFrom.Profile);
			}else{
				var result = await _profilePage.DisplayActionSheet (_profilePage.Profile.Phone, "Cancel", null, "Change", "Remove");
				if(result == "Change"){
					BlrtManager.App.OpenAddPhone (BlrtData.ExecutionPipeline.Executor.System(), OpenFrom.Profile);
				}else if(result == "Remove"){
					try{
						var item = BlrtUserHelper.ContactDetails.Where (x => x.Type == BlrtData.Contacts.BlrtContactInfoType.PhoneNumber).FirstOrDefault ();
						if(item==null){
							//fetch from server
							using(Acr.UserDialogs.UserDialogs.Instance.Loading("",()=>{throw new Exception("No Phone Number in Contact");}, BlrtUtil.PlatformHelper.Translate("Cancel"))){
								await BlrtData.Query.BlrtParseActions.GetContactDetails(new System.Threading.CancellationTokenSource(15 * 1000).Token);
							}
							item = BlrtUserHelper.ContactDetails.Where (x => x.Type == BlrtData.Contacts.BlrtContactInfoType.PhoneNumber).FirstOrDefault ();
							if(item==null){
								throw new Exception("No Phone Number in Contact");
							}
						}
						var id = item.ObjectId;
						RemoveContactDetail(id, true);
					}catch(Exception edsf){
						try{
							await ParseUser.CurrentUser.BetterFetchAsync (new System.Threading.CancellationTokenSource (15 * 1000).Token);
							_profilePage?.RefreshItemSource();
						}catch{}
					}
				}
			}
		}

		public async void RemoveContactDetail(string id, bool phone)
		{
			try {
				using(Acr.UserDialogs.UserDialogs.Instance.Loading("")){
					var request = new BlrtAPI.APIUserContactDetailDelete (new BlrtAPI.APIUserContactDetailDelete.Parameters () {
						Id = id
					});
					if (!await request.Run (new System.Threading.CancellationTokenSource (15000).Token)) {
						throw request.Exception;
					}
					if (request.Response.Success) {
						var t1 =  BlrtData.Query.BlrtSettingsQuery.RunQuery ().WaitAsync ();
						var t2 =  BlrtData.Query.BlrtParseActions.GetContactDetails (new System.Threading.CancellationTokenSource(15 * 1000).Token);
						await System.Threading.Tasks.Task.WhenAll(t1,t2);
						_profilePage.RefreshItemSource();
						if (phone) {
							BlrtData.Helpers.BlrtTrack.ProfileDisconnectService("Phone");
							BlrtUserHelper.IdentifyUser();
						}
					} else if (request.Response.Error.Code != BlrtAPI.APIUserContactDetailDeleteError.RecordNotFound){
						_profilePage.DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("OK")
						);
					} else{
						throw request.Exception;
					}
				}
			} catch(Exception eee) {
				_profilePage.DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("OK")
				);
				BlrtUtil.Debug.ReportException (eee);
			}
		}

		#endregion
	}
}
