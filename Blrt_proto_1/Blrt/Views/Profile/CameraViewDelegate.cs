﻿using System;
using UIKit;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using Foundation;
using BlrtiOS.Util;
using BlrtData;
using BlrtData.Contacts;

namespace BlrtiOS.Views.Profile
{
	public class CameraViewDelegate : UIImagePickerControllerDelegate, IExecutionBlocker
	{
		private ProfileNavigationPage _parent;
		private UIImagePickerController _picker;
		string _from;

		public CameraViewDelegate (ProfileNavigationPage parent, UIImagePickerController picker, string from) : base ()
		{
			_parent = parent;
			_picker = picker;
			_picker.AllowsEditing = true;
			_from = from;
		}

		public override UIInterfaceOrientationMask SupportedInterfaceOrientations (UINavigationController navigationController)
		{
			return UIInterfaceOrientationMask.All;
		}


		#region IExecutionBlocker implementation

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			if (_picker.PresentingViewController != null) {
				await _picker.DismissViewControllerAsync (true);
			}
			return true;
		}

		#endregion

		public override void WillShowViewController (UINavigationController navigationController, UIViewController viewController, bool animated)
		{
			ExecutionPerformer.RegisterBlocker (this);
		}

		public override void Canceled (UIImagePickerController picker)
		{
			if (picker.PresentingViewController != null) {
				picker.DismissViewController (true, () => {
					ExecutionPerformer.UnregisterBlocker (this);
				});
			}
		}

		public override void FinishedPickingMedia (UIImagePickerController picker, NSDictionary info)
		{
			UIImage image = (UIImage)info.ObjectForKey (
				UIImagePickerController.EditedImage
			);

			if (picker.PresentingViewController != null) {

				picker.DismissViewController (true, () => {
					ExecutionPerformer.UnregisterBlocker (this);
				});
			}
			LoadImage (image, picker.SourceType.ToString ());
		}

		async void  LoadImage (UIImage image, string source)
		{
			_parent.PushLoading ("image", true);
			using (var newImg = ParseImage (image)) {
				var filename = BlrtUserHelper.CreateUniqueUserAvatarName ();
				string path = BlrtConstants.Directories.Default.GetAvatarPath (filename);
				using (var data = newImg.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
					NSError e = null;
					data.Save (
						path,
						NSDataWritingOptions.Atomic,
						out e
					);
				}
				image = null;
				BlrtUserHelper.SaveAvatarToParse (path, filename, "user", _from);

				_parent.UserProfile.AvatarImage = path;
			}
			_parent.PopLoading ("image", true);
		}

		public static UIImage ParseImage (UIImage image)
		{
			const int maxSize = Avatar.MaxSize;
			if (NMath.Max (image.Size.Width, image.Size.Height) > maxSize) {

				nfloat max = maxSize / NMath.Max (image.Size.Width, image.Size.Height);

				var oldImage = image;

				image = BlrtUtilities.CreateThumbnail (image, (int)(image.Size.Width * max), (int)(image.Size.Height * max));

				BlrtUtil.BetterDispose (oldImage);
				oldImage = null;
			}

			return image;
		}
	}
}

