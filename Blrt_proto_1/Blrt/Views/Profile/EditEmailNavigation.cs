﻿using BlrtiOS.Views.Settings;
using BlrtData.Views.Settings.Profile;
using BlrtData;
using UIKit;
using Xamarin.Forms;
using System;
using BlrtData.Views.Settings;
using Parse;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using BlrtShared;

namespace BlrtiOS.Views.Profile
{
	public partial class ProfileNavigationPage : GenericNavigationController
	{
		#region emailpage

		EmailPage _emailPage;
		void OpenEmailPage ()
		{
			_emailPage = new EmailPage ();
			var emailController = _emailPage.CreateViewController ();
			emailController.Title = _emailPage.Title;

			_emailPage.EditPrimaryRequested += SafeEventHandler.FromAction ((object sender, BlrtUserContactDetail e) => {
				this.PushViewController (ShowEditPage ("editPrimary"), true);
			});

			_emailPage.SendVerificationEmailRequested += SafeEventHandler.PreventMulti<BlrtUserContactDetail> (SendVerificationEmail);

			_emailPage.MakePrimaryRequested += SafeEventHandler.PreventMulti<BlrtUserContactDetail> (MakePrimaryAsync);

			_emailPage.RemoveRequested += SafeEventHandler.PreventMulti<BlrtUserContactDetail> (RemoveContactAsync);

			_emailPage.OnAddSecondary += SafeEventHandler.PreventMulti (OnAddSecondary);

			_emailPage.Disappearing += SafeEventHandler.FromAction (() => {
				_profilePage?.RefreshItemSource ();
			});

			this.PushViewController (emailController, true);
		}

		async Task OnAddSecondary ()
		{
			PushLoading ("addSecondary", true);
			var shouldRefresh = await BlrtProfileHelper.AddSecondaryEmail (this);
			PopLoading ("addSecondary", true);
			if (shouldRefresh) {
				await _emailPage.RefreshItemSource (true);
			}
			var secondaryEmails="";

			foreach(var email in BlrtUserHelper.SecondaryEmails)
			{
				secondaryEmails += email.Value+", ";
			}

			//BlrtData.Helpers.BlrtTrack.ProfileEditEmail (ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, ""), BlrtUserHelper.SecondaryEmails.Count (), BlrtProfileHelper.saveSecondaryEmail);
			BlrtData.Helpers.BlrtTrack.ProfileEditEmail (ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, ""), BlrtUserHelper.SecondaryEmails.Count (), secondaryEmails );
		}

		public async Task RefreshItemSource ()
		{
			if (null != _emailPage) {
				await _emailPage.RefreshItemSource (true);
			}
		}

		UIViewController ShowEditPage (string operation, EmailPage.EmailCellItem data = null)
		{
			ContentPage editPage = null;
			UIViewController controller = null;

			switch (operation) {
				case "editPrimary":
					var title = BlrtUtil.PlatformHelper.Translate ("profile_email_primary_field", "profile");
					editPage = new EdittingPage (title, title, ParseUser.CurrentUser.Email) {
						EntryKeyboard = Keyboard.Email,
						DisableAutoCap = true
					};
					break;
			}

			controller = editPage.CreateViewController ();
			controller.Title = editPage.Title;
			controller.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Cancel, (s, args) => {
				PopViewController (true);
			});

			var editPageSt = editPage as EdittingPage;

			switch (operation) {
				case "editPrimary":
					controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, (s, args) => {
						View.EndEditing (true);
						EditPrimarySaveAsync (editPageSt).FireAndForget ();
					});
					break;
			}
			return controller;
		}

		// functions from old profileController.EmailEdit
		async Task SendVerificationEmail (object sender, BlrtUserContactDetail contact)
		{
			while (true) {
				PushLoading ("emailVerification", true);
				var success = await BlrtUserHelper.ResendConfirmation (contact);
				PopLoading ("emailVerification", true);

				if (success) {
					await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_sentverification", "profile", contact.Value);
				} else {
					var shouldRetry = await BlrtHelperiOS.GenericConfirmAsync ("profile_editemail_sendverification_error", "profile");
					if (shouldRetry) {
						continue;
					}
				}
				return;
			}
		}

		async Task EditPrimarySaveAsync (EdittingPage editPageSt)
		{
			if (!BlrtData.BlrtUtil.IsValidEmail (editPageSt.EdittingString)) {
				await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_not_email", "profile");
				return;
			}
			if (await BlrtProfileHelper.CheckPassword (this)) {
				await EditPrimaryFinish (editPageSt.EdittingString, true);
			}
		}

		async Task EditPrimaryFinish (string primaryEmail, bool notifyIfSwapped = true)
		{
			var swapped = false;

			PushLoading ("editPrimary", true);

			if (primaryEmail != ParseUser.CurrentUser.Email) {
				while (true) {
					bool connectionError = false;
					var request = new BlrtAPI.APIUserChangePrimary (new BlrtAPI.APIUserChangePrimary.Parameters () {
						Email = primaryEmail,
					});

					if (await request.Run (new CancellationTokenSource (TimeSpan.FromSeconds (15d)).Token)) {
						if (!request.Response.Success) {
							if (BlrtAPI.APIUserChangePrimaryError.EmailInUse == request.Response.Error.Code) {
								await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_emailinuse", "profile");
								break;
							} else {
								connectionError = true;
							}
						} else if (request.Response.Status.Code == BlrtAPI.APIUserChangePrimaryStatus.SwitchedWithSecondary) {
							swapped = true;
							BlrtData.Helpers.BlrtTrack.ProfileEditEmail (primaryEmail, BlrtUserHelper.SecondaryEmails.Count (),BlrtProfileHelper.saveSecondaryEmail);
						} else {//successfully change the primary email address
							BlrtData.Helpers.BlrtTrack.ProfileEditEmail (primaryEmail, BlrtUserHelper.SecondaryEmails.Count (),BlrtProfileHelper.saveSecondaryEmail);
						}
					} else {
						connectionError = true;
					}

					if (connectionError) {
						PopLoading ("editPrimary", true);
						if (await BlrtHelperiOS.GenericConfirmAsync ("profile_editemail_generic_connerror", "profile")) {
							continue;
						} else {
							return;
						}
					}
					break;
				}
			} else {
				PopLoading ("editPrimary", true);
				return;
			}

			PopViewController (true);
			await _emailPage.RefreshItemSource (true);
			PopLoading ("editPrimary", true);

			if (notifyIfSwapped && swapped) {
				await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_swapped_primary", "profile");
			}

		}


		async Task MakePrimaryAsync (object sender, BlrtUserContactDetail contact)
		{
			if (await BlrtProfileHelper.CheckPassword (this)) {
				await EditPrimaryFinish (contact.Value, false);
			}
		}

		async Task RemoveContactAsync (object sender, BlrtUserContactDetail contact)
		{
			if (await BlrtProfileHelper.CheckPassword (this)) {
				PushLoading ("romoveContact", true);
				while (true) {
					var request = new BlrtAPI.APIUserContactDetailDelete (new BlrtAPI.APIUserContactDetailDelete.Parameters () {
						Id = contact.ObjectId
					});
					if (await request.Run (new CancellationTokenSource (15000).Token)) {
						if (request.Response.Success || (request.Response.Error.Code == BlrtAPI.APIUserContactDetailDeleteError.RecordNotFound)) {
							break;
						}
					}
					PopLoading ("romoveContact", true);
					if (await BlrtHelperiOS.GenericConfirmAsync ("profile_editemail_generic_connerror", "profile")) {
						PushLoading ("romoveContact", true);
						continue;
					}
					return;
				}
				await _emailPage.RefreshItemSource (true);
				PopLoading ("romoveContact", true);
			}
		}
		#endregion
	}
}