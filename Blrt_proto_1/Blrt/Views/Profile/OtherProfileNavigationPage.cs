using BlrtiOS.Views.Settings;
using System;
using Xamarin.Forms;
using UIKit;
using BlrtData;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using BlrtData.Contacts;
using BlrtData.ExecutionPipeline;
using AddressBook;
using AddressBookUI;
using BlrtAPI;
using BlrtData.Query;
using System.Threading;
using Foundation;
using MessageUI;
using BlrtData.Views.Profile;
using Parse;

namespace BlrtiOS.Views.Profile
{
	public class OtherProfileNavigationPage: GenericNavigationController, IExecutionBlocker,IProfileNav
	{
		#region IExecutionBlocker implementation

		async Task<bool> IExecutionBlocker.Resolve (ExecutionPerformer e)
		{
			var tcs = new TaskCompletionSource<bool> ();
			_blrtContact.LastUsed = DateTime.Now;
			BlrtContactManager.SharedInstanced.Save();
			if(this.PresentingViewController!=null){
				this.DismissViewController(true,()=>{
					OnDismissed?.Invoke(this, EventArgs.Empty);
					tcs.TrySetResult(true);
					ExecutionPerformer.UnregisterBlocker(this);
				});
			}
			return await tcs.Task;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			ExecutionPerformer.RegisterBlocker (this);

			this.OnDismiss += (sender, e) => {
				_blrtContact.LastUsed = DateTime.Now;
				BlrtContactManager.SharedInstanced.Save();
				if(this.PresentingViewController!=null){
					this.DismissViewController(true,()=>{
						OnDismissed?.Invoke(this, EventArgs.Empty);
						ExecutionPerformer.UnregisterBlocker(this);
					});
				}
			};

			if (ABAddressBook.GetAuthorizationStatus () != ABAuthorizationStatus.Authorized) {
				var error = new Foundation.NSError ();
				var addressBook = ABAddressBook.Create (out error);
				if (addressBook != null) {
					addressBook.RequestAccess(delegate {});
				}
			}
		}

		public event EventHandler OnDismissed;
		private event EventHandler OnDismiss;
		public UserProfilePage ProfilePage;

		public void InitPage ()
		{
			ProfilePage = new UserProfilePage ();
			var page = ProfilePage;
			Reload ();

			var controller = page.CreateViewController();
			controller.Title = page.Title;

			controller.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem("Close", UIBarButtonItemStyle.Plain, (s,ev)=>{
				OnDismiss?.Invoke(this,EventArgs.Empty);
			}),false);

			this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.FormSheet;

			this.PushViewController (controller, false);

			ProfilePage.OnPremiumIconTapped+= (sender, e) => {
				OpenUpgradePage();
			};

			ProfilePage.OnBottomButtonTapped += (sender, e) => {
				if(_blrtUser!=null){
					Block(null, UserProfile.BottomButtonText == BlrtHelperiOS.Translate ("Unblock User"), _blrtUser.ObjectId);
				}
			};

			ProfilePage.OnShowBlrtContactTapped += async (sender, e) => {
				if(!_blrtContact.Hide){
					var result = await ProfilePage.DisplayAlert(
						BlrtUtil.PlatformHelper.Translate("hide_contact_title","profile"), 
						BlrtUtil.PlatformHelper.Translate("hide_contact_message","profile"), 
						BlrtUtil.PlatformHelper.Translate("hide_contact_accept","profile"), 
						BlrtUtil.PlatformHelper.Translate("hide_contact_cancel","profile")
					);
					if(result){
						_blrtContact.Hide = true;
					}else{
					}
				}else{
					_blrtContact.Hide = false;
				}
				BlrtData.Helpers.BlrtTrack.OtherProfileHideShow();
				BlrtContactManager.SharedInstanced.Save();
				Reload();
			};

			Refresh (true);
		}

		BlrtUser _blrtUser;
		BlrtContact _blrtContact;
		public OtherProfileNavigationPage (BlrtUser blrtUser):base()
		{
			_blrtUser = blrtUser;
			//find blrt contact accroding to id
			if(_blrtContact==null){
				_blrtContact = BlrtContactManager.SharedInstanced.GetMatching (_blrtUser);
			}
			if(_blrtContact == null){
				_blrtContact = BlrtContact.FromBlrtUser (_blrtUser);
				BlrtContactManager.SharedInstanced.Add (_blrtContact);
			}

			InitPage ();
		}

		public OtherProfileNavigationPage (BlrtContact blrtContact):base()
		{
			_blrtContact = blrtContact;
			//if blrtId !=null, find blrt person
			foreach(var i in _blrtContact.InfoList){
				if(i.ContactType == BlrtContactInfoType.BlrtUserId){
					_blrtUser = BlrtManager.DataManagers.Default.GetUser (i.ContactValue);
					break;
				}
			}

			InitPage ();
		}

		UserProfileViewModel _userProfile;
		public UserProfileViewModel UserProfile{
			get{
				return _userProfile;
			}
		}

		public void Reload(){
			ProfilePage.BindingContext = GetProfileModel ();
		}

		UserProfileViewModel GetProfileModel(){
			var rtn = _userProfile = new UserProfileViewModel ();

			rtn.DisplayName = _blrtContact.UsableName;
			if(_blrtContact.Avatar!=null){
				rtn.AvatarImage = _blrtContact.Avatar.Path;
			}
			if((string.IsNullOrEmpty(rtn.AvatarImage) || !File.Exists(rtn.AvatarImage)) && _blrtUser!=null){
				rtn.AvatarImage = _blrtUser.LocalAvatarPath;
			}
			if(string.IsNullOrEmpty(rtn.AvatarImage) || !File.Exists(rtn.AvatarImage)){
				rtn.AvatarImage = BlrtUserHelper.DefaultAvatar;
			}

			if (_blrtUser != null) {
				rtn.PrimaryEmail = _blrtUser.Email;
			}
			rtn.ContactInfoList = _blrtContact.InfoList;

			if (_blrtUser != null) {
				rtn.IsBlrtUser = true;
				rtn.Gender = GenderToString (_blrtUser.Gender);
				rtn.Organization = _blrtUser.Organization;
				rtn.IsPhoneLinked = _blrtUser.IsPhoneLinked;
				rtn.IsFBLinked = !string.IsNullOrEmpty (_blrtUser.FacebookId);

				if (BlrtUserHelper.BlockedUsers.Contains (_blrtUser.ObjectId)) {
					rtn.BottomButtonText = BlrtHelperiOS.Translate ("Unblock User");
				} else {
					rtn.BottomButtonText = BlrtHelperiOS.Translate ("Block User");
				}

				if(_blrtUser.AccountThreshold >= BlrtData.BlrtPermissions.BlrtAccountThreshold.Trial.GetHashCode()){
					rtn.IsPremium = true;
				}
			}

			rtn.ShowBlrtContact = !_blrtContact.Hide;

			var list = new List<ProfileCellModal> ();

			var cellModel = new ProfileCellModal () {
				Title = BlrtUtil.PlatformHelper.Translate("send_a_blrt_title","profile"),
				Detail = BlrtUtil.PlatformHelper.Translate("send_a_blrt_detail","profile"),
				Icon = "icon_blrt_light.png",
				Action = new Action (async () =>{
					string requestEmailOrPhone;
					if (!string.IsNullOrEmpty(rtn.PrimaryEmail)) {
						requestEmailOrPhone = rtn.PrimaryEmail;
					}else{
						var options = rtn.ContactInfoList.Where(x=>x.ContactType== BlrtContactInfoType.Email || x.ContactType== BlrtContactInfoType.PhoneNumber).Select(x=>x.ContactValue);

						if(options.Count()>1){
							var result  = await ProfilePage.DisplayActionSheet(BlrtUtil.PlatformHelper.Translate("send_to_title","profile"), "Cancel", null, options.ToArray());
							if(result=="Cancel"){
								return;
							}else{
								requestEmailOrPhone = result;
							}
						}else if(options.Count()==1){
							requestEmailOrPhone = options.First();
						}else{
							return;
						}
					}

					BlrtData.Helpers.BlrtTrack.OtherProfileSendBlrt(rtn.IsBlrtUser, BlrtUtil.IsValidPhoneNumber(requestEmailOrPhone)? "phoneNumber":"email");
					await new ConversationWithContactsCreator (Executor.System (), new BlrtRequestArgs(){Emails=new string[]{requestEmailOrPhone}}).RunAction ();
				})
			};
			list.Add (cellModel);

			if(ABAddressBook.GetAuthorizationStatus () != ABAuthorizationStatus.Authorized){
				
			}else if(AddressBookRecord!=null){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("view_contact_title","profile"),
					Icon = "icon_contact.png",
					Action = new Action (() => {
						OpenContactWithPerson (AddressBookRecord);
					})
				};
				list.Add (cellModel);
			}else{
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("add_contact_title","profile"),
					Icon = "icon_contact.png",
					Detail = BlrtUtil.PlatformHelper.Translate("add_contact_detail","profile"),
					Action = new Action (() => {
						AddNewContactToAddressBook();
					})
				};
				list.Add (cellModel);
			}

			if(_blrtUser!=null){
				if(_blrtUser.PublicBlrtCount==0){
					cellModel = new ProfileCellModal () {
						Title = BlrtUtil.PlatformHelper.Translate("No public Blrts"),
						Icon = "icon_public.png",
						SelectIcon = "",
					};
				}else{
					cellModel = new ProfileCellModal () {
						Title = string.Format("{0} public Blrt{1}", _blrtUser.PublicBlrtCount, _blrtUser.PublicBlrtCount==1? "":"s"),
						Icon = "icon_public.png",
						SelectIcon = "",
					};
				}
				list.Add (cellModel);
			}

			if(!rtn.IsBlrtUser){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("invite_to_blrt_title","profile"),
					Icon = "icon_invite.png",
					Action = new Action(()=>{
						ShareViaEmail();
					})
				};
				list.Add (cellModel);
			}
				
			rtn.OperationList = list;
			return rtn;
		}


		ABPerson AddressBookRecord{
			get{
				var persons = GetDataFromAddressBook ();
				if (persons == null || UserProfile.ContactInfoList.Count()<1)
					return null;
				foreach(var p in persons){
					foreach(var e in p.GetEmails()){
						if(UserProfile.ContactInfoList.Any(x=>x.ContactType== BlrtContactInfoType.Email && x.ContactValue==e.Value)){
							return p;
						}
					}

					foreach(var e in p.GetPhones()){
						if(UserProfile.ContactInfoList.Any(x=>x.ContactType== BlrtContactInfoType.PhoneNumber && x.ContactValue== BlrtUtil.ToPhoneNumberE164(e.Value))){
							return p;
						}
					}
				}
				return null;
			}
		}

		UIViewController _personController;
		void OpenContactWithPerson(ABPerson p){
			if (p == null)
				return;
			UIViewController controller;
			bool shouldOpenEdit = false;

			var myemails = UserProfile.ContactInfoList.Where (x => x.ContactType == BlrtContactInfoType.Email).Select (x => x.ContactValue);
			var myphones = UserProfile.ContactInfoList.Where (x => x.ContactType == BlrtContactInfoType.PhoneNumber).Select (x => x.ContactValue);

			if(myemails.Any(x=>!p.GetEmails().Any(a=>a.Value==x))){
				var emails = new ABMutableStringMultiValue();
				foreach (var e in myemails) {
					emails.Add (e, null);
				}
				p.SetEmails(emails);
				shouldOpenEdit = true;
			}

			if (myphones.Any(x=>!p.GetPhones().Any(a=>BlrtUtil.ToPhoneNumberE164(a.Value)==x))) {
				var phones = new ABMutableStringMultiValue ();
				foreach (var e in myphones) {
					phones.Add (e, null);
				}
				p.SetPhones (phones);
				shouldOpenEdit = true;
			}

			if(!p.HasImage && UserProfile.AvatarImage!= BlrtUserHelper.DefaultAvatar){
				p.Image = Foundation.NSData.FromFile (UserProfile.AvatarImage);
				shouldOpenEdit = true;
			}

			if(shouldOpenEdit){
				var c = new ABNewPersonViewController () {
					DisplayedPerson = p,
					Title = "Edit Contact"
				};
				c.NewPersonComplete+= (sender, e) => {
					this.PopViewController(true);
					Reload();
				};
				controller = c;
			}else{
				var c = new ABPersonViewController () {
					DisplayedPerson = p,
					AllowsEditing = true
				};
				controller = c;
			}
			_personController = controller;
			this.PushViewController (controller, true);
			ProfilePage.Appearing += ReloadFromABBook;
		}

		void ReloadFromABBook (object sender, EventArgs e)
		{
			ProfilePage.Appearing -= ReloadFromABBook;
			ABPerson p = AddressBookRecord;

			if(p!=null){
				var contact = ContactManagerIOS.PersonToContact (p, BlrtContactManager.SharedInstanced);
				if (contact == null) {
					Reload ();
					return;
				}

				foreach(var i in contact.InfoList){
					if(!_blrtContact.InfoList.Contains(i))
						_blrtContact.Add (i);
				}

				var toRemove = new List<BlrtContactInfo>();
				foreach(var i in _blrtContact.InfoList){
					if (i.ContactType != BlrtContactInfoType.Email && i.ContactType != BlrtContactInfoType.PhoneNumber)
						continue;
					if(!contact.InfoList.Contains(i) && (_blrtUser==null || _blrtUser.Email!=i.ContactValue))
						toRemove.Add(i);
				}
				foreach(var i in toRemove){
					_blrtContact.Remove (i);
				}

				BlrtContactManager.SharedInstanced.Save ();
				BlrtContactManager.SharedInstanced.Changed?.Invoke (this, EventArgs.Empty);
			}
			Reload ();
		}

		void AddNewContactToAddressBook(){
			var names = _blrtContact.UsableName.Trim().Split(' ');
			string firstName ="",lastName = "";
			if (names.Length > 1) {
				lastName = names [names.Length - 1];
			}  
			if (names.Length > 0) {
				firstName = names [0];
			}

			var p = new ABPerson(){
				FirstName = firstName,
				LastName = lastName
			};

			var emails = new ABMutableStringMultiValue();
			foreach (var e in UserProfile.ContactInfoList.Where(x=>x.ContactType== BlrtContactInfoType.Email).Select(x=>x.ContactValue)) {
				emails.Add (e, null);
			}
			p.SetEmails(emails);

			var phones = new ABMutableStringMultiValue ();
			foreach (var e in UserProfile.ContactInfoList.Where(x=>x.ContactType== BlrtContactInfoType.PhoneNumber).Select(x=>x.ContactValue)) {
				phones.Add (e, null);
			}
			p.SetPhones (phones);

			if(UserProfile.AvatarImage!= BlrtUserHelper.DefaultAvatar){
				p.Image = Foundation.NSData.FromFile (UserProfile.AvatarImage);
			}

			var controller = new ABNewPersonViewController () {
				DisplayedPerson = p
			};
			controller.NewPersonComplete+= (sender, e) => {
				this.PopViewController(true);
				ReloadFromABBook(sender,e);
				Reload();
				BlrtData.Helpers.BlrtTrack.OtherProfileAddContact();
			};

			this.PushViewController (controller, true);
		}

		private ABPerson[] GetDataFromAddressBook(){
			//get contacts from device
			var error = new Foundation.NSError ();
			var addressBook = ABAddressBook.Create (out error);

			if (ABAddressBook.GetAuthorizationStatus () != ABAuthorizationStatus.Authorized) {
				return null;
			}
			var persons = addressBook.GetPeople ();
			return persons;
		}

		string GenderToString(long g){
			switch(g){
				case 0 : 
					return "Undisclosed";
				case 1: 
					return "Male";
				case 2:
					return "Female";
				default:
					return "Undisclosed";
			}
		}

		async void ShareViaEmail(){
			var name = BlrtUserHelper.CurrentUserName;

			var m = new MFMailComposeViewController () {
			};
			m.SetSubject (String.Format (BlrtUtil.PlatformHelper.Translate ("message_email_subject", "share"), name));
			var url = await BlrtUtil.GenerateYozioShareUrl ("share_email");
			m.SetMessageBody (string.Format (BlrtUtil.PlatformHelper.Translate ("message_email", "share"), name, url, BlrtUtil.GetImpressionHtml("share_email")), true);

			m.Finished += (object s, MFComposeResultEventArgs e) => {
				DismissViewController (true, null);
			};

			var emailInfo = UserProfile.ContactInfoList.FirstOrDefault (x => x.ContactType == BlrtContactInfoType.Email);
			if (emailInfo != null) {
				m.SetToRecipients (new string[]{emailInfo.ContactValue});
			}
			m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			PresentViewController (m, true, null);
		}


		public async Task Block(string email= null, bool unblock= false, string id = null)
		{
			PushLoading ("", true);				
			var apiCall = new APIUserBlock (
				new APIUserBlock.Parameters () {
					BlockUserEmail = email,
					UnBlock = unblock,
					BlockUserId = id
				}
			);
			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
				if (apiCall.Response.Success) {
					BlrtData.Helpers.BlrtTrack.SettingsBlockUser(email??id,!unblock);
					await Refresh ();						
					PopLoading ("", true);
					return;
				}
				switch (apiCall.Response.Error.Code) {
					case APIUserBlockError.blockUserNotExist:
						//user unblock this user from other device, we only need to refresh
						await Refresh ();
						break;
					case APIUserBlockError.blockUserEmailNotExist:
						ProfilePage.DisplayAlert (
							BlrtUtil.PlatformHelper.Translate("profile_editemail_not_email_title", "profile"),
							BlrtUtil.PlatformHelper.Translate("profile_editemail_not_email_message", "profile"),
							BlrtUtil.PlatformHelper.Translate("profile_editemail_not_email_cancel", "profile")
						);
						break;
				}

			} else {
				ProfilePage.DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
			PopLoading ("",true);
		}

		public async Task Refresh(bool fetchingAvatar = false){
			try{
				await ParseUser.CurrentUser.BetterFetchAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				if(_blrtUser!=null){
					await BlrtParseActions.FindUsersQuery (new string[]{_blrtUser.ObjectId}, new CancellationTokenSource (15000).Token);
				}else{
					await BlrtContactManager.FindBlrtUserFromParse(new BlrtContact[]{_blrtContact});
					foreach(var i in _blrtContact.InfoList){
						if(i.ContactType == BlrtContactInfoType.BlrtUserId){
							_blrtUser = BlrtManager.DataManagers.Default.GetUser (i.ContactValue);
							break;
						}
					}
				}
				if(fetchingAvatar){
					await FetchAvatar();
				}

				Reload();
			}catch(Exception e) {
				BlrtUtil.Debug.ReportException (e, null, Xamarin.Insights.Severity.Warning);
			}
		}

		async Task FetchAvatar(){
			if(_blrtContact.Avatar!=null && _blrtContact.Avatar.CloudPath!=null && !File.Exists(_blrtContact.Avatar.Path)){
				await _blrtContact.DownloadFromParse ();
				return;
			}

			if(_blrtContact.Avatar==null || _blrtContact.Avatar.From <= BlrtData.Contacts.Avatar.AvatarFrom.LocalContact){
				var p = AddressBookRecord;
				if(p!=null && p.HasImage){
					using(var data = p.GetImage (ABPersonImageFormat.OriginalSize)){
						if(data!=null){
							var img = ContactManagerIOS.ParseImage (UIImage.LoadFromData (data));
							string filename = BlrtUserHelper.CreateUniqueUserAvatarName();
							var path = BlrtConstants.Directories.Default.GetAvatarPath (filename);
							NSError e = null;
							var succeed = img.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY).Save (
								path,
								NSDataWritingOptions.Atomic,
								out e
							);

							if (succeed) {
								_blrtContact.DeleteAvatar ();
								_blrtContact.Avatar = new Avatar () {
									From = Avatar.AvatarFrom.LocalContact,
									LastUpdatedAt = DateTime.Now,
									FileName = filename
								};
								return;
							}
						}
					}
				}
			}

			if (_blrtContact.Avatar == null || _blrtContact.Avatar.From <= BlrtData.Contacts.Avatar.AvatarFrom.Gravatar) {
				await _blrtContact.RefreshFromGravatar ();
				return;
			}
		}

		bool _upgradePageOpening;
		async void OpenUpgradePage(){
			if (_upgradePageOpening)
				return;
			_upgradePageOpening = true;
			var result = await BlrtManager.App.OpenUpgradeAccount(BlrtData.ExecutionPipeline.Executor.System());
			await result.Result.WaitAsyncCompletion ();
			_upgradePageOpening = false;
		}
	
	}
}

