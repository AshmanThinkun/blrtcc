﻿using System;
using UIKit;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtShared;

namespace BlrtData.Views.AddPhone
{
	public class AddPhoneNavPage : UINavigationController
	{
		OpenFrom _openFrom;
		TaskCompletionSource<bool> _phoneNumberAddedTcs;

		EnterNumberPage _rootPage;
		UIViewController _rootController;

		public AddPhoneNavPage (OpenFrom openfrom) : base ()
		{
			_openFrom = openfrom;
			_phoneNumberAddedTcs = new TaskCompletionSource<bool> ();

			_rootPage = new EnterNumberPage ();
			if (openfrom == OpenFrom.Profile) {
				//do nothing
			} else if (openfrom == OpenFrom.Signup) {
				_rootPage.SkipButton.IsVisible = true;
			} else if (openfrom == OpenFrom.SingupWithSMS) {
				_rootPage.SkipButton.IsVisible = true;
			}

			_rootController = _rootPage.CreateViewController ();
			_rootPage.TitleChanged += SafeEventHandler.FromAction<string>((sender, e) => {
				_rootController.Title = e;
			});
			_rootController.Title = _rootPage.Title;

			_rootPage.OpenCountryPage += SafeEventHandler.FromAction (OnOpenCountryPage);
			_rootPage.SkipPressed += SafeEventHandler.PreventMulti (OnSkipPressed);


			_rootController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (
				UIBarButtonSystemItem.Cancel,
				SafeEventHandler.FromAction (
					(a, b) => {
						DismissViewController (true, null);
						_phoneNumberAddedTcs.TrySetResult (false);
					}
				)
			);
			_rootController.NavigationItem.RightBarButtonItem = new UIBarButtonItem (
				UIBarButtonSystemItem.Done, 
				SafeEventHandler.PreventMulti (OpenVerificationPage)
			);

			this.PushViewController (_rootController, true);
		}

		async Task OnSkipPressed (object sender, EventArgs e)
		{
			bool result;
			if (_openFrom == OpenFrom.SingupWithSMS) {
				result = await _rootPage.DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("addPhone_skipsms_alert_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skipsms_alert_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skipsms_alert_accept", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skipsms_alert_cancel", "profile")
				);
			} else {
				result = await _rootPage.DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("addPhone_skip_alert_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skip_alert_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skip_alert_accept", "profile"),
					BlrtUtil.PlatformHelper.Translate ("addPhone_skip_alert_cancel", "profile")
				);
			}
			if (result) {
				DismissViewController (true, null);
				_phoneNumberAddedTcs.TrySetResult (false);
			}
		}

		void OnOpenCountryPage (object senderss, EventArgs ess)
		{
			var countryPage = new ChooseCountryPage (_rootPage.SupportCountryCodes, _rootPage.SelectedCountryCode);
			countryPage.Appearing += SafeEventHandler.FromAction ((sender, e) => {
				_rootController.Title = "Edit Number";
			});
			countryPage.Disappearing += SafeEventHandler.FromAction ((sender, e) => {
				_rootController.Title = _rootPage.Title;
			});
			_rootController.Title = "Edit Number";

			PushViewController (countryPage.CreateViewController (), true);
			countryPage.CountrySelected += SafeEventHandler.FromAction<string> ((sender2, selectCode) => {
				_rootPage.SelectedCountryCode = selectCode;
				PopViewController (true);
			});
		}

		async Task OpenVerificationPage (object senderss, EventArgs ess)
		{
			if (await _rootPage.Done ()) {
				// display verification page
				var nextPage = new EnterVerificationCodePage () {
					Title = _rootPage.Title,
					PhoneNumber = _rootPage.PhoneNumber
				};
				nextPage.Verified += SafeEventHandler.FromAction ((sender, e) => {
					DismissViewController (true, null);
					_phoneNumberAddedTcs.TrySetResult (true);
				});
				var nextController = nextPage.CreateViewController ();
				nextController.Title = nextPage.Title;

				nextPage.Appearing += SafeEventHandler.FromAction ((sender, e) => {
					_rootController.Title = "Edit Number";
				});
				nextPage.Disappearing += SafeEventHandler.FromAction ((sender, e) => {
					_rootController.Title = _rootPage.Title;
				});
				_rootController.Title = "Edit Number";
				PushViewController (nextController, true);
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			NavigationBar.Opaque = false;
			NavigationBar.Translucent = false;
		}

		public Task<bool> WaitResult {
			get {
				return _phoneNumberAddedTcs.Task;
			}
		}
	}
}

