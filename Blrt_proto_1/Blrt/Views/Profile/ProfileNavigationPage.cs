﻿using BlrtiOS.Views.Settings;
using System;
using Xamarin.Forms;
using UIKit;
using BlrtData;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using BlrtData.Views.Settings.Profile;
using System.IO;
using BlrtData.IO;
using BlrtData.Contacts;
using BlrtData.Views.Share;
using BlrtData.ExecutionPipeline;
using BlrtData.Views.Profile;
using Parse;

namespace BlrtiOS.Views.Profile
{
	public partial class ProfileNavigationPage: GenericNavigationController, IExecutionBlocker, IProfileNav
	{
		#region IExecutionBlocker implementation

		async Task<bool> IExecutionBlocker.Resolve (ExecutionPerformer e)
		{
			var tcs = new TaskCompletionSource<bool> ();
			if(this.PresentingViewController!=null){
				this.DismissViewController(true,()=>{
					OnDismissed?.Invoke(this, EventArgs.Empty);
					tcs.TrySetResult(true);
					ExecutionPerformer.UnregisterBlocker(this);
				});
			}
			return await tcs.Task;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ExecutionPerformer.RegisterBlocker (this);
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;
			this.OnDismiss += (sender, e) => {
				if(this.PresentingViewController!=null){
					this.DismissViewController(true,()=>{
						OnDismissed?.Invoke(this, EventArgs.Empty);
						ExecutionPerformer.UnregisterBlocker(this);
					});
				}
			};
		}

		public event EventHandler OnDismissed;
		private event EventHandler OnDismiss;
		public UserProfilePage ProfilePage;
		UIViewController _controller;
		public ProfileNavigationPage ():base()
		{
			ProfilePage = new UserProfilePage ();
			var page = ProfilePage;
			_controller = page.CreateViewController();
			ReloadData ();
			Refresh();

			_controller.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem("Close", UIBarButtonItemStyle.Plain, (s,ev)=>{
				OnDismiss?.Invoke(this,EventArgs.Empty);
			}),false);

			_controller.NavigationItem.SetRightBarButtonItem (new UIBarButtonItem("Edit", UIBarButtonItemStyle.Plain, (s,ev)=>{
				OpenEditProfile();
			}),false);

			this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.FormSheet;

			this.PushViewController (_controller, false);

			ProfilePage.Appearing += (sender, e) => {
				ReloadData();
			};

			ProfilePage.OnPremiumIconTapped+= (sender, e) => {
				OpenUpgradePage();
			};

			ProfilePage.OnFBIconTapped += (sender, e) => {
				OpenFacebookPage();
			};

			ProfilePage.OnChangeAvatarIconTapped += (sender, e) => {
				OpenChangeAvatar();
			};

			ProfilePage.OnEditProfileTapped += (sender, e) => {
				OpenEditProfile();
			};

			ProfilePage.OnBottomButtonTapped += (sender, e) => {
				Logout();
			};

			ProfilePage.OnPhoneIconTapped += (sender, e) => {
				OpenEditProfile();
			};

			//refresh user contact details

			ProfileMainPage.DeleteAccountLogoutHandler +=  DeleteAccount;

		}


	  void DeleteAccount (Object o, EventArgs e)
		{
			//for tracking purposes.
			BlrtData.Helpers.BlrtTrack.AccountDelete ();
			//this where the actual process starts
			this.DismissViewController (true, null);
			LogoutProcess ();
			BlrtConstants.DeleteEverything ();
		}


	


		void Refresh(){
			RefreshContactDetails ();
			RefreshAvatar ();
		}

		async void RefreshContactDetails(){
			try{
				var query = BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ();
				await query.WaitAsync ();
			} catch { }
			ReloadData ();
		}

		async void RefreshAvatar(){
			try{ //user may log out during this refresh
				var result = await BlrtUserHelper.GetLatestAvatar();
				if(!string.IsNullOrEmpty(result) && _userProfile!=null){
					_userProfile.AvatarImage = result;
				}
			}catch{}
		}


		public void ReloadData(){
			ProfilePage.BindingContext = GetProfileModel ();
			_controller.Title = ProfilePage.Title;
		}

		UserProfileViewModel _userProfile;
		public UserProfileViewModel UserProfile{
			get{
				return _userProfile;
			}
		}
		public UserProfileViewModel GetProfileModel(){
			var rtn = _userProfile = new UserProfileViewModel ();

			rtn.DisplayName = BlrtUserHelper.CurrentUserName;
			rtn.AvatarImage = BlrtUserHelper.GetLocalAvatar();
			rtn.PrimaryEmail = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, "");
			rtn.ContactInfoList = BlrtUserHelper.ContactDetails.Select(x=>{
				return new BlrtContactInfo(x.Type, x.Value){
					IsLinkToBlrtUser = x.Verified
				};
			});
			rtn.Gender = BlrtUserHelper.Gender;
			rtn.Organization = BlrtUserHelper.Organization;
			rtn.IsCurrentUser = rtn.IsBlrtUser = true;
			rtn.BottomButtonText = BlrtHelperiOS.Translate ("other_logout", "app_menu");
			rtn.IsPhoneLinked = !string.IsNullOrEmpty (BlrtUserHelper.PhoneNumber);

			if(BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free
				|| BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Unknown
				|| BlrtPermissions.AccountThreshold == null){
				rtn.IsPremium = false;
			}else{
				rtn.IsPremium = true;
			}

			rtn.IsFBLinked = FBManager.IsLinked;

			var list = new List<ProfileCellModal> ();
			ProfileCellModal cellModel;

			if(!BlrtUserHelper.EmailVerified){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("email_verify_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate("email_verify_action_detail", "profile"),
					TextColor = BlrtStyles.BlrtAccentRed,
					Action = new Action(()=>{
						OpenVerification();
					}),
					Icon = "icon_important.png",
					BackgroundColor = BlrtStyles.BlrtGray3,
					SeparatorColor = Color.Transparent
				};
				if(list.Count>0){
					list.Last ().SeparatorColor = Color.Transparent;
				}
				list.Add (cellModel);
			}

			if(BlrtUserHelper.HasnotSetDisplayname){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("display_name_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate("display_name_action_detail", "profile"),
					TextColor = BlrtStyles.BlrtAccentRed,
					Action = new Action(()=>{
						OpenEditProfile(true);
						SelectPage(ProfileMainPage.NavText.DisplayName.ToString());
					}),
					Icon = "icon_important.png",
					BackgroundColor = BlrtStyles.BlrtGray3,
					SeparatorColor = Color.Transparent,
				};
				if(list.Count>0){
					list.Last ().SeparatorColor = Color.Transparent;
				}
				list.Add (cellModel);
			}

			if(!BlrtUserHelper.HasSetPassword){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("no_password_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate("no_password_action_detail", "profile"),
					TextColor = BlrtStyles.BlrtAccentRed,
					Action = new Action(()=>{
						OpenEditProfile(true);
						SelectPage(ProfileMainPage.NavText.Password.ToString());
					}),
					Icon = "icon_important.png",
					BackgroundColor = BlrtStyles.BlrtGray3,
					SeparatorColor = Color.Transparent,
				};
				if(list.Count>0){
					list.Last ().SeparatorColor = Color.Transparent;
				}
				list.Add (cellModel);
			}

			if(string.IsNullOrEmpty(BlrtUserHelper.PhoneNumber)){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("phone_number_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate("phone_number_action_detail", "profile"),
					TextColor = BlrtStyles.BlrtAccentRed,
					Action = new Action(()=>{
						BlrtManager.App.OpenAddPhone(Executor.System(), BlrtData.Views.AddPhone.OpenFrom.Profile);
					}),
					Icon = "icon_important.png",
					BackgroundColor = BlrtStyles.BlrtGray3,
					SeparatorColor = Color.Transparent,
				};
				if(list.Count>0){
					list.Last ().SeparatorColor = Color.Transparent;
				}
				list.Add (cellModel);
			}

			if (!BlrtUserHelper.HasDoneFreeTrial && !rtn.IsPremium) {
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("try_premium_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate("try_premium_action_detail", "profile"),
					Action = new Action (() => {
						OpenUpgradePage ();
					}),
					Icon = "icon_blrt.png"
				};
				list.Add (cellModel);
			}else if(!rtn.IsPremium){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate ("upgrade_premium_action_title", "profile"),
					Detail = BlrtUtil.PlatformHelper.Translate ("upgrade_premium_action_detail", "profile"),
					Action = new Action (() => {
						OpenUpgradePage ();
					}),
					Icon = "icon_blrt.png"
				};
				list.Add (cellModel);
			}

			cellModel = new ProfileCellModal () {
				Title = BlrtUtil.PlatformHelper.Translate("connect_service_action_title", "profile"),
				Detail = BlrtUtil.PlatformHelper.Translate("connect_service_action_detail", "profile"),
				Action = new Action(()=>{
					OpenConnectServicePage();
				}),
				Icon = "icon_service.png"
			};
			list.Add (cellModel);

			cellModel = new ProfileCellModal () {
				Title = BlrtUtil.PlatformHelper.Translate("invite_friends_action_title", "profile"),
				Icon = "icon_invite.png",
				Action = new Action(()=>{
					OpenShare();
				})
			};
			list.Add (cellModel);

			cellModel = new ProfileCellModal () {
				Title = BlrtUtil.PlatformHelper.Translate("referral_bonus_title", "profile"),
				Icon = "referral.png",
				Action = new Action(()=>{
					OpenRefererWebPage();
				})
			};
			//list.Add (cellModel); // this is to temporarily hide this option (as instructed by Anu)

			if(BlrtUserHelper.PublicBlrtCount==0){
				cellModel = new ProfileCellModal () {
					Title = BlrtUtil.PlatformHelper.Translate("No public Blrts"),
					Icon = "icon_public.png",
					SelectIcon = "",
				};
			}else{
				cellModel = new ProfileCellModal () {
					Title = string.Format("{0} public Blrt{1}", BlrtUserHelper.PublicBlrtCount, BlrtUserHelper.PublicBlrtCount==1? "":"s"),
					Icon = "icon_public.png",
					SelectIcon = "",
				};
			}
			list.Add (cellModel);

			rtn.OperationList = list;

			return rtn;
		}



		async void OpenChangeAvatar(){
			string result;
			if(FBManager.IsLinked)
				result =await ProfilePage.DisplayActionSheet (
					BlrtUtil.PlatformHelper.Translate("avatar_choose_title","profile"), 
					"Cancel", null, 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_photos","profile"), 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_camera","profile"), 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_fb","profile"), 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_gravatar","profile"));
			else
				result =await ProfilePage.DisplayActionSheet (
					BlrtUtil.PlatformHelper.Translate("avatar_choose_title","profile"), 
					"Cancel", null, 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_photos","profile"), 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_camera","profile"), 
					BlrtUtil.PlatformHelper.Translate("avatar_choose_option_gravatar","profile"));

			var filename = BlrtUserHelper.CreateUniqueUserAvatarName ();
			string path = BlrtConstants.Directories.Default.GetThumbnailPath (filename);


			if (result == BlrtUtil.PlatformHelper.Translate ("avatar_choose_option_photos", "profile")) {
				OpenPhotos ();
			} else if (result == BlrtUtil.PlatformHelper.Translate ("avatar_choose_option_camera", "profile")) {
				OpenCamera ();
			} else if (result == BlrtUtil.PlatformHelper.Translate ("avatar_choose_option_fb", "profile")) {
				var url = await FBManager.FetchUserAvatar ();
				if (string.IsNullOrEmpty (url)) {
					ProfilePage.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("error_nofb_avatar_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("error_nofb_avatar_message", "profile"), 
						"OK");
				}else if(url=="blrt_error"){
					ProfilePage.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("error_connection_image_retrival_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("error_connection_image_retrival_message", "profile"), 
						"OK");
				}else{
					var downloader = BlrtDownloader.Download (10, new Uri (url), path);
					await downloader.WaitAsync ();
					if(File.Exists(path)){
						BlrtUserHelper.SaveAvatarToParse (path, filename, "facebook");
						if(_userProfile!=null){
							_userProfile.AvatarImage = path;
						}
					}
				}
			}else if(result == BlrtUtil.PlatformHelper.Translate("avatar_choose_option_gravatar","profile")){
				var tempFileName = await BlrtUserHelper.RefreshFromGravatar ();
				if(string.IsNullOrEmpty(tempFileName)){
					ProfilePage.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("error_nogravatar_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("error_nogravatar_message", "profile"), 
						"OK");
				}else if(tempFileName=="blrt_error"){
					ProfilePage.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("error_connection_image_retrival_title", "profile"), 
						BlrtUtil.PlatformHelper.Translate ("error_connection_image_retrival_message", "profile"), 
						"OK");
				}else{
					
					File.Copy (BlrtConstants.Directories.Default.GetThumbnailPath (tempFileName), path);
					//if any match, save to cloud
					BlrtUserHelper.SaveAvatarToParse (path, filename, "gravatar");
					if(_userProfile!=null){
						_userProfile.AvatarImage = path;
					}
				}
			}
		}

		void OpenPhotos(){
			var controller = new UIImagePickerController(){
				AllowsEditing = true,
				SourceType = UIImagePickerControllerSourceType.PhotoLibrary,
				ModalPresentationStyle = UIModalPresentationStyle.CurrentContext
			};
			controller.ImagePickerControllerDelegate = new CameraViewDelegate (this, controller, "gallery");
			this.PresentViewController (controller, true, null);
		}

		void OpenCamera(){
			var controller = MediaPicker.CameraController.Camera;
			controller.ImagePickerControllerDelegate = new CameraViewDelegate (this, controller, "camera");
			this.PresentViewController (controller, false, null);
		}

		bool _upgradePageOpening;
		async void OpenUpgradePage(){
			if (_upgradePageOpening)
				return;
			_upgradePageOpening = true;
			var result = await BlrtManager.App.OpenUpgradeAccount(BlrtData.ExecutionPipeline.Executor.System());
			await result.Result.WaitAsyncCompletion ();
			_upgradePageOpening = false;
			ReloadData ();
		}

		public void OpenFacebookPage(){
			var fbpage = new FacebookLinkPage ();
			var fbController = fbpage.CreateViewController();
			this.PushViewController (fbController, true);
			fbpage.OnOpenPasswordPage += (sender, e) => {
				if(_profilePage==null){
					_profilePage = new ProfileMainPage();
				}

				var con = ShowEditPage (ProfileMainPage.NavText.Password.ToString(), _profilePage.Profile);
				fbController.NavigationController.PushViewController (con, true);
			};
		}

		void OpenConnectServicePage(){
			var page = new ConnectServicePage ();
			page.BindingContext = GetConnectServiceInfo();
			var controller = page.CreateViewController ();
			controller.Title = page.Title;
			this.PushViewController (controller, true);

			page.Appearing += (sender, e) => {
				page.BindingContext = GetConnectServiceInfo();
			};
		}

		List<ProfileCellModal> GetConnectServiceInfo(){
			var list = new List<ProfileCellModal> ();
			list.Add(new ProfileCellModal(){
				Title = "Facebook",
				Detail = FBManager.IsLinked? "Connected as " + ParseUser.CurrentUser.Get<string>(BlrtUserHelper.FacebookMetaName, ""):"Not connected",
				Icon = FBManager.IsLinked? "icon_facebook_on_large": "icon_facebook_off_large",
				Action = new Action(()=>{
					OpenFacebookPage();
				})
			});
			list.Add(new ProfileCellModal(){
				Title = "Twitter",
				Detail = "Coming soon",
				Icon = "icon_twitter_off.png",
				SelectIcon = ""
			});
			list.Add(new ProfileCellModal(){
				Title = "Google",
				Detail = "Coming soon",
				Icon = "icon_google_off_large.png",
				SelectIcon = ""
			});
			return list;
		}

		void OpenShare(){
			var SharePage = new ShareController ();
			SharePage.fbShareClicked += FacebookSharePressed;
			SharePage.twShareClicked += TwitterSharePressed;
			SharePage.emailShareClicked += EmailSharePressed;
			SharePage.smsShareClicked += SmsSharePressed;
			SharePage.fbInviteClicked += BigSharseBtn;

			var shareController = SharePage.CreateViewController();
			shareController.Title = SharePage.Title;
			this.PushViewController (shareController, true);
		}

		async void OpenRefererWebPage(){
			PushLoading ("token", true);
			var token = await BlrtUserHelper.GetSessionToken ();
			PopLoading ("token", true);
			if (string.IsNullOrEmpty (token))
				return;

			var param = "token=" + token + "&key=" + BlrtSettings.ReferralProgramKey;
			var url = BlrtSettings.ReferralProgramUrl;
			if (string.IsNullOrEmpty (url))
				return;
			if(!url.Contains("?"))
				url += "?";
			url += param;

			var webpage = new BlrtData.Views.Login.SimpleWebPage(true){
				Source = new UrlWebViewSource() {
					Url = url
				},
				Title = BlrtUtil.PlatformHelper.Translate("referral_bonus_title", "profile"),
				ViewName = "MyProfile_Bonuses"
			};

			var controller = webpage.CreateViewController ();
			controller.Title = webpage.Title;

			this.PushViewController (controller, true);
		}


		void OpenVerification(){
			var page = new EmailVerificationPage ();
			page.OnEditProfileTapped += (sender, e) => {
				this.PopViewController(true);
				OpenEditProfile(true);
				OpenEmailPage ();
			};
			var controller = page.CreateViewController ();
			controller.Title = page.Title;
			this.PushViewController (controller, true);
		}
		public static event EventHandler<int> LogoutClicked;

		void Logout(){
			var alert = new UIAlertView (
				BlrtHelperiOS.Translate("alert_logout_title", "profile"),
				BlrtHelperiOS.Translate("alert_logout_message", "profile"),
				null,
				BlrtHelperiOS.Translate("alert_logout_cancel", "profile"),
				BlrtHelperiOS.Translate("alert_logout_okay", "profile")


			);

			alert.Clicked += (object s1, UIButtonEventArgs e) => {
				if(alert.CancelButtonIndex != e.ButtonIndex){


					LogoutProcess ();

				}
			};
			alert.Show();
		}


		void LogoutProcess()
		{
			CleanUpAfterLogout ();

			OnDismiss?.Invoke (this, EventArgs.Empty);

			BlrtData.BlrtManager.Logout (true);
		}
		/// <summary>
		/// This method is to clear out all the user info before loging off to protect the privacy of the user. Currently the following are cleared:
		/// <list type="number">
		/// <item>
		/// <description>
		/// Slave side of the split controller.
		/// </description>
		/// </item>
		/// </list>
		/// </summary>
		void CleanUpAfterLogout()
		{
			LogoutClicked?.Invoke (this, 1);
		}
	}
}

