using System;
using Xamarin.Forms;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtData;

namespace BlrtiOS.Views.Settings
{
	public class GenericNavigationController: Util.BaseUINavigationController
	{
		protected UIViewController _childController;
		public EventHandler BackPressed{ get; set;}

		CustomUI.BlrtLoadingOverlayView _loader;

		public GenericNavigationController (): base()
		{

		}

		public GenericNavigationController(UIViewController root): base(root)
		{

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationBar.Translucent = false;
			NavigationBar.BarTintColor = BlrtStyles.iOS.Gray3;
			_loader = new BlrtiOS.Views.CustomUI.BlrtLoadingOverlayView ();
			View.AddSubview (_loader);


		}

		public UIViewController AddBackButton(ref UIViewController controller){

			if(BlrtHelperiOS.IsPhone){
				controller.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (
					BlrtUtil.PlatformHelper.Translate ("button_back"),
					UIBarButtonItemStyle.Plain, (sender, e) => {
						if(null!=BackPressed)
							BackPressed(sender,e);
					});
			}

			return controller;
		}


		public override void PushLoading (string cause, bool animated)
		{
			_loader.Push (cause, animated);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loader.Pop (cause, animated);
		}
	}
}

