//using System;
//using UIKit;
//using MonoTouch.FacebookConnect;
//using CoreGraphics;
//using Foundation;
//
//namespace BlrtiOS.Views.Settings
//{
//	public class BlrteFacebookSignoutView : UIView
//	{
//		private const float Margins = 16;
//		private const float Spacing = 12;
//
//
//		//private StringElement _facebookNameElement;
//
//		public event EventHandler UnlinkPressed{
//			add{
//				_btn.TouchUpInside += value;
//			}remove{
//				_btn.TouchUpInside -= value;				
//			}
//		}
//
//		public string UserName {
//			get { return _userName;}
//			set{
//				if (_userName != value) {
//					_userName = value;
//					RecalutateLabel ();
//				}
//			}
//		}
//
//		private UILabel _lbl;
//		private UIImageView _img;
//		private UIButton _btn;
//
//
//		private string _userName;
//
//		public BlrteFacebookSignoutView() : base(new CGRect(0, 0, 320, BlrtHelperiOS.IsPhone ? 64 : 56))
//		{
//			AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin;
//
//
//			_img = new UIImageView(UIImage.FromBundle("sendto_fb.png")){
//				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
//			};
//
//
//
//
//			_btn = new UIButton(UIButtonType.System){
//				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin,
//				TintColor = BlrtConfig.BLRT_RED
//			};
//			_btn.SetTitle(BlrtHelperiOS.Translate ("profile_fb_unlink_button", "profile"), UIControlState.Normal);
//			_btn.SizeToFit();
//
//			_lbl = new UILabel(){
//				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight,
//				Lines = BlrtHelperiOS.IsPhone ? 2 : 1
//			};
//
//			_img.Frame = new CGRect(Margins, (Frame.Height - _img.Frame.Height) * 0.5f, _img.Frame.Width, _img.Frame.Height);
//			_btn.Frame = new CGRect(Frame.Width - (_btn.Frame.Width + Margins), 0, _btn.Frame.Width, Frame.Height);
//			_lbl.Frame = new CGRect(_img.Frame.Right + Spacing, 0, _btn.Frame.Left - _img.Frame.Right - Spacing, Frame.Height);
//
//
//
//			AddSubview(_img);
//			AddSubview(_btn);
//			AddSubview(_lbl);
//		}
//
//		void RecalutateLabel(){
//
//			var username = (BlrtHelperiOS.IsPhone ? "\n" : "") + _userName;
//			var title = BlrtHelperiOS.Translate ("profile_fb_unlink_title", "profile");
//
//
//
//
//			if (!FBManager.IsLinked) {
//				title = "???";
//				FBManager.LoginWithParse ();
//			} 
//
//
//
//			var str = new NSMutableAttributedString (
//				string.Format (title,
//					username), 
//				new UIStringAttributes () {
//					Font = BlrtConfig.LightFont.Large
//				}
//			);
//
//			_lbl.AttributedText = BlrtHelperiOS.AttributeString(
//				str,
//				"#",
//				new UIStringAttributes(){
//					Font = BlrtConfig.BoldFont.Large				
//				}
//			);
//
//		}
//
//		public override void LayoutSubviews ()
//		{
//			if(Superview != null)
//				Frame = new CGRect((Superview.Frame.Width - Frame.Width) * 0.5f, Frame.Y, Frame.Width, Frame.Height);
//			base.LayoutSubviews ();
//		}
//	}
//}
//
