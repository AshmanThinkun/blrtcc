// commented because it's not used any more. It was used in setting screen.
//using System;
//using Xamarin.Forms;
//using UIKit;
//using BlrtData;
//using Parse;
//using System.Threading;
//using BlrtData.Views.Settings;
//using BlrtData.Views.Settings.Profile;
//using System.Threading.Tasks;

//namespace BlrtiOS.Views.Settings
//{
//	public class EmailNavigationController : GenericNavigationController
//	{
//		EmailPage _emailPage;

//		public override void ViewDidLoad ()
//		{
//			base.ViewDidLoad ();

//			_emailPage = new EmailPage ();

//			_childController = _emailPage.CreateViewController ();

//			_childController.Title = _emailPage.Title;

//			AddBackButton (ref _childController);

//			_emailPage.OnEditPrimaryStart += (sender, e) => {
//				this.PushViewController (ShowEditPage ("editPrimary"), true);
//			};
//			_emailPage.OnSendVerificationEmail += (sender, e) => {
//				SendVerificationEmail (e as BlrtUserContactDetail);
//			};
//			_emailPage.OnMakePrimary += (sender, e) => {
//				MakePrimary (e as BlrtUserContactDetail);
//			};
//			_emailPage.OnRemoveStart += (sender, e) => {
//				RemoveStart (e);
//			};

//			_emailPage.OnAddSecondary += async (sender, e) => {
//				PushLoading ("addSecondary", true);
//				var shouldRefresh = await BlrtProfileHelper.AddSecondaryEmail (this);
//				PopLoading ("addSecondary", true);
//				if (shouldRefresh) {
//					await _emailPage.RefreshItemSource (true);
//				}
//			};
//			this.PushViewController (_childController, false);
//		}


//		public void RefreshItemSource ()
//		{
//			if (null != _emailPage) {
//				_emailPage.RefreshItemSource (true);
//			}
//		}

//		UIViewController ShowEditPage (string operation, EmailPage.EmailCellItem data = null)
//		{
//			ContentPage editPage = null;
//			UIViewController controller = null;

//			switch (operation) {
//				case "editPrimary":
//					var title = BlrtUtil.PlatformHelper.Translate ("profile_email_primary_field", "profile");
//					editPage = new EdittingPage (title, title, ParseUser.CurrentUser.Email) {
//						EntryKeyboard = Keyboard.Email,
//						DisableAutoCap = true
//					};
//					break;
//			}

//			controller = editPage.CreateViewController ();
//			controller.Title = editPage.Title;
//			controller.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Cancel, (s, args) => {
//				PopViewController (true);
//			});

//			var editPageSt = editPage as EdittingPage;

//			switch (operation) {
//				case "editPrimary":
//					controller.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, async (s, args) => {
//						View.EndEditing (true);
//						await EditPrimarySave (editPageSt);
//					});
//					break;
//			}
//			return controller;
//		}

//		// misterious vars
//		BlrtUserContactDetail _verificationEmailTarget = null;
//		string _primaryValue;
//		bool _skipSwapCheck;

//		// functions from old profileController.EmailEdit
//		async void SendVerificationEmail (BlrtUserContactDetail contact)
//		{
//			PushLoading ("emailVerification", true);

//			_verificationEmailTarget = contact;

//			var success = await BlrtUserHelper.ResendConfirmation (contact);

//			PopLoading ("emailVerification", true);

//			if (!success) {
//				BlrtHelperiOS.GenericOptionAlert ("profile_editemail_sendverification_error", "profile", true, SendVerificationEmail);
//				return;
//			}

//			_verificationEmailTarget = null;

//			await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_sentverification", "profile", contact.Value);
//		}

//		void SendVerificationEmail (bool proceed)
//		{
//			if (!proceed) {
//				_verificationEmailTarget = null;
//				return;
//			}

//			if (_verificationEmailTarget == null) {
//				SendVerificationEmail (BlrtUserHelper.PrimaryEmail);
//			} else {
//				SendVerificationEmail (_verificationEmailTarget);
//			}
//		}

//		async Task EditPrimarySave (EdittingPage editPageSt)
//		{
//			if (!BlrtData.BlrtUtil.IsValidEmail (editPageSt.EdittingString)) {
//				await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_not_email", "profile");
//				_skipSwapCheck = false;
//				return;
//			}

//			_primaryValue = editPageSt.EdittingString;

//			await BlrtProfileHelper.CheckPassword (this, EditPrimaryFinish);
//		}

//		async void EditPrimaryFinish (bool proceed)
//		{
//			if (!proceed) {
//				_primaryValue = "";
//				_skipSwapCheck = false;
//				return;
//			}

//			var swapped = false;

//			PushLoading ("editPrimary", true);

//			if (_primaryValue != ParseUser.CurrentUser.Email) {
//				string errorPrefix = null;
//				Action<bool> errorAction = null;

//				var request = new BlrtAPI.APIUserChangePrimary (new BlrtAPI.APIUserChangePrimary.Parameters () {
//					Email = _primaryValue,

//				});

//				if (await request.Run (new CancellationTokenSource (TimeSpan.FromSeconds (15d)).Token)) {
//					if (!request.Response.Success) {
//						switch (request.Response.Error.Code) {
//						case BlrtAPI.APIUserChangePrimaryError.EmailInUse:
//							errorPrefix = "profile_editemail_emailinuse";

//							break;
//						default:
//							errorPrefix = "profile_editemail_generic_connerror";
//							errorAction = EditPrimaryFinish;

//							break;
//						}
//					} else if (request.Response.Status.Code == BlrtAPI.APIUserChangePrimaryStatus.SwitchedWithSecondary)
//						swapped = true;
//				} else {
//					errorPrefix = "profile_editemail_generic_connerror";
//					errorAction = EditPrimaryFinish;
//				}

//				if (errorPrefix != null) {
//					PopLoading ("editPrimary", true);

//					if (errorAction == null) {
//						_primaryValue = "";
//						_skipSwapCheck = false;
//					}

//					BlrtHelperiOS.GenericOptionAlert (errorPrefix, "profile", true, errorAction);
//					return;
//				}
//			} else {
//				_primaryValue = "";
//				_skipSwapCheck = false;

//				PopLoading ("editPrimary", true);
//				return;
//			}

//			_primaryValue = "";

//			PopViewController (true);
//			await _emailPage.RefreshItemSource (true);
//			PopLoading ("editPrimary", true);

//			if (!_skipSwapCheck && swapped) {
//				await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_swapped_primary", "profile");
//			} else {
//				_skipSwapCheck = false;
//			}
//		}

//		void MakePrimary (BlrtUserContactDetail contact)
//		{
//			_primaryValue = contact.Value;
//			_skipSwapCheck = true;
//			BlrtProfileHelper.CheckPassword (this, EditPrimaryFinish);
//		}

//		void MakePrimary (bool tryAgain)
//		{
//			if (tryAgain) {
//				BlrtProfileHelper.CheckPassword (this, EditPrimaryFinish);
//			}
//		}

//		BlrtUserContactDetail _removingContact = null;

//		void RemoveStart (BlrtUserContactDetail contact)
//		{
//			_removingContact = contact;
//			BlrtProfileHelper.CheckPassword (this, RemoveFinish);
//		}

//		async void RemoveFinish (bool proceed)
//		{
//			if (!proceed) {
//				_removingContact = null;
//				return;
//			}

//			PushLoading ("romoveContact", true);

//			bool wasError = false;

//			var request = new BlrtAPI.APIUserContactDetailDelete (new BlrtAPI.APIUserContactDetailDelete.Parameters () {
//				Id = _removingContact.ObjectId
//			});

//			if (await request.Run (new System.Threading.CancellationTokenSource (15000).Token)) {
//				if (!request.Response.Success && request.Response.Error.Code != BlrtAPI.APIUserContactDetailDeleteError.RecordNotFound) {
//					wasError = true;
//				}
//			} else {
//				wasError = true;
//			}

//			if (wasError) {
//				PopLoading ("romoveContact", true);
//				BlrtHelperiOS.GenericOptionAlert ("profile_editemail_generic_connerror", "profile", true, RemoveFinish);
//				return;
//			}

//			_removingContact = null;

//			await _emailPage.RefreshItemSource (true);

//			PopLoading ("romoveContact", true);
//		}
//	}
//}
