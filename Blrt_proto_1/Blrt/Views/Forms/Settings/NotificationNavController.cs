using System;
using Xamarin.Forms;
using UIKit;
using BlrtData.Views.Settings;

namespace BlrtiOS.Views.Settings
{
	public class NotificationNavController:GenericNavigationController
	{
		private NotificationPage _notificationPage;

		public NotificationNavController ():base()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_notificationPage = new NotificationPage ();

			_childController = _notificationPage.CreateViewController ();

			_childController.Title = _notificationPage.Title;

			AddBackButton (ref _childController);

			_childController.NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, async (sender, e) => {
				PushLoading ("saveNotification",true);
				await _notificationPage.SavePressed (this, EventArgs.Empty);
				PopLoading ("saveNotification", true);
			});


			this.PushViewController (_childController,false);
		}
	}
}

