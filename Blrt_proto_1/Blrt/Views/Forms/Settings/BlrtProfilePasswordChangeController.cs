using System;
using MonoTouch.Dialog;
using BlrtiOS.Views.Util;
using System.Linq;
using UIKit;
using Parse;
using BlrtData;
using System.Threading.Tasks;
using BlrtShared;

namespace BlrtiOS.Views.Settings
{

	public class BlrtProfilePasswordChangeController : BaseDialogViewController
	{
		public override string ViewName { get { return "Profile_EditProfile_ChangePassword"; } }

		public event EventHandler OnCancelPressed;
		public event EventHandler OnSuccessfulSave;

		private EntryElement 	_oldPassword;
		private Section 		_oldPasswordSection;
		private EntryElement 	_newPassword;
		private EntryElement 	_newPasswordConfirm;


		private Section _emailSection;
		private StringElement _emailElement;

		public bool ShowEmailSection {
			get { return Root.Contains (_emailSection); }
			set {
				if (value != ShowEmailSection) {
					if (value)
						Root.Insert (0, _emailSection);
					else
						Root.Remove (_emailSection);
				}
			}
		}

		public BlrtProfilePasswordChangeController () : base (null)
		{
			_oldPasswordSection = new Section (BlrtHelperiOS.Translate ("edit_password_old_section_title", "profile"),
				BlrtHelperiOS.Translate ("edit_password_old_section_footer", "profile")){
				(_oldPassword = new EntryElement(BlrtHelperiOS.Translate("edit_password_old_field", "profile"), "", "", true){
					AutocapitalizationType  = UITextAutocapitalizationType.None,
					AutocorrectionType      = UITextAutocorrectionType.No,
				}),
			};


			_emailSection = new Section (){
				(_emailElement = new StringElement (BlrtHelperiOS.Translate ("profile_profile_email_field", "profile"), "") {
				})
			};


			Root = new RootElement (BlrtHelperiOS.Translate ("edit_password_title", "profile")){
				(new Section (BlrtHelperiOS.Translate("edit_password_new_section_title", "profile"),
					BlrtHelperiOS.Translate("edit_password_new_section_footer", "profile")){
					( _newPassword = new EntryElement(BlrtHelperiOS.Translate("edit_password_new_field", "profile"), "", "", true){
						AutocapitalizationType  = UITextAutocapitalizationType.None,
						AutocorrectionType      = UITextAutocorrectionType.No,
					}),
					( _newPasswordConfirm = new EntryElement(BlrtHelperiOS.Translate("edit_password_new_confirm_field", "profile"), "", "", true){
						AutocapitalizationType  = UITextAutocapitalizationType.None,
						AutocorrectionType      = UITextAutocorrectionType.No,
					})
				})
			};

			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Cancel, SafeEventHandler.FromAction (CancelPressed));
			NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, SafeEventHandler.PreventMulti (OnSavePressed));
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			_oldPassword.Value = "";
			_newPassword.Value = "";
			_newPasswordConfirm.Value = "";

			_emailElement.Value = ParseUser.CurrentUser.Email;


			if (BlrtData.BlrtUserHelper.HasSetPassword) {
				if (!Root.Contains (_oldPasswordSection)) {
					Root.Insert (Root.Count - 1, _oldPasswordSection);
				}
			} else {
				if (Root.Contains (_oldPasswordSection)) {
					Root.Remove (_oldPasswordSection);
				}
			}

			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Profile: Change Password");

			TableView.ReloadData ();
		}

		public void CancelPressed (object sender, EventArgs args)
		{
			if (_oldPassword.Value.Length > 0
				|| _newPassword.Value.Length > 0
				|| _newPasswordConfirm.Value.Length > 0) {

				var alert = new UIAlertView (
					BlrtHelperiOS.Translate ("edit_password_cancel_title", "profile"),
					BlrtHelperiOS.Translate ("edit_password_cancel_message", "profile"),
					null,
					BlrtHelperiOS.Translate ("edit_password_cancel_cancel", "profile"),
					BlrtHelperiOS.Translate ("edit_password_cancel_accept", "profile")
				);

				alert.Clicked += SafeEventHandler.FromAction (
					(object s, UIButtonEventArgs e) => {
						if (e.ButtonIndex != alert.CancelButtonIndex) {
							OnCancelPressed?.Invoke (this, EventArgs.Empty);
						}
					}
				);
				alert.Show ();
			} else {
				OnCancelPressed?.Invoke (this, EventArgs.Empty);
			}
		}

		public async Task OnSavePressed (object sender, EventArgs args)
		{
			if (String.IsNullOrWhiteSpace (_newPassword.Value) || String.IsNullOrWhiteSpace (_newPasswordConfirm.Value)) {
				var alert = new UIAlertView (
					BlrtHelperiOS.Translate ("edit_password_not_provided_title", "profile"),
					BlrtHelperiOS.Translate ("edit_password_not_provided_message", "profile"),
					null,
					BlrtHelperiOS.Translate ("edit_password_not_provided_cancel", "profile"));

				alert.Show ();
				return;
			}

			if (_newPassword.Value != _newPasswordConfirm.Value) {

				var alert = new UIAlertView (
					BlrtHelperiOS.Translate ("edit_password_match_error_title", "profile"),
					BlrtHelperiOS.Translate ("edit_password_match_error_message", "profile"),
					null,
					BlrtHelperiOS.Translate ("edit_password_match_error_cancel", "profile"));

				alert.Show ();
				return;
			}

			if (_newPassword.Value.Length < BlrtConstants.MinPasswordLength) {

				var alert = new UIAlertView (
					string.Format (BlrtHelperiOS.Translate ("edit_password_length_title", "profile"), BlrtConstants.MinPasswordLength),
					string.Format (BlrtHelperiOS.Translate ("edit_password_length_message", "profile"), BlrtConstants.MinPasswordLength),
					null,
					BlrtHelperiOS.Translate ("edit_password_length_cancel", "profile"));

				alert.Show ();
				return;
			}

			await CheckPasswordAsync (_oldPassword.Value);
		}


		public async Task CheckPasswordAsync (string password)
		{
			var nav = NavigationController as GenericNavigationController;
			nav.PushLoading ("passwordCheck", true);

			try {
				bool passed = !BlrtData.BlrtUserHelper.HasSetPassword;

				if (!passed) {
					passed = await BlrtUserHelper.CheckPassword (password);
				}

				if (passed) {
					ParseUser.CurrentUser [BlrtData.BlrtUserHelper.HasSetPasswordKey] = true;
					ParseUser.CurrentUser.Password = _newPassword.Value;
					await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
					OnSuccessfulSave?.Invoke (this, EventArgs.Empty);
				} else {
					new UIAlertView (
						BlrtHelperiOS.Translate ("edit_password_wrong_password_title", "profile"),
						BlrtHelperiOS.Translate ("edit_password_wrong_password_message", "profile"),
						null,
						BlrtHelperiOS.Translate ("edit_password_wrong_password_cancel", "profile")
					).Show ();
				}
				nav.PopLoading ("passwordCheck", true);
			} catch {
				nav.PopLoading ("passwordCheck", true);
				new UIAlertView (
					BlrtHelperiOS.Translate ("edit_profile_password_failed_title", "profile"),
					BlrtHelperiOS.Translate ("edit_profile_password_failed_message", "profile"),
					null,
					BlrtHelperiOS.Translate ("edit_profile_password_failed_cancel", "profile")
				).Show ();
			}
		}
	}
}

