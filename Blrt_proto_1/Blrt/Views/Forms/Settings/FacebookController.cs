//using System;
//using System.ComponentModel;
//using System.Runtime.CompilerServices;
//using BlrtData;
//using BlrtiOS.Views.Upgrade;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using UIKit;
//using MonoTouch.FacebookConnect;
//using CoreGraphics;
//using Foundation;
//using Parse;
//using BlrtiOS.Views.Util;
//using MonoTouch.Dialog;
//using System.Linq;
//
//namespace BlrtiOS.Views.Settings
//{
//	public class FacebookController: UIViewController
//	{
//		private UILabel _message;
//		private BlrteFacebookSigninView _fbSigninView;
//		private BlrteFacebookSignoutView _fbSignoutView;
//		private UIView _displayedFbView;
//
//		public const int labelHeight = 25;
//		public const int padding = 15;
//		public const int spacing = 2;
//
//		public FacebookController ():base()
//		{
//
//		}
//
//		public override void ViewDidLoad ()
//		{
//			base.ViewDidLoad ();
//
//			Title = BlrtUtil.PlatformHelper.Translate ("profile_facebook_title", "profile");
//			View.BackgroundColor = BlrtStyles.iOS.White;
//
//
//			_message = new UILabel () {
//				Frame = new CGRect (padding, padding, View.Bounds.Width - padding*2, labelHeight),
//				Lines = 0,
//				LineBreakMode = UILineBreakMode.WordWrap,
//				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
//				Font = BlrtStyles.iOS.CellTitleFont,
//				TextColor = BlrtStyles.iOS.Gray5,
//				Text = BlrtUtil.PlatformHelper.Translate("profile_fb_message","profile")
//			};
//
//			_fbSigninView = new BlrteFacebookSigninView (){
//				Frame = new CGRect (0, _message.Frame.Bottom+ spacing, View.Bounds.Width,  Login.GenericLoginController.BUTTON_HEIGHT + 30),
//			};
//
//			_fbSignoutView = new BlrteFacebookSignoutView () {
//				Frame = new CGRect (0, _message.Frame.Bottom+ spacing, View.Bounds.Width,  BlrtHelperiOS.IsPhone ? 64 : 56),
//			};
//
//			SetupFacebookDisplay ();
//
//			_fbSigninView.OnLogin += OnFacebookLink;
//			_fbSignoutView.UnlinkPressed += UnlinkFacebook;
//
//
//			View.Add (_message);
//			View.Add (_fbSigninView);
//			View.Add (_fbSignoutView);
//
//		}
//
//		public override void ViewWillAppear (bool animated)
//		{
//			base.ViewWillAppear (animated);
//			BlrtData.Helpers.BlrtTrack.TrackScreenView ("MyProfile_ConnectService_Facebook");
//		}
//
//		public override void ViewDidAppear (bool animated)
//		{
//			base.ViewDidAppear (animated);
//			_fbSigninView.Loading = false;
//		}
//
//		public override void ViewDidLayoutSubviews ()
//		{
//			base.ViewDidLayoutSubviews ();
//
//			_message.Frame = new CGRect (padding, padding, View.Bounds.Width - padding*2, labelHeight);
//			_message.SizeToFit ();
//
//			_displayedFbView.Frame = new CGRect (0, _message.Frame.Bottom+ spacing, View.Bounds.Width, _displayedFbView.Frame.Height);
//
//		}
//
//		public void SetupFacebookDisplay()
//		{
//			if (FBManager.IsLinked) {
//				if (!_fbSigninView.Hidden) {
//					_fbSigninView.Hidden = true;
//					_fbSignoutView.Hidden = false;
//					_displayedFbView = _fbSignoutView;
//				}
//				_fbSignoutView.UserName = ParseUser.CurrentUser.Get<string>(BlrtUserHelper.FacebookMetaName, ""); 
//
//			} else {
//
//				if (!_fbSignoutView.Hidden) {
//					_fbSignoutView.Hidden = true;
//					_fbSigninView.Hidden = false;
//					_displayedFbView = _fbSigninView;
//				}
//				_fbSigninView.Loading = false;
//			}
//
//			View.SetNeedsLayout ();
//		}
//
//		private async void OnFacebookLink(object sender, FBLoginViewUserInfoEventArgs arg)
//		{
//			try{
//				await FBManager.ConnectWithFacebook(FBSession.ActiveSession, arg.User);
//			} catch (Exception ex) {
//				if(!(FBSession.ActiveSession.State == FBSessionState.Closed || FBSession.ActiveSession.State == FBSessionState.ClosedLoginFailed))
//					FBSession.ActiveSession.CloseAndClearTokenInformation ();
//				UIAlertView alert = null;
//
//				// TODO: Avoid dodgy code...
//				if(ex.Message == BlrtAPI.APIUserContactDetailCreateError.DetailInUse.ToString ()) {
//					alert = new UIAlertView (
//						BlrtHelperiOS.Translate ("facebook_link_failed_user_exists_title", "profile"),
//						BlrtHelperiOS.Translate ("facebook_link_failed_user_exists_message", "profile"),
//						null,
//						BlrtHelperiOS.Translate ("facebook_link_failed_user_exists_cancel", "profile")
//					);
//				}
//
//				if(alert == null){
//					alert = new UIAlertView (
//						BlrtHelperiOS.Translate ("facebook_link_failed_unknown_title", "profile"),
//						BlrtHelperiOS.Translate ("facebook_link_failed_unknown_message", "profile"),
//						null,
//						BlrtHelperiOS.Translate ("facebook_link_failed_unknown_cancel", "profile")
//					);
//
//				}
//				alert.Show();
//			}
//			BlrtData.Helpers.BlrtTrack.ProfileConnectService ("Facebook");
//			await BlrtProfileHelper.ReloadContactDetails (true);
//			SetupFacebookDisplay();
//			View.LayoutSubviews();
//		}
//
//		private async void UnlinkFacebook(object sender, EventArgs arg)
//		{
//			_fbSigninView.Loading = true;
//			if (!FBManager.IsLinked) {
//
//				await BlrtProfileHelper.ReloadContactDetails (true);
//				SetupFacebookDisplay();
//				return;
//			}
//
//			if (BlrtUserHelper.HasSetPassword) {
//				var alert = new UIAlertView (
//					BlrtHelperiOS.Translate ("facebook_unlink_confirm_title", "profile"),
//					BlrtHelperiOS.Translate ("facebook_unlink_confirm_message", "profile"),
//					null,
//					BlrtHelperiOS.Translate ("facebook_unlink_confirm_cancel", "profile"),
//					BlrtHelperiOS.Translate ("facebook_unlink_confirm_accept", "profile")
//				);
//
//				alert.Clicked += async (object s, UIButtonEventArgs e) => {
//					if (e.ButtonIndex != alert.CancelButtonIndex) {
//						var nav = NavigationController as GenericNavigationController;
//						nav.PushLoading("fbLogin",true);
//
//						try {
//							await FBManager.DisconnectWithFacebook (FBSession.ActiveSession);
//							BlrtData.Helpers.BlrtTrack.ProfileDisconnectService ("Facebook");
//						} catch (Exception fbEx) {
//							new UIAlertView (
//								BlrtHelperiOS.Translate ("facebook_unlink_failed_unknown_title", "profile"),
//								BlrtHelperiOS.Translate ("facebook_unlink_failed_unknown_message", "profile"),
//								null,
//								BlrtHelperiOS.Translate ("facebook_unlink_failed_unknown_cancel", "profile")
//							).Show ();
//						}
////						await BlrtProfileHelper.ReloadContactDetails (true);
//						SetupFacebookDisplay();
//						View.LayoutSubviews();
//						nav.PopLoading("fbLogin",true);
//					}
//				};
//
//				alert.Show ();
//			} else {
//				new UIAlertView (
//					BlrtHelperiOS.Translate ("facebook_unlink_password_set_title", "profile"),
//					BlrtHelperiOS.Translate ("facebook_unlink_password_set_message", "profile"),
//					null,
//					BlrtHelperiOS.Translate ("facebook_unlink_password_set_accept", "profile")
//				).Show ();
//
//				var passwordController = new BlrtProfilePasswordChangeController ();
//				passwordController.ShowEmailSection = true;
//				passwordController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
//				passwordController.OnCancelPressed += (object s, EventArgs e) => {
//					DismissViewController(true, null);
//				};	
//				passwordController.OnSuccessfulSave += (object s, EventArgs e) => {
//					DismissViewController(true, null);
//					UnlinkFacebook(s, e);
//				};
//
//				PresentViewController (new GenericNavigationController (passwordController){
//					ModalPresentationStyle = UIModalPresentationStyle.FormSheet
//				}, true, null);
//
//			}
//		}
//			
//	}
//}
//
