//using System;
//using UIKit;
//using MonoTouch.FacebookConnect;
//using CoreGraphics;
//
//namespace BlrtiOS.Views.Settings
//{
//	public class BlrteFacebookSigninView : UIView
//	{
//		public bool Loading {
//			get { return _loading;}
//			set{
//				if (_loading != value)
//					RecalutateState (value);
//			}
//		}
//
//
//		private bool _loading;
//
//		private FBLoginView _facebookBtn;
//		private UIActivityIndicatorView _indicator;
//
//		public EventHandler<FBLoginViewUserInfoEventArgs> OnLogin { get; set; }
//
//		public BlrteFacebookSigninView() : base(new CGRect(0, 0, 320, Login.GenericLoginController.BUTTON_HEIGHT + 30))
//		{
//
//			AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
//
//
//			_indicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray){
//				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
//				Hidden =  true
//
//			};
//			AddSubview (_indicator);
//
//			if(!FBManager.IsLinked)
//				FBManager.Logout();
//
//
//			_facebookBtn = new FBLoginView (BlrtData.BlrtConstants.FacebookExtendedPermissions) {
//				Frame = new CGRect ((Frame.Width - Login.GenericLoginController.ButtonWidth) * 0.5f, (Frame.Height - Login.GenericLoginController.BUTTON_HEIGHT) * 0.5f, Login.GenericLoginController.ButtonWidth, Login.GenericLoginController.BUTTON_HEIGHT),
//				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin| UIViewAutoresizing.FlexibleRightMargin,
//			};
//
//			_facebookBtn.ShowingLoggedInUser += (object sender, EventArgs e) => { 
//				Loading = true; 
//			};
////			_facebookBtn.ShowingLoggedOutUser += (object sender, EventArgs e) => { Loading = true; };
//
//
//			_facebookBtn.FetchedUserInfo += SyncWithFacebook;
//			_facebookBtn.HandleError += (object sender, FBLoginViewErrorEventArgs e) => {
//				Loading = false;
//			};
//			AddSubview (_facebookBtn);
//		}
//
//		void RecalutateState(bool loading){
//
//			_loading = loading;
//
//			if (!_loading) {
//				_facebookBtn.Hidden = false;
//				_indicator.Hidden = true;
//				_indicator.StopAnimating ();
//			} else {
//				_indicator.Center = _facebookBtn.Center;
//
//				_facebookBtn.Hidden = true;
//				_indicator.StartAnimating ();
//				_indicator.Hidden = false;
//			}
//		}
//
//
//
//		public void SyncWithFacebook(object sender, FBLoginViewUserInfoEventArgs args)
//		{
//			if (sender != _facebookBtn || _facebookBtn.Window == null)
//				return;
//
//			RecalutateState (true);
//
//			if (OnLogin != null)
//				OnLogin (this, args);
//
//		}
//
//		public override void LayoutSubviews ()
//		{
//			if(Superview != null)
//				Frame = new CGRect((Superview.Frame.Width - Frame.Width) * 0.5f, Frame.Y, Frame.Width, Frame.Height);
//
//			RecalutateState (_loading);
//
//			base.LayoutSubviews ();
//		}
//	}
//}
//
