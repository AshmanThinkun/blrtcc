using System;
using Xamarin.Forms;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtData.Views.Settings;
using BlrtData.Views.Settings.Profile;
using BlrtData;




namespace BlrtiOS.Views.Settings
{
	public class SettingController : SlideDrawerViewController
	{
		UINavigationController _profileNav, _accountNav, _emailNav, _fbNav, _blockNav;

		UIViewController _master;
		UINavigationController _masterNavigation;

		SettingPage _settingPage;
		
		public SettingController ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_settingPage = new SettingPage ();

			_master = _settingPage.CreateViewController ();
			_masterNavigation = new BaseUINavigationController (_master);

			_master.Title = _settingPage.Title;
			_master.NavigationItem.LeftBarButtonItem = BlrtHelperiOS.OpenDrawerMenuButton (this);
			_master.NavigationItem.RightBarButtonItem = BlrtiOS.Views.CustomUI.NewBlrtButton.CreateBarBlrt (() => {
				Mailbox.MailboxListController.CreateNewBlrt ();
			});

			Master = _masterNavigation;

			OpenAccountPage ();

			//track page view for device bigger than ip6+
			if(Math.Min(BlrtUtil.PlatformHelper.GetDeviceWidth(), BlrtUtil.PlatformHelper.GetDeviceHeight()) > 413){
				BlrtData.Helpers.BlrtTrack.TrackScreenView("Settings_Account");
			}

			_settingPage.RowSelected+= (sender, e) => {
				RowPressed((e.SelectedItem as CellItem).NavText);
			};

			ShowSlider (false);

			Upgrade.UpgradeController.OnPremiumStateChange += (object sender, EventArgs e) => {
				if (_settingPage != null)
					_settingPage.RefreshItemSource ();
			};
		}


		public override void ViewDidLayoutSubviews ()
		{
			var isPhalet = NMath.Min (UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) > 400;
//			var isTablet = NMath.Min(UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height) > 640;

			if (isPhalet) {
				MasterWidth = 320;
				SlaveWidth = 320;
			} else {
				MasterWidth = 1024;
				SlaveWidth = 1024;
			}

			base.ViewDidLayoutSubviews ();
		}

		private void RowPressed (string navText)
		{
			switch (navText) {
				case "account":
					OpenAccountPage ();
					BlrtData.Helpers.BlrtTrack.TrackScreenView("Settings_Account");
					HideSlider (true);
					break;

				case "device":
					var devicePage = new DeviceSettingsPage ();
					var deviceController = devicePage.CreateViewController ();
					deviceController.Title = devicePage.Title;
					var deviceNav = new GenericNavigationController (deviceController);
					deviceNav.AddBackButton (ref deviceController);
					deviceNav.BackPressed += (a, b) => {
						ShowSlider (true);
					};

					Slave = deviceNav;
					HideSlider (true);
					break;
				case "notifications":
					var notificationNav = new NotificationNavController ();
					notificationNav.BackPressed += (a, b) => {
						ShowSlider (true);
					};

					Slave = notificationNav;
					HideSlider (true);
					break;

				case "update":
					updateBlrt ();
					break;
				case "review":
					reviewBlrt ();
					break;
				case "block":
					if(null == _blockNav){
						GenericNavigationController block;
						var blockPage = new BlockUserPage ();
						var blockPageController = blockPage.CreateViewController ();
						blockPageController.Title = blockPage.Title;
						_blockNav= block = new GenericNavigationController (blockPageController);
						block.AddBackButton (ref blockPageController);
						block.BackPressed+= (a,b)=>{
							ShowSlider (true);
						};
					}
					Slave = _blockNav;
					HideSlider(true);
					break;

			case "privacy":
				Device.OpenUri (new Uri (BlrtSettings.PrivacyUrl));
				break;

			case "tos":
				Device.OpenUri (new Uri (BlrtSettings.TermOfServiceUrl));
				break;

				default:
					BlrtUtil.Debug.ReportException (new NotImplementedException ("unimplemented setting item: " + navText), null, Xamarin.Insights.Severity.Critical);
					break;
				// commented because they are already moved from setting screen
				//case "email":
				//	if (null == _emailNav) {
				//		EmailNavigationController email;
				//		_emailNav = email = new EmailNavigationController ();
				//		email.BackPressed += (a, b) => {
				//			ShowSlider (true);
				//		};
				//	} else {
				//		(_emailNav as EmailNavigationController).RefreshItemSource ();
				//	}
				//	Slave = _emailNav;
				//	HideSlider (true);
				//	break;
				//case "version":
				//	var versionPage = new VersionPage ();
				//	var versionController = versionPage.CreateViewController ();
				//	versionController.Title = versionPage.Title;
				//	var versionNav = new GenericNavigationController (versionController);
				//	versionNav.AddBackButton (ref versionController);
				//	versionNav.BackPressed += (a, b) => {
				//		ShowSlider (true);
				//	};
				//	Slave = versionNav;
				//	HideSlider (true);
				//	break;
			}
		}


		void OpenAccountPage ()
		{
			var accountPage = new AccountPage ();
			var accountController = accountPage.CreateViewController ();
			accountController.Title = string.IsNullOrWhiteSpace (accountPage.Title) ? "Account" : accountPage.Title;
			var _accountNav = new GenericNavigationController (accountController);
			_accountNav.AddBackButton (ref accountController);
			_accountNav.BackPressed += (a, b) => {
				ShowSlider (true);
			};
			this._accountNav = _accountNav;

			accountPage.OnUpgrade += (a, b) => {
				RefreshItemSource ();
				accountController.Title = accountPage.Title;
			};
			Slave = this._accountNav;
		}

		public void RefreshItemSource ()
		{
			_settingPage.RefreshItemSource ();
		}

		public override void ShowSlider (bool animate)
		{
			base.ShowSlider (animate);
			_settingPage.DeSelectRow ();
		}

		public void updateBlrt ()
		{
			BlrtData.Helpers.BlrtTrack.SettingsCheckUpdate ();
			BlrtUtil.PlatformHelper.OpenAppStore ();
		}

		public void reviewBlrt ()
		{
			string id = "id";
			string appUrl = BlrtSettings.AppStoreUrl;
			var idIndex = appUrl.IndexOf (id);
			var lastIndex = appUrl.IndexOf ("?");
			string appId = "";
			if (idIndex == -1 || lastIndex < idIndex + id.Length) {
				return;
			} else {
				appId = appUrl.Substring (idIndex + id.Length, lastIndex - id.Length - idIndex);
			}
			BlrtData.Helpers.BlrtTrack.SettingsLeaveReview ();

			UIApplication.SharedApplication.OpenUrl (
				new NSUrl ("http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=" + appId + "&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8")
			);
		}

	}
}

