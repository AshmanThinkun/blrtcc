using System;
using UIKit;
using Xamarin.Forms;
using System.Collections.Generic;

namespace BlrtiOS
{
	public static class FormsHelper
	{
		static Dictionary<Page, WeakReference<UIViewController>> _registeredPages;
		public static UIViewController SharedViewController(this Page formsPage)
		{
			if (null == formsPage) {
				return null;
			}
			if (null == _registeredPages) {
				_registeredPages = new Dictionary<Page, WeakReference<UIViewController>> ();
			}
			WeakReference<UIViewController> pageRef = null;
			UIViewController iosPage = null;
			if (_registeredPages.TryGetValue (formsPage, out pageRef) && (null != pageRef) && pageRef.TryGetTarget (out iosPage) && (null != iosPage)) {
				return iosPage;
			} else {
				iosPage = formsPage.CreateViewController ();
				_registeredPages [formsPage] = new WeakReference<UIViewController> (iosPage);
				return iosPage;
			}
		}
	}
}

