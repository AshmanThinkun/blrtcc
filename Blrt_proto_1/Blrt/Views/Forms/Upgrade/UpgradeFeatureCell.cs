using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
namespace BlrtiOS.Views.Upgrade
{
	public class UpgradeFeatureCell : ViewCell
	{
		Image _image;
		Label _titleLabel, _textLabel;

		const int IMG_WIDTH = 58;
		static readonly Font TitleFont = Font.SystemFontOfSize (15);
		static readonly Font TextFont = Font.SystemFontOfSize (13.5);


		public UpgradeFeatureCell()
		{
			_image = new Image() {
				HorizontalOptions = LayoutOptions.Start
			};
			_image.WidthRequest = _image.HeightRequest = IMG_WIDTH;

			View = new StackLayout() {
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Padding = new Thickness(15,0,15,0),
				Spacing = 15,
				Children = { 
					_image, 
					new StackLayout()
					{
						HorizontalOptions = LayoutOptions.StartAndExpand,
						VerticalOptions = LayoutOptions.Center,
						Orientation = StackOrientation.Vertical,
						Children = { 
							(_titleLabel = new Label{
								HorizontalOptions= LayoutOptions.FillAndExpand,
								FontFamily = TitleFont.FontFamily,
								FontAttributes = TitleFont.FontAttributes,
								FontSize = TitleFont.FontSize,
							}), 
							(_textLabel = new Label{
								HorizontalOptions = LayoutOptions.FillAndExpand,
								FontFamily = TextFont.FontFamily,
								FontAttributes = TextFont.FontAttributes,
								FontSize = TextFont.FontSize,
							}) 
						}
					}, 
				}
			};


			_image.SetBinding(Image.SourceProperty, "ImageUri");
			_titleLabel.SetBinding(Label.TextProperty, "Title");
			_textLabel.SetBinding(Label.TextProperty, "Text");
		}
	}
}

