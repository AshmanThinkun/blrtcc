using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using Newtonsoft.Json;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.Upgrade
{
	public abstract class UpgradeController : BlrtContentPage, IExecutor
	{
		const int ROW_HEIGHT = 80;
		const int TRIAL_SEC_HEIGHT = 70;

		static readonly Font TitleTextFont = Font.SystemFontOfSize (12);

		protected Label _titleLbl;
		protected Button _trialBtn;
		protected ContentView _btnSec;

		public EventHandler TrialClicked{ get; set;}
		public EventHandler<EventArgs> OnDismiss { get; set; }
		public EventHandler<EventArgs> OnSuccess { get; set; }
		public EventHandler OnShowLoading{ get; set;}
		public EventHandler OnDismissLoading{ get; set;}
		public static event EventHandler OnPremiumStateChange;

		public static AccountUpgradeFooterViewState State;
		protected static readonly TimeSpan	MinimumTimeToExpirationForDayDisplay = new TimeSpan (30, 0, 0, 0);


		public override string ViewName {
			get {
				return "Upgrade";
			}
		}

		public override void TrackPageView ()
		{
			var dic = new Dictionary<string, object> ();
			dic.Add ("accountType", BlrtPermissions.AccountName);
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, dic);
		}

		public UpgradeController ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("upgrade_title", "upgrade");

			_titleLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("upgrade_text", "upgrade"),
				FontFamily = TitleTextFont.FontFamily,
				FontAttributes = TitleTextFont.FontAttributes,
				FontSize = TitleTextFont.FontSize,
				XAlign = TextAlignment.Start,
			};

			_trialBtn = new FormsBlrtButton () {
				Text = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_button", "iap"),
				BackgroundColor = BlrtStyles.BlrtCharcoal,
				TextColor = BlrtStyles.BlrtGreenHighlight,
				HeightRequest = 30,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Padding = new Thickness(10,4),
			};

			_trialBtn.Clicked += ButtonClick;

			var listView = new FormsListView{
				RowHeight = ROW_HEIGHT,
			};
			listView.ItemsSource = getFeatureObjs (BlrtSettings.UpgradeModalJson);
			listView.ItemTemplate = new DataTemplate(typeof(UpgradeFeatureCell));

			listView.ItemTapped += (sender, e) => {
				((ListView)sender).SelectedItem = null; // de-select the row
			};

			Content = new Grid(){
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
				},
				Padding = 0,
				RowSpacing = 0,
				ColumnSpacing = 0,


				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { 
					{new StackLayout (){
							VerticalOptions = LayoutOptions.EndAndExpand,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							Padding = new Thickness(15,15,0,0),
							Spacing = 15,
							Children = {
								new ContentView (){
									HorizontalOptions = LayoutOptions.FillAndExpand,
									Padding = new Thickness(10,0,0,0),
									Content = _titleLbl,
								},
								new ContentView (){
									Padding = 0,
									HorizontalOptions = LayoutOptions.FillAndExpand,
									HeightRequest = 1,
									BackgroundColor = BlrtStyles.BlrtGray5,
								}
							},
						},0,0},
					{listView,0,1},
					{_btnSec = new ContentView (){
							VerticalOptions = LayoutOptions.Fill,
							HorizontalOptions = LayoutOptions.Fill,
							BackgroundColor = BlrtStyles.BlrtGray3,
							HeightRequest = TRIAL_SEC_HEIGHT,
							Content = _trialBtn,
					},0,2}
				}
			};

		}

		#region IExecutor implementation
		ExecutionSource IExecutor.Cause {
			get {
				return ExecutionSource.InApp;
			}
		}
		#endregion

		protected void ButtonClick(object sender, EventArgs e){
				RequestUpgrade();
				if (TrialClicked != null)
					TrialClicked (this, EventArgs.Empty);
		}

		private Feature[] getFeatureObjs(string jsonString){

			Feature[] parsedObjects;

			try {
				parsedObjects = JsonConvert.DeserializeObject<Feature[]> (jsonString);
			}
			// JSON format errors
			catch (JsonReaderException exception) {
				Console.Error.WriteLine (exception.Message);

				return null;
			}

			return parsedObjects;
		}

		public class Feature : BlrtNotifyProperty{

			string _title,_text,_imageUri;

			[JsonProperty("title")]
			public string Title {
				get{ return _title;}
				set{
					OnPropertyChanged (ref _title, value);
				}
			}

			[JsonProperty("text")]
			public string Text {
				get{ return _text;}
				set{
					OnPropertyChanged (ref _text, value);
				}
			}

			[JsonProperty("imageUri")]
			public string ImageUri {
				get{ return _imageUri;}
				set{
					OnPropertyChanged (ref _imageUri, value);
				}
			}
		}




		protected void Dismiss (object sender, EventArgs args) {
			if (OnDismiss != null)
				OnDismiss (sender, args);
		}


		protected async void RequestUpgrade () {
			StartLoading ();


			BlrtAccountUpgradeRequest request = BuildRequest ();

			if(request==null){
				EndLoading ();
				var support = await DisplayAlert(
					BlrtUtil.PlatformHelper.Translate("no_iap_title","upgrade"),
					BlrtUtil.PlatformHelper.Translate("no_iap_message","upgrade"),
					BlrtUtil.PlatformHelper.Translate("no_iap_support","upgrade"),
					BlrtUtil.PlatformHelper.Translate("no_iap_ok","upgrade")
				);

				if (support) {
//					Dismiss(this,EventArgs.Empty);
//					var help = await BlrtManager.App.OpenSupport(this);
//					help.Result.OpenMessageSupport();
					await BlrtManager.App.OpenSendSupportMsg(this);
				}
				return;
			}

			BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestResponse result = await request.Start ();


			if (result != null && result.Success) {
				EndLoading ();

				if (OnSuccess != null)
					OnSuccess (this, EventArgs.Empty);

				Success ();
				if(OnPremiumStateChange!=null){
					OnPremiumStateChange (this, EventArgs.Empty);
				}
			} else {
				EndLoading ();

				Error (result != null ? result.ErrorCode : null);
			}
		}


		protected abstract BlrtAccountUpgradeRequest BuildRequest ();


		protected virtual void StartLoading ()
		{
			if (null != OnShowLoading)
				OnShowLoading (this, EventArgs.Empty);
		}
		protected virtual void EndLoading ()
		{
			if (null != OnDismissLoading)
				OnDismissLoading (this, EventArgs.Empty);
		}


		protected abstract void Success ();
		protected abstract void Error (BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode? error);


		public enum AccountUpgradeFooterViewState {
			Hidden,

			FreeTrial,
			TrialToPremium,
			Premium,
			PremiumRenew
		};


		public static AccountUpgradeFooterViewState GetAccountUpgradeState () {
			switch (BlrtPermissions.AccountThreshold) {
				case BlrtPermissions.BlrtAccountThreshold.Free:
					if (!BlrtUserHelper.HasDoneFreeTrial)
						State = AccountUpgradeFooterViewState.FreeTrial;
					else
						State = AccountUpgradeFooterViewState.Premium;

					break;
				case BlrtPermissions.BlrtAccountThreshold.Trial:
					State = AccountUpgradeFooterViewState.TrialToPremium;
					break;
				case BlrtPermissions.BlrtAccountThreshold.Premium:
					TimeSpan? timeToExpiration = BlrtUserHelper.TimeToAccountTypeExpiration ();

					if (timeToExpiration != null && timeToExpiration.Value < MinimumTimeToExpirationForDayDisplay) {
						State = AccountUpgradeFooterViewState.PremiumRenew;

						break;
					}

					goto default;
				default:
					State = AccountUpgradeFooterViewState.Hidden;
					break;
			}
			return State;
		}

		public static string GetUpgradeTitle(){
			if(Upgrade.UpgradeController.GetAccountUpgradeState() != BlrtiOS.Views.Upgrade.UpgradeController.AccountUpgradeFooterViewState.Hidden){
				if (Upgrade.UpgradeController.GetAccountUpgradeState () == BlrtiOS.Views.Upgrade.UpgradeController.AccountUpgradeFooterViewState.PremiumRenew) {
					return BlrtHelperiOS.Translate ("premium_modal_renew_title", "iap");
				} else {
					return BlrtHelperiOS.Translate ("other_upgrade", "app_menu");
				}
			}else{
				return BlrtHelperiOS.Translate("premium_features_title","iap");
			}
		}


		public static UpgradeController CreateModalController ()
		{
			GetAccountUpgradeState ();
			switch (State) {
				case AccountUpgradeFooterViewState.FreeTrial:
					return new UpgradeTrialPage ();
				case AccountUpgradeFooterViewState.TrialToPremium:
				case AccountUpgradeFooterViewState.Premium:
					return new UpgradePremiumPage ();
				case AccountUpgradeFooterViewState.PremiumRenew:
					return new UpgradePremiumPage (true);
				default:
					return new UpgradeTrialPage(false);
			}
		}
	}
}

