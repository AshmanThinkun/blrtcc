using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BlrtiOS.Views.Upgrade
{
	public class UpgradePremiumPage :UpgradeController
	{
		private bool _forRenew;

		public UpgradePremiumPage (bool forRenew = false):base()
		{
			_forRenew = forRenew;
			if(_forRenew){
				Title = BlrtUtil.PlatformHelper.Translate ("premium_modal_renew_title", "iap");
			}

			try{
			_trialBtn.Text = BlrtiOSIAPManager.PremiumIAP.ButtonText;
			}catch(Exception e){
				_trialBtn.Text = "Upgrade to premium";
			}
		}

		protected override BlrtAccountUpgradeRequest BuildRequest ()
		{
			if (BlrtiOSIAPManager.PremiumIAP == null || !BlrtiOSIAPManager.CanMakePayments ())
				return null;
			return new BlrtiOSIAPRequest (BlrtiOSIAPManager.PremiumIAP);
		}


		protected async override void Success ()
		{
			string title;
			string message;
			string buttonText;

			if (_forRenew) {
				title = BlrtUtil.PlatformHelper.Translate ("premium_modal_renew_success_popup_title", "iap");
				message = BlrtUtil.PlatformHelper.Translate ("premium_modal_renew_success_popup_message", "iap");
				buttonText = BlrtUtil.PlatformHelper.Translate ("premium_modal_renew_success_popup_buttontext", "iap");
			} else {
				title = BlrtUtil.PlatformHelper.Translate ("premium_modal_upgrade_success_popup_title", "iap");
				message = BlrtUtil.PlatformHelper.Translate ("premium_modal_upgrade_success_popup_message", "iap");
				buttonText = BlrtUtil.PlatformHelper.Translate ("premium_modal_upgrade_success_popup_buttontext", "iap");
				try{
					var price = Double.Parse(BlrtiOSIAPManager.PremiumIAP.product.Price.ToString());
					Yozio.Yozio.TrackPayment (price);
				}catch{
				}
			}


			await DisplayAlert (title,message,buttonText);
			Dismiss (this, EventArgs.Empty);
		}


		protected async override void Error (BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode? error)
		{
			string title = null;
			string message = null;
			string cancelText = null;
			string tryAgainText = null;

			switch (error) {
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.ServerConnectionError:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_serverconnectionerror_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_serverconnectionerror_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_serverconnectionerror_popup_buttontext", "iap");
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("premium_modal_serverconnectionerror_popup_tryagaintext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.DuplicateRequest:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_duplicaterequest_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_duplicaterequest_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_duplicaterequest_popup_buttontext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSUnknownPaymentError:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosunknownpaymenterror_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosunknownpaymenterror_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosunknownpaymenterror_popup_buttontext", "iap");
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosunknownpaymenterror_popup_tryagaintext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSAppStoreConnectionFailed:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosappstoreconnectionfailed_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosappstoreconnectionfailed_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosappstoreconnectionfailed_popup_buttontext", "iap");
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iosappstoreconnectionfailed_popup_tryagaintext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSCannotMakePayments:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_ioscannotmakepayments_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_ioscannotmakepayments_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_ioscannotmakepayments_popup_buttontext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSPaymentFailed:
					title = BlrtUtil.PlatformHelper.Translate ("premium_modal_iospaymentfailed_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("premium_modal_iospaymentfailed_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iospaymentfailed_popup_buttontext", "iap");
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("premium_modal_iospaymentfailed_popup_tryagaintext", "iap");

					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.Unknown:
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("premium_modal_unknown_popup_tryagaintext", "iap");

					goto default;
				default:
					if (title == null) {
						title = BlrtUtil.PlatformHelper.Translate ("premium_modal_unknown_popup_title", "iap");
						message = BlrtUtil.PlatformHelper.Translate ("premium_modal_unknown_popup_message", "iap");
						cancelText = BlrtUtil.PlatformHelper.Translate ("premium_modal_unknown_popup_buttontext", "iap");
					}


					if (tryAgainText != null) {
						var shouldTryAgain = await DisplayAlert (title,message,tryAgainText,cancelText);
						if(shouldTryAgain)
							RequestUpgrade ();
						else{
							Dismiss (this, EventArgs.Empty);
						}
					}else{
						DisplayAlert (title, message, cancelText);
					}
						

					break;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.PaymentCancelled:
					// Just stay on the modal >.>

					break;
			}
		}
	}
}

