using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using Newtonsoft.Json;
using System.Collections.Generic;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.Upgrade
{
	public class UpgradeTrialPage: UpgradeController
	{
		public UpgradeTrialPage (bool showButton=true):base()
		{
			_trialBtn.Text = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_button", "iap");

			if(!showButton){
				_trialBtn.IsVisible = false;
				_btnSec.IsVisible = false;
				Title = BlrtUtil.PlatformHelper.Translate ("premium_features_title", "iap");
				_titleLbl.Text = BlrtUtil.PlatformHelper.Translate ("upgrade_text_features", "upgrade");
			}
		}


		protected override BlrtAccountUpgradeRequest BuildRequest ()
		{
			return new BlrtFreeTrialRequest ();
		}


		protected async override void Success ()
		{
			string title = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_success_popup_title", "iap");
			string message = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_success_popup_message", "iap");
			string buttonText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_success_popup_buttontext", "iap");
			BlrtData.Helpers.BlrtTrack.AccountStartTrial ();
			await DisplayAlert (title,message,buttonText);
			Dismiss (this, EventArgs.Empty);
		}


		protected override async void Error (BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode? error)
		{
			string title = null;
			string message = null;
			string cancelText = null;
			string tryAgainText = null;

			bool populated = false;


			switch (error) {
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.ServerConnectionError:
					title = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_serverconnectionerror_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_serverconnectionerror_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_serverconnectionerror_popup_dismisstext", "iap");
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_serverconnectionerror_popup_tryagaintext", "iap");

					populated = true;


					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.TrialAlreadyUsed:
					title = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_trialalreadyused_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_trialalreadyused_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_trialalreadyused_popup_dismisstext", "iap");

					populated = true;


					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.CurrentAccountTypeSuperior:
					title = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_currentaccounttypesuperior_popup_title", "iap");
					message = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_currentaccounttypesuperior_popup_message", "iap");
					cancelText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_currentaccounttypesuperior_popup_dismisstext", "iap");

					populated = true;


					goto default;
				case BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.Unknown:
					tryAgainText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_unknown_popup_tryagaintext", "iap");


					goto default;
				default:
					if (!populated) {
						title = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_unknown_popup_title", "iap");
						message = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_unknown_popup_message", "iap");
						cancelText = BlrtUtil.PlatformHelper.Translate ("free_trial_modal_unknown_popup_dismisstext", "iap");
					}



					if (tryAgainText != null) {
						var shouldTryAgain = await DisplayAlert (title,message,tryAgainText,cancelText);
						if(shouldTryAgain)
							RequestUpgrade ();
					}else{
						DisplayAlert (title, message, cancelText);
					}

					break;
			}
		}
	}
}

