using System;
using UIKit;
using Xamarin.Forms;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Upgrade
{
	public class UpgradeNavController: Util.BaseUINavigationController, IUpgradeScreen, IExecutionBlocker
	{
		public UpgradeController UpgradePage;
		private UIViewController _upgradeController;
		CustomUI.BlrtLoadingOverlayView _loader;

		TaskCompletionSource<bool> _tcs;


		public UpgradeNavController (): base()
		{
			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			_tcs = new TaskCompletionSource<bool> ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationBar.Translucent = false;
			NavigationBar.BackgroundColor = BlrtStyles.iOS.Green;
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;


			_loader = new BlrtiOS.Views.CustomUI.BlrtLoadingOverlayView ();
			View.AddSubview (_loader);

			UpgradePage = UpgradeController.CreateModalController ();
			_upgradeController = UpgradePage.SharedViewController ();

			_upgradeController.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem(UIBarButtonSystemItem.Cancel, (s,ev)=>{
				DismissViewController (true,null);
				_tcs.TrySetResult (true);
			}),false);
			_upgradeController.Title = UpgradePage.Title;

			UpgradePage.OnDismiss += (sender, e) => {
				DismissViewController (true,null);
				_tcs.TrySetResult (true);
			};

			UpgradePage.OnShowLoading += (sender, e) => {
				InvokeOnMainThread (()=>{
					PushLoading ("upgrade",true);
				});
			};
			UpgradePage.OnDismissLoading += (sender, e) => {
				InvokeOnMainThread (()=>{
					PopLoading ("upgrade",true);
				});
			};

			this.PushViewController (_upgradeController,false);

		}

		public override void PushLoading (string cause, bool animated)
		{
			_loader.Push (cause, animated);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loader.Pop (cause, animated);
		}

		#region IUpgradeScreen implementation

		public Task WaitAsyncCompletion ()
		{
			return _tcs.Task;
		}

		#endregion

		#region IExecutionBlocker implementation
		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;
		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			DismissViewController (true,null);
			_tcs.TrySetResult (true);
			await _tcsLeaveThisScreen.Task;
			return true;
		}

		#endregion

		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				// enter navigation stack
				ExecutionPerformer.RegisterBlocker (this);
			}
			base.ViewWillAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}
	}
}

