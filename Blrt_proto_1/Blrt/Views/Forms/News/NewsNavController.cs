using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;
using BlrtData.Views.News;
using Foundation;
using UIKit;
using Xamarin.Forms;
using BlrtiOS.Views.Util;

namespace BlrtiOS.Views.News
{
	public class NewsNavController: BaseUINavigationController
	{
		private NewsPage _newsPage;
		public NewsNavController (): base()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_newsPage = new NewsPage ();
			_newsPage.ReadMoreClicked += OnNewsItemClicked;

			var controller = _newsPage.CreateViewController ();
			controller.Title = _newsPage.Title;
			controller.NavigationItem.LeftBarButtonItem = BlrtHelperiOS.OpenDrawerMenuButton(this);


			controller.NavigationItem.RightBarButtonItem = BlrtiOS.Views.CustomUI.NewBlrtButton.CreateBarBlrt (()=>{
				Mailbox.MailboxListController.CreateNewBlrt();
			});


			PushViewController (controller, false);

			RefreshContent ();
		}

		private async void RefreshContent()
		{
			PushLoading ("newsFetch", true);
			await _newsPage.RefreshItemsSource ();
			PopLoading ("newsFetch", true);
		}


		private void OnNewsItemClicked(object sender, string url)
		{
			if (url == null)
				return;


			var webController = new WebViewController () {
				ModalPresentationStyle =  UIModalPresentationStyle.FullScreen
			};
			webController.Url = url;


			var _webNavController = new UINavigationController( webController );
			_webNavController.ModalPresentationStyle = webController.ModalPresentationStyle;


			webController.OnDismiss += (s1, e1) =>{
				DismissViewController(true, null);
			};


			PresentViewController (_webNavController, true, null);

			var dic = new Dictionary<string, object> ();
			dic.Add ("url", url);
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("News_Item",false,dic);
		}
	}
}

