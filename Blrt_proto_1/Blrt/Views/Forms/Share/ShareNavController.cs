using System;
using UIKit;
using Xamarin.Forms;
using BlrtiOS.ViewControllers;
using Social;
using Foundation;
using BlrtData;
using MessageUI;
using Newtonsoft.Json;
using BlrtiOS.Views.Settings;
using BlrtData.ExecutionPipeline;
using System.Web;
using BlrtData.Views.Share;

namespace BlrtiOS.Views.Share
{
	public class ShareNavController: Util.BaseUINavigationController
	{
		public ShareController SharePage;
		private UIViewController _shareController;

		public ShareNavController (): base()
		{
			ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
		}

		public const int LeftBtnSpace = 10;


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationBar.Translucent = false;
			NavigationBar.BackgroundColor = BlrtStyles.iOS.Green;
			NavigationBar.BarTintColor = BlrtStyles.iOS.Green;

			SharePage = new ShareController ();
			_shareController = SharePage.CreateViewController ();

			_shareController.View.TintColor = BlrtStyles.iOS.White;




			var btn = BlrtHelperiOS.OpenDrawerMenuButton (this);


			_shareController.NavigationItem.RightBarButtonItem = BlrtiOS.Views.CustomUI.NewBlrtButton.CreateBarBlrt (()=>{
				Mailbox.MailboxListController.CreateNewBlrt();
			});
			_shareController.NavigationItem.SetLeftBarButtonItem (btn, false);
			_shareController.Title = SharePage.Title;



			this.PushViewController (_shareController,false);

			SharePage.fbShareClicked += FacebookSharePressed;
			SharePage.twShareClicked += TwitterSharePressed;
			SharePage.emailShareClicked += EmailSharePressed;
			SharePage.smsShareClicked += SmsSharePressed;
			SharePage.fbInviteClicked += BigSharseBtn;

		}




		private async void FacebookSharePressed(object sender, string args)
		{



			bool value = SLComposeViewController.IsAvailable (SLServiceType.Facebook);

			if (IsAppInstalled("fb://")) {

				var slComposer = SLComposeViewController.FromService (SLServiceType.Facebook);

				BlrtData.Helpers.BlrtTrack.Share ("Facebook");
				PushLoading ("facebookShare", true);

				slComposer.AddUrl (NSUrl.FromString (args));
				slComposer.SetInitialText (BlrtUtil.PlatformHelper.Translate ("message_facebook", "share"));


				PresentViewController (slComposer, true, () => {
					PopLoading ("facebookShare", true);
				});
			} else
			{
				ShowAppDoesNotExistAlert ("Facebook");
				return;
			
			}

		}

		private bool IsAppInstalled (string packageName)
		{
			bool isInstalled = false;
			string nsString = packageName;

			var nsUrl = new NSUrl (packageName);


			if(UIApplication.SharedApplication.CanOpenUrl(nsUrl))
			{
				isInstalled = true;
			}

			return isInstalled;
		}



		private void ShowAppDoesNotExistAlert(string appName= "App")
		{
			var alertController = UIAlertController.Create (appName+" not installed", "Please install the "+appName+" app from the App store to proceed. ", UIAlertControllerStyle.Alert);


			alertController.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Default, null));


			PresentViewController (alertController, true, null);
			return;
		}


		private async void TwitterSharePressed(object sender, string args)
		{
			//if (SLComposeViewController.IsAvailable (SLServiceType.Twitter)) {
			if(IsAppInstalled("twitter://")){
			BlrtData.Helpers.BlrtTrack.Share ("Twitter");
				PushLoading ("twitterShare", true);

				var slComposer = SLComposeViewController.FromService (SLServiceType.Twitter);

				slComposer.AddUrl (NSUrl.FromString (args));
				slComposer.SetInitialText (BlrtUtil.PlatformHelper.Translate ("message_twitter", "share"));

				PresentViewController (slComposer, true, () => {
					PopLoading ("twitterShare", true);
				});
			} else
			{

				ShowAppDoesNotExistAlert ("Twitter");
			}
		}

		private async void EmailSharePressed(object sender, string args)
		{
			if (MFMailComposeViewController.CanSendMail) {
				BlrtData.Helpers.BlrtTrack.Share ("email");
				PushLoading ("emailShare", true);

				var name = BlrtUserHelper.CurrentUserName;

				var m = new MFMailComposeViewController () {
				};
				m.SetSubject (String.Format (BlrtUtil.PlatformHelper.Translate ("message_email_subject", "share"), name));
				m.SetMessageBody (string.Format (BlrtUtil.PlatformHelper.Translate ("message_email", "share"), name, args, BlrtUtil.GetImpressionHtml("share_email")), true);

				m.Finished += (object s, MFComposeResultEventArgs e) => {
					DismissViewController (true, null);
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				PresentViewController (m, true, ()=>{
					PopLoading("emailShare",true);
				});
			} else {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert(
						platforHelper.Translate ("cannot_send_email_error_title"),
						platforHelper.Translate ("cannot_send_email_error_message"),
						platforHelper.Translate ("cannot_send_email_error_cancel")
					)
				);
			}
		}

		private async void SmsSharePressed(object sender, string args)
		{
			if (MFMessageComposeViewController.CanSendText) {

				BlrtData.Helpers.BlrtTrack.Share ("SMS");
				PushLoading ("smsShare", true);


				var m = new MFMessageComposeViewController () {
					Body = string.Format (BlrtUtil.PlatformHelper.Translate ("message_sms", "share"), args),
				};
				m.Finished += (object s, MFMessageComposeResultEventArgs e) => {
					DismissViewController (true, null);
					if(e.Result == MessageComposeResult.Sent){
						BlrtUtil.TrackImpression("share_sms");
					}
				};
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				PresentViewController (m, true, ()=>{
					PopLoading("smsShare",true);
				});
			} else {

				var alert = new UIAlertView(
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_title", 		"share"), 
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_message", 		"share"),
					null,
					BlrtUtil.PlatformHelper.Translate("alert_enable_messages_cancel", 		"share")); 



				alert.Show ();
			}
		}

		public class InviteDel: Facebook.ShareKit.AppInviteDialogDelegate
		{
			#region implemented abstract members of AppInviteDialogDelegate
			public override void DidComplete (Facebook.ShareKit.AppInviteDialog appInviteDialog, NSDictionary results)
			{
				Console.WriteLine (results.ToString ());
			}
			public override void DidFail (Facebook.ShareKit.AppInviteDialog appInviteDialog, NSError error)
			{
				Console.WriteLine (error.ToString ());
			}
			#endregion

		}

		private void BigSharseBtn(object sender, string args)
		{
			var url =new NSUrl(args);
			BlrtData.Helpers.BlrtTrack.Share ("Facebook");
			var content = new Facebook.ShareKit.AppInviteContent();
			content.AppLinkURL = url;
			content.PreviewImageURL = new NSUrl ("https://s3.amazonaws.com/blrt-cc-images/share/blrt_share_fbinvite.jpg");

			var aDelegate = new InviteDel();

			Facebook.ShareKit.AppInviteDialog.Show(content,aDelegate);

		}
	}
}

