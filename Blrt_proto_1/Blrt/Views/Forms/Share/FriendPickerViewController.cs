//using System;
//using Foundation;
//using UIKit;
//using MonoTouch.FacebookConnect;
//using CoreGraphics;
//using BlrtiOS.Views.Send;
//
//namespace BlrtiOS.Views.Share
//{
//	public class FriendPickerViewController : FBFriendPickerViewController
//	{
//
//		private FriendPickerViewDelegate _friendPickerVD;
//
//		public EventHandler OnSend { get; set; }
//
//
//		public FriendPickerViewController ()
//		{
//			_friendPickerVD = new FriendPickerViewDelegate (this);
//			Title = "Select a Friend";
//			Delegate = _friendPickerVD;
//
//			AllowsMultipleSelection = true;
//			ConnectedFacebookIfNeeded ();
//		}
//
//		private async void ConnectedFacebookIfNeeded()
//		{
//			if (FBManager.IsLinked) {
//				try {
//					if (!FBManager.HasPermissions) {
//
//						await FBManager.LoginWithParse ();
//						MonoTouch.FacebookConnect.FBSession.ActiveSession.RequestNewReadPermissions (
//							BlrtData.BlrtConstants.FacebookExtendedPermissions,
//							null
//						);
//
//					}else if(!FBManager.FBConnected()){
//						var re = FBManager.OpenSessionWithAllowLoginUI(false);
//						if(!re){
//							await FBManager.LoginWithParse ();
//							MonoTouch.FacebookConnect.FBSession.ActiveSession.RequestNewReadPermissions (
//								BlrtData.BlrtConstants.FacebookExtendedPermissions,
//								null
//							);
//						}
//					}
//
//					//					this.LoadData();
//				} catch (Exception e) {
//					e.ToString ();
//				}
//			}
//		}
//
//		public override void ViewDidLoad ()
//		{
//			base.ViewDidLoad ();
//			const float searchBarHeight = 44;
//			var searchBar = new UISearchBar (new CGRect (0, 0, this.View.Bounds.Size.Width, searchBarHeight));
//			searchBar.AutoresizingMask = searchBar.AutoresizingMask | UIViewAutoresizing.FlexibleWidth;
//			searchBar.ShowsCancelButton = true;
//
//			// trigger search whenever the user change the search text
//			searchBar.TextChanged += (sender, e) => {
//				_friendPickerVD.FriendsSearchText = searchBar.Text;
//				UpdateView ();
//			};
//
//			searchBar.SearchButtonClicked += (sender, e) => {
//				_friendPickerVD.FriendsSearchText = searchBar.Text;
//				UpdateView ();
//			};
//
//			searchBar.CancelButtonClicked += (sender, e) => {
//				_friendPickerVD.FriendsSearchText = null;
//				searchBar.ResignFirstResponder ();
//			};
//
//			this.CanvasView.AddSubview (searchBar);
//			var newFrame = this.View.Bounds;
//			newFrame.Height -= searchBarHeight;
//			newFrame.Y = searchBarHeight;
//			this.TableView.Frame = newFrame;
//
//
//			var tap = new UITapGestureRecognizer (() => {
//				View.EndEditing (true);
//			});
//			tap.CancelsTouchesInView = false;
//			View.AddGestureRecognizer (tap);
//		}
//
//		public override void ViewWillAppear (bool animated)
//		{
//			base.ViewWillAppear (animated);
//
//			this.LoadData ();
//
//
//			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Share_FacebookInvite");
//
//			NavigationItem.RightBarButtonItem = null;
//		}
//
//		public async void FriendSelected (IFBGraphUser selection)
//		{
//			var toFriendId = selection.GetId ();
//
//
//			var dlgParam = new Foundation.NSMutableDictionary ();
//
//			string str = await BlrtData.BlrtUtil.GenerateYozioShareUrl("share_facebook");
//			dlgParam.Add (new Foundation.NSString (FBManager.FB_FEED_LINK), new Foundation.NSString (str));
//
//			if (!string.IsNullOrWhiteSpace(toFriendId))
//				dlgParam.Add (new Foundation.NSString (FBManager.FB_FEED_TO), new Foundation.NSString (toFriendId));
//
//			// show the FB feed dialog
//			FBWebDialogs.PresentFeedDialogModally (null, dlgParam, (feedResult, feedResultUrl, feedError) => {
//				if ((null != feedResultUrl) && (null != feedResultUrl.Query) && (feedResultUrl.Query.Contains ("post_id="))) {
//					if(OnSend != null) OnSend(this, EventArgs.Empty);
//				} else {
//				}
//			});
//
//			//FBSession.OpenActiveSession(
//		}
//
//
//		private class FriendPickerViewDelegate : FBFriendPickerDelegate
//		{
//			private FriendPickerViewController _friendPicker;
//			private string _searchText;
//
//			private int counter = 0;
//
//			public FriendPickerViewDelegate (FriendPickerViewController controller)
//			{
//				_friendPicker = controller;
//			}
//
//			public string FriendsSearchText {
//				get {
//					return _searchText;
//				}
//
//				set {
//					_searchText = value;
//				}
//			}
//
//			public override void CancelWasPressed (NSObject sender)
//			{
//			}
//
//			public override void DataDidChange (FBFriendPickerViewController friendPicker)
//			{
//			}
//
//			public override void DoneWasPressed (NSObject sender)
//			{
//			}
//
//			public override void HandleError (FBFriendPickerViewController friendPicker, NSError error)
//			{
//				//				error = error;
//			}
//
//			public override void SelectionDidChange (FBFriendPickerViewController friendPicker)
//			{
//				if ((null != friendPicker.Selection) && (friendPicker.Selection.Length > counter)) {
//					_friendPicker.FriendSelected (friendPicker.Selection[friendPicker.Selection.Length - 1]);
//				} else {
//				}
//				friendPicker.ClearSelection ();
//			}
//
//			public override bool ShouldIncludeUser (FBFriendPickerViewController friendPicker, IFBGraphUser user)
//			{
//				if ((null != _searchText) && (_searchText.Length > 0)) {
//					if (user.GetName ().IndexOf (_searchText, StringComparison.CurrentCultureIgnoreCase) > -1) {
//						return true;
//					} else {
//						return false;
//					}
//				}
//				return true;
//			}
//		}
//	}
//}
