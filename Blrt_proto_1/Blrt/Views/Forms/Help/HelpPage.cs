using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using BlrtData.Views.CustomUI;

namespace BlrtiOS.Views.Help
{
	public class HelpPage: ContentPage
	{
		static readonly int rowHeight = HelpPageCell.ImageHeight + 1;

		public EventHandler<SelectedItemChangedEventArgs> RowClicked{ get; set;}

		private ListView _listView;

		public HelpPage ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("title","help");

			_listView = new FormsListView{
				RowHeight = rowHeight,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				SeparatorInset = new Thickness(HelpPageCell.ImageWidth + 10,0,0,0),
			};

			RefreshItemSource ();

			_listView.ItemTemplate = new DataTemplate(typeof(HelpPageCell));
			_listView.ItemSelected += (sender, e) => {
				if(e.SelectedItem==null){
					return;
				}
				_listView.SelectedItem = null;
				if (RowClicked != null)
					RowClicked (this, e);
			};


			Content = _listView;
		}


		void RefreshItemSource()
		{
			_listView.ItemsSource = new HelpCellSource[] {
				new HelpCellSource(){
					Title = BlrtUtil.PlatformHelper.Translate ("knowledge_base_title","help"), 
					Text = BlrtUtil.PlatformHelper.Translate ("knowledge_base_text","help"), 
					ImageUri ="help_knowledgebase@2x.png", 
					NavText = "knowledge",
				},
				new HelpCellSource(){
					Title = BlrtUtil.PlatformHelper.Translate ("msg_blrt_title","help"), 
					Text = BlrtUtil.PlatformHelper.Translate ("msg_blrt_text","help"), 
					ImageUri ="help_messageblrt.png", 
					NavText = "message",
				},
			};
		}

		public class HelpCellSource
		{
			public string Title{ get; set; }
			public string Text{ get; set; }
			public string ImageUri{ get; set; }
			public string NavText{ get; set; }
		}
	}
}

