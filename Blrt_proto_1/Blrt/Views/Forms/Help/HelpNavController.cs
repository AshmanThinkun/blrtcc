using System;
using Xamarin.Forms;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BlrtiOS.Views.Help
{
	public class HelpNavController: BaseUINavigationController,IHelpScreen
	{
		private HelpPage _helpPage;

		public HelpNavController ():base()
		{
		}

		public override string ViewName {
			get {
				return "Help";
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_helpPage = new HelpPage ();
			_helpPage.RowClicked += (sender, e) => {
				var item = e.SelectedItem as HelpPage.HelpCellSource;
				if(item.NavText=="knowledge"){
					this.OpenKnowledge();
				}else if (item.NavText=="message"){
					this.OpenMessageSupport();
				}
			};


			var controller = _helpPage.CreateViewController ();
			controller.Title = _helpPage.Title;
			controller.NavigationItem.LeftBarButtonItem = BlrtHelperiOS.OpenDrawerMenuButton(this);

			controller.NavigationItem.RightBarButtonItem = BlrtiOS.Views.CustomUI.NewBlrtButton.CreateBarBlrt (()=>{
				Mailbox.MailboxListController.CreateNewBlrt();
			});

			PushViewController (controller, false);
		}

		#region IHelpScreen implementation

		public Task<ExecutionResult<object>> OpenMessageSupport ()
		{
			return OpenMessageSupport ((string)null);
		}

		// TODO VERY BAD!!!!! NEED REFACTORING!!!!

		SimpleBlocker _msgBlocker;
		SimpleBlocker _knwBlocker;

		public static async Task<ExecutionResult<object>> OpenMessageSupport (UIViewController parent, string additionalMsg = null)
		{
			if (null != parent) {
				BlrtUserVoice.PresentSupport (parent, additionalMsg);
				return new ExecutionResult<object> (new object ());
			} else {
				return new ExecutionResult<object> (new ArgumentNullException("parent"));
			}
		}

		public Task<ExecutionResult<object>> OpenMessageSupport (string additionalInformation)
		{
			if (null != _knwBlocker) {
				ExecutionPerformer.RegisterBlocker (_knwBlocker);
				_knwBlocker = null;
			}
			if (null != _msgBlocker) {
				ExecutionPerformer.RegisterBlocker (_msgBlocker);
				_msgBlocker = null;
			}
			_msgBlocker = new SimpleBlocker (async () => {
				if (null != this.PresentedViewController) {
					await this.PresentedViewController.DismissViewControllerAsync (true);
				}
				ExecutionPerformer.UnregisterBlocker (_msgBlocker);
				_msgBlocker = null;
				return true;
			});
			ExecutionPerformer.RegisterBlocker (_msgBlocker);
			BlrtUserVoice.PresentSupport (this, additionalInformation);
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Help_Feedback");
			return Task<Object>.Run(() => {
				return new ExecutionResult<Object> (new Object ());
			});
		}

		public Task<ExecutionResult<object>> OpenKnowledge ()
		{
			if (null != _knwBlocker) {
				ExecutionPerformer.RegisterBlocker (_knwBlocker);
				_knwBlocker = null;
			}
			if (null != _msgBlocker) {
				ExecutionPerformer.RegisterBlocker (_msgBlocker);
				_msgBlocker = null;
			}
			_knwBlocker = new SimpleBlocker (async () => {
				if (null != this.PresentedViewController) {
					await this.PresentedViewController.DismissViewControllerAsync (true);
				}
				ExecutionPerformer.UnregisterBlocker (_knwBlocker);
				_knwBlocker = null;
				return true;
			});
			ExecutionPerformer.RegisterBlocker (_knwBlocker);
			BlrtUserVoice.PresentUVInterface (this);

			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Help_KnowledgeBase");
			return Task<Object>.Run(() => {
				return new ExecutionResult<Object> (new Object ());
			});
		}

		public override void ViewWillDisappear (bool animated)
		{
			if (null != _knwBlocker) {
				ExecutionPerformer.RegisterBlocker (_knwBlocker);
				_knwBlocker = null;
			}
			if (null != _msgBlocker) {
				ExecutionPerformer.RegisterBlocker (_msgBlocker);
				_msgBlocker = null;
			}
			base.ViewWillDisappear (animated);
		}
		#endregion
	}
}

