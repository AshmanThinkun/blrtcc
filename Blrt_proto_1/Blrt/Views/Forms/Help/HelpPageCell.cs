using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;

namespace BlrtiOS.Views.Help
{
	public class HelpPageCell:ViewCell
	{
		Image _image;
		Label _titleLabel, _textLabel;

		public static readonly int ImageHeight = 86;
		public static readonly int ImageWidth = ImageHeight;
		static readonly Font TitleFont = BlrtStyles.CellTitleFont;
		static readonly Font TextFont = BlrtStyles.CellContentFont;

		public HelpPageCell () : base ()
		{
			_image = new Image () {
				HorizontalOptions = LayoutOptions.Start
			};
			_image.WidthRequest = ImageWidth;
			_image.HeightRequest = ImageHeight; 

			_titleLabel = new Label {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontFamily = TitleFont.FontFamily,
				FontAttributes = TitleFont.FontAttributes,
				FontSize = TitleFont.FontSize,
				TextColor = BlrtStyles.BlrtAccentBlue,
			};

			_textLabel = new Label {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontFamily = TextFont.FontFamily,
				FontAttributes = TextFont.FontAttributes,
				FontSize = TextFont.FontSize,
			};

			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) },
				},

				RowSpacing = 0,
				ColumnSpacing = 0,
				Padding = 0,

				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{ _image, 0,0 }, { 
						new StackLayout () {
							HorizontalOptions = LayoutOptions.Fill,
							VerticalOptions = LayoutOptions.Start,
							Orientation = StackOrientation.Vertical,
							Padding = new Thickness (10),
							Spacing = 8,
							Children = { 
								_titleLabel, 
								_textLabel 
							}
						},1,0
					}, {
						new Image () {
							Source = "help_arrow@2x.png",
							WidthRequest = 40,
							HorizontalOptions = LayoutOptions.Center,
							VerticalOptions = LayoutOptions.Center
						},2,0
					},
				}
			};

			_image.SetBinding (Image.SourceProperty, "ImageUri");
			_titleLabel.SetBinding (Label.TextProperty, "Title");
			_textLabel.SetBinding (Label.TextProperty, "Text");
		}
	}
}

