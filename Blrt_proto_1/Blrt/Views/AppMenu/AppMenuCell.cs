using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.Views.Helpers;



namespace BlrtiOS.Views.AppMenu
{
	public class AppMenuCellModal : ITableModelCell {

		string _title;
		string _image;
		int _badge;
		bool _isRemovable = false;

		public string CellIdentifer { get { return AppMenuCell.Indentifer; } }
		public string Title { get{ return _title;} set{ PropertyChange (ref _title, value);} }
		public string Image { get{ return _image;} set{ PropertyChange (ref _image, value);} }
		public int Badge { get{ return _badge;} set{ PropertyChange (ref _badge, value);} }
		public bool IsRemovable { get{ return _isRemovable;} set{ PropertyChange (ref _isRemovable, value);} }

		public ICellAction Action {
			get; set;
		}

		public ICellAction RemoveCellAction {
			get; set;
		}

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;


		void PropertyChange<T> (ref T current, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
		{
			if (current != null ? !current.Equals(value) : value != null) {
				current = value;
				if(PropertyChanged != null)
					PropertyChanged.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}


	public class AppMenuCell : UITableViewCell, IBindable<AppMenuCellModal> {

		public const string Indentifer = "AppMenuCell";
		public const int CellHeight = 48;

		BlrtiOS.Views.CustomUI.BadgeView _badgeView;
		AppMenuCellModal _modal;
		UIButton _removeBtn = null;

		public AppMenuCell (IntPtr handle) 
			: base(handle)
		{
			BackgroundColor = ContentView.BackgroundColor = AppMenuTableView.CellBackgroundColor;
			SelectionStyle = UITableViewCellSelectionStyle.None;

			ImageView.ContentMode = UIViewContentMode.Center;
			TextLabel.Font = BlrtStyles.iOS.AppMenuFont;

			_badgeView = new BlrtiOS.Views.CustomUI.BadgeView ();
			AddSubview (_badgeView);
		}


		public void Bind (AppMenuCellModal modal)
		{
		
			if (_modal != modal)
				_modal = modal;

			TextLabel.Text = _modal.Title;

			var image = UIImage.FromBundle (_modal.Image);
			if (image != null)
				ImageView.Image = image.ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate);
			else
				ImageView.Image = null;

			if (_modal.Badge > 0) {
				_badgeView.Hidden = false;
				_badgeView.SetState(_modal.Badge);
			} else {
				_badgeView.Hidden = true;
			}

			if (_modal.IsRemovable) {
				if (null == _removeBtn) {
					// IsRemovable changed from false to true
					_removeBtn = new UIButton ();
					_removeBtn.SetImage (BlrtHelperiOS.FromBundleTemplate ("cross.png"), UIControlState.Normal);
					_removeBtn.ContentMode = UIViewContentMode.Center;
					_removeBtn.TouchUpInside += RemovePressed;
					AddSubview (_removeBtn);
				}
			} else {
				if (null != _removeBtn) {
					// IsRemovable changed from true to false
					_removeBtn.TouchUpInside -= RemovePressed;
					_removeBtn.RemoveFromSuperview ();
					_removeBtn.Dispose ();
					_removeBtn = null;
				}
			}
		}

		void RemovePressed (object sender, EventArgs e)
		{
			_modal?.RemoveCellAction?.Action (this);
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			DrawSelectColor (Selected, Highlighted);
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			DrawSelectColor (Selected, Highlighted);
		}

		public void DrawSelectColor(bool selected, bool highlighted){
			if (selected || highlighted)
				TintColor = TextLabel.TextColor = BlrtStyles.iOS.Green;
			else
				TintColor = TextLabel.TextColor = BlrtStyles.iOS.Gray3;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			const int spacer = 4;

			ImageView.Frame = new CoreGraphics.CGRect (0, 0, ContentView.Frame.Height, ContentView.Frame.Height);
			TextLabel.Frame = new CoreGraphics.CGRect (ContentView.Frame.Height + spacer, 0, ContentView.Frame.Width - ContentView.Frame.Height - spacer, ContentView.Frame.Height);

			_badgeView.Center = new CoreGraphics.CGPoint (ContentView.Frame.Height * 0.85f, ContentView.Frame.Height * 0.25f);
			if (null != _removeBtn) {
				_removeBtn.Frame = new CoreGraphics.CGRect (ContentView.Frame.Width - ContentView.Frame.Height, 0, ContentView.Frame.Height, ContentView.Frame.Height);
			}
		}
	}
}

