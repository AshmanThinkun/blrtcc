using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.Views.Helpers;
using System.Collections.ObjectModel;
using BlrtData.Models.Mailbox;
using BlrtData;
using Acr.UserDialogs;
using BlrtData.Query;
using System.Threading.Tasks;

namespace BlrtiOS.Views.AppMenu
{
	public class AppMenuTableView : TableModelView<AppMenuCellModal>, BlrtData.ExecutionPipeline.IExecutor
	{
		public BlrtData.ExecutionPipeline.ExecutionSource Cause {
			get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
		}

		public static readonly UIColor SectionBackgroundColor = BlrtStyles.iOS.Black;
		public static readonly UIColor CellBackgroundColor = BlrtStyles.iOS.CharcoalShadow;

		public AppMenuCellModal InboxCell { get; private set; }
		public AppMenuCellModal ArchiveCell { get; private set; }

		public AppMenuCellModal UpgradeCell		{ get; set; }
		public AppMenuCellModal ShareCell		{ get; set; }
		public AppMenuCellModal SettingsCell	{ get; set; }
		public AppMenuCellModal HelpCell		{ get; set; }
		public AppMenuCellModal NewsCell		{ get; set; }
		public AppMenuCellModal LogoutCell		{ get; set; }
		public AppMenuCellModal ContactsCell		{ get; set; }
		public UserProfileCellModal UserProfileCell		{ get; set; }

		AppMenuController _parent;

		public AppMenuTableView (AppMenuController parent)
			: base()
		{
			_parent = parent;

			RegisterClassForCellReuse (typeof(AppMenuCell), new NSString (AppMenuCell.Indentifer));
			RegisterClassForHeaderFooterViewReuse (typeof(AppMenuHeaderCell), new NSString (AppMenuHeaderCell.Indentifer));
			RegisterClassForHeaderFooterViewReuse (typeof(AppUserProfileCell), new NSString (AppUserProfileCell.Indentifer));
			UserProfileCell = new UserProfileCellModal (){
				Action = new OpenProfile()
			};
			UserProfileCell.Add (InboxCell = new AppMenuCellModal () {
				Title = BlrtHelperiOS.Translate ("conversation_inbox", "app_menu"),
				Image = "menu_myblrts.png",
				Action = new OpenInbox ()
			});
			UserProfileCell.Add (ArchiveCell = new AppMenuCellModal () {
				Title = BlrtHelperiOS.Translate ("conversation_archive", "app_menu"),
				Image = "menu_archive.png",
				Action = new OpenArchive ()
			});


			List = new ObservableCollection<TableModelSection<AppMenuCellModal>> () {
				UserProfileCell,
				new AppMenuSectionModal(BlrtHelperiOS.Translate("other_title", "app_menu")){
					(ContactsCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("Contacts"),
						Image = "menu_contacts.png",
						Action = new OpenContacts()
					}),
					(UpgradeCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("other_upgrade", "app_menu"),
						Image = "menu_upgrade.png",
						Action = new OpenUpgrade()
					}),
					(ShareCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("other_share", "app_menu"),
						Image = "menu_share.png",
						Action = new OpenShare()
					}),
					(NewsCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("other_news", "app_menu"),
						Image = "master_menu_news.png",
						Action = new OpenNews()
					}),
					(SettingsCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("other_settings", "app_menu"),
						Image = "master_menu_settings.png",
						Action = new OpenSettings()
					}),
					(HelpCell = new AppMenuCellModal(){
						Title = BlrtHelperiOS.Translate("other_help", "app_menu"),
						Image = "master_menu_help.png",
						Action = new OpenHelp()
					})
					
				}			
			};

			this.IndicatorStyle			= UIScrollViewIndicatorStyle.White;
			this.BackgroundColor		= CellBackgroundColor;
			this.Bounces				= true;
			this.SeparatorStyle			= UITableViewCellSeparatorStyle.None;

			this.RowHeight				= AppMenuCell.CellHeight;
			this.SectionHeaderHeight	= 15;
			this.SectionFooterHeight	= 0;
			this.Bounces = false;

		//	this.TableFooterView = new UIView (new RectangleF (0, 0, 1, 1));
			UserOrganisationRelation.CurrentUpdated += UserOrganisationRelationUpdated;
			ApplyOrganisationList ();
		}

		void UserOrganisationRelationUpdated (object sender, EventArgs e)
		{
			ApplyOrganisationList ();
		}

		AppMenuSectionModal BuildCurrentOrganisationSection ()
		{
			var newSec = new AppMenuSectionModal ("Blrt.tv") {
				IsTextLabelHidden = false
			};
			foreach (var listId in UserOrganisationRelation.Current.SubscribedOCListIds) {
				var list = UserOrganisationRelation.Current.LocalStorage.DataManager.GetOCList (listId);
				if (null != list) {
					newSec.Add (new AppMenuCellModal () {
						Title = list.MenuTitle,
						Image = "menu_chaser.png",
						Action = new OpenOrganisationList(list.ObjectId),
						IsRemovable = true,
						RemoveCellAction = new RemoveBlrtList(list.ObjectId, list.MenuTitle)
					});
				}
			}
			return newSec;
		}

		void ApplyOrganisationList ()
		{
			if ((null == UserOrganisationRelation.Current) 
				|| (null == UserOrganisationRelation.Current.SubscribedOCListIds)
				|| (UserOrganisationRelation.Current.SubscribedOCListIds.Length <= 0))
			{
				// no need to show the section
				if (2 == List.Count) {
					// the organisation section is not shown
					// do nothing
				} else if (3 == List.Count) {
					// the organisation section is shown
					// remove it
					List.RemoveAt (1);
				} else {
					throw new NotImplementedException ("Error: App menu section count=" + List.Count);
				}
			}
			else
			{
				// need to show the section
				if (2 == List.Count) {
					// the organisation section is not shown
					// show it
					List.Insert (1, BuildCurrentOrganisationSection ());
				} else if (3 == List.Count) {
					// the organisation section is shown
					// refresh it
					List [1] = BuildCurrentOrganisationSection ();
				} else {
					throw new NotImplementedException ("Error: App menu section count=" + List.Count);
				}
			}
		}

		public void Update ()
		{
			UserOrganisationRelation.UpdateCurrent ();
			UpgradeCell.Title = Upgrade.UpgradeController.GetUpgradeTitle ();
			UserProfileCell.UpdateDetail ();
		}

		public override nfloat GetHeightForHeader (TableModelSection<AppMenuCellModal> header)
		{
			if (header == UserProfileCell)
				return AppUserProfileCell.CellHeight;
			else if ((header is AppMenuSectionModal) && (!(header as AppMenuSectionModal).IsTextLabelHidden))
				return 32.0f;
			else
				return base.GetHeightForHeader (header);
		}

		public class OpenProfile : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenProfile (this);
			}
		}

		public class OpenInbox : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenMailbox (this, MailboxType.Inbox);
			}
		}
		public class OpenArchive  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenMailbox (this, MailboxType.Archive);
			}
		}


		public class OpenContacts  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}
			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenContactsPage(this);
			}
		}
		public class OpenUpgrade  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}
			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenUpgradeAccount(this);
			}
		}
		public class OpenShare  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenShare(this);
			}
		}
		public class OpenSettings  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenSettings(this);
			}
		}
		public class OpenHelp  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenSupport(this);
			}
		}
		public class OpenNews  : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenNews(this);
			}
		}

		public class OpenOrganisationList: ICellAction, BlrtData.ExecutionPipeline.IExecutor{
			string _id;
			public OpenOrganisationList (string id)
			{
				_id = id;
			}
			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender){
				BlrtData.BlrtManager.App.OpenOrganisationList (this, _id);
			}
		}

		public class RemoveBlrtList: ICellAction, BlrtData.ExecutionPipeline.IExecutor{
			string _id;
			string _name;
			public RemoveBlrtList (string id, string name)
			{
				_id = id;
				_name = name;
			}
			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			public void Action (object sender) {
				DoActionAsync ().FireAndForget ();
			}

			async Task DoActionAsync () 
			{
				var result = await BlrtData.BlrtManager.App.ShowAlert (
					this,
					new SimpleAlert (string.Format ("Remove {0}?", _name), "You can follow the subscription link to add this channel to Blrt again.", "Cancel", "Remove")
				);
				if (result.Success) {
					if ((!result.Result.Cancelled) && (0 == result.Result.Result)) {
						using (UserDialogs.Instance.Loading ("Unsubscribing...")) {
							try {
								var query = BlrtUnsubscribeOCListQuery.RunQuery (_id);
								await query.WaitAsync ();
							} catch { }
						}
					}
				}
			}
		}

		public class Logout : ICellAction, BlrtData.ExecutionPipeline.IExecutor {

			public BlrtData.ExecutionPipeline.ExecutionSource Cause {
				get { return  BlrtData.ExecutionPipeline.ExecutionSource.System; }
			}

			AppMenuController _parent;
			public Logout(AppMenuController parent){
				_parent = parent;
			}

			public void Action (object sender){

				var alert = new UIAlertView (
					BlrtHelperiOS.Translate("alert_logout_title", "profile"),
					BlrtHelperiOS.Translate("alert_logout_message", "profile"),
					null,
					BlrtHelperiOS.Translate("alert_logout_cancel", "profile"),
					BlrtHelperiOS.Translate("alert_logout_okay", "profile")
				);

				alert.Clicked += (object s1, UIButtonEventArgs e) => {
					if(alert.CancelButtonIndex != e.ButtonIndex){
						BlrtData.BlrtManager.Logout(true);
					}else{
						_parent.QuietSelect(_parent.LastSelection, true);
					}
				};
				alert.Show();
			}
		}
	}
}

