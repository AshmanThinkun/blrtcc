using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtData.ExecutionPipeline;
using BlrtData.Models.Mailbox;


namespace BlrtiOS.Views.AppMenu
{
	public class AppMenuController : UIViewController
	{
		BlrtiOS.Views.AppMenu.AppMenuTableView _table;

		public CellType LastSelection;

		public AppMenuController ()
			: base()
		{
			_table = new AppMenu.AppMenuTableView (this) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			LastSelection = CellType.Inbox;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.AddSubview (_table);

			var tmp = NMath.Min (UIApplication.SharedApplication.StatusBarFrame.Height, UIApplication.SharedApplication.StatusBarFrame.Width);

			_table.Frame = new CGRect (
				0, 
				tmp, 
				View.Bounds.Width, 
				View.Bounds.Height - tmp
			);

			View.BackgroundColor = AppMenuTableView.SectionBackgroundColor;
		}

		public void SetMailbox (MailboxType type)
		{
			switch (type) {
				case MailboxType.Inbox:
					_table.QuietSelect (_table.InboxCell, true);
					LastSelection = CellType.Inbox;
					break;
				case MailboxType.Archive:
					_table.QuietSelect (_table.ArchiveCell,		true);
					LastSelection = CellType.Archive;
					break;
				case MailboxType.Any:
				default:
					if(_table.SelectedCell != _table.InboxCell
						&& _table.SelectedCell != _table.ArchiveCell) 
					{
						_table.QuietSelect (_table.InboxCell, true);
						LastSelection = CellType.Inbox;
					}
					break;
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

//			_table.ScrollTo (_table.InboxCell, UITableViewScrollPosition.Top, true);
			UIApplication.SharedApplication.SetStatusBarStyle (UIStatusBarStyle.LightContent, animated);


			_table.Update();
			_table.ReloadData ();
			QuietSelect (LastSelection, false);

		}

		public void QuietSelect(CellType type, bool animate){
			AppMenuCellModal cellmodal = null;

			switch(type){
				case CellType.Inbox:
					cellmodal = _table.InboxCell;
					break;
				case CellType.Archive:
					cellmodal = _table.ArchiveCell;
					break;
				case CellType.Upgrade:
					cellmodal = _table.UpgradeCell;
					break;
				case CellType.Share:
					cellmodal = _table.ShareCell;
					break;
				case CellType.News:
					cellmodal = _table.NewsCell;
					break;
				case CellType.Help:
					cellmodal = _table.HelpCell;
					break;
				case CellType.Settings:
					cellmodal = _table.SettingsCell;
					break;
				case CellType.Logout:
					cellmodal = _table.LogoutCell;
					break;
				default:
					break;
			}

			if(cellmodal!=null){
				_table.QuietSelect (cellmodal, animate);
			}
		}

		public void SetInboxBadge(int badge){
			if (_table != null && _table.InboxCell != null)
				_table.InboxCell.Badge = badge;
		}
	}


	public enum CellType{
		Inbox,
		Archive,
		Upgrade,
		Share,
		News,
		Help,
		Settings,
		Logout
	}
}
