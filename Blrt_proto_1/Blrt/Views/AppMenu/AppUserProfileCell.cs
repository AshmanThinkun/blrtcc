﻿using System;
using UIKit;
using BlrtData.Views.Helpers;
using CoreGraphics;
using BlrtiOS.Views.Conversation.Cells;
using BlrtData;
using Parse;

namespace BlrtiOS.Views.AppMenu
{
	public class AppUserProfileCell : UITableViewHeaderFooterView, IBindable<TableModelSection<AppMenuCellModal>>{
		public const string Indentifer = "AppUserProfileCell";

		public const int AvatarBorder = 3;
		public const int ThumbnailSize = 44;
		public const int CellHeight = 74;

		public const int RealHeight = 66;

		UserProfileCellModal content;

		UILabel _title, _detail, _unreadLbl;
		UserThumbnailView _initial;
		UIImageView _avatarBackground;

		UIView _bottomView;


		UIImage _basicImg, _premiumImg;
		public AppUserProfileCell (IntPtr ptr) 
			: base(ptr)
		{
			this.ContentView.BackgroundColor = AppMenuTableView.SectionBackgroundColor;

			_bottomView = new UIView () {
				BackgroundColor = AppMenuTableView.CellBackgroundColor,
			};

			_avatarBackground = new UIImageView(){
				Frame = new CGRect(10,3, 50, 58),
				Image = _basicImg = UIImage.FromBundle("avatar_basic.png")
			};

			_initial = new UserThumbnailView () {
				Frame = new CGRect (10+AvatarBorder, 3+AvatarBorder, ThumbnailSize, ThumbnailSize),
				BackgroundColor = UIColor.Clear
			};

			_unreadLbl = new UILabel(){
				Frame = new CGRect(_initial.Frame.Right-6, _initial.Frame.Top -5, 16,16),
				ClipsToBounds = true,
				BackgroundColor = BlrtStyles.iOS.AccentRed,
				Font = BlrtStyles.iOS.CellDetailFont,
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center,
				Hidden = true
			};
			_unreadLbl.Layer.CornerRadius = 8;

			_title = new UILabel () {
				Frame = new CGRect (_initial.Frame.Right + 15, 13, ContentView.Bounds.Width - (_initial.Frame.Right + 15), 20),
				TextColor = BlrtStyles.iOS.White,
				Font = BlrtStyles.iOS.CellTitleBoldFont,
			};

			_detail = new UILabel () {
				Frame = new CGRect (_title.Frame.Left, _title.Frame.Bottom, _title.Frame.Width, _title.Frame.Height),
				TextColor = BlrtStyles.iOS.White,
				Font = BlrtStyles.iOS.CellContentFont
			};

			this.ContentView.AddSubviews (_avatarBackground,_initial, _title, _detail, _unreadLbl);

			this.AddSubview (_bottomView);

			this.ContentView.AddGestureRecognizer(new UITapGestureRecognizer(() => {
				if(content!=null){
					content.Action.Action(this);
				}
			}));
		}

		public void Bind (TableModelSection<AppMenuCellModal> data)
		{
			content = data as UserProfileCellModal;

			if (content == null)
				return;
			this._title.Text = content.Title;
			this._initial.ImagePath = content.ThumbnaiPath;
			this._detail.Text = content.Detail;
			if(content.IsPremium && this._avatarBackground.Image!=_premiumImg){
				if (_premiumImg == null)
					_premiumImg = UIImage.FromBundle ("avatar_premium.png");
				this._avatarBackground.Image = _premiumImg;
				_detail.TextColor = BlrtStyles.iOS.Green;
			}else if(!content.IsPremium && this._avatarBackground.Image!=_basicImg){
				this._avatarBackground.Image = _basicImg;
				_detail.TextColor = BlrtStyles.iOS.White;
			}

			_unreadLbl.Text = BlrtUserHelper.UnreadUserMessageCount.ToString ();
			_unreadLbl.Hidden = BlrtUserHelper.UnreadUserMessageCount == 0;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_title.Frame = new CGRect (_initial.Frame.Right + 15, 13, ContentView.Bounds.Width - (_initial.Frame.Right + 15), 20);
			_detail.Frame = new CGRect (_title.Frame.Left, _title.Frame.Bottom, _title.Frame.Width, _title.Frame.Height);
			_bottomView.Frame = new CGRect (0, RealHeight, ContentView.Bounds.Width,CellHeight - RealHeight);
		}
	}

	public class UserProfileCellModal: TableModelSection<AppMenuCellModal>{
		public override string HeaderIdentifer { get { return AppUserProfileCell.Indentifer; } }
		public override string FooterIdentifer { get { return null; } }

		public override string Title{get; set;}
		public string Detail{get; private set;}
		public string Initials{ get; private set;}
		public string ThumbnaiPath{ get; private set;}
		public bool IsPremium{ get; private set;}

		ICellAction _action;
		public override ICellAction Action {
			get { return _action; } set { _action = value; }
		}

		public UserProfileCellModal(){
			Title = Detail = Initials = ThumbnaiPath = "";
			UpdateDetail ();
		}

		public void UpdateDetail(){
			if (ParseUser.CurrentUser == null)
				return;
			Title = BlrtUserHelper.CurrentUserName;
			Initials = BlrtUtil.GetInitials (Title);
			Detail = BlrtPermissions.AccountName;
			var localAvatar = BlrtUserHelper.GetLocalAvatar ();
			ThumbnaiPath = localAvatar == BlrtUserHelper.DefaultAvatar ? "" : localAvatar;
			if(BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free
				|| BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Unknown
				|| BlrtPermissions.AccountThreshold == null
			){
				IsPremium = false;
			}else{
				IsPremium = true;
			}
		}
	}
}

