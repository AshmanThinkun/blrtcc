using System;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData.Views.Helpers;

namespace BlrtiOS.Views.AppMenu
{
	public class AppMenuSectionModal : TableModelSection<AppMenuCellModal> {

		public override string HeaderIdentifer { get { return AppMenuHeaderCell.Indentifer; } }
		public override string FooterIdentifer { get { return null; } }
		public override string Title { get; set; }


		ICellAction _action;
		public override ICellAction Action {
			get { return _action; } set { _action = value; }
		}



//		public string IsButtonVisible { get; set; }
//		public string Button { get; set; }
//		public ICellAction ButtonAction { get; set; }

		public bool IsTextLabelHidden { get; set; } = true;

		public AppMenuSectionModal(string title) {
			Title = title;
		}
	}


	public class AppMenuHeaderCell : UITableViewHeaderFooterView, IBindable<TableModelSection<AppMenuCellModal>> {

		public const string Indentifer = "AppMenuHeaderCell";

		AppMenuSectionModal content;

		UIView _sep;

		public AppMenuHeaderCell (IntPtr ptr) 
			: base(ptr)
		{
			ContentView.BackgroundColor = AppMenuTableView.CellBackgroundColor;

			TextLabel.TextColor = BlrtStyles.iOS.Gray4;
			TextLabel.Font = BlrtStyles.iOS.AppMenuFont;
			TextLabel.Hidden = true;

			_sep = new UIView () {
				BackgroundColor = BlrtStyles.iOS.Gray6
			};
			AddSubview (_sep);
		}

		public void Bind (TableModelSection<AppMenuCellModal> data)
		{
			content = data as AppMenuSectionModal;

			if (content == null)
				return;

			TextLabel.Text = content.Title;
			TextLabel.Hidden = content.IsTextLabelHidden;
			if (!TextLabel.Hidden) {
				TextLabel.Font = BlrtStyles.iOS.AppMenuFont;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			// A bit of a hack, but the view will reset this font when it tries to display it for the first time
			_sep.Frame = new CoreGraphics.CGRect (10, 5, Bounds.Width - 20, 1);
			var oldFrame = TextLabel.Frame;
			oldFrame.X -= 5;
			oldFrame.Y += 7;
			TextLabel.Frame = oldFrame;
		}
	}
}

