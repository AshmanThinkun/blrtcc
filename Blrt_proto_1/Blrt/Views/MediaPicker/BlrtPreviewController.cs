using System;
using System.IO;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

using BlrtiOS.UI;
using BlrtiOS.Util;
using BlrtData.IO;
using BlrtData;
using BlrtData.Media;
using BlrtCanvas;
using System.Threading.Tasks;
using BlrtCanvas.Pages;
using BlrtiOS.Media;


namespace BlrtiOS.Views.MediaPicker
{
	public interface IPreviewMaster
	{
		ICanvasPage GetPage (int i);
		int PageCount { get; }
		string GAViewNamePrefix { get;}
	}

	public class BlrtPreviewController : Util.BaseUIViewController
	{
		public event EventHandler Dismiss;

		public override string ViewName { get { return _master.GAViewNamePrefix + "_AddMedia_Preview"; } }

		UIScrollView _scrollView;

		UIImageView _image;
		BlrtPdfView _pdfView;

		BlrtPreiviewScrollDelegate _delegate;


		IPreviewMaster _master;
		int _page;

		public BlrtPreviewController (IPreviewMaster master) : base ()
		{
			_master = master;

			Title = BlrtHelperiOS.Translate ("preview_title", "preview");
			HidesBottomBarWhenPushed = true;

			View.BackgroundColor = BlrtConfig.BLRT_BACKGROUND_COLOR;
		}

		UIBarButtonItem _prevBtn;
		UIBarButtonItem _nextBtn;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			EdgesForExtendedLayout = UIRectEdge.None;

			_scrollView = new UIScrollView (new CGRect (CGPoint.Empty, View.Frame.Size)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				MaximumZoomScale = 16,
				MinimumZoomScale = 1,
				Bounces = false,
				BouncesZoom = false
			};
			View.AddSubview (_scrollView);

			_image = new UIImageView (new CGRect (CGPoint.Empty, View.Frame.Size)) {
				AutoresizingMask = UIViewAutoresizing.All,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			_scrollView.AddSubview (_image);
			_pdfView = new BlrtPdfView (new CGRect (CGPoint.Empty, View.Frame.Size)) {
				AutoresizingMask = UIViewAutoresizing.All,
				ContentMode = UIViewContentMode.ScaleAspectFit,
				FitPDFToView	= true
			};
			_scrollView.AddSubview (_pdfView);


			_prevBtn = new UIBarButtonItem (BlrtHelperiOS.FromBundleTemplate ("canvas_page_prev.png"), UIBarButtonItemStyle.Plain, null);
			_nextBtn = new UIBarButtonItem (BlrtHelperiOS.FromBundleTemplate ("canvas_page_next.png"), UIBarButtonItemStyle.Plain, null);


			NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done, (object sender, EventArgs e) => {
				if (Dismiss != null)
					Dismiss (sender, e);
			});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem [] {
				_prevBtn,
				new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
					Width = 16
				},
				_nextBtn
			};

			View.AddGestureRecognizer (new UISwipeGestureRecognizer (DirectionSwipe) {
				Direction = UISwipeGestureRecognizerDirection.Left
			});
			View.AddGestureRecognizer (new UISwipeGestureRecognizer (DirectionSwipe) {
				Direction = UISwipeGestureRecognizerDirection.Right
			});



			_prevBtn.Clicked += (object sender, EventArgs e) => {
				PrevPage ();
			};
			_nextBtn.Clicked += (object sender, EventArgs e) => {
				NextPage ();
			};

			_scrollView.Delegate = _delegate = new BlrtPreiviewScrollDelegate (_scrollView);
		}

		void DirectionSwipe (UISwipeGestureRecognizer swipe)
		{
		
			switch (swipe.Direction) {
				case UISwipeGestureRecognizerDirection.Right:
					PrevPage ();
					break;
				case UISwipeGestureRecognizerDirection.Left:
					NextPage ();
					break;
			}
		
		
		}

		void PrevPage ()
		{
			SetPage (_page - 1);
		}

		void NextPage ()
		{
			SetPage (_page + 1);
		}


		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (NavigationController != null) {
				NavigationController.SetNavigationBarHidden (false, animated);
			}

			_scrollView.ContentOffset = CGPoint.Empty;
			_scrollView.SetZoomScale (1, false);
		}


		public void SetPage (int page)
		{
			if (page <= 0 || page > _master.PageCount)
				return;


			_page = page;
			SetMedia (_master.GetPage (_page));



			_prevBtn.Enabled = page > 1;
			_nextBtn.Enabled = page < _master.PageCount;
		}

		void SetMedia (ICanvasPage page)
		{
			if (!page.IsDecypted) {
				Task.WaitAll (page.DecyptData ());
			}
			switch (page.Format) {
				case BlrtMediaFormat.Image:
					var imgPage = page as BlrtImagePage;
					if (null == imgPage)
						return;
					_image.Image = UIImage.FromFile (imgPage.DecyptedPath);
					_delegate.focus = _image;

					_image.Hidden = false;
					_pdfView.Hidden = true;
					_pdfView.Document = null;
					break;
				case BlrtMediaFormat.Template:
					var tmplPage = page as BlrtTemplatePage;
					if (null == tmplPage)
						return;
					_image.Image = UIImage.FromFile (tmplPage.TemplateImagePath);
					;
					_delegate.focus = _image;

					_image.Hidden = false;
					_pdfView.Hidden = true;
					_pdfView.Document = null;
					break;
				case BlrtMediaFormat.PDF:
					var pdfPage = page as BlrtPDFPage;
					if (null == pdfPage)
						return;
					var pdf = BlrtPDF.FromFile (pdfPage.DecyptedPath);
					_pdfView.Document = pdf.Document;
					_pdfView.Page = page.MediaPage;
					_delegate.focus = _pdfView;
					_image.Image = null;

					_image.Hidden = true;
					_pdfView.Hidden = false;
					break;
				default:
					break;
			}

			_scrollView.ContentOffset = CGPoint.Empty;
			_scrollView.SetZoomScale (1, false);
			_scrollView.SetNeedsDisplay ();

			ContentChanged ();
		}

		public virtual void ContentChanged ()
		{
		}


		private class BlrtPreiviewScrollDelegate : UIScrollViewDelegate
		{
			public UIView focus;

			public BlrtPreiviewScrollDelegate (UIView focus) : base ()
			{
				this.focus = focus;
			}

			public override void WillEndDragging (UIScrollView scrollView, CGPoint velocity, ref CGPoint targetContentOffset)
			{
				targetContentOffset = scrollView.ContentOffset;
			}

			public override UIView ViewForZoomingInScrollView (UIScrollView scrollView)
			{
				return focus;
			}
		}

	}
}

