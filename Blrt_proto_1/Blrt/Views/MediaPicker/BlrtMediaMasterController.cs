using System;
using System.Collections.Generic;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;
using UIKit;
using BlrtData;
using System.IO;
using System.Threading;
using BlrtCanvas;
using BlrtData.ExecutionPipeline;
using BlrtShared;
using BlrtCanvas.Pages;
using BlrtiOS.Util;

namespace BlrtiOS.Views.MediaPicker
{
	using HiddenParameterFor = BlrtMediaPageListController.PagePermissions.HiddenParameterFor;
	public partial class BlrtMediaMasterController : Util.BaseUIViewController, IBlrtAddMediaController, IPreviewMaster,
	IExecutionBlocker
	{
		public const string GA_EventCategory		= "AddMedia";
		public const string GA_EventAddMedia		= "AddPage";
		public const string GA_EventAddPageImage	= GA_EventAddMedia + "_Image";
		public const string GA_EventAddPagePDF		= GA_EventAddMedia + "_PDF";
		public const string GA_EventAddPageTemplate	= GA_EventAddMedia + "_Template";

		#region implemented abstract members of BaseUIViewController
		public override string ViewName {
			get { return GAViewNamePrefix; }
		}
		#endregion

		public bool ForReBlrt {
			get {
				return _pageController.ForReBlrt;
			}
			set {
				_pageController.ForReBlrt = value;

				if (_playerArgs.IsDraft && !_playerArgs.IsReply) {
					_pageController.Title = BlrtHelperiOS.Translate ("media_view_blrt_title", "media");
					_done.Title = BlrtHelperiOS.Translate ("media_view_blrt_done", "media");
				} else {
					_pageController.Title = BlrtHelperiOS.Translate ("media_view_reblrt_title", "media");
					_done.Title = BlrtHelperiOS.Translate ("media_view_reblrt_done", "media");
				}
				_pageController.NavigationItem.RightBarButtonItem = _done;
			}
		}

		public const float HeightOfSelectTypeTablet = 560;

		public string GAViewNamePrefix {
			get {
				if (_playerArgs != null) {
					if (_playerArgs.IsDraft && !_playerArgs.IsReply)
						return "NewBlrt_AddMedia";
					else
						return "BlrtReply_AddMedia";
				}
				return "NewBlrt_AddMedia";
			}
		}

		private BlrtSplitAddMediaController _ipadController;
		private BlrtAddMediaTypeSelectController _iphoneController;

		private BlrtMediaPageListController _pageController;
		private UINavigationController _navigationController;

		UIView _coverPageSelect;

		UIBarButtonItem _done;

		BlrtLoaderCoverView _loading;

		IBlrtCanvasData _playerArgs;
		public IConversationScreen ConversationScreen { get; private set;}

		bool _showSelect;

		//string RequestedMediaDecryptedPath { get; set;}
		//BlrtMediaFormat RequestedMediaFormat { get; set; }

		TaskCompletionSource<MediaPickerResult> _tsc;

		IEnumerable<MediaAsset> BlrtTheseMediaAssets { get; set; }

		public BlrtMediaMasterController (IConversationScreen conversationScreen, IEnumerable <MediaAsset>  blrtTheseMediaAssets)
		{
			BlrtTheseMediaAssets = blrtTheseMediaAssets;
			ConversationScreen = conversationScreen;
			//RequestedMediaDecryptedPath = requestedMediaDecryptedPath;
			//RequestedMediaFormat = requestedMediaFormat;

			this.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;

			_tsc = new TaskCompletionSource<MediaPickerResult> ();

			_pageController = new BlrtMediaPageListController (this);
			_pageController.PageRemoved += SafeEventHandler.FromTaskFunc (SetLocalCanvasDataAsync);
			_navigationController = new UINavigationController (_pageController);

			_done = new UIBarButtonItem (
				"Create", UIBarButtonItemStyle.Done, SafeEventHandler.PreventMulti (DonePressed) 
			);

			_showSelect = false;

			EdgesForExtendedLayout = UIRectEdge.None;
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_ipadController);
			_ipadController = null;
			BlrtUtil.BetterDispose (_iphoneController);
			_iphoneController = null;
			BlrtUtil.BetterDispose (_pageController);
			_pageController = null;
			BlrtUtil.BetterDispose (_navigationController);
			_navigationController = null;
			BlrtUtil.BetterDispose (_coverPageSelect);
			_coverPageSelect = null;
			BlrtUtil.BetterDispose (_done);
			_done = null;
			BlrtUtil.BetterDispose (_loading);
			_loading = null;
			BlrtUtil.BetterDispose (_tsc);
			_tsc = null;
		}

		public async Task SetBlrtThisAssets (bool hidden)
		{
			if (BlrtTheseMediaAssets == null) 
			{
				return;
			}

			InvokeOnMainThread (() => {
				PushThinking (true);
			});

			List<ICanvasPage> imagePageList = new List<ICanvasPage>();

			foreach (var media in BlrtTheseMediaAssets) {
				if (media.Format == BlrtMediaFormat.Image) {
					imagePageList.Add (new BlrtImagePage (media));
				} else if (media.Format == BlrtMediaFormat.PDF) {
					imagePageList.AddRange (BlrtUtil.GetPDFCanvasPages (media));
				}
			}

			//if (RequestedAsset == BlrtMediaFormat.Image) {

			//	var image = UIImage.FromFile (RequestedMediaDecryptedPath);

			//	if (image == null) {
			//		InvokeOnMainThread (() => {
			//			PopThinking (true);
			//		});
			//		return;
			//	}

			//	imagePageList = new List<ICanvasPage> (1);
			//	bool oversized = false;

			//	if (!oversized && (NMath.Max (image.Size.Width, image.Size.Height) > BlrtPermissions.MaxImageResolution)) {
			//		oversized = true;
			//	}
			//	imagePageList.Add (new BlrtImagePage (RequestedMediaDecryptedPath));

			//	if (oversized) {
			//		await BlrtHelperiOS.AwaitAlert (
			//			BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxImageResolution)
			//		);
			//	}
			//}

			//if (RequestedMediaFormat == BlrtMediaFormat.PDF) {
				//imagePageList = BlrtUtilities.GetPDFCanvasPages (RequestedMediaDecryptedPath);
			//}

			if (imagePageList != null && imagePageList.Count > 0) {
				InvokeOnMainThread (() => {
					AddMedia (imagePageList, hidden).FireAndForget ();
					DismissAddMediaController (true);
				});
			}

			InvokeOnMainThread (() => {
				PopThinking (true);
			});


		}

		void SetResult (MediaPickerResult result)
		{
			_tsc?.TrySetResult (result);
		}

		public void ActivateWaiting ()
		{
			_tsc = new TaskCompletionSource<MediaPickerResult> ();
		}

		public void DeactivateWaiting ()
		{
			_tsc = null;
		}

		public Task<MediaPickerResult> WaitResult ()
		{
			return _tsc?.Task;
		}

		#region IExecutionBlocker implementation

		TaskCompletionSource<bool> _tcsAbandonThisBlrtConfirm = null;
		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;
		bool _confirmDlgShown = false;
		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsAbandonThisBlrtConfirm = new TaskCompletionSource<bool> ();
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			if (!_confirmDlgShown) {
				CancelPressed (this, EventArgs.Empty);
			}
			bool abandonThisBlrt = await _tcsAbandonThisBlrtConfirm.Task;
			_tcsAbandonThisBlrtConfirm = null;
			if (abandonThisBlrt) {
				await _tcsLeaveThisScreen.Task;
				return true;
			} else {
				_tcsLeaveThisScreen.SetCanceled ();
				_tcsLeaveThisScreen = null;
				return false;
			}
		}

		#endregion
		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				// enter navigation stack
				ExecutionPerformer.RegisterBlocker (this);
			}
			base.ViewWillAppear (animated);
		}

		public async override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			//if (_pageController.GetTotalPageCount () == 0) {
				await ExcutePlayerArgs ();
			//}

			LoadingContext.Get ("toMediaPicker").Count = 0;

			if (BlrtHelperiOS.IsPhone) {
				_pageController.ViewPresent ();
			} else if (!_pageController.HiddenCalled) {
				_pageController.ViewPresent ();
			}
		}

		public override void ViewWillDisappear (bool animated)
		{
			_pageController.ViewHidden ();
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}


		public override void LoadView ()
		{
			base.LoadView ();

			AddChildViewController (_navigationController);
			_navigationController.View.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			_navigationController.View.Frame = new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height);
			View.AddSubview (_navigationController.View);

			_pageController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (
				UIBarButtonSystemItem.Cancel,
				SafeEventHandler.FromAction (CancelPressed)
			);

			if (BlrtHelperiOS.IsPhone) {
				_iphoneController = new BlrtAddMediaTypeSelectController (this);
			} else {
				_ipadController = new BlrtSplitAddMediaController (this);

				_ipadController.View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleTopMargin;
				_ipadController.View.Frame = new CGRect (0, View.Bounds.Height, View.Bounds.Width, 0);
			}

			_loading = new BlrtLoaderCoverView (View.Bounds);
			_loading.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			View.AddSubview (_loading);

			_coverPageSelect = new UIView (_navigationController.View.Bounds) {
				Hidden = true,
				BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0.5f),
				Alpha = 0
			};
			_navigationController.View.AddSubview (_coverPageSelect);

			_coverPageSelect.AddGestureRecognizer (
				new UITapGestureRecognizer ((UITapGestureRecognizer rec) => {
					DismissAddMediaController (true);
				}) {
				});

			BlrtRecentFileManager.RefreshFileInfo ();

			this.View.TintColor = BlrtConfig.BlrtBlack;
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			return BlrtHelperiOS.IsPhone ? UIInterfaceOrientationMask.Portrait : UIInterfaceOrientationMask.All;
		}

		async Task DonePressed (object sender, EventArgs e)
		{
			var pages = _pageController.GetUsedPages ();

			if (pages.Length > 0) {
				var mpp = await _pageController.GetMediaPagePrototype (true);

				if (_playerArgs == null || !_playerArgs.IsReply) {
					string convoName = "";
					if (_playerArgs != null) {
						convoName = _playerArgs.ConversationName;
					}
					if (string.IsNullOrWhiteSpace (convoName)) {
						convoName = string.Format ("Blrt {0}", DateTime.Now.ToString ("dd MMMMM yyyy"));
					}

					_playerArgs = BlrtCanvasData.FromDraft (convoName, mpp, _playerArgs != null ? _playerArgs.RequestArgs : null);
					SetResult (MediaPickerResult.Success (_playerArgs));
				} else {
					_playerArgs = _playerArgs.CreateDraft (mpp, _playerArgs != null ? _playerArgs.RequestArgs : null);
					SetResult (MediaPickerResult.Success (_playerArgs));
				}
			} else {

				if (ForReBlrt) {
					new UIAlertView (
						BlrtHelperiOS.Translate ("no_media_selected_reblrt_title", "media"),
						BlrtHelperiOS.Translate ("no_media_selected_reblrt_message", "media"),
						null,
						BlrtHelperiOS.Translate ("no_media_selected_reblrt_accept", "media")
					).Show ();
				} else {
					new UIAlertView (
						BlrtHelperiOS.Translate ("no_media_selected_blrt_title", "media"),
						BlrtHelperiOS.Translate ("no_media_selected_blrt_message", "media"),
						null,
						BlrtHelperiOS.Translate ("no_media_selected_blrt_accept", "media")
					).Show ();
				}
			}
		}


		void CancelPressed (object sender, EventArgs e)
		{
			if (_pageController.HasDeletableContent ()) {
				_confirmDlgShown = true;

				string prefix = "alert_cancel_new";

				if (ForReBlrt)
					prefix = "alert_cancel_reblrt";

				UIAlertView alert = new UIAlertView (
					BlrtHelperiOS.Translate (prefix + "_title", "media"),
					BlrtHelperiOS.Translate (prefix + "_message", "media"),
					null,
					BlrtHelperiOS.Translate (prefix + "_cancel", "media"),
					BlrtHelperiOS.Translate (prefix + "_confirm", "media")
				);

				alert.Clicked += SafeEventHandler.FromAction (
					(object s1, UIButtonEventArgs args) => {
						if (null != _tcsAbandonThisBlrtConfirm) {
							_tcsAbandonThisBlrtConfirm.SetResult (args.ButtonIndex != alert.CancelButtonIndex);
						}
						if (args.ButtonIndex != alert.CancelButtonIndex) {
							SetResult (MediaPickerResult.Cancel ());
						}
						_confirmDlgShown = false;
					}
				);
				alert.Show ();

			} else {
				if (null != _tcsAbandonThisBlrtConfirm) {
					_tcsAbandonThisBlrtConfirm.SetResult (true);
				}
				SetResult (MediaPickerResult.Cancel ());
			}
		}

		public void ResetViewController (bool animated)
		{
			DismissAddMediaController (animated);
			_playerArgs = null;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
		}

		public override bool ShouldAutomaticallyForwardRotationMethods {
			get {
				var a = base.ShouldAutomaticallyForwardRotationMethods;
				return true;
			}
		}

		public void ShowAddMediaController (bool animated)
		{
			if (ForReBlrt && !BlrtPermissions.CanMediaAddOnReblrt) {
				BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.CanMediaAddOnReblrt);
				return;
			}

			if (BlrtPermissions.MaxConvoSizeKb * 1024.0f <= _pageController.GetConvoSizeByte ()) {
				BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxConvoSizeKB);
				return;
			}
			if (BlrtPermissions.MaxBlrtSizeKb * 1024.0f <= _pageController.GetBlrtSizeByte ()) {
				BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtSizeKB);
				return;
			}
			if (BlrtPermissions.MaxBlrtPageCount <= _pageController.GetTotalShowingPageCount ()) {
				BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
				return;
			}

			View.Hidden = false;
			if (BlrtHelperiOS.IsPhone) {
				if (_navigationController.VisibleViewController == _pageController) {
					_navigationController.PushViewController (_iphoneController, animated);
				}
			} else {
				PositioningLogic (true, animated);
			}

			_pageController.ViewHidden ();
		}


		public void DismissAddMediaController (bool animated)
		{
			View.Hidden = false;
			if (BlrtHelperiOS.IsPhone) {
				if (_navigationController.VisibleViewController != _pageController) {
					_navigationController.PopToRootViewController (animated);
					TrackPageView ();
				}
			} else {
				if (_ipadController.ParentViewController != null) {
					TrackPageView ();
				}
				PositioningLogic (false, animated);
			}
			_pageController.ViewPresent ();
		}

		public void ShowAddMediaTypeController (BlrtAddMediaType type, bool animated)
		{
			View.Hidden = false;
			if (BlrtHelperiOS.IsPhone) {

				ShowAddMediaController (false);

				if (_navigationController.VisibleViewController != _iphoneController)
					_navigationController.PopToViewController (_iphoneController, false);

				_navigationController.PushViewController (BlrtSplitAddMediaController.CreateController (type, this), animated);

			} else {
				ShowAddMediaController (false);
				_ipadController.ShowAddMediaTypeController (type, animated);
			}
		}


		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}

		void PositioningLogic (bool showSelect, bool animate)
		{
			if (_showSelect == showSelect)
				animate = false;

			_showSelect = showSelect;

			if (!BlrtHelperiOS.IsPhone) {
				if (_showSelect && _ipadController.ParentViewController == null) {
					AddChildViewController (_ipadController);
					View.AddSubview (_ipadController.View);
				}

				if (animate) {
					_coverPageSelect.Hidden = false;
					UIView.Animate (0.4,
						() => {
							PostionChildViews ();
						}, () => {
							_coverPageSelect.Hidden = !_showSelect;

							if (!_showSelect) {
								if (_ipadController.ParentViewController != null) {
									_ipadController.RemoveFromParentViewController ();
									_ipadController.View.RemoveFromSuperview ();
								}
							}
						}
					);
				} else {
					PostionChildViews ();
					_coverPageSelect.Hidden = !_showSelect;

					if (!_showSelect && _ipadController.ParentViewController != null) {
						_ipadController.RemoveFromParentViewController ();
						_ipadController.View.RemoveFromSuperview ();
					}
				}
			}
		}

		void PostionChildViews ()
		{
			if (_showSelect) {
				_navigationController.View.Frame = new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height - HeightOfSelectTypeTablet - 1);
				_ipadController.View.Frame = new CGRect (0, View.Bounds.Height - HeightOfSelectTypeTablet, View.Bounds.Width, HeightOfSelectTypeTablet);
				_coverPageSelect.Alpha = 1;
			} else {
				_navigationController.View.Frame = new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height);
				_ipadController.View.Frame = new CGRect (0, View.Bounds.Height, View.Bounds.Width, 0);
				_coverPageSelect.Alpha = 0;
			}
		}

		public Task AddMedia (ICanvasPage media)
		{
			return AddMedia (media, false);
		}

		public Task AddMedia (ICanvasPage media, bool hidden)
		{
			return AddMedia (new ICanvasPage [] { media }, hidden);
		}

		public Task AddMedia (IEnumerable<ICanvasPage> media)
		{
			return AddMedia (media, false);
		}

		public async Task AddMedia (IEnumerable<ICanvasPage> media, bool hidden)
		{
			View.Hidden = false;
			await _pageController.AddPages (media,
				new BlrtMediaPageListController.PagePermissions (hidden, true), 
			    false
			);
			await SetLocalCanvasDataAsync ();
		}

		public static BlrtCanvasData LocalCanvasData {
			get {
				try {
					var filepath = BlrtConstants.Directories.Default.GetDataPath ("local_canvas_data.dat");
					if (File.Exists (filepath)) {
						using (var reader = File.OpenRead (filepath)) {
							var result = BlrtUtil.DeserializeFromStream<BlrtCanvasData> (reader);
							reader.Close ();
							return result;
						}
					}
					return null;
				} catch {
					return null;
				}
			}

			set {
				try {
					var filepath = BlrtConstants.Directories.Default.GetDataPath ("local_canvas_data.dat");
					if (value == null) {
						System.IO.File.Delete (filepath);
						return;
					}
					var stream = BlrtUtil.SerializeToStream (value);
					BlrtUtil.StreamToFile (filepath, stream);
				} catch {
					var t = 0;
				}
			}
		}

		async Task SetLocalCanvasDataAsync ()
		{
			var pages = _pageController.GetUsedPages ();

			if (pages.Length > 0) {
				var mpp = await _pageController.GetMediaPagePrototype (false);

				if (_playerArgs == null || !_playerArgs.IsReply) {
					LocalCanvasData = BlrtCanvasData.FromDraft (_playerArgs.IsReply ? _playerArgs.ConversationName : "", mpp, _playerArgs != null ? _playerArgs.RequestArgs : null);
				} else {
					LocalCanvasData = _playerArgs.CreateDraft (mpp, _playerArgs != null ? _playerArgs.RequestArgs : null) as BlrtCanvasData;
				}
			} else {
				LocalCanvasData = null;
			}
		}

		public void SetData (IBlrtCanvasData playerArgs,
		                     bool lastBlrtMediaHidden,
		                     HashSet<string> mediaIdsToSetHiddenParamFor)
		{
			_playerArgs = playerArgs;
			if (_playerArgs != null) {
				ForReBlrt = _playerArgs.IsReply;
				if (playerArgs.PageCount > 0 || playerArgs.HiddenPageCount > 0) {
					AddMedia (playerArgs, lastBlrtMediaHidden, mediaIdsToSetHiddenParamFor);
				}
			} else {
				ForReBlrt = false;
			}

			try {
				// for local canvas data
				var localData = LocalCanvasData;
				if (localData == null || localData.PageCount < 1) //empty local data
					return;

				if (_playerArgs == null || (!ForReBlrt && _playerArgs.PageCount < 1 && _playerArgs.HiddenPageCount < 1)) { //this is a new blrt with no media
					if (_playerArgs.RequestArgs != null) {
						var requestArgs = _playerArgs.RequestArgs;
						if (requestArgs.DownloadUrls != null && requestArgs.DownloadUrls.Length > 0) {
							return;
						} else if (!string.IsNullOrEmpty (requestArgs.CaptureUrl)) {
							return;
						}
					}

					if (!localData.IsReply) {
						AddMedia (localData, false);
					}
				}
			} catch {}
		}

		public void AddMedia (IBlrtCanvasData playerArgs, 
		                      bool mediaHidden,
		                      HashSet<string> mediaIdsToSetHiddenParamFor = null)
		{

			var pages = new List<ICanvasPage> ();
			var hiddenPages = new List<ICanvasPage> ();
			bool canhide = BlrtPermissions.CanMediaShowHide;

			for (int i = 0; i < playerArgs.PageCount; ++i) {
				pages.Add (playerArgs.GetPage (i + 1));
			}
			for (int i = 0; i < playerArgs.HiddenPageCount; ++i) {
				hiddenPages.Add (playerArgs.GetHiddenPage (i + 1));
			}

			_pageController.AddPages (pages,
			                          new BlrtMediaPageListController.PagePermissions (mediaHidden, !playerArgs.IsReply, mediaIdsToSetHiddenParamFor), true
             ).FireAndForget ();


			_pageController.AddPages (hiddenPages,
				new BlrtMediaPageListController.PagePermissions (canhide, !playerArgs.IsReply), true
             ).FireAndForget ();
		}

		public bool CheckInformation (BlrtMediaFormat format, long size)
		{
			return CheckInformationAlert (format, size) == null;
		}

		public int MediaCountLeft (BlrtMediaFormat format)
		{
			switch (format) {
			case BlrtMediaFormat.Image:
			case BlrtMediaFormat.PDF:
				if (BlrtPermissions.MaxConvoSizeKb * 1024.0f <= _pageController.GetConvoSizeByte ()) {
					return 0;
				}
				if (BlrtPermissions.MaxBlrtSizeKb * 1024.0f <= _pageController.GetBlrtSizeByte ()) {
					return 0;
				}
				return BlrtPermissions.MaxBlrtPageCount - _pageController.GetTotalShowingPageCount ();
			}
			return -1;
		}

		public UIAlertView CheckInformationAlert (BlrtMediaFormat format, long size)
		{
			if (BlrtPermissions.MaxConvoSizeKb * 1024.0f <= _pageController.GetConvoSizeByte ()) {
				return BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxConvoSizeKB);
			}
			if (BlrtPermissions.MaxBlrtSizeKb * 1024.0f <= _pageController.GetBlrtSizeByte ()) {
				return BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtSizeKB);
			}
			switch (format) {
				case BlrtMediaFormat.Image:
					if (BlrtPermissions.MaxBlrtPageCount <= _pageController.GetTotalShowingPageCount ()) {
						return BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
					}
					break;
				case BlrtMediaFormat.PDF:
					if (BlrtPermissions.MaxBlrtPageCount <= _pageController.GetTotalShowingPageCount ()) {
						return BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
					}
					if (size > 1024 * BlrtPermissions.MaxPDFSizeKB) {
						return BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxPDFSizeKB, size);
					}
					break;
			}
			return null;
		}

		BlrtPreviewController _mediaPreviewer;
		UINavigationController _mediaPreviewerNavigation;

		public void PreviewPage (int page)
		{
			if (_mediaPreviewer == null) {
				_mediaPreviewer = new BlrtPreviewController (this);
				_mediaPreviewer.Dismiss += SafeEventHandler.FromAction (
					(object sender, EventArgs args) => {
						DismissViewController (true, null);
					}
				);
				_mediaPreviewerNavigation = new UINavigationController (_mediaPreviewer);
			}

			_mediaPreviewer.SetPage (page);
			PresentViewController (_mediaPreviewerNavigation, true, null);
		}


		async Task ExcutePlayerArgs ()
		{
			if (_playerArgs == null) {
				ShowAddMediaController (true);
				return;
			}

			PushThinking (true);

			try { 
				if (_playerArgs.RequestArgs != null && _playerArgs.RequestArgs.DownloadUrls != null && _playerArgs.RequestArgs.DownloadUrls.Length > 0) {
					await BlrtAddMediaTypes.OpenMediaFromUrl (MediaSource.OpenIn, _playerArgs.RequestArgs.DownloadUrls, View, async (mediaUrlManager) => {
						try {
							var media = await mediaUrlManager.PopMedia ();
							await AddMedia (media);

						} catch (Exception e) { }
					});
					_playerArgs.RequestArgs.DownloadUrls = new string [0];
				}
			} catch {}

			if (_playerArgs.RequestArgs != null && !string.IsNullOrWhiteSpace (_playerArgs.RequestArgs.CaptureUrl)) {
				OpenWebCapture (_playerArgs.RequestArgs.CaptureUrl);
				_playerArgs.RequestArgs.CaptureUrl = "";
				BlrtHelperiOS.GenericAlertAsync (
					"capture_request_alert",
					"media"
				).FireAndForget ();
			} else {
				if (_pageController.GetTotalPageCount () == 0) {
					ShowAddMediaController (true);
				}
			}

			PopThinking (false);
		}

		public void OpenWebCapture (string captureWebsite)
		{
			ShowAddMediaTypeController (BlrtAddMediaType.WebPage, false);

			if (BlrtHelperiOS.IsPhone) {
				var v = _navigationController.VisibleViewController as BlrtAddMediaTypeController;

				if (v != null) {
					v.OpenWebPage (captureWebsite);
				}
			} else {
				_ipadController.OpenWebPage (captureWebsite);
			}
		}

		public async Task OpenMediaFromUrl (MediaSource source, string [] url, UIView view, BlrtAddMediaTypes.MediaPickedCallback mediaPickedCallback)
		{ 
			await BlrtAddMediaTypes.OpenMediaFromUrl (source, url, view, mediaPickedCallback);
		}

		public class MediaPickerResult
		{
			public static MediaPickerResult Cancel ()
			{
				return new MediaPickerResult ();
			}

			public static MediaPickerResult Success (IBlrtCanvasData data)
			{
				return new MediaPickerResult () {
					Data = data
				};
			}

			public bool Successful { get { return Data != null; } }
			public IBlrtCanvasData Data { get; private set; }
		}


		#region IPreviewMaster implementation
		public ICanvasPage GetPage (int page)
		{
			return _pageController.GetPage (page);
		}

		public int PageCount {
			get {
				return _pageController.GetTotalPageCount ();
			}
		}

		public string ConversationId {
			get {
				return _playerArgs.Conversation;
			}
		}
		#endregion
	}
}
