using System;
using UIKit;
using AssetsLibrary;
using Vernacular;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using System.Threading.Tasks;
using BlrtiOS.Views.MediaPicker;
using System.Linq;
using BlrtData;
using BlrtiOS;

namespace ELCPicker
{
	public class AssetResult
	{
		public UIImageOrientation Orientation{ get; set;}
		public ALAssetRepresentation Representation{get;set;}
	}


	/** 
     * Presents a photo picker dialog capable of selecting multiple images at once.
     * Usage:
     * 
     * var picker = ELCImagePickerViewController.Instance;
     * picker.MaximumImagesCount = 15;
     * picker.Completion.ContinueWith (t => {
     *   if (t.IsCancelled || t.Exception != null) {
     *     // no pictures for you!
     *   } else {
     *      // t.Result is a List<AssetResult>
     *    }
     * });
     * 
     * PresentViewController (picker, true, null);
     */
	public class ImagePickerViewController : UINavigationController
	{
		public int MaximumImagesCount { get; set; }
		public float ImageThumbnailSize { get; set;}
		public float AlbumThumbnailSize { get; set; }
		public bool CanTakeCameraShot { get; set; }

		readonly TaskCompletionSource<object> _TaskCompletionSource = new TaskCompletionSource<object> ();

		public Task<object> Completion {
			get {
				return _TaskCompletionSource.Task;
			}
		}

		public static ImagePickerViewController Instance {
			get {
				var albumPicker = new ImageAlbumPicker ();
				var picker = new ImagePickerViewController (albumPicker);
				picker.MaximumImagesCount = int.MaxValue;
				picker.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;
				picker.CanTakeCameraShot = true;

			    var width = Math.Min (BlrtUtil.PlatformHelper.GetDeviceWidth (), BlrtUtil.PlatformHelper.GetDeviceHeight ());
				if (BlrtHelperiOS.IsPhone) {
					picker.ImageThumbnailSize = (width - 3.0f) / 4.0f;
					picker.AlbumThumbnailSize = (width - ImageAlbumPicker.CELL_SPACING * 3) / 2.0f;
				} else {
					picker.ImageThumbnailSize = (width - 7.0f) / 8.0f;
					picker.AlbumThumbnailSize = (width - ImageAlbumPicker.CELL_SPACING * 3) / 2.0f;
				}

				albumPicker.Parent = picker;
				return picker;
			}
		}

		ImagePickerViewController (UIViewController rootController) : base (rootController)
		{
			View.BackgroundColor = BlrtStyles.iOS.White;
			NavigationBar.BackgroundColor = BlrtStyles.iOS.Gray4;
		}

		void SelectedAssets (List<ALAsset> assets)
		{
			var results = new List<AssetResult> (assets.Count);
			foreach (var asset in assets) { 
				var obj = asset.AssetType;
				if (obj == default (ALAssetType))
					continue;

				var rep = asset.DefaultRepresentation;
				if (rep != null) {
					var result = new AssetResult ();
					var orientation = ImagePicker.ALOrientationToUI (rep.Orientation);
					result.Representation = rep;
					result.Orientation = orientation;
					results.Add (result);
				}
			}

			_TaskCompletionSource.TrySetResult (results);
		}

		void SelectedCameraImage (Tuple<UIImage, string> imgData)
		{
			_TaskCompletionSource.TrySetResult (imgData);
		}

		void CancelledPicker ()
		{
			_TaskCompletionSource.TrySetCanceled ();
		}

		bool ShouldSelectAsset (ALAsset asset, int previousCount)
		{
			return MaximumImagesCount <= 0 || previousCount < MaximumImagesCount;
		}

		public class ImageAlbumPicker : UICollectionViewController
		{
			static readonly NSObject _Dispatcher = new NSObject ();
			public static readonly float CELL_SPACING = 20f;

			readonly List<ALAssetsGroup> MediaSources = new List<ALAssetsGroup> ();
			// Group of albums

			ALAssetsLibrary Library;

			WeakReference _Parent;

			static UICollectionViewFlowLayout CollectionViewLayout {
				get {
					var MinimumLineSpacing = 80f;
					return new UICollectionViewFlowLayout () {
						MinimumInteritemSpacing = CELL_SPACING, // Space between cells
						MinimumLineSpacing = MinimumLineSpacing,
						SectionInset = new UIEdgeInsets (CELL_SPACING, CELL_SPACING, MinimumLineSpacing, CELL_SPACING)
					};
				}
			}

			public ImagePickerViewController Parent {
				get {
					return _Parent == null ? null : _Parent.Target as ImagePickerViewController;
				}
				set {
					_Parent = new WeakReference (value);
					(Layout as UICollectionViewFlowLayout).ItemSize = new CGSize (Parent.AlbumThumbnailSize, Parent.AlbumThumbnailSize);
				}
			}

			UIColor BackgroundColor {
				get { return BlrtStyles.iOS.White; }
			}

			public ImageAlbumPicker () : base (CollectionViewLayout)
			{ }

			public override void ViewDidLoad ()
			{
				base.ViewDidLoad ();

				CollectionView.BackgroundColor = BackgroundColor;
				CollectionView.AllowsMultipleSelection = false;
				CollectionView.RegisterClassForCell (typeof (AlbumCell), AlbumCell.AlbumCellId);

				NavigationItem.Title = "Loading...";
				var cancelButton = new UIBarButtonItem (UIBarButtonSystemItem.Cancel);
				cancelButton.Clicked += CancelClicked;
				NavigationItem.RightBarButtonItem = cancelButton;

				MediaSources.Clear ();

				Library = new ALAssetsLibrary ();
				Library.Enumerate (ALAssetsGroupType.All, GroupsEnumerator, GroupsEnumeratorFailed); // Enumerate through albums and display them
			}

			public override void ViewDidDisappear (bool animated)
			{
				base.ViewDidDisappear (animated);
				if (IsMovingFromParentViewController || IsBeingDismissed) {
					NavigationItem.RightBarButtonItem.Clicked -= CancelClicked;
				}
			}

			public override nint NumberOfSections (UICollectionView collectionView)
			{
				return 1;
			}

			public override nint GetItemsCount (UICollectionView collectionView, nint section)
			{
				return MediaSources.Count;
			}

			public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
			{
				var albumCell = (AlbumCell)collectionView.DequeueReusableCell (AlbumCell.AlbumCellId, indexPath);

				var mediaSrc = MediaSources [indexPath.Row];
				if (indexPath.Row == 0 && Parent.CanTakeCameraShot) {
					albumCell.AlbumName = new NSString ("Take a photo");
					albumCell.ImageCount = " ";

					try {
						albumCell.Image = UIImage.FromBundle ("camera_album_thumbnail.png");
					} catch (Exception e) {
						Console.WriteLine ("ELCImagePicker, GetCell-SetThumbnail, Exception: " + e.ToString ());
					}

				} else {
					// Get count
					mediaSrc.SetAssetsFilter (ALAssetsFilter.AllPhotos);
					albumCell.AlbumName = mediaSrc.Name;
					albumCell.ImageCount = mediaSrc.Count.ToString ();

					try {
						UIImage image = UIImage.FromImage (mediaSrc.PosterImage);
						image = Resize (image, new CGSize (78, 78));
						albumCell.Image = image;
					} catch (Exception e) {
						Console.WriteLine ("ELCImagePicker, GetCell-SetThumbnail, Exception: " + e.ToString ());
					}
				}

				return albumCell;
			}

			void CancelClicked (object sender = null, EventArgs e = null)
			{
				var parent = Parent;
				if (parent != null) {
					parent.CancelledPicker ();
				}
			}

			void GroupsEnumeratorFailed (Foundation.NSError error)
			{
				Console.WriteLine ("Enumerator failed!");
			}

			/**
				 * Enumerates albums
				*/
			void GroupsEnumerator (ALAssetsGroup agroup, ref bool stop)
			{
				if (agroup == null) {
					if (Parent.CanTakeCameraShot) {
					MediaSources.Insert (0, new object() as ALAssetsGroup);
					}
					return;
				}

				// Put camera rolla as a first album in the list of albums
				if (agroup.Name.ToString ().ToLower () == "camera roll" && agroup.Type == ALAssetsGroupType.SavedPhotos) {
					MediaSources.Insert (0, agroup);
				} else {
					MediaSources.Add (agroup);
				}

				_Dispatcher.BeginInvokeOnMainThread (ReloadCollectionView);
			}

			void ReloadCollectionView ()
			{
				CollectionView.ReloadData ();
				NavigationItem.Title = "Select an Album";
			}

			/**
				 * Resize image to be displayed
				*/
			float SCALE_FACTOR = 0.0f;

			UIImage Resize (UIImage image, CGSize newSize)
			{
				UIGraphics.BeginImageContextWithOptions (newSize, false, SCALE_FACTOR);
				image.Draw (new CGRect (0, 0, newSize.Width, newSize.Height));
				UIImage newImage = UIGraphics.GetImageFromCurrentImageContext ();
				UIGraphics.EndImageContext ();
				return newImage;
			}

			public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
			{
				var mediaSrc = MediaSources [indexPath.Row];
				if (indexPath.Row == 0 && Parent.CanTakeCameraShot) {
					try {

						var _camera = CameraController.Camera;
						_camera.ImagePickerControllerDelegate = new CameraControllerDelegate (_camera);

						UIViewController v = this;
						while (v.ParentViewController != null)
							v = v.ParentViewController;

						UIView.AnimationsEnabled = false;
						v.PresentViewController (_camera, false, null);
						UIView.AnimationsEnabled = true;

						(_camera.ImagePickerControllerDelegate as CameraControllerDelegate).Completion.ContinueWith ((arg) => {
							if (arg.IsCanceled || arg.IsFaulted) {
								_camera.PopViewController (true);
								_camera.Dispose();
								_camera = null;
							} else {
								Parent.SelectedCameraImage (arg.Result);
							}
						});

					} catch {

					}

				} else {
					var picker = new ImagePicker (mediaSrc);
					picker.Parent = Parent;
					NavigationController.PushViewController (picker, true);
				}
			}

			class AlbumCell : UICollectionViewCell
			{
				public static readonly NSString AlbumCellId = new NSString ("AlbumCell");

				nfloat CellCornerRadius = 3f;
				UIImageView imageView;
				UILabel albumName;
				UILabel imageCount;

				UIColor AlbumNameTextColor {
				get { return BlrtStyles.iOS.Gray7; }
				}

				UIColor ImageCountTextColor {
					get { return BlrtStyles.iOS.Gray4; }
				}

				public AlbumCell ()
				{
				}

				[Export ("initWithFrame:")]
				public AlbumCell (CGRect frame) : base (frame)
				{
					ContentView.ContentMode = UIViewContentMode.ScaleAspectFit; // Main aspect ratio
					imageView = new UIImageView (UIImage.FromBundle ("blrt_tpd.png"));
					imageView.AutoresizingMask = UIViewAutoresizing.All;
					imageView.ClipsToBounds = true; // So that cell is contained inside completely

					albumName = new UILabel () {
						Lines = 1,
						TextColor = AlbumNameTextColor
					};

					imageCount = new UILabel () {
						Lines = 1,
						TextColor = ImageCountTextColor
					};

					Layer.CornerRadius = ContentView.Layer.CornerRadius = imageView.Layer.CornerRadius = CellCornerRadius;
					ContentView.AddSubviews (new UIView [] {
						imageView,
						albumName,
						imageCount
					});
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					var imgCountLabelSize = imageCount.SizeThatFits (ContentView.Frame.Size);

					var infoSize = new CGSize (ContentView.Frame.Width,
											  albumName.SizeThatFits (ContentView.Frame.Size).Height + imgCountLabelSize.Height);

					imageView.Layer.Frame = new CGRect (ContentView.Frame.Left,
														ContentView.Frame.Top,
														ContentView.Frame.Width,
														ContentView.Frame.Height);

					albumName.Frame = new CGRect (ContentView.Frame.Left, ContentView.Frame.Bottom, infoSize.Width, infoSize.Height);

					imageCount.Frame = new CGRect (albumName.Frame.Left, albumName.Frame.Bottom, albumName.Frame.Width, imgCountLabelSize.Height);
				}

				public UIImage Image {
					set {
						imageView.Image = value;
					}
					get {
						return imageView.Image;
					}
				}

				public string AlbumName {
					set {
						albumName.Text = value;
					}
					get {
						return albumName.Text;
					}
				}

				public string ImageCount {
					set {
						imageCount.Text = value;
					}
					get {
						return imageCount.Text;
					}
				}

				public override void PrepareForReuse ()
				{
					base.PrepareForReuse ();

					Image = null;
					ImageCount = AlbumName = " ";
				}
			}
		}
		
			/**
		 * Represents table that lists all the images in the album user selected on the last screen
		 */
		public class ImagePicker : UITableViewController
		{
			int Columns = 4;

			public bool SingleSelection { get; set; }

			public bool ImmediateReturn { get; set; }

			bool PreviewerOnDisplay { get; set;}
			readonly ALAssetsGroup AssetGroup;

			List<ELCAsset> ElcAssets = new List<ELCAsset> ();
			// List of images

			WeakReference _Parent;
			UILabel TotalImagesSelectedView;
			UILabel MaxImageAlertView;
			public int TotalSelectedAssets;
			readonly int TEXT_VIEW_HEIGHT = ImagePreviewController.TOP_BAR_HEIGHT;

			public ImagePickerViewController Parent {
				get {
					return _Parent == null ? null : _Parent.Target as ImagePickerViewController;
				}
				set {
					_Parent = new WeakReference (value);
				}
			}

			public ImagePicker (ALAssetsGroup assetGroup)
			{
				AssetGroup = assetGroup;
			}

			UIBarButtonItem doneButtonItem;
			
			public override void ViewDidLoad ()
			{
				TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
				TableView.ContentInset = new UIEdgeInsets (0,0,TEXT_VIEW_HEIGHT,0);
				TableView.AllowsSelection = false;

				PreviewerOnDisplay = false;
				if (ImmediateReturn) {

				} else {
					doneButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done);
					doneButtonItem.Clicked += DoneClicked;
					NavigationItem.RightBarButtonItem = doneButtonItem;
					NavigationItem.Title = AssetGroup.Name;
				}

				AddSubviews ();
				Task.Run ((Action)PreparePhotos); // Prepare photos to be displayed
			}

			public override void ViewWillAppear (bool animated)
			{
				base.ViewWillAppear (animated);

				doneButtonItem.Clicked += DoneClicked;
				Columns = GetNumberOfColumns ();
				TotalImagesSelectedView.Hidden = false;
				TotalImagesSelectedView.Hidden = false;
			}

			public override void ViewWillLayoutSubviews ()
			{
				base.ViewWillLayoutSubviews ();

				var TableHeight = Parent.View.Bounds.Bottom;
				TableView.Frame = new CGRect (0, 0, View.Bounds.Width, TableHeight);
				SetInfoBarsFrames ();
			}

			void SetInfoBarsFrames () 
			{
				var NavBarHeight = NavigationController.NavigationBar.Frame.Size.Height;
				var StatusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Size.Height;

				TotalImagesSelectedView.Frame = new CGRect (0,
															TableView.Frame.Bottom - TEXT_VIEW_HEIGHT,
															View.Bounds.Width,
															TEXT_VIEW_HEIGHT);

				MaxImageAlertView.Frame = new CGRect (0,
													  StatusBarHeight +
													  NavBarHeight +
													  (PreviewerOnDisplay ? ImagePreviewController.TOP_BAR_HEIGHT : 0),
													  View.Bounds.Width,
													  TEXT_VIEW_HEIGHT);
			}

			void AddSubviews ()
			{
				TotalImagesSelectedView = new UILabel () {
						TextAlignment = UITextAlignment.Center,
						Font = BlrtStyles.iOS.CellTitleBoldFont,
						BackgroundColor = BlrtStyles.iOS.Gray3,
						Alpha = 0
					};

				MaxImageAlertView = new UILabel () {
					TextAlignment = UITextAlignment.Center,
					Font = BlrtStyles.iOS.CellTitleBoldFont,
					Text = string.Format("Maximum {0} items please", Parent.MaximumImagesCount),
					TextColor = BlrtStyles.iOS.Green,
					BackgroundColor = BlrtStyles.iOS.Charcoal,
					Alpha = 0
				};

				Parent.View.AddSubviews (new UIView () { TotalImagesSelectedView, MaxImageAlertView });
			}

			void DisplayNumberOfSelectedImages ()
			{
				if (TotalSelectedAssets <= 1) {
					UIView.Animate (ANIMATION_DURATION, delegate {
						TotalImagesSelectedView.Text = string.Format ("{0} items selected", TotalSelectedAssets);
						TotalImagesSelectedView.Alpha = TotalSelectedAssets > 0 ? 0.90f : 0.0f;
					});
				} else {
					TotalImagesSelectedView.Text = string.Format ("{0} items selected", TotalSelectedAssets);
				}
			}

			int GetNumberOfColumns ()
			{
				return (int)((View.Bounds.Size.Width) / (Parent.ImageThumbnailSize));
			}

			public override void ViewDidDisappear (bool animated)
			{
				base.ViewDidDisappear (animated);
				if (IsMovingFromParentViewController || IsBeingDismissed) {
					NavigationItem.RightBarButtonItem.Clicked -= DoneClicked;
				}
			}

			public override void ViewWillDisappear (bool animated)
			{
				base.ViewWillDisappear (animated);

				if (IsMovingFromParentViewController) {
					TotalImagesSelectedView.RemoveFromSuperview();
					TotalImagesSelectedView.Dispose ();
					MaxImageAlertView.RemoveFromSuperview();
					MaxImageAlertView.Dispose ();
				}
			}

			public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
			{
				base.DidRotate (fromInterfaceOrientation);
				Columns = GetNumberOfColumns ();
				TableView.ReloadData ();
			}

			void PreparePhotos ()
			{
				AssetGroup.Enumerate (PhotoEnumerator);

				BeginInvokeOnMainThread (() => {
					TableView.ReloadData ();
					// scroll to bottom
					nint section = NumberOfSections (TableView) - 1;
					nint row = TableView.NumberOfRowsInSection (section) - 1;
					if (section >= 0 && row >= 0) {
						var ip = NSIndexPath.FromRowSection (row, section);
						TableView.ScrollToRow (ip, UITableViewScrollPosition.Bottom, false);
					}
				});
			}

			/**
				 * Enumerates through eah image in an album and ignores images without default default representation
				*/
			void PhotoEnumerator (ALAsset result, nint index, ref bool stop)
			{
				if (result == null) {
					return;
				}

				ELCAsset elcAsset = new ELCAsset (this, result, (int)index);

				if (result.DefaultRepresentation != null) {
					ElcAssets.Add (elcAsset);
				}
			}

			void DoneClicked (object sender = null, EventArgs e = null)
			{
				var selected = new List<ALAsset> ();

				foreach (var asset in ElcAssets) {
					if (asset.Selected) {
						selected.Add (asset.Asset);
					}
				}

				var parent = Parent;
				if (parent != null) {
					parent.SelectedAssets (selected);
				}
			}

			/**
			 * Decides whether user can select the image the user just tapped on
			*/
			bool ShouldSelectAsset (ELCAsset asset)
			{
				int selectionCount = TotalSelectedAssets;
				bool shouldSelect = true;

				var parent = Parent;
				if (parent != null) {
					shouldSelect = parent.ShouldSelectAsset (asset.Asset, selectionCount);
				}

				if (!shouldSelect) {
					DisplayAlertView ();
				}
				return shouldSelect;
			}

			const int ALERT_DELAY_TIME = 5000;
			async void DisplayAlertView ()
			{
				DisplayAlertView (0.9f); //Make visible	
				await Task.Delay (ALERT_DELAY_TIME);
				DisplayAlertView (0); //Make invisible
			}

			readonly float ANIMATION_DURATION = 0.3f;

			void DisplayAlertView (float alpha)
			{
				UIView.Animate (ANIMATION_DURATION, delegate {
					MaxImageAlertView.Alpha = alpha;
				});
			}

			void AssetSelected (ELCAsset asset, bool selected)
			{
				TotalSelectedAssets += (selected) ? 1 : -1;
				DisplayNumberOfSelectedImages ();
				View.SetNeedsDisplay ();

				if (SingleSelection) {
					foreach (var elcAsset in ElcAssets) {
						if (asset != elcAsset) {
							elcAsset.Selected = false;
						}
					}
				}
				if (ImmediateReturn) {
					var parent = Parent;
					var obj = new List<ALAsset> (1);
					obj.Add (asset.Asset);
					parent.SelectedAssets (obj);
				}
			}

			public override nint NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				if (Columns <= 0)
					return 4;
				int numRows = (int)Math.Ceiling ((float)ElcAssets.Count / Columns);
				return numRows;
			}

			List<ELCAsset> AssetsForIndexPath (NSIndexPath path)
			{
				int index = path.Row * Columns;
				int length = Math.Min (Columns, ElcAssets.Count - index);
				return ElcAssets.GetRange (index, length);
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				const string cellIdentifier = "Cell";

				var cell = TableView.DequeueReusableCell (cellIdentifier) as ImageCell;
				if (cell == null) {
					cell = new ImageCell (UITableViewCellStyle.Default, cellIdentifier);
				}
				cell.SetAssets (AssetsForIndexPath (indexPath), Columns, Parent.ImageThumbnailSize);
				cell.OnPreviewTapped -= OpenPreview;
				cell.OnPreviewTapped += OpenPreview;
				return cell;
			}

			ImagePreviewController _previewer;
			bool _openingPreview;
	
			void OpenPreview (object sender, int e)
			{
				if (_openingPreview) //some very large images will take a bit of time to load, we don't want this function to be called during this period of time
					return;
			
				_openingPreview = true;

				var cell = sender as ImageCell;

			if (_previewer == null) {
					_previewer = new ImagePreviewController (ElcAssets, cell.RowAssets.ElementAt (e).Index + 1); // index is zero based
					_previewer.DoneButtonItem.Clicked += DoneClicked;
					_previewer.PreviewWillRotate += delegate { 
						var NavBarHeight = NavigationController.NavigationBar.Frame.Size.Height;
						var StatusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Size.Height;

						TotalImagesSelectedView.Frame = new CGRect (0,
						                                            _previewer.View.Frame.Bottom - TEXT_VIEW_HEIGHT,
																_previewer.View.Bounds.Width,
																TEXT_VIEW_HEIGHT);

						MaxImageAlertView.Frame = new CGRect (0,
															  StatusBarHeight +
															  NavBarHeight +
															  (PreviewerOnDisplay ? ImagePreviewController.TOP_BAR_HEIGHT : 0),
															  _previewer.View.Bounds.Width,
															  TEXT_VIEW_HEIGHT);
					};
					_previewer.PreviewClosed += delegate {
						PreviewerOnDisplay = false;
						TableView.ReloadData(); 
					};
				}
				else {
					_previewer.SetImage (cell.RowAssets.ElementAt (e).Index + 1);
				}
				
				_previewer.RemainingImageCount = Parent.MaximumImagesCount - TotalSelectedAssets;
				NavigationController.PushViewController (_previewer, true);

				PreviewerOnDisplay = true;
				_openingPreview = false;
			}

			public static UIImageOrientation ALOrientationToUI (ALAssetOrientation alOrientation)
			{
				UIImageOrientation orientation = UIImageOrientation.Up;
				switch (alOrientation) {
				case ALAssetOrientation.Up:
					orientation = UIImageOrientation.Up;
					break;
				case ALAssetOrientation.UpMirrored:
					orientation = UIImageOrientation.UpMirrored;
					break;
				case ALAssetOrientation.Down:
					orientation = UIImageOrientation.Down;
					break;
				case ALAssetOrientation.DownMirrored:
					orientation = UIImageOrientation.DownMirrored;
					break;
				case ALAssetOrientation.Left:
					orientation = UIImageOrientation.Left;
					break;
				case ALAssetOrientation.LeftMirrored:
					orientation = UIImageOrientation.LeftMirrored;
					break;
				case ALAssetOrientation.Right:
					orientation = UIImageOrientation.Right;
					break;
				case ALAssetOrientation.RightMirrored:
					orientation = UIImageOrientation.RightMirrored;
					break;
				default:
					break;
				}
				return orientation;
			}

			float VERTICAL_PADDING = 2;

			public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				return (Parent.ImageThumbnailSize + VERTICAL_PADDING);
			}

			/**
				 * Represents image state such as selection state.
				*/
			public class ELCAsset
			{
				public readonly ALAsset Asset;
				readonly WeakReference _Parent;
				public int Index { get; private set; }	

				bool _Selected;

				public ELCAsset (ImagePicker parent, ALAsset asset, int index)
				{
					_Parent = new WeakReference (parent);
					Asset = asset;
					Index = index;
				}

				public void ToggleSelected ()
				{
					Selected = !Selected;
				}

				public bool Selected {
					get {
						return _Selected;
					}

					set {
						var parent = _Parent.Target as ImagePicker;
						if (value && parent != null && !parent.ShouldSelectAsset (this)) {
							return;
						}

						_Selected = value;

						if (parent != null) {
							parent.AssetSelected (this, value);
						}
					}
				}
			}

			class ImageCell : UITableViewCell
			{
				public List<ImagePicker.ELCAsset> RowAssets;
				int Columns;
				float ThumbnailSize;
				readonly List<UIImageView> ImageViewArray = new List<UIImageView> ();
				readonly List<UIImageView> OverlayViewArray = new List<UIImageView> ();
				readonly List<UIImageView> PreviewViewArray = new List<UIImageView> ();

				readonly float START_X_FOR_SUBVIEWS = 0;
				readonly float START_Y_FOR_SUBVIEWS = 4;
				readonly float IMAGE_FRAME_HORIZONTAL_PADDING = 1;
				readonly float IMAGE_FRAME_VERTICAL_PADDING = 1;

				public event EventHandler<int> OnPreviewTapped;

				UIImage _selectImg, _previewImage;

				public ImageCell (UITableViewCellStyle style, string reuseIdentifier) : base (style, reuseIdentifier)
				{
					UITapGestureRecognizer tapRecognizer = new UITapGestureRecognizer (CellTapped);
					AddGestureRecognizer (tapRecognizer);

					_selectImg = new UIImage ("img_select_lg.png");
					_previewImage = new UIImage ("preview_image.png");
				}

				/**
					 * Sets an image, preview icon and "selected" ison for each image
					 */
				public void SetAssets (List<ImagePicker.ELCAsset> assets, int columns, float thumbnailSize)
				{
					RowAssets = assets;
					Columns = columns;
					ThumbnailSize = thumbnailSize;

					foreach (var view in ImageViewArray) {
						view.RemoveFromSuperview ();
					}
					foreach (var view in OverlayViewArray) {
						view.RemoveFromSuperview ();
					}

					for (int i = 0; i < RowAssets.Count; i++) {
						var asset = RowAssets [i];

						UIImageView imageView = null;
						if (i < ImageViewArray.Count) {
							imageView = ImageViewArray [i];
							imageView.Image = new UIImage (asset.Asset.Thumbnail);
						} else {
							imageView = new UIImageView (new UIImage (asset.Asset.Thumbnail));
							ImageViewArray.Add (imageView);
						}

						UIImageView overlayView = null;
						if (i < OverlayViewArray.Count) {
							overlayView = OverlayViewArray [i];
							overlayView.Image = _selectImg;
							overlayView.Alpha = 1; // OPACITY SET TO 100%
						} else {
							overlayView = new UIImageView (_selectImg);
							OverlayViewArray.Add (overlayView);
						}

						UIImageView previewView = null;
						if (i < PreviewViewArray.Count) {
							previewView = PreviewViewArray [i];
							previewView.Image = _previewImage;
						} else {
							previewView = new UIImageView ();
							previewView.Image = _previewImage;
							previewView.ContentMode = UIViewContentMode.TopRight;
							PreviewViewArray.Add (previewView);
						}

						AddSubviews (new UIView () { imageView, overlayView, previewView });


					}
				}

				float ANIMATION_DURATION = 0.2f;

				/**
					 * Checks whether image needs to be selected or previewed, and excutes appropriate action
					 */
				void CellTapped (UITapGestureRecognizer tapRecognizer)
				{
					CGPoint point = tapRecognizer.LocationInView (this);

					// Locate point in frame of every image to identify whether user wants to preview or select
					var frame = new CGRect (START_X_FOR_SUBVIEWS, START_Y_FOR_SUBVIEWS, ThumbnailSize, ThumbnailSize);
					for (int i = 0; i < RowAssets.Count; ++i) {
						var previewFrame = PreviewViewArray [i].Frame;
						if (previewFrame.Contains (point)) {
							if (OnPreviewTapped != null)
								OnPreviewTapped (this, i);
							break;
						} else if (frame.Contains (point)) {
							RowAssets [i].Selected = !RowAssets [i].Selected;
							UIView.Animate (ANIMATION_DURATION, () => {
								OverlayViewArray [i].Alpha = RowAssets.Count > i && RowAssets [i].Selected ? 1 : 0;
							});
							break;
						}
						var x = frame.X + frame.Width + IMAGE_FRAME_HORIZONTAL_PADDING;
						frame = new CGRect (x, frame.Y, frame.Width, frame.Height);
					}
				}

				/**
					 * Display image, magnifying glass and checked sign
					*/
				public override void LayoutSubviews ()
				{
					//image is 28,this is the tapping area
					var previewWidth = Math.Max (21, ThumbnailSize * 0.35f);

					var frame = new CGRect (START_X_FOR_SUBVIEWS, START_Y_FOR_SUBVIEWS, ThumbnailSize, ThumbnailSize);

					int i = 0;
					foreach (var imageView in ImageViewArray) {
						imageView.Frame = frame;


						var overlayView = OverlayViewArray [i];
						overlayView.Center = new CGPoint (frame.X + frame.Width * 0.5f, frame.Y + frame.Height * 0.5f);
						overlayView.Alpha = RowAssets.Count > i && RowAssets [i].Selected ? 1 : 0;

						var previewView = PreviewViewArray [i];
						previewView.Frame = new CGRect (frame.X + frame.Width - previewWidth - IMAGE_FRAME_HORIZONTAL_PADDING,
							frame.Y + IMAGE_FRAME_VERTICAL_PADDING, previewWidth, previewWidth);
						previewView.Alpha = RowAssets.Count > i ? 1 : 0;

						i++;
						var x = frame.X + frame.Width + IMAGE_FRAME_HORIZONTAL_PADDING;
						frame = new CGRect (x, frame.Y, frame.Width, frame.Height);
					}
				}
			}
		}
	}
}