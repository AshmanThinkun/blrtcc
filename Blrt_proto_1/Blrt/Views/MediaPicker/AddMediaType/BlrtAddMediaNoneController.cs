using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;
using UIKit;
using Foundation;
using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public abstract class  BlrtAddMediaNoneController : BlrtAddMediaTypeController
	{
		UIView 		_headerView;
		UIImageView _imageView;
		UILabel 	_titleLabel;
		UILabel 	_textLabel;

		bool _dirty = true;

		public BlrtAddMediaNoneController (IntPtr handler)
			: base (handler)
		{}

		public BlrtAddMediaNoneController (IBlrtAddMediaController parent)
			: base (parent)
		{}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_headerView);
			_headerView = null;
			BlrtUtil.BetterDispose (_imageView);
			_imageView = null;
			BlrtUtil.BetterDispose (_titleLabel);
			_titleLabel = null;
			BlrtUtil.BetterDispose (_textLabel);
			_textLabel = null;
		}

		public override void LoadView ()
		{
			base.LoadView ();

			_headerView = new UIView () {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				Frame = new CGRect (0, 0, 100, 100)
			};
			_imageView = new UIImageView ();
			_titleLabel = new UILabel () {
				Lines = 0
			};
			_textLabel = new UILabel () {
				Lines = 0
			};

			_headerView.AddSubview (_imageView);
			_headerView.AddSubview (_titleLabel);
			_headerView.AddSubview (_textLabel);

			_headerView.AddSubview (new UIView (new CGRect (0, _headerView.Frame.Height - 1, _headerView.Frame.Width, 1)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth,
				BackgroundColor = BlrtConfig.BLRT_MID_GRAY
			});
		}

		public override void OptionSelected (BlrtAddMediaTypeOption option, UIView target)
		{
			var o = option as ImageMediaSourceOption;
			_openingDropbox = false;
			switch (o.source) {
				case MediaSource.ImageGallery:
					OpenGalleryAsync ().FireAndForget ();
					break;
				case MediaSource.DocumentPicker:
					BlrtAddMediaTypes.OpenDocumentPicker (target, this, CreateCanvasPages, 
				                                          new string [] {
															MobileCoreServices.UTType.PDF,
															MobileCoreServices.UTType.Image
														});
					return;
				case MediaSource.iCloudDrive:
					BlrtAddMediaTypes.OpeniCloudDrive (this, CreateCanvasPages);
					return;
				case MediaSource.Camera:
				OpenCamera ().FireAndForget ();
					break;
				case MediaSource.Url:
					OpenUrlDownload ();
					break;
				case MediaSource.PasteImage:
					OpenPasteImageAsync ().FireAndForget ();
					break;
				case MediaSource.DropBox:
					OpenDropbox ();
					break;
				case MediaSource.OpenIn:
					OpenOpenIn ();
					break;
				case MediaSource.PasteUrl:
					OpenPasteUrl ();
					break;
				case MediaSource.Webpage:
					OpenWebPage ();
					break;
				case MediaSource.ConversationImage:
					OpenUnusedConversationImagePicker ().FireAndForget();
					break;
				case MediaSource.ConversationPDF:
                    OpenMediaPicker (BlrtMediaFormat.PDF).FireAndForget();
					break;
			}


			LLogger.WriteEvent ("Media Source", "openned", null, o.source.ToString ());
		}

		public void SetHeader (UIImage image, string title, string text)
		{
			var titleAttr = new NSMutableAttributedString (
				title,
				new UIStringAttributes () {
					Font = BlrtConfig.BoldFont.Large,
					ForegroundColor = BlrtConfig.BlrtBlack
				}
			);
			titleAttr = BlrtHelperiOS.AttributeString (
				titleAttr, 
				"#",
				new UIStringAttributes () {
					ForegroundColor = BlrtConfig.BLRT_BLUE
				}
			);

			var textAttr = new NSMutableAttributedString (
				text,
				new UIStringAttributes () {
					Font = BlrtConfig.NormalFont.Normal,
					ForegroundColor = BlrtConfig.BLRT_DARK_TEXT
				}
			);
			SetHeader (image, titleAttr, textAttr);
		}

		public void SetHeader (UIImage image, NSMutableAttributedString title, NSMutableAttributedString text)
		{
			_imageView.Image = image;
			_titleLabel.AttributedText = title;
			_textLabel.AttributedText = text;

			View.SetNeedsLayout ();

			HeaderView = _headerView;

			_dirty = true;
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			if (HeaderView == _headerView && _dirty) {

				nfloat imgWidth = _imageView.Image != null ? _imageView.Image.Size.Width : 0;
				nfloat imgHeight = _imageView.Image != null ? _imageView.Image.Size.Height : 0;
				nfloat headerWidth = CollectionView.Bounds.Width - BlrtAddMediaTypeController.Padding * 2;

				const float VerticalMargin = 4;
				const float HorizontalMargin = 28;
				nfloat height = 0;

				if (BlrtHelperiOS.IsPhone) {
					_imageView.Frame = new CGRect ((int)(headerWidth / 2 - imgWidth / 2), 0, imgWidth, imgHeight);
					if (imgHeight != 0)
						height += imgHeight + VerticalMargin;

					var s = _titleLabel.SizeThatFits (new CGSize (headerWidth, nfloat.MaxValue));
					_titleLabel.Frame = new CGRect (0, height, headerWidth, s.Height);
					if (_titleLabel.Frame.Height != 0)
						height += _titleLabel.Frame.Height + VerticalMargin;

					s = _textLabel.SizeThatFits (new CGSize (headerWidth, nfloat.MaxValue));
					_textLabel.Frame = new CGRect (0, height, headerWidth, s.Height);
					if (_textLabel.Frame.Height != 0)
						height += _textLabel.Frame.Height + VerticalMargin;

					height += 8;

				} else {

					_imageView.Frame = new CGRect (0, 0, imgWidth, imgHeight);

					nfloat width = headerWidth - imgWidth - HorizontalMargin;

					var s = _titleLabel.SizeThatFits (new CGSize (width, nfloat.MaxValue));
					_titleLabel.Frame = new CGRect (imgWidth + HorizontalMargin, height - 4, width, s.Height);
					if (_titleLabel.Frame.Height != 0)
						height = _titleLabel.Frame.Bottom + VerticalMargin;

					s = _textLabel.SizeThatFits (new CGSize (width, nfloat.MaxValue));
					_textLabel.Frame = new CGRect (imgWidth + HorizontalMargin, height, width, s.Height);
					if (_textLabel.Frame.Height != 0)
						height = _textLabel.Frame.Bottom + VerticalMargin;

					height = NMath.Max (height, imgHeight);
				}

				if (height != _headerView.Frame.Height) {
					_headerView.Frame = new CGRect (0, 0, _headerView.Frame.Width, height);
					HeaderView = _headerView;
				} else {
					_dirty = false;
				}
			}
		}

		public class ImageMediaSourceOption : BlrtAddMediaTypeOption
		{

			public MediaSource source;

			public ImageMediaSourceOption (MediaSource source)
			{
				this.source = source;

				switch (source) {

					case MediaSource.ImageGallery:
						Title = BlrtHelperiOS.Translate ("source_image_gallery", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-photogallery.png");
						break;

					case MediaSource.DocumentPicker:
						Title = BlrtHelperiOS.Translate ("source_document_picker", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-document.png");
						break;

					case MediaSource.iCloudDrive:
						Title = BlrtHelperiOS.Translate ("source_icloud_drive", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-iCloud.png");
						break;

					case MediaSource.Camera:
						Title = BlrtHelperiOS.Translate ("source_camera", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-camera.png");
						break;

					case MediaSource.Url:
						Title = BlrtHelperiOS.Translate ("source_url", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-URL.png");
						break;

					case MediaSource.PasteImage:
						Title = BlrtHelperiOS.Translate ("source_paste_image", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-paste.png");
						break;

					case MediaSource.PasteUrl:
						Title = BlrtHelperiOS.Translate ("source_paste_url", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-paste.png");
						break;

					case MediaSource.DropBox:
						Title = BlrtHelperiOS.Translate ("source_dropbox", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-dropbox.png");
						break;		

					case MediaSource.OpenIn:
						Title = BlrtHelperiOS.Translate ("source_openin", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-email.png");
						break;		

					case MediaSource.Webpage:
						Title = BlrtHelperiOS.Translate ("source_webpage", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediasource-web.png");
						break;
					case MediaSource.ConversationImage:
						Title = BlrtHelperiOS.Translate ("source_conversation_images", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediapicker_convoimg.png");
						break;
					case MediaSource.ConversationPDF:
						Title = BlrtHelperiOS.Translate ("source_conversation_pdfs", "media");
						Image = UIImage.FromBundle ("mediapicker/sources/mediapicker_convopdf.png");
						break;
				}
			}
		}
	}
}

