using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public class  BlrtAddMediaImageController : BlrtAddMediaNoneController
	{
		public override string GAName {	get { return "Image"; } }

		public BlrtAddMediaImageController (IntPtr handler) : base (handler)
		{
		}

		public BlrtAddMediaImageController (IBlrtAddMediaController parent) : base (parent)
		{
			Title = BlrtHelperiOS.Translate ("select_type_image", "media");
		}


		public override void LoadView ()
		{
			base.LoadView ();


			if (BlrtHelperiOS.IsIOS8Above) {
				Options = new BlrtAddMediaTypeOption[] {
					new ImageMediaSourceOption (MediaSource.ImageGallery),
					new ImageMediaSourceOption (MediaSource.Camera),
					new ImageMediaSourceOption (MediaSource.DocumentPicker),
					new ImageMediaSourceOption (MediaSource.iCloudDrive),
					new ImageMediaSourceOption (MediaSource.DropBox),
					new ImageMediaSourceOption (MediaSource.Url),
					new ImageMediaSourceOption (MediaSource.PasteImage),
					new ImageMediaSourceOption (MediaSource.ConversationImage)
				};															    	
			} else {																
				Options = new BlrtAddMediaTypeOption[] {							
					new ImageMediaSourceOption (MediaSource.ImageGallery),
					new ImageMediaSourceOption (MediaSource.Camera),
					new ImageMediaSourceOption (MediaSource.DropBox),
					new ImageMediaSourceOption (MediaSource.Url),
					new ImageMediaSourceOption (MediaSource.PasteImage),
					new ImageMediaSourceOption (MediaSource.ConversationImage)
				};
			}

			UIImage image = null;

			if (BlrtHelperiOS.IsPhone)
				image = UIImage.FromBundle ("mediapicker/sources/iPhone-media-feature-image.jpg");
			else
				image = UIImage.FromBundle ("mediapicker/sources/iPad-media-feature-image.jpg");

			SetHeader (
				image,
				BlrtHelperiOS.Translate ("select_type_image_title", "media"),
				BlrtHelperiOS.Translate ("select_type_image_subtitle", "media") 
			);
		}
			
	}
}

