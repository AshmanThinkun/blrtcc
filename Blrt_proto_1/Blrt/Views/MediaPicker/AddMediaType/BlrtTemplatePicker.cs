using System;
using System.Linq;
using System.Collections.Generic;

using CoreGraphics;

using Foundation;
using UIKit;

using BlrtData;
using BlrtData.Media;

using BlrtiOS.UI;
using BlrtData.IO;

using Newtonsoft.Json;
using BlrtCanvas.Pages;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtTemplatePicker : BlrtAddMediaNoneController
	{

		public BlrtTemplatePicker (IntPtr handler)
			: base (handler)
		{
			//this.Parent = parent;
		}
		public BlrtTemplatePicker (IBlrtAddMediaController parent) : base(parent)
		{
			Title = BlrtHelperiOS.Translate ("select_type_template", "media");
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();



			var templates =
				from t in BlrtTemplate.GetTemplateData()
					where t.Legacy == false
				select t;

			var list = new List<TemplateMediaSourceOption> ();

			foreach(var t in templates){

				list.Add (new TemplateMediaSourceOption (t));
			}

			Options = list.ToArray ();


			UIImage image = null;

			if (BlrtHelperiOS.IsPhone)
				image = UIImage.FromBundle ("mediapicker/sources/iPhone-media-feature-template.jpg");
			else
				image = UIImage.FromBundle ("mediapicker/sources/iPad-media-feature-template.jpg");


			SetHeader (
				image,
				BlrtHelperiOS.Translate("select_type_template_title", "media"),
				BlrtHelperiOS.Translate("select_type_template_subtitle", "media") 
			);
		}

		public override void OptionSelected (BlrtAddMediaTypeOption option, UIView target)
		{
			var o = option as TemplateMediaSourceOption;

			if (o != null) {
			
				Parent.AddMedia(new BlrtTemplatePage(o.source.Id));
				Parent.DismissAddMediaController(true);
			}
		}



		private class TemplateMediaSourceOption : BlrtAddMediaTypeOption{

			public BlrtTemplate source;

			public TemplateMediaSourceOption (BlrtTemplate source)
			{
				this.source = source;

				this.Title = source.Name;
				this.Image = UIImage.FromFile (BlrtTemplate.GetThumbnailPath(source));
			}
		}
		public override string GAName {	get { return "Template"; } }

	}
}

