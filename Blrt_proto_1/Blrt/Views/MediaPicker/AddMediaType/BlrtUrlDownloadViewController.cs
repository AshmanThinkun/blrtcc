using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using CoreGraphics;

using Foundation;
using UIKit;

using BlrtData;
using BlrtData.Media;

using BlrtiOS.UI;
using BlrtData.IO;

using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using BlrtiOS.View.CustomUI;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtUrlDownloadViewController : UIViewController
	{
		public event EventHandler<string> LinkSelected;
		public event EventHandler<UIImage> ImageSelected;



		private BlrtTextView _textField;
		private BlrtStackPanel _stackPanel;
		private UILabel _lbl;
		private BlrtActionButton _pasteBtn;

		private UIBarButtonItem _doneBtn;

		public BlrtUrlDownloadViewController () : base ()
		{
			PreferredContentSize = new CGSize (320, 320);
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = BlrtConfig.BLRT_LIGHT_GRAY;
			Title = BlrtHelperiOS.Translate ("url_downloader_title");


			EdgesForExtendedLayout = UIRectEdge.None;


			const float padding = 20;
			const float btnPadding = 10;

			_stackPanel = new BlrtStackPanel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Frame = new CGRect (padding, padding, View.Frame.Width - padding * 2, View.Frame.Height - padding * 2)
			};
			View.AddSubviews (_stackPanel);




			_lbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				Frame = new CGRect (0, 0, _stackPanel.Frame.Width, 32),
				Lines = 0,

				Text = BlrtHelperiOS.Translate ("url_downloader_help"),
				TextColor = BlrtConfig.BlrtTextGray,
				Font = BlrtConfig.NormalFont.Normal
			};
			_stackPanel.AddSubviews (_lbl);


			_stackPanel.AddSubviews (new UIView (new CGRect (0, 0, 10, 10)));


			_textField = new BlrtTextView () {
				AutoresizingMask 		= UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				Frame 					= new CGRect (0, 0, _stackPanel.Frame.Width, 32),
				BackgroundColor 		= UIColor.FromRGBA (1, 1, 1, 0.5f),
				AutocapitalizationType 	= UITextAutocapitalizationType.None,
				AutocorrectionType 		= UITextAutocorrectionType.No,
				KeyboardType 			= UIKeyboardType.Url,
				PlaceHolder 			= BlrtHelperiOS.Translate ("url_downloader_textfield_placeholder"),
			};
			_textField.Layer.BorderColor = UIColor.FromRGBA (0, 0, 0, 0.25f).CGColor;
			_textField.Layer.BorderWidth = 1;
			_stackPanel.AddSubviews (_textField);

			_stackPanel.AddSubviews (new UIView (new CGRect (0, 0, 10, 10)));




			var orLbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleRightMargin,
				Text = BlrtHelperiOS.Translate ("url_downloader_paste_or"),
				TextColor = BlrtConfig.BlrtTextGray,
				Font = BlrtConfig.NormalFont.Normal
			};

			orLbl.SizeToFit ();

			_pasteBtn = new BlrtActionButton (BlrtHelperiOS.Translate ("url_downloader_paste")) {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleHeight,
			};
			_pasteBtn.Click += PasteText;

			_pasteBtn.SizeToFit ();
			orLbl.SizeToFit ();

			orLbl.Frame = new CGRect (0, 0, orLbl.Bounds.Width, _pasteBtn.Bounds.Height);
			_pasteBtn.Frame = new CGRect (orLbl.Frame.Right + 8, 0, _pasteBtn.Bounds.Width, _pasteBtn.Bounds.Height);

			var tmpView = new  UIView (new CGRect (0, 0, _stackPanel.Bounds.Width, _pasteBtn.Bounds.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
			};
			tmpView.AddSubview (orLbl);
			tmpView.AddSubview (_pasteBtn);
			_stackPanel.AddSubviews (tmpView);



			NavigationItem.RightBarButtonItem = _doneBtn = new UIBarButtonItem (BlrtHelperiOS.Translate ("url_downloader_next"), UIBarButtonItemStyle.Done, DonePressed);


			_textField.Changed += (object sender, EventArgs e) => {
				EvaluateInput ();
			};

			EvaluateInput ();
		}

		private void DonePressed (object sender, EventArgs arg)
		{

			switch (GetInputState ()) {
			
				case LinkState.Invalid:
				case LinkState.UnknownHost:
				case LinkState.UnknownProtocol:

					new UIAlertView (
						BlrtHelperiOS.Translate ("invalid_url_alert_title"),
						BlrtHelperiOS.Translate ("invalid_url_alert_message"),
						null,
						BlrtHelperiOS.Translate ("invalid_url_alert_accept")
					).Show ();

					return;
				case LinkState.Ready:
					break;
				default: 
					return;
			}


			if (LinkSelected != null)
				LinkSelected (this, BlrtUtil.ParseUrl (_textField.Text));
		}

		private void PasteText (object sender, EventArgs arg)
		{

			if (UIPasteboard.General.Image != null) {

				if (ImageSelected != null) {
					ImageSelected (this, UIPasteboard.General.Image);
				}
			} else {
				_textField.Text = UIPasteboard.General.String;
				EvaluateInput ();
			}
		}

		private void EvaluateInput ()
		{

			var s = _lbl.SizeThatFits (new CGSize (_stackPanel.Bounds.Width, nfloat.MaxValue));
			_lbl.Frame = new CGRect (0, _lbl.Frame.Y, _stackPanel.Bounds.Width, s.Height + 1);

			_textField.LayoutIfNeeded ();
			_textField.Frame = new CGRect (0, _textField.Frame.Y, _stackPanel.Frame.Width, NMath.Max (32, _textField.ContentSize.Height)); 


			var state = GetInputState ();


			_doneBtn.Enabled = state != LinkState.EmptyOrWhitespace;
		}


		private LinkState GetInputState ()
		{

			if (string.IsNullOrWhiteSpace (_textField.Text))
				return LinkState.EmptyOrWhitespace;

			var str = BlrtUtil.ParseUrl (_textField.Text);

			var nsurl = NSUrl.FromString (str);

			if (nsurl == null || string.IsNullOrEmpty (nsurl.Scheme))
				return LinkState.UnknownProtocol;

			if (nsurl == null || string.IsNullOrEmpty (nsurl.Host))
				return LinkState.UnknownHost;

			//if(!_regex.IsMatch(str))
			//	return LinkState.Invalid;

			return LinkState.Ready;
		}


		private enum LinkState
		{
			EmptyOrWhitespace,
			UnknownHost,
			UnknownProtocol,
			Invalid,

			Ready
		}
	}
}

