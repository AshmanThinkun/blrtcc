using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public class  BlrtAddMediaPDFController : BlrtAddMediaNoneController
	{
		public override string GAName {	get { return "PDF"; } }

		public BlrtAddMediaPDFController (IntPtr handler) : base (handler)
		{
		}

		public BlrtAddMediaPDFController (IBlrtAddMediaController parent) : base (parent)
		{
			Title = BlrtHelperiOS.Translate ("select_type_pdf", "media");
		}


		public override void LoadView ()
		{
			base.LoadView ();


			if (BlrtHelperiOS.IsIOS8Above) {
				Options = new BlrtAddMediaTypeOption [] {
					new ImageMediaSourceOption (MediaSource.DocumentPicker),
					new ImageMediaSourceOption (MediaSource.iCloudDrive),
					new ImageMediaSourceOption (MediaSource.DropBox),
					new ImageMediaSourceOption (MediaSource.Url),
					new ImageMediaSourceOption (MediaSource.OpenIn),
					new ImageMediaSourceOption (MediaSource.ConversationPDF)
				};
			} else {
				Options = new BlrtAddMediaTypeOption[] {
					new ImageMediaSourceOption (MediaSource.DropBox),
					new ImageMediaSourceOption (MediaSource.Url),
					new ImageMediaSourceOption (MediaSource.OpenIn),
					new ImageMediaSourceOption (MediaSource.ConversationPDF)
				};
			}

			UIImage image = null;

			if (BlrtHelperiOS.IsPhone)
				image = UIImage.FromBundle ("mediapicker/sources/iPhone-media-feature-pdf.jpg");
			else
				image = UIImage.FromBundle ("mediapicker/sources/iPad-media-feature-pdf.jpg");


			SetHeader (
				image,
				BlrtHelperiOS.Translate ("select_type_pdf_title", "media"),
				BlrtHelperiOS.Translate ("select_type_pdf_subtitle", "media") 
			);
		}
	}
}

