using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;

namespace BlrtiOS.Views.MediaPicker
{
	using System.Threading.Tasks;
	using Acr.UserDialogs;
	using BlrtData;
	using BlrtiOS.Util;
	using BlrtiOS.Views.CustomUI;
	using Foundation;

	/*public class ImageViewerViewController : UIViewController
	{
		const float indentX = 50;
		const float indentY = 10;
		const float imageHeight = 100;

		UIViewFullscreen vMain;

		UILabel lBigImage;
		UIImageViewClickable ivcThumbnail1;

		UILabel lSmallImage;
		UIImageViewClickable ivcThumbnail2;

		public ImageViewerViewController () : base ("ImageViewerViewController", null)
		{
			var t = 0;
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			lBigImage = new UILabel ();
			lBigImage.Text = "Big image";
			lBigImage.BackgroundColor = UIColor.Clear;
			lBigImage.SizeToFit ();
			lBigImage.Frame = new CGRect (new CGPoint (View.Frame.Width / 2 - lBigImage.Frame.Width / 2, indentY), 
				lBigImage.Frame.Size);
			View.AddSubview (lBigImage);

			ivcThumbnail1 = new UIImageViewClickable ();
			ivcThumbnail1.Image = UIImage.FromFile ("Resource/DSC_0069.JPG");
			ivcThumbnail1.ContentMode = UIViewContentMode.ScaleAspectFit;
			ivcThumbnail1.AutoresizingMask = UIViewAutoresizing.All;
			ivcThumbnail1.OnClick += () => {
				if (vMain == null) {
					vMain = new UIViewFullscreen ();
				}
				vMain.SetImage (ivcThumbnail1.Image);
				vMain.Show ();
			};
			ivcThumbnail1.Frame = new CGRect (indentX, lBigImage.Frame.Bottom + indentY, 
				View.Frame.Width - indentX * 2, imageHeight);
			View.AddSubview (ivcThumbnail1);

			lSmallImage = new UILabel ();
			lSmallImage.Text = "Small image";
			lSmallImage.BackgroundColor = UIColor.Clear;
			lSmallImage.SizeToFit ();
			lSmallImage.Frame = new CGRect (new CGPoint (View.Frame.Width / 2 - lSmallImage.Frame.Width / 2, 
				ivcThumbnail1.Frame.Bottom + indentY), 
				lSmallImage.Frame.Size);
			View.AddSubview (lSmallImage);

			ivcThumbnail2 = new UIImageViewClickable ();
			ivcThumbnail2.Image = UIImage.FromFile ("Resource/DSC_0100.JPG");
			ivcThumbnail2.ContentMode = UIViewContentMode.ScaleAspectFit;
			ivcThumbnail2.AutoresizingMask = UIViewAutoresizing.All;
			ivcThumbnail2.OnClick += () => {
				if (vMain == null) {
					vMain = new UIViewFullscreen ();
				}
				vMain.SetImage (ivcThumbnail2.Image);
				vMain.Show ();
			};

			ivcThumbnail2.Frame = new CGRect (indentX, lSmallImage.Frame.Bottom + indentY, 
				View.Frame.Width - indentX * 2, imageHeight);
			View.AddSubview (ivcThumbnail2);
		}

//		[Obsolete]
//		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
//		{
//			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
//		}
	}

	public class UIImageViewClickable: UIImageView
	{
		UITapGestureRecognizer grTap;

		event Action onCl;

		public event Action OnClick {
			add {
				onCl += value;
				UpdateUserInteractionFlag ();
			}
			remove {
				onCl -= value;
				UpdateUserInteractionFlag ();
			}
		}

		void UpdateUserInteractionFlag ()
		{
			UserInteractionEnabled = ((onCl != null) && (onCl.GetInvocationList ().Length > 0));
			if (UserInteractionEnabled) {
				if (grTap == null) {
					grTap = new UITapGestureRecognizer (() => {
						if (onCl != null) {
							onCl ();
						}
					});
					grTap.CancelsTouchesInView = true;
					AddGestureRecognizer (grTap);
				}
			} else {
				if (grTap != null) {
					RemoveGestureRecognizer (grTap);
					grTap = null;
				}
			}
		}

		public UIImageViewClickable ()
		{
		}
	}
	*/
	using Asset = ELCPicker.ImagePickerViewController.ImagePicker.ELCAsset;

	public class BlrtThisBtnPressedEventArgs : EventArgs
	{
		public string Path { get; private set; }
		public BlrtMediaFormat Format { get; private set; }

		public BlrtThisBtnPressedEventArgs (string decyptedPath, BlrtMediaFormat format)
		{
			Path = decyptedPath;
			Format = format;
		}
	}

	public class PDFPreviewController : UIViewController
	{
		const int PADDING = 20;
		UIWebView PDFView { get; set; }
		MediaAsset PDFAsset { get; set; }
		UIButton BlrtThisBtn { get; set; }

		public event EventHandler<BlrtThisBtnPressedEventArgs> BlrtThisBtnPressed;

		public PDFPreviewController (MediaAsset pdfAsset)
		{
			PDFAsset = pdfAsset;

			PDFView = new UIWebView () { ScalesPageToFit = true };
			PDFView.ScrollView.ShowsVerticalScrollIndicator = PDFView.ScrollView.ShowsHorizontalScrollIndicator = false;
			var url = new NSUrl (pdfAsset.DecyptedPath);
			var request = NSUrlRequest.FromUrl (url);
			PDFView.LoadRequest (request);
			BlrtThisBtn = NewBlrtButton.CreateBlrtThisBtn (null);
			BlrtThisBtn.TouchUpInside += (sender, e) => {
				if (BlrtThisBtnPressed != null) {
					BlrtThisBtnPressed (null,
					                    new BlrtThisBtnPressedEventArgs (PDFAsset.DecyptedPath, PDFAsset.Format));
				}
			};
			BlrtThisBtn.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;

			View.AddSubview (PDFView);
			View.AddSubview (BlrtThisBtn);
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			var newRect = View.Bounds;
			if (!PDFView.Frame.Equals (newRect))
				PDFView.Frame = newRect;

			var size = BlrtThisBtn.SizeThatFits (View.Bounds.Size);
			newRect = new CGRect(
				new CGPoint(View.Bounds.GetMidX() - size.Width * 0.5, View.Bounds.Bottom - size.Height - PADDING), 
				size);
			
			if (!BlrtThisBtn.Frame.Equals (newRect))
				BlrtThisBtn.Frame = newRect;
		}
	}

	public class ImagePreviewController : UIViewController
	{
		public EventHandler PreviewClosed;

		public static readonly int TOP_BAR_HEIGHT = 45;

		UIImagePreview ImagePreview { get; set; }
		List<Asset> AssetsFromImagePicker { get; set; }
		List<MediaAsset> AssetsFromConvoItem { get; set; }
		public UIBarButtonItem DoneButtonItem { get; set; }
		TopNavBarView AssetNavBar { get; set; }
		BlrtMediaFormat MediaFormat { get; set; }
		int CurrentAssetNum { get { return AssetNavBar.CurrentAssetNum; } set { AssetNavBar.CurrentAssetNum = value; } }
		int TotalAssetCount { get { return AssetNavBar.TotalAssetCount; } set { AssetNavBar.TotalAssetCount = value; } }

		public event EventHandler<BlrtThisBtnPressedEventArgs> BlrtThisBtnPressed;

		public int RemainingImageCount {
			get { return ImagePreview.RemainingImageCount; }
			set { ImagePreview.RemainingImageCount = value; }
		}

		bool IsSrcConvoItem {get; set;}
		int PreviousAssetNum { get; set; }

		public ImagePreviewController (List<MediaAsset> items) : this (1, items.Count, true)
		{
			AssetsFromConvoItem = items;
			ImagePreview.BlrtThisBtnPressed += (sender, e) => {
				if (BlrtThisBtnPressed != null) {
					var item = items [CurrentAssetNum - 1];
						BlrtThisBtnPressed (null, 
					                        new BlrtThisBtnPressedEventArgs (item.DecyptedPath, item.Format));
				}
			};
		}

		ImagePreviewController (int currentAssetNum, int assetCount, bool isSrcConvoItem)
		{
			EdgesForExtendedLayout = UIRectEdge.None;
			IsSrcConvoItem = isSrcConvoItem;
			ImagePreview = new UIImagePreview (IsSrcConvoItem);
			AssetNavBar = new TopNavBarView (currentAssetNum, assetCount);
			AssetNavBar.UpdateImage += delegate { SetImage (CurrentAssetNum); };
			PreviousAssetNum = -1;

			View.AddSubviews (new UIView [] {
				ImagePreview,
				AssetNavBar
			});
		}

		public ImagePreviewController (List<Asset> assets, int currentAssetNum) : this (currentAssetNum, assets.Count, false)
		{
			AssetsFromImagePicker = assets;
			ImagePreview.SelectBtn.TouchUpInside += delegate { 
				AssetsFromImagePicker [CurrentAssetNum - 1].Selected = !AssetsFromImagePicker [CurrentAssetNum - 1].Selected;	
			};
		}

		public void SetImage (int currentAssetNum)
		{
			if (PreviousAssetNum == currentAssetNum)
				return;
			
			CurrentAssetNum = PreviousAssetNum = currentAssetNum;
			AssetNavBar.CurrentAssetNumText = CurrentAssetNum.ToString ();

			if (AssetsFromImagePicker != null) {
				SetImageFromImagePicker ();
			} else {
				SetImageFromConvoItem ();
			}

			AssetNavBar.SetNeedsLayout ();
			ImagePreview.SetNeedsLayout ();
			//ImagePreview.SetNeedsDisplay ();
		}

		void SetImageFromConvoItem ()
		{
			var item = AssetsFromConvoItem [CurrentAssetNum - 1];
			NavigationItem.Title = item.Name;

			var img = UIImage.FromFile (item.DecyptedPath);
			if (img == null) {
				NavigationController.PopViewController (true);
				return;
			}
			
			ImagePreview.SetImage (img, false);
			img = null;
		}

		void SetImageFromImagePicker ()
		{
			var item = AssetsFromImagePicker [CurrentAssetNum - 1];
			var rep = item.Asset.DefaultRepresentation;
			NavigationItem.Title = item.Asset.DefaultRepresentation.Filename;
			CGImage cgImage = null;
			cgImage = rep.GetFullScreenImage ();
			var oldImg = new UIImage (cgImage);
			cgImage = null;
			ImagePreview.SetImage (oldImg, item.Selected);
			oldImg = null;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (IsSrcConvoItem)
				return;
			
			DoneButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done);
			DoneButtonItem.Clicked += delegate {
				ImagePreview?.Dispose ();
				NavigationController.PopViewController (true); 
			};
			NavigationItem.RightBarButtonItem = DoneButtonItem;
		}

		public EventHandler PreviewWillRotate;

		public override void DidRotate (UIInterfaceOrientation toInterfaceOrientation)
		{
			base.DidRotate (toInterfaceOrientation);

			if (PreviewWillRotate != null) {
				PreviewWillRotate (null, null);
			}
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			AssetNavBar.Frame = new CGRect (View.Bounds.Left,
											View.Bounds.Top,
											View.Bounds.Width,
											TOP_BAR_HEIGHT);

			ImagePreview.Frame = new CGRect (AssetNavBar.Frame.Left,
											 AssetNavBar.Frame.Bottom,
											 View.Bounds.Width,
											 View.Bounds.Height - TOP_BAR_HEIGHT);

			SetImage (CurrentAssetNum);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (IsMovingFromParentViewController) {
				if (PreviewClosed != null) {
					PreviewClosed (null, null);
				}
			}
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			if (IsMovingFromParentViewController) {
				ImagePreview?.Dispose ();
			}
		}

		class TopNavBarView : UIView
		{
			static readonly int PAGE_INFO_PADDING = 10;
			readonly int BTN_PADDING = 10;
			readonly int SPACING = 25;

			UIButton PrevBtn { get; set; }
			UIButton NextBtn { get; set; }
			InfoView Info { get; set; }
			public string CurrentAssetNumText { get { return Info.CurrentAssetNum.Text; } set { Info.CurrentAssetNum.Text = value; } }
			string TotalAssetsFromImagePickerText { get { return Info.TotalAssetsFromImagePicker.Text; } set { Info.TotalAssetsFromImagePicker.Text = value; } }

			public int CurrentAssetNum { get; set; }
			public int TotalAssetCount { get; set; }

			UIImage NextBtnImage { get { return UIImage.FromBundle ("preview_right_arrow.png"); } }
			UIImage PrevBtnImage { get { return UIImage.FromBundle ("preview_left_arrow.png"); } }

			public event EventHandler UpdateImage;

			public TopNavBarView (int currentAssetNum, int assetCount)
			{
				BackgroundColor = BlrtStyles.iOS.Charcoal;
				Layer.BorderWidth = 0;

				PrevBtn = new UIButton () {
					UserInteractionEnabled = true
				};

				NextBtn = new UIButton () {
					UserInteractionEnabled = true
				};

				NextBtn.SetImage (NextBtnImage, UIControlState.Normal);
				PrevBtn.SetImage (PrevBtnImage, UIControlState.Normal);

				NextBtn.Frame = new CGRect (0, 0, 
				                            NextBtn.ImageView.Image.Size.Width + BTN_PADDING * 2,
				                            0);

				PrevBtn.Frame = new CGRect (0, 0, 
				                            PrevBtn.ImageView.Image.Size.Width + BTN_PADDING * 2, 
											0);

				NextBtn.TouchUpInside += ShowNextImage;
				PrevBtn.TouchUpInside += ShowPrevImage;

				Info = new InfoView ();

				CurrentAssetNum = currentAssetNum;
				TotalAssetCount = assetCount;

				TotalAssetsFromImagePickerText = TotalAssetCount.ToString();
				CurrentAssetNumText = CurrentAssetNum.ToString ();

				AddSubviews (new UIView [] {
					Info,
					PrevBtn,
					NextBtn
				});
			}

			void ShowNextImage (object sender, EventArgs e)
			{
				var old = CurrentAssetNum;
				var current = Math.Min (CurrentAssetNum + 1, TotalAssetCount);

				if (old == current) return;

				CurrentAssetNum = current;
				CurrentAssetNumText = current.ToString();
				Info.SetNeedsLayout ();

				if (UpdateImage != null) {
					UpdateImage (null, null);
				}
			}

			void ShowPrevImage (object sender, EventArgs e)
			{
				var old = CurrentAssetNum;
				var current = Math.Max (CurrentAssetNum - 1, 1);

				if (old == current) return;

				CurrentAssetNum = current;
				CurrentAssetNumText = current.ToString ();
				Info.SetNeedsLayout ();

				if (UpdateImage != null) {
					UpdateImage (null, null);
				}
			}

			public override void SetNeedsLayout ()
			{
				base.SetNeedsLayout ();
				Info.SetNeedsLayout ();
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var size = Info.SizeThatFits (new CGSize (Bounds.Width, Bounds.Height - PAGE_INFO_PADDING));
				var left = (Bounds.Width - size.Width) * 0.5;

				var newRect = new CGRect (left - PrevBtn.Frame.Width - SPACING,
											Bounds.Top + PAGE_INFO_PADDING * 0.5,
											PrevBtn.Frame.Width,
											Bounds.Height - PAGE_INFO_PADDING);

				if (!newRect.Equals(PrevBtn.Frame))
					PrevBtn.Frame = newRect;

				newRect = new CGRect (left + size.Width + SPACING,
											Bounds.Top + PAGE_INFO_PADDING * 0.5,
											NextBtn.Frame.Width,
											Bounds.Height - PAGE_INFO_PADDING);

				if (!newRect.Equals (NextBtn.Frame))
					NextBtn.Frame = newRect;

				newRect = new CGRect (new CGPoint (left, Bounds.Top + PAGE_INFO_PADDING * 0.5), size);

				if (!newRect.Equals (Info.Frame))
					Info.Frame = newRect;		
			}

			class InfoView : UIView
			{
				readonly int INSET = PAGE_INFO_PADDING * 2;

				public UILabel CurrentAssetNum { get; set; }
				public UILabel TotalAssetsFromImagePicker { get; set; }
				UILabel Separator { get; set; }

				UIColor LabelTextColor { get { return BlrtStyles.iOS.Gray7; } }
				UIFont LabelFont { get { return BlrtStyles.iOS.CellTitleBoldFont; } }
				string SeparatorSymbol { get { return " / "; } }

				public InfoView ()
				{
					BackgroundColor = BlrtStyles.iOS.White;
					Layer.CornerRadius = 4;

					CurrentAssetNum = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont
					};

					TotalAssetsFromImagePicker = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont
					};

					Separator = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont,
						Text = SeparatorSymbol
					};

					AddSubviews (new UIView [] {
						CurrentAssetNum,
						TotalAssetsFromImagePicker,
						Separator
					});
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					var currentAssetSize = CurrentAssetNum.SizeThatFits (Bounds.Size);
					var totalAssetSize = TotalAssetsFromImagePicker.SizeThatFits (Bounds.Size);
					var separatorSize = Separator.SizeThatFits (Bounds.Size);
					var width = totalAssetSize.Width * 2 + separatorSize.Width;

					var newRect = new CGRect (Bounds.X + INSET * 0.5 + width - totalAssetSize.Width,
												  Bounds.Top + PAGE_INFO_PADDING * 0.5,
												  totalAssetSize.Width,
												  Bounds.Height - PAGE_INFO_PADDING);

					if(!newRect.Equals(TotalAssetsFromImagePicker.Frame))
						TotalAssetsFromImagePicker.Frame = newRect;

					newRect = new CGRect (TotalAssetsFromImagePicker.Frame.Left - separatorSize.Width,
												  Bounds.Top + PAGE_INFO_PADDING * 0.5,
												  separatorSize.Width,
												  CurrentAssetNum.Frame.Height);

					if (!newRect.Equals (Separator.Frame))
						Separator.Frame = newRect;

					newRect = new CGRect (Separator.Frame.Left - currentAssetSize.Width,
											  Bounds.Top + PAGE_INFO_PADDING * 0.5,
											  currentAssetSize.Width,
											  Bounds.Height - PAGE_INFO_PADDING);

					if (!newRect.Equals (CurrentAssetNum.Frame))
						CurrentAssetNum.Frame = newRect;
				}

				public override CGSize SizeThatFits (CGSize size)
				{
					base.SizeThatFits (size);

					var totalAssetSize = TotalAssetsFromImagePicker.SizeThatFits (Bounds.Size);
					var separatorSize = Separator.SizeThatFits (Bounds.Size);
					var width = totalAssetSize.Width * 2 + separatorSize.Width + INSET;

					return new CGSize (width, size.Height); 
				}
			}
		}

		class UIImagePreview : UIView
		{
			readonly int SELECT_BUTTON_PADDING = 25;

			public int RemainingImageCount { get; set; }
			UIImage iImage;
			ScrollView scrollView;
			bool IsSelected { get; set; }
			UIImage selectedStateImg;
			UIImage deSelectedStateImg;

			UIImage SelectedStateImg {
				get { return selectedStateImg ?? (selectedStateImg = UIImage.FromBundle ("img_select_sm.png")); }
			}

			UIImage DeSelectedStateImg {
				get { return deSelectedStateImg ?? (deSelectedStateImg = UIImage.FromBundle ("img_deselect_sm.png")); }
			}

			UIImage SelectBtnImg {
				get { return IsSelected ? SelectedStateImg : DeSelectedStateImg; }
			}

			public event EventHandler BlrtThisBtnPressed {
				add { BlrtThisBtn.TouchUpInside += value; }
				remove { BlrtThisBtn.TouchUpInside -= value; }
			}

			public UIButton SelectBtn;
			public UIButton BlrtThisBtn;

			bool IsSrcConvoItem {get; set;}
			public bool UseAnimation = true;
			public float AnimationDuration = 0.2f;

			public UIImagePreview (bool isSrcConvoItem)
			{
				IsSrcConvoItem = isSrcConvoItem;
				BackgroundColor = BlrtStyles.iOS.Gray2;

				if (IsSrcConvoItem) {

					BlrtThisBtn = NewBlrtButton.CreateBlrtThisBtn (null);
					BlrtThisBtn.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;
					scrollView = new ScrollView (ref BlrtThisBtn, true);
				} 
				else {
					SelectBtn = new UIButton ();
					SelectBtn.SetImage (SelectBtnImg, UIControlState.Normal);
					SelectBtn.Frame = new CGRect (0,
												  0,
												  SelectBtn.ImageView.Image.Size.Width + SELECT_BUTTON_PADDING,
												  SelectBtn.ImageView.Image.Size.Height + SELECT_BUTTON_PADDING);

					SelectBtn.TouchUpInside += delegate {

						if (IsSelected) {
							RemainingImageCount++;
						} else {
							if (RemainingImageCount > 0) {
								RemainingImageCount--;
							} else {
								return;
							}
						}
						IsSelected = !IsSelected;

						Animate (AnimationDuration, delegate {
							SelectBtn.SetImage (SelectBtnImg, UIControlState.Normal);
						});
					};
					scrollView = new ScrollView (ref SelectBtn, false);
				}

				AddSubview (scrollView);

				if (SelectBtn != null)
					AddSubview (SelectBtn);

				if (BlrtThisBtn != null)
					AddSubview (BlrtThisBtn);

			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();
				if (scrollView.Frame != Bounds)
					scrollView.Frame = Bounds;
				
				scrollView.SetImage (iImage);
			}

			public void SetImage (UIImage image, bool isSelected)
			{
				if (iImage != null) {
					iImage.Dispose ();
					iImage = null;
				}

				iImage = image;

				if (IsSrcConvoItem)
					return;
				
				IsSelected = isSelected;
				SelectBtn.SetImage (SelectBtnImg, UIControlState.Normal);
			}

			protected override void Dispose (bool disposing)
			{
				base.Dispose (disposing);

				if (iImage != null) {
					iImage.Dispose ();
					iImage = null;
				}
			}

			class ScrollView : UIScrollView
			{
				/// <summary>
				/// The double tap interval. Measured in ms
				/// </summary>
				const float ANIMATION_DURATION = 0.4f;

				float AnimationDuration { get; set; }

				const float defaultZoom = 2f;
				const float minZoom = 0.1f;
				const float maxZoom = 3f;
				nfloat sizeToFitZoom = 1f;

				UIImageView imageView;

				//UITapGestureRecognizer grTap;
				UITapGestureRecognizer grDoubleTap;

				//public event Action OnSingleTap;
				UIButton SelectBtn;
				UIButton BlrtThisBtn;

				public ScrollView (ref UIButton ActionButton, bool IsSrcConvoItem)
				{
					AnimationDuration = 0.0f;

					if (IsSrcConvoItem) {
						BlrtThisBtn = ActionButton;
					} else {
						SelectBtn = ActionButton;
					}

					AutoresizingMask = UIViewAutoresizing.All;

					imageView = new UIImageView ();
					imageView.ContentMode = UIViewContentMode.ScaleAspectFit;

					AddSubview (imageView);

					// Setup zoom
					MaximumZoomScale = maxZoom;
					MinimumZoomScale = minZoom;
					ShowsVerticalScrollIndicator = false;
					ShowsHorizontalScrollIndicator = false;
					BouncesZoom = true;
					ViewForZoomingInScrollView += (UIScrollView sv) => {
						return imageView;
					};

					// Setup gestures
					//grTap = new UITapGestureRecognizer (() => {
					//	if (OnSingleTap != null) {
					//		OnSingleTap ();
					//	}
					//});
					//AddGestureRecognizer (grTap);

					grDoubleTap = new UITapGestureRecognizer (() => {
						if (ZoomScale >= defaultZoom) {
							AnimationDuration = ANIMATION_DURATION;
							SetZoomScale (sizeToFitZoom, true);
						} else {
							AnimationDuration = ANIMATION_DURATION;

							// Zoom to user specified point instead of center
							var point = grDoubleTap.LocationInView (grDoubleTap.View);
							var zoomRect = GetZoomRect (defaultZoom, point);

							ZoomToRect (zoomRect, true);
						}
					});
					grDoubleTap.NumberOfTapsRequired = 2;
					AddGestureRecognizer (grDoubleTap);

					// To use single tap and double tap gesture recognizers together. See for reference:
					// http://stackoverflow.com/questions/8876202/uitapgesturerecognizer-single-tap-and-double-tap
					//grTap.RequireGestureRecognizerToFail (grDoubleTap);
				}

				public void SetImage (UIImage image)
				{
					if (image == null)
						return;
					
					ZoomScale = 1;
					imageView.Image = image;
					imageView.Frame = new CGRect (new CGPoint (), image.Size);
					ContentSize = image.Size;

					nfloat wScale = Frame.Width / image.Size.Width;
					nfloat hScale = Frame.Height / image.Size.Height;

					MinimumZoomScale = NMath.Min (wScale, hScale);
					sizeToFitZoom = MinimumZoomScale;
					ZoomScale = MinimumZoomScale;

					//
					imageView.Frame = CenterScrollViewContents ();
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					var newRect = CenterScrollViewContents ();
					if (!newRect.Equals(imageView.Frame))
						imageView.Frame = newRect;

					if (SelectBtn != null) {

						var right = imageView.Frame.Right > Frame.Right ? Frame.Right : imageView.Frame.Right;
						var Bottom = Frame.Bottom - TOP_BAR_HEIGHT;
						var bottom = imageView.Frame.Bottom > Bottom ? Bottom : imageView.Frame.Bottom;

						newRect = new CGRect (right - SelectBtn.Frame.Width,
												  bottom - SelectBtn.Frame.Height,
												  SelectBtn.Frame.Width,
												  SelectBtn.Frame.Height);

						if (SelectBtn.Frame.Equals (newRect))
							return;

					} else if (BlrtThisBtn != null) {

						var imgViewMidX = imageView.Frame.GetMidX ();
						var frameMidX = Frame.GetMidX ();
						var centerX = imgViewMidX > frameMidX ? frameMidX : imgViewMidX;
						var blrtThisBtnFrame = BlrtThisBtn.SizeThatFits (Frame.Size);
						var bottom = Frame.Bottom - blrtThisBtnFrame.Height;
						//var bottom = imageView.Frame.Bottom > Bottom ? Bottom : imageView.Frame.Bottom;

						newRect = new CGRect (centerX - blrtThisBtnFrame.Width * 0.5,
												  bottom - blrtThisBtnFrame.Height,
												  blrtThisBtnFrame.Width,
												  blrtThisBtnFrame.Height);

						if (BlrtThisBtn.Frame.Equals (newRect))
							return;
					}

					if (AnimationDuration > 0) {
						Animate (AnimationDuration, delegate {
							if (SelectBtn != null)
								SelectBtn.Frame = newRect;
							
							if (BlrtThisBtn != null)
								BlrtThisBtn.Frame = newRect;
						});
						AnimationDuration = 0.0f;
					} else {
						if (SelectBtn != null)
							SelectBtn.Frame = newRect;

						if (BlrtThisBtn != null)
							BlrtThisBtn.Frame = newRect;
					}
				}

				public override CGRect Frame {
					get {
						return base.Frame;
					}
					set {
						if (!base.Frame.Equals (value))
							base.Frame = value;

						if (imageView != null) {
							if (!imageView.Equals (value))
								imageView.Frame = value;
						}
					}
				}

				public CGRect CenterScrollViewContents ()
				{
					var boundsSize = Frame.Size;
					var contentsFrame = imageView.Frame;

					if (contentsFrame.Width < boundsSize.Width) {
						contentsFrame.X = (boundsSize.Width - contentsFrame.Width) / 2;
					} else {
						contentsFrame.X = 0;
					}

					if (contentsFrame.Height < boundsSize.Height) {
						contentsFrame.Y = (boundsSize.Height - contentsFrame.Height) / 2;
					} else {
						contentsFrame.Y = 0;
					}

					return contentsFrame;
				}

				// Reference:
				// http://stackoverflow.com/a/11003277/548395
				CGRect GetZoomRect (float scale, CGPoint center)
				{
					var size = new CGSize (imageView.Frame.Size.Height / scale, imageView.Frame.Size.Width / scale);

					var center2 = ConvertPointToView (center, imageView);
					var location2 = new CGPoint (center2.X - (size.Width / 2.0f),
						center2.Y - (size.Height / 2.0f)
					);

					var zoomRect = new CGRect (location2, size);
					return zoomRect;
				}
			}
		}
	}
}

