using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public class  BlrtAddMediaWebpageController : BlrtAddMediaNoneController
	{
		public override string GAName {	get { return "Website"; } }

		public BlrtAddMediaWebpageController (IntPtr handler)
			: base (handler)
		{
			//this.Parent = parent;
		}

		public BlrtAddMediaWebpageController (IBlrtAddMediaController parent)
			: base (parent)
		{
			Title = BlrtHelperiOS.Translate ("select_type_website","media");
		}


		public override void LoadView ()
		{
			base.LoadView ();


			Options = new BlrtAddMediaTypeOption[] {
				new ImageMediaSourceOption(MediaSource.Webpage),
				new ImageMediaSourceOption(MediaSource.PasteUrl),
			};

			UIImage image = null;

			if (BlrtHelperiOS.IsPhone)
				image = UIImage.FromBundle ("mediapicker/sources/iPhone-media-feature-website.jpg");
			else
				image = UIImage.FromBundle ("mediapicker/sources/iPad-media-feature-website.jpg");


			SetHeader (
				image,
				BlrtHelperiOS.Translate("select_type_website_title",	"media"),
				BlrtHelperiOS.Translate("select_type_website_subtitle",	"media") 
			);
		}
	}
}

