﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BlrtData;
using BlrtiOS.UI;
using BlrtShared;
using UIKit;

namespace BlrtiOS.Views.MediaPicker
{
	public static class BlrtAddMediaTypes
	{
		public delegate Task MediaPickedCallback (BlrtMediaUrlManager mediaUrlManager);

		public static void OpenDocumentPicker (UIView target, UIViewController requestingController, 
		                                       MediaPickedCallback mediaPickedCallback, string [] MediaTypes)
		{
			try {
				var picker = new UIDocumentMenuViewController (
					MediaTypes,
					UIDocumentPickerMode.Import
				);

				picker.DidPickDocumentPicker += (object sender, UIDocumentMenuDocumentPickedEventArgs e) => {

					var _picker = e.DocumentPicker;

					while (requestingController.ParentViewController != null)
						requestingController = requestingController.ParentViewController;

					requestingController.PresentViewController (_picker, true, null);

					_picker.DidPickDocument += (object s, UIDocumentPickedEventArgs ev) => {
						OpenMediaFromUrl (ImageMediaSource.DocumentPicker, 
						                  new [] { ev.Url.AbsoluteString }, 
						                  requestingController.View,
						                  mediaPickedCallback).FireAndForget();
					};
				};

				if (BlrtHelperiOS.IsPhone) {
				} else {
					picker.PopoverPresentationController.SourceView = target;
					picker.PopoverPresentationController.SourceRect = target.Bounds;
				}

				while (requestingController.ParentViewController != null) {
					requestingController = requestingController.ParentViewController;
				}
				requestingController.PresentViewController (picker, true, null);

			} catch {
				OpeniCloudDrive (requestingController, mediaPickedCallback);
			}
		}

		public static async Task OpenMediaFromUrl (ImageMediaSource source, string [] url, UIView view, MediaPickedCallback mediaPickedCallback)
		{
			BlrtMediaUrlManager manager = new BlrtMediaUrlManager (url);

			bool tryAgain = true;
			bool cancelled = false;

			bool alertFinished = false;

			BlrtProgressAlertView alert = null;
			Exception ex = null;

			while (tryAgain && !cancelled && manager.Count > 0) {
				tryAgain = false;
				try {
					var cancellation = new CancellationTokenSource ();
					var task = manager.TryLocalizeMedia (cancellation.Token);

					alert = new BlrtProgressAlertView (
						BlrtHelperiOS.Translate ("alert_media_downloading_title", "media"),
						string.Format (BlrtHelperiOS.Translate ("alert_media_downloading_message", "media"), manager.FinishedCount, manager.Count),
									null,
						BlrtHelperiOS.Translate ("alert_media_downloading_cancel", "media")
								);
					alert.Clicked += SafeEventHandler.FromAction (
						(object sender, UIButtonEventArgs e) => {
							if (e.ButtonIndex == alert.CancelButtonIndex) {
								cancellation.Cancel ();
								cancelled = true;
							}
						}
					);

					alertFinished = false;
					alert.Show ();

					Foundation.NSObject.InvokeInBackground (() => {
						float lastValue = -1;
						while (!alertFinished && alert != null) {
							var tmp = manager.GetProgress ();
							if (tmp != lastValue) {
								lastValue = tmp;
								view.InvokeOnMainThread (() => {
									if (alert != null) {
										alert.SetProgress (lastValue, true);
										alert.Message = string.Format (BlrtHelperiOS.Translate ("alert_media_downloading_message", "media"), manager.FinishedCount, manager.Count);
									}
								});
							}
							Thread.Sleep (100);
						}
					});

					await task;

				} catch (Exception e) {
					ex = e;
				} finally {
					alertFinished = true;
					if (alert != null) {
						alert.DismissWithClickedButtonIndex (-9999, true);
						alert = null;
					}
				}

				try {
					await mediaPickedCallback (manager);
				} catch (Exception e) {
					ex = e;
				}

				if (manager.Count > 0 && !cancelled) {

					if (manager.HasInternetProblem) {

						var internetProblem = new UIAlertView (
							BlrtHelperiOS.Translate ("alert_media_failed_downloading_title", "media"),
							BlrtHelperiOS.Translate ("alert_media_failed_downloading_message", "media"),
							null,
							BlrtHelperiOS.Translate ("alert_media_failed_downloading_cancel", "media"),
							BlrtHelperiOS.Translate ("alert_media_failed_downloading_retry", "media")
						);
						internetProblem.Clicked += SafeEventHandler.FromAction (
							(object sender, UIButtonEventArgs e) => {
								if (e.ButtonIndex != internetProblem.CancelButtonIndex) {
									tryAgain = true;
								}
							}
						);
						internetProblem.Show ();
						await BlrtHelperiOS.AwaitAlert (internetProblem);
					}
				}

				if (manager.HasFileTooLarge) {
					var invalidFormatAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxPDFSizeKB, 0);
					manager.RemoveFileTooLarge ();
					await BlrtHelperiOS.AwaitAlert (invalidFormatAlert);
				}
				if (manager.HasInvalidFormat) {
					var invalidFormatAlert = new UIAlertView (
						BlrtHelperiOS.Translate ("alert_unknown_format_title", "media"),
						BlrtHelperiOS.Translate ("alert_unknown_format_message", "media"),
						null,
						BlrtHelperiOS.Translate ("alert_unknown_format_cancel", "media")
					);
					invalidFormatAlert.Show ();
					await BlrtHelperiOS.AwaitAlert (invalidFormatAlert);
				}
			}
		}

		public static void OpeniCloudDrive (UIViewController requestingController, MediaPickedCallback mediaPickedCallback)
		{
			var picker = new UIDocumentPickerViewController (
				new string [] {
					MobileCoreServices.UTType.PDF,
					MobileCoreServices.UTType.Image,
				},
				UIDocumentPickerMode.Import
			);

			picker.DidPickDocument += (object s, UIDocumentPickedEventArgs ev) => {
				OpenMediaFromUrl (ImageMediaSource.DocumentPicker, 
				                  new [] { ev.Url.AbsoluteString }, 
				                  requestingController.View, 
				                  mediaPickedCallback);
			};

			while (requestingController.ParentViewController != null)
				requestingController = requestingController.ParentViewController;

			requestingController.PresentViewController (picker, true, null);
		}

		public class DocumentPickerReturnedMediaInfo
		{
			public string MediaName { get; private set; }
			public string MediaURL { get; private set; }
			public BlrtMediaFormat MediaFormat { get; private set; }

			public DocumentPickerReturnedMediaInfo (string mediaName,
													string mediaURL,
													BlrtMediaFormat mediaFormat)
			{
				MediaName = mediaName;
				MediaURL = mediaURL;
				MediaFormat = mediaFormat;
			}
		}
	}
}
