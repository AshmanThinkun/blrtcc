﻿using System;
using BlrtData;
using UIKit;

namespace ImagePickers
{
	namespace ImageAssets
	{
		public class PhotoAssetForMediaAsset : BaseImageAsset
		{
			public readonly MediaAsset Asset; 

			public PhotoAssetForMediaAsset (BaseImagePicker parent, MediaAsset asset, int index) : base (parent, index)
			{
				Asset = asset;
			}

			public override UIImage Thumbnail {
				get {
					return UIImage.FromFile (Asset.LocalThumbnailPath);
				}
			}

			public override string Title {
				get {
					return Asset.Name;
				}
			}
		}
	}
}