﻿using System;

using Acr.Support.iOS;
using Foundation;
using PhotoKitGallery;
using Photos;
using UIKit;

namespace ImagePickers
{
	namespace ImageAssets
	{
		public class PhotoGalleryAsset :  BaseImageAsset
		{
			public PHAsset Asset;



			public PhotoGalleryAsset (UICollectionView parent, PHAsset asset, int index) : base (parent, index)
			{
				Asset = asset;
			}



			public override UIImage Thumbnail {
				get {
					//return UIImage.FromImage (Asset.Thumbnail);
					var options = new PHImageRequestOptions {
						Synchronous = true
					};
					var cell = new UICollectionViewCell ();
					var image = new UIImage ();
					PHImageManager.DefaultManager.RequestImageForAsset (Asset, PHImageManager.MaximumSize, PHImageContentMode.AspectFit, options, (requestedImage, _) => {
						image = requestedImage;

					});

					return image;
				}
			}

			public override string Title {
				get {
					//return Asset.DefaultRepresentation.Filename;

					var resources = PHAssetResource.GetAssetResources (Asset);

					string fileName = resources [0].OriginalFilename;

					return fileName;


				}
			}
		}

	}

}