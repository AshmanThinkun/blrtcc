﻿using System;
using PhotoKitGallery;
using Photos;
using UIKit;

namespace ImagePickers
{
	namespace ImageAssets
	{
		/**
		 * Represents image state such as selection state.
		*/
		public abstract class BaseImageAsset
		{
			readonly WeakReference _Parent;
			public int Index { get; private set; }

			bool _Selected;

			public BaseImageAsset (object parent, int index)
			{
				_Parent = new WeakReference (parent);
				Index = index;
			}

			public void ToggleSelected ()
			{
				Selected = !Selected;
			}

			public bool Selected {
				get {
					return _Selected;
				}

				set {
					var parent = _Parent.Target as AlbumPhotosController;
					if (value && parent != null ) {
						return;
					}

					_Selected = value;

					if (parent != null) {
					//	parent.PreviewAssetSelected (this);
					}
				}
			}

			public abstract string Title { get; }
			public abstract UIImage Thumbnail { get; }
		}
	}
}