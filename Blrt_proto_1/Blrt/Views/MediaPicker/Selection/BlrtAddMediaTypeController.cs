using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;
using UIKit;
using Foundation;
using BlrtData;
using BlrtData.IO;
using BlrtData.Media;
using Dropins.Chooser.iOS;
using System.Net;
using System.IO;
using BlrtCanvas;
using BlrtCanvas.Pages;
using BlrtiOS.Util;
using BlrtData.ExecutionPipeline;
using ImagePickers;
using System.Collections;
using Acr.UserDialogs;
using BlrtiOS.Views.FullScreenViewers;

using System.Drawing;
using PhotoKitGallery;
using Photos;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtAddMediaTypeOption : IDisposable
	{
		public virtual UIImage Image { get; set; }
		public virtual string Title { get; set; }

		#region IDisposable implementation

		public void Dispose ()
		{
			BlrtUtil.BetterDispose (Image);
		}

		#endregion
	}



	public abstract partial class BlrtAddMediaTypeController : UICollectionViewController
	{

		UIView _headerView;

		public IBlrtAddMediaController Parent { get; private set; }
		AlbumCollectionViewController albumCollectionViewController;


		public UIView HeaderView {
			get { return _headerView; }
			set {
				if (_headerView != null && _headerView != value)
					_headerView.RemoveFromSuperview ();
				_headerView = value;
				CollectionView.ReloadData ();
			}
		}

		private UIViewController _cameraPresentingView;

		BlrtAddMediaTypeOption [] _options;

		private NSString _identifer = new NSString ("nubnub, i eat candy 4444");
		private NSString _headerIdentifer = new NSString ("nbu, i eat candy 323232");


		public BlrtAddMediaTypeOption [] Options {
			get {
				return _options;
			}
			set {
				if (_options != value) {
					BlrtUtil.BetterDispose (_options);
					_options = value;
					CollectionView.ReloadData ();
				}
			}
		}


		public BlrtAddMediaTypeController (IntPtr handler) : base (handler)
		{ }

		public BlrtAddMediaTypeController(){}
		public BlrtAddMediaTypeController (IBlrtAddMediaController parent)
			: base (new BlrtAddMediaTypeControllerLayout ())
		{
			this.Parent = parent;

			View.BackgroundColor = UIColor.White;
			CollectionView.BackgroundColor = UIColor.White;
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_headerView);
			_headerView = null;
			BlrtUtil.BetterDispose (_cameraPresentingView);
			_cameraPresentingView = null;
			BlrtUtil.BetterDispose (_options);
			_options = null;
		}

		public override void LoadView ()
		{
			base.LoadView ();

			CollectionView.CollectionViewLayout = new BlrtAddMediaTypeControllerLayout (this);
			CollectionView.Delegate = new BlrtAddMediaTypeControllerLayoutDelegate (this);

			CollectionView.RegisterClassForCell (
				typeof (BlrtAddMediaTypeControllerCell),
				_identifer
			);

			CollectionView.RegisterClassForSupplementaryView (
				typeof (BlrtAddMediaTypeControllerHeader),
				UICollectionElementKindSection.Header,
				_headerIdentifer
			);

			UIDevice.CurrentDevice.BeginGeneratingDeviceOrientationNotifications ();
			NSNotificationCenter center = NSNotificationCenter.DefaultCenter;
			center.AddObserver (UIDevice.OrientationDidChangeNotification, (notify) => {
				if (_cameraPresentingView != null && _cameraPresentingView.PresentedViewController is CameraController) {

					var cameraView = _cameraPresentingView.PresentedViewController as CameraController;
					var uidevice = notify.Object as UIDevice;

					cameraView.Rotate (uidevice.Orientation);

				}
			});
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			TrackPageView ();



		}

		public void OpenCameraLink(object o, int e)
		{
			OpenCamera ().FireAndForget();
			
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			_openingDropbox = false;
		}

		public void TrackPageView ()
		{
			TrackPageView (GAName);
		}

		public void TrackPageView (string name)
		{
			BlrtData.Helpers.BlrtTrack.TrackScreenView (Parent.GAViewNamePrefix + "_MediaPicker_" + name);
		}

		public override nint NumberOfSections (UICollectionView collectionView)
		{
			return 1;
		}

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return _options == null ? 0 : _options.Length;
		}

		public override UICollectionReusableView GetViewForSupplementaryElement (UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
		{
			if (indexPath.Section == 0 && UICollectionElementKindSectionKey.Header == elementKind && HeaderView != null) {

				var view = CollectionView.DequeueReusableSupplementaryView (
					UICollectionElementKindSection.Header,
					_headerIdentifer,
					indexPath
				) as BlrtAddMediaTypeControllerHeader;


				float padding = Padding;

				if (HeaderView.Superview != view)
					HeaderView.RemoveFromSuperview ();

				HeaderView.Frame = new CGRect (padding, padding, collectionView.Bounds.Width - padding * 2, view.Bounds.Height - padding);
				view.AddSubview (HeaderView);

				return view;
			}


			return null;
		}

		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell (_identifer, indexPath) as BlrtAddMediaTypeControllerCell;

			var o = _options [indexPath.Row];
			cell.SetData (o.Image, o.Title);

			return cell;
		}


		public abstract void OptionSelected (BlrtAddMediaTypeOption option, UIView target);

		public abstract string GAName { get; }

		#region Importing media stuff

		public void OpenWebPage ()
		{
			OpenWebPage (null);
		}

		public void OpenWebPage (string url)
		{
			if (!Parent.CheckInformation (BlrtMediaFormat.Image, 0))
				return;

			var web = new BlrtWebCaptureController ();

			web.OnCancel += (object sender, EventArgs e) => {

				web.DismissViewController (true, () => {
					web.Dispose ();
					web = null;
					InvokeOnMainThread (() => {
						GC.Collect ();
					});
				});
			};

			web.OnCapture += (object sender, ICanvasPage [] e) => {
				Parent.AddMedia (e);
				Parent.DismissAddMediaController (true);
				web.DismissViewController (true, () => {
					web.Dispose ();
					web = null;
					InvokeOnMainThread (() => {
						GC.Collect ();
					});
				});
			};

			web.OnDownloadUrl += async (object sender, string e) => {
				await Parent.OpenMediaFromUrl (MediaSource.Webpage, new [] { e }, View, CreateCanvasPages);
				Parent.DismissAddMediaController (true);

				web.DismissViewController (true, () => {
					web.Dispose ();
					web = null;
					InvokeOnMainThread (() => {
						GC.Collect ();
					});
				});
			};


			if (!string.IsNullOrWhiteSpace (url))
				web.OpenUrl (url);


			UIViewController v = this;
			while (v.ParentViewController != null)
				v = v.ParentViewController;

			v.PresentViewController (web, true, null);
			TrackPageView (GAName + "_Browser");
		}

		public void OpenPasteUrl ()
		{
			if (UIPasteboard.General.String != null) {
				OpenWebPage (UIPasteboard.General.String);
			} else {
				new UIAlertView (
					BlrtHelperiOS.Translate ("alert_paste_no_url_title", "media"),
					BlrtHelperiOS.Translate ("alert_paste_no_url_message", "media"),
					null,
					BlrtHelperiOS.Translate ("alert_paste_no_url_cancel", "media")
				).Show ();
			}
		}



		public async Task OpenPasteImageAsync ()
		{
			if (!Parent.CheckInformation (BlrtMediaFormat.Image, 0)) {
				return;
			}
			if (UIPasteboard.General.Image != null) {
				var parent = Parent;
				UIImage pastBoardImg = null;
				InvokeOnMainThread (() => {
					parent.PushThinking (true);
					pastBoardImg = UIPasteboard.General.Image;
				});
				var path = await Task.Run (async () => {
					var image = await BlrtUtilities.ParseImage (pastBoardImg);
					return await BlrtUtilities.SaveImageToLocalJPG (image, "paste-url");
				});
				InvokeOnMainThread (() => {
					if (!string.IsNullOrEmpty (path)) {
						Parent.AddMedia (
							new BlrtImagePage (path)
						);
						Parent.DismissAddMediaController (true);
					}
					parent.PopThinking (true);
				});
			} else {
				new UIAlertView (
					BlrtHelperiOS.Translate ("alert_paste_no_image_title", "media"),
					BlrtHelperiOS.Translate ("alert_paste_no_image_message", "media"),
					null,
					BlrtHelperiOS.Translate ("alert_paste_no_image_cancel", "media")
				).Show ();
			}
		}


		public void OpenUrlDownload ()
		{
			var urlDownloader = new BlrtUrlDownloadViewController ();

			urlDownloader.ImageSelected += async (object sender, UIImage e) => {
				using (var image = await BlrtUtilities.ParseImage (e)) {
					var path = await BlrtUtilities.SaveImageToLocalJPG (image, "paste-url");
					if (!string.IsNullOrEmpty (path)) {
						Parent.AddMedia (
							new BlrtImagePage (path)
						).FireAndForget ();
						Parent.DismissAddMediaController (true);
					}
				}
			};
			urlDownloader.LinkSelected += (object s, string url) => {
				OpenWebPage (url);
			};

			TrackPageView (GAName + "_URL");

			NavigationController.PushViewController (urlDownloader, true);
		}

		protected bool _openingDropbox;
		public void OpenDropbox ()
		{
			_openingDropbox = true;
			bool changed = true;
			UIViewController controller = this;

			while (changed) {
				changed = false;

				if (controller.ParentViewController != null) {
					controller = controller.ParentViewController;
					changed = true;
					continue;
				}
			}

			DBChooser.DefaultChooser.OpenChooser (DBChooserLinkType.Direct, controller, (results) => {
				if (!_openingDropbox)
					return;
				_openingDropbox = false;
				// results is null if the user cancels
				if (results == null || results.Length == 0) {

				} else {
					var result = results [0];
					Parent.OpenMediaFromUrl (MediaSource.DropBox, new [] { result.Link.AbsoluteString }, View, CreateCanvasPages);
					Parent.DismissAddMediaController (true);
				}
			});
		}



		public void OpenOpenIn ()
		{
			new UIAlertView (
				BlrtHelperiOS.Translate ("source_openin_title", "media"),
				BlrtHelperiOS.Translate ("source_openin_message", "media"),
				null,
				BlrtHelperiOS.Translate ("source_openin_accept", "media")
			).Show ();
		}

		public async Task CreateCanvasPages (BlrtMediaUrlManager mediaUrlManager)
		{
			try {
				InvokeOnMainThread (() => { Parent.DismissAddMediaController (true); });
				var media = await mediaUrlManager.PopMedia ();
				await Parent.AddMedia (media);

			} catch (Exception e) { }
		}

		public async Task OpenGalleryAsync ()
		{
			if (!Parent.CheckInformation (BlrtMediaFormat.Image, 0)) {
				return;
			}
			albumCollectionViewController = new AlbumCollectionViewController ();
			albumCollectionViewController.OnCameraTappedInAlbums += OpenCameraLink;

			var parent = Parent;
			var gallery = ImagePickerViewController.Instance (albumCollectionViewController);
			gallery.MaximumImagesCount = Parent.MediaCountLeft (BlrtMediaFormat.Image);
			gallery.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;
			gallery.CanTakeCameraShot = false;



			UIViewController v = this;
			while (v.ParentViewController != null) {
				v = v.ParentViewController;
			}

			v.PresentViewController (gallery, true, null);



			List<PhotoGalleryAssetResult> images = null;
			try {
				images = await albumCollectionViewController.Completion as List<PhotoGalleryAssetResult>;
			} catch (Exception ex) {
				BlrtUtil.Debug.ReportException (ex, new Dictionary<string, string> { { "message", "exception from ELCImagePickerViewController" } });
			} finally {
				this.InvokeOnMainThread (() => {
					gallery.DismissViewController (true, null);
				});
			}

			if (null != images) {
				await LoadImages (images, UIImagePickerControllerSourceType.PhotoLibrary.ToString ());
			}
		}



		public async Task OpenCamera ()
		{
			if (!Parent.CheckInformation (BlrtMediaFormat.Image, 0))
				return;

			try {

				var _camera = CameraController.Camera;
				_camera.ImagePickerControllerDelegate = new CameraControllerDelegate (_camera);

				UIViewController v = this;
				while (v.ParentViewController != null)
					v = v.ParentViewController;

				_cameraPresentingView = v;
				UIView.AnimationsEnabled = false;
				v.PresentViewController (_camera, true, null);
				UIView.AnimationsEnabled = true;

				(_camera.ImagePickerControllerDelegate as CameraControllerDelegate).Completion.ContinueWith ((arg) => {
					if (!arg.IsCanceled || !arg.IsFaulted) {
						LoadImage (arg.Result.Item1, arg.Result.Item2).FireAndForget ();
					}
				}).FireAndForget ();

			} catch {

			}
		}

		async Task LoadImage (UIImage image, string source)
		{
			InvokeOnMainThread (() => {
				Parent.PushThinking (true);
			});

			string path = await BlrtUtilities.CompressedImagePath (image, source);
			image.Dispose ();
			image = null;
			if (!string.IsNullOrEmpty (path)) {
				InvokeOnMainThread (async () => {
					await Parent.AddMedia (
						new BlrtImagePage (path)
					);
					Parent.DismissAddMediaController (true);
				});
			}

			InvokeOnMainThread (() => {
				Parent.PopThinking (true);
			});
		}

		public async Task LoadImages (IEnumerable<IAsset> images, string source)
		{
			this.InvokeOnMainThread (() => {
				Parent.PushThinking (true);
			});

			var imagePageList = new List<ICanvasPage> ();
			bool oversized = false;


			foreach (var asset in images) {
				//order matters
				if (!oversized && (NMath.Max (asset.Width, asset.Height) > BlrtPermissions.MaxImageResolution)) {
					oversized = true;
				}
				imagePageList.AddRange (await asset.CanvasPageList ());
			}

			if (oversized) {
				await BlrtHelperiOS.AwaitAlert (
					BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxImageResolution)
				);
			}

			if (imagePageList.Count > 0) {
				this.InvokeOnMainThread (() => {
					Parent.AddMedia (imagePageList);
					Parent.DismissAddMediaController (true);
				});
			}
			this.InvokeOnMainThread (() => {
				Parent.PopThinking (true);
			});
		}

		public async Task OpenUnusedConversationImagePicker ()
		{
			if (!Parent.CheckInformation (BlrtMediaFormat.Image, 0)) {
				return;
			}

			await OpenMediaPicker (BlrtMediaFormat.Image);
		}

		public async Task OpenMediaPicker (BlrtMediaFormat requestedMediaFormat)
		{
			var mediaAssets = GetMediaAssets (requestedMediaFormat);
			var title = requestedMediaFormat == BlrtMediaFormat.PDF ? "Conversation PDFs" : "Conversation images";
			var picker = new ImagePickerForMediaAssets (mediaAssets, title, requestedMediaFormat);
			////	var gallery = ImagePickerViewController.Instance (picker);
			//var gallery = new AlbumPhotosController ();
			////gallery.MaximumImagesCount = Parent.MediaCountLeft (requestedMediaFormat);
			////gallery.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;

			var gallery = ImagePickerViewController.Instance (picker);
			gallery.MaximumImagesCount = Parent.MediaCountLeft (BlrtMediaFormat.Image);
		//	gallery.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;







			UIViewController v = this;
			while (v.ParentViewController != null) {
				v = v.ParentViewController;
			}
			v.PresentViewController (gallery, true, null);

			List<MediaAssetResult> media = null;
			try {
				media = await gallery.Completion as List<MediaAssetResult>;

			} catch (Exception ex) {
				BlrtUtil.Debug.ReportException (ex, new Dictionary<string, string> { { "message", "exception from ELCImagePickerViewController" } });
			} finally {
				InvokeOnMainThread (() => {
					gallery.DismissViewController (true, null);
				});
			}
			var mediaWithNoDuplicates = media.Distinct ().ToList ();

			bool success = false;
			using (UserDialogs.Instance.Loading ("Downloading...")) {

				success = await DownloadDecryptData (mediaWithNoDuplicates);
			
			}

			if (null != media && success) {
				await LoadImages (mediaWithNoDuplicates, UIImagePickerControllerSourceType.PhotoLibrary.ToString ());
			}
		}

		IEnumerable<MediaAsset> GetMediaAssets (BlrtMediaFormat requestedMediaFormat)
		{
			var conversation = BlrtManager.DataManagers.Default.GetConversation (Parent.ConversationId);
			return BlrtUtil.GetMediaAssets (conversation, conversation.UnusedConversationMediaItems.ToArray ())
							 .Where (a => a.Format == requestedMediaFormat);
		}

		#endregion

		async Task<bool> DownloadDecryptData (List<MediaAssetResult> images)
		{
			var success = true;
			foreach (var m in images) {
				var asset = m.Asset;

				if (!asset.IsDataLocal ()) {
					var downloader = BlrtDownloader.Download (1000, asset.CloudMediaPath, asset.LocalMediaPath);
					await downloader.WaitAsync ();
					if (asset.IsDataLocal ()) {
						success = await DecryptData (asset);
					} else {
						success = false;
					}
				}

				success = await DecryptData (asset);

				if (!success) {
					return false;
				}
			}
			return success;
		}

		async Task<bool> DecryptData (MediaAsset asset)
		{
			if (asset.IsDecypted) return true;

			try {
				await asset.DecyptData ();
			} catch (BlrtEncryptorManagerException e3) {
				return false;
			} catch (InvalidOperationException e4) {
				return false;
			}
			return true;
		}
	}

}