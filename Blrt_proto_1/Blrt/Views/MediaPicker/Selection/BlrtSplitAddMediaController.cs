using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

using BlrtCanvas;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtSplitAddMediaController : UIViewController, IBlrtAddMediaController
	{
		public string GAViewNamePrefix { get{ return _parent.GAViewNamePrefix;} }


		IBlrtAddMediaController _parent;
		BlrtAddMediaTypeSelectController _selectController;
		UINavigationController _selectNavigationController;

		BlrtAddMediaTypeController _selectedMediaType;

		UIViewController master;
		UIViewController slave;

		public IConversationScreen ConversationScreen {	get {	return _parent.ConversationScreen;	}	}

		public string ConversationId {
			get {
				return _parent.ConversationId;
			}
		}

		public BlrtSplitAddMediaController (IBlrtAddMediaController parent) : base ()
		{
			_parent = parent;

			_selectController = new BlrtAddMediaTypeSelectController (this);
			_selectNavigationController = new UINavigationController (_selectController);

		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_selectController);
			_selectController = null;
			BlrtUtil.BetterDispose (_selectNavigationController);
			_selectNavigationController = null;
			BlrtUtil.BetterDispose (_selectedMediaType);
			_selectedMediaType = null;
			BlrtUtil.BetterDispose (master);
			master = null;
			BlrtUtil.BetterDispose (slave);
			slave = null;
		}


		public void OpenWebPage(string url)
		{
			_selectController.SelectAddMediaType (BlrtAddMediaType.WebPage);

			if (slave is UINavigationController) {	
				var nav = slave as UINavigationController;
				if (nav.VisibleViewController is BlrtAddMediaTypeController) {			
					var controller = nav.VisibleViewController as BlrtAddMediaTypeController;
					controller.OpenWebPage (url);			
				}
			}
		}


		public override void LoadView ()
		{
			base.LoadView ();

			master = _selectNavigationController;
			AddChildViewController (master);
			View.AddSubview (master.View);

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (_selectedMediaType == null) {
				ShowAddMediaTypeController (BlrtAddMediaType.Image, true);
				_selectController.TableView.SelectRow (NSIndexPath.FromRowSection (0, 0), false, UITableViewScrollPosition.None);
			}

			LayoutMasterSlave ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			LayoutMasterSlave ();
		}


		public void ShowAddMediaTypeController (BlrtAddMediaType type, bool animated)
		{
			_selectedMediaType = CreateController (type, this);

			SetSlaveView (new UINavigationController(_selectedMediaType));
		}

		public static BlrtAddMediaTypeController CreateController(BlrtAddMediaType type, IBlrtAddMediaController parent){
		
			BlrtAddMediaTypeController controller = null;

			switch (type) {
				case BlrtAddMediaType.Image:
					controller = new BlrtAddMediaImageController(parent);
					break;
				case BlrtAddMediaType.PDF:
					controller = new BlrtAddMediaPDFController(parent);
					break;
				case BlrtAddMediaType.Template:
					controller = new BlrtTemplatePicker(parent);
					break;
				case BlrtAddMediaType.WebPage:
					controller = new BlrtAddMediaWebpageController(parent);
					break;
			}

			LLogger.WriteEvent("Media Source Collection", "openned", null, type.ToString());

			if (controller == null)
				controller = null;
		
			return controller;
		}

		public Task AddMedia (ICanvasPage media)
		{
			return _parent.AddMedia (media);
		}

		public Task AddMedia (IEnumerable<ICanvasPage> media)
		{
			return _parent.AddMedia (media);
		}

		public Task AddMedia (ICanvasPage media, bool hidden)
		{
			return _parent.AddMedia (media, hidden);
		}

		public Task AddMedia (IEnumerable<ICanvasPage> media, bool hidden)
		{
			return _parent.AddMedia (media, hidden);
		}

		public void ShowAddMediaController (bool animated)
		{
			_parent.ShowAddMediaController (animated);
		}

		public void DismissAddMediaController (bool animated)
		{
			_parent.DismissAddMediaController (animated);
		}

		public int MediaCountLeft(BlrtMediaFormat format){
			return _parent.MediaCountLeft (format);
		}

		public bool CheckInformation (BlrtMediaFormat format, long size)
		{
			return _parent.CheckInformation (format, size);
		}

		public void PreviewPage (int page)
		{
			_parent.PreviewPage (page);
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
		}



		public Task OpenMediaFromUrl (MediaSource source, string[] url, UIView view, BlrtAddMediaTypes.MediaPickedCallback mediaPickedCallback)
		{
			return _parent.OpenMediaFromUrl (source, url, view, mediaPickedCallback);
		}


		public void PushThinking (bool animate)
		{
			_parent.PushThinking (animate);
		}

		public void PopThinking (bool animate)
		{
			_parent.PopThinking (animate);
		}

		void SetSlaveView (UIViewController newSlave)
		{
			if (newSlave != slave) {
			
				if (slave != null) {
					slave.View.RemoveFromSuperview ();
					slave.RemoveFromParentViewController ();
				}
			
				slave = newSlave;
				AddChildViewController (newSlave);
				View.AddSubview (newSlave.View);
			}

			LayoutMasterSlave ();
		}

		private void LayoutMasterSlave()
		{

			if (master == null || slave == null)
				return;


			nfloat delta = 0;//slave.View.Frame.Left - master.View.Frame.Right;

			nfloat masterWidth = 210;
			nfloat slaveWidth = View.Bounds.Width - masterWidth - delta;

			master.View.Frame = new CGRect (0, 0, masterWidth, View.Bounds.Height);
			slave.View.Frame = new CGRect (masterWidth + delta, 0, slaveWidth, View.Bounds.Height);

			master.View.SetNeedsLayout ();
			slave.View.SetNeedsLayout ();
		}
			
	}
}

