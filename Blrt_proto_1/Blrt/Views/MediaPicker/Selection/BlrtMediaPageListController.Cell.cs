using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public partial class BlrtAddMediaTypeController
	{
		public static readonly float Padding = BlrtHelperiOS.IsPhone ? 10 : 20;


		private class BlrtAddMediaTypeControllerLayout : UICollectionViewFlowLayout
		{

			public BlrtAddMediaTypeControllerLayout () : this (null)
			{
			}

			public BlrtAddMediaTypeControllerLayout (BlrtAddMediaTypeController parent) : base ()
			{
				float Margin = 2;

				this.ItemSize = new CGSize (BlrtAddMediaTypeControllerCell.CellWidth, BlrtAddMediaTypeControllerCell.CellHeight);
				this.SectionInset = new UIEdgeInsets (Padding, Padding, Padding, Padding);

				this.MinimumInteritemSpacing = Margin;
				this.MinimumLineSpacing = Padding;
			}

		}


		private class BlrtAddMediaTypeControllerLayoutDelegate : UICollectionViewDelegateFlowLayout
		{
			BlrtAddMediaTypeController _parent;

			public BlrtAddMediaTypeControllerLayoutDelegate () : this (null)
			{
			}

			public BlrtAddMediaTypeControllerLayoutDelegate (BlrtAddMediaTypeController parent) : base ()
			{
				this._parent = parent;
			}

			public override CGSize GetReferenceSizeForHeader (UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (section == 0 && _parent != null && _parent.HeaderView != null) {
					return new CGSize (1, _parent.HeaderView.Bounds.Height + Padding);
				}
				return CGSize.Empty;
			}

			public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
			{
				collectionView.DeselectItem (indexPath, false);
				_parent.OptionSelected (_parent._options [indexPath.Row], collectionView.CellForItem (indexPath));
			}
		}


		private class BlrtAddMediaTypeControllerCell : UICollectionViewCell
		{
			public static readonly int ImageSize = BlrtHelperiOS.IsPhone ? 94 : 110;
			public static readonly int LabelHeight = 36;
			public static readonly int Gap = 2;

			public static readonly float CellWidth = ImageSize;
			public static readonly float CellHeight = ImageSize + LabelHeight + Gap;


			private UIImageView _imageView;
			private UILabel _label;

			public BlrtAddMediaTypeControllerCell ()
				: base (new CGRect (0, 0, CellWidth, CellHeight))
			{
				InitThis ();
			}

			public BlrtAddMediaTypeControllerCell (IntPtr handle)
				: base (handle)
			{
				InitThis ();
			}

			void InitThis ()
			{
				_imageView = new UIImageView (new CGRect (0, 0, ImageSize, ImageSize)) {
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
				};
				ContentView.AddSubview (_imageView);


				_label = new UILabel (new CGRect (0, ImageSize, ImageSize, LabelHeight)) {
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					Lines = 2,
					TextAlignment = UITextAlignment.Center,
					Font = BlrtConfig.NormalFont.Normal,
					TextColor = BlrtConfig.BLRT_BLUE
				};
				ContentView.AddSubview (_label);
			}

			public void SetData (UIImage image, string title)
			{
				_imageView.BackgroundColor = BlrtConfig.BLRT_MID_GRAY;
				_imageView.Image = image;

				_label.Text = title;

				var s = _label.SizeThatFits (new CGSize (ImageSize, nfloat.MaxValue));
				_label.Frame = new CGRect (0, Gap + ImageSize, CellWidth, NMath.Min (s.Height, LabelHeight));
			}
		}

		private class BlrtAddMediaTypeControllerHeader : UICollectionReusableView
		{
			public BlrtAddMediaTypeControllerHeader (IntPtr handle) : base (handle)
			{
				InitThis ();
			}

			void InitThis ()
			{

			}

		}
	}
}

