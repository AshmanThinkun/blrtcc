using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtAddMediaTypeSelectController : UITableViewController
	{
		BlrtAddMediaType[] _typeList;

		IBlrtAddMediaController _parent; 

		public BlrtAddMediaTypeSelectController (IBlrtAddMediaController parent) : base (UITableViewStyle.Plain)
		{ 
			Title = BlrtHelperiOS.Translate ("select_type_title", "media");
			_parent = parent;
			_typeList = new BlrtAddMediaType[] {

				BlrtAddMediaType.Image,
				BlrtAddMediaType.PDF,

				BlrtAddMediaType.WebPage,

				BlrtAddMediaType.Template,
			};
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			var name = _parent.GAViewNamePrefix + "_MediaPicker";
			BlrtData.Helpers.BlrtTrack.TrackScreenView (name);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			if (IsMovingFromParentViewController || IsBeingDismissed) {
				InvokeOnMainThread (() => {
					_parent.DismissAddMediaController (true);
				});
			}
		}

		public void SelectAddMediaType (BlrtAddMediaType mediatype)
		{
			int index = -1;

			for(int  i = 0; i < _typeList.Length; ++i){
				if(_typeList[i] == mediatype)
					index = i;
			}

			if (index >= 0) {
				TableView.SelectRow(NSIndexPath.FromRowSection(index, 0), false, UITableViewScrollPosition.Middle);
			}
		}

		public override void LoadView ()
		{
			base.LoadView ();

			TableView.BackgroundColor = BlrtConfig.BLRT_DARK_GRAY;
			TableView.SeparatorColor = BlrtConfig.BLRT_DARK_GRAY;
			TableView.TableFooterView = new UIView (new CGRect (0, 0, 1, 1));

			ClearsSelectionOnViewWillAppear = BlrtHelperiOS.IsPhone;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}


		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _typeList.Length;
		}


		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 60;
		}


		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.CellAt (indexPath) as AddMediaCell;

			if (cell != null)
				cell.Reload ();

			_parent.ShowAddMediaTypeController (_typeList [indexPath.Row % _typeList.Length], true);
		}


		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.CellAt (indexPath) as AddMediaCell;

			if (cell != null)
				cell.Reload ();
		}


		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath) {
			var cell = tableView.DequeueReusableCell (AddMediaCell.Identifer) as AddMediaCell;

			if (cell == null) {
				cell = new AddMediaCell ();
			}


			cell.Type = _typeList[indexPath.Row % _typeList.Length];
			cell.Reload ();

			return cell;
		}



		private class AddMediaCell : UITableViewCell {
		
			public const string Identifer = "wookawookaPACMAN";

			BlrtAddMediaType _type;

			UIView _seperatorLeftView;

			public BlrtAddMediaType Type {
				get {
					return _type;
				}
				set {
					if (_type != value) {
						SetAddMediaType (value);
					}
				}
			}


			public AddMediaCell () : base(UITableViewCellStyle.Default, Identifer) {

				SelectionStyle = UITableViewCellSelectionStyle.None;

				_seperatorLeftView = new UIView(new CGRect(Bounds.Width - 1,0,1,Bounds.Height)){
					AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleHeight,
					BackgroundColor = BlrtConfig.BLRT_DARK_GRAY
				};
				AddSubview(_seperatorLeftView);

				SetAddMediaType(BlrtAddMediaType.Image);
			}

			public override void SetSelected (bool selected, bool animated)
			{
				base.SetSelected (selected, animated);
				Reload ();
			}

			public override void SetHighlighted (bool highlighted, bool animated)
			{
				base.SetHighlighted (highlighted, animated);
				Reload ();
			}

			public void Reload(){

				BackgroundColor = Selected ? UIColor.White : BlrtConfig.BLRT_MID_GRAY;
				_seperatorLeftView.Hidden = Selected || BlrtHelperiOS.IsPhone;
				SetNeedsLayout ();
			}
		
			void SetAddMediaType (BlrtAddMediaType type)
			{
				_type = type;

				ImageView.TintColor = BlrtConfig.BLRT_BLUE;

				switch (type) {
					case BlrtAddMediaType.Image:
						ImageView.Image = BlrtHelperiOS.FromBundleTemplate("mediapicker/nav-image.png");
						TextLabel.Text = BlrtHelperiOS.Translate ("select_type_image", "media");
						break;
					case BlrtAddMediaType.PDF:
						ImageView.Image = BlrtHelperiOS.FromBundleTemplate("mediapicker/nav-pdf.png");
						TextLabel.Text = BlrtHelperiOS.Translate ("select_type_pdf", "media");
						break;
					case BlrtAddMediaType.Template:
						ImageView.Image = BlrtHelperiOS.FromBundleTemplate("mediapicker/nav-template.png");
						TextLabel.Text = BlrtHelperiOS.Translate ("select_type_template", "media");
						break;
					case BlrtAddMediaType.WebPage:
						ImageView.Image = BlrtHelperiOS.FromBundleTemplate("mediapicker/nav-website.png");
						TextLabel.Text = BlrtHelperiOS.Translate ("select_type_website", "media");
						break;
					default:
						ImageView.Image = null;
						TextLabel.Text = type.ToString ();
						break;
				}

				Reload ();
			}
		}
	}

	public enum BlrtAddMediaType {

		None,

		Image,
		PDF,

		Template,
		WebPage,
	}

	public enum MediaSource {
		iCloudDrive,

		DocumentPicker,

		OpenIn,

		ImageGallery,
		Camera,
		Url,
		PasteImage,
		PasteUrl,
		DropBox,

		Webpage,
		ConversationImage,
		ConversationPDF,

		MediaSharing
	}
}

