﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlrtData;
using CoreGraphics;
using ImagePickers.ImageAssets;
using Foundation;
using UIKit;
using BlrtiOS;
using BlrtiOS.Views.FullScreenViewers;

namespace ImagePickers
{
	public abstract class BaseImagePicker : UITableViewController
	{
		protected int Columns = 4;
		WeakReference _Parent;
		float _imageThumbnailSize;
		public bool SingleSelection { get; set; }

		public bool ImmediateReturn { get; set; }

		bool PreviewerOnDisplay { get; set; }
		BlrtMediaFormat RequestedMediaFormat { get; set; }

		public ImagePickerViewController Parent {
			get {
				return _Parent == null ? null : _Parent.Target as ImagePickerViewController;
			}
			set {
				_Parent = new WeakReference (value);
			}
		}

		protected float ImageThumbnailSize {
			get {
				if (_imageThumbnailSize <= 0) {
					var width = Math.Min (BlrtUtil.PlatformHelper.GetDeviceWidth (), BlrtUtil.PlatformHelper.GetDeviceHeight ());
					if (BlrtHelperiOS.IsPhone) {
						_imageThumbnailSize = (width - 3.0f) / 4.0f;
					} else {
						_imageThumbnailSize = (width - 7.0f) / 8.0f;
					}
				}
				return _imageThumbnailSize;
			}
		}

		protected List<BaseImageAsset> Assets = new List<BaseImageAsset> (); // List of images

		UILabel TotalImagesSelectedView;
		UILabel MaxImageAlertView;
		public int TotalSelectedAssets;
		readonly int TEXT_VIEW_HEIGHT = ImagePreviewController.TOP_BAR_HEIGHT;

		protected UIBarButtonItem doneButtonItem;

		public override void ViewDidLoad ()
		{
			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.ContentInset = new UIEdgeInsets (0, 0, TEXT_VIEW_HEIGHT, 0);
			TableView.AllowsSelection = false;
			PreviewerOnDisplay = false;
			if (ImmediateReturn) {

			} else {
				doneButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done);
				NavigationItem.RightBarButtonItem = doneButtonItem;
			}

			AddSubviews ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			Columns = GetNumberOfColumns ();
			TotalImagesSelectedView.Hidden = false;
			TotalImagesSelectedView.Hidden = false;
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			var TableHeight = Parent.View.Bounds.Bottom;
			TableView.Frame = new CGRect (0, 0, View.Bounds.Width, TableHeight);
			SetInfoBarsFrames ();
		}

		void SetInfoBarsFrames ()
		{
			var NavBarHeight = 0.0;
			var nullanbleNavBarHeight = NavigationController?.NavigationBar?.Frame.Size.Height;
			if (!nullanbleNavBarHeight.HasValue) {
				return;
			}

			NavBarHeight = nullanbleNavBarHeight.Value;
			var StatusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Size.Height;

			TotalImagesSelectedView.Frame = new CGRect (0,
														TableView.Frame.Bottom - TEXT_VIEW_HEIGHT,
														View.Bounds.Width,
														TEXT_VIEW_HEIGHT);

			MaxImageAlertView.Frame = new CGRect (0,
												  StatusBarHeight +
												  NavBarHeight +
												  (PreviewerOnDisplay ? ImagePreviewController.TOP_BAR_HEIGHT : 0),
												  View.Bounds.Width,
												  TEXT_VIEW_HEIGHT);
		}

		void AddSubviews ()
		{
			TotalImagesSelectedView = new UILabel () {
				TextAlignment = UITextAlignment.Center,
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				BackgroundColor = BlrtStyles.iOS.Gray3,
				Alpha = 0
			};

			MaxImageAlertView = new UILabel () {
				TextAlignment = UITextAlignment.Center,
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				Text = string.Format ("Maximum {0} items please", Parent.MaximumImagesCount),
				TextColor = BlrtStyles.iOS.Green,
				BackgroundColor = BlrtStyles.iOS.Charcoal,
				Alpha = 0
			};

			Parent.View.AddSubviews (new UIView () { TotalImagesSelectedView, MaxImageAlertView });
		}

		protected void DisplayNumberOfSelectedImages ()
		{
			if (TotalSelectedAssets <= 1) {
				UIView.Animate (ANIMATION_DURATION, delegate {
					TotalImagesSelectedView.Text = string.Format ("{0} items selected", TotalSelectedAssets);
					TotalImagesSelectedView.Alpha = TotalSelectedAssets > 0 ? 0.90f : 0.0f;
				});
			} else {
				TotalImagesSelectedView.Text = string.Format ("{0} items selected", TotalSelectedAssets);
			}
		}

		int GetNumberOfColumns ()
		{
			return (int)((View.Bounds.Size.Width) / (ImageThumbnailSize));
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (IsMovingFromParentViewController) {
				TotalImagesSelectedView.RemoveFromSuperview ();
				TotalImagesSelectedView.Dispose ();
				MaxImageAlertView.RemoveFromSuperview ();
				MaxImageAlertView.Dispose ();
			}
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);
			Columns = GetNumberOfColumns ();
			TableView.ReloadData ();
		}

		bool ShouldSelectAsset (int previousCount)
		{
			return Parent.MaximumImagesCount <= 0 || previousCount < Parent.MaximumImagesCount;
		}

		/**
		 * Decides whether user can select the image the user just tapped on
		*/
		public bool ShouldSelectAsset ()
		{
			int selectionCount = TotalSelectedAssets;
			bool shouldSelect = true;

			var parent = Parent;
			if (parent != null) {
				shouldSelect = ShouldSelectAsset (selectionCount);
			}

			if (!shouldSelect) {
				DisplayAlertView ().FireAndForget ();
			}
			return shouldSelect;
		}

		const int ALERT_DELAY_TIME = 5000;
		protected async Task DisplayAlertView ()
		{
			DisplayAlertView (0.9f); //Make visible	
			await Task.Delay (ALERT_DELAY_TIME);
			DisplayAlertView (0); //Make invisible
		}

		readonly float ANIMATION_DURATION = 0.3f;

		void DisplayAlertView (float alpha)
		{
			UIView.Animate (ANIMATION_DURATION, delegate {
				MaxImageAlertView.Alpha = alpha;
			});
		}

		public virtual void AssetSelected (BaseImageAsset asset, bool selected)
		{
			TotalSelectedAssets += (selected) ? 1 : -1;
			DisplayNumberOfSelectedImages ();
			View.SetNeedsDisplay ();

			if (SingleSelection) {
				foreach (var anAsset in Assets) {
					if (asset != anAsset) {
						anAsset.Selected = false;
					}
				}
			}
			//if (ImmediateReturn) {
			//	var parent = Parent;
			//	var obj = new List<ALAsset> (1);
			//	obj.Add (asset.Asset);
			//	parent.SelectedAssets (obj);
			//}
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			if (Columns <= 0)
				return 4;
			int numRows = (int)Math.Ceiling ((float)Assets.Count / Columns);
			return numRows;
		}

		List<BaseImageAsset> AssetsForIndexPath (NSIndexPath path)
		{
			int index = path.Row * Columns;
			int length = Math.Min (Columns, Assets.Count - index);
			return Assets.GetRange (index, length);
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			const string cellIdentifier = "Cell";

			var cell = TableView.DequeueReusableCell (cellIdentifier) as ImageCell;
			if (cell == null) {
				cell = new ImageCell (UITableViewCellStyle.Default, cellIdentifier);
			}
			cell.SetAssets (AssetsForIndexPath (indexPath), Columns, ImageThumbnailSize);

			return cell;
		}

		protected ImagePreviewController _previewer;
		bool _openingPreview;

		protected void OpenPreview (object sender, int index, EventHandler OnDoneButtonPressed)
		{
			if (_openingPreview) //some very large images will take a bit of time to load, we don't want this function to be called during this period of time
				return;

			_openingPreview = true;

			var cell = sender as ImageCell;

			if (RequestedMediaFormat == BlrtMediaFormat.Image) {
				ShowImageViewer (cell, OnDoneButtonPressed, index);
			}

			if (RequestedMediaFormat == BlrtMediaFormat.PDF) {
				ShowPDFViewer (cell, OnDoneButtonPressed, index);
			}

			_openingPreview = false;
		}

		void ShowPDFViewer (ImageCell cell, EventHandler onDoneButtonPressed, int index)
		{
			var baseAsset = cell.RowAssets.ElementAt (index);
			var asset = (PhotoAssetForMediaAsset)baseAsset;
			var previewer = new PDFPreviewController (asset.Asset, false, false);
			NavigationController.PushViewController (previewer, true);
		}



		void ShowImageViewer (ImageCell cell, EventHandler OnDoneButtonPressed, int index)
		{
			List<BaseImageAsset> selectedAssets = new List<BaseImageAsset> ();
			bool selectionStatus  =  false;
			if(cell.Selected==true)
			{
				foreach(var asset in Assets)
				{
					if(asset.Index == index )
					{
						selectedAssets.Add (asset);
						selectionStatus = true;
					}
				}
			}
			if (_previewer == null) {
				_previewer = new ImagePreviewController (Assets ,selectedAssets,cell.RowAssets.ElementAt (index).Index ,true,selectionStatus); // index is zero based
				_previewer.DoneButtonItem.Clicked += OnDoneButtonPressed;
				_previewer.DoneButtonClicked += OnDoneButtonPressed;

				_previewer.PreviewWillRotate += delegate {
					var NavBarHeight = NavigationController.NavigationBar.Frame.Size.Height;
					var StatusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Size.Height;

					TotalImagesSelectedView.Frame = new CGRect (0,
																_previewer.View.Frame.Bottom - TEXT_VIEW_HEIGHT,
																_previewer.View.Bounds.Width,
																TEXT_VIEW_HEIGHT);
					MaxImageAlertView.Frame = new CGRect (0,
											  StatusBarHeight +
											  NavBarHeight +
											  (PreviewerOnDisplay ? ImagePreviewController.TOP_BAR_HEIGHT : 0),
											  _previewer.View.Bounds.Width,
											  TEXT_VIEW_HEIGHT);
				};
				_previewer.PreviewClosed += delegate {
					PreviewerOnDisplay = false;
					TableView.ReloadData ();
				};
			} else {
				_previewer.SetImage (cell.RowAssets.ElementAt (index).Index + 1);
			}

			_previewer.RemainingImageCount = Parent.MaximumImagesCount - TotalSelectedAssets;
			NavigationController.PushViewController (_previewer, true);
		
			PreviewerOnDisplay = true;
		}

		float VERTICAL_PADDING = 2;

		public BaseImagePicker (BlrtMediaFormat requestedMediaFormat = BlrtMediaFormat.Image)
		{
			RequestedMediaFormat = requestedMediaFormat;
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return (ImageThumbnailSize + VERTICAL_PADDING);
		}
	}
}
