﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ImagePickers.ImageAssets;
using Foundation;
using UIKit;
using BlrtData;
using System.Linq;

namespace ImagePickers
{
	/**
	* Represents table that lists all the media assets provided
	*/
	public class ImagePickerForMediaAssets : BaseImagePicker
	{
		readonly IEnumerable<MediaAsset> MediaAssets;

		string PageTitle { get; set; }

		public ImagePickerForMediaAssets (IEnumerable<MediaAsset> mediaAssets, string pageTitle, BlrtMediaFormat requestedMediaFormat) : base (requestedMediaFormat)
		{
			PageTitle = pageTitle;
			MediaAssets = mediaAssets;
		}

		public ImagePickerForMediaAssets(){}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = base.GetCell (tableView, indexPath) as ImageCell;
			cell.OnPreviewTapped -= OpenPreview;
			cell.OnPreviewTapped += OpenPreview;

			return cell;
		}

		public void OpenPreview (object sender, int e)
		{
			OpenPreview (sender, e, DoneClicked);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			NavigationItem.Title = PageTitle;
			doneButtonItem.Clicked += DoneClicked;
			Task.Run ((Action)PreparePhotos); // Prepare photos to be displayed
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			doneButtonItem.Clicked += DoneClicked;
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (IsMovingFromParentViewController || IsBeingDismissed) {
				NavigationItem.RightBarButtonItem.Clicked -= DoneClicked;
			}
		}

		public void PreparePhotos ()
		{
			PopulateMediaList ();

			BeginInvokeOnMainThread (() => {
				TableView.ReloadData ();
				// scroll to bottom
				nint section = NumberOfSections (TableView) - 1;
				nint row = TableView.NumberOfRowsInSection (section) - 1;
				if (section >= 0 && row >= 0) {
					var ip = NSIndexPath.FromRowSection (row, section);
					TableView.ScrollToRow (ip, UITableViewScrollPosition.Bottom, false);
				}
			});
		}

		void PopulateMediaList ()
		{
			var count = MediaAssets.Count ();
			for (int i = 0; i < count; i++) {
				var elcAsset = new PhotoAssetForMediaAsset (this, MediaAssets.ElementAt (i), i);
				Assets.Add (elcAsset);
			}

		}

		void DoneClicked (object sender, EventArgs e)
		{ 
			var selected = new List<MediaAsset> ();

			foreach (var asset in Assets) {
				if (asset.Selected) {
					selected.Add ((asset as PhotoAssetForMediaAsset).Asset);
				}
			}

			var parent = Parent;
			if (parent != null) {
				parent.SelectedAssets (selected);
			}
		}
	}
}
