using System;
using UIKit;
using AssetsLibrary;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData;
using PhotoKitGallery;
using Photos;

namespace ImagePickers
{
	/** 
     * Presents a photo picker dialog capable of selecting multiple images at once.
     * Usage:
     * 
     * var picker = ELCImagePickerViewController.Instance;
     * picker.MaximumImagesCount = 15;
     * picker.Completion.ContinueWith (t => {
     *   if (t.IsCancelled || t.Exception != null) {
     *     // no pictures for you!
     *   } else {
     *      // t.Result is a List<AssetResult>
     *    }
     * });
     * 
     * PresentViewController (picker, true, null);
     */
	public class ImagePickerViewController : UINavigationController
	{
		public int MaximumImagesCount { get; set; }
		public bool CanTakeCameraShot { get; set; }

		readonly TaskCompletionSource<object> _TaskCompletionSource = new TaskCompletionSource<object> ();

		public Task<object> Completion {
			get {
				return _TaskCompletionSource.Task;
			}
		}

		public static ImagePickerViewController Instance (UIViewController rootController)
		{

			var picker = new ImagePickerViewController (rootController);
			picker.MaximumImagesCount = BlrtiOS.Views.Conversation.Cells.ImageCell.MAX_IMAGES;
			picker.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;
			picker.CanTakeCameraShot = true;

			//if (rootController is ImageAlbumPicker) {
			//((ImageAlbumPicker)rootController).Parent = picker;
			//	} else
			if (rootController is ImagePickerForMediaAssets) {
				((ImagePickerForMediaAssets)rootController).Parent = picker;
			}
		// else if(rootController is PhotosViewController)
		//{
		//		((PhotosViewController) rootController).Parent = picker;
		//}

			return picker;
		}

		ImagePickerViewController (UIViewController rootController) : base (rootController)
		{
			View.BackgroundColor = BlrtStyles.iOS.White;
			NavigationBar.BackgroundColor = BlrtStyles.iOS.Gray4;
		}

		
		public void SelectedAssets (List<MediaAsset> assets)
		{
			var results = new List<MediaAssetResult> (assets.Count);
			foreach (var asset in assets) {
			
				var result = new MediaAssetResult ();
				result.Asset = asset;
				results.Add (result);
			}

			_TaskCompletionSource.TrySetResult (results);	
		}

		public void SelectedAssets(List<PHAsset> assets)
		{


			var results = new List<PhotoGalleryAssetResult> (assets.Count);
			foreach (var asset in assets) {
				
					var result = new PhotoGalleryAssetResult ();
					result.pHAsset = asset;	
					
				
					results.Add (result);
				}
			

			_TaskCompletionSource.TrySetResult (results);
		}
	
		public void SelectedCameraImage (Tuple<UIImage, string> imgData)
		{
			_TaskCompletionSource.TrySetResult (imgData);
		}

		public void CancelledPicker ()
		{
			_TaskCompletionSource.TrySetCanceled ();
		}
	}
}