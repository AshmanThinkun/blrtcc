﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtCanvas;
using UIKit;

namespace BlrtiOS.Views.MediaPicker
{
	public interface IAsset
	{
		nfloat Width { get; }
		nfloat Height { get; }
		UIImageOrientation Orientation { get; }
		//CGImage Image { get; }
		Task<IEnumerable<ICanvasPage>> CanvasPageList ();
	}


}
