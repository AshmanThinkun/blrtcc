﻿using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.Views.MediaPicker;
using AssetsLibrary;
using BlrtCanvas;
using System.Collections.Generic;
using BlrtCanvas.Pages;
using BlrtiOS.Util;
using System.Threading.Tasks;
using Photos;
using PhotoKitGallery;

namespace ImagePickers
{
	public class PhotoGalleryAssetResult : IAsset
	{
		public UIImageOrientation Orientation { get; set; }
		//public ALAssetRepresentation Representation { get; set; }
		public PHAsset pHAsset;

		public nfloat Height {
			get {
				//return Representation.Dimensions.Height;
				return pHAsset.PixelHeight;
			}
		}

		public CGImage Image {


			get {
				//return Representation.GetImage ();
				//var imageCell = (ImageCellForAllPhotos)collectionView.DequeueReusableCell (cellId, indexPath);
				//PHImageManager imageMgr = new PHImageManager ();
				var options = new PHImageRequestOptions {
					Synchronous = true
				};
				var cell = new UICollectionViewCell ();
				var image = new UIImage();
				PHImageManager.DefaultManager.RequestImageForAsset (pHAsset, PHImageManager.MaximumSize, PHImageContentMode.AspectFit, options, (requestedImage, _) => {
					image = requestedImage;

				});

				return image.CGImage;
			}
			set { }
		}

		public nfloat Width {
			get {
				//return Representation.Dimensions.Width;
				return pHAsset.PixelWidth;
			}
		}


		public string Title {
			get {
				//return Asset.DefaultRepresentation.Filename;

				var resources = PHAssetResource.GetAssetResources (pHAsset);

				string fileName = resources [0].OriginalFilename;

				return fileName;


			}
		}
		public async Task<IEnumerable<ICanvasPage>> CanvasPageList ()
		{
			var t = await BlrtUtilities.GetCanvasPageForImage (Image, Orientation);
			var list =new List<ICanvasPage> { 
				 t};
			return list;
		}
	}
}