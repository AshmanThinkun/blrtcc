﻿using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.Views.MediaPicker;
using BlrtData;
using BlrtCanvas;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtiOS.Util;

namespace ImagePickers
{
	public class MediaAssetResult : IAsset
	{
		public MediaAsset Asset { get; set; }
		public UIImageOrientation Orientation {
			get {
				return ImageUi.Orientation;
			}
		}

		public nfloat Height {
			get {
				if (Asset.Format == BlrtMediaFormat.PDF){
					return 0; // pdfs don't really have lengths. 
					// We don't have to worry about pdf image size as we ourselves are creating images for pdf as 
					// we create the images of correct sizes when we need.
				}
				return Image.Height;
			}
		}

		UIImage _image;
		UIImage ImageUi {
			get {
				if (_image == null) {
					_image = UIImage.FromFile (Asset.DecyptedPath);
				}
				return _image;
			}
		}
		public CGImage Image {
			get {
				return ImageUi.CGImage;
			}
		}

		public nfloat Width {
			get {
				if (Asset.Format == BlrtMediaFormat.PDF){
					return 0; // pdfs don't really have lengths. 
					// We don't have to worry about pdf image size as we ourselves are creating images for pdf as 
					// we create the images of correct sizes when we need.
				}
				return Image.Width;
			}
		}

		public async Task<IEnumerable<ICanvasPage>> CanvasPageList () {

			if (Asset.Format == BlrtMediaFormat.Image)
			{
				return new List<ICanvasPage> { await BlrtUtilities.GetCanvasPageForImage (Image, Orientation) };
			}

			if (Asset.Format == BlrtMediaFormat.PDF)
			{
				return BlrtUtilities.GetPDFCanvasPages (Asset.DecyptedPath);
			}

			return null;
		}
	}
}