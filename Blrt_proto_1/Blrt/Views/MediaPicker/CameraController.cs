﻿using System;
using System.Threading.Tasks;
using BlrtCanvas.Pages;
using BlrtData;
using BlrtData.ExecutionPipeline;
using BlrtiOS.Util;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.MediaPicker
{
	public class CameraController
			: UIImagePickerController
	{
		static CameraController _camera;
		UIDeviceOrientation _initOrientation;

		public static CameraController Camera {
			get {
				_camera = new CameraController ();
				return _camera;
			}
		}

		CameraController ()
		{
			SourceType = UIImagePickerControllerSourceType.Camera;
			AllowsEditing = false;

			CameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo;
			ShowsCameraControls = true;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			CameraViewTransform = CoreGraphics.CGAffineTransform.MakeIdentity ();

			UIInterfaceOrientation interfaceOrientation = UIApplication.SharedApplication.StatusBarOrientation;
			UIDeviceOrientation interfaceToDevice;
			switch (interfaceOrientation) {
			case UIInterfaceOrientation.PortraitUpsideDown:
				interfaceToDevice = UIDeviceOrientation.PortraitUpsideDown;
				break;
			case UIInterfaceOrientation.Portrait:
				interfaceToDevice = UIDeviceOrientation.Portrait;
				break;
			case UIInterfaceOrientation.LandscapeLeft:
				interfaceToDevice = UIDeviceOrientation.LandscapeRight;
				break;
			case UIInterfaceOrientation.LandscapeRight:
				interfaceToDevice = UIDeviceOrientation.LandscapeLeft;
				break;
			default:
				interfaceToDevice = UIDeviceOrientation.Unknown;
				break;
			}


			var deviceOrientation = UIDevice.CurrentDevice.Orientation;
			switch (deviceOrientation) {
			case UIDeviceOrientation.FaceDown:
			case UIDeviceOrientation.FaceUp:
			case UIDeviceOrientation.Unknown:
				_initOrientation = UIDeviceOrientation.Portrait;
				Rotate (interfaceToDevice);
				break;
			default:
				_initOrientation = deviceOrientation;
				break;
			}
		}


		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return false;
		}

		public void Rotate (UIDeviceOrientation orientation)
		{
			if (BlrtiOS.BlrtHelperiOS.IsPhone || (new Version ("8.3") <= BlrtHelperiOS.DeviceVersion))
				return;

			// rotation bugs only for iPads ios version older than 8.3
			int currentOrientationDegree = OrientationToDegree (orientation);
			int previouOrientationDegree = OrientationToDegree (_initOrientation);

			if (currentOrientationDegree != -999 && previouOrientationDegree != -999) {
				this.CameraViewTransform = CoreGraphics.CGAffineTransform.MakeRotation (ConvertToRadians (previouOrientationDegree - currentOrientationDegree));
			}
		}

		public int OrientationToDegree (UIDeviceOrientation orientation)
		{
			int currentOrientationDegree;
			switch (orientation) {
			case UIDeviceOrientation.Portrait:
				currentOrientationDegree = 0;
				break;
			case UIDeviceOrientation.LandscapeLeft:
				currentOrientationDegree = 90;
				break;
			case UIDeviceOrientation.PortraitUpsideDown:
				currentOrientationDegree = 180;
				break;
			case UIDeviceOrientation.LandscapeRight:
				currentOrientationDegree = 270;
				break;
			default:
				currentOrientationDegree = -999;
				break;
			}

			return currentOrientationDegree;
		}

		public nfloat ConvertToRadians (int angle)
		{
			return ((NMath.PI / 180) * angle);
		}
	}

	public class CameraControllerDelegate : UIImagePickerControllerDelegate, IExecutionBlocker
	{
		UIImagePickerController _picker;

		readonly TaskCompletionSource<Tuple<UIImage, string>> DonePickingMedia = 
			new TaskCompletionSource<Tuple<UIImage, string>>();

		public Task<Tuple<UIImage, string>> Completion {
			get {
				return DonePickingMedia.Task;
			}
		}

		public CameraControllerDelegate (UIImagePickerController picker) : base ()
		{
			_picker = picker;
		}

		public override UIInterfaceOrientationMask SupportedInterfaceOrientations (UINavigationController navigationController)
		{
			return UIInterfaceOrientationMask.All;
		}


		#region IExecutionBlocker implementation

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			if (_picker.PresentingViewController != null) {
				await _picker.DismissViewControllerAsync (true);
			}
			return true;
		}

		#endregion

		public override void WillShowViewController (UINavigationController navigationController, UIViewController viewController, bool animated)
		{
			ExecutionPerformer.RegisterBlocker (this);
		}

		public override void Canceled (UIImagePickerController picker)
		{
			if (picker.PresentingViewController != null) {
				picker.DismissViewController (true, () => {
					ExecutionPerformer.UnregisterBlocker (this);
				});
			}
			DonePickingMedia.TrySetCanceled ();
		}

		public override void FinishedPickingMedia (UIImagePickerController picker, NSDictionary info)
		{
			if (picker.PresentingViewController != null) {

				picker.DismissViewController (true, () => {
					ExecutionPerformer.UnregisterBlocker (this);
				});
			}

			UIImage image = (UIImage)info.ObjectForKey (
				UIImagePickerController.OriginalImage
			);

			DonePickingMedia.TrySetResult (new Tuple<UIImage, string>(image, picker.SourceType.ToString ()));
		}


	}
}
