using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using BlrtCanvas;
using BlrtCanvas.Pages;
using BlrtData;
using BlrtiOS;
using BlrtiOS.Util;
using BlrtiOS.Views.MediaPicker;
using CoreGraphics;
using Foundation;
using ImagePickers;
using PhotoKitGallery;
using Photos;
using UIKit;

namespace PhotoKitGallery
{

	///<summary>
	///<para>
	///  This UICollectionViewController displayes all albums from the Photos library. 
	/// </para>
	/// <para>
	/// Array of albums displayed in this UICollectionView.
	/// </para>
	/// </summary>
	public class AlbumCollectionViewController : UICollectionViewController
	{

		public readonly List<PHAssetCollection> albums = new List<PHAssetCollection> ();

		static readonly NSString cellId = new NSString ("ImageCell");

		public static readonly float CELL_SPACING = 20f;

		float AlbumThumbnailSize { get; set; }

		bool ShowCameraIcon;

		public EventHandler<int> OnCameraTappedInAlbums;



		public AlbumCollectionViewController (bool showCameraIcon = false) : base (CollectionViewLayout)
		{
			ShowCameraIcon = showCameraIcon;
			UIBarButtonItem CancelButton = new UIBarButtonItem ();
			CancelButton.Title = "Cancel";
			CancelButton.Style = UIBarButtonItemStyle.Plain;
			CancelButton.Clicked += (object sender, EventArgs e) => {
				DismissViewController (true, null);
			};

			NavigationItem.RightBarButtonItem = CancelButton;
		
			UIBarButtonItem backButton = new UIBarButtonItem ();
			backButton.Title = "Albums";
			NavigationItem.BackBarButtonItem = backButton;

			NavigationItem.SetHidesBackButton (true, false);



		}


		static UICollectionViewFlowLayout CollectionViewLayout {
			get {
				var MinimumLineSpacing = 80f;
				return new UICollectionViewFlowLayout () {
					MinimumInteritemSpacing = CELL_SPACING, // Space between cells
					MinimumLineSpacing = MinimumLineSpacing,
					SectionInset = new UIEdgeInsets (CELL_SPACING, CELL_SPACING, MinimumLineSpacing, CELL_SPACING)
				};
			}
		}



		public bool LoadingFirstTime = true;// this is to show the all photos first
		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();

			NavigationItem.Title = "Select an Album";
			this.NavigationController.NavigationBar.Translucent = false;


		}

		///<summary>
		/// <para>Also contains code to show all
		/// the photos when loading the user clicks on to open gallery 
		/// first time.
		/// </para>
		/// </summary>
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			if (LoadingFirstTime == true && ShowCameraIcon == false)
			{
				ShowAllPhotos ();
				LoadingFirstTime = false;
				SendImages ().FireAndForget ();

			}

			View.LayoutIfNeeded ();
		}
		/// <summary>
		/// <para>
		/// This method is used to programatically navigate to the "All Photos/ Camera Roll" Album when the user
		/// enter the photo gallery for the first time.
		/// </para>
		/// </summary>


		public void ShowAllPhotos()
		{
			albumPhotosController = new AlbumPhotosController () ;


			foreach(var asset in albums)
			{

				if (asset.AssetCollectionSubtype == PHAssetCollectionSubtype.SmartAlbumUserLibrary) {

					albumPhotosController.AssetCollection = asset;

				}
			}

			this.NavigationController.PushViewController (albumPhotosController, true);
				
		}


		/// <summary>
		/// <para>
		/// Has the code that sets the order of Albums being displayed. All photos and favorites 
		/// are given 1st and 2nd priority respectively.
		/// </para>
			/// <para>
			/// Contains code that queries, fetches and populates the albums in a list from the photo library.
			/// </para>
			/// <para>Asks the collection view to reload data, so the fetched albums is displayed.</para>
			/// </summary>
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			CollectionView.BackgroundColor = UIColor.White;
			CollectionView.AllowsMultipleSelection = false;
		

			PHPhotoLibrary.RequestAuthorization (status => {
				if (status != PHAuthorizationStatus.Authorized)
					return;

				var smartAlbums = PHAssetCollection.FetchAssetCollections (PHAssetCollectionType.SmartAlbum, PHAssetCollectionSubtype.Any, null)
												   .Cast<PHAssetCollection> ();
				albums.AddRange (smartAlbums);


				var allAlbums = PHAssetCollection.FetchAssetCollections (PHAssetCollectionType.Album, PHAssetCollectionSubtype.Any, null)
									 .Cast<PHAssetCollection> ();

				albums.AddRange (allAlbums);


				for (int i = 0; i < albums.Count;i++) 
				{
					var k = 0;
						
					if (ShowCameraIcon == true)
					{
						k = 1;
					}
						
					var asset = albums [k];

					if (albums[i].AssetCollectionSubtype == PHAssetCollectionSubtype.SmartAlbumUserLibrary) {

						albums [k] = albums [i];
						albums [i] = asset;
					}

					var asset2 = albums [k+1];

					if (albums[i].AssetCollectionSubtype == PHAssetCollectionSubtype.SmartAlbumFavorites )
					
					{	albums [k+1] = albums [i];
							albums [i] = asset2;
					}
				}
				


				NSOperationQueue.MainQueue.AddOperation (() => {
					CollectionView?.ReloadData ();
				});
			});

			CollectionView.RegisterClassForCell (typeof (ImageCell), cellId);
			SetThumnailSize ();
			(Layout as UICollectionViewFlowLayout).ItemSize = new CGSize (AlbumThumbnailSize, AlbumThumbnailSize);


		}

		void SetThumnailSize ()
		{
			var width = Math.Min (BlrtUtil.PlatformHelper.GetDeviceWidth (), BlrtUtil.PlatformHelper.GetDeviceHeight ());
			if (BlrtHelperiOS.IsPhone) {
				AlbumThumbnailSize = (width - CELL_SPACING * 3) / 2.0f;
			} else {
				AlbumThumbnailSize = (width - CELL_SPACING * 3) / 2.0f;
			}
		}
		#region UICollectionViewController

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return albums.Count;
		}

		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = (ImageCell)collectionView.DequeueReusableCell (cellId, indexPath);
			if (cell == null)
				throw new InvalidProgramException ("Unable to dequeue an AlbumCollectionViewCell");

			var collection = albums [indexPath.Row];
			cell.imageView.Image = null;

			if (indexPath.Row == 0 && ShowCameraIcon== true ) {
				cell.imageTitle.Text=  "Take a photo";
				cell.ImageCount = " ";

				try {
					cell.imageView.Image = UIImage.FromBundle ("camera_album_thumbnail.png");
				} catch (Exception e) {
#if DEBUG
					Console.WriteLine ("ELCImagePicker, GetCell-SetThumbnail, Exception: " + e.ToString ());
#endif
				}

			} else {
				

				cell.imageTitle.Text = collection.LocalizedTitle;
				cell.ImageCount = collection.EstimatedAssetCount.ToString ();


				var firstAsset = (PHAsset)PHAsset.FetchAssets (collection, new PHFetchOptions ()).LastObject;
				if (firstAsset != null) {
					var options = new PHImageRequestOptions {
						Synchronous = true
					};

					PHImageManager.DefaultManager.RequestImageForAsset (firstAsset, new SizeF (160, 160), PHImageContentMode.AspectFit, options, (requestedImage, _) => {
						cell.imageView.Image = requestedImage;//setting the thumbnail image of the album to most recent photo
					});

				}
			}
			return cell;
		}

		#endregion

		#region Storyboard seque
		readonly TaskCompletionSource<object> _TaskCompletionSource = new TaskCompletionSource<object> ();

		public Task<object> Completion {
			get {

				return _TaskCompletionSource.Task;
			}
		}

		AlbumPhotosController albumPhotosController;

		/// <summary>
		///<para>
		/// The following method presents the albumphotoscontroller which 
		/// displays the photos inside that specific selected item.
		/// </para>
		/// </summary>
	
		public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{

			if (indexPath.Row == 0 && ShowCameraIcon == true) {
					try {

						var _camera = CameraController.Camera;
						_camera.ImagePickerControllerDelegate = new CameraControllerDelegate (_camera);

						UIViewController v = this;
						while (v.ParentViewController != null)
							v = v.ParentViewController;

						UIView.AnimationsEnabled = false;
						v.PresentViewController (_camera, false, null);
						UIView.AnimationsEnabled = true;

						(_camera.ImagePickerControllerDelegate as CameraControllerDelegate).Completion.ContinueWith ((arg) => {
							if (arg.IsCanceled || arg.IsFaulted) {
								_camera.PopViewController (true);
								_camera.Dispose ();
								_camera = null;
							} else {

								Tuple<UIImage, string> result = arg.Result;
								//Parent.SelectedCameraImage (arg.Result);
								//List<PhotoGalleryAssetResult> images = new List<PhotoGalleryAssetResult> ();
								//PhotoGalleryAssetResult photoGalleryAssetResult = new PhotoGalleryAssetResult ();

								//photoGalleryAssetResult.Image = result.Item1.CGImage;
								//images.Add (photoGalleryAssetResult);

								//_TaskCompletionSource.TrySetResult (images);

								SelectedCameraImage (arg.Result);

							}
						});

					} catch {

					}

				//OnCameraTappedInAlbums?.Invoke (this, 1);
			} 
			else
				{
					albumPhotosController = new AlbumPhotosController () {
					AssetCollection = albums [indexPath.Row]
				};

				this.NavigationController.PushViewController (albumPhotosController, true);
				SendImages ().FireAndForget ();
			}

		}
		#endregion


		public void SelectedCameraImage (Tuple<UIImage, string> imgData)
		{
			_TaskCompletionSource.TrySetResult (imgData);
		}

		async Task SendImages ()
		{

			List<PhotoGalleryAssetResult> images = null;
			images = await albumPhotosController.Completion as List<PhotoGalleryAssetResult>;


			_TaskCompletionSource.TrySetResult (images);
			
		}

	}

	/// <summary>
	/// Custom uicollectionviewcell to display the albums.
	/// </summary>

	public class ImageCell : UICollectionViewCell
	{
		nfloat CellCornerRadius = 3f;
		public UIImageView imageView;
		public UILabel imageTitle;
		UILabel imageCount;

		[Export ("initWithFrame:")]
		public ImageCell (CGRect frame) : base (frame)
		{
			ContentView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView = new UIImageView ();
			imageView.BackgroundColor = BlrtStyles.iOS.Gray3;
			imageView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView.ClipsToBounds = true;
			ContentView.AddSubview (imageView);


			imageTitle = new UILabel ();
			//imageTitle.Text = "Hello";
			imageTitle.TextAlignment = UITextAlignment.Center;
			imageTitle.TextColor = BlrtStyles.iOS.Gray7;
			ContentView.AddSubview (imageTitle);

			imageCount = new UILabel () {
				Lines = 1,
				TextColor = BlrtStyles.iOS.Gray4
			};

			Layer.CornerRadius = ContentView.Layer.CornerRadius = imageView.Layer.CornerRadius = CellCornerRadius;
		}
		/// <summary>
		/// Layouts the subviews.
		/// <para>
		/// Sets the frames of image title, image view and image count.
		/// </para>
		/// </summary>


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var imgCountLabelSize = imageCount.SizeThatFits (ContentView.Frame.Size);

			var infoSize = new CGSize (ContentView.Frame.Width,
									   imageTitle.SizeThatFits (ContentView.Frame.Size).Height + imgCountLabelSize.Height);

			imageView.Layer.Frame = new CGRect (ContentView.Frame.Left,
												ContentView.Frame.Top,
												ContentView.Frame.Width,
												ContentView.Frame.Height);

			imageTitle.Frame = new CGRect (ContentView.Frame.Left, ContentView.Frame.Bottom, infoSize.Width, infoSize.Height);

			imageCount.Frame = new CGRect (imageTitle.Frame.Left, imageTitle.Frame.Bottom, imageTitle.Frame.Width, imgCountLabelSize.Height);
		}

		public UIImage Image {
			set {
				if (value == null) {
					imageView.BackgroundColor = BlrtStyles.iOS.Gray3;
				} else {
					imageView.Image = value;
				}
			}
			get {
				return imageView.Image;
			}
		}

		public string AlbumName {
			set {
				imageTitle.Text = value;
			}
			get {
				return imageTitle.Text;
			}
		}

		public string ImageCount {
			set {
				imageCount.Text = value;
			}
			get {
				return imageCount.Text;
			}
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();

			Image = null;
			ImageCount = AlbumName = " ";
		}

	}



}
