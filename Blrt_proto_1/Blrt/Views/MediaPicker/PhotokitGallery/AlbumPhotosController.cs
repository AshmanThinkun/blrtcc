﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using BlrtData;
using BlrtiOS;
using BlrtiOS.Views.Conversation;
using BlrtiOS.Views.FullScreenViewers;
using CoreGraphics;
using Foundation;
using ImagePickers;
using ImagePickers.ImageAssets;
using PhotoKitGallery;
using Photos;
using UIKit;

namespace PhotoKitGallery
{
	/// <summary>
	/// <para>This view controller is responsible for sorting and presenting the 
	/// photos in a specific album using the photokit libarary.
	/// </para>
	/// <para> In addition to these, it also presents a previewer when the user 
	/// clicks on the magniying glass.
	/// </para>
	/// </summary>

	public class AlbumPhotosController : UICollectionViewController
	{

		static readonly NSString cellId = new NSString ("ImageCellForAllPhotos");

		readonly List<PHAsset> assets = new List<PHAsset> ();

		public PHAssetCollection AssetCollection { get; internal set; }
		public static readonly float CELL_SPACING = 0f;
		float _imageThumbnailSize;
		PHFetchResult fetchResults;
		bool firstTimeScrollDownInViewDidLayoutSubviews;

		UILabel TotalImagesSelectedView;


		static UICollectionViewFlowLayout CollectionViewLayout {
			get {
				var MinimumLineSpacing = 0f;
				return new UICollectionViewFlowLayout () {
					MinimumInteritemSpacing = CELL_SPACING, // Space between cells
					MinimumLineSpacing = MinimumLineSpacing,
					SectionInset = new UIEdgeInsets (CELL_SPACING, CELL_SPACING, MinimumLineSpacing, CELL_SPACING)
				};
			}
		}

		public AlbumPhotosController () : base (CollectionViewLayout)
		{

			fetchResults = PHAsset.FetchAssets (PHAssetMediaType.Image, null);

			UIBarButtonItem DoneButton = new UIBarButtonItem ();
			DoneButton.Title = "Done";
			DoneButton.Style = UIBarButtonItemStyle.Plain;
			DoneButton.Clicked += (object sender, EventArgs e) => {

				DoneButtonClicked ();

			};
			NavigationItem.RightBarButtonItem = DoneButton;

			RootController.ShouldHideStatusBar = false;


		}

		BlrtiOS.Views.Root.RootAppController RootController {
			get {
				return BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
			}
		}

		List<PHAsset> selected = new List<PHAsset> ();

		readonly TaskCompletionSource<object> _TaskCompletionSource = new TaskCompletionSource<object> ();
		WeakReference _Parent;

		public ImagePickerViewController Parent {
			get {
				return _Parent == null ? null : _Parent.Target as ImagePickerViewController;
			}
			set {
				_Parent = new WeakReference (value);
			}
		}




		public Task<object> Completion {
			get {

				return _TaskCompletionSource.Task;
			}
		}

		/// <summary>
		/// <para>
		/// Adds the selected images to the results list and sets the task completion source.
		/// </para>
		/// </summary>
		void DoneButtonClicked ()
		{

			var results = new List<PhotoGalleryAssetResult> (selected.Count);
			foreach (var asset in selected) {

				var result = new PhotoGalleryAssetResult ();
				result.pHAsset = asset;

				results.Add (result);
			}


			_TaskCompletionSource.TrySetResult (results);

		}


		///<summary>
		///<para>
		/// It also includes code which queries, recieves and populates a list of phasset with photos
		/// in a specific album.
		/// </para> 
		/// </summary>



		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

#region query and populate photos
			var assetCollection = AssetCollection;
			if (assetCollection == null)
				return;

			var items = PHAsset.FetchAssets (assetCollection, new PHFetchOptions ())
							   .Cast<PHAsset> ();


			assets.AddRange (items);
#endregion


			CollectionView.BackgroundColor = UIColor.White;
			CollectionView.AllowsMultipleSelection = true;

			(Layout as UICollectionViewFlowLayout).ItemSize = new CGSize (ImageThumbnailSize, ImageThumbnailSize);
			CollectionView.RegisterClassForCell (typeof (ImageCellForAllPhotos), cellId);

			TotalImagesSelectedView = new UILabel () {
				TextAlignment = UITextAlignment.Center,
				Font = BlrtStyles.iOS.CellTitleBoldFont,
				BackgroundColor = BlrtStyles.iOS.Gray3,
				Alpha = 0
			};

			View.AddSubview (TotalImagesSelectedView);



			NavigationItem.Title = AssetCollection.LocalizedTitle;

			CollectionView.ContentInset = new UIEdgeInsets (0, 0, 45, 0);
			var newBottomOffset = new CGRect (CollectionView.ContentSize.Width - 1, CollectionView.ContentSize.Height - 1, 1, 1);
			//CollectionView.ScrollRectToVisible (newBottomOffset, false);
		//	CollectionView.SetContentOffset (new CGPoint (0, CollectionView.ContentSize.Height - CollectionView.Frame.Size.Height+45), true);
			View.LayoutIfNeeded ();


		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			//CollectionView.SetContentOffset (new CGPoint (0, CollectionView.ContentSize.Height - CollectionView.Frame.Size.Height+45), true);
			//CollectionView.ContentInset = new UIEdgeInsets (0, 0, 45, 0);
			var newBottomOffset = new CGRect (CollectionView.ContentSize.Width - 1,CollectionView.ContentSize.Height - 1  , 1, 1);
			CollectionView.ScrollRectToVisible (newBottomOffset, false);

			//View.LayoutIfNeeded ();


		}


		/// <summary>
		/// <para>
		/// 
		/// </para>
		/// </summary>

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			if(TotalImagesSelectedView== null)
			{
				TotalImagesSelectedView = new UILabel () {
					TextAlignment = UITextAlignment.Center,
					Font = BlrtStyles.iOS.CellTitleBoldFont,
					BackgroundColor = BlrtStyles.iOS.Gray3,
					Alpha = 0
				};
				View.AddSubview (TotalImagesSelectedView);
			}



			TotalImagesSelectedView.Frame = new CGRect (0,
														CollectionView.Frame.Bottom - 45,
														View.Bounds.Width,
														45);
		}






		protected float ImageThumbnailSize {
			get {
				if (_imageThumbnailSize <= 0) {
					var width = Math.Min (BlrtUtil.PlatformHelper.GetDeviceWidth (), BlrtUtil.PlatformHelper.GetDeviceHeight ());
					if (BlrtHelperiOS.IsPhone) {
						_imageThumbnailSize = (width - 3.0f) / 4.0f;
					} else {
						_imageThumbnailSize = (width - 7.0f) / 8.0f;
					}
				}
				return _imageThumbnailSize;
			}
		}

		#region UICollectionViewController delegate methods

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return assets.Count;
		}
		///<summary>
		///<para>
		///  <c>PHImageManager</c>is used to set the imageview of the cell using the fetched phAsset.
		///</para>
		/// <para>
		/// Since the cells are recycled when the view is scrolled and 
		/// there is no guarantee that the same cell will be used for an image 
		/// before and after scrolling, the selected list is used to retain the selection.
		/// </para>
		/// <para>
		/// <c>OnPreviewTapped</c> event listener gets triggered when it recieves an event of 
			/// the "magnifying glass" being tapped on the cell from class "ImageCellForAllPhotos".
			///this basically leads to a method which shows a preview of the image.
		/// </para>
		/// </summary>
		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{

			var cell = (ImageCellForAllPhotos)collectionView.DequeueReusableCell (cellId, indexPath);

			cell.SetThumnailSize (ImageThumbnailSize);

			if (cell == null)
				throw new InvalidProgramException ("Unable to dequeue a AlbumPhotosViewCell");

			cell.imageView.Image = null;
			cell.overlay.Alpha = 0;

			var options = new PHImageRequestOptions {
				Synchronous = true
			};

			var asset = assets [indexPath.Row];

			PHImageManager.DefaultManager.RequestImageForAsset (asset, new SizeF (160, 160), PHImageContentMode.AspectFit, options, (requestedImage, _) => {
				cell.imageView.Image = requestedImage;
			});


			foreach (var selectedAsset in selected) {
				if (asset == selectedAsset) {
					cell.overlay.Alpha = 1; // overlay is the uiview that holds the checkmark
				}
			}

			cell.OnPreviewTapped += OpenPreview;


			return cell;

		}

		bool cellPreviewTapped;

		/// <summary>
		/// This method acts as an event listener which is triggered when the magnifying glass on the cell is touched.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>

		void OpenPreview (object sender, int e)
		{
			cellPreviewTapped = true;
		}

		#endregion

		#region Storyboard seque
		/// <summary>
		/// <para>(a)Removes the phAsset from selected list.</para>
		/// <para>(b)Removes the checkmark.</para>
		/// <para>(c)Sets the selection state of the cell to false.</para>
		/// </summary>
		/// <param name="collectionView">Collection view.</param>
		/// <param name="indexPath">Index path.</param>
		public override void ItemDeselected (UICollectionView collectionView, NSIndexPath indexPath)
		{
			var asset = assets [indexPath.Row];

			if (cellPreviewTapped == false) {

				if (selected.Contains (asset)) {
					selected.Remove (asset);

					ImageCellForAllPhotos cell = CollectionView.CellForItem (indexPath) as ImageCellForAllPhotos;
					cell.overlay.Alpha = 0;//makes the checkmark invisible
					cell.isSelected = false;
				}

			}
			collectionView.LayoutIfNeeded ();
			DisplaySelectedNumberOfPhotos ();//shows the number of selected photos at the bottom of the screen

		}
		///<summary> 
		/// <para>
		/// Also calls the method <c>ShowImageViewer</c> when the magnifying glass is tapped on a cell.
		/// </para>
		/// <para>
		/// (a) Checks if the magnifying glass is selected.
		/// </para>
		/// <para>
		/// (b) Checks if the list already has the selected element.
		/// </para>
		/// <para>
		/// (c) Adds checkmark.
		/// </para>
		/// <para>
		/// (d) Sets the selection state of the cell to true.
		/// </para>
		/// </summary>
		/// 
		public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{

			var BubbleImageCellMaxImageCount = BlrtiOS.Views.Conversation.Cells.ImageCell.MAX_IMAGES;

			//+1 because selected starts at 0
			var totalImagesSelected = selected.Count + 1 ;

			//this is in case of sharing images in a convo and the user exceeds the image limit.
			if (ConversationController.IsForImageSharing && (totalImagesSelected > BubbleImageCellMaxImageCount) )
			{
				//Create Alert
				var okAlertController = UIAlertController.Create ("Image selection limit", "You cannot select more than "+BubbleImageCellMaxImageCount+" images at a time", UIAlertControllerStyle.Alert);

				//Add Action
				okAlertController.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Default, null));

				// Present Alert
				PresentViewController (okAlertController, true, null);
				return;
			}

			//var cell = (ImageCellForAllPhotos)collectionView.DequeueReusableCell (cellId, indexPath);
			var asset = assets [indexPath.Row];

			if (cellPreviewTapped == true) {
				ShowImageViewer (indexPath, collectionView);
				cellPreviewTapped = false;

			} 
			else
			{
				
				if (selected.Contains (asset)) {
					//do nothing
				} 

				else
				
				{
					selected.Add (asset);
					selected.Distinct ().ToList ();
				}

				ImageCellForAllPhotos cell = CollectionView.CellForItem (indexPath) as ImageCellForAllPhotos;
				cell.overlay.Alpha = 1;
				cell.isSelected = true;

			}
			collectionView.LayoutIfNeeded ();
			DisplaySelectedNumberOfPhotos ();

		}
		/// <summary>
		/// <para>
		/// This method displays a uiview which shows the number of selected photos with a fade in 
		/// and fade out animation.
		/// </para> 
		/// </summary>
		void DisplaySelectedNumberOfPhotos ()
		{
			var ANIMATION_DURATION = 0.3f;

			if (selected.Count <= 1) {
				UIView.Animate (ANIMATION_DURATION, delegate {
					TotalImagesSelectedView.Text = string.Format ("{0} items selected", selected.Count);
					TotalImagesSelectedView.Alpha = selected.Count > 0 ? 0.90f : 0.0f;

						
				});
			} else {
				TotalImagesSelectedView.Text = string.Format ("{0} items selected", selected.Count);
			}

		}

		protected ImagePreviewController _previewer;
		readonly int TEXT_VIEW_HEIGHT = ImagePreviewController.TOP_BAR_HEIGHT;

		bool PreviewerOnDisplay;

		/// <summary>
		/// <para>
		/// This method initializes a previewer which appears when the user clicks on the 
		/// magnifying glass.
		/// </para>
		/// <para>
		/// These lists of PhotoGalleryAsset type since the ImagePreviewController 
		/// accepts the assets of type "BaseImageAsset". 
		/// </para>
		/// <para>
		/// The <c>isSelectedEventHandler</c> and <c>isDeSelectedEventHandler</c> event listener are triggered when the select button is clicked on the 
		/// preview. The selection state of the image in the preview is now being updated here
		/// in this view controller.
		// </para>
		/// </summary>
	
		void ShowImageViewer (NSIndexPath index, UICollectionView collectionView)//(ImageCell cell, EventHandler OnDoneButtonPressed, int index)
		{
			
			List<PhotoGalleryAsset> photoGalleryAssets = new List<PhotoGalleryAsset> ();
			List<PhotoGalleryAsset> selectedPhotoGalleryAssets = new List<PhotoGalleryAsset> ();

			////this is to avoid items at index zero which is causing issues in previewer.
			//PhotoGalleryAsset assetNull = new PhotoGalleryAsset (collectionView, null, index.Row);

			//photoGalleryAssets.Add (assetNull);
			//selectedPhotoGalleryAssets.Add (assetNull);

			for (int i = 0; i < assets.Count; i++) {

				PhotoGalleryAsset asset = new PhotoGalleryAsset (collectionView, assets [i], index.Row);
				photoGalleryAssets.Add (asset);
			}

			for (int i = 0; i < selected.Count; i++) {

				PhotoGalleryAsset selectedAsset = new PhotoGalleryAsset (collectionView, selected [i], index.Row);
				selectedPhotoGalleryAssets.Add (selectedAsset);
			}


			List<BaseImageAsset> Assets = new List<BaseImageAsset> (photoGalleryAssets.Count);
			List<BaseImageAsset> SelectedAssets = new List<BaseImageAsset> (selectedPhotoGalleryAssets.Count);


			Assets.AddRange (photoGalleryAssets);

			var initialAssetNumber = Math.Max(index.Row, 0);
			

			SelectedAssets.AddRange (selectedPhotoGalleryAssets);

				if (selected.Contains (photoGalleryAssets [index.Row].Asset)) {
				_previewer = new ImagePreviewController (Assets, SelectedAssets,initialAssetNumber, true, true); 
				} 
				else
				{
				_previewer = new ImagePreviewController (Assets,SelectedAssets ,initialAssetNumber, true, false); 
				}
				//_previewer.DoneButtonItem.Clicked += OnDoneButtonPressed;
				_previewer.PreviewWillRotate += delegate {
					var NavBarHeight = NavigationController.NavigationBar.Frame.Size.Height;
					var StatusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Size.Height;

					TotalImagesSelectedView.Frame = new CGRect (0,
																_previewer.View.Frame.Bottom - TEXT_VIEW_HEIGHT,
																_previewer.View.Bounds.Width,
																TEXT_VIEW_HEIGHT);
					//MaxImageAlertView.Frame = new CGRect (0,
					//StatusBarHeight +
					//NavBarHeight +
					//(PreviewerOnDisplay ? ImagePreviewController.TOP_BAR_HEIGHT : 0),
					//_previewer.View.Bounds.Width,
					//TEXT_VIEW_HEIGHT);
				};
				_previewer.PreviewClosed += delegate {
					PreviewerOnDisplay = false;
					//TableView.ReloadData ();
				};
			 

			//_previewer.RemainingImageCount = Parent.MaximumImagesCount - TotalSelectedAssets;

			NavigationController.PushViewController (_previewer, true);
			//_previewer.isSelectedEventHandler -= SelectedPreviewImage;


				_previewer.isSelectedEventHandler += (object o, int e) => {

				var indexPath = NSIndexPath.FromRowSection (e, 0);

				ItemSelected (collectionView, indexPath);
				ImageCellForAllPhotos cell =	CollectionView.CellForItem (index) as ImageCellForAllPhotos;
					

				};

			_previewer.isDeSelectedEventHandler += (object o, int e) => {

				ItemDeselected (collectionView, index);
				ImageCellForAllPhotos cell = CollectionView.CellForItem (index) as ImageCellForAllPhotos;

			};


			PreviewerOnDisplay = true;
		}


		#endregion
	}

	/// <summary>
	/// <para>
	/// This is a custom uicollectionviewcell class for the cells that are used to view all photos or photos in a specific album.
	/// </para>
	/// </summary>
	public class ImageCellForAllPhotos : UICollectionViewCell
	{
		readonly float START_X_FOR_SUBVIEWS = 0;
		readonly float START_Y_FOR_SUBVIEWS = 4;
		readonly float IMAGE_FRAME_HORIZONTAL_PADDING = 1;
		readonly float IMAGE_FRAME_VERTICAL_PADDING = 1;

		/// <summary>
		/// <para>
		/// <c>overlay</c> is used to show the checkmark on the cell
		/// </para>
		/// <para>
		/// <c>preview</c> is used to show the magnifying glass
		/// </para>
		/// <para>
		/// <c>i</c>
		/// </para>
		/// </summary>


		nfloat CellCornerRadius = 3f;
		public UIImageView imageView;
		public UILabel imageTitle;
		public bool isSelected;
		UILabel imageCount;
		UIImage _selectImg;
		UIImage _previewImage;
		public UIImageView overlay;//shows the checkmark
		public UIImageView preview;//shows the magnifying glass
		float ThumbnailSize;
		//public bool clickedOnPreview;

		public event EventHandler<int> OnPreviewTapped;

		[Export ("initWithFrame:")]
		public ImageCellForAllPhotos (CGRect frame) : base (frame)
		{
			ContentView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView = new UIImageView ();

			imageView.ContentMode = UIViewContentMode.ScaleAspectFill;
			imageView.ClipsToBounds = true;
			ContentView.AddSubview (imageView);


			imageTitle = new UILabel ();

			imageTitle.TextAlignment = UITextAlignment.Center;
			imageTitle.TextColor = BlrtStyles.iOS.Gray7;
			ContentView.AddSubview (imageTitle);

			imageCount = new UILabel () {
				Lines = 1,
				TextColor = BlrtStyles.iOS.Gray4
			};

			Layer.CornerRadius = ContentView.Layer.CornerRadius = imageView.Layer.CornerRadius = CellCornerRadius;

			UITapGestureRecognizer tapRecognizer = new UITapGestureRecognizer (CellTapped);
			tapRecognizer.CancelsTouchesInView = false;
			AddGestureRecognizer (tapRecognizer);



			_selectImg = new UIImage ("img_select_lg.png");
			_previewImage = new UIImage ("preview_image.png");

			overlay = new UIImageView ();
			//overlay.Frame = ContentView.Frame;
			overlay.Image = _selectImg;
			overlay.ContentMode = UIViewContentMode.Center;
			overlay.Alpha = 0;
			ContentView.AddSubview (overlay);



			preview = new UIImageView ();
			preview.Image = _previewImage;
			preview.Alpha = 1;
			preview.ContentMode = UIViewContentMode.TopRight;
			ContentView.AddSubview (preview);


		}

		public void SetThumnailSize (float thumbnailSize)
		{
			ThumbnailSize = thumbnailSize;

		}

		///<summary>
		/// <para> Event triggered in case the magnifying glass is touched</para>
		/// </summary>
		public void CellTapped (UITapGestureRecognizer tapRecognizer)
		{
			CGPoint point = tapRecognizer.LocationInView (this);
			tapRecognizer.CancelsTouchesInView = false;
			var frame = new CGRect (START_X_FOR_SUBVIEWS, START_Y_FOR_SUBVIEWS, ThumbnailSize, ThumbnailSize);

			if (preview.Frame.Contains (point)) {
				
				OnPreviewTapped?.Invoke (this, 1);


			} else if (frame.Contains (point)) {
				
				return;

			}


		}

		///<summary>
		/// <para>
		/// Contains lines of code are used to track the user's touch 
		/// </para>
		/// </summary>

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var imgCountLabelSize = imageCount.SizeThatFits (ContentView.Frame.Size);

			var infoSize = new CGSize (ContentView.Frame.Width,
									   imageTitle.SizeThatFits (ContentView.Frame.Size).Height + imgCountLabelSize.Height);

			imageView.Layer.Frame = new CGRect (ContentView.Frame.Left,
												ContentView.Frame.Top,
												ContentView.Frame.Width,
												ContentView.Frame.Height);

			imageTitle.Frame = new CGRect (ContentView.Frame.Left, ContentView.Frame.Bottom, infoSize.Width, infoSize.Height);

			imageCount.Frame = new CGRect (imageTitle.Frame.Left, imageTitle.Frame.Bottom, imageTitle.Frame.Width, imgCountLabelSize.Height);

			var previewWidth = Math.Max (21, ThumbnailSize * 0.35f);


			var frame = new CGRect (0, 4, ThumbnailSize, ThumbnailSize);

			preview.Frame = new CGRect (frame.X + frame.Width - previewWidth - 1,
			frame.Y + 1, previewWidth, previewWidth);
			overlay.Center = new CGPoint (frame.X + frame.Width * 0.5f, frame.Y + frame.Height * 0.5f);


		}

		public UIImage Image {
			set {
				if (value == null) {
					imageView.BackgroundColor = BlrtStyles.iOS.Gray3;
				} else {
					imageView.Image = value;
				}
			}
			get {
				return imageView.Image;
			}
		}

		public string AlbumName {
			set {
				imageTitle.Text = value;
			}
			get {
				return imageTitle.Text;
			}
		}

		public string ImageCount {
			set {
				imageCount.Text = value;
			}
			get {
				return imageCount.Text;
			}
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();

			Image = null;
			ImageCount = AlbumName = " ";
		}




	}


}