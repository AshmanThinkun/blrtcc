﻿using System;
using System.Collections.Generic;
using BlrtData;
using CoreGraphics;
using Foundation;
using ImagePickers.ImageAssets;
using UIKit;

namespace ImagePickers
{
	public class ImageCell : UITableViewCell
	{
		public List<BaseImageAsset> RowAssets;
		int Columns;
		float ThumbnailSize;
		readonly List<UIImageView> ImageViewArray = new List<UIImageView> ();
		readonly List<UIImageView> OverlayViewArray = new List<UIImageView> ();
		readonly List<UIImageView> PreviewViewArray = new List<UIImageView> ();

		readonly float START_X_FOR_SUBVIEWS = 0;
		readonly float START_Y_FOR_SUBVIEWS = 4;
		readonly float IMAGE_FRAME_HORIZONTAL_PADDING = 1;
		readonly float IMAGE_FRAME_VERTICAL_PADDING = 1;

		public event EventHandler<int> OnPreviewTapped;

		UIImage _selectImg, _previewImage;

		public ImageCell (UITableViewCellStyle style, string reuseIdentifier) : base (style, reuseIdentifier)
		{
			UITapGestureRecognizer tapRecognizer = new UITapGestureRecognizer (CellTapped);
			AddGestureRecognizer (tapRecognizer);

			_selectImg = new UIImage ("img_select_lg.png");
			_previewImage = new UIImage ("preview_image.png");
		}

		/**
			 * Sets an image, preview icon and "selected" ison for each image
			 */
		public void SetAssets (List<BaseImageAsset> assets, int columns, float thumbnailSize)
		{
			RowAssets = assets;
			Columns = columns;
			ThumbnailSize = thumbnailSize;
			foreach (var view in ImageViewArray) {
				view.RemoveFromSuperview ();
			}
			foreach (var view in OverlayViewArray) {
				view.RemoveFromSuperview ();
			}

			var firstAsset = RowAssets [0];
			var photoAssetForMediaAsset = firstAsset is PhotoAssetForMediaAsset;

			for (int i = 0; i < RowAssets.Count; i++) {
				var asset = RowAssets [i];

				UIImageView imageView = null;
				if (i < ImageViewArray.Count) {
					imageView = ImageViewArray [i];
				} else {
					imageView = new UIImageView ();
					ImageViewArray.Add (imageView);
				}
				imageView.Image = asset.Thumbnail;

				UIImageView overlayView = null;
				if (i < OverlayViewArray.Count) {
					overlayView = OverlayViewArray [i];
					overlayView.Image = _selectImg;
					overlayView.Alpha = 1; // OPACITY SET TO 100%
				} else {
					overlayView = new UIImageView (_selectImg);
					OverlayViewArray.Add (overlayView);
				}

				UIImageView previewView = null;
				if (i < PreviewViewArray.Count) {
					previewView = PreviewViewArray [i];
					previewView.Image = _previewImage;
				} else {
					previewView = new UIImageView ();
					previewView.Image = _previewImage;
					previewView.ContentMode = UIViewContentMode.TopRight;
					PreviewViewArray.Add (previewView);
				}

				AddSubviews (new UIView () { imageView, overlayView, previewView });
			}
		}

		float ANIMATION_DURATION = 0.2f;

		/**
			 * Checks whether image needs to be selected or previewed, and excutes appropriate action
			 */
		void CellTapped (UITapGestureRecognizer tapRecognizer)
		{
			CGPoint point = tapRecognizer.LocationInView (this);

			// Locate point in frame of every image to identify whether user wants to preview or select
			var frame = new CGRect (START_X_FOR_SUBVIEWS, START_Y_FOR_SUBVIEWS, ThumbnailSize, ThumbnailSize);
			for (int i = 0; i < RowAssets.Count; ++i) {
				var previewFrame = PreviewViewArray [i].Frame;
				if (previewFrame.Contains (point)) {
					if (OnPreviewTapped != null)
						OnPreviewTapped (this, i);
					break;
				} else if (frame.Contains (point)) {
					RowAssets [i].Selected = !RowAssets [i].Selected;
					Animate (ANIMATION_DURATION, () => {
						OverlayViewArray [i].Alpha = RowAssets.Count > i && RowAssets [i].Selected ? 1 : 0;
					});
					break;
				}
				var x = frame.X + frame.Width + IMAGE_FRAME_HORIZONTAL_PADDING;
				frame = new CGRect (x, frame.Y, frame.Width, frame.Height);
			}
		}

		/**
			 * Display image, magnifying glass and checked sign
			*/
		public override void LayoutSubviews ()
		{
			//image is 28,this is the tapping area
			var previewWidth = Math.Max (21, ThumbnailSize * 0.35f);

			var frame = new CGRect (START_X_FOR_SUBVIEWS, START_Y_FOR_SUBVIEWS, ThumbnailSize, ThumbnailSize);

			int i = 0;
			foreach (var imageView in ImageViewArray) {
				imageView.Frame = frame;


				var overlayView = OverlayViewArray [i];
				overlayView.Center = new CGPoint (frame.X + frame.Width * 0.5f, frame.Y + frame.Height * 0.5f);
				overlayView.Alpha = RowAssets.Count > i && RowAssets [i].Selected ? 1 : 0;

				var previewView = PreviewViewArray [i];
				previewView.Frame = new CGRect (frame.X + frame.Width - previewWidth - IMAGE_FRAME_HORIZONTAL_PADDING,
					frame.Y + IMAGE_FRAME_VERTICAL_PADDING, previewWidth, previewWidth);
				previewView.Alpha = RowAssets.Count > i ? 1 : 0;

				i++;
				var x = frame.X + frame.Width + IMAGE_FRAME_HORIZONTAL_PADDING;
				frame = new CGRect (x, frame.Y, frame.Width, frame.Height);
			}
		}
	}
}