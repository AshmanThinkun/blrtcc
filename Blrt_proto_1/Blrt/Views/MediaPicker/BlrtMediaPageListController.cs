using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;
using BlrtData.Instructions;
using BlrtCanvas;
using BlrtCanvas.Pages;
using BlrtiOS.Util;

namespace BlrtiOS.Views.MediaPicker
{
	public partial class BlrtMediaPageListController : UICollectionViewController
	{

		public bool ForReBlrt { get; set; }


		NSString _mediaCellIdentifer = new NSString ("MediaCell");
		NSString _headerIdentifer = new NSString ("HeaderCell");

		IBlrtAddMediaController _parent;
		List<PageData> _data;

		UILongPressGestureRecognizer _longPressGesture;

		nfloat _strength;

		nfloat _scaleOfScreen;



		public bool HighlightNew { get; set; }

		bool _generatingThumbnails;

		NSIndexPath _draggingIndex;

		NSIndexPath _startingIndex;

		BlrtMediaPageListControllerCell _draggingView;

		List<UIImage> _allocatedImage;

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_data);
			_data.Clear ();
			_data = null;
			BlrtUtil.BetterDispose (_allocatedImage);
			_allocatedImage.Clear ();
			_allocatedImage = null;
			BlrtUtil.BetterDispose (_longPressGesture);
			_longPressGesture = null;
			BlrtUtil.BetterDispose (_draggingView);
			_draggingView = null;
		}

		public BlrtMediaPageListController (IBlrtAddMediaController parent)
			: base (new BlrtMediaPageListControllerLayout ())
		{
			_scaleOfScreen = UIScreen.MainScreen.Scale;

			_parent = parent;
			_data = new List<PageData> ();
			_allocatedImage = new List<UIImage> ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			(Layout as UICollectionViewFlowLayout).HeaderReferenceSize = new CGSize (CollectionView.Bounds.Width, BlrtMediaPageListControllerHeader.HEIGHT);
		}

		public override void LoadView ()
		{
			base.LoadView ();

			_scaleOfScreen = UIScreen.MainScreen.Scale;


			CollectionView.RegisterClassForCell (
				typeof(BlrtMediaPageListControllerCell),
				_mediaCellIdentifer
			);

			// add header view
			CollectionView.RegisterClassForSupplementaryView (
				typeof (BlrtMediaPageListControllerHeader),
				UICollectionElementKindSection.Header,
				_headerIdentifer
			);

			CollectionView.BackgroundColor = BlrtConfig.BLRT_MID_GRAY;


			_draggingView = new BlrtMediaPageListControllerCell ();

			_longPressGesture = new UILongPressGestureRecognizer (LongPressHandler) {
				MinimumPressDuration = 0.2f
			};
			CollectionView.AddGestureRecognizer (_longPressGesture);

			CollectionView.ReloadData ();
		}

		WeakReference<BlrtMediaPageListControllerHeader> _header;

		BlrtMediaPageListControllerHeader GetHeader ()
		{
			BlrtMediaPageListControllerHeader t = null;
			_header?.TryGetTarget (out t);

			return t;
		}

		public override UICollectionReusableView GetViewForSupplementaryElement (UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
		{
			var headerView = (BlrtMediaPageListControllerHeader)collectionView.DequeueReusableSupplementaryView (elementKind, _headerIdentifer, indexPath);
			headerView.ShowAllButtonClicked += OnShowAllButtonClicked;
			headerView.HideAllButtonClicked += OnHideAllButtonClicked;

			if (_header == null) {
				_header = new WeakReference<BlrtMediaPageListControllerHeader> (headerView);
			} else {
				_header.SetTarget (headerView);
			}

			return headerView;
		}

		void OnHideAllButtonClicked (object sender, EventArgs e)
		{
			foreach (var item in _data) {
				if (item.Hidable) {
					item.hidden = true;
				}
			}

			CollectionView.ReloadData ();
		}

		void OnShowAllButtonClicked (object sender, EventArgs e)
		{
			foreach (var item in _data) {
				item.hidden = false;
			}

			CollectionView.ReloadData ();
		}

		public int GetImageSourceCount ()
		{
			return GetItemSourceCount (new []{ BlrtMediaFormat.Image });
		}

		public int GetPDFSourceCount ()
		{
			return GetItemSourceCount (new []{ BlrtMediaFormat.PDF });
		}

		public int GetTotalPageCount ()
		{
			return _data.Count;
		}

		public int GetTotalShowingPageCount ()
		{
			return (from page in _data where !page.hidden select page).Count ();
		}

		public long GetConvoSizeByte (IEnumerable<PageData> extraAddedPages = null)
		{
			var mediaRepresentatives = new List<ICanvasPage> ();
			if (null != extraAddedPages) {
				foreach (var exPage in extraAddedPages) {
					if (!mediaRepresentatives.Any (representative => representative.SameMedia (exPage.page))) {
						mediaRepresentatives.Add (exPage.page);
					}
				}
			}
			foreach (var data in _data) {
				if (data.MediaRemovable && data.hidden) {
					continue;
				}
				if (!mediaRepresentatives.Any (representative => representative.SameMedia (data.page))) {
					mediaRepresentatives.Add (data.page);
				}
			}
			return mediaRepresentatives.Sum (representative => representative.MediaFileSizeByte);
		}

		public long GetBlrtSizeByte (IEnumerable<PageData> extraAddedPages = null)
		{
			var mediaRepresentatives = new List<ICanvasPage> ();
			if (null != extraAddedPages) {
				foreach (var exPage in extraAddedPages) {
					if (!mediaRepresentatives.Any (representative => representative.SameMedia (exPage.page))) {
						mediaRepresentatives.Add (exPage.page);
					}
				}
			}
			foreach (var data in _data) {
				if (data.hidden) {
					continue;
				}
				if (!mediaRepresentatives.Any (representative => representative.SameMedia (data.page))) {
					mediaRepresentatives.Add (data.page);
				}
			}
			return mediaRepresentatives.Sum (representative => representative.MediaFileSizeByte);
		}

		public ICanvasPage GetPage (int page)
		{
			return _data [page - 1].page;
		}

		public int GetItemSourceCount (BlrtMediaFormat[] formats)
		{
			var list = new List<ICanvasPage> ();

			foreach (var data in _data) {
				if (formats.Contains (data.page.Format)) {

					bool exists = false;

					foreach (var page in list) {
						if (page.SameMedia (data.page)) {
							exists = true;
							break;
						}
					}

					if (!exists) {
						list.Add (data.page);
					}
				}
			}

			return list.Count;
		}

		public bool HasDeletableContent ()
		{
			foreach (var data in _data) {
				if (data.PageDeletable) {
					return true;
				}
			}

			return false;
		}


		public async Task AddPages (IEnumerable<ICanvasPage> pages, PagePermissions permissions, bool forced)
		{
			if ((null == pages) || (0 == pages.Count ())) {
				return;
			}

			View.Hidden = false;

			if (!forced) {

				if (ForReBlrt && !BlrtPermissions.CanMediaAddOnReblrt) {
					await BlrtHelperiOS.AwaitAlert (BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.CanMediaAddOnReblrt));
					return; 
				}

				if (BlrtPermissions.MaxBlrtPageCount <= GetTotalShowingPageCount ()) {
					await BlrtHelperiOS.AwaitAlert (BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount));
					return;
				}
			}

			UIAlertView showedAlert = null;

			if (null == _data) {
				return;
			}

			var newList = new List<PageData> ();
			foreach (var page in pages) {

				if (null == page) {
					continue;
				}

				PageData data = null;

				bool needMediaSizeCheck = true;

				foreach (var d in _data) {

					if ((null != d) && (null != d.page) && d.page.SameMedia (page)) {

						if (!d.hidden) {
							needMediaSizeCheck = false;
							if (null != data) {
								break;
							}
						}

						if (d.page.MediaPage == page.MediaPage) {
							data = new PageData (page, permissions) {
								thumbnail = d.thumbnail
							};
							if (!needMediaSizeCheck) {
								break;
							}
						}
					}
				}


				foreach (var d in newList) {
					if ((null != d) && (null != d.page) && d.page.SameMedia (page) && (!d.hidden)) {
						needMediaSizeCheck = false;
						break;
					}
				}


				if (data == null) {
					data = new PageData (page, permissions);
				}

				bool addPage = true;


				if (!forced) {
					if (needMediaSizeCheck) {
						var tempList = new List<PageData> (newList);
						tempList.Add (data);
						if (BlrtPermissions.MaxConvoSizeKb * 1024.0f <= GetConvoSizeByte (tempList)) {
							if (showedAlert == null)
								showedAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxConvoSizeKB);
							addPage = false;
						}

						if (BlrtPermissions.MaxBlrtSizeKb * 1024.0f <= GetBlrtSizeByte (tempList)) {
							if (showedAlert == null)
								showedAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtSizeKB);
							addPage = false;
						}
					}

					if (addPage) {
						var newShowingPageCount = newList.Count (thisPageData => !thisPageData.hidden);
						if (BlrtPermissions.MaxBlrtPageCount <= GetTotalShowingPageCount () + newShowingPageCount) {
							if (showedAlert == null) {
								showedAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
							}

							data.hidden = true;
							addPage = (page.Format == BlrtMediaFormat.PDF);
						}
					}
				} else {
					if (needMediaSizeCheck) {
						var tempList = new List<PageData> (newList);
						tempList.Add (data);
						if (BlrtPermissions.MaxBlrtSizeKb * 1024.0f <= GetBlrtSizeByte (tempList)) {
							if (showedAlert == null)
								showedAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtSizeKB);
							data.hidden = true;
						}
					}

					if (!data.hidden) {
						var newShowingPageCount = newList.Count (thisPageData => !thisPageData.hidden);
						if (BlrtPermissions.MaxBlrtPageCount <= GetTotalShowingPageCount () + newShowingPageCount) {
							if (showedAlert == null) {
								showedAlert = BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
							}

							data.hidden = true;
						}
					}
				}

				if (addPage) {
					newList.Add (data);
				}
			}

			var newIndex = new NSIndexPath[newList.Count];
			for (int i = 0; i < newIndex.Length; ++i) {
				newIndex [i] = NSIndexPath.FromRowSection (i + _data.Count, 0);
			}


			if (CollectionView.NumberOfItemsInSection (0) == _data.Count) {
				CollectionView.ReloadData ();
			} else {
				_data.AddRange (newList);
				CollectionView.InsertItems (newIndex);
			}

			ReloadDuplicates (newList);

//			GenerateThumbnails ();

			if (showedAlert != null)
				await BlrtHelperiOS.AwaitAlert (showedAlert);

			return;
		}

		public event EventHandler PageRemoved;
		public void RemovePage (int index)
		{

			if (index < 0 || index >= _data.Count)
				return;


			var data = _data [index];

			ReloadDuplicates (data);

			if (data.hasDuplicate) {

				RemovePage (data);
				return;
			}

			switch (data.page.Format) {
				case BlrtMediaFormat.PDF:
					RemoveMedia (data.page);
					break;
				default:
					RemovePage (data);
					break;			
			}
		}

		void RemoveMedia (ICanvasPage media, bool forced = false)
		{
			if (forced) {
				var everyTime = _data.Where ((PageData arg) => {
					return media.SameMedia (arg.page);
				}).ToArray ();


				var indexpaths = new NSIndexPath[everyTime.Length];

				for (int i = 0; i < everyTime.Length; ++i) {			
					indexpaths [i] = NSIndexPath.FromRowSection (_data.IndexOf (everyTime [i]), 0);	
				}

				_data.RemoveAll ((PageData obj) => {
					return everyTime.Contains (obj);
				});
				CollectionView.DeleteItems (indexpaths);
				Array.ForEach (everyTime, (obj) => {
					obj.thumbnail = null;
				});
				everyTime = null;
				PageRemoved?.Invoke (this, EventArgs.Empty);
			} else {
				string removePrefix = "alert_deleting_template";
				switch (media.Format) {
					case BlrtMediaFormat.Image:
						removePrefix = "alert_deleting_image";
						break;
					case BlrtMediaFormat.PDF:
						removePrefix = "alert_deleting_pdf";
						break;
				}

				UIAlertView alert = new UIAlertView (
                    BlrtHelperiOS.Translate (removePrefix + "_title", "media"),
                    BlrtHelperiOS.Translate (removePrefix + "_message", "media"),
                    null,
                    BlrtHelperiOS.Translate (removePrefix + "_cancel", "media"),
                    BlrtHelperiOS.Translate (removePrefix + "_delete", "media")
                );

				alert.Clicked += (object sender, UIButtonEventArgs e) => {
					;
					if (e.ButtonIndex != alert.CancelButtonIndex)
						RemoveMedia (media, true);
				};
				alert.Show ();
			}
		}

		void RemovePage (PageData page, bool forced = false)
		{
			if (forced) {
				int index = _data.IndexOf (page);
				_data.Remove (page);
				CollectionView.DeleteItems (new NSIndexPath[]{ NSIndexPath.FromRowSection (index, 0) });	
				ReloadDuplicates (page);
				PageRemoved?.Invoke (this, EventArgs.Empty);
			} else {
				string removePrefix = "alert_deleting_template";

				switch (page.page.Format) {
					case BlrtMediaFormat.Image:
						if (page.hasDuplicate)
							removePrefix = "alert_deleting_image_duplicate";
						else
							removePrefix = "alert_deleting_image";
						break;
					case BlrtMediaFormat.PDF:
						if (page.hasDuplicate)
							removePrefix = "alert_deleting_pdf_duplicate";
						else
							removePrefix = "alert_deleting_pdf";
						break;
				}

				UIAlertView alert = new UIAlertView (
                    BlrtHelperiOS.Translate (removePrefix + "_title", "media"),
                    BlrtHelperiOS.Translate (removePrefix + "_message", "media"),
                    null,
                    BlrtHelperiOS.Translate (removePrefix + "_cancel", "media"),
                    BlrtHelperiOS.Translate (removePrefix + "_delete", "media")
                );

				alert.Clicked += (object sender, UIButtonEventArgs e) => {
					if (e.ButtonIndex != alert.CancelButtonIndex)
						RemovePage (page, true);
				};
				alert.Show ();
			}
		}


		public ICanvasPage[] GetUsedPages ()
		{
			return (from d in _data
			        where !d.hidden
			        select d.page).ToArray ();
		}

		public ICanvasPage[] GetHiddenPages ()
		{
			return (from d in _data
			        where d.hidden
			        select d.page).ToArray ();
		}

		class NewMediaInfo
		{
			public ICanvasPage page;
			public bool shouldRemove = true;
		};

		public async Task<BlrtMediaPagePrototype> GetMediaPagePrototype (bool trimMedia)
		{
			var mpp = new BlrtMediaPagePrototype ();
			var pageDataSrc = _data;
			if (trimMedia) {
				pageDataSrc = new List<PageData> (_data);

				var newMediaRepresentative = new List<NewMediaInfo> ();
				foreach (var data in pageDataSrc) {
					if (!data.MediaRemovable) {
						continue;
					}
					var found = newMediaRepresentative.FirstOrDefault (representative => representative.page.SameMedia (data.page));
					if (null == found) {
						newMediaRepresentative.Add (new NewMediaInfo {
							page = data.page,
							shouldRemove = data.hidden
						});
					} else {
						if (found.shouldRemove && (!data.hidden)) {
							found.shouldRemove = false;
						}
					}
				}

				foreach (var media in newMediaRepresentative) {
					if (media.shouldRemove) {
						pageDataSrc.RemoveAll ((page) => page.page.SameMedia (media.page));
					}
				}
			}

			foreach (var p in pageDataSrc) {
				if (string.IsNullOrEmpty (p.page.ObjectId) && !p.hidden) {
					await CreateMediaAsset (p.page);
				}
			}

			mpp.UncompressedItems = (from p in pageDataSrc
			                         where !string.IsNullOrEmpty (p.page.ObjectId) && !p.hidden
			                         select new BlrtMediaPagePrototype.PageIndex (p.page.ObjectId, p.page.MediaPage)
									).ToArray ();

			mpp.UncompressedHiddenItems = (from p in pageDataSrc
			                               where !string.IsNullOrEmpty (p.page.ObjectId) && p.hidden
			                               select new BlrtMediaPagePrototype.PageIndex (p.page.ObjectId, p.page.MediaPage)
										  ).ToArray ();

			return mpp;
		}

		async Task CreateMediaAsset (ICanvasPage page)
		{
			if (page is IMediaAssetContainer) {
				var container = page as IMediaAssetContainer;

				var siblings = (from other in _data
				                where other.page is IMediaAssetContainer && ((IMediaAssetContainer)other.page).SameMedia (container)
				                select other.page as IMediaAssetContainer).ToArray ();

				//after, as it may change the media while  moving it, like a png to jpeg
				try {
					await container.CreateAsset ();
				} catch (BlrtEncryptorManagerException ex) {
					if (ex.Code == BlrtEncryptorManagerException.ExceptionCodes.EncryptionFailed) {
						BeginInvokeOnMainThread (() => {
							var alert =  new UIAlertView (
								"Encryption failed",
								"Unable to encrypt file. Please contact Blrt Support.",
								null, 
								"OK"
							);
							alert.Show ();
						});
					}
					LLogger.WriteEvent ("encrypt", "error", "encryption failed", "BlrtMediaPageListController; error: " + ex.ToString ());
					return;
				}


				for (int i = 0; i < siblings.Length; ++i)
					siblings [i].SetMedia (container);
			}
		}
			

		void LongPressHandler (UILongPressGestureRecognizer sender)
		{

			const float AnimationLength = 0.2f;

			_parent.DismissAddMediaController (true);

			CGPoint loc = sender.LocationInView (CollectionView);
			CGPoint locInScreen = sender.LocationInView (View);
			//float heightInScreen = fmodf((loc.y-self.theCollectionView.contentOffset.y), CGRectGetHeight(self.theCollectionView.frame));
			//new PointF( loc.x-self.theCollectionView.contentOffset.x, heightInScreen );

			if (sender.State == UIGestureRecognizerState.Began) {
				_draggingIndex = CollectionView.IndexPathForItemAtPoint (loc);


				if (_draggingIndex != null && _draggingIndex.Row >= _data.Count) {
					_draggingIndex = null;
				}


				if (_draggingIndex != null) {
					if (_draggingView.Superview == null)
						View.AddSubview (_draggingView);

					_startingIndex = _draggingIndex;

					_draggingView.Center = locInScreen;
					//dragViewStartLocation = self.draggingView.center;

					_draggingView.Alpha = 0;
					_draggingView.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);

					UIView.Animate (AnimationLength, () => {
						_draggingView.Alpha = 0.8f;
						_draggingView.Transform = CGAffineTransform.MakeScale (1.2f, 1.2f);
					});


					/*
					[UIView animateWithDuration:.4f animations:^{
						CGAffineTransform transform = CGAffineTransformMakeScale(1.2f, 1.2f);
						self.draggingView.transform = transform;
					}];*/

					this.CollectionView.ReloadItems (new NSIndexPath[]{ _draggingIndex });
				}
			}

			if (this._draggingIndex == null || this._draggingView.Superview == null)
				return;

			View.BringSubviewToFront (_draggingView);
			this._draggingView.Center = locInScreen;




			if (sender.State == UIGestureRecognizerState.Changed) {

				nfloat strength = 0;
				nfloat scrollHeight = 80;
				nfloat maxStrength = 20;


				if (locInScreen.Y < scrollHeight + CollectionView.ContentInset.Top && CollectionView.ContentOffset.Y > -CollectionView.ContentInset.Top) {
					strength = (int)(NMath.Min (1, (CollectionView.ContentInset.Top + scrollHeight - locInScreen.Y) / scrollHeight + 0.2f) * -maxStrength);
				}

				if (CollectionView.Bounds.Height - locInScreen.Y < scrollHeight + CollectionView.ContentInset.Bottom
				    && CollectionView.ContentOffset.Y < CollectionView.ContentSize.Height - CollectionView.Bounds.Height + CollectionView.ContentInset.Bottom) {
					strength = (int)(NMath.Min (1, (CollectionView.ContentInset.Bottom + scrollHeight - CollectionView.Bounds.Height + locInScreen.Y) / scrollHeight + 0.2f) * maxStrength);
				}
				_strength = strength;
			}



			if ((_strength == 0 && sender.State == UIGestureRecognizerState.Changed) || sender.State == UIGestureRecognizerState.Ended) {

				var moveToIndex = CollectionView.IndexPathForItemAtPoint (loc);

				if (moveToIndex != null && moveToIndex.Row >= _data.Count) {
					moveToIndex = NSIndexPath.FromRowSection (_data.Count - 1, moveToIndex.Section);
				}

				if (moveToIndex != null && moveToIndex.Row != _draggingIndex.Row) {
					var data = _data [_draggingIndex.Row];
					_data.Remove (data);
					_data.Insert (moveToIndex.Row, data);

					var tmp = _draggingIndex;
					_draggingIndex = moveToIndex;

					CollectionView.MoveItem (tmp, moveToIndex);

				}
			}



			if (sender.State == UIGestureRecognizerState.Ended) {
				_strength = 0;

				var tmpIndex = _draggingIndex;
				_draggingIndex = null;


				UIView.Animate (AnimationLength, () => {
					_draggingView.Alpha = 0;
					_draggingView.Transform = CGAffineTransform.MakeScale (1.0f, 1.0f);
				}, () => {
					if (_draggingView.Alpha == 0)
						_draggingView.RemoveFromSuperview ();				
				});



				if (BlrtPermissions.CanMediaReorder) {

					if (_startingIndex.Row != tmpIndex.Row || _startingIndex.Section != tmpIndex.Section) {
						var data = _data [tmpIndex.Row];
					}
				} else {

					if (_startingIndex.Row != tmpIndex.Row || _startingIndex.Section != tmpIndex.Section) {
						BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.CanMediaReorder);


						var data = _data [tmpIndex.Row];
						_data.Remove (data);
						_data.Insert (_startingIndex.Row, data);

						CollectionView.MoveItem (tmpIndex, _startingIndex);

						tmpIndex = _startingIndex;
					}
				}

				this.CollectionView.ReloadItems (new NSIndexPath[]{ tmpIndex });
			}

			if (_strength != 0) {
				AnimateMovement ();
			}
		}

		NSTimer _timer;

		public void AnimateMovement ()
		{
		
			if (_strength != 0 && _timer == null) {

				_timer = NSTimer.CreateRepeatingScheduledTimer (0.01f, (t) => {

					InvokeOnMainThread (() => {

						nfloat d = NMath.Max (
							          -CollectionView.ContentInset.Top,
							          NMath.Min (CollectionView.ContentSize.Height - CollectionView.Bounds.Height + CollectionView.ContentInset.Bottom, CollectionView.ContentOffset.Y + _strength)
						          );

						CollectionView.ContentOffset = new CGPoint (
							CollectionView.ContentOffset.X, 
							d
						);
					});

					if (_strength == 0) {
						_timer.Invalidate ();
						_timer = null;
					}
				});
			}
		}



		public override nint NumberOfSections (UICollectionView collectionView)
		{
			return 1;
		}

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return _data.Count + 1;
		}

		IEnumerable<PageData> FindDuplicates (PageData pageData)
		{
			foreach (var d in _data) {
				if (d.page.SameMedia (pageData.page) && d.page.MediaPage == pageData.page.MediaPage)
					yield return d;
			}
		}

		void ReloadDuplicates (PageData pageDatas)
		{
			ReloadDuplicates (new[]{ pageDatas });
		}

		void ReloadDuplicates (IEnumerable<PageData> pageDatas)
		{

			var reloaders = new List<NSIndexPath> ();

			foreach (var item in pageDatas) {
				var duplicates = FindDuplicates (item);
				bool count = duplicates.Count () > 1;

				foreach (var dup in duplicates) {

					if (dup.hasDuplicate != count) {
						dup.hasDuplicate = count;

						if (item != dup) {
							reloaders.Add (NSIndexPath.FromRowSection (_data.IndexOf (dup), 0));
						}
					}
				}
			}

			if (reloaders.Count > 0) {
				ReloadCollectionView (reloaders.ToArray ());
			}
		}

		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell (_mediaCellIdentifer, indexPath) as BlrtMediaPageListControllerCell;

			if (!cell.EventsSetup) {
				cell.SetupEvents (CellSettingsPressed, CellCrossPressed);
			}

			if (indexPath.Row >= _data.Count) {
				cell.SetAddNewPage ();
			} else {

				var data = _data [indexPath.Row];

				ReloadDuplicates (data);
                cell.SetPage (data);
				if (data.thumbnail == null) {
					GenerateThumbnails ().FireAndForget ();
				}
			}

			if (_draggingIndex != null && _draggingIndex.Row == indexPath.Row && _draggingIndex.Section == indexPath.Section) {
				_draggingView.SetPage (_data [indexPath.Row]);
				cell.ContentView.Hidden = true;
			} else {
				cell.ContentView.Hidden = false;
			}

			return cell;
		}

		void CellSettingsPressed (object sender, EventArgs arg)
		{
			if (sender is UIView) {
				var btn = sender as UIView;
				var cell = BlrtUtilities.GetSuperOfType<BlrtMediaPageListControllerCell> (btn);

				if (cell != null) {
					var indexPath = CollectionView.IndexPathForCell (cell);

					var data 		= _data [indexPath.Row];

					var action 		= new UIActionSheet (BlrtHelperiOS.Translate ("media_view_option_title", "media"));

					nint delete 	= -999;
					nint hiddable	= -999;

					nint preview = action.AddButton (BlrtHelperiOS.Translate ("media_view_option_preview", "media"));

					if (data.Hidable)
						hiddable = action.AddButton (BlrtHelperiOS.Translate ("media_view_option_hide", "media"));

					nint duplicate = action.AddButton (BlrtHelperiOS.Translate ("media_view_option_duplicate", "media"));

					if (data.PageDeletable)
						delete = action.AddButton (BlrtHelperiOS.Translate ("media_view_option_delete", "media"));


					action.CancelButtonIndex = action.AddButton (BlrtHelperiOS.Translate ("media_view_option_cancel", "media"));

					action.Clicked += (object s1, UIButtonEventArgs e) => {

						if (e.ButtonIndex == preview) {
							Task.Delay (400).ContinueWith ((task) => {
								InvokeOnMainThread (() => {
									_parent.PreviewPage (indexPath.Row + 1);
								});
							});
						} 
						if (e.ButtonIndex == duplicate) {
							AddPages (new ICanvasPage [] { data.page }, new PagePermissions (false, true), false).FireAndForget ();
						}
						if (e.ButtonIndex == delete) {
							RemovePage (indexPath.Row);
						}
						if (e.ButtonIndex == hiddable) {
							HideShowPage (data);
							ReloadCollectionView (new NSIndexPath[]{ indexPath });
						}

					};

					action.ShowFrom (
						btn.Bounds,
						btn,
						true
					);

				}
			}
		}

		void HideShowPage (PageData data)
		{
			if (data.Hidable) {
				if (!BlrtPermissions.CanMediaShowHide) {
					BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.CanMediaShowHide);
					return;
				}
				if (data.hidden) {
					// will show a page
					if (GetTotalShowingPageCount () + 1 > BlrtPermissions.MaxBlrtPageCount) {
						BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtPageCount);
						return;
					} else if (GetConvoSizeByte (new PageData[] { data }) > BlrtPermissions.MaxConvoSizeKb * 1024.0f) {
						BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxConvoSizeKB);
						return;
					} else if (GetBlrtSizeByte (new PageData [] { data }) > BlrtPermissions.MaxBlrtSizeKb * 1024.0f) {
						BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxBlrtSizeKB);
						return;
					} else {
						data.hidden = false;
					}
				} else {
					// will hide a page
					data.hidden = true;
				}
			}
		}

		void CellCrossPressed (object sender, EventArgs e)
		{
			if (sender is UIView) {
				var btn = sender as UIView;
				var cell = BlrtUtilities.GetSuperOfType<BlrtMediaPageListControllerCell> (btn);

				if (cell != null) {
					var indexPath = CollectionView.IndexPathForCell (cell);
				
					if (indexPath != null)
						RemovePage (indexPath.Row);
				}
			}
		}

		public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{
			if (indexPath.Row >= _data.Count) {
				_parent.ShowAddMediaController (true);
			} else {
				HideShowPage (_data [indexPath.Row]);
			}
			collectionView.DeselectItem (indexPath, true);
			InvokeOnMainThread(() => {
				ReloadCollectionView (new NSIndexPath [] { indexPath });
			});
		}

		public async Task GenerateThumbnails ()
		{
			if (_generatingThumbnails)
				return;
			_generatingThumbnails = true;
			for (int i = 0; i < _data.Count; ++i) {

				try {
					var data = _data [i];
					if (data.thumbnail == null) {

						LLogger.WriteLine ("Media thumbnail needs to be generated");
						var thumbnailSize = (int)(BlrtMediaPageListControllerCell.CellSize * _scaleOfScreen);
						var path = await (data.page as IThumbnailable).GenerateThumbnail (new System.Drawing.Size (thumbnailSize, thumbnailSize));
						var thumbnail = UIImage.FromFile (path);
						//_allocatedImage.Add (thumbnail);
						LLogger.WriteLine ("Media thumbnail has been generated");

						var indexs = new List<NSIndexPath> ();

						for (int j = 0; j < _data.Count; ++j) {

							var single = _data [j];

							if (data.page.SameMedia (single.page) && data.page.MediaPage == single.page.MediaPage) {
								single.thumbnail = thumbnail;
								indexs.Add (NSIndexPath.FromRowSection (j, 0));
							}
						}
						thumbnail = null;
						if (indexs.Count > 0) {
							InvokeOnMainThread (() => {
								ReloadCollectionView (
									indexs.ToArray ()
								);
							});
						}
						LLogger.WriteLine ("Media thumbnail applied to {0} cells", indexs.Count);
					}
				} catch (Exception e) {
					LLogger.WriteLine ("Failed to generate thumbnail for Previewer; " + e.ToString ());
				}
			}
			_generatingThumbnails = false;
		}



		bool _appeared = false;
		List <NSIndexPath> _waitingIndexPath = null;

		void ReloadCollectionView (NSIndexPath[] indexPaths)
		{
			if (indexPaths == null)
				return;
			if (_appeared) {
				this.CollectionView.ReloadItems (indexPaths);
			} else {
				if (null == _waitingIndexPath) {
					_waitingIndexPath = new List<NSIndexPath> ();
				}
				foreach( var index in indexPaths){
					if(!_waitingIndexPath.Contains(index)){
						_waitingIndexPath.Add (index);
					}
				}
			}
		}

		public bool HiddenCalled = false;

		public void ViewHidden ()
		{
			HiddenCalled = true;
			_appeared = false;
			if (null != _waitingIndexPath) {
				_waitingIndexPath.Clear ();
			}
		}

		public void ViewPresent ()
		{
			_appeared = true;
			if (null != _waitingIndexPath && _waitingIndexPath.Count > 0) {
				InvokeOnMainThread (() => {
					this.CollectionView.ReloadItems(_waitingIndexPath.ToArray());
					_waitingIndexPath.Clear ();
				});
			}
		}


		public class PageData
		{
			public bool PageDeletable {
				get {
					return _mediaRemovable && (
					    (page.Format != BlrtMediaFormat.PDF || page.MediaPage == 1)
					    || hasDuplicate
					);
				}
			}

			public bool Hidable {
				get { return true; }
			}

			public bool MediaRemovable {
				get { return _mediaRemovable; }
			}

			public bool hasDuplicate;

			public UIImage thumbnail;

			public bool hidden;
			public ICanvasPage page;

			bool _mediaRemovable;

//			public PageData (ICanvasPage page)
//			{
//				this.page = page;
//				this.hidden = false;
//				this.thumbnail = null;
//
//				_mediaRemovable = true;
//			}

			public PageData (ICanvasPage page, PagePermissions permissions)
			{
				this.page = page;
				this.hidden = permissions.Hidden (page.ObjectId);
				this.thumbnail = null;

				_mediaRemovable = permissions.mediaRemovable;
			}
		}

		/// <summary>
		/// Thse permissions define the state of media in the media picker.
		/// </summary>
		public struct PagePermissions
		{
			public enum HiddenParameterFor
			{
				All, // all the media is intended 
				Some // some of the media is inted
			}

			// defines whether "hidden" is intended for all the media
			public readonly HiddenParameterFor hiddenParameterFor;

			/* 
			 * defines visibility of the media based on "hiddenParameterFor" variable.
			 * For media not included in "hiddenParameterFor", opposite of "hidden" (!hidden) is considered
			*/ 
			public readonly bool mediaHidden;

			// defines whether media can be removed
			public readonly bool mediaRemovable;

			/* 
			 * if the "hiddenParameterFor" is set to ".Some", this variable defines those 
			 * media ids "hidden" variable is intended for
			*/
			public readonly HashSet<string> mediaIdsToSetHiddenParamFor;

			public bool Hidden (string mediaId) {
				
				if (hiddenParameterFor == HiddenParameterFor.All || mediaIdsToSetHiddenParamFor.Contains (mediaId)) {
					return mediaHidden;
				}
				return !mediaHidden;
			}

			public PagePermissions(bool mediaHidden, bool mediaRemovable, HashSet<string> mediaIdsToSetHiddenParamFor = null)
			{
				this.mediaHidden = mediaHidden;
                this.mediaRemovable = mediaRemovable;
				this.mediaIdsToSetHiddenParamFor =  mediaIdsToSetHiddenParamFor;

				hiddenParameterFor = mediaIdsToSetHiddenParamFor != null 
					&& mediaIdsToSetHiddenParamFor.Count > 0 ? HiddenParameterFor.Some : HiddenParameterFor.All;
			}
		}
	}
}

