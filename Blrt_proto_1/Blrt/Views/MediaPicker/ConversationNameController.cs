using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;
using System.Net;
using BlrtData.IO;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.MediaPicker
{
	public class NameThatBlrtController: UINavigationController, IExecutionBlocker
	{
		public string ConversationName { get { return _rootController.ConversationName; } set { _rootController.ConversationName = value; } }
		public bool CreateWithPublicBlrt { get { return _rootController.CreateWithPublicBlrt; } set { _rootController.CreateWithPublicBlrt = value; } }
		public string PublicBlrtName { get { return _rootController.PublicBlrtName; } set { _rootController.PublicBlrtName = value; } }

		private ConversationNameController _rootController;
		public NameThatBlrtController():base()
		{
			_rootController = new ConversationNameController ();

			this.PushViewController (_rootController,false);
		}

		public Task<bool> WaitAsync ()
		{
			return _rootController.WaitAsync();
		}


		#region IExecutionBlocker implementation

		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			_rootController.OnDismiss (this, EventArgs.Empty);
			await _tcsLeaveThisScreen.Task;
			return true;
		}

		#endregion

		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				ExecutionPerformer.RegisterBlocker (this);
			}
			base.ViewWillAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}
	}

	public class ConversationNameController :  Util.KeyboardController
	{
		public override string ViewName { get { return "NewBlrt_AddMedia_NameTheBlrt"; } }

		const int MarginLeft = 10;
		const int MarginTop = 20;
		const int SmallMargin = 10;
		const int SIZE = 34;
		const int SWITCH_WIDTH = 80;

		TaskCompletionSource<bool> _tsc;

		UIBarButtonItem _cancelBtn;
		UIBarButtonItem _nextBtn;

		UILabel _allowPublicText;
		UISwitch _createPublicBlrtSwitch;
		UITextField _publicNameField;

		UIView _sep1, _sep2;
		UILabel _conversationNameLbl, _conversationDispLbl;

		UITextField _nameFeild;
		UILabel _publicSwitchDescription;

		public string ConversationName { get { return _nameFeild.Text.Trim (); } set { _nameFeild.Text = value; } }

		public bool CreateWithPublicBlrt{ get { return _createPublicBlrtSwitch.On; } set { _createPublicBlrtSwitch.On = value; } }

		public string PublicBlrtName{ get { return _publicNameField.Text.Trim (); } set { _publicNameField.Text = value; } }


		public ConversationNameController () : base ()
		{
			View.BackgroundColor = BlrtStyles.iOS.Gray2;

			_tsc = new TaskCompletionSource<bool> ();
		}

		public Task<bool> WaitAsync ()
		{
			return _tsc.Task;
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.EdgesForExtendedLayout = UIRectEdge.None;
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Preview: Name and Tag");

			Title = BlrtUtil.PlatformHelper.Translate ("blrt_name_title_create", "preview");

			_cancelBtn = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, OnDismiss);
			NavigationItem.SetLeftBarButtonItem (_cancelBtn, false);
			_nextBtn = new UIBarButtonItem (BlrtUtil.PlatformHelper.Translate ("blrt_name_next_create", "preview"), UIBarButtonItemStyle.Plain, OnNext){
				TintColor = BlrtStyles.iOS.AccentBlue
			};
			NavigationItem.SetRightBarButtonItem (_nextBtn, false);

			_conversationNameLbl = new UILabel (new CGRect (MarginLeft, MarginTop, View.Frame.Width - MarginLeft * 2, SIZE)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleBottomMargin,
				Text = BlrtUtil.PlatformHelper.Translate("convo_name_lbl", "preview")
			};

			_nameFeild = new BlrtTextField (new CGRect (MarginLeft, _conversationNameLbl.Frame.Bottom + SmallMargin, View.Frame.Width - MarginLeft * 2, SIZE)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				Placeholder = BlrtHelperiOS.Translate ("blrt_name_feild_placeholder", "preview"),
//				BackgroundColor = BlrtStyles.iOS.Gray2,
				Font = BlrtStyles.iOS.CellTitleFont
			};

			_conversationDispLbl = new UILabel (new CGRect (MarginLeft, _nameFeild.Frame.Bottom + SmallMargin*0.5f, View.Frame.Width - MarginLeft * 2, SIZE)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleBottomMargin,
				Text = BlrtUtil.PlatformHelper.Translate("create_with_public_description_a", "preview"),
				Lines = 0,
				Font = BlrtStyles.iOS.CellDetailFont,
				TextColor = BlrtStyles.iOS.Gray5
			};

			_sep1 = new UIView (new CGRect (MarginLeft, _conversationDispLbl.Frame.Bottom +SmallMargin*2, View.Frame.Width - MarginLeft * 2, 1)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				BackgroundColor = BlrtStyles.iOS.Gray3
			};

			_createPublicBlrtSwitch = new UISwitch () {
				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleBottomMargin,
				HorizontalAlignment = UIControlContentHorizontalAlignment.Right,
				VerticalAlignment = UIControlContentVerticalAlignment.Center
			};
			var switchSize = _createPublicBlrtSwitch.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
			_createPublicBlrtSwitch.Frame = new CGRect (View.Frame.Width - MarginLeft - switchSize.Width, _sep1.Frame.Bottom + SmallMargin*0.5f + (SIZE - switchSize.Height) * 0.5f, switchSize.Width, switchSize.Height);


			_allowPublicText = new UILabel (new CGRect (MarginLeft, _sep1.Frame.Bottom + SmallMargin*0.5f , View.Frame.Width - MarginLeft * 3 - switchSize.Width, SIZE)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				Text = BlrtHelperiOS.Translate ("create_with_public", "preview"),
			};

			_sep2 = new UIView (new CGRect (MarginLeft, _allowPublicText.Frame.Bottom + SmallMargin*0.5f , View.Frame.Width - MarginLeft * 2, 1)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin,
				BackgroundColor = BlrtStyles.iOS.Gray3
			};


			_publicSwitchDescription = new UILabel (new CGRect (MarginLeft, _sep2.Frame.Bottom + SmallMargin, View.Frame.Width - MarginLeft * 2, SIZE)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleBottomMargin,
				Text = BlrtHelperiOS.Translate ("create_with_public_description_b", "preview"),
				Lines = 0,
				Font = BlrtStyles.iOS.CellDetailFont,
				TextColor = BlrtStyles.iOS.Gray5
			};

			View.AddSubview (_conversationNameLbl);
			View.AddSubview (_nameFeild);
			View.AddSubview (_conversationDispLbl);
			View.AddSubview (_sep1);
			View.AddSubview (_allowPublicText);
			View.AddSubview (_createPublicBlrtSwitch);
			View.AddSubview (_sep2);
			View.AddSubview (_publicSwitchDescription);

			_createPublicBlrtSwitch.ValueChanged+= (sender, e) => {
				_nextBtn.Title = _createPublicBlrtSwitch.On ? BlrtUtil.PlatformHelper.Translate("blrt_name_next_next","preview"): BlrtUtil.PlatformHelper.Translate("blrt_name_next_create","preview");
			};

			_publicNameField = new BlrtTextField () {
				Placeholder = BlrtHelperiOS.Translate ("create_with_public_placeholder", "preview"),
//				BackgroundColor = BlrtStyles.iOS.Gray2,
				Font = BlrtStyles.iOS.CellTitleFont
			};
		}
			
		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			var adSize = _publicSwitchDescription.SizeThatFits (new CGSize (View.Frame.Width - MarginLeft * 2, nfloat.MaxValue));
			_publicSwitchDescription.Frame = new CGRect (MarginLeft, _sep2.Frame.Bottom + SmallMargin, View.Frame.Width - MarginLeft * 2, adSize.Height);
		}
			

		private void OnNext (object sender, EventArgs args)
		{
			if (!BlrtHelperiOS.TryValidateConversationName (_nameFeild.Text)) {
				return;
			}
			if(_createPublicBlrtSwitch.On){
				if(string.IsNullOrWhiteSpace(PublicBlrtName)){
					PublicBlrtName = _nameFeild.Text.Trim ();
				}
				this.NavigationController.PushViewController (new PublicNameController (this), true);
				return;
			}
			_tsc.SetResult (true);
		}
			

		public void OnDismiss (object sender, EventArgs args)
		{
			_tsc.SetResult (false);
		}


		private class PublicNameController: Util.KeyboardController{
			UIBarButtonItem _nextBtn;
			UILabel _publicInfoLbl, _publicBlrtNameLbl;
			ConversationNameController _parent;


			public PublicNameController(ConversationNameController parent){
				_parent= parent;
			}

			public override void ViewDidLoad ()
			{
				base.ViewDidLoad ();
				this.EdgesForExtendedLayout = UIRectEdge.None;
				View.BackgroundColor = BlrtStyles.iOS.Gray2;
				Title = BlrtUtil.PlatformHelper.Translate ("blrt_name_title_create", "preview");

				_nextBtn = new UIBarButtonItem ("Create", UIBarButtonItemStyle.Plain, OnNext){
					TintColor = BlrtStyles.iOS.AccentBlue
				};
				NavigationItem.SetRightBarButtonItem (_nextBtn, false);

				var cancelBtn = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, delegate {
					this.NavigationController.PopViewController(true);
				}){
					TintColor = BlrtStyles.iOS.AccentBlue
				};
				NavigationItem.SetLeftBarButtonItem (cancelBtn, false);

				_publicBlrtNameLbl = new UILabel (new CGRect (MarginLeft, MarginTop, View.Frame.Width - MarginLeft * 2, SIZE)) {
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight,
					Text = BlrtUtil.PlatformHelper.Translate ("public_name_lbl", "preview")
				};

				_parent._publicNameField.Frame = new CGRect (MarginLeft, _publicBlrtNameLbl.Frame.Bottom + SmallMargin, View.Frame.Width - MarginLeft * 2, SIZE);
				_parent._publicNameField.AutoresizingMask = UIViewAutoresizing.FlexibleWidth |UIViewAutoresizing.FlexibleBottomMargin;

				_publicInfoLbl = new UILabel (new CGRect (MarginLeft, _parent._publicNameField.Frame.Bottom + SmallMargin*0.5f, View.Frame.Width - MarginLeft * 2, SIZE)) {
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight,
					Text = BlrtHelperiOS.Translate ("create_with_public_description_c", "preview"),
					Font = BlrtStyles.iOS.CellDetailFont,
					TextColor = BlrtStyles.iOS.Gray5,
					Lines = 0
				};

				View.AddSubview (_publicBlrtNameLbl);
				View.AddSubview (_parent._publicNameField);
				View.AddSubview (_publicInfoLbl);
			}

			void OnNext(object sender, EventArgs args){
				var publicName = _parent._publicNameField.Text;
				if (string.IsNullOrWhiteSpace (publicName) || publicName.Trim ().Length > 56) {
					var alert = new UIAlertView (
						BlrtUtil.PlatformHelper.Translate ("public_blrt_name_invalid_title", "preview"),
						BlrtUtil.PlatformHelper.Translate ("public_blrt_name_invalid_message", "preview"),
						null,
						"OK"
					);
					alert.Show ();
					return;
				}
				_parent._tsc.SetResult (true);
			}
		}
	}
}

