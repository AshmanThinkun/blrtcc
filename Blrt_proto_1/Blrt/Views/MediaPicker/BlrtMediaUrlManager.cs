using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

using System.Net;
using BlrtData.IO;
using System.Threading;
using BlrtCanvas;
using BlrtCanvas.Pages;
using BlrtiOS.Util;

namespace BlrtiOS.Views.MediaPicker
{
	using MediaInfo = BlrtAddMediaTypes.DocumentPickerReturnedMediaInfo;
	public class BlrtMediaUrlManager
	{
		public int Count { get { return _items.Count; } }
		public int FinishedCount { get { return _items.Count (arg => arg.Finished); } }


		List<BlrtMediaUrlItem> _items;

		public BlrtMediaUrlManager (string [] urls)
		{
			_items = new List<BlrtMediaUrlItem> ();

			for (int i = 0; i < urls.Length; ++i) {
				if (!string.IsNullOrWhiteSpace (urls [i]))
					_items.Add (new BlrtMediaUrlItem (urls [i]));
			}
		}

		public BlrtDownloader [] GetDownloaders ()
		{

			return (from i in _items
					where !i.IsLocalFile && i.Downloader != null
					select i.Downloader
			).ToArray (); ;
		}

		public float GetProgress ()
		{

			long total = 0;
			long current = 0;

			float started = _items.Count;

			foreach (var item in _items) {

				if (item.Size == 0)
					started--;

				total += item.Size;

				if (item.Finished) {
					current += item.Size;
				} else if (item.Downloader != null) {
					current += item.Downloader.ReceivedSize;
				}
			}

			if (total == 0)
				return 0;

			return current * 1f / (total * _items.Count) * started;
		}


		public bool MissingDownloader ()
		{
			return _items.Exists ((BlrtMediaUrlItem i) => {
				return !(i.IsLocalFile && i.Downloader != null);
			});
		}

		public async Task TryLocalizeMedia (CancellationToken token)
		{

			var formatTasks = new List<Task> ();
			var semaphore = new SemaphoreSlim (8, 8);

			foreach (var item in _items) {

				//wait to get the format of the file, which the less this runs, the better
				await semaphore.WaitAsync (token);

				if (!token.IsCancellationRequested) {

					formatTasks.Add (item.GetFormat ().ContinueWith ((Task t) => {
						if (!token.IsCancellationRequested) {
							semaphore.Release ();
							formatTasks.Add (item.GetFile ());
						}
					}));
				}
			}

			while (formatTasks.Exists (task => task == null || !(task.IsCompleted || task.IsCanceled || task.IsFaulted))) {

				if (token.IsCancellationRequested)
					throw new TaskCanceledException ();

				await Task.Delay (120);
			}
		}

		public List<MediaInfo> PopMediaUrls ()
		{
			var media = new List<MediaInfo> ();

			var items = (from i in _items
						 where i.Finished
						 select i);
			
			foreach (var item in items) {
				media.Add (new MediaInfo (item.FileName, item.LocalPath, item.Format));
			}

			_items.RemoveAll (obj => items.Contains (obj));
			return media;
		}

		public async Task<IEnumerable<ICanvasPage>> PopMedia(){
			var items = (from i in _items
			           	where i.Finished
				select i);

			var output = new List<ICanvasPage> ();
			foreach (var m in (from i in _items where i.Finished select i.GetMediaPages()))
				output.AddRange (await m);

			_items.RemoveAll (obj => items.Contains(obj));

			return output;
		}

		public bool HasInternetProblem {
			get{
				return _items.Exists (arg => arg.State == BlrtMediaUrlState.InternetFailure);
			}
		}

		public bool HasFileTooLarge {
			get{
				return _items.Exists (arg => arg.State == BlrtMediaUrlState.FileTooLarge);
			}
		}

		public bool HasInvalidFormat {
			get{
				return _items.Exists (arg => arg.State == BlrtMediaUrlState.InvalidFormat);
			}
		}

		public void RemoveFileTooLarge(){
			_items.RemoveAll (arg => arg.State == BlrtMediaUrlState.FileTooLarge);		
		}

		public void RemoveInvalidFormat(){
			_items.RemoveAll (arg => arg.State == BlrtMediaUrlState.InvalidFormat);		
		}


		private enum BlrtMediaUrlState{

			Idle,
			Finished,

			InvalidFormat,
			InternetFailure,
			FormatNotAccepted,
			FileTooLarge,
		}

		private class BlrtMediaUrlItem{
		
			public string Source { get; private set;}
			public string LocalPath { get; private set;}
			public string FileName { get; private set; }

			public bool Finished { get { return State == BlrtMediaUrlState.Finished; }}
			public bool IsLocalFile { get; private set;}

			public BlrtMediaFormat Format { get; private set;}
			public long Size { get; private set;}



			public BlrtMediaUrlState State { get; private set;}
			public BlrtDownloader Downloader { get; private set;}
			public Exception Exception { get; private set;}

			public BlrtMediaUrlItem(string source){
			
				this.Source = source; 
				FileName = Path.GetFileName (System.Web.HttpUtility.UrlDecode (source));

				State = BlrtMediaUrlState.Idle;
				Format = BlrtMediaFormat.Invalid;

				if(source.Contains ("file://") || source.StartsWith("/")){
					IsLocalFile 	= true;
				} else {
					IsLocalFile 	= false;
				}
			}		

			public async Task  GetFormat(){

				if (Finished)
					return;

				Exception = null;

				if (IsLocalFile) {
					LocalAction ();
				} else {
					if(Format == BlrtMediaFormat.Invalid)
						await FindFormat();
				}
			}

			public async Task  GetFile(){

				if (Finished || Format == BlrtMediaFormat.Invalid)
					return;

				if (IsLocalFile) {
					LocalAction ();
				} else {
					await DownloadFile();
				}
			}




		
			void LocalAction(){

				if (Finished)
					return;

				var path = Source.Replace ("file://", "");
				path = System.Web.HttpUtility.UrlDecode (path);
				string extension = Path.GetExtension (path).ToLower ();

				if(string.IsNullOrEmpty(this.LocalPath)){

					this.LocalPath = BlrtRecentFileManager.GetUniqueFile(extension); 
				}



				Size = new FileInfo (path).Length;

				switch (extension.ToLower()) {
					case ".png":
					case ".jpeg":
					case ".jpg":
					case ".gif":
						Format = BlrtMediaFormat.Image;
						break;
					case ".pdf":
						Format = BlrtMediaFormat.PDF;	
						break;
					case ".mp3":
					case ".m4a":
					case ".wav":
						Format = BlrtMediaFormat.Audio;
						break;
					default:
						Format = BlrtMediaFormat.Invalid;
						State = BlrtMediaUrlState.InvalidFormat;
						return;
				}

				if (Format == BlrtMediaFormat.PDF && 1024 * BlrtPermissions.MaxPDFSizeKB < Size) {
					State = BlrtMediaUrlState.FileTooLarge;
					return;
				}

				File.Copy (path, LocalPath);
				State = BlrtMediaUrlState.Finished;
			}

			async Task DownloadFile(){

				if (Finished)
					return;

				if (Format == BlrtMediaFormat.PDF && 1024 * BlrtPermissions.MaxPDFSizeKB < Size) {
					State = BlrtMediaUrlState.FileTooLarge;
					return;
				}


				if(string.IsNullOrEmpty(this.LocalPath)){
					switch(Format){
						case BlrtMediaFormat.Image:
							this.LocalPath = BlrtRecentFileManager.GetUniqueFile(".png"); 
							break;
						case BlrtMediaFormat.PDF:
							this.LocalPath = BlrtRecentFileManager.GetUniqueFile(".pdf"); 
							break;
					}				
				}
				Downloader = BlrtDownloader.Download (1000, new Uri (Source), LocalPath);

				await Downloader.WaitAsync ();

				if (Downloader.IsFinished) {
					State = BlrtMediaUrlState.Finished;
				} else {
					State = BlrtMediaUrlState.InternetFailure;
					Exception = Downloader.Exception;
				}
			}


			async Task FindFormat(){
			
				if(Format != BlrtMediaFormat.Invalid || Finished){
					return;
				}





				HttpWebResponse response = null;

				if (!this.Source.Contains ("://"))
					this.Source = "http://" + this.Source;

				try{
					var request = HttpWebRequest.Create(this.Source) as HttpWebRequest;

					// This will open the chooser view
					if (request != null)
					{
						try{
							response = await BlrtUtil.AwaitOrCancelTask<WebResponse>(
								request.GetResponseAsync(), 
								new System.Threading.CancellationTokenSource(4500).Token
							) as HttpWebResponse;

						} catch {
							request.Abort();
							throw;
						}

						if (response != null) {

							Size = response.ContentLength;

							string 	contentType 	= response.ContentType;

							if(!string.IsNullOrWhiteSpace(contentType)){

								if(contentType.Contains(";"))
									contentType = contentType.Remove(contentType.IndexOf(";"));

								switch(contentType.ToLower()){
									case "application/pdf":
										Format = BlrtMediaFormat.PDF;

										if (1024 * BlrtPermissions.MaxPDFSizeKB < Size) {
											State = BlrtMediaUrlState.FileTooLarge;
											return;
										}
										break;
									case "image/gif":
									case "image/jpeg":
									case "image/pjpeg":
									case "image/png":
										Format = BlrtMediaFormat.Image;
										break;
								}
							}

							if(Format == BlrtMediaFormat.Invalid){
								State = BlrtMediaUrlState.InvalidFormat;
							}
						}
					}
				} catch (Exception ex) {
					State = BlrtMediaUrlState.InternetFailure;
					Exception = ex;
				} finally {
					if (response != null)
						response.Dispose ();
				}
			}

			public async Task<ICanvasPage[]> GetMediaPages(){
			
				if (!Finished) {
					throw new Exception ("Not local");		
				}

				switch (Format) {
					case BlrtMediaFormat.Image:
						var receivedImg = UIImage.FromFile (LocalPath);
						var parsedImage = await BlrtUtilities.ParseImage (receivedImg);
						if (parsedImage != receivedImg) {
							using(var data = parsedImage.AsJPEG(BlrtUtil.JPEG_COMPRESSION_QUALITY)){
								NSError e = null;
								data.Save(
									LocalPath,
									NSDataWritingOptions.Atomic,
									out e
								);
							}
						}
						return new ICanvasPage[] {
							new BlrtImagePage(LocalPath)
						};
					case BlrtMediaFormat.PDF:
					return BlrtUtilities.GetPDFCanvasPages (LocalPath).ToArray();
				}
				throw new NotSupportedException ();			
			}
		}
	}
}

