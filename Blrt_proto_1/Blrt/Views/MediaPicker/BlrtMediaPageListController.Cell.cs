using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;

namespace BlrtiOS.Views.MediaPicker
{
	public partial class BlrtMediaPageListController
	{
		private class BlrtMediaPageListControllerLayout : UICollectionViewFlowLayout
		{
			public BlrtMediaPageListControllerLayout () : base ()
			{
				float Margin = 0;
				float Padding = 20 - BlrtMediaPageListControllerCell.CellPadding;


				this.ItemSize = new CGSize (BlrtMediaPageListControllerCell.CellWidth, BlrtMediaPageListControllerCell.CellHeight);
				this.SectionInset = new UIEdgeInsets (Padding, Padding, Padding, Padding);

				this.MinimumInteritemSpacing = this.MinimumLineSpacing = Margin;
			}
		}

		/// <summary>
		/// Blrt media page list controller header.
		/// </summary>
		class BlrtMediaPageListControllerHeader : UICollectionReusableView
		{
			static int PADDING_V = 30; // total vertical padding inside header
			static string SHOW_ALL_BTN_TITLE = "Show all";
			static string HIDE_TITLE = "Hide all";
			static int BTN_SIZE_H = 60;
			static int BTN_SIZE_W = 130;
			static int BTN_SPACING = 10;

			internal static nfloat HEIGHT = BTN_SIZE_H + PADDING_V; // total header height

			UIButton ShowAllButton;
			UIButton HideAllButton;

			public event EventHandler ShowAllButtonClicked {
				add { ShowAllButton.TouchUpInside += value; }
				remove { ShowAllButton.TouchUpInside -= value; }
			}

			public event EventHandler HideAllButtonClicked {
				add { HideAllButton.TouchUpInside += value; }
				remove { HideAllButton.TouchUpInside -= value; }
			}


			public BlrtMediaPageListControllerHeader (IntPtr handle) : base (handle) 
			{
				BackgroundColor = BlrtConfig.BLRT_MID_GRAY;
				InitSubviews (); 
			}

			void OnShowAllButtonClicked (object sender, EventArgs e)
			{}

			void OnHideAllButtonClicked (object sender, EventArgs e)
			{}

			/// <summary>
			/// Initializes the sub-views.
			/// </summary>
			void InitSubviews ()
			{
				// set show-all button
				ShowAllButton = GetButtonForTitle (SHOW_ALL_BTN_TITLE);
				ShowAllButtonClicked += OnShowAllButtonClicked;

				// set hide-all button
				HideAllButton = GetButtonForTitle (HIDE_TITLE);
				HideAllButtonClicked += OnHideAllButtonClicked;

				AddSubview (ShowAllButton);
				AddSubview (HideAllButton);
			}

			/// <summary>
			/// Gets the button for title.
			/// </summary>
			/// <returns>The button for title.</returns>
			/// <param name="title">Title.</param>
			UIButton GetButtonForTitle (string title)
			{
				var btn = new UIButton (UIButtonType.System);
				btn.Layer.BorderColor = BlrtStyles.iOS.BlueGray.CGColor;
				btn.Layer.CornerRadius = 4;
				btn.Layer.BorderWidth = 1;
				btn.SetTitle (title, UIControlState.Normal);
				btn.Frame = new CGRect (0, 0, BTN_SIZE_W, BTN_SIZE_H);

				// highlight button when user presses the button
				btn.TouchUpInside += delegate {
					Animate (0.07, delegate {
						btn.BackgroundColor = BlrtStyles.iOS.BlueGray;
						btn.SetTitleColor (BlrtConfig.BLRT_MID_GRAY, UIControlState.Normal);
					},delegate {
						btn.BackgroundColor = BlrtConfig.BLRT_MID_GRAY;
						btn.SetTitleColor (BlrtStyles.iOS.Charcoal, UIControlState.Normal);
					});
				};

				return btn;
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var boundsMidX = Bounds.GetMidX ();
				var boundsMidY = Bounds.GetMidY ();

				double newShowAllBtnX = boundsMidX - (BTN_SPACING * 0.5) - BTN_SIZE_W;
				double currentShowAllBtnX = ShowAllButton.Frame.X;

				if (Math.Abs (newShowAllBtnX - currentShowAllBtnX) >= Double.Epsilon) {

					// layout show-all
					var btnY = boundsMidY - (BTN_SIZE_H * 0.5);
					ShowAllButton.Frame = new CGRect (newShowAllBtnX, btnY, BTN_SIZE_W, BTN_SIZE_H);

					// layout hide-all
					double newhideBtnX = boundsMidX + (BTN_SPACING * 0.5);
					HideAllButton.Frame = new CGRect (newhideBtnX, btnY, BTN_SIZE_W, BTN_SIZE_H);
				}
			}
		}

		class BlrtMediaPageListControllerCell : UICollectionViewCell
		{
			public static readonly int CellSize = BlrtHelperiOS.IsPhone ? 80 : 100;
			public static readonly int CellPadding = BlrtHelperiOS.IsPhone ? 8 : 12;


			public static readonly float CellWidth = CellSize + CellPadding * 2;
			public static readonly float CellHeight = CellSize + CellPadding * 2;


			UIButton _removeButton;


			UIView _bodyView;

			UIButton _cogButton;
			UIView _showImage;

			UIView _shadowView;
			UIImageView _imageView;
			UIActivityIndicatorView _loaderView;
			BlrtPageCounterView _pageCounter;

			public bool EventsSetup { get; private set; }

			EventHandler _settingClicked;
			EventHandler _crossClicked;

			public BlrtMediaPageListControllerCell ()
				: base (new CGRect (0, 0, CellWidth, CellHeight))
			{
				Initialise ();
			}

			public BlrtMediaPageListControllerCell (IntPtr handle)
				: base (handle)
			{
				Initialise ();
			}

			void Initialise ()
			{

				EventsSetup = false;

				_bodyView = new UIView () {
					Frame = new CGRect (0, 0, CellWidth, CellHeight),
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				};
				ContentView.AddSubview (_bodyView);


				_shadowView = new UIView () {
					Frame = new CGRect (CellPadding + 2, CellPadding + 2, CellSize, CellSize),
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					BackgroundColor = UIColor.Black
				};
				_bodyView.AddSubview (_shadowView);


				_imageView = new UIImageView () {
					Frame = new CGRect (CellPadding, CellPadding, CellSize, CellSize),
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					ContentMode = UIViewContentMode.Center
				};
				_bodyView.AddSubview (_imageView);



				_removeButton = new UIButton () {
					AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				};
				_removeButton.SetImage (UIImage.FromBundle ("mediapicker/cross-overlay.png"), UIControlState.Normal);
				_removeButton.SizeToFit ();
				_removeButton.Center = new CGPoint (CellPadding, CellPadding);
				ContentView.AddSubview (_removeButton);

				const float padding = 10;

				_cogButton = new UIButton () {
					AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				};
				_cogButton.SetImage (UIImage.FromBundle ("mediapicker/settings-icon.png"), UIControlState.Normal);
				_cogButton.Frame = new CGRect (CellPadding - padding, CellPadding + CellSize * 0.75f - padding, CellSize * 0.25f + padding * 2, CellSize * 0.25f + padding * 2);
				_bodyView.AddSubview (_cogButton);

				_pageCounter = new BlrtPageCounterView () {
					BackgroundColor = UIColor.FromRGBA (1, 1, 1, 0.90f)
				};
				//_pageCounter.Center = new PointF(CellPadding + CellSize * 0.125f, CellPadding + CellSize * 0.875f);
				_pageCounter.Frame = new CGRect (CellPadding + CellSize - _pageCounter.Bounds.Width - 0, CellPadding + CellSize - _pageCounter.Bounds.Height - 0, _pageCounter.Bounds.Width, _pageCounter.Bounds.Height);
				_pageCounter.ForceSinglePage = true;
				_bodyView.AddSubview (_pageCounter);



				_showImage = new UIImageView (UIImage.FromBundle ("mediapicker/tick-overlay.png")) {
					AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
					Center = new CGPoint (CellWidth * 0.5f, CellHeight * 0.5f)
				};
				_bodyView.AddSubview (_showImage);

				_loaderView = new UIActivityIndicatorView () {
					Frame = new CGRect (CellPadding, CellPadding, CellSize, CellSize),
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					ContentMode = UIViewContentMode.Center,
					ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge,
					HidesWhenStopped = true
				};
				_bodyView.AddSubview (_loaderView);



				_cogButton.TouchUpInside += (object sender, EventArgs e) => {
					if (_settingClicked != null)
						_settingClicked (_cogButton, e);
				};
				_removeButton.TouchUpInside += (object sender, EventArgs e) => {
					if (_crossClicked != null)
						_crossClicked (_cogButton, e);
				};
			}

			protected override void Dispose (bool disposing)
			{
				base.Dispose (disposing);
				BlrtUtil.BetterDispose (_removeButton);
				_removeButton = null;
				BlrtUtil.BetterDispose (_bodyView);
				_bodyView = null;
				BlrtUtil.BetterDispose (_cogButton);
				_cogButton = null;
				BlrtUtil.BetterDispose (_showImage);
				_showImage = null;
				BlrtUtil.BetterDispose (_shadowView);
				_shadowView = null;
				BlrtUtil.BetterDispose (_imageView);
				_imageView = null;
				BlrtUtil.BetterDispose (_pageCounter);
				_pageCounter = null;
				BlrtUtil.BetterDispose (_loaderView);
				_pageCounter = null;
			}

			public void SetupEvents (EventHandler settingClicked, EventHandler crossClicked)
			{

				EventsSetup = true;

				_settingClicked += settingClicked;
				_crossClicked += crossClicked;
			}


			public override UIView HitTest (CGPoint point, UIEvent uievent)
			{
				var view = base.HitTest (point, uievent);

				if (view is UIButton)
					return view;

				return view != null ? this : null;
			}


			public void SetAddNewPage ()
			{

				_showImage.Alpha = 0;
				_shadowView.Alpha = 0;

				_bodyView.Alpha = 1;

				_imageView.Image = UIImage.FromBundle ("mediapicker/media-plus.png");
				_imageView.BackgroundColor = UIColor.White;
				_imageView.ContentMode = UIViewContentMode.Center;

				_cogButton.Hidden = true;

				_removeButton.Hidden = true;
				_pageCounter.Hidden = true;
			}

			public void SetPage (PageData pageData)
			{
				_showImage.Alpha = pageData.hidden ? 0 : (pageData.Hidable ? 1 : 0.80f);
				_bodyView.Alpha = pageData.hidden ? 0.5f : 1;

				_imageView.Image = pageData.thumbnail;
				_imageView.BackgroundColor = UIColor.Gray;
				_imageView.ContentMode = UIViewContentMode.ScaleAspectFit;

				if (pageData.thumbnail == null) {
					_showImage.Alpha = 0;
					_loaderView.Hidden = false;
					_loaderView.StartAnimating ();
				} else {
					_loaderView.StopAnimating ();


					_shadowView.Alpha = 0.2f;
					_cogButton.Hidden = false;

					_removeButton.Hidden = !pageData.PageDeletable;
					_removeButton.Alpha = 1;

					if (pageData.page.Format == BlrtMediaFormat.PDF) {
						_pageCounter.Hidden = false;
						_pageCounter.SetPages (pageData.page.MediaPage);
					} else {
						_pageCounter.Hidden = true;
					}
				}
			}
		}
	}
}

