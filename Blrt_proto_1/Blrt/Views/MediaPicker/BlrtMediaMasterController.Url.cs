using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;
using System.Net;
using BlrtData.IO;

namespace BlrtiOS.Views.MediaPicker
{
	public partial class BlrtMediaMasterController : Util.BaseUIViewController, IBlrtAddMediaController
	{



		/*


		public async Task<AddMediaResult> ShowDownloadUrl (string url, BlrtMediaFormat format, string name)
		{
			string extension = "";

			switch (format) {
				case BlrtMediaFormat.Image:
					extension = ".png";
					break;
				case BlrtMediaFormat.PDF:
					extension = ".pdf";
					break;
			}



			string str = BlrtRecentFileManager.GetUniqueFile(extension);
			var d = BlrtDownloader.Download (1000, new Uri(url), str);

			var downloader = new BlrtDownloadAlertView (
				BlrtHelperiOS.Translate ("alert_media_downloading_title", "media"),
				BlrtHelperiOS.Translate ("alert_media_downloading_message", "media"),
				BlrtHelperiOS.Translate ("alert_media_downloading_cancel", "media")
			);


			downloader.Finished += (object s, EventArgs e) => {
			};

			downloader.Canceled += (object s, EventArgs e) => {
				d.Cancel();
			};

			downloader.Failed += (object s, EventArgs e) => {
				downloader.DismissWithClickedButtonIndex(downloader.CancelButtonIndex + 99999999, true);
				ShowDownloadUrlFailed(0, d.Exception);
			};

			downloader.Show ();
			downloader.StartDownloading (new BlrtDownloaderList(d));

			await d.AwaitAsync ();

			if (!d.Failed) {

				downloader.DismissWithClickedButtonIndex(downloader.CancelButtonIndex + 99999999, true);
				return await OpenFromFile(
					str, 
					format,
					name,
					false
				);
			}

			return d.Cancelled ? AddMediaResult.Cancelled : AddMediaResult.FailedWithInternetIssue;
		}



		async Task<AddMediaResult> OpenFromFile (string filepath, BlrtMediaFormat format, string name, bool encoded)
		{
			if (encoded) {
				filepath = System.Web.HttpUtility.UrlDecode (filepath);
			}


			if (filepath.StartsWith ("file://")) {
				filepath = filepath.Remove (0, "file://".Length);
			}

			IMediaPage[] pages = null;
			try{
				var fileinfo = new FileInfo(filepath);

				var alert = CheckInformationAlert (format, fileinfo.Length);

				if (alert != null){
					await BlrtHelperiOS.AwaitAlert(alert);
					return AddMediaResult.FailedWithPermissionIssue;
				}

				switch(format)
				{
					case BlrtMediaFormat.Image:
						var image = UIImage.FromFile(filepath);
						image = await BlrtAddMediaTypeController.ParseImage(image);
						pages = new IMediaPage[]{
							BlrtiOS.Media.BlrtImagePage.FromMediaAsset (image, name)
						};
						break;
					case BlrtMediaFormat.PDF:
						var pdf = BlrtPDF.FromFile(filepath);
						pages = BlrtPDFPage.FromMedia(pdf, name);
						break;
				}

				if(pages == null)
					throw new NullReferenceException();

				if(NavigationController != null)
					NavigationController.PopToRootViewController(true);

				DismissAddMediaController (true);

				return AddMedia (pages);

			} catch (Exception e) {

			}
			await BlrtHelperiOS.AwaitAlert (ShowUnsupportedFormat (0));
			return AddMediaResult.FailedWithUnknownFormat;
		}

		public UIAlertView ShowUnsupportedFormat (ImageMediaSource source)
		{
			if(false){
				var alert = new UIAlertView (
					            BlrtHelperiOS.Translate ("invalid_url_alert_title", "media"),
					            BlrtHelperiOS.Translate ("invalid_url_alert_message", "media"),
					            null,
					            BlrtHelperiOS.Translate ("invalid_url_alert_accept", "media")
				);
				alert.Show ();
				return alert;
			} else {
				var alert = new UIAlertView (
					            BlrtHelperiOS.Translate ("alert_unknown_format_title", "media"),
					            BlrtHelperiOS.Translate ("alert_unknown_format_message", "media"),
					            null,
					            BlrtHelperiOS.Translate ("alert_unknown_format_cancel", "media")
				            );
				alert.Show ();
				return alert;
			}

			return null;
		}

		void ShowDownloadUrlFailed (ImageMediaSource source, Exception ex) {
			if(ex != null){

				UIAlertView alert = null;

				if(ex is WebException){
					var we = ex as WebException;
					var resp = we.Response as HttpWebResponse;

					if(resp != null){
						switch(resp.StatusCode){
							case HttpStatusCode.BadRequest:
							case HttpStatusCode.Forbidden:
							case HttpStatusCode.NotFound:
							case HttpStatusCode.Gone:
							case HttpStatusCode.MovedPermanently:

								alert = new UIAlertView (
									BlrtHelperiOS.Translate ("alert_media_bad_server_response_title", "media"),
									string.Format(
										BlrtHelperiOS.Translate ("alert_media_bad_server_response_message", "media"),
										resp.StatusDescription										
									),
									null,
									BlrtHelperiOS.Translate ("alert_media_bad_server_response_cancel", "media")
								);

								break;
						}
					}
				}

				if(alert == null){
					alert = new UIAlertView (
						BlrtHelperiOS.Translate ("alert_media_failed_downloading_title", "media"),
						BlrtHelperiOS.Translate ("alert_media_failed_downloading_message", "media"),
						null,
						BlrtHelperiOS.Translate ("alert_media_failed_downloading_cancel", "media")
					);
				}

				alert.Show();
			}
		}
		*/









		int counter = 0;

		public void PushThinking (bool animate)
		{
			++counter;

			if (counter > 0) {
				View.BringSubviewToFront (_loading);
				_loading.Show (animate);
			}
		}

		public void PopThinking (bool animate)
		{
			--counter;

			if (counter <= 0) {

				_loading.Hide (animate);


			}
		}
	}
}

