using System;
using System.Collections.Generic;
using System.Linq;
using BlrtiOS.UI;
using CoreGraphics;
using System.Threading.Tasks;

using UIKit;
using Foundation;

using BlrtData;
using BlrtData.Media;
using BlrtCanvas;
using BlrtiOS.Views.MediaPicker;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS.Views.MediaPicker
{
	public interface IBlrtAddMediaController
	{

		string GAViewNamePrefix { get; }

		void ShowAddMediaTypeController (BlrtAddMediaType type, bool animated);


		Task AddMedia (ICanvasPage media);
		Task AddMedia (IEnumerable<ICanvasPage> media);
		Task AddMedia (ICanvasPage media, bool hidden);
		Task AddMedia (IEnumerable<ICanvasPage> media, bool hidden);


		void ShowAddMediaController (bool animated);
		void DismissAddMediaController (bool animated);



		bool CheckInformation (BlrtMediaFormat format, long size);
		int MediaCountLeft (BlrtMediaFormat format);

		void PushThinking (bool animate);
		void PopThinking (bool animate);

		Task OpenMediaFromUrl (MediaSource source, string [] url, UIView view, BlrtAddMediaTypes.MediaPickedCallback mediaPickedCallback);

		void PreviewPage (int page);

		IConversationScreen ConversationScreen { get; }
		string ConversationId { get; }
	}
}

