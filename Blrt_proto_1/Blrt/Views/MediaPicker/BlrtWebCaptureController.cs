using System;
using System.IO;
using CoreGraphics;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using BlrtData;
using BlrtiOS.UI;
using BlrtCanvas;
using BlrtCanvas.Pages;
using BlrtData.ExecutionPipeline;
using System.Net;
using BlrtShared;

namespace BlrtiOS.Views.MediaPicker
{
	public class BlrtWebCaptureController : UINavigationController, IExecutionBlocker
	{
		private static string _lastWebpage = null;

		BlrtWebCaptureInnerController _webviewController;

		const float MaxScreenShotHeight = 1600;
		const float MaxLongScreenShotHeight = 3200;

		public event EventHandler OnCancel;
		public event EventHandler<ICanvasPage[]> OnCapture;
		public event EventHandler<string> OnDownloadUrl;

		public BlrtLoaderCoverView _loader;

		public BlrtWebCaptureController () : base ()
		{
			_webviewController = new BlrtWebCaptureInnerController (this);
			PushViewController (_webviewController, false);

			View.BackgroundColor = UIColor.White;
		}

		protected override void Dispose (bool disposing)
		{
			_webviewController.Dispose ();
			_webviewController = null;
			base.Dispose (disposing);
		}

		#region IExecutionBlocker implementation

		TaskCompletionSource<bool> _tcsLeaveThisScreen = null;

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			_tcsLeaveThisScreen = new TaskCompletionSource<bool> ();
			Cancel (this, EventArgs.Empty);
			await _tcsLeaveThisScreen.Task;
			return true;
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			_loader = new BlrtLoaderCoverView (View.Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
			};
			View.AddSubview (_loader);
			_loader.Hide (false);
		}


		public override void ViewWillAppear (bool animated)
		{
			if (null == PresentedViewController) {
				// enter navigation stack
				ExecutionPerformer.RegisterBlocker (this);
			}
			base.ViewWillAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			if (null == PresentingViewController) {
				// leave navigation stack
				ExecutionPerformer.UnregisterBlocker (this);
				if (null != _tcsLeaveThisScreen) {
					_tcsLeaveThisScreen.SetResult (true);
				}
			}
		}


		void Cancel (object sender, EventArgs e)
		{
			if (OnCancel != null)
				OnCancel (this, e);
		}

		public void OpenUrl (string url)
		{

			if (_webviewController.webView == null)
				_webviewController.View.BackgroundColor = UIColor.White;


			_webviewController.GoSearch (url);
		}

		void CaptureCurrentWebpage ()
		{

			if (OnCapture == null)
				return;

			CaptureCurrentWebpageAsync ();
		}

		public Task CaptureCurrentWebpageAsync ()
		{

			return Task.Run (async delegate {
				try {
					var web = _webviewController.webView;

					nfloat scale = 1;

					InvokeOnMainThread (() => {
						web.DrawRect (web.Frame, new UIViewPrintFormatter ());
						scale = UIScreen.MainScreen.Scale;
					});

					UIGraphics.BeginImageContextWithOptions (new CGSize (web.Bounds.Width, web.Bounds.Height), false, scale);

					var context = UIGraphics.GetCurrentContext ();
					context.SetShouldAntialias (true);
					var renderingTask = new TaskCompletionSource<bool> ();

					// since UIWebView's layer is involved, it is not safe to run it on background thread 
					InvokeOnMainThread (delegate {
						web.Layer.RenderInContext (context);
						renderingTask.SetResult (true);
					});

					await renderingTask.Task;

					string path = BlrtRecentFileManager.GetUniqueFile (".png");
					using (var img = UIGraphics.GetImageFromCurrentImageContext ()) {
						NSError e = null;

						LLogger.WriteLine ("Long web page screenshot about to save");
						using (var data = img.AsPNG ()) {
							data.Save (
								path,
								NSDataWritingOptions.Atomic,
								out e
							);
						}
						LLogger.WriteLine ("Long web page screenshot saved");
					}

					UIGraphics.EndImageContext ();
					InvokeOnMainThread (() => {
						OnCapture (this, new []{ new BlrtImagePage (path) });
					});
				} catch (Exception e) {
					e.ToString ();
				}
			});
		}


		void CaptureWebpage (bool longPage)
		{
			CaptureWebpage (longPage, int.MaxValue);
		}


		void CaptureWebpage (bool longPage, int maxPageCount)
		{
			if (OnCapture == null)
				return;

			const bool removeView = true;

			View.BringSubviewToFront (_loader);
			_loader.Show (true);

			try {
				var web = _webviewController.webView;
				web.ScrollView.ContentOffset = new CGPoint (0, 0);

				web.ClipsToBounds = true;
				web.ClearsContextBeforeDrawing = true;

				web.DrawRect (web.Bounds, new UIViewPrintFormatter ());

				nfloat height = 0;
				nfloat width = 0;
				nfloat imageHeight = 0;
				int imageCount = 0;

				float maxHeight = longPage ? MaxLongScreenShotHeight : MaxScreenShotHeight;


				while (NMath.Abs (imageHeight - web.Frame.Height) > 1) {
					width = NMath.Min (BlrtPermissions.MaxImageResolution, web.ScrollView.ContentSize.Width);
					height = web.ScrollView.ContentSize.Height;


					height = NMath.Min (height, web.ScrollView.ContentSize.Height);

					imageCount = ((int)(height / maxHeight) + 1);

					imageHeight = (int)(height / imageCount);

					web.Frame = new CGRect (0, 0, width, imageHeight);
				}

				imageCount = Math.Min (maxPageCount, imageCount);


				if (removeView)
					web.RemoveFromSuperview ();
				
				web.DrawRect (web.Bounds, new UIViewPrintFormatter ());

				InvokeInBackground (async delegate {

					string[] paths = new string[imageCount];

					for (int i = 0; i < imageCount; ++i) {

						LLogger.WriteLine ("Multi web page {0} started", i);
						while (string.IsNullOrEmpty (paths [i])) {
							try {
								bool waiter = true;

								InvokeOnMainThread (() => {
									web.Frame = new CGRect (0, 0, width, imageHeight);
									web.ScrollView.ContentOffset = new CGPoint (0, i * imageHeight);

									web.DrawRect (web.Bounds, new UIViewPrintFormatter ());

									waiter = false;
								});

								while (waiter)
									System.Threading.Thread.Sleep (100);

								LLogger.WriteLine ("Multi web page {0} waiting", i);

								System.Threading.Thread.Sleep (500);
								LLogger.WriteLine ("Multi web page {0} positioning", i);

								UIGraphics.BeginImageContextWithOptions (new CGSize (width, imageHeight), true, 1);
								using (var context = UIGraphics.GetCurrentContext ()) {
									LLogger.WriteLine ("Multi web page {0} context created and loaded", i);

									var renderingTask = new TaskCompletionSource<bool> ();

									// since UIWebView's layer is involved, it is not safe to run it on background thread 
									InvokeOnMainThread (delegate {
										web.Layer.RenderInContext (context);
										renderingTask.SetResult (true);
									});

									await renderingTask.Task;

									LLogger.WriteLine ("Multi web page {0} rendered", i);
									string path = BlrtRecentFileManager.GetUniqueFile (".png");

									using (var img = UIGraphics.GetImageFromCurrentImageContext ()) {
										NSError e = null;

										LLogger.WriteLine ("Multi web page {0} as image", i);

										using (var data = img.AsPNG ()) {
											data.Save (
												path,
												NSDataWritingOptions.Atomic,
												out e
											);
										}
									}

									LLogger.WriteLine ("Multi web page {0} saved", i);
									paths [i] = path;
								}

							} catch {
								LLogger.WriteLine ("Multi web page {0} failed", i);
							} finally {
								UIGraphics.EndImageContext ();
							}
							LLogger.WriteLine ("Multi web page {0} finished", i);
						}
					}

					InvokeOnMainThread (() => {
						GC.Collect();

						//we don't have enough memory to reload the webview(and we don't need to, because we are going to dismiss it)
//						LLogger.WriteLine ("Multi web page about to load.");
//
//						web.Frame = new CGRect (0, 0, View.Bounds.Width, View.Bounds.Height - BlrtWebCaptureInnerController.ToolBarHeight);
//						_loader.Hide (true);
//
//						if (removeView)
//							_webviewController.View.AddSubview (web);
//
//						web.Frame = new CGRect (0, 0, width, imageHeight);

						_loader.Hide (true);

						var output = new BlrtImagePage[imageCount];

						for (int i = 0; i < imageCount; ++i) {
							output [i] = new BlrtImagePage (paths [i]);
						}

						OnCapture (this, output);
						LLogger.WriteLine ("Multi web page screenshot image finished");
					});	
				});
			} catch (Exception e) {
				e.ToString ();
			}
		}


		public void Download (string location)
		{
			if (OnDownloadUrl != null && !string.IsNullOrWhiteSpace (location))
				OnDownloadUrl (this, location);
		}



		private class BlrtWebCaptureInnerController : UIViewController
		{
			private const float IconPadding = 16;
			const string AboutBlank = "about:blank";
			public readonly static float ToolBarHeight = BlrtHelperiOS.IsPhone ? 44 : 0;


			public UIWebView webView;

			UIBarButtonItem _pageBackButton;
			UIBarButtonItem _pageForwardButton;


			UIBarButtonItem _cancelBtn;

			UIBarButtonItem _infoBtn;
			UIBarButtonItem _captureBtn;
			BlrtActionButton _captureBtnView;


			UIToolbar _toolBar;

			BlrtWebCaptureController _parent;
			WebPageFormat _format;

			BlrtTextField _textView;

			UIActivityIndicatorView _loading;
			UIButton _crossBtn;


			string _title;
			string _location;

			string _lastInput;
			NSUrl _lastRequested;

			bool _editing;
			string _blankPageHtml;

			WebPageFormat Format {
				get {
					return _format;
				}
				set {
					if (_format != value) {
						SetFormat (value);
					}
				}
			}

			public BlrtWebCaptureInnerController (BlrtWebCaptureController parent)
				: base ()
			{
				_parent = parent;
			}


			public override void DidReceiveMemoryWarning ()
			{
				base.DidReceiveMemoryWarning ();

				NSUrlCache.SharedCache.RemoveAllCachedResponses ();
			}

			protected override void Dispose (bool disposing)
			{
				webView.Dispose ();
				webView = null;
				base.Dispose (disposing);
			}

			public override void ViewDidLoad ()
			{
				base.ViewDidLoad ();

				EdgesForExtendedLayout = UIRectEdge.None;

				string localHtmlUrl = Path.Combine (NSBundle.MainBundle.BundlePath, "blank.html");
				_blankPageHtml = File.ReadAllText (localHtmlUrl);
				_blankPageHtml = _blankPageHtml.Replace ("{0}", BlrtHelperiOS.Translate ("capture_blank_page", "media"));

				_captureBtnView = new BlrtActionButton ("capture");


				webView = new UIWebView (new CGRect (0, 0, View.Frame.Width, View.Frame.Height - ToolBarHeight)) {
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					ScalesPageToFit = true,
				};
				View.AddSubview (webView);

				webView.ScrollView.Delegate = new StopZoomDelegate ();


				if (BlrtHelperiOS.IsPhone) {
					_toolBar = new UIToolbar (new CGRect (0, View.Frame.Height - ToolBarHeight, View.Bounds.Width, ToolBarHeight)) {
						AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleTopMargin,
					};
					View.AddSubview (_toolBar);
				} else {
				}

				_captureBtnView.TintAdjustmentMode = UIViewTintAdjustmentMode.Normal;


				_pageBackButton 	= new UIBarButtonItem (BlrtHelperiOS.FromBundleTemplate ("canvas_page_prev.png"), UIBarButtonItemStyle.Plain, null);
				_pageForwardButton	= new UIBarButtonItem (BlrtHelperiOS.FromBundleTemplate ("canvas_page_next.png"), UIBarButtonItemStyle.Plain, null);
				_infoBtn 			= new UIBarButtonItem (BlrtHelperiOS.FromBundleTemplate ("help_info.png"), UIBarButtonItemStyle.Plain, null);
				_captureBtn 		= new UIBarButtonItem (_captureBtnView);
				_cancelBtn 			= new UIBarButtonItem (UIBarButtonSystemItem.Cancel);


				_infoBtn.Clicked += SafeEventHandler.PreventMulti (InfoPressed);


				_captureBtnView.TintColor = BlrtConfig.BlrtBlack;

				//webView.ScalesPageToFit = true;

				webView.ShouldStartLoad += ShouldLoadUrl;
				webView.LoadStarted += SafeEventHandler.FromAction (
					(object sender, EventArgs e) => {
						Format = WebPageFormat.Unknown;
						RecalulateState ();
					}
				);
				webView.LoadFinished += SafeEventHandler.FromAction (
					(object sender, EventArgs e) => {
						Format = WebPageFormat.Unknown;
						RecalulateState ();
					}
				);

				webView.LoadError += SafeEventHandler.FromAction (
					(object sender, UIWebErrorArgs e) => {
						if (e.Error.Code == -1003 || e.Error.Code == -1100) {
							webView.LoadRequest (
								NSUrlRequest.FromUrl (
									GetSearchUrl (_lastInput)
								)
							);

						} else if (e.Error.Code != -999) {

							string str = BlrtHelperiOS.Translate ("web_failled_page_load", "media");

							webView.LoadHtmlString (
								"<html><head><title></title></head><body><p style=\"color:#4e4e4e;margin:48px 20px;text-align:center;font:24px sans-serif;\">"
								+ str
								+ "</p></body></html>",
								_lastRequested
							);
						}
					}
				);

				_cancelBtn.Clicked += _parent.Cancel;
				_captureBtnView.Click += SafeEventHandler.PreventMulti (Capture);

				_pageBackButton.Clicked += GoBack;
				_pageForwardButton.Clicked += GoForward;

				_textView = new UI.BlrtTextField () {
					Frame 					= new CGRect (0, 0, 1024, 32),
					AutoresizingMask 		= UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin,
					AutocorrectionType 		= UITextAutocorrectionType.No,
					AutocapitalizationType 	= UITextAutocapitalizationType.None,
					ContentInset 			= new UIEdgeInsets (0, 5, 0, 22),
					Font 					= BlrtConfig.NormalFont.Large,
					KeyboardType  			= UIKeyboardType.Default
				};



				_textView.EditingDidBegin += (object sender, EventArgs e) => {
					SetSearch (true);
				};
				_textView.EditingDidEnd += (object sender, EventArgs e) => {
					SetSearch (true);
				};
				_textView.ReturnKeyType = UIReturnKeyType.Go;
				_textView.ShouldReturn += GoSearch;


				if (BlrtHelperiOS.IsPhone) {
					_toolBar.SetItems (new UIBarButtonItem[] {
						_pageBackButton,
						new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
							Width = 32
						},
						_pageForwardButton,
						new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
						new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
						_infoBtn,
						new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
							Width = 16
						},
						_captureBtn
					}, false);
				}

				_crossBtn = new UIButton () {
					AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
				};
				_crossBtn.SetImage (UIImage.FromBundle ("mediapicker/browser-cross.png"), UIControlState.Normal);
				_crossBtn.SizeToFit ();
				_crossBtn.Frame = new CGRect (_textView.Bounds.Width - (IconPadding + _crossBtn.Bounds.Width), 0, (IconPadding + _crossBtn.Bounds.Width), _textView.Bounds.Height);
				_textView.AddSubview (_crossBtn);

				_loading = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
					AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
				};
				_loading.Center = new CGPoint (_textView.Bounds.Width - (IconPadding + _loading.Bounds.Width) * 0.5f, _textView.Bounds.Height * 0.5f);
				_textView.AddSubview (_loading);

				_crossBtn.TouchUpInside += (object sender, EventArgs e) => {
					_textView.BecomeFirstResponder ();
					_textView.Text = "";
				};


				NavigationItem.TitleView = _textView;

				SetFormat (WebPageFormat.Unknown);
				SetSearch (false);


				webView.AddGestureRecognizer (new UITapGestureRecognizer (() => {
					View.EndEditing (true);
				}) {
					CancelsTouchesInView = false
				});
			}

			public override void ViewWillDisappear (bool animated)
			{
				base.ViewWillDisappear (animated);
				webView.LoadHtmlString ("", new NSUrl (AboutBlank));
			}

			async Task InfoPressed (object sender, EventArgs e)
			{
				await BlrtHelperiOS.GenericAlertAsync (
					"capture_info_alert",
					"media"
				);
			}

			public void GoToBlank ()
			{
				webView.LoadHtmlString (
					_blankPageHtml,
					new NSUrl (AboutBlank)
				);
			}

			bool ShouldLoadUrl (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
			{
				_lastRequested = request.Url;
				return true;
			}

			void SetSearch (bool animated)
			{
				if (_textView.IsEditing) {

					if (!_editing) {
						if (_location == AboutBlank)
							_textView.Text = "";
						else
							_textView.Text = _location;

						_textView.SelectedTextRange = _textView.GetTextRange (_textView.BeginningOfDocument, _textView.EndOfDocument);
					}

					NavigationItem.SetLeftBarButtonItems (new UIBarButtonItem[0], animated);
					NavigationItem.SetRightBarButtonItems (new UIBarButtonItem[]{ }, animated); 

					_textView.BackgroundColor = BlrtConfig.BLRT_DARK_GRAY.ColorWithAlpha (0.75f);
					_textView.TextAlignment = UITextAlignment.Left;
				} else {

					if (BlrtHelperiOS.IsPhone) {

						NavigationItem.SetLeftBarButtonItems (new UIBarButtonItem[] {
							_cancelBtn,
						}, animated);

					} else {
						NavigationItem.SetLeftBarButtonItems (new UIBarButtonItem[] {
							_cancelBtn,
							new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
								Width = 16
							},
							_pageBackButton,
							_pageForwardButton,
							new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
								Width = 16
							},
						}, animated);
						NavigationItem.SetRightBarButtonItems (new UIBarButtonItem[] {
							_captureBtn,
							new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
								Width = 8
							},
							_infoBtn,
							new UIBarButtonItem (UIBarButtonSystemItem.FixedSpace) {
								Width = 16
							},
						}, animated); 
					}

					_textView.TextAlignment = UITextAlignment.Center;
					_textView.BackgroundColor = BlrtConfig.BLRT_DARK_GRAY.ColorWithAlpha (0.5f);
				}

				_editing = _textView.IsEditing;

				_captureBtnView.TintColor = BlrtConfig.BlrtBlack;

				RecalulateState ();
			}

			void SetFormat (WebPageFormat format)
			{
				if (_format != format) {
					_format = format;
					BeginInvokeOnMainThread (() => {
						switch (_format) {
							case WebPageFormat.Image:
								_captureBtnView.Text = BlrtHelperiOS.Translate ("web_capture_image",	"media");
								break;
							case WebPageFormat.PDF:
								_captureBtnView.Text = BlrtHelperiOS.Translate ("web_capture_pdf", "media");
								break;
							default:
								_captureBtnView.Text = BlrtHelperiOS.Translate ("web_capture_webpage",	"media");
								break;

						}
						_captureBtnView.Enable (true);
						_captureBtnView.SizeToFit ();
					});
				}
			}



			public override void ViewWillAppear (bool animated)
			{
				base.ViewWillAppear (animated);

				if (!string.IsNullOrWhiteSpace (_lastWebpage) && _lastWebpage != AboutBlank) {
					webView.LoadRequest (
						NSUrlRequest.FromUrl (NSUrl.FromString (_lastWebpage))
					);
				} else {
					GoToBlank ();
				}
			}

			public override void ViewDidAppear (bool animated)
			{
				base.ViewDidAppear (animated);
			}

			bool GoSearch (UITextField textField)
			{
				return GoSearch (textField.Text);
			}

			public bool GoSearch (string input)
			{
				_textView.ResignFirstResponder ();

				if (!string.IsNullOrWhiteSpace (input) && input.Contains(".")) {
					_lastInput = input.Trim ();
					var url = NSUrl.FromString (BlrtUtil.ParseUrl (_lastInput));

					if (url == null) {
						url = GetSearchUrl (_lastInput);										
					}

					_lastWebpage = url.AbsoluteString;

					webView.LoadRequest (
						NSUrlRequest.FromUrl (url)
					);
				}else if(!string.IsNullOrWhiteSpace(input)){
					_lastInput = input.Trim ();
					webView.LoadRequest (
						NSUrlRequest.FromUrl (
							GetSearchUrl (_lastInput)
						)
					);
				}else {
					GoToBlank ();
					_lastWebpage = AboutBlank;
				}


				RecalulateState ();
				return true;
			}

			NSUrl GetSearchUrl (string str)
			{
				return NSUrl.FromString ("http://www.google.com/search?q=" + System.Web.HttpUtility.UrlEncode (str.Trim ()));
			}


			void GoBack (object sender, EventArgs e)
			{
				webView.GoBack ();
			}

			void GoForward (object sender, EventArgs e)
			{
				webView.GoForward ();
			}

			async Task Capture (object sender, EventArgs e)
			{
				_parent._loader.Show (true);

				_format = WebPageFormat.Unknown;

				InvokeInBackground (() => {
					InvokeOnMainThread (() => {
						RecalulateState ();
					});
				});

				while (_format == WebPageFormat.Unknown) {
					await Task.Delay (100);
				}

				_parent._loader.Hide (true);

				switch (_format) {
					case WebPageFormat.Image:
					case WebPageFormat.PDF:
						_parent.Download (_location);
						break;
					default:

						if (sender is UIView) {
							var view = sender as UIView;

							nint current 	= -1;
							nint single 	= -1;
							nint multi 		= -1;

							nfloat totalHeight = webView.ScrollView.ContentSize.Height;

							if (totalHeight > webView.Bounds.Height) {

								var action = new UIActionSheet (
									             BlrtHelperiOS.Translate ("capture_options_title", "media")
								             );

								current = action.AddButton (BlrtHelperiOS.Translate ("capture_options_current", "media"));


								if (totalHeight > MaxScreenShotHeight)
									multi = action.AddButton (BlrtHelperiOS.Translate ("capture_options_multi", "media"));

								if (totalHeight > webView.Bounds.Height) {
								
									if (totalHeight > MaxLongScreenShotHeight)
										single = action.AddButton (BlrtHelperiOS.Translate ("capture_options_long", "media"));
									else
										single = action.AddButton (BlrtHelperiOS.Translate ("capture_options_single", "media"));
								}

								action.CancelButtonIndex = action.AddButton (BlrtHelperiOS.Translate ("capture_options_cancel", "media"));

								action.Clicked += (object s1, UIButtonEventArgs e1) => {

									if (e1.ButtonIndex == current)
										_parent.CaptureCurrentWebpage ();
									if (e1.ButtonIndex == single)
										_parent.CaptureWebpage (true);
									if (e1.ButtonIndex == multi)
										_parent.CaptureWebpage (false);


								};
								action.ShowFrom (view.Bounds, view, true);
							} else {
								_parent.CaptureCurrentWebpage ();
							}



						}

						break;
				}
			}

		


			Task _t;
			Object _toLock = new Object();
			void RecalulateState () {
				lock (_toLock) {
					if (webView!=null && (_t == null || _t.IsCompleted || !webView.IsLoading)) {
						var tcs = new TaskCompletionSource<bool> ();
						BeginInvokeOnMainThread (async () => {
							try{
								await RecalulateStateSingleRun ();
								tcs.TrySetResult (true);
							}catch{}
						});
						_t = tcs.Task;
					}
				}
			}

			async Task RecalulateStateSingleRun ()
			{
				if (webView == null)
					return;
				_title = webView.EvaluateJavascript (@"document.title");
				_location	= webView.EvaluateJavascript (@"window.location.href");

				if (Format == WebPageFormat.Unknown) {
					await CalculateState (_location);
				}

				if (!string.IsNullOrWhiteSpace (_location) && _location != AboutBlank) {
					_lastWebpage = _location;
				}


				if (!_textView.IsEditing)
					_textView.Text = string.IsNullOrWhiteSpace (_title) ? _location : _title;

				_pageBackButton.Enabled = webView.CanGoBack;
				_pageForwardButton.Enabled = webView.CanGoForward;

				if (_textView.IsEditing && !string.IsNullOrEmpty (_textView.Text)) {
					_crossBtn.Hidden = false;
					_textView.ContentInset = new UIEdgeInsets (0, 5, 0, (IconPadding + _crossBtn.Bounds.Width));
					_textView.SetNeedsLayout ();
					_loading.Hidden = true;
					_loading.StopAnimating ();
				} else if(webView.IsLoading) {
					_textView.ContentInset = new UIEdgeInsets (0, 5, 0, (IconPadding + _loading.Bounds.Width));
					_crossBtn.Hidden = true;
					_loading.Hidden = false;
					_loading.StartAnimating ();
				} else {
					_textView.ContentInset = new UIEdgeInsets (0, 5, 0, 5);			
					_crossBtn.Hidden = true;
					_loading.Hidden = true;
					_loading.StopAnimating ();
				}
					
			}

			string _preLocation, _preType;
			async Task CalculateState(string requestUrl){
				string contentType = "";

				var t = Task.Factory.StartNew (() => {
					try {
						if(requestUrl == _preLocation){
							contentType = _preType;
							return;
						}
						_preLocation = requestUrl;
						_preType = "";
						HttpWebRequest request = (HttpWebRequest)WebRequest.Create (requestUrl);
						using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
							contentType = response.ContentType;
							_preType = contentType;
						}
					} catch{}
				});

				try{
					await BlrtUtil.AwaitOrCancelTask(t, new System.Threading.CancellationTokenSource(10000).Token);
				}catch{}

				//fallback 
				if(string.IsNullOrWhiteSpace(contentType) && !string.IsNullOrWhiteSpace (requestUrl)){
					var index = requestUrl.LastIndexOf (".");
					if (index != -1){
						var ext = requestUrl.Substring (index);
						switch (ext.Trim ().ToLower ()) {
							case ".pdf":
								Format = WebPageFormat.PDF;
								break;
							case ".gif":
							case ".jpeg":
							case ".jpg":
							case ".png":
								Format = WebPageFormat.Image;
								break;
							default:
								Format = WebPageFormat.Webpage;
								break;
						}
						return;
					}
				}

				switch (contentType.Trim ().ToLower ()) {
					case "application/pdf":
						Format = WebPageFormat.PDF;
						break;
					case "image/gif":
					case "image/jpeg":
					case "image/pjpeg":
					case "image/png":
						Format = WebPageFormat.Image;
						break;
					default:
						Format = WebPageFormat.Webpage;
						break;
				}
			}

		}


		public class StopZoomDelegate : UIScrollViewDelegate
		{
			public override UIView ViewForZoomingInScrollView (UIScrollView scrollView)
			{
				return null;
			}
		}


		private enum WebPageFormat
		{
			Webpage,
			Image,
			PDF,
			Unknown
		}
	}
}

