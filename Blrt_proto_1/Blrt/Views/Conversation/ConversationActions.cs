using System;
using UIKit;
using BlrtData;
using MessageUI;
using BlrtData.Views.Helpers;
using BlrtData.Models.ConversationScreen;
using BlrtAPI;
using System.Threading.Tasks;
using System.Web;
using Social;
using Foundation;
using BlrtData.ExecutionPipeline;
using System.Collections.Generic;
using BlrtData.Views.Conversation;
using Xamarin.Forms;
using BlrtData.Contents;
using System.Linq;
using Parse;

namespace BlrtiOS.Views.Conversation
{
	public class ConversationActionBuilder : ConversationHandler.IActionBuilder
	{

		ConversationController _controller;

		public ConversationActionBuilder (ConversationController controller)
		{
			_controller = controller;
		}


		#region IActionBuilder implementation

		public ICellAction CloudAction (ConversationCellModel model)
		{
			return new CloudAction (_controller, model);
		}

		public ICellAction SettingsAction (ConversationCellModel model)
		{
			return new SettingAction (_controller, model);
		}

		#endregion
	}







	public class CloudAction : ICellAction
	{
		ConversationController _controller;
		ConversationCellModel _model;

		public CloudAction (ConversationController controller, ConversationCellModel model)
		{
			_controller = controller;
			_model = model;
		}

		#region ICellAction implementation

		public void Action (object sender)
		{
			switch (_model.CloudState) {
				case BlrtData.CloudState.ContentMissing:
					_controller.DownloadContent (_model);
					break;
				case BlrtData.CloudState.NotOnCloud:
					_controller.TryUploadItem (_model);
					break;
			}
		}

		#endregion
	}

	public class SettingAction : ICellAction
	{
		ConversationController _controller;
		ConversationCellModel _model;
		BlrtData.Conversation	_conversation{
			get{
				return BlrtManager.DataManagers.Default.GetConversation (_controller.ConversationId);
			}
		}
		ConversationItem _content{
			get{
				return BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			}
		}

		public SettingAction (ConversationController controller, ConversationCellModel model)
		{
			_controller = controller;
			_model = model;
		}

		#region ICellAction implementation

		string _url_blrt_sms, _url_blrt_facebook, _url_blrt_twitter, _url_blrt_email, _url_blrt_other = "";

		public void PrepareUrl(){  //we can hook this with the bubble on appearing method as well
			if (!_model.OnParse)
				return;
			Task.Factory.StartNew (() => {
				GenerateLink ("blrt_sms");
				GenerateLink ("blrt_facebook");
				GenerateLink ("blrt_twitter");
				GenerateLink ("blrt_email");
				GenerateLink ("blrt_other");
			});
		}

		public void InvalidateUrl(){
			_url_blrt_sms = _url_blrt_facebook =_url_blrt_twitter = _url_blrt_email = _url_blrt_other = "";
		}

		async Task GenerateLink(string urlType){
			switch(urlType){
				case "blrt_sms":
					if (string.IsNullOrEmpty (_url_blrt_sms) || !_url_blrt_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_sms = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_facebook":
					if (string.IsNullOrEmpty (_url_blrt_facebook) || !_url_blrt_facebook.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_facebook = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_twitter":
					if (string.IsNullOrEmpty (_url_blrt_twitter) || !_url_blrt_twitter.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_twitter = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_email":
					if (string.IsNullOrEmpty (_url_blrt_email) || !_url_blrt_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_email = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
				case "blrt_other":
					if (string.IsNullOrEmpty (_url_blrt_other) || !_url_blrt_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_blrt_other = await BlrtUtil.GenerateYozioUrl (_content, urlType);
					}
					break;
			}
		}

		UIView _sender;
		public void Action (object sender)
		{
			nint upload = -999;
			nint download = -999;
			nint blrtLink = -999;
			nint embededLink = -999;
			nint copyText = -999;
			nint markRead = -999;
			nint makePublic = -999;
			nint shareToFB = -999;
			nint shareToTwitter = -999;
			nint duplicateBlrt = -999;
			nint blrtThis = -999;

			var actionSheet = new UIActionSheet ();


			if (!_model.OnParse) {
			
				upload = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_upload", "conversation_screen"));

			} else {

				if (_model.CloudState == CloudState.ContentMissing) {
					download = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_download", "conversation_screen"));
				
				}

				if (_model.ContentType == BlrtContentType.Blrt
				    || _model.ContentType == BlrtContentType.BlrtReply) {

					if (!_conversation.DisAllowPublicBlrt && _model.Author.ObjectId == ParseUser.CurrentUser.ObjectId && !_model.IsPublicBlrt) {
						makePublic = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_make_public", "conversation_screen"));
					} else if (_model.IsPublicBlrt) {
						actionSheet.Title = "Public Blrt";
					}

					blrtLink = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_blrt_link", "conversation_screen"));

					embededLink = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_embed_code", "conversation_screen"));

					if (_model.Unread) {
						markRead = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_read", "conversation_screen"));
					}

					if(_model.IsPublicBlrt){
						shareToFB = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("Share on Facebook"));
						shareToTwitter = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("Share on Twitter"));
					}

					if(_model.Author.ObjectId == ParseUser.CurrentUser.ObjectId){
						duplicateBlrt = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_duplicate", "conversation_screen"));
					}
				} else if (_model.ContentType == BlrtContentType.Media) {
					blrtThis = actionSheet.AddButton ("Blrt this");
				}
			}

			if (_model.ContentType == BlrtContentType.Comment) {

				copyText = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_copy_text", "conversation_screen"));
			}

			actionSheet.CancelButtonIndex = actionSheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_cancel", "conversation_screen"));

			actionSheet.Clicked += (sender1, e) => {
				if (e.ButtonIndex == actionSheet.CancelButtonIndex)
					return;

				else if (e.ButtonIndex == upload) {
					_controller.TryUploadItem (_model);
				}
				else if (e.ButtonIndex == download) {
					_controller.DownloadContent (_model);
				}

				else if (e.ButtonIndex == makePublic) {
					MakePublicBlrt (_model.ContentId);
				}

				else if (e.ButtonIndex == markRead) {
					var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
					content.TryMarkRead ();
				}

				else if (e.ButtonIndex == blrtLink) {
					actionSheet.Dismissed += async delegate {
						BlrtLinkClicked (sender);
					};
					actionSheet.DismissWithClickedButtonIndex (actionSheet.CancelButtonIndex, true);
				}

				else if (e.ButtonIndex == embededLink) {
					actionSheet.Dismissed += async delegate {
						EmbededLinkClicked ();
					};
					actionSheet.DismissWithClickedButtonIndex (actionSheet.CancelButtonIndex, true);
				}

				else if (e.ButtonIndex == copyText) {
					UIPasteboard.General.String = _model.Text==null? "": _model.Text;
				}

				else if (e.ButtonIndex == shareToFB){
					FacebookSharePressed();
				}else if (e.ButtonIndex ==  shareToTwitter){
					TwitterSharePressed();
				}

				else if(e.ButtonIndex == duplicateBlrt){
					DuplicateBlrtPressed(_model.ContentId);
				} 

				else if (e.ButtonIndex == blrtThis) {
					_controller.BlrtThisMedia (new string [] { _model.ContentId }).FireAndForget ();				
				}
			};

			if (sender is UIView) {

				var view = _sender = sender as UIView;
				var rect = view.Bounds;
				rect.Inflate (10240, 0);


				actionSheet.ShowFrom (
					rect,
					view,
					true
				);
			}

			PrepareUrl ();
		}

		#endregion

		void BlrtLinkClicked (object sender)
		{
			nint copyLink, shareLink, emailLink, messageLink, fbLink;
			var acsheet = new UIActionSheet ();

			copyLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_copy_link", "conversation_screen"));
			shareLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_share_link", "conversation_screen"));
			emailLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_email_link", "conversation_screen"));
			messageLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_message_link", "conversation_screen"));
			fbLink = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_fb_link", "conversation_screen"));
			acsheet.CancelButtonIndex = acsheet.AddButton (BlrtUtil.PlatformHelper.Translate ("item_options_cancel", "conversation_screen"));

			acsheet.Clicked += (senders, es) => {
				if (es.ButtonIndex == copyLink) {
					CopyShareLinkPressed ();
				} else if (es.ButtonIndex == shareLink) {
					ShareShareLinkPressed ();
				} else if (es.ButtonIndex == emailLink) {
					EmailShareLinkPressed ();
				} else if (es.ButtonIndex == messageLink) {
					MessageShareLinkPressed ();
				} else if (es.ButtonIndex == fbLink) {
					FbMessageShareLinkPressed ();
				}
			};

			if (sender is UIView) {

				var view = sender as UIView;
				var rect = view.Bounds;
				rect.Inflate (10240, 0);

				acsheet.ShowFrom (
					rect,
					view,
					true
				);
			}
		}

		async void CopyShareLinkPressed ()
		{
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			_controller.PushLoading ("", true);
			await GenerateLink ("blrt_other");
			_controller.PopLoading ("", true);
			var url = _url_blrt_other;

			UIPasteboard.General.String = url;
			new UIAlertView (
				BlrtHelperiOS.Translate ("alert_copy_url_title",	"conversation_screen"),
				BlrtHelperiOS.Translate ("alert_copy_url_message",	"conversation_screen"),
				null,
				BlrtHelperiOS.Translate ("alert_copy_url_accept",	"conversation_screen")
			).Show ();
		}

		public async void EmailShareLinkPressed ()
		{
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			_controller.PushLoading ("", true);
			await GenerateLink ("blrt_email");
			_controller.PopLoading ("", true);
			var url = _url_blrt_email;
		
			var conversation = BlrtManager.DataManagers.Default.GetConversation (content.ConversationId);

			if (_model.IsPublicBlrt) {
				var body = await BlrtUtil.GetEmailBodyUsing (bodyTemplate: BlrtHelperiOS.Translate ("email_public_blrt_url_message_body_html", "conversation_screen"),
														  	 itemUrl: url,
														  	 itemName: _model.PublicName,
															 forPublicItem: true,
															 forEmbedCode: false);
				ShowEmailViewController (
					string.Format (BlrtHelperiOS.Translate ("email_public_blrt_url_subject", "conversation_screen"), _model.PublicName),
					body,
					true,
					!_model.IsPublicBlrt
				);
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
			} else {
				var body = await BlrtUtil.GetEmailBodyUsing (bodyTemplate: BlrtHelperiOS.Translate ("email_blrt_url_message_body_html", "conversation_screen"),
														  	itemUrl: url,
														  	itemName: conversation.Name,
															forPublicItem: false,
				                                            forEmbedCode: false);
				ShowEmailViewController (
					string.Format (BlrtHelperiOS.Translate ("email_blrt_url_subject", "conversation_screen"), conversation.Name),
					body,
					true,
					!_model.IsPublicBlrt
				);
			}
		}

		public async void MessageShareLinkPressed ()
		{
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			_controller.PushLoading ("", true);
			await GenerateLink ("blrt_sms");
			_controller.PopLoading ("", true);
			var url = _url_blrt_sms;
			string blrtName = null;
			if (_model.IsPublicBlrt) {
				blrtName = _model.PublicName;
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
			} else {
				var conversation = BlrtManager.DataManagers.Default.GetConversation (content.ConversationId);
				blrtName = conversation.Name;
			}
			ShowSMSViewController (blrtName, url);
		}

		public async void ShareShareLinkPressed(){
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			_controller.PushLoading ("", true);
			await GenerateLink ("blrt_other");
			_controller.PopLoading ("", true);
			var url = _url_blrt_other;

			var activeVc = new UIActivityViewController (new NSObject[]{NSUrl.FromString(url)}, null);

			if(content.IsPublicBlrt){
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
			}

			if (BlrtHelperiOS.IsPhone) {
				_controller.PresentViewController (activeVc, true, null);
			}else{
				var popup = new UIPopoverController(activeVc);
				if(_sender!=null){
					var rect = _sender.Bounds;
					rect.Inflate (10240, 0);
					Device.BeginInvokeOnMainThread(()=>{
						popup.PresentFromRect(rect, _sender, UIPopoverArrowDirection.Any, false);
						
					});
				}else{
					Device.BeginInvokeOnMainThread(()=>{
						popup.PresentFromRect (new CoreGraphics.CGRect(235,225,10,10), _controller.View, UIPopoverArrowDirection.Any, false);
					});
				}
			}
		}

		public async void FbMessageShareLinkPressed(){
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			_controller.PushLoading ("", true);
			await GenerateLink ("blrt_facebook");
			_controller.PopLoading ("", true);
			var url = _url_blrt_facebook;

			var nsurl = new NSUrl (url);
			FBManager.TrySendFBMessenger (nsurl, _controller);
		}

		void ShowSMSViewController (string blrtName, string url)
		{
			var bodyTemplate = BlrtHelperiOS.Translate ("message_blrt_url_sms_body", "conversation_screen");
			var sendSmsController = BlrtiOS.Util.BlrtUtilities.TryGetSendSmsViewController (bodyTemplate, blrtName, url);

			if (sendSmsController == null) {
				return;
			}

			sendSmsController.Finished += delegate {
				sendSmsController.DismissViewController (true, null);
			};

			sendSmsController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

			_controller.BeginInvokeOnMainThread (() => {
				_controller.PresentViewController (sendSmsController, true, null);
			});
		}

		public void EmbededLinkClicked ()
		{
			var page = new EmbedPreviewPage (_content.ObjectId, _content.IsPublicBlrt);
			var con = page.CreateViewController ();
			con.Title = page.Title;
			con.NavigationItem.SetLeftBarButtonItem (new UIBarButtonItem(UIImage.FromBundle("cross.png"), UIBarButtonItemStyle.Plain, (s,v)=>{
				con.DismissViewController(true,null);
			}),false);
			var nav = new Util.BaseUINavigationController (con);
			nav.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

			page.LearnMorePressed += (sender, e) => {
				var webPage = new BlrtData.Views.Login.SimpleWebPage (true){
					Source = new UrlWebViewSource() {
						Url = BlrtSettings.EmbedPreviewLearnMore
					},
					Title = "Learn More",
				};
				var webController = webPage.CreateViewController();
				webController.Title = webPage.Title;
				nav.PushViewController(webController, true);
				var dic = new Dictionary<string,object>();
				dic.Add("blrtId", _content.ObjectId);
				dic.Add("type", _content.IsPublicBlrt? "public": "private");
				if(BlrtData.Helpers.BlrtTrack.LastConvoDic!=null){
					foreach(var kv in BlrtData.Helpers.BlrtTrack.LastConvoDic){
						dic.Add(kv.Key,kv.Value);
					}
				}

				BlrtData.Helpers.BlrtTrack.TrackScreenView("Conversation_EmbedPreview_LearnMore",false, dic);
			};

			page.CopyPlayerPressed += (sender, e) => {
				CopyEmbedCodePressed(e);
			};

			page.EmailPlayerPressed += async (sender, e) => {
				EmailEmbedCodePressed(nav, e);
			};

			page.CopySnippetPressed += (sender, e) => {
				CopyEmbedCodePressed(e, page.Theme, false);
			};

			page.EmailSnippetPressed += (sender, e) => {
				EmailEmbedCodePressed(nav, e, page.Theme, false);
			};


			_controller.PresentViewController (nav,true,null);

			if(_content.IsPublicBlrt){
				BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (_content.ConversationId, _content.ObjectId, (_content as ConversationBlrtItem).PublicBlrtName);
			}
		}

		void CopyEmbedCodePressed(string html, EmbedPreviewPage.SnippetTheme theme = EmbedPreviewPage.SnippetTheme.Light, bool isWebplayer=true){
			UIPasteboard.General.String = 
				string.Format (
					BlrtUtil.PlatformHelper.Translate ("copy_embed_string", "conversation_screen"),
					_model.IsPublicBlrt ? "Public " : "",
					isWebplayer? " - web player": string.Format(" - snippet ({0} theme)", theme == EmbedPreviewPage.SnippetTheme.Light ? "light": "dark"),
					html
				);
			new UIAlertView (
				BlrtHelperiOS.Translate ("copy_embed_to_clipboard_title",	"conversation_screen"),
				BlrtHelperiOS.Translate ("copy_embed_to_clipboard_message",	"conversation_screen"),
				null,
				BlrtHelperiOS.Translate ("copy_embed_to_clipboard_cancel",	"conversation_screen")
			).Show ();
		}

		async void EmailEmbedCodePressed(UINavigationController nav, string html, EmbedPreviewPage.SnippetTheme theme = EmbedPreviewPage.SnippetTheme.Light, bool isWebplayer=true){
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			await GenerateLink ("blrt_email");
			var url = _url_blrt_email;

			if (_model.IsPublicBlrt) {
				var body = await BlrtUtil.GetEmailBodyUsing (bodyTemplate: BlrtUtil.PlatformHelper.Translate ("public_blrt_body", "conversation_screen"),
														  	 itemUrl: url,
														  	 itemName: _model.PublicName,
															 forPublicItem: true,
															 forEmbedCode: true, 
				                                             encodedDataForEmbedCode: HttpUtility.HtmlEncode(html));
				
				if (isWebplayer) {
					body = body.Replace (@"{EMBED_TITLE_EXTRA}", " - web player");
					body = body.Replace (@"{CODE_TYPE}", "Blrt Web Player");
				}else{
					body = body.Replace(@"{EMBED_TITLE_EXTRA}", string.Format(" - snippet ({0} theme)", theme == EmbedPreviewPage.SnippetTheme.Light ? "light": "dark"));
					body = body.Replace(@"{CODE_TYPE}", "Blrt Snippet");
				}

				var emailController = ShowEmailViewController (
					string.Format (BlrtUtil.PlatformHelper.Translate ("public_blrt_subject", "conversation_screen"), _model.PublicName),
					body,
					true,false,false
				);
				if(emailController!=null)
					nav.PresentViewController(emailController,true,null);

			} else {
				var body = await BlrtUtil.GetEmailBodyUsing (bodyTemplate: BlrtUtil.PlatformHelper.Translate ("private_blrt_body", "conversation_screen"),
														  	 itemUrl: url,
														  	 itemName: _conversation.Name,
															 forPublicItem: false,
															 forEmbedCode: true,
															 encodedDataForEmbedCode: HttpUtility.HtmlEncode (html));
				
				if (isWebplayer) {
					body = body.Replace (@"{EMBED_TITLE_EXTRA}", " - web player");
					body = body.Replace (@"{CODE_TYPE}", "Blrt Web Player");
				}else{
					body = body.Replace(@"{EMBED_TITLE_EXTRA}", string.Format(" - snippet ({0} theme)", theme == EmbedPreviewPage.SnippetTheme.Light ? "light": "dark"));
					body = body.Replace(@"{CODE_TYPE}", "Blrt Snippet");
				}

				var emailController = ShowEmailViewController (
					string.Format (BlrtUtil.PlatformHelper.Translate ("private_blrt_subject", "conversation_screen"), _conversation.Name),
					body,
					true,false,false
				);
				if(emailController!=null)
					nav.PresentViewController(emailController,true,null);
			}
		}

		UIViewController ShowEmailViewController (string subject, string body, bool isHtml, bool showAlert = false, bool presentController = true)
		{
			var sendEmailController = BlrtiOS.Util.BlrtUtilities.TryGetSendEmailViewController (null, subject, body, isHtml);

			if (sendEmailController == null) {
				return null;
			}

			try {
				sendEmailController.Finished += (sender, e) => {
					sendEmailController.BeginInvokeOnMainThread (() => {
						sendEmailController.DismissViewController (true, null);
					});
				};
				sendEmailController.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;

				if(presentController){
					_controller.BeginInvokeOnMainThread(()=>{
						if(_controller.PresentedViewController!=null && Device.Idiom == TargetIdiom.Phone){
							_controller.DismissViewController(false,()=>{
								_controller.PresentViewController (sendEmailController, true, null);
							});
						}else{
							_controller.PresentViewController (sendEmailController, true, null);
						}
					});
				}

				if (showAlert) {
					new UIAlertView (
						BlrtHelperiOS.Translate ("alert_before_send_url_title", "conversation_screen"),
						BlrtHelperiOS.Translate ("alert_before_send_url_message",	"conversation_screen"),
						null,
						BlrtHelperiOS.Translate ("alert_before_send_url_accept",	"conversation_screen")
					).Show ();
				}
			} catch {
				return null;
			}
			return sendEmailController;
		}


		public async void MakePublicBlrt (string blrtId, string publicName = "")
		{

			string name = publicName;
			if (string.IsNullOrWhiteSpace (publicName)) {
				//get public name
				name = await GetPublicName ();
				if (string.IsNullOrWhiteSpace (name))
					return;
			}

			var apiCall = new APIConversationItemPublic (
				              new APIConversationItemPublic.Parameters () {
					BlrtId = blrtId,
					Name = name
				}
			              );

			var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
			app.PushLoading (true);
			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
				if (apiCall.Response.Success) {
					//tracking
					BlrtData.Helpers.BlrtTrack.ConvoMakeBlrtPublic (_content.ConversationId, _content.ObjectId, name);

					await _controller.Refresh ();
					InvalidateUrl ();
					PrepareUrl ();
					app.PopLoading (true);
					return;
				}
				app.PopLoading (true);
				switch (apiCall.Response.Error.Code) {
					case APIConversationItemPublicError.conversationPrivate:
						try {
							var query = BlrtData.Query.BlrtConversationQuery.RunQuery (
								_conversation.ObjectId, -1, 0
							);
							query.WaitAsync ();
						} catch {}

						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("conversation_private_title", "conversation_screen"), 
							BlrtUtil.PlatformHelper.Translate ("conversation_private_message", "conversation_screen"), 
							BlrtUtil.PlatformHelper.Translate ("OK"));
						break;
					default:
						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("OK"));
						break;
				}

			} else {
				app.PopLoading (true);
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
		}

		async Task<string> GetPublicName ()
		{
			var tcs = new TaskCompletionSource<bool> ();
			var alert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("public_name_title", "conversation_screen"),
	            BlrtUtil.PlatformHelper.Translate ("public_name_message", "conversation_screen"),
	            null,
	            BlrtUtil.PlatformHelper.Translate ("public_name_cancel", "conversation_screen"),
	            BlrtUtil.PlatformHelper.Translate ("public_name_accept", "conversation_screen")
            );

			alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKeyboardType.Default;
			alert.GetTextField (0).Placeholder = BlrtUtil.PlatformHelper.Translate ("public_name_placeholder", "conversation_screen");

			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				tcs.TrySetResult ((alert.CancelButtonIndex != e.ButtonIndex));
			};

			alert.Show ();

			if (!await tcs.Task)
				return "";

			//check that the value given is usable
			var name = alert.GetTextField (0).Text;
			if (string.IsNullOrWhiteSpace (name) || name.Trim ().Length >= 56) {
				var invalidAlert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("public_blrt_name_invalid_title", "preview"),
                    BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_message", "preview"),
                    null,
                    "OK"
                );
				invalidAlert.Show ();
				await BlrtHelperiOS.AwaitAlert (invalidAlert);
				return await GetPublicName ();
			} else {
				return name;
			}
		}


		public async Task<bool> DisplayAlert (string title, string message, string cancel)
		{
			var tcs = new TaskCompletionSource<bool> ();
			var alert = new UIAlertView (title, message, null, cancel);
			alert.Clicked += (sender, e) => {
				tcs.TrySetResult (true);
			};
			alert.Show ();

			return await tcs.Task;
		}

		public async void FacebookSharePressed()
		{
			_controller.PushLoading("facebookShare", true);
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			var slComposer = SLComposeViewController.FromService (SLServiceType.Facebook);
			await GenerateLink ("blrt_facebook");
			var url = _url_blrt_facebook;

			slComposer.AddUrl (NSUrl.FromString (url));
			slComposer.SetInitialText (string.Format(BlrtUtil.PlatformHelper.Translate ("Public Blrt: {0}"), _model.PublicName));
			Device.BeginInvokeOnMainThread (() => {
				_controller.PresentViewController (slComposer, true, ()=>{
					_controller.PopLoading("facebookShare",true);
				});
			});

			BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);
		}

		public async void TwitterSharePressed()
		{
			_controller.PushLoading ("twitterShare", true);
			var content = BlrtManager.DataManagers.Default.GetConversationItem (_model.ContentId);
			var slComposer = SLComposeViewController.FromService (SLServiceType.Twitter);
			await GenerateLink ("blrt_twitter");
			var url = _url_blrt_twitter;

			slComposer.AddUrl (NSUrl.FromString (url));
			slComposer.SetInitialText (string.Format(BlrtUtil.PlatformHelper.Translate ("Public Blrt: {0}"), _model.PublicName));
			Device.BeginInvokeOnMainThread (() => {
				_controller.PresentViewController (slComposer, true, () => {
					_controller.PopLoading ("twitterShare", true);
				});
			});
			BlrtData.Helpers.BlrtTrack.ConvoSharePublicBlrt (content.ConversationId, content.ObjectId, (content as ConversationBlrtItem).PublicBlrtName);

		}


		public async void DuplicateBlrtPressed (string blrtId, string conversationName = "")
		{
			string name = conversationName;
			if (string.IsNullOrWhiteSpace (conversationName)) {
				//get convo name
				name = await GetDuplicateConversationName (_conversation.Name + " copy");
				if (string.IsNullOrWhiteSpace (name))
					return;
			}

			var apiCall = new APIConversationDuplicateFromBlrt (
				new APIConversationDuplicateFromBlrt.Parameters () {
					ConversationItemId = blrtId,
					ConversationName = name
				}
			);

			var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
			app.PushLoading (true);
			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
				if (apiCall.Response.Success) {
					if(apiCall.Response.ConversationIds !=null && apiCall.Response.ConversationIds.Length>0){
						app.PopLoading (true);
						BlrtManager.App.OpenMailbox (Executor.System (), BlrtData.Models.Mailbox.MailboxType.Inbox);
						var openSuccess = await new OpenConversationPerformer (Executor.System(), apiCall.Response.ConversationIds[0] ).RunAction ();
						if(openSuccess){
							//track
							var blrtLength = (_content as ConversationBlrtItem).AudioLength;
							var convoId = apiCall.Response.ConversationIds[0];
							var title = name;
							var numberPages = (_content as ConversationBlrtItem).Pages;
							var medias = BlrtManager.DataManagers.Default.GetMediaAssets(_content);
							var pageTypes = medias.Select(x=>x.Format.ToString()).Concat((_content as ConversationBlrtItem).MediaData.Items.Any(y=>y.MediaId.StartsWith("_template"))? new string[]{"Template"}:new string[]{}).Distinct();

							//BlrtData.Helpers.BlrtTrack.ConvoCreate(blrtLength, convoId, title, numberPages,pageTypes);
							BlrtData.Helpers.BlrtTrack.ConvoCreate (convoId, title);
						}
					}
					return;
				}
				app.PopLoading (true);
				switch (apiCall.Response.Error.Code) {
					default:
						BlrtUtil.Debug.ReportException (new Exception ("Conversation duplicate failure"), new Dictionary<string,string> {
							{"error code", apiCall.Response.Error.Code.ToString()},
							{"blrt id", blrtId},
							{"new convo name", name}
						}, Xamarin.Insights.Severity.Error);
						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("OK"));
						break;
				}

			} else {
				app.PopLoading (true);
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
		}

		async Task<string> GetDuplicateConversationName (string prefillName)
		{
			var tcs = new TaskCompletionSource<bool> ();
			var alert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("duplicate_title", "conversation_screen"),
				"",
				null,
				BlrtUtil.PlatformHelper.Translate ("Cancel"),
				BlrtUtil.PlatformHelper.Translate ("OK")
			);

			alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKeyboardType.Default;
			alert.GetTextField (0).Placeholder = BlrtUtil.PlatformHelper.Translate ("convo_name_lbl", "preview");
			alert.GetTextField (0).Text = prefillName;

			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				tcs.TrySetResult ((alert.CancelButtonIndex != e.ButtonIndex));
			};

			alert.Show ();

			if (!await tcs.Task)
				return "";

			//check that the value given is usable
			var name = alert.GetTextField (0).Text;
			if (string.IsNullOrWhiteSpace (name) || name.Trim ().Length >= 56) {
				var invalidAlert = new UIAlertView (BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_title", "preview"),
					BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_message", "preview"),
					null,
					BlrtUtil.PlatformHelper.Translate ("blrt_name_invalid_accept", "preview")
				);
				invalidAlert.Show ();
				await BlrtHelperiOS.AwaitAlert (invalidAlert);
				return await GetDuplicateConversationName (prefillName);
			} else {
				return name;
			}
		}
	}
}

