using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData;
using BlrtData.Models.ConversationScreen;
using BlrtData.Views.Helpers;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Conversation
{
	public delegate Task AudioPlayPauseBtnDelegate (string contentId, bool shouldPause);
	public class CellSizeManager
	{
		Dictionary<string, UIView> _dic;

		AudioPlayPauseBtnDelegate _AudioPlayPauseBtnDelegate { get; set;}
		public CellSizeManager (AudioPlayPauseBtnDelegate audioPlayPauseBtnDelegate)
		{
			_AudioPlayPauseBtnDelegate = audioPlayPauseBtnDelegate;
			_dic = new Dictionary<string, UIView> ();
		}

		UIView GetView (ConversationCellModel cell)
		{
			if (!_dic.ContainsKey (cell.CellIdentifer)) {

				switch (cell.CellIdentifer) {
					case Cells.CommentCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.CommentCell ());
						break;
					case Cells.BlrtCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.BlrtCell ());
						break;
					case Cells.EventCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.EventCell ());
						break;
					case Cells.MissingContentCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.MissingContentCell ());
						break;
					case Cells.SendFailureCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.SendFailureCell ());
						break;
					case Cells.ImageCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.ImageCell ());
						break;
					case Cells.PDFCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.PDFCell ());
						break;
					case Cells.AudioCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.AudioCell (_AudioPlayPauseBtnDelegate));
						break;
					case Cells.DateCell.Indentifer:
						_dic.Add (cell.CellIdentifer, new Cells.DateCell ());
						break;


				}
			}

			return _dic [cell.CellIdentifer];
		}


		public nfloat GetHeightForRow (ConversationCellModel cell, nfloat width)
		{
			var view = GetView (cell);

			if (view is IBindable<ConversationCellModel>)
				(view as IBindable<ConversationCellModel>).Bind (cell);

			return view.SizeThatFits (new CGSize (width, nfloat.MaxValue)).Height;	
		}
	}
}

