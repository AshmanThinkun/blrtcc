﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using BlrtData;

namespace BlrtiOS.Views.Conversation
{
	public partial class ConversationNameSetupViewParent : UIView
	{
		class ConversationNameSetupView : UIView
		{
			public static readonly UIEdgeInsets Insets = new UIEdgeInsets (14, 10, 14, 10);

			public UITextField ConvoNameField { get; set; }

			const int BTN_SIZE = 44;
			const int PADDING = 12;
			const int MAX_CHAR_COUNT = 56;
			const int CORNER_RADIUS = 4;

			UIButton SaveConvoNameBtn { get; set; }
			UIButton CancelBtn { get; set; }
			UILabel CharCountStatus { get; set; }
			UILabel UserInfoLabel { get; set; }
			CAShapeLayer NameFieldBorder { get; set; }

 			string SaveConvoNameBtnTitle { 
				get { 
					return  ForEditingExistingConvoName 
					? BlrtUtil.PlatformHelper.Translate ("save_convo_name_btn_title") : 
			          BlrtUtil.PlatformHelper.Translate ("create_convo_btn_title"); 
				} 
			}
			
			string CancelBtnTitle { get { return "Cancel"; } }

			readonly string UserInfo = BlrtUtil.PlatformHelper.Translate ("name_your_convo");

			public event EventHandler ConvoSaveBtnPressed {
				add { SaveConvoNameBtn.TouchUpInside += value; }
				remove { SaveConvoNameBtn.TouchUpInside -= value; }
			}

			public event EventHandler CancelBtnPressed {
				add { CancelBtn.TouchUpInside += value; }
				remove { CancelBtn.TouchUpInside -= value; }
			}

			public EventHandler EditingHasStarted;

			bool ForEditingExistingConvoName { get; set; }

			public ConversationNameSetupView (bool forEditingExistingConvoName)
			{
				ForEditingExistingConvoName = forEditingExistingConvoName;
				BackgroundColor = BlrtStyles.iOS.White;

				UserInfoLabel = new UILabel () {
					Lines = 0,
					Text = UserInfo,
					Font = BlrtStyles.iOS.CellTitleFont,
					TextColor = BlrtStyles.iOS.Black
				};
				UserInfoLabel.SizeToFit ();

				SaveConvoNameBtn = new UIButton (new CGRect (0, 0, BTN_SIZE * 2, BTN_SIZE)) {
					BackgroundColor = BlrtStyles.iOS.Green
				};
				SaveConvoNameBtn.Layer.CornerRadius = CORNER_RADIUS;
				SaveConvoNameBtn.SetTitleColor (BlrtStyles.iOS.Black, UIControlState.Normal);
				SaveConvoNameBtn.SetTitle (SaveConvoNameBtnTitle, UIControlState.Normal);
				ConvoSaveBtnPressed += delegate {
					ConvoNameField.ResignFirstResponder ();
				};

				CancelBtn = new UIButton (new CGRect (0, 0, BTN_SIZE * 2, BTN_SIZE)) {
					BackgroundColor = BlrtStyles.iOS.BlueGray
				};
				CancelBtn.Layer.CornerRadius = CORNER_RADIUS;
				CancelBtn.SetTitleColor (BlrtStyles.iOS.White, UIControlState.Normal);
				CancelBtn.SetTitle (CancelBtnTitle, UIControlState.Normal);

				ConvoNameField = new UITextField () {
					BackgroundColor = BlrtStyles.iOS.Gray2
				};
				ConvoNameField.ShouldChangeCharacters += OnCharChanged;
				ConvoNameField.Layer.SublayerTransform = CATransform3D.MakeTranslation (Insets.Left, 0, 0);

				NameFieldBorder = new CAShapeLayer ();
				NameFieldBorder.StrokeColor = BlrtStyles.iOS.Green.CGColor;
				NameFieldBorder.FillColor = null;
				NameFieldBorder.LineDashPattern = new NSNumber [] { 4, 2 };
				Layer.AddSublayer (NameFieldBorder);

				CharCountStatus = new UILabel (new CGRect (0, 0, 60, 0)) {
					Lines = 1,
					TextAlignment = UITextAlignment.Right,
					TextColor = BlrtStyles.iOS.Gray7,
					Font = BlrtStyles.iOS.CellDetailFont,
					Text = string.Empty
				};

				AddSubview (SaveConvoNameBtn);
                AddSubview (CancelBtn);
                AddSubview (UserInfoLabel);
                AddSubview (ConvoNameField);
				AddSubview (CharCountStatus);
			}

			bool OnCharChanged (UITextField textField, NSRange range, string replacementString)
			{
				var currentCharCount = ConvoNameField.Text.Length;
				var newLength = currentCharCount + replacementString.Length - range.Length;

				var addChar = newLength < MAX_CHAR_COUNT;
				UIColor textColor = null;

				if (newLength >= MAX_CHAR_COUNT) {
					if (newLength == MAX_CHAR_COUNT) {
						addChar = true;
					}
					textColor = BlrtStyles.iOS.AccentRedShadow;
				} else {
					textColor = BlrtStyles.iOS.Gray7;
				}

				SetCharCountStatus (addChar ? (int)newLength : MAX_CHAR_COUNT, textColor);
				return addChar;
			}

			public void SetCharCountStatus (int currentCharCount, UIColor textColor)
			{
				CharCountStatus.Text = string.Format ("{0} / {1}", currentCharCount, MAX_CHAR_COUNT);
				CharCountStatus.TextColor = textColor;
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				// info label
				var newRect = new CGRect (Insets.Left,
										  Insets.Top,
										  UserInfoLabel.Frame.Size.Width,
										  UserInfoLabel.Frame.Size.Height);

				if (newRect.Equals (UserInfoLabel.Frame))
					return;

				UserInfoLabel.Frame = newRect;

				// char count status label
				newRect = new CGRect (Bounds.Width - CharCountStatus.Frame.Width - Insets.Left,
									  Insets.Top,
									  CharCountStatus.Frame.Width,
									  UserInfoLabel.Frame.Size.Height);

				if (!newRect.Equals (CharCountStatus.Frame)) {
					CharCountStatus.Frame = newRect;
				}

				// text field
				var width = Bounds.Width - Insets.Left - Insets.Right;
				var height = BTN_SIZE;

				newRect = new CGRect (Insets.Left,
									  UserInfoLabel.Frame.Bottom + PADDING * 0.5,
									  width,
									  height);

				if (!newRect.Equals (ConvoNameField.Frame)) {
					ConvoNameField.Frame = newRect;
					NameFieldBorder.Path = UIBezierPath.FromRoundedRect (ConvoNameField.Frame, 2).CGPath;
				}

				// cancel button
				var boundsMidX = Bounds.GetMidX ();
				var size = CancelBtn.Frame.Size;
				newRect = new CGRect (boundsMidX - PADDING - size.Width,
									  ConvoNameField.Frame.Bottom + PADDING,
									  size.Width,
									  size.Height);

				CancelBtn.Frame = newRect;

				// submit button
				size = SaveConvoNameBtn.Frame.Size;
				newRect = new CGRect (boundsMidX + PADDING,
									  ConvoNameField.Frame.Bottom + PADDING,
									  size.Width,
									  size.Height);

				SaveConvoNameBtn.Frame = newRect;
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var height = UserInfoLabel.Frame.Height + PADDING + BTN_SIZE + PADDING + BTN_SIZE;
				height = Insets.Top + Insets.Bottom + height;

				return new CGSize (size.Width, height);
			}



		}
	}
}