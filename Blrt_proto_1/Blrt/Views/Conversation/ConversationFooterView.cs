using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using CoreGraphics;

using Foundation;
using UIKit;
using BlrtiOS.Views.CustomUI;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Conversation
{
	public class ConversationFooterView : UIView
	{
		static readonly float BUTTON_PADDING = 12;

		public static readonly float BUTTON_HEIGHT = 44;
		public static readonly float BUTTON_WIDTH = BUTTON_HEIGHT + BUTTON_PADDING;

		readonly float ANIMATION_DURATION = ConversationController.ANIMATION_DURATION;
		const float COMMENT_VIEW_FRAME_PADDING = 10;

		/// <summary>
		/// Gets the audio record permission.
		/// </summary>
		/// <value>The audio record permission.</value>
		bool? AudioRecordPermission {
			get {
				return BlrtiOS.Util.BlrtUtilities.AudioRecordPermission;
			}
		}

		bool isCommenting;
		bool isCommentingStatusChanged;

		public event EventHandler AudioRecordingErrorViewHAsAutoShutDown;
		public event EventHandler LongPressGestureStarted;
		public event EventHandler MicButtonPressed;

		BlrtCommentView CommentView { get; set; }
		UIButton CameraBtn { get; set; }
		UIButton MicBtn { get; set; }
		UIButton AddBtn { get; set; }
		UITextView ReadonlyAlertView { get; set; }
		BlrtAudioRecordingView AudioRecordingView { get; set;}
		bool ShouldHideRecordingView { get; set; }
		bool IsRecording { get; set; }

		public event EventHandler CommentTextChanged {
			add { CommentView.TextChanged += value; }
			remove { CommentView.TextChanged -= value; }
		}

		public event EventHandler CommentSendPressed {
			add { CommentView.SendPressed += value; }
			remove { CommentView.SendPressed -= value; }
		}

		public string CommentText { 
			get { return CommentView.Comment; }
			set { CommentView.Comment = value; }
		}

		//public event EventHandler MicButtonPressed {
		//	add { MicBtnPressed += value; }
		//	remove { MicBtnPressed -= value; }
		//}

		public event EventHandler CameraButtonPressed {
			add { CameraBtn.TouchUpInside += value; }
			remove { CameraBtn.TouchUpInside -= value; }
		}

		public event EventHandler AddButtonPressed {
			add { AddBtn.TouchUpInside += value; }
			remove { AddBtn.TouchUpInside -= value; }
		}

		public event EventHandler AudioRecordingStarted {
			add { AudioRecordingView.AudioRecordingStarted += value; }
			remove { AudioRecordingView.AudioRecordingStarted -= value; }
		}

		public event EventHandler AudioDoneBtnPressed {
			add { AudioRecordingView.DonePressed += value; }
			remove { AudioRecordingView.DonePressed -= value; }
		}

		public event EventHandler AudioCancelBtnPressed {
			add { AudioRecordingView.CancelPressed += value; }
			remove { AudioRecordingView.CancelPressed -= value; }
		}

		UIImage CameraBtnImage {
			get {
				return UIImage.FromBundle ("media_camera.png");
			}
		}

		UIImage MicBtnImage {
			get {
				return UIImage.FromBundle ("media_audio_dark.png");
			}
		}

		UIImage AddBtnImage {
			get {
				return UIImage.FromBundle ("add_image_picker.png");
			}
		}

		CGRect CommentViewFrame { 
			get {
				return isCommenting ? 
					new CGRect (Bounds.Left + COMMENT_VIEW_FRAME_PADDING * 0.5,
								Bounds.Top,
					            Bounds.Width - COMMENT_VIEW_FRAME_PADDING,
					            Math.Max(Bounds.Height, BUTTON_HEIGHT))
						: 
					new CGRect (Bounds.Left + BUTTON_WIDTH, // both left and right paddings for add button 
					            Bounds.Top, 
					            Bounds.Width - BUTTON_WIDTH * 3 - CameraBtnPadding * 0.5,
					            BUTTON_HEIGHT);
			} 
		}

		nfloat TotalEmptySpacingBetweenCommentFieldAndFooterLeadingEdge {
			get {
				return MicBtnPadding + CameraBtnPadding;
			}
		}

		nfloat MicBtnPadding {
			get {
				return BUTTON_WIDTH - MicBtn.ImageView.Image?.Size.Width ?? 0;
			}
		}

		nfloat CameraBtnPadding {
			get {
				return BUTTON_WIDTH - CameraBtn.ImageView.Image?.Size.Width ?? 0;
			}
		}

		public void HideAudioRecordingView(bool shouldHide, bool shouldAnimate) 
		{
			if (AudioRecordingView == null) {
				AudioRecordingView = new BlrtAudioRecordingView ();
				AudioRecordingView.Hidden = shouldHide;
				AudioRecordingView.Frame = new CGRect (Bounds.Left,
													   Bounds.Bottom, // Needs to be hidden
													   Bounds.Width,
													   Bounds.Height +
													   ConversationController.FOOTER_BOTTOM_PADDING);
				AddSubview (AudioRecordingView);
			}

			Animate (shouldAnimate ? ANIMATION_DURATION : 0.0, delegate {
				AddBtn.Enabled = CommentView.Enabled = CameraBtn.Enabled = MicBtn.Enabled = shouldHide;
				AddBtn.Alpha = CommentView.Alpha = CameraBtn.Alpha = MicBtn.Alpha = shouldHide ? 1.0f : 0.0f;
				AudioRecordingView.Alpha = shouldHide ? 0.0f : 1.0f;
				AudioRecordingView.Hidden = shouldHide;
				AudioRecordingView.Frame = new CGRect (AudioRecordingView.Frame.Left,
														  shouldHide ? Bounds.Bottom: Bounds.Top, // Move up or down
														  AudioRecordingView.Frame.Width,
														  AudioRecordingView.Frame.Height);
			});
		}

		public ConversationFooterView ()
		{
			Layer.BorderWidth = 0;
			Layer.BorderColor = BlrtStyles.iOS.White.CGColor;
			TintColor = BlrtStyles.iOS.AccentBlue;
			BackgroundColor = BlrtStyles.iOS.White;
			Frame = new CGRect (0, 0, 10, 10);

			isCommenting = false;
			isCommentingStatusChanged = false;

			CommentView = new BlrtCommentView ();
			CommentView.TextStarted += (sender, e) => {
				CameraBtn.Enabled = MicBtn.Enabled = AddBtn.Enabled = false;
				isCommenting = true;
				isCommentingStatusChanged = true;
				SetNeedsLayout ();
				SetNeedsDisplay ();
			};
			CommentView.TextEnded += (sender, e) => {
				CameraBtn.Enabled = MicBtn.Enabled = AddBtn.Enabled = true;
				isCommenting = false;
				isCommentingStatusChanged = true;
				SetNeedsLayout ();
				SetNeedsDisplay ();
			};
			// BUG in Unified API: if there is any event listener of UITextView.Scrolled, the UITextView.Changed will not be fired
			//			CommentView.Scrolled += (object sender, EventArgs e) => {
			//				if (CommentView.ContentSize.Height == CommentView.Frame.Height)
			//					CommentView.ContentOffset = new CGPoint (0, 0);
			//			};

			CameraBtn = new UIButton ();
			MicBtn = new UIButton ();
			MicBtn.AddGestureRecognizer (MicBtnLongPressGestureRecognizer);
			AddBtn = new UIButton () { Frame = new CGRect (0, 
			                                               0, 
			                                               BUTTON_WIDTH, 
			                                               BUTTON_HEIGHT)};

			CameraBtn.SetImage (CameraBtnImage, UIControlState.Normal);
			MicBtn.SetImage (MicBtnImage, UIControlState.Normal);
			AddBtn.SetImage (AddBtnImage, UIControlState.Normal);

			CameraBtn.Hidden = MicBtn.Hidden = AddBtn.Hidden = false;
			CameraBtn.Enabled = MicBtn.Enabled = AddBtn.Enabled = true;

			MicBtn.TouchUpInside += OnMicBtnTouchUpInside;

			ReadonlyAlertView = new UITextView {
				Text = BlrtData.BlrtUtil.PlatformHelper.Translate ("convo_readonly_text", "conversation_screen"),
				Font = BlrtStyles.iOS.FontSize16,
				TextColor = BlrtStyles.iOS.AccentRed,
				TextAlignment = UITextAlignment.Center,
				BackgroundColor = BlrtStyles.iOS.Gray2,
				TextContainerInset = new UIEdgeInsets (5,5,5,5),
				UserInteractionEnabled = false,
				Editable = false
			};

			AddSubviews (new UIView [] {
				CommentView,
				CameraBtn,
				MicBtn,
				AddBtn,
				ReadonlyAlertView
			});
		}

		async void OnMicBtnTouchUpInside (object sender, EventArgs e)
		{
			var audioRecordPermission = AudioRecordPermission;
			if (audioRecordPermission == null || !audioRecordPermission.Value) {
				await RequestAudioRecordPermission ();
				return;
			}

			MicButtonPressed?.Invoke (null, null);

            HideAudioRecordingView (false, true);
			ShouldHideRecordingView = true;

			AudioRecordingView.MicBtnTouchDown -= HandleAudioRecordingViewMicBtnTouchDown;
			AudioRecordingView.MicBtnTouchUp -= HandleAudioRecordingViewMicBtnTouchUp;
			AudioRecordingStarted -= HandleAudioRecordingStarted;

			AudioRecordingView.MicBtnTouchDown += HandleAudioRecordingViewMicBtnTouchDown;
			AudioRecordingView.MicBtnTouchUp += HandleAudioRecordingViewMicBtnTouchUp;
			AudioRecordingStarted += HandleAudioRecordingStarted;

			InvokeInBackground (delegate {
				do {
					System.Threading.Thread.Sleep (1000);
					if (ShouldHideRecordingView) {
						BeginInvokeOnMainThread (delegate {
							if (AudioRecordingErrorViewHAsAutoShutDown != null) {
								AudioRecordingErrorViewHAsAutoShutDown (null, null);
							}
							HideAudioRecordingView (true, true);
						});
						break;
					}
				}
				while (!IsRecording); // only when recording hasn't started
			});
		}

		/// <summary>
		/// Requests the audio record permission ans shows alert to grant it.
		/// </summary>
		/// <returns>The audio record permission.</returns>
		async Task<bool> RequestAudioRecordPermission ()
		{
			var audioPermission = AudioRecordPermission;
			var isPermissionGranted = (audioPermission == null)
				? await BlrtiOS.Util.BlrtUtilities.RequestAudioRecordPermission () : audioPermission.Value;

			if (!isPermissionGranted) {
				await BlrtData.BlrtUtil.ShowMicPermissionAlert ();
			}

			return isPermissionGranted;
		}

		void HandleAudioRecordingViewMicBtnTouchDown(object o, EventArgs e)
		{
			ShouldHideRecordingView = false;
		}

		void HandleAudioRecordingViewMicBtnTouchUp (object o, EventArgs e)
		{
			ShouldHideRecordingView = true;
		}

		void HandleAudioRecordingStarted (object sender, EventArgs e)
		{
			UIGestureRecognizerState state;

			if (UIGestureRecognizerState.Changed == (state = (sender as UILongPressGestureRecognizer).State)) {
				return;
			}

			if (UIGestureRecognizerState.Began == state) {
				ShouldHideRecordingView = false;
				IsRecording = true;
			}

			if (UIGestureRecognizerState.Ended == state || UIGestureRecognizerState.Cancelled == state) {
				ShouldHideRecordingView = true;
				IsRecording = false;
			}
		}

		UILongPressGestureRecognizer MicBtnLongPressGestureRecognizer {
			get {
				return new UILongPressGestureRecognizer (async (obj) => {

					if (UIGestureRecognizerState.Began == obj.State) {

						var audioRecordPermission = AudioRecordPermission;
						if (audioRecordPermission == null || !audioRecordPermission.Value) {
							await RequestAudioRecordPermission ();
							return;
						}

						HideAudioRecordingView (false, false);

						if (LongPressGestureStarted != null) {
							LongPressGestureStarted (null, null);
						}

						AudioRecordingStarted -= HandleAudioRecordingStarted;
						AudioRecordingStarted += HandleAudioRecordingStarted;

						AudioRecordingView.HandleLongPressGestureBegan (obj);
					}

					if (UIGestureRecognizerState.Ended == obj.State ||
						UIGestureRecognizerState.Cancelled == obj.State) {

						var recordPermission = AudioRecordPermission;
						if (recordPermission == null || !recordPermission.Value) {
							return; // either permission denied or undetermined.
						}

						AudioRecordingView.HandleLongPressGestureEnd (obj);
						AudioRecordingStarted -= HandleAudioRecordingStarted;
					}

				}) { MinimumPressDuration = BlrtAudioRecordingView.MINIMUN_LONG_PRESS_DURATION };
			}
		}

		public void ResetAudioRecordingViewsToDefault()
		{
			AudioRecordingView.ResetDefaultViews(null, null);
		}

		public void ShowPostAudioRecodingViews()
		{
			AudioRecordingView.ShowPostRecordingViews ();
		}

		public void UpdateAudioDuration (float duration)
		{
			AudioRecordingView.SetDuration (duration);
		}

		public void UpdateAudioVolume (float currentVolume)
		{
			AudioRecordingView.SetVolume (currentVolume);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			return ReadonlyAlertView.Hidden ? CommentView.SizeThatFits (size) : ReadonlyAlertView.SizeThatFits (size);
		}

		void LayoutCameraAndMicButtons()
		{
			var newCommentViewFrame = CommentViewFrame;
			if (!newCommentViewFrame.Equals (CommentView.Frame)) {
				CommentView.Frame = CommentViewFrame;
				var x = CommentView.Frame.Right + CameraBtnPadding * 0.5;//+ MicBtnPadding * 0.5; // purpose of 0.5 is to leave equal leading and 
												 //trailing spaces before camera button and 
												 //after mic button respectively.

				//var micBtnExcessPadding = MicBtnExcessPadding;
				CameraBtn.Frame = new CGRect (x, 
				                              0,
				                              BUTTON_WIDTH, 
				                              BUTTON_HEIGHT);

				// Mic button
				var paddingDiff = MicBtnPadding - CameraBtnPadding;
				x = CameraBtn.Frame.Right;
				MicBtn.Frame = new CGRect (x,
				                           0, 
				                           BUTTON_WIDTH - paddingDiff,
				                           BUTTON_HEIGHT);

			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (!ReadonlyAlertView.Hidden) {
				LayoutReadonlyAlerView ();
				return;
			}

			if (isCommentingStatusChanged) {
				isCommentingStatusChanged = false;

				Animate (ANIMATION_DURATION, delegate {
					if (CommentView != null) {
						LayoutCameraAndMicButtons ();
					}
				});

				var animationDuration = isCommenting ? ANIMATION_DURATION * 0.3 : ANIMATION_DURATION * 3.3;
				Animate (animationDuration, delegate {
					if (isCommenting) {
						CameraBtn.Alpha = MicBtn.Alpha = AddBtn.Alpha = 0.0f;
					} else {
						CameraBtn.Alpha = MicBtn.Alpha = AddBtn.Alpha = 1.0f;
					}
				});
			} else {
				LayoutCameraAndMicButtons ();
			}
		}

		void LayoutReadonlyAlerView ()
		{
			var size = ReadonlyAlertView.SizeThatFits (new CGSize(Bounds.Width, int.MaxValue));
			ReadonlyAlertView.Frame = new CGRect (Bounds.X, Bounds.Y, Bounds.Width, size.Height);
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			return base.HitTest (point, uievent);
		}

		internal void UpdateView (bool isConversationReadonly)
		{
			/*if (ReadonlyAlertView.Hidden == !isConversationReadonly) {
				return;
			}*/
			var readonlyAlertHidden = !isConversationReadonly;
			CommentView.Hidden = CameraBtn.Hidden = AddBtn.Hidden = MicBtn.Hidden = !(ReadonlyAlertView.Hidden = readonlyAlertHidden);
			CommentView.Enabled = CameraBtn.Enabled = AddBtn.Enabled = MicBtn.Enabled = readonlyAlertHidden;
			SetNeedsLayout ();
		}
	}
}

