using System;
using System.Collections.Generic;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtiOS.Query;
using BlrtiOS.UI;

using System.IO;
using BlrtData;
using BlrtData.IO;

namespace BlrtiOS.Views.Conversation
{
	public class BlrtAlertsBarView : UIView
	{
		public event EventHandler SizeChanged;

		List<AlertView> _views;

		public BlrtAlertsBarView () : base ()
		{
			_views = new List<AlertView> ();
		}

		public void Clear (bool animated)
		{
			foreach (var d in _views) {
				d.Clear ();
			}

			ContentChanged (animated);
		}


		public void Show (BlrtAlertType type, bool animated)
		{
			if (type == BlrtAlertType.None)
				return;

			InvokeOnMainThread (() => {
				AlertView alert = null;

				foreach (var v in _views) {
					if (v.Alert == type) {
						return;
					}
					if (alert == null && v.Setable) {
						alert = v;
					}
				}

				if (alert == null) {

					alert = new AlertView ();
					_views.Add (alert);
					AddSubview (alert);
				}
				alert.SetAlert (type);

				_views.Sort ();

				nfloat height = 0;

				foreach (var v in _views) {
					v.Frame = new CGRect (0, height, Bounds.Width, v.Frame.Height);
					height = v.Frame.Bottom;
				}

				ContentChanged (animated);
			});
		}

		public void Hide (BlrtAlertType type, bool animated)
		{
			foreach (var v in _views) {
				if (v.Alert == type) {
					v.Clear ();
				}
			}

			ContentChanged (animated);
		}

		void ContentChanged (bool animated)
		{
			if (animated) {
				InvokeOnMainThread (delegate {
					UIView.Animate (0.4f, () => {
						ResizeContent ();
					});
				});
			} else {
				ResizeContent ();
			}
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			ResizeContent ();
		}

		void ResizeContent ()
		{
			nfloat height = 0;

			AlertView last = null;

			foreach (var v in _views) {

				if (v.ShouldDisplay) {

					var size = v.SizeThatFits (new CGSize (Bounds.Width, nfloat.MaxValue));
					v.Frame = new CGRect (0, height, Bounds.Width, size.Height);
					height = v.Frame.Bottom;

					if (last != null)
						last.ShowSeperator = true;
					last = v;

				} else {
					v.ShowSeperator = false;
					v.Frame = new CGRect (0, height, Bounds.Width, 0);
				}
			}

			var rect = new CGRect (Frame.X, Frame.Y, Bounds.Width, height);

			if (rect.Size != Frame.Size) {
				Frame = rect;
				if (SizeChanged != null)
					SizeChanged (this, EventArgs.Empty);
			}
		}

		class AlertView : UIView, IComparable<AlertView>
		{

			public bool ShouldDisplay {
				get { return Alert != BlrtAlertType.None; }
			}

			public bool Setable {
				get { return !ShouldDisplay && Frame.Height == 0; }
			}

			public bool ShowSeperator {
				get { return !_seperator.Hidden; }
				set { _seperator.Hidden = !value; }
			}

			public BlrtAlertType Alert { get; private set; }

			public UIEdgeInsets Padding { get; private set; }

			UILabel _lbl;
			UIView _seperator;

			public AlertView ()
			{

				Padding = new UIEdgeInsets (5, 16, 5, 16);

				AddSubview (_lbl = new UILabel () {
					Font = BlrtStyles.iOS.CellContentFont,
					Lines = 0
				});

				AddSubview (_seperator = new UIView () {
					BackgroundColor = BlrtStyles.iOS.Gray2,
					Hidden = true
				});

				ClipsToBounds = true;
			}

			public void SetAlert (BlrtAlertType alert)
			{
				Alert = alert;

				switch (alert) {
					case BlrtAlertType.NotSent:
						_lbl.Text = BlrtHelperiOS.Translate ("warning_non_sent", "conversation_screen");
						break;
					case BlrtAlertType.ContentUnsynced:
						_lbl.Text = BlrtHelperiOS.Translate ("warning_unsynced_content", "conversation_screen");
						break;
					case BlrtAlertType.ConversationUnsynced:
						_lbl.Text = BlrtHelperiOS.Translate ("warning_unsynced_converasation", "conversation_screen");
						break;
					case BlrtAlertType.NoInternet:
						_lbl.Text = BlrtHelperiOS.Translate ("warning_no_internet", "conversation_screen");
						break;
				}

				_lbl.TextColor = BlrtStyles.iOS.Gray1;
				BackgroundColor = BlrtStyles.iOS.CharcoalAlpha;
			}

			public void Clear ()
			{
				Alert = BlrtAlertType.None;
			}


			public override CGSize SizeThatFits (CGSize size)
			{
				size.Height -= Padding.Top + Padding.Bottom;
				size.Width -= Padding.Left + Padding.Right;

				size = _lbl.SizeThatFits (size);
				
				size.Height += Padding.Top + Padding.Bottom;
				size.Width += Padding.Left + Padding.Right;

				return size;
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				_lbl.Frame = Padding.InsetRect (Bounds);
				var point = 1 / UIScreen.MainScreen.Scale;

				_seperator.Frame = new CGRect (Padding.Left, Bounds.Height - point, Bounds.Width - Padding.Left, point);
			}

			#region IComparable implementation

			public int CompareTo (AlertView other)
			{
				return Alert.CompareTo (other.Alert);
			}

			#endregion
		}


		public enum BlrtAlertType
		{
			None,
			ConversationUnsynced,
			ContentUnsynced,
			NotSent,
			NoInternet,
		}
	}

}

