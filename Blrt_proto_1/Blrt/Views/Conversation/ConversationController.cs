using System;
using System.Linq;
using Foundation;
using UIKit;
using BlrtData.ExecutionPipeline;
using BlrtiOS.Views.Util;
using System.Threading.Tasks;
using BlrtData;
using BlrtCanvas;
using Xamarin.Forms;
using BlrtData.Contents;
using BlrtData.Models.ConversationScreen;
using BlrtData.Views.Helpers;
using BlrtData.Views.Conversation.ConversationOptions;
using BlrtAPI;
using Acr.UserDialogs;
using System.Collections.Generic;
using BlrtData.Views.Send;
using Parse;
using BlrtiOS.Util;
using BlrtiOS.Media;
using BlrtiOS.Views.MediaPicker;
using System.Runtime.CompilerServices;
using System.IO;
using System.Threading;
using AudioToolbox;
using BlrtiOS.ViewControllers.Canvas;
using BlrtiOS.Views.FullScreenViewers;
using ImagePickers;
using BlrtShared;
using MessageUI;
using PhotoKitGallery;

namespace BlrtiOS.Views.Conversation
{
	using MediaInfo = BlrtAddMediaTypes.DocumentPickerReturnedMediaInfo;

	public class ConversationController : KeyboardController, IConversationScreen, IExecutor
	{
		static readonly string EmptyConvoInfo = "Get started with your conversation below";
		static readonly string NoConvoSelectedInfo = "Select a conversation from the left to view";

		readonly int MAX_IMAGE_COUNT_TO_SHARE = BlrtiOS.Views.Conversation.Cells.ImageCell.MAX_IMAGES;

		#region implemented abstract members of BaseUIViewController

		public override string ViewName {
			get { return null; }
		}

		#endregion

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}

		#endregion

		public static readonly float ANIMATION_DURATION = 0.3f;
		public static readonly float FOOTER_BOTTOM_PADDING = 11;
		public static readonly int CONTROL_BUTTON_VIEWHEIGHT = 73;

		ConversationTableView _tableView;
		ControlButtonsView _controlButtonsView; // constains "blrt reply" and "Add people" buttons.
		ConversationFooterView _footerView;
		ConversationNameSetupViewParent _newConversationSetupView;

		ConversationActionBuilder _actionBuilder;

		UIBarButtonItem _addPeopleBtn;
		UIBarButtonItem _moreBtn;

		BlrtData.Conversation _conversationModel;

		bool ScrollToBottomAfterSetConversation { get; set; }

		/// <summary>
		/// Gets a value indicating whether conversation contains blrt.
		/// <value><c>true</c> if has blrt; otherwise, <c>false</c>.</value>
		bool HasBlrt { 
			get {
				return Conversation.BlrtCount > 0;
			} 
		}

		/// <summary>
		/// Gets a value indicating whether conversation is empty
		/// <value><c>true</c> if is empty; otherwise, <c>false</c>.</value>
		bool IsEmpty { 
			get {
				return Conversation.IsEmpty;
			}
		}

		public bool ConversationOpen { 
			get { return _conversation != null; }
		}

		public string ConversationId {
			get { return _conversation != null ? _conversation.Id : null; }
		}

		BlrtData.Conversation IConversationScreen.CurrentConversation {
			get {
				return Conversation;
			}
		}

		/// <summary>
		/// Gets the conversation from local storage. This should only be used if new instance of conversation is 
		/// required (in case of changes in model for example).
		/// </summary>
		/// <value>The conversation from local storage.</value>
		BlrtData.Conversation ConversationFromLocalStorage { 
			get {
				return _conversation.LocalStorage.DataManager.GetConversation (ConversationId ?? string.Empty);
			}
		}

		BlrtData.Conversation Conversation {
			get {
				if (_conversation == null) return null;

				if (_conversationModel == null) {
					_conversationModel = ConversationFromLocalStorage;
				}

				return _conversationModel;
			}
			set {
				_conversationModel = value;
			}
		}

		Root.RootAppController RootController {
			get {
				return BlrtManager.App as Root.RootAppController;
			}
		}

		async Task HandleAudioPlayPause (bool shouldPause, string contentId, string audioFilePath)
		{
			if (shouldPause) {
				AudioPlaybackManager.AudioManager.Pause ();
			} else {
				await AudioPlaybackManager.AudioManager.Play (contentId, audioFilePath);
				BlrtUtil.TryMarkingConversationItemAsRead (_conversation.LocalStorage.DataManager.GetConversationItem (contentId));
			}
		}

		ConversationHandler _conversation;
		MultiExecutionPreventor.SingleExecutionToken _multiExeToken = MultiExecutionPreventor.NewToken ();

		public async Task AudioPlayPauseBtnPressed (string contentId, bool shouldPause)
		{
			if (AudioPlaybackManager.CurrentContentId != null && AudioPlaybackManager.CurrentContentId.Equals (contentId)) {
				if (AudioPlaybackManager.IsAudioPlaying != shouldPause) {
					return;
				}

				await HandleAudioPlayPause (shouldPause, contentId, string.Empty);
				return;
			}

			ConversationItem item = null;
			try {

				item = await GetItem (contentId, LocalStorageParameters.Default);

			} catch (Exception ex1) {
				return;
			}

			if (item.Type == BlrtContentType.Audio) {
				var audio = item as ConversationAudioItem;

				_conversation.ReadAllComments ();

				var result = await DownloadContent (item, this, contentId);

				if (result.Exception != null) // Cancelled download
					return;
				
				try {
						Exception lastDecryptionException = null;
						if (null != audio) {
							if (!audio.IsAudioDecrypted) {
								using (UserDialogs.Instance.Loading ("Loading...")) {
									try {
										await audio.DecyptAudioData ();
									} catch (BlrtEncryptorManagerException e3) {
										lastDecryptionException = e3;
										audio.DeleteAudioData ();
									} catch (InvalidOperationException e4) {
										lastDecryptionException = e4;
										audio.DeleteAudioData ();
									}
								}
								if (lastDecryptionException != null) {
									bool retry = false;

									retry = await BlrtUtilities.AskUserForRetry ();

									if (retry) {
										await AudioPlayPauseBtnPressed (contentId, false);
									} else { return; }
								}
							}
							await HandleAudioPlayPause (false, contentId, audio.DecryptedAudioPath);
						}
						else { return; }

				} catch {
					return;
				}
			}
		}

		/// <summary>
		/// Setups the table view.
		/// </summary>
		void SetupTableView ()
		{
			_tableView = new ConversationTableView (this)
			{
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				ContentInset = new UIEdgeInsets (0, 0, 0, 0)
			};

			_tableView.TitleView.RefreshButton.TouchUpInside += (object sender, EventArgs e) => {

				Refresh ().FireAndForget ();
			};

			_tableView.TitleView.FilterPressed += (object sender, EventArgs e) => {
				OpenConversationOption (this).FireAndForget ();
			};

			_tableView.EstimatedRowHeight = 0;
			_tableView.EstimatedSectionHeaderHeight = 0;
			_tableView.EstimatedSectionFooterHeight = 0;

			SetConversationTableView ();
		}

		/// <summary>
		/// Setups the control buttons view. It contains the "new blrt/ blrt reply" button and "Add people" button
		/// </summary>
		void SetupControlButtonsView ()
		{
			_controlButtonsView = new ControlButtonsView ();

			_controlButtonsView.ReBlrt += (sender, e) => {

				OpenReBlrtCreation (this, null).FireAndForget ();
			};

			_controlButtonsView.AddPeople += (sender, e) => {
				OpenSendingPage (this, null).FireAndForget ();
			};

			_controlButtonsView.Hidden = true; // hide it initially
		}

		/// <summary>
		/// Setups the footer view. It includes the comment field, plus button, camera icon, mic icon.
		/// </summary>
		void SetupFooterView ()
		{
			_footerView = new ConversationFooterView ();
			_footerView.CommentTextChanged += (object sender, EventArgs e) => {
				View.SetNeedsLayout ();
			};

			_footerView.CommentSendPressed += (object sender, EventArgs e) => {
				SendComment ().FireAndForget ();
			};

			_footerView.MicButtonPressed += OnMicButtonPressed;
			_footerView.LongPressGestureStarted += delegate {
				SetEventHandlersAndHideViews (); 
			};

			_footerView.AddButtonPressed += OnAddButtonPressed;
			_footerView.CameraButtonPressed += delegate{ OpenGalleryAsync (true).FireAndForget (); };

			_footerView.Hidden = true; // hide it initially
		}

		public ConversationController () : base ()
		{
			Title = BlrtHelperiOS.Translate ("conversation_screen", "screen_titles");

			_actionBuilder = new ConversationActionBuilder (this);

			EdgesForExtendedLayout = UIRectEdge.None;
			AddDisabledKeyboardTap (typeof (ConversationFooterView));

			SetupTableView ();
			SetupControlButtonsView ();
			SetupFooterView ();

			_addPeopleBtn = new UIBarButtonItem (UIImage.FromBundle ("add_user.png"), UIBarButtonItemStyle.Done, (sender, e) => {
				OpenSendingPage (this, null).FireAndForget();
			});
			_moreBtn = new UIBarButtonItem (UIImage.FromBundle ("inbox_ellipsis.png"), UIBarButtonItemStyle.Done, (sender, e) => {
				OpenConversationOption (this).FireAndForget();
			});

			PushHandler.OnBlrtRefresh += async (object sender, string e) => {
				if (e == ConversationId) {
					await Refresh ();
					ScrollToBottom (true);
				}
			};

			MessagingCenter.Subscribe<Object> (this, "ConversationNameChanged", async (obj) => {
				if (_tableView != null && _conversation != null) {
					await _conversation.Refresh ();
				}
			});

			MessagingCenter.Subscribe<Object> (this, "DisplaynameChanged", async (obj) => {
				if (_tableView != null && _conversation != null) {
					await Refresh ();
					BeginInvokeOnMainThread (() => {
						_tableView.ReloadData ();
					});
				}
			});
		}


		public static bool IsForImageSharing;

		public async Task OpenGalleryAsync (bool showCameraCell)
		{
			IsForImageSharing = true;

			AlbumCollectionViewController albumCollectionViewController;
			albumCollectionViewController = new AlbumCollectionViewController (showCameraCell);
			var gallery = ImagePickerViewController.Instance(albumCollectionViewController);
			gallery.MaximumImagesCount = MAX_IMAGE_COUNT_TO_SHARE;
			gallery.ModalPresentationStyle = UIModalPresentationStyle.PageSheet;



			PresentViewController (gallery, true, null);

			object imgData = null;
			try {
				imgData = await albumCollectionViewController.Completion;
				//imgData = await albumCollectionViewController.Completion as List<PhotoGalleryAssetResult>;
			} catch (Exception ex) {
				BlrtUtil.Debug.ReportException (ex, new Dictionary<string, string> { { "message", "exception from ELCImagePickerViewController" } });
			} finally {
				gallery.DismissViewController (true, delegate { OnImagesReceived (imgData).FireAndForget(); });
				IsForImageSharing = false;
			}

		}

		async Task OnImagesReceived (object filesData)
		{
			if (filesData == null)
				return;
			
			string [] filePaths = null;
			string [] fileNames = null;

			if (filesData is List<PhotoGalleryAssetResult>) { // Images from library
				var images = filesData as List<PhotoGalleryAssetResult>;

				var imgCount = images.Count;
				if (null == images || imgCount == 0) {
					return;
				}

				fileNames = new string [imgCount];
				for (int i = 0; i < imgCount; i++) {
					fileNames [i] = images [i].Title;//images [i].Representation.Filename;
				}

				filePaths = await BlrtUtilities.CompressedImagePaths (images, UIImagePickerControllerSourceType.PhotoLibrary.ToString ());
			} else if (filesData is List<MediaInfo>) { // images from iOS locations
				var images = filesData as List<MediaInfo>;

				if (null == images || images.Count() == 0) {
					return;
				}

				fileNames = (from image in images select image.MediaName).ToArray();
				filePaths = await BlrtUtilities.CompressedImagePaths ((from image in images select image.MediaURL).ToList ()
				                                                      , UIImagePickerControllerSourceType.PhotoLibrary.ToString ());
			} else if (filesData is Tuple<UIImage, string>) { // Camera shot 
				var imageInfo = filesData as Tuple<UIImage, string>;

				if (imageInfo == null)
					return;

				fileNames = new string [] {"Camera shot"};
				filePaths = new string [] { await BlrtUtilities.CompressedImagePath (imageInfo.Item1, imageInfo.Item2) };
			}


			var result = (await SendMediaItem (filePaths, fileNames, BlrtMediaFormat.Image)).Result;

			if (result is Exception) {
				throw result as Exception;
			}

			filesData = null;
			GC.Collect ();
		}

		void OnAddButtonPressed (object sender, EventArgs e)
		{
			var alert = UIAlertController.Create (null, null, UIAlertControllerStyle.ActionSheet);

			var blrt = UIAlertAction.Create ("Blrt", UIAlertActionStyle.Default, OnBlrtActionTapped);
			var camera = UIAlertAction.Create ("Camera", UIAlertActionStyle.Default, OnCameraActionTapped);
			var photoLibrary = UIAlertAction.Create ("Photo Library", UIAlertActionStyle.Default, 
			                                        delegate { OpenGalleryAsync (false).FireAndForget (); });
			var document = UIAlertAction.Create ("Document", UIAlertActionStyle.Default, delegate { OnDocumentActionTapped (sender); });

			blrt.SetValueForKey (UIImage.FromBundle ("blrtoutline_dark.png"), new NSString ("image"));
			camera.SetValueForKey (UIImage.FromBundle ("camera_grn.png"), new NSString("image"));
			photoLibrary.SetValueForKey (UIImage.FromBundle ("image_grn.png"), new NSString ("image"));
			document.SetValueForKey (UIImage.FromBundle ("document_grn.png"), new NSString ("image"));
						           
			alert.AddAction (blrt);
			alert.AddAction (camera);
			alert.AddAction (photoLibrary);
			alert.AddAction (document);
			alert.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Cancel, null));

			var popover = alert.PopoverPresentationController;
			if (popover != null) {
				popover.SourceRect = (sender as UIButton).Frame;
				popover.SourceView = (sender as UIButton);
				popover.PermittedArrowDirections = UIPopoverArrowDirection.Any;
			}

			PresentViewController (alert, true, null);
		}

		void OnBlrtActionTapped (object sender)
		{
			OpenReBlrtCreation (this, null).FireAndForget ();
		}

		void OnCameraActionTapped (object sender)
		{
			try {
				var _camera = CameraController.Camera;
				_camera.ImagePickerControllerDelegate = new CameraControllerDelegate (_camera);

				UIViewController v = this;
				while (v.ParentViewController != null)
					v = v.ParentViewController;

				UIView.AnimationsEnabled = false;
				v.PresentViewController (_camera, false, null);
				UIView.AnimationsEnabled = true;

				(_camera.ImagePickerControllerDelegate as CameraControllerDelegate).Completion.ContinueWith ((arg) => {
					if (arg.IsCanceled || arg.IsFaulted) {
						_camera.PopViewController (true);
						_camera.Dispose ();
						_camera = null;
					} else {
						OnImagesReceived (arg.Result).FireAndForget();
					}
				});

			} catch {

			}
		}

		void OnDocumentActionTapped (object sender)
		{
			BlrtAddMediaTypes.OpenDocumentPicker (sender as UIButton, this, (mediaUrlManager) => {
			var files = mediaUrlManager.PopMediaUrls ();
				OnFilesReceived (files).FireAndForget();
				return Task.CompletedTask;
			}, new string [] {
						MobileCoreServices.UTType.PDF,
						MobileCoreServices.UTType.Image
					});
		}

		Task OnFilesReceived (List<MediaInfo> media)
		{
			if (media == null || media.Count == 0) {
				return Task.CompletedTask;
			}

			List<MediaInfo> images = null;
			List<MediaInfo> pdfs = null;

			images = media.Where (medium => medium.MediaFormat == BlrtMediaFormat.Image).ToList();
			pdfs = media.Where (medium => medium.MediaFormat == BlrtMediaFormat.PDF).ToList ();

			if (images != null) {
				var index = 0;
				var maxImagesInCell = Cells.ImageCell.MAX_IMAGES;
				var imgCount = images.Count;
				var imagesLeft = imgCount - index;

				while (index < imgCount) {
					if (imagesLeft >= maxImagesInCell) {
						OnImagesReceived (images.GetRange (index, maxImagesInCell)).FireAndForget();
					} else {
						OnImagesReceived (images.GetRange (index, imagesLeft)).FireAndForget ();
					}

					index += maxImagesInCell;
				}
			}

			if (pdfs != null) {
				for (int i = 0; i < pdfs.Count; i++) {
					OnPDFReceived (pdfs[i]).FireAndForget();
				}
			}
			return Task.CompletedTask;
		}

		/// <summary>
		/// Called when pdf is requested to be uploaded.
		/// </summary>
		/// <returns>The PDFR eceived.</returns>
		/// <param name="pdfInfo">Pdf info.</param>
		async Task OnPDFReceived (MediaInfo pdfInfo)
		{
			if (pdfInfo == null)
				return;

			var result = (await SendMediaItem (new string [] { pdfInfo.MediaURL }, 
			                                   new string [] { pdfInfo.MediaName }, 
			                                   pdfInfo.MediaFormat)).Result;

			if (result is Exception) {
				throw result as Exception;
			}
		}

		void SetEventHandlersAndHideViews()
		{
			_footerView.AudioRecordingStarted -= OnAudioRecordingStarted;
			_footerView.AudioRecordingStarted += OnAudioRecordingStarted;

			_footerView.AudioDoneBtnPressed -= OnAudioDoneBtnPressed;
			_footerView.AudioDoneBtnPressed += OnAudioDoneBtnPressed;

			_footerView.AudioCancelBtnPressed -= OnAudioCancelBtnPressed;
			_footerView.AudioCancelBtnPressed += OnAudioCancelBtnPressed;

			_controlButtonsView.Hidden = true;
		}

		void OnMicButtonPressed (object sender, EventArgs e)
		{
			_footerView.HideAudioRecordingView (true, true);
			SetEventHandlersAndHideViews ();

			_footerView.AudioRecordingErrorViewHAsAutoShutDown -= OnAudioRecordingErrorViewHAsAutoShutDown;
			_footerView.AudioRecordingErrorViewHAsAutoShutDown += OnAudioRecordingErrorViewHAsAutoShutDown;
		}

		void OnAudioRecordingStarted (object sender, EventArgs e)
		{
			UIGestureRecognizerState state;

			if (UIGestureRecognizerState.Changed == (state = (sender as UILongPressGestureRecognizer).State)) {
				return;
			}	

			if (UIGestureRecognizerState.Began == state) {
				SystemSound.Vibrate.PlayAlertSoundAsync ();
				OnAudioRecordingStarted ();
			}

			if (UIGestureRecognizerState.Ended == state || UIGestureRecognizerState.Cancelled == state) {
				OnAudioRecordingEnded ();
			}
		}

		void OnAudioRecordingErrorViewHAsAutoShutDown (object sender, EventArgs e)
		{
			_controlButtonsView.Hidden = false;
		}

		void OnAudioDoneBtnPressed (object sender, EventArgs e)
		{
			ShowMainFooterViews ();

			if (!string.IsNullOrWhiteSpace (TempDecryptedAudioPath)) {
				SendAudioConversationItem (TempDecryptedAudioPath, AudioRecorder.Duration, AudioRecorder.Extension).FireAndForget ();
				TempDecryptedAudioPath = string.Empty;
			}
			AudioRecorder.Release ();
			AudioRecorder = null;
			GC.Collect ();
		}

		void OnAudioCancelBtnPressed (object sender, EventArgs e)
		{
			ShowMainFooterViews ();
			if (!string.IsNullOrWhiteSpace (TempDecryptedAudioPath)) {
				try {
					File.Delete (TempDecryptedAudioPath);
				} catch { }

				TempDecryptedAudioPath = string.Empty;
			}
		}

		IBCAudioRecorder AudioRecorder { get; set;}
		string TempDecryptedAudioPath { get; set; }
		bool Recording { get; set; }

		const int TIME_INTERVAL = 100;
		void OnAudioRecordingStarted ()
		{
			AudioRecorder = new BCAudioRecorder ();
			if (AudioRecorder != null) {
				TempDecryptedAudioPath = BlrtData.BlrtConstants.Directories.Default.GetDecryptedPath ("cache" + AudioRecorder.Extension);
				AudioRecorder.SetFilePath (TempDecryptedAudioPath);
				AudioRecorder.Record ();

				Recording = true;
				InvokeInBackground (delegate {
					var timeElapsedSinceLastDurationUpdate = 0;
					while (Recording) {
						InvokeOnMainThread (delegate {
							if (timeElapsedSinceLastDurationUpdate >= 1000) {
								timeElapsedSinceLastDurationUpdate = 0;
								_footerView.UpdateAudioDuration (AudioRecorder.Duration);
							}
							_footerView.UpdateAudioVolume (AudioRecorder.CurrentVolume);
						});
						Thread.Sleep (TIME_INTERVAL);
						timeElapsedSinceLastDurationUpdate += TIME_INTERVAL;
					}
				});
			}
		}

		void OnAudioRecordingEnded ()
		{
			Recording = false;
			if (AudioRecorder.IsRecording) {
				AudioRecorder.Stop ();
			}

			var recordingDuration = AudioRecorder.Duration;

			// Discard recordings of length less than 1 second
			if (recordingDuration < 1.0f) {
				OnAudioCancelBtnPressed (null, null); // Treat it as if user presed cancel button
				_footerView.ResetAudioRecordingViewsToDefault ();
			}
			else {
				_footerView.ShowPostAudioRecodingViews();
			}
		}

		void ShowMainFooterViews ()
		{
			_footerView.HideAudioRecordingView (true, true);
			_controlButtonsView.Hidden = false;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = BlrtStyles.iOS.White;
			KeyboardView.AddSubview (_tableView);
			KeyboardView.AddSubview (_footerView);
			KeyboardView.AddSubview (_controlButtonsView);

			_controlButtonsView.Hidden = true;
			_footerView.Hidden = _conversation == null;

			_loader = new BlrtiOS.Views.CustomUI.BlrtLoadingOverlayView ();
			View.AddSubview (_loader);

			//View.SetNeedsLayout ();
		}

		//NSObject _applicationDidBecomeActiveObserverObj;
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			//_applicationDidBecomeActiveObserverObj = NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.DidBecomeActiveNotification, SafeEventHandler.Actionize<NSNotification> (ApplicationDidBecomeActive));
			_tableView.ReloadData ();

			var isOverlaySet = TrySetHelpOverlays (true);
			if (isOverlaySet) {
				var overlay = (_helpOverlay as BlrtiOS.UI.HelpOverlay);
				if (overlay != null) {
					overlay.ClearEventHandler ();
					overlay.Open ();
				}
			}

			var rootController = RootController;

			if (rootController.BlrtCreationFailed) {
				rootController.BlrtCreationFailed = false;
			} else if (ShouldHideStatusBar) {

				ShouldHideStatusBar = false;
				return;
			}

			ShouldHideStatusBar = false;
			rootController.ShouldHideStatusBar = false;
            SetNeedsStatusBarAppearanceUpdate ();

			if (ScrollToBottomAfterSetConversation) {
				ScrollToBottomAfterSetConversation = false;
				ScrollToBottom (true);
			}
		}

		/* 
		 * needs to be nullable to identify it hasn't been set. Each of true and false for this property 
		 * notifies different overlay creation. So default value (false) might mean that overlay corresponding 
		 * to false has been used when in fact it might not have been used.
		 */
		bool? IsLastHelpOverlayForConvo_WithNonWelcomeBlrt { get; set; }

		// tells wheteher last help overlay was for convo with welcome blrt
		bool IsLastHelpOverlayForConvo_WithWelcomeBlrt { get; set; }

		/// <summary>
		/// This method shows help overlays if they are required
		/// </summary>
		bool TrySetHelpOverlays (bool checkDevice)
		{
			try
			{
				if (Conversation == null || !IsHelpOverlayNeeded) {
					return false;
				}

				var notValidDevice = checkDevice ? Device.Idiom == TargetIdiom.Tablet : false;
				if (notValidDevice || ParseUser.CurrentUser == null || _conversation == null) 
				{
					return false;
				}

				if (Conversation.ContainsWelcomeBlrt) {
					SetHelpOverlayForConvoWithWelcomeBlrt ();
				} else {
					SetHelpOverlayForConvoWithoutWelcomeBlrt ();
				}

				return true;
			} 
			catch (Exception e)
			{
				LLogger.WriteLine ("Failed to set help overlay views in ConversationController.TrySetHelpOverlays. Message = {0}", e.Message);
				return false;
			}
		}

		/// <summary>
		/// Tries to set help overlay for convo with welcome blrt.
		/// </summary>
		void SetHelpOverlayForConvoWithWelcomeBlrt ()
		{
			if (!IsLastHelpOverlayForConvo_WithWelcomeBlrt)
			{
				IsLastHelpOverlayForConvo_WithWelcomeBlrt = true;

				if (_helpOverlay != null) {
					_helpOverlay.Dispose ();
				}
				_helpOverlay = (UIView)HelpOverlayHelper.AddConversationHelpOverlay (true, true);
			}
		}

		/// <summary>
		/// Tries to set help overlay for convo without welcome blrt.
		/// </summary>
		/// <returns><c>true</c>, if set help overlay for convo without welcome blrt was tried, <c>false</c> otherwise.</returns>
		void SetHelpOverlayForConvoWithoutWelcomeBlrt ()
		{
			var hasBlrt = HasBlrt;
			if (_helpOverlay == null)
			{
				_helpOverlay = (UIView)HelpOverlayHelper.AddConversationHelpOverlay (hasBlrt, false);
				IsLastHelpOverlayForConvo_WithNonWelcomeBlrt = hasBlrt;
				IsLastHelpOverlayForConvo_WithWelcomeBlrt = false;
			}
			else
			{
				if (hasBlrt != IsLastHelpOverlayForConvo_WithNonWelcomeBlrt) {
					IsLastHelpOverlayForConvo_WithNonWelcomeBlrt = hasBlrt;
					_helpOverlay.Dispose ();
					_helpOverlay = (UIView)HelpOverlayHelper.AddConversationHelpOverlay (hasBlrt, false);
					IsLastHelpOverlayForConvo_WithWelcomeBlrt = false;
				}
			}
		}

		/// <summary>
		/// This property, based on the type of conversation, tells whether help overlay is needed 
		/// for the currently open conversation
		/// </summary>
		/// <returns><c>true</c>, if help overlay is needed, <c>false</c> otherwise.</returns>
		/// <param name="isEmptyConvo">If set to <c>true</c> is empty convo</param>
		bool IsHelpOverlayNeeded
		{ 
			get {
				if (Conversation.ContainsWelcomeBlrt) {
					return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed)
							|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed];
				}

				if (HasBlrt) {
					return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed)
							|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed];
				}

				return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationViewed)
						|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed];
			}
		}

		bool ShouldHideStatusBar { get; set; }
		public override bool PrefersStatusBarHidden ()
		{
			return ShouldHideStatusBar;
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			if (_newConversationSetupView != null && !_newConversationSetupView.Hidden) {
				if (_newConversationSetupView.Frame.IsEmpty) {
					_newConversationSetupView.Frame = View.Bounds;
				}
				return;
			}

			_footerView.UpdateView (IsConversationReadonly);
			var footerViewMinSize = _footerView.SizeThatFits (KeyboardView.Bounds.Size);
			footerViewMinSize.Height = NMath.Min (footerViewMinSize.Height, KeyboardView.Bounds.Height);

			var footerViewNewFrame = new CoreGraphics.CGRect (0, 
			                                                  KeyboardView.Bounds.Height - 
			                                                  footerViewMinSize.Height - 
			                                                  FOOTER_BOTTOM_PADDING, 
			                                                  KeyboardView.Bounds.Width, 
			                                                  footerViewMinSize.Height);
			if (!_footerView.Frame.Equals(footerViewNewFrame))
				_footerView.Frame = footerViewNewFrame;

			var controlButtonsViewNewFrame = new CoreGraphics.CGRect (0, _footerView.Frame.Top - CONTROL_BUTTON_VIEWHEIGHT, KeyboardView.Bounds.Width, CONTROL_BUTTON_VIEWHEIGHT);
			if (!_controlButtonsView.Frame.Equals (controlButtonsViewNewFrame))
				_controlButtonsView.Frame = controlButtonsViewNewFrame;

			var tableViewNewFrame = new CoreGraphics.CGRect (0, 0, KeyboardView.Bounds.Width, _footerView.Frame.Bottom);
			if (!_tableView.Frame.Equals (tableViewNewFrame))
				_tableView.Frame = tableViewNewFrame;


			var insets = new UIEdgeInsets (_tableView.ContentInset.Top,
										_tableView.ContentInset.Left,
										_controlButtonsView.Frame.Height + _footerView.Frame.Height,
										_tableView.ContentInset.Right);

			if (!insets.Equals (_tableView.ContentInset)) {
				_tableView.ContentInset = insets;
			}
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			/*if (_applicationDidBecomeActiveObserverObj != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_applicationDidBecomeActiveObserverObj);
			}*/
			//if (_conversation != null) {
			//	//_conversation.CleanUp ();
			//	_conversation.ReadAllComments ();
			//}

			ResetConvoScreenAfterConvoCreationProcess ();
		}

		public async Task Refresh ()
		{
			await _conversation.Refresh ();
			ReloadList ();

		}

		void ReloadList ()
		{
			if (_conversation != null) {
				//InvokeOnMainThread (delegate {});

				if (_conversation.NotSent) {
					_tableView.AlertView.Show (BlrtAlertsBarView.BlrtAlertType.NotSent, true);
				} else {
					_tableView.AlertView.Hide (BlrtAlertsBarView.BlrtAlertType.NotSent, true);
				}

				if (!_conversation.OnCloud) {
					_tableView.AlertView.Show (BlrtAlertsBarView.BlrtAlertType.ConversationUnsynced, true);
					_tableView.AlertView.Hide (BlrtAlertsBarView.BlrtAlertType.ContentUnsynced, true);

				} else {
					_tableView.AlertView.Hide (BlrtAlertsBarView.BlrtAlertType.ConversationUnsynced, true);

					if (!_conversation.OnContentCloud) {
						_tableView.AlertView.Show (BlrtAlertsBarView.BlrtAlertType.ContentUnsynced, true);
					} else {
						_tableView.AlertView.Hide (BlrtAlertsBarView.BlrtAlertType.ContentUnsynced, true);
					}
				}

				_tableView.TitleView.FilterCount = _conversation.UserFilter.Length;

				InvokeOnMainThread (delegate {
					TableViewBackgroundHidden = !IsEmpty;
				});

				_conversation.ReloadContents ();
				var content = !_conversation.HasContent;

				InvokeOnMainThread (delegate {
					SetTableViewTitle (_conversation.Name);
				});

				//if (content)
					//ScrollToBottom (false);

				ScrollToBottom (false);
			}
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			Xamarin.Forms.MessagingCenter.Unsubscribe<Object> (this, "DisplaynameChanged");
			DisposeNewConvoSetupView ();
		}

		void ScrollToBottom (bool animated)
		{
			InvokeOnMainThread (() => {
				try {
					var noris = _tableView.NumberOfRowsInSection (0);
					if (noris <= 0 || noris == int.MaxValue) {
						return;
					}
					var maxValue = int.MaxValue;

					if (noris > maxValue) {
						return;
					}

					if (noris > 0) {
						var row = NSIndexPath.FromRowSection (noris - 1, 0);
						_tableView.ScrollToRow (row, UITableViewScrollPosition.Middle, animated);
					}
					_tableView.TitleView.ForceShow (animated);
				} catch (Exception e) {
					e.ToString ();
				}
			});
		}

		void RefreshChanged (object sender, EventArgs args)
		{
			InvokeOnMainThread (delegate {
			if (_conversation != null) {
				_tableView.TitleView.RefreshButton.Refreshing = _conversation.Refreshing;
					if ((_tableView.TitleView.RefreshButton.Refreshing && _conversation.HasContent)) {
						_tableView.FooterView.StartAnimating ();
					} else {
						_tableView.FooterView.StopAnimating ();
                        ReloadList ();
					}
				}
			});
		}

		#region Media upload and save logic

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		ConversationMediaItem CreateConversationMediaItem(string id, string [] assetNames, BlrtMediaFormat format)
		{
			return new ConversationMediaItem () {
				ConversationId = id,
				ShouldUpload = true,
				EncryptType = BlrtEncryptorManager.RecommendedEncyption,
				MediaType = format,
				MediaNames = assetNames
			};
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		int ThumbnailImageLength(int assetPathsCount, BlrtMediaFormat format)
		{
			int length = 0;
			if (assetPathsCount == 1) {
				if (format == BlrtMediaFormat.Image) {
					length =  (int)Cells.ImageCell.SingleImageSize.Width;
				} else {
					length =  (int)Cells.PDFCell.SinglePDFCellSize.Width;
				}
			} else {
			 	length = (int)Cells.ImageCell.MultiImageSize.Width;
			} 

			return BlrtUtil.GetStandardThumbnailSize (length);
		}

		async Task<ExecutionResult<object>> SendMediaItem (string [] assetPaths, string[] assetNames, BlrtMediaFormat format)
		{ 
			_conversation.ReadAllComments ();

			var conversation = Conversation;
			var mediaConvoItem = CreateConversationMediaItem (conversation.ObjectId, assetNames, format);

			var assetPathsCount = assetPaths.Count ();
			mediaConvoItem.LocalThumbnailNames = new string [assetPathsCount];
			var mediaIds = new string [assetPathsCount];

			var thumbnailImageLength = ThumbnailImageLength (assetPathsCount, format);
			int i = 0;
			
			foreach (var path in assetPaths) {
				var mediaAsset = await BlrtUtil.CreateAsset (path, format);
				mediaAsset.Format = format;
				mediaAsset.Name = assetNames[i];
				if (format == BlrtMediaFormat.PDF) {
					var pageCount = BlrtPDF.FromFile (path).Pages;
					mediaConvoItem.Pages = pageCount;
					mediaAsset.PageArgs = BlrtUtil.GetRangeArray (1, pageCount);
				} else if (format == BlrtMediaFormat.Image) {
					mediaAsset.PageArgs = BlrtUtil.GetRangeArray (1, assetPathsCount);
				}
				mediaAsset.PageArgs = format == BlrtMediaFormat.Image ? 
					BlrtUtil.GetRangeArray (1, assetPathsCount) : 
					BlrtUtil.GetRangeArray(1, BlrtPDF.FromFile (path).Pages);

				string src = mediaAsset.DecyptedPath;

				await BlrtEncryptorManager.Encrypt (
					src,
					mediaAsset.LocalMediaPath,
					mediaAsset
				);

				mediaIds[i] = mediaAsset.ObjectId;

				mediaConvoItem.LocalThumbnailNames [i] = mediaAsset.LocalThumbnailName = string.Format ("{0}-thumb.jpg", mediaAsset.ObjectId); 

				if (format == BlrtMediaFormat.Image) {
					await BlrtUtilities.CreateImgFileThumbnail (assetPaths [i],
																mediaAsset.LocalThumbnailPath,
																new System.Drawing.Size (thumbnailImageLength, thumbnailImageLength));
				}
				else if (format == BlrtMediaFormat.PDF) {
					await BlrtUtilities.CreatePDFThumbnail (assetPaths [i],
															i + 1,
															mediaAsset.LocalThumbnailPath, // TODO: need to consider pages wth pdf as well.
															new System.Drawing.Size (thumbnailImageLength, thumbnailImageLength));
				}
				i++;

				mediaAsset.LocalStorage.DataManager.AddUnsynced (mediaAsset);
			}

			mediaConvoItem.ShouldUpload = true;
			conversation.ContentUpdatedAt = DateTime.Now;

			mediaConvoItem.Media = mediaIds;
			mediaConvoItem.StringifyArgument ();
			conversation.LastMedia = mediaConvoItem.Media;
			conversation.LastMediaArgument = mediaConvoItem.Argument;

			conversation.LocalStorage.DataManager.AddUnsynced (mediaConvoItem);
			conversation.AddUnusedConversationMediaItems (mediaIds);

			var item = mediaConvoItem.LocalGuid;

			SetTableViewForDisplay ();

			TryUploadItem (mediaConvoItem).FireAndForget ();

			return new ExecutionResult<object> (item);
		}

		#endregion

		#region Audio upload and save logic

		async Task SendAudioConversationItem (string decryptedAudioPath, float audioLength, string audioExt)
		{
			_conversation.ReadAllComments ();

			var conversation = Conversation;

			var content = new ConversationAudioItem () {
				ConversationId = conversation.ObjectId,
				ShouldUpload = true,
				EncryptType = BlrtEncryptorManager.RecommendedEncyption,
				AudioLength = (int)audioLength
			};

			content.LocalAudioName = content.ObjectId + "-audio." + audioExt;

			try {
				await BlrtEncryptorManager.Encrypt (
					decryptedAudioPath,
					content.LocalAudioPath,
					content
				);
			} catch (BlrtEncryptorManagerException enc_ex) {
				if (enc_ex.Code == BlrtEncryptorManagerException.ExceptionCodes.EncryptionFailed) {
					await CanvasPageView.PlatformHelper.ShowAlertAsync (
						"Encryption failed",
						"Unable to encrypt file. Please contact Blrt Support.",
						"OK"
					);
				}
				LLogger.WriteEvent ("encrypt", "error", "encryption failed", "AudioConversationItem: audio; error: " + enc_ex.ToString ());
			}

			content.StringifyArgument ();

			conversation.ContentUpdatedAt = DateTime.Now;
			conversation.LastMediaArgument = content.Argument;
			conversation.LocalStorage.DataManager.AddUnsynced (content);

			SetTableViewForDisplay ();

			await TryUploadItem (content);
		}

		#endregion

		void HideEmptyConvoViews (bool hidden, bool isFirstConvoViewAfterCreation)
		{
			if (_tableView == null)
				return;
			
			_tableView.HideBackgroundView (hidden);
			_tableView.HideHeaderViews (!hidden);
			_tableView.IsFirstConvoViewAfterCreation = isFirstConvoViewAfterCreation;
		}

		void SetTableViewForDisplay (bool isFirstConvoViewAfterCreation = false)
		{
			if (IsEmpty)
				return;
			
			ReloadList ();
			InvokeOnMainThread (() => {
				HideEmptyConvoViews (true, isFirstConvoViewAfterCreation);
				ScrollToBottom (true);
			});
		}

		#region Comment upload and save logic

		async Task SendComment ()
		{
			if (string.IsNullOrWhiteSpace (_footerView.CommentText))
				return;

			_footerView.EndEditing (true);

			await BlrtManager.ApplyPermission (_conversation.PermissionHandler);

			if (!BlrtPermissions.CanCreateComment) {

				return;
			}

			var item = CreateComment (ConversationId, _footerView.CommentText);
			_footerView.CommentText = "";
			SetTableViewForDisplay ();
			await TryUploadItem (item);
		}

		/// <summary>
		/// Tries to upload item and reloads conversation to update view
		/// </summary>
		/// <returns>The upload item.</returns>
		/// <param name="item">Item.</param>
		async Task TryUploadItem (BlrtBase item)
		{
			try {
				var uploadTask = UploadItem (item);
				ReloadList ();
				await uploadTask;
			} catch {
			}

			ReloadList ();
		}

		public ConversationCommentItem CreateComment (string conversationId, string text)
		{
			_conversation.ReadAllComments ();

			var cacheComment = new ConversationCommentItem () {
				ConversationId = conversationId,
				Comment = text,
				ShouldUpload = true
			};

			_conversation.LocalStorage.DataManager.AddUnsynced (cacheComment);

			return cacheComment;
		}

		/// <summary>
		/// Uploads the item.
		/// </summary>
		/// <param name="item">Item.</param>
		async Task UploadItem (BlrtBase item)
		{
			if (!_conversation.OnParse) {
				var success = await UploadConversation ();
				if (!success) {
					//return;
				}
			}

			await BlrtUtil.UploadItem (item);
		}

		/// <summary>
		/// Uploads the conversation.
		/// </summary>
		/// <returns>The conversation.</returns>
		async Task<bool> UploadConversation ()
		{
			if (!_conversation.OnParse) {
				var uploader = await BlrtUtil.UploadItem (Conversation, true);
				if (uploader.Expection != null) {
					return false;
				}
				await RootController.RefreshMailbox ();
			}
			return true;
		}

		#endregion

		public async Task<Tuple<bool, string>> ConfirmNewConvoCreation (bool presentConversationSetupViewInSeparateController = false)
		{
			ConversationNameSetupViewParent newConversationSetupView = null;

			if (presentConversationSetupViewInSeparateController) {
				newConversationSetupView = new ConversationNameSetupViewParent ();
			} else {
				if (_newConversationSetupView == null) {
					_newConversationSetupView = new ConversationNameSetupViewParent ();
				}
				newConversationSetupView = _newConversationSetupView;
			}

			newConversationSetupView.Hidden = false;

			var tcs = new TaskCompletionSource<Tuple<bool, string>> ();
			newConversationSetupView.ConvoSaveBtnPressed += delegate {
				if (string.IsNullOrWhiteSpace (newConversationSetupView.ConversationName)) {
					return;
				}

				if (!presentConversationSetupViewInSeparateController) {
					tcs.TrySetResult (new Tuple<bool, string> (true, newConversationSetupView.ConversationName));
					/* 
					 * Setupview will be presented inside default conversation controller. 
					 * This controller is the one that displays the selected conversation.
					 */ 
					TableViewBackgroundHidden = false;
					ResetConvoScreenAfterConvoCreationProcess ();
				} else {
					DismissViewController (true, delegate {
						tcs.TrySetResult (new Tuple<bool, string> (true, newConversationSetupView.ConversationName));
					});
				}
			};

			newConversationSetupView.CancelBtnPressed += delegate {
				tcs.TrySetResult (new Tuple<bool, string> (false, string.Empty));

				if (!presentConversationSetupViewInSeparateController) {
					/* 
					 * Setupview will be presented inside default conversation controller. 
					 * This controller is the one that displays the selected convers.
					 */
					ResetConvoScreenAfterConvoCreationProcess ();
				} else {
					DismissViewController (true, null);
				}
			};

			if (!presentConversationSetupViewInSeparateController) {
				HideEmptyConvoViews (false, true); // hide previously opened conversation
				View.AddSubview (newConversationSetupView);
			} else {
				var newConvoController = new UINavigationController (new BlrtiOS.Views.Mailbox.ContainerForBlrtImportController (newConversationSetupView, 
				                                                                                                                 "Create a new conversation", false)) {
					ModalPresentationStyle = UIModalPresentationStyle.PageSheet,
				};

				PresentViewController (newConvoController, true, null);
			}

			newConversationSetupView.SetInitialConvoName (BlrtUtil.NewConversationDefaultName);
			return await tcs.Task;
		}

		/// <summary>
		/// Gets the table background view info. Message displayed in the table view 
		/// if either convo is empty or no convo is selected
		/// </summary>
		/// <value>The table background view info.</value>
		string TableBackgroundViewInfo {
			get {
				return _conversation == null ? NoConvoSelectedInfo : EmptyConvoInfo;
			}
		}

		/// <summary>
		/// Sets a value indicating whether table view background hidden.
		/// </summary>
		/// <value><c>true</c> if table view background hidden; otherwise, <c>false</c>.</value>
		bool TableViewBackgroundHidden
		{
			set {
				_tableView.BackgroundViewInfo = TableBackgroundViewInfo;
				if (_tableView.BackgroundView.Hidden != value) {
					_tableView.BackgroundView.Hidden = value;
				}
			}
		}

		bool TableViewHidden
		{
			set {
				_tableView.Hidden = value;
			}
		}

		bool TableViewHeaderHidden
		{
			set {
				_tableView.TitleView.Hidden = _tableView.AlertView.Hidden = value;
			}
		}

		bool TableViewScrollEnabled
		{
			set {
				_tableView.ScrollEnabled = value;
			}
		}

		void ResetConvoScreenAfterConvoCreationProcess ()
		{
			if (_newConversationSetupView != null && !_newConversationSetupView.Hidden) 
			{
				TableViewHeaderHidden = false;
                TableViewScrollEnabled  = true;
                HideAllNewConversationViews ();
				DisposeNewConvoSetupView ();
			}
		}

		void DisposeNewConvoSetupView ()
		{
			if (_newConversationSetupView != null) {
				_newConversationSetupView.Dispose ();
				_newConversationSetupView = null;
			}
		}

		void CreateNewConversationHandler (string conversationId, LocalStorageType localStorageType)
		{
			_conversation = new ConversationHandler (conversationId, localStorageType)
			{
				ConversationScreen = this,
				ActionBuilder = _actionBuilder
			};
			SetEventHandlersForConversation ();
			_conversation.SetIsItemRestrictionApplied ();
		}

		void SetEventHandlersForConversation ()
		{
			_conversation.CloudStateChanged += (sender, e) => {
                ReloadList ();
			};

			_conversation.RefreshChanged += RefreshChanged;

			_conversation.OnOpenSend += (sender, e) => {
				OpenSendingPage (this, null).FireAndForget ();
			};
		}

		/// <summary>
		/// Sets the conversation. This method is suppose to update conversation data with 
		/// new data so that new conversation data can be displayed.
		/// </summary>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="localStorageType">Local storage type.</param>
		void SetConversation (string conversationId, LocalStorageType localStorageType)
		{
			if (string.IsNullOrEmpty (conversationId)) 
			{
				// just remove currently displayed conversation
				UnsubscribeConversation ();
				_conversation = null;
				_conversationModel = null;
			} 
			else if (_conversation == null || ConversationId != conversationId) 
			{
				_conversationModel = null;

				BlrtUtil.DeleteDecryptedFiles ();
				UnsubscribeConversation ();

				//create new conversation handler
				CreateNewConversationHandler (conversationId, localStorageType);

				//_conversation.ReloadContents ();
				//_tableView.List = _conversation.GetList ();

				if (!_tableView.Hidden) {
					//TODO: set flag for  //ScrollToBottom (false);
					ScrollToBottomAfterSetConversation = true;
				}

				SetTableViewTitle (_conversation.Name);

				//subscribe
				MessagingCenter.Subscribe<object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), 
                     async (o, refreshFailed) => {
						 if (refreshFailed) {
							 await Refresh ();
						 } else {
							 ReloadList ();
						 }
				});

				MessagingCenter.Subscribe<object> (this, string.Format ("{0}.PersonRemoved", 
				                                                              _conversation.LocalGuid),
					 delegate {
						ReloadList ();
					}
				);

				// subscibe for deleted user update
				MessagingCenter.Subscribe<object, string> (this, string.Format ("{0}.ConverationUserDeleted", _conversation.LocalGuid), OnUserDeleted);

				MessagingCenter.Subscribe<Object> (this, 
                    string.Format ("{0}.SendingFailedContactsChanged", _conversation.LocalGuid), 
                	delegate {
						if (_tableView != null && _conversation != null) {
							ReloadList ();
						}
					}
				);

			}
		}

		/// <summary>
		/// Triggered when a user deleted.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="userId">User identifier.</param>
		void OnUserDeleted (object sender, string userId)
		{
			// Update conversation model
			Conversation = ConversationFromLocalStorage;

			// update convo UI accordingly
			InvokeOnMainThread (delegate {
                SetTableViewFooter ();
				_conversation.OnConversationUserDeleted (userId);

				var indexPaths = GetIndexPathsForCellsToUpdate (userId);
				_tableView.ReloadRows (indexPaths, UITableViewRowAnimation.Automatic);
			});
		}

		/// <summary>
		/// Gets the index paths for cells to update.
		/// </summary>
		/// <returns>The index paths for cells to update.</returns>
		/// <param name="userId">User identifier.</param>
		NSIndexPath[] GetIndexPathsForCellsToUpdate (string userId) 
		{
			return _tableView.IndexPathsForVisibleRows.Where (indexPath => {
				var cell = _tableView.CellAt (indexPath);
				var model = _tableView.GetCellForRow (cell); 

				return ((model?.Author?.ObjectId ?? string.Empty) == userId &&
						(model.ShowAuthor || model.IsLastConversationItemBySameAuthor));
			}).ToArray ();
		}
		
		/// <summary>
		/// Refreshes the conversation. Refreshes either entire conversation or particular conversation item.
		/// </summary>
		void RefreshConversation ()
		{
			if (_conversation != null && _conversation.ShouldRefreshContent) {
				if (_tableView.List.Count > 0) {
					var sec = _tableView.List.ElementAt (0);
					if (sec.Count > 0 && sec.ElementAt (0).CellIdentifer == ConversationCellModel.MissingContentCellIndentifer) {
						sec.ElementAt (0).Action.Action (this);
					} else {
						Refresh ().FireAndForget ();
					}
				} else {
					Refresh ().FireAndForget ();
				}
			}
		}

		/// <summary>
		/// Sets the conversation table view. This method is suppose to set views in conversation table view.
		/// </summary>
		void SetConversationTableView ()
		{
			TableViewHidden = false;
			TableViewBackgroundHidden = false;

			if (_conversation != null)
			{
				HideEmptyConvoViews (!IsEmpty, false);

				TableViewHeaderHidden = false;
				TableViewScrollEnabled  = true;
			} 
			else
			{
				_tableView.List?.Clear ();
				_tableView.ReloadData ();

				TableViewHeaderHidden = true;
				TableViewScrollEnabled = false;
			}
		}

		/// <summary>
		/// Sets the table view footer. Footer consists of: "Blrt Reply/ New Blrt" button, "Add People" button and
		/// bar with "+", comment bar, camera icon, mic icon
		/// </summary>
		void SetTableViewFooter ()
		{
			if (_conversation != null && !IsConversationReadonly) {
				_controlButtonsView.SetBlrtButton (HasBlrt);
			}
            
			SetTableViewFooterVisibility ();
			_controlButtonsView.UpdateButtonsTintColor (IsConversationReadonly);
			_footerView.UpdateView (IsConversationReadonly);
		}

		/// <summary>
		/// Sets the table view footer visibility. Footer consists of: "Blrt Reply/ New Blrt" button, "Add People" 
		/// button and bar with "+", comment bar, camera icon, mic icon
		/// </summary>
		void SetTableViewFooterVisibility()
		{
			var hide = _conversation == null;

			if (_controlButtonsView.Hidden != hide) {
				_controlButtonsView.Hidden = _footerView.Hidden = hide;
			}
		}

		/// <summary>
		/// Sets the conversation options button.
		/// </summary>
		void SetConversationOptionsButton ()
		{
			NavigationItem.SetRightBarButtonItems (new UIBarButtonItem [] {
					_moreBtn // options button with ellipses
				}, true);
		}

		/// <summary>
		/// Sets the conversation data when new conversation needs to be displayed.
		/// <para>
		/// There was a bug with <c>_tableView.List.Clear</c> which was resolved by checking the null reference.
		/// </para>
		/// </summary>
		/// <returns>The conversation.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="localStorageType">Local storage type.</param>
		/// <param name="IsNewConversation">If set to <c>true</c> is new conversation.</param>
		public Task SetConversation (string conversationId, LocalStorageType localStorageType = LocalStorageType.Default, bool IsNewConversation = false)
		{
			Recording = false;

			SetConversation (conversationId, localStorageType);
			SetConversationTableView ();

            SetTableViewFooter ();

            SetConversationOptionsButton ();
			ReloadList ();
			if (ConversationId != null) {
				_tableView.List = _conversation.GetList ();
			} else 
			{
				if (_tableView.List != null) 
				{
					_tableView.List.Clear ();
				}
			}
			RefreshConversation ();
			//RefreshChanged (this, EventArgs.Empty);

			TrySetHelpOverlays (false);

			SetConversationTracking ();

			return Task.CompletedTask;
		}

		/// <summary>
		/// Sets the conversation tracking to track the user's activity.
		/// </summary>
		void SetConversationTracking ()
		{
			if (_conversation != null) 
			{
				var dic = new Dictionary<string, object> ();
				var rel = _conversation.LocalStorage.DataManager.GetConversationUserRelation (
					ConversationId,
					ParseUser.CurrentUser.ObjectId
				);

				if (rel != null) {
					dic.Add ("isOwner", rel.CreatorId == ParseUser.CurrentUser.ObjectId);
				} else if (!_conversation.OnParse) {
					dic.Add ("isOwner", true);
				} else {
					#if DEBUG
					Console.WriteLine ("No relation found");
					#endif
				}
				dic.Add ("convoId", ConversationId);

				BlrtData.Helpers.BlrtTrack.TrackScreenView ("Conversation", false, dic);
			}
		}

		/// <summary>
		/// Sets the table view title. Title is the name of the conversation
		/// </summary>
		/// <param name="title">Title.</param>
		void SetTableViewTitle (string title)
		{
			_tableView.TitleView.SetTitle (title);
		}

		/// <summary>
		/// Cleans up and unsubscribes the conversation
		/// </summary>
		void UnsubscribeConversation ()
		{
			if (_conversation != null) {
				_conversation.RefreshChanged -= RefreshChanged;
				_conversation.CleanUp ();
				_conversation.ReadAllComments ();

				//unsubscribe
				MessagingCenter.Unsubscribe<Object> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid));
				MessagingCenter.Unsubscribe<Object> (this, "ConversationNameChnaged");
				MessagingCenter.Unsubscribe<Object> (this, string.Format ("{0}.SendingFailedContactsChanged", _conversation.LocalGuid));
				MessagingCenter.Unsubscribe<object, string> (this, string.Format ("{0}.ConverationUserDeleted", _conversation.LocalGuid));
				MessagingCenter.Unsubscribe<object, string> (this, string.Format ("{0}.PersonRemoved", _conversation.LocalGuid));
			}
		}

		/// <summary>
		/// Hides all new conversation views. These views include the buttons to cancel or create new conversation
		/// and text field to provide conversation name
		/// </summary>
		void HideAllNewConversationViews ()
		{
			if (_newConversationSetupView != null) {
				_newConversationSetupView.RemoveFromSuperview ();
				_newConversationSetupView.Hidden = true;
			}
		}

		public UIView _helpOverlay;
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (_helpOverlay != null && _helpOverlay.Superview != null) {
				_helpOverlay.RemoveFromSuperview ();
			}
		}

		IEnumerable<string> UnusedConversationMediaItems {
			get {
				return Conversation.UnusedConversationMediaItems;
			}
		}

		public async Task<bool> WaitDownloadAndDecryptUnusedConversationContent ()
		{
			var t = new BlrtData.IO.BlrtDownloaderList (null);
			var conversation = Conversation;
			var mediaAssets = BlrtUtil.GetMediaAssets (conversation, UnusedConversationMediaItems.ToArray ());
			return await WaitDownloadAndDecryptContent (conversation.DownloadUnusedConversationMedia, mediaAssets);
		}

		async Task<bool> WaitDownloadAndDecryptContent (BlrtUtil.DownloadMediaDelegate downloadMedia, IEnumerable<MediaAsset> mediaAssets)
		{
			return await BlrtUtil.DownloadAndDecryptContent (downloadMedia, mediaAssets);
		}

		public async Task DownloadContent (ConversationCellModel model)
		{
			const int MaxRetry = 2;
			for (int i = 0; i <= MaxRetry; ++i) {
				var item = BlrtManager.DataManagers.Default.GetConversationItem (model.ContentId);

				var media = item.LocalStorage.DataManager.GetMediaAssets (item);

				var downloaders = BlrtUtil.Concat (
					BlrtUtil.Concat (
						from m in media
						where !m.IsDataLocal ()
						select m.DownloadContent (1000)
					),
					item.IsDataLocal () ? null : item.DownloadContent (1000)
				);
				if (downloaders.Length > 0) {
					await new BlrtData.IO.BlrtDownloaderList (downloaders).WaitAsync ();
				}
				bool succeeded = true;
				foreach (var m in media) {
					if (!m.IsDecypted) {
						try {
							await m.DecyptData ();
						} catch (BlrtEncryptorManagerException e1) {
							succeeded = false;
							m.DeleteData ();
						} catch (InvalidOperationException e2) {
							succeeded = false;
							m.DeleteData ();
						}
					}
				}
				var blrt = item as ConversationBlrtItem;
				if (null != blrt) {
					if (!blrt.IsAudioDecrypted) {
						try {
							await blrt.DecyptAudioData ();
						} catch (BlrtEncryptorManagerException e3) {
							succeeded = false;
							blrt.DeleteAudioData ();
						} catch (InvalidOperationException e4) {
							succeeded = false;
							blrt.DeleteAudioData ();
						}
					}
					if (!blrt.IsTouchDecrypted) {
						try {
							await blrt.DecyptAudioData ();
						} catch (BlrtEncryptorManagerException e5) {
							succeeded = false;
							blrt.DeleteTouchData ();
						} catch (InvalidOperationException e6) {
							succeeded = false;
							blrt.DeleteTouchData ();
						}
					}
				}
				if (succeeded) {
					break;
				}
			}
		}

		/// <summary>
		/// Tries to upload the item represending the given cell model.
		/// </summary>
		/// <returns>The upload item.</returns>
		/// <param name="model">Model.</param>
		public Task TryUploadItem (ConversationCellModel model)
		{
			var item = _conversation.LocalStorage.DataManager.GetConversationItem (model.ContentId);
			return TryUploadItem (item);
		}


		/// <summary>
		/// Gets a conversation item, and show loading dialog when downloading
		/// </summary>
		/// <returns>The item.</returns>
		/// <param name="objectId">conversation item objectID.</param>
		/// <param name="localStorage">Storage position</param>
		/// <exception cref="Exception">This method will throw any exceptions happened when downloading the item</exception>
		public async Task<ConversationItem> GetItem (string objectId, ILocalStorageParameter localStorage)
		{

			var item = localStorage.DataManager.GetConversationItem (objectId);

			if (item == null) {
				var query = BlrtData.Query.BlrtConversationItemQuery.RunQuery (objectId, localStorage);

				await BlrtHelperiOS.ShowWaitingAlertAsync (
					query.WaitAsync (),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_title"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_message"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_cancel")
				);

				item = localStorage.DataManager.GetConversationItem (objectId);
			}

			return item;
		}

		private async Task<ConversationOptionsPage.ConversationOptionResult> OpenConvoOption (UIViewController parent, string convoId, ILocalStorageParameter localStorage, string [] filteredIds = null)
		{
			var rel = localStorage.DataManager.GetConversationUserRelation (
				  convoId,
				  ParseUser.CurrentUser.ObjectId
			  );

			if (rel == null)
				throw new Exception ("Relation doesn't exist.");

			var optionPage = new ConversationOptionsPage ((IConversationScreen)parent, rel, filteredIds, IsConversationReadonly);
			optionPage.ShowLoading += (sender, e) => {
				var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
				app.PushLoading (true);
			};
			optionPage.HideLoading += (senders, es) => {
				var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
				app.PopLoading (true);
			};
			var controller = optionPage.CreateViewController ();
			controller.Title = optionPage.Title;

			parent.NavigationController.PushViewController (controller, true);

			var result = await optionPage.WaitTask ();

			if ((!result.Navigated) && (parent.NavigationController.VisibleViewController == controller)) {
				parent.NavigationController.PopViewController (true);
			}

			return result;
		}


		#region IConversationScreen implementation

		public async Task<ExecutionResult<object>> OpenContentItem (IExecutor e, string contentId)
		{
			ConversationItem item = null;
			try {

				item = await GetItem (contentId, LocalStorageParameters.Default);

				if (item.Type == BlrtContentType.Audio) {
					return new ExecutionResult<object> (new object ());
				}

			} catch (Exception ex1) {
				return await RetryOpenningContentItem (e, contentId);
			}

			if (!BlrtIssueChecker.TryIssueCheck (item)) {
				return new ExecutionResult<object> (new Exception ("unsupported data"));
			}

			if (item.Type != BlrtContentType.Blrt && 
			    item.Type != BlrtContentType.BlrtReply && 
			    item.Type != BlrtContentType.Media) 
			{
				return new ExecutionResult<object> (new Exception ("unsupported data"));
			}

			_conversation.ReadAllComments ();

			var result = await DownloadContent (item, e, contentId);
			//Refresh ().FireAndForget();

			if (result.Exception != null) // Cancelled download
				return result;
			
			try {
				if (item.Type == BlrtContentType.Blrt || item.Type == BlrtContentType.BlrtReply) {
					await DecryptBlrtContent (item as ConversationBlrtItem);
				} else if (item.Type == BlrtContentType.Media) {
					await DecryptMediaContent (item as ConversationMediaItem);
				}
			} catch (Exception ex) {
				return await RetryOpenningContentItem (e, contentId);
			}

			switch (item.Type) {
				case BlrtContentType.Blrt:
				case BlrtContentType.BlrtReply:
					return await OpenBlrtOnBlrtItemTap (item as ConversationBlrtItem, e, contentId);
				case BlrtContentType.Media:
					return await OpenMediaOnMediaItemTap (item as ConversationMediaItem);
				default:
					break;
			}

			return new ExecutionResult<object> (new object ());
		}

		/// <summary>
		/// Decrypts the content of the conversation media item
		/// </summary>
		/// <returns>The media content.</returns>
		/// <param name="conversationMediaItem">Conversation media item.</param>
		async Task DecryptMediaContent (ConversationMediaItem conversationMediaItem)
		{
			using (UserDialogs.Instance.Loading ("Loading...")) {
				Exception lastDecryptionException = null;
				lastDecryptionException = await BlrtUtil.DecryptMedia (conversationMediaItem.
															  LocalStorage.
															  DataManager.
															  GetMediaAssets (conversationMediaItem).ToArray ());
				if (lastDecryptionException != null)
					throw lastDecryptionException;
			}
		}

		/* 
		 * it is used to reopen the item in the viewer if, after pressing blrtThis button in viewer, 
		 * user decides not to create blrt by pressing the cancel button in the media picker.
		*/
		Tuple<ConversationMediaItem, int> CurrentMediaItemOnDisplay { get; set; }

		/// <summary>
		/// Indicates whether this conversation is readonly.
		/// </summary>
		/// <value><c>true</c> if is conversation readonly; otherwise, <c>false</c>.</value>
		bool IsConversationReadonly { 
			get {
				return Conversation?.IsReadonly ?? false;
			}
		}

		/// <summary>
		/// Opens the media on media item tap. This methods opens the media viewer when user 
		/// taps on the conversation media item.
		/// </summary>
		/// <returns>The media on media item tap.</returns>
		/// <param name="item">Item.</param>
		async Task<ExecutionResult<object>> OpenMediaOnMediaItemTap (ConversationMediaItem item, int initialAssetNum = 1)
		{
			var media = item.LocalStorage.DataManager.GetMediaAssets (item.Media);

			if (!BlrtIssueChecker.TryIssueCheck (media)) {
				return new ExecutionResult<object> (new object ());
			}

			BaseMediaViewerController blrtThisButtonController = null;

			if (item.MediaType == BlrtMediaFormat.Image) {
				blrtThisButtonController = new ImagePreviewController (media.ToList (), initialAssetNum, !IsConversationReadonly);
			}

			if (item.MediaType == BlrtMediaFormat.PDF) {
				blrtThisButtonController = new PDFPreviewController (media.ToArray () [0], !IsConversationReadonly);
			}

			blrtThisButtonController.BlrtThisBtnPressed += (o, e) => {
				if (IsConversationReadonly) {
					BlrtUtil.ShowConversationReadonlyAlert ();
					return;
				}

				CurrentMediaItemOnDisplay = new Tuple<ConversationMediaItem, int> (item, blrtThisButtonController.CurrentAssetNum);
                OnBlrtThisButtonPressed (o, e);
			};

			NavigationController.PushViewController (blrtThisButtonController, true);
			var t = BlrtUtil.TryMarkingConversationItemAsRead (item);
			Console.WriteLine ("\nItemread = {0}\n", t);

			return new ExecutionResult<object> (new object ());
		}

		/// <summary>
		/// This method is called when a user taps on the blrtThis button availble in the media viewer.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void OnBlrtThisButtonPressed (object sender, BlrtThisBtnPressedEventArgs e)
		{
			var mediaAssets = GetBlrtThisMediaModel(e.Assets);

			/*var mediaUsedForBlrt = BlrtUtil.IsMediaUsedForBlrt (mediaAsset.ObjectId, 
			                                                    UnusedConversationMediaItems?.ToHashSet (), 
			                                                    _conversation);*/

			NavigationController.PopControllerWithFade();

            OpenReBlrtCreation (this,
			                    null,
			                    false,
                                GetBlrtThisMediaModel(e.Assets)).FireAndForget ();
		}

		/// <summary>
		/// Gets the blrt this media model.
		/// </summary>
		/// <returns>The blrt this media model.</returns>
		/// <param name="assets">Assets.</param>
		Tuple<IEnumerable<MediaAsset>, HashSet<string>> GetBlrtThisMediaModel (MediaAsset [] assets)
		{
			var mediaAssetIds = new HashSet<string> ();
			var mediaAssets = new List<MediaAsset> ();
			var unusedItems = UnusedConversationMediaItems?.ToHashSet ();

			foreach (var asset in assets) {

				// depending on cloud state, both of them might not be same
				mediaAssetIds.Add (asset.ObjectId);
				mediaAssetIds.Add (asset.LocalGuid);

				var mediaUsedForBlrt = BlrtUtil.IsMediaUsedForBlrt (asset.ObjectId,
				                                                    unusedItems,
				                                                    _conversation);
				if (!mediaUsedForBlrt) {
					mediaAssets.Add (asset);
				}
			}

			return new Tuple<IEnumerable<MediaAsset>, HashSet<string>> (mediaAssets, mediaAssetIds);
		}

		async Task<ExecutionResult<object>> DownloadContent (ConversationItem item, IExecutor e, string contentId)
		{
			try {
				var state = item.GetCloudState ();

				if (state != CloudState.UploadingContent) {

					var IsDownloadSuccessful = await BlrtUtilities.DownloadContent (GetMediaDownloaders (item));
					if (!IsDownloadSuccessful)
						return new ExecutionResult<object> (new Exception ("Cancelled download."));
				}

			} catch (Exception ex2) {
				return await RetryOpenningContentItem (e, contentId);
			}

			return new ExecutionResult<object> (new object ());
		}

		async Task DecryptBlrtContent (ConversationBlrtItem blrt)
		{
			using (UserDialogs.Instance.Loading ("Loading...")) {
				Exception lastDecryptionException = null;
				lastDecryptionException = await BlrtUtil.DecryptMedia ( 
															      blrt.
										                          LocalStorage.
										                          DataManager.
										                          GetMediaAssets (blrt).
										                          ToArray () );

				if (null != blrt) {
					if (!blrt.IsAudioDecrypted) {
						try {
							await blrt.DecyptAudioData ();
						} catch (BlrtEncryptorManagerException e3) {
							lastDecryptionException = e3;
							blrt.DeleteAudioData ();
						} catch (InvalidOperationException e4) {
							lastDecryptionException = e4;
							blrt.DeleteAudioData ();
						}
					}
					if (!blrt.IsTouchDecrypted) {
						try {
							await blrt.DecyptAudioData ();
						} catch (BlrtEncryptorManagerException e5) {
							lastDecryptionException = e5;
							blrt.DeleteTouchData ();
						} catch (InvalidOperationException e6) {
							lastDecryptionException = e6;
							blrt.DeleteTouchData ();
						}
					}
				}
				if (null != lastDecryptionException) {
					throw lastDecryptionException;
				}
			}
		}

		BlrtData.IO.BlrtDownloader[] GetMediaDownloaders (ConversationItem item)
		{
			var media = item.LocalStorage.DataManager.GetMediaAssets (item);

			return BlrtUtil.Concat (
				BlrtUtil.Concat (
					from m in media
					where !m.IsDataLocal ()
					select m.DownloadContent (1000)
				),
				item.IsDataLocal () ? null : item.DownloadContent (1000)
			);
		}

		async Task<ExecutionResult<object>> RetryOpenningContentItem(IExecutor e, string contentId)
		{
			bool retry = false;

			retry = await BlrtUtilities.AskUserForRetry ();

			if (retry) {
				return await OpenContentItem (e, contentId);
			}

			return new ExecutionResult<object> (new Exception ("Failed to find conversation"));
		}

		async Task<ExecutionResult<object>> OpenBlrtOnBlrtItemTap (ConversationBlrtItem item, IExecutor e, string contentId)
		{
			BlrtData.Helpers.BlrtTrack.ConvoBlrtClick (item.ObjectId);
			try {
				if (BlrtIssueChecker.TryIssueCheck (item)) {
					var media = item.LocalStorage.DataManager.GetMediaAssets (item.Media);
					if (!BlrtIssueChecker.TryIssueCheck (media))
						return new ExecutionResult<object> (new object ());

					var data = BlrtCanvasData.FromConversationItem (item as ConversationBlrtItem);
					if (item.IsPublicBlrt) {
						var encodedId = BlrtEncode.EncodeId (item.ObjectId);
						MessagingCenter.Subscribe (this, BlrtConstants.FormsMessageId.BlrtPlayedOnce, (object sender) => {
							IncreasePlayCounter (encodedId);
						});
					}
					//track open event
					BlrtData.Helpers.BlrtTrack.TrackViewedContent (item.IsPublicBlrt ? "Public" : "Private", item.ObjectId);
					var dic = new Dictionary<string, object> ();
					dic.Add ("type", item.IsPublicBlrt ? "public" : "private");
					dic.Add ("blrtLength", (item as ConversationBlrtItem).AudioLength);
					dic.Add ("blrtId", item.ObjectId);
					dic.Add ("numberOfPages", (item as ConversationBlrtItem).Pages);
					BlrtData.Helpers.BlrtTrack.TrackScreenView ("Canvas_Playback", false, dic);

					var result = await BlrtManager.App.OpenCanvas (this, data);
					if (item.IsPublicBlrt) {
						MessagingCenter.Unsubscribe<object> (this, BlrtConstants.FormsMessageId.BlrtPlayedOnce);
					}

					if (result.Result.ShowMediaPicker) {
						if (IsConversationReadonly) {
							BlrtUtil.ShowConversationReadonlyAlert ();
						} else {
							await BlrtData.BlrtManager.App.OpenBlrtCreation (
								this,
								data,
								false
							);
							ReloadList ();
						}
					}
					return new ExecutionResult<object> (new object ());
				}

			} catch (Exception exception) {
				BlrtUtil.Debug.ReportException (exception, null, Xamarin.Insights.Severity.Error);

				//clear local data and try again
				var localStorage = LocalStorageParameters.Default;
				var media = localStorage.DataManager.GetMediaAssets (item);
				foreach (var m in media) {
					m.DeleteData ();
				}

				localStorage.DataManager.Remove (media);
				localStorage.DataManager.Remove (item);
				await localStorage.DataManager.SaveLocal ();
				return await OpenContentItem (e, contentId);
			}
			return new ExecutionResult<object> (new object ());
		}

		async void IncreasePlayCounter (string encodedId)
		{
			var apiCall = new APIPublicConversationItemView (encodedId);
			await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000));
		}

		public async Task<ExecutionResult<object>> OpenSendingPage (IExecutor e, IRequestArgs args)
		{
			LLogger.WriteLine ("==ADPSL Step 1/14, Add-people btn pressed, time = {0}=======", DateTime.Now);
			if (_helpOverlay != null && _helpOverlay.Superview != null) {
				_helpOverlay.RemoveFromSuperview ();
			}
			_conversation.ReadAllComments ();
			await BlrtManager.ApplyPermission (_conversation.PermissionHandler);
			var convo = Conversation;

			if (convo.IsSpecialConversation) {
				new UIAlertView (
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_special_title", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_special_message", "conversation_screen"),
					null,
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_special_accept", "conversation_screen"),
					null
				).Show ();
				return new ExecutionResult<object> (new object ());
			}

			if (await BlrtUtil.CheckAddPeoplePermission (Conversation, false)) {
				NavigationController.PushViewController (new Send.AddPeopleViewController (Conversation), true);
			}

			return new ExecutionResult<object> (new object ());
		}

		public async Task<ExecutionResult<object>> OpenTagPage (IExecutor e)
		{
			if (!_conversation.OnParse || ParseUser.CurrentUser == null) {
				return new ExecutionResult<object> (new object ());
			}

			var wait = new TaskCompletionSource<bool> ();

			var filter = new BlrtData.Views.Mailbox.ApplyTagsPage ();
			filter.SetRelation (ConversationId, BlrtBaseHelpers.GetAllKnownTags ());
			var tagController = filter.CreateViewController ();
			tagController.Title = filter.Title;
			filter.Appearing += (object senders, EventArgs es) => {
				wait.TrySetResult (true);
			};

			this.NavigationController.PushViewController (
				new Util.KeyboardController (tagController) {
					Title = filter.Title
				}, true
			);

			await wait.Task;

			return new ExecutionResult<object> (new object ());
		}

		public async Task<ExecutionResult<object>> OpenConversationOption (IExecutor e)
		{
			if (ParseUser.CurrentUser == null) {
				return new ExecutionResult<object> (new object ());
				Console.WriteLine ("NameChanged");
			}

			if (!_conversation.OnParse) {
				ShowConversationUnSyncedAlert (_moreBtn);
				return new ExecutionResult<object> (new object ());
			}

			var multiExecLock = _multiExeToken.Lock (); // prevent multi execution

			//commented the following three lines of code as they were making the conversationOptions button unresponsive after changing the name. 
			//The reason could be improper implementation of locks. Need more details.

			if (multiExecLock == null) {
				return new ExecutionResult<object> (new object ());
			}

			_conversation.ReadAllComments ();

			try {
				var result = await OpenConvoOption (this, ConversationId, _conversation.LocalStorage, _conversation.UserFilter);

				if ((null != _conversation) && (null != result) && (ConversationId == result.ConversationId)) {
					var filteredUsers = result.FilterUsers;
					if ((_conversation.UserFilter?.Length ?? 0) != result.FilterUsers.Length ||
					    result.FilterUsers.Length != 0 && (!_conversation.UserFilter?.SequenceEqual (result.FilterUsers) ?? true)) {
						_conversation.UserFilter = result.FilterUsers; 
						ReloadList ();
					}
				}
			} catch (Exception ex) {
				LLogger.WriteLine ("OpenConvoOption exception: " + ex.ToString ());
			}
			finally {
				multiExecLock.Release (); // release lock to allow for future executions
			}

			return new ExecutionResult<object> (new object ());
		}

		string _url_convo_sms, _url_convo_facebook, _url_convo_email, _url_convo_other, _url_convo_addphone = string.Empty;
		/// <summary>
		/// Opens the share page.
		/// </summary>
		/// <returns>The share page.</returns>
		public async Task OpenSharePage ()
		{
			var convo = Conversation;
			if (convo == null || !convo.OnParse) {
				return;
			}

			_url_convo_other = await BlrtUtil.GenerateLink (_conversation.Id, "convo_other", _url_convo_other);

			var sharePage = new ShareUrlPage (_url_convo_other);
			var controller = sharePage.CreateViewController ();
			controller.Title = sharePage.Title;

			sharePage.FbShareClicked += SafeEventHandler.PreventMulti (OnFacebookSend);
			sharePage.MessageShareClicked += SafeEventHandler.PreventMulti (OnMessageSend);
			sharePage.EmailShareClicked += SafeEventHandler.PreventMulti (OnEmailSend);
			sharePage.ShareWithClicked += SafeEventHandler.PreventMulti (OnShareWithSend);

			NavigationController.PushViewController (controller, true);
		}

		/// <summary>
		/// On the facebook send pressed to send convo link.
		/// </summary>
		/// <returns>The facebook send.</returns>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async Task OnFacebookSend (object sender, EventArgs e)
		{
			_url_convo_facebook = await BlrtUtil.GenerateLink (_conversation.Id, "convo_facebook", _url_convo_facebook);

			var nsurl = new NSUrl (_url_convo_facebook);
			FBManager.TrySendFBMessenger (nsurl, this);
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Facebook");	
		}

		/// <summary>
		/// On the message send pressed to send convo link
		/// </summary>
		/// <returns>The message send.</returns>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async Task OnMessageSend (object sender, EventArgs e)
		{
			await SendSMSAsync ();
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("SMS");	
		}

		/// <summary>
		/// Sends the convo link via SMS async.
		/// </summary>
		/// <returns>The SMSA sync.</returns>
		/// <param name="recipients">Recipients.</param>
		/// <param name="url">URL.</param>
		async Task SendSMSAsync (string[] recipients = null, string url = null)
		{
			if (MFMessageComposeViewController.CanSendText) {
				if (string.IsNullOrWhiteSpace (url)) {
					_url_convo_sms = await BlrtUtil.GenerateLink (_conversation.Id, "convo_sms", _url_convo_sms);
					url = _url_convo_sms;
				}
				var m = new MFMessageComposeViewController {
					Body = string.Format (BlrtUtil.PlatformHelper.Translate ("message_blrt_url_sms_body", "conversation_screen"), _conversation.Name, url)
				};
				if (null != recipients && recipients.Length > 0) {
					m.Recipients = recipients;
				}
				m.Finished += SafeEventHandler.FromAction (
					(object s, MFMessageComposeResultEventArgs es) => {
						m.BeginInvokeOnMainThread (() => {
							m.DismissViewController (true, null);
						});
						if (es.Result == MessageComposeResult.Sent) {
							BlrtUtil.TrackImpression ("convo_sms").FireAndForget ();
						}
					}
				);
				m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
				(BlrtManager.App as UIViewController).TopPresentedViewController ().PresentViewController (m, true, null);
			} else {
				var platforHelper = BlrtUtil.PlatformHelper;
				await BlrtManager.App.ShowAlert (Executor.System (), 
					new SimpleAlert (
						platforHelper.Translate ("alert_enable_messages_title", 	"share"),
						platforHelper.Translate ("alert_enable_messages_message",	"share"),
						platforHelper.Translate ("alert_enable_messages_cancel", 	"share")
					)
				);
			}
		}

		/// <summary>
		/// On the email send pressed to send convo link.
		/// </summary>
		/// <returns>The email send.</returns>
		/// <param name="sendera">Sendera.</param>
		/// <param name="ea">Ea.</param>
		async Task OnEmailSend (object sendera, EventArgs ea)
		{
			_url_convo_email = await BlrtUtil.GenerateLink (_conversation.Id, "convo_email", _url_convo_email);

			try {
				if (MFMailComposeViewController.CanSendMail) {
					var m = new MFMailComposeViewController ();
					var impression = BlrtUtil.GetImpressionHtml ("convo_email");
					impression += BlrtUtil.GetImpressionHtml ("share_email");
					var shareLink = await BlrtUtil.GenerateYozioShareUrl ("share_email", true);
					var convoName = _conversation.Name;
					var messageString = string.Format (BlrtUtil.PlatformHelper.Translate ("email_convo_url_message_body_html"), _url_convo_email, convoName, impression, shareLink);

					m.SetSubject (string.Format (BlrtHelperiOS.Translate ("email_convo_url_subject"), convoName));
					m.SetMessageBody (messageString, true);
					m.Finished += SafeEventHandler.FromAction ((object sender, MFComposeResultEventArgs e) => {
						m.BeginInvokeOnMainThread (() => {
							m.DismissViewController (true, null);
						});
					});
					m.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
					(BlrtManager.App as UIViewController).TopPresentedViewController ().PresentViewController (m, true, null);
				} else {
					var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
					await BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), 
						new SimpleAlert (
							platforHelper.Translate ("cannot_send_email_error_title"),
							platforHelper.Translate ("cannot_send_email_error_message"),
							platforHelper.Translate ("cannot_send_email_error_cancel")
						)
					);
				}
			} catch {
			}
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Email");	
		}

		/// <summary>
		/// On the share pressed on share page to send convo link.
		/// </summary>
		/// <returns>The share with send.</returns>
		/// <param name="sendera">Sendera.</param>
		/// <param name="ea">Ea.</param>
		async Task OnShareWithSend (object sendera, EventArgs ea)
		{
			using (UserDialogs.Instance.Loading ("Generating link...")) {
				_url_convo_other = await BlrtUtil.GenerateLink (_conversation.Id, "convo_other", _url_convo_other);
			}

			var activeVc = new UIActivityViewController (new NSObject[]{ NSUrl.FromString (_url_convo_other) }, null);
			if (BlrtHelperiOS.IsPhone) {
				(BlrtManager.App as UIViewController).TopPresentedViewController ().PresentViewController (activeVc, true, null);
			} else {
				var popup = new UIPopoverController (activeVc);
				popup.PresentFromRect (new CoreGraphics.CGRect (235, 225, ShareUrlPage.IconSize, ShareUrlPage.IconSize), this.PresentedViewController.View, UIPopoverArrowDirection.Up, true);
			}
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Other");
		}
		
		/// <summary>
		/// Shows the conversation-unsynced-alert. 
		/// This alert is shown to let user know conversation is not synced with cloud.
		/// </summary>
		void ShowConversationUnSyncedAlert (object sender)
		{
			var title = BlrtUtil.PlatformHelper.Translate ("convo_unsynced_title", "conversation_option");
			var message = BlrtUtil.PlatformHelper.Translate ("convo_unsynced_msg", "conversation_option");

			var alert = UIAlertController.Create (title, message, UIAlertControllerStyle.ActionSheet);

			var uploadConvoBtn = UIAlertAction.Create ("Upload conversation",
													   UIAlertActionStyle.Default, 
               async delegate {
				   var success = await UploadConversation ();

				   if (!success) {
					   title = BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option");
					   message = BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option");
					   alert.Dispose ();
					   alert = null;
					   alert = UIAlertController.Create (title, message, UIAlertControllerStyle.Alert);
					   alert.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Default, null));
					   PresentViewController (alert, true, null);
				   }
			});
			
			var deleteConvoBtn = UIAlertAction.Create ("Delete conversation", UIAlertActionStyle.Destructive, async delegate{
				await BlrtUtil.DeleteConversationFromLocal (_conversation.LocalStorage, Conversation, this, null);
			});

			alert.AddAction (uploadConvoBtn);
			alert.AddAction (deleteConvoBtn);
			alert.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Cancel, null));

			var popover = alert.PopoverPresentationController;
			if (popover != null) {
				popover.SourceRect = (sender as UIButton).Frame;
				popover.SourceView = (sender as UIButton);
				popover.PermittedArrowDirections = UIPopoverArrowDirection.Any;
			}

			PresentViewController (alert, true, null);

		}

		/// <summary>
		/// Opens media picker to create blrt.
		/// </summary>
		/// <returns>Exception or anything else to indicate the completion of task</returns>
		/// <param name="e">Executer</param>
		/// <param name="args">Request arguments</param>
		/// <param name="blrtTheseMediaAssets">This parameter is provided if new media (media not used before to create blrt) 
		/// 								   need to be added to the media picker</param>
		/// <param name="hidden">If set to <c>true</c> hidden.</param>
		/// <param name="mediaIdsToSetHiddenParamFor">Media identifiers to set hidden parameter for. 
		/// 										  If it is null, "hidden" is applied to all the media provided 
		/// 										  by "args", otherwise it is applied only to the media provided 
		/// 										  by this param</param>
		public async Task<ExecutionResult<object>> OpenReBlrtCreation (IExecutor e,
																	   IRequestArgs args,
																	   bool mediaHidden = false,
		                                                               Tuple <IEnumerable<MediaAsset>, HashSet<string>> blrtThisMediaModel = null)
		{
			_conversation.ReadAllComments ();
			await BlrtManager.ApplyPermission (_conversation.PermissionHandler);

			if (!BlrtPermissions.CanCreateReblrt) {

				new UIAlertView (
					BlrtHelperiOS.Translate ("permissions_cant_comment_title"),
					BlrtHelperiOS.Translate ("permissions_cant_comment_message"),
					null,
					BlrtHelperiOS.Translate ("permissions_cant_comment_accept")
				).Show ();

				return new ExecutionResult<object> (123);
			}

			var conversation = Conversation;



			try {
				var mediaAssets = BlrtUtil.GetMediaAssets (conversation, conversation.LastMedia);
				var success = await WaitDownloadAndDecryptContent (conversation.DownloadBlrtMedia, mediaAssets);

				if (!success) {
					return new ExecutionResult<object> (new Exception ("download failed"));
				}
			} catch (Exception ex) {
				return new ExecutionResult<object> (ex);
			}

			var lastConvoItem = conversation.LocalStorage.DataManager.GetConversationItems (conversation)
				.OrderBy (item => item)
				.LastOrDefault (item => item is BlrtData.Contents.ConversationBlrtItem)
				as BlrtData.Contents.ConversationBlrtItem;
			
			if (BlrtIssueChecker.TryIssueCheck (conversation.LocalStorage.DataManager.GetMediaAssets (lastConvoItem))) {

				ShouldHideStatusBar = true;
				var canvasData = BlrtCanvasData.FromConversation (conversation);
				canvasData.SetRequestArgs (args);

				var result = await BlrtData.BlrtManager.App.OpenBlrtCreation (
					this,
					canvasData,
					mediaHidden,
					blrtThisMediaModel
				);

				//ShouldHideStatusBar = false;

				var res = result.Result;
				if (res == null || !res.CreatedContent) {
					ShouldHideStatusBar = false;

					if (CurrentMediaItemOnDisplay != null) {
						OpenMediaOnMediaItemTap (CurrentMediaItemOnDisplay.Item1, CurrentMediaItemOnDisplay.Item2).FireAndForget ();
						CurrentMediaItemOnDisplay = null;
					}
				}

				if (res != null) {
					SetTableViewForDisplay ();
					_controlButtonsView.SetBlrtButton (true); // update blrt button text
				}
			}

            ReloadList ();
			GC.Collect ();
			return new ExecutionResult<object> (123);
		}

		public MultiExecutionPreventor.SingleExecutionToken GetSubscreenExecutionToken ()
		{
			return _multiExeToken;
		}

		public async Task RefreshAsync ()
		{
			await Refresh ();
		}

		#endregion

		CustomUI.BlrtLoadingOverlayView _loader;
		public override void PushLoading (string cause, bool animated)
		{
			_loader.Push (cause, animated);
			View.BringSubviewToFront (_loader);
		}

		public override void PopLoading (string cause, bool animated)
		{
			_loader.Pop (cause, animated);
		}

		public void CreateConversationItemsUsingExternalMedia (object args)
		{
			var alert = UIAlertController.Create (BlrtUtil.PlatformHelper.Translate ("import_to_blrt_title", "import_to_blrt"), 
			                                      BlrtUtil.PlatformHelper.Translate ("send_or_make_blrt_msg", "import_to_blrt"), 
			                                      UIAlertControllerStyle.Alert);
			alert.View.TintColor = BlrtStyles.iOS.AccentBlue;

			var blrt = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("make_blrt_btn_title", "import_to_blrt"), 
			                                 UIAlertActionStyle.Default, delegate {
                OpenReBlrtCreation (this, args as IRequestArgs, true).FireAndForget ();
			});

			var media = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("send_media_btn_title", "import_to_blrt"), 
			                                  UIAlertActionStyle.Default, delegate {
				BlrtAddMediaTypes.OpenMediaFromUrl (MediaSource.MediaSharing, 
				                                    (args as IRequestArgs).DownloadUrls, 
				                                    View, 
				                                    OnExternalMediaDownloadedForSharing).FireAndForget ();
			});

			var cancel = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("import_to_blrt_cancel_btn_title", "import_to_blrt"), 
			                                   UIAlertActionStyle.Destructive,
			                                   null);

			alert.AddAction (blrt);
			alert.AddAction (media);
			alert.AddAction (cancel);

			InvokeOnMainThread (delegate {
				PresentViewController (alert, true, null);
			});
		}

		/// <summary>
		/// Triggered when the external media has downloaded for sharing.
		/// </summary>
		/// <returns>The external media downloaded for sharing.</returns>
		/// <param name="mediaUrlManager">Media URL manager.</param>
		async Task OnExternalMediaDownloadedForSharing (BlrtMediaUrlManager mediaUrlManager)
		{
			var mediaInfo = mediaUrlManager.PopMediaUrls ();

			foreach (var media in mediaInfo) {
				SendMediaItem (new string [] { media.MediaURL }, 
				               new string [] { media.MediaName }, 
				               media.MediaFormat).FireAndForget ();
			}
		}

		public async Task BlrtThisMedia (string [] mediaIds) 
		{
			if (IsConversationReadonly) {
				BlrtUtil.ShowConversationReadonlyAlert ();
				return;
			}

			ConversationItem item = null;
			var contentId = mediaIds [0];

			try {
				item = await GetItem (contentId, LocalStorageParameters.Default);
			} catch (Exception ex1) {
				return;
			}

			if (item.Type != BlrtContentType.Media) 
			{
				return;
			}

			if (!BlrtIssueChecker.TryIssueCheck (item)) {
				return;
			}

			_conversation.ReadAllComments ();

			var result = await DownloadContent (item, this, contentId);

			if (result.Exception != null) // Cancelled download
				return;

			try {
				await DecryptMediaContent (item as ConversationMediaItem);
			} catch {
				return;
			}

			var media = item.LocalStorage.DataManager.GetMediaAssets (item.Media).ToArray ();
			var eventArgs = new BlrtThisBtnPressedEventArgs (media);

			OnBlrtThisButtonPressed (this, eventArgs);
		}
	}
}

