using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using CoreGraphics;

using Foundation;
using UIKit;
using BlrtiOS.Views.CustomUI;

namespace BlrtiOS.Views.Conversation
{
	public class BlrtHangingTitleView : UIView
	{
		public event EventHandler FilterPressed {
			add { _filterIcon.TouchUpInside += value; }
			remove { _filterIcon.TouchUpInside -= value; }
		}


		const float RefreshSize = 48;

		UIEdgeInsets _labelInset;

		UITableView _parent;

		private UILabel _titleLbl;

		CGSize _size;
		nfloat _y;

		public CGSize ContentSize { get { return _size; } }

		public nfloat TopMargin { get; private set; }

		public RefreshButton RefreshButton { get; private set; }

		public int FilterCount {
			get{ return _filterIcon.Badge; }
			set { 
				if (_filterIcon.Badge != value) {

					_filterIcon.SetState (value);
					_filterIcon.Hidden = _filterIcon.Badge <= 0;

					ResizeTitle ();
				}
			}
		}

		FilterButton _filterIcon;

		public BlrtHangingTitleView (UITableView parent)
		{
			_parent = parent;

			BackgroundColor = BlrtStyles.iOS.WhiteAlpha;
			AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleWidth;
			Frame = new CGRect (0, 0, 120, 80);

			_titleLbl = new UILabel () {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleDimensions,
				TextColor = BlrtStyles.iOS.Charcoal,
				Lines = 3,
				Font = BlrtStyles.iOS.CellTitleBoldFont
			};

			RefreshButton = new RefreshButton () {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleLeftMargin,
				Frame = new CGRect (Bounds.Width - RefreshSize, (Bounds.Height - RefreshSize) * 0.5f, RefreshSize, RefreshSize)
			};
			_filterIcon = new FilterButton () {
				AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleLeftMargin,
			};

			AddSubview (_titleLbl);
			AddSubview (_filterIcon);
			AddSubview (RefreshButton);

			try {
				var point = 1 / UIScreen.MainScreen.Scale;

				AddSubview (new UIView () {
					BackgroundColor = BlrtStyles.iOS.Gray6,
					Frame = new CGRect (0, Bounds.Height - point, Bounds.Width, point),
					AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth
				});
			} catch (Exception e) {
				;
			}

			parent.AddSubview (this);

			RepositionTitle ();
		}

		public void SetTitle (string title)
		{
			_titleLbl.Text = title;
			ResizeTitle ();
		}

		public void ResizeTitle ()
		{
			_labelInset = new UIEdgeInsets (
				8, 12, 8,
				24 + RefreshSize + (_filterIcon.Hidden ? 0 : _filterIcon.Bounds.Width)
			);


			var s = _titleLbl.SizeThatFits (new CGSize (Bounds.Width - (_labelInset.Right + _labelInset.Left), nfloat.MaxValue));
			var size = new CGSize (Bounds.Width, NMath.Max (RefreshSize, s.Height + (_labelInset.Top + _labelInset.Bottom)));

			if (_size != size) {
				_size = size;
				RepositionTitle ();
			}
		}

		private void RepositionTitle (object sender, EventArgs e)
		{
			RepositionTitle ();
		}

		private nfloat lastKnownY = 0;

		private void RepositionTitle ()
		{
			if (RefreshButton.Refreshing || _filterIcon.Badge > 0) {
				ForceShow (true);
			} else {

				nfloat y = _y;

				if (_parent.Dragging) {
					y += (_parent.ContentOffset.Y - lastKnownY) * (_parent.ContentOffset.Y > lastKnownY ? 0f : 0.2f);
				}

				y = NMath.Max (y, 0 - _size.Height);
				y = NMath.Max (y, _parent.ContentOffset.Y - _size.Height - 80);

				y = NMath.Min (y, _parent.ContentOffset.Y + _parent.ContentInset.Top - _size.Height);
				SetY (y);
			}
		}

		public void ForceShow (bool animated)
		{
			SetY (_parent.ContentOffset.Y + _parent.ContentInset.Top - _size.Height);
		}

		void SetY (nfloat y)
		{
			_y = y;
			var h = _size.Height + y - NMath.Min (y, _parent.ContentOffset.Y);
			this.Frame = new CGRect (_parent.Frame.X, NMath.Min (y, _parent.ContentOffset.Y), _parent.Bounds.Width, h);


			_titleLbl.Frame = new CGRect (
				_labelInset.Left, 
				Frame.Height - _size.Height + _labelInset.Top, 
				_size.Width - (_labelInset.Right + _labelInset.Left), 
				_size.Height - (_labelInset.Top + _labelInset.Bottom)
			);
			RefreshButton.Center = new CGPoint (
				Frame.Width - 8 - RefreshSize * 0.5f, 
				Frame.Height - _size.Height * 0.5f
			);



			_filterIcon.Center = new CGPoint (
				RefreshButton.Frame.Left - 8 - _filterIcon.Bounds.Width * 0.5f, 
				Frame.Height - _size.Height * 0.5f
			);

			lastKnownY = _parent.ContentOffset.Y;
		}



		public void Scrolled (UIScrollView scrollView)
		{
			RepositionTitle ();
		}
	}
}

