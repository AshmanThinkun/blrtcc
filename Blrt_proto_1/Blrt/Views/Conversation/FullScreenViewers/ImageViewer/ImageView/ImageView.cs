﻿using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.Views.CustomUI;

namespace BlrtiOS.Views.FullScreenViewers
{
	public partial class ImagePreviewController
	{
		partial class UIImagePreview : UIView
		{
			readonly int SELECT_BUTTON_PADDING = 25;

			public int RemainingImageCount { get; set; }
			UIImage iImage;
			ScrollView scrollView;
			public bool IsSelected { get; set; }
			UIImage selectedStateImg;
			UIImage deSelectedStateImg;

			public UIImage SelectedStateImg {
				get { return selectedStateImg ?? (selectedStateImg = UIImage.FromBundle ("img_select_sm.png")); }
			}

			public UIImage DeSelectedStateImg {
				get { return deSelectedStateImg ?? (deSelectedStateImg = UIImage.FromBundle ("img_deselect_sm.png")); }
			}

			UIImage SelectBtnImg {
				get { return IsSelected ? SelectedStateImg : DeSelectedStateImg; }
			}

			public event EventHandler BlrtThisBtnPressed {
				add { BlrtThisBtn.TouchUpInside += value; }
				remove { BlrtThisBtn.TouchUpInside -= value; }
			}

			public UIButton SelectBtn;
			public UIButton BlrtThisBtn;

			bool IsSrcConvoItem { get; set; }
			public bool UseAnimation = true;
			public float AnimationDuration = 0.2f;

			public UIImagePreview (bool isSrcConvoItem)
			{
				IsSrcConvoItem = isSrcConvoItem;
				BackgroundColor = BlrtStyles.iOS.Gray2;

				if (IsSrcConvoItem) {

					BlrtThisBtn = NewBlrtButton.CreateBlrtThisBtn (null);
					BlrtThisBtn.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;
					scrollView = new ScrollView (ref BlrtThisBtn, true);
				} else {
					SelectBtn = new UIButton ();
					SelectBtn.SetImage (DeSelectedStateImg, UIControlState.Normal);
					SelectBtn.Frame = new CGRect (0,
												  0,
												  SelectBtn.ImageView.Image.Size.Width + SELECT_BUTTON_PADDING,
												  SelectBtn.ImageView.Image.Size.Height + SELECT_BUTTON_PADDING);

					SelectBtn.TouchUpInside += delegate {

						if (IsSelected) {
							RemainingImageCount++;
						} else {
							if (RemainingImageCount > 0) {
								RemainingImageCount--;
							} else {
								return;
							}
						}
						IsSelected = !IsSelected;

						Animate (AnimationDuration, delegate {
							SelectBtn.SetImage (DeSelectedStateImg, UIControlState.Normal);
						});
					};
					scrollView = new ScrollView (ref SelectBtn, false);
				}

				AddSubview (scrollView);

				if (SelectBtn != null)
					AddSubview (SelectBtn);

				if (BlrtThisBtn != null)
					AddSubview (BlrtThisBtn);

			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();
				if (scrollView.Frame != Bounds)
					scrollView.Frame = Bounds;

				scrollView.SetImage (iImage);
			}

			public void SetImage (UIImage image, bool isSelected)
			{
				if (iImage != null) {
					iImage.Dispose ();
					iImage = null;
				}

				iImage = image;

				if (IsSrcConvoItem)
					return;

				IsSelected = isSelected;

			}

			protected override void Dispose (bool disposing)
			{
				base.Dispose (disposing);

				if (iImage != null) {
					iImage.Dispose ();
					iImage = null;
				}
			}
		}
	}
}
