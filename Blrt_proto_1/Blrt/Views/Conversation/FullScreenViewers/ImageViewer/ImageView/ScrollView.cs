﻿using System;
using UIKit;
using CoreGraphics;
using BlrtiOS.Views.CustomUI;


namespace BlrtiOS.Views.FullScreenViewers
{
	public partial class ImagePreviewController
	{
		partial class UIImagePreview : UIView
		{
			class ScrollView : UIScrollView
			{
				/// <summary>
				/// The double tap interval. Measured in ms
				/// </summary>
				const float ANIMATION_DURATION = 0.4f;

				float AnimationDuration { get; set; }

				const float defaultZoom = 2f;
				const float minZoom = 0.1f;
				const float maxZoom = 3f;
				nfloat sizeToFitZoom = 1f;

				UIImageView imageView;

				//UITapGestureRecognizer grTap;
				UITapGestureRecognizer grDoubleTap;

				//public event Action OnSingleTap;
				UIButton SelectBtn;
				UIButton BlrtThisBtn;

				public ScrollView (ref UIButton ActionButton, bool IsSrcConvoItem)
				{
					AnimationDuration = 0.0f;

					if (IsSrcConvoItem) {
						BlrtThisBtn = ActionButton;
					} else {
						SelectBtn = ActionButton;
					}

					AutoresizingMask = UIViewAutoresizing.All;

					imageView = new UIImageView ();
					imageView.ContentMode = UIViewContentMode.ScaleAspectFit;

					AddSubview (imageView);

					// Setup zoom
					MaximumZoomScale = maxZoom;
					MinimumZoomScale = minZoom;
					ShowsVerticalScrollIndicator = false;
					ShowsHorizontalScrollIndicator = false;
					BouncesZoom = true;
					ViewForZoomingInScrollView += (UIScrollView sv) => {
						return imageView;
					};

					// Setup gestures
					//grTap = new UITapGestureRecognizer (() => {
					//	if (OnSingleTap != null) {
					//		OnSingleTap ();
					//	}
					//});
					//AddGestureRecognizer (grTap);

					grDoubleTap = new UITapGestureRecognizer (() => {
						if (ZoomScale >= defaultZoom) {
							AnimationDuration = ANIMATION_DURATION;
							SetZoomScale (sizeToFitZoom, true);
						} else {
							AnimationDuration = ANIMATION_DURATION;

							// Zoom to user specified point instead of center
							var point = grDoubleTap.LocationInView (grDoubleTap.View);
							var zoomRect = GetZoomRect (defaultZoom, point);

							ZoomToRect (zoomRect, true);
						}
					});
					grDoubleTap.NumberOfTapsRequired = 2;
					AddGestureRecognizer (grDoubleTap);

					// To use single tap and double tap gesture recognizers together. See for reference:
					// http://stackoverflow.com/questions/8876202/uitapgesturerecognizer-single-tap-and-double-tap
					//grTap.RequireGestureRecognizerToFail (grDoubleTap);
				}

				public void SetImage (UIImage image)
				{
					imageView.Image = image;

					if (image == null) {
						return;
					}

					ZoomScale = 1;
					imageView.Frame = new CGRect (new CGPoint (), image.Size);
					ContentSize = image.Size;

					nfloat wScale = Frame.Width / image.Size.Width;
					nfloat hScale = Frame.Height / image.Size.Height;

					MinimumZoomScale = NMath.Min (wScale, hScale);
					sizeToFitZoom = MinimumZoomScale;
					ZoomScale = MinimumZoomScale;

					//
					imageView.Frame = CenterScrollViewContents ();
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					var newRect = CenterScrollViewContents ();
					if (!newRect.Equals (imageView.Frame))
						imageView.Frame = newRect;

					if (SelectBtn != null) {

						var right = imageView.Frame.Right > Frame.Right ? Frame.Right : imageView.Frame.Right;
						var Bottom = Frame.Bottom - TOP_BAR_HEIGHT;
						var bottom = imageView.Frame.Bottom > Bottom ? Bottom : imageView.Frame.Bottom;

						newRect = new CGRect (right - SelectBtn.Frame.Width,
												  bottom - SelectBtn.Frame.Height,
												  SelectBtn.Frame.Width,
												  SelectBtn.Frame.Height);

						if (SelectBtn.Frame.Equals (newRect))
							return;

					} else if (BlrtThisBtn != null) {

						var imgViewMidX = imageView.Frame.GetMidX ();
						var frameMidX = Frame.GetMidX ();
						var centerX = imgViewMidX > frameMidX ? frameMidX : imgViewMidX;
						var blrtThisBtnFrame = BlrtThisBtn.SizeThatFits (Frame.Size);
						var bottom = Frame.Bottom - blrtThisBtnFrame.Height;
						//var bottom = imageView.Frame.Bottom > Bottom ? Bottom : imageView.Frame.Bottom;

						newRect = new CGRect (centerX - blrtThisBtnFrame.Width * 0.5,
												  bottom - blrtThisBtnFrame.Height,
												  blrtThisBtnFrame.Width,
												  blrtThisBtnFrame.Height);

						if (BlrtThisBtn.Frame.Equals (newRect))
							return;
					}

					if (AnimationDuration > 0) {
						Animate (AnimationDuration, delegate {
							if (SelectBtn != null)
								SelectBtn.Frame = newRect;

							if (BlrtThisBtn != null)
								BlrtThisBtn.Frame = newRect;
						});
						AnimationDuration = 0.0f;
					} else {
						if (SelectBtn != null)
							SelectBtn.Frame = newRect;

						if (BlrtThisBtn != null)
							BlrtThisBtn.Frame = newRect;
					}
				}

				public override CGRect Frame {
					get {
						return base.Frame;
					}
					set {
						if (!base.Frame.Equals (value))
							base.Frame = value;

						if (imageView != null) {
							if (!imageView.Equals (value))
								imageView.Frame = value;
						}
					}
				}

				public CGRect CenterScrollViewContents ()
				{
					var boundsSize = Frame.Size;
					var contentsFrame = imageView.Frame;

					if (contentsFrame.Width < boundsSize.Width) {
						contentsFrame.X = (boundsSize.Width - contentsFrame.Width) / 2;
					} else {
						contentsFrame.X = 0;
					}

					if (contentsFrame.Height < boundsSize.Height) {
						contentsFrame.Y = (boundsSize.Height - contentsFrame.Height) / 2;
					} else {
						contentsFrame.Y = 0;
					}

					return contentsFrame;
				}

				// Reference:
				// http://stackoverflow.com/a/11003277/548395
				CGRect GetZoomRect (float scale, CGPoint center)
				{
					var size = new CGSize (imageView.Frame.Size.Height / scale, imageView.Frame.Size.Width / scale);

					var center2 = ConvertPointToView (center, imageView);
					var location2 = new CGPoint (center2.X - (size.Width / 2.0f),
						center2.Y - (size.Height / 2.0f)
					);

					var zoomRect = new CGRect (location2, size);
					return zoomRect;
				}
			}
		}
	}
}