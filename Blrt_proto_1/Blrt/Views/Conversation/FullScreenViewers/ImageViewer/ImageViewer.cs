﻿using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using BlrtData;
using ImagePickers.ImageAssets;
using System.Threading.Tasks;
using System.Threading;
using Photos;

namespace BlrtiOS.Views.FullScreenViewers
{
	///<summary>
	///<para> This view controller presents the photos in the preview controller </para>

	/// </summary>


	public partial class ImagePreviewController : BaseMediaViewerController
	{
		public static readonly int TOP_BAR_HEIGHT = 45;

		public EventHandler PreviewClosed;

		UIActivityIndicatorView ImageLoadingIndicator { get; set; }
		UIImagePreview ImagePreview { get; set; }
		public List<BaseImageAsset> AssetsFromPhotosController { get; set; }
		List<MediaAsset> AssetsFromConvoItem { get; set; }
		List<PHAsset> PHAssetFromAlbumPhotoPicker { get; set; }
		public UIBarButtonItem DoneButtonItem { get; set; }
		TopNavBarView AssetNavBar { get; set; }
		BlrtMediaFormat MediaFormat { get; set; }
		public override int CurrentAssetNum { get { return AssetNavBar.CurrentAssetNum; } set { AssetNavBar.CurrentAssetNum = value; } }
		int TotalAssetCount { get { return AssetNavBar.TotalAssetCount; } set { AssetNavBar.TotalAssetCount = value; } }
		Task ImageLoadingTask { get; set; }
		CancellationTokenSource ImageLoadingTaskCancellationTokenSource { get; set; }
		int CurrentLoadingAssetNum { get; set; }
		CancellationToken LoadingCancellationToken { get; set; }
		bool IsLoading { get; set; }

		public int RemainingImageCount {
			get { return ImagePreview.RemainingImageCount; }
			set { ImagePreview.RemainingImageCount = value; }
		}

		bool IsSrcConvoItem { get; set; }
		int PreviousAssetNum { get; set; }
		public event EventHandler<int> isSelectedEventHandler;
		public event EventHandler<int> isDeSelectedEventHandler;
			

		public ImagePreviewController (List<MediaAsset> items, int currentAssetNum, bool blrtThisButtonEnabled) : this (currentAssetNum, items.Count, true, blrtThisButtonEnabled)
		{
			AssetsFromConvoItem = items;
			ImagePreview.BlrtThisBtnPressed += (sender, e) => {
				var item = items [CurrentAssetNum-1];
				var eventArgs = new BlrtThisBtnPressedEventArgs (new MediaAsset [] { item });
				OnBlrtThisButtonPressed (eventArgs);
			};

			AssetNavBar = new TopNavBarView (currentAssetNum, items.Count);

			AssetNavBar.UpdateImage += delegate { SetImage (CurrentAssetNum); };
			AssetNavBar.UpdateCurrentImageSelected += (object o, int i) => {

				CurrentAssetNum = i;

			};
			View.AddSubview (AssetNavBar);

		}


		ImagePreviewController (int currentAssetNum, int assetCount, bool isSrcConvoItem, bool blrtThisButtonEnabled = true)
		{
			EdgesForExtendedLayout = UIRectEdge.None;
			IsSrcConvoItem = isSrcConvoItem;
			ImagePreview = new UIImagePreview (IsSrcConvoItem);
		
			PreviousAssetNum = -1;
			ImageLoadingIndicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);

			View.AddSubviews (new UIView [] {
				ImagePreview,
				ImageLoadingIndicator
			});
		}

		bool imageSelected = false;
		bool checkMarkStatus = false;

		public ImagePreviewController (List<BaseImageAsset> assets, List<BaseImageAsset> selectedAssetsinPhotosController,int currentAssetNum, bool blrtThisButtonEnabled, bool selectedStatus) : this (currentAssetNum, assets.Count, false, blrtThisButtonEnabled)
		{
			
 			AssetsFromPhotosController = assets;
			selectedAssets = selectedAssetsinPhotosController;
			imageSelected = selectedStatus;
			InitializeSelectButtonImage ();
			ImagePreview.SelectBtn.LayoutIfNeeded ();

			AssetNavBar = new TopNavBarView (currentAssetNum, assets.Count-1);

			AssetNavBar.UpdateImage += delegate { SetImage (CurrentAssetNum); };
			AssetNavBar.UpdateCurrentImageSelected += (object o, int i) => {
				CurrentAssetNum = i;
				ToggleCheckmarkDuringNavigation ();
			};

			View.AddSubview (AssetNavBar);
			ImagePreview.SelectBtn.TouchUpInside += delegate {
				IsImageSelected ();
			
			};
		}

		/// <summary>
		/// sets the select button image based upon the selected state recieved from parent controller.
		/// </summary>

		public void InitializeSelectButtonImage()
		{
			if (imageSelected == true ) {
				
				ImagePreview.SelectBtn.SetImage (ImagePreview.SelectedStateImg, UIControlState.Normal);
				checkMarkStatus = true;

			} else if (imageSelected == false) {


				ImagePreview.SelectBtn.SetImage (ImagePreview.DeSelectedStateImg, UIControlState.Normal);
				checkMarkStatus = false;


			}
			
		}
		/// <summary>
		/// Sets the state of selected button checkmark while browsing through various photos in the preview. It helps in avoiding
		/// automatic selection or deselection of photos
		/// </summary>

		public void ToggleCheckmarkDuringNavigation()
		{
			
			if (selectedAssets.Contains (AssetsFromPhotosController [CurrentAssetNum]) == true) {

				ImagePreview.SelectBtn.SetImage (ImagePreview.SelectedStateImg, UIControlState.Normal);
				isSelectedEventHandler?.Invoke (AssetsFromPhotosController [CurrentAssetNum], CurrentAssetNum);
				checkMarkStatus = true;
				imageSelected = true;

			}
			else if(selectedAssets.Contains (AssetsFromPhotosController [CurrentAssetNum]) == false)
			{
				ImagePreview.SelectBtn.SetImage (ImagePreview.DeSelectedStateImg, UIControlState.Normal);
				isDeSelectedEventHandler?.Invoke (AssetsFromPhotosController [CurrentAssetNum], CurrentAssetNum);
				checkMarkStatus = false;
				imageSelected = false;
					
			} 
		}

		/// <summary>
		/// This asset <c>selectedAssets</c> keeps track of previously selected photos(in parent controller) and current selection and 
		/// deselection of photos
		/// </summary>

		List<BaseImageAsset> selectedAssets = new List<BaseImageAsset> ();

		public void IsImageSelected()
		{
			if (checkMarkStatus == false ) {

				ImagePreview.SelectBtn.SetImage (ImagePreview.SelectedStateImg, UIControlState.Normal);
				isSelectedEventHandler?.Invoke (AssetsFromPhotosController [CurrentAssetNum], CurrentAssetNum);//updates the parent list with regards to the selection of photos
				checkMarkStatus = true;
				imageSelected = true;
				selectedAssets.Add (AssetsFromPhotosController [CurrentAssetNum]);
			} 
			else if(checkMarkStatus == true)
			{
				ImagePreview.SelectBtn.SetImage (ImagePreview.DeSelectedStateImg, UIControlState.Normal);
				isDeSelectedEventHandler?.Invoke (AssetsFromPhotosController [CurrentAssetNum], CurrentAssetNum);//updates the parent list with regards to deselection
				checkMarkStatus = false;
				imageSelected = false;
				selectedAssets.Remove(AssetsFromPhotosController [CurrentAssetNum]);
			}
		}



		public void SetImage (int currentAssetNum)
		{
			if (PreviousAssetNum == currentAssetNum)
				return;

			CurrentAssetNum = PreviousAssetNum = currentAssetNum;
			AssetNavBar.CurrentAssetNumText = CurrentAssetNum.ToString ();

			if (AssetsFromPhotosController != null) {
				SetImageFromAlbumPhotosController ();
			} else {
				SetImageFromConvoItem ();
			}

			AssetNavBar.SetNeedsLayout ();
		}

		void SetImageFromConvoItem ()
		{
			//AssetNavBar = new TopNavBarView (CurrentAssetNum+1, AssetsFromConvoItem.Count+1);
			var item = AssetsFromConvoItem [CurrentAssetNum-1];
			NavigationItem.Title = item.Name;

			var img = UIImage.FromFile (item.DecyptedPath);
			if (img == null) {
				NavigationController.PopViewController (true);
				return;
			}

			ImagePreview.SetImage (img, false);
			img = null;

			ImagePreview.SetNeedsLayout ();
			AssetNavBar.SetNeedsLayout ();
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		UIImage GetImageFromELCAsset (PhotoGalleryAsset eLCAsset)
		{
			var rep = eLCAsset.Thumbnail;
			CGImage cgImage = null;
			cgImage = rep.CGImage;
			var img = new UIImage (cgImage);
			cgImage = null;
			return img;
		}


		/// <summary>
		/// sets the image in the preview
		/// </summary>
		void SetImageFromAlbumPhotosController ()
		{
			
				var item = AssetsFromPhotosController [CurrentAssetNum];
				NavigationItem.Title = item.Title;
				ImagePreview.SetImage (null, item.Selected);
				ImagePreview.SetNeedsLayout ();
				ImageLoadingIndicator.StartAnimating ();
				Task.Run (delegate {
					TryStartLoadingImage (item);
				});

				

	
		}

		void TryStartLoadingImage (BaseImageAsset item)
		{
			if (ImageLoadingTask != null) {
				bool InvalidCurrentLoadingAssetNum = false; // used to decide whether image about to be loaded is still valid
				if (!ImageLoadingTask.IsCompleted) {
					ImageLoadingTaskCancellationTokenSource.Cancel ();
					if (downloader != null) {
						// if downloder is running, try stopping it
						if (!downloader.IsFinished && !downloader.Cancelled) {
							try {
								downloader.Cancel ();
							} catch (Exception e) {
								// ignore
							}
						}
					}

					if (IsLoading) {
						// if task started previously hasn't been terminated yet, return
						// Upon task completion it will we decided whether to load new image or not based on "InvalidCurrentLoadingAssetNum"
						return;
					}

					IsLoading = true;
					ImageLoadingTask.Wait ();
					IsLoading = false;
					InvalidCurrentLoadingAssetNum = (CurrentLoadingAssetNum != CurrentAssetNum);
					ImageLoadingTaskCancellationTokenSource.Dispose ();
				}
				ImageLoadingTask.Dispose ();
				ImageLoadingTask = null;

				if (InvalidCurrentLoadingAssetNum) {
					// restart loading process to get correct image
					InvokeOnMainThread (delegate {
						SetImageFromAlbumPhotosController ();
					});
					return;
				}


			}

			CurrentLoadingAssetNum = CurrentAssetNum;
			ImageLoadingTaskCancellationTokenSource = new CancellationTokenSource ();
			LoadingCancellationToken = ImageLoadingTaskCancellationTokenSource.Token;
			ImageLoadingTask = Task.Run (async delegate {
				try {
					await GetImageLoadingTask (item);
				} catch (TaskCanceledException e) {
					// ignore. Would be "TaskCancelledException".
				} catch (Exception ex) {
					// something unexpected happened
					LLogger.WriteLine (string.Format ("Time = {0}, Origin = {1}, Message = {2}", DateTime.Now, Class.Name, ex.Message));
					CurrentLoadingAssetNum = -1; // to force reload
				}
			});
		}

		async Task GetImageLoadingTask (BaseImageAsset item)
		{
			UIImage img = null;

			if (item is PhotoGalleryAsset) {
				var elcAsset = item as PhotoGalleryAsset;
				img = GetImageFromELCAsset (elcAsset);
			} else if (item is PhotoAssetForMediaAsset) {
				var task = GetImageFromMediaAsset (((PhotoAssetForMediaAsset)item).Asset);
				img = await task;
			}
			if (!ImageLoadingTask.IsCanceled && CurrentLoadingAssetNum == CurrentAssetNum) {
				InvokeOnMainThread (delegate {
					ImagePreview.SetImage (img, item.Selected);
					ImagePreview.SetNeedsLayout ();
					ImageLoadingIndicator.StopAnimating ();
				});
			}
		}

		BlrtData.IO.BlrtDownloader downloader;
		async Task<UIImage> GetImageFromMediaAsset (MediaAsset asset)
		{
			var tcs = new TaskCompletionSource<UIImage> ();
			if (!asset.IsDataLocal ()) {
				downloader = BlrtData.IO.BlrtDownloader.Download (1000, asset.CloudMediaPath, asset.LocalMediaPath);

				downloader.OnDownloadFinished += async delegate {
					try {
						await DecryptMedia (tcs, asset);
					} catch (Exception e) {
						tcs.SetException (e);
					}
				};

			} else {
				try {
					await DecryptMedia (tcs, asset);
				} catch (Exception e) {
					tcs.SetException (e);
				}
			}
			return await tcs.Task;
		}

		async Task DecryptMedia (TaskCompletionSource<UIImage> tcs, MediaAsset asset)
		{
			if (LoadingCancellationToken.IsCancellationRequested) {
				throw new TaskCanceledException ("cancellation requested");
			}

			if (asset.IsDataLocal ()) {
				if (!asset.IsDecypted) {
					await asset.DecyptData ();
				}

				if (LoadingCancellationToken.IsCancellationRequested) {
					throw new TaskCanceledException ("cancellation requested");
				}

				var decryptedPath = asset.DecyptedPath;
				if (BlrtUtil.CanUseFile (decryptedPath, false)) {
					tcs.SetResult (UIImage.FromFile (decryptedPath));
				}
			}
		}
		public event EventHandler DoneButtonClicked;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (IsSrcConvoItem)
				return;

			DoneButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done);
			DoneButtonItem.Clicked += delegate {

				DoneButtonClicked?.Invoke (this, null);

				ImagePreview?.Dispose ();
				NavigationController.PopViewController (true);

			};
			NavigationItem.RightBarButtonItem = DoneButtonItem;

		}

		public EventHandler PreviewWillRotate;

		public override void DidRotate (UIInterfaceOrientation toInterfaceOrientation)
		{
			base.DidRotate (toInterfaceOrientation);

			if (PreviewWillRotate != null) {
				PreviewWillRotate (null, null);
			}
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			AssetNavBar.Frame = new CGRect (View.Bounds.Left,
											View.Bounds.Top,
											View.Bounds.Width,
											TOP_BAR_HEIGHT);

			ImagePreview.Frame = new CGRect (AssetNavBar.Frame.Left,
											 AssetNavBar.Frame.Bottom,
											 View.Bounds.Width,
											 View.Bounds.Height - TOP_BAR_HEIGHT);

			SetImage (CurrentAssetNum);
			ImageLoadingIndicator.Center = View.Center;


			//IsImageSelected ();

		}


		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();

			//IsImageSelected ();

			//ImagePreview.SelectBtn.SendActionForControlEvents (UIControlEvent.TouchUpInside);
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (IsMovingFromParentViewController) {
				if (PreviewClosed != null) {
					PreviewClosed (null, null);
				}
			}
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			if (IsMovingFromParentViewController) {
				ImagePreview?.Dispose ();
			}
		}
	}
}