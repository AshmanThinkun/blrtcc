﻿using System;
using UIKit;
using CoreGraphics;

namespace BlrtiOS.Views.FullScreenViewers
{
	public partial class ImagePreviewController
	{
		class TopNavBarView : UIView
		{
			static readonly int PAGE_INFO_PADDING = 10;
			readonly int BTN_PADDING = 10;
			readonly int SPACING = 25;


			UIButton PrevBtn { get; set; }
			UIButton NextBtn { get; set; }
			InfoView Info { get; set; }
			public string CurrentAssetNumText { get { return Info.CurrentAssetNum.Text; } set { Info.CurrentAssetNum.Text = value; } }
			string TotalAssetsFromImagePickerText { get { return Info.TotalAssetsFromImagePicker.Text; } set { Info.TotalAssetsFromImagePicker.Text = value; } }

			public int CurrentAssetNum { get; set; }
			public int TotalAssetCount { get; set; }

			UIImage NextBtnImage { get { return UIImage.FromBundle ("preview_right_arrow.png"); } }
			UIImage PrevBtnImage { get { return UIImage.FromBundle ("preview_left_arrow.png"); } }

			public event EventHandler UpdateImage;
			public event EventHandler<int> UpdateCurrentImageSelected;

			public TopNavBarView (int currentAssetNum, int assetCount)
			{
				BackgroundColor = BlrtStyles.iOS.Charcoal;
				Layer.BorderWidth = 0;



				PrevBtn = new UIButton () {
					UserInteractionEnabled = true
				};

				NextBtn = new UIButton () {
					UserInteractionEnabled = true
				};

				NextBtn.SetImage (NextBtnImage, UIControlState.Normal);
				PrevBtn.SetImage (PrevBtnImage, UIControlState.Normal);

				NextBtn.Frame = new CGRect (0, 0,
											NextBtn.ImageView.Image.Size.Width + BTN_PADDING * 2,
											0);

				PrevBtn.Frame = new CGRect (0, 0,
											PrevBtn.ImageView.Image.Size.Width + BTN_PADDING * 2,
											0);

				NextBtn.TouchUpInside += ShowNextImage;
				PrevBtn.TouchUpInside += ShowPrevImage;

				Info = new InfoView ();

				CurrentAssetNum = currentAssetNum;
				TotalAssetCount = assetCount;

				TotalAssetsFromImagePickerText = TotalAssetCount.ToString ();
				CurrentAssetNumText = CurrentAssetNum.ToString ();

				AddSubviews (new UIView [] {
							Info,
							PrevBtn,
							NextBtn
						});
			}

			void ShowNextImage (object sender, EventArgs e)
			{
				var old = CurrentAssetNum;
				var current = Math.Min (CurrentAssetNum + 1, TotalAssetCount);



				if (old == current) return;

				CurrentAssetNum = current;
				CurrentAssetNumText = current.ToString ();
				Info.SetNeedsLayout ();

				if (UpdateImage != null) {
					UpdateImage (null, null);
				}

				if (UpdateCurrentImageSelected != null) {
					UpdateCurrentImageSelected (null, CurrentAssetNum);
				}
			}

			void ShowPrevImage (object sender, EventArgs e)
			{
				var old = CurrentAssetNum;
				var current = Math.Max (CurrentAssetNum - 1, 0);



				if (old == current) return;

				CurrentAssetNum = current;
				CurrentAssetNumText = current.ToString ();
				Info.SetNeedsLayout ();

				if (UpdateImage != null) {
					UpdateImage (null, null);
				}

				if(UpdateCurrentImageSelected != null)
				{
					UpdateCurrentImageSelected (null, CurrentAssetNum);
				}
			}

			public override void SetNeedsLayout ()
			{
				base.SetNeedsLayout ();
				Info.SetNeedsLayout ();
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var size = Info.SizeThatFits (new CGSize (Bounds.Width, Bounds.Height - PAGE_INFO_PADDING));
				var left = (Bounds.Width - size.Width) * 0.5;

				var newRect = new CGRect (left - PrevBtn.Frame.Width - SPACING,
											Bounds.Top + PAGE_INFO_PADDING * 0.5,
											PrevBtn.Frame.Width,
											Bounds.Height - PAGE_INFO_PADDING);

				if (!newRect.Equals (PrevBtn.Frame))
					PrevBtn.Frame = newRect;

				newRect = new CGRect (left + size.Width + SPACING,
											Bounds.Top + PAGE_INFO_PADDING * 0.5,
											NextBtn.Frame.Width,
											Bounds.Height - PAGE_INFO_PADDING);

				if (!newRect.Equals (NextBtn.Frame))
					NextBtn.Frame = newRect;

				newRect = new CGRect (new CGPoint (left, Bounds.Top + PAGE_INFO_PADDING * 0.5), size);

				if (!newRect.Equals (Info.Frame))
					Info.Frame = newRect;
			}

			class InfoView : UIView
			{
				readonly int INSET = PAGE_INFO_PADDING * 2;

				public UILabel CurrentAssetNum { get; set; }
				public UILabel TotalAssetsFromImagePicker { get; set; }
				UILabel Separator { get; set; }

				UIColor LabelTextColor { get { return BlrtStyles.iOS.Gray7; } }
				UIFont LabelFont { get { return BlrtStyles.iOS.CellTitleBoldFont; } }
				string SeparatorSymbol { get { return " / "; } }

				public InfoView ()
				{
					BackgroundColor = BlrtStyles.iOS.White;
					Layer.CornerRadius = 4;

					CurrentAssetNum = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont
					};

					TotalAssetsFromImagePicker = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont
					};

					Separator = new UILabel () {
						Lines = 1,
						UserInteractionEnabled = false,
						TextColor = LabelTextColor,
						Font = LabelFont,
						Text = SeparatorSymbol
					};

					AddSubviews (new UIView [] {
								CurrentAssetNum,
								TotalAssetsFromImagePicker,
								Separator
							});
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					var currentAssetSize = CurrentAssetNum.SizeThatFits (Bounds.Size);
					var totalAssetSize = TotalAssetsFromImagePicker.SizeThatFits (Bounds.Size);
					var separatorSize = Separator.SizeThatFits (Bounds.Size);
					var width = totalAssetSize.Width * 2 + separatorSize.Width;

					var newRect = new CGRect (Bounds.X + INSET * 0.5 + width - totalAssetSize.Width,
												  Bounds.Top + PAGE_INFO_PADDING * 0.5,
												  totalAssetSize.Width,
												  Bounds.Height - PAGE_INFO_PADDING);

					if (!newRect.Equals (TotalAssetsFromImagePicker.Frame))
						TotalAssetsFromImagePicker.Frame = newRect;

					newRect = new CGRect (TotalAssetsFromImagePicker.Frame.Left - separatorSize.Width,
												  Bounds.Top + PAGE_INFO_PADDING * 0.5,
												  separatorSize.Width,
												  CurrentAssetNum.Frame.Height);

					if (!newRect.Equals (Separator.Frame))
						Separator.Frame = newRect;

					newRect = new CGRect (Separator.Frame.Left - currentAssetSize.Width,
											  Bounds.Top + PAGE_INFO_PADDING * 0.5,
											  currentAssetSize.Width,
											  Bounds.Height - PAGE_INFO_PADDING);

					if (!newRect.Equals (CurrentAssetNum.Frame))
						CurrentAssetNum.Frame = newRect;
				}

				public override CGSize SizeThatFits (CGSize size)
				{
					base.SizeThatFits (size);

					var totalAssetSize = TotalAssetsFromImagePicker.SizeThatFits (Bounds.Size);
					var separatorSize = Separator.SizeThatFits (Bounds.Size);
					var width = totalAssetSize.Width * 2 + separatorSize.Width + INSET;

					return new CGSize (width, size.Height);
				}
			}
		}
	}
}