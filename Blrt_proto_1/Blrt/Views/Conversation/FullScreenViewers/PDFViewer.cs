﻿using System;
using UIKit;
using CoreGraphics;
using BlrtData;
using Foundation;
using BlrtiOS.Views.CustomUI;
using System.Threading.Tasks;
using Acr.UserDialogs;
using System.IO;

namespace BlrtiOS.Views.FullScreenViewers
{
	public class PDFPreviewController : BaseMediaViewerController
	{
		const int PADDING = 20;
		UIWebView PDFView { get; set; }
		MediaAsset PDFAsset { get; set; }
		UIButton BlrtThisBtn { get; set; }

		bool ShouldDelete { get; set; }

		//public event EventHandler<BlrtThisBtnPressedEventArgs> BlrtThisBtnPressed;

		public bool ShowBlrtThisBtn { get; set; }
		public override int CurrentAssetNum { get { return 1; } set { } } // not in use for now as we don't support multiple pdfs at the moment.

		bool BlrtThisButtonEnabled { get; set; }

		public PDFPreviewController (MediaAsset pdfAsset, bool blrtThisButtonEnabled, bool showBlrtThisBtn = true)
		{
			BlrtThisButtonEnabled = blrtThisButtonEnabled;
			ShowBlrtThisBtn = showBlrtThisBtn;
			PDFAsset = pdfAsset;

			PDFView = new UIWebView () { ScalesPageToFit = true };
			PDFView.ScrollView.ShowsVerticalScrollIndicator = PDFView.ScrollView.ShowsHorizontalScrollIndicator = false;
			View.AddSubview (PDFView);

			if (ShowBlrtThisBtn) {
				BlrtThisBtn = NewBlrtButton.CreateBlrtThisBtn (null);
				BlrtThisBtn.BackgroundColor = blrtThisButtonEnabled ? BlrtStyles.iOS.GreenHighlight : BlrtStyles.iOS.Gray4;
				BlrtThisBtn.Layer.ShadowColor = blrtThisButtonEnabled ? BlrtStyles.iOS.GreenShadow.CGColor : BlrtStyles.iOS.Gray6.CGColor;
				BlrtThisBtn.Enabled = false;
				BlrtThisBtn.Hidden = true;
				BlrtThisBtn.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;
				View.AddSubview (BlrtThisBtn);
			}

			Task.Run (delegate {
				LoadPDF ().FireAndForget ();
			});
		}

		string PdfDesPath;

		async Task LoadPDF ()
		{
			using (UserDialogs.Instance.Loading ("Downloading...")) {
				if (await DownloadAndDecrypt ()) {
					SetPDFDesPath ();

					var url = new NSUrl (PdfDesPath);
					var request = NSUrlRequest.FromUrl (url);
					InvokeOnMainThread (delegate {
						PDFView.LoadRequest (request);

						if (ShowBlrtThisBtn) {
							BlrtThisBtn.TouchUpInside += (sender, e) => {

								var eventArgs = new BlrtThisBtnPressedEventArgs (new MediaAsset [] {PDFAsset});
								OnBlrtThisButtonPressed (eventArgs);
								//if (BlrtThisBtnPressed != null) {
	       //                         BlrtThisBtnPressed (null,
								//	                    new BlrtThisBtnPressedEventArgs (PDFAsset.DecyptedPath, PDFAsset.Format, PDFAsset.ObjectId));
								//}
							};
							BlrtThisBtn.Enabled = BlrtThisButtonEnabled;
							BlrtThisBtn.Hidden = false;
						}
				  });
				}
			}
		}

		void SetPDFDesPath ()
		{
			var src = PDFAsset.DecyptedPath;

			if (src.ToLower ().Contains (".pdf")) {
				PdfDesPath = src;
				ShouldDelete = false;

				/*
				 * Webview fails to load paths with white spaces. 
				 * So all the white spaces need to be removed.
				 */

				if (src.Contains (" ")) {
					PdfDesPath = src.Replace (" ", string.Empty);
					try {
						File.Copy (src, PdfDesPath);
						ShouldDelete = true;
					} catch {
						
					}
				}
				return;
			}

			// take care of any other extensions
			PdfDesPath = src.Substring (0, src.LastIndexOf ('.') + 1) + "pdf";
			try {
				File.Copy (src, PdfDesPath);
				ShouldDelete = true;
			} catch {
				
			}
		}

		async Task<bool> DownloadAndDecrypt ()
		{
			if (PDFAsset.IsDataLocal ()) {
				return await Decrypt ();
			}

			var downloader = PDFAsset.DownloadContent (1000)[0];
			await downloader.WaitAsync ();

			if (!PDFAsset.IsDataLocal ()) {
				return false;
			}

			return await Decrypt ();
		}

		async Task<bool> Decrypt ()
		{
			if (!PDFAsset.IsDecypted) {
				try {
					await PDFAsset.DecyptData ();
					return PDFAsset.IsDecypted;
				} catch (Exception e) {
					return false;
				}
			}
			return true;
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			try {
				if (ShouldDelete && File.Exists (PdfDesPath)) {
					File.Delete (PdfDesPath);
					ShouldDelete = false;
				}
			} catch {
				
			}
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();

			var newRect = View.Bounds;
			if (!PDFView.Frame.Equals (newRect))
				PDFView.Frame = newRect;

			if (ShowBlrtThisBtn) {
				var size = BlrtThisBtn.SizeThatFits (View.Bounds.Size);
				newRect = new CGRect (
					new CGPoint (View.Bounds.GetMidX () - size.Width * 0.5, View.Bounds.Bottom - size.Height - PADDING),
					size);

				if (!BlrtThisBtn.Frame.Equals (newRect))
					BlrtThisBtn.Frame = newRect;
			}
		}	
	}
}
