﻿using System;
using UIKit;

namespace BlrtiOS.Views.FullScreenViewers
{
	public abstract class BaseMediaViewerController : UIViewController {
		public event EventHandler<BlrtThisBtnPressedEventArgs> BlrtThisBtnPressed;

		internal void OnBlrtThisButtonPressed (BlrtThisBtnPressedEventArgs eventArgs)
		{
			if (BlrtThisBtnPressed != null) {
				BlrtThisBtnPressed (null, eventArgs);
			}
		}

		public abstract int CurrentAssetNum { get; set; }
	}
}