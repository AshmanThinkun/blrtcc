using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtiOS.Views.Util;
using BlrtData;
using BlrtData.ExecutionPipeline;
using BlrtData.Models.ConversationScreen;

namespace BlrtiOS.Views.Conversation
{
	public class ConversationTableView : TableModelView<ConversationCellModel>, IExecutor
	{
		class ConversationTableBackgroundView : UIView
		{
			const int PADDING = 6;

			public UILabel EmptyConvoInfoLabel { get; set; }
			UIImageView Logo { get; set;}

			public ConversationTableBackgroundView ()
			{
				EmptyConvoInfoLabel = new UILabel () {
					Lines = 1,
					Text = "Select a conversation from the left to view",
					TextAlignment = UITextAlignment.Center,
					TextColor = BlrtStyles.iOS.Gray3,
					Font = BlrtStyles.iOS.CellDetailFont
				};

				EmptyConvoInfoLabel.SizeToFit ();

				Logo = new UIImageView (UIImage.FromBundle ("blank_convo_b.png")){
					ContentMode = UIViewContentMode.ScaleAspectFit
				};

				AddSubview (Logo);
				AddSubview (EmptyConvoInfoLabel);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				Logo.Center = new CGPoint (Bounds.GetMidX(), 
				                           Logo.Frame.Height * 0.5f);
				EmptyConvoInfoLabel.Center = new CGPoint (Logo.Center.X, 
				                                          Bounds.Bottom - EmptyConvoInfoLabel.Frame.Height * 0.5f);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				return new CGSize (size.Width, 
				                   Logo.Frame.Height + EmptyConvoInfoLabel.Frame.Height + PADDING);
			}
		}

		#region IExecutor implementation

		public ExecutionSource Cause {
			get { return ExecutionSource.System; }
		}

		#endregion

		const int FooterSize = 30;

		public event EventHandler FilterPressed {
			add { TitleView.FilterPressed += value; }
			remove { TitleView.FilterPressed -= value; }
		}

		public static readonly UIColor TableBackgroundColor = BlrtStyles.iOS.White;
		public static readonly UIColor CellBackgroundColor = BlrtStyles.iOS.White;

		CellSizeManager _cellSizer;

		ConversationController _parent;

		public BlrtHangingTitleView TitleView { get; private set; }
		public UIActivityIndicatorView FooterView { get; private set; }
		public BlrtAlertsBarView AlertView { get; private set; }
		public bool IsFirstConvoViewAfterCreation { get; set; }

		nfloat _titleLastHeight;
		nfloat _tableLastHeight;

		string _backgroundViewInfo;
		/// <summary>
		/// Sets the background view text. If empty convo is visible text is different 
		/// than that shown when no convo is selected.
		/// </summary>
		/// <value>The background view info.</value>
		public string BackgroundViewInfo {
			set {
				if (!value.Equals (_backgroundViewInfo)) {
					_backgroundViewInfo = value;

					var bgv = BackgroundView as ConversationTableBackgroundView;

					if (bgv != null) {
						bgv.EmptyConvoInfoLabel.Text = _backgroundViewInfo;
					}
				}
		    }
		}

		public ConversationTableView (ConversationController parent)
			: base ()
		{
			_parent = parent;
			BackgroundView = new ConversationTableBackgroundView ();
			RegisterClassForCellReuse (typeof(Cells.CommentCell), new NSString (Cells.CommentCell.Indentifer));
			RegisterClassForCellReuse (typeof(Cells.BlrtCell), new NSString (Cells.BlrtCell.Indentifer));
			RegisterClassForCellReuse (typeof(Cells.EventCell), new NSString (Cells.EventCell.Indentifer));
			RegisterClassForCellReuse (typeof(Cells.MissingContentCell), new NSString (Cells.MissingContentCell.Indentifer));
			RegisterClassForCellReuse (typeof(Cells.SendFailureCell), new NSString (Cells.SendFailureCell.Indentifer));
			RegisterClassForCellReuse (typeof (Cells.AudioCell), new NSString (Cells.AudioCell.Indentifer));
			RegisterClassForCellReuse (typeof (Cells.DateCell), new NSString (Cells.DateCell.Indentifer));
			RegisterClassForCellReuse (typeof (Cells.ImageCell), new NSString (Cells.ImageCell.Indentifer));
			RegisterClassForCellReuse (typeof (Cells.PDFCell), new NSString (Cells.PDFCell.Indentifer));

			this.BackgroundColor = TableBackgroundColor;
			this.Bounces = true;
			this.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			this.RowHeight = 80;
			this.SectionHeaderHeight	= 0;
			this.SectionFooterHeight	= 0;

			FooterView = new UIActivityIndicatorView (new CGRect (0, 0, FooterSize, FooterSize)) {
				ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
				HidesWhenStopped = true
			};
			this.TableFooterView = FooterView;
			this._cellSizer = new CellSizeManager (_parent.AudioPlayPauseBtnPressed);
			AlertView = new BlrtAlertsBarView () {
				Frame = Bounds,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth
			};
			AlertView.SizeChanged += (object sender, EventArgs e) => {
				SetNeedsLayout ();
			};

			TitleView = new BlrtHangingTitleView (this);
			AddSubview (AlertView);

			var panGesture = new UIPanGestureRecognizer (HandleSlide);
			panGesture.ShouldRecognizeSimultaneously += (gestureRecognizer, otherGestureRecognizer) => {
				return true;
			};
			this.AddGestureRecognizer (panGesture);
		}

		public void HideBackgroundView (bool hide)
		{
			if (BackgroundView.Hidden == hide)
				return;
			
			BackgroundView.Hidden = hide;
		}

		public void HideHeaderViews (bool hide) 
		{
			if (TitleView.Hidden == hide)
				return;
			
			TitleView.Hidden = AlertView.Hidden = hide;
		}

		void HandleSlide (UIPanGestureRecognizer recognizer)
		{
			if (recognizer.State != UIGestureRecognizerState.Cancelled && recognizer.State != UIGestureRecognizerState.Ended) {
				CGPoint offset = recognizer.TranslationInView (this);
				SetSlide (offset.X, false);
			} else {
				SetSlide (0, true);
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (!BackgroundView.Hidden) {
				var size = BackgroundView.SizeThatFits (Bounds.Size);
				var height = size.Height;
				var y = Bounds.GetMidY () - height * 0.5f;

				var newFrame = new CGRect (Bounds.Left, y, size.Width, height);

				if (!newFrame.Equals (BackgroundView.Frame)) {
					BackgroundView.Frame = newFrame;
				}
			}

			if (IsFirstConvoViewAfterCreation) // need to hide convo name title
			{
				TitleView.Hidden = AlertView.Hidden = true;
				return;
			}

			if (_tableLastHeight - Bounds.Height > 0) {
				ContentOffset = new CGPoint (
					ContentOffset.X,
					NMath.Min (
						ContentOffset.Y + (_tableLastHeight - Bounds.Height),
						NMath.Max (ContentSize.Height + ContentInset.Bottom - Bounds.Height, 0)
					)
				);
			}

			TitleView.ResizeTitle ();

			var newTitleHeight = TitleView.ContentSize.Height + AlertView.Bounds.Height;
			var newContentInset = new UIEdgeInsets (ContentInset.Top - _titleLastHeight + newTitleHeight, 
			                                        ContentInset.Left, 
			                                        ContentInset.Bottom, 
			                                        ContentInset.Right);
			
			if (!ContentInset.Equals (newContentInset)) {
				ContentInset = newContentInset;
			}

			if (_titleLastHeight != newTitleHeight) {
				_titleLastHeight = newTitleHeight;
			}

			var newTableHeight = Bounds.Height;
			if (_tableLastHeight != newTableHeight) {
				_tableLastHeight = newTableHeight;
			}

			TitleView.Scrolled (this);

			var newAlertViewFrame = new CGRect (0, ContentOffset.Y, AlertView.Frame.Width, AlertView.Frame.Height);
			if (!newAlertViewFrame.Equals (AlertView.Frame)) {
				AlertView.Frame = newAlertViewFrame;
			}
		}

		public override nfloat GetHeightForRow (ConversationCellModel cell)
		{
			// to collapse all the rows so that background view can be seen
			return _cellSizer.GetHeightForRow (cell, Bounds.Width);
		}

		public override void CellSelected (ConversationCellModel cell)
		{
			if (cell.ContentType == BlrtContentType.Date) {
				return;
			}

			base.CellSelected (cell);

			DeselectCell (cell, true);

			if (cell.Action == null) {
				
				if (cell.ShouldHaveMedia) 
				{
					if (!cell.IsMediaDataValid) {
						return;
					}
				}

				_parent.OpenContentItem (this, cell.ContentId).FireAndForget();
			}
		}

		void SetSlide (nfloat slide, bool animated)
		{
			var cells = VisibleCells;
			var value = NMath.Min (NMath.Max ((slide + Cells.SlidingCell.StartOffset) * 0.6f, -Cells.SlidingCell.MaxWidth), 0);
		
			for (int i = 0; i < cells.Length; ++i) {
				if (cells [i] is Cells.SlidingCell) {
					(cells [i] as Cells.SlidingCell).SetSlide (value, animated);
				}
			}
		}
	}
}

