﻿using System;
using UIKit;
using CoreGraphics;

namespace BlrtiOS.Views.Conversation
{
	public partial class ConversationNameSetupViewParent : UIView
	{
		ConversationNameSetupView ConvoNameSetupView { get; set; }

		public string ConversationName {
			get { return ConvoNameSetupView.ConvoNameField.Text; }
			private set {
				ConvoNameSetupView.ConvoNameField.Text = value;
				ConvoNameSetupView.SetCharCountStatus (value.Length, BlrtStyles.iOS.Gray7);
			}
		}

		public event EventHandler ConvoSaveBtnPressed {
			add { ConvoNameSetupView.ConvoSaveBtnPressed += value; }
			remove { ConvoNameSetupView.ConvoSaveBtnPressed -= value; }
		}

		public event EventHandler CancelBtnPressed {
			add { ConvoNameSetupView.CancelBtnPressed += value; }
			remove { ConvoNameSetupView.CancelBtnPressed -= value; }
		}

		public ConversationNameSetupViewParent (bool forEditingExistingConvoName = false)
		{
			BackgroundColor = BlrtStyles.iOS.Gray7.ColorWithAlpha (0.9f);
			ConvoNameSetupView = new ConversationNameSetupView (forEditingExistingConvoName);
            AddSubview (ConvoNameSetupView);
		}


		public void SetInitialConvoName (string name)
		{
			ConversationName = name;
            SetConvoNameFieldAsFirstResponder ();
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var newRect = new CGRect (0,
									  0,
									  Bounds.Width,
									  ConvoNameSetupView.SizeThatFits (new CGSize (Bounds.Width, float.MaxValue)).Height);
			if (newRect.Equals (ConvoNameSetupView.Frame))
				return;

			ConvoNameSetupView.Frame = newRect;
			SetConvoNameFieldAsFirstResponder ();
		}

		void SetConvoNameFieldAsFirstResponder ()
		{
			ConvoNameSetupView.ConvoNameField.SelectAll (null);
			ConvoNameSetupView.ConvoNameField.BecomeFirstResponder ();
		}
	}
}
