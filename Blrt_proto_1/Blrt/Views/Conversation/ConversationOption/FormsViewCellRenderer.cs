using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using BlrtiOS.Views.Conversation.ConversationOptions;
using UIKit;
using BlrtData.Views.Conversation.ConversationOptions;

[assembly: ExportRenderer (typeof(FormsViewCell), typeof(FormsViewCellRenderer))]
namespace BlrtiOS.Views.Conversation.ConversationOptions
{
	public class FormsViewCellRenderer: ViewCellRenderer
	{
		public FormsViewCellRenderer () : base ()
		{
		}

		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var formsCell = item as FormsViewCell;
			var theCell = base.GetCell (item, reusableCell, tv);

			if (!formsCell.ShowSelection)
				theCell.SelectionStyle = UITableViewCellSelectionStyle.None;

			var inset = formsCell.SeparatorInset;
			theCell.SeparatorInset = new UIEdgeInsets ((nfloat)inset.Top, (nfloat)inset.Left, (nfloat)inset.Bottom, (nfloat)inset.Right);
			if (!formsCell.ShowSeparator)
				theCell.SeparatorInset = new UIEdgeInsets (0, 9999, 0, 0);
			return theCell;
		}
	}
}

