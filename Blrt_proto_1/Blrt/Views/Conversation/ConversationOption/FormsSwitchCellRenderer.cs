using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using BlrtiOS.Views.Conversation.ConversationOptions;
using UIKit;
using BlrtData.Views.Conversation.ConversationOptions;

[assembly: ExportRenderer (typeof (FormsSwitchCell), typeof (FormsSwitchCellRenderer))]
namespace BlrtiOS.Views.Conversation.ConversationOptions
{
	public class FormsSwitchCellRenderer: SwitchCellRenderer
	{
		// TODO-FORMS-130
		// WAS: public override MonoTouch.UIKit.UITableViewCell GetCell (Cell item, MonoTouch.UIKit.UITableView tv)
		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var formsCell = item as FormsSwitchCell;
			var theCell = base.GetCell (item, reusableCell, tv);
			if (formsCell.TextSize == 0) {
				theCell.TextLabel.Font = BlrtStyles.iOS.CellTitleFont;
				theCell.TextLabel.Lines = 2;
			}else{
				theCell.TextLabel.Font = UIFont.SystemFontOfSize (formsCell.TextSize);
				theCell.TextLabel.Lines = 2;
			}
			theCell.TextLabel.SizeToFit ();


			if(theCell.AccessoryView is UISwitch){
				var sw = theCell.AccessoryView as UISwitch;

				sw.Enabled = formsCell.IsSwitchEnabled;
			}

			theCell.SelectionStyle = UITableViewCellSelectionStyle.None;

			if (!formsCell.ShowSeparator) {
				theCell.SeparatorInset = new UIEdgeInsets (theCell.SeparatorInset.Top, theCell.SeparatorInset.Left, theCell.SeparatorInset.Bottom, tv.Bounds.Width - theCell.SeparatorInset.Left);
			}
			return theCell;
		}
	}
}

