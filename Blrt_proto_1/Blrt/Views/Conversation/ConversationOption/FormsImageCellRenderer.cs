using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using BlrtData.Views.Conversation.ConversationOptions;
using BlrtiOS.Views.Conversation.ConversationOptions;

[assembly: ExportRenderer (typeof (FormsImageCell), typeof (FormsImageCellRenderer))]
namespace BlrtiOS.Views.Conversation.ConversationOptions
{
	public class FormsImageCellRenderer: ImageCellRenderer
	{
		public FormsImageCellRenderer ():base()
		{
		}

		// TODO-FORMS-130
		// WAS: public override MonoTouch.UIKit.UITableViewCell GetCell (Cell item, MonoTouch.UIKit.UITableView tv)
		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var iosCell = base.GetCell (item, reusableCell, tv);
			return iosCell;
		}
	}
}

