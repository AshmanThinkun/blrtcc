using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData.Views.Helpers;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using BlrtData;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class CommentCell : BubbleCell, IBindable<ConversationCellModel>
	{
		public const string Indentifer = ConversationCellModel.CommentCellIndentifer;

		CommentView _commentView;

		public CommentCell (IntPtr handler) : base (handler) {
			SetBubbleView (BubbleViewFactory.CreateCommentBubbleView ());
			InitCommentCellViews ();
		}
		public CommentCell () {
			SetBubbleView (BubbleViewFactory.CreateCommentBubbleView ());
			InitCommentCellViews ();
		}

		void InitCommentCellViews ()
		{
			base.InitViews();

			BubbleContentView = _commentView = new CommentView ();
			BubblePadding = new UIEdgeInsets (4, 12, 4, 12);

			SettingsView.Hidden = true;
		}


		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);

			_commentView.Text = data.Text;

			if (data.IsAuthor) {
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				_commentView.TextColor = BlrtStyles.iOS.White;
			} else {
				BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
				_commentView.TextColor = BlrtStyles.iOS.Black;
			}
		}


		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetBackgroundColor ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetBackgroundColor ();
		}

		void SetBackgroundColor ()
		{
			if (Selected || Highlighted) {

				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlueHighlight;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
				}
			} else {

				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
				}
			}
		}

		class CommentView : UIView
		{
			UITextView _commentLbl;

			public string Text { get { return _commentLbl.AttributedText.Value; } set { _commentLbl.AttributedText = FilterString (value); } }
			public UIColor TextColor { get { return _commentLbl.TextColor; } set { _commentLbl.TextColor = value; _commentLbl.AttributedText = FilterString (Text); } }

			public CommentView ()
			{

				_commentLbl = new UITextView () {
					BackgroundColor = UIColor.Clear,
					Font = BlrtStyles.iOS.CellContentFont,
					Selectable = false,
					Editable = false,
					ScrollEnabled = false,
					TextContainerInset = new UIEdgeInsets (8, 0, 0, 0),
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				};
				AddSubview (_commentLbl);

				var tap = new UITapGestureRecognizer (CommentTapped) {
					CancelsTouchesInView = false
				};

				_commentLbl.AddGestureRecognizer (tap);

			}

			void CommentTapped (UITapGestureRecognizer tap)
			{

				var position = _commentLbl.GetClosestPositionToPoint (tap.LocationInView (_commentLbl));
				var range = _commentLbl.GetOffsetFromPosition (_commentLbl.BeginningOfDocument, position);

				var links = FindUrls (_commentLbl.Text);


				for (int i = (int)(links.Length * 0.5) - 1; i > -1; --i) {

					if (range >= links [i * 2]
						&& range < links [i * 2 + 1]) {
						var str = _commentLbl.Text.Substring (links [i * 2], links [i * 2 + 1] - links [i * 2]).Trim (); ;

						switch (GetHyperLinkType(str)) {
							case HyperlinkType.Conversation :
							case HyperlinkType.ConversationItem :
								var t = new Uri (str);
								str = str.Replace (t.Scheme, BlrtConstants.UrlProtocol);
								BlrtManager.ParseUrl (str, true);
								break;
							case HyperlinkType.Other :
								if (!str.Contains ("://")) {
									str = "http://" + str;
								}
								BlrtData.BlrtManager.OpenHyperlink (str).FireAndForget ();
								break;
						}
						return;
					}
				}
			}

			enum HyperlinkType {
				Conversation,
				ConversationItem,
				Other
			}

			/// <summary>
			/// Gets the type of the hyperlink.
			/// </summary>
			/// <returns>The hyper link type.</returns>
			/// <param name="hyperlink">Hyperlink.</param>
			static HyperlinkType GetHyperLinkType (string hyperlink)
			{
				if (IsConversationLink (hyperlink)) {
					return HyperlinkType.Conversation;
				}

				if (IsBlrtLink (hyperlink)) {
					return HyperlinkType.ConversationItem;
				}

				return HyperlinkType.Other;
			}

			static class RegexConstants {
				const string ALPHANUM_CHAR = "A-Za-z0-9";
				const string CONVERSATION = BlrtUtil.URLConstants.Paths.CONVERSATION;
				const string BLRT = BlrtUtil.URLConstants.Paths.BLRT;

				static string AtleastOneAlphaNumChar {
					get {
						return string.Format ("[{0}]+", ALPHANUM_CHAR);
					}
				}

				static string AlphaNumCharWithSpecialChars {
					get {
						return string.Format (@"[{0}-\:\@]*", ALPHANUM_CHAR);
					}
				}

				static string Scheme {
					get {
						return AtleastOneAlphaNumChar;
					}
				}

				static string Authority {
					get {
						return string.Format (@"({0}(\.{1})*)+", AlphaNumCharWithSpecialChars, AlphaNumCharWithSpecialChars);
					}
				}

				public static string ConversationLinkRegex {
					get {
						return string.Format ("^(({0}://)*{1})/{2}/({3}/+)$",
									  Scheme,
									  Authority,
									  CONVERSATION,
									  AtleastOneAlphaNumChar);// convo id
					}
				}
				
				public static string BlrtLinkRegex {
					get {
						return string.Format ("^(({0}://)*{1})(/{2}/{3})?/{4}/({5}.*)$",
								              Scheme,
								              Authority,
											  CONVERSATION,
								              AtleastOneAlphaNumChar, // convo id
											  BLRT,
								              AtleastOneAlphaNumChar); // blrt id
					}
				}


			}
			/// <summary>
			/// Checks whether the link provided a Blrt link.
			/// </summary>
			/// <returns><c>true</c>, if blrt link was ised, <c>false</c> otherwise.</returns>
			/// <param name="hyperlink">Hyperlink.</param>
			static bool IsBlrtLink (string hyperlink)
			{
				Regex blrtRegex = new Regex (RegexConstants.BlrtLinkRegex, RegexOptions.IgnoreCase);
				return blrtRegex.Match (hyperlink)?.Success ?? false;
			}

			/// <summary>
			/// Checks whether the link provided a conversation link.
			/// </summary>
			/// <returns><c>true</c>, if conversation link was ised, <c>false</c> otherwise.</returns>
			/// <param name="hyperlink">Hyperlink.</param>
			static bool IsConversationLink (string hyperlink)
			{
				Regex convoRegex = new Regex (RegexConstants.ConversationLinkRegex, RegexOptions.IgnoreCase);
				return convoRegex.Match (hyperlink)?.Success ?? false;
			}

			public static int[] FindUrls (string input)
			{
				var l = new List<int> ();

				Regex regx = new Regex(@"([A-Za-z0-9]+:\/\/|www.)([-\w]+\.\w+)+([a-zA-Z0-9\~\!\@\#\$\%\^\[\]\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);        
				MatchCollection mactches = regx.Matches(input);        
				foreach (Match match in mactches)
				{
					l.Add (match.Index);
					l.Add (match.Index + match.Length);
				}    
				return l.ToArray();
			}

			public NSMutableAttributedString FilterString (string text)
			{
				var trimmed = text == null ? "" : text.Trim ().Replace ("\u200B", "");

				var output = new NSMutableAttributedString (trimmed,
					new UIStringAttributes () {
						Font = _commentLbl.Font,
						ForegroundColor = TextColor,
						//ParagraphStyle = new NSMutableParagraphStyle(){
						//	ParagraphSpacing = 8
						//}
					}
				);

				UIColor alertColor;
				if (TextColor == BlrtStyles.iOS.White) {
					alertColor = BlrtStyles.iOS.Gray2;
				} else {
					alertColor = BlrtStyles.iOS.Gray7;
				}

				var items = FindUrls (trimmed);

				for (int i = 0; i < items.Length * 0.5f; ++i) {
					output.AddAttributes (
						new UIStringAttributes () {
							ForegroundColor = alertColor,
							UnderlineStyle = NSUnderlineStyle.Single
						},
						new NSRange (items [i * 2], items [i * 2 + 1] - items [i * 2 + 0])
					);
				}

				return output;
			}


			public override CGSize SizeThatFits (CGSize size)
			{
				var rect = _commentLbl.SizeThatFits (size);

				rect.Height += 8;
				return rect;
			}
		}
	}
}

