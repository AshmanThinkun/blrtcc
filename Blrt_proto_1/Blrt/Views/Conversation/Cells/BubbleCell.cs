using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class BubbleCell : SlidingCellWithDateView	
	{
		public const float UnreadDotSize = 12;
		public const float AuthorLblHeight = 20;

		public const float HeaderHeight = 11;

		public const float FooterHeight = 16;

		public static CGSize PublicIconAreaSize = new CGSize(184,32);

		static public readonly float BubbleMaxWidth = (float)(BlrtHelperiOS.IsPhone ? UIScreen.MainScreen.Bounds.Width / 1.725f : 300);

		BubbleView _bubbleView;
		//float _bubbleMaxWidth;
		UIEdgeInsets _bubbleMargin;
		protected UIView _unreadDot;

		protected bool _showFooter;

		public UserThumbnailView UserThumbnail		{ get; private set; }

		public UILabel AuthorLbl					{ get; private set; }
		public UILabel PublicNameLbl 				{ get; protected set; }
		public CustomUI.CloudStateView CloudView	{ get; private set; }
		public UIButton SettingsView				{ get; private set; }

		public BubbleView BubbleView				{ get { return _bubbleView; } }
		public UIView BubbleContentView				{ get { return _bubbleView.ContentView; } set { _bubbleView.ContentView = value; } }

		public bool Unread							{ get { return !_unreadDot.Hidden; } 
			set{ 
				_unreadDot.Hidden = !value; 
			} 
		}

		public bool AlignLeft { get { return _bubbleView.AlignLeft; } set { 
				if(_bubbleView.AlignLeft != value){
					_bubbleView.AlignLeft = value; 
					SetNeedsLayout();
				}
			}
		}
		public UIEdgeInsets BubbleMargin { get { return _bubbleMargin; } set { 
				if (!_bubbleMargin.Equals(value)) {
					_bubbleMargin = value;
					SetNeedsLayout ();
				}
			}
		}
		public UIEdgeInsets BubblePadding { get { return _bubbleView.BubblePadding; } set { 
				if(!_bubbleView.BubblePadding.Equals(value)){
					_bubbleView.BubblePadding = value; 
					SetNeedsLayout (); 
				}
			}
		}

		UILongPressGestureRecognizer PressAndHoldGesture { get; set; }

		protected ConversationCellModel _model;


		public BubbleCell (IntPtr handler) : base (handler) {}
		public BubbleCell () {}

		protected void SetBubbleView (BubbleView bubbleView)
		{
			_bubbleView = bubbleView;
			_bubbleView.BubblePadding = new UIEdgeInsets (8, 8, 8, 8);
			BubbleMargin = new UIEdgeInsets (4, 40, 4, 40);

			ContentView.AddSubview (_bubbleView);
		}

		public void InitViews ()
		{
			UserThumbnail = new UserThumbnailView () {
				Frame = new CGRect(0,0, 26, 26)
			};
			_unreadDot = new UIView (new CGRect (0, 0, UnreadDotSize, UnreadDotSize)) {
				BackgroundColor = BlrtStyles.iOS.AccentRed,
				Layer = {
					CornerRadius = UnreadDotSize * 0.5f
				}
			};
			AuthorLbl = new UILabel {
				LineBreakMode = UILineBreakMode.MiddleTruncation
			};
			CloudView = new BlrtiOS.Views.CustomUI.CloudStateView () {
			};
			SettingsView = new UIButton () {
				Frame = new CGRect(0,0,48,48)
			};
			SettingsView.SetImage (UIImage.FromBundle ("settings_cog.png"), UIControlState.Normal);


			CloudView.TouchUpInside		+= CloudViewClicked;
			SettingsView.TouchUpInside	+= SettingsViewClicked;

			AlignLeft = true;
			AuthorLbl.Text = "";

			ContentView.AddSubviews (new UIView[]{
				UserThumbnail,
				_unreadDot,
				AuthorLbl,
				SettingsView,
				CloudView,
			});

			//BubbleMaxWidth = BlrtHelperiOS.IsPhone ? 220 : 280;
		}

		void CloudViewClicked (object sender, EventArgs e) {

			CloudView.TryShowingLoadingIndicator ();
			if (_model != null && _model.CloudPressed != null)
				_model.CloudPressed.Action (sender);
			
		}

		void SettingsViewClicked (object sender, EventArgs e) {

			if (_model != null && _model.SettingPressed != null)
				_model.SettingPressed.Action (sender);
		}

		protected void FBSharePressed(object sender, EventArgs e){
			if (_model != null && _model.SettingPressed is SettingAction){
				var settingAc = _model.SettingPressed as SettingAction;
				settingAc.FacebookSharePressed ();
			}
		}

		protected void TwitterSharePressed(object sender, EventArgs e){
			if (_model != null && _model.SettingPressed is SettingAction){
				var settingAc = _model.SettingPressed as SettingAction;
				settingAc.TwitterSharePressed ();
			}
		}

		protected void EmailSharePressed(object sender, EventArgs e){
			if (_model != null && _model.SettingPressed is SettingAction){
				var settingAc = _model.SettingPressed as SettingAction;
				settingAc.EmailShareLinkPressed ();
			}
		}

		protected void MessageSharePressed(object sender, EventArgs e){
			if (_model != null && _model.SettingPressed is SettingAction){
				var settingAc = _model.SettingPressed as SettingAction;
				settingAc.MessageShareLinkPressed ();
			}
		}

		protected void EmbedSharePressed (object sender, EventArgs e)
		{
			if (_model != null && _model.SettingPressed is SettingAction){
				var settingAc = _model.SettingPressed as SettingAction;
				settingAc.EmbededLinkClicked ();
			}
		}

		public override void Bind (ConversationCellModel model)
		{ 
			_model = model;
			var shouldHaveMedia = model.ShouldHaveMedia;
			var isMediaDataValid = shouldHaveMedia ? _model.IsMediaDataValid : true;

			SetLongPressGestureRecognizer (shouldHaveMedia && !isMediaDataValid);

			BubbleView.IsLastConversationItemBySameAuthor = _model.IsLastConversationItemBySameAuthor;

			base.Bind (model);
			Unread = model.Unread && !model.IsAuthor && shouldHaveMedia && isMediaDataValid;
			AlignLeft = !model.IsAuthor;

			CloudView.CloudState = model.CloudState;
			CloudView.Hidden = (shouldHaveMedia && !isMediaDataValid) || CloudView.CloudState == BlrtData.CloudState.Synced;

			AuthorLbl.Hidden = !model.ShowAuthor;
			UserThumbnail.Hidden = !model.IsLastConversationItemBySameAuthor;

			_showFooter = model.ShowFooter || BubbleView.IsLastConversationItemBySameAuthor;

			var userDeleted = false;

			if (model.Author != null) {
				userDeleted = model.Author.IsDeleted;
				//AuthorLbl.Text = GetAuthorLabelText (model.Author, userDeleted, AuthorLbl.Frame.Width);
				AuthorLbl.AttributedText = GetAuthorLabelText (model.Author, userDeleted, GetAutorLabelWidth());
				//AuthorLbl.SizeToFit ();
				UserThumbnail.Text = userDeleted ? string.Empty : model.Author.GetInitials ();
				UserThumbnail.ImagePath = userDeleted ? BlrtUtil.DELETED_ACCOUNT_AVATAR_PATH : model.Author.LocalAvatarPath;
			}

			UserThumbnail.Clicked -= OpenProfile;

			if (!userDeleted) {
				UserThumbnail.Clicked += OpenProfile;
			}

			if (_model.IsPublicBlrt) {
				_showFooter = true;
			}
		}

		NSAttributedString GetAuthorLabelText (BlrtUser author, bool userDeleted, nfloat labelWdith)
		{
			var displayName = new NSAttributedString (author.DisplayName, BlrtStyles.iOS.CellDetailBoldFont, BlrtStyles.iOS.Gray5);
			if (!userDeleted) {
				return displayName;
			}

			var deletedString = BlrtHelperiOS.Translate ("deleted_user_string", "profile");
			var deletedUserMessage = " " + deletedString;

			var deleteAttributedString = new NSMutableAttributedString (deletedUserMessage, new UIStringAttributes { 
				Font = BlrtStyles.iOS.CellDetailFont, 
				ForegroundColor = BlrtStyles.iOS.AccentRed, 
				Obliqueness = 0.25f
			});


			var labelText = new NSMutableAttributedString ();
			labelText.Append (displayName);
			labelText.Append (deleteAttributedString);

			return labelText;
		}

		void SetLongPressGestureRecognizer (bool remove)
		{
			if (remove) {
				if (PressAndHoldGesture != null) {
					_bubbleView.RemoveGestureRecognizer (PressAndHoldGesture);
				}
				return;
			}

			if (PressAndHoldGesture == null) {
				PressAndHoldGesture = new UILongPressGestureRecognizer ((UILongPressGestureRecognizer obj) => {
					if (obj.State == UIGestureRecognizerState.Began)
						SettingsViewClicked (_bubbleView, EventArgs.Empty);
				});
			}
			_bubbleView.AddGestureRecognizer (PressAndHoldGesture);
		}

		void OpenProfile(object sender, EventArgs e){
			BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), _model.Author);
		}

		nfloat GetAuthorLabelLeftPadding ()
		{
			return AlignLeft ? 16 : 10;
		}

		nfloat GetAutorLabelWidth ()
		{
			return NMath.Min (Bounds.Width, BubbleMaxWidth - GetAuthorLabelLeftPadding());
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var rect = BubbleMargin.InsetRect (Bounds);

			if (!AlignLeft)
				rect.X += rect.Width - BubbleMaxWidth;
			
			if (rect.Width > BubbleMaxWidth) {
				rect.Width = Math.Min((float)BubbleView.SizeThatFits(rect.Size).Width, BubbleMaxWidth);
			}

			if (!AlignLeft)
				rect.X += BubbleMaxWidth - rect.Width;

			var LabelLeftPadding = GetAuthorLabelLeftPadding();

			if (!AuthorLbl.Hidden) {
				var authorLblWidth = GetAutorLabelWidth();
				var rectMinX = rect.GetMinX ();

				if (AlignLeft) {
					AuthorLbl.Frame = new CGRect (rect.X + LabelLeftPadding/*rectMaxX - authorLblWidth*/, rect.Top + HeaderHeight, authorLblWidth, AuthorLblHeight);
				}
				else {
					nfloat diff = 0.0f;
					if (authorLblWidth > rect.Width) {
						diff = authorLblWidth - rect.Width;
					}
					AuthorLbl.Frame = new CGRect (rect.X - diff + LabelLeftPadding/*rectMaxX - authorLblWidth*/, rect.Top + HeaderHeight, authorLblWidth, AuthorLblHeight);
				}
				rect.Y += AuthorLblHeight + HeaderHeight;
				rect.Height -= AuthorLblHeight + HeaderHeight;
			}
			if (_showFooter) {
				rect.Height -= FooterHeight;
			}

			if(PublicNameLbl!=null && !PublicNameLbl.Hidden){
				var nameSize = PublicNameLbl.SizeThatFits (new CGSize (rect.Size.Width - 10, nfloat.MaxValue));
				rect.Height -= nameSize.Height;
				rect.Height -= PublicIconAreaSize.Height + BubbleMargin.Bottom*2;
			}

			BubbleView.Frame = rect;

			if (DateLabel != null) {
				DateLabel.Center = new CGPoint (ContentView.Frame.Width + DateCenterOffset, rect.Y + (int)(rect.Height * 0.5f));
			}

			//if iphone 5 or less, then we need to position the cloud and cog diffrently
			var micro = Bounds.Width < 340;

			if (!AlignLeft) {
				UserThumbnail.Center	= new CGPoint (Bounds.Width - 25, BubbleView.Frame.Bottom - UserThumbnail.Bounds.Height * 0.5f);

				if (SettingsView.Hidden) {
					CloudView.Center		= new CGPoint (BubbleView.Frame.Left - 30, BubbleView.Center.Y);
				} else if (CloudView.Hidden) {
					SettingsView.Center		= new CGPoint (BubbleView.Frame.Left - 30, BubbleView.Center.Y);
				} else {

					if (micro) {
						SettingsView.Center		= new CGPoint (BubbleView.Frame.Left - 30, BubbleView.Center.Y - 16);
						CloudView.Center		= new CGPoint (BubbleView.Frame.Left - 30, BubbleView.Center.Y + 16);		
					} else {
						SettingsView.Center		= new CGPoint (BubbleView.Frame.Left - 30, BubbleView.Center.Y);
						CloudView.Center		= new CGPoint (BubbleView.Frame.Left - 70, BubbleView.Center.Y);					
					}

				}
			} else {
				UserThumbnail.Center	= new CGPoint (25, BubbleView.Frame.Bottom - UserThumbnail.Bounds.Height * 0.5f);

				if (SettingsView.Hidden) {
					CloudView.Center		= new CGPoint (BubbleView.Frame.Right + 30, BubbleView.Center.Y);
				} else if (CloudView.Hidden) {
					SettingsView.Center		= new CGPoint (BubbleView.Frame.Right + 30, BubbleView.Center.Y);
				} else {

					if (micro) {
						SettingsView.Center		= new CGPoint (BubbleView.Frame.Right + 30, BubbleView.Center.Y - 16);
						CloudView.Center		= new CGPoint (BubbleView.Frame.Right + 30, BubbleView.Center.Y + 16);
					} else {
						SettingsView.Center		= new CGPoint (BubbleView.Frame.Right + 30, BubbleView.Center.Y);
						CloudView.Center		= new CGPoint (BubbleView.Frame.Right + 70, BubbleView.Center.Y);				
					}

				}
			}
			_unreadDot.Frame = new CGRect (UserThumbnail.Frame.X + (UserThumbnail.Frame.Width - _unreadDot.Bounds.Width) * 0.5f, BubbleView.Frame.Top, _unreadDot.Bounds.Width, _unreadDot.Bounds.Height);

		}

		public override UIView HitTest (CGPoint point, UIEvent uievent) {
			var view = base.HitTest (point, uievent);

			if (BlrtHelperiOS.IsChild (view, BubbleView) || view == BubbleView || view==UserThumbnail)
				return view;

			if (view is UIButton)
				return view;

			return null;
		}


		public override CGSize SizeThatFits (CGSize size)
		{
			var height = (BubbleMargin.Bottom + BubbleMargin.Top);
			var width = size.Width;

			if (!AuthorLbl.Hidden)
				height += AuthorLblHeight + HeaderHeight;
			if (_showFooter)
				height += FooterHeight;

			var bubbleWidth = NMath.Min(width - (BubbleMargin.Left + BubbleMargin.Right), BubbleMaxWidth);
			var bubble = BubbleView.SizeThatFits (new CGSize (bubbleWidth, nfloat.MaxValue));



			return new CGSize (
				bubble.Width,
				NMath.Max(bubble.Height, _unreadDot.Frame.Height + UserThumbnail.Frame.Height + 1) + height
			);
		}
	}
}

