﻿using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class AudioCell : BubbleCell
	{
		public const string Indentifer = ConversationCellModel.AudioCellIndentifer;

		AudioCellView _view;
		static AudioPlayPauseBtnDelegate _AudioPlayPauseBtnDelegate { get; set;}

		public AudioCell (IntPtr handler) : base (handler) {
			SetBubbleView (BubbleViewFactory.CreateAudioBubbleView ());
			InitAudioCellViews ();
		}
		public AudioCell (AudioPlayPauseBtnDelegate audioPlayPauseBtnDelegate) {
			_AudioPlayPauseBtnDelegate = audioPlayPauseBtnDelegate; 
			SetBubbleView (BubbleViewFactory.CreateAudioBubbleView ());
			InitAudioCellViews (); 
		}

		public void InitAudioCellViews ()
		{   
			base.InitViews ();

			BubblePadding = new UIEdgeInsets (4, 4, 4, 4);
			BubbleContentView = _view = new AudioCellView (_AudioPlayPauseBtnDelegate);
			SettingsView.Hidden = true;
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
			_view.ResetViews (true);
		}

		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);
			SettingsView.Hidden = true;//!data.OnParse;

			_view.AlignLeft = !data.IsAuthor;
			_view.AudioDurationSec = data.Text;
			_view.ContentId = data.ContentId;

			if (data.IsAuthor) {
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
			} else {
				BubbleView.BubbleColor = BlrtStyles.iOS.Charcoal;
			}
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetBackgroundColor ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetBackgroundColor ();
		}

		void SetBackgroundColor ()
		{
			if (Selected || Highlighted) {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlueHighlight;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.CharcoalHighlight;
				}
			} else {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Charcoal;
				}
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var height = (BubbleMargin.Bottom + BubbleMargin.Top);
			var width = size.Width;

			if (!AuthorLbl.Hidden)
				height += AuthorLblHeight + HeaderHeight;

			if (_showFooter)
				height += FooterHeight;

			var bubbleWidth = NMath.Min (width - (BubbleMargin.Left + BubbleMargin.Right), BubbleMaxWidth);
			var bubble = BubbleView.SizeThatFits (new CGSize (bubbleWidth, nfloat.MaxValue));

			return new CGSize (
				bubble.Width,
				bubble.Height + height
			);
		}

		class AudioCellView : UIView
		{
			const int BTN_WIDTH = 44;
			const int SLIDER_PADDING = 5;

			string _contentId;
			public string ContentId { 
				get { return _contentId;}
				set {
					_contentId = value;
					UpdateViews ();
				} 
			}

			string MaxDuration { get; set; }
			UIButton PlayPauseButton { get; set; }
			UILabel AudioDuration { get; set; }
			UISlider ProgressSlider { get; set; }
			UIImage PlayBtnImage { get; set; }
			UIImage PauseBtnImage { get; set; }
			UIButton MicBtn { get; set; }
			int AudioLength { get; set; }
			bool _alignLeft;
			AudioPlayPauseBtnDelegate audioPlayPauseBtnDelegate;

			UIImage MicBtnImg {
				get {
					return UIImage.FromBundle ("audio_mic.png");
				}
			}

			UIImage PlayPauseButtonImage {
				get {
					return AudioPlaybackManager.IsAudioPlaying ? PauseBtnImage : PlayBtnImage;
				}
			}

			public string AudioDurationSec {
				get { return AudioLength.ToString (); }
				set {
					AudioLength = int.Parse (value);
					ProgressSlider.MaxValue = AudioLength;
					AudioDuration.Text = MaxDuration = BlrtUtil.ToMinutes (AudioLength);
				}
			}

			public bool AlignLeft {
				get { return _alignLeft; }
				set {
					if (value != _alignLeft) {
						_alignLeft = value;
						SetNeedsLayout ();
					}
				}
			}

			const int TIME_INTERVAL = 100;
			void ScheduleSliderAndDurationUpdates ()
			{
				InvokeInBackground(delegate {
					var timeElapsedSinceLastDurationUpdate = 0;
					while (AudioPlaybackManager.IsAudioPlaying &&
						   AudioPlaybackManager.CurrentContentId != null &&
						   ContentId.Equals (AudioPlaybackManager.CurrentContentId)) 
					{
						InvokeOnMainThread (delegate {
							var CurrentTime = AudioPlaybackManager.AudioManager.CurrentTime ();
							ProgressSlider.Value = CurrentTime;
							// time update needs less refresh rate as it's precision is down to a second.
							if (timeElapsedSinceLastDurationUpdate >= 1000) {
								/*
								 * Time reported by player is slightly short of 1 sec. If it is less than one second,
								 * time is considered to be 0 seconds, therefore display doesn't get updated and time 
								 * on display remains 0 sec after 1 sec. To tackle this issue, find the time difference 
								 * between 1 sec and the time reported by player. Add this difference to the time 
								 * reported by player. This makes sure that everytime when we update display, it is 
								 * after one second.
								 */
								var timeDiff = Math.Max (1.0f - CurrentTime, 0.0f);
								timeElapsedSinceLastDurationUpdate = 0;
								AudioDuration.Text = BlrtUtil.ToMinutes (CurrentTime + timeDiff);
							}
						});
						Thread.Sleep (TIME_INTERVAL);
						timeElapsedSinceLastDurationUpdate += TIME_INTERVAL;
					}});
			}

			public void ResetViews (bool forCellReuse)
			{
				ProgressSlider.Value = 0;
				PlayPauseButton.SetImage (forCellReuse? PlayBtnImage : PlayPauseButtonImage, UIControlState.Normal);
				AudioDuration.Text = MaxDuration;
			}

			public void OnAudioFinised (object s, EventArgs e)
			{
				AudioPlaybackManager.AudioManager.AudioFinishedCallback -= OnAudioFinised;

				if (!ContentId.Equals (AudioPlaybackManager.CurrentContentId)) {
					return;
				}

				AudioPlaybackManager.CurrentContentId = string.Empty;
				AudioPlaybackManager.IsAudioPlaying = false;
				ResetViews (false);
			}

			void OnPlayPauseButtonTouched (object s, EventArgs e) {

				if (!ContentId.Equals (AudioPlaybackManager.CurrentContentId)) {
					AudioPlaybackManager.AudioManager.AudioFinishedCallback -= OnAudioFinised;
					AudioPlaybackManager.AudioManager.AudioFinishedCallback += OnAudioFinised;
				}

				InvokeOnMainThread (async delegate {
					if (!ContentId.Equals (AudioPlaybackManager.CurrentContentId)) {
						AudioDuration.Text = "0:00";
					}
					await audioPlayPauseBtnDelegate (ContentId, AudioPlaybackManager.IsAudioPlaying);
					if (AudioPlaybackManager.IsAudioPlaying) {
						if (AudioPlaybackManager.LastAudioCell != null && AudioPlaybackManager.LastAudioCell != this) {
							var lastAudioCell = (AudioCellView)AudioPlaybackManager.LastAudioCell;
							lastAudioCell.AudioDuration.Text = AudioPlaybackManager.MaxDurationOfLastAudio;
							lastAudioCell.ResetViews (true);
						}
						AudioPlaybackManager.MaxDurationOfLastAudio = AudioDuration.Text;
						AudioPlaybackManager.LastAudioCell = this;
					} else { 
						AudioPlaybackManager.LastAudioCell = null; 
					}
					PlayPauseButton.SetImage (PlayPauseButtonImage, UIControlState.Normal);
					ScheduleSliderAndDurationUpdates ();
				});
			}

			public AudioCellView (AudioPlayPauseBtnDelegate audioPlayPauseBtnDelegate)
			{
				AudioLength = 0;
				ContentId = string.Empty;
				this.audioPlayPauseBtnDelegate = audioPlayPauseBtnDelegate;
				PlayBtnImage = UIImage.FromBundle ("audio_play.png");
				PauseBtnImage = UIImage.FromBundle ("audio_pause.png");
				PlayPauseButton = new UIButton (new CGRect (0, 0, BTN_WIDTH, BTN_WIDTH));

				PlayPauseButton.TouchUpInside += OnPlayPauseButtonTouched;

				PlayPauseButton.SetImage (PlayPauseButtonImage, UIControlState.Normal);

				AudioDuration = new UILabel () {
					Lines = 1,
					TextColor = BlrtStyles.iOS.White,
					Font = BlrtStyles.iOS.CellTitleBoldFont,
					Text = "0:00"
				};

				ProgressSlider = new UISlider () {
					MinValue = 0,
					TintColor = BlrtStyles.iOS.White,
					MinimumTrackTintColor = BlrtStyles.iOS.Green
				};

				ProgressSlider.SetThumbImage (UIImage.FromBundle ("audio_scrub.png"), UIControlState.Normal);
				MicBtn = new UIButton () {
					UserInteractionEnabled = false,
					Enabled = true
				};

				MicBtn.SetImage (MicBtnImg, UIControlState.Normal);
				MicBtn.SizeToFit ();

				Frame = new CGRect (0, 0, int.MaxValue, Math.Max (MicBtn.Frame.Height, PlayPauseButton.Frame.Height));

				AddSubviews (new UIView [] {
					PlayPauseButton,
					AudioDuration,
					ProgressSlider,
					MicBtn
				});

			}

			void UpdateViews ()
			{
				var ContentIdOfCurrentlyPlayingAudio = AudioPlaybackManager.CurrentContentId;
				if (ContentIdOfCurrentlyPlayingAudio != null && ContentIdOfCurrentlyPlayingAudio.Equals (ContentId)) {
					if (AudioPlaybackManager.IsAudioPlaying) {
						AudioPlaybackManager.AudioManager.AudioFinishedCallback -= OnAudioFinised;
						AudioPlaybackManager.AudioManager.AudioFinishedCallback += OnAudioFinised;
						PlayPauseButton.SetImage (PauseBtnImage, UIControlState.Normal);
						ScheduleSliderAndDurationUpdates ();
					}
				} else {
					// Do nothing, everything has already been reset to defaults by "PrepareForReuse" method.
				}
			}

			static readonly nfloat CELL_TRAILING_PADDING = 0;
			static readonly nfloat CELL_LEADING_PADDING = 10;
			static readonly nfloat AUDIO_LENGTH_LABEL_PADDING = 5;

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				// playpause button
				var Y = (Bounds.Height - PlayPauseButton.Frame.Size.Height) * 0.5;
				var newRect = new CGRect (Bounds.Left + CELL_TRAILING_PADDING,
										  Y,
										  PlayPauseButton.Frame.Size.Width,
										  PlayPauseButton.Frame.Size.Height);

				if (!PlayPauseButton.Frame.Equals (newRect)) {
					PlayPauseButton.Frame = newRect;
				}

				// MicBtn
				Y = (Bounds.Height - MicBtn.Frame.Size.Height) * 0.5;
				newRect = new CGRect (Bounds.Right - MicBtn.Frame.Size.Width - CELL_LEADING_PADDING,
									  Y,
									  MicBtn.Frame.Size.Width,
									  MicBtn.Frame.Size.Height);

				if (!MicBtn.Frame.Equals (newRect)) {
					MicBtn.Frame = newRect;
				}

				// Audio length label
				var size = AudioDuration.SizeThatFits (Bounds.Size);
				var halfHeight = size.Height * 0.5;
				Y = PlayPauseButton.Frame.GetMidY () - halfHeight;
				newRect = new CGRect (MicBtn.Frame.Left - size.Width - AUDIO_LENGTH_LABEL_PADDING,
									  Y,
									  size.Width + AUDIO_LENGTH_LABEL_PADDING,
									  size.Height);

				if (!AudioDuration.Frame.Equals (newRect)) {
					AudioDuration.Frame = newRect;
				}

				// Duration slider
				size = ProgressSlider.SizeThatFits (Bounds.Size);
				var width = AudioDuration.Frame.Left - PlayPauseButton.Frame.Right - SLIDER_PADDING;
				halfHeight = size.Height * 0.5;
				Y = PlayPauseButton.Frame.GetMidY () - halfHeight;
				newRect = new CGRect (PlayPauseButton.Frame.Right - SLIDER_PADDING * 0.5,
									  Y,
									  width,
									  size.Height);

				if (!ProgressSlider.Frame.Equals (newRect)) {
					ProgressSlider.Frame = newRect;
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				return new CGSize (Bounds.Width,
								   Math.Max (MicBtn.Frame.Height, PlayPauseButton.Frame.Height));
			}
		}
	}
}