﻿using System;
using BlrtData;

namespace BlrtiOS
{
	public static partial class BubbleViewFactory
	{
		public static Views.Conversation.Cells.BubbleView CreateBlrtBubbleView() 
		{
			return new BlrtBubbleView ();
		}

		public static Views.Conversation.Cells.BubbleView CreateCommentBubbleView ()
		{
			return new CommentBubbleView ();
		}

		public static Views.Conversation.Cells.BubbleView CreateAudioBubbleView ()
		{
			return new AudioBubbleView ();
		}

		public static Views.Conversation.Cells.BubbleView CreateMediaBubbleView ()
		{
			return new MediaBubbleView ();
		}

		public static Views.Conversation.Cells.BubbleView CreateDateBubbleView ()
		{
			return new DateBubbleView ();
		}
	}
}
