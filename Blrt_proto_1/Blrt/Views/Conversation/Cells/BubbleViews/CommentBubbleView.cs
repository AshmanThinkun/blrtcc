﻿using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS
{
	public static partial class BubbleViewFactory
	{
		class CommentBubbleView : Views.Conversation.Cells.BubbleView
		{
			public override void Draw (CoreGraphics.CGRect rect)
			{
				base.Draw (rect);

				using (CGContext gctx = UIGraphics.GetCurrentContext ()) {

					var shape = IsLastConversationItemBySameAuthor ?
						BlrtiOS.Util.BlrtUtilities.CreateMessageBubbleWithQuote (
							rect,
						    CornerRadius,
						    AligmentPadding,
						    AlignLeft
                        ) 
				           :
			            BlrtiOS.Util.BlrtUtilities.CreateMessageBubbleWithOutQuote (
				           rect,
						   CornerRadius,
						   AligmentPadding,
						   AlignLeft
			            );
					
					gctx.SetFillColor (BubbleColor.CGColor);
					gctx.AddPath (shape.CGPath);
					gctx.DrawPath (CGPathDrawingMode.Fill);
				}
			}
		}
	}
}