﻿using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS
{
	public static partial class BubbleViewFactory
	{
		class BlrtBubbleView : Views.Conversation.Cells.BubbleView
		{
			static readonly float STROKE_WIDTH = 4;

			public override void Draw (CoreGraphics.CGRect rect)
			{
				base.Draw (rect);

				using (CGContext gctx = UIGraphics.GetCurrentContext ()) {
					var shape = IsLastConversationItemBySameAuthor ?
						BlrtiOS.Util.BlrtUtilities.CreateMessageBubbleWithQuote (
							Bounds,
							Views.Conversation.Cells.BlrtCell.BLRT_VIEW_CORNER_RADIUS,
							AligmentPadding,
							AlignLeft
						)
						   :
						BlrtiOS.Util.BlrtUtilities.CreateMessageBubbleWithOutQuote (
						   Bounds,
						   Views.Conversation.Cells.BlrtCell.BLRT_VIEW_CORNER_RADIUS,
						   AligmentPadding,
						   AlignLeft
						);

					gctx.SetFillColor (BubbleColor.CGColor);
					gctx.SetStrokeColor (BlrtStyles.iOS.Green.CGColor);
					gctx.SetLineWidth (STROKE_WIDTH);

					gctx.AddPath (shape.CGPath);
					gctx.DrawPath (CGPathDrawingMode.FillStroke);
				}
			}
		}
	}
}
