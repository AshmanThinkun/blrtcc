﻿using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS
{
	public static partial class BubbleViewFactory
	{
		class DateBubbleView : Views.Conversation.Cells.BubbleView
		{
			public override void Draw (CGRect rect)
			{
				base.Draw (rect);

				using (CGContext gctx = UIGraphics.GetCurrentContext ()) {
					var shape = Util.BlrtUtilities.CreateMessageBubbleWithOutQuote (
					   Bounds,
					   CornerRadius,
					   AligmentPadding,
					   AlignLeft
					);
					gctx.SetFillColor (BubbleColor.CGColor);
					gctx.AddPath (shape.CGPath);
					gctx.DrawPath (CGPathDrawingMode.Fill);
				}
			}
		}
	}
}
