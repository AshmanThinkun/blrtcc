using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace BlrtiOS.Views.Conversation.Cells
{
	public abstract class BubbleView : UIView
	{
		public const float AligmentPadding  = 8;
		public const float CornerRadius  = 15;

		UIView _contentView;
		UIEdgeInsets _bubblePadding;
		UIColor _bubbleColor;
		bool _isLastConversationItemBySameAuthor;

		bool _alignmentLeft = false;

		public bool IsLastConversationItemBySameAuthor {
			get {
				return _isLastConversationItemBySameAuthor;
			}
			set {
				if (value != _isLastConversationItemBySameAuthor) {
					_isLastConversationItemBySameAuthor = value;
					BeginInvokeOnMainThread (() => {
						SetNeedsLayout ();
					});
				}
			}
		}

		public UIView ContentView { 
			get { return _contentView; } 
			set{
				if (_contentView != value) {
					if (_contentView != null)
						_contentView.RemoveFromSuperview ();
					_contentView = value;
					if (_contentView != null)
						AddSubview(_contentView);
				}
			}
		}

		public bool AlignLeft { get { return _alignmentLeft; } 
			set {
				if(!_alignmentLeft.Equals(value)){
					_alignmentLeft = value; 
					SetNeedsLayout (); 
				} 
			} 
		}

		public UIEdgeInsets BubblePadding { get { return _bubblePadding; } 
			set { 
				if(!_bubblePadding.Equals(value)){
					_bubblePadding = value; 
					SetNeedsLayout (); 
				} 
			} 
		}
		
		public UIColor BubbleColor { get { return _bubbleColor; } 
			set { 
				if(!_bubbleColor.Equals(value)){
					_bubbleColor = value; 
					SetNeedsDisplay (); 
				} 
			} 
		}

		public BubbleView() {

			_bubbleColor = BlrtStyles.iOS.Gray4;
			SetNeedsDisplay ();
			BackgroundColor = UIColor.Clear;
		}

		public override void Draw (CGRect rect) 
		{
			BackgroundColor = UIColor.Clear;
			base.Draw (rect);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (_contentView != null) {
				var rect = BubblePadding.InsetRect (Bounds);
				if (_alignmentLeft) {
					rect.X += AligmentPadding;
				}
				rect.Width -= AligmentPadding;
				_contentView.Frame = rect;
			}

			SetNeedsDisplay ();
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			if (_contentView != null) {
				size.Width = size.Width - (AligmentPadding + BubblePadding.Left + BubblePadding.Right);
				size.Height = size.Height - (BubblePadding.Top + BubblePadding.Bottom);

				var result = _contentView.SizeThatFits (size);


				return new CGSize (
					result.Width + (AligmentPadding + BubblePadding.Left + BubblePadding.Right),
					result.Height + (BubblePadding.Top + BubblePadding.Bottom)
				);
			}

			return new CGSize (
				(AligmentPadding + BubblePadding.Left + BubblePadding.Right),
				(BubblePadding.Top + BubblePadding.Bottom)
			);
		}
	}

}