﻿using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using System.Linq;
using CoreAnimation;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class PDFCell : BubbleCell
	{
		public const string Indentifer = ConversationCellModel.PdfCellIndentifer;
		static readonly UIEdgeInsets bubblePadding = new UIEdgeInsets (4, 4, 4, 4);

		static readonly nfloat IMAGE_CELL_MAX_WIDTH = BubbleMaxWidth - 40;
		protected static readonly nfloat PdfContentViewMaxWidth = IMAGE_CELL_MAX_WIDTH -
															   bubblePadding.Left -
															   bubblePadding.Right -
															   BubbleView.AligmentPadding;
		
		public static CGSize SinglePDFCellSize {
			get {
				return new CGSize (PDFCellView.GetSingleImageBubbleWidth (PdfContentViewMaxWidth),
								   PDFCellView.GetSingleImageBubbleHeight (PdfContentViewMaxWidth));
			}
		}

		PDFCellView _view;

		public PDFCell (IntPtr handler) : base (handler)
		{
			SetBubbleView (BubbleViewFactory.CreateMediaBubbleView ());
			InitMediaCellViews ();
		}
		public PDFCell ()
		{
			SetBubbleView (BubbleViewFactory.CreateMediaBubbleView ());
			InitMediaCellViews ();
		}

		public void InitMediaCellViews ()
		{
			base.InitViews ();

			BubblePadding = bubblePadding;
			BubbleContentView = _view = new PDFCellView ();
			SettingsView.Hidden = true;
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
			_view.ReleaseImages ();
		}

		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);

			var mediaType = data.MediaType;

			SettingsView.Hidden = !data.OnParse;

			_view.AlignLeft = !data.IsAuthor;
			_view.SetImageAsyncSources (data.CloudPathThumbnails, data.PathThumbnails);

			var mediaNames = data.MediaNames;
			if (mediaNames != null && mediaNames.Count () > 0) {
				_view._pdfName.Text = data.MediaNames [0];

				base.LayoutIfNeeded ();
			}

			if (data.IsAuthor) {
				_view._pdfName.TextColor = BlrtStyles.iOS.White;
			} else {
				_view._pdfName.TextColor = BlrtStyles.iOS.Black;
			}

			_view.PDFPageCount = data.PageCount;

			if (data.IsAuthor) {
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
			} else {
				BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
			}
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetBackgroundColor ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetBackgroundColor ();
		}

		void SetBackgroundColor ()
		{
			if (Selected || Highlighted) {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlueHighlight;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.CharcoalHighlight;
				}
			} else {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
				}
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var height = (BubbleMargin.Bottom + BubbleMargin.Top);
			var width = size.Width;

			if (!AuthorLbl.Hidden)
				height += AuthorLblHeight + HeaderHeight;

			if (_showFooter)
				height += FooterHeight;

			var bubbleWidth = NMath.Min (width - (BubbleMargin.Left + BubbleMargin.Right), BubbleMaxWidth);
			var bubble = BubbleView.SizeThatFits (new CGSize (bubbleWidth, nfloat.MaxValue));

			return new CGSize (
				bubble.Width,
				bubble.Height + height
			);
		}

		class PDFCellView : UIView
		{


			/********************* PDFCellViewHeader class ************************/
			class PDFCellViewHeader : UIView 
			{
				readonly int Spacing = 3;
				readonly string PDFLabelText = "PDF";

				int _pdfPageCount;
				PDFCellPageCounterView _pageCounterView;
				UILabel _pdfLabel;

				UIColor PrimaryColor {
					get {
						return BlrtStyles.iOS.Charcoal.ColorWithAlpha (0.95f);
					}
				}

				UIColor SecondaryColor {
					get {
						return BlrtStyles.iOS.WhiteAlpha.ColorWithAlpha (0.0f);
					}
				}

				CAGradientLayer ColorGradient {
					get {
						return new CAGradientLayer () {
							Colors = new CoreGraphics.CGColor [] { PrimaryColor.CGColor, SecondaryColor.CGColor },
							StartPoint = new CoreGraphics.CGPoint (1.0f, 0.0f),
							EndPoint = new CoreGraphics.CGPoint (1.0f, 1.0f)
						};
					}
				}

				public int PDFPageCount {
					get { return _pdfPageCount; }
					set {
						_pageCounterView.SetPages (value);
						_pdfPageCount = value;
					}
				}

				public PDFCellViewHeader()
				{ 
					BackgroundColor = SecondaryColor;
					Layer.InsertSublayer (ColorGradient, 0);
					Layer.CornerRadius = Layer.Sublayers [0].CornerRadius = PDFCellView.CellCornerRadius;

					_pageCounterView = new PDFCellPageCounterView ();

					_pdfLabel = new UILabel () {
						Lines = 1,
						Text = PDFLabelText,
						TextColor = BlrtStyles.iOS.White,
						Font = BlrtStyles.iOS.CellTitleBoldFont,
						TextAlignment = UITextAlignment.Center
					};
					_pdfLabel.SizeToFit ();

					AddSubview (_pageCounterView);
					AddSubview (_pdfLabel);
				}

				public override void LayoutSubviews ()
				{
					base.LayoutSubviews ();

					// gradient layer
					var newGradientLayerFrame = Bounds;
					if (!Layer.Sublayers [0].Frame.Equals (newGradientLayerFrame)) {
						Layer.Sublayers [0].Frame = Bounds;
					}

					// page counter
					_pageCounterView.SizeToFit ();
					var size = _pageCounterView.Frame.Size;
					var newPageCounterViewFrame = new CGRect (Bounds.Right - size.Width + 5,
					                                          Bounds.Top + Spacing * 3,
					                                          size.Width,
					                                          size.Height);
					
					if (!_pageCounterView.Frame.Equals (newPageCounterViewFrame)) {
						_pageCounterView.Frame = newPageCounterViewFrame;

						// PDF label
						size = _pdfLabel.Frame.Size;
						_pdfLabel.Center = new CGPoint (_pageCounterView.Frame.X - size.Width * 0.5,
						                                _pageCounterView.Center.Y);
					}
				}

				public override CGSize SizeThatFits (CGSize size)
				{
					var height = _pageCounterView.Frame.Height + Spacing * 15;
					return new CGSize (size.Width, height);
				}
			}
			/********************* PDFCellViewHeader class ************************/



			static readonly int Padding = 3;

			PDFCellViewHeader _headerView;
			CustomUI.AsyncImageView image;
			bool _alignLeft;

			static nfloat CellCornerRadius {
				get {
					return BubbleView.CornerRadius - 6;
				}
			}

			public UILabel _pdfName { get; set; }

			public int PDFPageCount {
				get { return _headerView.PDFPageCount; }
				set {
					_headerView.PDFPageCount = value;
				}
			}

			public nfloat ImageWidth_SingleImageBubble { get { return ImageHeight_SingleImageBubble; } }

			public nfloat ImageHeight_SingleImageBubble {
				get {
					return GetSingleImageBubbleHeight
						(
							PdfContentViewMaxWidth
						);
				}
			}

			internal static nfloat GetSingleImageBubbleHeight (nfloat contentMaxWidth)
			{
				return contentMaxWidth - Padding * 2;
			}

			internal static nfloat GetSingleImageBubbleWidth (nfloat contentMaxWidth)
			{
				return GetSingleImageBubbleHeight (contentMaxWidth);
			}

			public bool AlignLeft {
				get { return _alignLeft; }
				set {
					if (value != _alignLeft) {
						_alignLeft = value;
						//SetNeedsLayout ();
					}
				}
			}

			public PDFCellView ()
			{
				image = new CustomUI.AsyncImageView () {
					ClipsToBounds = true
				};

				_pdfName = new UILabel () {
					Font = BlrtStyles.iOS.CellContentBoldFont,
					TextColor = BlrtStyles.iOS.Gray6,
					Lines = 2,
					LineBreakMode = UILineBreakMode.MiddleTruncation,
					Text = "Placeholder.pdf"
				};

				_headerView = new PDFCellViewHeader ();

				AddSubview (image);
				AddSubview (_headerView);
				AddSubview (_pdfName);
			}

			nfloat ImageHeight {
				get {
					return ImageHeight_SingleImageBubble;
				}
			}

			nfloat ImageWidth {
				get {
					return ImageHeight_SingleImageBubble;
				}
			}

			public nfloat Columns {
				get {
					return 1;
				}
			}

			public void SetImageAsyncSources (Uri [] CloudPaths, string [] LocalPaths)
			{
				image.Layer.CornerRadius = CellCornerRadius;

				Uri cloud = null;
				string local = null;

				if (CloudPaths != null)
					cloud = CloudPaths [0];

				if (LocalPaths != null)
					local = LocalPaths [0];

				image.SetAsyncSource (cloud, local);
				SetNeedsLayout ();
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				_pdfName.Hidden = false;
				LayoutPDFCell ();
			}

			public void LayoutPDFCell ()
			{
				// image
				var newImgRect = new CGRect (Padding, Padding, ImageWidth, ImageHeight);
				if (!image.Frame.Equals (newImgRect)) {
					image.Frame = newImgRect;
				}

				// header view
				var newHeaderViewFrame = new CGRect (Padding, 
				                                     Padding, 
				                                     ImageWidth,
				                                     _headerView.SizeThatFits (new CGSize (ImageWidth, nfloat.MaxValue)).Height) ;
				if (!_headerView.Equals (newHeaderViewFrame)) {
					_headerView.Frame = newHeaderViewFrame;
				}

				// pdf name
				var X = Padding;
				var Y = image.Frame.Bottom + Padding;
				_pdfName.SizeToFit ();
				var maxWidth = Bounds.Width - Padding * 2; // go Spacing * 0.5f points inside pagecounter frame to get more room
				var newNameRect = new CGRect (X, Y, maxWidth, _pdfName.Frame.Height);

				if (!_pdfName.Frame.Equals (newNameRect)) {
					_pdfName.Frame = newNameRect;
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var Rows = 1;
				var PadingVertical = (Rows + 2) * Padding;
				var PadingHorizontal = (Columns + 1) * Padding;
				return new CGSize (ImageWidth + PadingHorizontal,
				                   ImageHeight + PadingVertical + _pdfName.Frame.Height);
			}

			public void ReleaseImages ()
			{
				image.ReleaseImage ();
			}
		}
	}
}