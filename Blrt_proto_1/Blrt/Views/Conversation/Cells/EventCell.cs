using System;
using CoreGraphics;

using Foundation;
using UIKit;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using BlrtData.Views.Conversation;
using BlrtiOS.Views.CustomUI;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class EventCell : SlidingCellWithDateView
	{
		public const string Indentifer = ConversationCellModel.EventCellIndentifer;
		const float UnreadDotSize = 12;

		public UIEdgeInsets ContentInset { get; set; }

		EventView _messageEventView;
		UIButton _infoBtn;
		UIView _unreadDot;

		UIButton _acceptBtn, _denyBtn;

		ConversationCellModel _data;

		public EventCell (IntPtr handler) : base (handler)
		{
			InitViews ();
		}

		public EventCell () : base ()
		{
			InitViews ();
		}

		void InitViews ()
		{
			ContentInset = new UIEdgeInsets (10, 50, 10, 50);

			_infoBtn = new UIButton () {
				Hidden = true
			};
			_infoBtn.SetImage (UIImage.FromBundle ("info_event.png"), UIControlState.Normal);

			_infoBtn.TouchUpInside += (sender, e) => {
				_data.ShouldPresentInfo = !_data.ShouldPresentInfo;
				UpdateText();
			};

			_unreadDot = new UIView (new CGRect (0, 0, UnreadDotSize, UnreadDotSize)) {
				BackgroundColor = BlrtStyles.iOS.AccentRed,
				Layer = {
					CornerRadius = UnreadDotSize * 0.5f
				},
				Hidden = true
			};

			_acceptBtn = new BlrtButton () {
				Hidden = true,
				BackgroundColor = BlrtStyles.iOS.Green,
				ContentEdgeInsets = new UIEdgeInsets(3,10,3,10),
			};
			_acceptBtn.TitleLabel.Font = BlrtStyles.iOS.CellDetailFont;
			_acceptBtn.SetTitle ("Accept", UIControlState.Normal);
			_acceptBtn.SetTitleColor (BlrtStyles.iOS.Black, UIControlState.Normal);
			_acceptBtn.Layer.CornerRadius = 3;
			_acceptBtn.Layer.ShadowRadius = 0;
			_acceptBtn.Layer.ShadowOffset = new CoreGraphics.CGSize (1, 1);
			_acceptBtn.Layer.ShadowOpacity = 1;
			_acceptBtn.Layer.ShadowColor = BlrtStyles.iOS.GreenShadow.CGColor;

			_denyBtn = new BlrtButton () {
				Hidden = true,
				BackgroundColor = BlrtStyles.iOS.AccentRed,
				ContentEdgeInsets = new UIEdgeInsets(3,10,3,10)
			};
			_denyBtn.TitleLabel.Font = BlrtStyles.iOS.CellDetailFont;
			_denyBtn.SetTitle ("Deny", UIControlState.Normal);
			_denyBtn.Layer.CornerRadius = 3;
			_denyBtn.Layer.ShadowRadius = 0;
			_denyBtn.Layer.ShadowOffset = new CoreGraphics.CGSize (1, 1);
			_denyBtn.Layer.ShadowOpacity = 1;
			_denyBtn.Layer.ShadowColor = BlrtStyles.iOS.AccentRedShadow.CGColor;

			_messageEventView = new EventView ();
			ContentView.AddSubview (_messageEventView);
			ContentView.AddSubview (_infoBtn);
			ContentView.AddSubview (_acceptBtn);
			ContentView.AddSubview (_denyBtn);
			ContentView.AddSubview (_unreadDot);

			_acceptBtn.TouchUpInside += (sender, e) => {
				ProcessRequest(true);
			};

			_denyBtn.TouchUpInside += (sender, e) => {
				ProcessRequest(false);
			};

		}

		void UpdateText(){
			FormattedString text;
			if(_data.ShouldPresentInfo){
				text = new FormattedString ();
				foreach(var s in _data.FormattedText.Spans){
					text.Spans.Add (s);
				}
				text.Spans.Add (new Span () {
					Text = "\n     \n",
					ForegroundColor = BlrtStyles.BlrtGray6,
					FontSize = 3,
					FontAttributes = FontAttributes.None
				});
				var infoText = _data.FormattedInfoText;
				if (infoText != null) {
					var spans = _data.FormattedInfoText.Spans;
					foreach (var s in spans) {
						text.Spans.Add (s);
					}
				}
			}else{
				text = _data.FormattedText;
			}
			_messageEventView.SetText (text.ToAttributed (
				Font.SystemFontOfSize (11),
				BlrtStyles.BlrtGray6
			));
		}

		public async void ProcessRequest(bool accept){
			
			//confirm alert
			var alertstr = accept? "accept": "deny";
			var alert = new UIAlertView (
				BlrtUtil.PlatformHelper.Translate("process_request_confirm_"+ alertstr +"_title", "conversation_screen"),
				BlrtUtil.PlatformHelper.Translate("process_request_confirm_"+ alertstr +"_message", "conversation_screen"),
				null,
				BlrtUtil.PlatformHelper.Translate("process_request_confirm_"+ alertstr +"_cancel", "conversation_screen"),
				BlrtUtil.PlatformHelper.Translate("process_request_confirm_"+ alertstr +"_accept", "conversation_screen")
			);
			var tcs = new TaskCompletionSource<bool> ();

			alert.Clicked += (sender, e) => {
				tcs.TrySetResult(e.ButtonIndex!= alert.CancelButtonIndex);
			};
			alert.Show ();

			var confirm = await tcs.Task;
			if(!confirm){
				return;
			}

			_acceptBtn.Enabled = false;
			_denyBtn.Enabled = false;
			BlrtManager.Responder.PushLoading (true);
			var refresh = await ConversationFunctions.ProcessAccessRequest (_data.ConversationId, _data.ContentId, accept);
			if(refresh){
				if(_data.ConversationHandle.ConversationScreen is ConversationController){
					var controller = _data.ConversationHandle.ConversationScreen as ConversationController;
					await controller.Refresh ();
				}
			}else{
				_acceptBtn.Enabled = true;
				_denyBtn.Enabled = true;
			}
			BlrtManager.Responder.PopLoading (true);
		}

		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);
			_data = data;
			_messageEventView.EnableMultipleLines (data.ContentType == BlrtData.BlrtContentType.MessageEvent);
			UpdateText ();

			_infoBtn.Hidden = !data.ShowInfoButton;
			_unreadDot.Hidden = !data.Unread;

			_acceptBtn.Hidden = !data.ShowRequestButton;
			_denyBtn.Hidden = !data.ShowRequestButton;

			if(data.BackgroundColor!=null){
				this.BackgroundColor = this.ContentView.BackgroundColor = BlrtStyles.iOS.ToUIColor (data.BackgroundColor);
			}else{
				this.BackgroundColor = this.ContentView.BackgroundColor =  UIColor.Clear;
			}

			if(_data.FormattedText!=null && string.IsNullOrWhiteSpace(_data.FormattedText.ToString())){
				this.DateLabel.Hidden = true;
			}else{
				this.DateLabel.Hidden = false;
			}
		}

		public override void LayoutSubviews ()
		{
			if ((null != _data) && (null != _data.FormattedText) && string.IsNullOrWhiteSpace (_data.FormattedText.ToString ())) {
				return;
			}///don't know what this means;
			base.LayoutSubviews ();
			var frame = ContentInset.InsetRect (Bounds);
			var size = _messageEventView.SizeThatFits (new CGSize (frame.Width, nfloat.MaxValue));
			_messageEventView.Frame = new CGRect (frame.X, frame.Y, size.Width, size.Height);

			//image size is 16x16
			var firstLblSize = _messageEventView.SizeThatFitsFirstLabel (new CGSize (frame.Width, nfloat.MaxValue));
			_infoBtn.Frame = new CGRect (0, NMath.Round (NMath.Max (0, (firstLblSize.Height + 20 - 32) * 0.5f)), ContentInset.Left, 32);
			_unreadDot.Frame = new CGRect (frame.X - UnreadDotSize - 10 , 12, UnreadDotSize, UnreadDotSize);

			//position request button
			var sizeAccept = _acceptBtn.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
			var sizeDeny = _denyBtn.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
			var middlePadding = 15;
			var paddingTop = 5;

			_acceptBtn.Frame = new CGRect (_messageEventView.Frame.X, _messageEventView.Frame.Bottom + paddingTop, sizeAccept.Width, sizeAccept.Height);
			_denyBtn.Frame = new CGRect (_acceptBtn.Frame.Right + middlePadding, _messageEventView.Frame.Bottom + paddingTop, sizeAccept.Width, sizeAccept.Height);
		}

		public override CGSize SizeThatFits (CGSize size)
		{		
			if(_data.FormattedText!=null && string.IsNullOrWhiteSpace(_data.FormattedText.ToString())){
				return new CGSize (0, 0);
			}

			size.Width -= ContentInset.Left + ContentInset.Right;
			size.Height -= ContentInset.Top + ContentInset.Bottom;

			var tmp = _messageEventView.SizeThatFits (new CGSize (size.Width, nfloat.MaxValue));

			tmp.Width += ContentInset.Left + ContentInset.Right;
			tmp.Height += ContentInset.Top + ContentInset.Bottom;

			if(!_acceptBtn.Hidden){
				var sizeA = _acceptBtn.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
				var paddingTop = 8;

				tmp.Height += paddingTop + sizeA.Height;
			}

			return tmp;
		}



		public class EventView : UIView
		{
			List<MyLabel> _list;
			int _lines;

			public EventView ()
			{
				_lines = 1;
				_list = new List<MyLabel> ();
			}

			public void SetText (NSAttributedString text)
			{			
				var lines = text != null ? text.Value.Split ('\n') : new string[0];
				var start = 0;

				while (lines.Length > _list.Count)
					AddLine ();
			
				for (int i = 0; i < _list.Count; ++i) {
				
					if (i >= lines.Length) {
						_list [i].Hidden = true;
					} else {
						_list [i].Lines = _lines;
						_list [i].Hidden	= false;

						_list [i].AttributedText = text.Substring (start, lines [i].Length);
						start += lines [i].Length + 1;
					}
				}

				SetNeedsLayout ();
			}

			public void EnableMultipleLines (bool enable)
			{			
				_lines = enable ? 0 : 1;
			}

			void AddLine ()
			{
				var lbl = new MyLabel () {
					Hidden = true,
					Font = BlrtStyles.iOS.CellContentFont,
					LineBreakMode = UILineBreakMode.MiddleTruncation
				};

				_list.Add (lbl);
				AddSubview (lbl);
			}



			public class MyLabel: UILabel{
				static UIEdgeInsets inset = new UIEdgeInsets (0, 0, 0, 5);
				public override void DrawText (CGRect rect)
				{
					base.DrawText (inset.InsetRect(rect));
				}

				public override CGSize SizeThatFits (CGSize size)
				{
					size.Width -= 5;
					var newSize = base.SizeThatFits (size);
					newSize.Width += 5;
					return newSize;
				}
			}


			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				nfloat y = 0;

				for (int i = 0; i < _list.Count; ++i) {
				
					if (!_list [i].Hidden) {
						var size = _list [i].SizeThatFits (Bounds.Size);

						_list [i].Frame = new CGRect (0, y, Bounds.Width, size.Height);
						y = _list [i].Frame.Bottom;
					}
				}
			}

			public CGSize SizeThatFitsFirstLabel (CGSize size)
			{
				float y = 0;

				for (int i = 0; i < _list.Count; ++i) {

					if (!_list [i].Hidden) {
						var s = _list [i].SizeThatFits (size);
						return new CGSize (size.Width, s.Height);
					}
				}
				return new CGSize (size.Width, y);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				nfloat y = 0;

				for (int i = 0; i < _list.Count; ++i) {

					if (!_list [i].Hidden) {
						var s = _list [i].SizeThatFits (size);
						y += s.Height;
					}
				}
				return new CGSize (size.Width, y);
			}
		}
	}
}

