﻿using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using System.Linq;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class ImageCell : BubbleCell
	{
		public const string Indentifer = ConversationCellModel.ImageCellIndentifer;
		public static readonly int MAX_IMAGES = 30;
		static readonly UIEdgeInsets bubblePadding = new UIEdgeInsets (4, 4, 4, 4);
		static readonly nfloat IMAGE_CELL_MAX_WIDTH = BubbleMaxWidth - 40;
		protected static readonly nfloat ImageContentViewMaxWidth = IMAGE_CELL_MAX_WIDTH -
															   bubblePadding.Left -
															   bubblePadding.Right -
															   BubbleView.AligmentPadding;

		bool IsMediaDataCorrupt { get; set; }

		public static CGSize SingleImageSize {
			get {
				return new CGSize (ImageCellView.GetSingleImageBubbleWidth (ImageContentViewMaxWidth),
								   ImageCellView.GetSingleImageBubbleHeight (ImageContentViewMaxWidth));
			}
		}

		public static CGSize MultiImageSize {
			get {
				return new CGSize (ImageCellView.GetMultiImageBubbleWidth (ImageContentViewMaxWidth),
								   ImageCellView.GetMultiImageBubbleHeight (ImageContentViewMaxWidth));
			}
		}

		ImageCellView _view;

		public ImageCell (IntPtr handler) : base (handler)
		{
			SetBubbleView (BubbleViewFactory.CreateMediaBubbleView ());
			InitMediaCellViews ();
		}
		public ImageCell ()
		{
			SetBubbleView (BubbleViewFactory.CreateMediaBubbleView ());
			InitMediaCellViews ();
		}

		public void InitMediaCellViews ()
		{
			base.InitViews ();

			BubblePadding = bubblePadding;
			BubbleContentView = _view = new ImageCellView ();
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
			_view.ReleaseImages ();
		}

		public override void Bind (ConversationCellModel data)
		{
 			base.Bind (data);

			var mediaType = data.MediaType;

			IsMediaDataCorrupt = data.MediaNames.Length == 0;
			SettingsView.Hidden = IsMediaDataCorrupt ? true : !data.OnParse ;

			_view.AlignLeft = !data.IsAuthor;
			_view.SetImageAsyncSources (data.CloudPathThumbnails, data.PathThumbnails);

			if (data.IsAuthor) {
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
			} else {
				BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
			}
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetBackgroundColor ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetBackgroundColor ();
		}

		void SetBackgroundColor ()
		{
			if (Selected || (Highlighted && !IsMediaDataCorrupt)) {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlueHighlight;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.CharcoalHighlight;
				}
			} else {
				if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Gray8;
				}
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var height = (BubbleMargin.Bottom + BubbleMargin.Top);
			var width = size.Width;

			if (!AuthorLbl.Hidden)
				height += AuthorLblHeight + HeaderHeight;

			if (_showFooter)
				height += FooterHeight;

			var bubbleWidth = NMath.Min (width - (BubbleMargin.Left + BubbleMargin.Right), BubbleMaxWidth);
			var bubble = BubbleView.SizeThatFits (new CGSize (bubbleWidth, nfloat.MaxValue));

			return new CGSize (
				bubble.Width,
				bubble.Height + height
			);
		}

		class ImageCellView : UIView
		{
			static readonly int ImagePadding = 3;
			static readonly int MaxImagesInRow = 3;

			CustomUI.AsyncImageView [] images;
			bool _alignLeft;

			public int ImageCount { get; set; }

			public nfloat ImageWidth_SingleImageBubble { get { return ImageHeight_SingleImageBubble; } }
			public nfloat ImageWidth_MultiImageBubble { get { return ImageHeight_MultiImageBubble; } } // Images in multi-image buggle have square shape.

			int MaxImagesInColumn { get { return MAX_IMAGES; } }

			public nfloat ImageHeight_SingleImageBubble {
				get {
					return GetSingleImageBubbleHeight
						(
							ImageContentViewMaxWidth
						);
				}
			}
			public nfloat ImageHeight_MultiImageBubble {
				get {
					return GetMultiImageBubbleHeight
						(
							ImageContentViewMaxWidth
						);
				}
			}

			internal static nfloat GetSingleImageBubbleHeight (nfloat contentMaxWidth)
			{
				return contentMaxWidth - ImagePadding * 2;
			}

			internal static nfloat GetSingleImageBubbleWidth (nfloat contentMaxWidth)
			{
				return GetSingleImageBubbleHeight (contentMaxWidth);
			}

			internal static nfloat GetMultiImageBubbleHeight (nfloat contentMaxWidth)
			{
				return (contentMaxWidth - ImagePadding * (MaxImagesInRow + 1)) / 3;
			}

			internal static nfloat GetMultiImageBubbleWidth (nfloat contentMaxWidth)
			{
				return GetMultiImageBubbleHeight (contentMaxWidth);
			}

			public bool AlignLeft {
				get { return _alignLeft; }
				set {
					if (value != _alignLeft) {
						_alignLeft = value;
					}
				}
			}

			public ImageCellView ()
			{
				images = new CustomUI.AsyncImageView [MAX_IMAGES];
				for (int i = 0; i < MAX_IMAGES; i++) {
					images [i] = new CustomUI.AsyncImageView () {
						ClipsToBounds = true
					};
				}

				AddSubviews (images);
			}

			public int Rows {
				get {
					return (int)NMath.Ceiling ((nfloat)ImageCount / (nfloat)MaxImagesInRow);
				}
			}

			nfloat ImageHeight {
				get {
					return ImageCount == 1 ? ImageHeight_SingleImageBubble : ImageHeight_MultiImageBubble;
				}
			}

			nfloat ImageWidth {
				get {
					return ImageCount == 1 ? ImageWidth_SingleImageBubble : ImageWidth_MultiImageBubble;
				}
			}

			public nfloat Height {
				get {
					return Rows * ImageHeight;
				}
			}

			public nfloat Columns {
				get {
					return Rows > 1 ? MaxImagesInRow : ImageCount;
				}
			}

			public nfloat Width {
				get {
					return Columns == MaxImagesInRow || Columns == 1  ? ImageWidth_SingleImageBubble : ImageCount * ImageWidth;
				}
			}

			public void SetImageAsyncSources (Uri [] CloudPaths, string [] LocalPaths)
			{
				var cloudPathCount = CloudPaths != null ? CloudPaths.Length : 0;
				var localPathCount = LocalPaths != null ? LocalPaths.Length : 0;
				ImageCount = localPathCount == 0 ? 1 : localPathCount;

				images [0].Layer.CornerRadius = ImageCount == 1 ? BubbleView.CornerRadius - 6 : 0;

				for (int i = 0; i < ImageCount; i++) {
					Uri cloud = null;
					string local = null;

					if (CloudPaths != null && cloudPathCount > 0)
						cloud = CloudPaths [i];

					if (LocalPaths != null && localPathCount > 0)
						local = LocalPaths [i];

					images [i].SetAsyncSource (cloud, local);
				}
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				LayoutImageCell ();
			}

			public void LayoutImageCell ()
			{
				var imgCount = 0;

				if (!AlignLeft) {
					var width = Bounds.Width;

					nfloat X = width - ImageWidth - ImagePadding;
					nfloat Y = ImagePadding;

					for (int row = 0; row < MaxImagesInColumn; ++row) {
						for (int column = 0; column < MaxImagesInRow; ++column) {

							if (imgCount == ImageCount)
								return;

							var newImgFrame = new CGRect (X, Y, ImageWidth, ImageHeight);
							if (!newImgFrame.Equals (images [imgCount].Frame)) {
								images [imgCount].Frame = newImgFrame;
							}
							imgCount++;

							X -= ImageWidth + ImagePadding + ImagePadding;
						}
						X = width - ImageWidth - ImagePadding;
						Y += ImageHeight + ImagePadding;
					}
				} else {
					nfloat X = ImagePadding;
					nfloat Y = ImagePadding;

					for (int row = 0; row < MaxImagesInColumn; ++row) {
						for (int column = 0; column < MaxImagesInRow; ++column) {

							if (imgCount == ImageCount)
								return;

							var newImgFrame = new CGRect (X, Y, ImageWidth, ImageHeight);
							if (!newImgFrame.Equals (images [imgCount].Frame)) {
								images [imgCount].Frame = newImgFrame;
							}
							imgCount++;

							X += ImageWidth + ImagePadding + ImagePadding;
						}
						X = ImagePadding;
						Y += ImageHeight + ImagePadding;
					}
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var PadingVertical = (Rows + 1) * ImagePadding;
				var PadingHorizontal = (Columns + 1) * ImagePadding;

				return new CGSize (Width + PadingHorizontal,
								  Height + PadingVertical);
			}

			public void ReleaseImages ()
			{
				for (int i = 0; i < ImageCount; i++) {
					images [i].ReleaseImage ();
				}
			}
		}
	}
}