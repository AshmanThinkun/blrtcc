﻿using System;
using UIKit;
using CoreGraphics;
using BlrtData.Views.Helpers;
using BlrtData.Models.ConversationScreen;
using BlrtiOS.Views.Util;

namespace BlrtiOS
{
	public class SlidingCellWithDateView : Views.Conversation.Cells.SlidingCell, IBindable<ConversationCellModel>, ITableInfoRequester
	{
		public const int DateCenterOffset = MaxWidth / 2;

		ConversationCellModel _model;
		public UILabel DateLabel { get; private set; }

		public SlidingCellWithDateView (IntPtr handler) : base (handler) { InitSlidingViews (); }
		public SlidingCellWithDateView () : base () { InitSlidingViews (); }

		void InitSlidingViews ()
		{
			DateLabel = new UILabel () {
				Frame = new CGRect (ContentView.Bounds.Width + Padding, (ContentView.Bounds.Height - 100) * 0.5f, Width, 100),
				AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin,
				TextColor = BlrtStyles.iOS.Gray5,
				LineBreakMode = UILineBreakMode.WordWrap,
				Font = BlrtStyles.iOS.CellDetailFont,
				Lines = 0
			};

			ContentView.AddSubview (DateLabel);
		}

		public virtual void Bind (ConversationCellModel model)
		{
			_model = model;

			DateLabel.Text = BlrtHelperiOS.TranslateDateLong (_model.CreatedAt);

			CGSize size = DateLabel.SizeThatFits (new CGSize (Width, nfloat.MaxValue));

			DateLabel.Frame = new CGRect (ContentView.Bounds.Width + Padding, (int)((ContentView.Bounds.Height - size.Height) * 0.5f), Width, size.Height);
		}

		ITableInfo _infoSrc = null;
		int _section = -1;
		int _row = -1;
		public void SetInfoSource (ITableInfo src, int section, int row)
		{
			_infoSrc = src;
			_section = section;
			_row = row;
		}
	}
}
