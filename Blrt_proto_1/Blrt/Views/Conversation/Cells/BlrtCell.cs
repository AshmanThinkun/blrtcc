using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class BlrtCell : BubbleCell
	{
		public const string Indentifer = ConversationCellModel.BlrtCellIndentifer;
		public static readonly float BLRT_VIEW_CORNER_RADIUS = 17;

		BlrtView _blrtView;

		PublicBlrtImageIconsView _shareImageArea;

		public BlrtCell (IntPtr handler) : base (handler){
			SetBubbleView (BubbleViewFactory.CreateBlrtBubbleView ());
			InitBlrtCellViews ();
		}

		public BlrtCell () { 
			SetBubbleView (BubbleViewFactory.CreateBlrtBubbleView ());
			InitBlrtCellViews ();
		}
			
		void InitBlrtCellViews ()
		{
			base.InitViews ();

			BubblePadding = new UIEdgeInsets (2, 2, 2, 2);
			BubbleContentView = _blrtView = new BlrtView ();

			PublicNameLbl = new UILabel () {
				Font = BlrtStyles.iOS.CellDetailBoldFont,
				TextColor = BlrtStyles.iOS.Gray5,
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap
			};


			_shareImageArea = new PublicBlrtImageIconsView (this);
			_shareImageArea.SetImage (UIImage.FromBundle ("public_share.png"), UIControlState.Normal);

			ContentView.AddSubviews (new UIView[] {
				PublicNameLbl,
				_shareImageArea
			});
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
			_blrtView.ReleaseImage ();
		}


		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);

			_blrtView.Image.SetAsyncSource (data.CloudPathThumbnails[0], data.PathThumbnails[0]);
			_blrtView.BlrtLength			= data.Text;
		
			_blrtView.AlignLeft				= !data.IsAuthor;

			_blrtView.SubText				= data.IsPublicBlrt? "Public Blrt":"";

			if(!data.IsPublicBlrt && !string.IsNullOrEmpty(data.PublicName)){
				_blrtView.SubText = "Public Blrt pending";
			}

			SettingsView.Hidden				= !data.OnParse;

			if (data.IsPublicBlrt){
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentRed;
			} else if (data.IsAuthor) {
				BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
			} else {
				BubbleView.BubbleColor = BlrtStyles.iOS.Charcoal;
			}

			_blrtView.DarkColorScheme = false;
			_blrtView.BlrtPageCount = data.PageCount;

			if (data.IsPublicBlrt) {
				var text = new NSMutableAttributedString ();
				text.Append (new NSAttributedString (BlrtUtil.PlatformHelper.Translate ("Public name "), new UIStringAttributes () {
					Font = BlrtStyles.iOS.CellDetailBoldFont,
					ForegroundColor = BlrtStyles.iOS.Gray5
				}));
				text.Append (new NSAttributedString (_model.PublicName, new UIStringAttributes () {
					Font = BlrtStyles.iOS.CellDetailFont,
					ForegroundColor = BlrtStyles.iOS.Black
				}));
				PublicNameLbl.AttributedText = text;
			}

			
			PublicNameLbl.Hidden = !data.IsPublicBlrt;
			_shareImageArea.Hidden = !data.IsPublicBlrt;
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
			SetBackgroundColor ();
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);
			SetBackgroundColor ();
		}

		void SetBackgroundColor ()
		{
			if (Selected || Highlighted) {
				if(_model!=null &&_model.IsPublicBlrt){
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentRedHighlight;
				}else if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlueHighlight;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.CharcoalHighlight;
				}
			} else {
				if(_model!=null &&_model.IsPublicBlrt){
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentRed;
				}else if (!AlignLeft) {
					BubbleView.BubbleColor = BlrtStyles.iOS.AccentBlue;
				} else {
					BubbleView.BubbleColor = BlrtStyles.iOS.Charcoal;
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			var LabelLeftPadding = AlignLeft ? 16 : 10;
			var rect = BubbleView.Frame;

			if(!PublicNameLbl.Hidden){
				var nameSize = PublicNameLbl.SizeThatFits (new CGSize (rect.Size.Width - 10, nfloat.MaxValue));
				var ButtonMargin = 4;

				PublicNameLbl.Frame = new CGRect(rect.X + LabelLeftPadding, rect.Bottom  + BubbleMargin.Bottom , nameSize.Width, nameSize.Height);
				_shareImageArea.Frame = new CGRect(PublicNameLbl.Frame.Left - 2, PublicNameLbl.Frame.Bottom + BubbleMargin.Bottom, 184, PublicIconAreaSize.Height);
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var height = (BubbleMargin.Bottom + BubbleMargin.Top);
			var width = size.Width;

			if (!AuthorLbl.Hidden)
				height += AuthorLblHeight + HeaderHeight;
			if (_showFooter)
				height += FooterHeight;

			var bubbleWidth = NMath.Min(width - (BubbleMargin.Left + BubbleMargin.Right), BubbleMaxWidth);
			var bubble = BubbleView.SizeThatFits (new CGSize (bubbleWidth, nfloat.MaxValue));

			if (!PublicNameLbl.Hidden){
				var nameSize = PublicNameLbl.SizeThatFits (new CGSize (bubbleWidth - 10, nfloat.MaxValue));
				height += nameSize.Height;
			}

			if(!_shareImageArea.Hidden){
				height += PublicIconAreaSize.Height + BubbleMargin.Bottom*2;
			}

			return new CGSize (
				width,
				NMath.Max(bubble.Height, _unreadDot.Frame.Height + UserThumbnail.Frame.Height + 1) + height
			);
		}


		class PublicBlrtImageIconsView: UIButton
		{
			BlrtCell _parent;
			public PublicBlrtImageIconsView(BlrtCell parent):base(){
				_parent = parent;
				this.AdjustsImageWhenHighlighted = false;
			}

			public override void TouchesEnded (NSSet touches, UIEvent evt)
			{
				base.TouchesEnded (touches, evt);

				var t = touches.AnyObject as UITouch;
				var p = t.LocationInView (this);

				if (p.Y < 0 || p.Y > PublicIconAreaSize.Height || p.X<0 || p.X>PublicIconAreaSize.Width)
					return;

				if (p.X > 184 - 32 -3)
					_parent.EmbedSharePressed (this, EventArgs.Empty);
				else if(p.X > 184- 32*2 - 3*3)
					_parent.MessageSharePressed (this, EventArgs.Empty);
				else if(p.X > 184- 32*3 - 3*5)
					_parent.EmailSharePressed (this, EventArgs.Empty);
				else if(p.X > 184- 32*4 - 3*7)
					_parent.TwitterSharePressed (this, EventArgs.Empty);
				else if(p.X > 184- 32*5 -3*9)
					_parent.FBSharePressed (this, EventArgs.Empty);
			}
		}

		class BlrtView : UIView 
		{
			const float ThumbnailSize = 60;
			static float PlayImageWidth = 9.0f * BlrtStyles.iOS.ScreenScaleFactor;

			UILabel _timeLbl;
			CustomUI.AsyncImageView _thumbnail;
			UIImageView _playButton;
			PageCounterView _pageCounterView;
			UILabel _subTextLbl;

			bool _alignLeft;

			public CustomUI.AsyncImageView Image	{ get { return _thumbnail; } }
			public string BlrtLength				{ get { return _timeLbl.Text; } set { _timeLbl.Text = value; } }

			private int blrtPageCount;

			public bool DarkColorScheme {
				set { _pageCounterView.DarkColorScheme = value;}
			}

			public int BlrtPageCount { 
				get { return blrtPageCount; } 
				set { 
					_pageCounterView.SetPages(value); 
					blrtPageCount = value; 
				} 
			}

			public bool AlignLeft 					{ get { return _alignLeft; } 
				set { 
					if(value!=_alignLeft){
						_alignLeft = value;  
						SetNeedsLayout ();
					}
				} 
			}
			public string SubText					{ get{return _subTextLbl.Text;} 
				set{
					if(_subTextLbl.Text!=value){
						_subTextLbl.Text = value; 
						SetNeedsLayout ();
					}
				}
			}

			public static float PlayImageWidth1 {
				get {
					return PlayImageWidth;
				}
			}

			public BlrtView(){
			
				Frame = new CGRect(0, 0, 320, ThumbnailSize + THUMBNAIL_MARGIN * 2);

				_thumbnail = new CustomUI.AsyncImageView() {
					Frame = new CGRect(0,0,ThumbnailSize,ThumbnailSize),
					ClipsToBounds = true
				};
				_thumbnail.Layer.CornerRadius = BLRT_VIEW_CORNER_RADIUS - 3;
				_playButton = new UIImageView(UIImage.FromBundle("blrt_play.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)) {
					ContentMode = UIViewContentMode.Center,
					TintColor	= BlrtStyles.iOS.White
				};


				_timeLbl = new UILabel(){
					Font		= BlrtStyles.iOS.HugeFont,
					TextColor	= BlrtStyles.iOS.White
				};

				_subTextLbl = new UILabel(){
					Font = BlrtStyles.iOS.CellDetailFont,
					TextColor = BlrtStyles.iOS.White
				};

				_pageCounterView = new PageCounterView();

				AddSubview(_thumbnail);
				AddSubview(_playButton);
				AddSubview(_timeLbl);
				AddSubview(_pageCounterView);
				AddSubview(_subTextLbl);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				size.Height = ThumbnailSize + THUMBNAIL_MARGIN * 2;
				return size;
			}

			const int MARGIN = 15;
			const int THUMBNAIL_MARGIN = 2;
			const int TIME_LBL_MARGIN = 4;

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var textSize = _subTextLbl.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));

				if (AlignLeft) {
					_pageCounterView.Center = new CGPoint (_pageCounterView.Bounds.Width * 0.5f, Bounds.Height * 0.5f);
					_thumbnail.Frame = new CGRect (Bounds.Width - ThumbnailSize - THUMBNAIL_MARGIN, THUMBNAIL_MARGIN, ThumbnailSize, ThumbnailSize);
					_playButton.Frame = new CGRect (_pageCounterView.Frame.Right, 0, PlayImageWidth1, ThumbnailSize);

				} else {
					_pageCounterView.Center = new CGPoint (Bounds.Width - _pageCounterView.Bounds.Width * 0.5f, Bounds.Height * 0.5f);
					_thumbnail.Frame = new CGRect (THUMBNAIL_MARGIN, THUMBNAIL_MARGIN, ThumbnailSize, ThumbnailSize);
					_playButton.Frame = new CGRect (_thumbnail.Frame.Right+ MARGIN, 0, PlayImageWidth1, ThumbnailSize);

				}

				var width = Bounds.Width - (_thumbnail.Frame.Width + _playButton.Frame.Width + _pageCounterView.Bounds.Width + TIME_LBL_MARGIN);

				_timeLbl.Frame = new CGRect(_playButton.Frame.Right + TIME_LBL_MARGIN, 0, width, ThumbnailSize);

				_subTextLbl.Frame = new CGRect (_playButton.Frame.Left + 8, 2, textSize.Width, textSize.Height);

			}

			public void ReleaseImage ()
			{
				_thumbnail.ReleaseImage ();
			}
		}
	}
}

