using System;
using CoreGraphics;

using Foundation;
using UIKit;
using System.Collections.Generic;

using Xamarin.Forms.Platform.iOS;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using BlrtData.Views.Conversation;
using BlrtiOS.Views.CustomUI;
using System.Threading.Tasks;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class SendFailureCell : SlidingCellWithDateView	
	{
		public const string Indentifer = ConversationCellModel.SendFailureCellIndentifer;
		const float UnreadDotSize = 12;

		public UIEdgeInsets ContentInset { get; set; }

		BlrtiOS.Views.Conversation.Cells.EventCell.EventView _messageEventView;
		UIActivityIndicatorView _loader;
		UIImageView _imgView;
		UILabel _infoView;

		ConversationCellModel _data;

		public SendFailureCell (IntPtr handler) : base (handler)
		{
			InitViews ();
		}

		public SendFailureCell () : base ()
		{
			InitViews ();
		}

		void InitViews ()
		{
			ContentInset = new UIEdgeInsets (10, 25, 10, 50); 

			_messageEventView = new BlrtiOS.Views.Conversation.Cells.EventCell.EventView ();
			_loader = new UIActivityIndicatorView (){
				Frame = new CGRect(0,0,IconSize,IconSize),
				Hidden = true,
				Color = BlrtStyles.iOS.Charcoal,
			};
			_imgView = new UIImageView () {
				Image = UIImage.FromBundle ("small_send_fail.png"),
				Frame = new CGRect(0,0,IconSize,IconSize),
				ContentMode = UIViewContentMode.ScaleAspectFit,
				Hidden = true
			};

			_infoView = new UILabel {
				Lines = 0,
				LineBreakMode = UILineBreakMode.WordWrap
			};

			ContentView.AddSubview (_messageEventView);
			ContentView.AddSubview (_loader);
			ContentView.AddSubview (_imgView);
			ContentView.AddSubview (_infoView);
		}

		public override void Bind (ConversationCellModel data)
		{
			base.Bind (data);
			_data = data;
			_messageEventView.EnableMultipleLines (false);

			InvokeOnMainThread (() => {
				if(_data.FormattedText.ToString() == BlrtData.Conversation.ActivityStatus.InProgress.ToString ()){
					_messageEventView.SetText (new Xamarin.Forms.FormattedString(){
						Spans = {
							new Xamarin.Forms.Span(){
								Text = "Sending"
							}
						}
					}.ToAttributed(BlrtStyles.CellTitleBoldFont , BlrtStyles.BlrtGray6) );
					_loader.Hidden = false;
					_loader.StartAnimating ();
					_imgView.Hidden = true;
					_infoView.Hidden = true;
				}else{
					_messageEventView.SetText (new Xamarin.Forms.FormattedString(){
						Spans = {
							new Xamarin.Forms.Span(){
								Text = "Sending failed"
							}
						}
					}.ToAttributed(BlrtStyles.CellContentFont , BlrtStyles.BlrtAccentRed) );
					_infoView.AttributedText = data.FormattedInfoText.ToAttributed (BlrtStyles.CellContentFont, BlrtStyles.BlrtGray7);
					_loader.Hidden = true;
					_loader.StopAnimating ();
					_imgView.Hidden = false;
					_infoView.Hidden = false;
				}
			});
		}


		public const int IconSize = 30;
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			var frame = ContentInset.InsetRect (Bounds);

			var size = _messageEventView.SizeThatFits (new CGSize (frame.Width-IconSize, nfloat.MaxValue));
			_messageEventView.Frame = new CGRect (frame.X+IconSize, frame.Y, size.Width, size.Height);


			_loader.Frame = new CGRect (frame.X, frame.Y, IconSize, size.Height);
			_imgView.Frame = _loader.Frame;

			if (!_infoView.Hidden) {
				size = _infoView.SizeThatFits (new CGSize (frame.Width - _imgView.Frame.Width, int.MaxValue));
				_infoView.Frame = new CGRect (_messageEventView.Frame.X, _messageEventView.Frame.Bottom, size.Width, size.Height);
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{		
			if(_data.FormattedText!=null && string.IsNullOrWhiteSpace(_data.FormattedText.ToString())){
				return new CGSize (0, 0);
			}

			size.Width -= ContentInset.Left + ContentInset.Right;
			size.Height -= ContentInset.Top + ContentInset.Bottom;

			size.Width -= IconSize;

			var tmp = _messageEventView.SizeThatFits (new CGSize (size.Width, nfloat.MaxValue));

			tmp.Width += ContentInset.Left + ContentInset.Right + IconSize;
			tmp.Height += ContentInset.Top + ContentInset.Bottom;

			if (!_infoView.Hidden) {
				size = _infoView.SizeThatFits (size);
				tmp.Height += size.Height;
				tmp.Width = (System.nfloat)Math.Max (tmp.Width, _infoView.Frame.Width);
			}

			return tmp;
		}

	}
}

