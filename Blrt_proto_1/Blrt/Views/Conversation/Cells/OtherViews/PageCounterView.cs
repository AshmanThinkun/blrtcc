using System;
using UIKit;
using Mono;
using Foundation;

namespace BlrtiOS.Views.Conversation
{
	public class PageCounterView : UIView
	{
		private const int Padding = 5;
		protected UILabel _lbl;
		protected UIImageView _img;

		public bool DarkColorScheme {
			set {
				//var oldSingleImage = _singleImg;
				if (value) {
					_lbl.TextColor = BlrtStyles.iOS.Black;
					_img.TintColor = BlrtStyles.iOS.Black;
					_singleImg?.Dispose ();
					_multiImg?.Dispose ();
					_singleImg = UIImage.FromBundle ("page_single_dark.png");
					_multiImg = UIImage.FromBundle ("page_dark.png");
				} else {
					_lbl.TextColor = BlrtStyles.iOS.White;
					_img.TintColor = BlrtStyles.iOS.White;
					_singleImg?.Dispose ();
					_multiImg?.Dispose ();
					_singleImg = UIImage.FromBundle ("page_single_white.png");
					_multiImg = UIImage.FromBundle ("page_white.png");
				}

				//if (_singleImg != oldSingleImage) {
				//	SetPages (int.Parse(_lbl.Text), true);
				//}
			}
		}

		UIImage _singleImg, _multiImg;
		public PageCounterView (bool forPDFCell = false)
		{
			Bounds = new CoreGraphics.CGRect (0, 0, 50, 26);
			BackgroundColor = UIColor.Clear;
			Opaque = false;

			_lbl = new UILabel () {
				Font = UIFont.SystemFontOfSize (forPDFCell ? 12: 10)
			};

			_singleImg = forPDFCell ? UIImage.FromBundle ("page_single_white.png") : UIImage.FromBundle ("page_single_dark.png");
			_multiImg = forPDFCell ? UIImage.FromBundle ("page_white.png") : UIImage.FromBundle ("page_dark.png");

			_img = new UIImageView (_singleImg) {
				TintColor = BlrtStyles.iOS.White
			};

			AddSubview (_lbl);
			AddSubview (_img);
		}

		public override void LayoutSubviews ()
		{
			int offset = 3;
			base.LayoutSubviews ();

			_img.Center = new CoreGraphics.CGPoint (Bounds.Width * 0.5f, Bounds.Height * 0.5f);

			var size = _lbl.SizeThatFits (new CoreGraphics.CGSize (Bounds.Width, 128));

			size.Height = NMath.Min (size.Height, Bounds.Height);

			if (_lbl.Text == "1")
				_lbl.Frame = new CoreGraphics.CGRect ((Bounds.Width - size.Width) * 0.5f, (Bounds.Height - size.Height) * 0.5f, size.Width, size.Height);
			else
				_lbl.Frame = new CoreGraphics.CGRect ((Bounds.Width - size.Width) * 0.5f + offset, (Bounds.Height - size.Height) * 0.5f + offset, size.Width, size.Height);
		}

		public void SetPages (int pageNumber, bool forced = false)
		{
			if (_lbl.Text != pageNumber.ToString () || forced){
				_lbl.Text = pageNumber.ToString ();
				//_img.Image?.Dispose ();
				if (pageNumber == 1) {
					_img.Image = _singleImg;
				} else {
					_img.Image = _multiImg;
				}
				_img.SizeToFit ();
				SetNeedsLayout ();
			}
		}
	}
}

