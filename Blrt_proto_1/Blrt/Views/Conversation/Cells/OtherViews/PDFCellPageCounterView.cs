﻿using System;
using BlrtiOS.Views.Conversation;

namespace BlrtiOS
{
	public class PDFCellPageCounterView : PageCounterView
	{
		public PDFCellPageCounterView () : base(true)
		{
			_lbl.TextColor = BlrtStyles.iOS.White;
			_img.TintColor = BlrtStyles.iOS.White;

		}
	}
}
