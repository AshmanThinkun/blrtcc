using System;
using Foundation;
using UIKit;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class UserThumbnailView : UIView
	{
		UILabel _intials;

		public string Text {
			get { return _intials.Text; }
			set {
				if (_intials.Text != value) {
					_intials.Text = (value ?? "").ToUpper ();
				}
			}
		}

		public UIFont Font {
			get {
				return _intials.Font;
			}
			set {
				_intials.Font = value;
			}
		}

		string _imagePath;
		public string ImagePath {
			get {
				return _imagePath;
			}
			set {
				if (value != _imagePath) {
					_imagePath = value;
					if (System.IO.File.Exists (_imagePath)) {
						_avatarImg = UIImage.FromFile (_imagePath);
						_avatarView.Image = _avatarImg;
						_avatarView.Hidden = false;
						_intials.Hidden = true;
					} else {
						_avatarView.Hidden = true;
						_intials.Hidden = false;
					}
				}
			}
		}

		public event EventHandler Clicked;

		UIImage _avatarImg;
		UIImageView _avatarView;

		UIView _mask;
		public UserThumbnailView ()
		{

			BackgroundColor = BlrtStyles.iOS.Gray4;

			_intials = new UILabel (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Font = BlrtStyles.iOS.CellDetailBoldFont,
				TextAlignment = UITextAlignment.Center,
				TextColor = BlrtStyles.iOS.White,
			};

			_avatarView = new UIImageView (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				ContentMode = UIViewContentMode.ScaleAspectFill,
				ClipsToBounds = true,
				Hidden = true
			};
			_mask = new UIView (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				BackgroundColor = UIColor.Black,
				ClipsToBounds = true,
				Alpha = 0
			};

			AddSubview (_intials);
			AddSubview (_avatarView);
			AddSubview (_mask);

			this.AddGestureRecognizer (new UITapGestureRecognizer (() => {
				Clicked?.Invoke (this, EventArgs.Empty);
			}) {
				ShouldRecognizeSimultaneously = new UIGesturesProbe ((a, b) => true)
			});
			SetNeedsLayout ();
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			if (Clicked != null) {
				_mask.Alpha = 0.5f;
			}
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			if (Clicked != null) {
				_mask.Alpha = 0;
			}
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			if (Clicked != null) {
				_mask.Alpha = 0;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			Layer.CornerRadius = NMath.Min (Bounds.Width, Bounds.Height) * 0.5f;
			_avatarView.Layer.CornerRadius = Bounds.Width * 0.5f;
			_mask.Layer.CornerRadius = Bounds.Width * 0.5f;
		}
	}

}