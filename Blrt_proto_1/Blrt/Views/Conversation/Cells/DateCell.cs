﻿using System;
using CoreGraphics;

using Foundation;
using UIKit;
using BlrtData.Models.ConversationScreen;
using BlrtData;
using BlrtData.Views.Helpers;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class DateCell : SlidingCell, IBindable<ConversationCellModel>
	{
		public const string Indentifer = ConversationCellModel.DateCellIndentifer;

		static readonly int DATE_VIEW_CELL_CORNER_RADIUS = 4;

		static readonly int BOTTOM_MARGIN = 18;
		static readonly int TOP_MARGIN = 8;

		DateView _dateView;

		public DateCell (IntPtr handler) : base (handler)
		{
			InitDateCellViews ();
		}

		public DateCell ()
		{
			InitDateCellViews ();
		}

		void InitDateCellViews ()
		{
			_dateView = new DateView () {
				BackgroundColor = BlrtStyles.iOS.Gray2
			};
			_dateView.Layer.CornerRadius = DATE_VIEW_CELL_CORNER_RADIUS;

			ContentView.AddSubview (_dateView);
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (false, false);
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (false, false);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			var size = _dateView.SizeThatFits (Bounds.Size);
			var X = Bounds.GetMidX () - size.Width * 0.5;
			_dateView.Frame = new CGRect (X, Bounds.Y + TOP_MARGIN, size.Width , size.Height);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			var height = 0.0;
			var width = size.Width;
			var dateLabelSize = _dateView.SizeThatFits(new CGSize(nfloat.MaxValue, size.Height));

			height += dateLabelSize.Height + TOP_MARGIN + BOTTOM_MARGIN;
			width += dateLabelSize.Width;

			return new CGSize (width, height);
		}

		public void Bind (ConversationCellModel model)
		{
			_dateView.SetDate (model.CreatedAt);
		}

		class DateView : UIView
		{
			static readonly UIEdgeInsets paddings = new UIEdgeInsets(8, 16, 8, 16);

			UILabel DateLabel { get; set;}

			public DateView ()
			{
				DateLabel = new UILabel () {
					Lines = 1,
					TextColor = BlrtStyles.iOS.Gray7,
					Font = BlrtStyles.iOS.FontSize14,
					TextAlignment = UITextAlignment.Center
				};

				AddSubview (DateLabel);
			}

			public void SetDate (DateTime newDate)
			{
				DateLabel.Text = GetDateString (newDate);
				DateLabel.SizeToFit ();
			}

			string GetDateString (DateTime newDate)
			{
				return BlrtUtil.GetDateString (newDate);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var dateLabelSize = DateLabel.SizeThatFits (size);
				var height = dateLabelSize.Height + paddings.Top + paddings.Bottom;
				var width = dateLabelSize.Width + paddings.Left + paddings.Right;

				return new CGSize (width, height);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var newCenterPoint = new CGPoint (Bounds.GetMidX (), Bounds.GetMidY ());
				
				if (!DateLabel.Center.Equals (newCenterPoint)) {
					DateLabel.Center = newCenterPoint;
				}
			}
		}
	}
}

