using System;
using CoreGraphics;

using Foundation;
using UIKit;
using System.Collections.Generic;
using BlrtData.Models.ConversationScreen;
using BlrtData.Views.Helpers;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class MissingContentCell : UITableViewCell, IBindable<ConversationCellModel>
	{
		public const string Indentifer =  ConversationCellModel.MissingContentCellIndentifer;
		public const int spacing = 1;

		const int UPGRADE_BTN_BOTTOM_MARGIN = 30;
		const int UPGRADE_BTN_TOP_MARGIN = 15;


		UIActivityIndicatorView _indicator;
		UILabel			_label;
		UIButton 		_upgradeButton;

		bool? _loading;

		public bool IsLoading {
			get {
				return _loading ?? false;
			}
			set{
				if (_loading != value) {
					_loading = value;
					if(!IsLoading)
						_label.Text = BlrtHelperiOS.Translate("missing_content_action", "conversation_screen");
					else
						_label.Text = BlrtHelperiOS.Translate("missing_content_loading", "conversation_screen");
					_indicator.Hidden = !value;
				}

				if (IsLoading) {
					_label.TextColor = BlrtStyles.iOS.Gray4;
					_indicator.StartAnimating ();
				} else {
					_label.TextColor = BlrtStyles.iOS.AccentBlue;
					_indicator.StopAnimating ();
				}
			}
		}

		public UIEdgeInsets ContentInset { get; set; }

		/// <summary>
		/// Text used when upgrade cell needs to be shown
		/// </summary>
		/// <value>The upgrade cell text.</value>
		string UpgradeCellText { get {
				return string.Format (BlrtHelperiOS.Translate ("upgrade_cell_title", "upgrade"), 
				                      BlrtData.BlrtSettings.ContentViewDays);
			} 
		}

		public MissingContentCell (IntPtr handler) : base (handler){ InitViews(); }
		public MissingContentCell () : base() { InitViews(); }


		void InitViews ()
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			ContentInset = new UIEdgeInsets (10, 8, 10, 8);

			_indicator = new UIActivityIndicatorView ();
			_indicator.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			_indicator.HidesWhenStopped = false;

			_label = new UILabel ();
			_label.Lines = 2;
			_label.Font = UIFont.SystemFontOfSize (14);
			_label.Text = BlrtHelperiOS.Translate("load_missing_content", "conversation_screen");
			_label.TextColor = BlrtStyles.iOS.Gray5;

			_upgradeButton = new UIButton (UIButtonType.RoundedRect) {
				BackgroundColor = BlrtStyles.iOS.Charcoal,
				Font = BlrtStyles.iOS.CellDetailFont,
				ContentEdgeInsets = new UIEdgeInsets(10, 12, 10, 12)
			};
			_upgradeButton.SetTitle ("Upgrade to premium", UIControlState.Normal);
			_upgradeButton.SetTitleColor (BlrtStyles.iOS.GreenHighlight, UIControlState.Normal);
			_upgradeButton.SizeToFit ();
			_upgradeButton.Layer.CornerRadius = 2;
			_upgradeButton.Hidden = true;

			_upgradeButton.TouchUpInside += delegate {
				BlrtData.BlrtManager.App.OpenUpgradeAccount (BlrtData.ExecutionPipeline.Executor.System ());
			};

			//ContentView.BackgroundColor = BlrtStyles.iOS.Gray2;
			AddSubview(_indicator);
			AddSubview(_label);
			AddSubview (_upgradeButton);
		}

		public void Bind (ConversationCellModel data)
		{
			IsLoading = data.CloudState == BlrtData.CloudState.DownloadingContent;

			if (data.ContentType == BlrtData.BlrtContentType.MissingOlderContent) {
				_label.Text = UpgradeCellText;
				_upgradeButton.Hidden = false;
				ContentView.BackgroundColor = BlrtStyles.iOS.White;
			} else {
				_label.Text = BlrtHelperiOS.Translate ("missing_content_loading", "conversation_screen");
				_upgradeButton.Hidden = true;
				ContentView.BackgroundColor = BlrtStyles.iOS.Gray2;
			}
		}

		public override void SetHighlighted (bool highlighted, bool animated)
		{
			base.SetHighlighted (highlighted, animated);

			if(highlighted)
				BackgroundColor = BlrtStyles.iOS.Gray3;
			else
				BackgroundColor = BlrtStyles.iOS.Gray2;

		}

		public override void SetSelected (bool selected, bool animated)
		{
			base.SetSelected (selected, animated);
//			_label.Selected = Selected;
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (!IsLoading) {
				var rect = ContentInset.InsetRect (Bounds);
				var size = _label.SizeThatFits (rect.Size);
				_label.Frame = new CGRect (
					rect.X + (rect.Width - size.Width) * 0.5f,
					rect.Y + ContentInset.Top,
					size.Width,
					size.Height
				);
			}else{
				var rect = ContentInset.InsetRect (Bounds);
				var size = _label.SizeThatFits (rect.Size);

				_indicator.Center = new CGPoint( rect.X + rect.Width * 0.5f, rect.Y + (rect.Height - spacing - 2*size.Height)*0.5f + size.Height*0.5f);

				_label.Frame = new CGRect (
					rect.X + (rect.Width - size.Width) * 0.5f,
					rect.Y + _indicator.Frame.Bottom+ spacing,
					size.Width,
					size.Height
				);
			}

			if (!_upgradeButton.Hidden) {
				var size = _upgradeButton.SizeThatFits (Bounds.Size);
				_upgradeButton.Frame = new CGRect (
					Bounds.GetMidX() - size.Width * 0.5f,
					Bounds.Bottom - UPGRADE_BTN_BOTTOM_MARGIN - size.Height,
					size.Width,
					size.Height
				);
			}
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			size.Width -= ContentInset.Left + ContentInset.Right;
			size.Height -= ContentInset.Top + ContentInset.Bottom;

			size = _label.SizeThatFits (size);

			if(IsLoading){
				size.Height += size.Height + spacing;
			}

			size.Width += ContentInset.Left + ContentInset.Right;
			size.Height += ContentInset.Top + ContentInset.Bottom;

			if (!_upgradeButton.Hidden) {
				size.Height += _upgradeButton.Bounds.Height;
				size.Height += UPGRADE_BTN_BOTTOM_MARGIN + UPGRADE_BTN_TOP_MARGIN;
			}

			return size;
		}
	}
}

