using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.Views.Conversation.Cells
{
	public class SlidingCell : UITableViewCell
	{
		nfloat _slide;

		public const int Padding = 10;
		public const int Width = 120;
		public const int MaxWidth = Width + Padding * 2;
		public const int StartOffset = 20;

		public SlidingCell (IntPtr handler) : base (handler){
			SelectionStyle = UITableViewCellSelectionStyle.None;
		}
		public SlidingCell () : base() {
			SelectionStyle = UITableViewCellSelectionStyle.None;
		}

		public override void PrepareForReuse ()
		{
			base.PrepareForReuse ();
			SetSlide(0, false);
		}

		public void SetSlide (nfloat slide, bool animated)
		{
			_slide = slide;

			if (animated) {
				UIView.Animate(0.2, () => {
					PositionSlider ();
				});
			} else {
				PositionSlider ();
			}
		}

		public virtual void PositionSlider(){
			ContentView.Frame = new CGRect (_slide, 0, Bounds.Width, Bounds.Height);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			PositionSlider ();
		}
	}
}

