using System;
using UIKit;
using BlrtiOS.Views.CustomUI;
using CoreGraphics;
using CoreAnimation;

namespace BlrtiOS.Views.Conversation
{
	public class ControlButtonsView : UIView
	{
		const string BLRT_REPLY_BTN_TITLE = " Reply";
		const string NEW_BLRT_BTN_TITLE = "New ";

		UIButton _reblrt;
		UIButton _addPeople;

		public event EventHandler ReBlrt;
		public event EventHandler AddPeople;

		readonly float ANIMATION_DURATION = ConversationController.ANIMATION_DURATION;

		UIColor PrimaryColor {
			get {
				return BlrtStyles.iOS.WhiteAlpha.ColorWithAlpha (0.0f);
			}
		}

		UIColor SecondaryColor {
			get {
				return BlrtStyles.iOS.White;
			}
		}

		CAGradientLayer ColorGradient { 
			get {
				return new CAGradientLayer () {
					Colors = new CoreGraphics.CGColor [] { PrimaryColor.CGColor, SecondaryColor.CGColor },
					StartPoint = new CoreGraphics.CGPoint(1.0f, 0.0f),
					EndPoint = new CoreGraphics.CGPoint(1.0f, 0.5f)
				};
			}
		}

		public override bool Hidden {
			get { return !_reblrt.Enabled; }
			set {
				Animate (ANIMATION_DURATION, delegate {
					_reblrt.Enabled = _addPeople.Enabled = !value;
					_reblrt.Alpha = _addPeople.Alpha = value ? 0.0f : 1.0f;
					(Layer.Sublayers [0] as CAGradientLayer).StartPoint = value ? new CoreGraphics.CGPoint (1.0f, 0.65f) : new CoreGraphics.CGPoint (1.0f, 0.0f);
					(Layer.Sublayers [0] as CAGradientLayer).EndPoint = value ? new CoreGraphics.CGPoint (1.0f, 1.0f) : new CoreGraphics.CGPoint (1.0f, 0.5f);
				});
			}
		}

		public void SetBlrtButton (bool conversationHasBlrt)
		{
			InvokeOnMainThread (delegate {
				if (conversationHasBlrt) {
					if (_reblrt.Title(UIControlState.Normal) == BLRT_REPLY_BTN_TITLE)
						return;
					
					_reblrt.SetTitle (BLRT_REPLY_BTN_TITLE, UIControlState.Normal);
					_reblrt.TitleEdgeInsets = _reblrt.ImageEdgeInsets = new UIEdgeInsets (0, 0, 0, 0);
				} else {
					if (_reblrt.Title (UIControlState.Normal) == NEW_BLRT_BTN_TITLE)
						return;
					
					_reblrt.SetTitle (NEW_BLRT_BTN_TITLE, UIControlState.Normal);
					_reblrt.TitleEdgeInsets = new UIEdgeInsets (0, -_reblrt.ImageView.Frame.Width, 0, _reblrt.ImageView.Frame.Width);
					_reblrt.ImageEdgeInsets = new UIEdgeInsets (0, _reblrt.TitleLabel.Frame.Width, 0, -_reblrt.TitleLabel.Frame.Width);
				}
				_reblrt.SizeToFit ();
			});
		}

		public ControlButtonsView () : base()
		{
			BackgroundColor = PrimaryColor;
			Layer.InsertSublayer (ColorGradient, 0);
			_reblrt = NewBlrtButton.CreateNewBlrt (null, NewBlrtButton.ColorScheme.GreenWithBlack, true);
			_reblrt.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;
			_reblrt.TouchUpInside += (object sender, EventArgs e) => {
				if (ReBlrt != null) {

					ReBlrt (this, null);
				}
			};

			_addPeople = NewBlrtButton.SendButton (null);
			_addPeople.AutoresizingMask = UIViewAutoresizing.FlexibleMargins ^ UIViewAutoresizing.FlexibleBottomMargin;
			_addPeople.TouchUpInside += (object sender, EventArgs e) => {
				if (AddPeople != null) {
					AddPeople (this, null);
				}
			};

			AddSubviews (_reblrt, _addPeople);
		}

		const int buttonPadding = 20;
		public override void LayoutSubviews () 
		{
			base.LayoutSubviews ();

			Layer.Sublayers [0].Frame = Bounds; // Frame for gradient layer

			var reblrtFrame = _reblrt.Frame;
			var sendFrame = _addPeople.Frame;

			_reblrt.Frame = new CGRect ((Bounds.Width - reblrtFrame.Width - sendFrame.Width - buttonPadding) * 0.5f, Bounds.Bottom - 50, reblrtFrame.Width, reblrtFrame.Height);
			_addPeople.Frame = new CGRect (_reblrt.Frame.Right + buttonPadding, _reblrt.Frame.Y, sendFrame.Width, sendFrame.Height);
		}

		internal void UpdateButtonsTintColor (bool isConversationReadonly)
		{
			_addPeople.UserInteractionEnabled = _reblrt.UserInteractionEnabled = !isConversationReadonly;
			_addPeople.BackgroundColor = _reblrt.BackgroundColor = isConversationReadonly ? BlrtStyles.iOS.Gray4 : BlrtStyles.iOS.GreenHighlight;
			_reblrt.Layer.ShadowColor = _addPeople.Layer.ShadowColor = isConversationReadonly ? BlrtStyles.iOS.Gray6.CGColor : BlrtStyles.iOS.GreenShadow.CGColor;
		}
	}
}
