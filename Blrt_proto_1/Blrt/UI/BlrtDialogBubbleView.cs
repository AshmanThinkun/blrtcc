using System;
using CoreGraphics;
using UIKit;
using CoreAnimation;

namespace BlrtiOS.UI
{
	public class BlrtDialogBubbleView : UIView
	{
		private UIColor _borderColor;
		private UIColor _bubbleBackgroundColor;
		private float _tailWidth;
		private bool _tailLeft;


		public UIColor BubbleColor {
			get { return _bubbleBackgroundColor; }
			set {
				if (value != _bubbleBackgroundColor) {
					_bubbleBackgroundColor = value;
					SetNeedsDisplay ();
				}
			}
		}
		//public UIColor BorderColor{ get{ return _borderColor; } set{ _borderColor = value; LayoutSubviews ();} }
		//public float BorderWidth{ get{ return _borderWidth; } set{ _borderWidth = value; LayoutSubviews ();} }
		public float TailWidth {
			get { return _tailWidth; }
			set {
				if (value != _tailWidth) {
					_tailWidth = value;
					SetNeedsDisplay ();
				}
			}
		}

		public bool TailLeft {
			get { return _tailLeft; }
			set {
				if (value != _tailLeft) {
					_tailLeft = value;
					SetNeedsDisplay ();
				}
			}
		}

		public override CGRect Frame {
			get { return base.Frame; }
			set {
				if (Frame.Y != value.Y || Frame.X != value.X || Frame.Width != value.Width || Frame.Height != value.Height) {
					base.Frame = value;
					SetNeedsDisplay ();
				} else
					base.Frame = value;
			}
		}

		public BlrtDialogBubbleView (CGRect frame) : base (frame)
		{
			_tailWidth = 5;

			_borderColor = null;

			_bubbleBackgroundColor = UIColor.Green;
			BackgroundColor = UIColor.Clear;
		}

		public override void Draw (CGRect rect)
		{
			BackgroundColor = UIColor.Clear;
			base.Draw (rect);

			using (CGContext gctx = UIGraphics.GetCurrentContext ()) {

				var shape = BlrtiOS.Util.BlrtUtilities.CreateMessageBubbleWithQuote (Bounds, BlrtHelperiOS.IsPhone ? 16 : 16, _tailWidth, _tailLeft);


				gctx.SetFillColor (_bubbleBackgroundColor.CGColor);

				if (_borderColor != null)
					gctx.SetStrokeColor (_borderColor.CGColor);
				else
					gctx.SetStrokeColor (new CGColor (0, 0.5f));

				gctx.SetLineWidth (2);

				gctx.AddPath (shape.CGPath);
				gctx.DrawPath (CGPathDrawingMode.Fill);	
		
			}
		}
	}
}

