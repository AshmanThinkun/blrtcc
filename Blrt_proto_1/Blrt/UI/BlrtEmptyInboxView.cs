using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtEmptyInboxView : UIView
	{
		private const float TopPadding   = 25f;
		private const float RightPadding = 30f;

		private UILabel _titleLbl;
		private UILabel _subtitleLbl;
		private UIImageView _image;

		public string TitleText {
			get{return _titleLbl != null ? _titleLbl.Text : "";}
			set{
				if(_titleLbl != null && value != _titleLbl.Text){
					_titleLbl.Text = value;
					SetNeedsLayout ();
				}
			}
		}

		public string SubtitleText {
			get{return _subtitleLbl!= null ? _subtitleLbl.Text : "";}
			set{
				if(_subtitleLbl != null && value != _subtitleLbl.Text){
					_subtitleLbl.Text = value;
					SetNeedsLayout ();
				}
			}
		}

		public BlrtEmptyInboxView () : base()
		{
			AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;


			if (BlrtHelperiOS.IsPhone) {

				_image = new UIImageView (UIImage.FromBundle("inbox-empty-iphone.png")){
					ContentMode = UIViewContentMode.ScaleAspectFit
				};
				AddSubview (_image);


			} else {
				_subtitleLbl = new UILabel (){
					Font = BlrtConfig.LightFont.SubTitle,
					TextColor = BlrtConfig.BlrtBlack,
					Lines = 1,
					TextAlignment = UITextAlignment.Center
				};
				_titleLbl = new UILabel (){
					Font = BlrtConfig.LightFont.Title,
					TextColor = BlrtConfig.BLRT_RED,
					Lines = 1,
					TextAlignment = UITextAlignment.Center
				};
				_image = new UIImageView (UIImage.FromBundle("mock-arrow.png")){
					ContentMode = UIViewContentMode.ScaleAspectFit
				};

				AddSubview (_image);
				AddSubview (_titleLbl);
				AddSubview (_subtitleLbl);


				var size = CalulateLayout (new CGSize (320, 320), false);
				Frame = new CGRect (CGPoint.Empty, size);
			}
		}

		public override CGSize SystemLayoutSizeFittingSize (CGSize size)
		{
			return CalulateLayout (size, false);
		}

		public override void SizeToFit ()
		{
			base.SizeToFit ();
			var f = CalulateLayout (Bounds.Size, true);
			Frame = new CGRect (Frame.Location, f);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			return CalulateLayout (size, false);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			CalulateLayout (Bounds.Size, true);
		}

		CGSize CalulateLayout (CGSize size, bool set)
		{
			// WHY?
			/*
			if (size.Height <= 0)
				size.Height = 320;
			*/

			var imageSize = _image.Image.Size;

			if (set) {
				if (BlrtHelperiOS.IsPhone) {
					_image.Frame = new CGRect (new CGPoint ((int)(size.Width - imageSize.Width) / 2, TopPadding), imageSize);
				} else {
					const float TopToLabelDistPct = 0.615f;
					const float LeftToLabelDistPct = 0.07f;
					const float LabelWidthPct = 0.631f;

					const float GAP_BETWEEN_LABELS = 8;

					_image.Frame = new CGRect (new CGPoint (size.Width - imageSize.Width - RightPadding, TopPadding), imageSize);

					nfloat labelX = (int)NMath.Ceiling (_image.Frame.X + imageSize.Width * LeftToLabelDistPct);
					nfloat labelY = (int)NMath.Ceiling (_image.Frame.Y + imageSize.Height * TopToLabelDistPct);
					nfloat labelWidth = (int)NMath.Ceiling (imageSize.Width * LabelWidthPct);

					var titleSize = _titleLbl.SizeThatFits (new CGSize (labelWidth, size.Height));
					var subtitleSize = _subtitleLbl.SizeThatFits (new CGSize (labelWidth, size.Height));

					_subtitleLbl.Frame = new CGRect (labelX, labelY, labelWidth, subtitleSize.Height);
					_titleLbl.Frame = new CGRect (labelX, _subtitleLbl.Frame.Bottom + GAP_BETWEEN_LABELS, labelWidth, titleSize.Height);
				}
			}

			size = new CGSize (size.Width, imageSize.Height + TopPadding);

			return size;
		}
	}
}

