using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Threading.Tasks;

namespace BlrtiOS.UI
{
	public class TaskAwaitAlertView : UIAlertView
	{
		UIActivityIndicatorView _activity;
		Task _task;
		TaskCompletionSource<bool> _tsc;

		public TaskAwaitAlertView (string title, string message, string cancel, Task task) : base (title, message, null, cancel)
		{
			_task = task;
			_tsc = new TaskCompletionSource<bool> ();

			_activity = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				AutoresizingMask	= UIViewAutoresizing.FlexibleMargins,
				Frame				= new CGRect(0,0,32,32)
			};
			_activity.Frame = new CGRect (new CGPoint (240, 10), _activity.Frame.Size);
			SetValueForKey (_activity, new NSString ("accessoryView"));

			Clicked += (object sender, UIButtonEventArgs e) => {
				if (e.ButtonIndex == CancelButtonIndex)
					Cancel ();
			};
		}

		public override void Show ()
		{
			_activity.StartAnimating ();
			base.Show ();
		}

		private void Cancel () {
			_tsc.SetResult (false);
			_activity.StopAnimating ();
		}

		public async Task<bool> WaitAsync () {

			await Task.WhenAny (_tsc.Task, _task);
			_tsc.TrySetResult(true);
			return await _tsc.Task;
		} 
	}
}

