using System;
using CoreGraphics;
using UIKit;
using System.IO;
using BlrtData.IO;
using BlrtData;

namespace BlrtiOS.UI
{
	public class BlrtAsyncImageView : UIImageView
	{
		private const int DefaultPriority = 15;


		private UIImage _placeholder;

		public UIImage Placeholder {
			get { return _placeholder; }
			set {
				_placeholder = value;


				if (State == AsyncImageState.Placeholder)
					Image = _placeholder;
			}
		}


		public enum AsyncImageState { Placeholder, Live };

		public AsyncImageState State { get; private set; }


		BlrtDownloader _downloader;

		public BlrtAsyncImageView (CGRect? frame = null, string url = null, UIImage placeholder = null, int priority = DefaultPriority)
			: base (frame ?? CGRect.Empty)
		{
			State = AsyncImageState.Placeholder;
			BackgroundColor = BlrtConfig.BLRT_MID_GRAY;

			if (placeholder != null)
				Placeholder = placeholder;
			else
				Placeholder = UIImage.FromBundle ("logo-placeholder.png");



			if (url != null)
				FromUrl (url, priority);
		}


		public void FromUrl (string url, int priority = DefaultPriority, bool forcePlaceholder = true)
		{
			string path = url;

			foreach (var character in Path.GetInvalidFileNameChars ())
				path = path.Replace (character, '_');


			path = BlrtConstants.Directories.Default.GetThumbnailPath (path);

			FromUrl (url, path, priority, forcePlaceholder);
		}

		public void FromUrl (string url, string path, int priority = DefaultPriority, bool forcePlaceholder = true)
		{
			FromUrl ( new Uri (url), path, priority, forcePlaceholder);
		}

		public void FromUrl (Uri url, string path, int priority = DefaultPriority, bool forcePlaceholder = true)
		{
			Image = UIImage.FromFile (path);

			if (Image != null) {
				return;
			}


			if (forcePlaceholder) {
				State = AsyncImageState.Placeholder;
				Image = Placeholder;
			}

			if (url != null && !string.IsNullOrEmpty (path)) {

				_downloader = BlrtDownloader.Download (priority, url, path);
				_downloader.OnDownloadFinished += DownloadingFinished;
			}
		}

		void DownloadingFinished(object sender, EventArgs e){

			if (sender == _downloader) {
				State = AsyncImageState.Live;

				if (!_downloader.Failed) {
					InvokeOnMainThread (() => {
						Image = UIImage.FromFile (_downloader.ToFile);
					});
				}
			}
		
		}
	}
}

