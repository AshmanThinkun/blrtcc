using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;

using BlrtData.IO;


namespace BlrtiOS.UI
{
	public class BlrtProgressAlertView : UIAlertView
	{
		private UIProgressView _progress;


		public BlrtProgressAlertView(string title, string message, UIAlertViewDelegate alertViewDelegate,  string cancel) : base(title, message, alertViewDelegate, cancel)
		{

			_progress = new UIProgressView(UIProgressViewStyle.Bar )
			{
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Frame = new CGRect(20, 20, 240, 15),
				Progress = 0,
				TrackTintColor = UIColor.FromRGBA(0,0,0,0.05f)
			};
			SetValueForKey(_progress, new NSString("accessoryView"));
		}

		public void SetProgress(float value, bool animated){
			_progress.SetProgress (value, true);
		}
	}




	// Not used in new 1.6.0 code
	public class BlrtDownloadAlertView : BlrtProgressAlertView
	{
		private BlrtDownloaderList _downloader;

		public event EventHandler Cancelled;
		public event EventHandler Failed;
		public event EventHandler Finished;


		public BlrtDownloadAlertView(string title, string message, string cancel) : base(title, message, null, cancel)
		{

			Clicked += (object sender, UIButtonEventArgs e) => {
				if(e.ButtonIndex == CancelButtonIndex)
					CancelDownload();
			};
		}



		public void StartDownloading(BlrtDownloaderList downloader)
		{
			_downloader = downloader;

			if (_downloader != null) {
				_downloader.OnFinished 		+= DownloadFinished;
				_downloader.OnProgress 		+= DownloadProgress;
				_downloader.OnFailed 		+= DownloadFailed;

				SetProgress (_downloader.Progress, false);


				if (_downloader.IsFinished)
					DownloadFinished (_downloader, null);
				if (_downloader.IsFailed)
					DownloadFailed (_downloader, null);
			}
		}

		public void CancelDownload()
		{
			if (_downloader == null)
				return;

			var d = _downloader;

			NullDownloader ();
		
			if (Cancelled != null)
				Cancelled (d, EventArgs.Empty);
		}

		private void NullDownloader(){
		
			if (_downloader != null) {

				_downloader.OnFinished -= DownloadFinished;
				_downloader.OnProgress -= DownloadProgress;
				_downloader.OnFailed -= DownloadFailed;
				_downloader = null;
			}
		
		}


		private void DownloadProgress(object sender, EventArgs args){


			if (_downloader == sender && _downloader != null) {
				InvokeOnMainThread (() => {
					SetProgress (_downloader != null ? _downloader.Progress : 1, true);
				});
			}
		}

		private void DownloadFailed(object sender, EventArgs args){
			if (_downloader == sender && _downloader != null) {
				var d = _downloader;
				NullDownloader ();

				InvokeOnMainThread (() => {

					if (Failed != null)
						Failed (d, EventArgs.Empty);

				});
			}
		}
		private void DownloadFinished(object sender, EventArgs args){

			if (_downloader == sender && _downloader != null) {
				var d = _downloader;
				NullDownloader ();

				InvokeOnMainThread (() => {
					SetProgress (1, true);

					if (Finished  != null)
						Finished (d, EventArgs.Empty);
				});
			}
		} 
	}
}

