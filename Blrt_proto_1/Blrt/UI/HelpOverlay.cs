﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.Collections.Generic;
using Xamarin.Forms;
using BlrtData;

namespace BlrtiOS.UI
{
	public class HelpOverlay:UIView
	{
		UIButton _confirmBtn;

		public event EventHandler ConfirmButtonTapped;

		public string Title {
			get { return _instructionView.Title; }
			set {
				_instructionView.Title = value;
				SetNeedsLayout ();
			}
		}

		public List<InstructionRowModel> InstructionsList {
			set {
				_instructionView.SetInstructionsList (value);
				SetNeedsLayout ();
			}
		}

		public string ConfirmButtonTitle {
			get {
				return _confirmBtn.Title (UIControlState.Normal);
			}
			set {
				_confirmBtn.SetTitle (value, UIControlState.Normal);
				SetNeedsLayout ();
			}
		}

		static readonly float DefaultRowHeight = Device.Idiom == TargetIdiom.Phone? 50: 70;
		static readonly float DefaultRowWidth = Device.Idiom == TargetIdiom.Phone? 280: 400;
		static readonly float ImageSectionWidth = Device.Idiom == TargetIdiom.Phone? 100: 130;
		static readonly float Padding = Device.Idiom == TargetIdiom.Phone? 12: 20;
		static readonly float PaddingForRowWithoutImage = Device.Idiom == TargetIdiom.Phone ? 18 : 24;
		static readonly float MarginForRowWithoutImage = 0;
		static readonly float Margin = 10;
		static float ConfirmButtonBottomSpacing = Device.Idiom == TargetIdiom.Phone? 20: 100;


		InstructionView _instructionView;

		public HelpOverlay (string confirmBtnTtile)
		{
			BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0.8f);
			AutoresizingMask = UIViewAutoresizing.All;
			Alpha = 0;
			Layer.ZPosition = nfloat.MaxValue;


			if(Device.Idiom == TargetIdiom.Phone){
				if(Math.Max(UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height)> 480){
					ConfirmButtonBottomSpacing = 40;
				}
			}

			_confirmBtn = new UIButton () {
				BackgroundColor = BlrtStyles.iOS.Green,
				ContentEdgeInsets = new UIEdgeInsets (10, 40, 10, 40),
				Font = Device.Idiom == TargetIdiom.Phone? BlrtStyles.iOS.CellTitleFont: BlrtStyles.iOS.HugeFont
			};
			_confirmBtn.Layer.CornerRadius = 5;
			_confirmBtn.SetTitle (confirmBtnTtile, UIControlState.Normal);
			_confirmBtn.SetTitleColor (BlrtStyles.iOS.Black, UIControlState.Normal);
			_confirmBtn.TouchUpInside += (sender, e) => {
				ConfirmButtonTapped?.Invoke (sender, e);
			};

			_instructionView = new InstructionView () {
				BackgroundColor = BlrtStyles.iOS.White
			};

			AddSubview (_confirmBtn);
			AddSubview (_instructionView);
		}

		public void Open(bool animate = true){
			this.Hidden = false;
			this.ConfirmButtonTapped += HelpOverlay_ConfirmButtonTapped;
			this.Frame = UIApplication.SharedApplication.Windows [0].Frame;
			UIApplication.SharedApplication.Windows [0].AddSubview( this);  //TODO: ios7 can't listen to the screen rotation event.
			var btnSize = _confirmBtn.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
			_confirmBtn.Frame = new CGRect ((Bounds.Width - btnSize.Width) * 0.5f, Bounds.Height+100, btnSize.Width, btnSize.Height);

			if (animate) {
				UIView.Animate (0.8f, () => {
					this.Alpha = 1f;
					_confirmBtn.Frame = new CGRect ((Bounds.Width - btnSize.Width) * 0.5f, Bounds.Height - btnSize.Height - ConfirmButtonBottomSpacing, btnSize.Width, btnSize.Height);
				});
			}else{
				this.Alpha = 1f;
				_confirmBtn.Frame = new CGRect ((Bounds.Width - btnSize.Width) * 0.5f, Bounds.Height - btnSize.Height - ConfirmButtonBottomSpacing, btnSize.Width, btnSize.Height);
			}
		}

		void HelpOverlay_ConfirmButtonTapped (object sender, EventArgs e)
		{
			UIView.Animate (0.3f, () => {
				this.Alpha = 0f;
			},()=>{
				this.RemoveFromSuperview();
			});
		}

		public void ClearEventHandler(){
			this.ConfirmButtonTapped -= HelpOverlay_ConfirmButtonTapped;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var btnSize = _confirmBtn.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
			_confirmBtn.Frame = new CGRect ((Bounds.Width - btnSize.Width) * 0.5f, Bounds.Height - btnSize.Height - ConfirmButtonBottomSpacing, btnSize.Width, btnSize.Height);


			var instructionSize = _instructionView.SizeThatFits (new CGSize (DefaultRowWidth, nfloat.MaxValue));
			_instructionView.Frame = new CGRect ((Bounds.Width - instructionSize.Width) * 0.5f, (Bounds.Height - instructionSize.Height) * 0.5f, instructionSize.Width, instructionSize.Height+20);

			if(_instructionView.Frame.Bottom> _confirmBtn.Frame.Top - ConfirmButtonBottomSpacing){
				var insFrame = _instructionView.Frame;
				insFrame.Y = Bounds.Height - insFrame.Height - _confirmBtn.Frame.Height - ConfirmButtonBottomSpacing*2;
				if(insFrame.Y< ConfirmButtonBottomSpacing){
					insFrame.Y = (insFrame.Y + ConfirmButtonBottomSpacing) * 0.5f;
				}
				if(insFrame.Y<0) {
					UIDevice.CurrentDevice.SetValueForKey (new NSNumber((int)UIDeviceOrientation.Portrait), new NSString ("orientation"));
				}

				_instructionView.Frame = insFrame;
			}
		}

		class InstructionView: UIView
		{
			public string Title { 
				get {
					return _titleLbl.Text;
				}set {
					_titleLbl.Text = value;
				}
			}

			public List<InstructionRowModel> _instructionsList;

			public void SetInstructionsList (List<InstructionRowModel> list)
			{
				_instructionsList = list;
				var instructionListCount = _instructionsList.Count;

				var rowToCreate = instructionListCount - _instructionRowList.Count;
				for (int i = 0; i < rowToCreate; i++) {
					var row = new InstructionRow ();
					_instructionRowList.Add (row);
				}

				foreach (var a in _instructionRowList) {
					if (a.Superview != null)
						a.RemoveFromSuperview ();
				}

				for (int i = 0; i < instructionListCount; i++) {
					var rowModel = _instructionsList [i];
					var row = _instructionRowList [i];

					row.Title = rowModel.Title;
					row.LeftAlign = rowModel.LeftAlign;
					row.SetImage (rowModel.Image);

					var separatorHidden = (i == instructionListCount - 1) ? true : rowModel.SeparatorHidden;
					row.SetSeparator (separatorHidden);
					AddSubview (row);
				}
			}


			UILabel _titleLbl;
			List<InstructionRow> _instructionRowList;

			public InstructionView () : base ()
			{
				Layer.CornerRadius = 10;
				ClipsToBounds = true;
				Layer.BorderColor = BlrtStyles.iOS.Black.CGColor;

				_titleLbl = new TitleLabel () {
					TextAlignment = UITextAlignment.Center,
					Font = Device.Idiom == TargetIdiom.Phone? BlrtStyles.iOS.CellTitleFont: UIFont.SystemFontOfSize(23),
					BackgroundColor = BlrtStyles.iOS.Green,
					Lines = 0,
					LineBreakMode = UILineBreakMode.WordWrap,
				};
				AddSubview (_titleLbl);

				_instructionRowList = new List<InstructionRow> ();
				_instructionsList = new List<InstructionRowModel> ();
			}


			class TitleLabel: UILabel
			{
				public override void DrawText (CGRect rect)
				{
					var inset = new UIEdgeInsets (0, 10, 0, 10);
					base.DrawText (inset.InsetRect (rect));
				}
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				var titleSize = _titleLbl.SizeThatFits (new CGSize (Bounds.Width, nfloat.MaxValue));
				titleSize.Height += 16;
				_titleLbl.Frame = new CGRect (0, 0, Bounds.Width, Math.Max (DefaultRowHeight, titleSize.Height));

				var top = _titleLbl.Frame.Bottom;
				foreach (var a in _instructionRowList) {
					if (a.Superview == null)
						continue;
					a.Frame = new CGRect (0, top, Bounds.Width, a.SizeThatFits (new CGSize (Bounds.Width, nfloat.MaxValue)).Height);
					top = a.Frame.Bottom;
				}
			}


			public override CGSize SizeThatFits (CGSize size)
			{
				var titleSize = _titleLbl.SizeThatFits (new CGSize (size.Width, nfloat.MaxValue));
				titleSize.Height += 16;
				var top = Math.Max (DefaultRowHeight, titleSize.Height);
				foreach (var a in _instructionRowList) {
					if (a.Superview == null)
						continue;
					top += a.SizeThatFits (new CGSize (size.Width, nfloat.MaxValue)).Height;
				}

				return new CGSize (size.Width, top);
			}
		}



		public class InstructionRowModel
		{
			public string Image{ get; set; }

			public string Title{ get; set; }

			public bool LeftAlign{get;set;}

			public bool SeparatorHidden { get; set;}
		}

		class InstructionRow: UIView
		{
			public string Title {
				get {
					return _label.Text;
				}set {
					_label.Text = value;
				}
			}

			public void SetImage (string image)
			{
				if (string.IsNullOrEmpty (image))
					_imgView.Image = null;
				else
					_imgView.Image = UIImage.FromBundle (image);
			}

			public void SetSeparator (bool hidden)
			{
				if (hidden) {
					return;
				}

				_sep = new UIView ()
				{
					BackgroundColor = BlrtStyles.iOS.Gray3,
				};
				AddSubview (_sep);
			}

			public bool LeftAlign{get;set;}

			UIImageView _imgView;
			UILabel _label;
			UIView _sep;

			public InstructionRow () : base ()
			{
				_imgView = new UIImageView ();

				_label = new UILabel () {
					Lines = 0,
					LineBreakMode = UILineBreakMode.WordWrap,
					Font = Device.Idiom == TargetIdiom.Phone? BlrtStyles.iOS.CellContentFont: UIFont.SystemFontOfSize(20)
				};

				AddSubviews (_imgView, _label);
			}

			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();

				if (_imgView.Image != null) {
					var imgSize = _imgView.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue));
					if(LeftAlign)
						_imgView.Frame = new CGRect (Padding, (Bounds.Height - imgSize.Height) * 0.5f, imgSize.Width, imgSize.Height);
					else
						_imgView.Frame = new CGRect ((ImageSectionWidth- imgSize.Width)*0.5f , (Bounds.Height - imgSize.Height) * 0.5f, imgSize.Width, imgSize.Height);
				} else {
					_imgView.Frame = CGRect.Empty;
				}

				nfloat labelX = PaddingForRowWithoutImage;
				nfloat labelY = PaddingForRowWithoutImage;
				nfloat labelWidth = Bounds.Width;
				nfloat vMargin = MarginForRowWithoutImage;

				CGSize labelSize;

				if (_imgView.Image != null) {
					labelWidth -= ImageSectionWidth + Padding;
					labelX = ImageSectionWidth;
					labelY = 0;
					vMargin = Margin;
				} else {
					labelWidth -= PaddingForRowWithoutImage * 2;
					_label.TextAlignment = UITextAlignment.Center;
				}

				labelSize = _label.SizeThatFits (new CGSize (labelWidth, nfloat.MaxValue));
				labelSize.Height += vMargin;

				_label.Frame = new CGRect (labelX, labelY, labelWidth, Math.Max (DefaultRowHeight, labelSize.Height));


				if (_sep != null) {
					_sep.Frame = new CGRect (ImageSectionWidth, Bounds.Height, Bounds.Width - ImageSectionWidth - 5, 0.5f);
				}
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				var vPadding = 10;
				var labelSize = _label.SizeThatFits (new CGSize (size.Width - ImageSectionWidth - Padding, nfloat.MaxValue));
				var imgHeight = _imgView.SizeThatFits (new CGSize (nfloat.MaxValue, nfloat.MaxValue)).Height + vPadding * 0.5;

				labelSize.Height += vPadding;

				var height = Math.Max (labelSize.Height, imgHeight);
				return new CGSize (size.Width, Math.Max (height, DefaultRowHeight));
			}

	}
	}
}

