using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtButtonAndLabel : UIView
	{
		UILabel _lbl;
		BlrtActionButton _btn;


		public EventHandler Click { get; set; }

		public BlrtButtonAndLabel () : base (new CGRect (0, 0, 0, 0))
		{
			_lbl = new UILabel (new CGRect (CGPoint.Empty, BlrtTooltip.SharedInstance.ContentSize)) {
				Lines = 0,
				Font = BlrtConfig.BASE_TEXT_FONT
			};
			AddSubview (_lbl);

			_btn = new BlrtActionButton ("", (sender, arg) => {
				if (Click != null)
					Click (this, arg);
			}) {
			};
			AddSubview (_btn);
		}



		public void SetText (string labelText, string buttonText, nfloat maxWidth)
		{
			const float MARGIN = 8;

			nfloat height = 0;
			nfloat width = 0;


			if (labelText != null && labelText.Length > 0) {
				_lbl.Hidden = false;

				_lbl.Text = labelText;
				var size = _lbl.SizeThatFits (new CGSize (maxWidth, 90001));
				_lbl.Frame = new CGRect (CGPoint.Empty, size);

				width = NMath.Max (size.Width, width);
				height += size.Height + MARGIN;

			} else {
				_lbl.Hidden = true;
			}

			if (buttonText != null && buttonText.Length > 0) {
				_btn.Hidden = false;

				_btn.Text = buttonText;
				_btn.SizeToFit ();

				var size = _btn.Frame.Size;

				width = NMath.Max (size.Width, width);
				height += size.Height + MARGIN;
			} else {
				_btn.Hidden = true;
			}

			height = NMath.Max (0, height - MARGIN);

			Frame = new CGRect (0, 0, width, height);

			_lbl.Frame = new CGRect ((width - _lbl.Frame.Width) * 0.5f, 0, _lbl.Frame.Width, _lbl.Frame.Height);
			_btn.Frame = new CGRect ((width - _btn.Frame.Width) * 0.5f, height - _btn.Frame.Height, _btn.Frame.Width, _btn.Frame.Height);
		}
	}
}

