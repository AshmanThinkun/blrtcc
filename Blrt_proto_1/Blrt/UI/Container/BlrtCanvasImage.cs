using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;


namespace Blrt.UI.Canvas
{
	public class BlrtCanvasImage : UIImageView
	{
		private BlrtCanvasContainer _container;

		//private UIImageView _imageView;

		public BlrtCanvasImage(BlrtCanvasContainer container)
			: base(new RectangleF(Point.Empty, container.Frame.Size))
		{
			this._container = container;
			this.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

			//_imageView = new UIImageView (_image);
			//AddSubview (_imageView);

			ContentMode = UIViewContentMode.Center;

			/*
			this.ContentSize = _imageView.Frame.Size;

			this.ViewForZoomingInScrollView += (UIScrollView scrollView) => {
				return _imageView;
			};
			*/


		}
	}
}

