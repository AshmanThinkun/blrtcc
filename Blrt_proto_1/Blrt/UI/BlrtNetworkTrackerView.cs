using System;
using CoreGraphics;
using Foundation;
using UIKit;

using BlrtData.IO;

namespace BlrtiOS.UI
{
	public class BlrtNetworkTrackerView : UIView
	{
		public const float IDEAL_HEGIHT = 32;

		private UILabel _lbl;
		private UIProgressView _prog;
		private UIEdgeInsets _inset;

		
		public UIFont Font { get { return _lbl.Font; } set { _lbl.Font = value; } }

		public BlrtDownloaderList _uploader;
		public BlrtDownloaderList _downloaders;

		public string Text { get; set; }

		public string FailedText { get; set; }

		public UIEdgeInsets Insets {
			get {
				return _inset;
			}set {
				_inset = value;
				LayoutSubviews ();
			}
		}

		public UIColor TextColor { 
			get {
				return _lbl.TextColor;
			} 
			set {
				_lbl.TextColor = value;
			} 
		}

		public EventHandler OnCompleted { 
			get; 
			set;
		}



		public BlrtNetworkTrackerView (CGRect frame) : base (frame)
		{
			InitView ();
		}

		private void InitView ()
		{
			if (_prog == null) {
				Text = "{0} ({1})";
				FailedText = "{0} Failed";

				_prog = new UIProgressView () {
					Style = UIProgressViewStyle.Bar,
					AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth,
					TrackTintColor = UIColor.FromHSBA (0, 0, 0.8f, 0.8f)
				};
				AddSubview (_prog);

				_lbl = new UILabel () {
					AutoresizingMask = UIViewAutoresizing.All ^ UIViewAutoresizing.FlexibleBottomMargin,
					TextAlignment = UITextAlignment.Center,
					Lines = 0,
					Font = UIFont.SystemFontOfSize (14),
					BackgroundColor = UIColor.Clear,
					Text = "temp"
				};
				AddSubview (_lbl);
			
			}

			LayoutSubviews ();
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			_lbl.Frame = new CGRect (_inset.Left, _inset.Top, Bounds.Width - (_inset.Right + _inset.Left), Bounds.Height - (_inset.Top + _inset.Bottom + _prog.Frame.Height));
			_prog.Frame = new CGRect (_inset.Left, _lbl.Frame.Y + _lbl.Frame.Height, Bounds.Width - (_inset.Right + _inset.Left), _prog.Frame.Height);
		}


		public void RemoveTracking (bool animated)
		{
			DetachTracking ();

			_lbl.Text = string.Format (Text, "Pending", "0%");

			_prog.Alpha = 1;
			_prog.SetProgress (0, false);
		}

		private void DetachTracking ()
		{
			if (_downloaders != null) {
				_downloaders.OnProgress -= OnProgress;
				_downloaders.OnFinished -= OnFinished;
				_downloaders.OnFailed -= OnFailed;
				//d.OnDownloadStarted -= OnStarted;


				_downloaders = null;
			}

			if (_uploader != null) {
				//_uploader.OnProgressChange 		-= OnProgress;
				//_uploader.OnFinished 			-= OnFinished;
				//_uploader.OnFailed 				-= OnFailed;
				//_uploader.OnStarted 			-= OnStarted;

				_uploader = null;
			}
		}

		/*
		public void TrackUpload(BlrtParseUploader uploader)
		{
			DetachTracking ();

			_prog.Alpha = 1;
			_prog.SetProgress (0, false);

			_uploader = uploader;


			if (_uploader.UploadingFiles || _uploader.UploadingParse)
				OnProgress (this, null);
			else
				_lbl.Text = string.Format (Text, "Pending", "0%");

			_uploader.OnProgressChange 		+= OnProgress;
			_uploader.OnFinished 			+= OnFinished;
			_uploader.OnFailed 				+= OnFailed;
			_uploader.OnStarted 			+= OnStarted;
		}*/

		public void TrackDownload (BlrtDownloaderList downloaders)
		{
			DetachTracking ();

			_prog.Alpha = 1;
			_prog.SetProgress (0, false);

			_downloaders = downloaders;

			_downloaders.OnProgress += OnProgress;
			_downloaders.OnFinished += OnFinished;
			_downloaders.OnFailed += OnFailed;

			OnProgress (this, null);
		}

		protected void OnStarted (object sender, EventArgs args)
		{
			InvokeOnMainThread (() => {
				if (_downloaders != null) { 
					
					_lbl.Text = string.Format (Text, "Downloading", "0%");

				} else if (_uploader != null) {
					_lbl.Text = string.Format (Text, "Uploading", "0%");
				}

				_prog.SetProgress (0, false);
			});
		}

		
		protected void OnProgress (object sender, EventArgs args)
		{
			InvokeOnMainThread (() => {

				if (_downloaders != null) { 


					if (_downloaders.Total > 0) {
						_prog.SetProgress (_downloaders.Progress, true);
						_lbl.Text = string.Format (Text, "Downloading", (int)(_downloaders.Progress * 100) + "%");
					} else {
						_prog.SetProgress (0, true);
						_lbl.Text = string.Format (Text, "Downloading", (int)(0) + "%");
					}
				} else if (_uploader != null) {

					_prog.SetProgress (_uploader.Progress, true);
					_lbl.Text = string.Format (Text, "Uploading", (int)(_uploader.Progress * 100) + "%");
				}
			});
		}

		protected void OnFinished (object sender, EventArgs args)
		{

			InvokeOnMainThread (() => {
				if (_downloaders != null) { 
					_lbl.Text = string.Format (Text, "Downloaded", "100%");
				} else if (_uploader != null) {
					
					_lbl.Text = string.Format (Text, "Uploaded", "100%");
				}
				_prog.SetProgress (1, true);

				if (OnCompleted != null)
					OnCompleted (this, EventArgs.Empty);
			});
		}

		protected void OnFailed (object sender, EventArgs args)
		{
			InvokeOnMainThread (() => {

				if (_downloaders != null) { 
					_lbl.Text = string.Format (FailedText, "Download", 0);
				} else if (_uploader != null) {
					//_lbl.Text = string.Format ( FailedText, "Upload",  ((int)_uploader.Progress * 100) + "%");
				}

				if (OnCompleted != null)
					OnCompleted (this, EventArgs.Empty);
			});
		}
	}
}

