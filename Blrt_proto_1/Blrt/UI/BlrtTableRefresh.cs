using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtTableRefresh : UIView
	{
		UITableView _table;

		UIActivityIndicatorView indicator;

		private bool _refreshing;

		private float heightDelta = 0;

		public float PullLength { get; set; }

		public float Height { get; set; }

		public EventHandler OnRefresh { get; set; }

		public bool Refreshing { get { return _refreshing; } }


		public BlrtTableRefresh () : base ()
		{
			if (BlrtHelperiOS.IsPhone) {
				PullLength = 60;
			} else {
				PullLength = 120;
			}

			Height = 50;



			indicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleTopMargin,
				TintColor = BlrtConfig.BlrtBlack,
				HidesWhenStopped = false,
			};
			AddSubview (indicator);

			RefreshEnded ();
		}



		public void SetTable (UITableView table)
		{
			_table = table;

			_table.AddSubview (this);
		}



		public void StartRefresh ()
		{
			SetRefresh (true, false);
		}

		public void EndRefresh ()
		{
			SetRefresh (false, false);
		}

		bool fireEvent;

		public void Scrolled ()
		{
			nfloat delta = 0;

			if (_table.ContentOffset.Y < 0) {
				delta = (1 - _table.ContentOffset.Y) / PullLength;
			}

			if (delta > 1 && _table.Dragging && fireEvent) {
				SetRefresh (true, true);
			} else {

				fireEvent = fireEvent || !_table.Dragging;

				ReDraw (delta);
			}
		}

		public virtual void RefreshStarted ()
		{
			PositionIndicator ();
		}

		public virtual void RefreshEnded ()
		{
			PositionIndicator ();
		}

		private void PositionIndicator ()
		{
			indicator.StartAnimating ();
			indicator.Hidden = false;
			indicator.Frame = new CGRect (
				(Bounds.Width - indicator.Frame.Width) * 0.5f, 
				Bounds.Height - indicator.Frame.Height - (Height - indicator.Frame.Height) * 0.5f, 
				indicator.Bounds.Width, 
				indicator.Bounds.Height
			);
		}

		private void SetRefresh (bool refresh, bool events)
		{
			if (refresh != _refreshing) {
				_refreshing = refresh;

				if (refresh) {
					RefreshStarted ();
					fireEvent = false;
				} else {
					RefreshEnded ();
				}



				var inset = _table.ContentInset;
				var yPos	= _table.ContentOffset.Y;

				if (refresh) {
					heightDelta = Height;
					inset.Top += heightDelta;
				} else {
					inset.Top -= heightDelta;
					heightDelta = 0;
				}

				_table.ContentInset = inset;

				if (_table.Dragging) {
					if (yPos < 0)
						_table.ContentOffset = new CGPoint (_table.ContentOffset.X, yPos + heightDelta);
	
				} else if (yPos == 0) {
					_table.ContentOffset = new CGPoint (_table.ContentOffset.X, -heightDelta);
				}


				if (events) {
					if (OnRefresh != null)
						OnRefresh (this, EventArgs.Empty);
				}
			}


			ReDraw (1);
		}

		private void ReDraw (nfloat delta)
		{
			if (_refreshing || delta > 1)
				delta = 1;

			//Alpha = delta;

			nfloat height = 0;

			if (_refreshing)
				height = NMath.Max (0 - _table.ContentOffset.Y, Height);
			else
				height = 0 - _table.ContentOffset.Y;

			Frame = new CGRect (0, NMath.Max (0 - height, _table.ContentOffset.Y), _table.Bounds.Width, height);			

		}

	}
}

