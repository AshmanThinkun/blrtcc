using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtRefreshButton : UIView
	{
		private const float MARGIN = 8;
		private const float SIZE = 32;

		private const float CORNER = 0;
		private const float ANIMATION = 0.2f;


		private UIView 						_backView;
		private UIButton 					_copyBtn;
		private UIButton 					_imageBtn;
		private UIActivityIndicatorView 	_indicator;


		public bool Refreshing{ get; private set; }

		public EventHandler OnClicked{ get; set; }




		public BlrtRefreshButton () : base()
		{
			Frame = new CGRect (0, 0, 100, 100);

			_backView = new UIView (new CGRect (-CORNER, -CORNER, CORNER + Frame.Width, CORNER + Frame.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			_backView.Layer.CornerRadius = CORNER;
			AddSubview (_backView);





			_indicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
			AddSubview (_indicator);

			_imageBtn = new UIButton () {
			};
			_imageBtn.SetImage (BlrtHelperiOS.FromBundleTemplate ("refresh.png"), UIControlState.Normal);
			_imageBtn.ContentMode = UIViewContentMode.Center;
			_imageBtn.Frame = new CGRect (MARGIN, MARGIN, SIZE, SIZE);
			AddSubview (_imageBtn);


			_copyBtn = new UIButton (UIButtonType.System);
			_copyBtn.SetTitle ("Refresh", UIControlState.Normal);
			_copyBtn.Frame = new CGRect (MARGIN * 2 + _imageBtn.Frame.Width, MARGIN, 60, SIZE);
			AddSubview (_copyBtn);

			_imageBtn.TouchUpInside += ButtonClicked; 
			_copyBtn.TouchUpInside += ButtonClicked; 


			Layer.ShadowRadius = 4;
			Layer.ShadowColor = UIColor.White.CGColor;



			var sep = new UIView (new CGRect (0, 0, Frame.Width, 1)) {
				BackgroundColor = UIColor.FromRGBA(0,0,0,0.1f),
				AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleWidth
			};
			AddSubview (sep);



			TintColor = BlrtConfig.BLRT_ACTION;
			ShowButton (false);

		}

		private void ButtonClicked(object sender, EventArgs arg)
		{
			if (!Refreshing && OnClicked != null)
				OnClicked (this, arg);
		}


		public void ShowButton(bool animated)
		{
			if (Refreshing)
				return;

			if (animated) {
				UIView.Animate (ANIMATION, () => {
					ShowButton (false);
				});
			} else {
				_backView.BackgroundColor = BlrtConfig.BlrtGreen;
				Frame = new CGRect(Frame.X, Frame.Y, MARGIN * 3 + _imageBtn.Frame.Width +  _copyBtn.Frame.Width, MARGIN * 2 + NMath.Max (_imageBtn.Frame.Height,  _copyBtn.Frame.Height ));
				_copyBtn.Alpha = 1;
			}
		}


		public void HideButton(bool animated)
		{


			if (animated) {
				UIView.Animate (ANIMATION, () => {
					HideButton (false);
				});
			} else {
				_backView.BackgroundColor = BlrtConfig.BlrtGreen;
				Frame = new CGRect(Frame.X, Frame.Y, MARGIN * 2 + _imageBtn.Frame.Width , MARGIN * 2 + _imageBtn.Frame.Height );
				_copyBtn.Alpha = 0;
			}
		}


		public void StartRefreshing(bool animated)
		{
			if (Refreshing)
				return;

			_indicator.Center = _imageBtn.Center;

			if (animated) {
				UIView.Animate (ANIMATION, () => {
					StartRefreshing (false);
				});
			} else {
				_backView.BackgroundColor = BlrtConfig.BlrtGreen;
				_imageBtn.Alpha = 0;
				_indicator.Alpha = 1;
				_indicator.StartAnimating ();
				HideButton (false);
			}

			Refreshing = true;
		}

		public void EndRefreshing(bool animated)
		{
			if (!Refreshing)
				return;

			if (animated) {
				UIView.Animate (ANIMATION, () => {
					EndRefreshing (false);
				});
			} else {
				_backView.BackgroundColor = BlrtConfig.BlrtGreen;
				_imageBtn.Alpha = 1;
				_indicator.Alpha = 0;
			}

			Refreshing = false;
		}
	}
}

