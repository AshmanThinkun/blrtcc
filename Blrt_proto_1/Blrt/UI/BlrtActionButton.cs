using System;

using CoreGraphics;

using Foundation;
using UIKit;



namespace BlrtiOS.UI
{
	public class BlrtActionButton : BlrtButtonBase
	{
		const float DefaultHorizontalPadding = 12f;
		const float DefaultVerticalPadding = 6f;
		const float DefaultImageTextSpacing = DefaultHorizontalPadding;


		private float? _verticalPadding;

		public float VerticalPadding {
			get { return _verticalPadding ?? DefaultVerticalPadding; }
			set { _verticalPadding = value; }
		}

		private float? _horizontalPadding;

		public float HorizontalPadding {
			get { return _horizontalPadding ?? DefaultHorizontalPadding; }
			set { _horizontalPadding = value; }
		}

		private float? _imageTextSpacing;

		public float ImageTextSpacing {
			get { return _imageTextSpacing ?? DefaultImageTextSpacing; }
			set { _imageTextSpacing = value; }
		}

		public float ImageHeightConstraint { get; set; }


		UILabel _lbl;
		UIView _container;
		UIImageView _imageContainer;

		public string Text {
			get { return _lbl.Text; }
			set { _lbl.Text = value; }
		}

		private UIColor _textColor;

		public UIColor TextColor {
			get { return _textColor; }
			set { 
				_textColor = value;
				_lbl.TextColor = Enabled ? _textColor : UIColor.FromRGB (0.5f, 0.5f, 0.5f); 

				if (null != _imageContainer)
					_imageContainer.TintColor = _lbl.TextColor;
			}
		}


		enum Mode
		{
			Normal,
			Image
		}

		Mode _mode;

		
		public BlrtActionButton (string text, EventHandler action)
			: base (CGRect.Empty)
		{
			_mode = Mode.Normal;
			InitView (text, BlrtConfig.MediumFont.Large);

			this.Click += action;
		}

		public BlrtActionButton (string text, UIFont font)
			: base (CGRect.Empty)
		{
			_mode = Mode.Normal;
			InitView (text, font);
		}

		public BlrtActionButton (string text, UIImage image, EventHandler action)
			: base (CGRect.Empty)
		{
			_mode = Mode.Image;
			InitView (text, image, BlrtConfig.MediumFont.Large);

			this.Click += action;
		}

		public BlrtActionButton (string text, UIImage image, UIFont font)
			: base (CGRect.Empty)
		{
			_mode = Mode.Image;
			InitView (text, image, font);
		}

		public BlrtActionButton (string text)
			: base (CGRect.Empty)
		{
			InitView (text, BlrtConfig.MediumFont.Large);
		}

		public static BlrtActionButton FacebookButton (string text)
		{

			return new BlrtActionButton (text, BlrtHelperiOS.FromBundleTemplate ("btn_fb.png"), (EventHandler)null) {
				TintColor = UIColor.FromRGB (59, 89, 153),
				TextColor = UIColor.White,
			};
		}


		void InitView (string text, UIFont font)
		{
			ImageHeightConstraint = 0;

			var rect = text.StringSize (font);
			Frame = new CGRect (0, 0, rect.Width + HorizontalPadding * 2, rect.Height + VerticalPadding * 2);


			_container = new UIView (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				BackgroundColor = BlrtConfig.BlrtBlack
			};
			_container.Layer.CornerRadius = 4;

			AddSubview (_container);


			_lbl = new UILabel (new CGRect (0, VerticalPadding, rect.Width + HorizontalPadding * 2, rect.Height)) {
				TextAlignment = UITextAlignment.Center,
				Text = text,
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Font = font
			};
			TextColor = BlrtConfig.BlrtGreen;

			_container.AddSubview (_lbl);
		}

		void InitView (string text, UIImage image, UIFont font)
		{
			ImageHeightConstraint = 0;
			var rect = text.StringSize (font);


			Frame = new CGRect (0, 0, rect.Width + HorizontalPadding * 2 + image.Size.Width + ImageTextSpacing, NMath.Max (image.Size.Height, rect.Height) + VerticalPadding * 2);

			_container = new UIView (Bounds) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				BackgroundColor = BlrtConfig.BlrtBlack
			};
			_container.Layer.CornerRadius = 4;

			AddSubview (_container);


			{
				var heightDifferenceLabelVsImage = (int)NMath.Ceiling ((rect.Height - image.Size.Height) * 0.5f);

				var imageY = NMath.Max (VerticalPadding, VerticalPadding + heightDifferenceLabelVsImage);

				_imageContainer = new UIImageView (new CGRect (new CGPoint (HorizontalPadding, imageY), new CGSize(image.Size.Width, Frame.Height))) {
					Image = image,
					TintColor = BlrtConfig.BlrtGreen,
					ContentMode = UIViewContentMode.ScaleAspectFit
				};


				var labelX = HorizontalPadding + image.Size.Width + ImageTextSpacing;
				var labelY = NMath.Max (VerticalPadding, VerticalPadding - heightDifferenceLabelVsImage);

				_lbl = new UILabel (new CGRect (labelX, labelY, rect.Width, rect.Height)) {
					TextAlignment = UITextAlignment.Center,
					Text = text,
					TextColor = BlrtConfig.BlrtGreen,
					AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
					Font = font
				};
				TextColor = BlrtConfig.BlrtGreen;
			}

			_container.AddSubview (_imageContainer);
			_container.AddSubview (_lbl);
		}


		public override void SizeToFit ()
		{
			var size = _lbl.SizeThatFits (new CGSize (899991921, 91919292));
		
			switch (_mode) {
				case Mode.Normal:
					Frame = new CGRect (Frame.X, Frame.Y, size.Width + HorizontalPadding * 2, NMath.Max (size.Height, ImageHeightConstraint) + VerticalPadding * 2);
					break;
				case Mode.Image:
					var width = _imageContainer.Bounds.Size.Width + size.Width + HorizontalPadding * 2 + ImageTextSpacing;
					var height = NMath.Max (_imageContainer.Bounds.Size.Height, size.Height) + VerticalPadding * 2;
					Frame = new CGRect (Frame.X, Frame.Y, width, height);

					_imageContainer.Frame = new CGRect (HorizontalPadding, (int)((height - _imageContainer.Frame.Height) * 0.5f), _imageContainer.Frame.Width, _imageContainer.Frame.Height);
					break;
			}
		}

		public override void Update (bool animated)
		{
			if (!animated) {
				var c = Enabled ? TintColor : UIColor.FromRGB (51, 51, 51);
				_container.BackgroundColor = c;
				TextColor = TextColor;


				if (Highlighted) {
					_container.Alpha = 0.5f;
				} else {
					_container.Alpha = 1f;			
				}
			} else {
				Update (0.2f);
			}
		}

		private void Update (float timer)
		{
			UIView.Animate (timer, () => {
				Update (false);
			});
		}
	}
}

