using System;
using CoreGraphics;
using Foundation;
using UIKit;
using CoreAnimation;
using ObjCRuntime;

namespace BlrtiOS
{
	public class BlrtPdfView : UIView
	{

		/// <summary>The PDF Document assigned to this View</summary>
		public CGPDFDocument Document { 
			get { 
				return _document; 
			} 
			set { 
				if (_document != value) {
					_page = -1;
					_document = value;
					Page = 1;
				}
			} 
		}

		/// <summary>The Current Page that this view is displaying</summary>
		public int Page { 
			get { 
				return _page;
			} 
			set {
				if (_document == null) {
					_page = 0;
					_subView.Page = null;
					LayoutSubviews ();
					return;
				}
				value = (int)Math.Max (1, Math.Min (value, _document.Pages));
				if (_page != value || _subView.Page == null) {
					_page = value;
					_subView.Page = _document.GetPage (_page);
					LayoutSubviews ();
				}
			}
		}

		public bool FitPDFToView { 
			get { 
				return _fit;
			} 
			set {
				if (_fit != value) {
					_fit = value;
					LayoutSubviews ();
				}
			}
		}

		public CGSize CurrentPageSize { 
			get { 
				return new CGSize (_subView.ContentBounds.Width, _subView.ContentBounds.Height);
			}
		}

		public float CurrentPageZoom {
			get {
				return (float)_subView.scale;
			}
		}


		
		private CGPDFDocument _document;
		private int _page;
		
		private BlrtPdfSubView _subView;
		private UIView _backgroundColor;
		private bool _fit;

		
		public BlrtPdfView (CGRect frame) : base (frame)
		{
			const float c1 = 0.90f;
			const float c2 = 0.80f;

			_backgroundColor = new UIView () {
				BackgroundColor = UIColor.FromPatternImage (
					BlrtiOS.Util.BlrtUtilities.CreateCheckerBoard (16, 
						UIColor.FromRGB (c1, c1, c1),
						UIColor.FromRGB (c2, c2, c2)
					)
				)
			};
			AddSubview (_backgroundColor);

			//feels like a hack, but it's to get aphla values to work
			_backgroundColor.Opaque = true;
			_backgroundColor.Layer.Opaque = true;



			_subView = new BlrtPdfSubView ();
			AddSubview (_subView);
			FitPDFToView = false;
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			if (_document == null)
				return;

			if (_fit) {

				var scale = NMath.Min (
					Bounds.Width	* 1f / _subView.ContentBounds.Width, 
					Bounds.Height	* 1f / _subView.ContentBounds.Height
				);

				if (nfloat.IsInfinity (scale) || nfloat.IsNaN (scale))
					_subView.scale = 0;
				else
					_subView.scale = NMath.Max (0, scale);
			} else {
				_subView.scale = 1;
			}

			_subView.Frame = new CGRect (
				(Bounds.Width - _subView.ContentBounds.Width * _subView.scale) * 0.5f, 
				(Bounds.Height - _subView.ContentBounds.Height * _subView.scale) * 0.5f, 
				_subView.ContentBounds.Width * _subView.scale, 
				_subView.ContentBounds.Height * _subView.scale
			);

			_backgroundColor.Frame = _subView.Frame;

			const int MIN_TILES = 2;

			_subView.TileSize = (int)NMath.Max (1, NMath.Min (_subView.Frame.Width, _subView.Frame.Height));
			_subView.SetNeedsDisplay ();
		}


		private class BlrtPdfSubView : UIView
		{

			public nfloat scale;

			private nfloat _tileSize = 2;

			public nfloat TileSize {
				get { return _tileSize; }
				set {
					if (_tileSize != value) {
						_tileSize = value;
						_tiledLayer.TileSize = new CGSize (_tileSize, _tileSize);
						_tiledLayer.Contents = null;
					}
				}
			}


			public CGRect ContentBounds{ get; protected set; }

			CATiledLayer _tiledLayer;

			public CGPDFPage Page {
				get {
					return _page;
				}
				set {
					if (_page != value) {
						_page = value;

						if (_page != null) {
							ContentBounds = _page.GetBoxRect (CGPDFBox.Media);

							if ((_page.RotationAngle % 180) == 90) {
								ContentBounds = new CGRect (0, 0, ContentBounds.Height, ContentBounds.Width);
							}
						} else {
							ContentBounds = CGRect.Empty;
						}

						_tiledLayer.Contents = null;
					}			
				}
			}

			private CGPDFPage _page;


			public BlrtPdfSubView ()
				: base ()
			{
				_tiledLayer = Layer as CATiledLayer;
				_tiledLayer.LevelsOfDetail = 8;
				_tiledLayer.LevelsOfDetailBias = 7;

				_tiledLayer.Delegate = new PDFDelegate (this);


				TileSize = 16;

				scale = 1;
			}

			public override void Draw (CGRect rect)
			{
			}


			[Export ("layerClass")]
			public static Class LayerClass ()
			{
				// instruct that we want a CATileLayer (not the default CALayer) for the Layer property
				return new Class (typeof(CATiledLayer));
			}

			public class PDFDelegate : CALayerDelegate
			{

				BlrtPdfSubView _parent;


				public PDFDelegate (BlrtPdfSubView parent)
				{
					_parent = parent;
				}

				public override void DrawLayer (CALayer layer, CGContext context)
				{
					var page = _parent.Page;
					var bounds = _parent.ContentBounds;
					var scale = _parent.scale;

					if (page != null) {
						context.SetFillColor (1.0f, 1.0f, 1.0f, 1.0f);
						context.FillRect (new CGRect (0, 0, bounds.Width * scale, bounds.Height * scale));

						context.SaveState ();

						// flip page so we render it as it's meant to be read
						switch (page.RotationAngle) {
							case 90:
							case 270:
								context.TranslateCTM (bounds.Width * scale, 0);
								context.ScaleCTM (-1f, 1f);
								break;
							default:
								context.TranslateCTM (0.0f, bounds.Height * scale);
								context.ScaleCTM (1f, -1f);
								break;
						}

						context.RotateCTM (page.RotationAngle * NMath.PI / 180);
						switch (page.RotationAngle) {
							case 90:
								context.TranslateCTM (0, -bounds.Width * scale);
								break;
							case 180:
								context.TranslateCTM (-bounds.Width * scale, -bounds.Height * scale);
								break;
							case 270:
								context.TranslateCTM (-bounds.Height * scale, 0);
								break;
						}
						context.ScaleCTM (scale, scale);



						context.DrawPDFPage (page);

						context.RestoreState ();
					} else {
						context.ClearRect (new CGRect (0, 0, bounds.Width * scale, bounds.Height * scale));
					}
				}
			}
		}
	}
}

