using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Blrt.UI
{
	public class BlrtButton : UIButton
	{
		public UIColor DefaultColor {
			get;
			set;
		}

		public bool _isDown;



		public BlrtButton (RectangleF frame) : base (BlrtConfig.IsIOS7() ? UIButtonType.Custom : UIButtonType.RoundedRect)
		{
			Frame = frame;


			if(BlrtConfig.IsIOS7())
			{
				_isDown = false;

				SetTitleColor (UIColor.White, UIControlState.Normal);
				SetTitleColor (UIColor.FromRGBA(1,1,1,0.8f), UIControlState.Highlighted);
				
				//this.AllEvents += (object sender, EventArgs args) => { Update (true); };
				//this.ValueChanged += (object sender, EventArgs args) => { Update (true); };

				UpdateDisplay (false);
			}
		}

		
		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
		}
		public override void DrawRect (RectangleF area, UIViewPrintFormatter formatter)
		{
			base.DrawRect (area, formatter);


			Update (true);
		}


		public void Update(bool animated)
		{
			UpdateDisplay (Highlighted, animated);
		}
		
		private void UpdateDisplay (bool isDown, bool animated)
		{
			if (isDown != _isDown) {
				if (animated) {
					UIView.Animate (0.2f, () => {
						UpdateDisplay (isDown);});
				} else {
					UpdateDisplay (isDown);
				}
			}
		}

		private void UpdateDisplay (bool isDown)
		{
			if(isDown){
				BackgroundColor = UIColor.FromHSB(0, 0.7f, 0.8f);
			} else {
				BackgroundColor = UIColor.FromHSB(0, 0.7f, 1);
			}

			_isDown = isDown;
		}
	}
}

