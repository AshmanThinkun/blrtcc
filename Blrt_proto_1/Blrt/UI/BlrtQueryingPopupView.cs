using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BlrtData.Query;
using BlrtData;
using System.Threading.Tasks;
using BlrtShared;

namespace BlrtiOS.UI
{
	public class BlrtQueryingPopupView : UIAlertView
	{
		UIActivityIndicatorView _activity;
		BlrtQueryItem _query;

		bool _started = false;
		bool _finished = false;

		public EventHandler OnFailedQuery;
		public EventHandler OnFinishedQuery;
		public EventHandler OnCancelledQuery;

		public BlrtQueryingPopupView (string title, string message, string cancel, BlrtQueryItem query) : base (title, message, null, cancel)
		{
			_query = query;

			_activity = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Frame = new CGRect (0, 0, 32, 32)
			};
			_activity.Frame = new CGRect (new CGPoint (240, 10), _activity.Frame.Size);
			SetValueForKey (_activity, new NSString ("accessoryView")); // TODO: Still this weird thing?

			Clicked += SafeEventHandler.FromAction (
				(object sender, UIButtonEventArgs e) => {
					if (e.ButtonIndex == CancelButtonIndex) {
						Cancel ();
					}
				}
			);
		}

		public void StartAndShow ()
		{
			_started = true;

			if (_query != null && !_query.Done) {
				Show ();
				_activity.StartAnimating ();
			}

			AwaitQuery ().FireAndForget ();

		}

		async Task AwaitQuery ()
		{
			try {
				if (_query != null) {
					if (!_query.Done) {
						await _query.WaitAsync ();
					}
					if (_query.Failed) {
						throw _query.Exception;
					}
					Finish ();
				}
			} catch (Exception e) {
				Fail ();
			}
		}

		private void Fail ()
		{
			if (_finished) {
				return;
			}
			OnFailedQuery?.Invoke (this, EventArgs.Empty);

			Terminate ();
		}

		private void Finish ()
		{
			if (_finished) {
				return;
			}
			OnFinishedQuery?.Invoke (this, EventArgs.Empty);

			Terminate ();
		}

		private void Cancel ()
		{
			if (_finished) {
				return;
			}
			OnCancelledQuery?.Invoke (this, EventArgs.Empty);

			Terminate ();
		}

		private void Terminate ()
		{
			if (_started && !_finished) {
				_finished = true;
			}

		}
	}
}

