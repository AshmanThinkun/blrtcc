using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.UI
{
	/// <summary>
	/// Button class for our uses, bacuse UIButton doesnt play nice with custom animation
	/// </summary>
	public abstract class BlrtButtonBase : UIView 
	{
		private bool _isDown;
		private bool _isSelected;

		public bool Enabled {
			get{
				return UserInteractionEnabled;
			}
		}

		public bool Toggle {
			get;
			set;
		}

		public bool Selected {
			get{
				return _isSelected;
			}
			protected set{
				_isSelected = value;
			}
		}

		public bool Highlighted {
			get{
				return _isDown;
			}
		}

		public EventHandler Click {
			get;
			set;
		}


		public BlrtButtonBase(CGRect frame) : base(frame)
		{
			UserInteractionEnabled = true;

			Toggle = _isDown = _isSelected = false;
		}

		public void Enable(bool animated)
		{
			if (!UserInteractionEnabled) {
				UserInteractionEnabled = true;
				Update (animated);
			}
		}

		public void Disable(bool animated)
		{
			if (UserInteractionEnabled) {
				UserInteractionEnabled = false;
				Update (animated);
			}


		}

		public override sealed void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				ManageTouches (tmp.ToArray<UITouch> ());
			else 
				ManageTouches (touches.ToArray<UITouch> ());
		}

		public override sealed void TouchesMoved (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);

			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				ManageTouches (tmp.ToArray<UITouch> ());
			else 
				ManageTouches (touches.ToArray<UITouch> ());
		}

		public override sealed void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);

			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				ManageTouches (tmp.ToArray<UITouch> ());
			else 
				ManageTouches (touches.ToArray<UITouch> ());
		}

		public override sealed void TouchesCancelled (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);

			var tmp = evt.TouchesForView (this);
			if(tmp != null)
				ManageTouches (tmp.ToArray<UITouch> ());
			else 
				ManageTouches (touches.ToArray<UITouch> ());
		}

		private void ManageTouches(UITouch[] touches)
		{
			bool isDown = false;
			bool dirty = false;

			if (!Enabled) return;

			for (int i = 0; i < touches.Length; ++i) 
			{
				CGPoint p;

				switch (touches [i].Phase) 
				{
				case UITouchPhase.Began:
				case UITouchPhase.Moved:
					p = touches [i].LocationInView (this);
					isDown = Bounds.Contains (p);
					break;
				case UITouchPhase.Stationary:
					isDown = _isDown;
					break;
				case UITouchPhase.Ended:
					p = touches [i].LocationInView (this);
					if (Bounds.Contains (p)) {

						if(Toggle)
							_isSelected = !_isSelected;

						if (Click != null)
							Click (this, EventArgs.Empty);

						dirty = true;
					}
					break;
				}				
			}

			if(_isDown != isDown
				|| dirty)
			{
				_isDown = isDown;
				Update (true);
			}
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			if (Alpha > 0 && base.HitTest(point, uievent) != null)
				return this;

			return null;
		}

		public override void TintColorDidChange ()
		{
			base.TintColorDidChange ();

			Update (true);
		}

		public void SetSelected(bool selected, bool animate)
		{
			if (_isSelected != selected) {
				_isSelected = selected;

				Update (animate);
			}
		}

		public abstract void Update (bool animated);

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			Update (false);
		}

	}
}

