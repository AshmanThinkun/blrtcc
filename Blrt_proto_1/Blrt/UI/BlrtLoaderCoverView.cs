using System;
using CoreGraphics;

using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtLoaderCoverView : UIView
	{
		
		UIActivityIndicatorView _progress;

		private bool _visable;
		private bool _animating;


		public CGPoint _offset;


		public float AnimationLength { get; set; }

		public override bool Hidden {
			get {
				return !_visable;
			}
			set {
				if (!value != _visable) {
					ChangeState (!value, false);
				}
			}
		}


		public BlrtLoaderCoverView(CGRect frame)
			: base(frame) 
		{
			BackgroundColor = new UIColor (0, 0, 0, 0.5f);

			AnimationLength = 0.4f;
			Layer.ZPosition = 100;

			AutoresizingMask = UIViewAutoresizing.All;

			_progress = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.White) {
			};
			this.AddSubview (_progress);
			base.Alpha = 0;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			_progress.Center = new CGPoint ((int)(Bounds.Width * 0.5f + _offset.X), (int)(Bounds.Height * 0.5f + _offset.Y));
		}


		private void ChangeState(bool state, bool animate){
			if (_visable != state) {

				_visable = state;

				if (animate) {
					_animating = true;
					Animate (AnimationLength, 
						() => {
							if (!_visable) {
								Alpha = 0;
							} else {
								_progress.StartAnimating ();
								Alpha = 1;
							}
						}, () => {
						_animating = false;
					});
				} else {
					Alpha = _visable ? 1 : 0;

					if(_visable)
						_progress.StartAnimating ();
					else
						_progress.StopAnimating ();
				}
			}
			SetNeedsLayout ();
		}

		public override bool PointInside (CGPoint point, UIEvent uievent)
		{
			if (!_visable || _animating)
				return false;

			return base.PointInside (point, uievent);
		}

		public void Hide (bool animate)
		{
			ChangeState (false, animate);
		}

		public void Show (bool animate)
		{
			Show (true, CGPoint.Empty);
		}

		public void Show (bool animate, CGPoint offset)
		{
			_offset = offset;
			ChangeState (true, animate);
		}

		public static BlrtLoaderCoverView CreateMicroLoader (CGRect bounds)
		{
			var v = new BlrtLoaderCoverView (new CGRect ((bounds.Width - 128) * 0.5f, (bounds.Height - 128) * 0.5f, 128, 128));
			v.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
			v.Layer.CornerRadius = 16;



			return v;
		}
	}
}

