using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace BlrtiOS.UI
{
	public class BlrtTooltip : UIView
	{

		private static BlrtTooltip _sharedInstance;
		public static BlrtTooltip SharedInstance{
			get {
				if (_sharedInstance == null) {
					_sharedInstance = new BlrtTooltip () {
						_isOnTop = false
					};
				}
				return _sharedInstance;
			}
		}


		const float SPACING_FROM_SUBJECT 	= 16f;
		const float TRIANGLE_HEIGHT 		= 14f;
		const float TRIANGLE_WIDTH 			= 32f;
		const float X_PADDING 				= 12f;
		const float Y_PADDING 				= 8f;
		const float EDGE_MARGIN 			= 4f;
		const float CORNER_RADIUS 			= 8f;


		const float ARROW_BASE_CURVE 		= 0.25f;
		const float ARROW_POINT_CURVE 		= 0.10f;

		const float SHOW_ANIMATION 			= 0.25f;
		const float HIDE_ANIMATION 			= 0.25f;



		private UIView _tooltipView;
		private UIView _target;
		private UIScrollView _scrollView;
		private UIView _contentView;



		private bool _isShowing;
		private bool _isArrowAbove;
		private CGSize _contentSize;
		private CGSize? _scrollContentSize;
		private bool _isContentSizeChangeAnimationEnabled;

		private bool _isOnTop;

		public bool IsShowing { get { return _isShowing; } }
		public CGSize ContentSize {
			get { return this._contentSize; }
			set {
				if (value != this._contentSize) {
					this._contentSize = value;
					if (!Hidden && _isContentSizeChangeAnimationEnabled) {
						UIView.Animate (SHOW_ANIMATION, () => {
							PositionTooltip ();
						});
					} else {
						PositionTooltip ();
					}
				}
			}
		}

		public CGSize? ScrollContentSize {
			get { return _scrollContentSize; }
			set {
				_scrollContentSize = value;
			}
		}

		public bool ContentSizeChangeAnimationEnabled {
			get {
				return _isContentSizeChangeAnimationEnabled;
			}
			set {
				_isContentSizeChangeAnimationEnabled = value;
			}
		}

		public UIColor TooltipBackgroundColor{ get { return _tooltipView.BackgroundColor; } set { _tooltipView.BackgroundColor = value; } }
		public UIView ContentView { get { return this._contentView; }  }


		public BlrtTooltip ()
		{
			BackgroundColor = UIColor.FromRGBA (0,0,0,0.2f);
			AutoresizingMask = UIViewAutoresizing.All;
			Hidden = true;
			Layer.ZPosition = nfloat.MaxValue;
			Alpha = 0;

			_tooltipView = new UIView () {
				BackgroundColor = UIColor.White
			};
			_scrollView = new UIScrollView () {
				Delegate = new BlrtTooltipScrollViewDelegate()
			};
			_contentView = new UIView ();
			AddSubview (_tooltipView);
			_tooltipView.AddSubview (_scrollView);
			_scrollView.AddSubview (_contentView);

			_isShowing = false;

			_isArrowAbove = true;

			_isOnTop = true;

			_isContentSizeChangeAnimationEnabled = true;
		}



		public virtual void ShowFrom (UIBarButtonItem barbtn, bool animated)
		{
			var view = barbtn.ValueForKey (new NSString ("view")) as UIView;

			if (view != null)
				ShowFrom (view, animated);
		}


		/// <summary>
		/// Shows the tooltip and set _isShowing true.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public virtual void ShowFrom (UIView view, bool animated)
		{

			ForceOntop ();

			if (null == Superview) {
				throw new Exception ("Tooltipview does not have a super view now!");
			}

			Frame = new CGRect (0, 0, Superview.Bounds.Width, Superview.Bounds.Height);
			_scrollView.ContentOffset = CGPoint.Empty;

			_target = view;

			PositionTooltip ();

			_isShowing = true;
			Hidden = false;
			if (animated) {
				UIView.Animate (SHOW_ANIMATION, () => {
					Alpha = 1;
				});
			} else {
				Alpha = 1;
			}
		}

		/// <summary>
		/// Hide the tooltip.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public virtual void Hide (bool animated)
		{
			_isShowing = false;

			if (animated) {
				UIView.Animate (HIDE_ANIMATION, () => {
					Alpha = 0;
				}, () => {
					Hidden = true;
				});
			} else {
				Alpha = 0;
				Hidden = true;
			}
		}

		/// <summary>
		/// Positions the tooltip.
		/// </summary>
		private void PositionTooltip ()
		{
			CGRect viewFrame = CGRect.Empty;
			CGRect bounds = CGRect.Empty;
			if (Superview != null && _target != null) {
				viewFrame = GetFrameRelativeToSuper (Superview, _target);
				bounds = Superview.Bounds;
			}

			// make the centers of both the tooltip box and the target in the same vertical line
			nfloat xCoordinate = viewFrame.X + (viewFrame.Width - (_contentSize.Width + X_PADDING * 2)) * 0.5f;

			// put the tooltip box above the target. SPACING_FROM_SUBJECT is the space between the tooltip box and the target
			nfloat yCoordinate = viewFrame.Y - (SPACING_FROM_SUBJECT + _contentSize.Height + Y_PADDING * 2);
		
		
			if (yCoordinate < EDGE_MARGIN) {// Not enough verticial space for tooltip to sit above, position it below the view
				// the tootlipView includes both the main content part and the triangle part
				yCoordinate = viewFrame.Y + viewFrame.Height + SPACING_FROM_SUBJECT - TRIANGLE_HEIGHT;
				_isArrowAbove = true;
			} else {
				_isArrowAbove = false;
			}

			// make sure that the tooltip box is in the screen (the margins are considered)
			xCoordinate = Clamp (xCoordinate, 
				EDGE_MARGIN, 
				bounds.Width - EDGE_MARGIN * 2 - (_contentSize.Width + X_PADDING * 2));
			yCoordinate = Clamp (yCoordinate, 
				EDGE_MARGIN + 22,  // 22 for status bar
				bounds.Height - 2 * EDGE_MARGIN - (_contentSize.Height + Y_PADDING * 2));

			_tooltipView.Frame = new CGRect (
				xCoordinate,
				yCoordinate, 
				_contentSize.Width + X_PADDING * 2, 
				_contentSize.Height + Y_PADDING * 2 + TRIANGLE_HEIGHT);

			_scrollView.Frame = new CGRect (0, (_isArrowAbove ? TRIANGLE_HEIGHT + Y_PADDING : Y_PADDING), _contentSize.Width + X_PADDING * 2, _contentSize.Height);
			ContentView.Frame = new CGRect (new CGPoint(X_PADDING, 0), _scrollContentSize ?? _contentSize);
			_scrollView.ContentSize = 
				(null != _scrollContentSize) 
				? (
					new CGSize (
						NMath.Max (_scrollContentSize.Value.Width, _contentSize.Width) + X_PADDING * 2, 
						NMath.Max (_scrollContentSize.Value.Height, _contentSize.Height)
					)
				) 
				: (
					new CGSize (
						_contentSize.Width + X_PADDING * 2, 
						_contentSize.Height
					)
				);

			// Draw the triangle
			_tooltipView.Layer.Mask = CreateTooptipBoxMask (_tooltipView.Bounds, 
				new CGSize (TRIANGLE_WIDTH, TRIANGLE_HEIGHT),
				viewFrame.X + viewFrame.Width * 0.5f - xCoordinate, CORNER_RADIUS, _isArrowAbove);


			_scrollView.Delegate.Scrolled (_scrollView);
		}

		public void SetView(UIView view)
		{
			bool exists = false;

			foreach (var s in _contentView.Subviews) {
				if (s == view) {
					s.Hidden = false;
					exists = true;
				} else
					s.Hidden = true;
			}

			if (!exists) {
				_contentView.AddSubview (view);
			}
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if (_isShowing)
				PositionTooltip ();
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);

			if (_isShowing){
				Hide (false);
			} else {
				Hide (true);
			}
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			if (Hidden)			return null;

			return base.HitTest(point, uievent);
		}



		private static nfloat Clamp (nfloat value, nfloat min, nfloat max)
		{
			if (value >= min && value <= max) {
				return value;
			} else if (value < min) {
				return min;
			}
			return max;

		}


		private void ForceOntop() {
			if (!_isOnTop) {
				//
				if (null != Superview) {
					RemoveFromSuperview ();
				}

				foreach(var v in UIApplication.SharedApplication.KeyWindow.Subviews) {
					if(!(v is UILabel)){
						v.AddSubview (this);
						_isOnTop = true;
						break;
					}
				}
			}
		}


		/// <summary>
		/// Gets the frame relative to super.
		/// </summary>
		/// <returns>The frame relative to super.</returns>
		/// <param name="super">Super.</param>
		/// <param name="subject">Subject.</param>
		public static CGRect GetFrameRelativeToSuper (UIView super, UIView subject)
		{
			return new CGRect (
				super.ConvertPointFromView (CGPoint.Empty, subject),
				subject.Bounds.Size);
		}



		protected static CoreAnimation.CAShapeLayer CreateTooptipBoxMask (CGRect frame, CGSize arrowSize, nfloat arrowX, nfloat cornerRadius, bool isArrowAbove)
		{
			UIBezierPath path = CreateTooptipBox (frame, arrowSize, arrowX, cornerRadius, isArrowAbove);
			//roundedPath

			var maskLayer = new CoreAnimation.CAShapeLayer ();
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = path.CGPath;

			return maskLayer;
		}

		protected static UIBezierPath CreateTooptipBox (CGRect frame, CGSize arrowSize, nfloat arrowX, nfloat cornerRadius, bool isArrowAbove)
		{
			UIBezierPath roundedPath = UIBezierPath.FromRoundedRect (
				                           new CGRect (0, isArrowAbove ? arrowSize.Height : 0, frame.Width, frame.Height - arrowSize.Height), 
				                           UIRectCorner.AllCorners, new CGSize (cornerRadius, cornerRadius));

			roundedPath.UsesEvenOddFillRule = false;
			if (isArrowAbove) {
				roundedPath.MoveTo (new CGPoint (arrowX - arrowSize.Width * 0.5f, arrowSize.Height)); // to the left point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, 0),
					new CGPoint (arrowX - arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowSize.Height),
					new CGPoint (arrowX - arrowSize.Width * ARROW_POINT_CURVE, 0)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX + arrowSize.Width * 0.5f, arrowSize.Height),
					new CGPoint (arrowX + arrowSize.Width * ARROW_POINT_CURVE, 0),
					new CGPoint (arrowX + arrowSize.Width * (0.5f - ARROW_BASE_CURVE), arrowSize.Height)
				);
		
			} else {
				roundedPath.MoveTo (new CGPoint (arrowX + arrowSize.Width * 0.5f, frame.Height - arrowSize.Height)); // to the right point of the triangle
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX, frame.Height),
					new CGPoint (arrowX + arrowSize.Width * (0.5f - ARROW_BASE_CURVE), frame.Height - arrowSize.Height),
					new CGPoint (arrowX + arrowSize.Width * ARROW_POINT_CURVE, frame.Height)
				);
				roundedPath.AddCurveToPoint (
					new CGPoint (arrowX - arrowSize.Width * 0.5f, frame.Height - arrowSize.Height),
					new CGPoint (arrowX - arrowSize.Width * ARROW_POINT_CURVE, frame.Height),
					new CGPoint (arrowX - arrowSize.Width * (0.5f - ARROW_BASE_CURVE), frame.Height - arrowSize.Height)
				);
			}
			roundedPath.ClosePath ();



			return roundedPath;
		}


		private class BlrtTooltipScrollViewDelegate : UIScrollViewDelegate {
		
			private const float  DELTA = 4;


			UIView top;
			UIView bottom;
			UIScrollView target;

			public override void Scrolled (UIScrollView scrollView)
			{
				if (target == null) {
					target = scrollView;

					top = new UIView (){
						BackgroundColor = UIColor.Black
					};
					top.Layer.ShadowColor = UIColor.Black.CGColor;
					top.Layer.ShadowRadius = 8;
					top.Layer.ShadowOpacity = 1f;
					top.Layer.ZPosition = 100;
					scrollView.AddSubview (top);

					bottom = new UIView () {
						BackgroundColor = UIColor.Black
					};
					bottom.Layer.ShadowColor = UIColor.Black.CGColor;
					bottom.Layer.ShadowRadius = 8;
					bottom.Layer.ShadowOpacity = 1f;
					bottom.Layer.ZPosition = 100;
					scrollView.AddSubview (bottom);
				}

				if (target == scrollView) {

					top.Alpha = NMath.Min (1, scrollView.ContentOffset.Y / (scrollView.Bounds.Height * 0.2f));
					top.Frame = new CGRect (0, scrollView.ContentOffset.Y - DELTA, scrollView.Frame.Width, DELTA);

					bottom.Alpha = NMath.Min (1, (scrollView.ContentSize.Height - scrollView.ContentOffset.Y - scrollView.Bounds.Height) / (scrollView.Bounds.Height * 0.2f));
					bottom.Frame = new CGRect (0, scrollView.ContentOffset.Y + scrollView.Bounds.Height + DELTA - 1, scrollView.Frame.Width, DELTA);

				}
			}
		}
	}	
}

