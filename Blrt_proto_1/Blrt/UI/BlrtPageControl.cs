using System;
using CoreGraphics;
using UIKit;
using System.Collections.Generic;

namespace BlrtiOS.UI
{
	/// <summary>
	/// A pagination control that can set radius, spacing and color of an indicator
	/// </summary>
	public class BlrtPageControl : UIControl
	{
		private const float DEFAULT_INDICATOR_SPACING = 10f;
		private const float DEFAULT_INDICATOR_RADIUS = 3f;
		private int _currentPage;
		private int _pages;
		private UIColor _currentPageIndicatorTintColor;
		private UIColor _pageIndicatorTintColor;
		private float _spacing;
		private float _indicatorRadius;
		private List<UIView> _indicators;

		public BlrtPageControl (CGRect frame)
		{
			Frame = frame;
			_currentPage = 0;
			_pages = 0;
			_currentPageIndicatorTintColor = UIColor.White;
			_pageIndicatorTintColor = UIColor.Black;
			_indicatorRadius = DEFAULT_INDICATOR_RADIUS;
			_spacing = DEFAULT_INDICATOR_SPACING;
			_indicators = new List<UIView> ();
		}

		/// <summary>
		/// Gets or sets the current page.
		/// </summary>
		/// <value>The current page.</value>
		public virtual int CurrentPage
		{
			get
			{
				return _currentPage;
			}
			set
			{
				if (_pages > 0) {
					if (value < 0) {
						ChangePage (0);
					} else if (value >= _pages) {
						ChangePage (_pages - 1);
					} else {
						ChangePage (value);
					}
				}
			}
		}

		/// <summary>
		/// Changes the current page.
		/// </summary>
		/// <param name="toPage">destination page to which should be change</param>
		private void ChangePage (int toPage)
		{
			if (toPage != _currentPage) {
				_indicators [_currentPage].BackgroundColor = _pageIndicatorTintColor;
				_indicators [toPage].BackgroundColor = _currentPageIndicatorTintColor;
				_currentPage = toPage;
			}
		}

		/// <summary>
		/// Gets or sets the color of the current page indicator.
		/// </summary>
		/// <value>The color of the current page indicator.</value>
		public virtual UIColor CurrentPageIndicatorTintColor
		{
			get
			{
				return _currentPageIndicatorTintColor;
			}
			set
			{
				if (value != _currentPageIndicatorTintColor) {
					_currentPageIndicatorTintColor = value;
					LayoutSubviews ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the color of the other page indicators (except the current one).
		/// </summary>
		/// <value>The color of the other page indicators.</value>
		public virtual UIColor PageIndicatorTintColor
		{
			get
			{
				return _pageIndicatorTintColor;
			}
			set
			{
				if (value != _pageIndicatorTintColor) {
					_pageIndicatorTintColor = value;
					LayoutSubviews ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the indicator radius.
		/// </summary>
		/// <value>The indicator radius.</value>
		public virtual float IndicatorRadius
		{
			get
			{
				return _indicatorRadius;
			}
			set
			{
				if (value != _indicatorRadius) {
					_indicatorRadius = value;
					LayoutSubviews ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the spacing between the margins of 2 indicators.
		/// </summary>
		/// <value>The spacing between the margins of 2 indicators.</value>
		public virtual float Spacing
		{
			get
			{
				return _spacing;
			}
			set
			{
				if (value != _spacing) {
					_spacing = value;
					LayoutSubviews ();
				}
			}
		}

		/// <summary>
		/// Gets or sets the total page count.
		/// </summary>
		/// <value>The total page count.</value>
		public virtual int Pages
		{
			get
			{
				return _pages;
			}
			set
			{
				SetPageCount (value);
			}
		}

		/// <summary>
		/// Sets the total page count.
		/// </summary>
		/// <param name="value">The total page count</param>
		private void SetPageCount(int value)
		{
			if ((value > 0) && (value != _pages)) {
				if (_pages < value) {
					// add new indicators
					for (int i = _pages; i < value; i++) {
						_indicators.Add (new UIView());
						AddSubview (_indicators [i]);
					}
				} else {
					if (_currentPage >= value) {
						ChangePage (value - 1);
					}
					// remove redundant indicators
					for (int i = _pages - 1; i >= value; i--) {
						_indicators [i].RemoveFromSuperview ();
						_indicators.RemoveAt (i);
					}
				}
				_pages = value;
				LayoutSubviews ();
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			nfloat centerY = Frame.Height * 0.5f;	// Y position of the center point of a indicator
			for (int i = 0; i < _pages; i++) {
				nfloat centerX = Frame.Width * 0.5f + (i - (_pages - 1) * 0.5f) * (_spacing + _indicatorRadius * 2);	// X position of the center point of a indicator
				_indicators [i].Frame = new CGRect (centerX - _indicatorRadius, centerY - _indicatorRadius, _indicatorRadius * 2, _indicatorRadius * 2);
				_indicators [i].BackgroundColor = (_currentPage == i) ? _currentPageIndicatorTintColor : _pageIndicatorTintColor;
				_indicators [i].Layer.CornerRadius = NMath.Min (_indicators [i].Frame.Width, _indicators [i].Frame.Height) * 0.5f;
			}
		}

		/// <summary>
		/// Occurs when current page changed.
		/// </summary>
		public new event EventHandler ValueChanged;

		public override void TouchesEnded (Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var touch = touches.AnyObject as UITouch;
			var location = touch.LocationInView (this);
			if (location.X > Frame.Width * 0.5f) {
				CurrentPage = CurrentPage + 1;
				ValueChanged (this, EventArgs.Empty);
			} else if (location.X < Frame.Width * 0.5f) {
				CurrentPage = CurrentPage - 1;
				ValueChanged (this, EventArgs.Empty);
			}
		}
	}
}

