using System;
using System.Collections.Generic;
using CoreGraphics;

using Foundation;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtAlertBarView : UIView
	{
		public const float CELL_HEIGHT = 24;

		UILabel _messageLbl;

		public UILabel Label { get { return _messageLbl; } }

		public string Text { get { return _messageLbl.Text;} set { _messageLbl.Text = value;}}


		public BlrtAlertBarView (CGRect frame) : base(frame)
		{
			ClipsToBounds = true;
			BackgroundColor = BlrtConfig.BLRT_RED;

			_messageLbl = new UILabel(new CGRect(0, 0, frame.Width, frame.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				Font = BlrtConfig.NormalFont.Normal,
				TextColor = UIColor.White,
				TextAlignment = UITextAlignment.Center,
				Lines = 0
			};
			AddSubview(_messageLbl);
		}
	}
}

