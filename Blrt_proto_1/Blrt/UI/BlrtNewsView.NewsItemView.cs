using System;
using CoreGraphics;
using UIKit;
using BlrtData;
using System.Text.RegularExpressions;


namespace BlrtiOS.UI
{
	public partial class BlrtNewsView
	{
		private class BlrtNewsItemView : UIView
		{
			public const float ImageWidthSize		= 80f;
			public const float ImageRightMargin		= 10f;
			public const float ImageCornerRadius 	= ImageWidthSize / 10;

			public const float TitleTopMargin		=  5f;
			public const float TitleBotMargin		= TitleTopMargin * 2;

			public const float TextTopMargin 		=  0f;
			public const float TextBotMargin 		= TitleBotMargin;

			public const float ReadMoreTopMargin 	= TextBotMargin * -0.5f;
			public const float ReadMoreBotMargin 	= TextBotMargin;


			private BlrtAsyncImageView	_imageView;
			private UILabel				_titleLabel;
			private UILabel				_textLabel;
			private UILabel				_readMoreLabel;


			private float _TopPaddingDiff;
			private float _BotPaddingDiff;


			public EventHandler<BlrtNewsItemViewClickedEventArgs> OnClicked { get; set; }


			public BlrtNewsItemView (BlrtNewsItemModel model, float topPaddingDiff = 0f, float botPaddingDiff = 0f)
				: base ()
			{
				this._TopPaddingDiff = topPaddingDiff;
				this._BotPaddingDiff = botPaddingDiff;


				_imageView = new BlrtAsyncImageView
					(new CGRect (0, 0, ImageWidthSize, ImageWidthSize), model.image, UIImage.FromBundle ("news-icon.png"));

				_imageView.Layer.MasksToBounds = true;
				_imageView.Layer.CornerRadius = ImageCornerRadius;


				Regex rx = new Regex ("<[^>]*>", RegexOptions.Compiled);


				_titleLabel = new UILabel () {
					Text = rx.Replace (model.title, ""),
					TextColor = BlrtConfig.BLRT_RED,
					Font = BlrtConfig.MediumFont.SubTitle,
					Lines = 0
				};


				_textLabel = null;

				if (model.text != null) {
					_textLabel = new UILabel () {
						Text = rx.Replace (model.text, ""),
						Font = BlrtConfig.LightFont.Large,
						Lines = 0
					};
				}


				_readMoreLabel = null;

				if (model.url != null) {
					_readMoreLabel = new UILabel () {
						Text = model.readMoreText,
						Font = BlrtConfig.MediumFont.Large,
						Lines = 0
					};
				}


				AddSubview (_imageView);
				AddSubview (_titleLabel);
				if (_textLabel != null)
					AddSubview (_textLabel);
				if (_readMoreLabel != null)
					AddSubview (_readMoreLabel);
				if (_readMoreLabel != null)
					AddSubview (_readMoreLabel);


				AddGestureRecognizer (new UITapGestureRecognizer (() => {
					if (OnClicked != null)
						OnClicked(this, new BlrtNewsItemViewClickedEventArgs () { Model = model });
				}));
			}


			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();


				SizeThatFitsWithLayout (Frame.Size, true);
			}

			public override CGSize SizeThatFits (CGSize size)
			{
				return SizeThatFitsWithLayout (size, false);
			}


			private CGSize SizeThatFitsWithLayout (CGSize size, bool doLayout) {
				nfloat calculatedHeight = 0;

				nfloat textLeftMargin = ImageWidthSize + ImageRightMargin;
				nfloat labelWidth = size.Width - textLeftMargin;


				calculatedHeight += TitleTopMargin - _TopPaddingDiff;


				var labelSize = BlrtHelperiOS.GetSizeOfString
					(_titleLabel.Text, new UIStringAttributes () { Font = _titleLabel.Font }, labelWidth);

				if (doLayout)
					_titleLabel.Frame = new CGRect (textLeftMargin, calculatedHeight, labelWidth, labelSize.Height);

				calculatedHeight += labelSize.Height;


				calculatedHeight += TitleBotMargin;


				if (_textLabel != null) {
					calculatedHeight += TextTopMargin;


					labelSize = BlrtHelperiOS.GetSizeOfString
						(_textLabel.Text, new UIStringAttributes () { Font = _textLabel.Font }, labelWidth);

					if (doLayout)
						_textLabel.Frame = new CGRect (textLeftMargin, calculatedHeight, labelWidth, labelSize.Height);

					calculatedHeight += labelSize.Height;


					calculatedHeight += TextBotMargin;
				}

				if (_readMoreLabel != null) {
					calculatedHeight += ReadMoreTopMargin;


					labelSize = BlrtHelperiOS.GetSizeOfString
						(_readMoreLabel.Text, new UIStringAttributes () { Font = _readMoreLabel.Font }, labelWidth);

					if (doLayout)
						_readMoreLabel.Frame = new CGRect (textLeftMargin, calculatedHeight, labelWidth, labelSize.Height);

					calculatedHeight += labelSize.Height;


					calculatedHeight += ReadMoreBotMargin;
				}


				calculatedHeight -= _BotPaddingDiff;


				if (doLayout) {
					Frame = new CGRect (Frame.X, Frame.Y, size.Width, NMath.Max (ImageWidthSize, calculatedHeight));
					Bounds = new CGRect (0, 0, size.Width, Frame.Height);
				}


				return new CGSize (size.Width, NMath.Max (ImageWidthSize, calculatedHeight));
			}
		}
	}
}

