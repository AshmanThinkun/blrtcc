using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;
using BlrtData;

namespace BlrtiOS.UI
{
	public partial class BlrtNewsView : UIView
	{
		private const float NewsViewPadding = 10f;

		private const float FirstItemPaddingDiff = 5f;
		private const float LastItemPaddingDiff = FirstItemPaddingDiff;

		private const float NewsSeparatorHeight = 1f;
		private readonly UIColor NewsSeparatorColor = BlrtConfig.BLRT_DARK_GRAY;


		private enum NewsViewState
		{
			Loading,
			Active,
			Error
		}


		private List<BlrtNewsItemView> _newsItemViews;
		private List<BlrtNewsItemModel> _newsItemModels;

		public List<BlrtNewsItemModel> NewsItemModels {
			private get { return _newsItemModels; }
			set {
				if (_newsItemViews != null) {
					foreach (var view in _newsItemViews) {
						view.RemoveFromSuperview ();
						view.Dispose ();
					}

					_newsItemViews = null;
				}


				_newsItemModels = value;


				if (NewsItemModels != null) {
					_newsItemViews = new List<BlrtNewsItemView> ();

					int i = 0;
					foreach (var model in NewsItemModels) {
						BlrtNewsItemView view = new BlrtNewsItemView (model,
							                        i == 0 ? FirstItemPaddingDiff : 0,
							                        i == NewsItemModels.Count - 1 ? LastItemPaddingDiff : 0);

						AddSubview (view);
						_newsItemViews.Add (view);


						view.OnClicked += NewsItemViewClicked;


						i++;
					}
				}


				State = NewsViewState.Active;
			}
		}


		private UIActivityIndicatorView _loadingIndicator;

		private UILabel _errorLabel;

		private NewsViewState _state;

		private NewsViewState State {
			get { return _state; }
			set {
				var oldState = _state;

				_state = value;


				HandleSubviews (oldState);
			}
		}


		private List<UIView> _newsSeparators;


		public EventHandler<BlrtNewsItemViewClickedEventArgs> OnNewsItemClicked { get; set; }


		public BlrtNewsView () : this (new CGRect ())
		{
		}

		public BlrtNewsView (CGRect frame) : base (frame)
		{
			_newsItemViews = null;
			_newsItemModels = null;
			_newsSeparators = null;


			_loadingIndicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray) {
				HidesWhenStopped = false
			};


			_errorLabel = new UILabel () {
				Font = BlrtConfig.MediumFont.Large,
				Hidden = true,
				Lines = 0
			};


			AddSubview (_loadingIndicator);
			AddSubview (_errorLabel);


			State = NewsViewState.Loading;
		}


		private void HandleSubviews (NewsViewState? oldState = null)
		{
			switch (State) {
				case NewsViewState.Active:
					_loadingIndicator.Hidden = true;
					_errorLabel.Hidden = true;

					break;

				case NewsViewState.Loading:
					_loadingIndicator.Hidden = false;
					_errorLabel.Hidden = true;

					break;

				case NewsViewState.Error:
					_loadingIndicator.Hidden = true;

					if (NewsItemModels != null && NewsItemModels.Count > 0)
						_errorLabel.Hidden = true;
					else
						_errorLabel.Hidden = false;

					break;
			}


			LayoutSubviews ();
		}


		public void DisplayLoading ()
		{
			State = NewsViewState.Loading;
		}

		public void DisplayError (string message)
		{
			_errorLabel.Text = message;

			var labelSize = new NSString (_errorLabel.Text)
				.GetSizeUsingAttributes (new UIStringAttributes () {
				Font = _errorLabel.Font
			});
			_errorLabel.Frame = new CGRect (new CGPoint (0, 0), labelSize);


			State = NewsViewState.Error;
		}


		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();


			SizeThatFitsWithLayout (Bounds.Size, true);
		}

		public override CGSize SizeThatFits (CGSize size)
		{
			return SizeThatFitsWithLayout (size, false);
		}

		private CGSize SizeThatFitsWithLayout (CGSize size, bool doLayout)
		{
			// TODO Fix this hack... sigh
			// The problem is that SOMETHING *shrug* is setting the Bounds to a super large width
			// The bounds is only ever set to size.Width here, and size.Width of the profile view and superview are never super large
			// So something somewhere is directly modifying Bounds.Size (without calling LayoutSubviews or anything)
			if (Superview != null)
				size.Width = Superview.Bounds.Width;


			if (doLayout) {
				if (_newsSeparators != null) {
					foreach (var separator in _newsSeparators) {
						separator.RemoveFromSuperview ();
						separator.Dispose ();
					}
				}

				_newsSeparators = new List<UIView> ();
			}


			nfloat calculatedHeight = NewsViewPadding;


			if (!_loadingIndicator.Hidden) {
				_errorLabel.Hidden = true;


				_loadingIndicator.Center = new CGPoint ((size.Width - NewsViewPadding * 2) * 0.5f, calculatedHeight + _loadingIndicator.Bounds.Height * 0.5f);
				_loadingIndicator.StartAnimating ();


				calculatedHeight += _loadingIndicator.Frame.Height + NewsViewPadding;
			} else if (!_errorLabel.Hidden) {
				var labelSize = BlrtHelperiOS.GetSizeOfString
					(_errorLabel.Text, new UIStringAttributes () { Font = _errorLabel.Font }, size.Width - NewsViewPadding * 2);

				if (doLayout)
					_errorLabel.Frame = new CGRect (NewsViewPadding, calculatedHeight, labelSize.Width, labelSize.Height);


				calculatedHeight += _errorLabel.Frame.Height + NewsViewPadding;
			}


			if (_newsItemViews != null && _newsItemViews.Count > 0) {
				float leftMargin = NewsViewPadding + BlrtNewsItemView.ImageWidthSize + BlrtNewsItemView.ImageRightMargin;
				uint i = 0;

				foreach (var view in _newsItemViews) {
					var sizeThatFitsView = view.SizeThatFits (new CGSize (size.Width - NewsViewPadding * 2, size.Height - NewsViewPadding * 2));

					if (doLayout)
						view.Frame = new CGRect (NewsViewPadding, calculatedHeight, sizeThatFitsView.Width, sizeThatFitsView.Height);
						
					calculatedHeight += sizeThatFitsView.Height;


					if (i != _newsItemViews.Count - 1) {
						if (doLayout) {
							var separator = new UIView (new CGRect (leftMargin, calculatedHeight, size.Width, NewsSeparatorHeight)) {
								BackgroundColor = NewsSeparatorColor
							};

							_newsSeparators.Add (separator);
							AddSubview (separator);
						}


						calculatedHeight += NewsSeparatorHeight;
					}


					i++;
				}


				calculatedHeight += NewsViewPadding;
			}


			if (doLayout) {
				Frame = new CGRect (Frame.X, Frame.Y, size.Width, calculatedHeight);
				Bounds = new CGRect (0, 0, size.Width, calculatedHeight);
			}


			return new CGSize (size.Width, calculatedHeight);
		}


		public class BlrtNewsItemViewClickedEventArgs : EventArgs
		{
			public BlrtNewsItemModel Model { get; set; }
		}

		private void NewsItemViewClicked (object sender, BlrtNewsItemViewClickedEventArgs args)
		{
			if (OnNewsItemClicked != null)
				OnNewsItemClicked (this, args);
		}
	}
}