using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtNavButton : BlrtButtonBase
	{
		private UILabel _lbl;
		private UIImageView _img;


		public BlrtNavButton (UIImage image, string text) : base (CGRect.Empty)
		{
			const float ITEM_GAP = 6;


			_img = new UIImageView (image) {
				AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
			};
			_lbl = new UILabel () {
				Text = text,
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
			};
			_lbl.SizeToFit ();



			nfloat height = NMath.Max (_img.Frame.Height, _img.Frame.Height);
			nfloat width = _img.Frame.Width + _lbl.Frame.Width + ITEM_GAP;

			_img.Frame = new CGRect (0, (height - _img.Frame.Height) * 0.5f, _img.Frame.Width, _img.Frame.Height);
			_lbl.Frame = new CGRect (_img.Frame.Width + ITEM_GAP, 0, _lbl.Frame.Width, height);

			Frame = new CGRect (0, 0, width, height);
			AddSubview (_img);
			AddSubview (_lbl);

		}

		public override void Update (bool animated)
		{
			if (animated) {

				UIView.Animate (0.2f, () => {							
					Update (false);
				});

				return;
			}

			Alpha = Highlighted ? 0.5f : 1;

			_lbl.TextColor = TintColor;
		}
	}
}

