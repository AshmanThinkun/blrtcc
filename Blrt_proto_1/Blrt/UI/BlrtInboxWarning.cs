using System;

using CoreGraphics;

using Foundation;
using UIKit;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Timers;
using System.Linq;



namespace BlrtiOS.UI
{
	public class BlrtInboxWarning : BlrtButtonBase
	{
		readonly float margin = 15;

		UILabel _lbl;

		private string Text {
			set {
				if (_lbl.Text != value) {
					_lbl.Text = value;

					if (Superview != null) {
						var s = _lbl.SizeThatFits (new CGSize (Superview.Frame.Width - margin * 2, nfloat.MaxValue));
						Frame = new CGRect (Frame.X, Frame.Y, Superview.Frame.Width, s.Height + 16);
						SetNeedsLayout ();
						if (SizeChanged != null)
							SizeChanged (this, EventArgs.Empty);
					}
				}
			} 
		}

		public event EventHandler SizeChanged;
		public List<WarningAction> WarningList{ get; private set;}

		public bool Visible{get{return this.IsVisible;}
			set{ 
				this.IsVisible = value;
				if (!_timer.Enabled && value) {
					TimerElapsed (this, EventArgs.Empty);
					_timer.Enabled = true;
				}
				_timer.Enabled = value;
			}}

		public bool IsVisible{get{return !this.Hidden;}
			set{
				if(value== Hidden){
					this.Hidden = !value;
					SetNeedsLayout ();
					if (SizeChanged != null)
						SizeChanged (this, EventArgs.Empty);
				}
			}
		}

		Timer _timer;
		WarningAction _currentWarning;

		public nfloat HeightRequest{
			get{
				if (Superview != null) {
					var s = _lbl.SizeThatFits (new CGSize (Superview.Frame.Width - margin * 2, nfloat.MaxValue));
					return s.Height + 16;
				}
				return 0;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if (Superview != null) {
				_lbl.Frame = new CGRect (margin, 0, Frame.Width - margin * 2, Frame.Height);
			}
		}

		public BlrtInboxWarning () : base (new CGRect (0, 0, 320, 320))
		{
			_lbl = new UILabel (new CGRect (margin, 0, Frame.Width - margin * 2, Frame.Height)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
				TextColor = UIColor.White,
				Font = BlrtConfig.LightFont.Normal,
				TextAlignment = UITextAlignment.Center,
				Lines = 0
			};
			AddSubview (_lbl);

			WarningList = new List<WarningAction> ();

			_timer = new Timer () {
				Interval = 10000
			};

			_timer.Elapsed += TimerElapsed;

			this.Click += delegate {
				if (_currentWarning != null && _currentWarning.Action != null) {
					_currentWarning.Action.Invoke ();
				}
			};
		}

		public override void Update (bool animated)
		{
			if (!animated) {
				if (Highlighted || Selected) {
					Alpha = 0.75f;
				} else {
					Alpha = 1f;			
				}
			} else {
				Update (0.2f);
			}
		}

		private void Update (float timer)
		{
			UIView.Animate (timer, () => {
				Update (false);
			});
		}


		void TimerElapsed(object sender, EventArgs e){
			if(WarningList.Count>0){
				if(_currentWarning==null){
					_currentWarning = WarningList.ElementAt(0);
				}else{
					var currentIndex = WarningList.IndexOf(_currentWarning);
					var newIndex = (currentIndex+1)% WarningList.Count;
					_currentWarning = WarningList.ElementAt(newIndex);
				}
				Device.BeginInvokeOnMainThread(()=>{
					Text = _currentWarning.Title;
					this.BackgroundColor = BlrtStyles.iOS.ToUIColor(_currentWarning.BackgroundColor);
					this.IsVisible = true;
				});
			}else{
				_currentWarning = null;
				this.IsVisible = false;
				_timer.Stop();
			}
		}
	}

	public class WarningAction
	{
		public string Title{ get; set;}
		public string Identifer{get;set;}
		public Action Action{get;set;}
		public Color BackgroundColor{get;set;}
	}
}

