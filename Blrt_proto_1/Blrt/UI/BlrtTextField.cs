using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.UI
{
	public class BlrtTextField : UITextField
	{
		public UIEdgeInsets ContentInset {
			get;
			set;
		}

		public BlrtTextField (CGRect frame) : base (frame)
		{
			InitView ();
		}
		public BlrtTextField () : base ()
		{
			InitView ();
		}

		void InitView (){

			Layer.CornerRadius		= BlrtHelperiOS.IsPhone ? 2 : 4;
			BackgroundColor			= UIColor.White;
			Font 					= BlrtConfig.BUTTON_FONT;
			VerticalAlignment 		= UIControlContentVerticalAlignment.Center;

			ContentInset			= new UIEdgeInsets (0, 5, 0, 5);
		}



		public override CGRect TextRect (CGRect forBounds)
		{
			return new CGRect (
				forBounds.X + ContentInset.Left,
				forBounds.Y + ContentInset.Top,
				forBounds.Width - (ContentInset.Left + ContentInset.Right),
				forBounds.Height - (ContentInset.Top + ContentInset.Bottom)
			);
		}

		public override CGRect EditingRect (CGRect forBounds)
		{
			return new CGRect (
				forBounds.X + ContentInset.Left,
				forBounds.Y + ContentInset.Top,
				forBounds.Width - (ContentInset.Left + ContentInset.Right),
				forBounds.Height - (ContentInset.Top + ContentInset.Bottom)
			);
		}

		public override UIView HitTest (CGPoint point, UIEvent uievent)
		{
			var result =  base.HitTest (point, uievent);


			return result;
		}
	}
}

