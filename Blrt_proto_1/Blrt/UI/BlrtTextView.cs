using System;
using CoreGraphics;
using UIKit;

namespace BlrtiOS.UI
{

	public class BlrtTextView: UITextView
	{
		protected UILabel placeHolderLbl;

		public float LeftPadding {
			set {
				TextContainerInset = new UIEdgeInsets (TextContainerInset.Top, value, TextContainerInset.Bottom, TextContainerInset.Right);
			}
		}

		public string PlaceHolder {
			get {
				return placeHolderLbl.Text;
			}
			set {
				if (placeHolderLbl.Text != value) {
					placeHolderLbl.Text = value;
					SetNeedsLayout ();
				}
			}
		}

		public UIColor PlaceHolderColor {
			get {
				return placeHolderLbl.TextColor;
			}
			set {
				placeHolderLbl.TextColor = value;
			}
		}

		public override UIFont Font {
			get {
				return base.Font;
			}
			set {
				base.Font = placeHolderLbl.Font = value;
				SetNeedsLayout ();
			}
		}

		public override string Text {
			get {
				return base.Text;
			}
			set {
				base.Text = value;
				placeHolderLbl.Hidden = !string.IsNullOrEmpty(value);
			}
		}

		public BlrtTextView()
		{
			placeHolderLbl = new UILabel(Bounds){
				Lines = 1
			};
			AddSubview(placeHolderLbl);
			PlaceHolderColor = BlrtConfig.BLRT_DARK_GRAY;
			Layer.CornerRadius = 4;


			//TextContainerInset = new UIEdgeInsets(4,5,4,5);

			Font = BlrtConfig.BASE_TEXT_FONT;

			Changed += (object sender, EventArgs e) => {
				placeHolderLbl.Hidden = !string.IsNullOrEmpty(base.Text);
			};
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			placeHolderLbl.SizeToFit ();
			placeHolderLbl.Frame = new CGRect (TextContainerInset.Left + 4, TextContainerInset.Top, placeHolderLbl.Frame.Width, placeHolderLbl.Frame.Height);
		}
	}
}

