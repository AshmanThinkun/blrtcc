using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;

using BlrtData.IO;
using BlrtData.ExecutionPipeline;


namespace BlrtiOS.UI
{

	public class ProgressAwaitAlertView : BlrtProgressAlertView
	{
		private IProgress _progressSource;
		private TaskCompletionSource<bool> _task;

		public ProgressAwaitAlertView (IProgress source, string title, string message, string cancel) : base(title, message, null, cancel)
		{
			_progressSource = source;
			_task = new TaskCompletionSource<bool> ();

			if (_progressSource != null) {
				_progressSource.OnFinished 		+= OnFinished;
				_progressSource.OnProgress 		+= OnProgress;
				_progressSource.OnFailed 		+= OnFailed;

				SetProgress (_progressSource.Progress, false);


				if (_progressSource.IsFinished)
					OnFinished (_progressSource, null);
				if (_progressSource.IsFailed)
					OnFailed (_progressSource, null);
			}

			Clicked += (object sender, UIButtonEventArgs e) => {
				if(e.ButtonIndex == CancelButtonIndex)
					CancelProgressSource();
			};
		}

		public void CancelProgressSource ()
		{
			if (_progressSource == null)
				return;

			NullProgressSource ();

			_task.SetResult(false);
		}

		private void NullProgressSource ()
		{
			if (_progressSource != null) {

				_progressSource.OnFinished -= OnFinished;
				_progressSource.OnProgress -= OnProgress;
				_progressSource.OnFailed -= OnFailed;
				_progressSource = null;
			}
		
		}


		private void OnProgress(object sender, EventArgs args){


			if (_progressSource == sender && _progressSource != null) {
				InvokeOnMainThread (() => {
					SetProgress (_progressSource != null ? _progressSource.Progress : 1, true);
				});
			}
		}

		private void OnFailed(object sender, EventArgs args){
			if (_progressSource == sender && _progressSource != null) {
				NullProgressSource ();

				InvokeOnMainThread (() => {

					_task.TrySetCanceled();
				});
			}
		}
		private void OnFinished(object sender, EventArgs args){

			if (_progressSource == sender && _progressSource != null) {
				NullProgressSource ();

				InvokeOnMainThread (() => {
					SetProgress (1, true);

					_task.TrySetResult(true);
				});
			}
		} 

		public Task<bool> WaitAsync ()
		{
			return _task.Task;
		}
	}
}

