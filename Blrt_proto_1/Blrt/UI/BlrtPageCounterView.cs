using System;
using CoreGraphics;
using UIKit;
using System.Collections.Generic;
using BlrtData;

namespace BlrtiOS.UI
{
	public class BlrtPageCounterView : UIView
	{
		private UIImageView _imageView;
		private UILabel 	_lbl;

		public bool ForceSinglePage {get;set;}
		public bool ForceMultiPage {get;set;}

		public BlrtPageCounterView ( ) : base ()
		{
			_imageView = new UIImageView(BlrtHelperiOS.FromBundleTemplate("page-single.png"));
			AddSubview(_imageView);

			_imageView.Frame = new CGRect(2, 2, _imageView.Frame.Width, _imageView.Frame.Height);
			Frame = new CGRect(0,0, _imageView.Frame.X * 2 + _imageView.Frame.Width, _imageView.Frame.Y * 2 + _imageView.Frame.Height);

			const float OuterPush = 16;

			_lbl = new UILabel(new CGRect( (int)(Frame.Width * 0.2f) - OuterPush, (int)(Frame.Height * 0.1f), (int)(Frame.Width * 0.8f) + OuterPush * 2, (int)(Frame.Height * 0.9f))){
				TextAlignment = UITextAlignment.Center,
				Font = BlrtConfig.TINY_FONT
			};
			AddSubview(_lbl);

			TintColor = BlrtConfig.BLRT_BLUE;
			_lbl.TextColor = BlrtConfig.BLRT_BLUE;
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
			BlrtUtil.BetterDispose (_imageView);
			_imageView = null;
			BlrtUtil.BetterDispose (_lbl);
			_lbl = null;
		}

		public void SetPages(int total)
		{
			if (total > 0) {
				_lbl.Text = total.ToString ();

				if ((total <= 1 || ForceSinglePage) && !ForceMultiPage)
					_imageView.Image = BlrtHelperiOS.FromBundleTemplate ("page-single.png");
				else
					_imageView.Image = BlrtHelperiOS.FromBundleTemplate ("page-multiple.png");


				Hidden = false;
			} else {
				Hidden = true;
			}
		}
	}
}

