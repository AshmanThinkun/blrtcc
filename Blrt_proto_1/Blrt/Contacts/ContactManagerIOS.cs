﻿using System;
using System.Threading.Tasks;
using Foundation;
using AddressBook;
using System.Collections.Generic;
using BlrtiOS;
using UIKit;
using BlrtiOS.Util;

namespace BlrtData.Contacts
{
	public static class ContactManagerIOS
	{
		public static void RefreshContacts (Action completionHandler = null)
		{
			Task.Run (async () => {
				if (BlrtContactManager.SharedInstanced == null)
					return;
				try {
					BlrtContactManager.SharedInstanced.ShouldLoadContacts = false;
					LoadContactsFromAddressBook (BlrtContactManager.SharedInstanced);

					if (FBManager.IsLinked) {
						await LoadFBContacts ();
					} else {
						FBManager.RemoveFacebookContacts ();
					}

					await BlrtContactManager.FindBlrtUserFromParse ();

					RefreshAllAvatar ();
				} 
				catch {}
				finally {
					completionHandler?.Invoke();	
				}
			});
		}

		public static void RefreshAllAvatar ()
		{
			BlrtContactManager.SharedInstanced.ToList ().ForEach ((item) => {
				if (item.ShouldRefreshFromGravatar ()) {
					item.RefreshFromGravatar ().FireAndForget ();
				} else if (item.ShouldDownloadAvatarFromParse ()) {
					item.DownloadFromParse ().FireAndForget ();
				}
			});
		}


		private static async Task LoadFBContacts ()
		{
			try {
				if (FBManager.HasPermissions && FBManager.FBConnected ()) {
					await FBManager.AddFBToContacts ();
				} else if (FBManager.IsLinked) {
					await FBManager.LoginWithParse ();
					await FBManager.AddFBToContacts ();
				}
			} catch (Exception e) {
				FBManager.RemoveFacebookContacts ();
				e.ToString ();
			}
		}

		public static UIImage ParseImage (UIImage image)
		{
			const int maxSize = Avatar.MaxSize;
			if (NMath.Max (image.Size.Width, image.Size.Height) > maxSize) {

				nfloat max = maxSize / NMath.Max (image.Size.Width, image.Size.Height);

				var oldImage = image;

				image = BlrtUtilities.CreateThumbnail (image, (int)(image.Size.Width * max), (int)(image.Size.Height * max));

				BlrtUtil.BetterDispose (oldImage);
				oldImage = null;
			}

			return image;
		}

		private static void LoadContactsFromAddressBook (BlrtContactManager contactInfo)
		{
			contactInfo.RemoveForm (BlrtContactForm.Contacts);

			//get contacts from device
			var error = new Foundation.NSError ();
			var addressBook = ABAddressBook.Create (out error);

			if (ABAddressBook.GetAuthorizationStatus () != ABAuthorizationStatus.Authorized) {
				if (addressBook != null) {
					addressBook.RequestAccess (
						(bool arg1, Foundation.NSError arg2) => {
							LoadContactsFromAddressBook (contactInfo);
						}
					);
				}
				return;
			}

			var persons = addressBook.GetPeople ();
			var list = new List<BlrtContact> ();

			foreach (var p in persons) {
				var c = PersonToContact (p, contactInfo);
				if (c != null) {
					list.Add (c);
				}
			}
			contactInfo.Add (list);
			contactInfo.RemoveInvalid ();
		}

		public static BlrtContact PersonToContact (ABPerson p, BlrtContactManager contactInfo)
		{
			var name = p.ToString ();
			var emails = p.GetEmails ();
			var phoneNumbers = p.GetPhones ();

			if (emails.Count > 0 || phoneNumbers.Count > 0) {
				BlrtContact contact = new BlrtContact () {
					Name = name,
					State = BlrtContactForm.Contacts,
				};

				foreach (var email in emails) {
					contact.Add (new BlrtContactInfo (BlrtContactInfoType.Email, email.Value));
				}

				foreach (var phone in phoneNumbers) {
					if (BlrtUtil.IsValidPhoneNumber (phone.Value)) {
						contact.Add (new BlrtContactInfo (BlrtContactInfoType.PhoneNumber, phone.Value));
					}
				}

				if (contactInfo.ShouldCopyAvatarFromLocalDevice (contact)) {
					//copy image to local
					using (var data = p.GetImage (ABPersonImageFormat.OriginalSize)) {
						if (data != null) {
							var img = ParseImage (UIImage.LoadFromData (data));
							string filename = BlrtUserHelper.CreateUniqueUserAvatarName ();
							var path = BlrtConstants.Directories.Default.GetAvatarPath (filename);
							NSError e = null;
							var succeed = img.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY).Save (
								path,
								NSDataWritingOptions.Atomic,
								out e
							);

							if (succeed) {
								contact.Avatar = new Avatar () {
									From = Avatar.AvatarFrom.LocalContact,
									LastUpdatedAt = DateTime.Now,
									FileName = filename
								};
							}
						}
					}
				}

				return contact;
			}
			return null;
		}
	}
}

