using System;
using System.Linq;
using UIKit;
using BlrtData;
using System.Threading.Tasks;
using System.Threading;
using Parse;
using BlrtiOS.Views.Upgrade;
using BlrtData.ExecutionPipeline;

namespace BlrtiOS
{
	public static class BlrtProfileHelper
	{

		public static string GetUpgradeButtonText()
		{
			var state = UpgradeController.GetAccountUpgradeState ();
			string actionText = BlrtUtil.PlatformHelper.Translate("premium_features_title","iap");
			switch (state) {
				case UpgradeController.AccountUpgradeFooterViewState.FreeTrial:
				case UpgradeController.AccountUpgradeFooterViewState.TrialToPremium:
				case UpgradeController.AccountUpgradeFooterViewState.Premium:
					actionText = BlrtUtil.PlatformHelper.Translate ("premium_modal_upgrade_title", "iap");
					break;
				case UpgradeController.AccountUpgradeFooterViewState.PremiumRenew:
					actionText = BlrtUtil.PlatformHelper.Translate ("premium_modal_renew_title", "iap");
					break;
			}

			return actionText;
		}
		public static string saveSecondaryEmail;

		public static async Task<bool> AddSecondaryEmail(UIViewController controller, string email = null)
		{
			//Get what the user wants as their secondary
			var tcs = new TaskCompletionSource<bool> ();

			var alert = new UIAlertView (BlrtHelperiOS.Translate ("profile_editemail_secondary_add_title", "profile"),
				BlrtHelperiOS.Translate ("profile_editemail_secondary_add_message", "profile"),
				null,
				BlrtHelperiOS.Translate ("profile_editemail_secondary_add_cancel", "profile"),
				BlrtHelperiOS.Translate ("profile_editemail_secondary_add_accept", "profile")
			);

			alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alert.GetTextField (0).KeyboardType = UIKeyboardType.EmailAddress;
			alert.GetTextField (0).Text = email;

			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				tcs.TrySetResult((alert.CancelButtonIndex != e.ButtonIndex));
			};

			alert.Show ();

			if (!await tcs.Task)
				return false;

			//check that the value given is usable
			email = alert.GetTextField (0).Text;
			saveSecondaryEmail = email;
			if (!BlrtData.BlrtUtil.IsValidEmail (email)) {
				await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_not_email", "profile");
				return await AddSecondaryEmail(controller, email);
			}

			if (!await CheckPassword (controller))
				return false;


			while (controller.ParentViewController != null)
				controller = controller.ParentViewController;

			var request = new BlrtAPI.APIUserContactDetailCreate (
				new BlrtAPI.APIUserContactDetailCreate.Parameters () {
					Email = email
				}
			);

			string errorPrefix = null;

			if (await request.Run (new CancellationTokenSource (15000).Token)) {
				if (!request.Response.Success) {

					switch (request.Response.Error.Code) {
						case BlrtAPI.APIUserContactDetailCreateError.DetailInUse:
							errorPrefix = "profile_editemail_emailinuse";
							break;
						case BlrtAPI.APIUserContactDetailCreateError.TooManyContacts:
							await ShowSecondaryLimitPopupAsync (controller);
							return false;
						default:
							errorPrefix = "profile_editemail_generic_connerror";
							break;
					}
				} else {
					if (request.Response.Status.Code == BlrtAPI.APIUserContactDetailCreateStatus.AlreadyAdded) {
						BlrtHelperiOS.GenericAlertAsync ("profile_editemail_secondary_alreadyadded", "profile").FireAndForget ();
					}
					await ReloadContactDetails (true);
					return true;
				}
			} else {
				errorPrefix = "profile_editemail_generic_connerror";
			}

			await ReloadContactDetails (true);

			if (errorPrefix != null) {
				if (await BlrtHelperiOS.GenericConfirmAsync (errorPrefix, "profile")) {
					return await AddSecondaryEmail (controller, email);
				}
			}
			return false;
		}


		public static async Task<bool> ReloadContactDetails (bool fetchUserObject = false) {
			if (null == ParseUser.CurrentUser) {
				return false;
			}
			if (fetchUserObject) {
				try {
					await ParseUser.CurrentUser.BetterFetchAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				} catch (ParseException) {

//					BlrtProfileController.LastTimeUpdatedEmails = DateTime.MinValue;
					return false;
				}
			}

			try{
				var query = BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ();
				await query.WaitAsync ();

				if (query.Failed) {
					if (!BlrtUserHelper.ContactDetails.Any ()) {
//						BlrtProfileController.LastTimeUpdatedEmails = DateTime.MinValue;
					}
					return false;
				}
			} catch { }


//			BlrtProfileController.LastTimeUpdatedEmails = DateTime.Now;

			return true;
		}

		/// <summary>
		/// Shows the upgrade page for basic users upon crossing the secondary email limit.
		/// </summary>
		/// <returns>The secondary limit popup async.</returns>

		static async Task ShowSecondaryLimitPopupAsync (UIViewController controller)
		{
			switch (BlrtPermissions.AccountThreshold) {
				case BlrtPermissions.BlrtAccountThreshold.Free:
				var stringPrefix = BlrtUserHelper.HasDoneFreeTrial ? "profile_editemail_secondarylimit_premium" : "profile_editemail_secondarylimit_trial";
				var shallUpdate = await GenericConfirmAsync (controller,stringPrefix, "profile");

					if (shallUpdate) {
						await BlrtManager.App.OpenUpgradeAccount (Executor.System ());
					}

					break;
				case BlrtPermissions.BlrtAccountThreshold.Trial:
				case BlrtPermissions.BlrtAccountThreshold.Premium:
				default:
					await BlrtHelperiOS.GenericAlertAsync ("profile_editemail_secondarylimit", "profile");
					break;
			}
		}


		public static async Task<bool> GenericConfirmAsync (UIViewController controller, string label, string table, params object [] formatStrings)
		{
			return await SimpleConfirmAsync (
				controller,
				string.Format (Translate (label + "_title", table), formatStrings),
				string.Format (Translate (label + "_message", table), formatStrings),
				string.Format (Translate (label + "_action", table), formatStrings),
				true,
				string.Format (Translate (label + "_cancel", table), formatStrings)
			);
		}

		public static Task<bool> SimpleConfirmAsync (
			UIViewController controller,
			string title,
			string message,
			string actionTitle,
			bool showCancel,
			string cancelTitle = "",
			bool isBlocker = true)
		{
			TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool> ();
			var alert = UIAlertController.Create (title, message, UIAlertControllerStyle.Alert);

			if (showCancel) {
				alert.AddAction (UIAlertAction.Create (cancelTitle, UIAlertActionStyle.Cancel, delegate {
					tcs.SetResult (false);
				}));
			}

			alert.AddAction (UIAlertAction.Create (actionTitle, UIAlertActionStyle.Default, delegate {
				tcs.SetResult (true);
			}));

			Action onDismissal = null;
			if (isBlocker) {
				var blocker = new SimpleBlocker (async () => {
					return !(await tcs.Task);
				});
				ExecutionPerformer.RegisterBlocker (blocker);

				onDismissal = new Action (delegate {
					ExecutionPerformer.UnregisterBlocker (blocker);
				});
			}

			controller.PresentViewController (alert, true, onDismissal);
		
			return tcs.Task;
		}

		public static string Translate (string key, string table)
		{
			return BlrtUtil.PlatformHelper.Translate (key, table);
		}

		#region password logic

		static DateTime _lastVerifiedPassword;
		static readonly TimeSpan VerificationDuration = new TimeSpan (0, 5, 0);


		public static async Task CheckPassword (UIViewController controller, Action<bool> action) {
			var result = await CheckPassword(controller);
			if(action != null)
				action.Invoke(result);
		}

		public static async Task<bool> CheckPassword (UIViewController controller) {
			// Callback expects instance variables to be in place
			// What if the user hasn't set a password?
			if (!BlrtData.BlrtUserHelper.HasSetPassword) {

				var tcs = new TaskCompletionSource<bool> ();

				var alert = new UIAlertView (BlrtHelperiOS.Translate ("profile_editemail_setpassword_title", "profile"),
					BlrtHelperiOS.Translate ("profile_editemail_setpassword_message", "profile"), 
					null, 
					BlrtHelperiOS.Translate ("profile_editemail_setpassword_cancel", "profile")
				);

				alert.Clicked += (object sender, UIButtonEventArgs e) => {
					var passwordController = new BlrtiOS.Views.Settings.BlrtProfilePasswordChangeController () {
						ShowEmailSection = true,
						ModalPresentationStyle = UIModalPresentationStyle.FormSheet
					};

					passwordController.OnCancelPressed += (object s2, EventArgs e2) => {
						controller.DismissViewController (true, null);

						tcs.TrySetResult (false);
					};	
					passwordController.OnSuccessfulSave += (object s2, EventArgs e2) => {
						controller.DismissViewController (true, null);

						_lastVerifiedPassword = DateTime.Now;
						tcs.TrySetResult  (true);
					};

					controller.PresentViewController (new BlrtiOS.Views.Settings.GenericNavigationController (passwordController) {
						ModalPresentationStyle = UIModalPresentationStyle.FormSheet
					}, true, null);
				};

				alert.Show ();

				return await tcs.Task;
			}

			// Check once every 5 minutes
			if (_lastVerifiedPassword < DateTime.Now - VerificationDuration) {
				return await VerifyPassword ();
			}

			return true;
		}

		static async Task<bool> VerifyPassword (bool again = false) {

			var tcs = new TaskCompletionSource<bool> ();

			// `again` displays different copy if we're requesting the password again (i.e. their last attempt was invalid)
			var alert = new UIAlertView (BlrtHelperiOS.Translate ("edit_profile_password_check" + (again ? "_again" : "") + "_title", "profile"),
				BlrtHelperiOS.Translate ("edit_profile_password_check" + (again ? "_again" : "") + "_message", "profile"), 
				null, 
				BlrtHelperiOS.Translate ("edit_profile_password_check" + (again ? "_again" : "") + "_cancel", "profile"), 
				BlrtHelperiOS.Translate ("edit_profile_password_check" + (again ? "_again" : "") + "_accept", "profile")
			);

			alert.AlertViewStyle = UIAlertViewStyle.SecureTextInput;
			alert.Clicked += (object sender, UIButtonEventArgs e) => {
				tcs.TrySetResult (alert.CancelButtonIndex != e.ButtonIndex);
			};
			alert.Show ();

			if(!await tcs.Task)
				return false;

			var success = await BlrtData.Query.BlrtParseActions.CheckPassword (alert.GetTextField (0).Text);

			// success is null if there was a connection/server error
			if (success != null) {
				// Wasn't a server error - if they failed then let them try again
				if (!success.Value) {
					return await VerifyPassword (true);
				}
			} else {
				var errorTcs = new TaskCompletionSource<Task<bool>> ();

				// Tell them it was a server error, not an invalid password, and get them to try again
				var errorAlert = new UIAlertView (
					BlrtHelperiOS.Translate ("edit_profile_password_check_connerror_title", "profile"),
					BlrtHelperiOS.Translate ("edit_profile_password_check_connerror_message", "profile"), 
					null, 
					BlrtHelperiOS.Translate ("edit_profile_password_check_connerror_ok", "profile")
				);
				errorAlert.Clicked += (object sender, UIButtonEventArgs e) => {
					errorTcs.TrySetResult (VerifyPassword (false));
				};
				errorAlert.Show ();

				var result = await errorTcs.Task;

				return await result;
			}

			// No problems, don't check passwords for another 5 minutes and run the callback
			_lastVerifiedPassword = DateTime.Now;
			return true;
		}

		#endregion
	}
}

