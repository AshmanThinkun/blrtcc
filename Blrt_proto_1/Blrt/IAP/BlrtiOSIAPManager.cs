using System;
using System.Collections.Generic;
using StoreKit;
using Foundation;
using UIKit;
using BlrtData;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using Newtonsoft.Json;

namespace BlrtiOS
{
	public static partial class BlrtiOSIAPManager
	{
		private static BlrtiOSIAPProductsRequestDelegate _productsRequestDelegate;
		private static BlrtiOSIAPPaymentTransactionObserver _transactionObserver;

		public static SKProduct[] AvailableIAPs { get; private set; }

		public static BlrtiOSSpecialCaseData PremiumIAP { get; private set; }


		public static EventHandler<EventArgs> OnAvailableiOSIAPsChanged;

		private static void AvailableiOSIAPsChanged ()
		{
			if (OnAvailableiOSIAPsChanged != null) {
				Task.Run (() => {
					var handler = BlrtiOSIAPManager.OnAvailableiOSIAPsChanged;
					if (handler != null)
						handler (null, null);
				});
			}
		}


		private static SemaphoreSlim _oneAttemptAtATime;
		private static SemaphoreSlim _completionWaiter;

		private static BlrtiOSIAPRequest _subjectRequest;
		private static BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestResponse _subjectResponse;


		// We need to keep track of this session's incomplete transactions incase the user tries again
		// Only necessary for this session because if they close and open the app again SK will restore incomplete ones
		private static Dictionary<Tuple<string, string>, SKPaymentTransaction> _incompleteTransactions;


		static BlrtiOSIAPManager ()
		{
			AvailableIAPs = null;
			PremiumIAP = null;


			_incompleteTransactions = new Dictionary<Tuple<string, string>, SKPaymentTransaction> ();


			// The semaphores will be initialised later to handle the case where the logic started
			// from old transactions trying to be fulfilled on app startup
			_oneAttemptAtATime = null;
			_completionWaiter = null;

			_subjectRequest = null;
			_subjectResponse = null;


			_productsRequestDelegate = new BlrtiOSIAPProductsRequestDelegate ();
			_transactionObserver = new BlrtiOSIAPPaymentTransactionObserver ();

			BlrtGlobalEventManager.OnAvailableIAPsChanged += (object sender, EventArgs args) => {
				UpdateIAPs ();
			};


			UpdateIAPs ();
		}


		public static void Setup ()
		{
			SKPaymentQueue.DefaultQueue.AddTransactionObserver (_transactionObserver);
		}


		public class BlrtiOSIAPPaymentTransactionObserver : SKPaymentTransactionObserver
		{
			public override void UpdatedTransactions (SKPaymentQueue queue, SKPaymentTransaction[] transactions)
			{
				if (_oneAttemptAtATime == null)
					// If this is the first IAP run then initialise the semaphore here
					// It's done this way just incase the first IAP run is actually in BlrtiOSIAPManager.AttemptPayment
					// (where it has to be initialised to 1)
					_oneAttemptAtATime = new SemaphoreSlim (0);


				foreach (SKPaymentTransaction transaction in transactions) {
					switch (transaction.TransactionState) {
						case SKPaymentTransactionState.Purchased:
							BlrtiOSIAPManager.CompleteTransaction (transaction);

							break;

						case SKPaymentTransactionState.Failed:
							BlrtiOSIAPManager.FailedTransaction (transaction);

							break;
					}
				}
			}

			public override void PaymentQueueRestoreCompletedTransactionsFinished (SKPaymentQueue queue)
			{
				#if DEBUG
				// Restore succeeded
				Console.WriteLine (" ** RESTORE PaymentQueueRestoreCompletedTransactionsFinished ");
				#endif
			}

			public override void RestoreCompletedTransactionsFailedWithError (SKPaymentQueue queue, NSError error)
			{
				#if DEBUG
				// Restore failed somewhere...
				Console.WriteLine (" ** RESTORE RestoreCompletedTransactionsFailedWithError " + error.LocalizedDescription);
				#endif
			}
		}


		public static bool CanMakePayments ()
		{
			return SKPaymentQueue.CanMakePayments;
		}


		public static void UpdateIAPs ()
		{
			if (CanMakePayments () && BlrtIAP.AvailableIAPs != null) {
				SKProductsRequest productsRequest = new SKProductsRequest (new NSSet (BlrtIAP.AvailableIAPs.ToArray ()));

				productsRequest.Delegate = _productsRequestDelegate;
				productsRequest.Start ();


				LLogger.WriteEvent ("iOSIAP", "UpdateIAPs", "Product request", "Request started");
			}
		}


		public static async Task<BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestResponse> AttemptPayment (BlrtiOSIAPRequest request)
		{
			if (_oneAttemptAtATime == null)
				// If this is the first IAP run then initialise the semaphore here
				// It's done this way just incase the first IAP run is actually in BlrtiOSIAPPaymentTransactionObserver.UpdatedTransactions
				// (where it has to be initialised to 0)
				_oneAttemptAtATime = new SemaphoreSlim (1);

			await _oneAttemptAtATime.WaitAsync ();


			if (_completionWaiter == null)
				_completionWaiter = new SemaphoreSlim (0);


			SKPaymentTransaction existingTransaction;
			Tuple<string, string> transactionDetails = new Tuple<string, string> (request.Payment.ApplicationUsername, request.Payment.ProductIdentifier);
			if (_incompleteTransactions.TryGetValue (transactionDetails, out existingTransaction)) {
				LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Started (skipping IAP due to _incompleteTransactions) for product:", request.Payment.ProductIdentifier + " and transaction ID: " + existingTransaction.TransactionIdentifier);

				// This transaction has already been attempted in this session, but CC failed
				// Skip the IAP and try to get it working on CC again
				CompleteTransaction (existingTransaction);
			} else {
				// It's not in the local _incompleteTransactions list...
				// but maybe it's in the SKPaymentQueue?
				// SKPaymentQueue may not recover ALL transactions in it's queue immediately - so if there are multiple in there,
				// there's a chance that one the user is trying to make the same transaction as one that's in there but hasn't been recovered yet
				SKPaymentTransaction match = null;
				foreach (var transaction in SKPaymentQueue.DefaultQueue.Transactions) {
					Tuple<string, string> tempTransactionDetails = new Tuple<string, string> (transaction.Payment.ApplicationUsername, transaction.Payment.ProductIdentifier);

					if (tempTransactionDetails == transactionDetails) {
						match = transaction;
						break;
					}
				}

				if (match != null) {
					LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Started (skipping IAP due to SKPaymentQueue) for product:", request.Payment.ProductIdentifier + " and transaction ID: " + existingTransaction.TransactionIdentifier);

					CompleteTransaction (match);
				} else {
					_subjectRequest = request;

					LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Started for product:", request.Payment.ProductIdentifier);
					SKPaymentQueue.DefaultQueue.AddPayment (request.Payment);
				}
			}


			LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Waiting for completion with ID:", request.Payment.ProductIdentifier);
			await _completionWaiter.WaitAsync ();


			var response = _subjectResponse;


			_subjectRequest = null;
			_subjectResponse = null;


			_oneAttemptAtATime.Release ();

			return response;
		}


		private static async void CompleteTransaction (SKPaymentTransaction transaction)
		{
			LLogger.WriteEvent ("iOSIAP", "Payment attempt", "IAP Completed and moving on to CC with transaction ID:", transaction.TransactionIdentifier);


			// We keep track of completed IAP transactions and remove them after CC is successful
			// If CC failed then we'll know to skip the IAP part of this transaction if the user tries again
			Tuple<string, string> transactionDetails = new Tuple<string, string> (transaction.Payment.ApplicationUsername, transaction.Payment.ProductIdentifier);
			if (!_incompleteTransactions.ContainsKey (transactionDetails))
				_incompleteTransactions.Add (transactionDetails, transaction);


			BlrtiOSIAPRequest request = null;

			if (_subjectRequest != null)
				request = _subjectRequest;
			else
				// Only null if running from an old transaction (meaning nobody will be listening)
				request = new BlrtiOSIAPRequest (transaction);


			_subjectResponse = await request.ContinueWithTransaction (transaction);


			if (_subjectResponse.ErrorCode == null || _subjectResponse.ErrorCode == BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.DuplicateRequest) // CC successful!
				_incompleteTransactions.Remove (transactionDetails);
			// If it failed for any reason at all then we shouldn't let them make a second payment... that's the idea behind this dictionary
			// Unless... it failed because it's a duplicate transaction. We'll stop it from trying again here


			FinishTransaction (transaction);
		}

		private static void FailedTransaction (SKPaymentTransaction transaction)
		{
			LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Failed with transaction ID:", transaction.TransactionIdentifier);
			LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Failed with error code: ", transaction.Error.Code);
			LLogger.WriteEvent ("iOSIAP", "Payment attempt", "Failed with error description: ", transaction.Error.LocalizedDescription);


			_subjectResponse = new BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestResponse ();

			switch ((SKError)((long)transaction.Error.Code)) {
				case SKError.PaymentCancelled:
					_subjectResponse.error_code_enum = BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.PaymentCancelled;

					break;
				case SKError.PaymentInvalid:
					_subjectResponse.error_code_enum = BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSPaymentFailed;

					break;
				case SKError.PaymentNotAllowed:
					_subjectResponse.error_code_enum = BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSCannotMakePayments;

					break;
				default:
					_subjectResponse.error_code_enum = BlrtAccountUpgradeRequest.BlrtAccountUpgradeRequestErrorCode.iOSUnknownPaymentError;

					break;
			}


			FinishTransaction (transaction);
		}

		private static void FinishTransaction (SKPaymentTransaction transaction)
		{
			// We should only remove the transaction from SKPaymentQueue if it's NOT marked as incomplete
			// This is just incase the user closes the app while the transaction remains incomplete, we need it to remain in the SKPaymentQueue to be recovered later
			Tuple<string, string> transactionDetails = new Tuple<string, string> (transaction.Payment.ApplicationUsername, transaction.Payment.ProductIdentifier);
			if (!_incompleteTransactions.ContainsKey (transactionDetails))
				SKPaymentQueue.DefaultQueue.FinishTransaction (transaction);


			if (_completionWaiter != null)
				// It will be null if it's running from an old transaction
				_completionWaiter.Release ();
		}


		public static string GetPriceString (SKProduct product, NSDecimalNumber multiplier = null)
		{
			if (multiplier == null)
				multiplier = new NSDecimalNumber ("1");


			NSNumberFormatter formatter = new NSNumberFormatter () {
				FormatterBehavior = NSNumberFormatterBehavior.Version_10_4,
				NumberStyle = NSNumberFormatterStyle.Currency,
				Locale = product.PriceLocale
			};


			return formatter.StringFromNumber (product.Price.Multiply (multiplier));
		}



		public class BlrtiOSSpecialCaseData
		{
			public SKProduct product;

			public string ButtonText {
				get {
					return String.Format (button_text, GetPriceString (product));
				}
			}

			public BlrtPermissions.BlrtAccountThreshold? Threshold { get { return threshold_enum; } }


			public int[] threshold;
			public BlrtPermissions.BlrtAccountThreshold? threshold_enum;

			public string button_text;


			public BlrtiOSSpecialCaseData ()
			{
				threshold = null;
				threshold_enum = null;

				button_text = null;
			}


			public void Unpack ()
			{
				if (threshold != null) {
					// If an account_threshold is specified but cannot be mapped to our enum then set it to Unkown

					bool accountThresholdSet = false;
					foreach (int i in threshold) {
						if (Enum.IsDefined (typeof(BlrtPermissions.BlrtAccountThreshold), i)) {
							threshold_enum = (BlrtPermissions.BlrtAccountThreshold)i;

							accountThresholdSet = true;
							break;
						}
					}

					if (!accountThresholdSet)
						threshold_enum = BlrtPermissions.BlrtAccountThreshold.Unknown;
				}
			}
		}
	}
}