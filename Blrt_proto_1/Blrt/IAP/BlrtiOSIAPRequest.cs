using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData;
using Parse;
using StoreKit;

namespace BlrtiOS
{
	public class BlrtiOSIAPRequest : BlrtAccountUpgradeRequest
	{
		public SKPayment Payment { get; private set; }


		public BlrtiOSIAPRequest (BlrtiOSIAPManager.BlrtiOSSpecialCaseData iapData)
			: base ()
		{
			var mutablePayment = SKMutablePayment.PaymentWithProduct (iapData.product);
			mutablePayment.Quantity = 1;
			mutablePayment.ApplicationUsername = ParseUser.CurrentUser.ObjectId;


			Payment = mutablePayment;
		}

		public BlrtiOSIAPRequest (SKPaymentTransaction transaction)
			: base ()
		{
			Payment = transaction.Payment;
		}


		public override async Task<BlrtAccountUpgradeRequestResponse> Start ()
		{
			return await BlrtiOSIAPManager.AttemptPayment (this);
		}


		public async Task<BlrtAccountUpgradeRequestResponse> ContinueWithTransaction (SKPaymentTransaction transaction)
		{
			var data = new Dictionary<string, object> ();
			data.Add ("user", transaction.Payment.ApplicationUsername);
			data.Add ("iap", transaction.Payment.ProductIdentifier);
			data.Add ("transactionId", transaction.TransactionIdentifier);

			return await DespatchToCC ("iosiap", data);
		}
	}
}