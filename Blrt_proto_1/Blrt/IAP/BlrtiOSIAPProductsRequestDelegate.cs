using System;
using System.Collections.Generic;
using StoreKit;
using Foundation;
using UIKit;
using BlrtData;
using Newtonsoft.Json;

namespace BlrtiOS
{
	public static partial class BlrtiOSIAPManager {
		private class BlrtiOSIAPProductsRequestDelegate : SKProductsRequestDelegate
		{
			public override void ReceivedResponse (SKProductsRequest request, SKProductsResponse response)
			{
				LLogger.WriteEvent ("iOSIAP", "UpdateIAPs", "Product request", "Received response");


				AvailableIAPs = null;
				PremiumIAP = null;


				foreach (SKProduct product in response.Products) {
					string specialCaseDataString = null;

					if (BlrtIAP.SpecialCases.ContainsKey (product.ProductIdentifier) && BlrtIAP.SpecialCases.TryGetValue (product.ProductIdentifier, out specialCaseDataString)) {
						BlrtiOSSpecialCaseData specialCaseData = JsonConvert.DeserializeObject<BlrtiOSSpecialCaseData> (specialCaseDataString);
						specialCaseData.product = product; // Ehh... don't like doing it this way (because anybody else can)... improve if you have time
						specialCaseData.Unpack ();


						switch (specialCaseData.Threshold) {
							case BlrtPermissions.BlrtAccountThreshold.Premium:
								PremiumIAP = specialCaseData;

								break;
						}
					}
				}


				AvailableIAPs = response.Products;


				foreach (string invalidProductId in response.InvalidProducts)
					LLogger.WriteEvent ("iOSIAP", "UpdateIAPs", "invalid IAP", invalidProductId);


				AvailableiOSIAPsChanged ();


				LLogger.WriteEvent ("iOSIAP", "UpdateIAPs", "Product request", "Completed succesfully");
			}


			public override void RequestFailed (SKRequest request, NSError error)
			{
				LLogger.WriteEvent ("iOSIAP", "UpdateIAPs", "Product request", "Request failed");
			}
		}
	}
}