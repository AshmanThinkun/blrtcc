using System.Threading.Tasks;
using BlrtData.Views.Helpers;
using UIKit;
using BlrtiOS;

[assembly: Xamarin.Forms.Dependency (typeof (BlrtProfileHelperRef))]
namespace BlrtData.Views.Helpers
{
	public class BlrtProfileHelperRef : IBlrtProfileHelper
	{
		public BlrtProfileHelperRef(){}
		#region IBlrtProfileHelper implementation
		public string GetUpgradeButtonText()
		{
			return BlrtProfileHelper.GetUpgradeButtonText ();
		}

		public Task<bool> AddSecondaryEmail (object controller, string email = null)
		{
			return BlrtProfileHelper.AddSecondaryEmail (controller as UIViewController, email);
		}

		public Task<bool> ReloadContactDetails (bool fetchUserObject = false)
		{
			return BlrtProfileHelper.ReloadContactDetails (fetchUserObject);
		}

		public async Task<bool> CheckPassword (object controller)
		{
			return await BlrtProfileHelper.CheckPassword (controller as UIViewController);
		}

		#endregion
	}
}

