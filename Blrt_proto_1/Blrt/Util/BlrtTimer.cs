using System;
using Foundation;

namespace BlrtiOS.Util
{
	/// <summary>
	/// A basic interval timer wrapper.  This will handle running an NSTimer, also with ensuring that it doesn't cause any errors while running 
	/// </summary>
	public class BlrtTimer : IDisposable
	{
		public delegate void BlrtTimerInterval (object sender);

		private double interval;
		private NSTimer _thread;

		public bool Running {
			get {
				return _thread != null;
			}
		}

		/// <summary>
		/// The method run on every interval
		/// </summary>
		public BlrtTimerInterval onInterval;

		public BlrtTimer (double interval)
		{
			this.interval = interval > 0.001 ? interval : 0.05;
		}


		/// <summary>
		/// Start up the interval, and runs until stopped. The timer will not start if onInterval is null
		/// </summary>
		public void Start ()
		{

			if (_thread == null && onInterval != null) {
				_thread = NSTimer.CreateRepeatingScheduledTimer (
					interval, RunInterval
				);
			}
		}

		/// <summary>
		/// Stops the Current Timer From Running
		/// </summary>
		/// <param name="runIntervalEnd">If set to <c>true</c> run onInterval a final time.</param>
		public void Stop (bool runIntervalEnd = true)
		{
			if (_thread != null) {
				_thread.Invalidate ();
				_thread = null;

				if (runIntervalEnd)
					RunInterval ();
			}
		}

		private void RunInterval (NSTimer timer = null)
		{
			if (onInterval != null) {
				onInterval (this);
			}
		}

		public void Dispose ()
		{
			Stop (false);
			_thread.Dispose ();
		}

	}
}

