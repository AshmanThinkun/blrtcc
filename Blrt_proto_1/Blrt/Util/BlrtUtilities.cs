using System;
using CoreGraphics;
using System.Text.RegularExpressions;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using UIKit;

using BlrtData;
using BlrtiOS.Media;
using BlrtCanvas;
using System.Threading.Tasks;
using Foundation;
using System.Collections.Generic;
using System.Linq;
using Acr.UserDialogs;
using BlrtCanvas.Pages;
using ImagePickers;
using AVFoundation;
using MessageUI;

namespace BlrtiOS.Util
{
	/// <summary>
	/// This class contains a bunch of useful static function
	/// </summary>

	public static partial class BlrtUtilities
	{
		internal static async Task<ICanvasPage> GetCanvasPageForImage (CGImage image, UIImageOrientation orientation)
		{
			using (image) {
				using (var oldImg = new UIImage (image, 1.0f, orientation)) {
					string path = await CompressedImagePath (oldImg,
														    UIImagePickerControllerSourceType
														    .PhotoLibrary
														    .ToString ());
					if (!string.IsNullOrEmpty (path)) {
						return new BlrtImagePage (path);
					}
					return null;
				}
			}
		}

		public static List<ICanvasPage> GetPDFCanvasPages (string pdfPath)
		{
			var pageCount = CanvasPageView.PlatformHelper.PDFPageCount (pdfPath);
			var returnList = new List<ICanvasPage> (pageCount);
			for (int i = 0; i < pageCount; i++) {
				returnList.Add(new BlrtPDFPage (pdfPath, i + 1, ""));
			}
			return returnList;
		}

		public static async Task<bool> AskUserForRetry ()
		{
			return await BlrtHelperiOS.SimpleConfirmAsync (
			BlrtHelperiOS.Translate ("checking_blrt_to_open_failed_title"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_failed_message"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_failed_retry"),
					true, 
					BlrtHelperiOS.Translate ("checking_blrt_to_open_failed_cancel")
				);
		}

		/// <summary>
		/// Gets the audio record permission.
		/// </summary>
		/// <value>The record permission.</value>
		internal static bool? AudioRecordPermission {
			get {
				switch (AVAudioSession.SharedInstance ().RecordPermission) {
					case AVAudioSessionRecordPermission.Denied:
						return false;
					case AVAudioSessionRecordPermission.Granted:
						return true;
					case AVAudioSessionRecordPermission.Undetermined:
					default:
						return null;
					}
			}
		}

		/// <summary>
		/// Requests the mic permission. Checks whetehr user has granted mic permission .
		/// </summary>
		/// <returns>The mic permission.</returns>
		internal static async Task<bool> RequestAudioRecordPermission ()
		{
			var tcs = new TaskCompletionSource<bool> ();
			AVAudioSession.SharedInstance ().RequestRecordPermission (((granted) => {
				tcs.SetResult (granted);
			}));
			return await tcs.Task;
		}

		public static async Task<bool> DownloadContent (BlrtData.IO.BlrtDownloader [] downloaders)
		{
			if (downloaders == null || downloaders.Count() <= 0)
				return true;
			
			bool downloadCanceled = false;
			if (downloaders.Length > 0) {
				downloadCanceled = !await BlrtHelperiOS.ShowWaitingProgressAlertAsync (
					downloaders,
					BlrtHelperiOS.Translate ("checking_blrt_to_open_title"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_message"),
					BlrtHelperiOS.Translate ("checking_blrt_to_open_cancel")
				);
			}
			return !downloadCanceled;
		}

		public static UIBezierPath InfoBubble (CGSize contentSize, nfloat borderWidth, nfloat radius, nfloat triangleHeight)
		{
			if (borderWidth > radius)
				return null;

			var rect = new CGRect (0, 0, contentSize.Width, contentSize.Height);
			rect.Offset (radius, radius + triangleHeight);

			var path = new UIBezierPath ();
			var radius2 = radius - borderWidth / 2; // Radius adjusted for the border width

			var triangleStartX = rect.GetMinX () + contentSize.Width * 0.1;
			path.MoveTo (new CGPoint (triangleStartX, rect.GetMinY () - radius2));
			path.AddLineTo (new CGPoint (triangleStartX + triangleHeight, rect.GetMinY () - radius2 - triangleHeight));
			path.AddLineTo (new CGPoint (triangleStartX + triangleHeight * 2, rect.GetMinY () - radius2));
			path.AddArc (new CGPoint (rect.GetMaxX (), rect.GetMinY ()), radius2, -NMath.PI / 2, 0, true);
			path.AddArc (new CGPoint (rect.GetMaxX (), rect.GetMaxY ()), radius2, 0, NMath.PI / 2, true);
			path.AddArc (new CGPoint (rect.GetMinX (), rect.GetMaxY ()), radius2, NMath.PI / 2, NMath.PI, true);
			path.AddArc (new CGPoint (rect.GetMinX (), rect.GetMinY ()), radius2, NMath.PI, -NMath.PI / 2, true);

			path.ClosePath ();

			return path;
		}

		public static async Task<string []> CompressedImagePaths (IEnumerable<PhotoGalleryAssetResult> images, string source)
		{
			string [] paths = new string [images.Count ()];
			int i = 0;

			foreach (var asset in images) {

				//we user alimagerepresentation as a param to pase between functions so that we can save memory
				using (var cgImage =  asset.Image){//asset.Representation.GetImage ()) {
					using (var oldImg = new UIImage (cgImage, 1.0f, asset.Orientation)) {
						paths [i++] = await CompressedImagePath (oldImg, source);
					}
				}
			}
			return paths;
		}

		public static async Task<string []> CompressedImagePaths (List<string> urls, string source)
		{
			string [] paths = new string [urls.Count ()];
			int i = 0;

			foreach (var url in urls) {

				//we user alimagerepresentation as a param to pase between functions so that we can save memory
				using (var image = UIImage.FromFile(url)) {
					paths [i++] = await CompressedImagePath (image, source);
				}
			}
			return paths;
		}

		public static async Task<string> CompressedImagePath (UIImage image, string source)
		{
			using (var newImg = await ParseImage (image)) {
				string path = await SaveImageToLocalJPG (newImg, source + "-");
				image.Dispose ();
				return path;
			}
		}

		public static T GetSuperOfType<T> (UIView view) where T : UIView
		{
			while (view.Superview != null && !(view.Superview is T)) {
				view = view.Superview;
			}

			return view.Superview as T;
		}


		public static CGRect GetBoundingBox (UIView view)
		{
			var min = new CGPoint ();
			var max = new CGPoint ();

			bool first = true;

			
			foreach (var v in view.Subviews) {
			
				var rect = BoundingBoxCalulate (v);


				if (first) {

					min.X = rect.X;
					min.Y = rect.Y;

					max.X = rect.X + rect.Width;
					max.Y = rect.Y + rect.Height;

					first = false;
				} else {

					min.X = NMath.Min (min.X, rect.X);
					min.Y = NMath.Min (min.Y, rect.Y);

					max.X = NMath.Max (max.X, rect.X + rect.Width);
					max.Y = NMath.Max (max.Y, rect.Y + rect.Height);
				
				}
					
			}

			return new CGRect (min.X, min.Y, max.X - min.X, max.Y - min.Y);
		}

		private static CGRect BoundingBoxCalulate (UIView view)
		{
			var min = new CGPoint (view.Frame.X, view.Frame.Y);
			var max = new CGPoint (view.Frame.X + view.Bounds.Width, view.Frame.Y + view.Bounds.Height);

			if (view.Subviews != null && view.Subviews.Length > 0) {
				foreach (var v in view.Subviews) {
			
					var tmp = BoundingBoxCalulate (v);

					min.X = NMath.Min (min.X, view.Frame.X + tmp.X);
					min.Y = NMath.Min (min.Y, view.Frame.Y + tmp.Y);

					max.X = NMath.Max (max.X, view.Frame.X + tmp.X + tmp.Width);
					max.Y = NMath.Max (max.Y, view.Frame.Y + tmp.Y + tmp.Height);
				}
			} else {
				return new CGRect (view.Frame.X, view.Frame.Y,
					view.Bounds.Width, view.Bounds.Height);
			}

			return new CGRect (min.X, min.Y, max.X - min.X, max.Y - min.Y);
		}



		public static UIImage CreateThumbnail (Media.BlrtPDF pdf, int pdfPage, int size)
		{
			return CreateThumbnail (pdf, pdfPage, size, size);
		}

		public static UIImage CreateThumbnail (Media.BlrtPDF pdf, int pdfPage, int width, int height)
		{
			return ImageFromPDF (pdf, pdfPage, width, height);
		}

		public static UIImage ImageFromPDF (Media.BlrtPDF pdf, int pdfPage, int width = -1, int height = -1)
		{
			using (var page = pdf.Document.GetPage (pdfPage)) {

				CGRect pageRect = page.GetBoxRect (CGPDFBox.Media);
				var orgPageSize = pageRect;
				if (page.RotationAngle == 90 || page.RotationAngle == 270) {
					var temp = pageRect.Height;
					pageRect.Height = pageRect.Width;
					pageRect.Width = temp;
					temp = orgPageSize.Height;
					orgPageSize.Height = orgPageSize.Width;
					orgPageSize.Width = temp;
				}

				if (width <= 0 || height <= 0) {
					width = (int)pageRect.Width;
					height = (int)pageRect.Height;
				}

				var pdfScale = NMath.Max (
					width * 1f / pageRect.Width, 
					height * 1f / pageRect.Height
				);

				pageRect.Size = new CGSize (pageRect.Width * pdfScale, pageRect.Height * pdfScale);

				UIGraphics.BeginImageContext (new CGSize (width, height));

				CGContext context = UIGraphics.GetCurrentContext ();
				if (context != null)
					context.SetAllowsAntialiasing (true);

				//White BG
				context.SetFillColor (1.0f, 1.0f, 1.0f, 1.0f);
				context.FillRect (pageRect);
				context.SaveState ();

				// Next 3 lines makes the rotations so that the page look in the right direction
				context.TranslateCTM (0.0f, pageRect.Size.Height);
				context.ScaleCTM (1.0f, -1.0f);

				pageRect.Location = new CGPoint (
					(width - pageRect.Width) * 0.5f, 
					(pageRect.Height - height) * 0.5f); //this value should be positive as image's scale 

				CGAffineTransform transform = page.GetDrawingTransform (CGPDFBox.Media, pageRect, 0, true);
				if (pdfScale > 1) {
					transform.Multiply (CGAffineTransform.MakeTranslation ((orgPageSize.Width - width) * 0.5f, (orgPageSize.Height - height) * 0.5f));
					transform.Multiply (CGAffineTransform.MakeScale (pdfScale, pdfScale));
				}
				context.ConcatCTM (transform);

				context.DrawPDFPage (page);
				context.RestoreState ();
			}
			UIImage img = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return img;
		}


		public static UIImage CreateCheckerBoard (int size, UIColor cell1, UIColor cell2)
		{

			UIGraphics.BeginImageContext (new CGSize (size, size));

			using (var context = UIGraphics.GetCurrentContext ()) {

				context.SetFillColor (cell1.CGColor);
				context.AddRect (new CGRect (0, 0, size, size));
				context.DrawPath (CGPathDrawingMode.Fill);

				context.SetFillColor (cell2.CGColor);
				context.AddRect (new CGRect (0, size / 2, size / 2, size / 2));
				context.AddRect (new CGRect (size / 2, 0, size / 2, size / 2));
				context.DrawPath (CGPathDrawingMode.Fill);

				var output = UIGraphics.GetImageFromCurrentImageContext ();

				UIGraphics.EndImageContext ();
			
				return output;
			}
		}

		public static UIImage CreateThumbnail (UIImage image, int size)
		{
			return CreateThumbnail (image, size, size);
		}

		public static async Task<bool> CreateImgFileThumbnail (string srcPath, string dstPath, System.Drawing.Size thumbnailSize)
		{
			return await Task.Run (() => {
				using (var srcImg = UIImage.FromFile (srcPath)) {
					using (var thumbnail = CreateThumbnail (srcImg, thumbnailSize.Width, thumbnailSize.Height)) {
						using (var data = thumbnail.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
							NSError e = null;
							var succeed = data.Save (
								dstPath,
								NSDataWritingOptions.Atomic,
								out e
							);
							return succeed;
						}
					}
				}
			});
		}

		public static Task<bool> CreatePDFThumbnail (string srcPDFPath, int mediaPage, string dstPath, System.Drawing.Size thumbnailSize)
		{
			return Task.Run (() => {
				var pdf = BlrtPDF.FromFile (srcPDFPath);
				using (var thumbnail = BlrtUtilities.CreateThumbnail (pdf, mediaPage, (int)thumbnailSize.Width, (int)thumbnailSize.Height)) {
					using (var data = thumbnail.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
						NSError e = null;
						var succeed = data.Save (
							dstPath,
							NSDataWritingOptions.Atomic,
							out e
						);
						return succeed;
					}
				}
			});
		}

		public static UIImage CreateThumbnail (UIImage image, nfloat width, nfloat height, string path = null)
		{
			if (path != null && image == null) {
				image = UIImage.FromFile (path);
			}

			UIGraphics.BeginImageContext (new CGSize (width, height));
			var context = UIGraphics.GetCurrentContext ();
			if (context != null)
				context.SetAllowsAntialiasing (true);

			var imgWidth = image.Size.Width;
			var imgHeight = image.Size.Height;

			var widthRatio = width * 1f / imgWidth;
			var heightRatio = height * 1f / imgHeight;

			var scaleRatio = NMath.Max (widthRatio, heightRatio);

			var scaledImgWidth = scaleRatio * imgWidth;
			var scaledImgHeight = scaleRatio * imgHeight;

			image.Draw (new CGRect (
				(width - scaledImgWidth) * 0.5f, //these need to change
				(height - scaledImgHeight) * 0.5f, //these need to change
				scaledImgWidth, 
				scaledImgHeight
			)
			);

			var scaledImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return scaledImage;
		}

		public static async Task<UIImage> ParseImage (UIImage image, bool handleAlert = true)
		{
			if (NMath.Max (image.Size.Width, image.Size.Height) > BlrtPermissions.MaxImageResolution) {

				nfloat max = BlrtPermissions.MaxImageResolution / NMath.Max (image.Size.Width, image.Size.Height);

				var oldImage = image;

				image = CreateThumbnail (image, (int)(image.Size.Width * max), (int)(image.Size.Height * max));

				BlrtUtil.BetterDispose (oldImage);
				oldImage = null;

				//if (handleAlert) {
				//	await BlrtHelperiOS.AwaitAlert (
				//		BlrtHelperiOS.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxImageResolution)
				//	);
				//}
			}

			return image;
		}


		public static Task<string> SaveImageToLocalJPG (UIImage image, string prefix = null)
		{
			return Task<string>.Run (() => {
				if (null != image) {
					var path = BlrtData.BlrtRecentFileManager.GetUniqueFile (".jpg", prefix);
					using (var data = image.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
						NSError e = null;
						var succeed = data.Save (
							path,
							NSDataWritingOptions.Atomic,
							out e
						);
						return succeed ? path : null;
					}
				}
				return null;
			});
		}

		//Allows a UINavigationController to push using a custom animation transition
		public static void PushControllerWithVerticalFlip (this UINavigationController target, UIViewController controllerToPush)
		{
			UIViewAnimationOptions transition = UIViewAnimationOptions.TransitionNone;

			switch (target.InterfaceOrientation) {
				case UIInterfaceOrientation.PortraitUpsideDown:
					transition = UIViewAnimationOptions.TransitionFlipFromBottom;
					break;
				case UIInterfaceOrientation.LandscapeRight:
					transition = UIViewAnimationOptions.TransitionFlipFromRight;
					break;
				case UIInterfaceOrientation.LandscapeLeft:
					transition = UIViewAnimationOptions.TransitionFlipFromLeft;
					break;
				case UIInterfaceOrientation.Portrait:
					transition = UIViewAnimationOptions.TransitionFlipFromTop;
					break;
			}

			UIView.Transition (target.View, 0.75, transition, delegate() {
				target.PushViewController (controllerToPush, false);
			}, null);
		}

		//Allows a UINavigationController to pop a using a custom animation
		public static void PopControllerWithVerticalFlip (this UINavigationController target)
		{
			UIViewAnimationOptions transition = UIViewAnimationOptions.TransitionNone;

			switch (target.InterfaceOrientation) {
				case UIInterfaceOrientation.PortraitUpsideDown:
					transition = UIViewAnimationOptions.TransitionFlipFromTop;
					break;
				case UIInterfaceOrientation.LandscapeRight:
					transition = UIViewAnimationOptions.TransitionFlipFromLeft;
					break;
				case UIInterfaceOrientation.LandscapeLeft:
					transition = UIViewAnimationOptions.TransitionFlipFromRight;
					break;
				case UIInterfaceOrientation.Portrait:
					transition = UIViewAnimationOptions.TransitionFlipFromBottom;
					break;
			}



			UIView.Transition (target.View, 0.75, transition, delegate() {
				target.PopViewController (false);
			}, null);         
		}

		//Allows a UINavigationController to push using a custom animation transition
		public static void PushControllerWithFade (this UINavigationController target, UIViewController controllerToPush)
		{
			UIView.Transition (target.View, 0.75, UIViewAnimationOptions.TransitionCrossDissolve, delegate() {
				target.PushViewController (controllerToPush, false);
			}, null);
		}

		//Allows a UINavigationController to pop a using a custom animation
		public static void PopControllerWithFade (this UINavigationController target)
		{
			UIView.Transition (target.View, 0.75, UIViewAnimationOptions.TransitionCrossDissolve, delegate() {
				target.PopViewController (false);
			}, null);         
		}


		//		public static Stream CreateThumbnailStream(Media.BlrtMediaWrapper mediaWrapper, int width = 256, int height = 256)
		//		{
		//			var item = mediaWrapper.GetPage (1);
		//
		//			return CreateThumbnailStream(item, width, height);
		//		}
		//
		//		public static Stream CreateThumbnailStream(ICanvasPage page,  int width = 256, int height = 256)
		//		{
		//			UIImage thumbnail = CreateThumbnail (page, width, height);
		//
		//			if (thumbnail != null)
		//				return thumbnail.AsJPEG (BlrtConfig.JPEG_COMPRESSION).AsStream();
		//
		//			return null;
		//		}
		//
		//		public static UIImage CreateThumbnail(ICanvasPage page, int width = 256, int height = 256)
		//		{
		//			switch(page.Format){
		//				case BlrtMediaFormat.Template:
		//				case BlrtMediaFormat.Image:
		//					return BlrtUtilities.CreateThumbnail (page.Media as UIImage, width, height);
		//				case BlrtMediaFormat.PDF:
		//					return BlrtUtilities.CreateThumbnail (page.Media as BlrtPDF, page.MediaPage, width, height);
		//			}
		//
		//			return null;
		//		}

		public static CoreAnimation.CAShapeLayer CreateRoundedMask (CGRect frame, float cornerRaduis, UIRectCorner corners)
		{
			UIBezierPath roundedPath = UIBezierPath.FromRoundedRect (frame, corners, new CGSize (cornerRaduis, cornerRaduis));


			var maskLayer = new CoreAnimation.CAShapeLayer ();
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = roundedPath.CGPath;

			return maskLayer;
		}

		static nfloat DegreeToRadians (nfloat angle)
		{
			return (nfloat)Math.PI * angle / 180.0f;
		}

		/// <summary>
		/// Gets the rounded rectangle common to both sides.
		/// </summary>
		/// <returns>The rounded rectangle common to both sides.</returns>
		/// <param name="frame">Frame.</param>
		/// <param name="cornerRadius">Corner radius.</param>
		static UIBezierPath GetRoundedRectangleCommonToBothSides(CGRect frame, float cornerRadius)
		{
			var path = new UIBezierPath ();

			// Draw common sides first

			// Left line
			path.MoveTo (new CGPoint (frame.Left, frame.Bottom - cornerRadius));
			path.AddLineTo (new CGPoint (frame.Left, frame.Top + cornerRadius));

			// Top-left rounded corner
			path.AddArc (
				new CGPoint (frame.Left + cornerRadius, frame.Top + cornerRadius),
				cornerRadius,
				DegreeToRadians (180),
				DegreeToRadians (270),
				true
			);

			// Top line
			path.AddLineTo (new CGPoint (frame.Right - cornerRadius, frame.Top));

			// Top-right rounded corner
			path.AddArc (
				new CGPoint (frame.Right - cornerRadius, frame.Top + cornerRadius),
				cornerRadius,
				DegreeToRadians (270),
				DegreeToRadians (360),
				true
			);

			// Right line
			path.AddLineTo (new CGPoint (frame.Right, frame.Bottom - cornerRadius));

			// Bottom line
			path.MoveTo (new CGPoint (frame.Left + cornerRadius, frame.Bottom));
			path.AddLineTo (new CGPoint (frame.Right - cornerRadius, frame.Bottom));

			/*
			 *   =============================
			 *  =                             =
			 * =                               =
			 * =                               =
			 * =    this is what we get        =
			 * =		 in the end            =
			 * =                               =
			 * =                               =
			 * 
			 *   =============================
			 */

			return path;
		}

		const float BOTTOM_CURVE_DEGREES_FOR_CELL_WITH_QUOTE = 40;

		/// <summary>
		/// Gets the rounded rectangle for bubble with quote.
		/// </summary>
		/// <returns>The rounded rectangle for bubble with quote.</returns>
		/// <param name="frame">Frame.</param>
		/// <param name="cornerRadius">Corner radius.</param>
		/// <param name="leftSide">If set to <c>true</c> left side.</param>
		static UIBezierPath GetRoundedRectangleForBubbleWithQuote (CGRect frame, float cornerRadius, bool leftSide)
		{
			var path = GetRoundedRectangleCommonToBothSides (frame, cornerRadius);

			if (leftSide) {
				// Add bottom-right rounded corner
				path.MoveTo (new CGPoint (frame.Right, frame.Bottom - cornerRadius));
				path.AddArc (
					new CGPoint (frame.Right - cornerRadius, frame.Bottom - cornerRadius),
					cornerRadius,
					DegreeToRadians (0),
					DegreeToRadians (90),
					true
				);

				// Add rounded curve to left
				//path.MoveTo (new CGPoint (frame.X + cornerRadius, frame.Height));
				path.AddArc (
					new CGPoint (frame.Left + cornerRadius, frame.Bottom - cornerRadius),
					cornerRadius,
					DegreeToRadians (90),
					DegreeToRadians (90 + BOTTOM_CURVE_DEGREES_FOR_CELL_WITH_QUOTE),
					true
				);
			} else {
				// Add bottom-left rounded corner
				path.MoveTo (new CGPoint (frame.Left, frame.Bottom - cornerRadius));
				path.AddArc (
					new CGPoint (frame.Left + cornerRadius, frame.Bottom - cornerRadius),
					cornerRadius,
					DegreeToRadians (180),
					DegreeToRadians (90),
					false
				);

				// Add rounded curve to right
				path.AddArc (
					new CGPoint (frame.Right - cornerRadius, frame.Bottom - cornerRadius),
					cornerRadius,
					DegreeToRadians (90),
					DegreeToRadians (90 - BOTTOM_CURVE_DEGREES_FOR_CELL_WITH_QUOTE),
					false
				);
			}
			return path;
		}

		/// <summary>
		/// Tries to get the send email view controller.
		/// </summary>
		/// <returns>The get send email view controller.</returns>
		/// <param name="subject">Subject.</param>
		/// <param name="body">Body.</param>
		/// <param name="isHtml">If set to <c>true</c> is html.</param>
		internal static MFMailComposeViewController TryGetSendEmailViewController (string[] recipients, string subject, string body, bool isHtml)
		{
			try {
				if (MFMailComposeViewController.CanSendMail) {
					var m = new MFMailComposeViewController ();
					m.SetSubject (subject);
					m.SetMessageBody (body, isHtml);

					m.Finished += (sender, args) => {
						LLogger.WriteLine (args.Result.ToString ());

						if(args.Result == MessageUI.MFMailComposeResult.Sent) {
							BlrtUtil.TrackImpression("blrt_email");
						}
					};

					if (recipients != null && recipients.Any ()) {
						m.SetToRecipients (recipients);
					}

					return m;
				}

				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), 
					new SimpleAlert (
						platforHelper.Translate ("cannot_send_email_error_title"),
						platforHelper.Translate ("cannot_send_email_error_message"),
						platforHelper.Translate ("cannot_send_email_error_cancel")
					)
				);
				throw new NotSupportedException ("Cannnot Send Mail");

			} catch {
				return null;
			}
		}

		/// <summary>
		/// Tries to get send sms view controller.
		/// </summary>
		/// <returns>The get send sms view controller.</returns>
		/// <param name="itemName">Item name.</param>
		/// <param name="url">URL.</param>
		internal static MFMessageComposeViewController TryGetSendSmsViewController (string bodyTemplate, string itemName, string url)
		{
			if (MFMessageComposeViewController.CanSendText) {

				var m = new MFMessageComposeViewController {
					Body = string.Format (bodyTemplate, itemName, url)
				};

				m.Finished += (sender, args) => {
					LLogger.WriteLine (args.Result.ToString ());

					if(args.Result == MessageUI.MessageComposeResult.Sent) {
						BlrtUtil.TrackImpression("blrt_sms");
					}
				};

				return m;

			}

			var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
			BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), 
				new SimpleAlert (
					platforHelper.Translate ("alert_enable_messages_title",         "share"),
					platforHelper.Translate ("alert_enable_messages_message", 		"share"),
					platforHelper.Translate ("alert_enable_messages_cancel", 		"share")
				)
			);
			
			return null;
		}

		/// <summary>
		/// Creates the message bubble with quote.
		/// </summary>
		/// <returns>The message bubble with quote.</returns>
		/// <param name="frame">Frame.</param>
		/// <param name="cornerRadius">Corner radius.</param>
		/// <param name="quoteLength">Quote length.</param>
		/// <param name="leftSide">If set to <c>true</c> left side.</param>
		public static UIBezierPath CreateMessageBubbleWithQuote (CGRect frame, 
		                                                         float cornerRadius, 
		                                                         float quoteLength, 
		                                                         bool leftSide)
		{
			/* 
			 * Vertical sections like left line and right line need to be atleast the size of corner radius. 
			 * This is the standard used by iOS.
			 */
			if (cornerRadius * 3 > frame.Height) {
				cornerRadius = (float)frame.Height * 0.5f;
			}

			// Get frame with quote length excluded
			var _frame = new CGRect (
				leftSide ? frame.X + quoteLength : frame.X, 
                frame.Y,
				frame.Width - quoteLength,
				frame.Height
           );

			var path = GetRoundedRectangleForBubbleWithQuote (_frame, cornerRadius, leftSide);

			if (leftSide) {
				
				path.AddCurveToPoint (
					new CGPoint (_frame.Left - quoteLength, _frame.Bottom),
					new CGPoint (_frame.Left - cornerRadius * 0.15, _frame.Bottom),
					new CGPoint (_frame.Left - quoteLength, _frame.Bottom)
				);

				path.AddCurveToPoint (
					new CGPoint (_frame.Left, _frame.Bottom - cornerRadius),
					new CGPoint (_frame.Left - cornerRadius * 0.1, _frame.Bottom - cornerRadius * 0.25),
					new CGPoint (_frame.Left, _frame.Bottom - cornerRadius)
				);

				path.MoveTo (new CGPoint (_frame.Right, _frame.Bottom - cornerRadius));

			} else {
				
				path.AddCurveToPoint (
					new CGPoint (_frame.Right + quoteLength, _frame.Bottom),
					new CGPoint (_frame.Right - cornerRadius * 0.15, _frame.Bottom),
					new CGPoint (_frame.Right + quoteLength, _frame.Bottom)
				);

				path.AddCurveToPoint (
					new CGPoint (_frame.Right, _frame.Bottom - cornerRadius),
					new CGPoint (_frame.Right - cornerRadius * 0.1, _frame.Bottom - cornerRadius * 0.25),
					new CGPoint (_frame.Right, _frame.Bottom - cornerRadius)
				);

				path.MoveTo (new CGPoint (_frame.X, _frame.Bottom - cornerRadius));
			}

			path.ClosePath ();
			path.AddClip ();

			return path;
		}

		/// <summary>
		/// Creates the message bubble without quote.
		/// </summary>
		/// <returns>The message bubble without quote.</returns>
		/// <param name="frame">Frame.</param>
		/// <param name="cornerRadius">Corner radius.</param>
		/// <param name="quoteLength">Quote length.</param>
		/// <param name="leftSide">If set to <c>true</c> left side.</param>
		public static UIBezierPath CreateMessageBubbleWithOutQuote(CGRect frame, 
		                                                           float cornerRadius, 
		                                                           float quoteLength, 
		                                                           bool leftSide)
		{
			var path = UIBezierPath.FromRoundedRect (
				new CGRect (
					frame.X + (leftSide ? quoteLength : 0), 
					frame.Y,
					frame.Width - quoteLength,
					frame.Height
				),
				UIRectCorner.AllCorners,
				new CGSize (cornerRadius, cornerRadius)
			);

			path.ClosePath ();
			path.AddClip ();

			return path;
		}
		//public static UIBezierPath CreateMessageBubble (CGRect frame, float cornerRaduis, float quoteBubbleLength, bool leftSide)
		//{	
		//	//var shape = CreateMessageBubbleWithQuote (frame, cornerRaduis, quoteBubbleLength, leftSide);
		//	//return shape;

		//	bool left = leftSide;

		//	UIBezierPath roundedPath = UIBezierPath.FromRoundedRect (
		//		new CGRect (frame.X + (left ? quoteBubbleLength : 0), frame.Y, frame.Width - quoteBubbleLength, frame.Height), 
		//		UIRectCorner.AllCorners, 
		//		new CGSize (cornerRaduis, cornerRaduis)
		//	);

		//	roundedPath.UsesEvenOddFillRule = false;

		//	if (left) {
		//		roundedPath.MoveTo (new CGPoint (quoteBubbleLength + cornerRaduis, frame.Height - cornerRaduis));
		//		roundedPath.AddCurveToPoint (
		//			new CGPoint (0, frame.Height), 
		//			new CGPoint (quoteBubbleLength + cornerRaduis * 0.25f, frame.Height),
		//			new CGPoint (0, frame.Height)
		//		);
		//		roundedPath.AddCurveToPoint (
		//			new CGPoint (quoteBubbleLength, frame.Height - cornerRaduis), 
		//			new CGPoint (0, frame.Height),
		//			new CGPoint (quoteBubbleLength, frame.Height - cornerRaduis * 0.5f)
		//		);
		//	} else {
		//		roundedPath.MoveTo (new CGPoint (frame.Width - quoteBubbleLength, frame.Height - cornerRaduis));
		//		roundedPath.AddCurveToPoint (
		//			new CGPoint (frame.Width, frame.Height), 
		//			new CGPoint (frame.Width - quoteBubbleLength, frame.Height - cornerRaduis * 0.5f),
		//			new CGPoint (frame.Width, frame.Height)
		//		);
		//		roundedPath.AddCurveToPoint (
		//			new CGPoint (frame.Width - (quoteBubbleLength + cornerRaduis), frame.Height - cornerRaduis), 
		//			new CGPoint (frame.Width, frame.Height),
		//			new CGPoint (frame.Width - (quoteBubbleLength + cornerRaduis * 0.25f), frame.Height)
		//		);
		//	}

		//	roundedPath.ClosePath ();
		//	roundedPath.AddClip ();

		//	return roundedPath;
		//}

		public static CoreAnimation.CAShapeLayer CreateMessageBubbleMask (CGRect frame, float cornerRaduis, float quoteBubbleLength, bool leftSide)
		{
			UIBezierPath path = CreateMessageBubbleWithQuote (frame, cornerRaduis, quoteBubbleLength, leftSide);
			//roundedPath

			var maskLayer = new CoreAnimation.CAShapeLayer ();
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = path.CGPath;

			return maskLayer;
		}
	}
}

