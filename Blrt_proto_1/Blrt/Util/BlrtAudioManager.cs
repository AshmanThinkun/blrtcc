//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.IO;
//using MonoTouch.Foundation;
//using MonoTouch.AVFoundation;
//
//using BlrtData;
//
//namespace BlrtiOS.Util
//{
//	public class BlrtAudioManager
//	{
//		public const string EXTENSION = "m4a";
////		public const string DRAFT_FILE = "draft." + EXTENSION;
//		private float _duration;
//		private bool _audioRecording = false;
//		private bool _audioPlaying = false;
//
//		public float Duration {
//			get {
//				if (_audioRecorder != null && _audioRecorder.Recording)
//					return BlrtPermissions.MaxBlrtDuration;
//				return _duration;
//			}
//			protected set {
//				if (_duration != value) {
//					_duration = value;
//				}
//			}
//		}
//
//
//		private double _lastTime = 0;
//
//		public double CurrentTime {
//			get {
//				if (_audioPlayer != null) {
//					return _lastTime = Math.Max(_lastTime, _audioPlayer.CurrentTime);
//				}
//				if (_audioRecorder != null)
//					return _audioRecorder.currentTime;
//
//				return 0;
//			}
//		}
//
//		public bool NoAudio {
//			get;
//			private set;
//		}
//
//		public bool Active {
//			get {
//
//				if (_audioPlayer != null && _audioPlayer.Playing)
//					return true;
//				if (_audioRecorder != null && _audioRecorder.Recording)
//					return true;
//
//				return false;
//			}
//		}
//
//		public string AudioPath { get { return _audioPath; } }
//
//		public EventHandler PlaybackStopped { get; set; }
//
//		public EventHandler RecordingStarted{ get; set;}
//		public EventHandler RecordingStopped{ get; set;}
//
//		private AudioSettings _audioSettings;
//		private AVAudioRecorder _audioRecorder;
//		private NSError _audioRecorderError;
//		private AVAudioPlayer _audioPlayer;
//		private string _audioPath;
//
//		public BlrtAudioManager ()
//		{
//			_audioRecorderError = new NSError (new NSString ("Blrt_proto_1"), 1);
//
//			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out _audioRecorderError);
//			AVAudioSession.SharedInstance ().SetActive (true, out _audioRecorderError);
//
//			NSDictionary values = new NSDictionary (
//				AVAudioSettings.AVSampleRateKey,			NSNumber.FromFloat (44100f / 4), //Sample Rate
//				AVAudioSettings.AVFormatIDKey,				NSNumber.FromInt32 ((int)MonoTouch.AudioToolbox.AudioFormatType.MPEG4AAC),
//				AVAudioSettings.AVNumberOfChannelsKey,		NSNumber.FromInt32 (1),  //Channels
//				AVAudioSettings.AVEncoderAudioQualityKey,	NSNumber.FromInt32 ((int)MonoTouch.AudioToolbox.AudioConverterQuality.Max)  //Channels
//			);
//
//			_audioSettings = new AudioSettings (values);
//		}
//
//		public Stream GetAudioStream ()
//		{
//			return NSData.FromFile (_audioPath).AsStream ();
//
//		}
//
//		public void LoadDraft (string audioPath)
//		{
//			if (_audioPlayer != null) {
//				_audioPlayer.Dispose ();
//				_audioPlayer = null;
//			}
//			/////// OLD
////			_audioPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), DRAFT_FILE);
//			/////// NEW
//			_audioPath = audioPath;
//			/////// END
//
//
//			NoAudio = true;
//		}
//
//		public void LoadAudio (string path)
//		{
//			if (_audioPlayer != null) {
//				_audioPlayer.Dispose ();
//				_audioPlayer = null;
//			}
//
//			// Changed follow the JIRA commit
//			//_audioPath = DRAFT_FILE;
//			_audioPath = path;
//
//			OpenAudioPlayer ();
//
//			if (_audioRecorderError != null) {
//				throw new Exception ("Fail to open audio file");
//			} 
//
//			NoAudio = (_audioPlayer.Duration == 0);
//		}
//
//		private bool OpenAudioPlayer ()
//		{
//
//			if (_audioPlayer != null)
//				return true;
//
//			if (_audioPlayer == null) {
//
//				try{
//
//					_audioPlayer = AVAudioPlayer.FromUrl (NSUrl.FromFilename (_audioPath), out _audioRecorderError);
//				} catch (Exception e){
//					throw e;
//				}
//				if (_audioPlayer == null) {
//					Duration = 0;
//					return false;
//				}
//			}
//
//			double dt = 0;
//
//			for (int i = 0; i < 16 && dt != _audioPlayer.Duration; ++i) {
//				_audioPlayer.CurrentTime = dt = _audioPlayer.Duration;
//				_audioPlayer.PrepareToPlay ();
//			}
//
//
//			Duration = (float)_audioPlayer.Duration;
//			_audioPlayer.CurrentTime = 0;
//
//
//			_audioPlayer.FinishedPlaying += delegate {
//				InternalPlaybackStop (true);
//			};
//
//			return true;
//		}
//
//		public void SetToPlayBack(){
//			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayback, out _audioRecorderError);
//		}
//
//		public void SetToRecord(){
//			AVAudioSession.SharedInstance ().SetCategory (AVAudioSession.CategoryPlayAndRecord, out _audioRecorderError);
//		}
//
//
//
//		public void Play ()
//		{
//			PlayFrom (null);
//		}
//
//		public void PlayFrom (double? time)
//		{
//
//			if (NoAudio)
//				throw new NullReferenceException ("Fail to open audio file");
//
//			SetToPlayBack ();
//
//			if (_audioRecorder != null) {
//				_audioRecorder.Dispose ();
//				_audioRecorder = null;
//			}
//			if (_audioPlayer == null) {
//
//				OpenAudioPlayer ();
//
//				if (_audioPlayer == null)
//					throw new NullReferenceException ("Fail to open audio file");
//			}
//
//			if (time != null)
//				_audioPlayer.CurrentTime = (time ?? 0);
//
//			_audioPlaying = true;
//
//			_audioPlayer.PrepareToPlay ();
//			_audioPlayer.Play ();
//
//			_lastTime = time ?? 0;
//
//			Duration = (float)_audioPlayer.Duration;
//		}
//
//		public void Record ()
//		{
//
//			if ((null != _audioRecorder) && _audioRecording && (!_audioRecorder.Recording)) {
//				_audioRecorder.Record ();
//				return;
//			}
//
//			if (_audioPlayer != null) {
//				_audioPlayer.Dispose ();
//				_audioPlayer = null;
//			}
//
//			SetToRecord ();
//
//			if (_audioRecorder == null) {
////				_audioPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), DRAFT_FILE);
//				_audioRecorder = AVAudioRecorder.ToUrl (NSUrl.FromFilename (_audioPath), _audioSettings.Dictionary, out _audioRecorderError);
//
//				_audioRecorder.BeginInterruption += (object sender, EventArgs e) => {
//					InternalRecordStop (true);
//				};
//				_audioRecorder.FinishedRecording += (object sender, AVStatusEventArgs e) => {
//					InternalRecordStop (true);
//				};
//
//			}
//
//			// extra 0.1 to push it over into the next second
//			float maxLength = 100000000000;//BlrtAccountSettings.MaxBlrtDuration + 0.15f;
//
//			_audioRecorder.PrepareToRecord ();
//			_audioRecorder.MeteringEnabled = true;
//			_audioRecorder.RecordFor (_audioRecording ? maxLength - _audioRecorder.currentTime : maxLength);
//			NoAudio = false;
//			_audioRecording = true;
//			if (RecordingStarted != null) {
//				RecordingStarted (this, EventArgs.Empty);
//			}
//		}
//
//		public void PauseRecord()
//		{
//			if ((null != _audioRecorder) && _audioRecording && (_audioRecorder.Recording)) {
//				_audioRecorder.Pause ();
//			}
//		}
//
//		public void StopPlayback ()
//		{
//			InternalPlaybackStop (false);
//		}
//
//		public void StopRecord ()
//		{
//			InternalRecordStop (false);
//		}
//
//		private void InternalPlaybackStop (bool fireEvent)
//		{
//			if (_audioPlayer != null && (_audioPlayer.Playing || _audioPlaying)) {
//				_audioPlaying = false;
//				_audioPlayer.Pause ();
//
//				if (fireEvent && PlaybackStopped != null)
//					PlaybackStopped (this, EventArgs.Empty);
//			}
//
//		}
//		private void InternalRecordStop (bool fireEvent)
//		{
//			if (_audioRecorder != null && (_audioRecorder.Recording || _audioRecording)) {
//				_audioRecording = false;
//				Duration = (float)_audioRecorder.currentTime;
//				_audioRecorder.Stop ();
//
//				if (fireEvent && RecordingStopped != null)
//					RecordingStopped (this, EventArgs.Empty);
//			}
//
//		}
//
//		float _avg = 0;
//
//		public float GetRecordingVolume ()
//		{
//			uint channel = 0;
//
//			if (_audioRecorder != null && _audioRecorder.Recording) {
//				_audioRecorder.UpdateMeters ();
//
//
//				const float Alpha = 0.9f;
//				const float Beta = 0.25f;
//
//				var value = _audioRecorder.PeakPower (channel) * Beta + _audioRecorder.AveragePower (channel) * (1 - Beta);
//				//var value = _audioRecorder.AveragePower (channel);
//
//				_avg = (float)(Alpha * value + (1.0 - Alpha) * _avg);
//
//				//double peakPowerForChannel = Math.Pow(10, (0.05 * value));
//				//_avg = (float)(ALPHA * peakPowerForChannel + (1.0 - ALPHA) * _avg);
//			} else {
//				_avg = -50;
//			}
//
//			return _avg;
//		}
//	}
//}
