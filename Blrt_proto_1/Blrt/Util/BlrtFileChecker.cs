using System;
using Foundation;
using System.Threading;
using System.IO;
using System.Net;
using UIKit;
using AssetsLibrary;

namespace BlrtiOS.Util
{
	public static class BlrtFileChecker
	{
		public delegate void FoundFileSize (FileCheckerStatus status, long fileSize);
		public enum FileCheckerStatus
		{
			Success,
			NotFound,
			Timeout,
			Cancelled,
			UnknownError,
			NoInternetAccess
		}

		/// <summary>
		/// Gets the size of the file.
		/// </summary>
		/// <param name="nsUrl">Ns URL.</param>
		/// <param name="callback">Callback.</param>
		static public void GetFileSize (NSUrl nsUrl, FoundFileSize callback)
		{
			if (nsUrl.IsFileUrl) {
				try {
					FileInfo fileInfo = new FileInfo (nsUrl.Path);
					callback (FileCheckerStatus.Success, fileInfo.Length);
				} catch (Exception noFileException) {
					callback (FileCheckerStatus.NotFound, 0);
				}
			} else if (nsUrl.AbsoluteString.StartsWith ("assets-library://")) {
				// When the nsUrl points to a local file
				using (ALAssetsLibrary library = new ALAssetsLibrary ()) {
					library.AssetForUrl (nsUrl, (ALAsset asset) => {
						ALAssetRepresentation representation = asset.DefaultRepresentation;
						callback (FileCheckerStatus.Success, representation.Size);
					}, (NSError e) => {
						callback (FileCheckerStatus.NotFound, 0);
					});
				}
			} else {
				// When the nsurls points to a website
				WebClient obj = new WebClient ();
				NetworkStatus internetStatus = Reachability.InternetConnectionStatus ();
				if (internetStatus == NetworkStatus.NotReachable) {
					callback (FileCheckerStatus.NoInternetAccess, 0);
					return;
				}
				obj.OpenReadCompleted += (object sender, OpenReadCompletedEventArgs e) => {
					if (e.Error == null) {
						Stream s = e.Result;

						callback (FileCheckerStatus.Success, e.Result.Length);
					} else if (e.Cancelled) {
						callback (FileCheckerStatus.Cancelled, 0);

					} else {

						var errorCode = FileCheckerStatus.UnknownError;

						if (e.Error is WebException) {
					
							var webError = e.Error as WebException;
							if ( webError.Status == WebExceptionStatus.ProtocolError ) {
								errorCode = FileCheckerStatus.NotFound;
							} else if ( webError.Status == WebExceptionStatus.Timeout ) {
								errorCode = FileCheckerStatus.Timeout;
							} else {
								errorCode = FileCheckerStatus.UnknownError;
							}
							callback (errorCode, 0);
						} 

					}
				};
				obj.OpenReadAsync (nsUrl);
			}
		}


	}
}

