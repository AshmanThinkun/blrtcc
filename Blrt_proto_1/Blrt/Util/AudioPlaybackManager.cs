﻿using System;
using System.Threading.Tasks;
using BlrtiOS.ViewControllers.Canvas;

namespace BlrtiOS
{
	public class AudioPlaybackManager : BlrtShared.AbstractAudioPlaybackManager
	{
		static AudioPlaybackManager _audioManager;

		public static AudioPlaybackManager AudioManager {
			get {
				if (_audioManager == null) {
					_audioManager = new AudioPlaybackManager ();
				}
				return _audioManager;
			}
		}

		AudioPlaybackManager ()
		{
			AudioPlayer = new BCAudioPlayer ();
			CurrentContentId = string.Empty;
		}

		public override float CurrentTime ()
		{
			return AudioPlayer.CurrentTime;
		}

		public override void Pause ()
		{
			if (!IsAudioPlaying) {
				return;
			}

			AudioPlayer.Pause();
			IsAudioPlaying = false;
		}

		public override async Task Play (string contentId, string audioFilePath)
		{
			if (CurrentContentId.Equals (contentId)) {
				if (IsAudioPlaying) {
					return;
				}

				AudioPlayer.Play ();
				IsAudioPlaying = true;
				return;
			}

			CurrentContentId = contentId;
			AudioPlayer.ReleaseFile ();
			await AudioPlayer.LoadAudio (audioFilePath);
			AudioPlayer.Play ();
			IsAudioPlaying = true;
		}

		public override void Seek (float time)
		{
			AudioPlayer.Seek(time);
		}
	}
}
