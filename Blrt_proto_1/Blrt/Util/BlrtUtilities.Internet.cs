﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Foundation;
using Plugin.Connectivity;

namespace BlrtiOS.Util
{
	/// <summary>
	/// This class contains a bunch of useful static function related to internet.
	/// </summary>

	public static partial class BlrtUtilities
	{
		public static readonly int MAX_API_CALL_RETRIES = 2;

		static readonly string SERVER_ADDRESS = BlrtData.BlrtConstants.ServerUrl;
		static readonly string INTERNET_CHECK_ADDRESS = "https://github.com";

		public static string NoInternetSourceErrorTitle {
			get {
				return BlrtData.BlrtUtil.PlatformHelper.Translate ("no_internet_source_title", "internet");
			}
		}

		public static string NoInternetSourceErrorMessage {
			get {
				return BlrtData.BlrtUtil.PlatformHelper.Translate ("no_internet_source_message", "internet");
			} 
		}

		public static string NoInternetErrorTitle {
			get {
				return BlrtData.BlrtUtil.PlatformHelper.Translate ("no_internet_title", "internet");
			}
		}

		public static string NoInternetErrorMessage {
			get {
				return BlrtData.BlrtUtil.PlatformHelper.Translate ("no_internet_message", "internet");
			}
		}

		public static string ServerUnreachableErrorMessage { 
			get {
				return BlrtData.BlrtUtil.PlatformHelper.Translate ("server_unreachable_message", "internet");
			} 
		}

		public static bool IsConnectedToInternetSource {
			get {
				return CrossConnectivity.Current.IsConnected;
			}
		}

		public static async Task<Tuple<bool, WebException>> PingUrl (string url, int timeout) {
			//var urlRequest = new NSUrlRequest (new NSUrl (url), NSUrlRequestCachePolicy.ReloadIgnoringCacheData, timeout);
			//var urlSession = NSUrlSession.SharedSession;
			//try {
			//	await urlSession.CreateDataTaskAsync (urlRequest);
			//	return true;
			//} catch {
			//	return false;
			//}
			try {
				if (!IsConnectedToInternetSource) {
					throw new WebException ("Device appears to be disconnected", WebExceptionStatus.NameResolutionFailure);
				}

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create (new Uri (url));
				request.Timeout = timeout;
				request.ReadWriteTimeout = timeout;
				using (var res = request.GetResponse ())
				return new Tuple<bool, WebException> (true, null);
			} catch (WebException e) {
				return new Tuple<bool, WebException> (false, e);
			}
		}

		public static async Task<Tuple<bool, WebException>> IsConnectedToInternet ()
		{
			var tcs = new TaskCompletionSource<Tuple<bool, WebException>> ();
			Foundation.NSObject.InvokeInBackground (async delegate {
				tcs.SetResult(await PingUrl (INTERNET_CHECK_ADDRESS, 5000));
			});
			return await tcs.Task;
		}

		public static async Task<Tuple<bool, WebException>> IsServerReachable () 
		{
			var tcs = new TaskCompletionSource<Tuple<bool, WebException>> ();
			Foundation.NSObject.InvokeInBackground (async delegate {
				tcs.SetResult (await PingUrl (SERVER_ADDRESS, 5000));
			});
			return await tcs.Task;
		}
	}
}