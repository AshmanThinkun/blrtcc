//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Xamarin.Auth;
//using UIKit;
//
//namespace BlrtiOS.Twitter
//{
//	public static class TwitterManager
//	{
//		const string TwitterRequestTokenUrl = "https://api.twitter.com/oauth/request_token";
//		const string TwitterAuthorizeUrl 	= "https://api.twitter.com/oauth/authorize";
//		const string TwitterAccessTokenUrl 	= "https://api.twitter.com/oauth/access_token";
//
//		static string _consumerKey;
//		static string _consumerSecret;
//		static string _callbackUrl;
//
//		static OAuth1Authenticator _auth;
//		static bool _authenticationInProgress;
//		static ITwitterLoginHandler _loginHandler;
//
//		public static bool Initialized { get{ return !(string.IsNullOrWhiteSpace(_consumerKey) || string.IsNullOrWhiteSpace(_consumerSecret)); }}
//		public static string CallbackUrl { get{ return _callbackUrl;} }
//
//
//
//
//		public static void Initialize(string consumerKey, string consumerSecret){
//
//			if (Initialized)
//				throw new TwitterManagerException (TwitterManagerException.ErrorCode.AlreadyInitialized);
//
//			_consumerKey = consumerKey;
//			_consumerSecret = consumerSecret;
//		}
//
//		public static void SetCallbackUrl(string callbackUrl){
//
//			CheckState ();
//
//			_callbackUrl = callbackUrl;
//		}
//
//		static void CheckState ()
//		{
//			if (!Initialized)
//				throw new TwitterManagerException (TwitterManagerException.ErrorCode.NotInitialized);
//
//			if (_authenticationInProgress)
//				throw new TwitterManagerException (TwitterManagerException.ErrorCode.AuthenticationInProgress);
//		}
//
//
//		public static void Authenticate(ITwitterLoginHandler handler, UIViewController parentViewController){
//
//			CheckState ();
//
//			try{
//				_loginHandler = handler;
//
//				_authenticationInProgress = true;
//				_auth = new OAuth1Authenticator(
//					_consumerKey,
//					_consumerSecret,
//					new Uri(TwitterRequestTokenUrl),
//					new Uri(TwitterAuthorizeUrl),
//					new Uri(TwitterAccessTokenUrl),
//					new Uri(_callbackUrl),
//					null
//				);
//				_auth.Completed += AuthCompleted;
//				_auth.Error 	+= AuthError;
//
//
//				parentViewController.PresentViewController(_auth.GetUI(), true, null);
//
//
//
//			} catch (Exception e) {
//				throw e;
//			}
//		}
//
//		static void AuthCompleted(object sender, AuthenticatorCompletedEventArgs e) {
//
//			_authenticationInProgress = false;
//
//			if (e.IsAuthenticated) {
//
//
//
//
//				if (_loginHandler != null)
//					_loginHandler.TwitterLoginSuccessful();
//
//			} else {							
//
//				if (_loginHandler != null)
//					_loginHandler.TwitterLoginFailed(new TwitterManagerException (TwitterManagerException.ErrorCode.UserCancelled));
//			}
//		}
//
//		static void AuthError(object sender, AuthenticatorErrorEventArgs e) {
//
//			_authenticationInProgress = false;
//
//			if (_loginHandler != null)
//				_loginHandler.TwitterLoginFailed(e.Exception);
//		}
//	}
//
//
//	public interface ITwitterLoginHandler{
//	
//		void TwitterLoginSuccessful();
//		void TwitterLoginFailed(Exception e);
//	
//	}
//
//
//	public class TwitterManagerException : Exception{
//
//
//		public ErrorCode Code { get; private set;}
//
//		public TwitterManagerException(ErrorCode code){
//			Code = code;
//		}
//
//
//		public enum ErrorCode{
//			AuthenticationInProgress,
//
//			NotInitialized,
//			AlreadyInitialized,
//
//			NoInternet,
//			UserCancelled
//		}
//	}
//}
//
