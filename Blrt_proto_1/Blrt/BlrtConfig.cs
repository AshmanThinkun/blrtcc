using System;
using System.IO;

using Foundation;
using UIKit;

using BlrtData;

namespace BlrtiOS
{
	public static class BlrtConfig
	{
		public static readonly string APP_VERSION	= NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
		public static readonly string APP_NAME		= NSBundle.MainBundle.InfoDictionary["CFBundleDisplayName"].ToString();


		public const float JPEG_COMPRESSION 				= 0.60f;


		public const float TooltipMaxWidth 					= 220;
		public const float TooltipMaxHeight 				= 96;


		public readonly static UIColor TEXT_COLOR 						= new UIColor(0.2f, 0.2f, 0.2f, 1);
		public readonly static UIColor BLRT_BACKGROUND_COLOR 			= new UIColor(0.80f, 0.80f, 0.80f, 1);
		public readonly static UIColor LIGHT_TEXT 						= UIColor.FromHSB(0, 0, 0.25f);


		public readonly static UIColor BlrtGreen					 	= UIColor.FromRGB(0, 		231, 	168);
		public readonly static UIColor BlrtBlack						= UIColor.FromRGB(44, 		53, 	62);
		public readonly static UIColor BLRT_RED 						= UIColor.FromRGB(226,	 	26, 	69);
		public readonly static UIColor BLRT_BLUE						= UIColor.FromRGB(123, 		151, 	173);
		public readonly static UIColor BLRT_PINK						= UIColor.FromRGB(243, 		230, 	233);


		public readonly static UIColor BLRT_LIGHT_GRAY					= UIColor.FromRGB(249, 	249, 	249);
		public readonly static UIColor BLRT_MID_GRAY					= UIColor.FromRGB(239, 	239, 	239);
		public readonly static UIColor BLRT_DARK_GRAY					= UIColor.FromRGB(200, 	199, 	204);

		public readonly static UIColor BlrtTextGray						= UIColor.FromRGB(145, 	145, 	145);



		public readonly static UIColor BLRT_TRANSPARENT_BLACK			= BlrtBlack.ColorWithAlpha(0.9f);
		public readonly static UIColor BLRT_TRANSPARENT_WHITE			= BLRT_MID_GRAY.ColorWithAlpha(0.9f);




		public readonly static UIColor BLRT_IOS_NEW 					= UIColor.Red;
		public readonly static UIColor BLRT_NEW 						= BLRT_RED;
		public readonly static UIColor BLRT_ALERT 						= BLRT_RED;
		public readonly static UIColor BLRT_ACTION 						= BlrtBlack;

		public readonly static UIColor BLRT_DARK_TEXT 					= BlrtBlack;
		public readonly static UIColor BLRT_LIGHT_TEXT 					= BLRT_LIGHT_GRAY;

		public readonly static UIColor BLRT_COMMENTS_GRAY				= BLRT_MID_GRAY;
		public readonly static UIColor BLRT_COMMENTS_RED				= BLRT_PINK;
		public readonly static UIColor BLRT_SEPARATOR					= BLRT_MID_GRAY;

		public readonly static UIColor BLRT_TABBAR_BACKGROUND		 	= BlrtBlack;
		public readonly static UIColor BLRT_TABBAR_TINT 				= BlrtGreen;
		public readonly static UIColor BLRT_NAVIGATION_BACKGROUND 		= BlrtGreen;
		public readonly static UIColor BLRT_NAVIGATION_TINT 			= BlrtBlack;

		public readonly static UIColor BLRT_TITLE_COLOR		 			= null;

		public readonly static UIColor BLRT_USER_BLRT_BACKGROUND 		= BlrtBlack;
		public readonly static UIColor BLRT_OTHER_BLRT_BACKGROUND		= BLRT_RED;

		public readonly static UIColor LIGHT_BACKGROUND_COLOR			= UIColor.White;



		public readonly static FontSet ThinFont 						= new FontSet("HelveticaNeue-Thin");

		public readonly static FontSet NormalFont 						= new FontSet();
		public readonly static FontSet LightFont 						= new FontSet("HelveticaNeue-Light");
		public readonly static FontSet MediumFont 						= new FontSet("HelveticaNeue-Medium");
		public readonly static FontSet BoldFont 						= new FontSet("HelveticaNeue-Bold");

		public readonly static UIFont TINY_FONT 						= UIFont.SystemFontOfSize(12);
		public readonly static UIFont BASE_TEXT_FONT		 			= UIFont.SystemFontOfSize(14);
		public readonly static UIFont LARGE_TEXT_FONT 					= UIFont.SystemFontOfSize(16);
		public readonly static UIFont ACTION_FONT 						= UIFont.FromName("HelveticaNeue-Medium",16);
		public readonly static UIFont TITLE_FONT 						= UIFont.SystemFontOfSize(20);

		public readonly static UIFont BUTTON_FONT						= ACTION_FONT;
		public readonly static UIFont NAV_TITLE_FONT					= UIFont.FromName("HelveticaNeue-Medium",17);
		public readonly static UIFont BOTTOM_TEXT_FONT					= UIFont.FromName("HelveticaNeue-Thin",19);
		public readonly static UIFont BOTTOM_BTN_FONT					= UIFont.FromName("HelveticaNeue-Bold",19);
		public readonly static UIFont SUBTITLE_FONT						= UIFont.FromName("HelveticaNeue-Light",34);




		public readonly static UIFont BASE_TEXT_BOLD_FONT 				= UIFont.BoldSystemFontOfSize(BASE_TEXT_FONT.PointSize);
		public readonly static UIFont LARGE_TEXT_BOLD_FONT 				= UIFont.BoldSystemFontOfSize(LARGE_TEXT_FONT.PointSize);
		public readonly static UIFont TITLE_BOLD_FONT 					= UIFont.BoldSystemFontOfSize(TITLE_FONT.PointSize);



		public static bool IsIOS7(){
			return UIDevice.CurrentDevice.CheckSystemVersion (7, 0);
		}

		public class FontSet{
			private const float TinyFontSize 		= 12;
			private const float NornalFontSize 		= 14;
			private const float LargeFontSize 		= 16;

			private const float TitleFontSize 		= 34;
			private const float SubtitleFontSize 	= 20;

			public readonly UIFont Tiny;
			public readonly UIFont Normal;
			public readonly UIFont Large;

			public readonly UIFont Title;
			public readonly UIFont SubTitle;

			public FontSet(){
				Tiny			= UIFont.SystemFontOfSize(TinyFontSize);
				Normal			= UIFont.SystemFontOfSize(NornalFontSize);
				Large			= UIFont.SystemFontOfSize(LargeFontSize);

				Title			= UIFont.SystemFontOfSize(TitleFontSize);
				SubTitle		= UIFont.SystemFontOfSize(SubtitleFontSize);			
			}

			public FontSet(string fontName){
				Tiny			= UIFont.FromName(fontName, TinyFontSize);
				Normal			= UIFont.FromName(fontName, NornalFontSize);
				Large			= UIFont.FromName(fontName, LargeFontSize);

				Title			= UIFont.FromName(fontName, TitleFontSize);
				SubTitle		= UIFont.FromName(fontName, SubtitleFontSize);			
			}
		
		}
	}
}

