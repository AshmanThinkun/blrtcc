using System;

namespace Blrt
{
	public interface SendToInterface
	{


		/// <summary>
		/// Gets or sets maxium amount of contact that can be sent to
		/// </summary>
		/// <value>The max contacts.</value>
		int MaxContacts { get; set; }



		bool EditableTitle { get; set; }
		bool EditableDiscription { get; set; }

		bool ShowProgress { get; set; }
		float Progress { get; set; }





		bool CancelEnabled { set; get; }
		bool SendEnabled { set; get; }


		EventHandler SendPressed { get; set; }
		EventHandler CancelPressed { get; set; }
	}
}

