using System;
using UIKit;
using Xamarin;

namespace BlrtiOS
{
	public class Application
	{
		// This is the main entry point of the application
		static void Main (string[] args)
		{
			// If you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here

			Insights.HasPendingCrashReport += (sender, isStartupCrash) =>
			{
				if (isStartupCrash) {
					Insights.PurgePendingCrashReports().Wait();
				}
			};
			Insights.Initialize(BlrtData.BlrtConstants.InsightApiKey);

			//using this if you are debuging
//			Insights.Initialize(Insights.DebugModeKey);


 			UIApplication.Main (args, null, "AppDelegate");
		}
	}
}
