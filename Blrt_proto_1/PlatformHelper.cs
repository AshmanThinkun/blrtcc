using System;
using UIKit;
using BlrtData;
using Foundation;
using System.Threading.Tasks;
using BlrtiOS;
using System.Drawing;
using BlrtiOS.Util;
using System.IO;

[assembly: Xamarin.Forms.Dependency (typeof(PlatformHelper))]
namespace BlrtData
{
	public class PlatformHelper : IPlatformHelper
	{
		public string Translate (string key)
		{
			var result = Vernacular.Catalog.GetString (key);
			if (result == "**BLRT_EMPTY_STRING")
				return "";
			return result;
		}

		public string Translate (string key, string table)
		{
			if (string.IsNullOrEmpty (table))
				return Translate (key);

			table = table.Replace ("_", "45");
			table = table.Replace ("-", "45");

			var result = Vernacular.Catalog.GetString (table + "__" + key);
			if (result == "**BLRT_EMPTY_STRING")
				return "";
			return result;
		}

		public string TranslateDate (DateTime date)
		{
			return BlrtiOS.BlrtHelperiOS.TranslateDate (date);
		}

		public string TranslateDateLong (DateTime date)
		{
			return BlrtiOS.BlrtHelperiOS.TranslateDateLong (date);
		}

		public Xamarin.Forms.FormattedString TranslateTime (DateTime date)
		{
			throw new NotImplementedException ();
		}

		public float GetDeviceWidth ()
		{  
			var width = UIScreen.MainScreen.Bounds.Width;
			return (float)width;
		}

		public float GetDeviceHeight ()
		{
			var height = UIScreen.MainScreen.Bounds.Height;
			return (float)height;
		}

		public bool IsPhone { get { return BlrtiOS.BlrtHelperiOS.IsPhone; } }

		public string GetAppVersion ()
		{
			return NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"].ToString ();
		}

		public string GetPlatformName ()
		{
			return "iOS";
		}

		public void ShowUpgradeDialog (BlrtPermissions.PermissionEnum permission, long value = -1)
		{
			BlrtiOS.BlrtHelperiOS.ShowUpgradeDialog (permission, value);
		}

		public async Task OpenAppStore ()
		{
			var tcs = new TaskCompletionSource<bool>();
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				//appStoreUrl = http://itunes.apple.com/app/id874881426?mt=8
				UIApplication.SharedApplication.OpenUrl (
					new NSUrl (BlrtSettings.AppStoreUrl));
				tcs?.TrySetResult (true);
			});
			await tcs.Task;
		}

		public string CountryCode{
			get{
				var code = Foundation.NSLocale.CurrentLocale.CountryCode;
				if(string.IsNullOrEmpty(code)){
					code = "AU";
				}
				return code;
			}
		}

		public Task<bool> ShowWaitAlert (Task task, string title, string message, string cancel)
		{
			return BlrtHelperiOS.ShowWaitingAlertAsync (task, title, message, cancel);
		}

		public Size GetImageSize (string filePath)
		{
			using (var img = UIImage.FromFile (filePath)) {
				return new Size ((int)(img.Size.Width), (int)(img.Size.Height));
			}
		}

		public async Task<string> CompressLargeImageToJpeg (string path, int maxSize)
		{
			using (var img = UIImage.FromFile (path)) {
				if (img.Size.Width <= maxSize && img.Size.Height <= maxSize) {
					var extStartIdx = path.LastIndexOf ('.');
					string ext = (extStartIdx >= 0) ? path.Remove (0, extStartIdx) : "";
					switch (ext.ToLower()) {
						case ".jpg":
						case ".jpeg":
							return path;
						default:
							var dstFile = BlrtData.BlrtRecentFileManager.GetUniqueFile (".jpg");
							using (var data = img.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
								NSError e = null;
								data.Save (
									dstFile,
									NSDataWritingOptions.Atomic,
									out e
								);
							}
							return dstFile;
					}
				} else {
					var dstFile = BlrtData.BlrtRecentFileManager.GetUniqueFile (".jpg");
					nfloat newWidth = maxSize;
					nfloat newHeight = maxSize;
					if (img.Size.Width > img.Size.Height) {
						newWidth = maxSize;
						newHeight = img.Size.Height / img.Size.Width * newWidth;
					} else {
						newHeight = maxSize;
						newWidth = img.Size.Width / img.Size.Height * newHeight;
					}
					using (var thumbnail = BlrtUtilities.CreateThumbnail (img, newWidth, newHeight)) {
						using (var data = thumbnail.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
							NSError e = null;
							var succeed = data.Save (
								dstFile,
								NSDataWritingOptions.Atomic,
								out e
							);
							return dstFile;
						}
					}
				}
				return null;
			}
		}

		public Task CopyAsJpeg (string src, string dst)
		{
			return Task.Run (() => {
				var ext = src.Remove (0, src.LastIndexOf ('.'));

				switch (ext.ToLower ()) {
				case ".png":
				case ".gif":
				case ".bmp":
					using (var image = UIImage.FromFile (src)) {
						using (var data = image.AsJPEG (BlrtUtil.JPEG_COMPRESSION_QUALITY)) {
							NSError e = null;
							data.Save (
								dst,
								NSDataWritingOptions.Atomic,
								out e
							);
						}
					}
					break;
				case ".jpg":
				case ".jpeg":
					File.Copy (src, dst, true);
					break;
				default:
					throw new BadImageFormatException ();
				}
			});
		}
	}
}

