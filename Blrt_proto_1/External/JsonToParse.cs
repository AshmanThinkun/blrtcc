using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Parse;
using Blrt.Entity;

namespace External
{
	public abstract class FactoryConverter<T> : Newtonsoft.Json.JsonConverter
	{
		/// <summary>
		/// Writes the JSON representation of the object.
		/// </summary>
		/// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
		/// <param name="value">The value.</param>
		/// <param name="serializer">The calling serializer.</param>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotSupportedException("CustomCreationConverter should only be used while deserializing.");
		}

		/// <summary>
		/// Reads the JSON representation of the object.
		/// </summary>
		/// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
		/// <param name="objectType">Type of the object.</param>
		/// <param name="existingValue">The existing value of object being read.</param>
		/// <param name="serializer">The calling serializer.</param>
		/// <returns>The object value.</returns>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
				return null;

			var args = serializer.Deserialize<JContainer> (reader);


			T value = CreateAndPopulate(objectType, args);

			if (value == null)
				throw new JsonSerializationException("No object created.");

			return value;
		}

		/// <summary>
		/// Creates an object which will then be populated by the serializer.
		/// </summary>
		/// <param name="objectType">Type of the object.</param>
		/// <returns></returns>
		public abstract T CreateAndPopulate(Type objectType, JContainer jsonFields);

		/// <summary>
		/// Determines whether this instance can convert the specified object type.
		/// </summary>
		/// <param name="objectType">Type of the object.</param>
		/// <returns>
		///     <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanConvert(Type objectType)
		{
			return typeof(T).IsAssignableFrom(objectType);
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="JsonConverter"/> can write JSON.
		/// </summary>
		/// <value>
		///     <c>true</c> if this <see cref="JsonConverter"/> can write JSON; otherwise, <c>false</c>.
		/// </value>
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}
	}

	public class ParseFactory : FactoryConverter<ParseObject>
	{
		string _className;

		public ParseFactory(string className)
		{
			_className = className;
		}

		public override ParseObject CreateAndPopulate(Type objectType, JContainer json)
		{
			var output =  new ParseObject(_className);
			/*
			foreach (var t in json) {

				var v = t ["Value"];

				switch (t ["Key"].ToString()) {
				case BlrtEntity.AUDIO_KEY:

					break;
				case BlrtEntity.IMAGE_KEY

					break;
				case BlrtEntity.TOUCH_KEY:

					break;
				case BlrtEntity.NAME_KEY:
					output[BlrtEntity.NAME_KEY]
					break;
				case BlrtEntity.CREATOR_KEY:

					break;
				}

			}

			*/
			return output;
		}
	}
}

