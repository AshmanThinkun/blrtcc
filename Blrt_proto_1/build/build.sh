#!/bin/sh
if [ $1 ]
then
	VER=`tail -c +4 version.txt | head -n 1`
	sed s/VERSION_NUMBER/$VER/ $1/Info.plist > ../Info.plist
	cp -R ../Resources/build/$1/ ../Resources/
else
    echo "A development stream argument (i.e. develop, staging, ce or live) must be provided to this pre-build script"
    exit 1
fi