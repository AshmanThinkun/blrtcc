using System;
using BlrtData;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using Parse;

[assembly: Xamarin.Forms.Dependency (typeof(ParsePlatformHelper))]
namespace BlrtData
{
	public class ParsePlatformHelper : IParsePlatformHelper
	{
		public ParsePlatformHelper ()
		{
		}

		#region IParsePlatformHelper implementation

		public async Task<ParseFile> UploadFile (string cloudFileName, string filePath, CancellationToken token, IProgress<float> progressCallback = null)
		{
			// Parse Android SDK still cannot return correct progress;
			// we will use the .Net library
			using (var data = File.Open (filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
				var parseFile = new ParseFile (cloudFileName, data);
				if (null == progressCallback) {
					await BlrtUtil.AwaitOrCancelTask (parseFile.SaveAsync (token), token);
				} else {
					progressCallback.Report (0f);
					await BlrtUtil.AwaitOrCancelTask (parseFile.SaveAsync (new ProgressListener (progressCallback), token), token);
					progressCallback.Report (1f);
				}
				return parseFile;
			}
		}

		#endregion

		private class ProgressListener : IProgress<ParseUploadProgressEventArgs>
		{
			IProgress<float> _listener;
			public ProgressListener (IProgress<float> listener)
			{
				if (null == listener) {
					throw new ArgumentNullException ("listener");
				}
				_listener = listener;
			}
			#region IProgress implementation

			public void Report (ParseUploadProgressEventArgs value)
			{
				_listener.Report ((float)(value.Progress));
			}

			#endregion


		}
	}
}

