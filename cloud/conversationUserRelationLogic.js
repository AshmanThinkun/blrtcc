var constants = require('cloud/constants.js');
var loggler = require("cloud/loggler.js");
var util = require("cloud/util.js");
var _ = require("underscore");


//After the nonUser signup, check if some users send conversation to this nonUser
Parse.Cloud.define("findPendingRelation", function (request, response) {
    var success = false;
    var user = request.user;

    if (user.get(constants.user.keys.ContactInfoDirty) === false)
        return response.success({});


    var userQuery = new Parse.Query(constants.contactTable.keys.className)
        .equalTo(constants.contactTable.keys.user, user)
        .notEqualTo(constants.contactTable.keys.placeholder, true);


    var relations = null;
    var convoIds = [];

    return userQuery.find({useMasterKey: true}).then(function (contactInfo) {

        var monsterQuery = null;
        var queryLength = contactInfo.length;

        var emails = [];

        for (var i = 0; i < queryLength; ++i) {
            if (contactInfo[i].get("type") == "email")
                emails.push(contactInfo[i].get("value"));

            let relationQuery = new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.QueryType, contactInfo[i].get(constants.contactTable.keys.type))
                .equalTo(constants.convoUserRelation.keys.QueryValue, contactInfo[i].get(constants.contactTable.keys.Value))
                .equalTo(constants.convoUserRelation.keys.user, null);


            if (monsterQuery == null)
                monsterQuery = relationQuery;
            else
                monsterQuery = Parse.Query.or(monsterQuery, relationQuery);
        }


        console.log("monsterQuery")
        console.log(JSON.stringify(monsterQuery))
        if (monsterQuery == null) {
            request.user.set(constants.user.keys.ContactInfoDirty, false);
            return request.user.save(null, {sessionToken: request.user.get('sessionToken')}).then(function () {
                success = true;
                return response.success("No valid contact infos to check");
            });
        }

        //[Guichi] it seems that the result of this operation is unimportant
        new Parse.Query("TimelyAction")
            .startsWith("type", "email_nonUserInvited")
            .containedIn("context", emails)
            .find({useMasterKey:true})
            .then(function (timelyActions) {
                if (timelyActions.length == 0)
                    return;

                Parse.Object.destroyAll(timelyActions, {useMasterKey: true}).fail(function (error) {
                    loggler.global.log("ALERT: Error deleting TimelyActions:", error).despatch();
                });
            }, function (error) {
                loggler.global.log("ALERT: Error fetching TimelyActions to delete:", error).despatch();
            });

        return monsterQuery.include("conversation").find({useMasterKey: true});

    }).then(function (relationResults) {
        if (success)
            return;
        relations = relationResults;


        var queryLength = relations.length;

        if (queryLength == 0) {
            request.user.set(constants.user.keys.ContactInfoDirty, false);
            return request.user.save(null,{sessionToken:request.user.get('sessionToken')}).then(function () {
                success = true;
                return response.success("No relations found for provided contact details");
            });
        }

        console.log("relations: " + queryLength);

        var conversations = [];

        for (var i = 0; i < queryLength; ++i) {
            conversations.push(relations[i].get(constants.convoUserRelation.keys.conversation));
        }
        return new Parse.Query(constants.convoUserRelation.keys.className)
            .containedIn(constants.convoUserRelation.keys.conversation, conversations)
            .equalTo(constants.convoUserRelation.keys.user, request.user)
            .find({useMasterKey: true});

    }).then(function (dupResults) {
        console.log("dupResults")
        console.log(JSON.stringify(dupResults))
        if (success)
            return;
        var destroyList = new Array();
        var saveList = new Array();
        var roleList = [];

        var dupLength = dupResults == null ? 0 : dupResults.length;
        var relationsLength = relations.length;

        for (var i = 0; i < relationsLength; ++i) {

            var destoryed = false;

            for (var j = 0; j < dupLength; ++j) {

                if (relations[i].get(constants.convoUserRelation.keys.conversation).id
                    == dupResults[j].get(constants.convoUserRelation.keys.conversation).id) {
                    destroyList[destroyList.length] = relations[i];
                    destoryed = true;
                }
            }


            if (destoryed)
                saveList.push(
                    relations[i]
                        .get("conversation")
                        .add("dirtyPendingAttempt_relationManagement", +new Date() + 15000)
                        .increment("dirtyCounter_relationManagement")
                );
            else {
                relations[i].set(constants.convoUserRelation.keys.user, request.user);
                saveList[saveList.length] = relations[i];

                var role = relations[i].get("conversation").get("conversationRole");
                if (role) {
                    role.getUsers().add(request.user);
                    roleList.push(role);
                }
            }

            if (!_.contains(convoIds, relations[i].get("conversation").id))
                convoIds.push(relations[i].get("conversation").id);
        }

        var savePromise;

        if (saveList.length != 0)
            savePromise = Parse.Object.saveAll(roleList,{useMasterKey:true})
                .then(function () {
                    return Parse.Object.saveAll(saveList,{useMasterKey:true});
                });
        else
            savePromise = Parse.Promise.as();

        return savePromise.then(function () {

            console.log("Are you still OK ??")
            var destroyPromise;

            if (destroyList.length != 0)
                destroyPromise = Parse.Object.destroyAll(destroyList)
                    .then(function () {
                        return cleanDirtyConvos(convoIds);
                    });
            else
                destroyPromise = Parse.Promise.as();

            return destroyPromise.then(function () {
                return request.user.set(constants.user.keys.ContactInfoDirty, false)
                    .save(null,{sessionToken:request.user.get('sessionToken')})
                    .then(function () {
                        return response.success({success: true});
                    });
            });
        }, function (error) {
            return response.error(error);
        });
    }, function (error) {
        return response.error(error);
    });
});


var job = require('../index').job
Parse.Cloud.job("cleanDirtyConvos", function (req, res) {
    return cleanDirtyConvos().then(function (result) {
        console.log(result)
        if (result === true)
            return res.success();
        else
            return res.error(result);
    }, function (error) {
        return res.error(error);
    });
}, "* */1 * * * *");

Parse.Cloud.define("cleanDirtyConvosFromRelations", function (req, res) {
    Parse.Cloud.useMasterKey();
    return new Parse.Query("ConversationUserRelation")
        .containedIn("objectId", req.params.relations)
        .find()
        .then(function (relations) {
            var convoIds = _.map(relations, function (rel) {
                return rel.get("conversation").id;
            });

            return cleanDirtyConvos(convoIds).then(function (result) {
                if (result === true)
                    return res.success();
                else
                    return res.error(result);
            }, function (error) {
                return res.error(error);
            });
        }, function (error) {
            loggler.global.log("cleanDirtyConvosFromRelations ALERT: failed to query relations:", error).despatch();
            return res.error(error);
        });
});

exports.cleanDirtyConvos = function (convoIds) {
    return cleanDirtyConvos(convoIds);
};

function cleanDirtyConvos(convoIds) {

    var l = loggler.create("job:cleanDirtyConvos");
    Parse.Cloud.useMasterKey();

    var convoData = {};
    var convoObjects = [];

    var now = +new Date();

    var specificConvos = false;

    var parseConvo = function (convos) {
        _.each(convos, function (convo) {
            var data = {
                object: convo,
                relations: [],
                items: []
            };

            // There are three reasons we may want to clean this:
            // 1. Flagged as dirty..
            // 2. Partial item creation found?
            // 3. Partial relation creation found?

            // If we found this conversation from it's partial item or relation count we need
            // to make sure that nobody's still working on those (i.e. that creation session
            // hasn't expired yet).
            if (convo.get("partialItemCreationCount") > 0) {
                var notExpired = 0;

                if (_.isArray(convo.get("partialItemCreationTimes"))) {
                    var expiredTimes = [];
                    _.each(convo.get("partialItemCreationTimes"), function (expirationTime) {
                        if (expirationTime < now)
                            expiredTimes.push(expirationTime);
                        else
                            ++notExpired;
                    });

                    if (expiredTimes.length != 0)
                        data.expiredPartialItemCreationTimes = expiredTimes;
                }

                data.partialItemCount = convo.get("partialItemCreationCount") - notExpired;
            }

            if (convo.get("dirtyCounter_relationManagement") > 0) {
                var notExpired = 0;

                if (_.isArray(convo.get("dirtyPendingAttempt_relationManagement"))) {
                    var expiredTimes = [];
                    _.each(convo.get("dirtyPendingAttempt_relationManagement"), function (expirationTime) {
                        if (expirationTime < now)
                            expiredTimes.push(expirationTime);
                        else
                            ++notExpired;
                    });

                    if (expiredTimes.length != 0)
                        data.expiredPartialRelationCreationTimes = expiredTimes;
                }

                data.partialRelationCount = convo.get("dirtyCounter_relationManagement") - notExpired;
            }

            if (specificConvos || convo.get("conversationDirty") || data.expiredPartialItemCreationTimes || data.expiredPartialRelationCreationTimes) {
                convoData[convo.id] = data;
                convoObjects.push(convo);
            }
        });
    }

    var parseEverything = function () {
        l.log("Parsing " + convoObjects.length + " convos:", _.pluck(convoObjects, "id"));

        var indexLinks = {};
        var modifiedObjects = [];

        _.each(convoData, function (convo) {
            var numItems = 0;
            var numReadableItems = 0;
            var numComments = 0;
            var numEvents = 0;
            var numNotes = 0;
            var numBlrts = 0;

            var convoHasBegun = false;
            var creator = convo.object.get("creator");

            var convoUsers = [];
            var convoUserIds = [];

            var roleACL = new Parse.ACL();
            roleACL.setRoleReadAccess("Conversation-" + convo.object.id, true);
            roleACL.setRoleWriteAccess("Conversation-" + convo.object.id, true);

            _.each(convo.items, function (item) {
                item.set(constants.convoItem.keys.generalIndex, numItems);

                var type = item.get(constants.convoItem.keys.type);

                if (constants.convoItem.readableTypes.indexOf(type) != -1) {
                    var oldIndex = item.get(constants.convoItem.keys.readableIndex);
                    if (oldIndex != null && oldIndex != -1)
                        indexLinks[oldIndex] = numReadableItems;

                    item.set(constants.convoItem.keys.readableIndex, numReadableItems);

                    var psuedoId = convo.object.id;
                    psuedoId += constants.convoItem.psuedoIdSeparator;
                    psuedoId += numReadableItems.toString();

                    item.set(constants.convoItem.keys.psuedoId, psuedoId);

                    ++numReadableItems;

                    // If it's readable and someone other than the convo creator made this,
                    // then we should make sure the convo is set as having begun.
                    if (!convoHasBegun && item.get("creator").id != creator.id)
                        convoHasBegun = true;
                } else {
                    item.set(constants.convoItem.keys.readableIndex, -1);
                    item.set(constants.convoItem.keys.psuedoId, null);
                }

                switch (type) {
                    case constants.convoItem.types.blrt:
                    case constants.convoItem.types.reBlrt:
                        ++numBlrts;
                        break;
                    case constants.convoItem.types.comment:
                        ++numComments;
                        break;
                    case constants.convoItem.types.eventAdd:
                        ++numEvents;
                        break;
                    case constants.convoItem.types.eventMessage:
                        ++numNotes;
                        break;
                }

                item.setACL(roleACL);

                modifiedObjects.push(item);

                numItems++;
            });

            _.each(convo.relations, function (relation) {
                var viewed = relation.get(constants.convoUserRelation.keys.indexViewedList);

                viewed =
                    _.chain(viewed)
                        .expand()
                        .map(function (index) {
                            return indexLinks[index];
                        })
                        .uniq()
                        .without(undefined)
                        .contract()
                        .value();

                if (viewed)
                    relation.set(constants.convoUserRelation.keys.indexViewedList, viewed);
                else
                    relation.set(constants.convoUserRelation.keys.indexViewedList, null);

                if (convoHasBegun || relation.get("user") == null || relation.get("user").id != creator.id)
                    relation.set("allowInInbox", true);
                else
                    relation.unset("allowInInbox");

                relation.set("convoCreator", creator);
                relation.setACL(roleACL);

                if (relation.get("user") != null) {
                    if (!_.contains(convoUserIds, relation.get("user").id)) {
                        convoUsers.push(relation.get("user"));
                        convoUserIds.push(relation.get("user").id);
                    }
                }

                modifiedObjects.push(relation);
            });

            convo.object.increment("noteCount", numNotes - (convo.object.get("noteCount") || 0));
            convo.object.increment("eventCount", numEvents - (convo.object.get("eventCount") || 0));

            convo.object.increment(constants.convo.keys.generalCount, numItems - (convo.object.get("contentCount") || 0));
            convo.object.increment(constants.convo.keys.readableCount, numReadableItems - (convo.object.get("readableCount") || 0));
            convo.object.increment(constants.convo.keys.commentCount, numComments - (convo.object.get("commentCount") || 0));
            convo.object.increment("blrtCount", numBlrts - (convo.object.get("blrtCount") || 0));
            convo.object.increment("bondCount", convo.relations.length - (convo.object.get("bondCount") || 0));

            convo.object.set("hasConversationBegan", convoHasBegun);

            convo.object.set(constants.convo.keys.convoDirty, null);
            convo.object.increment(constants.convo.keys.reloadNumber);

            convo.object.setACL(roleACL);

            // Remove any expired item creation attempts that we found.
            if (convo.partialItemCount) {
                _.each(convo.expiredPartialItemCreationTimes, function (expirationTime) {
                    convo.object.remove("partialItemCreationTimes", expirationTime);
                });

                convo.object.increment("partialItemCreationCount", -1 * convo.partialItemCount);
            }

            // And similarly for any relation attempts.
            if (convo.partialRelationCount) {
                _.each(convo.expiredPartialRelationCreationTimes, function (expirationTime) {
                    convo.object.remove("dirtyPendingAttempt_relationManagement", expirationTime);
                });

                convo.object.increment("dirtyCounter_relationManagement", -1 * convo.partialRelationCount);
            }

            modifiedObjects.push(convo.object);

            // Lastly, we'll fix up all of the roles.
            if (convo.role == null)
                convo.role = new Parse.Role("Conversation-" + convo.object.id, new Parse.ACL());

            convo.role.getUsers().add(convoUsers);

            modifiedObjects.push(convo.role);
        });

        return Parse.Object.saveAll(modifiedObjects)
            .then(function () {
                l.log(true, "Number of modifiedObjects: " + modifiedObjects.length).despatch();
                return true
            }, function (error) {
                l.log(false, "saveAll(modifiedObjects) error:", error).despatch();
                return "saveAll(modifiedObjects) error: " + JSON.stringify(error);
            });
    }

    if (_.isArray(convoIds)) {
        var convos = _.map(convoIds, function (id) {
            var object = new Parse.Object("Conversation");
            object.id = id;
            return object;
        });

        return Parse.Promise.when(
            Parse.Object.fetchAll(convos),
            new Parse.Query("ConversationUserRelation")
                .containedIn("conversation", convos)
                .find(),
            new Parse.Query("ConversationItem")
                .containedIn("conversation", convos)
                .ascending(constants.convoItem.keys.generalIndex) //order is important, because we need to rebuild the index based on the existing one
                .find(),
            new Parse.Query(Parse.Role)
                .containedIn("name", _.map(convoIds, function (id) {
                    return "Conversation-" + id;
                }))
                .find()
        ).then(function (convos, relations, items, roles) {
            if (convos.length == 0) {
                l.log("No conversations found with convoIds:", convoIds).despatch();
                return true;
            }

            specificConvos = true;

            parseConvo(convos);

            _.each(relations, function (relation) {
                convoData[relation.get("conversation").id].relations.push(relation);
            });

            _.each(items, function (item) {
                convoData[item.get("conversation").id].items.push(item);
            });

            _.each(roles, function (role) {
                var convoId = _.singleSplit(role.get("name"), "-")[1];
                convoData[convoId].role = role;
            });

            return parseEverything();
        }, function (error) {
            l.log("Couldn't fetch convos and their relations and items:", error).despatch();
            return "Couldn't fetch convos and their relations and items: " + JSON.stringify(error);
        });
    } else
        return Parse.Query.or(
            new Parse.Query(constants.convo.keys.className)
                .equalTo(constants.convo.keys.convoDirty, true)
                .notEqualTo("incomplete", true),
            new Parse.Query("Conversation")
                .greaterThan("partialItemCreationCount", 0)
                .notEqualTo("incomplete", true),
            new Parse.Query("Conversation")
                .greaterThan("dirtyCounter_relationManagement", 0)
                .notEqualTo("incomplete", true)
        ).count().then(function (count) {
            if (count == 0) {
                l.log(true, "No dirty conversations").despatch();
                return true;
            }

            var convoPromises = [Parse.Promise.as()];

            var convoFetchLimit = Math.floor(constants.maxQueryLimit / (0 + 1));
            for (var query_i = Math.floor(Math.min(constants.skipLimit - convoFetchLimit, count) / convoFetchLimit); query_i >= 0; --query_i) {
                var limitedQuery = Parse.Query.or(
                    new Parse.Query(constants.convo.keys.className)
                        .equalTo(constants.convo.keys.convoDirty, true),
                    new Parse.Query("Conversation")
                        .greaterThan("partialItemCreationCount", 0),
                    new Parse.Query("Conversation")
                        .greaterThan("dirtyCounter_relationManagement", 0)
                ).limit(convoFetchLimit).skip(query_i * convoFetchLimit);

                convoPromises.push(limitedQuery.find().then(parseConvo, function (error) {
                    l.log("convo limitedQuery.find() error:", error);
                    return Parse.Promise.error("convo limitedQuery.find() error: " + JSON.stringify(error));
                }));
            }

            return Parse.Promise.when(convoPromises).then(function () {
                // The convo promises may have been broken counters which were cleared, so we might
                // not actually be due to continue.
                if (convoObjects.length == 0) {
                    l.log("No dirty conversations found").despatch();
                    return true;
                }

                var itemQuery = new Parse.Query(constants.convoItem.keys.className)
                    .containedIn(constants.convoItem.keys.conversation, convoObjects)
                    .count()
                    .then(function (count) {
                        var itemPromises = [Parse.Promise.as([])];

                        var itemFetchLimit = Math.floor(constants.maxQueryLimit / (0 + 1));
                        for (var query_i = Math.floor(Math.min(constants.skipLimit - itemFetchLimit, count) / itemFetchLimit); query_i >= 0; --query_i) {
                            var limitedQuery = new Parse.Query(constants.convoItem.keys.className)
                                .containedIn(constants.convoItem.keys.conversation, convoObjects)
                                .ascending(constants.base.keys.createdAt)
                                .limit(itemFetchLimit)
                                .skip(query_i * itemFetchLimit);

                            itemPromises.push(limitedQuery.find());
                        }

                        return Parse.Promise.when(itemPromises).then(function () {
                            var items = _.flatten(arguments, true);

                            _.each(items, function (item) {
                                convoData[item.get(constants.convoItem.keys.conversation).id].items.push(item);
                            });
                        }, function (error) {
                            l.log("Parse.Promise.when(itemPromises) error:", error);
                            return Parse.Promise.error("Parse.Promise.when(itemPromises) error: " + JSON.stringify(error));
                        });
                    });

                var relationQuery = new Parse.Query(constants.convoUserRelation.keys.className)
                    .containedIn(constants.convoUserRelation.keys.conversation, convoObjects)
                    .count()
                    .then(function (count) {
                        var relationPromises = [Parse.Promise.as()];

                        var relationFetchLimit = Math.floor(constants.maxQueryLimit / (0 + 1));
                        for (var query_i = Math.floor(Math.min(constants.skipLimit - relationFetchLimit, count) / relationFetchLimit); query_i >= 0; --query_i) {
                            var limitedQuery = new Parse.Query(constants.convoUserRelation.keys.className)
                                .containedIn(constants.convoUserRelation.keys.conversation, convoObjects)
                                .limit(relationFetchLimit)
                                .skip(query_i * relationFetchLimit);

                            relationPromises.push(limitedQuery.find().then(function (relations) {
                                _.each(relations, function (relation) {
                                    convoData[relation.get(constants.convoUserRelation.keys.conversation).id].relations.push(relation);
                                });
                            }, function (error) {
                                l.log("convoUserRelation limitedQuery.find() error:", error);
                                return Parse.Promise.error("convoUserRelation limitedQuery.find() error: " + JSON.stringify(error));
                            }));
                        }

                        return Parse.Promise.when(relationPromises);
                    });

                var roleNames = _.map(convoObjects, function (convo) {
                    return "Conversation-" + convo.id;
                });
                var roleQuery = new Parse.Query(Parse.Role)
                    .containedIn("name", roleNames)
                    .count()
                    .then(function (count) {
                        var rolePromises = [Parse.Promise.as()];

                        var roleFetchLimit = Math.floor(constants.maxQueryLimit / (0 + 1));
                        for (var query_i = Math.floor(Math.min(constants.skipLimit - roleFetchLimit, count) / roleFetchLimit); query_i >= 0; --query_i) {
                            var limitedQuery = new Parse.Query(Parse.Role)
                                .containedIn("name", roleNames)
                                .limit(roleFetchLimit)
                                .skip(query_i * roleFetchLimit);

                            rolePromises.push(limitedQuery.find().then(function (roles) {
                                _.each(roles, function (role) {
                                    var convoId = _.singleSplit(role.get("name"), "-")[1];
                                    convoData[convoId].role = role;
                                });
                            }, function (error) {
                                l.log("role limitedQuery.find() error:", error);
                                return Parse.Promise.error("role limitedQuery.find() error: " + JSON.stringify(error));
                            }));
                        }

                        return Parse.Promise.when(rolePromises);
                    });

                return Parse.Promise.when([itemQuery, relationQuery, roleQuery]).then(parseEverything, function (error) {
                    l.log(false, "Parse.Promise.when([itemQuery, relationQuery, roleQuery]) error:", error).despatch();
                    return "Parse.Promise.when([itemQuery, relationQuery, roleQuery]) error: " + JSON.stringify(error);
                });
            }, function (error) {
                l.log(false, "Parse.Promise.when(convoPromises) error:", error).despatch();
                return "Parse.Promise.when(convoPromises) error: " + JSON.stringify(error);
            });
        }, function (error) {
            l.log("convoQuery.count() error:", error).despatch();
            return "convoQuery.count() error: " + JSON.stringify(error);
        });
}
