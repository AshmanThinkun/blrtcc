require("cloud/api/conversation");
require("cloud/api/conversationItem");
require("cloud/api/conversationUserRelation");
require("cloud/api/user");
require("cloud/api/userContactDetail");
require("cloud/api/other");
require("cloud/api/timelyAction");
require("cloud/api/developer");
require("cloud/api/request");
require("cloud/api/pendingAction");

// # api.blrt.com support
var crypto = require("crypto");
var loggler = require("cloud/loggler");

var _ = require("underscore");

// This API function will handle routing requests from api.blrt.com to their
// appropriate cloud functions. Although this function doesn't utilise the API
// framework, the routed functions will, so they can take full advantage of the
// results the framework provides.

// This API function expects a request as follows:
// {
//     __original_path__: "1/conv/request",
//     data: "ISDih9d28daSDdu192JWAhFd9awdsa...",
//     apiKey: "BDAi7dah8cSXffjewFIAOdj91uh3idG"
// }
// The data value must be AES256->base64 encrypted using the developer's private
// key, and then URL encoded. At a minimum, it must contain an expirationTime
// which is within 15 minutes of the current UTC time. A valid expirationTime
// value will not equal NaN after being Date.parse()d:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse

// The API will basically:
// 0.5. Validate the request - we'll exit straight away if it won't work out;
// 1. Find the developer associated with the provided apiKey;
// 2. Decode the data using that developer's private key.
//   * Here we'll also validate the request against the provided expiration time;
// 3. Call the API endpoint corresponding to the evaluated route; and
// 4. Return it's results to api.blrt.com.

// ## Flow
Parse.Cloud.define("api", function (req, res) {
    function psuedo404(log) {
        loggler.global.log(log + ":", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "The requested API endpoint was not found." }));
    }

    Parse.Cloud.useMasterKey();
    var params = req.params;

    // ### Step 0.5: Validate the request

    // * Do we have the original endpoint path that they requested?
    if(!params.__original_path__) {
        loggler.global.log("ALERT: Received api.blrt.com request with no original path:", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "A target API endpoint must be specified." }));
    }

    // * Did they provide a good API version?
    var routeFragments = params.__original_path__.split("/");
    var apiVersion = parseInt(routeFragments.shift());
    if(!apiVersion || _.isNaN(apiVersion)) {
        loggler.global.log("ALERT: Received api.blrt.com request with no numerical API version:", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "An integer API version must be specified." }));
    }

    if(apiVersion != 1) {
        loggler.global.log("ALERT: Received api.blrt.com request with an unsupported API version:", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "Unsupported API version requested." }));
    }

    // * Did they provide their API key?
    if(!params.apiKey) {
        loggler.global.log("ALERT: Received api.blrt.com request with no apiKey:", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "A developer API key must be provided with each request." }));
    }

    if(!params.data) {
        loggler.global.log("ALERT: Received api.blrt.com request with no provided data:", req).despatch();
        return res.error(JSON.stringify({ success: false, message: "An expiration time must be provided with each request." }));
    }

    // ### Step 1: Find the developer associated with the provided apiKey
    return new Parse.Query(Parse.User)
        .equalTo("developerPublicApiKey", params.apiKey)
        .first()
        .then(function (developer) {
            if(!developer) {
                loggler.global.log("ALERT: Couldn't find developer for api.blrt.com request:", req).despatch();
                return res.error(JSON.stringify({ success: false, message: "Invalid API key." }));
            }

            // ### Step 2: Decode the data using that developer's private key
            var data = {};
            var decryption;

            try {
                var decipher = crypto.createDecipher("aes256", developer.get("developerPrivateKey"));
                decryption = decipher.update(params.data, "base64");
                decryption += decipher.final("utf8");
            } catch(e) {
                loggler.global.log("ALERT: An error occurred when deciphering api.blrt.com data:", e).despatch();
                return res.error(JSON.stringify({ success: false, message: "Failed to decrypt the provided data." }));
            }

            try {
                data = JSON.parse(decryption);
            } catch(e) {
                loggler.global.log("ALERT: An error occurred when JSON decoding the api.blrt.com decryped data:", e).despatch();
                return res.error(JSON.stringify({ success: false, message: "Failed to decrypt the provided data." }));
            }

            // * Validate the expirationTime of the request too.
            if(!data.expirationTime) {
                loggler.global.log("ALERT: Received api.blrt.com request with no expiration time specified:", req).despatch();
                return res.error(JSON.stringify({ success: false, message: "An expiration time must be provided with each request." }));
            }

            var expirationTime = Date.parse(data.expirationTime);
            if(!expirationTime) {
                loggler.global.log("ALERT: Received api.blrt.com request with invalid expirationTime (NaN):", req).despatch();
                return res.error(JSON.stringify({ success: false, message: "Failed to parse the provided expiration time." }));
            }

            var later = new Date();
            later.setMinutes(later.getMinutes() + 15);

            if(expirationTime < Date.now()) {
                loggler.global.log("ALERT: Received expired api.blrt.com request:", req).despatch();
                return res.error(JSON.stringify({ success: false, message: "This request has expired." }));
            } else if(expirationTime > later.getTime()) {
                loggler.global.log("ALERT: Received api.blrt.com request with expirationTime more than 15 minutes from now:", req).despatch();
                return res.error(JSON.stringify({ success: false, message: "The provided expiration time must be no later than 15 minutes from now." }));
            }

            // The expirationTime isn't needed for anything else at this point, so we'll clear it out.
            delete data.expirationTime;

            // ### Step 3: Call the API endpoint corresponding to the evaluated route
            var target;
            switch(routeFragments[0]) {
                case "conv":
                    switch(routeFragments[1]) {
                        case "request":
                            target = "conversationRequest";
                            break;
                        default:
                            return psuedo404("ALERT: No route matching api.blrt.com request original path");
                    }

                    break;
                case "test":
                    return res.success(JSON.stringify({
                        success: true,
                        email: developer.get("email"),
                        name: developer.get("name")
                    }));
                default:
                    return psuedo404("ALERT: No route matching api.blrt.com request original path");
            }

            // Before we call the endpoint, add any extra data that may be necessary.
            switch(target) {
                case "conversationRequest":
                    data.type = "api.blrt.com";
                    data.developerId = developer.id;

                    break;
            }

            return Parse.Cloud.run(target, {
                api: {
                    version: apiVersion,
                    endpoint: target
                },
                device: {
                    version: "1.0",
                    device: "www",
                    os: "www",
                    osVersion: "1.0"
                },
                data: data
            }).then(function (result) {
                // ### Step 4: Return it's results to api.blrt.com

                // If we've been given a result, we'll simply return that - no augmentation required.
                // However if the result wasn't successful, we'll use res.error instead of res.success
                // so a more appropriate HTTP status code is returned. It shouldn't be 200 if their
                // request ultimately failed.
                try {
                    result = JSON.parse(result);
                } catch(e) {
                    loggler.global.log("ALERT: Failed to JSON parse api.blrt.com target function response:", e).despatch();
                    return res.error(JSON.stringify({ success: false, message: "An internal error occurred." }));
                }

                if(result.success)
                    return res.success(JSON.stringify(result));
                else if(!_.has(result, "success"))
                    return res.error(JSON.stringify({ success: false, message: "An internal error occurred." }));
                return res.error(JSON.stringify(result));
            }, function (functionError) {
                loggler.global.log("ALERT: Error occurred when api.blrt.com executed final endpoint:", functionError).despatch();
                return res.error(JSON.stringify({ success: false, message: "An internal error occurred." }));
            });
        }, function (promiseError) {
            loggler.global.log("ALERT: Error when searching for api.blrt.com request developer:", promiseError).despatch();
            return res.error(JSON.stringify({ success: false, message: "An internal error occurred." }));
        });
});