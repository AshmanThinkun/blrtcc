/* global Parse */

var _ = require("underscore");
var emailJS = require("cloud/email");

exports.get = function(user, installation, version) {
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query('PendingAction').equalTo('recipient', user).equalTo('recipientDevice', installation).equalTo('complete', false);
    if (version) {
        query.notEqualTo('appVersion', version);
    }
    return query.find({useMasterKey: true}).then(function(results){
        //console.log(results);
        return results;
    });
}

exports.send = function (pendingActionList) {
    _.each(pendingActionList, function(pendingAction){
        if (typeof pendingAction === 'object' && pendingAction.className === 'PendingAction') {
            var pushNotification = emailJS.GenerateSilentPush({
                t: pendingAction.get('type'),
                pid: pendingAction.id,
                args: JSON.parse(pendingAction.get('arguments'))
            });
            console.log(pendingAction.get('arguments'));
            var z = pendingAction.get('recipientDevice').id;
            console.log('sending to '+z);
            Parse.Push.send({
                    where: new Parse
                    .Query(Parse.Installation)
                    .equalTo('objectId', z),
                    data: pushNotification
                }, {useMasterKey: true}).then(function (result) {
                    pendingAction.increment('tryCount', 1);
                    console.log(result)
                    pendingAction.save();
                }).fail(function (error) {
                    console
                    .log("ALERT: Error sending userDeleted push notifications:", error)
                    .despatch();
                });
        }
    });
}

exports.create = function (type, args, users) {
    Parse.Cloud.useMasterKey();
    var PendingAction = Parse.Object.extend("PendingAction");
    console.log("\n[!] pendingActions.create() called");
    console.log(type);
    console.log(args);
    console.log(users);
    if (typeof users === 'object' && users.length > 0) {
        if (typeof type === 'string' && isNaN(type) === false) {
            type = parseInt(type);
        }
        if (typeof type === 'number' && (100 <= type <= 999)) {
            if (typeof args === 'object') {
                args = JSON.stringify(args);
                console.log("[!] Querying installations");
                return new Parse
                    .Query(Parse.Installation)
                    .containedIn("owner", users)
                    .include("owner")
                    .find({useMasterKey: true})
                    .then(function(installations) {
                        var objectsToSave = [];
                        console.log("[!] creating pendingActions for each installation")
                        _.each(installations, function(theInstallation){
                            var pendingAction = new PendingAction();
                            pendingAction.set("recipient", theInstallation.get('owner'));
                            pendingAction.set("recipientDevice", theInstallation);
                            pendingAction.set("type", type);
                            pendingAction.set("tryCount", 0);
                            pendingAction.set("failCount", 0);
                            pendingAction.set("complete", false);
                            pendingAction.set("arguments", args);
                            objectsToSave.push(pendingAction);
                        });
                        return Parse.Object.saveAll(objectsToSave)
                            .then(function(list){
                                console.log("[!] Pending objects have been saved");
                                // var pushNotification = emailJS.GenerateSilentPush({
                                //     t: type,
                                //     pid: 'haha',
                                //     args: pushArgs
                                // });
                                /*Parse.Push.send({
                                    where: new Parse
                                    .Query(Parse.Installation)
                                    .containedIn("owner", users),
                                    data: deleteNotification
                                }, {useMasterKey: true}).then(function (result) {
                                    console.log(result)
                                }).fail(function (error) {
                                    console
                                    .log("ALERT: Error sending userDeleted push notifications:", error)
                                    .despatch();
                                });*/
                                console.log(list);
                                console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                                exports.send(list);
                                return Parse.Promise.as({success: true, error: "pendingAction objects have been saved"});
                            },function(error){
                                return Parse.Promise.as({success: false, error: "Error saving pendingAction objects"});
                            });
                    });
                console.log("[!] Code after querying installations");
            } else { // need to add json validation
                console.log("[!] 'args' needs to be a valid JSON *object*");
            }
        } else {
            console.log("[!] Type '"+type+"' not valid for pendingActions");
        }
    } else {
        console.log("[!] 'users' must be a non-empty list object");
    }
    console.log("[!] END");
    return Parse.Promise.as({success: false, error: "Error creating pendingActions"});
}