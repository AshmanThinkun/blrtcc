var _ = require("underscore");
var loggler = require("cloud/loggler");
var constants = require("cloud/constants");

// # ParseMutex

// This allows the user to specify a ParseObject and an arbitrary action name that they
// wish to make mutually exclusive (i.e. nothing else can perform this action on this
// ParseObject simultaneously).

// Once mutual exclusion is acquired (and guaranteed), the provided function will be
// executed and then mutual exclusion will be relinquished.

// Relinquishment involves saving the locked object and users can take advantage of this
// to save critical data to that object under the guarantee that it won't be overridden
// and there will be no possible race conditions.

// This is important when dealing with things like counters which are related to
// permissions and limits imposed on creation of objects.

// __Acquiring mutual exclusion involves:__

// 1. Increment dirty counter and add expiration time to the pendingAttempt list;
// 2. If dirty counter is 1, we have mutual exlusion and the object's not dirty!
// 3. Otherwise:
//   3.1 If least recent other pendingAttempt < 15 seconds old, relinquish and exit, otherwise:
//   3.2 Mark mutex as dirty (so the calling function can behave appropriately).
// 4. Run provided function providing locked object and whether or not it's dirty.
// 5. Relinquish mutex: decrement dirty counter and remove current time from pendingAttempts.
exports.fnWithLock = function (object, action, fn) {
    // The duration of the lock may change based on whether this is a function call or job.
    // Assuming it's a function, we give it a full 15 seconds.
    //
    return fn(object,false).then(function (result) {
        return result;
    },function (err) {
        return err
    })
    // //
    // function relinquish (promise) {
    //     return promise.then(function (r) {
    //         return r
    //     },function (e) {
    //         return e
    //     })
    // }

    // return relinquish(fn(object,false))
    //
    // var duration = constants.hour / 60 / 4;
    // // If it's a job, we give it 15 minutes instead.
    // if(constants.isJob)
    //     duration *= 60;
    //
    // var now = +new Date();
    // // The expiration time is being handled as an integer.
    // var expiration = now + duration;

    // ## Step 1: Increment dirty counter and add expiration time to the pendingAttempt list
    // return object
    //     .increment("dirtyCounter_" + action)
    //     .add("dirtyPendingAttempt_" + action, expiration)
    //     .save()
    //     .then(function () {
    //         // ## Step 3: If dirty counter isn't 1, mark dirty, check pendingAttempt and maybe exit
    //
    //         // We've incremented our counters - if it's > 1 that means it was either 1 before and that function
    //         // failed to come back and decrement the counter, or another function call increased the counter at
    //         // practically the same time.
    //         var isDirty = false;
    //         if(object.get("dirtyCounter_" + action) > 1) {
    //             isDirty = true;
    //
    //             // Next we'll go through each pending attempt (excluding our own) to see if it's expired or not.
    //             // We're assuming that no two requests will be fulfilled within the same ms, Parse isn't capable :P
    //             var retryTime;
    //             _.each(object.get("dirtyPendingAttempt_" + action), function (timeStarted) {
    //                 if(timeStarted != expiration && timeStarted > now)
    //                     // If one hasn't expired yet then we'll tell the client to wait until that's done.
    //                     retryTime = new Date(timeStarted);
    //             });
    //
    //             if(retryTime != null)
    //                 return relinquish(Parse.Promise.as(retryTime));
    //         }
    //
    //         // ## Step 4: Run provided function providing locked object
    //
    //         // If it's been detected as dirty, provide this mutex's expiration time incase they want to clear the
    //         // list but make sure that this mutex's time is still in there.
    //         // We're also assuming that the requested function returns a promise that we can wait on.
    //         return relinquish(fn(object, isDirty ? expiration : false));
    //
    //         // ## Step 5: Relniquish the mutex
    //         function relinquish(promise) {
    //             var promiseResult;
    //             var promiseError;
    //
    //             return promise.then(function (result) {
    //                 promiseResult = result;
    //             }, function (error) {
    //                 promiseError = error;
    //             }).always(function () {
    //                 return object
    //                     .increment("dirtyCounter_" + action, -1)
    //                     .remove("dirtyPendingAttempt_" + action, expiration)
    //                     .save()
    //                     .then(function () {
    //                         if(retryTime != null)
    //                             return Parse.Promise.error(retryTime);
    //                         else if(promiseResult)
    //                             return Parse.Promise.as(promiseResult);
    //                         else
    //                             return Parse.Promise.error(promiseError);
    //                     }, function (error) {
    //                         loggler.global.log("ALERT: Error unsetting dirty fields when relinquishing mutex:", error).despatch();
    //                         return error;
    //                     });
    //             });
    //         }
    //     }, function (error) {
    //         loggler.global.log("ALERT: Error setting dirty fields when acquiring mutex:", error).despatch();
    //         return error;
    //     });
};