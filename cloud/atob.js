//todo ready to remove
var Buffer = require('buffer').Buffer;

(function () {
  "use strict";

  function atob(str) {
    return new Buffer(str, 'base64').toString('binary');
  }

  module.exports = atob;
}());