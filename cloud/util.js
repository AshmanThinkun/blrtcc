var crypto = require('crypto');
var constants = require('cloud/constants.js');
var Buffer = require('buffer').Buffer;
var _ = require("underscore");

exports.format = function (str, obj) {
	return str.replace(/\{\s*([0-9]+)\s*\}(?!\})/g, function(m, p1, offset, string) {
		return obj[p1]
	});
};


exports.inArray = function (theArray, subject) {

    var l = theArray.length;

    for(var i in theArray) {
        if(subject === theArray[i])
            return true;
    }

    return false;
};


exports.irreversibleHash = function (str) {
    return crypto.createHash('sha256').update(constants.salt + str).digest('base64');
};


exports.isEmail = function (str) {
	var re = /^(([^<>()[\]\\.,;:\s@\"']+(\.[^<>()[\]\\.,;:\s@\"']+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(str);
};


exports.byteLength = function (str) {
	var s = str.length;
	for (var i=str.length-1; i>=0; i--) {
		var code = str.charCodeAt(i);
		if(code > 0x7f && code <= 0x7ff) s++;
		else if(code > 0x7ff && code <= 0xffff) s+=2;
		if(code >= 0xDC00 && code <= 0xDFFF) i--;
	}
	return s;
};


exports.merge = function (deep) {
    var result = {},
        num_args = arguments.length;

    for(var arg_i = 1; arg_i < num_args; ++arg_i) {
        for(var key in arguments[arg_i]) {
            if(arguments[arg_i].hasOwnProperty(key)) {
            	if(deep == true && typeof arguments[arg_i][key] == "object")
            		result[key] = exports.merge(true, result[key], arguments[arg_i][key]);
            	else
                	result[key] = arguments[arg_i][key];
            }
        }
    }

    return result;
};

exports.mergeArray = function (deep, array) {
	array.unshift(deep);

	return shallowMerge.apply(null, array);
};

exports.idsFromPointerArray = function (arr) {
	if(arr == null || arr.length == 0)
		return arr;

	var result = [];

	for(var arr_i = arr.length - 1; arr_i >= 0; --arr_i)
		result.push(arr[arr_i][constants.base.keys.objectId]);

	return result;
};

exports.getPlatform = function (appName, os) {
	if ("ChromeWebPlayer" === appName) {
		return constants.platforms.WebPlayer;
	}
	if ("Android" === os) {
		return constants.platforms.Android;
	}
	if ("iPhone OS" === os) {
		return constants.platforms.iOS;
	}
	if ("www" === os) {
		return constants.platforms.OtherWeb;
	}
	return constants.platforms.OtherWeb;
}

const BlockByteSize = 16;
exports.generateRandomNumber = function() {
	var buf = null;
	try {
		buf = crypto.randomBytes(BlockByteSize);
	} catch (err) {
		console.log("cannot use cryptographically strong RNG. err: " + err);
		try {
			buf = crypto.pseudoRandomBytes(BlockByteSize);
		} catch (err) {
			console.error("error after tried pseudo random bit. error: " + err);
		}
	}
	return buf;
}

exports.httpsParseFileUrl = function(parseFileUrl) {
	var url = parseFileUrl.toLowerCase();
	function addPrefix(pureURL) {
		return "https://s3.amazonaws.com/" + pureURL;
	}
	if ('http://' == url.substring(0, 7)) {
		return addPrefix(parseFileUrl.substring(7));
	} else if ('https://' == url.substring(0, 8)) {
		return parseFileUrl;
	} else if ('//' == url.substring(0, 2)) {
		return parseFileUrl;
	} else {
		return addPrefix(parseFileUrl);
	}
}

exports.reqToSessionToken = function(req) {
	try {
		return req.user.getSessionToken();
	} catch (e) {}
	return null;
}

exports.reqToCloudFuncOptions = function(req) {
	try {
		return {sessionToken: req.user.getSessionToken()};
	} catch (e) {}
	return undefined; // return undefined so it can be passed to Parse.Cloud.run directly
}

_.mixin({
    _isProvided: function (input, trim) {
        trim = trim == undefined ? true : trim;
        return input != null && (!_.isString(input) || (!trim || input.trim() != ""));
    },
    _isValidEmail: function (str) {
        return _.isString(str) && exports.isEmail(str);
    },
	singleSplit: function (str, delimiter) {
		var i = str.indexOf(delimiter);
		return i !== -1 ? [str.slice(0, i), str.slice(i + delimiter.length)] : [str];
	},
	salt: function (min, max) {
		min = _.isFinite(min) ? min : 0;
		max = _.isFinite(max) ? max : 10000;

		return parseInt(min + Math.random() * (max - min)).toString();
	},
	expand: function (str) {
		var result = [];

		if(!_.isString(str))
			// TODO: Error should be here?
			return result;

		_.each(str.split(","), function (values) {
			var split = values.split("-");

			if(split.length == 1)
				result.push(parseInt(split[0]));
			else if(split.length == 2)
				result = result.concat(_.range(parseInt(split[0]), parseInt(split[1]) + 1));
		});

		return result;
	},
	contract: function (list) {
		var start = null;
		var end = null;

		result = [];

		if(!_.isArray(list))
			// TODO: Error should be here?
			return result;

		_.each(list, function (value) {
			if(!_.isNumber(value))
				return;

			if(start == null || end == null) {
				start = value;
				end = value;
			} else if(value >= start - 1 && value <= end + 1) {
				start = Math.min(start, value);
				end = Math.max(end, value);
			} else {
				if(start == end)
					result.push(start);
				else
					result.push(start + "-" + end);

				start = value;
				end = value;
			}
		});

		if(start != null && end != null) {
			if(start == end)
				result.push(start);
			else
				result.push(start + "-" + end);
		}

		return result.join();
	},
	encryptId: function (str) {
		if(!_.isString(str))
			// TODO: Error should be here?
			return str;

		return _.map(_.range(str.length), function (index) {
			return constants.sharedObjectIdCryptToken[constants.objectIdChars.indexOf(str[index])];
		}).join("");
	},
	decryptId: function (str) {
		if(!_.isString(str))
			// TODO: Error should be here?
			return str;

		return _.map(_.range(str.length), function (index) {
			return constants.objectIdChars[constants.sharedObjectIdCryptToken.indexOf(str[index])];
		}).join("");
	},
	encryptStringAES: function (str, secret) {
		if(!_.isString(str))
			return str;
		if(!secret)
			secret = constants.AESKey1;

		var IV = exports.generateRandomNumber();
		var inputBuffer = new Buffer(str, 'utf16le');
		var cipher = crypto.createCipheriv('aes-256-cbc', secret, IV);
		var encryptedData = Buffer.concat([cipher.update(inputBuffer), cipher.final()]);
		var result = Buffer.concat([IV,encryptedData]).toString('base64');
		return result;
	},
	decryptStringAES: function (str, secret) {
		if(!_.isString(str))
			return str;
		var wholeBuffer = new Buffer(str,'base64');
		if(wholeBuffer.length<=BlockByteSize)
			return str;
		if(!secret)
			secret = constants.AESKey1;

		var IV = wholeBuffer.slice(0,BlockByteSize); 
		var inputBuffer = wholeBuffer.slice(BlockByteSize);

		var cipher = crypto.createDecipheriv('aes-256-cbc', secret, IV);

		var decryptedData = Buffer.concat([cipher.update(inputBuffer), cipher.final()]);

		var result = decryptedData.toString("utf16le");  
		return result;
	},
	hashedQueryArgs: function (argsOrKeys, vals) { // Does NOT include the "?"
		// Can either take an object or two arrays
		
		// The key will eventuate as the irreversibleHashed sum of all values
		var key = "";

		var result = _.chain(vals != null ? _.zip(argsOrKeys, vals) : _.pairs(argsOrKeys))
            .sortBy(function (tuple) { return tuple[0]; })
            .reduce(function (str, tuple) {
            	if(!_.isString(tuple[1])) {
            		if(_.isObject(tuple[1]))
            			tuple[1] = JSON.stringify(tuple[1]);
            		else
            			tuple[1] = "";
            	}

            	key += tuple[1];
            	tuple[1] = encodeURIComponent(tuple[1]);

            	return str + (str == "" ? "" : "&") + tuple.join("=");
            }, "")
            .value();

		return result && result + "&key=" + encodeURIComponent(exports.irreversibleHash(key));
	},
	// Returns false if no key is specified in the query args
	// Otherwise returns true or false based on whether the ordered sum of all arguments exactly equals the key
	validQueryArgs: function (query) { // `query` should be everything after the ? (i.e. for abc.com/def?query=args `query` == "query=args")
		// Alternatively, query can be an object which will be converted into the above string
		if(_.isObject(query))
			query = _.chain(query)
					 .pairs()
					 .map(function (pair) { return pair.join("="); })
					 .value()
					 .join("&");

		var key = "";

		return exports.irreversibleHash(_.chain(query.split("&"))
            .map(function (arg) { return _.singleSplit(arg, "="); })
            .sortBy(function (tuple) { return tuple[0]; })
            .reduce(function (str, tuple) {
            	if(tuple[0] != "key")
            		return str + tuple[1];

            	key = tuple[1];
            	return str;
            }, "")
            .value()) == key;
	},
    responsify: function (objects) {
        var result = [];
        var forArray = _.isArray(objects);
        if(!forArray) objects = [objects];

        _.each(objects, function (object) {
            result.push({
                type: object.className,
                id: object.id,
                localGuid: object.get(constants.base.keys.localGuid)
            });
        });

        return forArray ? result : result[0];
    },
    durationify: function (numSeconds) {
        if(numSeconds >= 0)
            return ( numSeconds / 60 >= 1
                    ? Math.floor(numSeconds / 60) + " minute" + (numSeconds / 60 >= 2 ? "s" : "") + " "
                    : ""
                ) +
                ( numSeconds / 60 < 1 || numSeconds % 60 != 0
                    ? numSeconds % 60 + " second" + (numSeconds % 60 != 1 ? "s" : "")
                    : ""
                );
        else
            return "";
    },
    commaAndList: function (list) {
        var length = list.length;
        return _.reduceRight(list, function (memo, item, i) {
            return item + (memo != "" ? (i == list.length - 2 ? " and " : ", ") : "") + memo;
        }, "");
    },
	extension: function (url) {
		if(!_.isString(url))
			return "";

		var lastPart = url.substr(1 + url.lastIndexOf("/")).split(/[?;]/)[0];
		var periodIndex = lastPart.lastIndexOf(".");

	    return periodIndex != -1 ? lastPart.substr(periodIndex) : "";
	},
	isEmail: function (str) {
		return _.isString(str) && /^[^.]([^@\s.]*|\.[^.])*[^.]?@[^.][^@\s]*(\.[^.][^@\s.]*)+$/.test(str);
	},
	isUrl: function (str) {
		return _.isString(str) && /^([^. ][^. ]*\.)+[^. ]+(\/.*)?$/.test(str);
	},
    androidLink: function (url) {
        var indexOf = url.indexOf("://");

        var protocol = url.substr(0, indexOf);
        var remainder = url.substr(indexOf + 3);
        return "intent://" + remainder + "#Intent;scheme=" + protocol + ";package=" + constants.androidPackageId + ";end";
    },
    randomString: function (length) {
        length = typeof length != "undefined" ? length : 10;

        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(var i = length; i != 0; --i)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },
    stringToList: function (listStr) {
    	if ('string' != typeof listStr) {
    		return null;
    	}
    	if (listStr.length <= 0) {
    		return [];
    	}
    	var returnList = [];
    	listStr.split(',').forEach(function(value) {
    		var range = value.trim().split('-');
    		if (1 == range.length) {
    			// is not a range
    			var number = Number(range[0]);
    			if (NaN == number) {
    				return null;
    			} else {
    				returnList.push(number);
    			}
    		} else if (2 == range.length) {
    			// is a range
    			var start = Number(range[0]);
    			var end = Number(range[1]);
    			if (NaN == start || NaN == end) {
    				return null;
    			} else {
    				for (var i = start; i <= end; ++i) {
    					returnList.push(i);
    				}
    			}
    		} else {
    			// invalid
    			return null;
    		}
    	});
    	return returnList;
    },
    listToString: function (array) {
    	if (!_.isArray(array)) {
    		return null;
    	}
    	if (0 == array.length) {
    		return "";
    	}
    	var list = array.sort(function(a, b) {return (a - b)});
    	var strSec = [];
    	var rngStart = null;
    	var pre = null;
    	for (var i = 0; i < list.length; ++i) {
    		if ('number' != typeof list[i] || NaN == list[i]) {
	    		return null;
	    	}
    		if (null !== pre) {
    			if (pre === list[i]) {
    				continue;
    			} else if ((pre + 1) === list[i]) {
    				if (null === rngStart) {
    					rngStart = pre;
    				}
    			} else {
    				if (null === rngStart) {
    					strSec.push(pre.toString());
    				} else {
    					strSec.push(rngStart.toString() + ((pre - rngStart > 1) ? '-' : ',') + pre);
    					rngStart = null;
    				}
    			}
    		}
    		pre = list[i];
    	}
    	if (null === rngStart) {
			strSec.push(pre.toString());
		} else {
			strSec.push(rngStart.toString() + ((pre - rngStart > 1) ? '-' : ',') + pre);
			rngStart = null;
		}
		return strSec.join(',');
    }
});