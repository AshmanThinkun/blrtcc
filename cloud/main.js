//This is the entry point of the cloud function and is the only place to override parse sdk
//we override job because parse do not provide a job system
//we override run because it is pretty buggy
// Parse.Cloud.job=require('../index').job
var run = require("../lib/run");
Parse.Cloud.run = run;

var email = require('cloud/email.js');
var constants = require('cloud/constants.js');
//todo In this stage just cloud api not express ,so disable them temperately
if (!constants.isOpensourceServer) {
    require('cloud/app.js');
}


require('cloud/api');
require('cloud/settings.js');
require('cloud/accounttype.js');
require('cloud/specialBlrtCreation.js');
require('cloud/conversationUserRelationLogic.js');
require('cloud/migration.js');
require('cloud/permissions.js');
require("cloud/jobs");
require("cloud/requestGenerator");
require("cloud/pioneer");
require("cloud/beforeSave");
require("cloud/webplayer");
require("cloud/webrecorder");
require("cloud/yozio.js");
require("cloud/ocList.js");
require("cloud/accessToken.js");
require("cloud/api/webappQuery/index");
require("cloud/triggers/triggers");
require("cloud/user.js")


const Blrt = require('cloud/api/models/Blrt')

var loggler = require('cloud/loggler.js');
var _ = require("underscore");

var k_ct = constants.contactTable.keys;

Parse.Cloud.job("updateSettings", function (request, status) {
    var l = loggler.create("job:updateSettings");

    var pushQuery = new Parse.Query(Parse.Installation);
    pushQuery.greaterThan("updatedAt", new Date(+new Date() - constants.hour));

    return Parse.Push.send({where: pushQuery, data: email.StandardizePushMessage({t: "0"})}, {
        success: function () {
            status.success();
        },
        error: function (error) {
            l.log(false, "Parse Push error:", error).despatch();

            status.error();
        },
        useMasterKey: true
    });
});

/////////////////
// implemented because Parse stops supporting just a boolean
/////////////////
Parse.Cloud.define("passwordCheck147", function (request, response) {
    var password = request.params.password;
    return Parse.User.logIn(request.user.getUsername(), password, {
        success: function (results) {
            return response.success({result: true});
        },
        error: function () {
            return response.success({result: false});
        }
    });
});

Parse.Cloud.define("assignInstallation", function (request, response) {
    var l = loggler.create("function:assignInstallation");
    console.log("\n\n\n\n\nDEVICEEEEEEEEEEEEEEEEEEEEEEE")
    console.log(request.params);
    console.log("\n\n\n\n\nDEVICEEEEEEEEEEEEEEEEEEEEEEE")
    return new Parse.Query(Parse.Installation)
        .equalTo("installationId", request.params.installationId)
        .first({
            success: function (item) {
                if (item == null) {
                    l.log("New Installation for deviceToken:", request.params.deviceToken);
                    item = new Parse.Installation();
                    item.set("deviceToken", request.params.deviceToken);
                    item.set("installationId", request.params.installationId);
                } else {
                    l.log("Installation existed, updating");
                }
                if (request.params.deviceToken != null) {
                    item.set("deviceToken", request.params.deviceToken);
                }
                item.set("owner", request.user);
                item.set("deviceType", request.params.deviceType);
                // for android START
                item.set("deviceTokenLastModified", request.params.deviceTokenLastModified);
                item.set("pushType", request.params.pushType);
                // END


                item.set("badge", request.params.badge);
                item.set("timeZone", request.params.timeZone);
                item.set("appName", request.params.appName);
                item.set("appVersion", request.params.appVersion);
                item.set("parseVersion", request.params.parseVersion);
                item.set("appIdentifier", request.params.appIdentifier);

                item.set("industry", request.params.industry);
                item.set("locationCountry", request.params.locationCountry);
                item.set("locationState", request.params.locationState);
                item.set("accountType", request.params.accountType);
                item.set("accountThreshold", request.params.accountThreshold);

                // to bypass a Parse error
                item.set("installationId", item.get("installationId"));

                item.save().then(function () {
                    l.log(true).despatch();
                    response.success();
                }, function (error) {
                    l.log(false, "item.save() error:", error).despatch();
                    return response.error("item.save() error: " + JSON.stringify(error));
                });
            }, error: function (error) {
                l.log(false, "query.find() error:", error).despatch();
                return response.error("query.find() error: " + JSON.stringify(error));
            }, useMasterKey:true
        });
});


Parse.Cloud.define("findUserInfo", function (request, response) {
    var userIds = request.params.userIds === undefined ? new Array() : request.params.userIds;
    if (request.params.version == 2 && request.params.userIds) {
        try {
            userIds = JSON.parse(_.decryptStringAES(request.params.userIds));
        } catch (e) {
        }
    }


    var query = new Parse.Query(Parse.User)
        .containedIn("objectId", userIds)
        .include("activeAccountTypeId")
        .limit(300);

    return query.find({
        success: function (results) {
            var output = new Array();

            for (var i = 0; i < results.length; ++i) {
                var userTmp = getPublicUserInfo(results[i]);
                output[i] = userTmp;
            }
            //
            // console.log("output output")
            // console.log(JSON.stringify(output))

            if (request.params.version === 2) {
                output = _.encryptStringAES(JSON.stringify(output));
            }
            return response.success(output);
        },
        error: function (error) {
            l.log(false, "query.find() error:", error).despatch();
            response.error("query.find() error: " + JSON.stringify(error));
        },
        useMasterKey:true
    });
});


/*

 request after decryption
 {
 version: 2,
 contacts: [
 {
 value: "97668787876789798",  //we asume this contact doesn't exist
 type: "facebook"
 },
 {
 value: "hao@thinkun.net",
 type: "email"
 },
 {
 value: "+61420350909",
 type: "phoneNumber"
 }
 ]
 }

 success response before encryption
 [
 {
 email: 		"hao@thinkun.net",
 name: 		"Hao Feng",
 hasSetDisplayName: true,
 gender: 	"0",
 organization: "Thinkun",
 publicBlrtCount: 5,
 id: 		uHIuhiusdfDF,
 createdAt: 	user.createdAt,
 avatarName: "",
 avatarUrl: "",
 facebookId: "",
 accountThreshold: 0,
 contactDetails:[
 {
 value: "hao@thinkun.net",
 type: "email"
 },
 {
 value: "+61420350909",
 type: "phoneNumber"
 }
 ]
 }
 ]

 */
Parse.Cloud.define("findUserInfoFromContacts", function (request, response) {
    var l = loggler.create("function:findUserInfoFromContacts");
    l.log("Params provided:", request.params);

    Parse.Cloud.useMasterKey();

    var contacts = request.params.contacts === undefined ? new Array() : request.params.contacts;

    if (request.params.version == 2 && request.params.contacts) {
        try {
            contacts = JSON.parse(_.decryptStringAES(request.params.contacts));
        } catch (e) {
        }
    }


    var query = new Parse.Query("UserContactDetails")
        .containedIn("value", _.map(contacts, function (c) {
            return c.value;
        }))
        .notEqualTo("placeholder", true)
        .include("user")
        .include("user.activeAccountTypeId");

    return query.find({
        success: function (results) {
            var output = [];

            for (var i = 0; i < results.length; ++i) {
                var contact = _.find(contacts, function (val) {
                    return val.value == results[i].get("value")
                        && val.type == results[i].get("type");
                });


                if (contact) {
                    var exist = _.find(output, function (val) {
                        return val.id == results[i].get("user").id;
                    });
                    if (!exist) {
                        var userTmp = getPublicUserInfo(results[i].get("user"));
                        output.push(userTmp);
                        exist = userTmp;
                    }
                    if (!exist.contactDetails) {
                        exist.contactDetails = [];
                    }
                    exist.contactDetails.push(contact);
                }
            }

            l.log(true, output).despatch();
            if (request.params.version == 2) {
                output = _.encryptStringAES(JSON.stringify(output));
            }
            return response.success(output);
        },
        error: function (error) {
            l.log(false, "query.find() error:", error).despatch();
            response.error("query.find() error: " + JSON.stringify(error));
        },
        useMasterKey:true
    });
});


function getPublicUserInfo(user) {
    var result = {
        email: (user.get("allowPublicEmail") == null || user.get("allowPublicEmail")) ? user.getUsername() : "",
        name: user.get("name"),
        hasSetDisplayName: user.get("hasnotSetDisplayname") ? false : true,
        gender: user.get("gender"),
        organization: user.get("organization") ? user.get("organization") : "",
        publicBlrtCount: user.get("publicBlrtCount") ? user.get("publicBlrtCount") : 0,
        id: user.id,
        createdAt: user.createdAt,
        isPhoneLinked: user.get("phoneNumber") ? user.get("phoneNumber") != "" : false,
        accountStatus: user.get("accountStatus") ? user.get("accountStatus") : ""
    };

    var avatar = user.get("avatar");
    if (avatar) {
        result.avatarName = avatar.name();
        result.avatarUrl = avatar.url();
    } else {
        result.avatarName = "";
        result.avatarUrl = "";
    }

    var authData = user.get("authData");
    if (authData && authData.facebook) {
        result.facebookId = authData.facebook.id;
    } else {
        result.facebookId = "";
    }

    if (user.get("activeAccountTypeId") != null) {
        result.accountThreshold = user.get("activeAccountTypeId").get("threshold")[0];
    } else {
        result.accountThreshold = 0;
    }

    // if (user.get("deleted")) //old user deletion logic
    //     result[email] = user.get("preDeletionEmail");

    if (user.get("accountStatus") == "deleted")
        result[email] = user.get("preDeletionEmail");  //deleted user's username should not be displayed

    return result;
}