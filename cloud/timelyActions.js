var email_nonUserInvited = require("cloud/timelyActions/email_nonUserInvited");
var pioneerTimelyAction = require("cloud/timelyActions/pioneerTimelyAction");
var pioneerInviteeMakeBlrt = require("cloud/timelyActions/pioneerInviteeMakeBlrt");
var pioneerUpgradeFailure = require("cloud/timelyActions/pioneerUpgradeFailure");
var linkBlrtRequest = require("cloud/timelyActions/linkBlrtRequest");
var runWebhook = require("cloud/timelyActions/runWebhook");
var signupTrial = require("cloud/timelyActions/signupTrial");
var autoComment = require("cloud/timelyActions/autoComment");
var constants = require("cloud/constants");
var loggler = require("cloud/loggler");
var _ = require("underscore");

exports.process = function (action) {
    console.log("Timely Action : ")
    console.log(JSON.stringify(action))

    switch (action.get("type")) {
        case "email_nonUserInvited":
        case "email_nonUserInvited_spawner":
            return email_nonUserInvited.process(action);
        case "PioneerTimelyAction":
            return pioneerTimelyAction.process(action);
        case "PioneerInviteeMakeBlrt":
            return pioneerInviteeMakeBlrt.process(action);
        case "PioneerUpgradeFailure":
            return pioneerUpgradeFailure.process(action);
        case "linkBlrtRequest":
        case "linkBlrtRequest_retryWebhook":
            return linkBlrtRequest.process(action);
        case "runWebhook":
            return runWebhook.process(action);
        case "signupTrial":
            return signupTrial.process(action);
        case "autoComment":
            return autoComment.process(action);
    }

    return Parse.Promise.error("No handler associated with action type for action with ID: " + action.id + " and type: " + action.get("type"));
};

exports.processAction = function (query, users) {
    Parse.Cloud.useMasterKey();

    var now = new Date();

    var numError = 0;
    var numSuccess = 0;
    var errors = [];
    var processedUserLoginUsers = [];
    var processedUserLoginUsersId = [];

    if (users) {
        processedUserLoginUsers = users;
        _.each(processedUserLoginUsers, function (user) {
            processedUserLoginUsersId.push(user.id);
        });
    }

    return query
        .include("user")
        .include("relation")
        .include("convo")
        .include("blrtRequest")
        .each(function (action) {
            var user = action.get("user");
            var category = action.get("category");
            if (category == "userLogin" && user) {  // this userLogin category is only used in Pioneer Feature(if i remember correctly.)
                //TODO: MAGIC
                if (_.indexOf(processedUserLoginUsersId, user.id) == -1) {
                    processedUserLoginUsers.push(user);
                    processedUserLoginUsersId.push(user.id);
                }
            }

            //process records in TimelyAction table.
            //return nothing if error do not present!
            return exports.process(action).then(function (dontDelete) {
                ++numSuccess;

                if (action.get("count") != null && action.get("count") > 1)
                    return action.increment("count", -1)
                        .set("executionTime", new Date(+new Date(action.get("executionTime")) + constants.hour * 24 * action.get("period")))
                        .save()
                        .fail(function (promiseError) {
                            loggler.global.log("ALERT: Error reducing count for TimelyAction " + action.id + ":", promiseError).despatch();
                            return Parse.Promise.as();
                        });
                else
                    return action.destroy()
                        .then(function () {
                            if (category == "userLogin" && user) {
                                return user.increment("userLoginActions", -1).save();
                            }
                        }, function (promiseError) {
                            loggler.global.log("ALERT: Error destroying TimelyAction " + action.id + ":", promiseError).despatch();
                            return Parse.Promise.as();
                        });
            }, function (actionError) {
                ++numError;
                errors.push(actionError);

                return Parse.Promise.as();
            });
        }).then(function () {
            //In the case that the deletion succeeds but the decrement fails, match the userLoginCounter
            var counterPromises = [];
            _.each(processedUserLoginUsers, function (user, index) {
                counterPromises.push(
                    user.fetch().then(function (user) {
                        var userLoginCounter = user.get("userLoginActions") || 0;
                        return new Parse.Query("TimelyAction")
                            .equalTo("category", "userLogin")
                            .equalTo("user", user)
                            .count()
                            .then(function (count) {
                                if (count != userLoginCounter) {
                                    loggler.global.log("Changed userLoginCounter:", user).despatch();
                                    return user.increment("userLoginActions", count - userLoginCounter).save();
                                }
                            }, function (error) {
                                loggler.global.log("ALERT: Error counting timelyAction:", user).despatch();
                                return Parse.Promise.error("Error counting timelyAction: " + JSON.stringify(user));
                            });
                    }, function (error) {
                        loggler.global.log("ALERT: Error fetch user:", user).despatch();
                        return Parse.Promise.error("Error fetch user: " + JSON.stringify(user));
                    })
                );
            });

            return Parse.Promise.when(counterPromises).then(function () {
                if (numError) {
                    loggler.global.log(false, "ALERT: " + numSuccess + " successful, " + numError + " errors:", errors).despatch();
                    return {
                        success: false,
                        errors: errors,
                        message: numSuccess + " successful, " + numError + " errors:\n" + JSON.stringify(errors)
                    };
                }
                return {success: true, message: numSuccess + " processed"};
            });
        }, function (promiseError) {
            loggler.global.log("ALERT: Error searching for TimelyActions:", promiseError).despatch();
            return {success: false, message: promiseError};
        });
};