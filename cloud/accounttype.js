var constants = require('cloud/constants')

var trackImmediate = require('cloud/analytics-node').trackImmediate;
// var constants = require("cloud/constants.js");
var Settings = require("cloud/settings.js");
var Email = require("cloud/email.js");
var loggler = require("cloud/loggler.js");
var _ = require("underscore");

var stripe = require("../lib/parse-stripe");
stripe.initialize(constants.stripeSecretKey);
// var job=require('../index').job
Parse.Cloud.job("nearExpireNotifications", function (request, response) {
    var l = loggler.create("job:nearExpireNotifications");
    var now = new Date();

    var accountTypeQuery = new Parse.Query(constants.accountType.keys.className)
        .notEqualTo(constants.accountType.keys.nearExpirationNotifications, null);


    var emailPromises = [new Parse.Promise.as(null)];

    var accountTypes = {};

    var notificationGroups = {};
    var notificationGroupsIds = {};

    return accountTypeQuery.find(function (ats) {
        for (var at_i = ats.length - 1; at_i >= 0; --at_i) {
            var at = ats[at_i];
            accountTypes[at.id] = at; // Hold it for later


            var keys = Object.keys(at.get(constants.accountType.keys.nearExpirationNotifications));

            for (var keys_i = keys.length - 1; keys_i >= 0; --keys_i) {
                var key = keys[keys_i];


                if (!(key in notificationGroups)) {
                    notificationGroups[key] = [];
                    notificationGroupsIds[key] = [];
                }

                notificationGroups[key].push(at);
                notificationGroupsIds[key].push(at.id);
            }
        }

        l.log("AccountTypes with nearExpirationNotifications:", ats).log("Account type notificationGroups:", notificationGroups);
    }).then(function () {
        if (Object.getOwnPropertyNames(notificationGroups).length == 0) {
            l.log(true, "No notifications setup").despatch();
            return response.success("No notifications setup");
        }


        var daysToNotificationArray = [];
        var userQuery = null;

        // TODO: more than 9 groups (or limit)
        for (var daysToNotification in notificationGroups) {
            daysToNotificationArray.push(daysToNotification);


            var query = new Parse.Query(Parse.User)
                .containedIn(constants.user.keys.activeAccountType, notificationGroups[daysToNotification])
                .lessThan(constants.user.keys.AccountTypeExpiration, new Date(now * 1 + constants.hour * 24 * parseInt(daysToNotification)))
                .equalTo(constants.user.keys.ExpirationInPrefix + daysToNotification, null);


            if (userQuery == null)
                userQuery = query;
            else
                userQuery = Parse.Query.or(userQuery, query);
        }

        daysToNotificationArray.sort(function (a, b) {
            return a - b;
        });
        l.log("daysToNotificationArray:", daysToNotificationArray);


        var modifiedUsers = [];
        var notificationCounter = 0;
        var emailsSent = 0;
        var pushesSent = 0;
        var users = [];
        return userQuery.each(function (theTempUser) {
            return users.push(theTempUser);
        }).then(function () {
            for (var user_i = users.length - 1; user_i >= 0; --user_i) {
                var user = users[user_i];

                if (user.get(constants.user.keys.AccountTypeExpiration) <= now) {
                    l.log("Skipping seemingly expired user:", user.id);
                    continue;
                }

                var userAccountType = accountTypes[user.get(constants.user.keys.activeAccountType).id]; // Save the .include, already fetched accounttypes :P

                for (var d_i = 0; d_i < daysToNotificationArray.length; ++d_i) { // Order is important
                    var daysToNotification = daysToNotificationArray[d_i];

                    if (notificationGroupsIds[daysToNotification].indexOf(userAccountType.id) > -1 &&
                        user.get(constants.user.keys.AccountTypeExpiration) < new Date(now * 1 + constants.hour * 24 * parseInt(daysToNotification))) {

                        if (user.get(constants.user.keys.ExpirationInPrefix + daysToNotification) != null) {
                            l.log("Skipping user with nearExpiration" + daysToNotification + " set, user.id:", user.id);
                            break;
                        }

                        var timeToExpiration = user.get(constants.user.keys.AccountTypeExpiration) * 1 - now * 1;

                        var notifications = userAccountType.get(constants.accountType.keys.nearExpirationNotifications);
                        var notification = notifications[daysToNotification]; // Apparently I need to avoid double barrelling: [][]
                        l.log("User:", user.id, "with timeToExpiration:", timeToExpiration, "gets notification:", notification, "from AccountType:", userAccountType);


                        var daysLeft = parseInt(timeToExpiration / (constants.hour * 24));

                        if (notification[constants.accountType.keys.NotificationParam.Email]) {
                            l.log("User " + user.id + " will get notification email");
                            var template = notification[constants.accountType.keys.NotificationParam.Email];


                            emailPromises.push(Email.SendTemplateEmail({
                                template: template,
                                convo: null,
                                content: null,
                                recipients: [{
                                    name: user.get(constants.user.keys.FullName),
                                    email: user.get(constants.user.keys.Email),
                                    isUser: true,
                                    user: user
                                }],
                                sender: null,
                                creator: null,
                                customMergeVars: {
                                    "DAYS_REMAINING": daysLeft,
                                    "DAY_OR_DAYS": (daysLeft == 1 ? "day" : "days"),
                                    "EXPIRATION_DATE": user.get(constants.user.keys.AccountTypeExpiration)
                                }
                            }));


                            ++emailsSent;
                        } else
                            l.log("User " + user.id + " will NOT get notification email");


                        if (notification[constants.accountType.keys.NotificationParam.Push]) {
                            l.log("User " + user.id + " will get notification push");
                            var message = Email.GeneratePush(notification[constants.accountType.keys.NotificationParam.Push], {
                                t: 0
                            }, [
                                daysLeft,
                                (daysLeft == 1 ? "day" : "days")
                            ]);

                            var pushQuery = new Parse.Query(Parse.Installation)
                                .equalTo('owner', user);

                            Parse.Push.send({where: pushQuery, data: message}, { useMasterKey: true });


                            ++pushesSent;
                        } else
                            l.log("User " + user.id + " will NOT get notification push");


                        if (notification[constants.accountType.keys.NotificationParam.Action]) {
                            user.set(constants.user.keys.ExpirationInPrefix + daysToNotification, JSON.stringify(notification[constants.accountType.keys.NotificationParam.Action]));

                            ++notificationCounter;
                        } else
                            user.set(constants.user.keys.ExpirationInPrefix + daysToNotification, "true");

                        modifiedUsers.push(user);


                        break;
                    }
                }
            }

            if (modifiedUsers.length == 0) {
                l.log(true, "No notifications sent").despatch();
                return response.success("No notifications sent");
            }


            return Parse.Object.saveAll(modifiedUsers).then(function () {
                var pushQuery = new Parse.Query(Parse.Installation)
                    .containedIn("owner", modifiedUsers);

                Parse.Push.send({where: pushQuery, data: Email.StandardizePushMessage({t:"0"})}, { useMasterKey: true });

                return Parse.Promise.when(emailPromises).then(function () {
                    l.log(true, "Notification popups: " + notificationCounter + ", emails: " + emailsSent + ", pushes: " + pushesSent).despatch();
                    return response.success("Notification popups: " + notificationCounter + ", emails: " + emailsSent + ", pushes: " + pushesSent);
                }, function (errors) {
                    l.log(true, "Notification popups: " + notificationCounter + ", pushes: " + pushesSent + ", emails failed though:", errors).despatch();
                    return Parse.Promise.as(response.success("Notification popups: " + notificationCounter + ", pushes: " + pushesSent + ", emails failed though: " + JSON.stringify(errors)));
                });
            }, function (error) {
                l.log(false, "saveAll(modifiedUsers) error:", error).despatch();
                return response.error("saveAll(modifiedUsers) error: " + JSON.stringify(error));
            });
        }, function (error) {
            l.log(false, "userQuery.each() error:", error).despatch();
            return response.error("userQuery.each() error: " + JSON.stringify(error));
        });
    }, function (error) {
        l.log(false, "accountTypeQuery.find() error:", error).despatch();
        return response.error("accountTypeQuery.find() error: " + JSON.stringify(error));
    });
},"* */1 * * * *");


Parse.Cloud.job("expireAccounts", function (request, response) {
    console.log("=============== expireAccounts =============")
    var now = new Date();

    var userQuery = new Parse.Query(Parse.User)
        .notEqualTo("accountTypeExpirationDate", null)
        .lessThanOrEqualTo(constants.user.keys.AccountTypeExpiration, now)
        .include(constants.user.keys.activeAccountType);

    var extraPromises = [new Parse.Promise.as(null)];

    var users = [];
    var userTrails = {};
    var _users = [];

    return userQuery.each(function (theuser) {
        return _users.push(theuser);
    }).then(function () {
        if (_users.length == 0) {
            // l.log(true, "No users to expire").despatch();
            return response.success("No users to expire");
        }

        users = _users; // Scared of JS scoping


        var trailQuery = new Parse.Query(constants.accountTypeTrail.keys.className)
            .containedIn(constants.accountTypeTrail.keys.user, users)
            .include(constants.accountTypeTrail.keys.AccountType);

        var toSave = users.slice();

        var trails = [];
        return trailQuery.each(function (theTrail) {
            return trails.push(theTrail);
        }).then(function () {
            for (var trail_i = trails.length - 1; trail_i >= 0; --trail_i) {
                var trailItem = trails[trail_i];
                var userId = trailItem.get(constants.accountTypeTrail.keys.user).id;


                var start = trailItem.get(constants.accountTypeTrail.keys.Start);
                var expiration = trailItem.get(constants.accountTypeTrail.keys.Expire);

                if (start > now || (expiration != null && expiration <= now))
                    continue;


                if (!(userId in userTrails))
                    userTrails[userId] = [];

                userTrails[userId].push(trailItem);
            }

            // For some reason some users have existed without a Free AccountTypeTrail.
            // If this is the case, then at some point (when all of their other AccountTypes
            // expire) they'll have nothing to fall-back to. This should never be the case.
            var promise = Parse.Promise.as();

            // So if this turns up somehow, we should create their Free trail and use it.
            var freeAccountTypePromise = new Parse.Promise();
            var userTrailPromises = [];
            _.chain(users)
                .filter(function (user) {
                    return !_.has(userTrails, user.id);
                })
                .map(function (userWithoutTrail) {
                    loggler.global.log("ALERT: Found user for expiration without any AccountTypeTrails. User ID: " + userWithoutTrail.id).despatch();

                    userTrailPromises.push(freeAccountTypePromise.then(function (accountType) {
                        var freeTrail = new Parse.Object("AccountTypeTrail")
                            .set("userId", userWithoutTrail)
                            .set("accountTypeId", accountType)
                            .set("startDate", userWithoutTrail.createdAt);

                        userTrails[userWithoutTrail.id] = [freeTrail];
                        toSave.unshift(freeTrail, new Parse.Object("AccountTypeLog")
                            .set("userId", userWithoutTrail)
                            .set("destinationAccountTypeId", accountType)
                            .set("consistentCreatedAt", userWithoutTrail.createdAt)
                            .set("parsed", true)
                            .set("note", "Was missing free log")
                        );
                    }));

                    return userWithoutTrail;
                })
                .tap(function (users) {
                    if (!_.isEmpty(users)) {
                        new Parse.Query("Settings")
                            .equalTo("key", "default_account_type_slug")
                            .first()
                            .then(function (defaultAccountTypeSlug) {
                                return new Parse.Query("AccountType")
                                    .equalTo("slug", defaultAccountTypeSlug.get("value"))
                                    .first()
                                    .then(function (accountType) {
                                        freeAccountTypePromise.resolve(accountType);
                                    });
                            });

                        promise = Parse.Promise.when(userTrailPromises);
                    }
                });

            return promise.then(function () {
                var oldUserAccountTypes = {};
                var destinationAccountTypeTrails = {};


                for (var user_i = users.length - 1; user_i >= 0; --user_i) {
                    var user = users[user_i];


                    var destinationTrail;
                    var maxThreshold = -Number.MAX_VALUE;

                    for (var trailItem_i = userTrails[user.id].length - 1; trailItem_i >= 0; --trailItem_i) {
                        var trailItem = userTrails[user.id][trailItem_i];


                        var accountType = trailItem.get(constants.accountTypeTrail.keys.AccountType);


                        var threshold = -Number.MAX_VALUE;
                        for (var i = accountType.get(constants.accountType.keys.Threshold).length - 1; i >= 0; --i)
                            threshold = Math.max(threshold, accountType.get(constants.accountType.keys.Threshold)[i]);


                        if (threshold > maxThreshold || (threshold == maxThreshold
                            && destinationTrail.get(constants.accountTypeTrail.keys.Start) < trailItem.get(constants.accountTypeTrail.keys.Start))) {

                            destinationTrail = trailItem;
                            maxThreshold = threshold;
                        }
                    }


                    oldUserAccountTypes[user.id] = user.get(constants.user.keys.activeAccountType);
                    destinationAccountTypeTrails[user.id] = destinationTrail;


                    var expirationNotifications = oldUserAccountTypes[user.id].get(constants.accountType.keys.nearExpirationNotifications);
                    if (expirationNotifications != null) {
                        var keys = Object.keys(expirationNotifications);

                        for (var keys_i = keys.length - 1; keys_i >= 0; --keys_i)
                            user.set(constants.user.keys.ExpirationInPrefix + keys[keys_i], null);
                    }


                    user.set(constants.user.keys.activeAccountType, destinationTrail.get(constants.accountTypeTrail.keys.AccountType));

                    user.set(constants.user.keys.AccountTypeStart, destinationTrail.get(constants.accountTypeTrail.keys.Start));
                    if (destinationTrail.get(constants.accountTypeTrail.keys.Expire) == null)
                        user.set(constants.user.keys.AccountTypeExpiration, null);
                    else
                        user.set(constants.user.keys.AccountTypeExpiration, destinationTrail.get(constants.accountTypeTrail.keys.Expire));

                    user.set(constants.user.keys.expiredAction, JSON.stringify(oldUserAccountTypes[user.id].get(constants.accountType.keys.expiredAction)));
                }

                return Parse.Object.saveAll(toSave, function () {
                    // var mp = new Mixpanel({batchMode: true});

                    for (var user_i = users.length - 1; user_i >= 0; --user_i) {
                        var user = users[user_i];
                        var oldAccountType = oldUserAccountTypes[user.id];
                        var destinationTrail = destinationAccountTypeTrails[user.id];


                        var push = oldAccountType.get(constants.accountType.keys.expiredPush);
                        if (push != null) {
                            var message = Email.GeneratePush(push, {
                                t: 5
                            });

                            var pushQuery = new Parse.Query(Parse.Installation)
                                .equalTo('owner', user);

                            Parse.Push.send({where: pushQuery, data: message}, { useMasterKey: true });
                        }

                        var template = oldAccountType.get(constants.accountType.keys.expiredEmailTemplate);
                        if (template != null) {
                            extraPromises.push(Email.SendTemplateEmail({
                                template: template,
                                convo: null,
                                content: null,
                                recipients: [{
                                    name: user.get(constants.user.keys.FullName),
                                    email: user.get(constants.user.keys.Email),
                                    isUser: true,
                                    user: user
                                }],
                                sender: null,
                                creator: null
                            }));
                        }

                        trackImmediate.track({
                            userId: user.id,
                            event: 'Account Type Expired',
                                properties: {
                                    "Source Account Type": oldAccountType.id,
                                    "Destination Account Type": destinationTrail.get("accountTypeId").id,
                                    "From ebs" :"hahahh"
                                }
                        });
                    }

                    // extraPromises.push(mp.despatch());

                    var pushQuery = new Parse.Query(Parse.Installation)
                        .containedIn("owner", users);

                    Parse.Push.send({where: pushQuery, data: Email.StandardizePushMessage({t:"0"})}, { useMasterKey: true });

                    return Parse.Promise.when(extraPromises).then(function () {
                        l.log(true, "Expired " + users.length + " users").despatch();
                        return response.success("Expired users: " + users.length);
                    }, function (errors) {
                        l.log(true, "Expired " + users.length + " users, but emails failed:", errors).despatch();
                        return Parse.Promise.as(response.success("Expired users: " + users.length + ", but emails failed: " + JSON.stringify(errors)));
                    });
                });
            });
        }, function (error) {
            l.log(false, "trailQuery.find() error:", error).despatch();
            return response.error("trailQuery.find() error: " + JSON.stringify(error));
        });
    }, function (error) {
        l.log(false, "userQuery.find() error:", error).despatch();
        return response.error("userQuery.find() error: " + JSON.stringify(error));
    });
},"* */1 * * * *");


Parse.Cloud.job("parseAccountTypeLogs", function (request, response) {
    var l = loggler.create("job:parseAccountTypeLogs");
    Parse.Cloud.useMasterKey();


    var logQuery = new Parse.Query(constants.accountTypeLog.keys.className)
        .equalTo(constants.accountTypeLog.keys.Parsed, null)
        .include(constants.accountTypeLog.keys.DestinationAccountType);

    var users = [];
    //id -> log
    var userLogs = {};
    //id -> trail
    var userTrails = {};

    var youngestLog = Number.MAX_VALUE;

    var modifiedLogs = [];
    var modifiedTrails = [];

    var logs = [];
    return logQuery.each(function (log) {
        return logs.push(log);
    }).then(function () {
        if (_.isEmpty(logs)) {
            l.log(true, "No logs to parse").despatch();
            return response.success("No logs to parse");
        }


        for (var log_i = logs.length - 1; log_i >= 0; --log_i) {
            var log = logs[log_i];

            var user = log.get(constants.accountTypeLog.keys.user);

            //[Guichi] One user can generate multiple AccountTypeLog entry
            //i.e. A user start free trail ,within the following 15 minutes ,upgrade account
            //Also some unexpected duplication error can happen i.e. some entry can be saved more than once
            //due to server error
            if (!(user.id in userLogs)) {
                users.push(user);
                userLogs[user.id] = [];
                userTrails[user.id] = [];
            }

            //[Guichi] it seems the oldest to me ,when some unlucky programmers read the code do not be confused by the name
            //[Guichi] "youngestLog" is actually oldest un-parsed timestamp
            var consistentCreatedAt = log.get(constants.accountTypeLog.keys.consistentCreatedAt);
            if (youngestLog > consistentCreatedAt)
                youngestLog = consistentCreatedAt;


            log.set(constants.accountTypeLog.keys.Parsed, true);
            userLogs[user.id].push(log);
            //the modify only apply for "Parsed"
            modifiedLogs.push(log);
        }

        //[Guichi] after this step userLogs is populated
    }).then(function () {
        //todo where "expire" from and what is do
        //todo "expire" seems to be expire date for a specific account type
        //todo order the un-parsed log ,the younger ones seem to be processed already,but where they from ???
        var trailQuery = Parse.Query.or(
            new Parse.Query(constants.accountTypeTrail.keys.className)
                .containedIn(constants.accountTypeTrail.keys.user, users)
                .greaterThanOrEqualTo(constants.accountTypeTrail.keys.Expire, youngestLog), // not expire from sometime in past
            new Parse.Query(constants.accountTypeTrail.keys.className)
                .containedIn(constants.accountTypeTrail.keys.user, users)
                .equalTo(constants.accountTypeTrail.keys.Expire, null)
        ).include(constants.accountTypeTrail.keys.AccountType);

        var trails = [];
        return trailQuery.each(function (trail) {
            return trails.push(trail);
        }).then(function () {
            for (var trail_i = trails.length - 1; trail_i >= 0; --trail_i) {
                var trailItem = trails[trail_i];

                var user = trailItem.get(constants.accountTypeTrail.keys.user);

                if (trailItem.get(constants.accountTypeTrail.keys.Expire) == null ||
                    trailItem.get(constants.accountTypeTrail.keys.Expire) >= userLogs[user.id][0].get(constants.accountTypeLog.keys.consistentCreatedAt)
                )
                    userTrails[trailItem.get(constants.accountTypeTrail.keys.user).id].push(trailItem);
            }

        }).then(function () {
            for (var user_i = users.length - 1; user_i >= 0; --user_i) {
                var user = users[user_i];


                var logs = userLogs[user.id];
                var trails = userTrails[user.id];

                // The order of parsing the logs is essential to maintaining correctness
                logs.sort(function (a, b) {
                    return a.get(constants.accountTypeLog.keys.consistentCreatedAt) - b.get(constants.accountTypeLog.keys.consistentCreatedAt);
                });
                // The trail list is required for when delaying trails (purchased a weaker account)
                trails.sort(function (a, b) {
                    return a.get(constants.accountTypeTrail.keys.Start) - b.get(constants.accountTypeTrail.keys.Start);
                });


                var trailStack = [];

                if (trails.length == 0) {
                    // If no trails exist then this is the first log for the user, which should be for the free account type, indefinitely
                    var firstTrail = new Parse.Object(constants.accountTypeTrail.keys.className);

                    firstTrail.set(constants.accountTypeTrail.keys.user, user);
                    firstTrail.set(constants.accountTypeTrail.keys.AccountType, logs[0].get(constants.accountTypeLog.keys.DestinationAccountType));
                    firstTrail.set(constants.accountTypeTrail.keys.Start, logs[0].get(constants.accountTypeLog.keys.consistentCreatedAt));

                    trailStack.push(firstTrail);

                    // This new trail will be saved after the first log is parsed
                    // This is because the first log is guaranteed to be the "free" log (which is created when the user signs up)
                    // This is the case because the only way the user would have no existing trails is if none of their logs were ever parsed
                }

                for (var log_i = 0; log_i < logs.length; ++log_i) {
                    var log = logs[log_i];

                    // Build the trail stack to see which trails apply based on the time for this log
                    if (log_i == 0) {
                        _.each(trails, function (trail) {
                            if (trail.get(constants.accountTypeTrail.keys.Expire) == null
                                || trail.get(constants.accountTypeTrail.keys.Expire) >= log.get(constants.accountTypeLog.keys.consistentCreatedAt)
                            )
                                trailStack.push(trail);
                        });
                    } else {
                        while (trailStack[trailStack.length - 1].get(constants.accountTypeTrail.keys.Expire) != null
                        && trailStack[trailStack.length - 1].get(constants.accountTypeTrail.keys.Expire) < log.get(constants.accountTypeLog.keys.consistentCreatedAt)
                            )
                            trailStack.pop();
                    }

                    var strongestAccountTypeTrail = trailStack[trailStack.length - 1];

                    l.log("Applying log ID: " + log.id + " to stack:", trailStack);

                    var strongestAccountType = strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.AccountType);

                    var strongestAccountTypeThreshold = Number.MAX_VALUE;
                    for (var j = strongestAccountType.get(constants.accountType.keys.Threshold).length - 1; j >= 0; --j)
                        strongestAccountTypeThreshold = Math.min(strongestAccountTypeThreshold, strongestAccountType.get(constants.accountType.keys.Threshold)[j]);

                    var logAccountType = log.get(constants.accountTypeLog.keys.DestinationAccountType);

                    var threshold = -Number.MAX_VALUE;
                    for (var k = logAccountType.get(constants.accountType.keys.Threshold).length - 1; k >= 0; --k)
                        threshold = Math.max(threshold, logAccountType.get(constants.accountType.keys.Threshold)[k]);

                    var start = log.get(constants.accountTypeLog.keys.consistentCreatedAt);
                    var duration = null;

                    if (log.get(constants.accountTypeLog.keys.customDuration) != null) {
                        if (log.get(constants.accountTypeLog.keys.customDuration) != 0)
                            duration = log.get(constants.accountTypeLog.keys.customDuration);
                    } else
                        duration = logAccountType.get(constants.accountType.keys.Duration);

                    if (logAccountType.id == strongestAccountType.id) {
                        if (duration != null) {
                            var expiration = strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.Expire);
                            if (expiration == null)
                                expiration = log.get(constants.accountTypeLog.keys.consistentCreatedAt);

                            strongestAccountTypeTrail.set(constants.accountTypeTrail.keys.Expire, new Date(expiration * 1 + constants.hour * 24 * duration));
                        } else
                            strongestAccountTypeTrail.set(constants.accountTypeTrail.keys.Expire, null);


                        modifiedTrails.push(strongestAccountTypeTrail);

                        l.log("Applied log ID: " + log.id, "Resulting trail:", strongestAccountTypeTrail);
                    } else if (threshold >= strongestAccountTypeThreshold) {
                        var expiration = null;

                        // Otherwise activate the currentAccountType now
                        if (duration != null) {
                            if (strongestAccountTypeThreshold == constants.accountType.accountThreshold.Trial
                                && threshold == constants.accountType.accountThreshold.Premium) {
                                // If you're currently on trial and upgrading to premium then add the remaining duration to the subscription
                                expiration = new Date(strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.Expire, expiration) * 1 + constants.hour * 24 * duration);
                            } else
                                expiration = new Date(start * 1 + constants.hour * 24 * duration);
                        }


                        var newTrail = new Parse.Object(constants.accountTypeTrail.keys.className);
                        newTrail.set(constants.accountTypeTrail.keys.user, user);
                        newTrail.set(constants.accountTypeTrail.keys.AccountType, logAccountType);
                        newTrail.set(constants.accountTypeTrail.keys.Start, start);

                        if (expiration != null)
                            newTrail.set(constants.accountTypeTrail.keys.Expire, expiration);

                        strongestAccountType = logAccountType;

                        strongestAccountTypeThreshold = Number.MAX_VALUE;
                        for (var k = strongestAccountType.get(constants.accountType.keys.Threshold).length - 1; k >= 0; --k)
                            strongestAccountTypeThreshold = Math.min(strongestAccountTypeThreshold, strongestAccountType.get(constants.accountType.keys.Threshold)[k]);

                        trailStack.push(newTrail);

                        modifiedTrails.push(newTrail);

                        l.log("Applied log ID: " + log.id, "Resulting trail:", newTrail);
                    } else {
                        // TO BE IMPLEMENTED IN THE FUTURE MAYBE - PURCHASING LOWER ACCOUNTS (delayed activation)
                        l.log(false, "Attempted to downgrade? Log:", log).despatch();
                        return response.error("SCARY BIG ALERT error (log \"" + log.id + "\") - account downgrade not supported yet!!!!! REQUIRES MANUAL INTERVENTION");
                    }
                }
            }

            return Parse.Object.saveAll(modifiedLogs.concat(modifiedTrails)).then(function () {
                l.log(true, "Parsed " + modifiedLogs.length + " logs").despatch();
                return response.success("Parsed logs: " + modifiedLogs.length);
            }, function (error) {
                l.log(false, "saveAll(modifiedLogs.concat(modifiedTrails)) error:", error).despatch();
                return response.error("Log/Trail saveAll error: " + JSON.stringify(error));
            });
        }, function (error) {
            l.log(false, "trailQuery.find() error:", error).despatch();
            return response.error("trailQuery.find() error: " + JSON.stringify(error));
        });
    }, function (error) {
        l.log(false, "logQuery.find() error:", error).despatch();
        return response.error("logQuery.find() error: " + JSON.stringify(error));
    });
},"* */1 * * * *");

/*
 // REQUEST
 request.params = {
 requestFor: "trial",
 skipTracking: true,
 data: {
 coupon: <coupon code>
 }
 }

 // no user provided
 request.params = {
 requestFor: "stripe",
 data: {
 token: <stripe token>,
 destination: <account type slug>,
 coupon: <coupon code>
 }
 }

 // request.user may be required
 request.params = {
 requestFor: "iosiap",
 data: {
 user: <user objectId>,
 iap: <iap product id>
 transactionId: <iap transaction id>
 }
 }

 // request.user may be required
 request.params = {
 requestFor: "admin",
 data: {
 userEmail: <user objectId>,
 destination: <account type slug>,
 duration: <days>
 }
 }

 // RESPONSE
 response = {
 success: true
 }

 response = {
 success: false,
 error_code: BlrtAccountUpgradeResponseErrorCode
 }
 */

Parse.Cloud.define("requestAccountUpgrade", function (request, response) {
    //client doesn't allow coupon for trial
    if (request.params.requestFor == "trial" && request.params.data && request.params.data.coupon) {
        request.params.data.coupon = null;
    } else if (!request.params.data) {
        request.params.data = {};
    }

    var beforePromise = new Parse.Promise.as();
    //If we do not do this,the timely action will override the result
    if (request.params.requestFor == "trial" && request.user && request.user.get("signupCoupon")) {
        //check if there is a timely action
        if (!request.params.data) {
            request.params.data = {};
        }
        request.params.data.coupon = request.user.get("signupCoupon");

        //remove signupCoupon from user obj
        beforePromise = request.user.unset("signupCoupon").save(null,{useMasterKey:true});
    }

    var upgradeResult;
    return beforePromise
        .then(function () {
            //if request coming from client, we should skip tracking as client should already does this.
            request.params.skipTracking = true;

            return exports.requestAccountUpgrade(request);
        }).then(function (result) {
            upgradeResult = result;
            if (request.params.requestFor == "trial" && request.params.data.coupon) {
                if (result.success) {
                    //delete timely action
                    return new Parse.Query("TimelyAction")
                        .equalTo("user", request.user)
                        .equalTo("type", "signupTrial")
                        .first({useMasterKey:true})
                        .then(function (timely) {
                            if (!timely)
                                return;
                            return timely.destroy({useMasterKey:true});
                        });
                } else {
                    //recover signupCoupon
                    return request.user.set("signupCoupon", request.params.data.coupon).save(null,{useMasterKey:true});
                }
            }

        }).then(function () {
            return response.success(JSON.stringify(upgradeResult));
        }, function (error) {
            return response.error(error);
        });
});

exports.stripeRequest = function (token, destination, country, coupon) {
    return exports.requestAccountUpgrade({
        params: {
            requestFor: "stripe",
            data: {
                token: token,
                destination: destination,
                country: country,
                coupon: coupon
            }
        }
    }).fail(function (error) {
        return {success: false, message: constants.stripeGenericErrorMessage};
    });
};

exports.requestAccountUpgrade = function (request) {
    var l = loggler.create("function:requestAccountUpgrade");
    var user = request.user;
    var requestFor = request.params.requestFor;

    var invitedByPioneer;
    if (request.params.data)
        invitedByPioneer = request.params.data.invitedByPioneer;

    var transactionId = null;
    var charge = null;
    var couponObj = null;
    var couponValid = false;
    var couponCustomDuration = false;
    var stripeDiscount = 0;
    var stripePrice = null;
    var stripeCoupon = "";
    var durationModified = false;
    var payment = null;

    var now = new Date();

    var extraPromises = [Parse.Promise.as()];
    var waitables = [Parse.Promise.as()];


    //step 1: prepare coupon object if included ,append user object to request if not included
    return Parse.Promise.as().then(function () {
        switch (requestFor) {
            case constants.accountType.RequestFor.Trial:
                if (!user) {
                    return {
                        success: false,
                        message: "Cannot upgrade account: user not provided",
                        error_code: constants.accountType.ErrorCode.Unknown
                    };
                }

                if (user.get(constants.user.keys.HasDoneFreeTrial)) {
                    return {
                        success: false,
                        message: "Cannout upgrade account to free trial: user with objectId \"" + user.id + "\" has started a free trial in the past",
                        error_code: constants.accountType.ErrorCode.TrialAlreadyUsed
                    };
                }
                //[Guichi] free trail can extend the days with a coupon ?
                if (_.isString(request.params.data.coupon)) {
                    waitables.push(new Parse.Query("CouponCode")
                        .equalTo("code", request.params.data.coupon.trim().toLowerCase())
                        .first({useMasterKey:true})
                        .then(function (coupon) {
                            if (!coupon)
                                return;
                            couponObj = coupon;
                            couponCustomDuration = coupon.get("customDuration");
                            couponValid = true;
                        })
                    );
                }

                break;
            case constants.accountType.RequestFor.stripe:


                //[Guichi] we support two type of stripe upgrade
                //If the coupon code is not included or discount of the code is not 1,the stripe info is required
                //Otherwise,the stripe payment data is not required,i.e, free upgrade
                //In this stage ,only check the existence of coupon ,checking if the discount is 1 will be deferred until couponObj is available
                if (!request.params.data || !request.params.data.destination || (!request.params.data.token && !request.params.data.coupon)) {
                    return {
                        success: false,
                        message: constants.stripeGenericErrorMessage
                    };
                }
                var shouldBeFreeUpdate;
                if (request.params.data.token && request.params.data.token.id) {
                    transactionId = constants.accountType.RequestFor.stripe + "-" + request.params.data.token.id;
                }

                if (!request.params.data.token && request.params.data.coupon) {
                    shouldBeFreeUpdate = true;
                }


                //[Guichi]
                //previously ,this cloud function called at blrt.com ,so the request.user is not included,we need append the
                //the user to request based on email
                //now this function call be called in webapp,Parse sdk will handle this
                if (!request.user) {
                    waitables.push(new Parse.Query(constants.contactTable.keys.className)
                        // The non-logged in use able to send request as long as he provide a valid email address
                            .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                            .equalTo(constants.contactTable.keys.Value, request.params.data.token.email.toLowerCase())
                            .include(constants.contactTable.keys.user)
                            .first({useMasterKey:true})
                            .then(function (contact) {
                                if (contact != null)
                                    user = contact.get(constants.contactTable.keys.user);
                                request.user = user;
                                return true;
                                // return l.log("fetchedUser (email: " + request.params.data.token.email.toLowerCase() + "):", user);
                            })
                    );
                }


                if (_.isString(request.params.data.coupon)) {
                    waitables.push(new Parse.Query("CouponCode")
                        .equalTo("code", request.params.data.coupon.trim().toLowerCase())
                        .first({useMasterKey:true})
                        .then(function (coupon) {
                            if (!coupon)
                                return;
                            couponObj = coupon;
                            stripeDiscount = coupon.get("discount");
                            stripePrice = coupon.get("price");
                            stripeCoupon = coupon.get("marketingName");
                            couponCustomDuration = coupon.get("customDuration");
                        })
                    )
                }

                break;
            case constants.accountType.RequestFor.iOSIAP:
            case constants.accountType.RequestFor.AndroidIAP:
                var transactionIdPrefix = requestFor == constants.accountType.RequestFor.iOSIAP ? constants.accountType.RequestFor.iOSIAP : constants.accountType.RequestFor.AndroidIAP;

                if (request.params.data != null && request.params.data.transactionId != null)
                    transactionId = transactionIdPrefix + "-" + request.params.data.transactionId;

                if (transactionId == null) {
                    return Parse.Promise.error("Missing transactionId for IAP request!");
                }

            //////////// ALERT
            // Continues onto next case
            ////////////
            case constants.accountType.RequestFor.admin:
                if ((request.params.data.user != null || request.params.data.userEmail != null) // If any additional user data was provided
                    && (user == null || request.params.data.user != user.id)) { // and the request does not contain the any or the correct user
                    // get the user that's actually supposed to be changed
                    var userQuery = new Parse.Query(Parse.User);
                    if (request.params.data.user != null)
                        userQuery = userQuery
                            .equalTo(constants.base.keys.objectId, request.params.data.user);
                    else
                        userQuery = userQuery
                            .equalTo("username", request.params.data.userEmail.trim().toLowerCase());


                    waitables.push(userQuery.first({useMasterKey:true}).then(function (fetchedUser) {
                        request.user = fetchedUser;
                        user = fetchedUser;

                        l.log("fetchedUser:", fetchedUser);
                        return Parse.Promise.as();
                    }, function (error) {
                        l.log("userQuery.first() error:", error);
                        return Parse.Promise.error("userQuery.first() error: " + JSON.stringify(error));
                    }));
                } else if (user == null) {
                    return {
                        success: false,
                        message: "Cannot upgrade account: cannot deduce user from provided parameters",
                        error_code: constants.accountType.ErrorCode.Unknown
                    };
                }

                break;
            default:
                l.log(false, "Unknown request type:", requestFor).despatch();
                return {
                    success: false,
                    message: "Unknown request type: " + requestFor,
                    error_code: constants.accountType.ErrorCode.Unknown
                };
        }


        //step 2
        //2.1 validate coupon and user
        //2.2 prepare destinationAccountType and accountTypeLog
        return Parse.Promise.when(waitables).then(function () {


            //2.1 validate coupon for strip
            if (requestFor != constants.accountType.RequestFor.stripe || !request.user || !request.params.data.coupon)
                return;
            if (!couponObj && request.params.data.coupon) {
                return {valid: false, code: 5}
            }
            // if (couponObj&&couponObj.get("couponType") != "stripe")
            //     return {valid: false, code: 1};
            return IsValidCouponForUser(couponObj, request.user, {shouldBeFreeUpdate: shouldBeFreeUpdate}).then(function (validRes) {
                couponValid = validRes.valid;
                return validRes;
            });
        }).then(function (stripeValidRes) {

            if (stripeValidRes && !stripeValidRes.valid) {
                if (stripeValidRes.code == 2) {
                    return {
                        success: false,
                        message: "Sorry, this coupon code has been used up. Your payment has not been processed."
                    };
                }

                if (stripeValidRes.code == 4) {
                    return {
                        success: false,
                        message: "Not a free coupon ,stripe details needed "
                    };
                }
                if (stripeValidRes.code == 5) {
                    return {
                        success: false,
                        message: "The provided coupon is not valid "
                    };
                }

                else
                    return {success: false, message: "Sorry, that coupon code is invalid."};
            }

            // return {success: true, message: "stop here "}

            var iapSource = null;
            var settingsQuery = null;
            var transactionQuery = null;

            if (user == null) {
                return {
                    success: false,
                    message: requestFor == constants.accountType.RequestFor.stripe
                        ? "No account exists for the specified email address. Make sure the email address used in the Upgrade form matches the primary email of the account you want to upgrade"
                        : "Failed to find user to upgrade",
                    error_code: constants.accountType.ErrorCode.CurrentAccountTypeSuperior,
                    dropToken: true
                };
            }

            switch (requestFor) {
                case constants.accountType.RequestFor.stripe:
                    settingsQuery = Parse.Promise.as(request.params.data.destination);
                    if (transactionId) {
                        transactionQuery = new Parse.Query(constants.accountTypeLog.keys.className)
                            .equalTo(constants.accountTypeLog.keys.transactionId, transactionId)
                            .first({useMasterKey:true});
                    }

                    break;
                case constants.accountType.RequestFor.AndroidIAP:
                case constants.accountType.RequestFor.iOSIAP: // Not C# - case order is important
                    iapSource = requestFor == constants.accountType.RequestFor.iOSIAP ? constants.os.iOS : constants.os.Android;

                    transactionQuery = new Parse.Query(constants.accountTypeLog.keys.className)
                        .equalTo(constants.accountTypeLog.keys.transactionId, transactionId)
                        .first({useMasterKey:true});

                // Continues into next case
                case constants.accountType.RequestFor.Trial:
                    settingsQuery = Settings.GetSettings(request, true, iapSource).then(function (settings) {
                        switch (requestFor) {
                            case constants.accountType.RequestFor.Trial:
                                return settings.Settings[constants.settings.keys.FreeTrialAccountTypeSlug];
                            case constants.accountType.RequestFor.AndroidIAP:
                            case constants.accountType.RequestFor.iOSIAP:
                                l.log(false, "Settings:", settings).despatch();
                                var availableIAPs = settings.IAP[constants.iap.AvailableIAPs];
                                return availableIAPs[request.params.data.iap];
                        }
                    });


                    break;
                case constants.accountType.RequestFor.admin:
                    if (user.get(constants.user.keys.activeAccountType) == null)
                        settingsQuery = Settings.GetSettings(request, true) // Run settings so this user has their default account type set
                            .then(function () {
                                return user.fetch(); // Update the user record
                            }).then(function () {
                                return request.params.data.destination;
                            });
                    else
                        settingsQuery = Parse.Promise.as(request.params.data.destination);
                    break;
            }

            if (transactionQuery == null)
                transactionQuery = Parse.Promise.as(null);

            //step 3
            // 3.1 check accountTypeLog
            // 3.2 prepare current accountType and destination accountType
            return Parse.Promise.when(settingsQuery, transactionQuery).then(function (destinationAccountTypeSlug, duplicateTransactionLog) {
                //3.1 check accountTypeLog
                if (duplicateTransactionLog != null) {
                    if (duplicateTransactionLog.get(constants.accountTypeLog.keys.consistentCreatedAt) > new Date(now * 1 - constants.hour / 12)) {
                        l.log(false, "Recent duplicate transaction found:", duplicateTransactionLog).despatch();
                        return {
                            success: requestFor == constants.accountType.RequestFor.stripe ? true : false,
                            message: "Recent duplicate transaction found, with log: " + duplicateTransactionLog.id,
                            error_code: constants.accountType.ErrorCode.recentDuplicateRequest
                        };
                    } else {
                        l.log(false, "Duplicate transaction found:", duplicateTransactionLog).despatch();
                        return {
                            success: requestFor == constants.accountType.RequestFor.stripe ? true : false,
                            message: "Duplicate transaction found, with log: " + duplicateTransactionLog.id,
                            error_code: constants.accountType.ErrorCode.duplicateRequest
                        };
                    }
                }


                //3.2 prepare current accountType and destination accountType
                var userAccountTypeQuery = new Parse.Query(constants.accountType.keys.className)
                    .equalTo(constants.base.keys.objectId, user.get(constants.user.keys.activeAccountType).id);

                var destinationAccountTypeQuery = new Parse.Query(constants.accountType.keys.className)
                    .equalTo(constants.accountType.keys.Slug, destinationAccountTypeSlug);

                var accountTypeQuery = Parse.Query.or(userAccountTypeQuery, destinationAccountTypeQuery)
                    .include(constants.base.keys.permissions);


                //step 4
                //4.1 compare current accountType and destination accountType
                //4.2 add couponLog
                return accountTypeQuery.find({useMasterKey:true}).then(function (results) {
                    //4.1 compare current accountType and destination accountType
                    var currentAccountType = null;
                    var destinationAccountType = null;

                    for (var i = results.length - 1; i >= 0; --i) {
                        var result = results[i];

                        // This is only necessary because I'm using the same query to fetch the destination and active account type (to reduce API calls)
                        if (result.id == user.get(constants.user.keys.activeAccountType).id)
                            currentAccountType = result;
                        if (result.get(constants.accountType.keys.Slug) == destinationAccountTypeSlug)
                            destinationAccountType = result;
                    }
                    ;
                    var destinationThreshold = -Number.MAX_VALUE;
                    for (var i = destinationAccountType.get(constants.accountType.keys.Threshold).length - 1; i >= 0; --i)
                        destinationThreshold = Math.max(destinationThreshold, destinationAccountType.get(constants.accountType.keys.Threshold)[i]);

                    var currentThreshold = Number.MAX_VALUE;
                    for (var i = currentAccountType.get(constants.accountType.keys.Threshold).length - 1; i >= 0; --i)
                        currentThreshold = Math.min(currentThreshold, currentAccountType.get(constants.accountType.keys.Threshold)[i]);


                    var renewed = false;

                    var end;
                    var expiration = user.get(constants.user.keys.AccountTypeExpiration);

                    if (expiration == null)
                        expiration = now;
                    var duration;
                    if (requestFor == constants.accountType.RequestFor.admin) {
                        duration = request.params.data.duration;
                        durationModified = true;
                    } else
                        duration = destinationAccountType.get(constants.accountType.keys.Duration);
                    if (couponValid && couponCustomDuration) {
                        duration = couponCustomDuration;
                        durationModified = true;
                    }




                    switch (requestFor) {
                        case constants.accountType.RequestFor.Trial:
                            if (currentThreshold >= destinationThreshold) {
                                l.log(false, "Can't do free trial from currentAccountType:", currentAccountType, "destinationAccountType:", destinationAccountType).despatch();
                                return {
                                    success: false,
                                    message: "No free trial granted: active account type is not \"free trialable\" for user with objectId \"" + user.id + "\"",
                                    error_code: constants.accountType.ErrorCode.CurrentAccountTypeSuperior
                                };
                            }

                            user.set(constants.user.keys.HasDoneFreeTrial, true);
                        default:
                            if (destinationThreshold < currentThreshold) {
                                l.log(false, "Attempted to downgrade? currentAccountType:", currentAccountType, "destinationAccountType:", destinationAccountType).despatch();
                                return {
                                    success: false,
                                    message: requestFor == constants.accountType.RequestFor.stripe
                                        ? "Your current account type is superior. No payment has been made" // TODO: User facing error
                                        : "Account Upgrade denied because current account type is superior",
                                    error_code: constants.accountType.ErrorCode.CurrentAccountTypeSuperior
                                };
                            } else if (currentAccountType.id == destinationAccountType.id) {
                                renewed = true;

                                // If the accountTypes are the same then extend the duration
                                if (duration != null)
                                    end = new Date(expiration * 1 + constants.hour * 24 * duration);
                            } else {
                                // Otherwise activate the currentAccountType now
                                if (duration != null) { // If there's no duration, it should be indefinite
                                    if (currentThreshold == constants.accountType.accountThreshold.Trial
                                        && destinationThreshold == constants.accountType.accountThreshold.Premium) {
                                        // If you're currently on trial and upgrading to premium then add the remaining duration to the subscription
                                        end = new Date(expiration * 1 + constants.hour * 24 * duration);
                                    } else
                                        end = new Date(now * 1 + constants.hour * 24 * duration);
                                }


                                user.set(constants.user.keys.AccountTypeStart, now);
                                user.set(constants.user.keys.activeAccountType, destinationAccountType);
                            }

                            // return {success: true, message: "stop here "}

                            break;
                    }


                    if (end != null)
                        user.set(constants.user.keys.AccountTypeExpiration, end);
                    else
                        user.set(constants.user.keys.AccountTypeExpiration, null);


                    var expirationNotifications = currentAccountType.get(constants.accountType.keys.nearExpirationNotifications);
                    if (expirationNotifications != null) {
                        var keys = Object.keys(expirationNotifications);

                        for (var keys_i = keys.length - 1; keys_i >= 0; --keys_i)
                            user.set(constants.user.keys.ExpirationInPrefix + keys[keys_i], null);
                    }


                    var accountTypeLog = new Parse.Object(constants.accountTypeLog.keys.className);
                    accountTypeLog.set(constants.accountTypeLog.keys.user, user);
                    accountTypeLog.set(constants.accountTypeLog.keys.DestinationAccountType, destinationAccountType);
                    accountTypeLog.set(constants.accountTypeLog.keys.consistentCreatedAt, now);



                    if (transactionId) {
                        accountTypeLog.set(constants.accountTypeLog.keys.transactionId, transactionId);
                    }
                    if (durationModified) {
                        accountTypeLog.set(constants.accountTypeLog.keys.customDuration, duration);
                    }
                    if (requestFor == constants.accountType.RequestFor.admin) {
                        accountTypeLog.set(constants.accountTypeLog.keys.customDuration, duration || 0);
                        accountTypeLog.set(constants.accountTypeLog.keys.note, "admin");
                    }


                    //add coupon log
                    var couponLog;
                    if (couponObj && couponValid) {
                        couponObj.increment("totalNumOfUses");
                        couponLog = new Parse.Object("CouponLog")
                            .set("user", request.user)
                            .set("coupon", couponObj);
                    }

                    var finalPromise = Parse.Promise.as();
                    var price
                    if (requestFor == constants.accountType.RequestFor.stripe && transactionId) {
                        function makeStripeCharge() {
                            var metadata = {
                                permissions: _.reduce(destinationAccountType.get(constants.base.keys.permissions), function (total, permission) {
                                    return total += (total != "" ? "," : "") + permission.get(constants.permissions.keys.slug);
                                }, ""),
                                destination: destinationAccountTypeSlug,
                                duration: duration,
                                user: user.id,
                                country: request.params.data.country
                            };

                            //[Guichi] we only support US dollar now
                            var country = "base";
                            var description = constants.stripePremiumDescription;
                            // if (country == "AU") description += " incl GST";

                            price = constants.stripePremiumPrice[country].amount;
                            var currency = constants.stripePremiumPrice[country].currency;

                            switch (destinationAccountTypeSlug) {
                                case "plus-annual":
                                    description = constants.stripePremiumDescription;
                                    price = constants.stripePlusPrice[country].amount;
                                    currency = constants.stripePlusPrice[country].currency;
                                    break;
                                case "premium-annual":
                                    description = constants.stripePremiumDescription;
                                    price = constants.stripePremiumPrice[country].amount;
                                    currency = constants.stripePremiumPrice[country].currency;
                                    break;

                            }


                            if (couponObj && couponValid) {
                                description += " (" + stripeCoupon + ")";
                                metadata.coupon = stripeCoupon;

                                var couponCountry = 'base';
                                if (stripeDiscount) {
                                    // var couponCountry = request.params.data.country in stripeDiscount ?
                                    //     request.params.data.country : "base";
                                    var discount = stripeDiscount[couponCountry];
                                    if (discount < 1) {
                                        price = parseInt(price * (1 - discount));
                                    }

                                    else
                                        price = parseInt(discount * 100);
                                }
                                if (stripePrice) {
                                    // couponCountry = request.params.data.country in stripePrice ?
                                    //     request.params.data.country : "base";
                                    price = parseInt(stripePrice[couponCountry].amount * 100);
                                    currency = stripePrice[couponCountry].currency;
                                }
                            }


                            return stripe.Charges.create({
                                amount: price,
                                currency: currency,
                                card: request.params.data.token.id,
                                description: description,
                                metadata: metadata,
                                receipt_email: user.get("email")
                            }).then(function (successfulCharge) {
                                charge = successfulCharge;
                                accountTypeLog.set(constants.accountTypeLog.keys.note, "stripe charge: " + charge.id);
                                accountTypeLog.set("metadata", metadata);

                                payment = {
                                    amount: price,
                                    currency: currency
                                };

                                return Parse.Promise.as();
                            }, function (error) {
                                l.log("ALERT: Charge error: " + error.message);
                                return error;
                            });
                        }

                        finalPromise = stripe.Tokens.retrieve(request.params.data.token.id)
                            .then(function (token) {
                                if (token.used) {
                                    // TODO: Make sure the charge was for the correct purpose
                                    // Payment already made
                                } else
                                    return makeStripeCharge();
                            }, function (error) {
                                if (error.name == "invalid_request_error")
                                    return Parse.Promise.as(makeStripeCharge());
                                else
                                    return error;
                            });
                    }


                    console.log("request.params request.params")
                    console.log(JSON.stringify(request.params))
                    console.log("couponCustomDurationcouponCustomDuration")
                    console.log(couponCustomDuration)

                    //step 4 save coupon object and couponLog object
                    return finalPromise.then(function (message) {
                        //message only present when error arise
                        if (requestFor == constants.accountType.RequestFor.stripe && message != null)
                            return message;

                        var objsToSave = [user, accountTypeLog];
                        if (couponLog) {
                            objsToSave.push(couponObj);
                            objsToSave.push(couponLog);
                        }
                        //step 5 send email and mixpanel event etc
                        return Parse.Object.saveAll(objsToSave,{useMasterKey:true}).then(function () {
                            var template;

                            if (request.params.template)
                                template = request.params.template;
                            else if (renewed)
                                template = destinationAccountType.get(constants.accountType.keys.renewedEmailTemplate);
                            else
                                template = destinationAccountType.get(constants.accountType.keys.startedEmailTemplate);




                            if (template != null) {
                                var customMergeVars;

                                if(couponCustomDuration){ //for free trail
                                    customMergeVars = {
                                        "BLRT_CUSTOMDURATIONDAY": couponCustomDuration
                                    };
                                }
                                if (durationModified && duration) { //for admin
                                    customMergeVars = {
                                        "BLRT_ISPIONEERINVITEE": invitedByPioneer,
                                        "BLRT_CUSTOMDURATIONDAY": duration
                                    };
                                }else {
                                    customMergeVars = {
                                        "BLRT_ISPIONEERINVITEE": invitedByPioneer,
                                    };
                                }
                                extraPromises.push(Email.SendTemplateEmail({
                                    template: template,
                                    convo: null,
                                    content: null,
                                    recipients: [{
                                        name: user.get(constants.user.keys.FullName),
                                        email: user.get(constants.user.keys.Email),
                                        isUser: true,
                                        user: user
                                    }],
                                    sender: null,
                                    creator: null,
                                    customMergeVars: customMergeVars
                                }));
                            }

                            return Parse.Promise.when(extraPromises).then(function () {
                                l.log(true).despatch();
                                return {success: true, revenue: price};
                            }, function (error) {
                                l.log(true, "Upgrade successful, but failed emails:", error).despatch();
                                return Parse.Promise.as({
                                    success: true,
                                    message: "Successfully upgraded, but failed to send emails: " + JSON.stringify(error)
                                });
                            });
                        }, function (error) {
                            l.log(false, "user/accountTypeLog.saveAll() error:", error).despatch();
                            return Parse.Promise.error("user/accountTypeLog.saveAll() error (user \"" + user.id + "\"): " + JSON.stringify(error));
                        });
                    }, function (error) {
                        l.log(false, "finalPromise error:", error).despatch();
                        return Parse.Promise.error("finalPromise error (user \"" + user.id + "\"): " + JSON.stringify(error));
                    });
                }, function (error) {
                    l.log(false, "accountTypeQuery.find() error:", error).despatch();
                    return Parse.Promise.error("accountTypeQuery error (user \"" + user.id + "\"): " + JSON.stringify(error));
                });
            }, function (error) {
                l.log(false, "getSettings error:", error).despatch();
                return Parse.Promise.error("GetSettings error (user \"" + user.id + "\"): " + JSON.stringify(error));
            });
        }, function (error) {
            console.log(error)
            l.log(false, "waitables error:", error).despatch();
            return Parse.Promise.error("waitables error (user \"" + (user == null ? "<null>" : user.id) + "\"): " + JSON.stringify(error));
        });
    });
}


/*
 response = {
 valid: false,
 code: 1
 }

 code 1 = general
 code 2 = maxNumOfUsesPerUser
 */
function IsValidCouponForUser(couponObj, user, option) {
    console.log(arguments)
    return Parse.Promise.as()
        .then(function () {
            if (!couponObj)
                return {valid: false, code: 1};
            var now = new Date();
            if (couponObj.get("expiryAt") && couponObj.get("expiryAt") < now) {
                return {valid: false, code: 1};
            }
            if (option&&option.shouldBeFreeUpdate && couponObj.get('discount')&& couponObj.get('discount').base !== 1) {
                return {valid: false, code: 4};
            }

            //todo [Guichi] never in use
            // var maxNumOfUsesPerCoupon = couponObj.get("maxNumOfUsesPerCoupon");
            // var totalNumOfUses = couponObj.get("totalNumOfUses");
            // if (maxNumOfUsesPerCoupon) {
            //     if (!totalNumOfUses) {
            //         totalNumOfUses = 0;
            //     }
            //     if (totalNumOfUses + 1 > maxNumOfUsesPerCoupon) {
            //         return {valid: false, code: 1};
            //     }
            // }

            //maxNumOfUsesPerUser
            var maxNumOfUsesPerUser = couponObj.get("maxNumOfUsesPerUser");
            if (!maxNumOfUsesPerUser)
                return {valid: true};
            if (typeof user == "undefined" || user == null)
                return {valid: true};
            if (!user)
                return {valid: false, code: 1};
            return new Parse.Query("CouponLog")
                .equalTo("coupon", couponObj)
                .equalTo("user", user)
                .find()
                .then(function (result) {
                    var count = 0;
                    if (result) {
                        count = result.length;
                    }
                    if (count + 1 > maxNumOfUsesPerUser)
                        return {valid: false, code: 2};
                    return {valid: true};
                }, function (error) {
                    return {valid: false, code: 1};
                });
        });
}

//we need to check coupon before performing upgrading .However such checking are not so acurrate though
//We do not know if the coupon should be free or not
Parse.Cloud.define("IsValidCouponForUser", function (req, res) {
    var params = req.params;
    var user = req.user;
    var coupon = params.coupon;
    var destination = params.destination;
    var couponObj;
    var discount;
    new Parse.Query('CouponCode')
        .equalTo('code', coupon)
        .equalTo('targetAccountSlug', destination)
        .first({useMasterKey: true})
        .then(function (Obj) {
            couponObj=Obj
            return IsValidCouponForUser(couponObj, user)
        })
        .then(function (rst) {
            //we need extra info to provide to the frontend
            if(rst.valid&&couponObj.get("discount"))
            {
                rst.discount=couponObj.get("discount").base;
                console.log(typeof rst.discount)
                if(rst.discount==1)
                {
                    rst.duration=couponObj.get("customDuration");
                }
            }
            res.success(rst)
        })

        .fail(function (e) {
            console.log(e)
            res.error(e)
        })


})

exports.IsValidCouponForUser = IsValidCouponForUser;
















