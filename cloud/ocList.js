var _ = require("underscore");

const Errors = {
	InvalidParameters: {code: 400, message: "Invalid parameters"},
	NotLoggedIn: {code: 401, message: "Not logged in"},
	OCListNotFound: {code: 404, message: "OCList not found"},
	AlreadySubscribed: {code: 460, message: "Already subscribed this OCList"},
	InternalError: {code: 500, message: "Server error"}
}

const RelationClassName = "UserOrganisationRelation";
const ListClassName = "OCList";

Parse.Cloud.define('subscribeOCList', function(req, res) {
	var ocListId = _.decryptId(req.params.listId);
	if(!_.isString(ocListId)) {
		return res.success(JSON.stringify({ success: false, error: Errors.InvalidParameters }));
	}

	var user = req.user;
	
	if (!user){
		return res.success(JSON.stringify({ success: false, error: Errors.NotLoggedIn }));
	}

	var RelationClass = Parse.Object.extend(RelationClassName);
	var ListClass = Parse.Object.extend(ListClassName);
	
	var relationQuery = new Parse.Query(RelationClassName)
		.equalTo("user", req.user)
		.first();
	var listQuery = new Parse.Query(ListClassName)
		.equalTo("objectId", ocListId)
		.first();

	Parse.Promise.when(relationQuery, listQuery).then(function(relationObj, listObj) {
		if (!listObj) {
			return res.success(JSON.stringify({ success: false, error: Errors.OCListNotFound }));
		}

		var subscribedLists = null;
		if (relationObj) {
			subscribedLists = relationObj.get("oclists");
			var subscribed = _.find(subscribedLists, function(list) {return list.id == ocListId});
			if (subscribed) {
				return res.success(JSON.stringify({ success: false, error: Errors.AlreadySubscribed }));
			}
			subscribedLists.push(listObj);
		} else {
			relationObj = new RelationClass();
			relationObj.set("user", req.user)
				.setACL(new Parse.ACL(user));
			subscribedLists = [listObj];
		}
		relationObj.set("dbversion", 0);
		relationObj.set("oclists", subscribedLists);
		relationObj.save()
			.then(function(obj) {
				return res.success(JSON.stringify({ success: true, message: "subscribed" }));
			}, function(error) {
				return res.success(JSON.stringify({ success: false, error: Errors.InternalError }));
			});
	}, function(error) {
		return res.success(JSON.stringify({ success: false, error: Errors.InternalError }));
	});
});

Parse.Cloud.define('unsubscribeOCList', function(req, res) {
	var ocListId = _.decryptId(req.params.listId);
	if(!_.isString(ocListId)) {
		return res.success(JSON.stringify({ success: false, error: Errors.InvalidParameters }));
	}

	var user = req.user;
	
	if (!user){
		return res.success(JSON.stringify({ success: false, error: Errors.NotLoggedIn }));
	}

	var RelationClass = Parse.Object.extend(RelationClassName);
	var ListClass = Parse.Object.extend(ListClassName);
	
	new Parse.Query(RelationClassName)
		.equalTo("user", req.user)
		.first()
		.then(function(relationObj) {
			if (relationObj) {
				var subscribedLists = relationObj.get("oclists");
				var listFound = false;
				var i = 0;
				while (i < subscribedLists.length) {
					if (ocListId == subscribedLists[i].id) {
						listFound = true;
						subscribedLists.splice(i, 1);
					} else {
						++i;
					}
				}
				if (listFound) {
					relationObj.set("dbversion", 0);
					relationObj.set("oclists", subscribedLists);
					relationObj.save()
						.then(function(obj) {
							return res.success(JSON.stringify({ success: true, message: "subscribed" }));
						}, function(error) {
							return res.success(JSON.stringify({ success: false, error: Errors.InternalError }));
						});
					} else {
						return res.success(JSON.stringify({ success: false, error: Errors.OCListNotFound }));
					}
			} else {
				return res.success(JSON.stringify({ success: false, error: Errors.OCListNotFound }));
			}
		}, function(error) {
			return res.success(JSON.stringify({ success: false, error: Errors.InternalError }));
		});
});
