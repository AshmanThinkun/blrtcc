var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var Buffer = require('buffer').Buffer;
var crypto = require('crypto');
var moment = require('moment');
var _ = require("underscore");

Parse.Cloud.define('getWebplayerPlayData', function(req, res) {
	return exports.getPlayData(req.params.blrtId, req.params.token, null, util.reqToCloudFuncOptions(req)).then(function (playData) {
		return res.success(JSON.stringify({ success: true, playData: playData }));
	}, function (error) {
		return res.success(JSON.stringify({ success: false, message: "Failed to get play data." }));
	});
});

Parse.Cloud.define('setWebplayerBlrtViewed', function(req, res) {
	return markBlrtViewed(req.params.blrtId, req).then(function () {
		return res.success(JSON.stringify({ success: true}));
	}, function (error) {
		return res.success(JSON.stringify({ success: false, message: JSON.stringify(error) }));
	});
});

Parse.Cloud.define('getWebplayerCanAccess', function(req, res) {
	return getCanAccess(req.params.blrtId, util.reqToCloudFuncOptions(req)).then(function (canAccessInfo) {
		var result = {success: true, canAccess: canAccessInfo.canAccess};
		if (!(result.canAccess)) {
			result.convoId = canAccessInfo.convoId;
		}
		return res.success(JSON.stringify(result));
	}, function (error) {
		return res.success(JSON.stringify({ success: false, message: JSON.stringify(error) }));
	});
});

exports.getPlayData = function(blrtId, token, conversationItemFromUrlResponse, sessionOptions) {
	if (!blrtId || typeof blrtId != 'string') {
		return Parse.Promise.error("invalid blrt Id");
	}
	var responsePromise = null;
	if (conversationItemFromUrlResponse) {
		responsePromise = Parse.Promise.as(conversationItemFromUrlResponse);
	} else {
		var params = {};
		var url = constants.siteAddress + "/blrt/" + blrtId;
		// var url = "d.blrt.co/blrt/" + blrtId; // for debug locally
		if (("string" == typeof token) && (constants.accessToken.tokenLength == token.length)) {
			url += "?tk=" + token
		}
		params.data = {
			url: url,
			includeMedia: true
		};

		params.api = {version: 1, endpoint: "conversationItemFromUrl"};
		params.device = {
	                version: "1.0",
	                device: "www",  
	                os: "www",      
	                osVersion: "1.0"
	            };
	    responsePromise = Parse.Cloud.run("conversationItemFromUrl", params, sessionOptions).then(function(response) {
		    try {
	    		response = JSON.parse(response);
	    	} catch(e) {
	    		return Parse.Promise.error(e);
	    	}
	    	if (response.error && response.error.code == 620) {
	    		return Parse.Promise.error(response.error);
	    	}
	    	return response;
	    });
	}
	return responsePromise.then(function(response) {
		// encrypt page embedded data for webplayer
	  	var IV = util.generateRandomNumber();
	  	var outputObj = {};
	  	if(response.error) {
	  		// user cannot view this blrt
	  		outputObj = {error: response.error};
	  		if (response.error.code == 621) {
	  			outputObj.convoId = response.data.conversationId;
	  		}
	  	} else {
	  		outputObj = response.data.playData;
	  	}
	    outputObj.isPublic = response.data.isPublicBlrt;
	    outputObj.blrtId = blrtId;
	    var t = JSON.stringify(outputObj);
	    var outputData = new Buffer(t);
	    var cipher = crypto.createCipheriv('aes-256-cbc', constants.PageEmbededDataSecretKey, IV);
	    var encryptedData = Buffer.concat([cipher.update(outputData), cipher.final()]);
	    return { 
	    	v: 1, 
	    	k: IV.toString('base64'), // here we call the IV 'k' to obfuscate
	    	id: blrtId, 
	    	d:encryptedData.toString('base64'),
	    	title: response.data.title,
	    	creator: response.data.creator,
	    	date: moment(response.data.createdAt).format('D MMM YYYY'),
	    	duration: response.data.duration,
	    	thumbnail: util.httpsParseFileUrl(response.data.thumbnail),
	    	isPublic: response.data.isPublicBlrt
	    };
	});
}

function addId(idListStr, idStr) {
	if (('string' != typeof idListStr) && ('undefined' != typeof idListStr) && (null != idListStr)) {
		throw "invalid idListStr";
	}
	var id = Number(idStr);
	if (NaN == id) {
		throw "invalid idStr";
	}
	if (id < 0) {
		return null;
	}
	var idArray = [];
	if ('string' == typeof idListStr) {
		idArray = _.stringToList(idListStr);
	}
	if (idArray) {
		var index = idArray.indexOf(id);
		if (0 <= index && index < idArray.length) {
			return null;
		} else {
			idArray.push(id);
			return _.listToString(idArray);
		}
	} else {
		return null;
	}
}

function markBlrtViewed(blrtId, req) {
	// validate blrt id
	if (!blrtId || typeof blrtId != 'string') {
		return Parse.Promise.error("invalid blrt Id");
	}
	// there has to be a user logged in
	if (!(req && req.user)) {
		return Parse.Promise.error("no user specified");
	}
	sessionOptions = util.reqToCloudFuncOptions(req);
	blrtId = _.decryptId(blrtId)
	return new Parse.Query("ConversationItem")
		.equalTo("objectId", blrtId)
		.first()
		.then(function(blrtItem) {
			if (!blrtItem) {
				return true;
			}
			return new Parse.Query("ConversationUserRelation")
				.equalTo("conversation",blrtItem.get("conversation"))
				.equalTo("user", req.user)
				.first(sessionOptions)
				.then(function(userRelationItem) {
					if (!userRelationItem) {
						return true;
					}
					var newIdList = addId(userRelationItem.get("indexViewedList"), blrtItem.get("readableIndex"));
					if (newIdList) {
						userRelationItem.set("indexViewedList", newIdList);
						return userRelationItem.save(sessionOptions);
					} else {
						return true;
					}
				});
		});
}

function getCanAccess(blrtId, sessionOptions) {
	// validate blrt id
	if (!blrtId || typeof blrtId != 'string') {
		return Parse.Promise.error("invalid blrt Id");
	}
	var params = {};
	params.data = {
		url: constants.siteAddress + "/blrt/" + blrtId,
		includeMedia: true
	};
	params.api = {version: 1, endpoint: "conversationItemFromUrl"};
	params.device = {
        version: "1.0",
        device: "www",
        os: "www",
        osVersion: "1.0"
    };
    return Parse.Cloud.run("conversationItemFromUrl", params, sessionOptions).then(function(response) {
	    try {
    		response = JSON.parse(response);
    	} catch(e) {
    		return Parse.Promise.error(e);
    	}
    	if (response.error) {
    		switch (response.error.code) {
    			case 621:
    			case 622:
    				return {canAccess: false, convoId: response.data.conversationId};
    			default:
    				return Parse.Promise.error(response.error);
    		}
    	}
    	return {canAccess: true, convoId: null};
    });
}

