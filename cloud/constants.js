var Buffer = require('buffer').Buffer;

exports.skipLimit = 10000;
exports.maxQueryLimit = 950;
exports.cloudVersion = "1.4.7";


exports.hour = 1000 * 60 * 60;


exports.blrtWebsiteUrl                      = "https://www.blrt.com";
exports.appStoreUrl                         = "//itunes.apple.com/au/app/id874881426?mt=8";
exports.googleStoreUrl                      = "//play.google.com/store/apps/details?id=com.blrt.android";
exports.appStoreId                          = "874881426";

exports.androidPackageId                    = "com.blrt.android";

exports.pioneerSalt                         = "DFG8OY34HFA9GW54SDFG";
exports.bitlyToken                         = "12a827d1c906261e263a8b0d2bf9a0d5c7a96080";


exports.os = {
    iOS:                                    "iPhone OS",
    Android:                                "Android"
};

exports.AESKey1 = new Buffer([0xEB, 0xBF, 0x34, 0xE9, 0x45, 0xDF, 0xD0, 0x60, 0x8B, 0xA6, 0xDF, 0x09, 0xFF, 0xF4, 0xA6, 0xA5, 0xC0, 0x19, 0x20, 0xC7, 0x7A, 0x5E, 0x6A, 0xBC, 0xA5, 0xB6, 0x0E, 0xC6, 0xEB, 0xB4, 0xA3, 0x28]);
exports.PageEmbededDataSecretKey = new Buffer([0x3a, 0xe1, 0xb2, 0x93, 0x3c, 0x9f, 0x54, 0x79, 0x67, 0xc7, 0xb5, 0x76, 0x57, 0xde, 0xf3, 0xfa, 0x44, 0x2d, 0x75, 0x0b, 0xf9, 0xbd, 0x20, 0xba, 0x59, 0x2d, 0xb0, 0x79, 0x9e, 0x8f, 0x92, 0x1e]);


exports.base = {
    keys: {
        version:                            "dbversion",
        versionCreated:                     "dbversionCreated",

        appVersion:                         "appVersion",
        device:                             "device",
        os:                                 "os",
        osVersion:                          "osVersion",
        localGuid:                          "localGuid",

        encryptType:                        "encryptType",
        encryptValue:                       "encryptValue",

        creator:                            "creator",

        DeletedAt:                          "deletedAt",
        IsNew:                              "IsNew",
        permissions:                        "permissionsIds",
        convoCreatorPermissions:            "convoOwnerPermissionsIds",

        //PARSE
        objectId:                           "objectId",
        createdAt:                          "createdAt",
        UpdatedAt:                          "updatedAt"
    }
};



exports.settings = {
    keys: {
        className:                          "Settings",

        Key:                                "key",
        Value:                              "value",

        DefaultAccountTypeSlug:             "default_account_type_slug",
        FreeTrialAccountTypeSlug:           "free_trial_account_type_slug",
        FreeTrialDuration:                  "free_trial_duration",

        // not actually in the database
        iOSIAPPrefix:                       "iosiap",
        AndroidIAPPrefix:                   "androidiap"
    },
    appStoreUrl:                            "app_store_url"
}

exports.permissions = {
    keys: {
        className:                          "Permissions",

        slug:                               "slug",
        stackLevel:                         "stackLevel",

        // not actually in the database
        accountThreshold:                   "account_threshold",
        accountName:                        "account_name",
        permissions:                        "permissions",

        // For records
        values:                             "values"
    },
    values: {
        maxBlrtDuration:                    "max_blrt_duration",
        maxImagePageCount:                  "max_image_page_count",
        maxImageResolution:                 "max_image_resolution",
        maxConvoUsers:                      "max_conversation_users",
        maxPDFSizeKB:                       "max_pdf_size_kb",
        maxPDFPageCount:                    "max_pdf_page_count",
        maxPDFFileCount:                    "max_pdf_file_count",
        maxFileCount:                       "max_file_count",
        maxSecondaryEmails:                 "max_secondary_emails",
        canUseWebViewer:                    "can_use_web_viewer",
        canUseDesktopViewer:                "can_use_desktop_viewer",

        canMediaReorder:                    "can_media_reorder",
        canMediaAddOnReblrt:                "can_media_add_on_reblrt",
        canMediaShowhide:                   "can_media_showhide",
        canCreateConversation:              "can_create_conversation",
        canCreateComment:                   "can_create_comment",
        canCreateReblrt:                    "can_create_reblrt",
        canAddUsers:                        "can_add_users",

        // new blrt size limit
        maxConvoSizeKBkey:                  "max_convo_size_kb",
        maxBlrtPageCount:                   "max_blrt_page_count",
        maxBlrtSizeKB:                      "max_blrt_size_kb",

        // not actually in the database
        AccountThreshold:                   "account_threshold",
        AccountName:                        "account_name",

        ContentViewDays:                    "content_view_days"

    },
    stack:                                  "stack"
}

// not actually in the database (needed by the settings manager)
exports.iap = {
    AvailableIAPs:                      "available_iaps",
    SpecialCases:                       "special_cases"
}




exports.convo = {
    version:                                6,
    keys: {
        className:                          "Conversation",

        SpecialSlug:                        "specialSlug",
        Name:                               "blrtName",
        ThumbnailFile:                      "thumbnailFile",
        CreationType:                       "creationType",
        StatusCode:                         "statusCode",
        ContentUpdate:                      "contentUpdate",

        generalCount:                       "contentCount",
        readableCount:                      "readableCount",
        BondCount:                          "bondCount",
        commentCount:                       "commentCount",
        blrtCount:                          "blrtCount",

        incomplete:                         "incomplete",

        partialItemCreationCount:           "partialItemCreationCount",

        convoDirty:                         "conversationDirty",
        reloadNumber:                       "reloadNumber",

        AwaitingRequest:                    "awaitingRequest",
        LastMedia:                          "lastMedia",
        LastMediaArgument:                  "lastMediaArgument",

        HasConversationBegan:               "hasConversationBegan",

        //cloud only        
        Role:                               "conversationRole",
    },
    creationTypes: {
        normal:                             0,
        duplicate:                          10,
        template:                           100
    },
    statusCodes: {
        normal:                             100
    },
    nameCharLimit:                          56
};

exports.convoItem = {
    version:                                6,
    keys: {
        className:                          "ConversationItem",

        conversation:                       "conversation",

        type:                               "type",


        generalIndex:                       "index",
        readableIndex:                      "readableIndex",
        Argument:                           "argument",
        Media:                              "media",

        TouchDataFile:                      "touchDataFile",
        AudioFile:                          "audioFile",
        thumbnail:                          "thumbnailFile",

        psuedoId:                           "psuedoId",

        PublicName:                         "publicName",

        // CC ONLY
        isSpecial:                          true
    },
    encryptTypes: {
        none:                               0
    },
    types: {
        blrt:                               0,
        request:                            1,// not in use any more
        reBlrt:                             2,
        comment:                            3,
        eventAdd:                           4,
        eventMessage:                       5,
        eventLeave:                         6,//not in use ,because we do not want to add new event for some reason
        audio:                              7,
        images:                             8,
    },
    readableTypes:                          [0, 1, 2, 3,7,8],

    maxPrivateViewCounter:                  3,

    psuedoIdSeparator:                      "-",

    boldPrefix:                             "@#$",

    blackPrefix:                            "*%^B"
};

exports.convoUserRelation = {
    version:                                6,
    keys: {
        className:                          "ConversationUserRelation",

        user:                               "user",
        convoCreator:                       "convoCreator",
        conversation:                       "conversation",

        ContentUpdate:                      "contentUpdate",
        
        QueryValue:                         "queryValue",
        QueryType:                          "queryType",

        QueryName:                          "queryName",

        allowInInbox:                       "allowInInbox",

        Tags:                               "tags",
        indexViewedList:                    "indexViewedList",
        lastViewed:                         "lastViewed",
        lastEdited:                         "lastEdited",

        Archived:                           "archived",

        HasComment:                         "hasComment",
        HasMadeReBlrt:                      "hasMadeReBlrt",
        HasMadeRequest:                     "hasMadeRequest",
        HasInvitedPeople:                   "hasInvitedPeople",


        flaggedForCommentEmailAt:           "flaggedForCommentEmailAt"
    }
};

exports.mediaAsset = {
    version:                                5,
    keys: {
        className:                          "MediaAsset",

        mediaFile:                          "mediaFile",
        mediaFormat:                        "mediaFormat",
        name:                               "name",
        pageArgs:                           "pageArgs"
    }
}

exports.contactTable = {
    keys: {
        className:                          "UserContactDetails",

        user:                               "user",

        Value:                              "value",
        type:                               "type",
        verified:                           "verified",
        dirty:                              "dirty",
        placeholder:                        "placeholder",


        refuseUserSave:                     "refuseUserSave",
        refuseUserSaveCheck:                "refuseUserSaveCheck",


        VerificationKey:                    "verificationKey"
    },
    types: {
        email:                              "email",
        facebook:                           "facebook",
        phoneNumber:                        "phoneNumber",
    }
};

exports.user = {
    keys: {
        FullName:                           "name",
        firstName:                          "first_name",
        lastName:                           "last_name",
        gender:                             "gender",
        LastActive:                         "lastActive",

        username:                           "username",
        password:                           "password",

        Email:                              "email",
        EmailVerified:                      "emailVerified",
        EmailVerified2:                     "emailVerified2",

        hasSetPassword:                     "hasSetPassword",

        AllowEmailComments:                 "allowEmailComments",
        AllowEmailNewBlrt:                  "allowEmailNewBlrt",
        AllowEmailReBlrt:                   "allowEmailReBlrt",
        AllowEmailRequest:                  "allowEmailRequest",

        langArray:                          "deviceLangArray",
        locale:                             "deviceLocal",

        RelationsDirty:                     "relationsDirty",

        CreatedFlag:                        "createdFlag",

        SettingsLastUpdated:                "settingsLastUpdated",

        MandrillWhitelistFlag:              "mandrillWhitelistFlag",

        activeAccountType:                  "activeAccountTypeId",
        AccountTypeStart:                   "accountTypeStartDate",
        AccountTypeExpiration:              "accountTypeExpirationDate",
        ExpiredAction:                      "expiredAction",
        ExpirationInPrefix:                 "expirationIn",
        HasDoneFreeTrial:                   "hasDoneFreeTrial",

        authData:                           "authData",

        ContactInfoDirty:                   "relationsDirty",

        isAdminUser:                        "isAdminUser",

        conversationCreationCounter:        "conversationCreationCounter",
        commentCreationCounter:             "commentCreationCounter",
        blrtCreationCounter:                "blrtCreationCounter",
        requestCreationCounter:             "requestCreationCounter",
        convoInvitationCounter:             "convoInvitationCounter",

        queuedConvoTemplates:               "queuedConvoTemplates",

        pendingQueue:                       "pendingQueue",
        pendingQueueCount:                  "pendingQueueCount"
    },
    pendingQueueActions: {
        processContactDetails:              "processContactDetails",
        sendVerificationEmail:              "sendVerificationEmail"
    },
    introTemplate:                          "welcome-jan15"
};

exports.accountType = {
    keys: {
        className:                          "AccountType",

        Slug:                               "slug",
        Threshold:                          "threshold",
        Duration:                           "dayDuration",
        MarketingName:                      "marketingName",

        nearExpirationNotifications:        "nearExpirationNotifications",
        startedEmailTemplate:               "startedEmailTemplate",
        renewedEmailTemplate:               "renewedEmailTemplate",
        expiredEmailTemplate:               "expiredEmailTemplate",
        expiredPush:                        "expiredPush",
        expiredAction:                      "expiredAction",

        NotificationParam: {
            Email:                          "email",
            Push:                           "push",
            Action:                         "action"
        }
    },

    RequestFor: {
        Trial:                              "trial",
        stripe:                             "stripe",
        iOSIAP:                             "iosiap",
        AndroidIAP:                         "androidiap",
        admin:                              "admin"
    },
    accountThreshold: {
        Free:                               0,
        Trial:                              500,
        Premium:                            1500
    },
    ErrorCode: {
        Unknown:                            0,

        ServerConnectionError:              1,
        PaymentCancelled:                   2,
        recentDuplicateRequest:             3,
        duplicateRequest:                   4,

        TrialAlreadyUsed:                   101,
        CurrentAccountTypeSuperior:         102,

        iOSAppStoreConnectionFailed:        201,
        iOSPaymentFailed:                   202,
        iOSPayerRejected:                   203
    }
}

exports.accountTypeLog = {
    keys: {
        className:                          "AccountTypeLog",

        user:                               "userId",
        DestinationAccountType:             "destinationAccountTypeId",
        consistentCreatedAt:                "consistentCreatedAt",
        Parsed:                             "parsed",

        transactionId:                      "transactionId",

        customDuration:                     "customDuration",
        note:                               "note"
    }
}

exports.accountTypeTrail = {
    keys: {
        className:                          "AccountTypeTrail",

        user:                               "userId",
        AccountType:                        "accountTypeId",
        Start:                              "startDate",
        Expire:                             "expireDate"
    }
}

exports.accessToken = {
    keys: {
        className:                          "BlrtAccessToken",

        token:                              "token",
        conversation:                       "conversation",
        conversationUserRelation:           "conversationUserRelation",
        info:                               "info",
        accessCountdown:                    "accessCountdown",
        isEnabled:                          "isEnabled",
        group:                              "group"
    },
    tokenLength:                            6,
    queryStringKey:                         "tk"
}


exports.mandrillTemplates = {
    userNewConvo:                           "user-send",
    nonUserNewConvo:                        "non-user-send",
    userExistingConvo:                      "user-add-conversation",

    blrtReply:                              "user-update",
    AggregateComment:                       "user-update-comment",

    pioneerApplication:                     "pioneer-application",
    pioneerInviteeSignupAdmin:              "pioneer-invitee-signup-admin-notification",
    pioneerInviteeSignup:                   "pioneer-invitee-signup",
    pioneerApproved:                        "pioneer-approved",
    pioneerUpgradeRequest:                  "pioneer-upgrade-request",
    pioneerInviteeComplete:                 "pioneer-invitee-complete",
    pioneerUpgradeCompleteAdmin:            "pioneer-upgrade-complete-admin",
    premiumRenewedPioneer5:                 "premium-renewed-pioneer-5",
    premiumRenewedPioneer50:                "premium-renewed-pioneer-50",
};



exports.help = {
    facebookSetPasswordSteps:               "http://help.blrt.com/knowledgebase/articles/392554"
}

exports.isDevelop   = (Parse.applicationId == "btEvYlbdW0pSlDv4Nqfqq80t0TpNXpFFs8ABG4JR"||Parse.applicationId=="blrtLocal"||Parse.applicationId=="blrtDev");
exports.isQA        = (Parse.applicationId == "QIMCw35biDACwVYwJZedvo5MQrspAVBdQlBW5OPX"||Parse.applicationId=="blrtQa");
exports.isStaging   = (Parse.applicationId == "xwcQLBxg0Hb5C2HOtRj4cHFbzS1QUtnWMxz55OzQ"||Parse.applicationId=="blrtStaging");
exports.isLive      = (Parse.applicationId == "Jcau0RObxeLbFEJi3rqSdGy2BbpktyLjOtwjbPJG"||Parse.applicationId=="blrt");
exports.isSpecial   = (Parse.applicationId == "blrtSpe");
exports.isOpensourceServer=Parse.applicationId.indexOf('blrt')>-1;
exports.semDisabled                         = false;
exports.parseEmailVerification              = false;

// These must be consistent with ALL functional versions of the app (meaning when this changes, there must be a force update)
exports.objectIdChars                       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
exports.sharedObjectIdCryptToken            = "gGuskwfEOAJl0zTKvMNpYHPDab513qjdQyCe6iRXrth28oI4c7nFW9UZSBLmxV";

exports.parseApplicationId                  = Parse.applicationId;
exports.serverURL                           = Parse.serverURL
exports.parseJavascriptKey                  = Parse.javaScriptKey;

exports.grabzitApiKey                       = "NzMwMzQ1ZGE4MjRiNGYxMDljMzM2YzdhNmU4NDNiYjQ=";

exports.supportedImageExtensions            = ['.jpg','.jpeg','.png','.gif','.bmp'];

// Change this to http(s) when our SSL certificate is enabled/disabled
exports.protocol                            = "https://";

if(exports.isDevelop || exports.isSpecial) {

    exports.RESTApiKey="5KA3PqRWj8t1rc2PLwelc3DMvY80GaLXmZQIRebn"
    exports.pioneerAdmin = {
        name: "Hao",
        email: "hao@thinkun.net",
    };

    exports.oclistSuperAdmin={
        name: "Guichi",
        email: "dev1@thinkun.net",
    }

    exports.developerAdmin = {
        name: "Bilal",
        email: "bilal@thinkun.net",
        isUser: true
    };
} else if(exports.isQA) {
    exports.RESTApiKey="QuAnOId39EQovD4FscZVihn1jJHvBMl1hRARysJa"
    exports.pioneerAdmin = {
        name: "Christian",
        email: "christian@thinkun.net",
    };
    exports.oclistSuperAdmin={
        name: "Christian",
        email: "christian@thinkun.net",
    }
    exports.developerAdmin = {
        name: "Bilal",
        email: "bilal@thinkun.net",
        isUser: true
    };
} else {
    exports.RESTApiKey="RESF0euA5M6Ru0hxPWiBiLhea9sRGFm6PJeZm79N"
    exports.pioneerAdmin = {
        name: "Anu",
        email: "anu+pioneers@thinkun.net",
    };

    exports.oclistSuperAdmin={
        name: "Anu",
        email: "anu+chaser@thinkun.net",
    }

    exports.developerAdmin = {
        name: "Anu",
        email: "anu+developer@thinkun.net",
        isUser: true
    };
}

const recorderVersionQuery = "?v=2016.07.20.01";

if(exports.isDevelop) {
    exports.salt                            = "W4ffl35  D3str0YZ $LL  B_r1t0  in  T-R3X   F4GHT!11`!1!";

    exports.appProtocol                     = "blrtdev";
    exports.siteAddress                     = process.env.PARSE_DOMAIN||"d.blrt.co";
    // new dev e server on Azure -- 29/08/2018
    exports.embedServer                     = "https://de.blrt.com";

    exports.fbAppId                         = "1417078645178637";
    exports.mandrillKey                     = "cKMAu25QxPorE-z6DLhHSw";
    exports.logglyToken                     = "db2de934-f547-4c6c-9e6b-5be312a5768d";

    exports.stripeSecretKey                 = "sk_test_uPBg1qH2JdSftIHVUyKayNFZ";
    exports.stripePublicKey                 = "pk_test_T3rtrfIh67QmuCHw2Yi6XVRs";


    exports.hour                            /= 24;

    exports.mixpanelToken                   = "04e88384de4be4c336f416d627c0b659";
    exports.mixpanelApiKey                  = "66b86dded051262ddf1e5a1225794d49";
    exports.mixpanelSecret                  = "66b86dded051262ddf1e5a1225794d49";
    exports.stream                          = "d";
    exports.googleAnalyticKey               = "UA-45564248-2";
    exports.webplayerUrl                    = "//s3.amazonaws.com/blrt-webplayer/d/bin/Blrt.nmf";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/d/bin/Blrt_recorder.nmf";
    //websocket notification and email url
    exports.webappSiteAddress               = process.env.WEBAPP_SERVER_URL||"https://dweb.blrt.com";


    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/d/bin/Blrt_recorder.nmf" + recorderVersionQuery;
} else if(exports.isQA) {
    exports.salt                            = "uuu uuu   uuuu u u u u uuu u u u u uuuuuu u uu uu uuu  uuuuuu u uuuuu u u u u uu";

    exports.appProtocol                     = "blrtqa";
    exports.siteAddress                     = "q.blrt.co";
    exports.embedServer                     = "https://blrt-embed-qa.herokuapp.com";

    exports.fbAppId                         = "1417078645178637";   // Same to dev; this is not a mistake; we use the same ID for both qa and dev because we want to reduce app numbers; we might separate them in future.
    exports.mandrillKey                     = "cKMAu25QxPorE-z6DLhHSw";// QA mandrill account is closed; old key: "bNaFyedL1P44jI5PJY-FKw";
    exports.logglyToken                     = "db2de934-f547-4c6c-9e6b-5be312a5768d";

    exports.stripeSecretKey                 = "sk_test_uPBg1qH2JdSftIHVUyKayNFZ";
    exports.stripePublicKey                 = "pk_test_T3rtrfIh67QmuCHw2Yi6XVRs";
    // exports.stripeSecretKey                 = "sk_live_XV0KZzObhDLNGStfhmkzQzFF";
    // exports.stripePublicKey                 = "pk_live_OLYzv94gcAjFQsYEABWE08DF";


    exports.hour                            /= 10;

    exports.mixpanelToken                   = "1825026d978cb1eb3dc708573439a9ed";
    exports.mixpanelApiKey                  = "40ad0dc991709744485f143516805e03";
    exports.mixpanelSecret                  = "d3cd4e35732616296288ebe1550e9882";
    exports.stream                          = "q";
    exports.googleAnalyticKey               = "UA-45564248-6";
    exports.webplayerUrl                    = "//s3.amazonaws.com/blrt-webplayer/q/bin/Blrt.nmf";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/q/bin/Blrt_recorder.nmf";

    //websocket notification
    exports.webappSiteAddress               = "https://blrt-webapp-qa.herokuapp.com";

    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/q/bin/Blrt_recorder.nmf" + recorderVersionQuery
} else if(exports.isStaging) {
    exports.salt                            = "th3c4k315r34l.,.n0t4l13";

    exports.appProtocol                     = "blrtstaging";
    exports.siteAddress                     = "s.blrt.co";
    exports.embedServer                     = "https://e.blrt.com";

    exports.fbAppId                         = "622198157834943";
    exports.mandrillKey                     = "cKMAu25QxPorE-z6DLhHSw";// Staging mandrill account is closed; old key: "tA7_Ssg01PbbHEgaItbaWA";
    exports.logglyToken                     = "db2de934-f547-4c6c-9e6b-5be312a5768d";

    exports.stripeSecretKey                 = "sk_test_uPBg1qH2JdSftIHVUyKayNFZ";
    exports.stripePublicKey                 = "pk_test_T3rtrfIh67QmuCHw2Yi6XVRs";

    exports.mixpanelToken                   = "16fae0ae7990cf5ef9b36155b58c0df4";
    exports.mixpanelApiKey                  = "b249a9161d156c909a5b9a2ccaf93ae8";
    exports.mixpanelSecret                  = "11c8e437b4644d4ff3d95ecf51ff6de9";
    exports.stream                          = "s";
    exports.googleAnalyticKey               = "UA-45564248-4";
    exports.webplayerUrl                    = "//s3.amazonaws.com/blrt-webplayer/s/bin/Blrt.nmf";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/s/bin/Blrt_recorder.nmf" + recorderVersionQuery;
} else if(exports.isLive){
    exports.salt                            = "xGp4zv0TYRBHvb5OFMc0HqS5Wp2q484FjHcxWCdJbQAl0SnbElxtRFDzXptU0ON5zt1sBbw8gfqQQNtL";

    exports.appProtocol                     = "blrt";
    exports.siteAddress                     = "m.blrt.co";
    exports.embedServer                     = "https://e.blrt.com";

    exports.fbAppId                         = "1451170328452442";
    exports.mandrillKey                     = "Uziz-NFjdIPpUw3lEv08og";
    exports.logglyToken                     = "693fde69-f9e4-4d71-b507-c573ce514eec";
    //
    exports.stripeSecretKey                 = "sk_live_XV0KZzObhDLNGStfhmkzQzFF";
    exports.stripePublicKey                 = "pk_live_OLYzv94gcAjFQsYEABWE08DF";

    exports.mixpanelToken                   = "70e57dbb31b6995f20acfadb25ef832e";
    exports.mixpanelApiKey                  = "abc91dc7f10078577ac61c1f877ed451";
    exports.mixpanelSecret                  = "62890eaf8c93300996b7944bea27d096";
    exports.stream                          = "m";
    exports.googleAnalyticKey               = "UA-45564248-5";
    exports.webplayerUrl                    = "//s3.amazonaws.com/blrt-webplayer/m/bin/Blrt.nmf";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/m/bin/Blrt_recorder.nmf";
    //websocket notification
    exports.webappSiteAddress               = "https://web.blrt.com";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/m/bin/Blrt_recorder.nmf" + recorderVersionQuery;

} else if(exports.isSpecial) {
    exports.salt                            = "W4ffl35  D3str0YZ $LL  B_r1t0  in  T-R3X   F4GHT!11`!1!";

    exports.appProtocol                     = "blrtspe";
    exports.siteAddress                     = process.env.PARSE_DOMAIN||"d.blrt.co";
    // new dev e server on Azure -- 29/08/2018
    exports.embedServer                     = "https://spe.blrt.com";

    exports.fbAppId                         = "1417078645178637";
    exports.mandrillKey                     = "cKMAu25QxPorE-z6DLhHSw";
    exports.logglyToken                     = "db2de934-f547-4c6c-9e6b-5be312a5768d";

    exports.stripeSecretKey                 = "sk_test_uPBg1qH2JdSftIHVUyKayNFZ";
    exports.stripePublicKey                 = "pk_test_T3rtrfIh67QmuCHw2Yi6XVRs";


    exports.hour                            /= 24;

    exports.mixpanelToken                   = "04e88384de4be4c336f416d627c0b659";
    exports.mixpanelApiKey                  = "66b86dded051262ddf1e5a1225794d49";
    exports.mixpanelSecret                  = "66b86dded051262ddf1e5a1225794d49";
    exports.stream                          = "d";
    exports.googleAnalyticKey               = "UA-45564248-2";
    exports.webplayerUrl                    = "//s3.amazonaws.com/blrt-webplayer/d/bin/Blrt.nmf";
    exports.webrecorderUrl                  = "//s3.amazonaws.com/blrt-webplayer/d/bin/Blrt_recorder.nmf";
    //websocket notification and email url
    exports.webappSiteAddress               = process.env.WEBAPP_SERVER_URL||"https://spweb.blrt.com";
}

exports.msForCommentEmail                   = exports.hour; // MUST BE AFTER isDevelop/is... BLOCK (because that changes the definition of an hour)



exports.stripeGenericErrorMessage           = "A problem was enountered when processing your payment details";
exports.stripePremiumPrice                  = {
    "base":                                 {
        "amount":                           11988,
        "currency":                         "USD"
    }
};
exports.stripePremiumDescription            = "Blrt Premium 1 year";

exports.stripePlusPrice                     = {
    "base":                                 {
        "amount":                           5988,
        "currency":                         "USD"
    }
};
exports.stripePremiumDescription            = "Blrt Plus 1 year";

// `error` follows the wiki documentation and is extended by other error
// enums (i.e. in api/util).

// Note that the message component of the error is generally not intended
// for user viewing, but rather for logging/debugging purposes.
exports.error = {
    unknown:                    { code: 0, message: "An unknown error occurred."}
};

// These errors however were defined with less planning and no
// documentation and are used for secondary emails. They're good and
// consistent within their own domains however the above enum is planned
// to be used everywhere.
exports.errors = {
    // Generic
    noError:                    0,
    unknown:                    -1,
    alreadyDone:                1,
    alreadyUsed:                5,
    objectNotFound:             10,
    objectInvalid:              11,
    permissionDenied:           50,

    // Specific
    updatePrimaryEmail: {
        taken:                  5
    },
    addSecondaryEmail:  {
        alreadyAdded:           1,
        taken:                  5,
        tooMany:                50
    },
    resendVerificationEmail: {
        alreadyVerified:        1,
        contactNotFound:        10,
        contactInvalid:         11
    },
    removeContact: {
        contactNotFound:        10
    }
}

exports.platforms = {
    iOS: "iOS",
    Android: "Android",
    OtherWeb: "Other Web",
    WebPlayer: "Web Player",
    WebApp: "Web App"
}


//yozio
if(exports.isDevelop || exports.isSpecial) {
    exports.yozioKey                            = "86878035-9559-401b-9451-fc9173f0a201";
    exports.yozioSecret                         = "579f1aca-7367-4ceb-b2bd-db95b1d7bc22";
    exports.yozioObj = {
        root_url: "http://api.yozio.com/v2.0/",
        click_url: "http://r.blrt.com/",
        impression_url: "http://i.yoz.io/",
        convo_sms: "BP.c.w",
        convo_facebook: "BP.c.x",
        convo_email: "BP.c.z",
        convo_other: "BP.c.B",
        blrt_sms: "BP.c.C",
        blrt_facebook: "BP.c.D",
        blrt_twitter: "BP.c.F",
        blrt_email: "BP.c.G",
        // temporarily changing the dev slug to live slug for redirect testing
        // blrt_email: "DL.c.f",
        blrt_other: "BP.c.H",
        blrtrequest_web: "BP.c.P",
        blrtrequest_api: "BP.c.Q",
        share_sms: "BP.c.J",
        share_facebook: "BP.c.K",
        share_twitter: "BP.c.L",
        share_email: "BP.c.M",
        share_other: "BP.c.N",
        oclist_email: "BP.c.T",
        oclist_facebook: "BP.c.V",
        oclist_other: "BP.c.S",
        oclist_sms: "BP.c.W"
    };
} else if(exports.isQA) {
    exports.yozioKey                            = "95bc5706-aa5a-4010-998e-1728a37f3986";
    exports.yozioSecret                         = "3cf1da18-9076-482b-89e8-6d2ebb476497";
    exports.yozioObj = {
        root_url: "http://api.yozio.com/v2.0/",
        click_url: "http://r.yoz.io/",
        impression_url: "http://i.yoz.io/",
        convo_sms: "Dt.c.q",
        convo_facebook: "Dt.c.n",
        convo_email: "Dt.c.m",
        convo_other: "Dt.c.p",
        blrt_sms: "Dt.c.h",
        blrt_facebook: "Dt.c.f",
        blrt_twitter: "Dt.c.j",
        blrt_email: "Dt.c.d",
        blrt_other: "Dt.c.g",
        blrtrequest_web: "Dt.c.l",
        blrtrequest_api: "Dt.c.k",
        share_sms: "Dt.c.v",
        share_facebook: "Dt.c.s",
        share_twitter: "Dt.c.w",
        share_email: "Dt.c.r",
        share_other: "Dt.c.t",
        oclist_email: "Dt.c.z",
        oclist_facebook: "Dt.c.B",
        oclist_other: "Dt.c.C",
        oclist_sms: "Dt.c.D",
        nonuser_preview:"Dt.c.F"
    };
} else {
    exports.yozioKey                            = "d92c37cf-93d7-4a7a-a395-0bda0f24bcaf";
    exports.yozioSecret                         = "eeb4e71c-dbfd-4714-91f3-7a23f002d7f9";
    exports.yozioObj = {
        root_url: "http://api.yozio.com/v2.0/",
        click_url: "http://r.blrt.com/",
        impression_url: "http://i.yoz.io/",
        convo_sms: "DL.c.r",
        convo_facebook: "DL.c.p",
        convo_email: "DL.c.n",
        convo_other: "DL.c.q",
        blrt_sms: "DL.c.j",
        blrt_facebook: "DL.c.g",
        blrt_twitter: "DL.c.k",
        blrt_email: "DL.c.f",
        blrt_other: "DL.c.h",
        blrtrequest_web: "DL.c.m",
        blrtrequest_api: "DL.c.l",
        share_sms: "DL.c.w",
        share_facebook: "DL.c.t",
        share_twitter: "DL.c.x",
        share_email: "DL.c.s",
        share_other: "DL.c.v",
        oclist_email: "DL.c.K",
        oclist_facebook: "DL.c.L",
        oclist_other: "DL.c.M",
        oclist_sms: "DL.c.N",
        nonuser_preview:"preview"
    };
}


exports.twilioKey = "ACd88a73ac835938233f28cdbd297b3f0a";
exports.twilioToken = "2981b121fc4c0834f7bd991516d61a55";
exports.twilioNumber={
    AU: "+61437826155",
    US: "+19035002578",
};

exports.countryCodes = ["AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","VG","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CK","CR","HR","CU","CW","CY","CZ","CD","DK","DJ","DM","DO","TL","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","CI","JM","JP","JE","JO","KZ","KE","KI","XK","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","NC","NZ","NI","NE","NG","NU","NF","KP","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","CG","RE","RO","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SX","SK","SI","SB","SO","ZA","GS","KR","SS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TJ","TZ","TH","TG","TK","TO","TT","TN","TR","TM","TC","TV","VI","UG","UA","AE","GB","US","UM","UY","UZ","VU","VA","VE","VN","WF","EH","YE","ZM","ZW"];