var constants = require('cloud/constants.js');
var _ = require("underscore");


exports.isMaintenanceMode = function() {
	return false;
};
exports.isRushJob = function () {
	return true;
}


/*

info = {
	appVersion: "",
	OS: 		"",
}

info = {
	"version": "",
	"device": "",
	"os": "",
	"osVersion": ""
}

*/
exports.IsVersionAllowances = function(info) {

console.log ("IsVersionAllowances START ====    ", JSON.stringify(info))
	//console.log("__IsVersonAllowances__"+JSON.stringify(info))
	var version = info.appVersion == null ? info.version : info.appVersion;
	
	return exports.GetAllowances().then(function (result) {
		if(!result.maintenanceMode) {
			var validVersions;
			var currentVersions;

			if(!info.os && info.OS) info.os = info.OS;

			var isiOS = false;

			switch(info.os) {
				case "iPhone OS":
				case "iOS": // starting from iOS SDK 9.3, the name is changed from "iPhone OS" to "iOS"
					isiOS = true;
					validVersions = [
						// "1.4.7", "1.4.8", "1.4.9", "1.4.10", 
						// "1.5.0", "1.5.1", "1.5.2", "1.5.3", "1.5.3.1", "1.5.4", "1.5.5", "1.5.6", "1.5.7", "1.5.8", "1.5.9", "1.5.10", "1.5.11", 
						// "1.6.0", "1.6.1", "1.6.2", "1.6.3", "1.6.4", "1.6.5", "1.6.6", "1.6.7", "1.6.8", "1.6.9", 
						// "1.7.0", "1.7.1", "1.7.2", "1.7.3", "1.7.4", "1.7.5", "1.7.6", "1.7.7", "1.7.8", "1.7.9", 
						// "1.8.0", "1.8.1", "1.8.2", "1.8.3", "1.8.4", "1.8.5", "1.8.6", "1.8.7", "1.8.8", "1.8.9", "1.9.0", "1.9.1",
						// "2.0.0", "2.0.1", 
						// "2.0.2", "2.0.3", "2.0.4", "2.0.5", "2.0.6", "2.0.7", "2.0.8", "2.0.9",
						// "2.1.0", "2.1.1", "2.1.2", 
						// "2.1.3", "2.1.4", "2.1.5", "2.1.6", "2.1.7", "2.1.8", "2.1.9",
						// "2.2.0", "2.2.1",
						// "2.3.0", "2.3.1","2.3.2","2.3.3","2.3.4","2.3.5","2.3.6","2.3.7","2.3.9","2.4.0","2.4.1","2.4.2","2.4.3","2.4.4","2.4.5",
						// "2.4.6", "2.4.7","2.4.8",
						//"2.4.9","2.4.10",'2.4.11','2.4.12','2.4.13',
                        //'2.5.0','2.5.1','2.5.2','2.5.5','2.5.6','2.5.7','2.5.8','2.5.9',
                        //'2.6.0','2.6.1','2.6.2','2.6.3','2.6.4','2.6.5','2.6.6', '2.6.7', '2.6.8', '2.6.9', 
                        //'2.7.0', '2.7.4', '2.7.5', '2.7.6', '2.7.7', '2.7.8', '2.7.9', '2.8.0', '2.8.1', '2.8.2', '2.8.3', '2.8.4', '2.8.5', '2.8.6', '2.8.7', '2.8.8', '2.8.9', '2.9.0', '2.9.1', '2.9.2', '2.9.3', '2.9.31', '2.9.4', '2.9.5', '2.9.6',

                        '2.9.7', '2.9.8', '2.9.9', '3.0.0', '3.0.1','3.0.2','3.0.3','3.0.4','3.0.5','3.0.6', '3.0.7','3.0.8'
					];
					currentVersions = validVersions;
					break;
				case "www":
					validVersions = ["1.0"];
					currentVersions = validVersions;
					break;
				//case "webApp":
				//	validVersions = ["0.1"];
				//	currentVersions =validVersions;
				//	break;
				default:
					if ("ChromeWebPlayer" == info.appName) {
						validVersions = ["1.0.0", "1.0.1", "1.0.2", "1.0.3", "1.0.4", "1.0.5", "1.0.6", "1.0.7", "1.0.8"];
					} else if("WebApp" ==info.appName){
						validVersions = ["0.1","0.2","0.3"]
					}else {//Android
						validVersions = [
							// "0.8.0", 
							// "1.0.0", "1.0.1", "1.0.2", "1.0.3", "1.0.4", "1.0.5", "1.0.6", "1.0.7", "1.0.8", "1.0.9", 
							// "1.1.0", "1.1.1", "1.1.2", "1.1.3", "1.1.4", "1.1.5", "1.1.6", "1.1.7", "1.1.8", "1.1.9", 
							// "1.2.0", "1.2.1", "1.2.2", "1.2.3", "1.2.4", "1.2.5", "1.2.6", 
							// "1.2.7", "1.2.8", "1.2.9", 
							// "1.3.0", "1.3.1",
							// "1.4.0", "1.4.1", "1.4.2", "1.4.3", "1.4.4", 
							// "1.4.5", "1.4.6", "1.4.7", "1.4.8", "1.4.9", "1.5.0",
							// "1.6.0", "1.6.1","1.6.2","1.6.3","1.6.4","1.6.5","1.6.6","1.6.7","1.6.8","1.6.9","1.6.10","1.6.11",
							//"1.7.0","1.7.1","1.7.2","1.7.3","1.7.4","1.7.5","1.7.6","1.7.7","1.7.8","1.7.9","1.7.10","1.7.11",'1.7.12','1.7.13',
                            //'1.8.0','1.8.1','1.8.2','1.8.3','1.8.4','1.8.5','1.8.6','1.8.7','1.8.8', '1.8.9', '1.9.0', '1.9.1', '1.9.2', '1.9.3', '1.9.4', '1.9.5', '1.9.6', '1.9.7', '1.9.8', '1.9.9', '2.0.0', '2.0.1', 

                           
                            '2.0.2', '2.0.3', '2.0.4','2.0.5','2.0.6','2.0.7','2.0.8','2.0.9','2.1.0', '2.1.1', '2.1.2'
						];
					}
					currentVersions = validVersions;
					break;
			}

			var invalidVersion = true;

			version = version.trim();

			//console.log("version: "+version);
			//console.log("validVersions: "+JSON.stringify(validVersions))

			_.each(validVersions, function (ver) {
				if(version.indexOf(ver) == 0
					&& (version.length == ver.length
						|| (version.charAt(ver.length) != "."
							&& _.isNaN(parseInt(version.charAt(ver.length)))
						)))
					invalidVersion = false;
			});

			//console.log("invalid version: "+invalidVersion)

			var versionOutdated = true;

			_.each(currentVersions, function (ver) {
				if(version.indexOf(ver) == 0
					&& (version.length == ver.length
						|| (version.charAt(ver.length) != "."
							&& _.isNaN(parseInt(version.charAt(ver.length)))
						)))
					versionOutdated = false;
			});


			result.isVersionOutOfDate = versionOutdated;
			result.invalidVersion = invalidVersion;
			result.invalidVersionMessage = JSON.stringify({
				Title: 		isiOS ? 
					"Blrt update required" :
					"Blrt update required",
				Message: 	isiOS ?
					"Please download the free update from the App Store to continue Blrting." :
					"The Blrt alpha is moving forward and some old versions needed to be discontinued. Please download the free update from the Google Play store to continue Blrting.",
				Options: [
					{
						type: 		"url",
						label: 		"Check for update",
						arg: 		"{\"url\":\"https:" + (isiOS? constants.appStoreUrl: constants.googleStoreUrl) + "\"}", 
						fallback: 	null
					}
				]
			});
		}
		// console.log('result IsVersionAllowances ===   '+JSON.stringify(result))
		return result;
	});
};

/* THE FUTURE (will require force update)
{
	"type": "alert",
	"deps": ["alert-iap"],
	"args": {
		"title": "Moo",
		"message": "Cows are good",
		"cancel": "Right...",
		"options": [
			{
				"type": "alert-url",
				"args": "{
					"label": "Buy it!",
					"url": "http://moo.com/"
				}",
				"fallback": {
					"type": "alert-iap",
					"args": {
						"label": "Buy from store!",
						"url": "http://store.com/moo"
					}
				}
			}
		]
	},
	"fallback": {
		"type": "bsod",
		"args": {
			"title": "oops",
			"message": "Invalid version\n\nPlease update your app from the app store."
		}
	}
}
*/


/* THE PRESENT
Message = {
	Title: 				"Title",
	Message: 			"Message",
	Cancel: 			"OK",
	Options: [
		{
			type: 		"url",
			label: 		"Go to AppStore",
			arg: 		"http://www.apple.com/",
			fallback:  {
				type: 		"iap",
				label: 		"Go to AppStore",
				arg: 		"{"iapId":""}}",
				fallback: 	null

			}

		}
	]
}
*/


exports.GetAllowances = function(){
	return Parse.Promise.as(null).then(function () {
		var mm = exports.isMaintenanceMode();
        console.log ("\n\n\n\n in GetAllowances\n\n\n")

		return {
			maintenanceMode: 	mm,
			maintenanceMessage: JSON.stringify({
				Title: 			"Blrt is currently down for maintenance",
				Message: 		"Check back soon.",
				Option: 		[]
			}),
			canView: 			!mm && true,
			warningMessage: 	null
		};
	});
};


exports.GetSettings = function () {
	var settingsQuery = new Parse.Query(constants.settings.keys.className);


	return settingsQuery.find().then(function (results) {
		var settings = {};

		var length = results.length;
		for (var i = 0; i < length; ++i) {
			var key = results[i].get(constants.settings.keys.Key);
			settings[key] = results[i].get(constants.settings.keys.Value);
		}
		return settings;
	});
}
