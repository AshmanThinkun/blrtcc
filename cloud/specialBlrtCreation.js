 var constants = require('cloud/constants.js');
var EmailJS = require('cloud/email.js');
var Util = require('cloud/util.js');
var loggler = require("cloud/loggler.js");
const MediaAsset = require("cloud/api/models/MediaAsset");
const Blrt = require("cloud/api/models/Blrt");

var api = require("cloud/api/util");
var _ = require("underscore");
const muteJS = require("cloud/api/conversationUserRelation/mute");

var ParseMutex = require("cloud/ParseMutex");
var define=require('../lib/cloud').define()
Parse.Cloud.define=define
var specialConstants  = {
    className:              "CloudConversationTemplates",

    Slug:                   "slug",
    SegmentIndex:           "segmentIndex", 

    Title:                  "title",
    ThumbnailFile:          "thumbnailFile",

    ItemType:               "itemType",
    ItemArgument:           "itemArgument",
    AudioFile:              "audioFile",
    TouchFile:              "touchFile",
    MediaObjects:           "mediaObjects",
};
// console.log(Parse.Cloud.job.toString())
Parse.Cloud.job("processQueuedConvoTemplates", function (req, res) {
    var l = loggler.create("job:processQueuedConvoTemplates");
    return new Parse.Query(Parse.User)
        .greaterThan("queuedConvoTemplateCount", 0)
        .find()
        .then(function (users) {
            var promises = [];
            var errors = []

            _.each(users, function (user) {
                if(!_.isArray(user.get("queuedConvoTemplates")) || user.get("queuedConvoTemplates").length == 0)
                    return promises.push(user.increment("queuedConvoTemplateCount", -1 * user.get("queuedConvoTemplateCount")).save());
                promises.push(processQueuedConvoTemplates ({ user: user }, l));
            });

            console.log("would you mind return something ???? ")
            return Parse.Promise.when(promises).then(
                function () {  res.success({success:true}); },
                function () {  res.error({success:false}); }
            );
        }, function (error) {
            console.log("error "+error)
            l.log(false, "Parse.User.find() error:", error).despatch();
            return res.error("Parse.User.find() error: " + JSON.stringify(error));
        });
},'* */1 * * * *');

// This is called from a specific user to get their own templates processed.
Parse.Cloud.define("processQueuedConvoTemplates", function (req, res) {
    console.log('\n\n\nprocessQueuedConvoTemplates req===');
    console.log(req);
    let userFromBlrtCom;
    if(req.params.user) {
        return new Parse
            .Query('User')
            .equalTo('username', req.params.user)
            .first(user=>{
                console.log('\n\n\n===user user user===');
                console.log(user);
                var l = loggler.create("function:processQueuedConvoTemplates");
                return processQueuedConvoTemplates (req, user, l)
                    .then(function(result) { 
                        console.log('\n\n\n===result welcome convo===');
                        console.log(result); 
                        return res.success({success:true}); 
                    }, (error)=> { 
                        console.log('\n\n\n===error welcome convo===');
                        console.log(error); 
                        return res.error({success:false}); 
                    }
                );
            })
    }else {
        var l = loggler.create("function:processQueuedConvoTemplates");
        return processQueuedConvoTemplates (req, null, l).then(
            function () { return res.success({success:true}); },
            function () { return res.error({success:false}); }
        );
    } 
});

// This is used by the job and user function to go through the pendingQueue for the user
// and pop one template out of it.
function processQueuedConvoTemplates (req, userFromBlrtCom, l) {
    return Parse.Promise.as().then(function () {
        var user = req.user || userFromBlrtCom; 
        console.log('\n\n\n\n===user===');
        console.log(user);
        
        if(user === null || user.id == null) {
            l.log(false, "User object is required").despatch();
            return Parse.Promise.error("User object is required");
        }

        var templates = user.get(constants.user.keys.queuedConvoTemplates);
        console.log('\n\nqueuedConvoTemplates==='+templates);
        templates = templates.slice(); // Scared of reference pointers and Parse >< make a duplicate here
        if(templates == null || templates.length === 0) {
            l.log(false, "User's queuedConvoTemplates is empty").despatch();
            return;
        }

        var template;
        do {
            template = templates.shift().trim();
        } while(template === null || template === "");

        console.log('template after transformation==='+template);

        return sendBlrtFunction({
            users: [user],
            slug: template,
        });
    });
}

// This doesn't look at the user's pending queue at all and therefore doesn't need
// to update it when creation finishes - it simply attempts to create a template
// based on the request.

Parse.Cloud.define("giveSpecialConversationFromEmail", function(request, response) {
    console.log('\n\nfunction=== === ===');
    // var l = loggler.create("function:giveSpecialConversationFromEmail");
    Parse.Cloud.useMasterKey();

    const {
        arg,
        info,
        slug,
        emails,
    } = request.params.data

    let usersEmail = new Array();

    for(let i = 0; i < emails.length; ++i) {
        usersEmail[i] = emails[i].toLowerCase();
    }   

    return new Parse.Query(Parse.User)
        .containedIn(constants.user.keys.username, usersEmail)
        .find({useMasterKey: true})
        .then(results=>{
            // l.log("userQuery.find() results:", results);
            console.log('\n\nresults length==='+results.length);
            if(results.length > 0) {
                if(arg && arg != 'convoTitle') {
                    console.log('\n\n===template==='+arg);
                    return sendTemplateToConvo({
                        slug: slug,
                        users: results,
                        info: info,
                        arg: arg,
                    }, true).then(function(result) { 
                        console.log('\n\nresult result result==='+JSON.stringify(result));
                        if(result) {
                            return response.success({success: true});
                        }else {
                            return response.success({success: false, error: 'Parse error'});
                        }
                    }, (error)=>{ 
                        console.log('\n\n===error1===');
                        return response.error({success: false, error: error}); 
                    }); 
                }
                // response.success({success: true});
            }else {
                console.log('\n\n???===user no found===')
                response.success({success: false, error: 'User no found'});
            }
        }).catch((error)=>{
            response.error({success: false, error: error});
        });

    // response.success({success: true});
});

// Similar to the above job, except with user IDs instead of emails, this doesn't look
// at or update the user's pending queue.
Parse.Cloud.job("giveSpecialConversation", function(request, response) {

    Parse.Cloud.useMasterKey();

    var userQuery = new Parse.Query(Parse.User)
        .containedIn(constants.base.keys.objectId, request.params.users);

    return userQuery.find().then(function(results){
        return sendBlrtFunction({
            slug: request.params.slug,
            users: results
        }, true).then(
            function () { return response.success(); },
            function () { return response.error(); }
        );
    }).then (function(){
        response.success("Wins");
    }, function(error){
        response.error(JSON.stringify(error));
    });
},false);

/*
{
    slug: "slug slug",
    users: [
        parseUsers
    ]
}
*/

// # Issue conversation templates

// This implementation can be provided multiple users, but will only evaluate them one at a time.
// It can be broken down into 9 steps, each of which can be completed asynchronously:
// 1. Acquire mutex;
// 2. If dirty, resent counter and array to solely represent the current mutex;
// 3. Check for existing COMPLETE convo template issued to this user, and fetch admin user;
// 4. Create INCOMPLETE convo;
// 5. Create role with admin and subject user;
// 6. Create relations and items;
// 7. Mark convo as COMPLETE;
// 8. Dequeue template from user, increase admin statistics and send push notification; and
// 9. Relinquish mutex.
// While the loop is getting started we can fetch all media and such from the arguments and ensure
// that the arguments are formatted correctly. If they're not then the entire process should abort.
var sendBlrtFunction = function(args, ignoreQueuedConvoTemplates) {
    // Incase this is called from a direct template creation request (i.e. user admin form) which
    // doesn't require the template to be queued for the user, we also don't want to attempt to 
    // dequeue or decrement the counter for that user.
    ignoreQueuedConvoTemplates = ignoreQueuedConvoTemplates != undefined ? ignoreQueuedConvoTemplates : false;

    if(!_.isArray(args.users) || args.users.length == 0 || !_.isString(args.slug) || args.slug.length == 0)
        return;

    var users = args.users;
    var slug = args.slug;

    console.log('\n\n\nusers: '+users);
    console.log('\n\n\nslug==='+slug);

    // To do users synchronously we have to work with callbacks. That's because there's no way
    // to wait for a promise to finish. We just launch it and then tell the promise that when
    // you're finished, launch it again.
    function handleUser(i) {
        var l = loggler.create("actualSendConvoTemplateFunction");

        var user = users[i];
        var now = new Date();

        console.log('user email==='+user.get('email'));

        // Parse fields can't have hyphens in their names, so we need to make sure there are none
        // from when we add the template slug to the mutex action (because the mutex will create
        // fields to get it's job done).
        var mutexAction = "convoTemplate_" + slug.replace(/-/gi, "_");

        // ## Step 0: Fetch conversation template data
        var templateDataPromise = new Parse.Query("CloudConversationTemplates")
            .equalTo("slug", slug)
            .ascending("segmentIndex")
            .find()
            // We'll also use this opportunity to verify the data.
            .then(function (templateData) {
                // Did we get good template data? The first item has to be a conversation starting Blrt.
                if(templateData.length === 0)
                    return Parse.Promise.error("NO RESULTS");
                if(templateData[0].get("itemType") !== constants.convoItem.types.blrt)
                    return Parse.Promise.error("FIRST ITEM IS NOT BLRT");

                // Make sure all of the requested media is present!
                var mediaPointers = [];
                var badMediaObjectArray = "BAD MEDIA OBJECT ARRAY";
                try {
                    _.each(templateData, function (datum) {
                        switch(datum.get("itemType")) {
                            case constants.convoItem.types.blrt:
                            case constants.convoItem.types.reBlrt:
                                if(datum.get("mediaObjects") != null) {
                                    if(!_.isArray(datum.get("mediaObjects")))
                                        throw badMediaObjectArray;

                                    mediaPointers = mediaPointers.concat(datum.get("mediaObjects"));
                                }

                                break;
                        }
                    });
                } catch(e) {
                    if(e == badMediaObjectArray)
                        return Parse.Promise.error(e);
                    throw e;
                }

                var mediaPromise;
                if(mediaPointers.length == 0)
                    mediaPromise = Parse.Promise.as();
                else
                    mediaPromise = new Parse.Query("MediaAsset")
                        .containedIn("objectId", _.pluck(mediaPointers, "id"))
                        .find({useMasterKey:true})
                        .then(function (media) {
                            var mediaIds = _.pluck(media, "id");

                            // We've got all the media that they've listed in their mediaObject arrays,
                            // now we need to make sure that all of the arguments they've listed consist
                            // of existing media objects or templates ONLY. No imaginary stuff.
                            var badArgument = "BAD ARGUMENT";
                            var imaginaryMedia = "MISSING MEDIA OBJECT FOR TEMPLATE";
                            var problematicObject;

                            // We'll also make sure that the provided arguments are correct in all cases,
                            // structurally speaking. We'll be using API utility's ParamNode to do this
                            // relatively easily.
                            var blrtArgumentStructure = {
                                "AudioLength": {
                                    type: api.varTypes.int,
                                    required: { presence: true, min: 0 }
                                },
                                "Items": {
                                    type: api.varTypes.array,
                                    required: { presence: true, min: 1 },
                                    subtype: {
                                        "PageString": {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        "MediaId": {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                },
                                "HiddenItems": {
                                    type: api.varTypes.array,
                                    subtype: {
                                        "PageString": {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        "MediaId": {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                }
                            };
                            var commentArgumentStructure = {
                                "Comment": {
                                    type: api.varTypes.text,
                                    required: true
                                }
                            };
                            var eventArgumentStructure = {
                                items: {
                                    type: api.varTypes.array,
                                    required: { presence: true, min: 1 },
                                    subtype: {
                                        userId: {
                                            type: api.varTypes.text
                                        },
                                        type: {
                                            type: api.varTypes.contactType,
                                            required: true
                                        },
                                        value: {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        name: {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                }
                            };
                            var noteArgumentStructure = {
                                "Message": {
                                    type: api.varTypes.text,
                                    required: true
                                },
                                "MessageType": {
                                    type: api.varTypes.int,
                                    required: true
                                }
                            };

                            try {
                                _.each(templateData, function (datum) {
                                    function failed(e) {
                                        problematicObject = datum.id;
                                        throw e;
                                    }

                                    var argument;
                                    try {
                                        argument = JSON.parse(datum.get("itemArgument"));
                                    } catch(e) {
                                        failed(badArgument);
                                    }

                                    switch(datum.get("itemType")) {
                                        case constants.convoItem.types.blrt:
                                        case constants.convoItem.types.reBlrt:
                                            api.ParamNode.initialise(argument, blrtArgumentStructure);

                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);

                                            argument = api.ParamNode.root.value;

                                            // We'll be doing the same check for normal and hidden items.
                                            _.each(["Items", "HiddenItems"], function (targetArray) {
                                                if(argument[targetArray]) {
                                                    _.each(argument[targetArray], function (item) {
                                                        // Skip templates!
                                                        if(item["MediaId"].lastIndexOf("_template", 0) === 0)
                                                            return;

                                                        if(!_.contains(mediaIds, item["MediaId"]))
                                                            failed(imaginaryMedia);
                                                    });
                                                }
                                            });

                                            break;
                                        case constants.convoItem.types.comment:
                                            console.log('===comment===');
                                            api.ParamNode.initialise(argument, commentArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                        case constants.convoItem.types.eventAdd:
                                            api.ParamNode.initialise(argument, eventArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                        case constants.convoItem.types.eventMessage:
                                            api.ParamNode.initialise(argument, noteArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                    }
                                });
                            } catch(e) {
                                if(e == badArgument || e == imaginaryMedia) {
                                    var msg = e + " - template object: " + problematicObject;

                                    l.log(msg);
                                    return Parse.Promise.error(msg);
                                }
                                throw e;
                            }
                        });

                return mediaPromise.then(function () {
                    // We'll also use this opportunity to count everything up for them!
                    var readableCount = 0;
                    var commentCount = 0;
                    var blrtCount = 0;
                    var eventCount = 0;
                    var noteCount = 0;
                    _.each(templateData, function (datum) {
                        switch(datum.get("itemType")) {
                            case constants.convoItem.types.blrt:
                            case constants.convoItem.types.reBlrt:
                                ++blrtCount;
                                ++readableCount;
                                break;
                            case constants.convoItem.types.comment:
                                ++commentCount;
                                ++readableCount;
                                break;
                            case constants.convoItem.types.eventAdd:
                                ++eventCount;
                                break;
                            case constants.convoItem.types.eventMessage:
                                ++noteCount;
                                break;
                        }
                    });

                    // This is how we return multiple values apparently...
                    return Parse.Promise.when(
                        Parse.Promise.as(templateData),
                        Parse.Promise.as(readableCount),
                        Parse.Promise.as(blrtCount),
                        Parse.Promise.as(commentCount),
                        Parse.Promise.as(eventCount),
                        Parse.Promise.as(noteCount)
                    );
                });
            });

        // Under normal circumstances, if one user fails then the next can go through anyway.
        // However in the case of a super fail (i.e. if template data fails), nobody can.
        var superFail = false;

        // ## Step 1: Acquire mutex
        return ParseMutex.fnWithLock(user, mutexAction, function (user, dirty) {
            // ## Step 2: If dirty, reset counter and array to solely represent the current mutex
            var resetMutex;
            // if(dirty) {
            //     // If dirty was provided to us then it's a time integer representing the time that
            //     // this mutex should expire. We need to put that back into the mutex array.
            //     resetMutex = user
            //         .set("dirtyCounter_" + mutexAction, 1)
            //         .set("dirtyPendingAttempt_" + mutexAction, [dirty])
            //         .save(nu);
            // } else
                resetMutex = Parse.Promise.as();

            return resetMutex.then(function () {}, function (promiseError) {
                l.log(false, "Mutex was dirty but we failed to clean it... proceeding anyway:", promiseError).despatch();
            }).always(function () {
                // ## Step 3: Check for existing template and fetch admin user

                // We create the relation for this user in a way that indicates which template
                // it belongs to. This was done so we can easily query for it! Lets do that!
                return Parse.Promise.when(
                    new Parse.Query("ConversationUserRelation")
                        .equalTo("user", user) 
                        .equalTo("convoTemplateSlug", 'welcome-jan15')
                        .include("conversation")
                        .find({useMasterKey:true}),
                    new Parse.Query(Parse.User)
                        .equalTo("isAdminUser", true) // support@blrt.com
                        .first({useMasterKey:true})
                ).then(function (potentiallyClashingRelations, adminUser) {
                    // Make sure the admin user exists - we can't continue without support.
                    if(adminUser == null)
                        return;

                    console.log('\nadminUser email==='+adminUser.get('email'));
                    console.log('potentiallyClashingRelations: '+potentiallyClashingRelations);

                    // Next loop through the potentially clashing relations. If any of their conversations AREN'T
                    // incomplete (i.e. they've successfully created a conversation under this template) then exit.
                    var clash = false;
                    _.each(potentiallyClashingRelations, function (relation) {
                        console.log('incomplete==='+relation.get("conversation").get("incomplete"));
                        if(relation.get("conversation").get("incomplete") != true) {
                            l.log(true, "Conversation clash detected: " + relation.get("conversation").id).despatch();
                            clash = true;
                        }
                    });

                    // If we have a clash then we're done with this template.
                    if(clash) {
                        if(ignoreQueuedConvoTemplates) 
                            return;
                        return user
                            .remove("queuedConvoTemplates", slug)
                            .increment("queuedConvoTemplateCount", -1)
                            .save(null,{useMasterKey:true})
                            .fail(function (promiseError) {
                                l.log("ALERT: Error dequeueing convo template slug from user after finding clash!").despatch();
                                return Parse.Promise.as();
                            });
                    }

                    return templateDataPromise.then(function (templateData, readableCount, blrtCount, commentCount, eventCount, noteCount) {
                        // If the template data was invalid then we'd be in the error flow of this promise, not here :P
                        var initialBlrt = templateData[0];
                    
                        // ## Step 4: Create incomplete conversation
                        var thePrefillName = user.get("hasnotSetDisplayname")? "": user.get("name");
                        console.log('thePrefillName==='+thePrefillName);
                        return new Parse.Object(constants.convo.keys.className) //conversation
                            .set(constants.base.keys.version, constants.convo.version)
                            .set(constants.base.keys.creator, adminUser)
                            .set(constants.convo.keys.ThumbnailFile, initialBlrt.get("thumbnailFile"))
                            .set(constants.convo.keys.Name, Util.format(initialBlrt.get("title"), [thePrefillName]))
                            .set(constants.convo.keys.BondCount, 2)
                            .set(constants.convo.keys.generalCount, templateData.length)
                            .set(constants.convo.keys.readableCount, readableCount)
                            .set(constants.convo.keys.commentCount, commentCount)
                            .set(constants.convo.keys.blrtCount, blrtCount)
                            .set("specialSlug", slug)
                            .set("eventCount", eventCount)
                            .set("noteCount", noteCount)
                            .set(constants.convo.keys.ContentUpdate, now)
                            .set(constants.convo.keys.CreationType, constants.convo.creationTypes.template)
                            .set(constants.convo.keys.StatusCode, constants.convo.statusCodes.normal)
                            .set(constants.convo.keys.LastMedia, initialBlrt.get("mediaObjects"))
                            .set(constants.convo.keys.LastMediaArgument, initialBlrt.get("itemArgument"))
                            .set(constants.convo.keys.incomplete, true)
                            .save(null,{useMasterKey:true})
                            .then(function (convo) {
                                // ## Step 5: Create role with admin and subject user
                                var role = new Parse.Role("Conversation-" + convo.id, new Parse.ACL());
                                role.getUsers().add([adminUser, user]);

                                return role.save().then(function () {
                                    var roleACL = new Parse.ACL();
                                    roleACL.setReadAccess(role, true);
                                    roleACL.setWriteAccess(role, true);

                                    // ## Step 6: Create relations and items

                                    // * Create the admin relation - they're the creator and they've read everything.
                                    var adminRelation = new Parse.Object(constants.convoUserRelation.keys.className)
                                        .set(constants.base.keys.version, constants.convoUserRelation.version)
                                        .set(constants.base.keys.creator, adminUser)
                                        .set(constants.convoUserRelation.keys.conversation, convo)
                                        .set(constants.convoUserRelation.keys.convoCreator, adminUser)
                                        .set(constants.convoUserRelation.keys.user, adminUser)
                                        .set(constants.convoUserRelation.keys.indexViewedList, "0-" + (readableCount - 1))
                                        .set(constants.convoUserRelation.keys.lastViewed, now)
                                        .set(constants.convoUserRelation.keys.lastEdited, now)
                                        .set(constants.convoUserRelation.keys.QueryType, "_creator")
                                        .set("contentUpdate", now)
                                        .setACL(roleACL);

                                    // * Create the target user's relation. The admin invited them.
                                    var userRelation = new Parse.Object(constants.convoUserRelation.keys.className)
                                        .set(constants.base.keys.version, constants.convoUserRelation.version)
                                        .set(constants.base.keys.creator, adminUser)
                                        .set(constants.convoUserRelation.keys.conversation, convo)
                                        .set(constants.convoUserRelation.keys.convoCreator, adminUser)
                                        .set(constants.convoUserRelation.keys.user, user)
                                        .set(constants.convoUserRelation.keys.QueryType, constants.contactTable.types.email)
                                        .set(constants.convoUserRelation.keys.QueryValue, user.get("username"))
                                        .set("allowInInbox", true)
                                        .set("convoTemplateSlug", slug)
                                        .set("contentUpdate", now)
                                        .setACL(roleACL);

                                    // * Create each conversation item - note that they were fetched in the correct order.
                                    var contentItems = [];
                                    var generalPos = -1;
                                    var readablePos = -1;
                                    var nameField = thePrefillName==""? "": (" "+thePrefillName);

                                    _.each(templateData, function (datum) {
                                        var readable = _.contains(constants.convoItem.readableTypes, datum.get("itemType"));

                                        ++generalPos;
                                        if(readable) ++readablePos;

                                        var item = new Parse.Object(constants.convoItem.keys.className) //ConversationItem
                                            .set(constants.base.keys.version, constants.convoItem.version)
                                            .set(constants.base.keys.creator, adminUser)
                                            .set(constants.base.keys.encryptType, constants.convoItem.encryptTypes.none)
                                            .set(constants.convoItem.keys.conversation, convo)
                                            .set(constants.convoItem.keys.Argument, Util.format(datum.get("itemArgument"), [nameField]))
                                            .set(constants.convoItem.keys.type, datum.get("itemType"))
                                            .set(constants.convoItem.keys.generalIndex, generalPos)
                                            .setACL(roleACL);

                                        if(readable) item.set(constants.convoItem.keys.readableIndex, readablePos)
                                            .set(constants.convoItem.keys.psuedoId, convo.id + "-" + readablePos);
                                        else item.set(constants.convoItem.keys.readableIndex, -1);

                                        switch(datum.get("itemType")) {
                                            case constants.convoItem.types.blrt:
                                            case constants.convoItem.types.reBlrt:
                                                item.set(constants.convoItem.keys.AudioFile, datum.get("audioFile"))
                                                    .set(constants.convoItem.keys.TouchDataFile, datum.get("touchFile"))
                                                    .set(constants.convoItem.keys.Media, datum.get("mediaObjects"))
                                                    .set("thumbnailFile", datum.get("thumbnailFile"));
                                                break;
                                        }

                                        contentItems.push(item);
                                    });

                                    return Parse.Object.saveAll(_.union([adminRelation, userRelation], contentItems),{useMasterKey:true}).then(function () {
                                        // ## Step 7: Mark conversation as complete and set ACL
                                        return convo
                                            .unset("incomplete")
                                            .setACL(roleACL)
                                            .save(null,{useMasterKey:true})
                                            .then(function () {
                                                // ## Step 8: Dequeue template from user, increase admin statistics and send push notification

                                                // * Send push notification - nobody will wait for this.
                                                var message = EmailJS.GeneratePush("PN", {
                                                    t: 1,
                                                    bid: convo.id,
                                                }, [
                                                    adminUser.get("name"),
                                                    convo.get("blrtName")
                                                ]);
                                                Parse.Push.send({
                                                    where: new Parse.Query(Parse.Installation).equalTo('owner', user),
                                                    data: message
                                                }, { useMasterKey: true });

                                                var toSave = [];

                                                // * Remove the template from the list.
                                                if(!ignoreQueuedConvoTemplates)
                                                    toSave.push(user
                                                        .remove("queuedConvoTemplates", slug)
                                                        .increment("queuedConvoTemplateCount", -1)
                                                    );

                                                // * Increase admin's statistics.
                                                toSave.push(adminUser
                                                    .increment("convoInvitationCounter")
                                                    .increment("conversationCreationCounter")
                                                    .increment("blrtCreationCounter", blrtCount)
                                                    .increment("commentCreationCounter", commentCount)
                                                );

                                                return Parse.Object.saveAll(toSave,{useMasterKey:true}).then(function () {
                                                    console.log("everything is correct");
                                                    // ## Step 9: Relinquish mutex.
                                                    l.log(true).despatch();
                                                    return true;
                                                }, function (promiseError) {
                                                    l.log(false, "Error when dequeueing slug or incrementing admin stats:", promiseError).despatch();
                                                    return Parse.Promise.as();
                                                });
                                            }, function (promiseError) {
                                                l.log(false, "Error when marking conversation as complete:", promiseError).despatch();
                                                return Parse.Promise.as();
                                            });
                                    }, function (promiseError) {
                                        l.log(false, "Error when creating content items and relations:", promiseError).despatch();
                                        return Parse.Promise.as();
                                    });
                                }, function (promiseError) {
                                    l.log(false, "Error when creating convo role:", promiseError).despatch();
                                    return Parse.Promise.as();
                                });
                            }, function (promiseError) {
                                l.log(false, "Error when creating conversation object:", promiseError).despatch();
                                return Parse.Promise.as();
                            });
                    }).fail(function (promiseError) {
                        l.log(false, "Failed to fetch conversation template data:", promiseError).despatch();
                        // If we failed to get this then we shouldn't try again with other users...
                        superFail = true;
                        return Parse.Promise.as();
                    });
                }, function (promiseErrors) {
                    l.log(false, "Failed to fetch clashing relations or admin user:", promiseErrors).despatch();
                    return Parse.Promise.as();
                });
            });
        }).always(function () {

            if(!superFail) {
                // After this mutex is done, start with the next user (if one exists).
                if(users.length - 1 > ++i){
                    return handleUser(i).fail(function (error) {
                        console.log("ALERT: Caught something when handling a user.1..:" + error);
                        return Parse.Promise.as();
                    });
                }
            }
        });
    }

    console.log('users length==='+users.length);
    if(_.isArray(users) && users.length != 0) 
        return handleUser(0).fail(function (error) {
            console.log("ALERT: Caught something when handling a user.2..:" + error);
            return Parse.Promise.as();
        });
};


// request from the admin page, send template to any existing convo or new convo
var sendTemplateToConvo = function(args, ignoreQueuedConvoTemplates) {
    // Incase this is called from a direct template creation request (i.e. user admin form) which
    // doesn't require the template to be queued for the user, we also don't want to attempt to 
    // dequeue or decrement the counter for that user.
    ignoreQueuedConvoTemplates = ignoreQueuedConvoTemplates != undefined ? ignoreQueuedConvoTemplates : false;

    if(!_.isArray(args.users) || args.users.length == 0 || !_.isString(args.slug) || args.slug.length == 0)
        return;

    var users = args.users;
    var slug = args.slug;

    var arg = args.arg;
    var info = args.info;

    console.log('\n\nslug==='+slug);
    console.log('arg==='+arg);
    console.log('info==='+info);

    // To do users synchronously we have to work with callbacks. That's because there's no way
    // to wait for a promise to finish. We just launch it and then tell the promise that when
    // you're finished, launch it again.
    function handleUser(i) {
        var l = loggler.create("actualSendConvoTemplateFunction");

        var user = users ? users[i] : undefined;
        const now = new Date();
        const hourFromNow = new Date(+ new Date() + constants.msForCommentEmail);
        const expiration = new Date(+ now + 15000);

        // console.log('user email==='+user.get('email'));

        // Parse fields can't have hyphens in their names, so we need to make sure there are none
        // from when we add the template slug to the mutex action (because the mutex will create
        // fields to get it's job done).
        var mutexAction = "convoTemplate_" + slug.replace(/-/gi, "_");

        // ## Step 0: Fetch conversation template data
        var templateDataPromise = new Parse.Query("CloudConversationTemplates")
            .equalTo("slug", slug)                                                                                                                                                      
            .ascending("segmentIndex")
            .find({useMasterKey:true})
            // We'll also use this opportunity to verify the data.
            .then(function (templateData) {
                console.log('\n\n\n\ntemplateData length==='+templateData.length);
                // Did we get good template data? The first item has to be a conversation starting Blrt.
                if(templateData.length === 0)
                    return Parse.Promise.error("NO RESULTS");

                // Make sure all of the requested media is present!
                var mediaPointers = [];
                var badMediaObjectArray = "BAD MEDIA OBJECT ARRAY";
                try {
                    _.each(templateData, function (datum) {
                        switch(datum.get("itemType")) {
                            case constants.convoItem.types.blrt:
                            case constants.convoItem.types.reBlrt:
                                if(datum.get("mediaObjects") != null) {
                                    if(!_.isArray(datum.get("mediaObjects")))
                                        throw badMediaObjectArray;

                                    mediaPointers = mediaPointers.concat(datum.get("mediaObjects"));
                                }

                                break;
                        }
                    });
                } catch(e) {
                    if(e == badMediaObjectArray)
                        return Parse.Promise.error(e);
                    throw e;
                }

                var mediaPromise;
                if(mediaPointers.length == 0)
                    mediaPromise = Parse.Promise.as();
                else
                    mediaPromise = new Parse.Query("MediaAsset")
                        .containedIn("objectId", _.pluck(mediaPointers, "id"))
                        .find({useMasterKey:true})
                        .then(function (media) {
                            var mediaIds = _.pluck(media, "id");

                            // We've got all the media that they've listed in their mediaObject arrays,
                            // now we need to make sure that all of the arguments they've listed consist
                            // of existing media objects or templates ONLY. No imaginary stuff.
                            var badArgument = "BAD ARGUMENT";
                            var imaginaryMedia = "MISSING MEDIA OBJECT FOR TEMPLATE";
                            var problematicObject;

                            // We'll also make sure that the provided arguments are correct in all cases,
                            // structurally speaking. We'll be using API utility's ParamNode to do this
                            // relatively easily.
                            var blrtArgumentStructure = {
                                "AudioLength": {
                                    type: api.varTypes.int,
                                    required: { presence: true, min: 0 }
                                },
                                "Items": {
                                    type: api.varTypes.array,
                                    required: { presence: true, min: 1 },
                                    subtype: {
                                        "PageString": {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        "MediaId": {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                },
                                "HiddenItems": {
                                    type: api.varTypes.array,
                                    subtype: {
                                        "PageString": {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        "MediaId": {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                }
                            };
                            var commentArgumentStructure = {
                                "Comment": {
                                    type: api.varTypes.text,
                                    required: true
                                }
                            };
                            var eventArgumentStructure = {
                                items: {
                                    type: api.varTypes.array,
                                    required: { presence: true, min: 1 },
                                    subtype: {
                                        userId: {
                                            type: api.varTypes.text
                                        },
                                        type: {
                                            type: api.varTypes.contactType,
                                            required: true
                                        },
                                        value: {
                                            type: api.varTypes.text,
                                            required: true
                                        },
                                        name: {
                                            type: api.varTypes.text,
                                            required: true
                                        }
                                    }
                                }
                            };
                            var noteArgumentStructure = {
                                "Message": {
                                    type: api.varTypes.text,
                                    required: true
                                },
                                "MessageType": {
                                    type: api.varTypes.int,
                                    required: true
                                }
                            };

                            try {
                                _.each(templateData, function (datum) {
                                    function failed(e) {
                                        problematicObject = datum.id;
                                        throw e;
                                    }

                                    var argument;
                                    try {
                                        argument = JSON.parse(datum.get("itemArgument"));
                                    } catch(e) {
                                        failed(badArgument);
                                    }

                                    switch(datum.get("itemType")) {
                                        case constants.convoItem.types.blrt:
                                        case constants.convoItem.types.reBlrt:
                                            api.ParamNode.initialise(argument, blrtArgumentStructure);

                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);

                                            argument = api.ParamNode.root.value;

                                            // We'll be doing the same check for normal and hidden items.
                                            _.each(["Items", "HiddenItems"], function (targetArray) {
                                                if(argument[targetArray]) {
                                                    _.each(argument[targetArray], function (item) {
                                                        // Skip templates!
                                                        if(item["MediaId"].lastIndexOf("_template", 0) === 0)
                                                            return;

                                                        if(!_.contains(mediaIds, item["MediaId"]))
                                                            failed(imaginaryMedia);
                                                    });
                                                }
                                            });

                                            break;
                                        case constants.convoItem.types.comment:
                                            api.ParamNode.initialise(argument, commentArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                        case constants.convoItem.types.eventAdd:
                                            api.ParamNode.initialise(argument, eventArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                        case constants.convoItem.types.eventMessage:
                                            api.ParamNode.initialise(argument, noteArgumentStructure);
                                            if(!api.ParamNode.root.isValid())
                                                failed(badArgument);
                                            break;
                                    }
                                });
                            } catch(e) {
                                if(e == badArgument || e == imaginaryMedia) {
                                    var msg = e + " - template object: " + problematicObject;

                                    l.log(msg);
                                    return Parse.Promise.error(msg);
                                }
                                throw e;
                            }
                        });

                return mediaPromise.then(function () {
                    // We'll also use this opportunity to count everything up for them!
                    var readableCount = 0;
                    var commentCount = 0;
                    var blrtCount = 0;
                    var eventCount = 0;
                    var noteCount = 0;
                    _.each(templateData, function (datum) {
                        switch(datum.get("itemType")) {
                            case constants.convoItem.types.blrt:
                            case constants.convoItem.types.reBlrt:
                                ++blrtCount;
                                ++readableCount;
                                break;
                            case constants.convoItem.types.comment:
                                ++commentCount;
                                ++readableCount;
                                break;
                            case constants.convoItem.types.eventAdd:
                                ++eventCount;
                                break;
                            case constants.convoItem.types.eventMessage:
                                ++noteCount;
                            case constants.convoItem.types.images:
                                ++readableCount;
                                break;
                        }
                    });

                    // This is how we return multiple values apparently...
                    return Parse.Promise.when(
                        Parse.Promise.as(templateData),
                        Parse.Promise.as(readableCount),
                        Parse.Promise.as(blrtCount),
                        Parse.Promise.as(commentCount),
                        Parse.Promise.as(eventCount),
                        Parse.Promise.as(noteCount)
                    );
                });
            });
         
            // ## Step 3: Check for existing template and fetch admin user
            return new Parse.Query(Parse.User)
                .equalTo("email", 'support@blrt.com') // support@blrt.com
                .first({useMasterKey:true})
                .then(function (adminUser) {
                    // Make sure the admin user exists - we can't continue without support.
                    console.log('\nadminUser email==='+adminUser.get('email'));
                    console.log(adminUser);
                    if(adminUser === null) return;
                    
                    let relationQueryPromise;

                    switch(arg) {
                        case 'welcomeConvo': 
                            relationQueryPromise = new Parse.Query(constants.convoUserRelation.keys.className)
                                .equalTo("user", user) 
                                .equalTo("convoTemplateSlug", 'welcome-jan15') // welcome convo
                                .equalTo('creator', adminUser)
                                .include("conversation")
                                .include('user')
                                .first({useMasterKey: true})
                                .then(relation=>{
                                    return new Parse.Query(constants.convoUserRelation.keys.className)
                                        .equalTo('conversation', relation.get('conversation'))
                                        .include("conversation")
                                        .include('user')
                                        .find({useMasterKey: true})
                                })
                            break;

                        case 'convoId':
                            info = _.decryptId(info);  // its encrypted convoId
                            console.log('\n\n===convoId==='+info);
                            const convoQuery = new Parse.Object(constants.convo.keys.className);
                            convoQuery.id = info;
                            console.log(convoQuery);
                            relationQueryPromise = new Parse.Query('ConversationUserRelation')
                                .equalTo('conversation', convoQuery)
                                .include("conversation")
                                .include('user')
                                .find({useMasterKey:true});
                            // console.log(relationQueryPromise);
                            break;

                        case 'convoTitle': // create new conversation...

                            break;
                    }

                    console.log('\n===query convo===');
                    return relationQueryPromise
                        .then(function (relations) {    
                            console.log('relations length==='+relations.length);

                            if(relations.length === 0) return {success: false, error: 'Admin user no found'};

                            let pushRecipients = []; 
                            let updateIndexViewedList = false;

                            // step3: update the ConversationUserRelation collection
                            relations = relations.map(function (relation) {
                                if(relation.get("user")) {
                                    // The creator gets some special treatment.
                                    if(relation.get("user").id == relation.get('creator').id && !relation.get("allowInInbox")) {
                                        relation.set("allowInInbox", true);
                                    }
                                    // console.log('user email==='+relation.get('user').get('email'));
                      
                                    if(!(relation.get('user') === adminUser)) { 
                                        // We'll also hold onto the user so we can send them a push notification.
                                        pushRecipients.push(relation.get("user")); // the parameter is an array 
                                    }else { // consider the case where the adminUser is in the conversation
                                        updateIndexViewedList = true;
                                    }
                                    if(relation.get("archived")) {
                                        relation.unset("archived");
                                    }
                                    // it will override the old one ,give cloud job hint to send email Only set
                                    // flaggedForCommentEmailAt if they're a user.
                                    relation.set("flaggedForCommentEmailAt", hourFromNow);

                                    // console.log('now==='+now);
                                    relation.set("contentUpdate", now);
                                }
                                return relation;
                            })
                            
                            console.log('\n\n===test==='); // this condition temperately is used as creating BON1 in the welcome-convo
                            return templateDataPromise.then(function (templateData, readableCount, blrtCount, commentCount, eventCount, noteCount) {             
                                let convo = relations[0].get('conversation'); // we only need to update in one conversation
                                let readablePos = convo.get('readableCount');
                                let generalPos = convo.get('contentCount');
                                console.log('welcomeConvo id==='+convo.id);   

                                let thePrefillName = user.get("hasnotSetDisplayname") ? "" : user.get("name");
                                let nameField = thePrefillName=="" ? "" : (" "+thePrefillName);
                                // console.log('nameField==='+nameField);

                                //step1: update the Conversation collection
                                convo.increment(constants.convo.keys.partialItemCreationCount)
                                    .increment(constants.convo.keys.generalCount, templateData.length)
                                    .increment(constants.convo.keys.readableCount, readableCount)
                                    .increment(constants.convo.keys.blrtCount, blrtCount)
                                    .increment(constants.convo.keys.commentCount, commentCount)
                                    .increment("eventCount", eventCount)
                                    .increment("noteCount", noteCount)
                                    .add("partialItemCreationTimes", expiration)
                                    .set(constants.convo.keys.ContentUpdate, now)

                                _.each(templateData, datum=>{
                                    switch(datum.get("itemType")) {
                                        case constants.convoItem.types.blrt:
                                        case constants.convoItem.types.reBlrt:
                                            convo.set(constants.convo.keys.LastMedia, datum.get("mediaObjects"))
                                                        .set(constants.convo.keys.LastMediaArgument, datum.get("itemArgument"))
                                            break;
                                    }
                                });

                                //step2: create templates in welcome conversation
                                const roleACL = new Parse.ACL();
                                roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                                roleACL.setRoleWriteAccess("Conversation-" + convo.id, true); 
                                

                                let mainPromise = [];
                                return Parse.Promise.when(templateData.map(datum=>{
                                    console.log('\n\n===item===');
                                    let item = new Parse.Object(constants.convoItem.keys.className) //ConversationItem
                                        .set(constants.base.keys.version, constants.convoItem.version)
                                        .set(constants.base.keys.creator, adminUser)
                                        .set(constants.base.keys.encryptType, constants.convoItem.encryptTypes.none)
                                        .set(constants.convoItem.keys.conversation, convo)
                                        .set(constants.convoItem.keys.Argument, Util.format(datum.get("itemArgument"), [nameField]))
                                        .set(constants.convoItem.keys.type, datum.get("itemType"))
                                        .set(constants.convoItem.keys.generalIndex, generalPos)        

                                    generalPos++;
                            
                                    if(convo.get("conversationRole") != null) 
                                        item.setACL(roleACL);        

                                    // consider the readable item
                                    let readable = _.contains(constants.convoItem.readableTypes, datum.get("itemType"));
                                    console.log('readable==='+readable);
                                    if(readable) {
                                        console.log('readablePos==='+readablePos);
                                        item.set(constants.convoItem.keys.readableIndex, readablePos) // reableIndex starts from 0
                                            .set(constants.convoItem.keys.psuedoId, convo.id + "-" + readablePos);
                                        
                                        // update indexViewedList in ConversationUserRelation
                                        console.log('\nupdateIndexViewedList==='+updateIndexViewedList);
                                        if(updateIndexViewedList) { // only for the case where the adminUser in the conversation
                                            relations = relations.map(realtion=>{
                                                console.log('===relation==='+relation.get("indexViewedList"));
                                                console.log(_.expand(relation.get("indexViewedList")))
                                                console.log(relation.get('user') === adminUser);
                                                if(!(existingList.indexOf(readablePos)>-1) && relation.get('user') === adminUser && relation.get('indexViewedList')) {
                                                    let existingList=_.expand(relation.get("indexViewedList"));
                                                    console.log('\nexistingList==='+existingList);
                                                    existingList.push(readablePos);    
                                                    relation.set("indexViewedList", _.contract(existingList.sort()));
                                                }
                                            });
                                        }
                                        
                                        console.log('===relation setting===');
                                        readablePos++;
                                    }else item.set(constants.convoItem.keys.readableIndex, -1);                                

                                    // consider the blrt
                                    switch(datum.get("itemType")) {
                                        case constants.convoItem.types.blrt:
                                        case constants.convoItem.types.reBlrt:
                                            item.set(constants.convoItem.keys.AudioFile, datum.get("audioFile"))
                                                .set(constants.convoItem.keys.TouchDataFile, datum.get("touchFile"))
                                                .set(constants.convoItem.keys.Media, datum.get("mediaObjects"))
                                                .set("thumbnailFile", datum.get("thumbnailFile"));
                                            
                                            // create blrt object, blrt created from template is decrypted file, so the encryptType is 0, and encrytValue = undefined 
                                            Blrt.createBlrt(0, undefined, datum.get("mediaObjects"), JSON.parse(datum.get("itemArgument")), datum.get("audioFile"), datum.get("touchFile"), datum.get("thumbnailFile"))
                                                .then(blrt=>{
                                                    item.set('blrt', blrt);
                                                })
                                            
                                            break;

                                        case constants.convoItem.types.images:
                                            console.log('\n===images test===');
                                            item.set("thumbnailFile", datum.get("thumbnailFile"))
                                                .set(constants.convoItem.keys.Media, datum.get("mediaObjects"))
                                            break;
                                    }   

                                    let relationsSavedPromise = [];
                                    relations.map(relation=>{
                                        relationsSavedPromise.push(relation.save(null, {useMasterKey: true}));
                                    });

                                    return Parse.Promise.when(convo.save(null, {useMasterKey: true}), item.save(null, {useMasterKey: true}), relationsSavedPromise)
                                        .then((convo, item, relations)=>{
                                            // * Send push notification - nobody will wait for this.
                                            muteJS.getMuteFlags(pushRecipients, convo.id)
                                                .then(function (pushRecipientMuteFlags) { 
                                                        const pushRecipientsShow = [];
                                                        const pushRecipientsHide = [];
                                                        _.each(pushRecipients, function (recipient) {
                                                            // console.log('recipient==='+JSON.stringify(recipient));
                                                            if(muteJS.shouldMute(pushRecipientMuteFlags[recipient.id], muteJS.MuteEnum.push)) {
                                                                pushRecipientsHide.push(recipient);
                                                            }else {
                                                                pushRecipientsShow.push(recipient);
                                                            }
                                                        });                                            

                                                        const emailRecipients = EmailJS.ParseUsersToEmails(pushRecipients, EmailJS.ALLOW_EMAIL_REBLRT, pushRecipientMuteFlags);
                                                        let messageShow;  
                                                        let messageHide = EmailJS.GenerateSilentPush({t: 1, bid: convo.id, s: 1});                                            

                                                        // console.log('pushRecipientMuteFlags==='+JSON.stringify(pushRecipientMuteFlags));        
                                                        switch(datum.get("itemType")) {
                                                            case constants.convoItem.types.blrt:
                                                            case constants.convoItem.types.reBlrt:
                                                                console.log('\n\nemail push===blrt===');
                                                                messageShow = EmailJS.GeneratePush("PB", {
                                                                    t: 1,
                                                                    bid: convo.id,
                                                                    s: 1
                                                                }, [
                                                                    adminUser.get("name"),
                                                                    convo.get(constants.convo.keys.Name)
                                                                ]);
                                                                console.log('\n\n\nblrt blrt blrt send email: '+JSON.stringify(item));
                                                                EmailJS.SendTemplateEmail({
                                                                    template: constants.mandrillTemplates.blrtReply,
                                                                    convo: convo,
                                                                    blrt: item,
                                                                    content: item,
                                                                    recipients: emailRecipients,
                                                                    sender: adminUser,
                                                                    creator: adminUser,
                                                                })
                                                                .fail(function (error) {
                                                                    l.log("ALERT: Error sending new Blrt Reply emails:", error)
                                                                     .despatch();
                                                                });
                                                                console.log('===send email done===');
                                                                break;
                                                            case constants.convoItem.types.comment:
                                                                console.log('\n\nemail push===comment===');
                                                                messageShow = EmailJS.GeneratePush("PC", {
                                                                    t: 1,
                                                                    bid: convo.id,
                                                                    s: 1
                                                                }, [
                                                                    adminUser.get("name"),
                                                                    convo.get(constants.convo.keys.Name)
                                                                ]);
                                                                break;
                                                            case constants.convoItem.types.images:
                                                                messageShow = EmailJS.GeneratePush("PMI", {
                                                                    t: 1,
                                                                    bid: convo.id,
                                                                    s: 1
                                                                }, [
                                                                    adminUser.get("name"),
                                                                    convo.get(constants.convo.keys.Name)
                                                                ]);
                                                                break;
                                                            case constants.convoItem.types.audio:
                                                                messageShow = EmailJS.GeneratePush("PAI", {
                                                                    t: 1,
                                                                    bid: convo.id,
                                                                    s: 1
                                                                }, [
                                                                    adminUser.get("name"),
                                                                    convo.get(constants.convo.keys.Name)
                                                                ]);
                                                                break;                                                                                   
                                                        }    
                                                        // console.log("\n\nmessage: "+JSON.stringify(message));                                                  

                                                        if(pushRecipientsShow.length > 0) {
                                                            console.log('\n\n===pushRecipientsShow===');
                                                            Parse.Push.send({
                                                                    where: new Parse.Query(Parse.Installation).containedIn("owner", pushRecipientsShow),
                                                                    //todo desire information can not work ,but something random can work
                                                                    data: messageShow
                                                                }, {useMasterKey: true})
                                                                .then(function (result) {
                                                                    // console.log('push notification==='+JSON.stringify(result));
                                                                })
                                                                .fail(function (error) {
                                                                    console
                                                                        .log("ALERT: Error sending unhidden push notifications:", error)
                                                                        .despatch();
                                                                });
                                                        }                                                                                                        

                                                        if(pushRecipientsHide.length > 0) {
                                                            Parse.Push.send({
                                                                    where: new Parse.Query(Parse.Installation).containedIn("owner", pushRecipientsHide),
                                                                    data: messageHide
                                                                }, {useMasterKey: true})
                                                                .then(function (result) {
                                                                    console.log(result)
                                                                })
                                                                .fail(function (error) {
                                                                    console
                                                                        .log("ALERT: Error sending new conversation item push notifications:", error)
                                                                        .despatch();
                                                                });
                                                        }  
                                                });
                                            return Parse.Promise.as({success: true});
                                        })
                                })).then(function(results) {
                                    console.log('\n\n\n===item after saved==='+JSON.stringify(results));
                                    console.log("\n\n===everything is correct===\n\n");
                                    l.log(true).despatch();
                                    return {success: true};
                                }, function (promiseError) {
                                    l.log(false, "Error when dequeueing slug or incrementing admin stats:", promiseError).despatch();
                                    return {success: false, error: promiseError};
                                });       
                            })
                        });
                }, function (promiseErrors) {
                    console.log('\n\n===promise error===');
                    l.log(false, "Failed to fetch clashing relations or admin user:", promiseErrors).despatch();
                    return {success: false, error: 'Failed to fetch relations'};
                });
    }
    
    // console.log('users length==='+users.length);
    if(_.isArray(users) && users.length >= 0) {
        return handleUser(0).fail(function (error) {
            console.log("ALERT: Caught something when handling a user.2..:" + error);
            return  {success: false, error: error};
        }); 
    }      
};

module.exports = sendTemplateToConvo;
