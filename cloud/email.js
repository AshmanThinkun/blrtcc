var btoa = require('cloud/btoa.js');
var constants = require('cloud/constants.js');
var Util = require('cloud/util.js');
var loggler = require("cloud/loggler.js");
var _ = require("underscore");

// var Mixpanel = require("cloud/mixpanel");
var trackBatch = require('cloud/analytics-node').trackBatch;
var NonUser = require("cloud/nonUser");
var blockJS = require("cloud/api/user/block");
var muteJS = require("cloud/api/conversationUserRelation/mute");

exports.ALLOW_EMAIL_NEW_BLRT = 1;
exports.ALLOW_EMAIL_REQUEST = 2;
exports.ALLOW_EMAIL_REBLRT = 3;
exports.ALLOW_EMAIL_COMMENTS = 4;

function missingIndexes(indexViewedList, maxIndex) {
    var results = {};
    for (var i = maxIndex; i >= 0; --i)
        results[i] = true;

    if (_.isEmpty(indexViewedList))
        return _.range(maxIndex + 1);

    var splits = indexViewedList.replace(/\s+/g, "").split(",");
    for (var i = splits.length - 1; i >= 0; --i) {
        var subset = splits[i];


        var pages = [];
        if (isNaN(subset)) {
            var pageSplit = subset.split("-");

            for (var j = pageSplit[0]; j <= pageSplit[1]; ++j)
                pages.push(j);
        } else
            pages.push(parseInt(subset));


        for (var j = pages.length - 1; j >= 0; --j) {
            var num = pages[j];


            if (typeof results[num] != "undefined")
                delete results[num];
        }
    }


    var temp = [];
    for (var psuedoId in results) {
        if (results.hasOwnProperty(psuedoId))
            temp.push(psuedoId);
    }
    results = temp;


    return results;
}


// var job=require('../index').job
Parse.Cloud.job("sendCommentNotifications", function (request, status) {
    var KCUR = constants.convoUserRelation.keys;
    var KCI = constants.convoItem.keys;
    var KCK = constants.convo.keys;
    var KUK = constants.user.keys;
    var KBK = constants.base.keys;

    var now = new Date();


    Parse.Cloud.useMasterKey();


    var emailPromises = [new Parse.Promise.as(null)];

    var users = {};

    //add parseUser obj to global
    function holdOntoUserObject(object) {
        if (typeof users[object.id] == "undefined")
            users[object.id] = object;
    }

    var convos = {};

    //add parse Conversation obj to global
    function holdOntoConvoObject(object) {
        if (typeof convos[object.id] == "undefined")
            convos[object.id] = object;
    }

    var convoItems = {};

    //add parse ConversationItem to global ,why need to psudoId ?
    function holdOntoConvoItemObjectBypsuedoId(psuedoId, object) {
        if (typeof convoItems[psuedoId] == "undefined")
            convoItems[psuedoId] = object;
    }

    var emails = {};

    function userWillGetEmail(userId, convoId) {
        if (typeof emails[convoId] == "undefined")
            emails[convoId] = {};

        if (typeof emails[convoId][userId] == "undefined")
            emails[convoId][userId] = [];
    }

    function userRequiresConvoItemWithpsuedoId(userId, psuedoId, convoId) {
        emails[convoId][userId].push(psuedoId);
    }


    function missingIndexes(indexViewedList, maxIndex) {
        var results = {};
        for (var i = maxIndex; i >= 0; --i)
            results[i] = true;

        if (_.isEmpty(indexViewedList))
            return _.range(maxIndex + 1);

        var splits = indexViewedList.replace(/\s+/g, "").split(",");
        for (var i = splits.length - 1; i >= 0; --i) {
            var subset = splits[i];


            var pages = [];
            if (isNaN(subset)) {
                var pageSplit = subset.split("-");

                for (var j = pageSplit[0]; j <= pageSplit[1]; ++j)
                    pages.push(j);
            } else
                pages.push(parseInt(subset));


            for (var j = pages.length - 1; j >= 0; --j) {
                var num = pages[j];


                if (typeof results[num] != "undefined")
                    delete results[num];
            }
        }


        var temp = [];
        for (var psuedoId in results) {
            if (results.hasOwnProperty(psuedoId))
                temp.push(psuedoId);
        }
        results = temp;


        return results;
    }


    //remove the flaggedForCommentEmailAt field so that cancel the email sending logic
    var dirtyConvoUserRelations = [];
    var convoItempsuedoIdsToFetch = {};


    var query = new Parse.Query(KCUR.className)
        .notEqualTo(KCUR.user, null)
        .notEqualTo(KCUR.flaggedForCommentEmailAt, null)
        .lessThanOrEqualTo(KCUR.flaggedForCommentEmailAt, now)
        .include(KCUR.user)
        .include(KCUR.conversation);

    //BLRT-1323, to ensure the userMasterKey to work, we set it to true explicitly
    return query.count(null, {useMasterKey: true})
        .then(function (convoUserRelationCount) {
            if (convoUserRelationCount == 0)
                return status.success("No flaggedForCommentEmailAt on any Relations");
            Parse.Cloud.useMasterKey();


            var convoUserRelationPromises = [];

            var fetchLimit = Math.floor(constants.maxQueryLimit / (2 + 1));
            for (var i = Math.floor(Math.min(constants.skipLimit - fetchLimit, convoUserRelationCount) / fetchLimit); i >= 0; --i) {
                var limitedQuery = new Parse.Query(KCUR.className)
                    .notEqualTo(KCUR.user, null)
                    .notEqualTo(KCUR.flaggedForCommentEmailAt, null)
                    .lessThanOrEqualTo(KCUR.flaggedForCommentEmailAt, now)
                    .limit(fetchLimit)
                    .skip(i * fetchLimit)
                    .include(KCUR.user)
                    .include(KCUR.conversation);


                //todo can work without "then" ?
                convoUserRelationPromises.push(limitedQuery.find(function (results) {
                    for (var j = results.length - 1; j >= 0; --j) {
                        var result = results[j];

                        if (result.get(KCUR.user) == null || result.get(KCUR.user).get(KUK.AllowEmailComments) != true) {
                            result.set(KCUR.flaggedForCommentEmailAt, null);
                            if (result.get(KCUR.user))
                                dirtyConvoUserRelations.push(result); //todo how dirty work ?

                            continue;
                        }

                        var userId = result.get(KCUR.user).id;
                        var convoId = result.get(KCUR.conversation).id;


                        var indexViewedList = result.get(KCUR.indexViewedList);
                        if (typeof indexViewedList == "undefined")
                            indexViewedList = "";

                        var indexes = missingIndexes(indexViewedList, result.get(KCUR.conversation).get(KCK.readableCount) - 1);

                        // var numComments = Math.min(indexes.length, result.get(KCUR.conversation).get(KCK.commentCount));
                        // if (convoItempsuedoIdsToFetch.length + numComments > constants.skipLimit) {
                        //     var tooManyContentItems = true;
                        //     break;
                        // }

                        holdOntoUserObject(result.get(KCUR.user));
                        holdOntoConvoObject(result.get(KCUR.conversation));
                        userWillGetEmail(userId, convoId);


                        for (var k = indexes.length - 1; k >= 0; k--) {
                            var psuedoId = convoId + constants.convoItem.psuedoIdSeparator + indexes[k].toString();

                            if (typeof convoItempsuedoIdsToFetch[psuedoId] == "undefined")
                                convoItempsuedoIdsToFetch[psuedoId] = true;

                            userRequiresConvoItemWithpsuedoId(userId, psuedoId, convoId);
                        }


                        result.set(KCUR.flaggedForCommentEmailAt, null);
                        dirtyConvoUserRelations.push(result);
                    }
                }));
            }


            return Parse.Promise.when(convoUserRelationPromises);
        }, function (error) {
            return status.error("Failed to count relations: " + JSON.stringify(error));
        })
        .then(function () {
            var temp = [];
            for (var psuedoId in convoItempsuedoIdsToFetch) {
                if (convoItempsuedoIdsToFetch.hasOwnProperty(psuedoId))
                    temp.push(psuedoId);
            }
            convoItempsuedoIdsToFetch = temp;


            var convoItemPromises = [];


            var fetchLimit = Math.floor(constants.maxQueryLimit / (1 + 1));
            for (var i = Math.floor(convoItempsuedoIdsToFetch.length / fetchLimit); i >= 0; --i) {
                var limitedQuery = new Parse.Query(KCI.className)
                    .equalTo(KCI.type, 3)
                    .containedIn(KCI.psuedoId, convoItempsuedoIdsToFetch)
                    .limit(fetchLimit)
                    .skip(i * fetchLimit)
                    .include(KBK.creator);


                convoItemPromises.push(limitedQuery.find(function (results) {
                    if (!results)
                        return;
                    for (var j = results.length - 1; j >= 0; --j) {
                        var result = results[j];


                        holdOntoConvoItemObjectBypsuedoId(result.get(KCI.psuedoId), result);
                    }
                }));
            }


            return Parse.Promise.when(convoItemPromises);
        }).then(function () {
            var despatched = 0;


            // This MUST be an _.each, because a native for loop doesn't enforce tight scoping.
            // The convo variable declared at the beginning was being re-used in each loop, simply
            // overriding the previous value. This caused delayed functions (i.e. promises) to
            // point at the wrong conversation because the loop continued and updated it.

            // Changed @2014/11/11T15:40 - never use native for loops.
            _.each(emails, function (anotherUseless, convoId) {
                var convo = convos[convoId];

                _.each(emails[convoId], function (uselessValue, userId) {
                    if (!emails[convoId].hasOwnProperty(userId))
                        return;
                    var user = users[userId];


                    var numComments = 0;

                    var authors = {};
                    var authorNames = [];
                    for (var i = emails[convoId][userId].length - 1; i >= 0; --i) {
                        if (typeof convoItems[emails[convoId][userId][i]] == "undefined")
                            continue;
                        var convoItem = convoItems[emails[convoId][userId][i]];

                        // We had to check if the creator was null in some rare/old cases where a user was
                        // deleted from the database and the pointer is therefore stuffed.
                        if (convoItem.get(KBK.creator) == null || convoItem.get(KBK.creator).id == userId)
                            continue;


                        if (typeof authors[convoItem.get(KBK.creator).id] == "undefined") {
                            authors[convoItem.get(KBK.creator).id] = true;
                            authorNames.push(convoItem.get(KBK.creator).get(KUK.FullName));
                        }


                        numComments++;
                    }

                    if (numComments == 0)
                        return;


                    var authorString = "";
                    for (var i = authorNames.length - 1; i >= 0; --i) {
                        if (i != authorNames.length - 1) {
                            if (i == 0)
                                authorString += " and ";
                            else
                                authorString += ", ";
                        }

                        authorString += authorNames[i];
                    }


                    emailPromises.push(
                        muteJS.getMuteFlags([user], convo.id)
                            .then(function (flags) {
                                var recipients = exports.ParseUsersToEmails([user], exports.ALLOW_EMAIL_COMMENTS, flags);
                                if (recipients.length > 0) {
                                    return SendTemplateEmail({
                                        template: constants.mandrillTemplates.AggregateComment,
                                        convo: convo,
                                        content: null,
                                        recipients: recipients,
                                        sender: user,
                                        creator: user,
                                        customMergeVars: {
                                            "BLRT_SENDER_LIST": authorString,
                                            "BLRT_NUM_COMMENTS": numComments
                                        }
                                    });
                                }
                            }, function (error) {
                                loggler.global.log(false, "Error when getting muteFlags:", error).despatch();
                                return error;
                            })
                    );

                    despatched++;
                });
            });


            return Parse.Object.saveAll(dirtyConvoUserRelations, {
                success: function () {
                    return Parse.Promise.when(emailPromises).then(function () {
                        return status.success("Despatched emails: " + despatched);
                    }, function (error) {
                        return status.error("Failed to send emails: " + JSON.stringify(error));
                    });
                },
                error: function (error) {
                    return status.error("Failed to save relations: " + JSON.stringify(error));
                }
            });
        });
});

//There are two main effort after performing this cloud job
//1. send email
//2. reset the flaggedForCommentEmailAt in conversationUserRelation ,so that when next time run this job ,the entry will be skipped
// Parse.Cloud.define("sendCommentNotifications", (request, status)=> {
//     const KCUR = constants.convoUserRelation.keys;
//     const KCI = constants.convoItem.keys;
//     const KCK = constants.convo.keys;
//     const KUK = constants.user.keys;
//     const KBK = constants.base.keys;
//
//
//     //All the information required for sending email will be included in the following variables : email,convoMap,convoItemMap
//     //and these variables will be built up across each steps (promise)
//
//     //The first is to get all the relations with a email
//     //Second is to fetch items based on the relations
//
//     // {
//     //    convoId:{
//     //        userId:[pseudoId]
//     //    }
//     // }
//     var email={}
//     // {
//     //     convoId:convoParseObj
//     // }
//     var convoMap={}
//     // {
//     //     psudoId:convoItemObj
//     // }
//     var convoItemMap={}
//
//     var dirtyConvoUserRelations = []; //We use this variable to keep track all the relations to reset emailFlag after sending email
//     var now = new Date();
//     new Parse.Query(KCUR.className)
//         .notEqualTo(KCUR.user, null)
//         .notEqualTo(KCUR.flaggedForCommentEmailAt, null)
//         .lessThanOrEqualTo(KCUR.flaggedForCommentEmailAt, now)
//         .include(KCUR.user)
//         .include(KCUR.conversation)
//         .count(null, {useMasterKey: true})
//         .then((convoUserRelationCount)=> {
//             if (convoUserRelationCount == 0) {
//                 return status.success("No flaggedForCommentEmailAt on any Relations");
//             }
//             const fetchLimit = Math.floor(constants.maxQueryLimit / 3);
//             var convoUserRelationPromises = []
//             for (var i = Math.floor(Math.min(constants.skipLimit - fetchLimit, convoUserRelationCount) / fetchLimit); i >= 0; --i) {
//                 var limitedQuery = new Parse.Query(KCUR.className)
//                     .notEqualTo(KCUR.user, null)
//                     .notEqualTo(KCUR.flaggedForCommentEmailAt, null)
//                     .lessThanOrEqualTo(KCUR.flaggedForCommentEmailAt, now)
//                     .limit(fetchLimit)
//                     .skip(i * fetchLimit)
//                     .include(KCUR.user)
//                     .include(KCUR.conversation);
//
//                 convoUserRelationPromises.push(limitedQuery.find())
//             }
//
//             return Parse.Promise.when(convoUserRelationPromises)
//         })
//         .then((relationsToLook)=> {
//             var convoItempsuedoIdsToFetch = {};
//             for (let i = relationsToLook.length; i > 0; i++) {
//                 let relation = relationsToLook[i];
//                 relation.set(KCUR.flaggedForCommentEmailAt, null);
//                 dirtyConvoUserRelations.push(relation);
//                 //The user disable emailNotification
//                 if (relation.get(KCUR.user).get(KUK.AllowEmailComments) != true) {
//                 }
//                 else {
//                     email[relation.id]=[]
//                     var convoId = relation.get(KCUR.conversation).id;
//                     var indexViewedList = relation.get(KCUR.indexViewedList);
//                     if (typeof indexViewedList == "undefined") {
//                         indexViewedList = "";
//                     }
//
//                     var indexes = missingIndexes(indexViewedList, relation.get(KCUR.conversation).get(KCK.readableCount) - 1);
//                     for (var k = indexes.length - 1; k >= 0; k--) {
//                         var psuedoId = convoId + constants.convoItem.psuedoIdSeparator + indexes[k].toString();
//                         if (typeof convoItempsuedoIdsToFetch[psuedoId] == "undefined")
//                             convoItempsuedoIdsToFetch[psuedoId] = true;
//                     }
//                 }
//             }
//             var convoItemPromises = [];
//             var fetchLimit = Math.floor(constants.maxQueryLimit / 2);
//             for (var i = Math.floor(convoItempsuedoIdsToFetch.length / fetchLimit); i >= 0; --i) {
//                 var limitedQuery = new Parse.Query(KCI.className)
//                     .equalTo(KCI.type, 3)
//                     .containedIn(KCI.psuedoId, convoItempsuedoIdsToFetch)
//                     .limit(fetchLimit)
//                     .skip(i * fetchLimit)
//                     .include(KBK.creator);
//                 convoItemPromises.push(limitedQuery.find())
//             }
//             return Parse.Promise.when(convoItemPromises)
//
//         })
//         .then((itemsToLook)=>{
//             var emailPromises = [new Parse.Promise.as(null)];
//         })
// })

//muteFlags param is required if we want the mute function is working 
//use this function to allow the email disable working 
exports.ParseUsersToEmails = function (parseUsers, type, muteFlags) {
    console.log('ParseUsersToEmails parseUsers: '+parseUsers.length);
    var stringArg = "";

    var toArray = new Array();

    switch (type) {
        case exports.ALLOW_EMAIL_NEW_BLRT:
            stringArg = "allowEmailNewBlrt";
            break;
        //can not find anywhere uses this template
        case exports.ALLOW_EMAIL_REQUEST:
            stringArg = "allowEmailRequest";
            break;
        case exports.ALLOW_EMAIL_REBLRT:
            stringArg = "allowEmailReBlrt";
            break;
        case exports.ALLOW_EMAIL_COMMENTS:
            stringArg = "allowEmailComments";
            break;
    }
    function pushToArray(parseUser) {
        toArray.push({
            id: parseUser.id,
            email: parseUser.get("email"),
            name: parseUser.get("name"),
            isUser: true,
            user: parseUser
        });
    }

    _.each(parseUsers, function (parseUser) {
        if (parseUser.get(stringArg)) {
            if (muteFlags) {
                var muteFlag = muteFlags[parseUser.id];
                if (!muteJS.shouldMute(muteFlag, muteJS.MuteEnum.email)) // muteJS.MuteEnum.email == 2
                    pushToArray(parseUser);
            } else {
                pushToArray(parseUser);
            }
        }
    });

    return toArray;
}

exports.NonUsersToEmails = function (users, type) {
    var stringArg = "";

    var toArray = new Array();


    for (var i = 0; i < parseUsers.length; ++i) {
        toArray[toArray.length] = {
            id: -1,
            email: users["email"],
            name: users["name"],
            isUser: false
        };
    }

    return toArray;
}

function RemoveUnwantedRecipients(request) {
    //this method will remove the deleted and suspended user, we can add more filter here in the furture
    var newRecipientsList = [];
    _.each(request.recipients, function (recipient) {
        if (!recipient.isUser || recipient.isUser && !recipient.user) {
            newRecipientsList.push(recipient);
        } else {
            //recipient that is a Blrt user
            //check deleted and suspended user
            var user = recipient.user;
            if (user.get("accountStatus") == "deleted" || user.get("accountStatus") == "suspended") {
                return;
            }

            //check if the sender in block list
            if (request.sender) {
                var userBlocked = _.find(user.get("blockedUsers"), function (blockedUser) {
                    return blockedUser.id == request.sender.id;
                });

                if (userBlocked) {
                    return;
                }
            }

            newRecipientsList.push(recipient);
        }
    });

    request.recipients = newRecipientsList;
    return request;
}


//request should include some ParseObject, this function will convert the ParseObject to merge_vars and and global_merge_vars
var SendTemplateEmail = function (request) {
    console.log('===email.js SendTemplateEmail==='+JSON.stringify(request));
    var l = loggler.create("sendTemplateEmail");
    var usersCount = request.recipients.length;

    //layer to filter out all the deleted and suspended user
    if (usersCount > 0) {
        request = RemoveUnwantedRecipients(request);
        usersCount = request.recipients.length;
    }

    if (usersCount < 1) {
        l.log("no email need to send").despatch();
        return new Parse.Promise.as();
    }

    var to = [];
    var merge_vars = [];
    //used for mandrill Search API
    var recipient_metadata = [];


    var shouldAddBlockLink;
    if (request.sender && (request.template == constants.mandrillTemplates["userNewConvo"]
        || request.template == constants.mandrillTemplates["userExistingConvo"])) {
        shouldAddBlockLink = true;
    }


    /*
     request = {
     request.template: 		result.get("emailTemplate"),
     blrt: 					classObj,
     content: 				contentObj,
     recipients: 			emailRecipients,
     sender: 				senderObj,
     creator: 				creatorObj,
     cc:                     cc target
     }

     recipients = {
     id,
     name,
     email,
     isUser,
     user
     }
     */
    for (var i = 0; i < usersCount; ++i) {

        // 'vars' property of merge_vars
        var emailVars = [
            {
                "name": "USER_NAME",
                "content": request.recipients[i].name,
            }, {
                "name": "USER_EMAIL",
                "content": request.recipients[i].email,
            }, {
                "name": "USER_ACTIVE",
                "content": request.recipients[i].isUser,
            }
        ];

        //used to construct url ,how yozio use this url ???
        var args = {
            t: request.template,
            m: request.recipients[i].isUser,
            r: request.recipients[i].id
        };

        if (request.sender != null)
            args["s"] = request.sender.id;

        if (request.convo != null)
            args.b = request.convo.id;


        for (var j = 1; j <= 8; ++j) {
            args.i = j;
            var encoded = encodeURIComponent(btoa(JSON.stringify(args)));

            if (request.convo || request.blrt) {
                //use yozio link
                var url = constants.yozioObj.click_url;
                if (request.blrt) {
                    url += constants.yozioObj.blrt_email;
                } else if (request.convo) {
                    url += constants.yozioObj.convo_email;
                }
                url += "?in=" + encoded + "&platform=cc";
                if (request.blrt) {
                    url += "&blrt=" + _.encryptId(request.blrt.id);
                    var type = "private";
                    if (request.blrt.get("isPublicBlrt"))
                        type = "public";
                    url += "&type=" + type;
                }
                if (request.convo) {
                    url += "&convo=" + _.encryptId(request.convo.id);
                }
                if (request.accessTokens && request.accessTokens[i]) {
                    url += "&tk=" + request.accessTokens[i];
                }
                if (request.sender) {
                    url += "&inviter=" + _.encryptId(request.sender.id);
                }
                if (request.nonUserId) {
                    url += "&nonUserId=" + request.nonUserId;
                }


                if (request.recipients[i].relation) url += "&rel=" + _.encryptId(request.recipients[i].relation.id);
                console.log('url url url==='+url);
                emailVars[emailVars.length] = {
                    "name": "BLRT_URL" + j,
                    "content": url
                };
            }


            var appStoreUrl = "http://" + constants.siteAddress + "/appstore/" + encoded;

            emailVars[emailVars.length] = {
                "name": "APP_STORE_URL" + j,
                "content": appStoreUrl
            };

        }

        var salt = parseInt(Math.random() * 10000);
        emailVars[emailVars.length] = {
            "name": "UNSUBSCRIBE_URL",
            "content": "http://" + constants.siteAddress + "/us?em=" + encodeURIComponent(request.recipients[i].email)
            + "&s=" + salt + "&key=" + encodeURIComponent(Util.irreversibleHash(request.recipients[i].email + salt))
        };
        if (constants.semDisabled)
            emailVars[emailVars.length] = {
                "name": "SEM_DISABLED",
                "content": "true"
            };
        else
            emailVars[emailVars.length] = {
                "name": "SEM_FORM_LINK",
                "content": exports.secondaryEmailLink(request.recipients[i].email)
            };

        emailVars.push({
            "name": "UPGRADE_URL",
            "content": exports.upgradeLink(request.recipients[i].email)
        });

        if (shouldAddBlockLink) {
            emailVars.push({
                "name": "BLOCK_LINK",
                "content": blockJS.getBlockUrl(request.recipients[i].id, request.sender.id)
            });
        }

        if (request.blrt) {
            emailVars.push({
                "name": "IMPRESSION_HTML",
                "content": "<img src='" + constants.yozioObj.impression_url + constants.yozioObj.blrt_email + "' width='1px' style='display:none'>"
            });
        } else if (request.convo) {
            emailVars.push({
                "name": "IMPRESSION_HTML",
                "content": "<img src='" + constants.yozioObj.impression_url + constants.yozioObj.convo_email + "' width='1px' style='display:none'>"
            });
        } else if (request.template == "user-request" || request.template == "non-user-request") {
            emailVars.push({
                "name": "IMPRESSION_HTML",
                "content": "<img src='" + constants.yozioObj.impression_url + constants.yozioObj.blrtrequest_web + "' width='1px' style='display:none'>"
            });
        }

        console.log('emailVars emailVars emailVars: '+JSON.stringify(emailVars));

        to.push({
            "email": request.recipients[i].email,
            "name": request.recipients[i].name,
            "type": "to"
        });

        merge_vars.push({
            "rcpt": request.recipients[i].email,
            "vars": emailVars
        });

        recipient_metadata.push({
            "rcpt": request.recipients[i].email,
            "vars": [
                {
                    "name": "user_id",
                    "content": request.recipients[i].id,
                }
            ]
        });

        //only send email to first user ???
        if (i == 0 && request.cc != null) {

            to.push({
                "email": request.cc.email,
                "name": request.cc.name,
                "type": "to"
            });

            merge_vars.push({
                "rcpt": request.cc,
                "vars": emailVars
            });

            recipient_metadata.push({
                "rcpt": request.cc.email,
                "vars": [
                    {
                        "name": "user_id",
                        "content": request.cc.id,
                    }
                ]
            });
        }

    }

    var thumbnailUrl = request.content && request.content.get("thumbnailFile")
        ? request.content.get("thumbnailFile").url()
        : (request.convo && request.convo.get("thumbnailFile") ? request.convo.get("thumbnailFile").url() : "");

    //[Guichi] convo can be created without blrt
    var duration = _.durationify(request.convo && request.convo.get("lastMediaArgument") ? JSON.parse(request.convo.get("lastMediaArgument")).AudioLength : "");

    var global_merge_vars = [
        {
            "name": "USER_ACTIVE",
            "content": true
        }, {
            "name": "BLRT_TITLE",
            "content": request.convo == null ? "" : request.convo.get("blrtName")
        }, {
            "name": "BLRT_THUMBNAIL",
            "content": thumbnailUrl
        }, {
            "name": "BLRT_DURATION",
            "content": duration
        }, {
            "name": "BLRT_SENDER",
            "content": request.sender == null ? "" : request.sender.get("name")
        }, {
            "name": "BLRT_SENDER_EMAIL",
            "content": request.sender == null ? "" : request.sender.get("email")
        }, {
            "name": "BLRT_CREATOR",
            "content": request.creator == null ? "" : request.creator.get("name")
        }, {
            "name": "BLRT_CREATOR_EMAIL",
            "content": request.creator == null ? "" : request.creator.get("email")
        }, {
            "name": "BLRT_IS_REQUEST",
            "content": request.convo == null ? false : request.convo.get("blrtState") != 3
        }, {
            "name": "CONTENT_IS_BLRT",
            "content": request.content == null ? false : request.content.get("contentType") == 0
        }, {
            "name": "CONTENT_IS_REBLRT",
            "content": request.content == null ? false : request.content.get("contentType") == 2
        }, {
            "name": "CONTENT_IS_COMMENT",
            "content": request.content == null ? false : request.content.get("contentType") == 3
        }, {
            "name": "CONTENT_TEXT",
            "content": request.content == null ? "" : request.content.get("textContents")
        }, {
            "name": "CURRENT_TIMESTAMP",
            "content": new Date()
        }
    ];


    if (typeof request.customMergeVars != "undefined") {
        for (var mergeVar in request.customMergeVars) {
            if (!request.customMergeVars.hasOwnProperty(mergeVar))
                continue;

            global_merge_vars.push({"name": mergeVar, "content": request.customMergeVars[mergeVar]});
        }
    }


    var jsonData = {
        "key": constants.mandrillKey,
        "template_name": request.template,
        "template_content": [],
        "message": {
            "to": to,
            "headers": {},
            "important": false,
            "track_opens": null,
            "track_clicks": null,
            "inline_css": null,
            "url_strip_qs": null,
            "preserve_recipients": false,
            "view_content_link": null,
            "tracking_domain": null,
            "signing_domain": null,
            "return_path_domain": null,
            "merge": true,
            "global_merge_vars": global_merge_vars,
            "merge_vars": merge_vars,
            "tags": [],
            "recipient_metadata": recipient_metadata,
        },
        "async": true,
    };
    
    console.log('jsonData: '+JSON.stringify(jsonData));

    // l.log("About to send email template: " + request.template, jsonData);

    // var logs = new Parse.Object("EmailSendingLog")
    //     .set("to", to)
    //     .set("template", request.template)
    //     .set("global_merge_vars", global_merge_vars)
    //     .set("merge_vars", merge_vars)
    //     .set("recipient_metadata", recipient_metadata)
    //     .save();

    var users = [];
    var nonUsers = [];
    _.each(request.recipients, function (recipient) {
        if (!recipient.isUser) nonUsers.push({type: "email", value: recipient.email.toLowerCase()});
        else users.push(recipient.email.toLowerCase());
    });

    // var mp = new Mixpanel({ batchMode: true });
    // var track = {
    //     event: "CC Email Received",
    //     properties: {
    //         "Email Template": request.template
    //     }
    // };

    return Parse.Promise.when(
        Parse.Cloud.httpRequest({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            url: 'https://mandrillapp.com/api/1.0/messages/send-template.json',
            body: jsonData
        }).then(function () {
            l.log(true, "Email thinks it's sent").despatch();
            return Parse.Promise.as("Email with template: " + request.template + " thinks it's sent");
        }, function (error) {
            l.log(false, "Email send failed:", error, " from request with body:", jsonData).despatch();
            return Parse.Promise.error("Email send failed: " + JSON.stringify(error));
        }),
        !_.isEmpty(users)
            ? new Parse.Query("UserContactDetails")
                .equalTo("type", "email")
                .containedIn("value", users)
                .find()
                .then(function (existingUserContactDetails) {
                    _.each(existingUserContactDetails, function (contact) {
                        // mp.track(track, { id: contact.get("user").id });
                        trackBatch.track({
                            userId: contact.get("user").id,
                            event: 'CC Email Received',
                            properties: {
                                "Email Template": request.template
                            }
                        })

                    });

                    // return ;
                    // mp.despatch().fail(function (error) {
                    //     l.log("ALERT: Error occured when Mixpanel tracking existing user CC email:", error);
                    // });
                })
            : Parse.Promise.as(),
        !_.isEmpty(nonUsers)
            ? NonUser.get(nonUsers)
                .then(function (nonUsers) {
                    _.each(nonUsers, function (nonUser) {
                        // mp.track(track, {id: "nonuser-" + nonUser.id});
                        trackBatch.track({
                            userId: "nonuser-" + nonUser.id,
                            event: 'CC Email Received',
                            properties: {
                                "Email Template": request.template
                            }
                        })
                    });

                    // return mp.despatch().fail(function (error) {
                    //     l.log("ALERT: Error occured when Mixpanel tracking NONUSER user CC email:", error);
                    // });
                })
            : Parse.Promise.as()
    );
}
exports.SendTemplateEmail = SendTemplateEmail;
/*
 request = {
 request.template: 		result.get("emailTemplate"),
 blrt: 					classObj,
 content: 				contentObj,
 recipients: 			emailRecipients,
 sender: 				senderObj,
 creator: 				creatorObj,
 cc:                     cc target
 }

 recipients = {
 id,
 name,
 email,
 isUser,
 user
 }
 */
Parse.Cloud.define("sendEmbedCode", function (req, res) {
    var params = req.params
    var request = {
        template: "email-embed-code",
        recipients: [req.params.recipient],
        customMergeVars: {
            BLRT_TITLE: params.title,
            BLRT_URL: params.url,
            EMBED_TITLE_EXTRA: params.extra,
            BLRT_EMBED_CODE: params.code,
            EMBED_CODE_TYPE: params.type,
        }
    }
    SendTemplateEmail(request).then(function (rst) {
        res.success(JSON.stringify(rst))
    }, function (err) {
        res.error(JSON.stringify(err))
    })

})

var sendTextEmail = function (from, recipients, subject, message) {
    Parse.Cloud.useMasterKey();
    var emailAddresses = _.pluck(recipients, "email");
    // We need to fetch the associated users and NonUsers so we can Mixpanel track them.
    return new Parse.Query("UserContactDetails")
        .equalTo("type", "email")
        .containedIn("value", emailAddresses)
        .include("user")
        .find()
        .then(function (contactDetails) {
            var users = _.map(contactDetails, function (detail) {
                return detail.get("user");
            });
            var foundEmails = _.map(contactDetails, function (detail) {
                return detail.get("value");
            });

            var notFound = _.without(emailAddresses, foundEmails);

            console.log("===== Not found===")
            console.log(JSON.stringify(notFound))

            return Parse.Cloud.httpRequest({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                url: 'https://mandrillapp.com/api/1.0/messages/send.json',
                body: {
                    "key": constants.mandrillKey,
                    "message": {
                        "from_email": from.email,
                        "from_name": from.name,
                        "to": recipients,
                        "subject": subject,
                        "text": message
                    },
                    "async": true,
                }
            })
        }, function (e) {
            console.log(JSON.stringify(e))
        });
}
exports.sendTextEmail = sendTextEmail;

exports.SendPush = function (message, toUsers, after) {
    var theData = message;
    console.log('\n\n\n\n===theData theData theData: '+JSON.stringify(theData));
    var pushQuery = new Parse.Query(Parse.Installation);
    pushQuery.containedIn('owner', toUsers);

    return Parse.Push.send({
        where: pushQuery,
        data: theData
    }, after);
}

exports.GenerateSilentPush = function (data) {
    
    data['content-available'] = 1;

    if (constants.isOpensourceServer) {
        try {
            return data; //JSON.parse(data);
        } catch (ejps) {
            return null;
        }
    }

    return data;
};

exports.GeneratePush = function (messageString, data, varibles) {
    var output = null;
    exports.GenerateSilentPush(messageString, data, varibles);

    while (output == null || output.length > 250) {
        if (output != null && varibles != null && varibles.length > 0) {
            varibles[varibles.length - 1] = varibles[varibles.length - 1].substring(0, output.length - 250 - 3) + "...";
        }

        /* var obj = {
         "alert": {
         "loc-key": messageString,
         "loc-args": [userName, blrtName],
         "action-loc-key": messageString + "B"
         },
         "badge": 	"Increment",
         "bid":		blrtId
         }; */

        output = Util.format('{"alert":{"loc-key":"{0}",{1}"action-loc-key":"{0}B"},"badge":"Increment","sound":"default","content-available":1',
            [messageString, varibles != null ? '"loc-args":' + JSON.stringify(varibles) + ',' : ""]
        );


        for (key in data) {
            output += Util.format(',"{0}":"{1}"', [key, data[key]]);
        }
        output += "}";
    }

    if (constants.isOpensourceServer) {
        try {
            return JSON.parse(output);
        } catch (ejps) {
            return null;
        }
    }

    return output;
};

exports.StandardizePushMessage = function (msg) {
    if ("string" == typeof msg) {
        if (constants.isOpensourceServer) {
            try {
                return JSON.parse(msg);
            } catch (e1) {
                return null;
            }
        } else {
            return msg;
        }
    } else if ("object" == typeof msg) {
        if (constants.isOpensourceServer) {
            return msg;
        } else {
            return JSON.stringify(msg);
        }
    } else {
        return msg;
    }
};


/**
 * Context: user wishes to link a secondary email address to an existing Blrt account
 *
 * Creates a secondary email form link if `userObjectId` is unspecified (so the user can sign in to an account),
 * otherwise creates a secondary email verification link from `secondary` to the user denoted by `userObjectId`.
 */
exports.secondaryEmailLink = function (secondary, userObjectId) {
    return constants.protocol + constants.siteAddress + "/" + (
            !userObjectId // If a userObjectId is provided, we're creating a verification link instead of a form link
                ? "nsem?" + _.hashedQueryArgs(["sem", "s"], [secondary, _.salt()])
                : "vsem?" + _.hashedQueryArgs(["sem", "uoid", "s"], [secondary, _.encryptId(userObjectId), _.salt()])
        );
};

/**
 * Generates a link for the user to the upgrade page using their email address.
 *
 * If on the live stream, sends them to www.blrt.com instead of m.blrt.co.
 */
exports.upgradeLink = function (email) {
    return (constants.isLive ? constants.blrtWebsiteUrl : constants.protocol + constants.siteAddress)
        + "/upgrade?em=" + encodeURIComponent(btoa(email));
}


//CONV_HAS_BLRT
//FIRST_BLRT_BY_SENDER

const serverConfig= require('../config/serverConfig')
// localhost:8081/notifyPromoVideoConversionFinished?blrtId=1234&url=www.google.com
function nonUserEmailSending(nonUser, sender, conversation, customMergeVars) {
    console.log('===nonUserEmailSending===');
    console.log('nonUser id==='+nonUser.nonUserId+' nonUser: '+JSON.stringify(nonUser));
    let sendNonUserEmailPromise = Parse.Promise.as();
    // if(customMergeVars.blrt) { // link in received email to e server landing page
    //     sendNonUserEmailPromise = SendTemplateEmail({
    //         template:       constants.mandrillTemplates.blrtReply,
    //         convo:          conversation,
    //         blrt:           customMergeVars.blrt,
    //         nonUserId:      nonUser.nonUserId,
    //         content:        customMergeVars.blrt,
    //         recipients:     [nonUser],
    //         sender:         sender,
    //         creator:        conversation.get("creator"),
    //         customMergeVars: customMergeVars,
    //     })
    // }else { // link in received email to web app
        sendNonUserEmailPromise = SendTemplateEmail({
            template:       constants.mandrillTemplates.nonUserNewConvo,
            convo:          conversation,
            nonUserId:      nonUser.nonUserId,
            content:        null,
            recipients:     [nonUser],
            sender:         sender,
            creator:        conversation.get("creator"),
            customMergeVars: customMergeVars,
        })
    // }
    sendNonUserEmailPromise.then(function (r) {
        console.log("show me email result : ")
        console.log(JSON.stringify(r))
    }).fail(function (e) {
        console.log(JSON.stringify(e))
    })

}

exports.nonUserEmailSending = nonUserEmailSending;
