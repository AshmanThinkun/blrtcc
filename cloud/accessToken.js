var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var _ = require("underscore");

Parse.Cloud.define('accessWithToken', function(req, res) {
	if (("string" == typeof (req.params.token)) && ("string" == typeof (req.params.convoId))) {
		var token = req.params.token.trim();
		if (token.length == constants.accessToken.tokenLength) {
			Parse.Cloud.useMasterKey();
			var convo = new Parse.Object(constants.convo.keys.className);
			convo.id = req.params.convoId;
			return new Parse.Query(constants.accessToken.keys.className)
				.equalTo(constants.accessToken.keys.token, token)
				.equalTo(constants.accessToken.keys.conversation, convo)
				.equalTo(constants.accessToken.keys.isEnabled, true)
                .greaterThan(constants.accessToken.keys.accessCountdown, 0)
				.first()
				.then(
					function(result) {
						var succeed = false;
						var waitingPromise = [];
						if (result) {
							succeed = true;
							result.increment(constants.accessToken.keys.accessCountdown, -1);
							waitingPromise.push(result.save());
						}
						Parse.Promise.when(waitingPromise)
							.then(
								function(results) {
									return res.success({ success: succeed });
								}, 
								function(errors) {
									console.log("accessWithToken error:" + JSON.stringify(errors));
									return res.success({ success: false, error: "error2" });
								}
							);
					},
					function(error) {
						return res.success({ success: false, error: "error1"});
					}
				);
		}
	}
	return res.success({ success: false, error: "param"});
});


Parse.Cloud.define('getPhoneUserAccessToken', function(req, res) {
	if (req && req.user && ("string" == typeof (req.params.convoId))) {
		if (_.isArray(req.params.phoneNumbers) && (req.params.phoneNumbers.length > 0)) {
			var convo = new Parse.Object(constants.convo.keys.className);
			convo.id = req.params.convoId;
	        return new Parse.Query(constants.convoUserRelation.keys.className)
	        	.equalTo(constants.convoUserRelation.keys.conversation, convo)
                .equalTo(constants.convoUserRelation.keys.QueryType, "phoneNumber")
                .containedIn(constants.convoUserRelation.keys.QueryValue, req.params.phoneNumbers)
                .find()
                .then(
                	function(results) {
                		if (results.length > 0) {
                			var totalPlayableCount = constants.convoItem.maxPrivateViewCounter + results.length - 1;
                			Parse.Cloud.useMasterKey();
                			return new Parse.Query(constants.accessToken.keys.className)
                				.equalTo(constants.accessToken.keys.conversation, convo)
                				.equalTo(constants.accessToken.keys.isEnabled, true)
                				.greaterThan(constants.accessToken.keys.accessCountdown, 0)
                				.containsAll(constants.accessToken.keys.group, req.params.phoneNumbers)
                				.first()
                				.then(
                					function(result) {
                						if (result) {
                							return res.success(JSON.stringify({ success: true, token: result.get(constants.accessToken.keys.token)}));
                						} else {
                							var token = _.randomString(constants.accessToken.tokenLength);
                							var object = new Parse.Object(constants.accessToken.keys.className)
								                .set(constants.accessToken.keys.token, token)
								                .set(constants.accessToken.keys.conversation, convo)
								                .set(constants.accessToken.keys.accessCountdown, totalPlayableCount)
								                .set(constants.accessToken.keys.isEnabled, true)
								                .set(constants.accessToken.keys.group, req.params.phoneNumbers);
								            return object.save().then(function() {
								            	return res.success(JSON.stringify({ success: true, token: token}));
								            });
                						}
                					}, 
                					function(error) {
                						return res.success(JSON.stringify({ success: false, error: "error when finding tokens"}));
                					}
                				);
                		} else {
                			return res.success(JSON.stringify({ success: false, error: "relations not found"}));
                		}
                	}, 
                	function(error) {
                		return res.success(JSON.stringify({ success: false, error: "error when finding relations"}));
                	}
                );
		}
	}
	return res.success(JSON.stringify({ success: false, error: "param"}));
});
