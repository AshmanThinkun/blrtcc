var maintenance = require("cloud/maintenance.js");
var constants = require("cloud/constants.js");
var loggler = require("cloud/loggler.js");
var _ = require("underscore");

var k_b = constants.base.keys;
var k_cur = constants.convoUserRelation.keys;

// this file keeps all the old migration jobs, please comment out your one after you process it. Oherwise, people might process it again by mistake that will destroy all the data.


/*
Parse.Cloud.job("migratePublicBlrtThumbnail", function (req, res) {
    Parse.Cloud.useMasterKey();
    var counter = 0;
    var updated = 0;
    return new Parse.Query("ConversationItem")
        .equalTo("isPublicBlrt", true)
        .equalTo("thumbnailFile", null)
        .include("conversation")
        .each(function (cur) {
            counter++; 
            return cur.set("thumbnailFile", cur.get("conversation").get("thumbnailFile")).save().then(function(){return updated++});
        }).then(function () {
            return res.success("Relations checked: " + counter + "; migrated: " + updated);
        });
});
*/

/*
Parse.Cloud.job("migrateToMixpanel", function (req, res) {
    var Mixpanel = require("cloud/mixpanel");
    Parse.Cloud.useMasterKey();

    var mixpanel = new Mixpanel({
        batchMode: true,
        batchLimit: 20
    });

    var now = new Date();

    var promises = [];

    var accountTypeTrailData = {};

    return Parse.Promise.when(
        new Parse.Query(Parse.User).each(function (user) {
            var accountType = user.get("activeAccountTypeId");
            mixpanel.updateUser(user, { $ignore_time: true });
            mixpanel.engage({
                $set: {
                    "Migrated": true,
                    "Migrated At": now
                },
                $time: user.get("lastActive") || user.get("settingsLastUpdated") || user.updatedAt
            }, { id: user.id });
            mixpanel.track("Migrated", { id: user.id });
            mixpanel.track({
                event: "Registered",
                properties: {
                    time: user.createdAt
                }
            }, { id: user.id });
        }),
        new Parse.Query("NonUser").include("user").each(function (nonUser) {
            var viaSignup = nonUser.get("user") ? (
                Math.abs(nonUser.get("claimed") - nonUser.get("user").createdAt) < 5000
            ) : undefined;

            var properties = {
                $set: {
                    "Migrated": true,
                    "Migrated At": now,
                    "Claimed": Boolean(nonUser.get("claimed")),
                    "Claimed At": nonUser.get("claimedAt"),
                    "Claimed By": nonUser.get("user") ? nonUser.get("user").id : undefined,
                    "Claimed Via Signup": viaSignup,
                    $created: nonUser.createdAt,
                    $email: nonUser.get("type") == "email" ? nonUser.get("value") : "",
                    "Facebook ID": nonUser.get("type") == "facebook" ? nonUser.get("value") : "",
                    "Is NonUser": true,
                    $name: "NonUser: " + nonUser.get("value"),
                    "NonUser Type": nonUser.get("type"),
                    "NonUser Value": nonUser.get("value"),
                    "Uses Facebook": Boolean(nonUser.get("type") == "facebook")
                }
            };

            if(nonUser.get("claimed")) {
                properties.$time = nonUser.get("claimedAt");

                mixpanel.track("Claimed By User", { id: "nonuser-" + nonUser.id });
                mixpanel.track({
                    event: "NonUser Claimed",
                    properties: {
                        "NonUser ID": "nonuser-" + nonUser.id,
                        "Via Signup": viaSignup
                    }
                }, { id: nonUser.get("user").id });
            }

            mixpanel.engage(properties, { id: "nonuser-" + nonUser.id, updateLastSeen: Boolean(nonUser.get("claimed")) });
            mixpanel.track("Migrated", { id: "nonuser-" + nonUser.id });
        }),
        new Parse.Query("ConversationItem")
            .notEqualTo("type", 0)
            .each(function (convoItem) {
                if(!convoItem.get("creator")) return

                var blrtLength = null;
                try {
                    var arg = JSON.parse(convoItem.get("argument"));
                    blrtLength = parseInt(arg["AudioLength"]);
                } catch(err) { }

                var commentLength = null;
                try {
                    var arg = JSON.parse(convoItem.get("argument"));
                    commentLength = arg["Comment"].length;
                } catch(err) { }

                var generalProperties = {
                    time: convoItem.createdAt,
                    "From Device": convoItem.get("device"),
                    "From OS": convoItem.get("os"),
                    "From OS Version": convoItem.get("osVersion"),
                    "From Blrt Version": convoItem.get("appVersion"),
                    "Migrated": true
                };

                switch(convoItem.get("type")) {
                    case 2: // Blrt Reply
                        mixpanel.track({
                            event: "Blrt Reply Created",
                            properties: _.extend(generalProperties, {
                                "Blrt Length": blrtLength
                            })
                        }, { id: convoItem.get("creator").id });

                        break;
                    case 3: // Comment
                        mixpanel.track({
                            event: "Comment Created",
                            properties: _.extend(generalProperties, {
                                "Length": commentLength
                            })
                        }, { id: convoItem.get("creator").id });

                        break;
                }
            }),
        new Parse.Query("ConversationItem")
            .equalTo("type", 0)
            .count()
            .then(function (convoItemCount) {
                if(convoItemCount == 0) return;

                var convoItemPromises = [];

                var fetchLimit = Math.floor(constants.maxQueryLimit / (1 + 1));
                for(var i = Math.floor(Math.min(constants.skipLimit - fetchLimit, convoItemCount) / fetchLimit); i >= 0; --i) {
                    convoItemPromises.push(
                        new Parse.Query("ConversationItem")
                            .equalTo("type", 0)
                            .include("conversation")
                            .limit(fetchLimit)
                            .skip(i * fetchLimit)
                            .find()
                            .then(function (convoItems) {
                                _.each(convoItems, function (convoItem) {
                                    var convo = convoItem.get("conversation");
                                    if(!convoItem.get("creator") || !convo) return;

                                    var blrtLength = null;
                                    try {
                                        var arg = JSON.parse(convoItem.get("argument"));
                                        blrtLength = parseInt(arg["AudioLength"]);
                                    } catch(err) { }

                                    mixpanel.track({
                                        event: "Conversation Created",
                                        properties: {
                                            time: convoItem.createdAt,
                                            "Title": convo.get("blrtName"),
                                            "Title Length": convo.get("blrtName") ? convo.get("blrtName").length : null,
                                            "Blrt Length": blrtLength,
                                            "From Device": convoItem.get("device"),
                                            "From OS": convoItem.get("os"),
                                            "From OS Version": convoItem.get("osVersion"),
                                            "From Blrt Version": convoItem.get("appVersion"),
                                            "Migrated": true
                                        }
                                    }, { id: convoItem.get("creator").id });

                                });
                            })
                    );
                }

                return Parse.Promise.when(convoItemPromises);
            })
    ).then(function () {
        return mixpanel.despatch().then(function () {
            return res.success();
        });
    }).fail(function (error) {
        return res.error(JSON.stringify(error));
    });
});

Parse.Cloud.job("migratePublicBlrtCount", function (req, res) {
    Parse.Cloud.useMasterKey();
    var counter = 0;
    var updated = 0;
    var toSave = [];
    return new Parse.Query("ConversationItem")
        .equalTo("isPublicBlrt", true)
        .include("creator")
        .each(function (item) {
            var creator = item.get("creator");
            creator.increment("publicBlrtCount");
            toSave.push(creator);
            return true;
        }).then(function () {
            return Parse.Object.saveAll(toSave).then(function () {
                return res.success("Successfully migrated " + toSave.length + " convo items");
            });
        });
});
*/

/*
Parse.Cloud.job("migrateHasBegun", function (req, res) {
    Parse.Cloud.useMasterKey();
    var counter = 0;
    var updated = 0;
    return new Parse.Query(constants.convoUserRelation.keys.className)
        .notEqualTo(constants.convoUserRelation.keys.allowInInbox, true)
        .include(constants.convoUserRelation.keys.conversation)
        .each(function (cur) {
            counter++;
            if(
                cur.get(constants.convoUserRelation.keys.QueryType) != "_creator" ||
                (
                    cur.get(constants.convoUserRelation.keys.conversation) != null &&
                    cur.get(constants.convoUserRelation.keys.conversation).get(constants.convo.keys.HasConversationBegan)
                )
            ) {
                updated++;
                return cur.set(constants.convoUserRelation.keys.allowInInbox, true).save();
            }
        }).then(function () {
            return res.success("Relations checked: " + counter + "; migrated: " + updated);
        });
});

/*
Parse.Cloud.job("migrateConvoCreator", function (req, res) {
	var l = loggler.create("job:migrateConvoCreator");

	Parse.Cloud.useMasterKey();

	var query = new Parse.Query(k_cur.className);
	l.log("Count query:", query);

    return query.count()
        .then(function(convoUserRelationCount) {
            if(convoUserRelationCount == 0) {
            	l.log(true, "Apparently there are no convoUserRelations :P easy migration").despatch();
                return res.success("Apparently there are no convoUserRelations :P easy migration");
            }
			l.log("Count:", convoUserRelationCount);

            var promises = [Parse.Promise.as()];
            var objects = [];

            var fetchLimit = Math.floor(constants.maxQueryLimit / (1 + 1));
            for(var query_i = Math.floor(Math.min(constants.skipLimit - fetchLimit, convoUserRelationCount) / fetchLimit); query_i >= 0; --query_i) {
                var limitedQuery = new Parse.Query(k_cur.className)
                    .limit(fetchLimit)
                    .skip(query_i * fetchLimit)
                    .equalTo(k_cur.convoCreator, null)
                    .include(k_cur.conversation);

                l.log("limitedQuery(" + query_i + "):", limitedQuery);

                promises.push(limitedQuery.find(function (convoUserRelations) {
                	l.log("limitedQuery results:", convoUserRelations);

                	for(var cur_i = convoUserRelations.length - 1; cur_i >= 0; --cur_i) {
                		var convoUserRelation = convoUserRelations[cur_i];

                		convoUserRelation.set(
                			k_cur.convoCreator,
                			convoUserRelation.get(k_cur.conversation).get(k_b.creator)
                		);

                		objects.push(convoUserRelation);
                	}
                }, function (error) {
                	l.log("limitedQuery.find() error:", error);
                	return Parse.Promse.error("limitedQuery.find() error: " + JSON.stringify(error));
                }));
            }

            return Parse.Promise.when(promises).then(function () {
            	l.log("Saving objects");

            	return Parse.Object.saveAll(objects).then(function () {

            		l.log(true, "Successfully migrated " + objects.length + " relations").despatch();
            		return res.success("Successfully migrated " + objects.length + " relations");
            		
            	}, function (error) {
            		l.log(false, "Parse.Object.saveAll(objects) error:", error).despatch();
            		return res.error("Parse.Object.saveAll(objects) error: " + JSON.stringify(error));
            	});
            }, function (error) {
        		l.log(false, "Parse.Promise.when(promises) error:", error).despatch();
        		return res.error("Parse.Promise.when(promises) error: " + JSON.stringify(error));
            });
        }, function (error) {
    		l.log(false, "query.count() error:", error).despatch();
    		return res.error("query.count() error: " + JSON.stringify(error));
        });
});

/*
Parse.Cloud.job("migrateConvo5to6", function (request, response) {
	if(!maintenance.isMaintenanceMode ())
		response.error ("Can only migrate with maintenance mode enabled");

	Parse.Cloud.useMasterKey();

    function indexes(indexViewedList) {
    	var results = [];

    	var splits = indexViewedList.replace(/\s+/g, "").split(",");
    	for (var i = splits.length - 1; i >= 0; --i) {
    		var subset = splits[i];

    		var pages = [];
    		if(isNaN(subset)) {
    			var pageSplit = subset.split("-");

    			for (var j = pageSplit[0]; j <= pageSplit[1]; ++j)
    				pages.push(j);
    		} else
				pages.push(parseInt(subset));

			for (var j = pages.length - 1; j >= 0; --j) {
				var num = pages[j];

				results.push(num);
			}
    	}

        return results;
    }

	var bk   = constants.base.keys;
	var ck   = constants.convo.keys;
	var cik  = constants.convoItem.keys;
	var curk = constants.convoUserRelation.keys;

	var objectsToSave = [];

	var convoQuery = new Parse.Query(ck.className)
		.equalTo(bk.version, 5);

	if(request.params.ids != null)
		convoQuery = convoQuery.containedIn(bk.objectId, request.params.ids);

	return convoQuery.each(function (convo) {
			var itemIndexTransitions = { };

			var generalCount = 0;
			var readableCount = 0;
			var commentCount = 0;

			return convo.relation(ck.items)
				.query()
				.ascending(bk.createdAt)

				.find(function (items) {
				for(var item_i = 0; item_i < items.length; ++item_i) {
					var item = items[item_i];
					var type = item.get(cik.type);


					item.set(cik.generalIndex, generalCount);
					generalCount++;

					if(constants.convoItem.readableTypes.indexOf(type) != -1) {
						var oldIndex = item.get(cik.generalIndex);
						if(typeof itemIndexTransitions[oldIndex] == "undefined")
							itemIndexTransitions[oldIndex] = [];
						itemIndexTransitions[oldIndex].push(readableCount);

    					item.set(cik.readableIndex, readableCount);
    					item.set(cik.psuedoId, convo.id + constants.convoItem.psuedoIdSeparator + readableCount.toString());
						readableCount++;
					} else {
    					item.set(cik.readableIndex, -1);
    					item.set(cik.psuedoId, null);
					}
					
					if(type == constants.convoItem.types.comment) commentCount++;

					item.set(bk.version, 6);
					objectsToSave.push(item);
				}
			}).then(function () {
				return convo.relation(ck.userRelations)
					.query()

					.find(function (relations) {
						for(var ur_i = 0; ur_i < relations.length; ++ur_i) {
							var relation = relations[ur_i];

							if(relation.get(curk.indexViewedList) != null) {
								var newIndexList = [];

								var indexList = indexes(relation.get(curk.indexViewedList));
								for(var index_i = 0; index_i < indexList.length; ++index_i) {
									var index = indexList[index_i].toString();

									if(typeof itemIndexTransitions[index] != "undefined")
										newIndexList.push.apply(newIndexList, itemIndexTransitions[index]);
								}

								relation.set(curk.indexViewedList, newIndexList.join());
							}

							relation.set(bk.version, 6);
							objectsToSave.push(relation);
						}
					}).then(function () {
						convo.set(ck.generalCount, generalCount);
						convo.set(ck.readableCount, readableCount);
						if(commentCount) convo.set(ck.commentCount, commentCount);

						convo.set(bk.version, 6);
						objectsToSave.push(convo);
					});
			});
		}).then(function () {
			return Parse.Object.saveAll(objectsToSave, {
				success: function (saved) {
					return response.success("Objects migrated: " + saved.length);
				},
				error: function (error) {
					return response.error("Error saving all objects: " + JSON.stringify(error));
				}
			});
		});
});
*/

/*
Parse.Cloud.job("migrateConversationUserRelation", function (req, res) {
    Parse.Cloud.useMasterKey();

    var relsToSave = 0;

    return new Parse.Query("ConversationUserRelation")
    .include("conversation")
    .containedIn("contentUpdate", [false,null])
    .each(function(rel){
        rel.set("contentUpdate", rel.get("conversation").get("contentUpdate"));
        if(!rel.get("conversation").get("contentUpdate")){
            rel.set("contentUpdate", rel.get("conversation").createdAt);
        }
        relsToSave++;
        console.log(rel.id + rel.get("contentUpdate"));
        return rel.save();
    }).then(function(){
        return res.success(relsToSave + " objects saved");
    },function(error){
        return res.error(JSON.stringify(error));
    });
});
*/


/*
Parse.Cloud.job("migrateIncorrectAccountTypeLog", function (request, response) {
    var l = loggler.create("job:parseAccountTypeLogs");
    Parse.Cloud.useMasterKey();


    var logQuery = new Parse.Query(constants.accountTypeLog.keys.className)
        .equalTo(constants.accountTypeLog.keys.Parsed, null)
        .include(constants.accountTypeLog.keys.DestinationAccountType);
    l.log("logQuery:", logQuery);

    var users = [];
    var userLogs = {};
    var userTrails = {};

    var youngestLog = Number.MAX_VALUE;

    var modifiedLogs = [];
    var modifiedTrails = [];

    var logs = [];
    return logQuery.each(function(log){
            return logs.push(log);
        }).then(function () {
        if(_.isEmpty(logs)) {
            l.log(true, "No logs to parse").despatch();
            return response.success("No logs to parse");
        }


        for(var log_i = logs.length - 1; log_i >= 0; --log_i) {
            var log = logs[log_i];

            var user = log.get(constants.accountTypeLog.keys.user);

            if(!(user.id in userLogs)) {
                users.push(user);
                userLogs[user.id] = [];
                userTrails[user.id] = [];
            }


            var consistentCreatedAt = log.get(constants.accountTypeLog.keys.consistentCreatedAt);
            if(youngestLog > consistentCreatedAt)
                youngestLog = consistentCreatedAt;


            log.set(constants.accountTypeLog.keys.Parsed, true);
            userLogs[user.id].push(log);
            modifiedLogs.push(log);
        }

        l.log("logs:", logs).log("userLogs:", userLogs);
    }).then(function() {
        var trailQuery = Parse.Query.or(
            new Parse.Query(constants.accountTypeTrail.keys.className)
                .containedIn(constants.accountTypeTrail.keys.user, users)
                .greaterThanOrEqualTo(constants.accountTypeTrail.keys.Expire, youngestLog),
            new Parse.Query(constants.accountTypeTrail.keys.className)
                .containedIn(constants.accountTypeTrail.keys.user, users)
                .equalTo(constants.accountTypeTrail.keys.Expire, null)
        ).include(constants.accountTypeTrail.keys.AccountType);
        l.log("trailQuery:", trailQuery);

        var trails = [];
        return trailQuery.each(function(trail){
            return trails.push(trail);
        }).then(function () {
            for(var trail_i = trails.length - 1; trail_i >= 0; --trail_i) {
                var trailItem = trails[trail_i];

                var user = trailItem.get(constants.accountTypeTrail.keys.user);

                if(trailItem.get(constants.accountTypeTrail.keys.Expire) == null ||
                    trailItem.get(constants.accountTypeTrail.keys.Expire) >= userLogs[user.id][0].get(constants.accountTypeLog.keys.consistentCreatedAt)
                )
                    userTrails[trailItem.get(constants.accountTypeTrail.keys.user).id].push(trailItem);
            }

            l.log("trails:", trails).log("userTrails:", userTrails);
        }).then(function() {
            for(var user_i = users.length - 1; user_i >= 0; --user_i) {
                var user = users[user_i];


                var logs = userLogs[user.id];
                var trails = userTrails[user.id];

                // The order of parsing the logs is essential to maintaining correctness
                logs.sort(function (a, b) {
                    return a.get(constants.accountTypeLog.keys.consistentCreatedAt) - b.get(constants.accountTypeLog.keys.consistentCreatedAt);
                });

                // The trail list is required for when delaying trails (purchased a weaker account)
                trails.sort(function (a, b) {
                    return a.get(constants.accountTypeTrail.keys.Start) - b.get(constants.accountTypeTrail.keys.Start);
                });


                var trailStack = [];

                if(trails.length == 0) {
                    // If no trails exist then this is the first log for the user, which should be for the free account type, indefinitely
                    var firstTrail = new Parse.Object(constants.accountTypeTrail.keys.className);

                    firstTrail.set(constants.accountTypeTrail.keys.user, user);
                    firstTrail.set(constants.accountTypeTrail.keys.AccountType, logs[0].get(constants.accountTypeLog.keys.DestinationAccountType));
                    firstTrail.set(constants.accountTypeTrail.keys.Start, logs[0].get(constants.accountTypeLog.keys.consistentCreatedAt));

                    trailStack.push(firstTrail);

                    // This new trail will be saved after the first log is parsed
                    // This is because the first log is guaranteed to be the "free" log (which is created when the user signs up)
                    // This is the case because the only way the user would have no existing trails is if none of their logs were ever parsed
                }

                for(var log_i = 0; log_i < logs.length; ++log_i) {
                    var log = logs[log_i];

                    // Build the trail stack to see which trails apply based on the time for this log
                    if(log_i == 0) {
                        _.each(trails, function (trail) {
                            if(trail.get(constants.accountTypeTrail.keys.Expire) == null
                                || trail.get(constants.accountTypeTrail.keys.Expire) >= log.get(constants.accountTypeLog.keys.consistentCreatedAt)
                            )
                                trailStack.push(trail);
                        });
                    } else {
                        while(trailStack[trailStack.length - 1].get(constants.accountTypeTrail.keys.Expire) != null
                            && trailStack[trailStack.length - 1].get(constants.accountTypeTrail.keys.Expire) < log.get(constants.accountTypeLog.keys.consistentCreatedAt)
                        )
                            trailStack.pop();
                    }

                    var strongestAccountTypeTrail = trailStack[trailStack.length - 1];


                    var strongestAccountType = strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.AccountType);

                    var strongestAccountTypeThreshold = Number.MAX_VALUE;
                    for (var j = strongestAccountType.get(constants.accountType.keys.Threshold).length - 1; j >= 0; --j)
                        strongestAccountTypeThreshold = Math.min(strongestAccountTypeThreshold, strongestAccountType.get(constants.accountType.keys.Threshold)[j]);

                    var logAccountType = log.get(constants.accountTypeLog.keys.DestinationAccountType);

                    var threshold = -Number.MAX_VALUE;
                    for (var k = logAccountType.get(constants.accountType.keys.Threshold).length - 1; k >= 0; --k)
                        threshold = Math.max(threshold, logAccountType.get(constants.accountType.keys.Threshold)[k]);

                    var start = log.get(constants.accountTypeLog.keys.consistentCreatedAt);
                    var duration = null;

                    if(log.get(constants.accountTypeLog.keys.customDuration) != null) {
                        if(log.get(constants.accountTypeLog.keys.customDuration) != 0)
                            duration = log.get(constants.accountTypeLog.keys.customDuration);
                    } else
                        duration = logAccountType.get(constants.accountType.keys.Duration);

                    if(logAccountType.id == strongestAccountType.id) {
                        if(duration != null) {
                            var expiration = strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.Expire);
                            if(expiration == null)
                                expiration = log.get(constants.accountTypeLog.keys.consistentCreatedAt);

                            strongestAccountTypeTrail.set(constants.accountTypeTrail.keys.Expire, new Date(expiration * 1 + constants.hour * 24 * duration));
                        } else
                            strongestAccountTypeTrail.set(constants.accountTypeTrail.keys.Expire, null);


                        modifiedTrails.push(strongestAccountTypeTrail);

                    } else if(threshold >= strongestAccountTypeThreshold) {
                        var expiration = null;

                        // Otherwise activate the currentAccountType now
                        if(duration != null) {
                            if(strongestAccountTypeThreshold == constants.accountType.accountThreshold.Trial
                                && threshold == constants.accountType.accountThreshold.Premium) {
                                // If you're currently on trial and upgrading to premium then add the remaining duration to the subscription
                                expiration = new Date(strongestAccountTypeTrail.get(constants.accountTypeTrail.keys.Expire, expiration) * 1 + constants.hour * 24 * duration);
                            } else
                                expiration = new Date(start * 1 + constants.hour * 24 * duration);
                        }


                        var newTrail = new Parse.Object(constants.accountTypeTrail.keys.className);
                        newTrail.set(constants.accountTypeTrail.keys.user, user);
                        newTrail.set(constants.accountTypeTrail.keys.AccountType, logAccountType);
                        newTrail.set(constants.accountTypeTrail.keys.Start, start);
                        
                        if(expiration != null)
                            newTrail.set(constants.accountTypeTrail.keys.Expire, expiration);

                        strongestAccountType = logAccountType;

                        strongestAccountTypeThreshold = Number.MAX_VALUE;
                        for (var k = strongestAccountType.get(constants.accountType.keys.Threshold).length - 1; k >= 0; --k)
                            strongestAccountTypeThreshold = Math.min(strongestAccountTypeThreshold, strongestAccountType.get(constants.accountType.keys.Threshold)[k]);

                        trailStack.push(newTrail);

                        modifiedTrails.push(newTrail);

                    }else if (threshold<strongestAccountTypeThreshold && threshold == 0){
                        var existFree;
                        _.each(trails, function(theTrail){
                            if(theTrail.get("accountTypeId")==logAccountType)
                                existFree = theTrail;
                        });

                        if(existFree){
                            existFree.set("startDate", start);
                        }else{
                            var existFree = new Parse.Object(constants.accountTypeTrail.keys.className);
                            existFree.set(constants.accountTypeTrail.keys.user, user);
                            existFree.set(constants.accountTypeTrail.keys.AccountType, logAccountType);
                            existFree.set(constants.accountTypeTrail.keys.Start, start);
                        }

                        modifiedTrails.push(existFree);
                    }else{
                        var index = modifiedLogs.indexOf(log);
                        if(index == -1)
                            response.error("I tried my best :" + log.id );
                        modifiedLogs.remove(index);
                        l.log(false, "Attempted to downgrade? Log:", log).despatch();
                    }
                }
            }

            return Parse.Object.saveAll(modifiedLogs.concat(modifiedTrails)).then(function() {
                l.log(true, "Parsed " + modifiedLogs.length + " logs").despatch();
                return response.success("Parsed logs: " + modifiedLogs.length);
            }, function(error) {
                l.log(false, "saveAll(modifiedLogs.concat(modifiedTrails)) error:", error).despatch();
                return response.error("Log/Trail saveAll error: " + JSON.stringify(error));
            });
        }, function (error) {
            l.log(false, "trailQuery.find() error:", error).despatch();
            return response.error("trailQuery.find() error: " + JSON.stringify(error));
        });
    }, function (error) {
        l.log(false, "logQuery.find() error:", error).despatch();
        return response.error("logQuery.find() error: " + JSON.stringify(error));
    });
});
*/

// If a convo is sent to a non-user, after the non-user register, he / she will need to call his / her "findPendingRelation" and "getSettings".
// Due to some bugs, we have not ensured that.
// The following migration job will fix the problems.
// Parse.Cloud.job("migrateFixUninitializedNewUsers", function (request, status) {
//     Parse.Cloud.useMasterKey();


//     new Parse.Query(Parse.User)
//     //find All user who do have a settingsLastUpdate field which means the register the app but never use the mobile app
//         .doesNotExist("settingsLastUpdated")
//         .find()
//         .then(function (users) {
//             var sessionTokens = []
//             users.forEach(function (user) {
//                 console.log(user.get('username')+" : "+user.getSessionToken())
//                 var sessionToken = user.getSessionToken()
//                 sessionTokens.push(sessionToken)
//             })
//             var initPromise = new Parse.Promise.as(null);
//             return sessionTokens.reduce(function (prePromise, token) {
//                 return prePromise.then(function () {

//                     return Parse.Cloud.httpRequest({
//                             method: "POST",
//                             //not sure what will this cloud function does
//                             url: "https://api.parse.com/1/functions/findPendingRelation",
//                             headers: {
//                                 'X-Parse-Application-Id': Parse.applicationId,
//                                 "X-Parse-REST-API-Key": constants.RESTApiKey,
//                                 'X-Parse-Session-Token': token,
//                                 'Content-Type': "application/json"
//                             },
//                         })

//                         //call cloud function getSettings which will set settingsLastUpdate the date it execute
//                         .then(function (penddingResult) {
//                             console.log("penddingResult " + JSON.stringify(penddingResult))
//                             return Parse.Cloud.httpRequest({
//                                 method: "POST",
//                                 //call cloud function getSettings which will set settingsLastUpdate the date it execute
//                                 url: "https://api.parse.com/1/functions/getSettings",
//                                 headers: {
//                                     'X-Parse-Application-Id': Parse.applicationId,
//                                     "X-Parse-REST-API-Key": constants.RESTApiKey,
//                                     'X-Parse-Session-Token': token,
//                                     'Content-Type': "application/json"
//                                 },
//                                 body: JSON.stringify({
//                                     device: {
//                                         appName: "ChromeWebPlayer",
//                                         browser: "Chrome",
//                                         browserVersion: "47.0.2526.111",
//                                         device: "unknown",
//                                         os: "Mac OS",
//                                         osVersion: "10.10.5",
//                                         version: "1.0.8"
//                                     }
//                                 })
//                             })
//                         })
//                 })


//             }, initPromise)


//         })
//         .then(function () {
//             status.success("Cloud Job Success")
//         })
//         .fail(function () {
//             status.error("Unexpected error");
//         })

// });


// Parse.Cloud.job("migrateSetUserHasUsedMobileApp", function (req, res) {
//     Parse.Cloud.useMasterKey();
//     var toSave = [];
//     var usingIOS = new Parse.Query(Parse.User).equalTo("lastOS", "iPhone OS");
//     var usingAndroid = new Parse.Query(Parse.User).equalTo("lastOS", "Android");
//     return Parse.Query.or(usingIOS, usingAndroid)
//         .each(function (user) {
//             user.set("hasUsedMobileApp", true);
//             toSave.push(user);
//             return true;
//         }).then(function () {
//             return Parse.Object.saveAll(toSave).then(function () {
//                 return res.success("Successfully migrated " + toSave.length + " users");
//             });
//         });
// });
