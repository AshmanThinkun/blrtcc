var emailJS = require("cloud/email");
var loggler = require("cloud/loggler");
var constants = require("cloud/constants");
var NonUser = require("cloud/nonUser");
var _ = require("underscore");
var accountTypeJS = require("cloud/accounttype.js");

exports.process = function (action) {
    var context = new Context(action);
    if( action.get("type") == "PioneerUpgradeFailure"){
        return context.upgrade();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    Context.prototype.upgrade = function () {
        var action = this.action;
        var l = this.l;

        var pioneerUser = action.get("user");
        var data = action.get("data");
        return Parse.Promise.as().then(function(){
            if(pioneerUser.get("activeInvitee") < data.activeInvitee){
                pioneerUser.increment("activeInvitee");
            }

            var activeInvitee = pioneerUser.get("activeInvitee");
            var timesOfPioneerUpgrade = pioneerUser.get("timesOfPioneerUpgrade");
            activeInvitee = activeInvitee || 0;
            timesOfPioneerUpgrade = timesOfPioneerUpgrade || 0;

            if(activeInvitee < timesOfPioneerUpgrade*5 && activeInvitee!=0 ){
                return true;
            }

            var failurePromises = [];
            var failureTimelyAction;
            
            //add another timely action in case failure
            failurePromises.push(
                new Parse.Object("TimelyAction")
                    .set("type", "PioneerUpgradeFailure")
                    .set("user", pioneerUser)
                    .set("data", {activeInvitee: activeInvitee})
                    .set("executionTime", new Date(new Date().getTime() + 60000))
                    .save()
                    .then(function(result){
                        return failureTimelyAction = result;
                    },function(error){
                        l.log("ALERT: fail to save PioneerUpgradeFailure timely action").despatch();
                        return Parse.Promise.error("Error save PioneerUpgradeFailure: " + JSON.stringify(error));
                    })
            );

            return Parse.Promise.when(failurePromises).then(function(){
                return pioneerUser
                    .save()
                    .then(function(){
                        var duration = 180;
                        if(activeInvitee==50)
                            duration = null;

                        return accountTypeJS.requestAccountUpgrade({
                            user: null,
                            params: {
                                requestFor: constants.accountType.RequestFor.admin,
                                data: {
                                    userEmail: pioneerUser.get("email"),
                                    destination: "premium-annual",
                                    duration: duration,
                                    invitedByPioneer: false
                                },
                                template: "premium-started"
                            }
                        }).then(function(){
                            l.log("Upgrade pioneer success " + pioneerUser.get("email"));
                            pioneerUser.increment("timesOfPioneerUpgrade");
                            return pioneerUser.save().then(function(){
                                //delete timelyAction
                                return failureTimelyAction.destroy()
                                    .fail(function(error){
                                        l.log("ALERT: fail to delete failureTimelyAction", failureTimelyAction).despatch();
                                        return Parse.Promise.error("Error delete failureTimelyAction: " + JSON.stringify(error));
                                    });
                            },function(error){
                                l.log("ALERT: fail to save PioneerUser", pioneerUser).despatch();
                                return Parse.Promise.error("Error save PioneerUser: " + JSON.stringify(error));
                            }).then(function(){
                                return emailJS.SendTemplateEmail({
                                    template:   constants.mandrillTemplates.pioneerUpgradeCompleteAdmin,
                                    recipients: [{
                                                name: constants.pioneerAdmin.name,
                                                email: constants.pioneerAdmin.email,
                                                isUser: true
                                    }],
                                    sender:     null,
                                    creator:    null,
                                    customMergeVars: {
                                                "PIONEER_NAME": pioneerUser.get("name"),
                                                "PIONEER_EMAIL": pioneerUser.get("email"),
                                                "UPGRADE_DURATION": duration? "6 months":"life time",
                                                "ACTIVE_INVITEE_NUM":  activeInvitee
                                    }
                                }).fail(function(error){
                                    l.log("ALERT: Error send email to pioneerAdmin").despatch();
                                    return Parse.Promise.error("Error send email to pioneerAdmin: " + JSON.stringify(error));
                                });
                            });
                        },function(error){
                            l.log(false, "Error upgrading the pioneer:", error).despatch();
                            return Parse.Promise.error("Error upgrade pioneer: " + JSON.stringify(pioneerUser));
                        });
                    },function(error){
                        l.log("ALERT: fail to save PioneerUser", pioneerUser).despatch();
                        return Parse.Promise.error("Error save PioneerUser: " + JSON.stringify(error));
                    });
            });
        });
    }
    
    return Context;
})();