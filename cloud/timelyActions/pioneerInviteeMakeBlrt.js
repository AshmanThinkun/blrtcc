var emailJS = require("cloud/email");
var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");
var constants = require("cloud/constants");
// var NonUser = require("cloud/nonUser");
var _ = require("underscore");
var accountTypeJS = require("cloud/accounttype.js");

exports.process = function (action) {
    var context = new Context(action);
    if( action.get("type") == "PioneerInviteeMakeBlrt"){
        return context.upgrade();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    Context.prototype.upgrade = function () {
        var action = this.action;
        var l = this.l;

        var blrtUser = action.get("user");
        var relation = action.get("relation");
        return Parse.Promise.as().then(function(){
            if(relation.get("claimedPremium")){
                l.log(false, "Failure claimedPremium.").despatch();
                return false;
            }

            //upgrade the invitee
            return accountTypeJS.requestAccountUpgrade({
                user: null,
                params: {
                    requestFor: constants.accountType.RequestFor.admin,
                    data: {
                        userEmail: blrtUser.get("email"),
                        destination: "premium-annual",
                        duration: 180,
                        invitedByPioneer: true
                    },
                    template: "premium-started"
                }
            }).then(function (status) {
                var errorMsg;
                if(!status.success){
                    l.log(false, "Failure upgrade user: ",status).despatch();
                    errorMsg = status.message;
                }
                
                //send notification
                return new Parse.Query(Parse.User)
                    .equalTo("objectId", relation.get("pioneerId"))
                    .first()
                    .then(function(pioneerUser){
                        //increase pioneer activeInvitee
                        pioneerUser.increment("activeInvitee")
                        var activeInvitee = pioneerUser.get("activeInvitee");
                        var timesOfPioneerUpgrade = pioneerUser.get("timesOfPioneerUpgrade");
                        activeInvitee = activeInvitee? activeInvitee: 0;
                        timesOfPioneerUpgrade = timesOfPioneerUpgrade? timesOfPioneerUpgrade: 0;

                        var initialPromises = [];
                        var failureTimelyAction;
                        
                        //add another timely action in case failure
                        if(activeInvitee == timesOfPioneerUpgrade * 5 && activeInvitee != 0) {
                            initialPromises.push(
                                new Parse.Object("TimelyAction")
                                    .set("type", "PioneerUpgradeFailure")
                                    .set("user", pioneerUser)
                                    .set("data", {activeInvitee: activeInvitee})
                                    .set("executionTime", new Date(new Date().getTime() + 60000))
                                    .save()
                                    .then(function(result){
                                        return failureTimelyAction = result;
                                    },function(error){
                                        l.log("ALERT: fail to save PioneerUpgradeFailure timely action").despatch();
                                        return Parse.Promise.error("Error save PioneerUpgradeFailure: " + JSON.stringify(error));
                                    })
                            );
                        }

                        // var mp = new Mixpanel({ batchMode: true });
                        // mp.track("Pioneer Invitee Upgrade Received", { id: blrtUser.id });
                        // mp.track({
                        //     event: "Pioneer Invited User Contributed",
                        //     properties: {
                        //         "User ID": blrtUser.id
                        //     }
                        // }, { id: pioneerUser.id });
                        // initialPromises.push(mp.despatch());

                        return Parse.Promise.when(initialPromises).then(function(){
                            return pioneerUser
                                .save()
                                .then(function(pioneerUser){
                                    var promises = [];
                                    activeInvitee = pioneerUser.get("activeInvitee")||0;
                                    if(activeInvitee == timesOfPioneerUpgrade*5 && activeInvitee!=0 ){
                                        var duration = 180;
                                        var template = constants.mandrillTemplates.premiumRenewedPioneer5;
                                        if(activeInvitee==50){
                                            duration = null;
                                            template = constants.mandrillTemplates.premiumRenewedPioneer50;
                                        }

                                        promises.push(accountTypeJS.requestAccountUpgrade({
                                            user: null,
                                            params: {
                                                requestFor: constants.accountType.RequestFor.admin,
                                                data: {
                                                    userEmail: pioneerUser.get("email"),
                                                    destination: "premium-annual",
                                                    duration: duration,
                                                    invitedByPioneer: false
                                                },
                                                template: template
                                            }
                                        }).then(function(){
                                            l.log("Upgrade pioneer success " + pioneerUser.get("email"));
                                            pioneerUser.increment("timesOfPioneerUpgrade");
                                            return pioneerUser.save().then(function(){
                                                //delete timelyAction
                                                return failureTimelyAction.destroy()
                                                    .fail(function(error){
                                                        l.log("ALERT: fail to delete failureTimelyAction", failureTimelyAction).despatch();
                                                        return Parse.Promise.error("Error delete failureTimelyAction: " + JSON.stringify(error));
                                                    });
                                            },function(error){
                                                l.log("ALERT: fail to save PioneerUser", pioneerUser).despatch();
                                                return Parse.Promise.error("Error save PioneerUser: " + JSON.stringify(error));
                                            }).then(function(){
                                                return emailJS.SendTemplateEmail({
                                                    template:   constants.mandrillTemplates.pioneerUpgradeCompleteAdmin,
                                                    recipients: [{
                                                                name: constants.pioneerAdmin.name,
                                                                email: constants.pioneerAdmin.email,
                                                                isUser: true
                                                    }],
                                                    sender:     null,
                                                    creator:    null,
                                                    customMergeVars: {
                                                                "PIONEER_NAME": pioneerUser.get("name"),
                                                                "PIONEER_EMAIL": pioneerUser.get("email"),
                                                                "UPGRADE_DURATION": duration? "6 months":"life time",
                                                                "ACTIVE_INVITEE_NUM":  activeInvitee
                                                    }
                                                }).fail(function(error){
                                                    l.log("ALERT: Error send email to pioneerAdmin").despatch();
                                                    return Parse.Promise.error("Error send email to pioneerAdmin: " + JSON.stringify(error));
                                                });
                                            });
                                        },function(error){
                                            l.log("ALERT: Error upgrading the pioneer:", error).despatch();
                                            return Parse.Promise.error("Error upgrade pioneer: " + JSON.stringify(pioneerUser));
                                        }));
                                    }else{
                                        if(failureTimelyAction){
                                            promises.push(
                                                failureTimelyAction.destroy()
                                                    .fail(function(error){
                                                        l.log("ALERT: fail to delete failureTimelyAction", failureTimelyAction).despatch();
                                                        return Parse.Promise.error("Error delete failureTimelyAction: " + JSON.stringify(error));
                                                    })
                                            );
                                        }
                                    }


                                    return Parse.Promise.when(promises).then(function(){

                                        return Parse.Promise.when([
                                            emailJS.SendTemplateEmail({
                                                template:   constants.mandrillTemplates.pioneerInviteeComplete,
                                                recipients: [{
                                                            name: pioneerUser.get("name"),
                                                            email: pioneerUser.get("email"),
                                                            isUser: true,
                                                            user: pioneerUser
                                                }],
                                                sender:     null,
                                                creator:    null,
                                                customMergeVars: {
                                                            "INVITEE_NAME": blrtUser.get("name"),
                                                            "INVITEE_EMAIL": blrtUser.get("email"),
                                                }
                                            }).fail(function(error){
                                                l.log("ALERT: Error send email to pioneer").despatch();
                                                return Parse.Promise.error("Error send email to pioneer: " + JSON.stringify(error));
                                            }),
                                            emailJS.SendTemplateEmail({
                                                template:   constants.mandrillTemplates.pioneerUpgradeRequest,
                                                recipients: [{
                                                            name: constants.pioneerAdmin.name,
                                                            email: constants.pioneerAdmin.email,
                                                            isUser: true
                                                }],
                                                sender:     null,
                                                creator:    null,
                                                customMergeVars: {
                                                            "INVITEE_NAME": blrtUser.get("name"),
                                                            "INVITEE_EMAIL": blrtUser.get("email"),
                                                            "PIONEER_NAME": pioneerUser.get("name"),
                                                            "PIONEER_EMAIL": pioneerUser.get("email"),
                                                            "UPGRADE_ERROR": errorMsg,
                                                }
                                            }).fail(function(error){
                                                l.log("ALERT: Error send email to pioneerAdmin").despatch();
                                                return Parse.Promise.error("Error send email to pioneerAdmin: " + JSON.stringify(error));
                                            })
                                        ]);
                                    });
                                },function(error){
                                    l.log("ALERT: fail to save PioneerUser", pioneerUser).despatch();
                                    return Parse.Promise.error("Error save PioneerUser: " + JSON.stringify(error));
                                });

                            });
                }).then(function(){
                    //saveObjectAsNotified
                    return relation.fetch().then(function(relation){
                        relation.set("notifiedAt", new Date());
                        return relation.save().then(function(){
                            l.log("pioneerInviteeMakeBlrt timely action complete.").despatch();
                            return true;
                        });
                    });
                },function(error){
                    l.log(false, "Error sending emails for upgrade pioneer invitee:", error).despatch();
                    return Parse.Promise.error("Error sending emails for upgrade pioneer invitee: " + JSON.stringify(error));
                }); 

            }, function (error) {
                l.log(false, "Failure upgrade user: ", blrtUser).despatch();
                return Parse.Promise.error("Error upgrade user: " + JSON.stringify(blrtUser));
            });
        });
    }
    
    return Context;
})();