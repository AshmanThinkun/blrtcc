var emailJS = require("cloud/email");
var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");
var constants = require("cloud/constants");
var NonUser = require("cloud/nonUser");
var _ = require("underscore");

exports.process = function (action) {
    var context = new Context(action);
    if( action.get("type") == "PioneerTimelyAction"){
        return context.sendEmail();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    var template = "non-user-invited-reminder";
    Context.prototype.sendEmail = function () {
        var action = this.action;
        var l = this.l;
        var data = action.get("data");

        var blrtUser;

        return new Parse.Query("PioneerInvitationRelation")
            .equalTo("pioneerId", data.pioneerId)
            .equalTo("email", data.email)
            .first()
            .then(function (relationRecord) {
                if(!relationRecord) {
                    l.log(false, "Error finding PioneerInvitationRelation:", promiseError).despatch();
                    return Parse.Promise.error("Error finding PioneerInvitationRelation");
                }
               
                l.log("Found PioneerInvitationRelation");
                //get nonuser's relating user

                return new Parse.Query("NonUser")
                    .equalTo("type", "email")
                    .equalTo("value", data.email)
                    .equalTo("claimed", true)
                    .include("user")
                    .first()
                    .then(function(theuser){
                        blrtUser = theuser.get("user");

                        return relationRecord
                            .set("userSignedUp", true)
                            .set("user", blrtUser)
                            .save();
                    });
            }).then(function () {
                return new Parse.Query(Parse.User)
                    .equalTo("objectId", data.pioneerId)
                    .first()
                    .then(function (pioneer) {
                        blrtUser.set("invitedByPioneer", pioneer);

                        return blrtUser.save().then(function () {
                            // send email to the pioneer who invited the non-user
                            return Parse.Promise.when([
                                emailJS.SendTemplateEmail({
                                    template:   constants.mandrillTemplates.pioneerInviteeSignup,
                                    recipients: [{
                                                name: pioneer.get("name"),
                                                email: pioneer.get("email"),
                                                isUser: true,
                                                user: pioneer
                                    }],
                                    sender:     null,
                                    creator:    null,
                                    customMergeVars: {
                                                "INVITEE_NAME": data.email,
                                    }
                                }).fail(function (error) {
                                    l.log("ALERT: Failed to send pioneerInviteeSignup email:", error).despatch();
                                }),
                                // new Mixpanel({ id: pioneer.id, batchMode: false }).track({
                                //     event: "Pioneer Invited User Signed Up",
                                //     properties: {
                                //         "User ID": blrtUser.id
                                //     }
                                // }).fail(function (error) {
                                //     l.log("ALERT: Failed to Mixpanel track pioneerInviteeSignup:", error).despatch();
                                // }),
                                emailJS.SendTemplateEmail({
                                    template:   constants.mandrillTemplates.pioneerInviteeSignupAdmin,
                                    recipients: [{
                                                name: constants.pioneerAdmin.name,
                                                email: constants.pioneerAdmin.email,
                                                isUser: true
                                    }],
                                    sender:     null,
                                    creator:    null,
                                    customMergeVars: {
                                                "INVITEE_EMAIL": data.email,
                                                "PIONEER_NAME" : pioneer.get("name"),
                                                "PIONEER_EMAIL": pioneer.get("email"),
                                    }
                                }).fail(function (error) {
                                    l.log("ALERT: Failed to send pioneerInviteeSignupAdmin email:", error).despatch();
                                })
                            ]);
                        });
                    });  
            }, function (promiseError) {
                l.log(false, "Error updating PioneerInvitationRelation:", promiseError).despatch();
                return Parse.Promise.error("Error updating PioneerInvitationRelation: " + JSON.stringify(promiseError));
            });
    }
    
    return Context;
})();