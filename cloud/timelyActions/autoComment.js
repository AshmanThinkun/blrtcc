var loggler = require("cloud/loggler");
var convoItemCreateJS = require("cloud/api/conversationItem/create");
var constants = require("cloud/constants");
var _ = require("underscore");

// # autoComment TimelyAction
//action is a record in TimelyAction table
exports.process = function (action) {
    var context = new Context(action);

    switch(action.get("type")) {
        case "autoComment":
            return context.run();
    }
}

function fakeGuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var Context = (function () {
    //constructor
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    //instance variable
    Context.prototype.run = function () {
        var action = this.action;
        var l = this.l;
        var user = action.get("user");
        var convo = action.get("convo");
        var data = action.get("data");

        // we should already validated the input, just run it
        var requestParams = {
            user: user,
            device: {
                version: "1.0",
                device: "www",  
                os: "www",      
                osVersion: "1.0"
            },
            params: {
                conversationId: convo.id,
                items: []
            }
        };

        //create the comment param
        var commentObj = {
            "comment": {
                "text": data.comment,
                "localGuid": "local+=-"+fakeGuid()
            }
        };
        requestParams.params.items.push(commentObj);

        l.log("params for creation call:", requestParams);

        return convoItemCreateJS[1](requestParams).then(function (result) {
            l.log(true, result).despatch();
            return;
        }, function (error) {
            l.log(false, "Error running autoComment TimelyAction:", error).despatch();
            return Parse.Promise.error("Error running autoComment TimelyAction: " + JSON.stringify(error));
        });
    }

    return Context;
})();