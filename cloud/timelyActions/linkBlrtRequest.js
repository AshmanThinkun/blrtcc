var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");

var constants = require("cloud/constants");
var _ = require("underscore");

// # LinkBlrtRequest TimelyAction

// The purpose of this action is to take a Blrt Request and a Conversation that's
// been created from it, and link the two together. This should mark the Request
// as answered and run any actions related to that Request (i.e. a webhook if it
// was a api.blrt.com request).
exports.process = function (action) {
    var context = new Context(action);

    switch(action.get("type")) {
        case "linkBlrtRequest":
            return context.link();
        case "linkBlrtRequest_retryWebhook":
            return context.retryWebhook();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    function callWebhook(request, webhookUrl, convo) {
        var waitPromises = [];
        waitPromises.push(convo.get("creator").fetch());
        var itemQuery= new Parse.Query("ConversationItem")
            .equalTo("conversation", convo)
            .ascending("createdAt")
            .first();
        waitPromises.push(itemQuery);

        return Parse.Promise.when(waitPromises)
            .then(function (creator, blrt) {
                if(!blrt){
                    return Parse.Promise.error("blrt not found");
                }

                var duration;

                try {
                    var tmp = JSON.parse(convo.get("lastMediaArgument"));
                    duration = tmp["AudioLength"];
                } catch(e) { }

                return Parse.Cloud.httpRequest({
                    method: "POST",
                    url: webhookUrl,
                    body: {
                        endpoint: "/conv/request",
                        title: convo.get("blrtName"),
                        thumbnail: convo.get("thumbnailFile").url(),
                        url: constants.protocol + constants.siteAddress + "/conv/" + _.encryptId(convo.id) + "/blrt/" + _.encryptId(blrt.id),
                        duration: duration,
                        id: _.encryptId(request.id),
                        authorName: creator.get("name"),
                        authorEmail: creator.get("email")
                    }
                });  
            });
    }

    Context.prototype.link = function () {
        var action = this.action;
        var l = this.l;

        var convo = action.get("convo");
        var request = action.get("blrtRequest");

        // We'll first check for clashing actions. We can't run this twice.
        // We'll also fetch any objects we may need while waiting for that :P
        return Parse.Promise.when(
            new Parse.Query("TimelyAction")
                .equalTo("type", "linkBlrtRequest")
                .equalTo("convo", convo)
                .equalTo("blrtRequest", request)
                // With this we'll ensure two actions don't cancel each other out.
                .lessThan("executionTime", action.get("executionTime"))
                .first(),
            request.get("creator").fetch()
        ).then(function (clashingAction, requestCreator) {
            if(clashingAction) {
                l.log(false, "A clashing action was found:", clashingAction,
                    "This action will abort to let that one through.").despatch();
                return;
            }

            if(convo.get("incomplete")) {
                l.log(false, "The conversation never managed to complete - aborting link. Convo's ID: " + convo.id).despatch();
                return;
            }

            var requestee = request.get("requestee");
            var promises = [];
            var toSave = [];

            // First we'll mark the Blrt Request as answered.
            toSave.push(
                request
                    .set("answered", true)
                    .set("answeredBy", convo.get("creator"))
                    .set("answeredByRequestee", requestee ? requestee.id == convo.get("creator").id : false)
                    .set("conversation", convo) 
            );

            // Then we'll update a bunch of objects' counters.
            if(requestee)
                toSave.push(requestee.increment("unansweredReceivedBlrtRequestCount", -1));
            
            toSave.push(request.get("creator").increment("unansweredSentBlrtRequestCount", -1));

            promises.push(
                Parse.Object.saveAll(toSave)
                    .fail(function (promiseError) {
                        l.log(false, "Failed to save objects:", promiseError).despatch();
                        return Parse.Promise.error("Failed to save objects: " + JSON.stringify(promiseError));
                    })
            );

            // // Next is some Mixpanel tracking!
            // var mp = new Mixpanel({ batchMode: true });
            //
            // mp.track({
            //     event: "Blrt Request Responded To",
            //     properties: {
            //         "Original Requestee Responded": requestee ? requestee.id == convo.get("creator").id : false
            //     }
            // });
            //
            // mp.track("Blrt Request Response Received", { id: request.get("creator").id });
            //
            // promises.push(
            //     mp.despatch()
            //         .fail(function (promiseError) {
            //             l.log("ALERT: Failed to despatch Mixpanel events (but not aborting):", promiseError);
            //             return Parse.Promise.as();
            //         })
            // );

            // Last is, if necessary, /api/request webhookage.
            if(request.get("type") == "api.blrt.com") {
                var webhookUrl = (request.get("customWebhook") || requestCreator.get("developerRequestWebhookUrl") || "").trim();
                if(webhookUrl && !webhookUrl.match(/^(.*?:)?\/\//)) webhookUrl = "http://" + webhookUrl;

                if(webhookUrl && !_.isUrl(webhookUrl)) {
                    l.log("ALERT: Developer's webhook URL is not a URL: " + webhookUrl);
                    webhookUrl = false;
                }

                if(webhookUrl) {
                    promises.push(
                        callWebhook(request, webhookUrl, convo).fail(function (requestError) {
                            l.log("ALERT: Webhook request failed:", requestError);

                            // If the webhook request failed, we'll create another TimelyAction
                            // which can keep trying again (skipping all the other stuff).
                            return new Parse.Object("TimelyAction")
                                .set("executionTime", new Date())
                                .set("type", "linkBlrtRequest_retryWebhook")
                                .set("blrtRequest", request)
                                .save()
                                .fail(function (promiseError) {
                                    l.log(false, "Failed to save retryWebhook TimelyAction:", promiseError).despatch();
                                    return Parse.Promise.error("Failed to save retryWebhook TimelyAction: " + JSON.stringify(promiseError));
                                });
                        })
                    );
                }
            }

            return Parse.Promise.when(promises).then(function () {
                // And we're done!
                l.log(true).despatch();
                return;
            });
        }, function (promiseError) {
            l.log(false, "Error searching for clashing actions and other objects:", promiseError).despatch();
            return Parse.Promise.error("Error searching for clashing actions and other objects: " + JSON.stringify(promiseError));
        });
    }

    Context.prototype.retryWebhook = function () {
        var action = this.action;
        var l = this.l;

        var request = action.get("blrtRequest");

        if(!request.get("answered")) {
            l.log(false, "Request is not yet marked as answered - aborting: " + request.id).despatch();
            return Parse.Promise.as();
        }

        // We'll first check for clashing actions. We can't run this twice.
        // We'll also fetch any objects we may need while waiting for that :P
        return Parse.Promise.when(
            Parse.Query.or(
                new Parse.Query("TimelyAction")
                    .equalTo("type", "linkBlrtRequest_retryWebhook")
                    .equalTo("blrtRequest", request)
                    // With this we'll ensure two actions don't cancel each other out.
                    .lessThan("executionTime", action.get("executionTime")),
                new Parse.Query("TimelyAction")
                    .equalTo("type", "linkBlrtRequest")
                    .equalTo("blrtRequest", request)
            ).first(),
            request.get("creator").fetch(),
            request.get("conversation").fetch()
        ).then(function (clashingAction, requestCreator, convo) {
            if(clashingAction) {
                l.log(false, "A clashing action was found:", clashingAction, "This action will abort to let that one through.").despatch();
                return;
            }

            var webhookUrl = (request.get("customWebhook") || requestCreator.get("developerRequestWebhookUrl") || "").trim();
            if(webhookUrl && !webhookUrl.match(/^(.*?:)?\/\//)) webhookUrl = "http://" + webhookUrl;

            if(webhookUrl && !_.isUrl(webhookUrl)) {
                l.log("ALERT: Developer's webhook URL is not a URL: " + webhookUrl);
                webhookUrl = false;
            }

            if(webhookUrl)
                return callWebhook(request, webhookUrl, convo)
                    .then(function () {
                        l.log(true).despatch();
                    }, function (requestError) {
                        l.log(false, "Webhook request failed:", requestError).despatch();
                        return Parse.Promise.error("Webhook request failed: " + JSON.stringify(requestError));
                    });
        }, function (promiseError) {
            l.log(false, "Error searching for clashing actions and other objects:", promiseError).despatch();
            return Parse.Promise.error("Error searching for clashing actions and other objects: " + JSON.stringify(promiseError));
        });
    }

    return Context;
})();