var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");

var constants = require("cloud/constants");
var _ = require("underscore");

// # runWebhook TimelyAction

exports.process = function (action) {
    var context = new Context(action);

    switch(action.get("type")) {
        case "runWebhook":
            return context.run();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    function callWebhook(postingData) {
        return Parse.Cloud.httpRequest(postingData);
    }

    Context.prototype.run = function () {
        var action = this.action;
        var l = this.l;

        var data = action.get("data");

        // We'll first check for clashing actions. We can't run this twice.
        // We'll also fetch any objects we may need while waiting for that :P
        return Parse.Promise.when(
            new Parse.Query("TimelyAction")
                .equalTo("type", "runWebhook")
                .equalTo("data", data)
                // With this we'll ensure two actions don't cancel each other out.
                .lessThan("executionTime", action.get("executionTime"))
                .first()
        ).then(function (clashingAction) {
            if(clashingAction) {
                l.log(false, "A clashing action was found:", clashingAction,
                    "This action will abort to let that one through.").despatch();
                return;
            }

            var promises = [];
            // request webhookage.
            var webhookUrl = data.url;
            if(webhookUrl && !webhookUrl.match(/^(.*?:)?\/\//)) webhookUrl = "http://" + webhookUrl;

            if(webhookUrl && !_.isUrl(webhookUrl)) {
                l.log("ALERT: Developer's webhook URL is not a URL: " + webhookUrl);
                webhookUrl = false;
            }

            if(webhookUrl) {
                data.url = webhookUrl; 
                promises.push(
                    callWebhook(data).fail(function (requestError) {
                        l.log("ALERT: Webhook request failed:", requestError);
                    })
                );
            }

            return Parse.Promise.when(promises).then(function () {
                // And we're done!
                l.log(true).despatch();
                return;
            });
        }, function (promiseError) {
            l.log(false, "Error searching for clashing actions and other objects:", promiseError).despatch();
            return Parse.Promise.error("Error searching for clashing actions and other objects: " + JSON.stringify(promiseError));
        });
    }

    return Context;
})();