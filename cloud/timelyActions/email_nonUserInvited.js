var emailJS = require("cloud/email");
var loggler = require("cloud/loggler");
var curJS = require("cloud/conversationUserRelationLogic");

var constants = require("cloud/constants");
var _ = require("underscore");

exports.process = function (action) {
    var context = new Context(action);

    switch (action.get("type")) {
        case "email_nonUserInvited":
            return context.sendEmail();
        case "email_nonUserInvited_spawner":
            return context.spawnAction();
    }
}

var Context = (function () {
    function Context(action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    var template = "non-user-invited-reminder";
    Context.prototype.sendEmail = function () {
        var action = this.action;
        var l = this.l;

        return Parse.Promise.when(
            new Parse.Query("UserContactDetails")
                .equalTo("type", "email")
                .equalTo("value", action.get("context"))
                .notEqualTo("placeholder", true)
                .include("user")
                .first(),
            new Parse.Query("ConversationUserRelation")
                .equalTo("queryType", "email")
                .equalTo("queryValue", action.get("context"))
                .include("creator")
                .include("conversation")
                .include("conversation.creator")
                .find()
        ).then(function (userContact, relations) {
            if (userContact) {
                l.log("User detected as having signed up! Setting to refresh contact details (which will delete these actions)");

                return userContact.get("user")
                    //todo [Guichi] what is meanBy relationsDirty ?
                    //todo [Guichi] Here is one case : A nonUser is declared but the timely action do not realize it
                    //todo [Guichi] not sure why this useful
                    .set("relationsDirty", true)
                    .save()
                    .then(function () {
                        l.log(true).despatch();
                    }, function (promiseError) {
                        l.log(false, "Error updating user to refresh contact details:", promiseError).despatch();
                        return Parse.Promise.error("Error updating user to refresh contact details: " + JSON.stringify(promiseError));
                    });
            } else {
                // First check for dirty conversations and clean if necessary.
                var dirtyPromise = Parse.Promise.as();
                var dirtyConvoIds = [];

                _.each(relations, function (rel) {
                    console.log("non user relationId : "+rel.id)
                    var convo = rel.get("conversation");

                    console.log("convo ??? "+convo.id)

                    var isDirty = false;

                    console.log("blrtCount : "+convo.get("blrtCount"))

                    if (!convo.get("blrtCount"))
                        isDirty = true;

                    if (isDirty)
                        dirtyConvoIds.push(convo.id);
                });
                
                
                var accessTokenPromise=new Parse.Query("BlrtAccessToken")
                    .containedIn(constants.accessToken.keys.conversationUserRelation,relations)
                    .find()

                if (dirtyConvoIds.length != 0)
                    dirtyPromise = curJS.cleanDirtyConvos(dirtyConvoIds)
                        .then(function () {
                            return new Parse.Query("Conversation")
                                .containedIn("objectId", dirtyConvoIds)
                                .find()
                                .then(function (convos) {
                                    return _.reduce(convos, function (memo, convo) {
                                        memo[convo.id] = convo;
                                        return memo;
                                    }, {});
                                });
                        });
                
                //  Parse.Promise.when(dirtyPromise,accessTokenPromise).then(function(convos,accessTokens){
                //         console.log("access")
                // })

                return Parse.Promise.when(dirtyPromise,accessTokenPromise).then(function (convos,tokens) {

                    console.log("convos  ##########")
                    console.log(JSON.stringify(convos))

                    var customMergeVars = {};

                    var senders = [];
                    var numSenders = 0;
                    var creators = [];
                    var numCreators = 0;


                    _.each(relations, function (rel, i) {
                        _.each(tokens,function (tokenObj) {
                            if(tokenObj.get("conversationUserRelation")
                                &&tokenObj.get("conversationUserRelation").id==rel.id){
                                rel.token=tokenObj.get('token')
                            }
                        })

                        var sender = rel.get("creator");
                        var convo = convos && convos[rel.get("conversation").id] ? convos[rel.get("conversation").id] : rel.get("conversation");
                        var creator = convo.get("creator");

                        var senderName = sender.get("name");
                        if (!_.contains(senders, senderName)) {
                            senders.push(senderName);
                            customMergeVars["SENDER" + ++numSenders] = senderName;
                        }
                        var creatorName = creator.get("name");
                        if (!_.contains(creators, creatorName)) {
                            creators.push(creatorName);
                            customMergeVars["SENDER" + ++numCreators] = creatorName;
                        }

                        var duration = 42;
                        try {
                            duration = parseInt(JSON.parse(convo.get("lastMediaArgument"))["AudioLength"]);
                        } catch (err) {
                            l.log("ALERT: Couldn't extract a proper Blrt duration from convo with ID: " + convo.id, convo.get("lastMediaArgument"));
                        }
                        duration = _.durationify(duration);

                        var commentCount = convo.get("commentCount");
                        var ii = i + 1;
                        
                        console.log("######## token !!!! ")
                        console.log(rel.token)
                        
                        var convoUrl="http://" + constants.siteAddress + "/conv/" + _.encryptId(convo.id)
                        if(rel.token)
                        {
                            convoUrl+='?tk='+rel.token
                        }

                        customMergeVars["CONVO_TITLE" + ii] = convo.get("blrtName");
                        customMergeVars["CONVO_URL" + ii] =convoUrl ;
                        customMergeVars["CONVO_SENDER" + ii] = senderName;
                        customMergeVars["CONVO_CREATOR" + ii] = creatorName;
                        customMergeVars["CONVO_THUMBNAIL" + ii] = convo.get("thumbnailFile").url();
                        customMergeVars["CONVO_DURATION" + ii] = duration;
                        customMergeVars["CONVO_COUNT_USERS" + ii] = convo.get("bondCount");
                        customMergeVars["CONVO_COUNT_BLRTS" + ii] = convo.get("blrtCount");
                        customMergeVars["CONVO_COUNT_COMMENTS" + ii] = commentCount != null ? commentCount : 0;
                    });

                    customMergeVars["CONVO_SENDER_LIST"] = _.commaAndList(senders);
                    customMergeVars["CONVO_CREATOR_LIST"] = _.commaAndList(creators);

                    return emailJS.SendTemplateEmail({
                        template: template,
                        recipients: [{
                            name: "",
                            email: action.get("context"),
                            isUser: false
                        }],
                        customMergeVars: customMergeVars
                    }).then(function () {
                        l.log(true).despatch();
                    }, function (emailError) {
                        l.log(false, "Error sending email:", emailError).despatch();
                        return Parse.Promise.error("Error sending email: " + JSON.stringify(emailError));
                    });
                });
            }
        }, function (promiseError) {
            l.log(false, "Error searching for existing user contact:", promiseError).despatch();
            return Parse.Promise.error("Error searching for existing user contact: " + JSON.stringify(promiseError));
        });
    }

    Context.prototype.spawnAction = function () {
        var action = this.action;
        var l = this.l;

        return Parse.Promise.when(
            new Parse.Query("Settings")
                .equalTo("key", "email_nonUserInvited")
                .first(),
            new Parse.Query("TimelyAction")
                .startsWith("type", "email_nonUserInvited")
                .equalTo("context", action.get("context"))
                .notEqualTo("objectId", action.id)
                .find()
        ).then(function (settings, clashingActions) {
            if (clashingActions.length != 0) {
                l.log("Found clashing actions:", _.pluck(clashingActions, "id"))
                    .log("Abort is required if: it's not a spawning action OR it is an older spawning action");

                var abort = false;
                var thisDate = new Date(action.createdAt);

                _.each(clashingActions, function (clash) {
                    if (abort)
                        return;

                    if (clash.get("type") != "email_nonUserInvited_spawner" || new Date(clash.createdAt) < thisDate)
                        return abort = clash;
                });

                if (abort) {
                    l.log(true, "Aborting because of action:", abort).despatch();
                    return;
                } else
                    l.log("This action is older than the other spawner, so we're not aborting (that one will instead)");
            }

            if (settings == null) {
                l.log(false, "Setting with key email_nonUserInvited not found").despatch();
                return Parse.Promise.error("Setting with slug email_nonUserInvited not found");
            }

            try {
                settings = JSON.parse(settings.get("value"));
            } catch (error) {
                l.log(false, "Failed to JSON parse settings for setting:", settings, "With error:", error).despatch();
                return Parse.Promise.error("Failed to JSON parse settings: " + JSON.stringify(error));
            }

            if (!_.isFinite(settings.period) || !_.isFinite(settings.count)) {
                l.log(false, "settings.period or settings.count aren't numbers!").despatch();
                return Parse.Promise.error("settings.period or settings.count aren't numbers!");
            }

            return new Parse.Object("TimelyAction")
                .set("type", "email_nonUserInvited")
                .set("context", action.get("context"))
                .set("executionTime", new Date(+new Date(action.get("executionTime")) + constants.hour * 24 * settings.period))
                .set("period", settings.period)
                .set("count", settings.count)
                .setACL(new Parse.ACL())
                .save()
                .then(function () {
                    l.log(true).despatch();
                }, function (promiseError) {
                    l.log(false, "Error saving new email_nonUserInvited:", promiseError).despatch();
                    return Parse.Promise.error("Error saving new email_nonUserInvited: " + JSON.stringify(promiseError));
                });
        }, function (promiseError) {
            l.log(false, "Error searching for email_nonUserInvited settings:", promiseError).despatch();
            return Parse.Promise.error("Error searching for email_nonUserInvited settings: " + JSON.stringify(promiseError));
        });
    }

    return Context;
})();