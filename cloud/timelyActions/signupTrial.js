var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");
var accountTypeJS = require("cloud/accounttype");

var constants = require("cloud/constants");
var _ = require("underscore");

// # signupTrial TimelyAction

exports.process = function (action) {
    var context = new Context(action);

    switch(action.get("type")) {
        case "signupTrial":
            return context.run();
    }
}

var Context = (function () {
    function Context (action) {
        this.action = action;
        this.l = loggler.create(action.get("type"));
    }

    Context.prototype.run = function () {
        var action = this.action;
        var l = this.l;
        var user = action.get("user");
        var data = action.get("data");
        // we should already validated the input, just run it
        var requestParams = {
            user: action.get("user"),
            params: {
                requestFor: "trial",
                data: {
                    coupon: data.coupon
                }
            }
        };

        if(!user.get("signupCoupon")){
            l.log(false, "signupCoupon not detected, another requestAccountUpgrade process may running, actionId: ", action.id).despatch();
            return Parse.Promise.error("signupCoupon not detected, another requestAccountUpgrade process may running");
        }

        return user.unset("signupCoupon")
            .save()
            .then(function(){
                return accountTypeJS.requestAccountUpgrade(requestParams)
                    .fail(function(){
                        return user.set("signupCoupon", data.coupon)
                            .save();
                    });        
            }).then(function(response){
                l.log(true).despatch();
                return;
            }, function(error){
                l.log(false, "Error running signupTrial TimelyAction:", error).despatch();
                return Parse.Promise.error("Error running signupTrial TimelyAction: " + JSON.stringify(error));
            });
    }

    return Context;
})();