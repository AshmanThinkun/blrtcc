// var constants = require("cloud/constants");
// var emailJS = require("cloud/email");
// var Mixpanel = require("cloud/mixpanel");
//
// var _ = require("underscore");
// var Crypto = require('crypto');
// var Buffer = require('buffer').Buffer;
// var NonUser = require("cloud/nonUser");
// var loggler = require("cloud/loggler");
//
// Parse.Cloud.define('applyPioneer', function(req, res) {
// 	var params = req.params;
// 	var user = req.user;
// 	var sendEmail = constants.pioneerAdmin.email;
// 	var sendName = constants.pioneerAdmin.name;
//
// 	Parse.Cloud.useMasterKey();
//
//
// 	if(!user){
// 		return res.success(JSON.stringify({ success: false, error: "Please Login first." }));
// 	}
// 	var applyStatus = user.get("pioneerStatus");
//
//     if(applyStatus == "" || applyStatus == null){
//         user.set("pioneerStatus","pending");
//         user.set("phone", params.phone);
//         user.set("company", params.company);
//         user.set("twitterHandle", params.twitterHandle);
//         user.set("useFor", params.useFor);
//         user.save();
//
//
//         //send email
//         emailJS.SendTemplateEmail({
// 	        template: 	constants.mandrillTemplates.pioneerApplication,
// 	        recipients: [{
// 	         			name: sendName,
// 	         			email: sendEmail,
// 	         			isUser: true
// 	        }],
// 	        sender:   	user,
// 	        creator:  	user,
// 	        customMergeVars: {
// 	        			"USER2_NAME": user.get("name"),
// 	        			"USER2_EMAIL": user.get("username"),
//                         "USER2_PHONE": params.phone,
//                         "USER2_ORGANISATION": params.company,
//                         "USER2_INDUSTRY": user.get("industry")?  user.get("industry") : "NO INDUSTRY SET",
//                         "USER2_TWITTER": params.twitterHandle,
//                         "USER2_USEBLRT": params.useFor,
// 	        }
// 		});
//
//         new Mixpanel({ id: user.id }).track("Pioneer Application Sent");
// 		return res.success(JSON.stringify({ success: true, message: "Your application is being processed." }));
//     }
//     return res.success(JSON.stringify({ success: false, error: "You have already applied." }));
// });
//
//
//
// Parse.Cloud.define('getRelatedUserWithNonuser', function(req, res) {
//     var params = req.params;
//     var user = req.user;
//
//     Parse.Cloud.useMasterKey();
//
//
//     if(!user){
//         return res.success(JSON.stringify({ success: false, error: "Please Login first." }));
//     }
//
//     if(user.get("pioneerStatus") != "approved"){
//         return res.success(JSON.stringify({ success: false, error: "This is not a pioneer account." }));
//     }
//
//     new Parse.Query(Parse.Object.extend("PioneerInvitationRelation"))
//     .equalTo("pioneerId",user.id)
//     .include("user")
//     .find()
//     .then(function(invitedEmailObjs){
//         var invitedEmails = [];
//         _.each(invitedEmailObjs,function(emailObj){
//             invitedEmails.push(emailObj.get("email"));
//         });
//         new Parse.Query(Parse.Object.extend("ConversationUserRelation"))
//         .equalTo("creator",user)
//         .notEqualTo("user",user)
//         .descending("updatedAt")
//         .include("user")
//         .include("conversation")
//         .find()
//         .then(function(results){
//             //all related users  results[index].get("user");
//             var relatedUsers = [];
//             var fetchedName = [];
//
//             var promises = [];
//
//             _.each(results,function(value,index){
//                 var tempUser = {};
//                 var iterUser = value.get("user");
//                 tempUser.convoName = value.get("conversation").get("blrtName");
//                 tempUser.convoUrl = "//" + constants.siteAddress + "/conv/" + _.encryptId(value.get("conversation").id);
//                 tempUser.createNewUrl = "//" + constants.siteAddress + "/make/";
//                 if(!iterUser){
//                     if(value.get("queryType") == "email"){
//                         tempUser.username = value.get("queryValue");
//                         tempUser.name = value.get("queryValue");
//                         tempUser.type = "nonUser";
//                     }
//                     //TODO: type=facebook
//                 }else{
//                     tempUser.username = iterUser.get("email");
//                     tempUser.name = iterUser.get("name");
//                     var counter = iterUser.get("blrtCreationCounter");
//                     if(counter && counter>0){
//                         tempUser.type = "activeUser";
//                     }else{
//                         tempUser.type = "inactiveUser";
//                     }
//                 }
//                 tempUser.createNewUrl +="?e[]=" + encodeURIComponent(tempUser.username);
//
//
//                 if(_.indexOf(fetchedName, tempUser.username)==-1
//                     && _.indexOf(invitedEmails, tempUser.username) !=-1){
//                     var myPromise = new Parse.Promise();
//                     promises.push(myPromise);
//
//                     if(tempUser.type=="nonUser"){
//                         // the nonUser sign up , and become a user.....
//                         new Parse.Query("PioneerInvitationRelation")
//                         .equalTo("email",tempUser.username)
//                         .equalTo("userSignedUp",true)
//                         .include("user")
//                         .first()
//                         .then(function(relation){
//                             if(relation){
//                                 var signedupUser = relation.get("user");
//                                 var counter = signedupUser.get("blrtCreationCounter");
//                                 if(counter && counter>0){
//                                     tempUser.type = "activeUser";
//                                 }else{
//                                     tempUser.type = "inactiveUser";
//                                 }
//                                 tempUser.name = signedupUser.get("name");
//                                 tempUser.username = signedupUser.get("email");
//                             }
//                             relatedUsers.push(tempUser);
//                             fetchedName.push(tempUser.username);
//                             myPromise.resolve();
//                         });
//                     }else{
//                         relatedUsers.push(tempUser);
//                         fetchedName.push(tempUser.username);
//                         myPromise.resolve();
//                     }
//                 }
//             });
//
//             return Parse.Promise.when(promises).then(function(){
//
//                 _.each(invitedEmailObjs,function(emailObj){
//                     var tempUser = {};
//                     var user = emailObj.get("user");
//                     tempUser.username = emailObj.get("email");
//                     if(emailObj.get("claimedPremium")){
//                         tempUser.type = "activeUser";
//                         tempUser.name = user.get("name");
//                         tempUser.username = user.get("email");
//                     }else if(emailObj.get("userSignedUp")){
//                         tempUser.type = "inactiveUser";
//                         tempUser.name = user.get("name");
//                         tempUser.username = user.get("email");
//                     }else{
//                         tempUser.type = "nonUser";
//                         tempUser.name = tempUser.username;
//                     }
//                     if(_.indexOf(fetchedName, tempUser.username)==-1){
//                         tempUser.createNewUrl = "//" + constants.siteAddress + "/make/" + "?e[]=" + encodeURIComponent(tempUser.username);
//                         tempUser.convoName = "";
//                         tempUser.convoUrl ="#";
//                         relatedUsers.push(tempUser);
//                         fetchedName.push(tempUser.username);
//                     }
//                 });
//
//                 return new Mixpanel({ id: user.id }).track("Pioneer Control Panel Loaded")
//                     .fail(function (promiseError) {
//                         loggler.global.log("ALERT: Failed to Mixpanel track Pioneer Control Panel Loaded").despatch();
//                     })
//                     .always(function () {
//                         return res.success(JSON.stringify({ success: true, relatedUsers: relatedUsers}));
//                     });
//             });
//         },function(error){
//             loggler.global.log("ALERT: Error searching for relatedUsers:", error).despatch();
//         });
//     },function(error){
//         loggler.global.log("ALERT: Error searching for invitedEmails:", error).despatch();
//     });
//
// });
//
// Parse.Cloud.define('sendPrimeInvitation', function(req, res) {
// 	//this method will generate the invitation link for pioneer to send invitation
// 	var params = req.params;
// 	var user = req.user;
//
// 	Parse.Cloud.useMasterKey();
//
//
// 	if(!user){
// 		return res.success(JSON.stringify({ success: false, error: "Please Login first." }));
// 	}
//
// 	if(user.get("pioneerStatus") != "approved"){
//     	return res.success(JSON.stringify({ success: false, error: "This is not a pioneer account." }));
//     }
//
// 	//get pioneerSecure
// 	var pioneerSecure = user.get("pioneerSecure");
// 	if(!pioneerSecure || pioneerSecure==""){
// 		pioneerSecure = Crypto.createHmac('sha256',"SIH*&T*JPHIUY^").update(new Date().toISOString()).digest('base64');
// 		user.set("pioneerSecure", pioneerSecure);
// 		user.save();
// 	}
//
// 	//generate link
// 	return exports.generateLink(user.id, pioneerSecure)
//         .then(function(link){
//             return res.success(JSON.stringify({ success: true, link:link }));
//         });
// });
//
//
//
// Parse.Cloud.define('acceptInvitation', function(req, res) {
// 	var params = req.params;
// 	var user = req.user;
//
// 	Parse.Cloud.useMasterKey();
//
//  	exports.validSecure(params.id, params.secure).then(function(result){
//  		if(!result) {
//  			return res.success(JSON.stringify({ success: false, error: "Invalid invitation link." }));
//  		}
//
//     	//check if the email already registered or accepted the invitation for other pioneer
//     	new Parse.Query(Parse.User)
//     	.equalTo("username",params.email)
//     	.first()
//     	.then(function (result) {
//     		if(result){
//     			return res.success(JSON.stringify({ success: false, error: "This email already exists." }));
//     		}
//
//             return exports.addPioneerInvitationRelation(params.id, params.email, "email", false).then(function(result){
//                 res.success(JSON.stringify(result));
//             });
//     	});
//     });
// });
//
// exports.addPioneerInvitationRelation = function(pioneerId, value, type, viaConversationInvite) {
//     Parse.Cloud.useMasterKey();
//     return new Parse.Query(Parse.Object.extend("PioneerInvitationRelation"))
//         .equalTo("email",value)
//         .find()
//         .then(function (result) {
//             if(result && result.length>0){
//                 return { success: false, error: "You have already accepted a Blrt Pioneer's invitation." };
//             }
//             if(!type)
//                 type = "email";
//
//             //link the email to that pioneer
//             var PioneerInvitationRelation = Parse.Object.extend("PioneerInvitationRelation");
//             var myRelation = new PioneerInvitationRelation;
//             myRelation.set("pioneerId",pioneerId);
//             myRelation.set("email",value); //too late to change the property name, "email" should be value
//             myRelation.set("type",type);
//
//             myRelation.save();
//
//             //set non-user action when they signup (send email to pioneer and pioneerAdmin)
//             var detail = {
//                 type: type,
//                 value: value,
//                 referredBy: pioneerId
//             };
//
//             return NonUser.get([detail]).then(function(theuser){
//                 theuser = theuser[0];
//
//                 theuser.addAction("PioneerTimelyAction", { email: value, pioneerId: pioneerId }, { newOnly: true, existingOnly: false });
//
//                 var mp = new Mixpanel({ batchMode: true });
//                 mp.track({
//                     event: "Pioneer Invitation Received",
//                     properties: {
//                         "Pioneer ID": pioneerId,
//                         "Via": viaConversationInvite ? "Conversation invitation" : "Link"
//                     }
//                 }, { id: "nonuser-" + theuser.id, updateLastSeen: false });
//                 mp.engage({
//                     "Pioneer Invitation From": pioneerId
//                 }, { id: "nonuser-" + theuser.id, updateLastSeen: false });
//
//                 mp.track({
//                     event: "Pioneer Invitation Sent",
//                     properties: {
//                         "NonUser ID": "nonuser-" + theuser.id,
//                         "Via": viaConversationInvite ? "Conversation invitation" : "Link"
//                     }
//                 }, { id: pioneerId });
//
//                 return mp.despatch().always(function () {
//                     return { success: true, message: "All done. You will be granted FREE Blrt Premium for 6 months after you sign up and make your first Blrt. Get started by <a href='http://www.blrt.com'>downloading Blrt on your iPhone, iPad, iPod or Andoid device</a>." };
//                 });
//
//             });
//
//         }, function (error) {
//             loggler.global.log("ALERT: Error addPioneerInvitationRelation:", error).despatch();
//         });
// }
//
//
// var salt = constants.pioneerSalt;
// exports.generateLink = function (id, secure) {
// 	var url = constants.siteAddress +"/pioneers?";
//     if(constants.isLive){
//         url = "blrt.com/pioneers?";
//     }
//
// 	var paramsPart ="";
// 	paramsPart+="id="+ id;
// 	var outSecure = Crypto.createHmac('sha256', salt ).update(secure).digest('base64');
// 	paramsPart+="&secure="+outSecure;
//
//     var longUrl = "https://"+ url+paramsPart;
//     //user bitly to shorten url
//     return Parse.Cloud.httpRequest({
//             url: "https://api-ssl.bitly.com/v3/shorten?access_token=" + constants.bitlyToken + "&longUrl=" + encodeURIComponent(longUrl),
//         }).then(function (httpResponse) {
//             // return longUrl;
//             return httpResponse.data.data.url;
//         }, function (promiseError) {
//             return Parse.Promise.as(longUrl);
//         });
// }
//
// exports.validSecure = function(id,secure){
//     return new Parse.Query(Parse.User)
// 	    .equalTo("objectId",id)
// 	    .first().then(function(result){
// 	    	if(!result)
// 	    		return false;
// 	    	var token = result.get("pioneerSecure");
// 	    	var outSecure = Crypto.createHmac('sha256', salt ).update(token).digest('base64');
//             secure = secure.replace(new RegExp(" ", 'g'),"+");
// 	    	if(outSecure!=secure){
// 	    		return false;
// 	    	}
// 	    	return true;
// 	    });
// }
//
// Parse.Cloud.job("addUserPointerForExistingSignupPioneerInvitationRelation", function(request, status) {
//   // Set up to modify user data
//   Parse.Cloud.useMasterKey();
//   // Query for all users
//   var query = new Parse.Query("PioneerInvitationRelation")
//                 .equalTo("userSignedUp",true);
//   query.each(function(relation) {
//         return new Parse.Query("NonUser")
//             .equalTo("type", "email")
//             .equalTo("value", relation.get("email"))
//             .equalTo("claimed", true)
//             .include("user")
//             .first()
//             .then(function(theuser){
//                 return relation
//                 .set("user", theuser.get("user"))
//                 .save();
//             });
//   }).then(function() {
//     // Set the job's success status
//     status.success("Migration completed successfully.");
//   }, function(error) {
//     // Set the job's error status
//     status.error("Uh oh, something went wrong.");
//   });
// });
//
//
// Parse.Cloud.job("findPioneerInviteeMakeBlrt", function(request, status) {
//   // Set up to modify user data
//   Parse.Cloud.useMasterKey();
//   var numOfReceviver=0;
//   var emailList = [];
//   // Query for all users
//   var query = new Parse.Query("PioneerInvitationRelation")
//                 .equalTo("userSignedUp",true)
//                 .notEqualTo("claimedPremium", true)
//                 .include("user");
//   query.each(function(relation) {
//         var blrtUser = relation.get("user");
//         var blrtCounter = blrtUser.get("blrtCreationCounter");
//         if(blrtCounter && blrtCounter>0){
//             var notifiedDate = relation.get("notifiedAt");
//             var today = new Date();
//             if(notifiedDate && notifiedDate> today.setDate(today.getDate()-1)){
//                 return ;
//             }
//
//             //send notification
//             return new Parse.Query(Parse.User)
//                 .equalTo("objectId", relation.get("pioneerId"))
//                 .first()
//                 .then(function(pioneerUser){
//                     return emailJS.SendTemplateEmail({
//                         template:   constants.mandrillTemplates.pioneerInviteeComplete,
//                         recipients: [{
//                                     name: pioneerUser.get("name"),
//                                     email: pioneerUser.get("email"),
//                                     isUser: true,
//                                     user: pioneerUser
//                         }],
//                         sender:     null,
//                         creator:    null,
//                         customMergeVars: {
//                                     "INVITEE_NAME": blrtUser.get("name"),
//                                     "INVITEE_EMAIL": blrtUser.get("email"),
//                         }
//                     }).then(function(){
//                         return emailJS.SendTemplateEmail({
//                             template:   constants.mandrillTemplates.pioneerUpgradeRequest,
//                             recipients: [{
//                                         name: constants.pioneerAdmin.name,
//                                         email: constants.pioneerAdmin.email,
//                                         isUser: true
//                             }],
//                             sender:     null,
//                             creator:    null,
//                             customMergeVars: {
//                                         "INVITEE_NAME": blrtUser.get("name"),
//                                         "INVITEE_EMAIL": blrtUser.get("email"),
//                                         "PIONEER_NAME": pioneerUser.get("name"),
//                                         "PIONEER_EMAIL": pioneerUser.get("email"),
//                             }
//                         });
//                     });
//             }).then(function(){
//                 //saveObjectAsNotified
//                 relation.set("notifiedAt", new Date());
//                 return relation.save().then(function(){
//                     emailList.push(blrtUser.get("email"));
//                     numOfReceviver++;
//                 });
//             });
//         }
//   }).then(function() {
//     // Set the job's success status
//     status.success(numOfReceviver + " notifications were sent.");
//   }, function(error) {
//     // Set the job's error status
//     status.error("Uh oh, something went wrong.");
//   });
// });
//
//
// exports.upgradeIfPioneerInviteeMakeBlrt = function(user){
//     if(!user.get("invitedByPioneer")|| user.get("claimedPremium")){
//         return Parse.Promise.as({success: true, message: "Not Eligible."});
//     }
//     Parse.Cloud.useMasterKey();
//     return new Parse.Query("PioneerInvitationRelation")
//         .equalTo("user",user)
//         .equalTo("userSignedUp",true)
//         .notEqualTo("claimedPremium", true)
//         .first()
//         .then(function(relation){
//             if(!relation){
//                 loggler.global.log("Not Eligible pioneer invitee.").despatch();
//                 return {success: true, message: "Not Eligible."};
//             }
//             //add timely action
//             var data = {
//                 user: user,
//                 relation: relation
//             };
//
//             return new Parse.Query("TimelyAction")
//                 .equalTo("type", "PioneerInviteeMakeBlrt")
//                 .equalTo("user", user)
//                 .equalTo("relation", relation)
//                 .first()
//                 .then(function(result){
//                     if(result)
//                         return true;
//                     return new Parse.Object("TimelyAction")
//                         .set("type", "PioneerInviteeMakeBlrt")
//                         .set("user", user)
//                         .set("relation", relation)
//                         .set("executionTime", new Date())
//                         .save()
//                         .then(function(){
//                             return loggler.global.log("Add pioneerInviteeMakeBlrt TimelyAction").despatch();
//                         },function(error){
//                             loggler.global.log("ALERT: Error Adding pioneerInviteeMakeBlrt TimelyAction:", error).despatch();
//                             return Parse.Promise.as({ success: false, message: "Error Adding pioneerInviteeMakeBlrt TimelyAction."});
//                         });
//                 });
//
//         },function(error){
//             loggler.global.log("ALERT: Error finding PioneerInvitationRelation:", error).despatch();
//             return Parse.Promise.as({ success: false, message: "Error finding PioneerInvitationRelation."});
//         });
// }
//
//
// Parse.Cloud.job("copyFieldsFromPioneersRelationToUser", function(request, status) {
//   // Set up to modify user data
//   Parse.Cloud.useMasterKey();
//   // Query for all users
//   var query = new Parse.Query("PioneerInvitationRelation")
//                 .equalTo("userSignedUp",true)
//                 .include("user");
//   query.each(function(relation) {
//         var user = relation.get("user");
//         var pioneer;
//         return new Parse.Query(Parse.User)
//             .equalTo("objectId",relation.get("pioneerId"))
//             .first()
//             .then(function(result){
//                 pioneer = result;
//                 user.set("invitedByPioneer",pioneer);
//                 user.set("claimedPremium", relation.get("claimedPremium"));
//                 user.save();
//             });
//   }).then(function() {
//     // Set the job's success status
//     status.success("Migration completed successfully.");
//   }, function(error) {
//     // Set the job's error status
//     status.error("Uh oh, something went wrong.");
//   });
// });
//
//
//
// Parse.Cloud.job("migration_existingPioneer", function(request, status) {
//   // Set up to modify user data
//   Parse.Cloud.useMasterKey();
//   var pioneerList = [];
//   // Query for all users
//   var query = new Parse.Query("User")
//                 .equalTo("pioneerStatus","approved")
//   query.each(function(pioneer) {
//         //find how many people he invited
//         return new Parse.Query("PioneerInvitationRelation")
//             .equalTo("pioneerId", pioneer.id)
//             .equalTo("claimedPremium", true)
//             .find()
//             .then(function(results){
//                 pioneerList.push({
//                     pioneerId: pioneer.id,
//                     count:     results.length,
//                 });
//                 return pioneer.set("activeInvitee", results.length)
//                     .set("timesOfPioneerUpgrade" ,1)
//                     .save();
//             });
//   }).then(function() {
//     // Set the job's success status
//     status.success(JSON.stringify(pioneerList));
//   }, function(error) {
//     // Set the job's error status
//     status.error("Uh oh, something went wrong.");
//   });
// });
//
//
//
//
// Parse.Cloud.job("migration_existingPioneerInvitee", function(request, status) {
//   // Set up to modify user data
//   Parse.Cloud.useMasterKey();
//   var userList = [];
//   // Query for all users
//   var query = new Parse.Query("PioneerInvitationRelation")
//                 .equalTo("userSignedUp",true)
//                 .include("user")
//   query.each(function(relation) {
//         var user = relation.get("user");
//         //find how many people he invited
//         if(user.get("blrtCreationCounter") && user.get("blrtCreationCounter")>0){
//             userList.push({id: user.id, email: user.get("email")});
//             return relation.set("claimedPremium",true).save();
//         }
//
//
//   }).then(function() {
//     // Set the job's success status
//     status.success(JSON.stringify(userList));
//   }, function(error) {
//     // Set the job's error status
//     status.error("Uh oh, something went wrong.");
//   });
// });
//
