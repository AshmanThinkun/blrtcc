
var _=require('underscore')
function Analytics(writeKey, options){
    if (!(this instanceof Analytics)) return new Analytics(writeKey, options);
    options = options || {};
    this.queue = [];
    this.writeKey = writeKey;
    this.host = options.host || 'https://api.segment.io';
    this.flushAt = Math.max(options.flushAt, 1) || 20;
}

Analytics.prototype.identify = function(message, fn){
    return this.enqueue('identify', message, fn);
};

Analytics.prototype.track = function(message, fn){
    return this.enqueue('track', message, fn);
}
Analytics.prototype.alias = function(message, fn){
    return this.enqueue('alias', message, fn);
};

Analytics.prototype.enqueue = function(type, message){
    console.log("enqueue")
    message.type = type;
    message.context = _.extend(message.context || {}, { library: { name: 'analytics-node', version: "2.1.0" }});
    if (!message.timestamp) message.timestamp = new Date();
    if (!message.messageId) message.messageId = 'node-' + Math.random(1000);

    this.queue.push({
        message: message,
    });

    if (this.queue.length >= this.flushAt){
        return this.flush();
    }
};

var btoa=require('cloud/btoa')

Analytics.prototype.flush = function(){
    console.log("flush")
    var items = this.queue.splice(0, this.flushAt);
    var batch = items.map(function(_){ return _.message; });

    var data = {
        batch: batch,
        timestamp: new Date(),
        sentAt: new Date()
    };




    return Parse.Cloud.httpRequest({
        url:this.host + '/v1/batch',
        body: JSON.stringify(data),
        method:"POST",
        headers:{
            "Content-Type":"application/json",
            "Authorization":"Basic "+btoa(this.writeKey)
        }
    })

};
const applicationConfig=require('../config/applicationConfig');

exports.trackImmediate=new Analytics(applicationConfig.segmentKey,{flushAt:1});
exports.trackBatch=new Analytics(applicationConfig.segmentKey,{flushAt:1});


