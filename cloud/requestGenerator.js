var constants = require("cloud/constants");
var email = require("cloud/email");
// var Mixpanel = require("cloud/mixpanel");
var NonUser = require("cloud/nonUser");

var loggler = require("cloud/loggler");
var _ = require("underscore");


Parse.Cloud.define('generate', function(req, res) {
	return exports.generate(req, "request.blrt.com").then(function (result) {
		return res.success(JSON.stringify(result));
	}, function (error) {
		return res.success(JSON.stringify({ success: false, message: "Failed to send the request. Please try again." }));
	});
});

exports.generate = function (req, type) {
	function fail(message) {
		return {
			success: false,
			error: message
		};
	}

	var params = req.params;
	var user = type == "api.blrt.com" ? params.user : req.user;


	var errors = {};
	switch(type) {
		case "request.blrt.com":
			errors.invalidInput = "Invalid request. Make sure you fill out all fields before sending.";
			errors.generic = "Failed to send the request. Please try again.";

			break;
		case "api.blrt.com":
		default:
			errors.invalidInput = "Invalid input.";
			errors.generic = "An internal error occurred.";

			break;
	}

	Parse.Cloud.useMasterKey();

	// First chop out crappy values
	if(!_.isString(params.name) || params.name.length == 0 || params.name.length > constants.convo.nameCharLimit) params.name = null;

	if(!_.isArray(params.media)) params.media = null;
	else params.media = _.filter(params.media, function (url) { return _.isUrl(url); });
	if(params.media && params.media.length == 0) params.media = null;

	if(!_.isString(params.message) || params.message.length == 0 || params.message.length > 255) params.message = null;

	if(!_.isEmail(params.requestee)) params.requestee = null;

	if(!_.isArray(params.recipients)) params.recipients = null;
	else params.recipients = _.filter(params.recipients, function (email) { return _.isEmail(email); });
	if(params.recipients && params.recipients.length == 0) params.recipients = null;

	if(!_.isString(params.webhook)) params.webhook = null;
	if(params.webhook && !params.webhook.match(/^(.*?:)?\/\//)) params.webhook = "http://" + params.webhook;

	// Validate input before starting logic
	switch(type) {
		case "request.blrt.com":
			if(!(
				user &&
				params.name &&
				params.media &&
				params.media.length < 11 &&
				params.message &&
				params.requestee &&
				params.recipients &&
				params.recipients.length < 5 &&
				_.some(params.recipients, function (recipient) { return recipient.toLowerCase() == user.get("username"); })
			)) return Parse.Promise.as({ success: false, error: errors.invalidInput });

			break;
		case "api.blrt.com":
		default:
			if(!(
				user
			)) return Parse.Promise.as({ success: false, error: errors.invalidInput });

			if(_.isString(user)) {
				var tmp = user;
				user = new Parse.User();
				user.id = tmp;
			}

			break;
	}

	var contactDetailQuery = Parse.Promise.as();
	if(params.requestee)
		contactDetailQuery = new Parse.Query("UserContactDetails")
		.equalTo("type", "email")
		.equalTo("value", params.requestee.trim().toLowerCase())
		.notEqualTo("placeholder", true)
		.include("user")
		.first();

	return contactDetailQuery
		.then(function (requesteeContact) {
			if(params.requestee) {
				var requestee = false;
				if(requesteeContact)
					requestee = requesteeContact.get("user");
				requesteeContact = params.requestee.trim().toLowerCase();
			}


			var requestObject = new Parse.Object("BlrtRequest")
				.set("requestee", requestee ? requestee : null)
				.set("creator", user)
				.set("type", type);

			if(params.message) requestObject.set("message", params.message);
			if(params.requestee)
				requestObject
					.set("originalRequesteeType", "email")
					.set("originalRequesteeValue", requesteeContact);
			if(params.name)
				requestObject.set("name", params.name);

			if(params.webhook)
				requestObject.set("customWebhook", params.webhook);

			// Only master key can access BlrtRequests now!
			requestObject.setACL(new Parse.ACL());

			var generatedUrl;
			if(!constants.isLive) generatedUrl = "https://" + constants.siteAddress + "/make/";
			else                  generatedUrl = 'http://request.blrt.com/';

			if(params.name)
				generatedUrl += encodeURIComponent(params.name);

			generatedUrl += "?";

			var promises = [];

			function handleMedia(url) {
				if(url.indexOf("//") == 0)
					url = url.substring(2);

				return promises.push(Parse.Promise.as().then(function () {
					var extension = _.extension(url).toLowerCase();


					if(extension == ".pdf")
						return "pdf";
					else if(constants.supportedImageExtensions.indexOf(extension) != -1)
						return "img";

					// If we don't know what this file is based on it's extension,
					// download it and check the ContentType header.
					return Parse.Cloud.httpRequest({
						url: url
					}).then(function (httpResponse) {
						console.log("==== httpResponse headers ====")
						console.log(httpResponse.headers)

					    var contentType = httpResponse.headers['content-type'];

						console.log("=== contentType ===")
						console.log(contentType)

					    if(contentType.indexOf('image') != -1)
					    	return "img";
					    else if(contentType.indexOf('pdf') != -1)
					    	return "pdf";
					    else
					    	return "www";
					}, function (promiseError) {
						return Parse.Promise.as(promiseError.status);
					});
				}));
			}

			_.each(params.media, handleMedia);


			return Parse.Promise.when(promises).then(function (testOnly) {

				var pdfError = "Multiple PDFs!";
				var wwwError = "Multiple WWWs!";
				var httpError = "HTTP error";

				if(params.media) {
					try {
						generatedUrl += _.reduce(_.object(params.media, arguments), function (queryString, type, url) {
							if(type == "pdf") {
								if(requestObject.get("pdf")) throw pdfError;
							} else if(type == "www") {
								if(requestObject.get("url")) throw wwwError;
							} else if(type != "img") {
								if(type == "503")
									throw httpError;

								type = "www";
								if(requestObject.get("url")) throw wwwError;
							}

							if(type == "www")      	requestObject.set("url", url);
							else if(type == "pdf") 	requestObject.set("pdf", url);
							else                   	requestObject.add("media", url);

							if(type == "www") 		queryString += "url=" + encodeURIComponent(url);
							else 					queryString += "m[]=" + encodeURIComponent(url);

							if(type != "www")
								requestObject.add("mediaUrls", url);

							return queryString + "&";
						}, "");
					} catch(error) {
						if(error == pdfError)       return fail("You can only specify one PDF file per request.");
						else if(error == wwwError)  return fail("You can only specify one website per request.");
						else if(error == httpError) return fail("You have entered an invalid website URL. Please check and try again.");
						else                        throw error;
					}
				}


				generatedUrl += _.reduce(params.recipients, function (queryString, email) {
					email = email.trim();

					requestObject.add("recipients", email);

					if(queryString != "") queryString += "&";
					queryString += "e[]=" + encodeURIComponent(email);

					return queryString;
				}, "");


				return {
					success: true
				};
			},function HereIserror(e) {
            }).then(function (generatorResult) {
				console.log(generatorResult)
				if(!generatorResult.success) return generatorResult;

				var pdf = requestObject.get("pdf") ? 1 : 0;
				var www = requestObject.get("url") ? 1 : 0;
				var img = requestObject.get("media") ? requestObject.get("media").length : 0;
				var all = pdf + www + img;

				var thumbnail =
					www
						? "https://s3.amazonaws.com/blrt-emails/www-thumbnail.png"
						: (pdf
							? "https://s3.amazonaws.com/blrt-emails/pdf-thumbnail.png"
							: img
								? requestObject.get("mediaUrls")[0]
								: "https://www.blrt.com/images/request-default-thumb.png");
				requestObject
					.set("pdfCount",pdf)
					.set("wwwCount",www)
					.set("imageCount", img)
					.set("thumbnail", thumbnail);

				if(thumbnail && thumbnail.indexOf("http") != 0){
					thumbnail = "http://" + thumbnail;
				}

				console.log("before return ")
				return requestObject.save().then(function (blrtRequest) {

					console.log(JSON.stringify(blrtRequest))
					generatedUrl += "&req=" + _.encryptId(blrtRequest.id);
					generatedUrl = generatedUrl.replace("?&", "?");

					//no, we need to use Yozio link now
					generatedUrl = constants.yozioObj.click_url;
					if(type == "api.blrt.com"){
						generatedUrl+= constants.yozioObj.blrtrequest_api;
					}else{
						generatedUrl+= constants.yozioObj.blrtrequest_web;
					}
					generatedUrl+= "?req=" + _.encryptId(blrtRequest.id);
					if(type != "api.blrt.com"){
						generatedUrl += "&inviter=" + _.encryptId(user.id);
					}else{
						generatedUrl += "&api_user=" + _.encryptId(user.id);
					}

					//end of the link generation

					var waitFollowUps = [];

					console.log("requesteeContact : "+requesteeContact)
					console.log("user.id : "+user.id)
					waitFollowUps.push(requestee
							? Parse.Promise.as(false)
							: NonUser.get({ type: "email", value: requesteeContact, referredBy: user.id }));
					if(type != "api.blrt.com"){
						var  emailToSend = {
				                template: 	(requestee ? "" : "non-") + "user-request",
				                recipients: [{
				                 			name: requestee ? requestee.get("name") : requesteeContact,
				                 			email: requesteeContact,
				                 			isUser: Boolean(requestee),
				                 			user: requestee
				                }],
				                sender:   	user,
				                creator:  	user,
				                customMergeVars: {
				                			"REQUEST_URL": generatedUrl,
				                			"NUM_ITEMS": all,
				                			"NUM_WWWS": www,
				                			"NUM_PDFS": pdf,
				                			"NUM_IMAGES": img,
				                			"REQUEST_TITLE": params.name,
				                			"REQUEST_THUMBNAIL": thumbnail,
				                			"MESSAGE_TO_CREATOR": params.message,
				                }
							};
						if( requesteeContact.toLowerCase() != user.get("email").toLowerCase()){
							emailToSend.cc = {
				                	name: user.get("name"),
				                	email: user.get("email"),
				                	id: user.id,
				                };
						}
						waitFollowUps.push(email.SendTemplateEmail(emailToSend));
					}


					return Parse.Promise.when(waitFollowUps)
					.then(function (nonUser) {
						console.log("here is nonUser")
						console.log(nonUser)
						return Parse.Promise.when([
							user
								.increment("sentBlrtRequestCount")
								.increment("unansweredSentBlrtRequestCount")
								.save(),
							requestee
								? requestee
									.increment("receivedBlrtRequestCount")
									.increment("unansweredReceivedBlrtRequestCount")
									.save()
								: Parse.Promise.as(requestee)
						]).always(function () {
							return {
								success: true,
								link: generatedUrl,
								count: {
									all: all,
									image: img,
									pdf: pdf,
									www: www
								},
								mediaUrls: requestObject.get("mediaUrls"),
								requestId: requestObject.id
							};
						});
					}, function (emailError) {
						console.log("emailError")
						console.log(emailError)
						loggler.global.log(false, "ALERT: Failed to send request email or fetch NonUser:", emailError).despatch();
						return { success: false, error: errors.generic };
					});
				}, function (promiseError) {
					console.log("promiseError")
					console.log(promiseError)
					loggler.global.log(false, "ALERT: Failed to create BlrtRequest object:", promiseError).despatch();
					return { success: false, error: errors.generic };
				});
			});
		}, function (promiseError) {
			loggler.global.log(false, "ALERT: Failed to retrieve user for request generation:", promiseError).despatch();
			return { success: false, error: errors.generic };
		});
}


















//get the amazon signature
Parse.Cloud.define("awsinfo", function(request, response) {
	var result = exports.awsinfo(request.params);
	response.success(JSON.stringify(result));
});

var Crypto = require('crypto');
var Buffer = require('buffer').Buffer;
var accessKeyId = 'AKIAI5CQEURLMVYQIYCQ';
var secret_key = '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';
var bucket = 'blrtrequest';
var host = bucket + ".s3.amazonaws.com";
var keyPrefix = "request/uploads/";
var acl = "public-read";

var crypto = require("crypto")
function createHmacDigest(key, string) {
    var hmac = crypto.createHmac('sha256', key);
    hmac.write(string);
    hmac.end();
    return hmac.read();
}

exports.awsinfo = function(params) {
	var d = new Date();
	    d.setSeconds(0);
	    d.setMilliseconds(0);
	    d.setMinutes(d.getMinutes()+10);
	var date = d.toISOString().replace(/[-\.:]/g,"").replace("000Z","Z");
	var dateString = d.toString().replace("+0000 (UTC)","");
	var dateParts = dateString.split(" ");
	dateString = dateParts[0]+", "+dateParts[2]+" "+dateParts[1]+" "+dateParts[3]+" "+dateParts[4]+" "+dateParts[5];

	var type;
	var contentLength;
	var filename;
	var credential = accessKeyId + "/" + date.substr(0,8) + "/us-east-1/s3/aws4_request";
    type = params.type;

	function s3_upload_policy() {
	    var policy = {"expiration":d.toISOString(),
	                  "conditions":[
	                    {"bucket":bucket},
	                    {"acl":acl},
	                    {"content-type":type},
	                    ["starts-with","$key", keyPrefix],
	                    {"x-amz-credential": credential},
	                    {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
	                    {"x-amz-date": date}
	                  ]};
	    var buf = new Buffer(JSON.stringify(policy),'utf8');
	    var result = buf.toString('base64');

	    return result;
	}

	function s3_upload_signature() {
	    // var h1=  Crypto.createHmac('sha256',"AWS4"+secret_key).update(date.substr(0,8)).digest('binary');
	    // var h2 = Crypto.createHmac('sha256',h1).update("us-east-1").digest('binary');
	    // var h3 = Crypto.createHmac('sha256',h2).update("s3").digest('binary');
	    // var h4 = Crypto.createHmac('sha256',h3).update("aws4_request").digest('binary');
	    // return Crypto.createHmac('sha256',h4).update(s3_upload_policy()).digest('hex');

        var dateKey = createHmacDigest("AWS4" + secret_key, date.substr(0,8));
        var dateRegionKey = createHmacDigest(dateKey, "us-east-1");
        var dateRegionServiceKey = createHmacDigest(dateRegionKey, "s3");
        var signingKey = createHmacDigest(dateRegionServiceKey, "aws4_request");

// sign policy document with the signing key to generate upload signature
        var xAmzSignature = createHmacDigest(signingKey, s3_upload_policy()).toString("hex");
        return xAmzSignature
	}

	filename = new Date().toISOString().replace(/[-\.:]/g,"")+"_"+params.filename;

	contentLength = params.contentLength;
	var result = {
	            "acl":acl,
	            "date":date,
	            "filename": keyPrefix+filename,
	            "dateString": dateString,
	            "credential": credential,
	            "policy":s3_upload_policy(),
	            "signature":s3_upload_signature()};
	return result;
}


