var _ = require('underscore')
var serverConfig = require('../../config/serverConfig');
var webappServer = serverConfig.webappServerUrl;
const logger = console
if(webappServer) {
    function messageClient(info, endpoint) {
        console.log('\n\n\n===messageClient webappServer: '+webappServer);
        console.log('endpoint endpoint endpoint==='+endpoint);
        if(webappServer instanceof Array) {
            webappServer.forEach((url) => {
                return Parse
                    .Cloud
                    .httpRequest({url: `${url}/receiver/${endpoint}`, method: 'POST', body: info})
            })
        }else if(typeof webappServer === 'string') {
            console.log('=== === ===');
            console.log(`${webappServer}/receiver/${endpoint}`);
            // console.log('info: '+JSON.stringify(info));
            return Parse
                .Cloud
                .httpRequest({url: `${webappServer}/receiver/${endpoint}`, method: 'POST', body: info})
                .then(function(httpResponse) {
                    console.log('send request response: '+JSON.stringify(httpResponse));
                    console.log(httpResponse.text);
                }, function(httpResponse) {
                    console.error('Request failed with response code ' + httpResponse.status);
                });
        }
    }

    function getUsersInConversation(conversationId) {
        console.log('===getUsersInConversation===');
        var conversationStub = new Parse.Object("Conversation");
        conversationStub.id = conversationId;
        return new Parse
            .Query("ConversationUserRelation")
            .equalTo("conversation", conversationStub)
            .find({useMasterKey:true})
            .then(function (relations) {
                console.log('\n\n===relations: '+JSON.stringify(relations));
                var usersInConversation = [];
                relations.forEach(function (relation) {
                    //skip nonUser
                    if (relation.get("user")) {
                        usersInConversation.push(relation.get("user").id)
                    }
                })
                return usersInConversation;
            })
    };

    Parse.Cloud
        .afterSave('ConversationItem', function (req, res) {
            console.log('\n\n\n\n===ConversationItem afterSave===');
            var item = req.object;
            var info = {};
            var mediaPromises;
            if (item.get("media")) {
                info.media = []
                mediaPromises = Parse
                    .Promise
                    .when(_.map(item.get("media"), function (asset) {
                        // console.log(asset)
                        return asset
                            .fetch()
                            .then(function (fullMedia) {
                                return info
                                    .media
                                    .push({
                                        id: fullMedia.id,
                                        encryptType: fullMedia.get("encryptType"),
                                        encryptValue: fullMedia.get("encryptValue"),
                                        mediaFile: fullMedia
                                            .get("mediaFile")
                                            .url(),
                                        mediaFormat: fullMedia.get("mediaFormat"),
                                        pageArgs: fullMedia.get("pageArgs")
                                    });
                            })
                    }))
            }
            //
            var userStub = new Parse.Object('_User');
            userStub.id = item
                .get("creator")
                .id;

            Parse
                .Promise
                .when(userStub.fetch({useMasterKey: true}), getUsersInConversation(item.get('conversation').id), mediaPromises)
                .then(function (creator, users, media) {
                    info.index = item.get("index");
                    info.user = {};
                    info.id = _.encryptId(item.id);
                    // var creator=item.get("creator");
                    info.user.name = creator.get("name") || creator.get("username");
                    info.localGuid = item.get("localGuid");
                    info.conversationPlainedId = item.get("conversation").id;
                    info.conversationId = _.encryptId(item.get("conversation").id);
                    switch (item.get("type")) {
                        //readable types
                        case 0:
                        case 2:
                        case 3:
                        case 7:
                        case 8:
                            var readableIndex = item.get("readableIndex");
                            info.user.id = creator.id;
                            info.createdAt = item.createdAt;
                            info.user.avatar = creator.get("avatar") && creator
                                    .get("avatar")
                                    .url();
                            // info.user.name = creator.get("name") || creator.get("username");
                            info.read = false;
                            info.readableIndex = item.get("readableIndex");
                            break;
                    }
                    switch (item.get("type")) {
                        case 3:
                            info.text = JSON
                                .parse(item.get("argument"))
                                .Comment;
                            info.type = "comment"
                            break;
                        case 0:
                        case 2: 
                            info.type = "blrt";
                            info.isPublicBlrt = item.get("isPublicBlrt") || false;
                            info.publicName = item.get("publicName");
                            info.duration = JSON.parse(item.get("argument")).AudioLength;
                            // calculate the pageNumber of media used in blrt
                            let pNo = 0;
                            JSON.parse(item.get("argument")).Items.map(ii=>{
                                if(ii.PageString == '0' || ii.PageString == '1') { //image or one page pdf
                                    pNo++;
                                }else { //pdf
                                    let res = ii.PageString.split('-');
                                    console.log('\n\n\nres res res==='+Number(res[1]));
                                    pNo = pNo + Number(res[1]);
                                }
                            });
                            info.pageNumber = pNo;
                            // the blrt created from webrecorder may not have a thumbnail,use the
                            // conversation thumbnail as a tmp solution
                            info.thumbnail = item.get("thumbnailFile") && item
                                    .get("thumbnailFile")
                                    .url();

                            //up here ,for webplayer
                            info.touchDataFile = item.get("touchDataFile") && item
                                    .get("touchDataFile")
                                    .url();
                            info.audioFile = item.get("audioFile") && item
                                    .get("audioFile")
                                    .url();
                            info.argument = item.get("argument");
                            info.encryptValue = item.get("encryptValue");
                            info.encryptType = item.get("encryptType");
                            break;
                        case 4:
                            info.argument = JSON.parse(item.get("argument"));
                            info.type = "addEvent";
                            break;
                        case 5:
                            info.argument = JSON.parse(item.get("argument"));
                            info.type = "message";
                            // Decide if the blrtRequest processed or not
                            info.status = item.get("status");
                            break;
                        case 7:
                            info.type = 'audio';
                            info.length = JSON
                                .parse(item.get("argument"))
                                .AudioLength;
                            info.audioFile = item.get("audioFile") && item
                                    .get("audioFile")
                                    .url();
                            info.encryptType = item.get('encryptType');
                            info.encryptValue = item.get('encryptValue');
                            break;
                        case 8:

                            const arg = JSON.parse(item.get('argument'))
                            info.pageNumber = arg.PageCount
                            info.type = arg.MediaType === "PDF" ? 'pdf' : "images"
                            info.thumbnails = item.get("media").map(m => m.get('thumbnailFile') && m.get('thumbnailFile').url())
                            //up here ,for webplayer
                            info.media = _.map(item.get("media"), function (asset) {
                                return {
                                    id: asset.id,
                                    encryptType: asset.get("encryptType"),
                                    encryptValue: asset.get("encryptValue"),
                                    mediaFile: asset
                                        .get("mediaFile")
                                        .url(),
                                    mediaFormat: asset.get("mediaFormat"),
                                    pageArgs: asset.get("pageArgs"),
                                    name: asset.get("name")
                                };

                            });
                    }
                    return messageClient({
                        info: JSON.stringify(info),
                        users: JSON.stringify(users)
                    }, 'ConversationItem')
                })
                .then(function (r) {
                    res.success(r.data)
                })
                .fail(function (e) {
                    res.error(e)
                })
        });

    //Notify the online users when a NEW conversationUserRelation created or update
    Parse.Cloud
        .afterSave('ConversationUserRelation', function (req, res) {
            // var item = req.object; req.start();
            console.log('\n\n\n===ConversationUserRelation afterSave===');
            console.log(req.user);
            var conversationUserRelation = req.object;
            var conversationId = conversationUserRelation.get("conversation").id;
            console.log('conversationId==='+conversationId);
            getUsersInConversation(conversationId).then(function (usersInConversation) {
                console.log('\n\n===usersInConversation: '+JSON.stringify(usersInConversation));
                return messageClient({
                    users: JSON.stringify(usersInConversation)
                }, 'ConversationUserRelation')
            }).then(function (r) {
                res.success(r.data)
            }).fail(function (e) {
                res.error(e)
            })
        });
    
    // for account deletion
    Parse.Cloud
        .afterSave('UserContactDetails', (req, res)=>{
            console.log('\n\n\n===UserContactDetails afterSave===');
            let deletedUser = req.object;
            console.log('\n\ndeletedUser: '+JSON.stringify(deletedUser));
            // console.log('\n\ndeletedUser: '+deletedUser.get('user').id);
            let info = {};
            info.id = deletedUser.get('user').id;
            if(deletedUser.get('contactStatus') === 'deleted') {
                console.log('\n\n=== === ===deleted');
                messageClient({
                    info: JSON.stringify(info),
                }, 'UserContactDetails')
            }
            res.success();
    });

    //handle rename
    Parse.Cloud
        .beforeSave('Conversation', function (req, res) {
            console.log('\n\n\n===Conversation afterSave===');
            var conversation = req.object;
            var conversationId = conversation.id;
            const dirtyKeys = conversation.dirtyKeys()
            if(dirtyKeys.indexOf('blrtName') && !conversation.isNew()) {
                var info = {
                    newConvo: true
                }
                getUsersInConversation(conversationId).then(function (usersInConversation) {
                    return messageClient({
                        users: JSON.stringify(usersInConversation),
                        info: JSON.stringify(info)
                    }, 'Conversation')
                }).then(function (r) {
                        // logger.log("message success")
                        // logger.log(JSON.stringify(r))
                        // res.success()
                    })
                    .fail(function (e) {
                        // logger.log("message fail")
                        // logger.log(JSON.stringify(e))
                        // res.error(e)
                    })
            }

            res.success()
        });
}
