var constants = require("cloud/constants.js");
var util = require("cloud/util.js");
var loggler = require("cloud/loggler.js");
var email = require("cloud/email.js");
var permissionsJS = require("cloud/permissions.js");
var _ = require("underscore");

// Gets all contact details for the user performing the request
// This means a user is required to be logged in to do this
Parse.Cloud.define("getContactDetails", function(req, res) {
	var l = loggler.create("function:getUserContactDetails");
	l.log("Request provided:", req, "User ID:", req.user != null ? req.user.id : null);

	if(req.user == null) {
		l.log(false, "No user in request, aborting").despatch();
		return res.error("No user in request, aborting");
	}
		
	var query = new Parse.Query(constants.contactTable.keys.className)
		.equalTo(constants.contactTable.keys.user, req.user);

	return query.find({
		success: function(results) {
			l.log("Results found:", results);

			var output = [];
			
			for (var i = 0; i < results.length; ++i) {
				var record = results[i];

				var userTmp = {
					id: 			record.id,
					user: 			record.get(constants.contactTable.keys.user).id,
					type: 			record.get(constants.contactTable.keys.type),
					value: 			record.get(constants.contactTable.keys.Value),
					verified: 		record.get(constants.contactTable.keys.verified),
					placeholder: 	record.get(constants.contactTable.keys.placeholder)
				};

				output.push(userTmp);
			}
		
			l.log(true, output).despatch();
			return res.success(output);
		},
		error: function (error) {
			l.log(false, "query.find() error:", error).despatch();
			res.error("query.find() error: " + JSON.stringify(error));
		}
	});
});

Parse.Cloud.define("getSessionTokenForCurrentUser", function(req, res) {
	if(!req.user)
		res.error("No user logged in");
	console.log(JSON.stringify(req.user));
	return res.success(req.user._sessionToken);
});