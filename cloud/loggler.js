var constants = require("cloud/constants.js");
var util = require("cloud/util.js");
var _ = require("underscore");

var maxBulkSize = 5000000;
var maxLogSize = 1000000;

var redactText = "<...redacted>";

var tags = [constants.appProtocol, "cc-" + constants.appProtocol, "cc"];
var endpoint = "http://logs-01.loggly.com/bulk/" + constants.logglyToken + "/tag/" + tags.join() + "/";

var globalOrder = 0;

exports.create = function (id, options) {
	options = _.extend({
		skipInitial: false
	}, options || {}); 

	var loggler = Object.create(Loggler);
	loggler.id = id;
	loggler.rand = Math.round(Math.random() * 10000);

	if(!options.skipInitial)
		loggler.log("Loggler created");

	return loggler;
}

var logs = [];

var Loggler = {
	log: function () {
		if(this.id == null)
			throw "Use create(id) to instantiate a Loggler.";

		if(arguments.length == 0)
			return this;

		var msg = "";

		var data = {
			timestamp: new Date(),
			caller: this.id,
			rand: this.rand,
			version: constants.cloudVersion,
			glob: globalOrder++
		};

		for(var arg_i = 0; arg_i < arguments.length; ++arg_i) {
			var arg = arguments[arg_i];


			if(arg_i == 0 && typeof arg == "boolean") {
				data.success = arg;
				continue;
			}


			if(arg == null) {
				msg += "<null>" + "\n";
				continue;
			}

			if(typeof arg == "object")
				arg = JSON.stringify(arg);
			else if(typeof arg != "string")
				arg = arg.toString();

			arg = arg.trim();

			if(arg == "") {
				msg += "<empty string>" + "\n";
				continue;
			}

			msg += arg + "\n";
		}

		if(msg == "" && data.success == null)
			return this;
		else
			msg = msg.slice(0, -1);

		data.msg = msg;

		try{
			console.log("___LGLR___" + this.id + "|" + this.rand + "-" + data.glob + ": " + msg);
		}catch(e){}

		data = JSON.stringify(data);
		logs.push(data);

		return this;
	},
	despatch: function () {
		while(logs.length != 0) {
			var output = "";
			var size = 0;

			while(logs.length != 0 && size <= maxBulkSize) {
				var log = logs[0].replace(/\r?\n/gm, "\\n").trim();
				if(log == "") {
					logs.shift();
					continue;
				}
				log += "\n";

				var logSize = util.byteLength(log);
				if(logSize >= maxLogSize)
					log = log.substr(0, maxLogSize - util.byteLength(redactText) - 1) + redactText;
				logSize = util.byteLength(log);
				if(size + logSize >= maxBulkSize)
					break;

				logs.shift();

				output += log;
			}

			if(output == "")
				break;

			Parse.Cloud.httpRequest({
				method: 'POST',
				headers: {
					'Content-Type': 'text/plain',
				},
				url: endpoint,
				body: output
			}).then(function () {

			}, function (error) {
				console.log("Failed to despatch logs: " + JSON.stringify(error));
			});
		}
	}
};

exports.global = exports.create("__global", { skipInitial: true });