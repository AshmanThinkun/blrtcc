var constants = require("cloud/constants.js");
var util = require("cloud/util.js");
var loggler = require("cloud/loggler.js");

var k_b = constants.base.keys;
var k_p = constants.permissions.keys;
var k_u = constants.user.keys;
var k_cur = constants.convoUserRelation.keys;

var user_className = "object";
var broken_className = "_broken";

/*
 params: [
 {
 user: Parse.User | false,
 other: Parse.Object,

 // If the other object can't be sent (i.e. JSON issues.. stupid Parse)
 otherClassName: "ConversationUserRelation",
 otherObjectId: "zud67faw83",

 permissions = { // Optional - skips API calls if possible
 // other != null && user !== false
 user: [Parse.Object("Permissions")],
 accountType: [Parse.Object("Permissions")],

 other: [Parse.Object("Permissions")],

 // permissions.other is convoUserRelation
 convo: [Parse.Object("Permissions")],
 convoCreator: [Parse.Object("Permissions")],
 convoCreatorAccountType: [Parse.Object("Permissions")],
 },
 extraInfo = { // May be required in conjunction with permissions (above) to skip API calls
 // user
 accountThreshold = [1650, 1500],
 accountName = "Preeeemium"
 }
 }
 ]
 // If params.permissions is fully populated and the required extraInfo is provided (if any), API calls can be skipped
 */

/* RETURNS
 {
 "className-objectId": <variant>
 // forCC: one record (compilation results)
 // !forCC: array of records
 }
 */
Parse.Cloud.define("getPermissions", function(req, res) {
	return exports.getPermissions(req.params.objects, false).then(function (permissions) {
		return res.success(permissions);
	}, function (error) {
		return res.error(error);
	});
});

exports.getSinglePermissions = function (req, forCC) {
	return exports.getPermissions([req], forCC).then(function (permissions) {
		return permissions[Object.keys(permissions)[0]];
	});
};

//TODO [Guichi] clean up this logic ,it seems [User Conversation ConversationUserRelation] tables all 
//TODO has a pointer to permission table ,but I can not find such pointers in Conversation and ConversationUserRelation
//TODO only User table remain the pointer ,maybe the logic and database already changed ,but not the code !!!!!
exports.getPermissions = function (params, forCC) {

	var l = loggler.create("getPermissions");

	forCC = typeof forCC !== "undefined" ? forCC : false;


	// Make sure we've got something to look forward to
	if(typeof params !== "object" || params.length == 0) {
		l.log(false, "An array with at least one param object must be supplied, received:", params).despatch();
		return Parse.Promise.error("An array with at least one param object must be supplied, received: " + JSON.stringify(params));
	}

	// This function returns a promise to the caller
	return Parse.Promise.as().then(function () {
		//TODO [Guichi] what is otherObject ?
		// If otherClassName/otherObjectId we need to convert them into a blank new class to use it here
		
        console.log('\n\n\nparams: '+JSON.stringify(params));
		class_ifyParams(params);

		var groups = groupPermissions(params); // Split the params into groups based on their "other" className
		console.log('\n\n\n===groups: '+JSON.stringify(groups));

		var className = Object.keys(groups); // Will be individualised soon
        


		//TODO [Guichi] each object has a permission ? It seems conversationUserRelation has its own permission in ancient implementation
		if(className.length > 1) {
			// Multiple groups, recurse this function for each individual group
			var results = [];

			for(var group in groups)
				results.push(exports.getPermissions(groups[group], forCC));

			// Bring the results back together
			return util.mergeArray(false, results);
		}else className = className[0];

		// Bad arguments? Run away
		if(className == broken_className) {
			l.log(false, "Found broken params!").despatch();
			return { broken_className: Parse.Promise.error("Some broken params were found: " + JSON.stringify(params)) };
		}


		var promise;

		console.log('\n\n\n===className==='+className);

		// If they've provided us with enough information to skip the query, go with that
		var precomputed = precomputedPermissions(params, className, forCC);
		console.log('\n\n\n===precomputed: '+JSON.stringify(precomputed));

		if(precomputed !== false) {
			promise = Parse.Promise.as(precomputed);
		} else {
			// To compute the result properly, we need to know which permissions need the user's and which don't
			// To do this we need objectIds from the parameters
			// This will also make it easier to get all of the objectIds for our query
			var objectifiedParams = objectifyParams(params, className);
			console.log('\n\n\n===objectifiedParams: '+JSON.stringify(objectifiedParams)+'\nclassName==='+className+'\nparams: '+JSON.stringify(params));

			// Otherwise fetch the query based on which class we're looking for
			var permissionsQuery = getPermissionsQuery(className)
				.containedIn(k_b.objectId, getObjectIds(params, className));



			promise = permissionsQuery.find({useMasterKey:true}).then(function (results) {
				// Use the query results and the className to compute the permissions we need
                // console.log('\n\n\npermissionsQuery results: '+JSON.stringify(results));
				return computePermissions(results, className, objectifiedParams);
			}, function (error) {
				l.log(false, "Failed to query.find() with className: " + className + " and error:", error).despatch();
				return Parse.Promise.error("Failed to query.find() with className: " + className + " and error: " + JSON.stringify(error));
			});
		}


		return promise.then(function (permissions) {
			console.log('\n\n\n=== === ===');
			/*
			 permissions = {
			 "className-objectId": {
			 <extraInfoFields>: <extraInfoValues>,
			 <extraInfoFields>: <extraInfoValues>,
			 <extraInfoFields>: <extraInfoValues>,
			 permissions: [Parse.Object("Permissions")]
			 }
			 }
			 */
			// Convert permissions to records (which can be JSON deserialised in-app)
			for(var object in permissions) {
				var perm = permissions[object];
                console.log()
				perm[k_p.permissions] = processPermissionsRecords(perm[k_p.permissions]);

				// Cloud code wants compiled records (what the permissions stack evaluates to), and nothing else
				if(forCC)
					permissions[object] = compileRecords(perm[k_p.permissions]);
			}

			l.log(true).despatch();

            // console.log('\n\n\npermissions: '+JSON.stringify(permissions));
			return permissions;
		});
	});
};

function class_ifyParams (params) {
	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];

		if(param.otherClassName == null || param.otherObjectId == null)
			continue;

		param.other = new Parse.Object(param.otherClassName);
		param.other.id = param.otherObjectId;
	}
}

function groupPermissions (params) {
	/* RETURNS
	 groups = {
	 "className": params,
	 "object": params,
	 "_broken": params
	 }
	 */
	var groups = {};

	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];
		var other = param.other;

		var key;

		if(other != null)
			key = other.className;
		else if(param.user != null)
			key = user_className;
		else
			key = broken_className;

		if(!(key in groups))
			groups[key] = [];

		groups[key].push(param);
	}

	return groups;
}

// Returns results in the same format that computePermissions would, or false if not enough information is provided
function precomputedPermissions(params, className, forCC) {
	switch(className) {
		case k_cur.className:
			return precomputedConvoUserRelationPermissions(params, className, forCC);
		case user_className:
			return precomputedUserPermissions(params, className, forCC);
	}
}

function precomputedConvoUserRelationPermissions(params, className, forCC) {
	var computed = {};

	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];
		var perm = param[k_p.permissions];

		if(perm == null
			// We use "in" because they can be specified as null (if no permissions exist for it)
			|| !("other" in perm)
			|| !("convo" in perm)
			|| !("convoCreator" in perm)
			|| !("convoCreatorAccountType" in perm)
		)
			return false;

		if(param.user !== false
			&& (!("user" in perm) || !("accountType" in perm))
		)
			return false;

		var object = param.extraInfo || {};
		var newPerms = [];

		newPerms.push.apply(newPerms, perm.other);
		newPerms.push.apply(newPerms, perm.convo);
		newPerms.push.apply(newPerms, perm.convoCreator);
		newPerms.push.apply(newPerms, perm.convoCreatorAccountType);
		if(param.user !== false) {
			newPerms.push.apply(newPerms, perm.user);
			newPerms.push.apply(newPerms, perm.accountType);
		}

		object[k_p.permissions] = newPerms;

		computed[className + "-" + perm.other.id] = object;
	}

	return computed;
}

function precomputedUserPermissions(params, className, forCC) {
	var computed = {};

	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];
		var perm = param[k_p.permissions];

		if(perm == null || !("user" in perm) || !("accountType" in perm))
			return false;

		var extra = param.extraInfo;

		if(!forCC && (extra == null || extra[k_p.accountThreshold] == null || extra[k_p.accountName] == null))
			return false;

		var object = extra || {};
		var newPerms = [];

		newPerms.push.apply(newPerms, perm.user);
		newPerms.push.apply(newPerms, perm.accountType);

		object[k_p.permissions] = newPerms;

		computed[className + "-" + perm.user.id] = object;
	}

	return computed;
}

function objectifyParams(params, className) {
	var result = {};

	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];

		result[className + "-" + param[className == user_className ? "user" : "other"].id] = param;
	}

	return result;
}

function getObjectIds(params, className) {
	console.log('\n\n\n===getObjectIds params: '+JSON.stringify(params));
	console.log('className==='+className);
	var ids = [];

	for(var p_i = params.length - 1; p_i >= 0; --p_i) {
		var param = params[p_i];

		ids.push(param[className == user_className ? "user" : "other"].id);
	}
    console.log('\n\n\n===getObjectIds ids==='+ids);
	return ids;
}

function computePermissions(queryResults, className, objectifiedParams) {
	switch(className) {
		case k_cur.className:
			return computeConvoUserRelationPermissions(queryResults, className, objectifiedParams);
		case user_className:
			return computeUserPermissions(queryResults, className, objectifiedParams);
	}
}

function computeConvoUserRelationPermissions(convoUserRelations, className, objectifiedParams) {
	var computed = {};

	for(var cur_i = convoUserRelations.length - 1; cur_i >= 0; --cur_i) {
		var convoUserRelation = convoUserRelations[cur_i];

		var id = className + "-" + convoUserRelation.id;
		var object = objectifiedParams[id].extraInfo || {};

		var newPerms = [];

		var perm = convoUserRelation.get(k_b.permissions);
		if(perm != null) newPerms.push.apply(newPerms, perm);

		var convoCreator = convoUserRelation.get(k_cur.convoCreator);
		if(convoCreator != null) {
			perm = convoCreator.get(k_b.convoCreatorPermissions);
			if(perm != null) newPerms.push.apply(newPerms, perm);

			var accountType = convoCreator.get(k_u.activeAccountType);
			if(accountType != null) {
				perm = accountType.get(k_b.convoCreatorPermissions);
				if(perm != null) newPerms.push.apply(newPerms, perm);
			}
		}

		perm = convoUserRelation.get(k_cur.conversation).get(k_b.permissions);
		if(perm != null) newPerms.push.apply(newPerms, perm);

		if(objectifiedParams[id].user !== false) {
			var user = convoUserRelation.get(k_cur.user);
			if(user != null) {
				perm = user.get(k_b.permissions);
				if(perm != null) newPerms.push.apply(newPerms, perm);

				var accountType = user.get(k_u.activeAccountType);
				if(accountType != null) {
					perm = accountType.get(k_b.permissions);
					if(perm != null) newPerms.push.apply(newPerms, perm);
				}
			}
		}

		object[k_p.permissions] = newPerms;
		computed[id] = object;
	}

	return computed;
}

//TODO [Guichi] still in use I think
function computeUserPermissions(users, className, objectifiedParams) {

	var computed = {};

	for(var user_i = users.length - 1; user_i >= 0; --user_i) {
		var user = users[user_i];

		var id = className + "-" + user.id;
		var object = objectifiedParams[id].extraInfo || {};

		var newPerms = [];

		//TODO [Guichi] not exist
		var perm = user.get(k_b.permissions);
		if(perm != null) newPerms.push.apply(newPerms, perm);

		var accountType = user.get(k_u.activeAccountType);
		if(accountType != null) {
			perm = accountType.get(k_b.permissions);
			if(perm != null) newPerms.push.apply(newPerms, perm);

			object[k_p.accountThreshold] = accountType.get(constants.accountType.keys.Threshold);
			object[k_p.accountName] = accountType.get(constants.accountType.keys.MarketingName);
		}

		object[k_p.permissions] = newPerms;

		computed[id] = object;
	}

	return computed;
}

function getPermissionsQuery(className) {
	switch(className) {
		case k_cur.className:
			return getConvoUserRelationPermissionsQuery();
		case user_className:
			return getUserPermissionsQuery();
	}
}

function getConvoUserRelationPermissionsQuery() {
	var query = new Parse.Query(k_cur.className)
		.include(k_b.permissions)
		.include(k_cur.conversation)
		.include(k_cur.conversation + "." + k_b.permissions)
		.include(k_cur.convoCreator)
		.include(k_cur.convoCreator + "." + k_b.convoCreatorPermissions)
		.include(k_cur.convoCreator + "." + k_u.activeAccountType)
		.include(k_cur.convoCreator + "." + k_u.activeAccountType + "." + k_b.convoCreatorPermissions)
		.include(k_cur.user)
		.include(k_cur.user + "." + k_b.permissions)
		.include(k_cur.user + "." + k_u.activeAccountType)
		.include(k_cur.user + "." + k_u.activeAccountType + "." + k_b.permissions);

	return query;
}

function getUserPermissionsQuery() {
	return new Parse.Query(Parse.User)
		.include(k_b.permissions)
		.include(k_u.activeAccountType)
		.include(k_u.activeAccountType + "." + k_b.permissions);
}

function processPermissionsRecords (records) { // Plural
	var result = [];

	for(var rec_i = records.length - 1; rec_i >= 0; --rec_i)
		result.push(processPermissionsRecord(records[rec_i]));

	return result;
}

function processPermissionsRecord (record) { // Singular
	var result = {};

	result[k_p.stackLevel] = record.get(k_p.stackLevel) || 0;

	var values = {};

	for(var key in constants.permissions.values) {
		var field = constants.permissions.values[key];

		if(record.get(field) != null)
			values[field] = record.get(field);
	}

	result[constants.permissions.keys.values] = values;

	return result;
}

// Returns a single record which is the result of compiling the provided array of records based on their stackLevels
function compileRecords (records) {
	records.sort(function (a, b) { return a[k_p.stackLevel] - b[k_p.stackLevel]; });

	// console.log('/n/n/n===compileRecords sorted records: '+JSON.stringify(records));

	var compiled = {};

	var recordsLength = records.length;

	for(var rec_i = 0; rec_i < recordsLength; ++rec_i) {
		var record = records[rec_i];
		// console.log('\n\n\n===record: '+JSON.stringify(record));
		var values = record[k_p.values];

		for(var key in values)
			compiled[key] = values[key];
	}

	return compiled;
};