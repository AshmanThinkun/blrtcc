
// var _ = require("underscore");
// var constants = require("cloud/constants");
// var loggler = require("cloud/loggler");
// var Mixpanel = require("cloud/mixpanel");
//
// const rootUrl = "http://api.yozio.com/v2.0/";
//
// //metaData is an object
// exports.GenerateLinkForConvo = function(url, metaData){
// 	var iphone_deeplink_url = constants.appProtocol + "://" +  url;
// 	var android_deeplink_url = _.androidLink(constants.appProtocol + "://" + url);
//
// 	var name = require('crypto').createHash('md5').update(url).digest("hex");
//
// 	if(metaData && metaData.sendTo){
// 		name = metaData.sendTo + "-" + name;
// 	}
//
// 	var params = {
// 		app_key: constants.yozioKey,
// 		method: "link.create",
// 		name: name,
// 		disable_meta_data_passing_to_web_url: "true",
//
// 		override_default_fallback_settings: "true",
// 		fallback_url: "http://"+url,
//
// 		override_default_iphone_settings: "true",
// 		has_iphone_app: "true",
// 		enable_iphone_deeplink: "true",
// 		iphone_use_custom_download_url: constants.appStoreUrl,
// 		iphone_deeplink_url: iphone_deeplink_url,
//
// 		override_default_ipad_settings: "true",
// 		has_ipad_app: "true",
// 		enable_ipad_deeplink: "true",
// 		ipad_use_custom_download_url: constants.appStoreUrl,
// 		ipad_deeplink_url: iphone_deeplink_url
// 	};
//
// 	if(metaData){
// 		params.meta_data = metaData;
// 	}
//
// 	var paramsString = serialize(params);
//
// 	return Parse.Cloud.httpRequest({
// 		url: rootUrl+"?"+paramsString,
// 		headers: {
// 		    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0'
// 		  }
// 	}).then(function(httpResponse) {
// 		var resObj = JSON.parse(httpResponse.text);
//         loggler.global.log(true, "yozio link generated:", resObj.body.short_url).despatch();
// 		return "http://r.yoz.io/" +resObj.body.short_url;
// 	}, function(httpResponse) {
//         loggler.global.log("ALERT: yozio Request failed with url: "+rootUrl+"?"+paramsString+"\nresponse: "+ JSON.stringify(httpResponse)).despatch();
// 	});
// }
//
// exports.GenerateSubLink = function(superLink, data){
// 	console.log(superLink+" : "+data)
// 	var params = {
// 		app_key: constants.yozioKey,
// 		method: "sub.link.create",
// 		short_url: superLink,
// 		meta_data: data
// 	};
//
// 	var paramsString = serialize(params);
//
// 	return Parse.Cloud.httpRequest({
// 		url: rootUrl+"?"+paramsString,
// 		headers: {
// 		    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0'
// 		  }
// 	}).then(function(httpResponse) {
//
// 		var resObj = JSON.parse(httpResponse.text);
// 		console.log("## "+JSON.stringify(resObj))
//         loggler.global.log(true, "yozio subLink generated:", resObj.body.sub_link).despatch();
// 		return "http://r.yoz.io/" +resObj.body.sub_link;
// 	}, function(httpResponse) {
//         loggler.global.log("ALERT: yozio Request failed with url: "+rootUrl+"?"+paramsString+"\nresponse: "+ JSON.stringify(httpResponse)).despatch();
// 	});
// }
//
// Parse.Cloud.define("CreateSublink", function(req, res) {
// 	return exports.GenerateSubLink(req.params.superLink, req.params.metadata)
// 		.then(function(result){
// 			return res.success(result);
// 		}, function(error){
// 			return res.error(error);
// 		});
// });
//
// Parse.Cloud.define("TrackURLClick", function(req, res) {
// 	return exports.TrackURLClick(req.params.url, req.user? req.user.id : req.params.trackingId)
// 		.then(function(result){
// 			return res.success(result);
// 		}, function(error){
// 			return res.error(error);
// 		});
// });
//
// exports.TrackURLClick = function(url, userId){
// 	if(!userId)
// 		userId = "UnsignedUser: not specified";
// 	//pop all properties
// 	var properties = {
// 		url : url
// 	};
// 	if(url.indexOf("://")!=-1){
// 		properties.protocol = url.substr(0,url.indexOf("://"));
// 	}
//
// 	var splitArr = url.split("?");
// 	var queryString = "";
// 	if(splitArr.length>1){
// 		queryString = splitArr[1];
// 	}
//
// 	var kvs = queryString.split("&");
// 	_.each(kvs,function(value){
// 		var kvArr = value.split("=");
// 		var valueForKey = true;
// 		if(kvArr.length>0){
// 			valueForKey = decodeURIComponent(kvArr[1]);
// 		}
// 		properties[kvArr[0]] = valueForKey;
// 	});
//
//
// 	var mp = new Mixpanel({ batchMode: false });
// 	return mp.track({
//         event: "URL clicked",
//         properties: properties
//     }, { id: userId });
// }
//
//
// function serialize (obj, prefix) {
//   var str = [];
//   for(var p in obj) {
//     if (obj.hasOwnProperty(p)) {
//       var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
//       str.push(typeof v == "object" ?
//         serialize(v, k) :
//         k + "=" + v);
//     }
//   }
//   return str.join("&");
// }

