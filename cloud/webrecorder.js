var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var Buffer = require('buffer').Buffer;
var crypto = require('crypto');
var _ = require("underscore");
var run=require("../lib/run")

function getConvoId(req) {
    if ('string' == typeof req.params.convoId) {
        return Parse.Promise.as(_.decryptId(req.params.convoId));
    } else if ('string' == typeof req.params.blrtId) {
        var blrtId = _.decryptId(req.params.blrtId);
        return new Parse.Query("ConversationItem")
            .equalTo("objectId", blrtId)
            .first()
            .then(function(blrtItem) {
                if(!blrtItem){
                    return Parse.Promise.error("Conversation item id not found");
                }
                var convo = blrtItem.get("conversation");
                return convo.id;
            });
    } else {
        return Parse.Promise.error("Invalid parameters");
    } 
}

Parse.Cloud.define('getWebrecorderRecordData', function(req, res) {
	return getConvoId(req).then(exports.getRecordData).then(function (recordData) {
		return res.success(JSON.stringify({ success: true, recordData: recordData }));
	}, function (error) {
		return res.success(JSON.stringify({ success: false, message: "Failed to get recordData." }));
	});
});

function filterBlrtCreationResponse(apiResponse, res) {
    if (apiResponse) {
        try {
            var resObj = JSON.parse(apiResponse);
            if (true === resObj.success) {
                for (var i = 0; i < resObj.objects.length; ++i) {
                    var obj = resObj.objects[i];
                    switch (obj.type) {
                        case "ConversationItem":
                        case "Conversation":
                        case "ConversationUserRelation":
                            resObj.objects[i].id = _.encryptId(obj.id);
                            break;
                        default:
                            resObj.objects[i] = null;
                            break;
                    }
                }
                resObj.objects = _.compact(resObj.objects);
                return res.success(JSON.stringify(resObj));
            }
        } catch (e) {}
    }
    return res.success(apiResponse);
}

Parse.Cloud.define('webrecorder_conversationItemCreate', function(req, res) {
    try {
        req.params.data.conversationId = _.decryptId(req.params.data.conversationId);
    } catch (e) {
        return res.success(JSON.stringify({ success: false, message: "Invalid parameters" }));
    }
    Parse.Cloud.run("conversationItemCreate", req.params, util.reqToCloudFuncOptions(req)).then(function(response) {
        return filterBlrtCreationResponse(response, res);
    }, function(error) {
        return res.error(error);
    });
});

Parse.Cloud.define('webrecorder_conversationCreateWithBlrt', function(req, res) {
    Parse.Cloud.run("conversationCreateWithBlrt", req.params, util.reqToCloudFuncOptions(req)).then(function(response) {
        return filterBlrtCreationResponse(response, res);
    }, function(error) {
        console.log(error)
        return res.error(error);
    });
});


Parse.Cloud.define('getWebrecorderPermissions', function(req, res) {
    if (req && req.user) {
        return getConvoId(req).then(function(convoId) {
            var convo = new Parse.Object("Conversation");
            convo.id = convoId;
            return new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.conversation, convo)
                .first()
                .then(function (result) {
                    var params = {
                        objects: [
                            {
                                otherClassName: "ConversationUserRelation",
                                otherObjectId: result.id,
                                user: false
                            }
                        ]
                    }
                    return Parse.Cloud.run("getPermissions", params, util.reqToCloudFuncOptions(req));
                })
                .then(function (permissions) {
                    return res.success(JSON.stringify({ success: true, permissions: permissions }));
                })
                .fail(function (error) {
                    console.error("cannot find permissions for webrecorder" + JSON.stringify(error));
                    return res.success(JSON.stringify({ success: false, message: "error when getting permissions" }));
                });
        });
    } else {
        return res.success(JSON.stringify({ success: false, message: "User not logged in." }));
    }
});

exports.getRecordData = function(convoId) {
	if (!convoId || typeof convoId != 'string') {
		return Parse.Promise.error("invalid convo Id");
	}
	return new Parse.Query("Conversation")
        .equalTo("objectId", convoId)
        .include("lastMedia")
        .first()
        .then(function(convo) {
        	if(!convo){
                return Parse.Promise.error("Conversation id not found");
            }
            var outputObj = {
            	convoId: _.encryptId(convoId),
            	argument: convo.get("lastMediaArgument"),
                media: _.map(convo.get("lastMedia"), function(asset){
                    return {
                        id: asset.id,
                        encryptType: asset.get("encryptType"),
                        encryptValue: asset.get("encryptValue"),
                        mediaFile: asset.get("mediaFile").url(),
                        mediaFormat: asset.get("mediaFormat"),
                        pageArgs: asset.get("pageArgs")
                    };
                })
            };
            var IV = util.generateRandomNumber();
            var t = JSON.stringify(outputObj);
            var outputData = new Buffer(t);
            var cipher = crypto.createCipheriv('aes-256-cbc', constants.PageEmbededDataSecretKey, IV);
            var encryptedData = Buffer.concat([cipher.update(outputData), cipher.final()]);
            return { v: 1, k: IV.toString('base64'), d: encryptedData.toString('base64') }; // here we call the IV 'k' to obfuscate
        });
}


exports.getEmbeddedRecordData = function(id, isConvo) {
    if (isConvo) {
        return {c: id};
    } else {
        return {b: id};
    }
}

