var constants 		= require('cloud/constants.js');
var Maintenance 	= require('cloud/maintenance.js');
var loggler 		= require('cloud/loggler.js');
var permissionsJS 	= require("cloud/permissions.js");
var util 			= require("cloud/util.js");
var _ 				= require("underscore");

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};

Parse.Cloud.define("getSettings", function(request, response) {
	return exports.GetSettings(request).then(
		function (result) {
			console.log('\n\nGetSettings result: '+JSON.stringify(result));
			return response.success(JSON.stringify(result));
		},
		function (error) {
		    console.log("I know error goes here")
            // console.log(JSON.stringify(error))
			return response.error(error);
		});
});



exports.GetSettings = function (request, forCC, ccOS) {
	forCC = typeof forCC !== "undefined" ? forCC : false;

	return Parse.Promise.as(null).then(function () {
		var version = null;
		var os = null;
		var osVersion = null;
		var device = null;
		var langArray = null;
		var locale = null;
		var appName = null;

		if(request.params) {
			if(request.params.device) {
				version = request.params.device.version;
				os = request.params.device.os;
				osVersion = request.params.device.osVersion;
				device = request.params.device.device;
				langArray = request.params.device.langArray;
				locale = request.params.device.locale;
				appName = request.params.device.appName;
			}
		}
		if(forCC) {
			os = ccOS;
		}

		var user = request.user;


		var recordUserSettingsUpdated = false; //[Guichi] settingsLastUpdated column in User table will be up to date
		var mainPromise;


		if(typeof version != "undefined" && version != null && typeof os != "undefined" && os != null) {
			// console.log ("\n\n\n\n\n version not undefined\n\n\n")
			mainPromise = Maintenance.IsVersionAllowances({OS: os, appVersion: version, appName: appName});
			//as long as the valid data comes,always update
			recordUserSettingsUpdated = true;
		} else {
			// console.log ("\n\n\n\n\n version = ", version)
			mainPromise = Maintenance.GetAllowances();
		}


		return mainPromise.then(function (returnJson) {

			var dateTime = new Date().toLocaleString('en', {timeZone: 'Australia/Sydney'})
			
			var settings = {
				ServerTime: dateTime,
				Settings: returnJson
			};

			// settings.Settings["phoneNumberSupportedCountries"] = _.keys(constants.twilioNumber);
			settings.Settings["phoneNumberSupportedCountries"] = constants.countryCodes;


			// Force update
			if(!settings.Settings.canView)
				return settings;


			var settingsPromise = Maintenance.GetSettings().then(function (results) {


				var returnIAPSettings = false;

				var iap = {};
				iap[constants.iap.AvailableIAPs] = null,
				iap[constants.iap.SpecialCases] = {}


				for(var key in results) {
					if(!results.hasOwnProperty(key) || key in settings.Settings)
						continue;


					var addToSettings = true;

					switch(os) {
					case constants.os.Android:
						if(key.indexOf(constants.settings.keys.AndroidIAPPrefix) == 0) {
							addToSettings = false;


							// Key should either be "androidiap" or "androidiap_<specialcase>"
							var pattern = new RegExp("^" + constants.settings.keys.AndroidIAPPrefix + "_(.*)$");
							var match = pattern.exec(key);

							// If the pattern matched then we have a special case
							if(match) {
								iap[constants.iap.SpecialCases][match[1]] = results[key];
							} else if(key == constants.settings.keys.AndroidIAPPrefix) {
								// Otherwise it's just "androidiap", which should be the list of all available IAPs
								// If this is provided then we're good to return the IAP object
								returnIAPSettings = true;

								// We only need to return the IAP keys to the app - all of the other associated data is for CC usage
								if(!forCC)
									iap[constants.iap.AvailableIAPs] = Object.keys(JSON.parse(results[key]));
								else
									iap[constants.iap.AvailableIAPs] = JSON.parse(results[key]);
							}
						}

						break;
					case constants.os.iOS:
						if(key.indexOf(constants.settings.keys.iOSIAPPrefix) == 0) {
							addToSettings = false;


							// Key should either be "iosiap" or "iosiap_<specialcase>"
							var pattern = new RegExp("^" + constants.settings.keys.iOSIAPPrefix + "_(.*)$");
							var match = pattern.exec(key);

							// If the pattern matched then we have a special case
							if(match) {
								iap[constants.iap.SpecialCases][match[1]] = results[key];
							} else if(key == constants.settings.keys.iOSIAPPrefix) {
								// Otherwise it's just "iosiap", which should be the list of all available IAPs
								// If this is provided then we're good to return the IAP object
								returnIAPSettings = true;

								// We only need to return the IAP keys to the app - all of the other associated data is for CC usage
								if(!forCC)
									iap[constants.iap.AvailableIAPs] = Object.keys(JSON.parse(results[key]));
								else
									iap[constants.iap.AvailableIAPs] = JSON.parse(results[key]);
							}
						}

						break;
					}


					if(addToSettings)
						settings.Settings[key] = results[key];
				}


				if(returnIAPSettings)
					settings.IAP = iap;
			}, function (error) {
				return Parse.Promise.error(error);
			});


			//user session has expired and the function will terminate here
			if(user === undefined) {
				settings.invalidSessionToken=true
				return settingsPromise.then(function() {
					return settings;
				});
			}


			// If the user's activeAccountType is null (new user) then I need to wait for the
			//   settingsQuery to complete so we can get the default account type from there
			// Otherwise I can do the user and permissions stuff without waiting
			var waitingPromise;
			if(user.get(constants.user.keys.activeAccountType) == null)
				waitingPromise = settingsPromise;
			else
				waitingPromise = Parse.Promise.as(null);


			var accountTypeLogPromise = Parse.Promise.as(null);


			// If it's the first login for the user, we need to get the permission details after
			// their account type is saved (and before the user is saved)

			// An extra fetch for the user's permissions is faster than saving the user (so their account type saves)
			// and letting the permissions file do it's thing
			var accountTypePermissions;
			var userPermissions;
			var accountThreshold;
			var accountName;


			return waitingPromise.then(function () {
				if(user.get(constants.user.keys.activeAccountType) == null) {
					var accountTypeQuery = new Parse.Query(constants.accountType.keys.className)
						.equalTo(constants.accountType.keys.Slug, settings.Settings[constants.settings.keys.DefaultAccountTypeSlug])
						.include(constants.base.keys.permissions);

					//TODO [Guichi] permissionID not in use anymore ,remove it after test it thoroughly
					var userPermissionQuery = new Parse.Query(constants.permissions.keys.className)
						.containedIn(constants.base.keys.objectId, util.idsFromPointerArray(user.get(constants.base.keys.permissions)));


					var accountPromise = accountTypeQuery.first().then(function (result) {
						// console.log(JSON.stringify(result))
						user.set(constants.user.keys.activeAccountType, result);


		                var accountTypeLog = new Parse.Object(constants.accountTypeLog.keys.className);
		                accountTypeLog.set(constants.accountTypeLog.keys.user, user);
		                accountTypeLog.set(constants.accountTypeLog.keys.DestinationAccountType, result);
		                accountTypeLog.set(constants.accountTypeLog.keys.consistentCreatedAt, user.createdAt);

		                accountTypeLogPromise = accountTypeLog.save(null,{useMasterKey:true});

		                accountTypePermissions = result.get(constants.base.keys.permissions);
		                accountThreshold = result.get(constants.accountType.keys.Threshold);
		                accountName = result.get(constants.accountType.keys.MarketingName);

						return result;
					});

					var userPermissionPromise = userPermissionQuery.find({useMasterKey:true}).then(function (permissions) {
						userPermissions = permissions;
					});

					return Parse.Promise.when([accountPromise, userPermissionPromise]);
				}
			}).then(function (accountType) {
				if(recordUserSettingsUpdated) {
					user.set(constants.user.keys.SettingsLastUpdated, new Date());

					user.set("lastAppVersion", version)
						.set("lastDevice", device)
						.set("lastOS", os)
						.set("lastOSVersion", osVersion)
						.set("lastLangArray", langArray)
						.set("lastLanguage", _.isArray(langArray) && langArray.length > 0 ? langArray[0] : undefined)
						.set("lastLocale", locale);

					if (("iPhone OS" == os) || ("Android" == os)) {
						user.set("hasUsedMobileApp", true);
					}

				}

				var info = {
					user: user
				}

				//TODO [Guichi] seems it will be null forever ,remove it after test it thoroughly
				if(userPermissions != null) {
					var permissions = {
						user: userPermissions,
						accountType: accountTypePermissions
					};
					info[constants.permissions.keys.permissions] = permissions;

					var extraInfo = {};
					extraInfo[constants.permissions.keys.accountThreshold] = accountThreshold;
					extraInfo[constants.permissions.keys.accountName] = accountName;
					info.extraInfo = extraInfo;
				}

				return permissionsJS.getSinglePermissions(info, false).then(function(perms) {
					settings.Permissions = perms;


					var maybeUserSavePromise;
					if(recordUserSettingsUpdated)
						maybeUserSavePromise = user.save(null,{useMasterKey:true});
					else
						maybeUserSavePromise = Parse.Promise.as(null);


					return Parse.Promise.when(settingsPromise, maybeUserSavePromise, accountTypeLogPromise).then(function() {
						// return settings;
						return settings;
					});
				});
			});
		});
	});
};

