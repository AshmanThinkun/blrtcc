var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var emailJS = require("cloud/email.js");

var _ = require("underscore");

module.exports = function (app) {
    app
        .get("/notifyVideoConversionFinished", function (req, res) {
            console.log('\n\n\nreq.query===');
            console.log(req.query);
            Parse
                .Cloud
                .useMasterKey();
            if (req.query && req.query.recipient) {
                // var recipient=req.query.recipient;
                emailJS.sendTextEmail({
                    email: "support@blrt.com",
                    name: "Blrt Support"
                }, [
                    {
                        email: req.query.recipient
                    }
                ], "Video conversion complete", "Yay! Your Blrt " + req.query.webUrl + 
                    " has been converted to a video. You can download the video file here:\n" + req.query.url + 
                    " You can also download the zip file here:\n" + req.query.filesUrl +
                    "\n\nThis link will expire in 7 days and the video file will not be available after that." + 
                    "\n\nPlease reply to this email if you require support." + 
                    "\n\nGet the Blrt App on all your devices at http://www.blrt.com ")
                    .then(function (r) {
                        console.log("email send")
                        console.log(JSON.stringify(r))
                        res.end(":)")
                    }, function (e) {
                        console.log(JSON.stringify(e))
                        res.end(":(")

                    })

            } else {
                res.end(":(")
            }
        });

    //blrt]Id and url is used to set promoUrl of conversationItem
    //convoId is used to find pendingNonUserEmail
    app.get('/notifyPromoVideoConversionFinished', function (req, res) {
        const {blrtId, url, convoId} = req.query;
        console.log(JSON.stringify(blrtId))
        console.log(JSON.stringify(url))
        console.log(JSON.stringify(convoId))
        if (blrtId && url && convoId) {
            Parse.Promise.when(new Parse.Query('Conversation').get(convoId, {useMasterKey: true}), new Parse.Query('ConversationItem').get(blrtId, {useMasterKey: true}))
                .then(function (convo, blrt) {
                    return Parse.Promise.when(new Parse.Query('PendingNonUserEmail').equalTo('conversation', convo).find({useMasterKey: true}), blrt.set('promoVideo', url).save())
                })
                .then(function (pendingNonUserEmail, blrt) {
                    if (pendingNonUserEmail.length > 0) {
                        Parse
                            .Promise
                            .when(pendingNonUserEmail.map(function (entry) {
                                const convo = entry.get('conversation');
                                return Parse.Promise.when(convo.fetch({useMasterKey: true}),entry.get('sender').fetch({useMasterKey: true}))
                                    .then(function (fullConvo,fullSender) {
                                        return  emailJS.nonUserEmailSending(entry.get('nonUserObj'),fullSender,convo,entry.get('options'))
                                    })
                                    .then(function (s) {
                                        return entry
                                            .destroy({useMasterKey: true})
                                            .then(function (destroyed) {
                                                return s
                                            })
                                    })


                            }))
                            .then(function (result) {
                                res.json(result)
                            })
                    } else {
                        res.end("No email need to be send")
                    }

                })
                .fail(function (e) {
                    res.json(e)
                })

        } else {
            res.end("Invalid params")
        }
    })

    //end of exports
};
