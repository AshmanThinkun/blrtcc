var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var accountTypeJS = require("cloud/accounttype.js");
var permissions = require("cloud/permissions.js");
var NonUser = require("cloud/nonUser");
var loggler = require("cloud/loggler");
var _ = require("underscore");
var settingsJS = require("cloud/settings");
var emailJS = require("cloud/email");

module.exports = function (app) {

    app.all('/nsem', function (req, res, next) {
        var pageArgs = {
            blrt_url: (constants.appProtocol + "://" + req.host + req.url),
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            http_user_agent: req.headers['user-agent'],
            facebook_steps: constants.help.facebookSetPasswordSteps
        };

        if (req.query.key && req.query.s && req.query.sem && _.validQueryArgs(req.query)) {
            var sem = req.query.sem.toLowerCase();

            pageArgs["secondary"] = sem;

            Parse.Cloud.useMasterKey();

            // Ensure user contact details with sem doesn't exist
            new Parse.Query(constants.contactTable.keys.className)
                .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                .equalTo(constants.contactTable.keys.Value, req.query.sem)
                .notEqualTo(constants.contactTable.keys.placeholder, true)
                .count()
                .then(function (count) {
                    if (count == 0)
                        res.render('secondaryEmailForm', pageArgs);
                    else
                        res.redirect('/');
                });
        } else
            res.redirect('/');
    });

    app.post("/vsem", function (req, res, next) {
        var pageArgs = {
            blrt_url: (constants.appProtocol + "://" + req.host + req.url),
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            http_user_agent: req.headers['user-agent'],
            facebook_steps: constants.help.facebookSetPasswordSteps,
            _: _
        };

        if (req.body.sem && req.body.user && req.body.pass) {
            var sem = req.body.sem.toLowerCase();

            pageArgs["secondary"] = sem;

            Parse.Cloud.useMasterKey();

            Parse.Promise.when(
                Parse.User.logIn(req.body.user, req.body.pass),
                Parse.Query.or(
                    new Parse.Query(constants.contactTable.keys.className)
                        .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                        .equalTo(constants.contactTable.keys.Value, sem),
                    new Parse.Query(constants.contactTable.keys.className)
                        .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                        .matchesKeyInQuery(
                            constants.contactTable.keys.user,
                            constants.base.keys.objectId,
                            new Parse.Query(Parse.User)
                                .equalTo(constants.user.keys.username, req.body.user)
                        )
                ).find()
            ).then(function (currentUser, contacts) {
                handleSEMVerification(currentUser, contacts, pageArgs, res);
            }, function (errors) {
                if (errors[0] != null)
                    pageArgs["errorMsg"] = "Invalid login credentials.";
                else
                    pageArgs["errorMsg"] = "Something went wrong. Try submitting the form again and contact Blrt support if the problem persists.";

                res.render("secondaryEmailForm", pageArgs);
            });
        } else {
            pageArgs["errorMsg"] = "Invalid login credentials.";
            pageArgs["secondary"] = pageArgs["secondary"] ? pageArgs["secondary"] : "";
            res.render("secondaryEmailForm", pageArgs);
        }
    });

    app.all('/vsem', function (req, res, next) {
        var pageArgs = {
            http_user_agent: req.headers['user-agent']
        };

        if (req.query.key && req.query.s && req.query.sem && req.query.uoid && _.validQueryArgs(req.query)) {
            var sem = req.query.sem.toLowerCase();

            pageArgs["secondary"] = sem;

            Parse.Cloud.useMasterKey();

            // Check that user exists with uoid
            // Check that user doesn't exist with sem
            Parse.Promise.when(
                new Parse.Query(Parse.User)
                    .equalTo(constants.base.keys.objectId, _.decryptId(req.query.uoid))
                    .first(),
                Parse.Query.or(
                    new Parse.Query(constants.contactTable.keys.className)
                        .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                        .equalTo(constants.contactTable.keys.Value, sem),
                    new Parse.Query(constants.contactTable.keys.className)
                        .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                        .equalTo(constants.contactTable.keys.user, _.decryptId(req.query.uoid))
                ).find()
            ).then(function (user, contacts) {
                if (user == null)
                // If we got a link without a proper user... run away!!!
                    return res.redirect('/');

                handleSEMVerification(user, contacts, pageArgs, res);
            });
        } else
            res.redirect('/');
    });

    function handleSEMVerification(user, contacts, pageArgs, res) {
        var failed = false;
        _.each(contacts, function (contact) {
            if (contact.get(constants.contactTable.keys.placeholder) != true && contact.get(constants.contactTable.keys.user).id != user.id)
                failed = contact;
        });

        if (failed !== false) {
            // Someone else has verified this secondary email
            if (failed.get(constants.contactTable.keys.user).id != user.id) {
                pageArgs["title"] = "Email address already in use";
                pageArgs["msg"] = "The requested email address has already been connected to a different Blrt account.";
            } else {
                pageArgs["title"] = "Email address already added";
                pageArgs["msg"] = "This email address has already been added to your Blrt account.";
            }
            return res.render('genericMessageView', pageArgs);
        }

        var promise;

        // There should only be multiple contacts if they belong to different users
        var main = null;
        var userEmails = [pageArgs.secondary];
        var dels = [];

        _.each(contacts, function (contact) {
            if (contact.get(constants.contactTable.keys.user).id == user.id) {
                if (contact.get(constants.contactTable.keys.Value) == pageArgs.secondary)
                    main = contact;
                else if (contact.get(constants.contactTable.keys.placeholder) != true)
                    userEmails.push(contact.get(constants.contactTable.keys.Value));
            } else
                dels.push(contact);
        });

        // Before continuing we also need to set a flag to get the user to search for and link their contact details.
        // Even if something happens later that prevents this from being correct, it'll just run it through and do nothing.

        // On the other hand, if this was set after creating the contact details but that setting failed, then god knows
        // when the relation would eventually be linked to the new contact details. Better safe than sorry.
        user.set(constants.user.keys.ContactInfoDirty, true);

        return Parse.Promise.when(permissions.getSinglePermissions({user: user}, true), user.save()).then(function (permissions) {
            var maxSecondaryEmails = permissions[constants.permissions.values.maxSecondaryEmails];

            if (userEmails.length - 1 >= maxSecondaryEmails) {
                pageArgs["title"] = "Secondary email limit reached";
                pageArgs["msg"] = "You cannot add more than " + maxSecondaryEmails + " secondary email address" + (maxSecondaryEmails == 1 ? "" : "es") + " to a Blrt account. You must remove one before you can add another.";
                return res.render('genericMessageView', pageArgs);
            }

            pageArgs["user_emails"] = userEmails;

            if (main == null)
            // They don't have an existing placeholder contact, create a new one
                promise = new Parse.Object(constants.contactTable.keys.className)
                    .set(constants.contactTable.keys.dirty, true)
                    .set(constants.contactTable.keys.type, constants.contactTable.types.email)
                    .set(constants.contactTable.keys.user, user)
                    .set(constants.contactTable.keys.Value, pageArgs.secondary)
                    .set(constants.contactTable.keys.verified, true)
                    .setACL(new Parse.ACL(user))
                    .save()
                    .fail(function (error) {
                        loggler.global.log("ALERT: /vsem: Contact creation error:", error).despatch();
                    });
            else if (main.get(constants.contactTable.keys.placeholder) != true)
            // The contact has already been verified, just pretend they succeeded
                promise = Parse.Promise.as();
            else
            // We need to update the existing placeholder
                promise = main.unset(constants.contactTable.keys.placeholder)
                    .set(constants.contactTable.keys.dirty, true)
                    .set(constants.contactTable.keys.verified, true)
                    .save()
                    .fail(function (error) {
                        loggler.global.log("ALERT: /vsem: Contact update error:", error).despatch();
                    });

            // Delete any excess
            if (dels.length != 0)
                promise = Parse.Promise.when(
                    Parse.Object.destroyAll(dels)
                        .fail(function (error) {
                            if (error.code == Parse.Error.AGGREGATE_ERROR) {
                                for (var i = 0; i < error.errors.length; i++)
                                    loggler.global.log("ALERT: /vsem: Couldn't delete " + error.errors[i].object.id + ":", error.errors[i]);
                            } else
                                loggler.global.log("ALERT: /vsem: Delete aborted:", error);

                            loggler.global.despatch();
                        }),
                    promise
                );

            return promise.then(function () {
                var pushQuery = new Parse.Query(Parse.Installation)
                    .equalTo('owner', user);
                Parse.Push.send({
                    where: pushQuery,
                    data: emailJS.StandardizePushMessage('{"t":"5","alert":{"loc-key":"PVSE","action-loc-key":"PVSEB"}}')
                }, { useMasterKey: true });

                // Decided to do the non-user claiming only when added an email NOT also when user verifies that email 
                //return NonUser.associate(user).always(function () {
                    return res.render('secondaryEmailVerified', pageArgs);
                //});
            }, function (error) {
                loggler.global.log("ALERT: /vsem: deletion failure! Critical error:", error).despatch();
                pageArgs["title"] = "Something went wrong";
                pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                return res.render('genericMessageView', pageArgs);
            });
        }, function (errors) {
            loggler.global.log("ALERT: /vsem: Failed to check user permissions or save user contact detail check:", errors).despatch();
        });
    }

    app.all('/vem', function (req, res, next) {
        var pageArgs = {
            http_user_agent: req.headers['user-agent']
        };

        if (req.query.key && req.query.s && req.query.coid && _.validQueryArgs(req.query)) {
            Parse.Cloud.useMasterKey();

            var query = new Parse.Query(constants.contactTable.keys.className)
                .include(constants.contactTable.keys.user)
                .equalTo(constants.base.keys.objectId, _.decryptId(req.query.coid.trim()));

            return query.first().then(function (contact) {
                if (contact == null) {
                    loggler.global.log("ALERT: /vem: No contact details returned by verification search with query:", query).despatch();
                    pageArgs["title"] = "Something went wrong";
                    pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                    return res.render('genericMessageView', pageArgs);
                }

                if (contact.get(constants.contactTable.keys.verified) == true)
                    res.redirect('/usermanagement/email_verification.html');
                else {
                    new Parse.Query(constants.contactTable.keys.className)
                        .notEqualTo(constants.contactTable.keys.user, contact.get(constants.contactTable.keys.user))
                        .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                        .equalTo(constants.contactTable.keys.Value, contact.get(constants.contactTable.keys.Value))
                        .equalTo(constants.contactTable.keys.placeholder, true)
                        .find(function (deletables) {
                            var deletePromise;
                            if (deletables.length == 0)
                                deletePromise = Parse.Promise.as();
                            else
                                deletePromise = Parse.Object.destroyAll(deletables);

                            return deletePromise.then(function () {
                                var user = contact.get(constants.contactTable.keys.user);

                                // The first thing we'll do is ensure the client will realise that it should check the user's contact
                                // details and link it to existing relations - just incase Parse drop's out right after creating that
                                // new contact detail.
                                user.set(constants.user.keys.ContactInfoDirty, true);

                                return user.save().then(function () {
                                    var isPrimary = user.get(constants.user.keys.username) == contact.get(constants.contactTable.keys.Value);

                                    contact.set(constants.contactTable.keys.verified, true)
                                        .set(constants.contactTable.keys.placeholder, false);

                                    if (isPrimary) {
                                        user.set(constants.user.keys.EmailVerified2, true);
                                        promise = Parse.Object.saveAll([contact, user]);
                                    } else
                                        promise = contact.save();

                                    return promise.then(function () {
                                        var pushQuery = new Parse.Query(Parse.Installation)
                                            .equalTo('owner', user);
                                        if (isPrimary) {
                                            Parse.Push.send({
                                                where: pushQuery,
                                                data: emailJS.StandardizePushMessage('{"t":"5","alert":{"loc-key":"PVE","action-loc-key":"PVEB"}}')
                                            }, { useMasterKey: true });
                                            Parse.Cloud.httpRequest({
                                                url: constants.webappSiteAddress + "/notifyWebapp",
                                                method: "POST",
                                                body: {
                                                    type: "PRIMARY_EMAIL_VERIFICATION",
                                                    userId: contact.get("user").id
                                                }
                                            })
                                        }
                                        else {
                                            Parse.Push.send({
                                                where: pushQuery,
                                                data: emailJS.StandardizePushMessage('{"t":"5","alert":{"loc-key":"PVSE","action-loc-key":"PVSEB"}}')
                                            }, { useMasterKey: true });
                                            Parse.Cloud.httpRequest({
                                                url: constants.webappSiteAddress + "/notifyWebapp",
                                                method: "POST",
                                                body: {
                                                    type: "SECONDARY_EMAIL_VERIFICATION",
                                                    userId: contact.get("user").id
                                                }
                                            })
                                        }
                                        if (isPrimary)
                                            res.redirect('/usermanagement/email_verification.html');
                                        else {
                                            return Parse.Promise.when(
                                                new Parse.Query(constants.contactTable.keys.className)
                                                    .equalTo(constants.contactTable.keys.user, contact.get(constants.contactTable.keys.user))
                                                    .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                                                    .notEqualTo(constants.contactTable.keys.placeholder, true)
                                                    .find()//,
                                                    // Decided to do the non-user claiming only when added an email NOT also when user verifies that email 
                                                //NonUser.associate(user)
                                            ).then(function (contacts) {
                                                var userEmails = [];

                                                _.each(contacts, function (contact) {
                                                    userEmails.push(contact.get(constants.contactTable.keys.Value));
                                                });

                                                pageArgs["user_emails"] = userEmails;
                                                pageArgs["_"] = _;

                                                return res.render('secondaryEmailVerified', pageArgs);
                                            }, function (error) {
                                                loggler.global.log("ALERT: /vem: Failed to find contact details to list (after already successfully updating):", error).despatch();
                                                res.redirect('/usermanagement/email_verification.html');
                                            });
                                        }
                                    }, function (error) {
                                        loggler.global.log("ALERT: /vem: Failed to save verification:", error).despatch();
                                        pageArgs["title"] = "Something went wrong";
                                        pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                                        return res.render('genericMessageView', pageArgs);
                                    });
                                }, function (error) {
                                    loggler.global.log("ALERT: /vem: Failed to save contact detail check:", error).despatch();
                                    pageArgs["title"] = "Something went wrong";
                                    pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                                    return res.render('genericMessageView', pageArgs);
                                });
                            }, function (error) {
                                loggler.global.log("ALERT: /vem: Failed to delete all placeholders before verification:", error).despatch();
                                pageArgs["title"] = "Something went wrong";
                                pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                                return res.render('genericMessageView', pageArgs);
                            });
                        }, function (error) {
                            loggler.global.log("ALERT: /vem: Failed to find all deletable placeholders before verification:", error).despatch();
                            pageArgs["title"] = "Something went wrong";
                            pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                            return res.render('genericMessageView', pageArgs);
                        });
                }
            }, function (error) {
                loggler.global.log("ALERT: /vem: Failed to find contact details to update:", error).despatch();
                pageArgs["title"] = "Something went wrong";
                pageArgs["msg"] = "An error occurred during verification. Blrt support has been notified, however you can also contact them for more urgent assistance.";
                return res.render('genericMessageView', pageArgs);
            });
        } else
            res.redirect('/');
    });


//this link is used for blocking the sender to send
//"add conversation" emails to recevicer
    app.all("/block", function (req, res) {
        function resMsg(str) {
            return res.render('block', {
                message: str
            });
        }

        if (req.query == null)
            return resMsg("No arguments provided.");
        if (req.query.r == null)
            return resMsg("No receiver provided.");
        if (req.query.s == null)
            return resMsg("No sender provided.");
        var senderId = _.decryptId(req.query.s);
        var receiverId = _.decryptId(req.query.r);

        //prepare the params
        var params = {};
        params.data = {
            blockUserId: senderId,
            requestUserId: receiverId
        };
        params.api = {version: 1, endpoint: "userBlock"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };
        Parse.Cloud.useMasterKey();

        //use userBlock api
        return Parse.Cloud.run("userBlock", params, {
            success: function (response) {
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    resMsg("Failed to block user, please try again later.");
                }
                if (response.success)
                    resMsg(response.message);
                else
                    resMsg("Failed to block user, please try again later.");
            },
            error: function (error) {
                resMsg("Failed to block user, please try again later.");
            }
        });

    });


    app.get("/setPassword", function (req, res, next) {
        //validation
        function resError(str) {
            return res.jsonp({success: false, message: str});
        }

        if (req.query == null)
            return resError("No arguments provided.");
        if (req.query.id == null)
            return resError("Invalid arguments.");
        if (req.query.token == null)
            return resError("Invalid arguments.");

        var userId = _.decryptId(req.query.id);
        var token = req.query.token;
        var now = new Date();

        Parse.Cloud.useMasterKey();

        return new Parse.Query("User")
            .equalTo("objectId", userId)
            .equalTo("passwordResetToken", token)
            .notEqualTo("hasSetPassword", true)
            .greaterThan("passwordTokenCreateAt", new Date(now - 12096e5))
            .first()
            .then(function (user) {
                if (!user) {
                    return res.redirect("/usermanagement/invalid_link.html");
                }

                var pageArgs = {
                    id: req.query.id,
                    token: req.query.token,
                    name: user.get("hasnotSetDisplayname") ? "" : user.get("name")
                };
                return res.render('setPassword', pageArgs);
            }, function (error) {
                return resError("Something went wrong, please try again later.");
            });

    });

    app.post("/setPassword", function (req, res) {
        //validation
        req.params = req.body;
        function resError(str) {
            return res.jsonp({success: false, message: str});
        }

        if (req.params == null)
            return resError("No arguments provided.");
        if (req.params.id == null)
            return resError("Invalid arguments");
        if (req.params.token == null)
            return resError("Invalid arguments.");
        if (req.params.password == null || req.params.password.length < 8 || req.params.password.length > 33 || req.params.name == null) {
            return resError("Invalid arguments.");
        }

        var userId = _.decryptId(req.params.id);
        var token = req.params.token;
        var now = new Date();

        Parse.Cloud.useMasterKey();
        return new Parse.Query("User")
            .equalTo("objectId", userId)
            .equalTo("passwordResetToken", token)
            .notEqualTo("hasSetPassword", true)
            .greaterThan("passwordTokenCreateAt", new Date(now - 12096e5))
            .first()
            .then(function (user) {
                if (!user)
                    return resError("Invalid arguments.");
                var name = firstName = lastName = null;
                if (req.params.name) {
                    name = req.params.name;
                    var split = req.params.name.split(" ");
                    firstName = split[0];
                    lastName = split.length != 1 ? split[split.length - 1] : null;
                }

                return user.set("password", req.params.password)
                    .set("hasSetPassword", true)
                    .set("passwordResetToken", "fTUGUdh8o7y4rHKJHIUhf98fgj98ds")
                    .set("passwordTokenCreateAt", new Date(now - 12096e6))
                    .set("name", name)
                    .set("first_name", firstName ? firstName : "")
                    .set("last_name", lastName ? lastName : "")
                    .set("hasnotSetDisplayname", false)
                    .save()
                    .then(function () {
                        var pushQuery = new Parse.Query(Parse.Installation)
                            .equalTo('owner', user);
                        Parse.Push.send({
                            where: pushQuery,
                            data: emailJS.StandardizePushMessage('{"t":"5","alert":{"loc-key":"PM","action-loc-key":"PVEB","loc-args":["You have set your password"]}}')
                        }, { useMasterKey: true });
                        return res.render("message", {message: "Successfully updated your password!"});
                    }, function (err) {
                        return resError("Something went wrong, please try again later.");
                    });
            }, function (error) {
                return resError("Something went wrong, please try again later.");
            });
    });


    app.all("/interest", function (req, res) {
        res.header("Access-Control-Allow-Origin", "*");
        if (req.body && req.body.email) {
            req.query = req.body;
        }
        //process param
        var email, sourceUrl;
        if (!req.query || !req.query.email || !req.query.source || !util.isEmail(req.query.email)) {
            console.log("params:" + JSON.stringify(req.query));
            return res.jsonp({success: false, message: "invalid params."});
        } else {
            email = req.query.email.toLowerCase();
            sourceUrl = req.query.source.toLowerCase().trim();
            console.log('\n\n\n===email==='+email);
            console.log('sourceUrl==='+sourceUrl);
        }

        Parse.Cloud.useMasterKey();
        //check email existance
        return new Parse.Query("UserContactDetails")
            .equalTo("type", "email")
            .equalTo("value", email)
            .first()
            .then(function (contact) {
                if (contact) //if email exists, then just fail it.
                    return Parse.Promise.error({
                        success: false,
                        message: "Looks like you're already signed up to Blrt. Get the Blrt app on your iPhone, iPad or Android device."
                    });
                else
                    return true;
            }).then(function () {
                //nonuser tracking
                return NonUser.get({type: "email", value: email, source: "WebSignupStart"});
            }).then(function (theuser) {
                // var mp = new Mixpanel({batchMode: false});
                // return mp.track({
                //     event: "Web Signup Start NonUser",
                //     properties: {
                //         "sourceURL": sourceUrl,
                //         "userAgent": req.headers['user-agent'] || '',
                //         "os": req.useragent ? req.useragent.OS : "Other Web",
                //     }
                // }, {id: "nonuser-" + theuser.id, updateLastSeen: true});
                return true
            }).then(function () {
                return res.jsonp({success: true, message: "success"});
            }, function (error) {
                console.log(JSON.stringify(error));
                return res.jsonp({
                    success: false,
                    message: "Looks like you're already signed up to Blrt. Get the Blrt app on your iPhone, iPad or Android device."
                });
            });
    });

    app.all("/signup", function (req, res) {
        console.log('\n\n\n\nsignup is called===');
        console.log(req);
        res.header("Access-Control-Allow-Origin", "*");
        if (req.body && req.body.email) {
            req.query = req.body;
        }

        var email, password, name;
        if (!req.query || !req.query.email || !req.query.password || !util.isEmail(req.query.email) || !req.query.name) {
            return res.jsonp({success: false, message: "invalid params."});
        } else {
            email = req.query.email.toLowerCase();
            password = req.query.password;
            name = req.query.name;
        }

        if (name.length > 36 || name.length < 1 || password.length < 8 || password.length > 36) {
            return res.jsonp({success: false, message: "invalid params."});
        }


        //prepare the params
        var params = {};
        params.data = {
            email: email,
            password: password,
            name: name,
            extra: {
                hasAgreedToTOS: req.query.agreetos ? true : false,
                signupUrl: req.query.signupUrl
            }
        };
        params.api = {version: 1, endpoint: "userRegister"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };

        var registeredUser;
        //run cloud function
        console.log('\n\n\n\nuserRegister signup===');
        return Parse.Cloud.run("userRegister", params)
            .then(function (response) {
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    return Parse.Promise.error("error");
                }
                if (!response.success) {
                    return response;
                }
                return Parse.User.logIn(email, password)
                    .then(function (user) {
                        registeredUser = user;
                        return {success: true, message: "success", userId: user.id};
                    });
            }).then(function (response) {
                console.log('\n\n\nuserRegister response===');
                console.log(response);
                // if(!response.success || !req.query.coupon || !registeredUser) {
                //     console.log("will you returen");
                //     return response;
                // }

                if(response.success && registeredUser) {
                    //call function -- processQueuedConvoTemplates
                    console.log('\n\n\n===registeredUser===')
                    console.log(registeredUser);
                    if(!req.query.coupon) {
                        return Parse.Cloud.run('processQueuedConvoTemplates', {user: email})
                            .then(result=>{
                                console.log('\n\ncreate welcome convo done===');
                                return response;
                            });
                    }

                    let queuedConvoTemplatsPromise = Parse.Cloud.run('processQueuedConvoTemplates')
                    console.log("continue the coupon logic")
                    //coupon not validated yet
                    let couponPromise = new Parse.Query("CouponCode")
                        .equalTo("code", req.query.coupon)
                        .first()
                        .then(function (couponObj) {
                            if(!couponObj)
                                return {success: true, message: "coupon invalid", code: 204};
                            var now = new Date();
                            if(couponObj.get("expiryAt") && couponObj.get("expiryAt") < now)
                                return {success: true, message: "coupon invalid", code: 204};    

                            var maxNumOfUsesPerCoupon = couponObj.get("maxNumOfUsesPerCoupon");
                            var totalNumOfUses = couponObj.get("totalNumOfUses");
                            if (maxNumOfUsesPerCoupon) {
                                if (!totalNumOfUses) {
                                    totalNumOfUses = 0;
                                }
                                if (totalNumOfUses + 1 > maxNumOfUsesPerCoupon) {
                                    return {success: true, message: "coupon invalid", code: 204};
                                }
                            }    

                            //[Guichi] new feature come in :
                            //[Guichi] When user sign up , Including a stripe coupon code is also possible
                            //[Guichi] In this case ,do not create a timely action object,it will override the paid account
                            
                            //save a timely action
                            if (couponObj.get("couponType") !== "stripe") { //should be signupTrial
                                //run asynchronously, this can be run after the frontend already get the response
                                accountTypeJS.requestAccountUpgrade({
                                    user: registeredUser,
                                    params: {
                                        requestFor: "trial",
                                        data: {
                                            coupon: req.query.coupon
                                        }
                                    }
                                })    

                                return response;
                            } else {
                                response.code = 206;
                                //[Guichi]A little bit magic:
                                //[Guichi]function GetSettings will set account type for the user
                                //[Guichi]If the logic goes here,the user will upgrade account shortly,
                                //[Guichi]the server will throw an error if account type not set by that time
                                return settingsJS.GetSettings({user: registeredUser}).then(function () {
                                    return response
                                })    

                            }
                        });

                    return Parse.Promise.when(queuedConvoTemplatsPromise, couponPromise)
                        .then((queued, coupon)=>{
                            return response;
                        })
                        
                }else return response;

            }).then(function (response) {
                return res.jsonp(response);
            }, function (error) {
                return res.jsonp({success: false, error: error});
            });
    });

    app.all("/updateSignupDetail", function (req, res) {
        res.header("Access-Control-Allow-Origin", "*");
        if (req.body && req.body.country) {
            req.query = req.body;
        }

        var country, industry, device;
        if (!req.query || !req.query.country || !req.query.industry) {
            return res.jsonp({success: false, message: "invalid params."});
        } else {
            country = req.query.country;
            industry = req.query.industry;
            device = req.query.device;
            if (!device || !_.isArray(device) || device.length == 0) {
                device = false;
            }
        }

        var userFetchPromise;

        if (req.user) {
            userFetchPromise = req.user.fetch();
        } else if (req.query.email && req.query.password) {
            userFetchPromise = Parse.User.logIn(req.query.email, req.query.password);
        } else {
            return res.jsonp({success: false, message: "User not login"});
        }

        return userFetchPromise
            .then(function (user) {
                user.set("signupLocale", country)
                    .set("industry", industry);

                if (device) {
                    user.set("preferedDevice", device);
                }

                return user.save();
            }).then(function () {
                res.jsonp({success: true, message: "Details updated."});
            }, function () {
                res.jsonp({success: false, message: "Something went wrong"});
            });
    });

    app.all("/checkCoupon", function (req, res, next) {
        function fail(message, tried) {
            message = message == undefined ? "Connection error, please try again" : message;
            tried = tried == undefined ? false : tried;

            return res.jsonp({
                success: false,
                message: message,
                tried: tried
            });
        }

        if (!_.isString(req.query.coupon))
            return fail();

        var coupon = req.query.coupon.trim().toLowerCase();
        return new Parse.Query("CouponCode")
            .equalTo("code", coupon)
            .equalTo("couponType", "stripe")
            .first()
            .then(function (coupon) {
                console.log("coupon")
                console.log("### " + JSON.stringify(coupon))
                if (coupon == null)
                    return fail("Invalid promo code", true);
                return accountTypeJS.IsValidCouponForUser(coupon)
                    .then(function (validRes) {
                        if (validRes.valid) {
                            return res.jsonp({
                                success: true,
                                tried: true,
                                marketingName: coupon.get("marketingName"),
                                discount: coupon.get("discount"),
                                directPirce: coupon.get("price")
                            });
                        } else {
                            return fail("Invalid promo code", true);
                        }
                    });
            }, function (error) {
                loggler.global.log("ALERT: Error searching for coupon code:", error).despatch();
                return Parse.Promise.as(fail());
            });
    });
    var atob = require('cloud/atob');
    app.all('/upgrade', function (req, res, next) {
        console.log("########")
        // console.log(JSON.stringify(req))

        var pageArgs = {
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            http_user_agent: req.headers['user-agent'],
            image_url: "/" + constants.stripeImage,
            stripe_public_key: constants.stripePublicKey,
            server_url: constants.protocol + req.host + '/upgrade',
            coupon_url: constants.protocol + req.host + '/checkCoupon',
            JSON: JSON,
            stripe_premium_price: constants.stripePremiumPrice,
            stripe_premium_description: constants.stripePremiumDescription,
            web_root: "//www.blrt.com"
        };

        if (req.query.token && req.query.country)
            return accountTypeJS.stripeRequest(req.query.token, "premium-annual", req.query.country, req.query.coupon)
                .then(function (result) {
                    return res.jsonp(result);
                }, function (error) {
                    return res.jsonp({success: false, message: constants.stripeGenericErrorMessage});
                });

        if (req.query.em)
            pageArgs["email"] = atob(req.query.em);
        else
            pageArgs["email"] = "";

        res.render("upgrade", pageArgs);
    });

};