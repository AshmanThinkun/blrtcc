/**
 * Created by thinkun on 26/10/2015.
 */
var _ = require("underscore");
var constants = require("cloud/constants");
// var mandrillKey = constants
// var Mandrill = require('mandrill');
// Mandrill.initialize('cKMAu25QxPorE-z6DLhHSw');
var numberOfItemInPage = 10;
module.exports = function (app) {

    app.all("/ocList/:id/admin", function (req, res) {
        Parse.Cloud.useMasterKey();
        var user = req.user
        var blrtInfo = [];
        var href = "/oas/login?redirect=" + encodeURIComponent(req.url);
        console.log("user "+user)
        if (!user) {
            res.redirect(href)
        }
        else {
            checkUser(user, req)
                .then(function (ocList) {
                    if (ocList) {
                        var pageNo = req.query.page
                        if (!pageNo) {
                            pageNo = 0
                        }


                        if (pageNo == -1) {
                            pageNo = 0;
                        }
                        var bottonUri = ocList.get("bottomBanner").buttonUri;
                        var requestId = (bottonUri.split("?")[1]).split("=")[1];
                        var decryptedRequestId = _.decryptId(requestId);
                        getBlrtByRequestId(decryptedRequestId, pageNo, numberOfItemInPage)
                            .then(
                                function (blrts) {
                                    return checkStatus(blrts, ocList).then(
                                        function (existingBlrts) {

                                            var existingIds= _.pluck(existingBlrts,"blrtId")
                                            return _.map(blrts, function (blrt) {
                                                var status;
                                                var existingBlrtIndex=existingIds.indexOf(blrt.id);
                                                if(existingBlrtIndex > -1)
                                                {
                                                    status=existingBlrts[existingBlrtIndex].status;
                                                    if(!status)
                                                    {
                                                        status="enabled"
                                                    }
                                                }else {
                                                    status="enabled";
                                                }

                                                console.log(blrt.id+" : "+status)
                                                blrtInfo.push({
                                                    thumbnailFile: blrt.get("thumbnailFile").url(),
                                                    publicName: blrt.get("publicName"),
                                                    createdAt: blrt.createdAt,
                                                    creator: JSON.stringify(blrt.get("creator").get("name")),
                                                    blrtUrl: constants.embedServer + "/blrt/" + _.encryptId(blrt.id),
                                                    encryptedBlrtId: _.encryptId(blrt.id),
                                                    status: status
                                                })
                                            })
                                        }
                                    )
                                })
                            .then(
                                function () {
                                    var last = false
                                    if (blrtInfo.length < numberOfItemInPage) {
                                        last = true;
                                    }
                                    res.render('ocList', {blrts: blrtInfo, pageNo: pageNo, last: last});
                                }
                            )
                    }
                    else {
                        res.send("<h1>You do not have the access<h1>")
                    }
                }, function (err) {
                    res.send("<h1>OCList ID do not exist</h1>")
                })
        }
    })
    app.post("/ocList/:id/admin/approve", function (req, res) {
        var blrtId = req.body.encryptedId;
        blrtId = _.decryptId(blrtId);

        Parse.Cloud.useMasterKey();
        var user = req.user;
        if (!user) {
            res.json({success: false, message: "please login"})
        } else {
            checkUser(user, req).then(
                function (ocList) {
                    if (ocList) {
                        var blrt = new Parse.Object("ConversationItem")
                        blrt.id = blrtId;
                        Parse.Promise.when(ocList.fetch(), blrt.fetch())
                            .then(function (list, b) {
                                var pendingItemQuery = new Parse.Query("OCListItemPending");
                                return pendingItemQuery
                                    .equalTo("blrt", b)
                                    .equalTo("oclist", list)
                                    .first()
                                    .then(function(pendingItem){
                                        /**
                                         * pendingItem may be existing due to some users toggle the disable button back and forth for fun
                                         */
                                        if(!pendingItem){
                                            var pendingItem = Parse.Object("OCListItemPending");
                                            pendingItem.set("oclist", list);
                                            pendingItem.set("blrt", b);
                                        };
                                        pendingItem.set("status", "approved");


                                        return new Parse.Query("OCListItem")
                                            .equalTo("oclist",list)
                                            .descending("index")
                                            .first()
                                            .then(function (firstItem) {
                                                var item = Parse.Object("OCListItem");
                                                item.set("oclist", list);
                                                item.set("blrt", b);
                                                item.set("dbversion", 0);

                                                var index = 0;
                                                if(firstItem)
                                                {
                                                    index= firstItem.get("index");
                                                }
                                                item.set("index",(Math.floor(index))+1);
                                                return Parse.Promise.when(pendingItem.save(), item.save())

                                            })


                                    })

                                    .then(function (pendingItem) {
                                        var emailText = "OCListItemId: " + pendingItem.id + "\n" + "Blrt Url: "
                                            + constants.siteAddress + "/blrt/" + _.encryptId(blrtId);
                                        // Mandrill.sendEmail({
                                        //     message: {
                                        //         text: emailText,
                                        //         subject: "[Blrt Chaser Promo]",
                                        //         from_email: "no-reply@blrt.com",
                                        //         from_name: "Blrt",
                                        //         to: [
                                        //             {
                                        //                 email: constants.oclistSuperAdmin.email,
                                        //                 name: constants.oclistSuperAdmin.name
                                        //             }
                                        //         ]
                                        //     },
                                        //     async: true
                                        // }, {
                                        //     success: function (httpResponse) {
                                        //         console.log(httpResponse);
                                        //         //response.success("Email sent!");
                                        //     },
                                        //     error: function (httpResponse) {
                                        //         console.error(httpResponse);
                                        //         //response.error("Uh oh, something went wrong");
                                        //     }
                                        // });

                                        res.json({success: true, message: "good"})
                                    });
                            }, function (err) {
                                res.json({success: false, message: err})
                            })
                    }
                    else {
                        res.json({success: false, message: "You do not have access"})
                    }
                }
            )
        }

    })

    app.post("/ocList/:id/admin/toggle", function (req, res) {
        var blrtId = req.body.encryptedId;
        blrtId = _.decryptId(blrtId);
        var toggle = req.body.toggle == "true";
        Parse.Cloud.useMasterKey();
        var user = req.user;
        if (!user) {
            res.json({success: false, message: "please login"})
        } else {
            checkUser(user, req).then(
                function (ocList) {
                    if (ocList) {
                        var blrt = new Parse.Object("ConversationItem")
                        blrt.id = blrtId;
                        Parse.Promise.when(ocList.fetch(), blrt.fetch())
                            .then(function (l, b) {
                                var pendingItemQuery = new Parse.Query("OCListItemPending");
                                return pendingItemQuery
                                    .equalTo("blrt", b)
                                    .equalTo("oclist", l)
                                    .first()
                                    .then(function (itemPending) {
                                        if (!itemPending) {
                                            itemPending = Parse.Object("OCListItemPending");
                                            itemPending.set("oclist", l);
                                            itemPending.set("blrt", b);
                                        }

                                        if (!toggle)
                                        //add to oclistItemPending with "disabled" status if toggle is false i.e. "approve" is disabled;
                                        {
                                            itemPending.set("status", "disabled");
                                        }
                                        else
                                        //add to oclistItemPending with "enabled" status if toggle is false i.e. "approve" is enabled again;
                                        {
                                            itemPending.set("status", "enabled");
                                        }
                                        return itemPending.save()
                                    })
                            })
                            .then(function () {
                                res.json({success: true, message: "add item to ocListItemPending table"})
                            }, function () {
                                res.json({success: true, message: "can not save to ocListItemPending"})
                            })


                    }
                })
        }


    })

    /**
     *
     *
     *
     * */
    function checkUser(user, req) {
        var userId = user.id;
        var ocListId = _.decryptId(req.params.id);
        var ocListQuery = new Parse.Query("OCList");
        var auth = false;
        return ocListQuery.get(ocListId).then(
            function (ocList) {
                _.each(ocList.get("admin"), function (admin) {
                    if (admin.id == userId) {
                        auth = true
                    }
                });
                if (auth) {
                    return ocList;
                }
                else {
                    return false
                }
            },
            function () {
                return false;
            }
        )
    };
    function getBlrtByRequestId(id, pageNo, itemsPerPage) {
        var blrtRequest = new Parse.Object("BlrtRequest");
        blrtRequest.id = id;
        var conversationQuery = new Parse.Query("Conversation")
            .equalTo("blrtRequest", blrtRequest);

        return new Parse.Query("ConversationItem")
            .equalTo("isPublicBlrt", true)
            .matchesQuery("conversation", conversationQuery)
            .include("creator")
            .equalTo("type", 0)
            .descending("createdAt")
            .skip(itemsPerPage * (pageNo))
            .limit(itemsPerPage)
            .find();

    };
    function checkStatus(blrtArray, ocList) {
        return new Parse.Query("OCListItemPending")
            .equalTo("oclist", ocList)
            .containedIn("blrt", blrtArray)
            .find()
            .then(
                function (data) {
                    var blrtIds = [];
                    if (data) {
                        data.forEach(
                            function (i) {
                                blrtIds.push(
                                    {blrtId:i.get("blrt").id,status: i.get("status")});
                            }
                        )
                    }
                    return blrtIds;
                },
                function (err) {
                    return false;
                }
            )
    }
}
