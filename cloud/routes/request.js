var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var loggler = require("cloud/loggler");
var _ = require("underscore");

module.exports = function (app){

app.all('/request/send', function(req, res) {
	res.render('request', {
		constants: constants
	});
});

app.all("/make", function (req, res, next) {
    return res.redirect("https://web.blrt.com/make")
});

app.all("/make/(:name)?", function (req, res, next) {
	Parse.Cloud.useMasterKey();

	var pageArgs = { 
		blrt_url: req.useragent.isAndroid
			? _.androidLink(constants.appProtocol + "://" + req.host + req.url)
			: (constants.appProtocol + "://" + req.host + req.url),
		originalUrl: constants.appProtocol + "://" + req.host + req.url,
		isAndroid: req.useragent.isAndroid,
		app_store_url: constants.appStoreUrl,
		app_store_id: constants.appStoreId,
		http_user_agent : req.headers['user-agent'],
		blrt_website_url: constants.blrtWebsiteUrl,
		constants: constants,
		resourceHost: ("//s3.amazonaws.com/blrt-webplayer/" + constants.stream),
		isBlrtDebug: false
	};

	var fetchPromise = Parse.Promise.as();

	var developerMode   = false;
	var redirect;

	var thumbnail 		= false;
	var convoTitle  	= false;
	var requestItems  	= "";
	var creatorName  	= false;
	var message  		= false;

	var userAvatar		= "https://s3.amazonaws.com/blrt-webplayer/m/img/anony.png";

	if(req.query.req)
		fetchPromise = new Parse.Query("BlrtRequest")
			.equalTo("objectId", _.decryptId(req.query.req))
			.include("creator")
			.first();

	fetchPromise.then(function (blrtRequest) {
		if(blrtRequest) {
			if(blrtRequest.get("type") == "api.blrt.com")
				developerMode = true;
			else{
				creatorName = blrtRequest.get("creator").get("name");
				if(blrtRequest.get("creator").get("avatar")){
					userAvatar = blrtRequest.get("creator").get("avatar").url();
				}
			}

			if(blrtRequest.get("name"))
				convoTitle = blrtRequest.get("name");

			var www = blrtRequest.get("url");
			var pdf = blrtRequest.get("pdf");
			var img = blrtRequest.get("mediaUrls");

			img = img ? img.length - (pdf ? 1 : 0) : 0;

			if(www) {
				thumbnail = "//s3.amazonaws.com/blrt-emails/www-thumbnail.png";
				requestItems = "1 website";
			} 
			if(pdf) {
				thumbnail = thumbnail || "//s3.amazonaws.com/blrt-emails/pdf-thumbnail.png";
				requestItems += (requestItems ? (img ? ", " : " and ") : "") + "1 PDF";
			}
			if(img) {
				thumbnail = thumbnail || blrtRequest.get("mediaUrls")[0];
				requestItems += (requestItems ? " and " : "") + img + " image" + (img != 1 ? "s" : "");
			}

			if(blrtRequest.get("message"))
				message = blrtRequest.get("message");

			//if someone already made convo for this request, redirect to that convo
			if(blrtRequest.get("conversation"))
				redirect = "/conv/" + _.encryptId(blrtRequest.get("conversation").id);

			//if accepted but don't have convo pointer (only old requests), than make a normal blrt
			else if(blrtRequest.get("answered")){
				loggler.global.log("User redirected to because the link is answered: ", req.url).despatch();
				redirect = (req.url.replace("req=" + req.query.req, ""));
			}
		} else {
			if(req.params.name)
				convoTitle = req.params.name;

			if(req.query.url) {
				thumbnail = "//s3.amazonaws.com/blrt-emails/www-thumbnail.png";
				requestItems = "1 website";
			}

			if(req.query.m) {
				if(!req.query.url) {
					_.each(req.query.m, function (mediaUrl) {
						if(_.extension(mediaUrl).toLowerCase() == ".pdf")
							thumbnail = "//s3.amazonaws.com/blrt-emails/pdf-thumbnail.png";
						else if(!thumbnail)
							thumbnail = mediaUrl;
					});
				}

				var numPdf = 0;
				var numImg = 0;

				_.each(req.query.m, function (mediaUrl) {
					if(_.extension(mediaUrl).toLowerCase() == ".pdf") ++numPdf;
					else 											  ++numImg;
				});

				requestItems = requestItems ? requestItems + (numPdf && numImg ? ", " : " and ") : "";
				if(numPdf) requestItems += numPdf + " PDF" + (numPdf != 1 ? "s" : "") + (numImg ? " and " : "");
				if(numImg) requestItems += numImg + " image" + (numImg != 1 ? "s" : "");
			}
		}



		if(redirect)
			res.redirect(redirect);
		else if(developerMode || requestItems || creatorName || convoTitle) {
			if(req.useragent.isiPad || req.useragent.isiPhone || req.useragent.isiPod || req.useragent.isAndroid)
				res.render('iOS_redirect', pageArgs);
			else if(req.useragent.isMobile)
				res.render('mobile', pageArgs);
			else {
				pageArgs = _.extend(pageArgs, {
					developerMode: 	developerMode,
					thumbnail: 		thumbnail 		|| "//www.blrt.com/images/request-default-thumb.png",
					convoTitle: 	convoTitle 		|| "-",
					requestItems: 	requestItems 	|| "-",
					creatorName: 	creatorName 	|| "-",
					message: 		message 		|| "-",
					user_avatar:    userAvatar
				});

				if(!pageArgs.thumbnail.match(/^(.*:)?\/\//)) {
					if(pageArgs.thumbnail.match(/^blrtrequest.s3.amazonaws.com/))
						pageArgs.thumbnail = "https://" + pageArgs.thumbnail;
					else
						pageArgs.thumbnail = "http://" + pageArgs.thumbnail;
				}

				res.render('wrongPlatformRequest', pageArgs);
			}
		} else
			res.redirect("/request");
	});
});


};