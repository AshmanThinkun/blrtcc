var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var accountTypeJS = require("cloud/accounttype.js");
var emailJS = require("cloud/email.js");
var developerResetKeys = require("cloud/api/developer/resetKeys");
var loggler = require("cloud/loggler");
var _ = require("underscore");

var templateSendFn = require('../specialBlrtCreation.js');
let count = 0;

var parseExpressCookieSession = require('../../lib/parse-express-session-token');
module.exports = function (app) {

//all admin pages should pass oa admin auth check
//the order of the routes are very important, please place this route above all the other route to let it process the auth

    app.use('/oa', parseExpressCookieSession(), function (req, res, next) {
        //check user permission
        if (!req.user) {
            // return res.render("message", {message: "Please <a href='/oas/login'>login</a> as admin."});
            return res.redirect("/");
        }
        Parse.Cloud.useMasterKey();
        return new Parse.Query("Admin")
            .equalTo("user", req.user)
            .first()
            .then(function (admin) {
                // console.log(admin)
                if (!admin) {
                    return res.render("message", {message: "Please <a href='/oa/login'>login</a> as admin."});
                } else {
                    return next();
                }
            });
    });

    app.get('/oa/crypt-link-generator', function (req, res) {
        function resError(str) {
            return res.jsonp({success: false, message: str});
        }

        if (req.query == null)
            return resError("No arguments provided.");

        var args = req.query.args || [];
        var target = _.singleSplit(req.query.target || "", "-");
        // we may need further investigation for security.
        if (target.length != 0) {
            var x;

            switch (target[0]) {
                case "_":
                    x = _;
                    break;
                case "email":
                    x = email;
                    break;
                default:
                    return resError("Invalid target provided.");
            }

            if (target.length == 2)
                target = x[target[1]];
            else
                target = x;
        } else
            return resError("No target provided.");

        return res.jsonp({success: true, message: target.apply(target, args)});
    });

    app.get('/oa/send-convo-template',
        function (req, res) {
            function resError(str) {
                return res.jsonp({success: false, message: str});
            }

            if (req.query == null)
                return resError("No arguments provided.");

            var users = null;
            if ((users = req.query.users) == null)
                return resError("No users provided.");

            if (typeof users != "object")
                return resError("Invalid users array provided.");

            if (users.length == 0)
                return resError("No users provided.");

            var invalidUsers = [];
            for (var user_i = users.length - 1; user_i >= 0; --user_i) {
                if (!util.isEmail(users[user_i]))
                    invalidUsers.push(users[user_i]);
            }

            // if (invalidUsers.length != 0)
            //     return resError("Invalid users provided: " + JSON.stringify(invalidUsers));

            var slug = null;
            if (!req.query.slug || (slug = req.query.slug.trim()) == "")
                return resError("No template slug provided.");

            console.log('\n\n\n===email==='+users[0]);
    
            Parse.Cloud.run('processQueuedConvoTemplates', {
                user: users[0],
            }).then((result)=>{
                console.log('\n\nhaha===success===');
                console.log('success result: '+JSON.stringify(result));
                if(result.success) {
                    // return res.jsonp({success: true});
                }else {
                    console.log('\n\n===user no found===');
                    // return res.jsonp({success: false, error: result.error});
                }
            }, (error)=>{
                console.log('\n\nhaha===error===');
                console.log('error result: '+JSON.stringify(error));
                // return res.jsonp({success: false, error: error});
            });

            return res.jsonp({success: true});
        });

    app.get('/oa/manage-account-type',
        function (req, res) {
            function resError(str) {
                return res.jsonp({success: false, message: str});
            }

            if (req.query == null)
                return resError("No arguments provided.");

            var user = null;
            if (req.query.user == null || (user = req.query.user.trim()) == "")
                return resError("No user provided.");

            if (!util.isEmail(user))
                return resError("Invalid email provided.");

            var destination = null;
            if (!req.query.destination || (destination = req.query.destination.trim()) == "")
                return resError("No account type destination slug provided.");

            var duration = req.query.duration;
            if (duration == null || duration.trim() == "")
                duration = null;
            else {
                if (typeof duration == "string" && !_.isNumber(duration = Number(duration)))
                    return resError("Could not convert duration to an integer");
                if (typeof duration != "number")
                    return resError("Duration should be a number");
                if (duration < 0)
                    return resError("Provided duration must be greater than or equal to 0, or completely blank");
            }


            return accountTypeJS.requestAccountUpgrade({
                user: null,
                params: {
                    requestFor: constants.accountType.RequestFor.admin,
                    data: {
                        userEmail: user,
                        destination: destination,
                        duration: duration,
                        invitedByPioneer: req.query.invitedByPioneer
                    }
                }
            }).then(function (status) {
                if (status.success)
                    return res.jsonp({success: true});
                else
                    return resError(status.message);
            }, function (error) {
                return resError("<strong>Server error:</strong><br/>" + JSON.stringify(error));
            });
        });

    app.get('/oa/approve-developer',
        function (req, res) {
            Parse.Cloud.useMasterKey();

            function resError(str) {
                return res.jsonp({success: false, message: str});
            }

            if (req.query == null)
                return resError("No arguments provided.");

            var user = null;
            if (req.query.user == null || (user = req.query.user.trim()) == "")
                return resError("No user provided.");

            if (!util.isEmail(user))
                return resError("Invalid email provided.");

            return new Parse.Query(Parse.User)
                .equalTo("username", user.toLowerCase())
                .first()
                .then(function (user) {
                    if (!user)
                        return resError("Failed to find user with provided email address.");

                    if (user.get("developerStatus") == "active")
                        return resError("Requested user's developer status is already approved");

                    return developerResetKeys.resetUserObjectKeys(user)
                        .set("developerStatus", "active")
                        .save()
                        .then(function () {
                            return Parse.Promise.when(
                                emailJS.sendTextEmail(
                                    {name: "Blrt", email: "no-reply@blrt.com"},
                                    [constants.developerAdmin],
                                    "Blrt developer approved: " + user.get("email"),
                                    "A user has been approved as a Blrt developer. Their developer details follow.\n\n" +

                                    "Email: " + user.get("email")
                                ),
                                emailJS.sendTextEmail(
                                    {name: "Blrt", email: "no-reply@blrt.com"},
                                    [{name: user.get("name"), email: user.get("email")}],
                                    "You are now a Blrt Developer.",
                                    "Hi " + user.get("name") + ",\n\n" +

                                    "Congratulations, you're the world's newest Blrt developer.\n" +
                                    "We're excited to have you onboard and are keen to help you through your development journey.\n\n" +

                                    "A public API key and a private key have been generated for you.\n\n" +

                                    "Public API key: " + user.get("developerPublicApiKey") + "\n" +
                                    "Private key: visible in the developer control panel\n\n" +

                                    "You'll need these to make API calls. You can access and reset them anytime at https://developer.blrt.com/.\n\n" +

                                    "Don't forget to drop us a line at support@blrt.com if there's any help or clarification you need.\n\n" +

                                    "Happy Blrt developing,\n" +
                                    "Blrt Team."
                                )
                            ).fail(function (emailError) {
                                loggler.global.log("ALERT: Failed to send /approve-developer emails:", emailError).despatch();
                            }).always(function () {
                                return res.jsonp({success: true});
                            });
                        }, function (promiseError) {
                            loggler.global.log("ALERT: Failed to save /approve-developer user:", promiseError).despatch();
                            return resError("<strong>Server error:</strong><br/>" + JSON.stringify(promiseError));
                        });
                }, function (promiseError) {
                    loggler.global.log("ALERT: Failed to search for /approve-developer user:", promiseError).despatch();
                    return resError("<strong>Server error:</strong><br/>" + JSON.stringify(promiseError));
                });
        });

    app.get("/oa/approve-pioneer", function (req, res) {
        function resError(str) {
            return res.jsonp({success: false, message: str});
        }

        if (req.query == null)
            return resError("No arguments provided.");
        var user = null;
        if (req.query.user == null || (user = req.query.user.trim()) == "")
            return resError("No user provided.");
        if (!util.isEmail(user))
            return resError("Invalid user provided");
        if (req.query.insecureKey != "BUYBGTKMdsfweio724rwjksdf")
            return resError("You are not authorised.");


        // set the pioneer status to approved
        Parse.Cloud.useMasterKey();
        return new Parse.Query(Parse.User)
            .equalTo("username", user)
            .first()
            .then(function (result) {
                if (!result)
                    return resError("User doesn't exist.");
                if (result.get("pioneerStatus") == "approved") {
                    return resError("User already approved.")
                }
                result.set("pioneerStatus", "approved")
                    .set("activeInvitee", 0)
                    .set("timesOfPioneerUpgrade", 1);

                return result.save()
                    .then(function () {
                        //send email
                        return emailJS.SendTemplateEmail({
                            template: constants.mandrillTemplates.pioneerApproved,
                            recipients: [{
                                name: result.get("name"),
                                email: user,
                                isUser: true,
                                user: result
                            }],
                            sender: null,
                            creator: null,
                            customMergeVars: {}
                        });
                    }, function (error) {
                        resError("Update pioneeer status failure: " + JSON.stringify(error));
                    }).then(function () {
                        //upgrade the user
                        return accountTypeJS.requestAccountUpgrade({
                            user: null,
                            params: {
                                requestFor: constants.accountType.RequestFor.admin,
                                data: {
                                    userEmail: result.get("username"),
                                    destination: "premium-annual",
                                    duration: 180,
                                    invitedByPioneer: false,
                                },
                                template: "premium-started"
                            }
                        }).then(function () {
                            return emailJS.SendTemplateEmail({
                                template: constants.mandrillTemplates.pioneerUpgradeCompleteAdmin,
                                recipients: [{
                                    name: constants.pioneerAdmin.name,
                                    email: constants.pioneerAdmin.email,
                                    isUser: true
                                }],
                                sender: null,
                                creator: null,
                                customMergeVars: {
                                    "PIONEER_NAME": result.get("name"),
                                    "PIONEER_EMAIL": result.get("email"),
                                    "UPGRADE_DURATION": "6 months",
                                    "ACTIVE_INVITEE_NUM": result.get("activeInvitee") || 0
                                }
                            }).then(function () {
                                return res.jsonp({success: true, message: "Pioneer approved."});
                            });
                        });
                    }, function (error) {
                        resError("Sending email failure: " + JSON.stringify(error));
                    });
            });
    });

    app.get('/oa/duplicate-conversation',
        function (req, res) {
            Parse.Cloud.useMasterKey();

            function resError(str) {
                return res.jsonp({success: false, message: str});
            }

            if (req.query == null)
                return resError("No arguments provided.");

            var conversationItemId = null;
            if (req.query.conversationItemId == null || (conversationItemId = req.query.conversationItemId.trim()) == "")
                return resError("No conversationItemId provided.");

            var names = "";
            if (req.query.names && _.isString(req.query.names))
                names = req.query.names;

            names = names.split("\n");

            return new Parse.Query("ConversationItem")
                .equalTo("objectId", conversationItemId)
                .first()
                .then(function (convoItem) {
                    if (!convoItem)
                        return resError("ConversationItem with that ID was NOT found.");

                    return Parse.Cloud.run("conversationDuplicateFromBlrt", {
                        api: {
                            version: 1,
                            endpoint: "conversationDuplicateFromBlrt"
                        },
                        device: {
                            version: "1.0",
                            device: "www",
                            os: "www",
                            osVersion: "1.0"
                        },
                        data: {
                            userId: convoItem.get("creator").id,
                            conversationItemId: conversationItemId,
                            names: names
                        }
                    }).then(function (status) {
                        try {
                            status = JSON.parse(status);
                        } catch (error) {
                            return resError("<strong>Server error:</strong><br/>" + JSON.stringify(error));
                        }

                        if (status.success)
                            return res.jsonp({success: true});
                        else
                            return resError(status.message);
                    }, function (error) {
                        return resError("<strong>Server error:</strong><br/>" + JSON.stringify(error));
                    });
                }, function (error) {
                    return resError("<strong>Server error:</strong><br/>" + JSON.stringify(error));
                });
        });

    app.get('/oa/manage-account-status',
        function (req, res) {
            Parse.Cloud.useMasterKey();

            function resError(str) {
                return res.jsonp({
                    success: false, error: {
                        code: 342,
                        message: str
                    }
                });
            }

            //check params
            if (req.query == null)
                return resError("No arguments provided.");
            if (req.query.username == null)
                return resError("Invalid username");
            if (req.query.note == null)
                return resError("Invalid note");
            if (req.query.accountStatus == null)
                return resError("Invalid account status");

            var prep = {
                params: {
                    username: req.query.username,
                    accountStatus: req.query.accountStatus,
                    note: req.query.note
                }
            };

            //call func
            return require("cloud/api/user/changeAccountStatus")[1](prep).then(function (result) {
                return res.jsonp(result);
            }, function (error) {
                return resError("Something went wrong");
            });
        });

    app.get('/oa/videoConversion', function (req, res) {
        var id = req.query.blrtId;
        var blrtUrl = req.query.blrtUrl;
        var resolutionWidth = Number(req.query.resolution.split('x')[0])
        var resolutionHeight = Number(req.query.resolution.split('x')[1])
        var wm_position = Number(req.query.position)
        var wm_type = Number(req.query.type);
        var fps = Number(req.query.frameRate);

        console.log('outputFiles before==='+req.query.outputFiles)
        var outputFiles = Boolean(req.query.outputFiles);

        console.log('\n\n\noutputFiles==='+outputFiles);

        var body = {
            id: id,
            fps: fps,
            wm_position: wm_position,
            wm_type: wm_type,
            resolutionWidth: resolutionWidth,
            resolutionHeight: resolutionHeight,
        }

        //step1 check if the video exist
        Parse.Cloud.httpRequest({
            url: "http://ec2-52-55-48-231.compute-1.amazonaws.com:80/query",
            method: "POST",
            headers: {
                "key": "LBRsh3O5Of9LrZ13vISL77ZIGM9hnlsawvpzEFIo",
                "Content-Type": "application/json"
            },
            body: body
        })
        //fetch playData
        .then(function (checkResult) {
            //send create request
            var status = JSON.parse(checkResult.text).status;
            console.log("\n\n\ncheckResult: "+JSON.stringify(checkResult.text));
            if(status == 0) {
                var blrtId = _.decryptId(id)
                return new Parse.Query("ConversationItem")
                    .equalTo("objectId", blrtId)
                    .include("conversation")
                    .include("creator")
                    .include("media")
                    .first({useMasterKey: true})
                    .then(function (blrtItem) {
                        if (!blrtItem) {
                            console.log('\n\n\ninvalid blrtId');
                            return new Parse.Promise.error({success: false, url: "invalid url (blrtId)"});
                        } else {
                            console.log('\n\n\nvalid blrtId');
                            console.log('outputFiles=== ==='+outputFiles);
                            return Parse.Cloud.httpRequest({
                                url: "http://ec2-52-55-48-231.compute-1.amazonaws.com:80/create-video",
                                method: "POST",
                                headers: {
                                    "key": "LBRsh3O5Of9LrZ13vISL77ZIGM9hnlsawvpzEFIo",
                                    "Content-Type": "application/json"
                                },
                                body: {
                                    blrtId: id,
                                    touchDataFile: blrtItem.get("touchDataFile").url(),
                                    audioFile: blrtItem.get("audioFile").url(),
                                    argument: blrtItem.get("argument"),
                                    encryptValue: blrtItem.get("encryptValue"),
                                    encryptType: blrtItem.get("encryptType"),
                                    media: _.map(blrtItem.get("media"), function (asset) {
                                        return {
                                            id: asset.id,
                                            encryptType: asset.get("encryptType"),
                                            encryptValue: asset.get("encryptValue"),
                                            mediaFile: asset.get("mediaFile").url(),
                                            mediaFormat: asset.get("mediaFormat"),
                                            pageArgs: asset.get("pageArgs")
                                        };
                                    }),
                                    isPublic: false,
                                    fps: fps,
                                    outputFiles: outputFiles,
                                    wm_type: wm_type,
                                    wm_position: wm_position,
                                    resolutionWidth: resolutionWidth,
                                    resolutionHeight: resolutionHeight,
                                    // callback: "https://localhost:8081/notifyVideoConversionFinished?webUrl=" +blrtUrl+ "&key=name&recipient="+req.query.email
                                    callback: process.env.SITE_ADDRESS + "/notifyVideoConversionFinished?webUrl=" + blrtUrl + "&key=name&recipient=" + req.query.email
                                }
                            })
                        }
                    })
                    .then(function (creationResult) {
                        console.log('\n\ncreationResult: '+JSON.stringify(creationResult));
                        var result = JSON.parse(creationResult.text);
                        res.jsonp(result);
                    })
            }else if(status == 1) {
                res.jsonp({success: true, status: 1, url: 'Failed to download media'});
            }else if(status == 2) {
                console.log('\n\n\nurl to video==='+JSON.parse(checkResult.text).url);
                res.jsonp({success: true, status: 2, url: JSON.parse(checkResult.text).url});
            }else {
                res.jsonp({success: false, status: -1, url: JSON.parse(checkResult.text).url});
            }
        })
        .fail(function (e) {
            console.log(e);
            res.jsonp(e)
        })
    });

    app.get('/oa/getUserAccountTrail', function (req, res) {
        console.log('something==='+JSON.stringify(Parse.User));
        var email = req.query.email;
        var userInfo = [];
        new Parse.Query(Parse.User).equalTo('email', email).first()
            .then(function (user) {
                // console.log('===info==='+JSON.stringify(user));
                if(user){
                    let signupDevice=user.get('signupDevice');
                    let signupOS=user.get('signupOS');
                    let signupBlrtVersion=user.get('signupAppVersion');
                    let lastLoginDate=user.get('lastActive') && user.get('lastActive').toISOString().slice(0, 10);
                    let lastDevice=user.get('lastDevice');
                    let lastBlrtVersion=user.get('lastAppVersion');
                    userInfo.push({
                        userId: user.id,
                        name: user.get('name') || user.get('username'),
                        phoneNumber: user.get('phone'),
                        signupDevice: signupDevice,
                        signupOS: signupOS,
                        signupBlrtVersion: signupBlrtVersion,
                        lastLoginDate: lastLoginDate,
                        lastDevice: lastDevice,
                        lastBlrtVersion: lastBlrtVersion,
                    });
                }
                return new Parse.Query('AccountTypeLog').include('destinationAccountTypeId').equalTo('userId', user).find()
            })
            .then(function (trails) {
                // console.log(trails.object.toJSON())
                // console.log('trails==='+JSON.stringify(trails));
                var result=[]
                if(trails.length > 0) {
                    trails.forEach(function (value) {
                        let tmp_endDate = null;
                        let duration = 0;
                        // console.log('\n\n=== === ===');
                        // console.log('===createdAt==='+value.get('createdAt').toISOString().slice(0, 10));
                        if(value.get('customDuration') > 0) {
                            console.log('===customDuration===');
                            duration = value.get('customDuration');
                            tmp_endDate = new Date(value.get('createdAt') * 1 + 1000 * 60 * 60 * 24 * duration);
                        }else {
                            console.log('===defaultDuration===');
                            if(value.get('destinationAccountTypeId').get('slug') != 'free') {
                                duration = value.get('destinationAccountTypeId').get('dayDuration');
                                // console.log('===duration==='+duration);
                                // console.log('createdAt==='+value.get('createdAt').toISOString().slice(0, 10));
                                tmp_endDate = new Date(value.get('createdAt') * 1 + 1000 * 60 * 60 * 24 * duration); // 1000 milliseconds per second, 60 seconds per minute, 60 minutes per hour, 24 hours per day, and duration is day 
                            }
                        }
                        // console.log('\ntmp_endDate==='+tmp_endDate);
                        // console.log('createdAt==='+value.get('createdAt').toISOString().slice(0, 10));
                        result.push({
                            accountTypeId:value.get('destinationAccountTypeId').get('slug'),
                            startDate:value.get('createdAt') && value.get('createdAt').toISOString().slice(0, 10),
                            endDate:tmp_endDate && tmp_endDate.toISOString().slice(0, 10),
                        })
                    })
                    console.log(result);
                    console.log(userInfo);
                    res.jsonp({result: result, userInfo: userInfo});
                }else {
                    res.jsonp({userInfo: null});
                }
            })
            .fail(function (e) {
                console.log(e)
                res.end('something went wrong')
            })
    });

    app.get('/oa/getNonUserAccountTrail', function (req, res) {
        console.log('\n\n\n===getNonUserAccountTrail===');
        var email = req.query.email;
        var nonuserInfo = [];
        new Parse.Query('NonUser')
            .equalTo('value', email)
            .include('user')
            .include('referredBy')
            .first()
            .then(function (nonuser) {
                console.log('\n\n\n===info==='+JSON.stringify(nonuser));
                if(nonuser) {
                    nonuserInfo.push({
                        email: nonuser.get('type') === 'email' ? nonuser.get('value') : undefined,
                        phoneNumber: nonuser.get('type') === 'phoneNumber' ? nonuser.get('value') : undefined,
                        createdAt: nonuser.get('createdAt'),
                        claimedBy: nonuser.get('user') ? nonuser.get('user').get('username') : undefined,
                        claimedAt: nonuser.get('claimedAt'),
                        source: nonuser.get('source'),
                        referredBy: nonuser.get('referredBy') ? nonuser.get('referredBy').get('username') : undefined,
                    });
                    res.jsonp({nonuserInfo: nonuserInfo});
                }else {
                    res.jsonp({nonuserInfo: null});
                }
            }).fail(function (e) {
                console.log(e)
                res.end('something went wrong')
            })
    });

    app.get("/oa", function (req, res) {
        console.log("response")
        res.jsonp({success: true, stream: constants.stream});
    });
    

    // this new endpoint is for sending bons to welcome conve.
    // there is one problem with http request, previous process 
    // is html sends http request to backend side to call cloud 
    // code function -- queuedConversationTemplates, at meantime,
    // the html is also waiting for result from benckend, otherwise,
    // html will send another request again which causing multiple templates sent.
    // the new logic is when backend receives the request, instead of calling cloud 
    // code functon, run the Parse.Query. 
    app.get('/oa/send-convo-template-new', function (req, res) {
        function resError(str) {
            return res.jsonp({success: false, message: str});
        }

        console.log('emails==='+req.query.users);
        console.log('stream==='+req.query.stream);
        console.log('arg==='+req.query.arg);
        console.log('info==='+req.query.info);

        // console.log('\n\n\n===function templateSendFn===');
        // console.log(templateSendFn);

        // return res.jsonp({success: true});
 
        if(req.query == null)
            return resError("No arguments provided.");

        var users = null;
        if((users = req.query.users) == null)
            return resError("No users provided.");  

        if(typeof users != "object")
            return resError("Invalid users array provided.");  

        if(users.length == 0)
            return resError("No users provided.");  

        var invalidUsers = [];
        for (var user_i = users.length - 1; user_i >= 0; --user_i) {
            if (!util.isEmail(users[user_i]))
                invalidUsers.push(users[user_i]);
        }  

        var slug = null;
            if (!req.query.slug || (slug = req.query.slug.trim()) == "")
                return resError("No template slug provided.");

        console.log('emails==='+users);
        console.log('stream==='+req.query.stream);
        console.log('arg==='+req.query.arg);
        console.log('info==='+req.query.info);

        // let params = {
        //     emails: users,
        //     slug: slug,
        //     arg: req.query.arg, // for checking if create new convo or existing convo
        //     info: req.query.info, // could be convoId or convoTitle
        // }

        let usersEmail = new Array();

        for(let i = 0; i < users.length; ++i) {
            usersEmail[i] = users[i].toLowerCase();
        }  

        new Parse.Query(Parse.User)
            .containedIn(constants.user.keys.username, usersEmail)
            .find({useMasterKey: true})
            .then(results=>{
                // l.log("userQuery.find() results:", results);
                console.log('\n\nresults length==='+results.length);
                if(results.length > 0) {
                    if(req.query.arg && req.query.arg != 'convoTitle') {
                        console.log('\n\n===template==='+req.query.arg);
                        return templateSendFn({
                            slug: req.query.slug,
                            users: results,
                            info: req.query.info,
                            arg: req.query.arg,
                        }, true).then(function(result) { 
                            console.log('\n\nresult result result==='+JSON.stringify(result));
                            if(result) {
                                res.jsonp({success: true});
                            }else {
                                res.jsonp({success: false, error: 'Parse error'});
                            }
                        }, (error)=>{ 
                            console.log('\n\n===error1===');
                            res.jsonp({success: false, error: error}); 
                        }); 
                    }
                    // response.success({success: true});
                }else {
                    console.log('\n\n???===user no found===')
                    res.jsonp({success: false, error: 'User no found'});
                }
            }).catch((error)=>{
                res.jsonp({success: false, error: error});
            });
    });


    app.get('/oa/manage-account-deletion', function (req, res) {
        console.log('\n\n\n===req.query.email==='+req.query.email);
        return Parse.Cloud
            .run("userChangeAccountStatus", {
                api: {
                    version: 1,
                    endpoint: "userChangeAccountStatus"
                },
                device: {
                    version: "1.0",
                    device: "www",
                    os: "www",
                    osVersion: "1.0"
                },
                data: {
                    accountStatus: "deleted", 
                    from: "admin", 
                    username: req.query.email
                },
            })
            .then(function(result) {
                result = JSON.parse(result);
                console.log('\n\n\nmanage account deletion result: '+result.success);
                console.log('\n\n\nmanage account deletion result: '+result.message);
                if(result.success) {
                    res.jsonp({success: true, message: 'account deletion is done'})
                }else {
                    console.log('result error message==='+result.message);
                    res.jsonp({success: false, message: result.message});
                }
            }, function(error) {
                console.log(error);
                return resError(error);
            });
    });

//end of exports
};

