var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var webplayer = require("cloud/webplayer");
var webrecorder = require("cloud/webrecorder");
var moment = require('moment');
var loggler = require("cloud/loggler");
var _ = require("underscore");


module.exports = function (app) {

// (?:\/conv\/(.*?))?\/blrt\/(.*?).json
    app.all("(/conv/*)?/blrt/*.json", function (req, res, next) {
        function resMsg(str) {
            return res.jsonp({
                success: false,
                message: str
            });
        }

        //prepare the params
        var params = {};
        params.data = {
            url: constants.siteAddress + req.url.substring(0, req.url.indexOf(".json")),
            includeMedia: req.query.media ? true : false
        };
        params.api = {version: 1, endpoint: "conversationItemFromUrl"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };
        Parse.Cloud.useMasterKey();

        // res.end('aaaa')
        // console.log('adxadc')

        return Parse.Cloud.run("conversationItemFromUrl", params, {
            success: function (response) {
                console.log(response)
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    //todo open source server will be crushed with multiple response sent
                    // resMsg("Something went wrong, please try again later");
                }
                res.jsonp(response);
            },
            error: function (error) {
                // resMsg("Something went wrong, please try again later");
            }
        });
    });

    app.all("/create/publicBlrt/toCSV", function(req, res, next){
        Parse.Clound.run("conversationItemPublicBlrt")
                .then(function (result) {
                    result = JSON.parse(result);
                    if (result.success) {
                        console.log('success');
                    }
                });
    })


    app.all("/blrt/:blrtId", handleBlrtPage);
    app.all("/conv/:convId/blrt/:blrtId", handleBlrtPage);
    function handleBlrtPage(req, res, next) {
        console.log('\/conv\/ req url==='+req.url);
        res.redirect(constants.webappSiteAddress + req.url)
    };


    app.all("/blrt/:blrtId/reply", handleBlrtReply);
    app.all("/conv/:convId/blrt/:blrtId/reply", handleBlrtReply);
    function handleBlrtReply(req, res, next) {
        function resMsg(str) {
            return res.jsonp({
                success: false,
                message: str
            });
        }
        // desktop user
        var renderWebPlayer = false;
        try {
            if (req.useragent.isChrome && Number(req.useragent.Version.split('.')[0]) >= 38) {
                // for chrome v38+ user only

                // TODO:
                // Microsoft Edge is using a user agent containing "Chrome". e.g. "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240"
                // the express-useragent lib will detect it as Chrome; however, Edge is not Chrome, and it doesn't support NaCl
                // this case is handled here because until 07/08/2015, express-useragent lib still cannot distinguish Edge and Chrome
                // this part will be modified / deleted when express-useragent is updated, or when Edge starts support NaCl.
                var isMsEdge = /edge\/([\d\w\.\-]+)/i.test(req.headers['user-agent']);
                renderWebPlayer = !isMsEdge;
            }
        } catch (e) {
        }

        if (renderWebPlayer) {
            var pageArgs = {constants: constants};
            pageArgs.resourceHost = "//s3.amazonaws.com/blrt-webplayer/" + constants.stream;
            pageArgs.isBlrtDebug = false;
            pageArgs.authType = 0;
            if (!isNaN(req.query.authType)) {
                pageArgs.authType = Number(req.query.authType);
            }
            pageArgs.naclarg = JSON.stringify(webrecorder.getEmbeddedRecordData(req.params.blrtId, false));
            return res.render('webRecorder', pageArgs);
        } else {
            return res.redirect("http://www.blrt.com");
        }
    };

// handle "(/conv/:convId)?/blrt/:blrtId"
// it's written in 2 separated routers to fix the following Express.js bug:
// for "(/conv/:convId)?/blrt/:blrtId", router will fill req.params.convId="conv/<convId>", and req.params.blrtId="<convId>"
    app.post("/blrt/:blrtId/play", handleBlrtPlay);
    app.post("/conv/:convId/blrt/:blrtId/play", handleBlrtPlay);
    function handleBlrtPlay(req, res, next) {
        function resMsg(str) {
            return res.jsonp({
                success: false,
                message: str
            });
        }

        //prepare the params
        var params = {};
        params.data = {
            blrtId: _.decryptId(req.params.blrtId)
        };
        params.api = {version: 1, endpoint: "conversationItemView"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };
        // Parse.Cloud.useMasterKey();

        return Parse.Cloud.run("conversationItemView", params, {
            success: function (response) {
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    resMsg("Something went wrong, please try again later");
                }
                res.jsonp(response);
            },
            error: function (error) {
                resMsg("Something went wrong, please try again later");
            }
        });
    };


    app.all("/conv", function (req, res, next) {
        console.log('\n\n===request conv===');
        if (!req.query.convo) {
            return res.redirect("/");
        }
        var str = [];
        for (var p in req.query) {
            if ((p != "convo") && req.query.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(req.query[p]));
            }  
        }
        var queryString = str.join("&");
        if (queryString.length > 0) {
            queryString = "?" + queryString;
        }
        var url = '';
        console.log('query blrt: '+req.query.blrt);
        if(req.query.blrt) {
            url = "/conv/" + req.query.convo + "/blrt/" + req.query.blrt + queryString;
        }else {
            url = "/conv/" + req.query.convo + queryString;
        }
        res.redirect(url);
    });

    app.all("/conv/:convId/reply", handleConvoReply);
    function handleConvoReply(req, res, next) {
        function resMsg(str) {
            return res.jsonp({
                success: false,
                message: str
            });
        }
        // desktop user
        var renderWebPlayer = false;
        try {
            if (req.useragent.isChrome && Number(req.useragent.Version.split('.')[0]) >= 38) {
                // for chrome v38+ user only

                // TODO:
                // Microsoft Edge is using a user agent containing "Chrome". e.g. "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240"
                // the express-useragent lib will detect it as Chrome; however, Edge is not Chrome, and it doesn't support NaCl
                // this case is handled here because until 07/08/2015, express-useragent lib still cannot distinguish Edge and Chrome
                // this part will be modified / deleted when express-useragent is updated, or when Edge starts support NaCl.
                var isMsEdge = /edge\/([\d\w\.\-]+)/i.test(req.headers['user-agent']);
                renderWebPlayer = !isMsEdge;
            }
        } catch (e) {
        }

        if (renderWebPlayer) {
            var pageArgs = {constants: constants};
            pageArgs.resourceHost = "//s3.amazonaws.com/blrt-webplayer/" + constants.stream;
            pageArgs.isBlrtDebug = false;
            pageArgs.authType = 0;
            if (!isNaN(req.query.authType)) {
                pageArgs.authType = Number(req.query.authType);
            }
            pageArgs.naclarg = JSON.stringify(webrecorder.getEmbeddedRecordData(req.params.convId, true));
            return res.render('webRecorder', pageArgs);
        } else {
            return res.redirect("http://www.blrt.com");
        }
    };

    app.all("/conv/(:id)(;(.*))?", function (req, res, next) {
        console.log('\n\nreq: '+req.url);
        res.redirect(constants.webappSiteAddress + req.url)
    });

    //The endpoint for blrt.tv
    Parse.Cloud.define("conversationRecentBlrt", function (req, res) {
        Parse.Cloud.useMasterKey();
        var fakeConvo = new Parse.Object("Conversation");
        var id = _.decryptId(req.params.conversationId);
        fakeConvo.id = id;

        var checkUserPromise = fakeConvo.fetch()
            .then(
                function (fullConvo) {
                    var user = req.user;

                    if (!user) {
                        //the user not login
                        return {user: "NOT"}
                    }
                    else {
                        var roleSkeloton = fullConvo.get("conversationRole");
                        return roleSkeloton.fetch()
                            .then(function (role) {
                                var userRelation = role.relation("users");
                                return userRelation.query().find().then(function (us) {
                                    var currentId = user.id;
                                    var access = false;
                                    _.each(us, function (u) {
                                        if (u.id == currentId) {
                                            access = true
                                        }
                                    })
                                    return access ? {user: "GOOD"} : {user: "BAD"}
                                })
                            })
                    }
                }, function (err) {
                    console.log("Here is an error: " + JSON.stringify(err))
                }
            )
        checkUserPromise.then(function (result) {
            if (result.user == "NOT") {
                res.success("LOGIN")
            } else if (result.user == "BAD") {
                res.success("NOACCESS")
            } else if (result.user = "GOOD") {
                new Parse.Query("ConversationItem")
                    .exists("touchDataFile")
                    .equalTo("conversation", fakeConvo)
                    .descending("createdAt")
                    .include("creator")
                    .find()
                    .then(function (items) {
                        var blrtInfo = [];
                        _.each(items, function (item) {
                            var blrtId = _.encryptId(item.id)
                            blrtInfo.push({
                                blrtId: blrtId,
                                createdAt: item.createdAt,
                                createdBy: item.get("creator").get("name")
                            })
                        })
                        res.success(blrtInfo)
                    })

            } else {
                res.success("UNKNOWN")
            }
        }, function (err) {
            res.error(err)

        })
    });


//display conversation info for fbnotification record
    app.all('/fbcanvas', function (req, res) {
        function unsuccessful() {
            res.render("message", {"message": "Your friend invited you to Join <a href='//www.blrt.com'>Blrt</a>."});
        }

        var requestId;
        if (req.query.request_ids) {
            var lastc = req.query.request_ids.lastIndexOf(",");
            requestId = req.query.request_ids.substring(lastc + 1);
        }
        if (!requestId) {
            return unsuccessful();
        }
        Parse.Cloud.useMasterKey();
        return new Parse.Query("FBNotification")
            .equalTo("requestId", requestId)
            .include("conversation")
            .include("user")
            .first()
            .then(function (fbRecord) {
                if (!fbRecord)
                    return unsuccessful();
                var pageArgs = {
                    blrt_url: req.useragent.isAndroid
                        ? _.androidLink(constants.appProtocol + "://" + req.host + req.url)
                        : (constants.appProtocol + "://" + req.host + req.url),
                    originalUrl: constants.appProtocol + "://" + req.host + req.url,
                    isAndroid: req.useragent.isAndroid,
                    app_store_url: constants.appStoreUrl,
                    app_store_id: constants.appStoreId,
                    http_user_agent: req.headers['user-agent'],
                    blrt_website_url: constants.blrtWebsiteUrl,
                    constants: constants
                };

                var user = fbRecord.get("user");
                conversation = fbRecord.get("conversation");

                return new Parse.Query("ConversationItem")
                    .exists("touchDataFile")
                    .equalTo("conversation", conversation)
                    .descending("createdAt")
                    .include("conversation")
                    .include("conversation.creator")
                    .first()
                    .then(function (item) {
                        if (item == null) {
                            loggler.global.log("ALERT: No item found for conv wrongPlatform page, convo ID: " + id);
                            return unsuccessful();
                        }

                        var duration;

                        try {
                            duration = JSON.parse(item.get("argument"))["AudioLength"];

                            if (!_.isNumber(duration))
                                throw "Not a number!";
                        } catch (e) {
                            loggler.global.log("ALERT: convo wrongPlatform page couldn't extract the duration from Blrt/ReBlrt with item ID: " + item.id);
                            return unsuccessful();
                        }

                        var convo = item.get("conversation");
                        var creator = convo.get("creator");

                        pageArgs = _.extend(pageArgs, {
                            username: user.get("name"),
                            withRelation: false,
                            creatorName: creator.get("name"),
                            convoTitle: convo.get("blrtName"),
                            thumbnail: item.get("thumbnailFile") ? item.get("thumbnailFile").url() : convo.get("thumbnailFile").url(),
                            duration: _.durationify(duration),

                            deeplink: constants.appProtocol + "://" + req.host + req.url,
                            thisUrl: req.protocol + '://' + req.get('host') + req.originalUrl,

                            _: _,
                            constants: constants
                        });

                        return res.render("fbnotification", pageArgs);
                    }, function (error) {
                        loggler.global.log("ALERT: error with convItem query for wrongPlatform page, convo ID: " + id + " and error:", error);
                        return unsuccessful();
                    });
            });
    });


    app.all("/processConvoRequest", function (req, res, next) {
        function resMsg(str) {
            return res.render('message', {
                message: str
            });
        }

        if (req.query == null)
            return resMsg("Invalid link");
        if (req.query.token == null)
            return resMsg("Invalid link");
        if (req.query.c == null)
            return resMsg("Invalid link");
        if (req.query.e == null)
            return resMsg("Invalid link");
        if (req.query.accept == null)
            return resMsg("Invalid link");

        //prepare the params
        var params = {};
        params.data = {
            conversationId: req.query.c,
            eventId: req.query.e,
            accept: req.query.accept == 1 ? true : false,
            token: req.query.token
        };
        params.api = {version: 1, endpoint: "conversationProcessRequest"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };
        Parse.Cloud.useMasterKey();

        //use userBlock api
        return Parse.Cloud.run("conversationProcessRequest", params, {
            success: function (response) {
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    resMsg("Invalid link");
                }
                if (response.success)
                    resMsg(response.message);
                else {
                    if (response.error.code == 613) {
                        return resMsg("You have already taken action on this request.");
                    } else if (response.error.code == 450) {
                        return resMsg("This conversation has reached the user limit. You cannot grant access to more users.");
                    } else {
                        return resMsg("Invalid link");
                    }
                }
            },
            error: function (error) {
                resMsg("Invalid link");
            }
        });
    });


};