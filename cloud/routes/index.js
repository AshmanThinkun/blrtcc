var constants = require('cloud/constants.js');
var util = require('cloud/util.js');
var permissions = require("cloud/permissions.js");
var atob = require('cloud/atob.js');
var btoa = require('cloud/btoa.js');
var loggler = require("cloud/loggler");
var _ = require("underscore");


//todo what these ?????
// var parseExpressHttpsRedirect = require('parse-express-https-redirect');
// app.use(parseExpressHttpsRedirect());

var useragent = require('express-useragent');    // after var app = express();


module.exports=function (app) {

    // Global app configuration section
    app.set('views', 'cloud/views');  // Specify the folder to find templates
    app.set('view engine', 'ejs');    // Set the template engine

    app.use(useragent.express());   // before app.all
// //Login and request.blrt requirements
// to make parseExpressCookieSession
    app.use(require('body-parser').urlencoded({extended: false,limit: '50mb'}));    // Middleware for reading request body
    app.use(require('cookie-parser')('fdsfewfsx'));
    // app.use(parseExpressCookieSession({cookie: {maxAge: 3600000}}));





    require("cloud/jobs/postOCListItemEServer");
//inclue all admin routes
    require("cloud/routes/admin.js")(app); //special way of using routes, because parse is using a old version of node, and doesn't not support app.use to passing routes

//all blrt, blrt convo routes go here
    require("cloud/routes/blrt.js")(app);

//all request routes go here
    require("cloud/routes/request.js")(app);

//all user account management routes go here
    require("cloud/routes/user.js")(app);
    require("cloud/routes/ocList.js")(app);
    require("cloud/routes/webhook")(app)


    app.all('/appstore(/:arg)?', function (req, res, next) {

        var pageArgs = {
            blrt_url: (constants.appProtocol + "://" + req.host + req.url),
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            http_user_agent: req.headers['user-agent']
        };

        if (req.useragent.isAndroid)
            pageArgs.app_store_url = constants.googleStoreUrl;
        else if (!(req.useragent.isiPad || req.useragent.isiPhone || req.useragent.isiPod))
            pageArgs.app_store_url = constants.blrtWebsiteUrl;

        res.render('itunes_redirect', pageArgs);
    });

    app.all('/s(/:arg)?', function (req, res, next) {
        var pageArgs = {
            blrt_url: req.useragent.isAndroid
                ? _.androidLink(constants.appProtocol + "://" + req.host + req.url)
                : (constants.appProtocol + "://" + req.host + req.url),
            originalUrl: constants.appProtocol + "://" + req.host + req.url,
            isAndroid: req.useragent.isAndroid,
            isDesktop: (req.useragent.isiPad || req.useragent.isiPhone || req.useragent.isiPod || req.useragent.isAndroid) ? false : true,
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            google_play_url: constants.googleStoreUrl,
            blrt_website_url: constants.blrtWebsiteUrl,
            constants: constants,
            fb_app_id: constants.fbAppId,
            page_url: req.protocol + '://' + req.get('host') + req.originalUrl,
            http_user_agent: req.headers['user-agent']
        };

        res.render('share', pageArgs);
    });


//mandril unsubscribe
    app.all('/us', function (req, res, next) {
        var pageArgs = {
            http_user_agent: req.headers['user-agent']
        };

        if (req.query.key && req.query.s && req.query.em && util.irreversibleHash(req.query.em + req.query.s) == req.query.key) {
            var email = req.query.em;
            var salt = parseInt(Math.random() * 10000);
            pageArgs["resubscribe_link"] = constants.protocol + req.host + "/rs?em=" + encodeURIComponent(email) + "&s=" + salt
                + "&key=" + encodeURIComponent(util.irreversibleHash(salt + email));

            Parse.Cloud.httpRequest({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                url: 'https://mandrillapp.com/api/1.0/rejects/add.json',
                body: {
                    key: constants.mandrillKey,
                    email: req.query.em
                }
            });
        }


        res.render('unsubscribe', pageArgs);
    });

//mandril resubscribe
    app.all('/rs', function (req, res, next) {
        var pageArgs = {
            http_user_agent: req.headers['user-agent']
        };

        if (req.query.key && req.query.s && req.query.em && util.irreversibleHash(req.query.s + req.query.em) == req.query.key) {
            Parse.Cloud.httpRequest({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                url: 'https://mandrillapp.com/api/1.0/rejects/delete.json',
                body: {
                    key: constants.mandrillKey,
                    email: req.query.em
                }
            });
        }


        res.render('resubscribe', pageArgs);
    });


    app.all('/terms(/:args)?', function (req, res, next) {
        res.render('terms');
    });

    app.all('/privacy(/:args)?', function (req, res, next) {
        res.render('privacy');
    });

    app.all("/urlInfo", function (req, res, next) {
        function resMsg(str) {
            return res.jsonp({
                success: false,
                message: str
            });
        }

        //prepare the params
        var params = {};
        params.data = {
            url: req.query.url,
            includeMedia: req.query.media ? true : false
        };
        params.api = {version: 1, endpoint: "urlInfo"};
        params.device = {
            version: "1.0",
            device: "www",
            os: "www",
            osVersion: "1.0"
        };

        return Parse.Cloud.run("urlInfo", params, {
            success: function (response) {
                try {
                    response = JSON.parse(response);
                }
                catch (e) {
                    resMsg("Something went wrong, please try again later");
                }
                res.jsonp(response);
            },
            error: function (error) {
                resMsg("Something went wrong, please try again later");
            }
        });
    });


    app.all('/pioneers', function (req, res) {
        var params = req.query;
        if (params.id && params.id != "") {
            res.render('invite', {constants: constants});
        } else {
            res.render('pioneer', {
                constants: constants
            });
        }
    });

//referer program
    app.get('/refererBonus', function (req, res, next) {
        if (req.query.key != "gudfiahfglkiuYgfdhiudf")
            return res.redirect("/");

        res.render("refererProgram", {
            constants: constants,
            token: req.query.token
        });
    });


    app.all("/intro.json", function (req, res, next) {
        var id = "";
        if (constants.isDevelop) {
            id = "VY59kENJZL"
        } else if (constants.isQA) {
            id = "k3nHt8cVbg"
        } else {
            id = "k7zJ68mEPm"
        }


        var resObj = {
            intro: [
                {
                    page: 0,
                    blrt: id
                },
                {
                    page: 4,
                    blrt: id
                },
                {
                    page: -1,
                    blrt: id
                }
            ]
        };
        return res.jsonp(resObj);
    });

    app.post('/internal/crashreport', function (req, res, next) {
        if (!req.body || req.body.crashReportKey != "453wsgJLKfdGUYioudfFGDgfGFG564hui8idsfg") {
            res.send(false);
        }
        loggler.global.log(false, "receive crash report: ", req.body).despatch();
        console.error("receive crash report: " + new Date().toString());
        res.send(true);
    });

    app.get('/oas/login', function (req, res, next) {
        return res.render('login');
    });


    app.post('/oas/login', function (req, res, next) {
        console.log("post")
        //parse-express-cookie-session middleware can set a session storage in browser
        Parse.User.logIn(req.body.email.toLowerCase(), req.body.password).then(function (user) {
                console.log("user.sessionToken " + user.id)
                // handle the parse-express sessioToken ourself
                res.cookie("parse.sess", user.id, {signed: true});
                var redirectUrl = req.query.redirect
                console.log(redirectUrl)
                if (redirectUrl) {
                    res.redirect(redirectUrl)
                } else {
                    res.render("message", {message: "Login successfully"});
                }
            },
            function (error) {
                // Login failed
                res.render("message", {message: "Invalid username/password pair!"});
            });
    });


    app.all('/oas/logout', function (req, res, next) {
        res.clearCookie("parse.sess");
        res.render("message", {message: "Logout successfully"});
    });


    var constants = require('cloud/constants.js');
    app.all(/^\/(?!parse|dashboard).*$/, function (req, res, next) {
        console.log("call me ??")
        console.log(req.url)
        var pageArgs = {
            blrt_url: req.useragent.isAndroid
                ? _.androidLink(constants.appProtocol + "://" + req.host + req.url)
                : (constants.appProtocol + "://" + req.hostname + req.url),
            originalUrl: constants.appProtocol + "://" + req.hostname + req.url,
            isAndroid: req.useragent.isAndroid,
            app_store_url: constants.appStoreUrl,
            app_store_id: constants.appStoreId,
            http_user_agent: req.headers['user-agent'],
            blrt_website_url: constants.blrtWebsiteUrl,
            constants: constants
        };

        if (req.useragent.isiPad || req.useragent.isiPhone || req.useragent.isiPod || req.useragent.isAndroid)
            res.render('iOS_redirect', pageArgs);
        else if (req.useragent.isMobile)
            res.render('mobile', pageArgs);
        else
            res.render('desktop', pageArgs);
    });




}

