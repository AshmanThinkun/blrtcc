var _ = require("underscore");
var constants = require("cloud/constants");
var loggler = require("cloud/loggler");
var trackImmediate = require('cloud/analytics-node').trackImmediate;

// # NonUser

// This class helps us abstract from how NonUsers are represented in Parse
// and the Mixpanel. You should be able to use this instead of working with
// queries or direct saves on any NonUser object.

// All you should do is use the static .get, use Mixpanel's track
// with this object's ID as the distinct_id wherever you need to, add some
// actions to this under special circumstances and eventually associate it
// with a user using the static method so it can catch NonUsers belonging
// to all of that user's contact details.
var NonUser = (function () {
    var typeValueSeparator = ":";

    // ## Constructor
    function NonUser(nonUserParseObject) {
        this.parseObject = nonUserParseObject;
        this.id = nonUserParseObject.id;
    }

    NonUser.prototype.id = null;
    //[Guichi] globally cache
    //[Guichi] id -> promise(nonUserObj)
    var nonUserCache = {};

    // ## Static methods

    // ### Get

    // The details arg can either be a single object, or an array of:
    //     {
    //         type: "email",
    //         value: "bilal@thinkun.net",
    //         source: "WebSignupStart",
    //         referredBy: existUserId
    //     }
    // or UserContactDetails objects.

    // The function call will return a promise who's final value will
    // depend on whether an array was specified for details or not.

    // A single detail invocation will provide a single NonUser object.
    // An array will return an array of NonUsers. Logical.

    // NonUser objects will be created if it didn't exist beforehand.

    // The supported options are:
    //     updateLastSeen = true;
    //     dontCreate = false;

    // If dontCreate is true then this function won't create a new
    // NonUser when an existing one isn't found. If updateLastSeen is
    // false then Mixpanel tracking won't update the last seen date/time
    // for the NonUser.
    NonUser.get = function (details, options) {
        console.log('\n\n\narguments initial==='+JSON.stringify(arguments));
        Parse.Cloud.useMasterKey();

        options = _.extend({
            updateLastSeen: false,
            dontCreate: false
        }, options || {});

        // We'll use the same logic regardless of whether the user provided
        // an array as part of their request or not. We'll just check this
        // boolean before we return the result.
        var multiple = _.isArray(details);
        if (!multiple)
            details = [details];

        if (multiple && _.isEmpty(details))
            return Parse.Promise.as([]);
        // We'll organise the requested details by their IDs (which is a
        // combination of their type and value (plus a separator)) so we
        // can easily check if they've already been requested or have an
        // associate pending.
        details = _.reduce(details, function (memo, detail) {
            var type;
            var value;
            var source;
            var referredById;

            // Is the detail a ParseObject or simple detail object? Keep in
            // mind that you can provide this function UserContactDetails.
            //todo [guichi] what is "source" in UserContactDetails  ???
            //todo [guichi] seems typo or out of date
            if (typeof detail.get == "function") {
                type = detail.get("type");
                value = detail.get("value");
                source = detail.get("source");
                referredById = detail.get("referredBy") ? detail.get("referredBy").id : undefined;
            } else {
                type = detail.type;
                value = detail.value;
                source = detail.source;
                referredById = detail.referredBy;
            }
            if (!source) {
                source = "ConversationSent";
            }

            var id = type + typeValueSeparator + value;

            memo[id] = {
                id: id,
                type: type,
                value: value,
                source: source,
                referredBy: referredById
            };
            return memo;
        }, {});

        console.log("\n\n\n===details details details before===");
        console.log(JSON.stringify(details));
        // We're using a recursive function that's defined below to handle
        // promise generation as there may be complications when dealing
        // with pending associations. After an association, we need to
        // check all the promises again - hence the recursion.
        generatePromises(details, options);

        console.log("\n\n\n===details details details after===");
        console.log(JSON.stringify(details));

        // That's where most of the magic happens :P Life should be easy
        // from here on out.
        return Parse.Promise.when(
            _.pluck(_.values(details), "promise")
        ).then(function (result) {
            // console.log('\n\nresult==='+JSON.stringify(result));
            // console.log('\n\n\narguments before==='+JSON.stringify(arguments));
            //[Guichi] Parse has change their API internally,keep backward compatibility
            if (arguments[0] instanceof Array) {
                arguments = _.extend({}, arguments[0])
            }

            if(multiple) {
                // console.log('\n\n\n===arguments multiple==='+JSON.stringify(arguments));
                return arguments;
            }else {
                // console.log('\n\n\n===arguments==='+JSON.stringify(arguments[0]));
                return arguments[0];
            }
        });
    };

    // This is the recursive promise generating function used earlier.
    function generatePromises(details, options) {

        // TODO: Implement and test pendingAssociation (framework mostly done).
        // var pendingAssociation = {};


        //[Guichi] to be processed

        var nothingPending = {};

        console.log('\n\n\nnonUserCache==='+JSON.stringify(nonUserCache));

        _.each(details, function (detail) {
            console.log('\n\n\n===detail.id==='+detail.id);
            if (_.has(nonUserCache, detail.id)) {
                // We've already fetched, or are already fetching this
                // NonUser (even if it's a query waiting for an associate
                // to complete - still covered here).

                // We can simply use the existing promise that's already
                // set up for extracting and returning the appropriate
                // NonUser.
                console.log('\n\n===has nonUserCache detail.id===');
                detail.promise = nonUserCache[detail.id];
                /* I don't have enough time to implement and test this.
                 } else if(_.has(associatePromises, detail.id)) {
                 // NonUser has an associate already pending. We need to
                 // wait for this to complete and run our query.
                 pendingAssociation[detail.id] = detail;
                 */
            } else {
                // It's the first request for this NonUser!
                console.log('\n\n===nothingPending===');
                nothingPending[detail.id] = detail;
            }

            if (!detail.promise) {
                detail.promise = new Parse.Promise();
                nonUserCache[detail.id] = detail.promise;
                // console.log('\n\n\nnonUserCache==='+JSON.stringify(nonUserCache));
            }

            // We'll hook up the blank promises soon!
        });

        // if(!_.isEmpty(pendingAssociation)) {
        //     // Simply recurse after associations are complete!
        //     Parse.Promise.when(_.map(pendingAssociation, function (detail) {
        //         return associatePromises[detail.id];
        //     })).then(function () {
        //         generatePromises(pendingAssociation, options);
        //     });
        // }
        
        console.log('\n\n\n===nothingPending==='+JSON.stringify(nothingPending));

        //[Guichi] terminate the promise
        if (_.isEmpty(nothingPending))
            return;


        // Group the NonUsers into their types so we can bring together an
        // efficient query: .containedIn for value with .or for type.
        var types = _.reduce(nothingPending, function (memo, detail) {
            if (!(detail.type in memo))
                memo[detail.type] = [];

            memo[detail.type].push(detail.value);
            return memo;
        }, {});

        // Build the query by splitting up those types.
        var query = null;

        console.log('\n\n\n===types==='+JSON.stringify(types));

        _.each(types, function (values, type) {
            var subQuery = new Parse.Query("NonUser")
                .equalTo("type", type)
                .containedIn("value", values)
                .notEqualTo("claimed", true);

            if (query == null) query = subQuery;
            else query = Parse.Query.or(query, subQuery);
        });

        var promise = new Parse.Promise();

        query.find().then(function (nonUsers) {
            nonUsers = _.map(nonUsers, function (nonUser) {
                return new NonUser(nonUser);
            });

            console.log('\n\n\nnonUsers==='+JSON.stringify(nonUsers));

            // Resolve all of the promises for any NonUser objects we've
            // found existing, and prepare their IDs so we can filter with
            // them later.
            var foundNonUserIds = _.map(nonUsers, function (nonUser) {
                var id = nonUser.parseObject.get("type") + typeValueSeparator + nonUser.parseObject.get("value");

                nonUserCache[id].resolve(nonUser);

                return id;
            });

            console.log('\n\n\n===foundNonUserIds==='+JSON.stringify(foundNonUserIds));
            var missingNonUsers = _.omit(nothingPending, foundNonUserIds);

            // console.log('\n\n\n===missingNonUsers result===
            // If we were requested not to create new objects, then we're
            // done. We need to resolve the promises so it returns, and
            // then clear the cache so, if requested again, the requestee
            // can proceed to check if creation is necessary.
            if (options.dontCreate) {
                _.each(missingNonUsers, function (detail) {
                    nonUserCache[detail.id].resolve();
                    delete nonUserCache[detail.id];
                });

                return;
            }


            var nonUsersToCreate = _.map(missingNonUsers, function (detail) {
                var nonUserCreate = new Parse.Object("NonUser")
                    .set("type", detail.type)
                    .set("value", detail.value)
                    .set("source", detail.source);
                if (detail.referredBy) {
                    var referral = new Parse.User();
                    referral.id = detail.referredBy;
                    nonUserCreate.set("referredBy", referral);
                }

                return nonUserCreate;
            });

            console.log('\n\nnonUsersToCreate is empty==='+_.isEmpty(nonUsersToCreate));

            if (_.isEmpty(nonUsersToCreate))
                return;

            Parse.Object.saveAll(nonUsersToCreate,{useMasterKey:true}).then(function (savedObjects) {

                var createdNonUsers = _.map(savedObjects, function (nonUser) {
                    return new NonUser(nonUser);
                });
                var now = new Date();
                _.each(createdNonUsers, function (nonUser) {
                    var nonUserObj = nonUser.parseObject;

                    if (nonUserObj.get("value")) {
                        console.log("trackImmediatetrackImmediatetrackImmediate")
                        console.log("nonuser-" + nonUser.id)
                        trackImmediate.identify({
                            userId: "nonuser-" + nonUser.id,
                            traits: {
                                createdAt: now,
                                name: "NonUser: " + nonUserObj.get("value"),
                                email: nonUserObj.get("type") == "email" ? nonUserObj.get("value") : null,
                                "nonuserCreatedAt": now,
                                "facebookId": nonUserObj.get("type") == "facebook" ? nonUserObj.get("value") : null,
                                "usesFacebook": Boolean(nonUserObj.get("type") == "facebook"),
                                "isNonUser": true,
                                "nonUserType": nonUserObj.get("type"),
                                "nonUserValue": nonUserObj.get("value"),
                                "claimed": false,
                                "source": nonUserObj.get("source"),
                            }
                        })

                    } else {
                        //todo [Guichi] not sure when this could happen
                        console.error("nonUserObj")
                        console.error(JSON.stringify(nonUserObj))
                        console.error("nothingPending")
                        console.error(JSON.stringify(nothingPending))
                        console.error("foundNonUserIds")
                        console.error(JSON.stringify(foundNonUserIds))
                    }

                });
                _.each(createdNonUsers, function (nonUser) {
                    var id = nonUser.parseObject.get("type") + typeValueSeparator + nonUser.parseObject.get("value");
                    nonUserCache[id].resolve(nonUser);
                });
            }, function (promiseError) {
                loggler.global.log(false, "ALERT: Error creating new NonUser(s):", promiseError).despatch();
                promise.reject(promiseError);
            });
        }, function (promiseError) {
            loggler.global.log(false, "ALERT: Error searching for NonUser(s):", promiseError).despatch();
            promise.reject(promiseError);
        });
    }

    // ### Associate

    // The static version of associate takes a single ParseUser and tries
    // to find a NonUser object to associate them with. It does this in
    // two phases:

    // a) Query all of the user's ContactDetails; and
    // b) Query all NonUsers based on the found ContactDetails.

    // For all that are found, our NonUser object will be created and have
    // the instanced associate method invoked upon it with the same user.

    // A boolean representing whether this association was made with a new
    // or existing user should be provided as the second argument.
    NonUser.associate = function (user, isNew) {
        isNew = Boolean(isNew);
        var promise = new Parse.Promise();

        new Parse.Query("UserContactDetails")
            .equalTo("user", user)
            // .notEqualTo("placeholder", true) //todo secondary will not work with this
            .find({useMasterKey:true})
            .then(function (userContactDetails) {
                console.log('\n\n\nuserContactDetails===');
                console.log(userContactDetails);
                if (userContactDetails.length == 0) {
                    loggler.global.log(false, "ALERT: No UserContactDetails found for user in static:NonUser.associate for user:" + user.id).despatch();
                    promise.reject("No UserContactDetails found for user in static:NonUser.associate for user:" + user.id);
                    return;
                }

                console.log('\n\n\n===user signup===');

                NonUser.get(userContactDetails, {
                    dontCreate: true,
                    updateLastSeen: true
                }).then(function (nonUserObjects) {
                    // console.log('\n\n\nnonUserObjects before==='+JSON.stringify(nonUserObjects));
                    var underScoreParse = require('cloud/underscoreParse')
                    nonUserObjects = underScoreParse.compact(underScoreParse.flatten(nonUserObjects));
                    // console.log('\n\n\nnonUserObjects after==='+JSON.stringify(nonUserObjects));

                    if (_.isEmpty(nonUserObjects)) {
                        promise.resolve();
                        return;
                    }

                    var now = new Date();

                    var saveUser = false;
                    var saveAll = _.chain(nonUserObjects)
                        .map(function (obj) {
                            // console.log('\n\n\nnonUserObjects obj==='+JSON.stringify(obj));
                            obj.parseObject
                                .set("claimed", true)
                                .set("user", user)
                                .set("claimedAt", now);

                            if (obj.parseObject.get("referredBy") && !user.get("referredBy")) {
                                user.set("referredBy", obj.parseObject.get("referredBy"));
                                saveUser = true;
                            }

                            var timelyActions = [];
                            if (obj.parseObject.get("actions")) {
                                _.each(obj.parseObject.get("actions"), function (action) {
                                    if ((action.newOnly && !isNew) || (action.existingOnly && isNew)) return;

                                    timelyActions.push(new Parse.Object("TimelyAction")
                                        .set("category", "userLogin")
                                        .set("type", action.type)
                                        .set("user", user)
                                        .set("executionTime", now)
                                        .set("data", action.data));
                                });
                            }

                            var objectsToSave = _.union([obj.parseObject], timelyActions);

                            if (timelyActions.length) {
                                user.increment("userLoginActions", timelyActions.length);
                                objectsToSave.push(user);
                            }

                            return objectsToSave;
                        })
                        .flatten()
                        .value();
                    if (saveUser) {
                        saveAll.push(user);
                    }

                    Parse.Object.saveAll(saveAll,{useMasterKey:true})
                        .then(function (associatedNonUserObjects) {
                            console.log('\n\n\nassociatedNonUserObjects 1===');
                            console.log(associatedNonUserObjects);
                            var waitPromise = new Parse.Promise();

                            //do mixpanel alias
                            var candidate = _.find(associatedNonUserObjects, function (obj) {
                                console.log('\n\n\n===obj==='+JSON.stringify(obj));
                                return obj.className == "NonUser";
                            });

                            if (candidate && isNew) {
                                console.log('\n\n\n===alias user.id==='+user.id);
                                console.log('===previous.id==='+candidate.id);
                                // var mp = new Mixpanel({batchMode: false});
                                trackImmediate.alias({
                                    previousId: "nonuser-"+candidate.id, // nonuser
                                    userId: user.id // user
                                }).then(function () {
                                    waitPromise.resolve(associatedNonUserObjects);
                                }, function (error) {
                                    waitPromise.reject(error);
                                });
                            } else {
                                waitPromise.resolve(associatedNonUserObjects);
                            }


                            return waitPromise;
                        }).then(function (associatedNonUserObjects) {
                            console.log('\n\n\nassociatedNonUserObjects 2===');
                            console.log(associatedNonUserObjects);

                            var length = associatedNonUserObjects.length;
                            var obj = associatedNonUserObjects[0]; //NonUser objectId

                        //_.each(associatedNonUserObjects, function (obj) {
                            if (obj.className !== "NonUser" && obj.className !== '_User') {
                                return;
                            }

                            console.log("\n\n\ntrack nonUser obj id==="+obj.id);
                            console.log("track nonUser user id==="+user.id);

                            // associate mixpanel non-user profile with this user
                            var traits = { // user property 
                                    "claimed": true,
                                    "claimedBy": user.id,
                                    "claimedAt": obj.get("claimedAt"),
                                    "claimedViaSignup": isNew,
                                    "id": user.id,
                                };

                            if (!isNew) {
                                traits.name = "Claimed by: "+user.id;
                            }

                            // even implement track alias, utill next time login, 
                            // the userId is still 'nonuser-nonuserId', 
                            // and after alias before tracking, should do identify 
                            trackImmediate.identify({ //identify nonuser 
                                userId: "nonuser-"+obj.id,
                                traits: traits
                            })


                            // send events to the non-user profile for this user
                            trackImmediate.track({
                                userId: isNew ? "nonuser-"+obj.id : user.id, //
                                event: 'NonUser Claimed',
                                properties: {
                                    "NonUser ID": isNew ? "nonuser-"+obj.id : user.id,
                                    "Via Signup": isNew,
                                }
                            });

                            // send claimed event to the non-user profile for this user
                            trackImmediate.track({
                                userId: "nonuser-"+obj.id,
                                event: 'Claimed By User',
                            });

                            //t++;

                       // });

                        promise.resolve(associatedNonUserObjects)
                    }, function (promiseError) {
                        loggler.global.log(false, "ALERT: Error saving all user associations in static:NonUser.associate:", promiseError).despatch();
                        promise.reject(promiseError);
                    });
                }, function (promiseError) {
                    loggler.global.log(false, "ALERT: Error getting NonUser objects:", promiseError).despatch();
                    promise.reject(promiseError);
                })
            }, function (promiseError) {
                loggler.global.log(false, "ALERT: Error searching for UserContactDetails in static:NonUser.associate:", promiseError).despatch();
                promise.reject(promiseError);
            });

        return promise;
    };

    // ## Instance methods

    // ### Add action

    // You can add actions to a NonUser if it hasn't been claimed yet.
    // Those actions will be actioned upon when a user signs up with, or
    // an existing user claims this NonUser.

    // The action must have a slug that maps to the type of TimelyAction
    // that will be created. That TimelyAction will be created with
    // "userLogin" as it's category (so it can be actioned upon when the
    // user first logs in).

    // Arbitrary data can be attached to that TimelyAction to support your
    // custom functionality. Additionally, an options object can be
    // provided that supports the following options:
    //     {
    //         newOnly: Boolean,
    //         existingOnly: Boolean
    //     }

    // The newOnly and existingOnly options determine whether the action in
    // subject should be triggered only if the claiming user is a new or
    // existing user (i.e. if this NonUser was associated due to a user
    // signup or secondary email verification).
    NonUser.prototype.addAction = function (slug, data, options) {
        Parse.Cloud.useMasterKey();
        options = options || {};
        return this.parseObject.add("actions", {
            type: slug,
            category: "userLogin",
            newOnly: options.newOnly || false,
            existingOnly: options.existingOnly || false,
            data: data
        }).save();
    };

    NonUser.updateNoneUser = function updateNoneUser (contactValue, contactId) {
        return new Parse.Promise (function (resolve, reject) {
            // update non-user
            // associate mixpanel non-user profile with this user

            new Parse.Query("NonUser")
                .equalTo("value", contactValue)
                .find()
                .then (function (nonUser) {
                    if (nonUser!= null && nonUser.length > 0) {
                        // update non-user user properties
                        trackImmediate.identify({
                            userId: "nonuser-" + nonUser[0].id,
                            traits: {
                                "claimed": false,
                                "claimedBy": "unset",
                                "claimedAt": "unset",
                                "claimedViaSignup": "unset",
                                "name": "NonUser: "+contactValue
                            }
                        })

                        // send unclaimed event to the non-user profile
                        trackImmediate.track({
                            userId: "nonuser-" + nonUser[0].id,
                            event: 'Unclaimed By User',
                            properties: {
                                "User ID": contactId
                            }
                        });

                        // send unclaimed event to the user profile
                        trackImmediate.track({
                            userId: contactId,
                            event: 'NonUser Unclaimed',
                            properties: {
                                "NonUser ID": "nonuser-" + nonUser[0].id
                            }
                        });
                    }
                    resolve ();

                }, function (error) {
                    reject (error)
                }
            )
        });
    }

    return NonUser;
})();

module.exports = NonUser;
