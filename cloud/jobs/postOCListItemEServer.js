/**
 * Created by test on 30/10/15.
 */
var api = require("cloud/api/util");
var _ = require("underscore");
var constants=require("cloud/constants")

Parse.Cloud.job("postOCListItems", function (req, res) {
    Parse.Cloud.useMasterKey();
    var toSend = [];
    var toSave;
    new Parse.Query("OCListItem")
        .notEqualTo("sentToEServer", true)
        .find()
        .then(
        function (items) {
            toSave = items;
            items.forEach(
                function (item) {
                    toSend.push({
                        blrtId: _.encryptId(item.get("blrt").id),
                        ocListId:_.encryptId(item.get("oclist").id),
                        ocListItemId:_.encryptId(item.id),
                        createdAt: item.createdAt,
                        index:item.get("index")
                    })
                }
            )
            console.log("send request to endpoint : " + JSON.stringify(toSend))
            return Parse.Cloud.httpRequest(
                {
                    method: "POST",
                    url: constants.embedServer+"/ocList/cache",
                    body: {
                        data: JSON.stringify(toSend),
                        apiKey: "abcd"
                    }
                })
                .then(function (httpResponse) {
                    var responseText=httpResponse.text;
                    console.log("Response from the E-Server: " +responseText )
                    if(responseText=="SUCCESS"){
                    var toSavePromise = []
                    toSave.forEach(function (item) {
                        item.set("sentToEServer", true);
                        toSavePromise.push(item.save())
                    })
                    return Parse.Promise.when(toSavePromise).then(function(){
                        return toSavePromise.length;
                    })}
                    else{
                        console.log("The server response,but not a valid one")
                        return Parse.Promise.error("");
                    }
                },
                function (err) {
                    console.log("requesting sending failed")
                })

        },
        function(err){

            console.log("fail to query OCListItem table in parse server"+err)
            res.error("fail to query OCListItem table in parse server"+err)
        }
    )

        .then(function (count) {
            res.success("Sent data to E-Server successfully, processed: " + count)
        }, function () {
            res.error("fail to communicate E-Server")
        })


},"*/5 * * * * *")




















