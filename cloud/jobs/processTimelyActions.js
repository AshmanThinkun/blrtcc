var timelyActionsJS = require("cloud/timelyActions");

var constants = require("cloud/constants");
var loggler = require("cloud/loggler");
var _ = require("underscore");

Parse.Cloud.job("processTimelyActions", function (req, res) {
    var now = new Date();
    var query = new Parse.Query("TimelyAction")
        .lessThan("executionTime", now);

    return timelyActionsJS.processAction(query)
        .then(function (result) {
            if (!result.success)
                res.error(result.message);
            else {
                res.success(result.message);
            }
        });

},"*/5 * * * * *");