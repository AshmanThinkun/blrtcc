var api = require("cloud/api/util");

var create = require("cloud/api/conversationUserRelation/create");
var link = require("cloud/api/conversationUserRelation/link");
var publicInfo = require("cloud/api/conversationUserRelation/publicInfo");
var mute = require("cloud/api/conversationUserRelation/mute");
var query = require("cloud/api/conversationUserRelation/query");
var search = require("cloud/api/conversationUserRelation/search");
var webappquery = require("cloud/api/conversationUserRelation/webappquery");

var _delete = require("cloud/api/conversationUserRelation/delete");

var define = require('../../lib/cloud').define()
Parse.Cloud.define = define
// # ConversationUserRelation ## Create
Parse
    .Cloud
    .define("conversationUserRelationCreate", function (req, res) {
        console.log('\n\nconversationUserRelationCreate req===');
        console.log(req);
        req.start(); //start tracking
        var supported = api.supports(req, "conversationUserRelationCreate");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        conversationId: {
                            type: api.varTypes.text, // why does conversationId use text type?
                            // type: api.varTypes.id,
                            required: true
                        },
                        conversationName: {
                            type: api.varTypes.text
                        },
                        note: {
                            type: api.varTypes.text
                        },
                        dontAddNoteInConvo: {
                            type: api.varTypes.bool
                        },
                        relations: {
                            type: api.varTypes.array,
                            required: {
                                presence: true,
                                min: 1
                            },
                            subtype: {
                                type: {
                                    type: api.varTypes.contactType,
                                    required: true
                                },
                                value: {
                                    type: api.varTypes.text
                                },
                                    name: {
                                        type: api.varTypes.text
                                    }
                                }
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            console.log('\n\nprocessed.req===');
                            console.log(processed.req);
                            return create[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// console.log(Parse.Cloud.define.toString())
Parse
    .Cloud
    .define("conversationUserRelationQuery", function (req, res) {
        req.start()
        var supported = api.supports(req, "conversationUserRelationQuery");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        page: {
                            type: api.varTypes.int,
                            required: true
                        },
                        //inbox,archive
                        type: {
                            type: api.varTypes.text,
                            required: true
                        },
                        convoId: {
                            type: api.varTypes.text,
                            required: false
                        },

                        ctags: {
                            type: api.varTypes.text,
                            required: false
                        },
                    }
                })
                    .then(function (processed) {
                        if (processed.success) {
                            return query[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// console.log(Parse.Cloud.define.toString())
Parse
    .Cloud
    .define("conversationWebUserRelationQuery", function (req, res) {
        req.start()
        var supported = api.supports(req, "conversationWebUserRelationQuery");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        type: {
                            type: api.varTypes.text,
                            required: true
                        },

                        convoId: {
                            type: api.varTypes.id,
                            required: false
                        },

                        ctags: {
                            type: api.varTypes.text,
                            required: false
                        },
                        
                        direction: {
                            type: api.varTypes.text,
                            required: false
                        },
                    }
                })
                    .then(function (processed) {
                        if (processed.success) {
                            return webappquery[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

Parse.Cloud.define("conversationUserRelationSearch", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationUserRelationSearch");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    type: {
                        type: api.varTypes.text,
                        required: true,
                    },
                    input: {
                        type: api.varTypes.text,
                        required: false,
                    },
                    bottomConvoId: {
                        type: api.varTypes.text,
                        required: false,
                    },
                }
            }).then(function (processed) {
                if (processed.success) {
                    return search[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api
                            .loggler
                            .log(false, "ALERT: Big error, caught outside function:", error)
                            .despatch();
                        return res.error({success: false, error: api.error.subqueryError});
                    });
                } else 
                    return res.success(JSON.stringify(processed));
                }
            , function (error) {
                return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
            });
        }
    };

    return v[supported.version]();

});

// ## Link A relation existing for a particular NonUser may need to be claimed
// by a different user. That's done explicitly by using this link endpoint
// providing the ID of a NonUser relation while authorised as a user.
Parse
    .Cloud
    .define("conversationUserRelationLink", function (req, res) {
        req.start(); //start tracking
        var supported = api.supports(req, "conversationUserRelationLink");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api
                    .process(req, {
                        auth: true,
                        params: {
                            relationId: {
                                type: api.varTypes.text,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return link[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## Public info This function will return information safe for a client to
// request without ownership of the requested ConversationUserRelation (i.e.
// after following a link from an email, even if not logged in or logged into
// the wrong account).
Parse
    .Cloud
    .define("conversationUserRelationPublicInfo", function (req, res) {
        req.start(); //start tracking
        var supported = api.supports(req, "conversationUserRelationPublicInfo");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api
                    .process(req, {
                        params: {
                            relationId: {
                                type: api.varTypes.text,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return publicInfo[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## Mute The function will mute the conversation for the user Mute options
// include push, email notification, and all
Parse
    .Cloud
    .define("conversationUserRelationMute", function (req, res) {
        res.start();
        var supported = api.supports(req, "conversationUserRelationMute");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        relationId: {
                            type: api.varTypes.text,
                            required: true
                        },
                            muteFlag: {
                                type: api.varTypes.int,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return mute[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

var setConversationUserRelation = require("cloud/api/conversationUserRelation/set");
Parse
    .Cloud
    .define("conversationUserRelationSet", function (req, res) {
        req.start(); //start tracking
        console.log("conversationUserRelationSet")
        var supported = api.supports(req, "alwaysAvailable");

        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        relationId: {
                            type: api.varTypes.id,
                            required: true
                        },
                        settings: {
                            type: api.varTypes.array,
                            required: true,
                            subtype: {
                                setting: {
                                    type: api.varTypes.text,
                                    required: true
                                },
                                    value: {
                                        type: api.varTypes.bool
                                    }
                                }
                            }
                        }
                    })
                    .then(function (processed) {
                        console.log(processed)
                        if (processed.success) {
                            return setConversationUserRelation[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });
const readAll = require('cloud/api/conversationUserRelation/readAll')
Parse
    .Cloud
    .define("markAllAsRead", function (req, res) {
        console.log("readAll")
        var supported = api.supports(req, "alwaysAvailable");

        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api
                    .process(req, {
                        auth: true,
                        params: {
                            relationId: {
                                type: api.varTypes.id,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return readAll[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// delete is reserved in JS
var _delete = require("cloud/api/conversationUserRelation/delete");
Parse
    .Cloud
    .define("conversationUserRelationDelete", function (req, res) {
        var supported = api.supports(req, "conversationUserRelationDelete");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        relationIds: {
                            type: api.varTypes.array,
                            required: {
                                presence: true,
                                min: 1
                            },
                            subtype: {
                                type: api.varTypes.id,
                                required: true
                            }
                        },
                            muteFlag: {
                                type: api.varTypes.int
                            }
                        }
                    })
                    .then(function (processed) {
                        console.log("===== processed : ")
                        console.log(JSON.stringify(processed))
                        if (processed.success) {
                            return _delete[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });