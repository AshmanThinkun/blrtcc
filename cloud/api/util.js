var constants = require("cloud/constants");
var maintenance = require("cloud/maintenance");

var loggler = require("cloud/loggler");
var util = require("cloud/util");
var _ = require("underscore");

// Just incase someone runs this from outside the API, we need to set the
// loggler here (whereas we'd normally set it based on which endpoint is
// being requested).
exports.loggler = loggler.global;

// # Blrt API utilities

// ## Structures

// Structures are used for simplicity and consistency. These should be
// used wherever possible by API endpoint implementations.

// ### authToken

// Although this isn't created explicitly, it's provided in requests
// and should look like:

//     {
//         id: "...",
//         token: "...",
//         expiration: "...",
//         target: "facebook" || "twitter" || ...
//     }

var supportedAuthTargets = {
    facebook: "facebook"
};

// ### APIError enum

// Subtypes the basic error enum.
var apiError = _.extend(constants.error, {
    unknown: {code: 0, message: "An unknown error occurred."},
    invalidVersion: {code: 300, message: "The API for the provided version did not define the requested function."},
    authenticationRequired: {code: 301, message: "The request has invalid authentication parameters."},
    invalidParameters: {code: 400, message: "The request provided invalid parameters."},
    subqueryError: {code: 500, message: "One or more of the request's subqueries failed to complete."},
    maintenanceMode: {code: 600, message: "The request could not be completed because the server is under maintenance."}
});

exports.error = apiError;

// ## Functions

// These functions are to be used by API endpoints to make their lives
// easier. In some cases, the API endpoint is expected to call the
// functions (i.e. with `supports` and `process`).

// ### Supports

// We need a list of supported API version/endpoint pairs so we can stop
// functions from trying to process impossible things.
var support = {
    1: [

        //avoid all the bullshit
        "alwaysAvailable",
        // api/conversation
        "conversationCreateWithBlrt",               // /createWithBlrt
        "conversationDelete",                       // /delete
        "conversationDuplicateFromBlrt",            // /duplicateFromBlrt
        "conversationRequest",                      // /request
        "conversationPublic",                       // /public
        "conversationRequestAccess",                // /requestAccess
        "conversationProcessRequest",               // /processRequest

        // api/conversationItem
        "conversationItemCreate",                   // /create
        "conversationItemPublic",                   // /public
        "conversationItemFromUrl",                  // /fromUrl
        "conversationItemView",                     // /view
        "fetchAllConversationItem",                 //fetch all conversationItem by conversationId
        "search",                                   //search
        "setRead",
        "conversationItemPublicBlrt",                   // /public

        // api/conversationUserRelation
        "conversationUserRelationCreate",           // /create
        "conversationUserRelationLink",             // /link
        "conversationUserRelationPublicInfo",       // /publicInfo
        "conversationUserRelationMute",             // /mute
        "conversationUserRelationQuery",            // fetch from inbox
        "conversationWebUserRelationQuery",            // fetch from inbox
        "conversationUserRelationSearch",           // search from db
        "conversationUserRelationDelete",           // /delete


        // api/user
        "userRegister",                             // /register
        "userChangePrimary",                        // /changePrimary
        "userBlock",                                // /block
        "userGetTipBanners",                        // /getTipBanners
        "userChangeAccountStatus",                  // /changeAccountStatus

        // api/userContactDetail
        "userContactDetailCreate",                  // /create
        "userContactDetailDelete",                  // /delete
        "userContactDetailResendVerification",      // /resendVerification
        "userContactDetailFindExistance",           // /findExistance

        // api/timelyAction
        "timelyActionRunByCategory",                // /runByCategory
        
        // pendingActions
        "pendingActionsHandled",
        "getPendingActions",

        // api/developer
        "developerApply",                           // /apply
        "developerResetKeys",                       // /resetKeys
        "developerUpdateSettings",                  // /updateSettings

        // api/request
        "requestDetails",                           // /details

        // api/other
        "urlInfo",
        "RequestPhoneVerificationCode"
    ]
};

exports.supports = function (req, targetEndpoint) {
    // First we need to extract the desired version from the request.
    // We do this because we don't want them to have to look through
    // the request every time an endpoint is being defined, we'll do
    // it for them here instead.
    var version;

    if (req == null || req.params == null || req.params.api == null || !_.isNumber((version = req.params.api.version)))
        return {success: false, error: apiError.invalidParameters};

    // Now all we need to do is check that the requested endpoint is in
    // the corrsponding version's array of supported things!
    if (support[version] != null && support[version].indexOf(targetEndpoint) != -1)
        return {success: true, version: version};
    return {success: false, error: apiError.invalidVersion};
};

// ### Process

// The process utility is provided a the raw request that an endpoint
// received and some validation information and performs the task of
// validating the parameters and returning a simplified version of it.

// It will also perform API logging of things like the version and device
// that requested the endpoint.

// First we'll declare what types of variables we'll be able to validate
// as an enum for the endpoints to follow.
var varTypes = {
    email: "email",
    password: "password",
    text: "text",
    bool: "bool",
    dictionary: "dictionary",
    object: "object",
    contactType: "contactType",
    ambiguous: "ambiguous",
    authToken: "authToken",
    int: "int",
    file: "file",
    array: "array",
    id: "id"
};

exports.varTypes = varTypes;

// We also have a list of contactTypes, which is a superset of the list
// of supportedAuthTargets:
var contactTypes = _.extend(supportedAuthTargets, {
    email: "email",
    phoneNumber: "phoneNumber",
});

exports.contactTypes = contactTypes;

// #### Expected request structure

//     {
//         user: ParseUser,
//         params: {
//             api: {
//                 version: apiVersion,
//                 endpoint: targetEndpoint
//             },
//             device: {
//                 version: appVersion,
//                 device: deviceType,  // i.e. "iPhone" "Android" "Sumsang t-100", "PC" "Mac" "Chrome"
//                 os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS) "iPhone OS" "Android" "Windows" "Linux" "Mac OS"
//                 osVersion: deviceOSVersion,
//                 langArray: ["en-GB", "en-US", ...],
//                 locale: "AU"
//             },
//             data: params
//         }
//     }

// #### Example result

// ##### Success

//     {
//         success: true,
//         req: {
//             user: ParseUser || null,
//             device: device,
//             params: validatedParams
//         }
//     }

// ##### Error

//     {
//         success: false,
//         error: apiError.invalidParameters
//     }

// #### Parameter support

//     {
//         auth: null || bool       // If auth is null, authentication is ignored.
//                                  // Otherwise it's strictly checked (true or false).
//         params: {
//             <paramName>: {
//                 type: varType,   // From the varTypes enum above
//                 required:
//                     null ||      // Type is checked if present - otherwise ignored.
//                     bool ||      // false acts the same as null
//                     <condition>  // See the conditional requirement explained below.
//             },
//             <paramName>: {
//                 ...
//             },
//             ...
//         }
//     }

// ##### Conditional requirement

// As opposed to null or a bool, parameters can be required conditionally.
// For instance, to only be required when a token is not provided, set the
// requirement to "!token". Exclude the "!" if you need that other paramater
// for the current to be required too.

// #### Implementation

// We need to check that `req` is valid according to `params`.
exports.process = function (req, params) {
    // We have a helper function to simplify returning errors because that happens everwhere.
    // Note that when it's an error, the returned value should match that of an erroneous
    // response so that it can be quickly returned by the calling function.
    function unsuccessful(error) {
        return {
            success: false,
            error: error
        };
    }

    // console.log('\n\n\nprocess req.params===');
    // console.log(req.params);

    return Parse.Promise.as().then(function () {
        // ##### Basic checks

        var l = loggler.create(req.params.api.endpoint);
        exports.loggler = l;

        // * __Request basic presence check:__ does the request object contain all of the basic blocks for us to proceed?
        if (req == null || req.params == null ||
            req.params.api == null ||
            req.params.api.version == null || req.params.api.endpoint == null
        ) {
            l.log(false, "Processing: request doesn't meet basic requirements").despatch();
            return unsuccessful(apiError.invalidParameters);
        }

        // * __Request device check:__ Is the provided device formatted properly?
        if (req.params.device == null || !_.isString(req.params.device.version) || // Version is something like "1.3.12.1d"
            !_.isString(req.params.device.device) || !_.isString(req.params.device.os) || !_.isString(req.params.device.osVersion) ||
            (req.params.device.langArray && (!_.isArray(req.params.device.langArray) || _.some(req.params.device.langArray, function (lang) {
                return !_.isString(lang);
            }))) ||
            (req.params.device.locale && !_.isString(req.params.device.locale)) ||
            (req.params.device.appName && !_.isString(req.params.device.appName)) ||
            (req.params.device.browser && !_.isString(req.params.device.browser)) ||
            (req.params.device.browserVersion && !_.isString(req.params.device.browserVersion))
        ) {
            l.log(false, "Processing: device format check failed").despatch();
            return unsuccessful(apiError.invalidParameters);
        }

        // } TODO: Sanitise other device data (like langArray)
        // } If sanitisation fails here, there shouldn't be an error, just remove it

        // * __Maintenance/version check:__
        //   * We check whether the app is in maintenance mode or if it doesn't support the
        //     version of the requesting device.


        // console.log("\n\nEverything is fine before the maintenance!!!!!!!!!!!")
        return maintenance.IsVersionAllowances(req.params.device).then(function (allowances) {
            if (allowances.maintenanceMode) {
                l.log(false, "Processing: maintenance mode detected");
                return unsuccessful(apiError.maintenanceMode);
            }

            //   * Next we make sure that the version of the device is valid.
            if (allowances.invalidVersion) {
                l.log(false, "Processing: invalid version detected");
                return unsuccessful(apiError.invalidVersion);
            }

            // ##### Authentication check

            // * Sanitise the authenticated user.

            // If it's not a ParseUser then set it to null.
            if (!_.isObject(req.user) || req.user.className != "_User")
                req.user = null;

            // * Check against requirement.
            // If req.params.auth == null then we don't need to check anything, otherwise:
            // |                                    | req.params.auth == true | req.params.auth == false |
            // |------------------------------------|:-----------------------:|:------------------------:|
            // | (req.params.user == null) == true  | Invalid                 | Valid                    |
            // | (req.params.user == null) == false | Valid                   | Invalid                  |
            // Therefore it's invalid when `req.params.auth == (req.params.user == null)`!
            if (params.auth != null && params.auth == (req.user == null)) {
                l.log(false, "Processing: authentication check failed");
                return unsuccessful(apiError.authenticationRequired);
            }

            // ##### Parameter checks

            // If they didn't provide any information for validation then we can just say everything's good.
            if (params.params == null || _.size(params.params) == 0) {
                l.log(true, "Processing: no parameters, everything's valid");
                return {success: true, req: {user: req.user, device: req.params.device}};
            }

            // See the ParamNode class below for implementation details.
            // console.log('\n\n\nbefore initialise===');
            // console.log(ParamNode);
            ParamNode.initialise(req.params.data, params.params);
            // console.log('\n\n\nafter initialise===');
            // console.log(ParamNode);

            // And just like that... everything's in place!
            if (ParamNode.root.isValid())
                // console.log('\n\n\nParamNode isValid after===');
                // console.log(ParamNode);
                return {
                    success: true,
                    req: {
                        user: req.user,
                        master: req.master,
                        device: req.params.device,
                        params: ParamNode.root.value
                    }
                };

            // If that didn't return then something went wrong in validation, so we tell them that.
            l.log(false, "Processing told us something's invalid :(");
            return unsuccessful(apiError.invalidParameters);
        });
    });
}

// # ParamNode class

// The ParamNode class facilitates effective and efficient processing of complex parameters, mostly recursively.

// Before this class can be used, it's expected that the .initalise() static member has been called to provide
// it with the "background" it requires to perform it's duties.

// This class should be used via the .root static member - that is, imagine there is no public constructor for
// this class, but rather only 
var ParamNode;
(function () {
    ParamNode = function (path, params, provided) {
        // console.log('\n\n\nParamNode function params===');
        // console.log(params);
        // Three private variables are provided via the arguments - no computation needs to be done on them.
        // Note that I'm assuming the ParamNode is being initialised correctly instead of verifying it.

        // `path` should be a string representing it's position in the global param structure, i.e.:
        // item.media.5.mediaId means this node represents the item's 5th media ID.

        // The `params` variable should follow the following structure:
        // {
        //     type: varType,
        //     subtype: <params object>,
        //     required: null || bool || string,
        //     strict: bool
        // }

        // The `provided` variable stores the raw provided value from the original request. This is what
        // all validation will be performed against.

        var valid = undefined;

        // ## NodeFromRelativePath

        // This private is used to retrieve another ParamNode when based on the current one, for instance
        // where the requirement path reads: "!item.media._.mediaId" means "when the mediaId in the same array
        // path as me is not present".
        var nodeFromRelativePath = function (relativePath) {
            // Split the relative path by it's dots so we can substitute "_"s with this node's actual position.
            relativePath = relativePath.split(".");

            _.each(path.split("."), function (fragment, i) {
                if (relativePath[i] == "_")
                    relativePath[i] = fragment;
            });

            // Join it back together and give them the appropriate node.
            return _nodeFromPath(relativePath.join("."));
        };

        // I've unfortunately had to make quite a few things public so one ParamNode can access them in another
        // ParamNode. In a truely OO language these would remain private, but I can't do that with JavaScript.
        this.value = undefined;

        // The public `children` will store a list of child ParamNodes as they're computed so we can quickly
        // go through them to check against requirements. We'll assume that, other than immediately when populated,
        // the children stored here will have been processed so we can easily check whether a value has been
        // provided from it or not.

        // I'd have liked this to be private but I need a node to be able to access a different node's children...
        // Unfortunately this isn't "real" OO so I can't achieve that with a private variable.
        this.children = {};

        // isValid is the big function that recursively checks this node and all of it's children for validity.
        // If this node and all of it's child nodes are valid considering their content and requirement, then
        // return true. Otherwise, if anybody breaches a rule, return false.
        this.isValid = function () {
            // console.log('\n\nisValid function is called===');
            console.log(ParamNode);
            if (valid == undefined) {
                // 1. Sanitise the raw data.
                this.sanitise();

                if (provided != null && this.value == null)
                    exports.loggler.log(path + " has been sanitised to null, from a " + typeof(provided) + ":", provided);

                // 2. Check whether the requirements of this and it's children are met.
                valid = this.checkRequirements();
                // 3. Done!

                if (valid == false) exports.loggler.log("Validation failure with node: " + path + " and value: ", this.value);
            }
            return valid;
        };

        // Below is where all the heavy lifting is performed. It's been split up into three major components.
        // All the best.

        // ## sanitise

        // The purpose of sanitise is to either set the value to null if we consider it as not having been provided,
        // or (if it was provided) ensuring it's value fits that which the type dictates. If it doesn't, it should be
        // set to null.
        this.sanitise = function () {
            // We only want to sanitise it once.
            if (this.value == undefined)
                this.value = provided;
            else
                return;

            // Escape! Setting it to null is very important here, because null is interpreted as "not provided".
            // For instance, if we were looking for a bool, even though null doubles up as an implicit "false"
            // we won't let it behave that way - null simply means "not provided" in our validation.
            if (this.value == null || (_.isString(this.value) && this.value.length == 0))
                return this.value = null;

            // I'm doing the switch in two passes for simplicity - the first pass will mainly nullify values.
            switch (params.type) {
                case varTypes.int:
                    if (!_.isFinite(this.value) || this.value % 1 !== 0)
                        return this.value = null;
                    break;
                case varTypes.contactType:
                case varTypes.email:
                case varTypes.text:
                case varTypes.id:
                    if (!_.isString(this.value))
                        return this.value = null;

                    this.value = this.value.trim();
                    break;
                case varTypes.password:
                    if (!_.isString(this.value))
                        return this.value = null;
                    break;
                case varTypes.bool:
                    if (!_.isBoolean(this.value))
                        return this.value = null;
                    break;
                case varTypes.authToken:
                case varTypes.object:
                case varTypes.dictionary:
                case varTypes.file:
                    if (!_.isObject(this.value))
                        return this.value = null;
                    break;
                case varTypes.array:
                    if (!_.isArray(this.value))
                        return this.value = null;
                    break;
            }

            // This switch will just be to check empty strings.. annoying :/
            switch (params.type) {
                case varTypes.contactType:
                case varTypes.email:
                case varTypes.text:
                case varTypes.id:
                case varTypes.password:
                case varTypes.ambiguous:
                    if (this.value == "" && !params.allowBlank)
                        return this.value = null;
                    break;
            }

            // The second pass will ensure that they're the correct format.
            // Note that value won't be null here because we would've already returned if that was the case.
            // console.log('\n\n\nparams.type===');
            // console.log(params.type);
            switch (params.type) {
                case varTypes.contactType:
                    if (!(param in contactTypes))
                        return this.value = null;
                    break;
                case varTypes.email:
                    if (!_._isValidEmail(this.value))
                        return this.value = null;
                    break;
                case varTypes.id:
                    // console.log('\n\n\nid value===');
                    console.log(this.value);
                    this.value = _.decryptId(this.value);
                    break
                case varTypes.authToken:
                    // Make sure it matches the authToken structure atop this file.
                    if (!_.isString(this.value.id) || this.value.id.trim() == "" || !_.isString(this.value.token) || this.value.token.trim() == "" || !_.isString(this.value.expiration) || this.value.expiration.trim() == "" || !_.isString(this.value.target) || !(this.value.target.trim() in supportedAuthTargets))
                        return valid = false;

                    // Lets recreate the token to clean it up and remove any unnecessaries.
                    this.value = {
                        id: this.value.id.trim(),
                        token: this.value.token.trim(),
                        expiration: this.value.expiration.trim(),
                        target: this.value.target.trim()
                    };

                    break;
                // Objects and arrays share their logic because they're primarily the same: loop through
                // your children if a subtype was provided and process them the same.
                case varTypes.object:
                case varTypes.array:
                    if (params.subtype == null)
                        break;

                    if (params.type == varTypes.object) this.value = {};
                    else this.value = [];

                    var _this = this; // We need to hold onto this so we can use it in _'s .each.
                    _.each(provided, function (subparam, key) {
                        var subtype = params.subtype;
                        if (params.type == varTypes.object)
                            subtype = subtype[key];

                        if (subtype == null)
                            return;

                        if (_this.children[key] == null)
                            _this.children[key] = new ParamNode(path + (path != "" ? "." : "") + key, subtype, provided[key]);
                        var child = _this.children[key];

                        child.sanitise();

                        if (params.type == varTypes.object) _this.value[key] = child.value;
                        else _this.value.push(child.value);
                    });

                    break;
            }
        };

        // ## checkRequirements
        this.checkRequirements = function () {

            /*********************************************
             * A fix to existing bug:
             * If the param spcify {require:true} and such data is not provided
             * at all in client request,a error should be thrown
             *********************************************/
            if (params.type == "object") {
                //the child object
                var subTypeObject = params.subtype;
                //suppose everything right,i.e.the required is set true,and the data exist from client request
                var requiredValueExist = true;
                Object.keys(subTypeObject).forEach(
                    function (key) {
                        //console.log("Key: " + key)
                        //console.log("Value: " + JSON.stringify(subTypeObject[key]))

                        //if the "required" attribute is specified
                        /********
                         * second pass fix:the "require" should match the exact string "true",
                         * not just interpreted as true
                         ********/
                        if (subTypeObject[key].required && subTypeObject[key].required.toString() == "true") {
                            if (provided[key] == undefined) {
                                requiredValueExist = false
                            }
                        }
                    }
                )
                if (requiredValueExist == false) {
                    return false
                }
            }

            /*********************************************
             *********************************************
             *********************************************/


            // 1. Check this node's presence against the requirement.
            // If the value is null, then this is considered as not being present. The sanitisation function
            // should've set things to null where we'd consider them as not having been appropriate input.
            var present = this.value != null;
            var required = params.required;

            //   * Simple case first, if no requirement details were provided, escape!
            if (required == null)
                return true;

            //   * More complicated: if the requirement is referencing another parameter's presence, dereference
            //     that first so we can continue and treat it like a more simple true or false.
            if (_.isString(required)) {
                var negate = required.charAt(0) == "!";
                var requirePath = required.substring(negate ? 1 : 0);
                var node = nodeFromRelativePath(requirePath);

                required = (node != null && node.value != null) == !negate;
            }

            if (_.isObject(required)) {

            } else {
                //   * If it's not required then the only error case is when that requirement is strict (i.e. they
                //     must not provide a value here).
                if (required == false && params.strict == true && present == true)
                    return false;

                //   * If it is required and they didn't provide a value then they're chopped.
                if (required == true && present == false)
                    return false;
            }

            // 2. Ensure all of our children meet their requirements.
            var erroneousChild = false;
            _.each(this.children, function (child, key) {
                if (erroneousChild)
                    return;

                // isValid will check the presence and requirement stuff for us.
                // There's never be an infinite loop because we're asking for a child's isValid only - never going
                // back up the tree. A child's isValid will continue down too - no cycles, I promise (I hope).
                if (!child.isValid())
                    return erroneousChild = true;
            });
            if (erroneousChild)
                return false;

            // 3. All good!
            return true;
        };
    };

    // ## Static members

    // ### NodeFromPath

    // This should be executed after sanitisation, so the node tree would've been fully recursively developed.
    // The purpose of this function is to assist with evaluating requirements like "item.media.0.mediaId".

    // Returns a node (possibly after validating it) by following a path from the root and organising the
    // appropriate variables and values. Bear in mind that these things should only be used AFTER initialising.
    var _nodeFromPath = function (path) {
        path = path.split(".");

        // We'll loop our way down the path and try to find what they're looking for.
        var node = ParamNode.root;
        var i = 0;
        while (i < path.length && node != null) {
            var fragment = path[i++];

            node = node.children[fragment];
        }

        // Return the node that's been traversed down to!
        return node;
    };

    // ## Initialisation

    // Initialisation simply creates and provides a static root node from which all results can be acquired.
    ParamNode.initialise = function (providedParams, requestedParams) {
        ParamNode.root = new ParamNode("", {
            type: varTypes.object,
            subtype: requestedParams,
            required: true,
            bool: true
        }, providedParams);
    };

})();

exports.ParamNode = ParamNode;