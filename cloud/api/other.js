var loggler = require("cloud/loggler");

var api = require("cloud/api/util");

var urlInfo = require("cloud/api/other/urlInfo");
var define=require('../../lib/cloud').define()
Parse.Cloud.define=define
var phone = require("cloud/api/other/phone.js");

// # Other endpoints

// ## Connection check

// This function exists for clients to test if they have a successful
// connection to cloud code when they're experiencing connection issues.
Parse.Cloud.define("connectionCheck", function (req, res) {
    loggler.global.log("connectionCheck for user: " +
        (req.user != null ? req.user.id : "<null>") +
        " at time: " + new Date());

    // No logic required, the client simply needs to know that their
    // request was received and that they can receive our response.
    return res.success();
});

//facebook canvas notification
//this is for saving request recored, so that the corresponding converstion can be displayed in canvas
Parse.Cloud.define("fbNotification", function (req, res) {
    if(!req.user)
        return res.error("false");
    //create record
    Parse.Cloud.useMasterKey();
    console.log("id" +req.conversationId);
    return new Parse.Query("Conversation")
        .equalTo("objectId", req.params.conversationId)
        .first()
        .then(function(conversation){
            if(!conversation)
                return res.error("false");
            var notification = new Parse.Object("FBNotification")
            .set("requestId", req.params.requestId)
            .set("conversation", conversation)
            .set("user", req.user);
            return notification.save().then(function(){
                return res.success("success");
            });
        });
});

// ## urlInfo
Parse.Cloud.define("urlInfo", function (req, res) {
    var supported = api.supports(req, "urlInfo");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    url: {
                        type: api.varTypes.text,
                        required: true
                    },
                    includeMedia: {
                        type: api.varTypes.bool
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return urlInfo[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## RequestPhoneVerificationCode
Parse.Cloud.define("RequestPhoneVerificationCode", function (req, res) {
    var supported = api.supports(req, "RequestPhoneVerificationCode");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    phoneNumber: {
                        type: api.varTypes.text,
                        required: true
                    },
                    countryCode: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return phone[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});