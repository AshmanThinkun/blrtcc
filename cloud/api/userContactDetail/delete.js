//todo api endpoint of parse.com need to be changed
var constants = require("cloud/constants");
var api = require("cloud/api/util");
// var Mixpanel = require("cloud/mixpanel");
var _ = require("underscore");
var trackImmediate = require('cloud/analytics-node').trackImmediate;
var NonUser = require("cloud/nonUser");

// # v1: userContactDetailDelete
(function () {

    var params;
    var user;

    // ## Enums

    // ### userContactDetailDeleteError

    // Subclasses APIError.
    var userContactDetailDeleteError = _.extend(api.error, { 
        recordNotFound:     { code: 404, message: "The requested contact could not be found." },
        detailIsPrimary:    { code: 450, message: "The requested contact could not be deleted because it's a user's primary contact." }
    });

    // Aliased as error for simplicity of internal use.
    var error = userContactDetailDeleteError;

    // ## Request examples
    
    //     req = {
    //         user: ParseUser,
    //         device: {
    //             version: appVersion,
    //             device: deviceType,  // i.e. "iPhone"
    //             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
    //             osVersion: deviceOSVersion,
    //             langArray: ["en-GB", "en-US", ...],
    //             locale: "AU"
    //         },
    //         params: {
    //             id: text
    //         }
    //     }

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        // The master key is not being used here because we won't be interacting with anything for other users.

        params = req.params;
        user = req.user;

        // ### Get the contact detail
        return new Parse.Query(constants.contactTable.keys.className)
            .equalTo(constants.base.keys.objectId, params.id)
            .include(constants.contactTable.keys.user)
            .first()
            .then(function (contact) {

                // If we didn't find anything then report that and abort.
                if(contact == null) {
                    l.log(false, "ALERT: Could not find contact matching requested ID").despatch();
                    return {
                        success: false,
                        error: error.recordNotFound
                    };
                }

                // * Deny requests to delete the primary contact detail.
                if(user.get(constants.user.keys.username) == contact.get(constants.contactTable.keys.Value)) {
                    l.log(false, "ALERT: Requested contact in use as primary").despatch();
                    return {
                        success: false,
                        error: error.detailIsPrimary
                    };
                }

                // If it wasn't a secondary email then we may need to perform some operations on the user,
                // for instance unlinking their Facebook account.
                var userPromise = Parse.Promise.as();
                var contactType = contact.get("type");

                switch(contactType) {
                    case api.contactTypes.facebook:
                        var body = {
                            authData: {
                                facebook: null
                            }
                        };
                        body[constants.user.keys.pendingQueue] = {
                            __op: "Add",
                            objects: [constants.user.pendingQueueActions.processContactDetails]
                        };
                        body[constants.user.keys.pendingQueueCount] = {
                            __op: "Increment",
                            amount: 1
                        };

                        var url = constants.isOpensourceServer ? Parse.serverURL + "/users/" : "https://api.parse.com/1/users/";
                        userPromise = Parse.Cloud.httpRequest({
                            method: "PUT",
                            url: url + user.id,
                            headers: {
                                'X-Parse-Application-Id':   Parse.applicationId,
                                'X-Parse-Master-Key':       Parse.masterKey,
                                'Content-Type':             "application/json"
                            },
                            body: JSON.stringify(body)
                        });

                        break;
                    case api.contactTypes.phoneNumber:
                        userPromise = user.set("phoneNumber", "").save(); //we can't unset, because of a parse C# lib bug. It will not update the record on the client until the next time user login
                        break;
                }

                return userPromise.then(function () {
                    // ### Delete the contact detail
                    return contact.destroy().then(function () {
                         return NonUser.updateNoneUser (contact.get(constants.contactTable.keys.Value), contact.get(constants.contactTable.keys.user).id).then (function () {
                            return { success: true }
                         }, function (queryError) {
                            l.log(false, "ALERT: Error updating the non-user:", queryError).despatch();
                            return Parse.Promise.as({
                                success: false,
                                error: error.subqueryError
                            })
                         })
                        // return new Mixpanel().track({
                        //     event: "Contact Detail Deleted",
                        //     properties: {
                        //         "Type": contactType
                        //     }
                        // }, { id: user.id }).fail(function (promiseError) {
                        //     l.log("ALERT: Failed to Mixpanel track Contact Detail Deleted");
                        // }).always(function () {
                        //     l.log(true).despatch();
                        //     return { success: true };
                        // });

                        

                    }, function (queryError) {
                        l.log(false, "ALERT: Error deleting the contact:", queryError).despatch();
                        return Parse.Promise.as({
                            success: false,
                            error: error.subqueryError
                        })
                    });
                }, function (queryError) {
                    l.log(false, "ALERT: Error updating the user record:", queryError).despatch();
                    return Parse.Promise.as({
                        success: false,
                        error: error.subqueryError
                    })
                });
            }, function (queryError) {
                l.log(false, "ALERT: Error searching for matching contact detail:", queryError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };

})();
