var constants = require("cloud/constants");
var api = require("cloud/api/util");
var _ = require("underscore");

// # v1: userContactDetailFindExistance
(function () {

    var params;
    // ## Enums

    // Aliased as error for simplicity of internal use.
    var error = api.error;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        params = req.params;

        var emails = params.emails;
        if(!emails){
            return Parse.Promise.as({
                success: false,
                error: error.invalidParameters
            });
        }

        emails = _.map(emails, function(email){
            return email.toLowerCase();
        });

        // ### Get the contact detail
        return new Parse.Query(constants.contactTable.keys.className)
            .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
            .containedIn(constants.contactTable.keys.Value, emails)
            .find({useMasterKey:true})
            .then(function (contacts) {
                if(!contacts){
                    return {
                        success: true,
                        existedEmails: []
                    }; 
                }

                var existedEmails = _.map(contacts, function(contact){
                    return contact.get("value");
                });

                l.log(true, "find existedEmails", existedEmails).despatch();
                return {
                    success: true,
                    existedEmails: existedEmails
                };
               
            }, function (queryError) {
                l.log(false, "ALERT: Error find existing email", queryError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };

})();
