var constants = require("cloud/constants");
var api = require("cloud/api/util");

// # v1: userContactDetailCreate
(function () {

    var params;
    var user

    // ## Flow
    exports[1] = function (req) {
        params = req.params;
        user = req.user;

        return new Parse.Query("UserContactDetails")
            .equalTo("user", user)
            .find({useMasterKey:true})
            .then(function (contacts) {
                return contacts.map(function (contact) {
                    return {
                        id: contact.id,
                        type: contact.get("type"),
                        value: contact.get("value"),
                        verified: contact.get("verified")
                    }
                })
            })

    }
})();
