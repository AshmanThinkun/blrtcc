var emailJS = require("cloud/email");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");

// # v1: userContactDetailResendVerification
(function () {

    var params;
    var user;

    // ## Enums

    // ### userContactDetailResendVerificationError

    // Subclasses APIError.
    var userContactDetailResendVerificationError = _.extend(api.error, { 
        recordNotFound:             { code: 404, message: "No UserContactDetail record was found with the provided information." },
        alreadyVerified:            { code: 410, message: "A verification email could not be sent because the requested contact detail is already verified." }
    });

    // Aliased as error for simplicity of internal use.
    var error = userContactDetailResendVerificationError;

    // ## Request examples

    //     req = {
    //         user: ParseUser,
    //         device: {
    //             version: appVersion,
    //             device: deviceType,  // i.e. "iPhone"
    //             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
    //             osVersion: deviceOSVersion,
    //             langArray: ["en-GB", "en-US", ...],
    //             locale: "AU"
    //         },
    //         params: {
    //             id: text
    //         }
    //     }

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        // Don't use master key here - they should be authenticated to the correct user to resend verification.

        params = req.params;
        user = req.user;

        // 1. Search for matching contact detail.
        return new Parse.Query(constants.contactTable.keys.className)
            .equalTo(constants.base.keys.objectId, params.id)
            .include(constants.contactTable.keys.user)
            .first({useMasterKey:true})
            .then(function (first) {
                if(first == null) {
                    // If no record was found we should exit and return an appropriate error.
                    l.log(false, "No contact detail found").despatch();
                    return {
                        success: false,
                        error: error.recordNotFound
                    };
                }

                var contactUser = first.get(constants.contactTable.keys.user);

                // 2. Make sure that the user attached to the contact detail belongs to the requesting user.
                if(contactUser.id != user.id) {
                    l.log(false, "Contact belongs to different user, contact: " + contactUser.id + ", user: " + user.id).despatch();
                    return {
                        success: false,
                        // If this happened then it's probably a malicious request, so we shouldn't tell them
                        // exactly what the problem was.
                        error: error.unknown
                    };
                // 3. Make sure that the contact detail is not already verified.
                } else if(first.get(constants.contactTable.keys.verified)) {
                    l.log(false, "Contact is already verified", first).despatch();
                    return {
                        success: false,
                        error: error.alreadyVerified
                    };
                }

                // 4. Prepare email variables.
                var targetEmail = first.get(constants.contactTable.keys.Value);

                var customMergeVars = {
                    "BASE_EMAIL": user.get(constants.user.keys.Email),
                    "EMAIL_VERIFICATION_LINK": constants.protocol + constants.siteAddress + "/vem?" + _.hashedQueryArgs({
                        "coid": _.encryptId(first.id),
                        "s": _.salt()
                    })
                };

                // Secondary or primary email?
                if(user.get(constants.user.keys.username) != first.get(constants.contactTable.keys.Value))
                    customMergeVars["IS_SECONDARY"] = "true";

                // 5. Send the email!
                return emailJS.SendTemplateEmail({
                    template:       "email-verification",
                    recipients:     [{
                        name: user.get(constants.user.keys.FullName),
                        email: targetEmail,
                        isUser: true,
                        user:   user
                    }],
                    customMergeVars: customMergeVars
                }).then(function () {
                    l.log(true).despatch();
                    return {success: true};
                }, function (error) {
                    // It'd be a crying shame if something went wrong at this point...
                    l.log(false, "Error sending email template:", error).despatch();
                    return {
                        success: false,
                        error: error.subqueryError
                    };
                });
            }, function (error) {
                // Error for "1. Search for matching contact detail".
                l.log(false, "Error searching for matching contact detail:", error).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };

})();
