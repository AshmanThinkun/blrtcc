var emailJS = require("cloud/email");
var permissionsJS = require("cloud/permissions");
// var Mixpanel = require("cloud/mixpanel");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var phoneJS = require("cloud/api/other/phone.js");
var NonUser = require("cloud/nonUser");

// # v1: userContactDetailCreate
(function () {

    var params;
    var user;

    // ## Enums

    // ### userContactDetailCreateStatus
    var userContactDetailCreateStatus = { 
        detailCreated:                      { code: 100, message: "The requested contact detail was successfully created." },
        detailCreatedNoVerificationEmail:   { code: 101, message: "The requested contact detail was created but there was an error when sending the verification email." },
        alreadyAdded:                       { code: 110, message: "The user was already using a contact detail that matched the request." }
    };

    // ### userContactDetailCreateError

    // Subclasses APIError.
    var userContactDetailCreateError = _.extend(api.error, { 
        detailInUse:                { code: 410, message: "Another user owns a contact detail which the request would've clashed with." },
        tooManyContacts:            { code: 450, message: "The user has reached the contact detail limit permissible by their active account type." }
    });

    // Aliased as error for simplicity of internal use.
    var error = userContactDetailCreateError;

    // ## Request examples

    // ### New secondary email
    
        // req = {
        //     user: ParseUser,
        //     device: {
        //         version: appVersion,
        //         device: deviceType,  // i.e. "iPhone"
        //         os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
        //         osVersion: deviceOSVersion,
        //         langArray: ["en-GB", "en-US", ...],
        //         locale: "AU"
        //     },
        //     params: {
        //         email: email
        //     }
        // }

    // ### New token contact (i.e. Facebook)
    
    //     req = {
    //         user: ParseUser,
    //         device: {
    //             version: appVersion,
    //             device: deviceType,  // i.e. "iPhone"
    //             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
    //             osVersion: deviceOSVersion,
    //             langArray: ["en-GB", "en-US", ...],
    //             locale: "AU"
    //         },
    //         params: {
    //             token: authToken
    //         }
    //     }

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        params = req.params;
        user = req.user;

        //checking param
        if(!(params.token || params.phoneNumber || params.email)){
            return Parse.Promise.as({ success: false, error: api.error.invalidParameters });
        }
        if(params.email && (params.token || params.phoneNumber || params.extra)){
            return Parse.Promise.as({ success: false, error: api.error.invalidParameters });
        }
        if((params.token || params.phoneNumber) && !params.extra){
            return Parse.Promise.as({ success: false, error: api.error.invalidParameters });
        }

        var requestType;
        var value;
        if(params.token){
            requestType = "token";
            value = params.token.id;
        }else if(params.email){
            requestType = "email";
            value = params.email.trim().toLowerCase();
        }else if(params.phoneNumber){
            requestType = "phoneNumber";
            value = params.phoneNumber;
        }

        // ### Perform preliminary checks
        return Parse.Promise.when(
            permissionsJS.getSinglePermissions({ user: user }, true),
            Parse.Query.or(
                // Look for a conflicting contact detail if one exists.
                new Parse.Query(constants.contactTable.keys.className)
                    .equalTo(constants.contactTable.keys.type, params.token ? params.token.target : requestType)
                    .equalTo(constants.contactTable.keys.Value, value)
                    .notEqualTo(constants.contactTable.keys.placeholder, true),
                // We're also grabbing all of the user's contact details to check for permissions.
                new Parse.Query(constants.contactTable.keys.className)
                    .equalTo(constants.contactTable.keys.user, user)
            ).find({useMasterKey:true})
        ).then(function (permissions, contacts) {
            console.log('\n\n\n===email create permissions: '+JSON.stringify(permissions));
            // #### Existing contact detail check
            var duplicate = null;
            var numEmails = 0;
            var numPhones = 0;
            var oldPhoneNumberDetails = [];

            _.each(contacts, function (contact) {
                if(duplicate)
                    return;

                if(contact.get(constants.contactTable.keys.user).id != user.id) {
                    l.log(false, "Somebody else has claimed this contact detail:", contact).despatch();
                    duplicate = contact;
                } else if(contact.get(constants.contactTable.keys.Value) == value) {
                    l.log(false, "Requesting user is already using target contact detail").despatch();
                    duplicate = contact;
                }

                if(contact.get("type") == constants.contactTable.types.email)
                    ++numEmails;
                if(contact.get("type") == constants.contactTable.types.phoneNumber){
                    ++numPhones;
                    oldPhoneNumberDetails.push(contact);
                }
            });

            // If we found an existing one, return an appropriate response.
            if(duplicate) {
                if(duplicate.get(constants.contactTable.keys.user).id == user.id)
                    return {
                        success: true,
                        status: userContactDetailCreateStatus.alreadyAdded
                    };
                else
                    return {
                        success: false,
                        error: error.detailInUse
                    };
            }

            // #### User permission check

            // We don't need to check this if it's not for a secondary email.
            if(params.email) {
                // Reduce the count by one to account for their primary email.
                if(numEmails - 1 >= permissions[constants.permissions.values.maxSecondaryEmails]) {
                    l.log(false, "Requesting user does not have permission to add this many secondary emails").despatch();
                    return {
                        success: false,
                        error: error.tooManyContacts
                    }
                }
            }

            var extraPromise = Parse.Promise.as();

            // ### Update user record
            // We need to link the user to a service if this request is coming from a token.
            if(params.token) {
                // Extra information may be provided which we need to put in too, like the Facebook name and gender, etc...
                if(params.extra != null) {
                    _.each(params.extra, function (value, key) {
                        user.set(key, value);
                    });
                }

                // Inform clients to search for and link relationships that may have been pointing to this token before.
                // For emails, this will be done on verification instead.
                user.set(constants.user.keys.ContactInfoDirty, true);
                
                switch(params.token.target) {
                    // This isn't necessary for secondary emails, but it is in other cases, for instance
                    // for linking the ParseUser to Facebook.
                    case api.contactTypes.facebook:
                        user.add(constants.user.keys.pendingQueue, constants.user.pendingQueueActions.processContactDetails);
                        user.increment(constants.user.keys.pendingQueueCount);

                        extraPromise = Parse.FacebookUtils.link(user, {
                            id: params.token.id,
                            access_token: params.token.token,
                            expiration_date: params.token.expiration
                        }).then(function(){return true;});

                        break;
                }
            }else if(params.phoneNumber){
                //validate the phoneNumber
                extraPromise = phoneJS.VerifyPhoneNumber(params.phoneNumber, params.extra.verificationCode);
            }

            return extraPromise.then(function (verificationResult) {
                if(verificationResult===false){
                    return Parse.Promise.as({ success: false, error: api.error.invalidParameters });
                }

                // ### Create new contact detail
                var contact = new Parse.Object(constants.contactTable.keys.className)
                    .set(constants.contactTable.keys.user, user)
                    .set(constants.contactTable.keys.type, params.token ? params.token.target : requestType)
                    .set(constants.contactTable.keys.Value, value)
                    .setACL(new Parse.ACL(user));

                if(params.email)
                    contact.set(constants.contactTable.keys.placeholder, true);
                else
                    contact.set(constants.contactTable.keys.verified, true);

                return contact.save(null,{useMasterKey:true}).then(function () {
                    // ### Follow-up actions
                    var promises = [Parse.Promise.as()];

                    // #### Delete old phone Number details
                    if(requestType=="phoneNumber" && oldPhoneNumberDetails.length>0){
                        promises.push(Parse.Object.destroyAll(oldPhoneNumberDetails));
                    }

                    // // #### Associate NonUser
                    // if(requestType=="facebook"|| requestType=="phoneNumber"){
                    //     promises.push(NonUser.associate(user));
                    // }

                    // #### Send verification email
                    var verificationEmailError = false;

                    if(params.email) {
                        promises.push(emailJS.SendTemplateEmail({
                            template:       "email-verification",
                            recipients:     [{
                                name: user.get(constants.user.keys.FullName),
                                email: value,
                                isUser: true,
                                user:   user
                            }],
                            customMergeVars: {
                                "BASE_EMAIL": user.get(constants.user.keys.Email),
                                "EMAIL_VERIFICATION_LINK": constants.protocol + constants.siteAddress + "/vem?" + _.hashedQueryArgs({
                                    "coid": _.encryptId(contact.id),
                                    "s": _.salt()
                                }),
                                "IS_SECONDARY": true
                            }
                        }).fail(function (error) {
                            l.log("ALERT: Error when sending the verification email:", error);
                            verificationEmailError = true;
                        }));
                    }

                    // #### Remove pending queue action
                    if(params.token) {
                        promises.push(
                            user.remove(constants.user.keys.pendingQueue, constants.user.pendingQueueActions.processContactDetails)
                                .increment(constants.user.keys.pendingQueueCount, -1)
                                .save(null,{useMasterKey:true})
                                .fail(function (error) {
                                    l.log("ALERT: Error when removing from user's pending queue:", error);
                                })
                        );
                    }

                    if(params.phoneNumber){
                        promises.push(
                            user.set(constants.user.keys.ContactInfoDirty, true)
                                .set("phoneNumber", params.phoneNumber)
                                .save(null,{useMasterKey:true})
                        );
                    }


                    // ### Wrap up
                    return Parse.Promise.when(promises).always(function () {
                        NonUser.associate(user,false);
                        return {
                            success: true,
                            status: verificationEmailError ?
                                userContactDetailCreateStatus.detailCreatedNoVerificationEmail :
                                userContactDetailCreateStatus.detailCreated
                        };
                    });
                }, function (error) {
                    l.log(false, "Error saving the new contact detail:", error).despatch();
                    return Parse.Promise.as({
                        success: false,
                        error: api.error.subqueryError
                    });
                });
            }, function (error) {
                l.log(false, "Error occurred in updating the user record:", error).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: api.error.subqueryError
                });
            });
        }, function (errors) {
            // An error occurred during the preliminary checks.
            if(preliminaryError != null)    // Was a response prepared for this error beforehand?
                return Parse.Promise.as(preliminaryError);

            l.log(false, "Error(s) occurred during preliminary checks:", error).despatch();
            return Parse.Promise.as({
                success: false,
                error: api.error.subqueryError
            });
        });
    }
})();
