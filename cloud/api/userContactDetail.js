var api = require("cloud/api/util");
var create = require("cloud/api/userContactDetail/create");
var delete_ = require("cloud/api/userContactDetail/delete");
var resendVerification = require("cloud/api/userContactDetail/resendVerification");
var findExistance = require("cloud/api/userContactDetail/findExistance");
var fetch=require("cloud/api/userContactDetail/fetch")

var define=require('../../lib/cloud').define()
Parse.Cloud.define=define
// # UserContactDetail
// ## Create
Parse.Cloud.define("userContactDetailCreate", function (req, res) {
    var supported = api.supports(req, "userContactDetailCreate");
    if(!supported.success)
        // } We can't run this endpoint under the requested version!
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: { //complicated condition checking need to be implmented by your own logic
                    email: {
                        type: api.varTypes.email,
                    },
                    token: {
                        type: api.varTypes.authToken,
                    },
                    phoneNumber:{
                        type: api.varTypes.text,
                    },
                    extra: {
                        type: api.varTypes.dictionary,
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return create[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Delete
Parse.Cloud.define("userContactDetailDelete", function (req, res) {
    var supported = api.supports(req, "userContactDetailDelete");
    if(!supported.success)
        // } We can't run this endpoint under the requested version!
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    id: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return delete_[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Resend verification
Parse.Cloud.define("userContactDetailResendVerification", function (req, res) { 
    var supported = api.supports(req, "userContactDetailResendVerification");
    if(!supported.success)
        // } We can't run this endpoint under the requested version!
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,                            // An authenticated user is expected
                params: {
                    id: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return resendVerification[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


// ## Find Existance
Parse.Cloud.define("userContactDetailFindExistance", function (req, res) { 
    var supported = api.supports(req, "userContactDetailFindExistance");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    emails: {
                        type: api.varTypes.array,
                        required: true
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return findExistance[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

Parse.Cloud.define("getUserContactDetails", function (req, res) {
    var supported = api.supports(req, "alwaysAvailable");
    if(!supported.success)
        return res.success(JSON.stringify(supported));
    var v = {
        1: function () {
            api.process(req, {
                auth: true,
            }).then(function (processed) {
                if(processed.success) {
                    return fetch[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});