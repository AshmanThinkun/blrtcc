/* global Parse */
var _ = require("underscore");
var pendingActions = require("cloud/pendingActions");
var api = require("cloud/api/util");

var define=require('../../lib/cloud').define()
Parse.Cloud.define=define

Parse.Cloud.define("pendingActionsHandled", function (req, res) {
    var PendingAction = Parse.Object.extend("PendingAction");
    var params = req.params;
    console.log(params);
    var user = req.user;
    var version = req.headers['x-parse-app-build-version'];
    var Installation = req.headers['x-parse-installation-id'];
    var objectsToSave = [];
    var objectIds = _.keys(params);
    return new Parse.Query('PendingAction').containedIn('objectId', objectIds).find({useMasterKey: true}).then(function(results){
        console.log("[!] User "+user.id+" with installation "+Installation+" and version "+version+" has handled their Pending Actions");
        _.each(results, function(pendingAction){
           var success = params[pendingAction.id];
           pendingAction.set('appVersion', version);
           if (success) {
               pendingAction.set('complete', true);
               console.log(" `-- Pending Action "+pendingAction.id+" successfully completed.");
           } else {
               pendingAction.increment('failCount');
               console.log(" `-- Pending Action "+pendingAction.id+" failed to complete "+pendingAction.get('failCount')+" time(s) with latest version: "+version);
           }
           objectsToSave.push(pendingAction);
        });
        return Parse.Object.saveAll(objectsToSave)
            .then(function(){
                console.log("[!] Pending actions resolved");
                res.success({success: true});
            },function(error){
                console.log('[!] Failed to save objects');
                res.error({success: false});
            });
    });
    res.success("");
});

Parse.Cloud.define("getPendingActions", function (req, res) {
    var user = req.user;
    var version = req.headers['x-parse-app-build-version'];
    var Installation = req.headers['x-parse-installation-id'];
    var objectsToSave = [];
    return new Parse
        .Query(Parse.Installation)
        .equalTo("installationId", Installation)
        .first({useMasterKey: true})
        .then(function(installation) {
            pendingActions.get(user, installation).then(function(results){
                console.log("[!] User "+user.id+" with installation "+Installation+" and version "+version+" has requested their Pending Actions");
                var response = [];
                _.each(results, function(action) {
                    if (action.get('failCount') == 0 || (action.get('failCount') > 0 &&  action.get('appVersion') != version)) {
                        console.log(" `-- Pending Action "+action.id+" with appVersion "+action.get('appVersion')+", failCount "+action.get('failCount')+" and tryCount "+action.get('tryCount'));
                        response.push({t: action.get('type'), pid: action.id, args: JSON.parse(action.get('arguments'))});
                        action.increment('tryCount');
                        action.set('appVersion', version);
                        objectsToSave.push(action);
                    } else {
                        console.log(" `-- SKIPPING Action "+action.id+" with appVersion "+action.get('appVersion')+", failCount "+action.get('failCount')+" and tryCount "+action.get('tryCount'));
                    }
                });
                return Parse.Object.saveAll(objectsToSave)
                    .then(function(){
                        console.log(response);
                        res.success(response);
                    },function(error){
                        console.log('[!] Failed to save objects');
                        res.error({success: false});
                    });
            });
        });
});