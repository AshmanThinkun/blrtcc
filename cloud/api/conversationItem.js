var api = require("cloud/api/util");

var create = require("cloud/api/conversationItem/create");
var publicJS = require("cloud/api/conversationItem/public");
var fromUrl = require("cloud/api/conversationItem/fromUrl");
var viewJ = require("cloud/api/conversationItem/view");
var fetchAll = require("cloud/api/conversationItem/fetchAll");
var search = require("cloud/api/conversationItem/search");

var publicBlrt = require("cloud/api/conversationItem/publicBlrt");

var define=require('../../lib/cloud').define()
Parse.Cloud.define=define

// # ConversationItem

// ## Create
Parse.Cloud.define("conversationItemCreate", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationItemCreate");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    conversationId: {
                        // type: api.varTypes.id,
                        type: api.varTypes.text,
                        required: true
                    },
                    items: {
                        type: api.varTypes.array,
                        required: {presence: true, min: 1},
                        subtype: {
                            blrt: {
                                type: api.varTypes.object,
                                required: "!items._.comment",
                                strict: true,
                                subtype: {
                                    arg: {
                                        type: api.varTypes.object,
                                        required: true,
                                        subtype: {
                                            audioLength: {
                                                type: api.varTypes.int,
                                                required: {presence: true, min: 0}
                                            },
                                            items: {
                                                type: api.varTypes.array,
                                                required: {presence: true, min: 1},
                                                subtype: {
                                                    pageString: {
                                                        type: api.varTypes.text,
                                                        required: true
                                                    },
                                                    mediaId: {
                                                        type: api.varTypes.text,
                                                        required: true
                                                    }
                                                }
                                            },
                                            hiddenItems: {
                                                type: api.varTypes.array,
                                                subtype: {
                                                    pageString: {
                                                        type: api.varTypes.text,
                                                        required: true
                                                    },
                                                    mediaId: {
                                                        type: api.varTypes.text,
                                                        required: true
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    localGuid: {
                                        type: api.varTypes.text,
                                        required: true
                                    },
                                    thumbnail: {
                                        type: api.varTypes.file,
                                        required: true
                                    },
                                    files: {
                                        type: api.varTypes.object,
                                        required: true,
                                        subtype: {
                                            encryptType: {
                                                type: api.varTypes.int,
                                                required: true
                                            },
                                            encryptValue: {
                                                type: api.varTypes.text
                                            },
                                            audio: {
                                                type: api.varTypes.file,
                                                required: true
                                            },
                                            touch: {
                                                type: api.varTypes.file,
                                                required: true
                                            }
                                        }
                                    },
                                    media: {
                                        // This may not be provided if the conversation is created solely from templates.
                                        type: api.varTypes.array,
                                        subtype: {
                                            mediaId: {
                                                type: api.varTypes.text
                                            },
                                            name: {
                                                type: api.varTypes.text,
                                                // All requirements start from the root of the parameters so we need to build a path back here.
                                                // "_" tells the requirement to reference the object sobject to the variable being checked.
                                                required: "!item.media._.mediaId",
                                                strict: true
                                            },
                                            localGuid: {
                                                type: api.varTypes.text,
                                                required: "!item.media._.mediaId",
                                                strict: true
                                            },
                                            file: {
                                                type: api.varTypes.file,
                                                required: "!item.media._.mediaId",
                                                strict: true
                                            },
                                            format: {
                                                type: api.varTypes.int,
                                                required: "!item.media._.mediaId",
                                                strict: true
                                            },
                                            encryptType: {
                                                type: api.varTypes.int,
                                                required: "!item.media._.mediaId",
                                                strict: true
                                            },
                                            encryptValue: {
                                                type: api.varTypes.text,
                                                // This strictly cannot be provided if mediaId is provided, however if mediaId is not provided then
                                                // this either can or cannot be provided. Therefore, when !item.media._.mediaId then required = null.

                                                // In JavaScript, if you typed `var x = 5 && null` x would evaluate to equal null.
                                                // } TODO: support this! (don't forget to uncomment pageArgs after)
                                                // } required: "!item.media._.mediaId && null",
                                                // A strict null requirement doesn't check anything.
                                                // } strict: true
                                            },
                                            pageArgs: {
                                                type: api.varTypes.text,
                                                // } required: "!item.media._.mediaId && null",
                                                // } strict: true
                                            }
                                        }
                                    }
                                }
                            },
                            comment: {
                                type: api.varTypes.object,
                                subtype: {
                                    localGuid: {
                                        type: api.varTypes.text,
                                        required: true
                                    },
                                    text: {
                                        type: api.varTypes.text,
                                        required: true
                                    }
                                }
                            }
                        }
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return create[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


// ## Public
Parse.Cloud.define("conversationItemPublic", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationItemPublic");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, { // An authenticated user is expected for client app side api call
                auth: true,
                params: {
                    blrtId: {
                        type: api.varTypes.text,
                        required: true
                    },
                    name: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return publicJS[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


Parse.Cloud.define("conversationItemPublicBlrt", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationItemPublicBlrt");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, { // An authenticated user is expected for client app side api call
                auth: true,
                params: {
                }
            }).then(function (processed) {
                if (processed.success) {
                    console.log('public blrt');
                    return publicBlrt[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


// ## FromUrl
// get conversation item from url
Parse.Cloud.define("conversationItemFromUrl", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationItemFromUrl");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    url: {
                        type: api.varTypes.text,
                        required: true
                    },
                    includeMedia: {
                        type: api.varTypes.bool
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return fromUrl[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


// ## View
// increase private play counter
Parse.Cloud.define("conversationItemView", function (req, res) {
    req.start();
    var supported = api.supports(req, "conversationItemView");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    blrtId: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return viewJ[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


Parse.Cloud.define("fetchAllConversationItem", function (req, res) {
    console.log('\n\nfetchAllConversationItem req===');
    console.log(req);
    req.start(); //start tracking
    var supported = api.supports(req, "fetchAllConversationItem");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    conversationId: {
                        type: api.varTypes.id,
                        required: true
                    },
                    pageNo: {
                        type: api.varTypes.int,
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    console.log('\n\nprocessed.req===');
                    console.log(processed.req);
                    return fetchAll[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        },error);
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


var setRead=require("cloud/api/conversationItem/setRead");
Parse.Cloud.define("setRead", function (req, res) {
    console.log('\n\n===setRead cloud code function called===');
    console.log(req);
    req.start(); //start tracking
    var supported = api.supports(req, "setRead");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    conversationUserRelationId: {
                        type: api.varTypes.id,
                        required: true
                    },
                    readableIndex: {
                        // type: api.varTypes.int,
                        type: api.varTypes.array,
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return setRead[1](processed.req).then(function (result) {
                        console.log("The result is : "+JSON.stringify(result))
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});







// Parse.Cloud.define("search", function (req, res) {
//     res.start();
//     var supported = api.supports(req, "search");
//     if(!supported.success)
//         return res.success(JSON.stringify(supported));
//
//     console.log("!!!!!!!!!!!!!")
//
//     var v = {
//         1: function () {
//             api.process(req, {
//                 auth: true,
//                 params: {
//                     text: {
//                         type: api.varTypes.text,
//                         required: true
//                     }
//                 }
//             }).then(function (processed) {
//                 if(processed.success) {
//                     return search[1](processed.req).then(function (result) {
//                         return res.success(JSON.stringify(result));
//                     }, function (error) {
//                         api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
//                         return res.error({
//                             success: false,
//                             error: api.error.subqueryError
//                         });
//                     });
//                 } else
//                     return res.success(JSON.stringify(processed));
//             }, function (error) {
//                 return res.success(JSON.stringify({
//                     success: false,
//                     error: api.apiError.subqueryError
//                 }))
//             });
//         }
//     };
//
//     return v[supported.version]();
// });
//

