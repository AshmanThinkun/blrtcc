const _ = require('underscore');
Parse
    .Cloud
    .define('getContacts', (req, res) => {
        const {user} = req;
        let contactRelationQuery = new Parse
            .Query('ConversationUserRelation')
            .matchesKeyInQuery('conversation', 'conversation', new Parse.Query('ConversationUserRelation').equalTo('user', user))
            .notEqualTo('user', user)
            .include('user')
            .find({useMasterKey: true})
            .then((relations) => {
                const userIds = [];
                const nonUserEmails = []; //we only care about email not others such as facebook
                const nonUserInfo = []; //format should be returned

                relations.forEach((relation) => {
                    // console.log(relation.get('queryValue'))
                    let user = relation.get('user');
                    if (user) {
                        userIds.push(user.id);
                    } else if (relation.get('queryType') === 'email') {
                        nonUserEmails.push(relation.get('queryValue'))
                        nonUserInfo.push({
                            isNonUser: true,
                            email: relation.get('queryValue'),
                            name: relation.get('queryName')
                        })
                    } else {
                        //nonUser is added by phoneNumber ? we can not do anything in webapp anyway
                    }
                })

                return new Parse //look if there is any nonUser is claimed
                    .Query('UserContactDetail')
                    .equalTo('type', 'eamil')
                    .containedIn('value', nonUserEmails)
                    .find({useMasterKey: true})
                    .then((claimedUsers) => {
                        const claimedUserIds = []
                        const claimedUserEmails = []
                        claimedUsers.forEach((cu) => {
                            claimedUserIds.push(cu.get('user').id);
                            claimedUserEmails.push(cu.get('value'));
                        })

                        const allUserIds = userIds.concat(claimedUserIds);
                        const allUserIdsWithoutSelf = _.without(allUserIds, user.id)
                        const finalUserIds = _.uniq(allUserIdsWithoutSelf);
                        const remainningNonUserInfo = _.reject(nonUserInfo, (info) => {
                            return claimedUserEmails.indexOf(info.email) > -1
                        })
                        const finalNonUserInfo = _.uniq(remainningNonUserInfo, (x) => {
                            return x.email;
                        })

                        Parse
                            .Promise
                            .when(finalUserIds.map(id => new Parse.Query(Parse.User).get(id)))
                            .then((users) => {
                                const finalUserInfo = users.map((u) => ({
                                    id: u.id,
                                    email: u.get('email'),
                                    name: u.get('name') || u.get('username'),
                                    avatar: u.get('avatar') && u
                                        .get('avatar')
                                        .url(),
                                    facebook: u.get('fb_id')
                                        ? true
                                        : false,
                                    accountStatus: u.get('accountStatus') || false,

                                }))
                                res.success(finalUserInfo.concat(finalNonUserInfo))
                            })
                    })

            })
            .fail((e) => {
                console.log(e)
            })
    })