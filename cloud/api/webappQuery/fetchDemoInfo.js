var _ = require('underscore');
Parse.Cloud.define('demoInfo', function (req, res) {
    Parse.Cloud.useMasterKey()
    var result = {}
    result.convo = {};
    result.blrt = {}
    var convoId = _.decryptId(req.params.convoId);
    new Parse.Query('Conversation')
        .equalTo('objectId', convoId)
        .include('creator')
        .first()
        .then(function (convo) {
            if (!convo) return new Parse.Promise.error('convo not found')
            var convoResult = {}
            convoResult.title = convo.get('blrtName');
            convoResult.contentCount = convo.get('contentCount');
            var convoCreator = convo.get('creator');
            convoResult.creatorName = convoCreator.get('name') || convoCreator.get('username');
            convoResult.contentUpdate = convo.get('contentUpdate');
            convoResult.thumbnail = convo.get('thumbnailFile').url();
            result.convo = convoResult;
            return new Parse.Query('ConversationItem')
                .equalTo('conversation', convo)
                .exists('audioFile')
                .ascending('index')
                .include('creator')
                .first()
        })
        .then(function (blrt) {
            var blrtResult = {}
            var blrtCreator = blrt.get('creator');
            var arguments = JSON.parse(blrt.get('argument'))
            blrtResult.id = _.encryptId(blrt.id)
            blrtResult.isPublic = blrt.get('isPublicBlrt')
            blrtResult.publicName = blrt.get('publicName') || false
            blrtResult.creatorName = blrtCreator.get('name') || blrtCreator.get('username')
            blrtResult.creatorAvatar = blrtCreator.get('avatar') ? blrtCreator.get('avatar').url() : false
            blrtResult.contentUpdate = blrt.updatedAt;
            blrtResult.audioLength = arguments.AudioLength;
            blrtResult.itemCount = arguments.Items.length;
            blrtResult.thumbnail = blrt.get('thumbnailFile') ? blrt.get('thumbnailFile').url() : false;
            result.blrt = blrtResult;
            res.success(result)
        })
        .fail(function (err) {
            res.error(err)
        })
})

Parse.Cloud.define('previewInfo', function (req, res) {
    console.log("call  previewInfo")
    const {convo, token}=req.params;
    console.log('===token==='+JSON.stringify(token)+' convo==='+JSON.stringify(convo));
    if (!(convo && token)) {
        res.error({success: false, message: "convo and token are required"})
    } else {
        Parse.Promise.when(
            new Parse.Query('Conversation').get(convo),
            new Parse.Query('BlrtAccessToken')
                .equalTo('token', token)
                .first({useMasterKey: true})
        ).then(function (convo, token) {
            console.log(convo)
            console.log(token)
            if (token.get('accessCountdown') <= 0) {
                return res.success({success: false, type: 0, message: "you can not view more than 3 times"})
            } else {
                console.log('\n\n\n===Conversation found===');
                new Parse.Query('ConversationItem')
                    .equalTo('conversation', convo)
                    .containedIn('type', [0, 2])
                    .ascending('index')
                    .include('creator')
                    .first()
                    .then(function (blrt) {
                        console.log('===blrt found==='+JSON.stringify(blrt));
                        if(!blrt) {
                            return res.success({success: false, type: 3, message: "The blrt is not available"})
                        }else if(!blrt.get('promoVideo')) {
                            return res.success({success: false, type: 1, message: "The video is not available"})
                        }else {
                            console.log('\n\n\n===promoVideo==='+blrt.get('promoVideo'));
                            token.increment('accessCountdown', -1).save()
                            return res.success({
                                success: true, type: 2, info: {
                                    title: convo.get('blrtName'),
                                    src: blrt.get('promoVideo')
                                }
                            })
                        }
                    })
            }


        }).fail(function (e) {
            res.error({success: false, message: JSON.stringify(e)})
        })


    }


})