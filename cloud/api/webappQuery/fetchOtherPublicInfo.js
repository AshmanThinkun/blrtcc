Parse.Cloud.define('otherUserPublic',function (req,res) {
    Parse.Cloud.useMasterKey();
    var otherUserId=req.params.otherUserId;
    console.log("otherUserId "+otherUserId)
    var result={}
    new Parse.Query(Parse.User)
        .equalTo('objectId',otherUserId)
        .include('activeAccountTypeId')
        .first()
        .then(function (user) {
            if(!user) {
                return new Parse.Promise.error('user do not exist')
            }else
            {
                result.accountType=user.get('activeAccountTypeId').get('marketingName');
                result.primaryEmail=user.get('email');
                result.publicBlrtCount=user.get('publicBlrtCount')||0;
                return new Parse.Query('UserContactDetails').equalTo('user',user).find()
            }

        })
        .then(function (contactDetails) {
            var isPhone=false;
            var isFB=false;
            contactDetails.forEach(function (detail) {
                if(detail.get('type')==="phoneNumber")
                {
                    isPhone=true
                }else if(detail.get('type')==="facebook")
                {
                    isFB=true
                }
            })
            result.isPhone=isPhone;
            result.isFB=isFB;
            res.success(result);
        })
        .fail(function (err) {
            res.error(err)
        })
        
})