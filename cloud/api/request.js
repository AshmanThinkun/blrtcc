var api = require("cloud/api/util");
var details = require("cloud/api/request/details");
var define=require('../../lib/cloud').define()
Parse.Cloud.define=define
// # Request

// ## Details
Parse.Cloud.define("requestDetails", function (req, res) {
    var supported = api.supports(req, "requestDetails");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                	requestId: {
                		type: api.varTypes.id,
                		required: true
                	}
                }
            }).then(function (processed) {
                if(processed.success) {
                    return details[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});