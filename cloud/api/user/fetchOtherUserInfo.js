

var constants = require("cloud/constants");
var api = require("cloud/api/util");
var _ = require("underscore");

// # v1: fetch other user's information
(function () {
    var user;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        var paramsUserId = req.params.userId;

        var resultObject = {};
        return new Parse.Query(Parse.User)
            .get(paramsUserId,{useMasterKey: true })
            .then(function (user) {
                resultObject.user = user;
                return new Parse.Query("UserContactDetails")
                    .equalTo("user", user)
                    .find({useMasterKey: true })

            })
            .then(function (contacts) {
                contacts=contacts.map(function (contact) {
                    return {
                        id: contact.id,
                        type: contact.get("type"),
                        value: contact.get("value"),
                        verified: contact.get("verified")
                    }
                })
                resultObject.contacts=contacts;
                return {success:true,resultObject:resultObject}
            })
        .fail(function (error) {
            return {success:false,error:error}
        })


    }
})();
