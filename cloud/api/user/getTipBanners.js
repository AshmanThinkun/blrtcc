var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var utilJS = require("cloud/util");
var ParseMutex = require("cloud/ParseMutex");
// var Mixpanel = require("cloud/mixpanel");

// # v1: userGetTipBanners
(function () {
    var params;
    var user;

    // ## Enums
    var colorScheme = {
        darkGrey:       1,
        blrtGreen:      2,
        darkRed:        3,
        lightRed:       4,
    };

    // ### userGetTipBanners

    // Subclasses APIError.
    var conversationDeleteError = _.extend(api.error, { 
        cantFind:              { code: 423, message: "Error finding tipBanners." },
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationDeleteError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        // params = req.params;
        // user = req.user;

        return new Parse.Query("TipBanner")
            .equalTo("isVisible", true)
            .descending("priority")
            .find()
            .then(function(results) {
                var tipBanners = _.map(results,function(value,index){
                    return {
                        colorScheme:    value.get("colorScheme"),
                        title:          value.get("title"),
                        timeToStay:     value.get("timeToStay"),
						actionText: 	value.get("actionText"),
                        action:         value.get("action"),
                        filter:         value.get("filter") 
                    };
                });

                l.log(true, "Find tip banners:", tipBanners).despatch();
                return {
                    success: true, 
                    message: "Tip banners fetched.",
                    data: tipBanners,
                };
            },function(error){
                l.log(false, "ALERT request user does not exist: ", params.requestUserId).despatch();
                return unsuccessful(error.cantFind);
            });
    };
})();
