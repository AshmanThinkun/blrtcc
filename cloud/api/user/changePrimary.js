var emailJS = require("cloud/email");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var NonUser = require("cloud/nonUser");

// # v1: userChangePrimary
(function () {

    var params;
    var user;

    // ## Enums

    // ### userChangePrimaryStatus
    var userChangePrimaryStatus = { 
        primaryChanged:                     { code: 100, message: "The primary email address was successfully changed and a verification email was sent." },
        primaryChangedNoVerificationEmail:  { code: 101, message: "The primary email address was successfully changed but there was an error when sending the verification email." },
        switchedWithSecondary:              { code: 102, message: "The user owned a secondary email contact with the requested address and that was switched with the current primary contact detail." },
        alreadyUsingTargetEmail:            { code: 110, message: "The requested email is already in use as the primary address for this user." }
    };

    // Aliased as status for simplicity of internal use.
    var status = userChangePrimaryStatus;

    // ### userContactDetailResendVerificationError

    // Subclasses APIError.
    var userChangePrimaryError = _.extend(api.error, { 
        emailInUse: {code: 410, message: "The requested email address is already in use by another user."}
    });

    // Aliased as error for simplicity of internal use.
    var error = userChangePrimaryError;

    // ## Request examples

    //     req = {
    //         user: ParseUser,
    //         device: {
    //             version: appVersion,
    //             device: deviceType,  // i.e. "iPhone"
    //             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
    //             osVersion: deviceOSVersion,
    //             langArray: ["en-GB", "en-US", ...],
    //             locale: "AU"
    //         },
    //         params: {
    //             email: email
    //         }
    //     }

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        params = req.params;
        user = req.user;

        var oldPrimary = user.get(constants.user.keys.username);

        // Don't do anything if they're already using the requested email as their primary.
        if(oldPrimary == params.email)
            return Parse.Promise.as({
                success: true,
                status: status.alreadyUsingTargetEmail
            });

        // 1. Search for the user's contact details and any other conflicting ones.
        return Parse.Query.or(
            new Parse.Query(constants.contactTable.keys.className)
                .equalTo(constants.contactTable.keys.user, user)
                .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                .include(constants.contactTable.keys.user),
            new Parse.Query(constants.contactTable.keys.className)
                .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
                .equalTo(constants.contactTable.keys.Value, params.email)
                .include(constants.contactTable.keys.user)

        ).find({useMasterKey:true})
        .then(function (contacts) {
            var primary;            // Hold the user's current primary contact incase we need to change it's value
            var secondary;          // Hold a secondary contact for the user if it's using the requested email
            var deletables = [];    // Hold all placeholder contacts that use the requested email and need to be deleted
            var inUse = false;      // If we find out that the email is inUse by someone else then we need to bail out

            // 2. Go through all found contacts and put them into their variables.
            _.each(contacts, function(contact) {
                if(inUse)   // We need this because we can't `break` out of `_.each`
                    return;

                if(contact.get(constants.contactTable.keys.Value) == oldPrimary)
                    primary = contact;
                else if(contact.get(constants.contactTable.keys.Value) == params.email.toLowerCase()) {
                    if(contact.get(constants.contactTable.keys.user).id == user.id)
                        secondary = contact;
                    else if(contact.get(constants.contactTable.keys.placeholder) == true)
                        deletables.push(contact);
                    else
                        inUse = true;
                }
            });

            // 3. If we found someone else using this email then return an appropriate error.
            if(inUse)
                return {
                    success: false,
                    error: error.emailInUse
                };


            // 4. If there's anything to delete, then delete them.
            var deletePromise;
            if(deletables.length == 0)
                deletePromise = Parse.Promise.as();
            else
                deletePromise = Parse.Object.destroyAll(deletables,{useMasterKey:true});

            return deletePromise.then(function () {
                // 5. Update the user.
                var verified = secondary == null ? false : secondary.get(constants.contactTable.keys.verified);
                // For some reason this wasn't being set properly...? Didn't have time to properly debug :(
                verified = verified == true ? true : false;

                user.set(constants.user.keys.username, params.email.toLowerCase())
                    .set(constants.user.keys.Email, params.email)
                    .set(constants.user.keys.EmailVerified2, verified)
                    .set(constants.user.keys.ContactInfoDirty, true);

                return user.save(null,{useMasterKey:true}).then(function () {
                    // 6. Follow-up actions:
                    var followUps = [Parse.Promise.as()];

                    //   * Update user's contact details.
                    // If they're not switching then we need to update the existing record with the new email
                    // and make sure that it's not verified.
                    if(secondary == null)
                        followUps.push(
                            primary
                                .set(constants.contactTable.keys.Value, params.email.toLowerCase())
                                .set(constants.contactTable.keys.verified, false) 
                                .save(null,{useMasterKey:true})
                                .fail(function (error) {
                                    l.log("ALERT: Error modifying primary contact detail:", error).despatch();
                                    return Parse.Promise.as();
                                })
                        );
                    // Otherwise, if they are switching with a secondary email, we need to make sure that it's
                    // no longer flagged as a placeholder email (if it was already).
                    else if(secondary.get(constants.contactTable.keys.placeholder) == true)
                        followUps.push(
                            secondary
                                .set(constants.contactTable.keys.placeholder, false)
                                .save(null,{useMasterKey:true})
                                .fail(function (error) {
                                    l.log("ALERT: Error changing placeholder state of secondary (now primary) contact detail:", error).despatch();
                                    return Parse.Promise.as();
                                })
                        );

                    //   * Send verification email.
                    // We need to keep track of whether or not this was successful so we can return an
                    // appropriate response upon conclusion of this API endpoint.
                    var verificationEmailError = false;
                    if(!verified) {
                        // Prepare email variables
                        var customMergeVars = {
                            "BASE_EMAIL": params.email,
                            "EMAIL_VERIFICATION_LINK": constants.protocol + constants.siteAddress + "/vem?" + _.hashedQueryArgs({
                                "coid": _.encryptId(secondary == null ? primary.id : secondary.id),
                                "s": _.salt()
                            })
                        };

                        // Send the email!
                        followUps.push(emailJS.SendTemplateEmail({
                            template:       "email-verification",
                            recipients:     [{
                                name: user.get(constants.user.keys.FullName),
                                email: params.email,
                                isUser: true,
                                user: user
                            }],
                            customMergeVars: customMergeVars
                        }).fail(function (error) {
                            l.log("Error sending verification email:", error).despatch();
                            verificationEmailError = true;
                            return Parse.Promise.as();
                        }));
                    }

                    //   * Manage Mandrill whitelist.
                    // We want to add the new email to the whitelist.
                    followUps.push(
                        Parse.Cloud.httpRequest({
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            url: 'https://mandrillapp.com/api/1.0/whitelists/add.json',
                            body: {
                                key: constants.mandrillKey,
                                email: params.email
                            }
                        })
                    );

                    // We want to remove the old email from the whitelist.
                    followUps.push(
                        Parse.Cloud.httpRequest({
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            url: 'https://mandrillapp.com/api/1.0/whitelists/delete.json',
                            body: {
                                key: constants.mandrillKey,
                                email: oldPrimary
                            }
                        })
                    );
                    //[Guichi] should be a bug, the nonUser association logic should happen the email is change
                    //[Guichi] move it to next promise
                    // //   * Associate with any matching NonUsers.
                    // followUps.push(NonUser.associate(user,false));

                    return Parse.Promise.when(followUps).then(function () {
                        NonUser.associate(user,false)
                        NonUser.updateNoneUser (oldPrimary, primary.get(constants.contactTable.keys.user).id)
                        // 7. Wrapping up.
                        //   * Return an appropriate response.
                        return {
                            success: true,
                            status: verificationEmailError ? 
                                status.primaryChangedNoVerificationEmail :
                                (secondary == null ? status.primaryChanged : status.switchedWithSecondary)
                        };
                    }, function (promiseError) {
                        l.log(false, "Something went wrong with the followUp promises:", promiseError).despatch();
                        return {
                            success: true,
                            status: verificationEmailError ? 
                                status.primaryChangedNoVerificationEmail :
                                (secondary == null ? status.primaryChanged : status.switchedWithSecondary)
                        };
                    });
                }, function (error) {
                    // Error for "5. If there's anything to delete, then delete them".
                    l.log(false, "Error deleting placeholder contacts:", error).despatch();
                    return Parse.Promise.as({
                        success: false,
                        error: error.subqueryError
                    });
                });
            }, function (error) {
                // Error for "4. If there's anything to delete, then delete them".
                l.log(false, "Error deleting placeholder contacts:", error).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
        }, function (error) {
            // Error for "1. Search for the user's contact details and any other conflicting ones".
            l.log(false, "Error searching for user contact details:", error).despatch();
            return Parse.Promise.as({
                success: false,
                error: error.subqueryError
            });
        });
    };

})();
