var constants = require("cloud/constants");
var api = require("cloud/api/util");
var _ = require("underscore");
var utilJS = require("cloud/util");
var ParseMutex = require("cloud/ParseMutex");

// # v1: userBlock
(function () {
    var params;
    var user;

    // ## Enums

    // ### userBlock

    // Subclasses APIError.
    var conversationDeleteError = _.extend(api.error, {
        blockUserNotExist: {code: 420, message: "The requested user cannot be found."},
        requestUserRequired: {code: 421, message: "A requesting user could not be found."},
        blockUserEmailNotExist: {code: 422, message: "The requested email cannot be found"},

    });

    // Block flag
    var blockFlag = {
        block: 1,
        unBlock: 2,
    };

    // Aliased as error for simplicity of internal use.
    var error = conversationDeleteError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        var skipTracking = req.device.device != "www";
        var userPromises = [];

        //check existance for request user and block user

        //if !auth, then requestUserId is required
        if (!user) {
            userPromises.push(
                new Parse.Query("User")
                    .get(params.requestUserId)
                    .then(function (requestUser) {
                            return user = requestUser;
                        }
                    )
            );
        }

        var blockUserId = params.blockUserId;
        var blockUserEmail = params.blockUserEmail;
        var blockUser;

        console.log('email==='+blockUserEmail);

        if (blockUserId != null) {
            userPromises.push(
                new Parse.Query("User")
                    .get(blockUserId)
                    .then(function (result) {
                            return blockUser = result
                        }
                    )
            );
        }
        else if (blockUserEmail != null) {
            userPromises.push(
                new Parse.Query("User")
                    .equalTo("username", blockUserEmail.toLowerCase())
                    .first()
                    .then(function (result) {
                        return blockUser = result;
                    })
            );
        }
        else {
            return unsuccessful(error.blockUserNotExist);
        }


        function DoMkTracking(blocked, blockUser, user, skipTracking) {
            return true;
        }


        //add or remove the block user to blockedUsers array
        return Parse.Promise.when(userPromises).then(function () {
            if (!blockUser) {
                if (blockUserEmail != null)
                    return unsuccessful(error.blockUserEmailNotExist);
                return unsuccessful(error.blockUserNotExist);
            }
            if (!user)
                return unsuccessful(error.requestUserRequired);


            console.log(user)
            console.log(blockUser)

            //use ParseMutex in case concurrency issue for blockedUserFlags object
            return ParseMutex.fnWithLock(user, "userBlockingManagement", function (user) {
                var blockedUserFlags = user.get("blockedUserFlags") || {};

                // Are they unblocking a user? That needs some different logic
                if (params.blockFlag == blockFlag.unBlock) { // 2
                    blockedUserFlags[blockUser.id] = {email: blockUserEmail, name: blockUser.get('name'), flag: blockFlag.unBlock};;
                    console.log('\n=== === ===');

                    return user.remove("blockedUsers", blockUser)
                        .set("blockedUserFlags", blockedUserFlags)
                        .save()
                        .then(function () {
                            return {success: true, message: "Unblocked User: " + blockUserId}
                        }, function (error) {
                            l.log(false, "Error unblocking user: ", blockUserId);
                            return unsuccessful(error.subqueryError);
                        });
                }

                if(blockedUserFlags[blockUser.id] && blockedUserFlags[blockUser.id]['flag'] == blockFlag.block) {
                    l.log(true, "User is already blocked: ", blockUserId);
                    return Parse.Promise.as({success: false, message: "You have already blocked this user."});
                }
                

                blockedUserFlags[blockUser.id] = {email: blockUserEmail, name: blockUser.get('name'), flag: blockFlag.block};

                return user.addUnique("blockedUsers", blockUser)
                    .set("blockedUserFlags", blockedUserFlags)
                    .save()
                    .then(function () {
                        console.log("before return")
                        return {
                            success: true,
                            message: "You have successfully blocked  " + blockUser.get("email") + ". You will no longer receive any Blrts from this user.",
                            blockedId:blockUser.id
                        };
                    }, function (error) {
                        l.log(false, "Error adding blockUser: ", blockUserId);
                        return unsuccessful(error.subqueryError);
                    });
            }, function (error) {
                l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();

                if (_.isDate(promiseError)) {
                    // The mutual exclusion was in use by another calling function - we'll have to try again later.
                    return Parse.Promise.as({success: false, error: error.subqueryError, tryAgainIn: promiseError});
                }

                return Parse.Promise.as({success: false, error: error.subqueryError});
            });
        }, function (promiseError) {
            l.log(false, "Error finding Users").despatch();
            return unsuccessful(error.subqueryError);
        });
    };

})();


//the url can be used to blcok a user, this will be used when send a "add to conversation" email
exports.getBlockUrl = function (requestId, blockId) {
    var r = _.encryptId(requestId);
    var s = _.encryptId(blockId);
    var generatedUrl = "https://" + constants.siteAddress + "/block?";

    generatedUrl += "r=" + encodeURIComponent(r);
    generatedUrl += "&s=" + encodeURIComponent(s);
    return generatedUrl;
};
