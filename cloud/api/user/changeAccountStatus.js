/* global Parse */

var emailJS = require("cloud/email");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var NonUser = require("cloud/nonUser");
var pendingActions = require("cloud/pendingActions");

// # v1: userChangeAccountStatus
(function () {

    // Subclasses APIError.
    var userChangeAccountStatusError = _.extend(api.error, { 
        usernameNotExist: { code: 404, message: "The username doesn't exist" }
    });

    // Aliased as error for simplicity of internal use.
    var error = userChangeAccountStatusError;

    // ## Request examples

    //     req = {
    //         user: ParseUser,
    //         device: {
    //             version: appVersion,
    //             device: deviceType,  // i.e. "iPhone"
    //             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
    //             osVersion: deviceOSVersion,
    //             langArray: ["en-GB", "en-US", ...],
    //             locale: "AU"
    //         },
    //         params: {
    //             from: "app"
    //         }
    //     }

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };
            return Parse.Promise.as(response);
        }

        var l = api.loggler;
        Parse.Cloud.useMasterKey();

        let params = req.params;
        let user = req.user;

        // console.log('\n\n\nuserChangeAccountStatus: '+JSON.stringify(params));
        console.log('\n\nuser user user===');
        console.log(user);

        // promises
        let userQuery = true;

        let nonUsersQuery = Parse.Promise.as();
        let relationsQuery = Parse.Promise.as();
        let userContactsQuery = Parse.Promise.as();

        var objectsToSave = [];
        var conversations = []; // save all converations that the user was in

        let currentTime = new Date().getTime();

        if(params.username) {
            userQuery = new Parse.Query("User")
                .equalTo("username", params.username)
                .first()
                .then(function(theUser) {
                    // todo: consider the case where theUser is not found...
                    if(theUser) {
                        user = theUser; // we must have a user to be deleted, if admin deletes a user, the param.username should be set
                        return true;
                    }else return false;
                    
                });
        }else {
            if(!user) {
                return unsuccessful(error.usernameNotExist);
            }
        }

        return Parse.Promise.when(userQuery)
            .then(function(result) {
                // console.log('\n\n\nuserQuery result: '+JSON.stringify(result));
                // todo: if theUser is not found...
                if(!result) {
                    return {success: false, message: 'User not found'};
                }

                // check user accountStatus
                if(user && user.accountStatus === 'deleted') {
                    return {
                        success: false,
                        message: "User has been deleted"
                    };
                }

                switch(params.accountStatus) {
                    case 'deleted':
                        console.log('\n\n===deleted===');
                        // console.log(user.get('email'));
                        // update User table
                        user.set("preDeletionEmail", user.get("email"))
                            .set("username", "__deleted__"+user.get("username")+"__"+currentTime)
                            .set("email", currentTime+user.get("email"))
                            .set("accountStatus", "deleted")
                            .set("authData", {facebook: null});

                        console.log('\n\nafter user update===');
                        console.log(user);
                        // update NonUser table
                        nonUsersQuery = new Parse.Query("NonUser")
                            .equalTo("user", user)
                            .equalTo("claimed", true)
                            .find({useMasterKey: true})
                            .then(function(nonUsers) {
                                // console.log('\n\n\nbefore nonUsers: '+JSON.stringify(nonUsers));
                                nonUsers = _.map(nonUsers, function(theNonUser) {
                                    return theNonUser.set("value", "__deleted__"+theNonUser.get("value")+"__"+currentTime);
                                    // objectsToSave.push(theNonUser);
                                });
                                // console.log('\n\n\nafter nonUsers: '+JSON.stringify(nonUsers));
                                return nonUsers;
                            },function(error){
                                l.log(false, "Error query nonuser:", error);
                                return unsuccessful(error.subqueryError);
                            });

                        // update ConversationUserRelation table
                        relationsQuery = new Parse.Query("ConversationUserRelation")
                            .equalTo("user", user)
                            .include("conversation")
                            .include("creator")
                            .find({useMasterKey: true})
                            .then(function(relations) {
                                relations = _.map(relations, function(theRelation) {
                                    var theConvo = theRelation.get('conversation');
                                    var theCreator = theRelation.get('creator');
                                    theRelation.set("userHasDeleted", true);
                                    theRelation.set("deletedAt", new Date());
                                    if(user.equals(theCreator)) {
                                        theConvo.set("readonly", true);
                                    }else {
                                        // check bondCount
                                        // console.log('\n\ntheConvo bondCount==='+theConvo.get('bondCount'));
                                        if(theConvo.get('bondCount') >= 2) {
                                            theConvo.increment('bondCount', -1); // leave space for allowing more people to be added in conversation
                                        }
                                    }
                                    conversations.push(theConvo);

                                    // objectsToSave.push(theRelation);
                                    // objectsToSave.push(theConvo);
                                    console.log("MUH LOOP FINISHED");
                                    return theRelation;
                                });
                                return relations;
                            }, function(error) {
                                l.log(false, "Error query relation:", error);
                                return unsuccessful(error.subqueryError);
                            });

                        // update UserContactDetails table    
                        userContactsQuery = new Parse.Query("UserContactDetails")
                            .equalTo("user", user)
                            .find({useMasterKey: true})
                            .then(function(contacts) {
                                contacts = _.map(contacts, function(contact) {
                                    contact.set("value", "__deleted__"+contact.get("value")+"__"+currentTime);
                                    contact.set('contactStatus', params.accountStatus)
                                    return contact;
                                    // objectsToSave.push(contact);
                                });
                                return contacts;
                            }, function(error){
                                l.log(false, "Error query contact:").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                        break;

                    case 'active':
                        user.set("accountStatus", "suspended");
                        // update contactDetail table, so that people can register with that email again
                        userContactsQuery = new Parse.Query("UserContactDetails")
                            .equalTo("user", user)
                            .find()
                            .then(function(contacts) {
                                contacts = _.map(contacts,function(contact) {
                                    contact.unset("contactStatus");
                                    // objectsToSave.push(contact);
                                    return contact
                                });
                                return true;
                            }, function(error){
                                l.log(false, "Error query contact:").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                        break;

                    case 'suspended':
                        user.unset("accountStatus");
                        // update contactDetail table, so that people can register with that email again
                        userContactsQuery = new Parse.Query("UserContactDetails")
                            .equalTo("user", user)
                            .find()
                            .then(function(contacts) {
                                contacts = _.map(contacts, function(contact) {
                                    contact.set("contactStatus", params.accountStatus);
                                    // objectsToSave.push(contact);
                                    return contact;
                                });
                                return true;
                            }, function(error){
                                l.log(false, "Error query contact:").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                        break;
                }
                
                // create AccountStatusLog record
                var accountLog = new Parse.Object("AccountStatusLog")
                    .set("user", user)
                    .set("targetStatus", params.accountStatus)
                    .set("from", params.from)
                    .set("note", params.note);

                // objectsToSave.push(user);
                // objectsToSave.push(accountLogPromise);

                console.log('\n\n===after all queries finished===');

                return Parse.Promise.when(nonUsersQuery, relationsQuery, userContactsQuery)
                    .then(function(nonusers, relations, contacts) {
                        console.log('\n\n\n===after query===');
                        console.log('relations relations relations: '+relations.length);
                        // console.log('nonusers nonusers nonusers: '+JSON.stringify(nonusers));

                        let relationsSavedPromise = [];
                        let contactsSavedPromise = [];
                        let nonusersSavedPromise = [];

                        relations.map(relation=>{
                            relationsSavedPromise.push(relation.save(null, {useMasterKey: true}));
                        });
                        nonusersSavedPromise.map(nonuser=>{
                            nonusersSavedPromise.push(nonuser.save(null, {useMasterKey: true}));
                        });
                        contacts.map(contact=>{
                            contactsSavedPromise.push(contact.save(null, {useMasterKey: true}));
                        });

                        return Parse.Promise.when(relationsSavedPromise, nonusersSavedPromise, contactsSavedPromise, user.save(null, {useMasterKey: true}), accountLog.save(null, {useMasterKey: true}))
                            .then(function(savedResult) {
                                // console.log('\n\n\nafter save data savedResult: '+JSON.stringify(savedResult));
                                let mandrillRemovePromise = Parse.Promise.as(true);
                                // remove user from mandrill whitelisting 
                                if(params.accountStatus == "deleted") {
                                    mandrillRemovePromise = Parse.Cloud.httpRequest({
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/json',
                                        },
                                        url: 'https://mandrillapp.com/api/1.0/whitelists/delete.json',
                                        body: {
                                            key: constants.mandrillKey,
                                            email: user.get("email").toLowerCase()
                                        }
                                    });    
                                }

                                return mandrillRemovePromise.then(function(result) {
                                    // if(result) {
                                    // find associated users, and send silent push notification
                                    return new Parse.Query("ConversationUserRelation")
                                        .containedIn("conversation", conversations)
                                        .notEqualTo("user", user)
                                        .notEqualTo("accountStatus", 'deleted')
                                        .include("user")
                                        .find({useMasterKey: true})
                                        .then(function(relations) {
                                            var users = [];
                                            _.each(relations, function(theRelation){
                                                var theUser = theRelation.get('user');
                                                if(users.indexOf(theUser) < 0 && theUser !== undefined) { // dont input duplicate users
                                                    users.push(theUser);
                                                }
                                                console.log("MUH LOOP FINISHED 2");
                                            });
                                            // send silent push notification
                                               var pushArgs = {
                                                   'user': user.id
                                               };
                                            pendingActions.create(101, pushArgs, users);

                                            // On android, parse SDK has a bug; it fails to parse a payload with empty args object
                                            // Thus add some dummy key, value pair in the args object
                                            if(params.accountStatus != "active") { // send silent push notification to the user to force it logout
                                                pendingActions.create(102, {o:1}, [user]);
                                            }

                                            l.log(true, "change account status success, user: ", user.id).despatch();
                                            return Parse.Promise.as({success: true, message: 'Update user accountStatus success'});
                                        },function(error) {
                                            l.log(false, "Error query relation2: ", error);
                                            return Parse.Promise.as({success: false, message: error});
                                        });
                                    // }
                                }).fail(function (error) {
                                    console.log(error);
                                    l.log(false, "Error remove user from mandrill whitelisting", error).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                            }, function(error) {
                                l.log(false, "Error saving objects").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                    }, function(error) {
                        l.log(false, "Error subquery").despatch();
                        return unsuccessful(error.subqueryError);
                    });
            },function(error) {
                l.log(false, "Error find user:", params.username).despatch();
                return unsuccessful(error.subqueryError);
            });
    };
})();





