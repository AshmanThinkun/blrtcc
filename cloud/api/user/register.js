var emailJS = require("cloud/email");
// var Mixpanel = require("cloud/mixpanel");
// var trackImmediate = require('cloud/analytics-node').trackImmediate;
var constants = require("cloud/constants");
var api = require("cloud/api/util");
var Util = require("cloud/util");
var NonUser = require("cloud/nonUser");
var _ = require("underscore");

// # v1: userRegister

var params;
var user;

// ## Enums

// ### userRegisterStatus

// Used as part of a successful response.
// This is necessary because there are two very different success cases.

// tokenAccountExists is necessary because this same endpoint is used for creation and logging
// in of tokens, and that's because Facebook (and similar) share a sign up and login button.
// The simplest flow for a client is to attempt login after successful registration, but we need
// to let them know when a new account wasn't actually created so they can avoid displaying things
// like welcome messages.
var userRegisterStatus = {
    accountCreated: {code: 100, message: "A new account was created as requested."},
    tokenAccountExists: {code: 110, message: "An account already exists for the provided token."}
};

// ### userRegisterError

// Subclasses APIError.
var userRegisterError = _.extend(api.error, {
    emailInUse: {code: 410, message: "The requested email is already being used as a primary or secondary email."}
});

// Aliased as error for simplicity of internal use.
var error = userRegisterError;

// ## Request examples

// ### Standard signup

//     req = {
//         user: null,
//         device: {
//             version: appVersion,
//             device: deviceType,  // i.e. "iPhone"
//             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
//             osVersion: deviceOSVersion,
//             langArray: ["en-GB", "en-US", ...],
//             locale: "AU"
//         },
//         params: {
//             email: email,
//             password: password,
//             name: text,
//             extra: {
//                 industry: text,
//                 hasAgreedToTOS: true,
//                 referredBy: absdfsdf
//                 signupUrl: fsfsd.fsdf.com?fsd=dfsdf&fdss=ddd
//             }
//         }
//     }

// ### Token (i.e. Facebook) signup

//     req = {
//         user: null,
//         device: {
//             version: appVersion,
//             device: deviceType,  // i.e. "iPhone"
//             os: deviceOS,        // i.e. "iPhone OS" (Apple's weird way of saying iOS)
//             osVersion: deviceOSVersion,
//             langArray: ["en-GB", "en-US", ...],
//             locale: "AU"
//         },
//         params: {
//             email: email,
//             token: authToken,
//             name: text,
//             extra: {
//                 gender: number,
//                 first_name: text,
//                 last_name: text,
//                 fb_id: number,
//                 fb_username: ...,
//                 fb_...: ...,
//                 ...
//             }
//         }
//     }

// ## Flow
exports[1] = function (req) {
    console.log('\n\n\n\nuserRegister===');
    console.log(req.params);

    var l = api.loggler;
    var now = new Date();

    var sessionToken;
    // These'll be accessed around the function.. easiest to declare here.
    var firstName = null;
    var lastName = null;

    params = req.params;

    //return invalid params or continue
    var query1, query2, query;
    if (params.email) {
        query1 = new Parse.Query(constants.contactTable.keys.className)
            .equalTo(constants.contactTable.keys.type, constants.contactTable.types.email)
            .equalTo(constants.contactTable.keys.Value, params.email.toLowerCase());
    }

    // This is necessary even for Facebook accounts because although the Facebook ID may not be in
    // use, the email associated with that account may be in used as a secondary email.

    // On the other hand, if a contact with the same email exists but a contact with a matching
    // Facebook ID also exists, we should still let them through so they can login to their account.
    if (params.token) {
        query2 = new Parse.Query(constants.contactTable.keys.className)
            .equalTo(constants.contactTable.keys.type, constants.contactTable.types.facebook)
            .equalTo(constants.contactTable.keys.Value, params.token.id);
    }

    if (query1 && query2) {
        query = Parse.Query.or(query1, query2);
    } else {
        query = query1 ? query1 : query2;
    }

    return query.find({useMasterKey:true})
        .then(function (results) {
            var matchingEmail;
            var matchingToken;

            _.each(results, function (contact) {
                if (contact.get(constants.contactTable.keys.type) == constants.contactTable.types.email)
                    matchingEmail = contact;
                if (contact.get(constants.contactTable.keys.type) == constants.contactTable.types.facebook)
                    matchingToken = contact;
            });

            if (matchingEmail != null && matchingToken == null) {
                return {
                    success: false,
                    error: userRegisterError.emailInUse
                };
            }
            if (params.token && !matchingToken && !params.email) {
                //fb register needs email
                return {
                    success: false,
                    error: userRegisterError.invalidParameters
                }
            }
            if (params.token && matchingToken) {
                //login with this fb user
                params.email = "fake@email.com";
            }
            var body = {};

            // * Process provided arguments.

            // * setup name
            if (!params.name) {
                params.name = params.email;
                params.hasnotSetDisplayname = true;
                body["hasnotSetDisplayname"] = true;
            }

            //   * Split the name into first and last name.
            var split = params.name.split(" ");
            firstName = body["hasnotSetDisplayname"] ? null : split[0];
            lastName = split.length != 1 ? split[split.length - 1] : null;

            body[constants.user.keys.username] = params.email.toLowerCase();
            body[constants.user.keys.Email] = params.email;
            body[constants.user.keys.FullName] = params.name;

            if (params.token) {
                var token = {};
                token[params.token.target] = {
                    id: params.token.id,
                    access_token: params.token.token,
                    expiration_date: params.token.expiration
                };
                //Parse has some special treatment of 'authData', if authData match existing one, no new user will be created
                //previous programmer really take a good advantage of the feature.
                //refer to RestWrite.prototype.findUsersWithAuthData = function(authData) in RestWrite.js of the open source Parse server
                body[constants.user.keys.authData] = token;

                // We're setting this to true because a token's contact details are considered verified immediately
                // so we can let the client search for and link relations straight away.
                body[constants.user.keys.ContactInfoDirty] = true;
            }

            if (params.password != null) {
                body[constants.user.keys.password] = params.password;
                body[constants.user.keys.hasSetPassword] = !params.isQuickSignUp;
            }
            body[constants.user.keys.queuedConvoTemplates] = [constants.user.introTemplate];
            body["queuedConvoTemplateCount"] = 1; // for checking if user has welcome convo

            //   * And add any other details that was passed to us.
            if (params.extra != null) {
                _.each(params.extra, function (value, key) {
                    if (key != "referredBy")
                        body[key] = value;
                });
            }

            // * Fill in the blanks.
            body[constants.user.keys.AllowEmailComments] = true;
            body[constants.user.keys.AllowEmailNewBlrt] = true;
            body[constants.user.keys.AllowEmailReBlrt] = true;
            body[constants.user.keys.AllowEmailRequest] = true;
            body["signupAppVersion"] = req.device.version;
            body["signupDevice"] = req.device.device;
            body["signupOS"] = req.device.os;
            body["signupOSVersion"] = req.device.osVersion;
            body["lastAppVersion"] = req.device.version;
            body["lastDevice"] = req.device.device;
            body["lastOS"] = req.device.os;
            body["lastOSVersion"] = req.device.osVersion;
            if (req.device.langArray != null) { // We're trusting the API processor to keep it sanitised
                body[constants.user.keys.langArray] = req.device.langArray;
                body["signupLangArray"] = req.device.langArray;
                body["signupLanguage"] = req.device.langArray[0];
                body["lastLangArray"] = req.device.langArray;
                body["lastLanguage"] = req.device.langArray[0];
            }
            if (req.device.locale != null) {
                body[constants.user.keys.locale] = req.device.locale;
                body["signupLocale"] = req.device.locale;
                body["lastLocale"] = req.device.locale;
            }
            body[constants.user.keys.firstName] = firstName;
            if (lastName != null)
                body[constants.user.keys.lastName] = lastName;
            body[constants.user.keys.gender] = 0; // 0 is the undefined gender
            body["relationsDirty"] = true;
            
            // REST request for signup
            var url = Parse.serverURL + "/users/"; 
            console.log('\n\n\nurl url url==='+url);

            return Parse.Cloud.httpRequest({
                method: "POST",
                url: url,
                headers: {
                    'X-Parse-Application-Id': Parse.applicationId,
                    'X-Parse-Master-Key': Parse.masterKey,
                    'Content-Type': "application/json"
                },
                body: JSON.stringify(body)
            }).always(function (response) {
                console.log('response httpRequest===');
                console.log(response);
                if (response.status != 201) {
                    // If someone's logging in with an existing token then we could've got back an
                    // OK 200 response - that's good, just send them off.
                    if (response.status == 200)
                    // We're returning an error to skip all of the other promises and go straight
                    // to returning. This is the case in the other response returns here too.
                        return {
                            success: true,
                            status: userRegisterStatus.tokenAccountExists,
                            sessionToken: response.data.sessionToken,
                        };
                    if (response.status == 400 && response.data.code == 202)
                        return {
                            success: false,
                            error: error.emailInUse
                        };

                    return {
                        success: false,
                        error: error.subqueryError
                    };
                }
                // If we're here then creation is successful and we're holding onto a brand new user!
                user = new Parse.User();
                user.id = response.data.objectId;
                sessionToken = response.data.sessionToken;
                var toSave = [];
                // #### UserContactDetail creation
                var primary;
                primary = new Parse.Object(constants.contactTable.keys.className)
                    .set(constants.contactTable.keys.user, user)
                    .set(constants.contactTable.keys.type, constants.contactTable.types.email)
                    .set(constants.contactTable.keys.Value, params.email.toLowerCase())
                    .setACL(new Parse.ACL(user));

                toSave.push(primary);
                if (params.token) {
                    toSave.push(new Parse.Object(constants.contactTable.keys.className)
                        .set(constants.contactTable.keys.user, user)
                        .set(constants.contactTable.keys.type, params.token.target)
                        .set(constants.contactTable.keys.Value, params.token.id)
                        .set(constants.contactTable.keys.verified, true) // These shouldn't need verification
                        .setACL(new Parse.ACL(user))
                    );
                }

                user.setACL(new Parse.ACL(user))
                if (!params.hasnotSetDisplayname) {
                    user.set("name", params.name)
                        .set("first_name", firstName)
                        .set("last_name", lastName);
                    toSave.push(user);
                }
                if (params.extra && params.extra.referredBy && params.extra.referredBy != "" && !user.get("referredBy")) {
                    var referer = new Parse.Object("User");
                    referer.id = params.extra.referredBy;
                    user.set("referredBy", referer);
                }

                toSave.push(user);

                return Parse.Promise.when(Parse.Object.saveAll(toSave,{useMasterKey:true}))
                //todo after migration,move this to next promise
                    .then(function (result) {
                        console.log('\n\n\nUserContactDetails save result: '+JSON.stringify(result));
                        return NonUser.associate(user, true)
                            .then(function (nonUserResult) {
                                if(!nonUserResult)
                                    return;
                                _.each(nonUserResult, function (unknownObj) {
                                    if (unknownObj && unknownObj.className == "_User" && unknownObj.id == user.id) {
                                        user = unknownObj;
                                    }
                                });    
                                console.log('\n\n\n===success===');                                                                                                                                               
                                return {user: user, nonUser: "nonuser-"+nonUserResult[0].id};
                            }, function (e) {
                                console.log('\n\n\n===failed===');
                                return {user: null, nonUser: null};//true
                            })
                    }).then(function (nonUserClaimingResult) { // nothing ensential up to here, if something go bad in this step, just fine
                        console.log('\n\n\n===nonUserClaimingResult===');
                        console.log(nonUserClaimingResult);
                        // Quickly associate this user with any NonUsers (now that their ContactDetails exist).
                        var emailPromises = [];
                        emailPromises.push(
                            emailJS.SendTemplateEmail({
                                template: "welcome",
                                convo: null,
                                content: null,
                                recipients: [{
                                    id: user.id,
                                    name: params.name,
                                    email: params.email,
                                    isUser: true,
                                    user: user
                                }]
                            }).then(function (result) {
                                console.log("welcome email success " + (new Date() - now) / 1000 + " s")
                            })
                        );


                        // #### Verification email

                        // This had to wait until UserContactDetail creation finished so we can access the new
                        // record's ID (which is required for it to be verified).
                        var verficationPromise = emailJS.SendTemplateEmail({
                            template: "email-verification",
                            convo: null,
                            content: null,
                            recipients: [{
                                id: user.id,
                                name: params.name,
                                email: params.email,
                                isUser: true,
                                user: user
                            }],
                            customMergeVars: {
                                "BASE_EMAIL": params.email,
                                "EMAIL_VERIFICATION_LINK": constants.protocol + constants.siteAddress + "/vem?" + _.hashedQueryArgs({
                                    "coid": _.encryptId(primary.id),
                                    "s": _.salt()
                                })
                            }
                        }).then(function (result) {
                            console.log("verification email success" + (new Date() - now) / 1000 + " s");
                            return Parse.Promise.as();
                        });

                        emailPromises.push(verficationPromise);

                        // #### SetPassword email
                        if (params.isQuickSignUp) {
                            var buf = require('crypto').randomBytes(48);
                            var token = buf.toString('hex');
                            var setPasswordPromise = user.set("passwordResetToken", token)
                                .set("passwordTokenCreateAt", now)
                                .save()
                                .then(function () {
                                    return emailJS.SendTemplateEmail({
                                        template: "email-setup",
                                        convo: null,
                                        content: null,
                                        recipients: [{
                                            id: user.id,
                                            name: params.name,
                                            email: params.email,
                                            isUser: true,
                                            user: user
                                        }],
                                        customMergeVars: {
                                            "LINK": constants.protocol + constants.siteAddress + "/setPassword?token=" + encodeURIComponent(token) + "&id=" + _.encryptId(user.id)
                                        }
                                    })
                                    //todo log error properly
                                        .fail(function (error) {
                                            l.log("email-setup error:", error);
                                            return Parse.Promise.as();
                                        });
                                });

                            emailPromises.push(setPasswordPromise);
                        }

                        var whiteListPromise = Parse.Cloud.httpRequest({
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            url: 'https://mandrillapp.com/api/1.0/whitelists/add.json',
                            body: {
                                key: constants.mandrillKey,
                                email: params.email.toLowerCase()
                            }
                        }).then(function (r) {
                            console.log("whitelist success " + (new Date() - now) / 1000 + " s")
                        })

                        //console.log("\n\n\n\n\n nonUserClaimingResult.nonUser = "+nonUserClaimingResult.nonUser);
                        console.log('\n\nbefore return===');
                        return Parse.Promise.as(
                            //absort all the errors for these unimportant ones
                            {
                                success: true,
                                status: userRegisterStatus.accountCreated,
                                sessionToken: sessionToken,
                                nonUserClaimed: nonUserClaimingResult && nonUserClaimingResult.nonUser
                            }
                        );

                        //absort all the errors for these unimportant ones


                    })
            })
        })

        .fail(function (error) {
            return {
                success: false,
                error: error.subqueryError
            };
        })


};

