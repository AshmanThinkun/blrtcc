const {videoConversationURL, siteAddress, webappServerUrl} = require('../../../config/serverConfig');
const _ = require('underscore');


//we do not use "Blrt" collection for now, "blrt" is actually conversationItem
module.exports = function (blrt) {
    // Blrt in new logic, ConversationItem for backward compatibility params for
    const blrtClass = blrt.constructor.name;
    console.log("=== what typeof data received ?? " + blrtClass);
    let queryBody = {
        id: blrt.id
    };
    const blrtDataPromise = new Promise(function (resolve, reject) {
        // query endpoint, used to identify blrt and check status params for create
        // endpoint, including the necessary data to convert blrt to video
        let blrtData = {
            blrtId: blrt.id,
            touchDataFile: blrt
                .get('touchDataFile')
                .url(),
            audioFile: blrt
                .get('audioFile')
                .url(),
            argument: blrt.get("argument"),
            encryptValue: blrt.get("encryptValue"),
            encryptType: blrt.get("encryptType")
        }
        Parse
            .Object
            .fetchAllIfNeeded(blrt.get("media"))
            .then(function (media) {
                blrtData.media = _.map(media, function (asset) {
                    return {
                        id: asset.id,
                        encryptType: asset.get("encryptType"),
                        encryptValue: asset.get("encryptValue"),
                        mediaFile: asset
                            .get("mediaFile")
                            .url(),
                        mediaFormat: asset.get("mediaFormat"),
                        pageArgs: asset.get("pageArgs")
                    }
                })
                resolve(blrtData)
            })


    })
    const convoId = blrt.get('conversation').id

    return Parse
        .Cloud
        .httpRequest({
            url: `${videoConversationURL}/query-promo`,
            method: "POST",
            headers: {
                "key": "LBRsh3O5Of9LrZ13vISL77ZIGM9hnlsawvpzEFIo",
                "Content-Type": "application/json"
            },
            body: queryBody
        })
        .then(function (checkResult) {
            var status = JSON
                .parse(checkResult.text)
                .status;
            console.log("checking conversion status ******  ..... " + status)

            return blrtDataPromise.then(function (blrtData) {
                let cb = `${siteAddress}/notifyPromoVideoConversionFinished?blrtId=${blrtData.blrtId}&convoId=${convoId}`
                if (status == 0) {
                    return Parse
                        .Cloud
                        .httpRequest({
                            url: `${videoConversationURL}/create-promo-video`,
                            method: "POST",
                            headers: {
                                "key": "LBRsh3O5Of9LrZ13vISL77ZIGM9hnlsawvpzEFIo",
                                "Content-Type": "application/json"
                            },
                            body: _.extend(blrtData,
                                {callback: cb})
                        })
                } else if (status == 1) {
                    return ({success: true, status: 1})
                } else if (status == 2) {
                    try {
                        console.log(JSON.parse(checkResult.text))
                        let url = JSON.parse(checkResult.text).url
                        console.log(url)
                        cb = cb + '&url=' + url;
                        Parse
                            .Cloud
                            .httpRequest({
                                url: cb,
                                method: "GET",
                                headers: {
                                    "key": "LBRsh3O5Of9LrZ13vISL77ZIGM9hnlsawvpzEFIo",
                                    "Content-Type": "application/json"
                                }
                            })

                        return ({success: true, status: 2})
                    } catch (e) {
                        console.log(e)
                    }

                } else {
                    return ({success: false, message: "unknown response from video server"});
                }

            })
                .then(function (creationResult) {
                    return JSON.parse(creationResult.text);
                })

        })
        .fail(function (e) {
            return {success: false, message: JSON.stringify(e)}
        })

}

