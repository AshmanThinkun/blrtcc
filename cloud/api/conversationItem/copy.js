const emailJS = require("cloud/email");
const constants = require("cloud/constants");
const api = require("cloud/api/util");
const _ = require("underscore");
const muteJS = require("cloud/api/conversationUserRelation/mute");
const createMedia = require("cloud/api/mediaAsset").createMedia;
const Blrt = require("cloud/api/models/Blrt");


// # v1: conversationItemCreate
// Aliased as error for simplicity of internal use.
var error = _.extend(api.error, {
    invalidBlrtArg: {
        code: 410,
        message: "The provided Blrt argument is invalid or does not cover each provided media asset."
    }
});

// ## Request examples

// } TODO: Provide examples

// ## Response examples

// } TODO: Provide examples

// ## Flow
exports[1] = function (req) {
    //[Guichi] a range of objects already in DB or ready to save in DB ,the mobile client may need these to determine which is
    //success and which is failed, for instance ,some mediaSet entries are successfully create while the whole function fail for
    //some resaon ,the client can take advantage the successful part
    let savedObjects = [];
    const {params, user, device}=req;
    const {conversationId, items} = params
    const l = api.loggler;


    const now = new Date();
    const expiration = new Date(+now + 15000);
    if (true) {
        const convo = new Parse.Object(constants.convo.keys.className);
        convo.id = conversationId;
        return Parse.Promise.when(convo.fetch(),
            new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.conversation, convo)
                .include("user")
                .find())
            .then(function (convo, relations) {
                let currentUserRelation;
                let othersRelation = [];
                _.each(relations, function (r) {
                    if (r.get("user") && r.get("user").id == user.id) {
                        currentUserRelation = r;
                    } else {
                        othersRelation.push(r)
                    }
                })

                return _.reduce(items, function (memo, i) {
                    let type, item;
                    if (i.blrt) {
                        type = 'blrt';
                        item = i.blrt;
                    } else if (i.comment) {
                        type = 'comment'
                        item = i.comment;
                    }
                    return memo.then(function (preConvo, preRelation, preUser) {
                        if (preConvo && preRelation) {
                            return createItem(type, item, preConvo, preRelation, preUser)
                        } else {
                            return createItem(type, item, convo, currentUserRelation, user)
                        }

                    })
                }, Parse.Promise.as())
                //update all the relations
            }).then(function (convo, relation, user) {
                savedObjects.push(convo, relation, user)
                return Parse.Promise.as({success: true, objects: _.responsify(savedObjects)});
            })


        // const queryExtingMediaPromise=
        // generateItem


        //step1 :matching existing localGuid
        //step2 :add convo lock ,create independent blrt or mediaSet
        //when some item created ,some counters for convo ,relation and user will be updated
        function createItem(type, item, convo, relation, user) {
            console.log("I want to see convo id " + convo.id)
            return new Parse.Query('ConversationItem')
                .equalTo('localGuid', item.localGuid)
                .first()
                .then(function (foundItem) {
                    if (foundItem) {
                        return {success: true}
                    } else {
                        convo
                            .increment(constants.convo.keys.partialItemCreationCount)
                            .increment(constants.convo.keys.generalCount)
                            .increment(constants.convo.keys.readableCount)
                            .add("partialItemCreationTimes", expiration);
                        let updateConvoAndCreationPromise;
                        switch (type) {
                            //create mediaSet document in parse
                            case 'images':
                                updateConvoAndCreationPromise = 'image no implemented yet';
                                break
                            case 'blrt':
                                const {media, arg}=item;
                                convo
                                    .increment(constants.convo.keys.blrtCount, 1)
                                if (!convo.get(constants.convo.keys.ThumbnailFile)) {
                                    convo.set(constants.convo.keys.ThumbnailFile, item.thumbnail)
                                }
                                updateConvoAndCreationPromise = createMedia({
                                    media,
                                    arg
                                }, user, savedObjects, device).then(function (createMediaResult) {
                                    const {encryptType, encryptValue, audio, touch} =item.files;
                                    const media = createMediaResult.foundAndCreatedMedia
                                    const args = createMediaResult.dbArg
                                    const thumbnail = item.thumbnail
                                    return Parse.Promise.when(convo.save(), Parse.Promise.as(createMediaResult), Blrt.createBlrt(encryptType, encryptValue, media, args, audio, touch, thumbnail))
                                })
                                break;
                            case 'comment':
                                updateConvoAndCreationPromise = convo.increment(constants.convo.keys.commentCount, 1).save()
                                break;

                            case 'audio':
                                updateConvoAndCreationPromise = 'audio not implemented yet'
                                break;
                        }
                        return updateConvoAndCreationPromise
                            .then(function (convo, media, blrt) {
                                const roleACL = new Parse.ACL();
                                roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                                roleACL.setRoleWriteAccess("Conversation-" + convo.id, true);

                                let hasMadeComment = false;
                                let hasMadeBlrt = false;
                                let lastArg
                                let lastMedia

                                const object = new Parse.Object(constants.convoItem.keys.className)
                                    .set(constants.base.keys.version, constants.convoItem.version)
                                    .set(constants.base.keys.appVersion, req.device.version)
                                    .set(constants.base.keys.device, req.device.device)
                                    .set(constants.base.keys.os, req.device.os)
                                    .set(constants.base.keys.osVersion, req.device.osVersion)
                                    .set(constants.base.keys.creator, user)
                                    .set(constants.convoItem.keys.conversation, convo)
                                    .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) - 1)
                                    .set(constants.convoItem.keys.readableIndex, convo.get(constants.convo.keys.readableCount) - 1)
                                    // a pointer to parent conversation,used to decide if an item is read or unread
                                    .set(constants.convoItem.keys.psuedoId, convo.id + "-" + (convo.get(constants.convo.keys.readableCount) - 1));

                                if (convo.get("conversationRole") != null)
                                    object.setACL(roleACL);

                                switch (type) {
                                    case 'images':
                                        object
                                            .set(constants.convoItem.keys.type, constants.convoItem.types.images)
                                            .set(constants.base.keys.localGuid, item.localGuid)
                                            .set(constants.convoItem.keys.thumbnail, item.thumbnail)
                                            .set(constants.base.keys.encryptType, item.files.encryptType)
                                            .set(constants.base.keys.encryptValue, item.files.encryptValue)
                                        break;
                                    case 'blrt':
                                        lastArg = JSON.stringify(media.dbArg);
                                        lastMedia = media.foundAndCreatedMedia;
                                        hasMadeBlrt = true;
                                        object
                                            .set(constants.convoItem.keys.Media, lastMedia)
                                            .set(constants.convoItem.keys.Argument, lastArg)
                                            .set(constants.convoItem.keys.type, constants.convoItem.types.reBlrt)
                                            .set(constants.base.keys.localGuid, item.localGuid)
                                            .set(constants.base.keys.encryptType, item.files.encryptType)
                                            .set(constants.base.keys.encryptValue, item.files.encryptValue)
                                            .set(constants.convoItem.keys.AudioFile, item.files.audio)
                                            .set(constants.convoItem.keys.TouchDataFile, item.files.touch)
                                            .set(constants.convoItem.keys.thumbnail, item.thumbnail)
                                            .set('blrt', blrt);

                                        break;
                                    case 'comment':
                                        hasMadeComment = true;
                                        object
                                            .set(constants.convoItem.keys.type, 3)
                                            .set(constants.base.keys.localGuid, item.localGuid)
                                            .set(constants.convoItem.keys.Argument, JSON.stringify({"Comment": item.text}));
                                        break;
                                    case 'audio':
                                        break;
                                }
                                return Parse.Promise.when(object.save(), {lastArg, lastMedia})
                            })
                            .then(function (convo, media) {
                                convo
                                    .increment(constants.convo.keys.partialItemCreationCount, -1)
                                    .remove("partialItemCreationTimes", expiration)
                                    .set(constants.convo.keys.ContentUpdate, now);
                                if (media != null) {
                                    const {lastArg, lastMedia} = media
                                    convo
                                        .set(constants.convo.keys.LastMedia, lastMedia)
                                        .set(constants.convo.keys.LastMediaArgument, lastArg);
                                }

                                if (
                                    convo.get(constants.convo.keys.HasConversationBegan) != true &&
                                    user.id != convo.get(constants.base.keys.creator).id
                                ) {
                                    l.log("The conversation has now begun!");
                                    convo.set(constants.convo.keys.HasConversationBegan, true);
                                }

                                // * Mark and update relation as appropriate (hasMadeComment/Blrt...).
                                // if (hasMadeComment && relation.get(constants.convoUserRelation.keys.HasComment) != true)
                                //     relation.set(constants.convoUserRelation.keys.HasComment, true);
                                //
                                // if (hasMadeBlrt && relation.get(constants.convoUserRelation.keys.HasMadeReBlrt) != true)
                                //     relation.set(constants.convoUserRelation.keys.HasMadeReBlrt, true);


                                // If it was blank before then be sure not to add a comma!
                                relation.set(
                                    constants.convoUserRelation.keys.indexViewedList,
                                    (
                                        !relation.get(constants.convoUserRelation.keys.indexViewedList) ? ""
                                            : relation.get(constants.convoUserRelation.keys.indexViewedList) + ","
                                    ) +
                                    (convo.get(constants.convo.keys.readableCount) - (1)) +
                                    (1 > 1 ? "-" + (convo.get(constants.convo.keys.readableCount) - 1) : "")
                                ).set(constants.convoUserRelation.keys.lastEdited, now);

                                relation.set("contentUpdate", now);
                                user
                                    .increment(constants.user.keys.blrtCreationCounter, 1)
                                    .increment(constants.user.keys.commentCreationCounter, 1);


                                return Parse.Promise.when(convo.save(), relation.save(), user.save())
                            })
                    }

                })
        }


    }


    function unsuccessful(error) {
        let response = {
            success: false,
            error: error
        };
        if (savedObjects.length != 0)
            response.objects = _.responsify(savedObjects);
        return Parse.Promise.as(response);
    }


    // The flow of creation can be split into 5 major steps:
    // 1. Find the conversation, relation, and all objects with matching local GUIDs;
    // 2. Create all media, verify arguments and increment conversation counter (and dirty counter);
    // 3. Substitute media IDs, create all items;
    // 4. Reduce conversation dirty counter, update convo and update user/relation as appropriate (hasMadeComment, etc); and
    // 5. Send push notifications, Blrt emails and Mixpanel events.
    // All creation in a single step can be combined into a single saveAll.

    if (false) {

        // ### Step 1: Matching local GUIDs
        var itemGuids = [];                                 // ConversationItem GUID
        var createMediaPromise = Parse.Promise.as()
        _.each(items, function (item) {
            if (item.blrt || item.images) {
                createMediaPromise = createMedia(_.reduce(item.blrt, function (result, value, key) {
                    //we only need 'arg' and 'media' in order to generate mediaSet
                    if (key === 'arg' || key === 'media') {
                        result[key] = value;
                    }
                    return result;
                }, {}), user, savedObjects, device)
                if (item.blrt) {
                    itemGuids.push(item.blrt.localGuid);
                } else if (item.images) {
                    itemGuids.push(item.images.localGuid);
                }

            } else if (item.audio) {
                itemGuids.push(item.audio.localGuid)
            }
            else if (item.comment) // comment todo audio and image
            {
                itemGuids.push(item.comment.localGuid);
            }

        });


        // We have to create a fake conversation object now so we can use it in the
        // query to find the convoUserRelation... because Parse won't compare a string ID.
        var convo = new Parse.Object(constants.convo.keys.className);
        convo.id = params.conversationId;

        return Parse.Promise.when(
            convo.fetch(),
            //query all the relation in the conversation to notify everyone in the conversation
            new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.conversation, convo)
                .include("user")
                .find(),
            new Parse.Query(constants.convoItem.keys.className)
                .containedIn(constants.base.keys.localGuid, itemGuids)
                .find(),
            createMediaPromise
        ).then(function (convo, relations, items, createMediaResult) {
            // We'll grab the user's relation here ^_^
            var relation;
            _.each(relations, function (object) {
                if (object.get("user") && object.get("user").id == user.id)
                    relation = object;
            });


            // If we couldn't find the conversation or relation then we've got a problem... // is this ever the case ?
            if (convo == null || relation == null) {
                l.log(false, "Convo or relation is null - can't create items, convo == null?", convo == null).despatch();
                return unsuccessful(error.subqueryError);
            }

            // Lets first go through all the items and cull what we don't need to create.
            var foundItems = _.map(items, function (item) {
                savedObjects.push(item);
                return item.get(constants.base.keys.localGuid);
            });

            // Subtract what was found from what's left to be created.
            //todo more conversationItem type comes code should be changed here

            params.items = _.reject(params.items, function (item) {
                let localGuid;
                if (item.blrt) {
                    localGuid = item.blrt.localGuid;
                } else if (item.comment) {
                    localGuid = item.comment.localGuid;
                } else if (item.images) {
                    localGuid = item.images.localGuid
                } else if (item.audio) {
                    localGuid = item.audio.localGuid
                }
                return _.contains(foundItems, localGuid);
            });

            // If we found everything they were asking for then we can return success right now! Awesome!
            if (params.items.length == 0) {
                l.log(true).despatch();
                return {success: true, objects: _.responsify(savedObjects)};
            }

            // * Increment conversation counters.
            var newBlrts = 0;
            var newComments = 0;
            // var newImages = 0
            //todo more type will come
            var blrtPromise = Parse.Promise.as()
            _.each(params.items, function (item) {
                if (item.blrt) {
                    newBlrts++;
                    if (!convo.get(constants.convo.keys.ThumbnailFile)) {
                        convo.set(constants.convo.keys.ThumbnailFile, item.blrt.thumbnail)
                    }
                    var {encryptionType, encryptionValue, audio, touch} =item.blrt.files;
                    var media = createMediaResult.foundAndCreatedMedia
                    var args = createMediaResult.dbArg
                    var thumbnail = item.blrt.thumbnail
                    blrtPromise = Blrt.createBlrt(encryptionType, encryptionValue, media, args, audio, touch, thumbnail)

                } else if (item.images) {
                    // newImages++;
                }
                else {
                    newComments++;
                }

            });


            convo
                .increment(constants.convo.keys.generalCount, newBlrts + newComments)
                .increment(constants.convo.keys.readableCount, newBlrts + newComments)
                .increment(constants.convo.keys.commentCount, newComments)
                .increment(constants.convo.keys.blrtCount, newBlrts)
                //At this time point,new blrt object is constructed but not saved in db, actually ,NO operation happen
                //in DB at all,so use 'partialItemCreationCount' to mark this conversation 'dirty'
                .increment(constants.convo.keys.partialItemCreationCount, 1)
                .add("partialItemCreationTimes", expiration);

            return Parse.Promise.when(
                blrtPromise,
                convo.save()).then(function (rawBlrt) {
                // ### Step 3: Create all items.
                var newItems = [];

                var hasMadeComment = false;
                var hasMadeBlrt = false;

                // These need to be held on to so we can update the conversation at the end.
                var lastArg;
                var lastMedia;

                var roleACL = new Parse.ACL();
                roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                roleACL.setRoleWriteAccess("Conversation-" + convo.id, true);


                _.each(params.items, function (item, i) {
                    var object = new Parse.Object(constants.convoItem.keys.className)
                        .set(constants.base.keys.version, constants.convoItem.version)
                        .set(constants.base.keys.appVersion, req.device.version)
                        .set(constants.base.keys.device, req.device.device)
                        .set(constants.base.keys.os, req.device.os)
                        .set(constants.base.keys.osVersion, req.device.osVersion)
                        .set(constants.base.keys.creator, user)
                        .set(constants.convoItem.keys.conversation, convo)
                        //todo do not forget to test the index completely
                        .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) - 1)
                        .set(constants.convoItem.keys.readableIndex, convo.get(constants.convo.keys.readableCount) - 1)
                        // a pointer to parent conversation,used to decide if an item is read or unread
                        .set(constants.convoItem.keys.psuedoId, convo.id + "-" + (convo.get(constants.convo.keys.readableCount) - 1));

                    if (convo.get("conversationRole") != null)
                        object.setACL(roleACL);

                    //todo more type will come
                    if (item.comment) {
                        hasMadeComment = true;
                        object
                            .set(constants.convoItem.keys.type, 3)
                            .set(constants.base.keys.localGuid, item.comment.localGuid)
                            .set(constants.convoItem.keys.Argument, JSON.stringify({"Comment": item.comment.text}));
                    }
                    else if (item.blrt ) {
                        lastArg = JSON.stringify(createMediaResult.dbArg);
                        lastMedia = createMediaResult.foundAndCreatedMedia;
                        object
                            .set(constants.convoItem.keys.Media, lastMedia)
                            .set(constants.convoItem.keys.Argument, lastArg)
                        if (item.blrt) {
                            hasMadeBlrt = true;
                            object
                                .set(constants.convoItem.keys.type, constants.convoItem.types.reBlrt)
                                .set(constants.base.keys.localGuid, item.blrt.localGuid)
                                .set(constants.base.keys.encryptType, item.blrt.files.encryptType)
                                .set(constants.base.keys.encryptValue, item.blrt.files.encryptValue)
                                .set(constants.convoItem.keys.AudioFile, item.blrt.files.audio)
                                .set(constants.convoItem.keys.TouchDataFile, item.blrt.files.touch)
                                .set(constants.convoItem.keys.thumbnail, item.blrt.thumbnail)
                                .set('blrt', rawBlrt);

                            item.blrt.dbArg = createMediaResult.dbArg;
                        } else if (item.images) {
                            object
                                .set(constants.convoItem.keys.type, constants.convoItem.types.images)
                                .set(constants.base.keys.localGuid, item.images.localGuid)
                                .set(constants.convoItem.keys.thumbnail, item.images.thumbnail)
                                .set(constants.base.keys.encryptType, item.images.files.encryptType)
                                .set(constants.base.keys.encryptValue, item.images.files.encryptValue)
                        }
                    }
                    else if (item.audio) {
                        object
                            .set(constants.convoItem.keys.type, constants.convoItem.types.audio)
                            .set(constants.base.keys.localGuid, item.audio.localGuid)
                            .set(constants.base.keys.encryptType, item.audio.files.encryptType)
                            .set(constants.base.keys.encryptValue, item.audio.files.encryptValue)
                            .set(constants.convoItem.keys.AudioFile, item.audio.files.audio)

                    }


                    item.object = object;
                    newItems.push(object);
                });

                return Parse.Object.saveAll(newItems).then(function (parseReturnedSavedObj) {
                    savedObjects = _.union(savedObjects, newItems);

                    // ### Step 4: Reduce conversation dirty counter and update convo, relation and user
                    // In this stage the conversationItem (including Blrt is successfully created)
                    //todo need to be modify for new type
                    convo
                        .increment(constants.convo.keys.partialItemCreationCount, -1)
                        .remove("partialItemCreationTimes", expiration)
                        .set(constants.convo.keys.ContentUpdate, now);

                    if (lastArg != null)
                        convo
                            .set(constants.convo.keys.LastMedia, lastMedia)
                            .set(constants.convo.keys.LastMediaArgument, lastArg);

                    // We also want to specify whether or not the conversation has begun.
                    // We define the conversation's beginning as when someone OTHER than the conversation creator
                    // creates either a Blrt or Comment.
                    if (
                        convo.get(constants.convo.keys.HasConversationBegan) != true &&
                        user.id != convo.get(constants.base.keys.creator).id
                    ) {
                        l.log("The conversation has now begun!");
                        convo.set(constants.convo.keys.HasConversationBegan, true);
                    }

                    // * Mark and update relation as appropriate (hasMadeComment/Blrt...).
                    if (hasMadeComment && relation.get(constants.convoUserRelation.keys.HasComment) != true)
                        relation.set(constants.convoUserRelation.keys.HasComment, true);

                    if (hasMadeBlrt && relation.get(constants.convoUserRelation.keys.HasMadeReBlrt) != true)
                        relation.set(constants.convoUserRelation.keys.HasMadeReBlrt, true);

                    // Also update the relation's indexViewedList to mark all of the newly created items as read.
                    // We'll just append them to the end (after a new comma), and if more than one item was added
                    // we'll easily hyphenate it to represent everything in between too.

                    // If it was blank before then be sure not to add a comma!
                    relation.set(
                        constants.convoUserRelation.keys.indexViewedList,
                        (
                            !relation.get(constants.convoUserRelation.keys.indexViewedList) ? ""
                                : relation.get(constants.convoUserRelation.keys.indexViewedList) + ","
                        ) +
                        (convo.get(constants.convo.keys.readableCount) - (newBlrts + newComments)) +
                        (newBlrts + newComments > 1 ? "-" + (convo.get(constants.convo.keys.readableCount) - 1) : "")
                    ).set(constants.convoUserRelation.keys.lastEdited, now);

                    relation.set("contentUpdate", now);

                    // An hour from now is when comment emails should send to users unless they view the conversation.
                    var hourFromNow = new Date(+new Date() + constants.msForCommentEmail);

                    // We also need to update each other relationship to ensure that their updatedAt is set to
                    // now (because that's the only way we can query for the most recent conversations for a user
                    // because Parse doesn't allow us to sort on an included object's fields).
                    var pushRecipients = [];
                    relations = _.reject(relations, function (object) {
                        // Don't work on the user's own relation here.
                        if (object.id == relation.id)
                            return true;

                        if (object.get("archived"))
                            object.unset("archived");

                        if (object.get("user")) {
                            // The creator gets some special treatment.
                            if (object.get("user").id == convo.get("creator").id) {
                                if (convo.get("hasConversationBegan") == true && !object.get("allowInInbox"))
                                    object.set("allowInInbox", true);
                            }

                            //todo it will override the old one ,give cloud job hint to send email
                            // Only set flaggedForCommentEmailAt if they're a user.
                            object.set("flaggedForCommentEmailAt", hourFromNow);

                            // We'll also hold onto the user so we can send them a push notification.
                            pushRecipients.push(object.get("user"));
                        }
                        object.set("contentUpdate", now);

                        return false;
                    });

                    // * Update user counters - statistics are the best!
                    user
                        .increment(constants.user.keys.blrtCreationCounter, newBlrts)
                        .increment(constants.user.keys.commentCreationCounter, newComments);

                    var beforeNotificationPromises = [];
                    var pushRecipientMuteFlags = {};

                    // get muteFlags so that we will only send email and push notification to the user who have not mute the convo
                    beforeNotificationPromises.push(
                        muteJS.getMuteFlags(pushRecipients, convo.id).then(function (flags) {
                            return pushRecipientMuteFlags = flags;
                        })
                    );

                    beforeNotificationPromises.push(
                        Parse.Object.saveAll(_.union([convo, relation, user], relations))
                    );

                    return Parse.Promise.when(beforeNotificationPromises)
                        .then(function () {

                            console.log("==== how the pushRecipientMuteFlags looks like  ??? ")
                            console.log(JSON.stringify(pushRecipientMuteFlags))

                            savedObjects.push(convo);
                            savedObjects.push(relation);
                            savedObjects.push(user);

                            var waitables = [];
                            // var haveHandledPioneer = false;
                            // ### Step 5: Send push notifications, Blrt emails and Mixpanel events

                            var pushRecipientsShow = [];
                            var pushRecipientsHide = [];
                            _.each(pushRecipients, function (recipient) {
                                if (muteJS.shouldMute(pushRecipientMuteFlags[recipient.id], muteJS.MuteEnum.push)) {
                                    pushRecipientsHide.push(recipient);
                                } else {
                                    pushRecipientsShow.push(recipient);
                                }
                            });

                            var emailRecipients = emailJS.ParseUsersToEmails(pushRecipients, emailJS.ALLOW_EMAIL_REBLRT, pushRecipientMuteFlags);


                            _.each(params.items, function (item, itemIndexInParam) {
                                // * Build and send the push notification.
                                // We build the push giving it the related conversation and the relation creator's information.
                                //todo more types comes
                                var messageShow = emailJS.GeneratePush(item.blrt ? "PB" : "PC", {
                                    t: 1,
                                    bid: convo.id,
                                    s: 1
                                }, [
                                    user.get("name"),
                                    convo.get(constants.convo.keys.Name)
                                ]);

                                //send push notification but don't show alert
                                var messageHide = emailJS.StandardizePushMessage({
                                    t: 1,
                                    bid: convo.id,
                                    s: 0
                                });

                                // We send it to all devices owned by receiving users.
                                if (pushRecipientsShow.length > 0) {
                                    waitables.push(Parse.Push.send({
                                        where: new Parse.Query(Parse.Installation)
                                            .containedIn("owner", pushRecipientsShow),
                                        //todo desire information can not work ,but something random can work
                                        data: messageShow
                                    }, {useMasterKey: true}).then(function (result) {
                                        console.log(result)
                                    }).fail(function (error) {
                                        l.log("ALERT: Error sending unhidden push notifications:", error).despatch();
                                    }));
                                }

                                if (pushRecipientsHide.length > 0) {
                                    waitables.push(Parse.Push.send({
                                        where: new Parse.Query(Parse.Installation)
                                            .containedIn("owner", pushRecipientsHide),
                                        data: messageHide
                                    }, {useMasterKey: true}).then(function (result) {
                                        console.log(result)
                                    }).fail(function (error) {
                                        l.log("ALERT: Error sending new conversation item push notifications:", error).despatch();
                                    }));
                                }

                                // * Next send out emails.
                                // We only do this immediately for Blrts. Comment emails are covered by a job sendCommentNotifications
                                if (item.blrt) {
                                    var blrtInfo = null;
                                    if (parseReturnedSavedObj) {
                                        var thisBlrt = parseReturnedSavedObj[itemIndexInParam];
                                        if (thisBlrt && thisBlrt.get("touchDataFile") && thisBlrt.id) {
                                            blrtInfo = thisBlrt;
                                        }
                                    }
                                    if (emailRecipients.length > 0) {
                                        waitables.push(emailJS.SendTemplateEmail({
                                            template: constants.mandrillTemplates.blrtReply,
                                            convo: convo,
                                            blrt: blrtInfo,
                                            content: item.object,
                                            recipients: emailRecipients,
                                            sender: user,
                                            creator: user
                                        }).fail(function (error) {
                                            l.log("ALERT: Error sending new Blrt Reply emails:", error).despatch();
                                        }));
                                    }
                                } else {
                                    //do nothing
                                }
                            });

                            // waitables.push(m.despatch());

                            return Parse.Promise.when(waitables).always(function () {
                                return Parse.Promise.as({success: true, objects: _.responsify(savedObjects)});
                            });
                        }, function (error) {
                            l.log(false, "Error when decrementing convo partialItemCreationCount:", error).despatch();
                            return unsuccessful(error.subqueryError);
                        });
                }, function (errors) {
                    l.log(false, "Error(s) when creating convoItems or updating relation:", errors).despatch();
                    return unsuccessful(error.subqueryError);
                });
            }, function (errors) {
                l.log(false, "Error(s) when incrementing convo counters or creating media:", errors).despatch();
                return unsuccessful(error.subqueryError);
            });
        }, function (errors) {
            l.log(false, "Error(s) when searching for matching local GUIDs:", errors).despatch();
            return unsuccessful(error.subqueryError);
        });
    }

};
