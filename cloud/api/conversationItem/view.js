var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
// var utilJS = require("cloud/util");
// var Mixpanel = require("cloud/mixpanel");


// # v1: conversationItemView
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationItemView

    // Subclasses APIError.
    var conversationItemViewError = _.extend(api.error, { 
        isPublicBlrt:              	    { code: 620, message: "This is an invalid url." },
        notBlrt:                        { code: 621, message: "This is not a Blrt"}
    });

   
    // Aliased as error for simplicity of internal use.
    var error = conversationItemViewError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        var blrtId = params.blrtId;

        //get conversationItem
        return new Parse.Query("ConversationItem")
            .equalTo("objectId", blrtId)
            .include("media")
            .first()
            .then(function (blrtItem) {
                if(! blrtItem){
                    l.log(false, "Conversation item id not found: ", blrtId).despatch();
                    return unsuccessful(error.notBlrt);
                }
				
                //check whether this is a blrt
                if(!blrtItem.get("media")){
                    l.log(false, "Requesting blrt id is not a blrt: ", blrtId).despatch();
                    return unsuccessful(error.notBlrt);
                }

				if(blrtItem.get("isPublicBlrt")){
                    l.log(false, "Requesting blrt id is a public Blrt: ", blrtId).despatch();
                    return unsuccessful(error.isPublicBlrt);
                }

                return blrtItem.increment("privateViewCounter")
                    .save()
                    .then(function(){
                        l.log(true,"Private view counter incresed ", blrtId).despatch();
                        return {success: true, message: "Private view counter incresed "};
                    },function(error){
                        l.log(false, "Error saving blrtItem:", blrtId).despatch();
                        return unsuccessful(error.subqueryError);
                    });
            }, function(error){
                l.log(false, "Error finding conversation item: " + blrtId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();
