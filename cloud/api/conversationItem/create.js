const emailJS = require("cloud/email");
const constants = require("cloud/constants");
// process.env.DEBUG="*" var debug = require('debug')('ConversationItemCreate::
// ')
const api = require("cloud/api/util");
const _ = require("underscore");
const muteJS = require("cloud/api/conversationUserRelation/mute");
const Blrt = require("cloud/api/models/Blrt");
const MediaAsset = require("cloud/api/models/MediaAsset");
const convertBlrtToVideo = require("cloud/api/shared/convertVideo");

// ## Request examples } TODO: Provide examples ## Response examples } TODO:
// Provide examples ## Flow The flow of creation can be split into 3 major
// steps:
// 1. Find the conversation, all the relations from the conversation
// 2. Create all the item as well as update the counter for conversation and
// user and relation of that user
// 3. Update all the relations and send push notifications, Blrt emails. After
// step 2 ,the response are already sent to client ,step 3 will be performed
// asynchronously ,because they are not that enssential
exports[1] = function (req) {
    // [Guichi] a range of objects already in DB or created in DB by this cloud
    // function, the mobile client need these to determine to which extent the cloud
    // function success The original author may reckon that even if the function
    // fail as a whole ,there may still be some operations completed successfully
    let savedObjects = [];
    const {params, user, device} = req;
    const {conversationId, items} = params
    const l = api.loggler;

    const now = new Date();
    const hourFromNow = new Date(+ new Date() + constants.msForCommentEmail);
    const expiration = new Date(+ now + 15000);
    //# step 1 Find the conversation, all the relations from the conversation
    const convo = new Parse.Object(constants.convo.keys.className);
    convo.id = conversationId;
    return Parse
        .Promise
        .when(convo.fetch({useMasterKey: true}), new Parse.Query(constants.convoUserRelation.keys.className).equalTo(constants.convoUserRelation.keys.conversation, convo).include("user").find({useMasterKey: true}))
        .then(function (convo, relations) {
            console.log('\n\nrelations length==='+relations.length);
            // the relation of the current user ,we need to set indexViewedList in the
            // following steps
            let currentUserRelation;
            // all the other relations except for the current relation ,we need to set
            // contentUpdate ,cause the order of inbox query is displayed by this
            let othersRelations = [];
            _.each(relations, function (r) {
                if (r.get("user") && r.get("user").id == user.id) {
                    currentUserRelation = r;
                } else {
                    othersRelations.push(r)
                }
            })

            // create each requested items in sequence by "reducing" the promises. Actually
            // ,the webapp can never send multiple item in one request,mobile app may send
            // such batch request when the Internet is lost and come back again
            return _.reduce(items, function (memo, i) {
                let type,
                    item;
                if (i.blrt) {
                    type = 'blrt';
                    item = i.blrt;
                } else if (i.comment) {
                    type = 'comment'
                    item = i.comment;
                } else if (i.images) {
                    type = 'images';
                    item = i.images;
                } else if (i.audio) {
                    type = 'audio';
                    item = i.audio;
                }
                return memo.then(function (preConvo, preRelation, preUser) {
                    console.log('\n\npreRelation: '+JSON.stringify(preRelation));
                    if (preConvo && preRelation) {
                        return createItem(type, item, preConvo, preRelation, preUser).then(function (convo, relation, user, createdItem) {
                            updateRelationsAndSendNotifications(othersRelations, type, convo, createdItem)
                            return Parse
                                .Promise
                                .when(convo, relation, user, createdItem)
                        })
                    } else {
                        return createItem(type, item, convo, currentUserRelation, user).then(function (convo, relation, user, createdItem) {
                            updateRelationsAndSendNotifications(othersRelations, type, convo, createdItem)
                            return Parse
                                .Promise
                                .when(convo, relation, user, createdItem)
                        })
                    }
                })
            }, Parse.Promise.as())
            //update all the relations
            //todo maybe multiple items in some rare cases
        })
        .then(function (convo, relation, user, item) {
            savedObjects.push(convo, relation, user, item)
            return Parse
                .Promise
                .as({
                    success: true,
                    objects: _.responsify(savedObjects)
                });
        }, function (error) {
            return unsuccessful(error)
        })

    /**
     * # step 3.1 : Update relations and prepare for muteflags
     * # step 3.2 : Send push notification and email

     * Actually this function will be call even after the the client receive the response
     * @param {[Parse.Object]} relations - other relations need to be update
     * @param {string} type - type of conversationItem
     * @param {Parse.Object} convo - some information is needed for push notification
     * @param {Parse.Object} blrt - email need some information from the blrt
     **/
    function updateRelationsAndSendNotifications(relations, type, convo, blrt) {
        console.log('\n\n=== updateRelationsAndSendNotifications ===');
        const pushRecipients = [];
        const savedRelations = relations.map(function (relation) {
            console.log('===savedRelations user==='+relation.get("user"));
            if (relation.get("user")) {
                // The creator gets some special treatment.
                if (relation.get("user").id == convo.get("creator").id && !relation.get("allowInInbox")) {
                    relation.set("allowInInbox", true);
                }
                if (relation.get("archived")) {
                    relation.unset("archived");
                }
                // it will override the old one ,give cloud job hint to send email Only set
                // flaggedForCommentEmailAt if they're a user.
                relation.set("flaggedForCommentEmailAt", hourFromNow);
                // We'll also hold onto the user so we can send them a push notification.
                pushRecipients.push(relation.get("user"));
            }
            relation.set("contentUpdate", now);
            return relation.save(null, {useMasterKey: true})
        })

        return Parse
            .Promise
            .when(muteJS.getMuteFlags(pushRecipients, convo.id), ...savedRelations)
            .then(function (pushRecipientMuteFlags) {
                const pushRecipientsShow = [];
                const pushRecipientsHide = [];
                _.each(pushRecipients, function (recipient) {
                    if (muteJS.shouldMute(pushRecipientMuteFlags[recipient.id], muteJS.MuteEnum.push)) {
                        pushRecipientsHide.push(recipient);
                    } else {
                        pushRecipientsShow.push(recipient);
                    }
                });
                const emailRecipients = emailJS.ParseUsersToEmails(pushRecipients, emailJS.ALLOW_EMAIL_REBLRT, pushRecipientMuteFlags);

                let messageShow
                switch (type) {
                    case 'blrt':
                        messageShow = emailJS.GeneratePush("PB", {
                            t: 1,
                            bid: convo.id,
                            s: 1
                        }, [
                            user.get("name"),
                            convo.get(constants.convo.keys.Name)
                        ]);
                        break;
                    case 'comment':
                        messageShow = emailJS.GeneratePush("PC", {
                            t: 1,
                            bid: convo.id,
                            s: 1
                        }, [
                            user.get("name"),
                            convo.get(constants.convo.keys.Name)
                        ]);
                        break;
                    case 'images':
                        messageShow = emailJS.GeneratePush("PMI", {
                            t: 1,
                            bid: convo.id,
                            s: 1
                        }, [
                            user.get("name"),
                            convo.get(constants.convo.keys.Name)
                        ]);
                        break
                    case 'audio':
                        messageShow = emailJS.GeneratePush("PAI", {
                            t: 1,
                            bid: convo.id,
                            s: 1
                        }, [
                            user.get("name"),
                            convo.get(constants.convo.keys.Name)
                        ]);
                        break

                }
                let messageHide = emailJS.GenerateSilentPush({t: 1, bid: convo.id, s: 1});

                new Parse
                    .Query(Parse.Installation)
                    .containedIn("owner", pushRecipientsShow)
                    .find({useMasterKey: true})
                    .then(owners=>{
                        // console.log('\n\n\n\nowners length==='+owners.length);
                        // console.log('Parse Installation owners: '+JSON.stringify(owners));
                        // console.log('\n\n\nmessageShow: '+JSON.stringify(messageShow));
                        
                        // workaround for android push notification
                        // let insId = [];
                        // owners.forEach(owner=>{
                        //     console.log('\n\nowner installationId: '+owner.get('installationId'));
                        //     insId.push(owner.get('installationId'));
                        // })
                        // messageShow.installationId = insId;

                        console.log('\n\n\nmessageShow: '+JSON.stringify(messageShow));
                        console.log('pushRecipientsShow pushRecipientsShow: '+JSON.stringify(pushRecipientsShow));

                        if (pushRecipientsShow.length > 0) {
                            Parse
                                .Push
                                .send({
                                    where: new Parse
                                        .Query(Parse.Installation)
                                        .containedIn("owner", pushRecipientsShow),
                                    //todo desire information can not work ,but something random can work
                                    data: messageShow
                                }, {useMasterKey: true})
                                .then(function (result) {
                                    console.log('result result result: '+JSON.stringify(result));
                                })
                                .fail(function (error) {
                                    console
                                        .log("ALERT: Error sending unhidden push notifications:", error)
                                        .despatch();
                                });
                        }        

                        if (pushRecipientsHide.length > 0) {
                            Parse
                                .Push
                                .send({
                                    where: new Parse
                                        .Query(Parse.Installation)
                                        .containedIn("owner", pushRecipientsHide),
                                    data: messageHide
                                }, {useMasterKey: true})
                                .then(function (result) {
                                    console.log(result)
                                })
                                .fail(function (error) {
                                    console
                                        .log("ALERT: Error sending new conversation item push notifications:", error)
                                        .despatch();
                                });
                        }
                        
                    })


                // if (pushRecipientsShow.length > 0) {
                //     Parse
                //         .Push
                //         .send({
                //             where: new Parse
                //                 .Query(Parse.Installation)
                //                 .containedIn("owner", pushRecipientsShow),
                //             //todo desire information can not work ,but something random can work
                //             data: messageShow
                //         }, {useMasterKey: true})
                //         .then(function (result) {
                //             console.log(result)
                //         })
                //         .fail(function (error) {
                //             console
                //                 .log("ALERT: Error sending unhidden push notifications:", error)
                //                 .despatch();
                //         });
                // }

                // if (pushRecipientsHide.length > 0) {
                //     Parse
                //         .Push
                //         .send({
                //             where: new Parse
                //                 .Query(Parse.Installation)
                //                 .containedIn("owner", pushRecipientsHide),
                //             data: messageHide
                //         }, {useMasterKey: true})
                //         .then(function (result) {
                //             console.log(result)
                //         })
                //         .fail(function (error) {
                //             console
                //                 .log("ALERT: Error sending new conversation item push notifications:", error)
                //                 .despatch();
                //         });
                // }

                // * Next send out emails. We only do this immediately for Blrts. Comment emails
                // are covered by a job sendCommentNotifications
                console.log('\n\n\n===before sending the email for the blrt creation===');
                if(type === 'blrt') {
                    console.log('\n\n\nblrt blrt blrt sending email: '+JSON.stringify(blrt));
                    console.log('===emailRecipients length==='+emailRecipients.length);
                    if(emailRecipients.length > 0) {
                        emailJS
                            .SendTemplateEmail({
                                template: constants.mandrillTemplates.blrtReply,
                                convo: convo,
                                blrt: blrt,
                                content: blrt,
                                recipients: emailRecipients,
                                sender: user,
                                creator: user
                            }).fail(function (error) {
                                l
                                    .log("ALERT: Error sending new Blrt Reply emails:", error)
                                    .despatch();
                            });
                    }
                }else {
                    //do nothing
                }

            }, function (e) {
                console.log(e)
            })
    }

    /**
     * # step 2.1 : Matching existing localGuid
     * # step 2.2 : Update conversation counters and create independent blrt or mediaSet
     * # step 2.3 : Create conversationItem
     * # step 2.4 : Update conversation counter again and update relation's indexViewedList
     *
     * @param {string} type - type of conversationItem
     * @param {Object} item - necessary information to create item
     * @param {Parse.Object} convo - some counter need to be updated
     * @param {Parse.Object} relation - some fields need to be updated
     * @param {Parse.Object} user -some counter will be updated
     **/
    function createItem(type, item, convo, relation, user) {
        console.log('\n\n\n===createItem===');
        console.log('item: '+JSON.stringify(item));
        //# step 2.1 matching existing localGuid
        return new Parse
            .Query('ConversationItem')
            .equalTo('localGuid', item.localGuid)
            .first({useMasterKey: true})
            .then(function (foundItem) {
                //# step 2.2 Update convo counters and create independent blrt or mediaSet
                if (foundItem) {
                    // The request is already processed before,the function will be returned from
                    // here The signature still need to be matched
                    return Parse
                        .Promise
                        .when(convo, relation, user, foundItem)
                } else {
                    // update counters as wll as dirty counter the dirty counter
                    // (partialItemCreationCount,partialItemCreationTimes) serve as a lock in order
                    // to guarantee data integrity because the api of parse.com fail quite
                    // frequently For now ,maybe it is not necessary any more,leave the logic here
                    // in honor of the work of previous programmers
                    convo
                        .increment(constants.convo.keys.partialItemCreationCount)
                        .increment(constants.convo.keys.generalCount)
                        .increment(constants.convo.keys.readableCount)
                        .add("partialItemCreationTimes", expiration);
                    let updateConvoAndCreationPromise;
                    const {media, arg} = item;
                    switch (type) {
                        case 'images':
                            //create mediaSet document in parse
                            updateConvoAndCreationPromise = Parse
                                .Promise
                                .when(convo, MediaAsset.createMediaForImageItem(media, user, device));
                            break;
                        case 'blrt':
                            convo.increment(constants.convo.keys.blrtCount, 1);
                            if (!convo.get(constants.convo.keys.ThumbnailFile)) {
                                convo.set(constants.convo.keys.ThumbnailFile, item.thumbnail)
                            }
                            //create blrt document in parse

                            console.log('\n\n\n===device==='+JSON.stringify(device));

                            updateConvoAndCreationPromise = MediaAsset
                                .createMediaForBlrt(media, arg, user, savedObjects, device)
                                .then(function (createMediaResult) {
                                    const {encryptType, encryptValue, audio, touch} = item.files;
                                    const media = createMediaResult.foundAndCreatedMedia
                                    const args = createMediaResult.dbArg
                                    const thumbnail = item.thumbnail

                                    console.log('\n\n\n===media==='+JSON.stringify(media));
                                    console.log('\n\n\n===args==='+JSON.stringify(args));

                                    return Parse
                                        .Promise
                                        .when(convo.save(null, {useMasterKey: true}), Parse.Promise.as(createMediaResult), Blrt.createBlrt(encryptType, encryptValue, media, args, audio, touch, thumbnail))
                                });
                            break;
                        case 'comment':
                            updateConvoAndCreationPromise = convo
                                .increment(constants.convo.keys.commentCount, 1)
                                .save(null, {useMasterKey: true});
                            break;

                        case 'audio':
                            updateConvoAndCreationPromise = convo.save(null, {useMasterKey: true});
                            break;
                    }
                    return updateConvoAndCreationPromise
                    //step 2.3 : Create conversationItem
                        .then(function (convo, media, blrt) {
                        const roleACL = new Parse.ACL();
                        roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                        roleACL.setRoleWriteAccess("Conversation-" + convo.id, true);

                        let hasMadeComment = false;
                        let hasMadeBlrt = false;
                        // Blrt reply use these two params to display media
                        let lastArg
                        let lastMedia

                        const object = new Parse
                            .Object(constants.convoItem.keys.className)
                            .set(constants.base.keys.version, constants.convoItem.version)
                            .set(constants.base.keys.appVersion, req.device.version)
                            .set(constants.base.keys.device, req.device.device)
                            .set(constants.base.keys.os, req.device.os)
                            .set(constants.base.keys.osVersion, req.device.osVersion)
                            .set(constants.base.keys.localGuid, item.localGuid)
                            .set(constants.base.keys.creator, user)
                            .set(constants.convoItem.keys.conversation, convo)
                            .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) - 1)
                            .set(constants.convoItem.keys.readableIndex, convo.get(constants.convo.keys.readableCount) - 1)
                            // a pointer to parent conversation,used to decide if an item is read or unread
                            .set(constants.convoItem.keys.psuedoId, convo.id + "-" + (convo.get(constants.convo.keys.readableCount) - 1));

                        if (convo.get("conversationRole") != null) 
                            object.setACL(roleACL);
                        
                        switch (type) {
                            case 'images':
                                object.set(constants.convoItem.keys.type, constants.convoItem.types.images)
                                // .set(constants.base.keys.localGuid, item.localGuid)
                                    .set("thumbnails", item.thumbnails)
                                    .set(constants.convoItem.keys.Media, media)
                                    .set(constants.convoItem.keys.Argument, JSON.stringify(item.arg));
                                lastMedia = media

                                break;
                            case 'blrt':
                                console.log('\n\n\n===blrt blrt blrt: '+JSON.stringify(blrt));
                                console.log('\n\n\n===item item item: '+JSON.stringify(item));
                                lastArg = JSON.stringify(media.dbArg);
                                lastMedia = media.foundAndCreatedMedia;
                                hasMadeBlrt = true;
                                object
                                //users should be able to reply individual blrts
                                    .set(constants.convoItem.keys.Media, lastMedia)
                                    .set(constants.convoItem.keys.Argument, lastArg)
                                    //TODO the blrt data is  duplicated in both conversationItem and blrt table
                                    .set(constants.convoItem.keys.type, constants.convoItem.types.reBlrt)
                                    // .set(constants.base.keys.localGuid, item.localGuid)
                                    .set(constants.base.keys.encryptType, item.files.encryptType)
                                    .set(constants.base.keys.encryptValue, item.files.encryptValue)
                                    .set(constants.convoItem.keys.AudioFile, item.files.audio)
                                    .set(constants.convoItem.keys.TouchDataFile, item.files.touch)
                                    .set(constants.convoItem.keys.thumbnail, item.thumbnail)
                                    .set('blrt', blrt);
                                break;
                            case 'comment':
                                hasMadeComment = true;
                                object.set(constants.convoItem.keys.type, 3)
                                // .set(constants.base.keys.localGuid, item.localGuid)
                                    .set(constants.convoItem.keys.Argument, JSON.stringify({"Comment": item.text}));
                                break;
                            case 'audio':
                                object
                                    .set(constants.base.keys.encryptType, item.files.encryptType)
                                    .set(constants.base.keys.encryptValue, item.files.encryptValue)
                                    .set(constants.convoItem.keys.type, constants.convoItem.types.audio)
                                    .set(constants.convoItem.keys.AudioFile, item.files.audio)
                                    .set(constants.convoItem.keys.Argument, JSON.stringify({"AudioLength": item.arg.audioLength}));

                                break;
                        }
                        return Parse
                            .Promise
                            .when(convo, {
                                lastArg,
                                lastMedia
                            }, object.save(null, {useMasterKey: true}))
                    })
                    // step 2.4 : Update conversation counter again and update relation's indexViewedList
                        .then(function (convo, media, item) {
                            convo
                                .increment(constants.convo.keys.partialItemCreationCount, -1)
                                .remove("partialItemCreationTimes", expiration)
                                .set(constants.convo.keys.ContentUpdate, now);

                            const currentUnusedMedia = convo.get('unusedMedia');
                            const currentUnusedMediaPointers = currentUnusedMedia
                                ? currentUnusedMedia.map(m => {console.log('\n\n===media toPointer'); console.log(m.toPointer); return m.toPointer();})
                                : [];

                            //In the following two case, media never used in blrt should be calculated
                            switch (type) {
                                case 'blrt':
                                    const {lastArg, lastMedia} = media;
                                    let blrt = item.get('blrt');
                                    // Blrt reply will use lastArg and lastMedia of the convo ,which are the same as
                                    // the ones in Blrt
                                    convo
                                        .set(constants.convo.keys.LastMedia, lastMedia)
                                        .set(constants.convo.keys.LastMediaArgument, lastArg);

                                    //remove possible media from unused media;
                                    convo.set('unusedMedia', _.reject(currentUnusedMediaPointers, (c) => {
                                        console.log("\n\nc.id +' : '+c.objectId " + c.id + ' : ' + c.objectId)
                                        return (_.findIndex(lastMedia, (lm) => {
                                            return c.objectId === lm.id;
                                        }) > -1)
                                    }))
                                    //every blrt should be converted to video
                                    convertBlrtToVideo(item);
                                    break;
                                case 'images':
                                    const newMediaPointers = media
                                        .lastMedia
                                        .map(m => m.toPointer());
                                    const newUnusedMediaPointers = currentUnusedMediaPointers.concat(newMediaPointers);
                                    convo.set('unusedMedia', newUnusedMediaPointers);
                                    break
                            }

                            if (convo.get(constants.convo.keys.HasConversationBegan) != true && user.id != convo.get(constants.base.keys.creator).id) {
                                l.log("The conversation has now begun!");
                                convo.set(constants.convo.keys.HasConversationBegan, true);
                            }

                            relation.set(constants.convoUserRelation.keys.indexViewedList, (!relation.get(constants.convoUserRelation.keys.indexViewedList)
                                ? ""
                                : relation.get(constants.convoUserRelation.keys.indexViewedList) + ",") + (convo.get(constants.convo.keys.readableCount) - (1)) + (1 > 1
                                ? "-" + (convo.get(constants.convo.keys.readableCount) - 1)
                                : "")).set(constants.convoUserRelation.keys.lastEdited, now);

                            relation.set("contentUpdate", now);
                            user
                                .increment(constants.user.keys.blrtCreationCounter, 1)
                                .increment(constants.user.keys.commentCreationCounter, 1);

                            return Parse
                                .Promise
                                .when(convo.save(null, {useMasterKey: true}), relation.save(null, {useMasterKey: true}), user.save(null, {useMasterKey: true}), Parse.Promise.as(item))
                        })
                }

            })
    }

    function unsuccessful(error) {
        let response = {
            success: false,
            error: JSON.stringify(error)
        };
        if (savedObjects.length != 0) 
            response.objects = _.responsify(savedObjects);
        return Parse
            .Promise
            .as(response);
    }

};
