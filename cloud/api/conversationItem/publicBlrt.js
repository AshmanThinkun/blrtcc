var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var utilJS = require("cloud/util");
var ParseMutex = require("cloud/ParseMutex");
// var Mixpanel = require("cloud/mixpanel");
var curJS = require("cloud/conversationUserRelationLogic");
var runWebhookJS = require("cloud/timelyActions/runWebhook");
var fromUrl = require("cloud/api/conversationItem/fromUrl");
var muteJS = require("cloud/api/conversationUserRelation/mute");
var emailJS = require("cloud/email");

var csv_export=require('csv-export');

var fs = require('fs');
 

// # v1: conversationItemPublic
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationItemPublic

    // Subclasses APIError.
    var conversationItemPublicError = _.extend(api.error, { 
        blrtIdNotFound:                 { code: 620, message: "The Blrt id cannot be found." },
        requestUserRequired:            { code: 621, message: "A requesting user could not be found." },
        notCreator:                     { code: 622, message: "The requesting user is not the Blrt creator." },
        conversationPrivate:            { code: 623, message: "This conversation doesn't allow public Blrt" },
        notBlrt:                        { code: 624, message: "This is not a Blrt"}
    });

   
    // Aliased as error for simplicity of internal use.
    var error = conversationItemPublicError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();
        console.log('======req: '+req);

        // params = req.params;
        user = req.user;
        if(!user)
            return unsuccessful(error.requestUserRequired);

        // var blrtId = params.blrtId;
        // var name = params.name;

        console.log('======================\n\n');

        //get conversationItem
        return new Parse.Query("ConversationItem")
            .include("creator")
            .equalTo('isPublicBlrt', true)
            // .notEqualTo('blrt', null)
            .descending("createdAt")
            .limit(1000)
            .find({useMasterKey: true})
            .then(function (blrtItem) {
                // console.log('blrtItem: '+blrtItem.length);
                console.log("===blrtItem===\n\n");
                var publicBlrtInfo = [];

                blrtItem.forEach(function (item) {
                    let url = constants.siteAddress+"/blrt/" +_.encryptId(item.id);
                    // console.log('===item===\n');
                    // console.log('item: '+url);
                    // if(item.get('blrt')) {
                        // console.log('blrtId==='+item.get('blrt').id); 
                        publicBlrtInfo.push({
                            blrtId: item.id,
                            blrtName: item.get('publicName'),
                            creatorEmail: item.get('creator').get('email'),
                            eServerLink: url,
                            createdAt: item.get('createdAt'),
                        })
                    // }
                })

                return JSON.stringify(publicBlrtInfo);

                console.log('=============');
                csv_export.export(publicBlrtInfo,function(buffer){
                  //this module returns a buffer for the csv files already compressed into a single zip.
                  //save the zip or force file download via express or other server
                  fs.writeFileSync('./data.zip', buffer);
                });

            }, function(error){
                l.log(false, "Error finding conversation item: " + blrtId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();
