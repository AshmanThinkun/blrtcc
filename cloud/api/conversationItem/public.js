var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var utilJS = require("cloud/util");
var ParseMutex = require("cloud/ParseMutex");
// var Mixpanel = require("cloud/mixpanel");
var curJS = require("cloud/conversationUserRelationLogic");
var runWebhookJS = require("cloud/timelyActions/runWebhook");
var fromUrl = require("cloud/api/conversationItem/fromUrl");
var muteJS = require("cloud/api/conversationUserRelation/mute");
var emailJS = require("cloud/email");

// # v1: conversationItemPublic
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationItemPublic

    // Subclasses APIError.
    var conversationItemPublicError = _.extend(api.error, { 
        blrtIdNotFound:                 { code: 620, message: "The Blrt id cannot be found." },
        requestUserRequired:            { code: 621, message: "A requesting user could not be found." },
        notCreator:                     { code: 622, message: "The requesting user is not the Blrt creator." },
        conversationPrivate:            { code: 623, message: "This conversation doesn't allow public Blrt" },
        notBlrt:                        { code: 624, message: "This is not a Blrt"}
    });

   
    // Aliased as error for simplicity of internal use.
    var error = conversationItemPublicError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;
        if(!user)
            return unsuccessful(error.requestUserRequired);

        var blrtId = params.blrtId;
        var name = params.name;

        //get conversationItem
        return new Parse.Query("ConversationItem")
            .equalTo("objectId", blrtId)
            .include("conversation")
            //todo [Guichi] necessary ??
            .include("creator")
            .first()
            .then(function (blrtItem) {
                if(! blrtItem){
                    l.log(false, "Conversation item id not found: ", blrtId).despatch();
                    return unsuccessful(error.blrtIdNotFound);
                }
                
                if(blrtItem.get("isPublicBlrt")){
                    return {success: true, message:"Make Public Blrt"};
                }

                //check whether this is a blrt
                if(!blrtItem.get("media")){
                    return unsuccessful(error.notBlrt);
                }

                var conversation = blrtItem.get("conversation");
                return ParseMutex.fnWithLock(conversation, "publicBlrtManagement", function (convo, isDirty) {
                    var dirtyPromise = Parse.Promise.as();
                    if(isDirty) {
                        l.log("It was dirty! Cleaning before proceeding");
                        dirtyPromise = curJS.cleanDirtyConvos([convo.id]);
                    }

                    return dirtyPromise.then(function () {
                        if(convo.get("disallowPublicBlrt")){
                            return unsuccessful(error.conversationPrivate);
                        }

                        if(blrtItem.get("creator").id != user.id){
                            return unsuccessful(error.notCreator);
                        }
                        var toSave = [];

                        var now = new Date();
                        var pushRecipientsShow = [];
                        var pushRecipientsHide = [];

                        //create event
                        return convo.increment(constants.convo.keys.generalCount)
                            .set("contentUpdate", now)
                            .save()
                            .then(function(convo){
                                return GetPushRecipients(convo, user.id)
                                .then(function(recResult){
                                    pushRecipientsShow = recResult.pushRecipientsShow;
                                    pushRecipientsHide = recResult.pushRecipientsHide;

                                    return recResult.relations;
                                }).then(function(relations){
                                    _.each(relations, function(rel){
                                        rel.set("contentUpdate", now);
                                        toSave.push(rel);
                                    });

                                    return convo;
                                });
                            }).then(function(convo){
                                var acl = new Parse.ACL();
                                acl.setPublicReadAccess(true);
                                acl.setPublicWriteAccess(true);
                                blrtItem.set("isPublicBlrt", true)
                                    .set("publicName", name)
                                    .setACL(acl);

                                //thumbnail
                                if(!blrtItem.get("thumbnailFile")){
                                    blrtItem.set("thumbnailFile", convo.get("thumbnailFile"))
                                }

                                //add event
                                var info = "Public Blrts can be viewed by anyone and embedded in web pages. People can only make their own Blrts public. Comments and other Blrts in the conversation are visible only to people added to the conversation.";

                                var noteItem = new Parse.Object(constants.convoItem.keys.className)
                                                        .set(constants.base.keys.version, constants.convoItem.version)
                                                        .set(constants.base.keys.creator, user)
                                                        .set(constants.convoItem.keys.conversation, convo)
                                                        .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) -1)
                                                        .set(constants.convoItem.keys.readableIndex, -1)
                                                        .set(constants.convoItem.keys.type, 5)
                                                        .set(constants.convoItem.keys.Argument, JSON.stringify({
                                                            "Message" : "A Blrt was made public with name: " + name,
                                                            "MessageType": -999,
                                                            "Fallback" : {
                                                                "Message" : constants.convoItem.boldPrefix + "{0}"+constants.convoItem.boldPrefix+" made a Blrt public with name " + constants.convoItem.blackPrefix + name + constants.convoItem.blackPrefix ,
                                                                "MessageType" : 2,
                                                                "Info": info,
                                                                "Fallback" : {
                                                                    "Message" : constants.convoItem.boldPrefix + "{0}"+constants.convoItem.boldPrefix+" made a Blrt public with name " + constants.convoItem.blackPrefix + name + constants.convoItem.blackPrefix ,
                                                                    "MessageType" : 1,
                                                                    "Info": info
                                                                }
                                                            }
                                                        }));

                                toSave.push(blrtItem);
                                toSave.push(noteItem);

                                //increase public Blrt counter
                                user.increment("publicBlrtCount");
                                toSave.push(user);

                                return Parse.Object.saveAll(toSave)
                                .then(function(){
                                    //send push notification

                                    // * Build and send the push notification.
                                    // We build the push giving it the related conversation and the relation creator's information.
                                    var alertString = user.get("name") + " made a Blrt public with name " + name;
                                    var maxLength = 40;
                                    if(alertString.length>maxLength){
                                        alertString = alertString.substr(0,maxLength);
                                    }

                                    var messageShow = emailJS.StandardizePushMessage({
                                        alert: alertString,
                                        sound: "default",
                                        t: 1, 
                                        bid: convo.id,
                                        s: 1
                                    });
                                    
                                    //send push notification but don't show alert
                                    var messageHide = emailJS.StandardizePushMessage({
                                        t: 1, 
                                        bid: convo.id,
                                        s: 0
                                    });

                                    var pushPromises = [];

                                    // We send it to all devices owned by receiving users.
                                    if(pushRecipientsShow.length > 0) {
                                        pushPromises.push(Parse.Push.send({
                                            where: new Parse.Query(Parse.Installation)
                                                .containedIn("owner", pushRecipientsShow),
                                            data: messageShow
                                        }, { useMasterKey: true }).fail(function (error) {
                                            l.log("ALERT: Error sending unhidden push notifications:", error).despatch();
                                        }));
                                    }

                                    if(pushRecipientsHide.length > 0) {
                                        pushPromises.push(Parse.Push.send({
                                            where: new Parse.Query(Parse.Installation)
                                                .containedIn("owner", pushRecipientsHide),
                                            data: messageHide
                                        }).fail(function (error) {
                                            l.log("ALERT: Error sending new conversation item push notifications:", error).despatch();
                                        }));
                                    }

                                    return Parse.Promise.when(pushPromises);

                                }).then(function(){
                                    var fetchParams = {
                                        params: {
                                            url: constants.siteAddress+"/blrt/" +_.encryptId(blrtItem.id),
                                            includeMedia: true
                                        }
                                    };

                                    return fromUrl[1](fetchParams).then(function (result) {
                                        if(!result.success){
                                            l.log(false, "Error get blrt media").despatch();
                                            return unsuccessful(error.subqueryError);
                                        }

                                        else
                                        {
                                            console.log("^^ : "+constants.embedServer + "/blrt/receive")
                                            return Parse.Cloud.httpRequest({
                                                                method: "POST",
                                                                url: constants.embedServer + "/blrt/receive",
                                                                body: {
                                                                    id: _.encryptId(blrtItem.id),
                                                                    data: JSON.stringify(result.data)
                                                                }
                                                            }).then().always(function () {
                                                return {success: true, message:"Make Public Blrt"};
                                            })

                                        }

                                        // //webhook
                                        // var action = new Parse.Object("TimelyAction")
                                        //             .set("executionTime", new Date())
                                        //             .set("type", "runWebhook")
                                        //             .set("data",{
                                        //                 method: "POST",
                                        //                 url: constants.embedServer + "/blrt/receive",
                                        //                 body: {
                                        //                     id: _.encryptId(blrtItem.id),
                                        //                     data: JSON.stringify(result.data)
                                        //                 }
                                        //             });
                                        //
                                        // return action.save()
                                        //     .then(function(){
                                        //         return runWebhookJS.process(action)
                                        //             .then(function(){
                                        //                 console.log("all good")
                                        //                 action.destroy().then(function(){
                                        //                     l.log("Action resolved: " + action.id).despatch();
                                        //                 },function (promiseError) {
                                        //                     l.log("ALERT: Error destroying TimelyAction " + action.id + ":", promiseError).despatch();
                                        //                 });
                                        //             },function(error){
                                        //                 console.log("AAAA "+err)
                                        //                 l.log(false, "Error run timelyActions: ", action.id).despatch();
                                        //                 return Parse.Promise.as();
                                        //             }).always(function(){
                                        //                 l.log(true, "Make Public Blrt: ", blrtItem.id).despatch();
                                        //                 return {success: true, message:"Make Public Blrt"};
                                        //             });
                                        //     }, function(error){
                                        //         l.log(false, "Error saving TimelyAction");
                                        //         return unsuccessful(error.subqueryError);
                                        //     });

                                    }, function (error) {
                                        l.log(false, "Error get blrt media").despatch();
                                        return unsuccessful(error.subqueryError);
                                    });
                                }, function(error){
                                    l.log(false, "Error saving Blrt item: ", blrtItem.id);
                                    return unsuccessful(error.subqueryError);
                                });

                            },function(error){
                                l.log(false, "Error saving convo").despatch();
                                return unsuccessful(error.subqueryError);
                            });

                    }, function (promiseError) {
                        l.log(false, "Couldn't clean dirty convo:", promiseError).despatch();
                        return Parse.Promise.as({ success: false, error: error.subqueryError });
                    });
                }, function (promiseError) {
                    l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();
                    
                    if(_.isDate(promiseError)) {
                        // The mutual exclusion was in use by another calling function - we'll have to try again later.
                        return Parse.Promise.as({ success: false, error: error.subqueryError, tryAgainIn: promiseError });
                    }

                    return Parse.Promise.as({ success: false, error: error.subqueryError });
                });
                    

            }, function(error){
                l.log(false, "Error finding conversation item: " + blrtId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();

// input convo id, current user id
//return pushRecipientsShow, pushRecipientsHide
function GetPushRecipients(convo, currentUserId){
    var muteFlags = {};
    var pushRecipients = [];

    return new Parse.Query("ConversationUserRelation")
        .equalTo("conversation", convo)
        .include("user")
        .find()
        .then(function(rels){
            if(!rels)
                rels = []; 
            rels = _.reject(rels, function(rel){
                return rel.get("user")? false:true;
            });
            _.each(rels, function(rel){
                if( rel.get("user").id != currentUserId)
                    pushRecipients.push(rel.get("user"));
            });

            //get mute data
            _.each(rels, function(relation){
                var muteFlag = relation.get("muteFlag");
                return muteFlags[relation.get("user").id] = muteFlag;
            });

            var pushRecipientsShow = [];
            var pushRecipientsHide = [];
            _.each(pushRecipients,function(recipient){
                if(muteJS.shouldMute(muteFlags[recipient.id], muteJS.MuteEnum.push)){
                    pushRecipientsHide.push(recipient);
                }else{
                    pushRecipientsShow.push(recipient);
                }
            });

            return {
                relations: rels,
                pushRecipientsShow: pushRecipientsShow,
                pushRecipientsHide: pushRecipientsHide
            };
        });
}
