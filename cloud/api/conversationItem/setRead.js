
/******************************
 * set an unread comment as read
 ******************************/

var _ = require("underscore");
var api = require("cloud/api/util");
//just for encryptId
require("cloud/util");

var fetchAllItemError = _.extend(api.error, {
    noExist: {code: 501, message: "The conversationUserRelation requested do not exist"},
});

var error = fetchAllItemError;

exports[1] = function (req) {
    const {params, user} = req;

    var convoUserRelationStub =new Parse.Object("ConversationUserRelation");
    convoUserRelationStub.id = params.conversationUserRelationId;
    var readableIndex = params.readableIndex;

    console.log('\n\n\nsetRead params.readableIndex==='+readableIndex);
    console.log(typeof readableIndex);
   
    return convoUserRelationStub.fetch({useMasterKey:true})
        .then(function(relation) {
            var existingList = _.expand(relation.get("indexViewedList"));
            console.log('\n\nexistingList==='+existingList);
            readableIndex.forEach(item=>{
                console.log('\n\nreadableIndex typeof==='+item);
                console.log(typeof item);
                if(!(existingList.indexOf(item)>-1)) {
                    existingList.push(item);
                    console.log('\n\nnew existingList==='+existingList);
                    console.log('contract sorted list: '+_.contract(existingList.sort()));
                    relation.set("indexViewedList", _.contract(existingList.sort()));
                }
            });
            return relation.save(null,{useMasterKey:true})
        }, function() {
            return new Parse.Promise.error({errorType:error.noAccess})
        }).then(function() {
            return Parse.Promise.as({success:true})
        }).fail(function(error) {
            return Parse.Promise.as({success:false,error:error.errorType})
        })
}

