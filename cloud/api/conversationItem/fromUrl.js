var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");


function getParameterByName(name, queryString) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(queryString);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// # v1: conversationItemFromUrl
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationItemFromUrl

    // Subclasses APIError.
    var conversationItemFromUrlError = _.extend(api.error, { 
        invalidUrl:              	    { code: 620, message: "This is an invalid url." },
        authenticationFailure:          { code: 621, message: "User is not allowed for accessing this Blrt" },
        playCounterLimit:              	{ code: 622, message: "Play counter limit reached for this private blrt" },
        notBlrt:                        { code: 624, message: "This is not a Blrt"}
    });

   
    // Aliased as error for simplicity of internal use.
    var error = conversationItemFromUrlError;

    // ## Flow

    /*
    {
        success,
        message,
        data: {
            
            title,
            creator,
            createdAt,
            duration,
            thumbnail,
            pages,
            isPublicBlrt
            
            playData: {
                touchDataFile,
                audioFile,
                argument,
                encryptValue,
                encryptType,
                media: [
                    {
                        id,
                        encryptType,
                        encryptValue,
                        mediaFile,
                        mediaFormat,
                        pageArgs
                    }
                ]
            }
        }
    }
    */
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        //[Guichi] if invoked from blrt.js in cloud code ,the user may not be included
        //PS: that function will be removed and use blrt webapp instead
        user = req.user;
        var url = params.url;
        var includeMedia = params.includeMedia? true: false;


        console.log("url "+url)
        //parse url to get the conversation item id
        if(!_.isUrl(url)){
            l.log(false, "Invalid url: ", url).despatch();
            return unsuccessful(error.invalidUrl);
        }
        // var pre = constants.siteAddress+"/blrt/";
        var pre = "/blrt/";
        var index = url.indexOf(pre);
        if(index==-1){
            l.log(false, "Invalid url: ", url).despatch();
            return unsuccessful(error.invalidUrl);
        }
        
        var blrtId = url.substr(index + pre.length);

        var queryString = _.singleSplit(blrtId, '?');
        if(queryString.length == 2) {
            blrtId = queryString[0];
            queryString = queryString[1];
        } else
            queryString = '';

        blrtId = blrtId.replace(/\/$/, '');

        blrtId = _.decryptId(blrtId);

        //get conversationItem
        return new Parse.Query("ConversationItem")
            .equalTo("objectId", blrtId)
            .include("conversation")
            .include("creator")
            .include("media")
            .first()
            .then(function (blrtItem) {
                if(! blrtItem){
                    l.log(false, "Conversation item id not found: ", blrtId).despatch();
                    return unsuccessful(error.invalidUrl);
                }
				
                //check whether this is a blrt
                if(!blrtItem.get("media")){
                    l.log(false, "Requesting blrt id is not a blrt: ", blrtId).despatch();
                    return unsuccessful(error.notBlrt);
                }

				var conversation = blrtItem.get("conversation");
                var isPublicBlrt = blrtItem.get("isPublicBlrt");

                //if conversation is deleted
                if(conversation.get("deletedAt")){
                    l.log(false, "Requesting blrt deleted: ", blrtId).despatch();
                    return unsuccessful(error.notBlrt);
                }

                var argData;
                try {
                    argData = JSON.parse(blrtItem.get("argument"));
                } catch(error) {
                    l.log(false, "Can't get arg from json string", blrtItem.get("argument")).despatch();
                    return unsuccessful(error.subqueryError);
                }

                var thumbnailFile  =  blrtItem.get("thumbnailFile");
                if(!thumbnailFile){
                    thumbnailFile = conversation.get("thumbnailFile");
                }
                var oembed = {
                    title:                  isPublicBlrt? blrtItem.get(constants.convoItem.keys.PublicName): conversation.get(constants.convo.keys.Name),
                    creator:                blrtItem.get("creator").get("name"),
                    createdAt:              blrtItem.createdAt,
                    duration:               argData.AudioLength,
                    thumbnail:              thumbnailFile? thumbnailFile.url(): "",
                    pages:                  blrtItem.get("media").length,
                    isPublicBlrt:           isPublicBlrt? true:false,
                    conversationId:         _.encryptId(conversation.id)
                };

                if(!includeMedia){
                    l.log(true, "Return Blrt item from url: ", url).despatch();
                    return {
                        success: true, 
                        message:"Return Blrt item from url",
                        data: oembed
                    };
                }

                // get the meidas 
                var media ={};
                var canPlay = false;
                var waitPromises = [];

                var playCounter = blrtItem.get("privateViewCounter") || 0;
                if (!isPublicBlrt) {
                    //user is the conversation member, then they can play
                    if (user) {
                        var query = new Parse.Query("ConversationUserRelation")
                            .equalTo("conversation", conversation)
                            .equalTo("user", user)
                            .count()
                            .then(function(relationCount){
                                if(relationCount>0)
                                    canPlay = true;
                                return;
                            }, function(error){
                                l.log(false, "Error find conversation user relation", user.id ).despatch();
                                return unsuccessful(error.subqueryError);
                            });
                        waitPromises.push(query);
                    } else {
                        var token = getParameterByName(constants.accessToken.queryStringKey, "?" + queryString);
                        if (("string" == typeof token) && (token.length > 0)) {
                            var checkTokenPromise = Parse.Cloud.run('accessWithToken', {token: token, convoId: conversation.id})
                                .then(
                                    function(result) {
                                        if (result.success) {
                                            canPlay = true;
                                        } else {
                                            canPlay = false;
                                        }
                                    },
                                    function(error) {
                                        canPlay = false;
                                    }
                                );
                            waitPromises.push(checkTokenPromise);
                        } else {
                            canPlay = false;
                        }
                    }
                } else {
                    canPlay = true;
                }

                return Parse.Promise.when(waitPromises).then(function(){
                    if(!canPlay){
                        l.log(false, "user doesn't allowed to view this blrt" ).despatch();
                        resError = user? error.authenticationFailure: error.playCounterLimit;

                        return {
                            success: false,
                            error: resError,
                            data: oembed
                        };
                    }
                    var playData = {
                        touchDataFile:      blrtItem.get("touchDataFile").url(),
                        audioFile:          blrtItem.get("audioFile").url(),
                        argument:           blrtItem.get("argument"),
                        encryptValue:       blrtItem.get("encryptValue"),
                        encryptType:        blrtItem.get("encryptType"),
                        media:              _.map(blrtItem.get("media"), function(asset){
                            return {
                                id: asset.id,
                                encryptType: asset.get("encryptType"),
                                encryptValue: asset.get("encryptValue"),
                                mediaFile: asset.get("mediaFile").url(),
                                mediaFormat: asset.get("mediaFormat"),
                                pageArgs: asset.get("pageArgs")
                            };
                        })
                    };



                    l.log(true, "Return Blrt item from url: ", url).despatch();
                    var toReturn =  {
                        success: true, 
                        message:"Return Blrt item from url",
                        data: oembed
                    };

                    toReturn.data.playData = playData;

                    if(user){
                        toReturn.data.conversationId = _.encryptId(conversation.id);
                    }

                    return toReturn;

                },function (error){
                    return unsuccessful(error.subqueryError);
                });
				

            }, function(error){
                l.log(false, "Error finding conversation item: " + blrtId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();
