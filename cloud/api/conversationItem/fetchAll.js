/******************************
 * Fetch all conversationItems of a conversation
 * used by webapp to display all the conversationItems in a conversation
 ******************************/

var _ = require("underscore");
var api = require("cloud/api/util");
//just for encryptId
require("cloud/util")

const ITEM_PER_REQUEST = 10; //webapp also need this to check if there is more items

var fetchAllItemError = _.extend(api.error, {
    noAccess: {code: 500, message: "The user do not have access to the conversation requested"},
    noExist: {code: 501, message: "The conversation requested do not exist"},
});

var error = fetchAllItemError;
exports[1] = function (req) {
    var params = req.params;
    var user = req.user;

    var convoStub = new Parse.Object("Conversation");
    convoStub.id = params.conversationId;

    console.log('\n\nconversationId==='+params.conversationId);

    var pageNo = params.pageNo;
    return new Parse.Query("ConversationUserRelation")
        .equalTo("conversation", convoStub)
        .find({useMasterKey:true})
        .then(function (relations) {
            var currentUserViewIndex;

            //the relations is empty
            if (!Boolean(relations.length)) {
                return {success: false, error: error.noExist}
            }

            var userHasAccess = false;
            relations.forEach(function (relation) {
                if(relation.get("user") && relation.get("user").id == user.id) {
                    console.log('\n\nindexViewedList===');
                    console.log(relation.get("indexViewedList"));
                    userHasAccess = true;
                    currentUserViewIndex = _.expand(relation.get("indexViewedList"))
                }
            })


            // the user initialize the request do not belong to this conversation.
            if (!userHasAccess) {
                return {success: false, error: error.noAccess}
            }

            //Begin to prepare the data to return
            else {
                //fetch data from ConversationItem table
                var itemInfo = [];
                var conversationItemPromise = new Parse.Query("ConversationItem")
                    .equalTo("conversation", convoStub)
                    .include("creator")
                    .include("media")
                    .ascending("index");

                if (typeof pageNo === 'number') {
                    conversationItemPromise
                        .skip(pageNo * ITEM_PER_REQUEST)
                        .limit(ITEM_PER_REQUEST)
                        .descending("index");
                }

                var conversationItemInfoPromise = conversationItemPromise.find({useMasterKey:true})
                    .then(function (items) {
                        
                        console.log('\n\nitems: '+items.length);
                        console.log(items);

                        items.forEach(function(item) {
                            console.log('\n\n\nitem forEach===');
                            console.log(JSON.stringify(item));
                            var info = {};
                            info.index = item.get("index");
                            info.user = {};
                            info.id = _.encryptId(item.id)
                            info.plainId = item.id;
                            var creator = item.get("creator");
                            info.user.name = creator.get("name") || creator.get("username");
                            info.user.email = creator.get("username");
                            info.user.accountStatus = creator.get('accountStatus') || false;
                            info.createdAt = item.get('createdAt');

                            switch (item.get("type")) {
                                //readable types
                                case 0:
                                case 2:
                                case 3:
                                case 7:
                                case 8:
                                    var readableIndex = item.get("readableIndex");
                                    console.log('\n\n\nreadableIndex==='+readableIndex);
                                    console.log('\ncurrentUserViewIndex==='+currentUserViewIndex);
                                    info.user.id = creator.id;
                                    info.createdAt = item.createdAt;
                                    info.user.avatar = creator.get("avatar") && creator.get("avatar").url();
                                    // info.user.name = creator.get("name") || creator.get("username");
                                    info.read = currentUserViewIndex.indexOf(readableIndex) > -1;
                                    info.readableIndex = item.get("readableIndex");
                                    break;
                            }
                            switch (item.get("type")) {
                                case 3:
                                    console.log('\n\n\nComment argument===');
                                    info.text = JSON.parse(item.get("argument")).Comment;
                                    info.type = "comment";
                                    break;
                                case 0:
                                case 2:
                                    // console.log(item.id)
                                    info.type = "blrt";
                                    info.isPublicBlrt = item.get("isPublicBlrt") || false;
                                    info.publicName = item.get("publicName");
                                    info.duration = JSON.parse(item.get("argument")).AudioLength;
                                    // info.pageNumber = JSON.parse(item.get("argument")).Items.length;
                                    // calculate the pageNumber of media used in blrt
                                    let pNo = 0;
                                    JSON.parse(item.get("argument")).Items.map(ii=>{
                                        if(ii.PageString == '0' || ii.PageString == '1') { //image or one page pdf
                                            pNo++;
                                        }else { //pdf
                                            let res = ii.PageString.split('-');
                                            console.log('\n\n\nres res res==='+Number(res[1]));
                                            pNo = pNo + Number(res[1]);
                                        }
                                    });
                                    info.pageNumber = pNo;
                                    //the blrt created from webrecorder may not have a thumbnail,use the conversation thumbnail as a tmp solution
                                    info.thumbnail = item.get("thumbnailFile") && item.get("thumbnailFile").url();

                                    //up here ,for webplayer
                                    info.touchDataFile = item.get("touchDataFile") && item.get("touchDataFile").url();
                                    info.audioFile = item.get("audioFile") && item.get("audioFile").url();
                                    info.argument = item.get("argument");
                                    info.encryptValue = item.get("encryptValue");
                                    info.encryptType = item.get("encryptType");
                                    info.media = _.map(item.get("media"), function (asset) {
                                        return {
                                            id: asset.id,
                                            encryptType: asset.get("encryptType"),
                                            encryptValue: asset.get("encryptValue"),
                                            mediaFile: asset.get("mediaFile").url(),
                                            mediaFormat: asset.get("mediaFormat"),
                                            pageArgs: asset.get("pageArgs"),
                                            name:asset.get("name")
                                        };
                                    });


                                    break;
                                case 4:
                                    info.argument = JSON.parse(item.get("argument"));
                                    info.type = "addEvent";
                                    break;
                                case 5:
                                    info.argument = JSON.parse(item.get("argument"));
                                    info.type = "message";
                                    // Decide if the blrtRequest processed or not
                                    info.status = item.get("status");
                                    break;
                                case 7:
                                    info.type = "audio";
                                    info.length = JSON.parse(item.get("argument")).AudioLength;
                                    info.audioFile = item.get("audioFile") && item.get("audioFile").url();
                                    info.encryptType=item.get('encryptType');
                                    info.encryptValue=item.get('encryptValue');
                                    break;
                                case 8:
                                    info.type = "images";
                                    if (item.get("argument")) {
                                        if (JSON.parse(item.get("argument")).MediaType === "PDF") {
                                            info.type = "pdf";
                                            info.pageNumber = JSON.parse(item.get("argument")).PageCount;
                                        }
                                    }
                                    // info.thumbnails = item.get("thumbnails") && item.get("thumbnails").map(tbn => tbn.url());
                                    info.thumbnails = item.get("media").map(m=>m.get('thumbnailFile')&&m.get('thumbnailFile').url())
                                    //up here ,for webplayer
                                    info.media = _.map(item.get("media"), function (asset) {
                                        return {
                                            id: asset.id,
                                            encryptType: asset.get("encryptType"),
                                            encryptValue: asset.get("encryptValue"),
                                            mediaFile: asset.get("mediaFile").url(),
                                            mediaFormat: asset.get("mediaFormat"),
                                            pageArgs: asset.get("pageArgs"),
                                            name: asset.get("name"),
                                        };
                                    });
                            }
                            // console.log(JSON.stringify(info))
                            itemInfo.push(info);
                        })
                    })


                return conversationItemInfoPromise.then(function () {
                    return convoStub
                        .fetch({useMasterKey:true})
                        .then(function (fullConversation) {
                            var thumbnail = fullConversation.get("thumbnailFile") && fullConversation.get("thumbnailFile").url();
                            itemInfo.forEach(function (item) {
                                if (item.type == "blrt" && !item.thumbnail) {
                                    item.thumbnail = thumbnail;
                                }
                            })
                            if (fullConversation.get("deletedAt")) {
                                return {success: false, error: error.noExist}
                            }
                            else {
                                return {success: true, items: itemInfo, serverTime: new Date()};
                            }


                        })


                })
            }
        })
}
