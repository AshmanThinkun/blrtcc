var _ = require("underscore");

var api = require("cloud/api/util");

// # v1: developerResetKeys
(function () {
    // ## Enums

    // We return only a simple subqueryError here, if it happens.
    var error = api.error;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        return exports.resetUserObjectKeys(req.user)
            .save()
            .then(function () {
                l.log(true).despatch();
                return { success: true };
            }, function (promiseError) {
                l.log(false, "Failed to save user:", promiseError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };
})();

exports.resetUserObjectKeys = function (user) {
    return user
        .set("developerPublicApiKey", _.randomString(35))
        .set("developerPrivateKey", _.randomString(35));
}