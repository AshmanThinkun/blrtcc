var _ = require("underscore");

var api = require("cloud/api/util");
var constants = require("cloud/constants");
var emailJS = require("cloud/email");

// # v1: developerApply
(function () {
    // ## Enums

    // We return only a simple subqueryError here, if it happens.
    var error = api.error;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        var user = req.user;

        var status = user.get("developerStatus");

        if(status == "pending") {
            l.log(true, "ALERT: Developer was already pending when applying").despatch();
            return Parse.Promise.as({ success: true });
        } else if(status == "active") {
            l.log(false, "Developer was already active when applying").despatch();
            return Parse.Promise.as({ success: false, message: "This user already has an active developer status." });
        }

        return user
            .set("developerStatus", "pending")
            .save()
            .then(function () {
                return emailJS.sendTextEmail(
                    { name: "Blrt", email: "no-reply@blrt.com" },
                    [constants.developerAdmin],
                    "Blrt developer application received: " + user.get("email"),
                    "A Blrt developer request was received. Details follow.\n\n" +

                        "Email: " + user.get("email")
                ).fail(function (emailError) {
                    l.log("ALERT: Failed to save email:", emailError);
                }).always(function () {
                    l.log(true).despatch();
                    return { success: true };
                });
            }, function (promiseError) {
                l.log(false, "Failed to save user:", promiseError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };
})();