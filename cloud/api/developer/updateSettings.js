var _ = require("underscore");

var api = require("cloud/api/util");

// # v1: developerUpdateSettings
(function () {
    var user;
    var params;

    // ## Enums

    // We return only a simple subqueryError here, if it happens.
    var error = api.error;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        user = req.user;
        params = req.params;

        // We need to check the type just incase they provided an empty string.
        if(typeof params.requestWebhookUrl != "undefined")
            user.set("developerRequestWebhookUrl", params.requestWebhookUrl);

        l.log(typeof params.requestWebhookUrl);

        if(!user.dirty()) {
            l.log(true, "ALERT: Developer just updated settings without anything changing, legit?").despatch();

            return { success: true };
        }

        return user
            .save()
            .then(function () {
                l.log(true).despatch();
                return { success: true };
            }, function (promiseError) {
                l.log(false, "Failed to save user:", promiseError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };
})();