var constants = require("cloud/constants");
var api = require("cloud/api/util");
var _ = require("underscore");
const MediaAsset=require("cloud/api/models/MediaAsset")
// # v1: conversationCreateWithBlrt
// =======
const createMedia = require("cloud/api/mediaAsset").createMedia
// ## Enums

// ### conversationCreateWithBlrtError
var error = _.extend(api.error, {
    invalidItemArg: {
        code: 410,
        message: "The provided item argument is invalid or does not cover each provided media asset."
    },
    invalidName: {
        code: 411,
        message: "The provided name exceeds the " + constants.convo.nameCharLimit + " character limit."
    }
});

// ## Request examples

// } TODO: Provide examples

// ## Response examples

// } TODO: Provide examples

// ## Flow
exports[1] = function (req) {
    //[Guichi] return to client ,the client need this to calculate the success part,such as replace replace some
    //localGuid with mediaId
    var savedObjects = [];

    function unsuccessful(error) {
        var response = {
            success: false,
            error: error
        };

        if (savedObjects.length != 0)
            response.objects = _.responsify(savedObjects);

        return Parse.Promise.as(response);
    }

    var l = api.loggler;
    var now = new Date();



    var {params, user, device} = req;

    console.log("== params ==")
    console.dir(JSON.stringify(params))


    // The flow of creation can be split into 6 major steps:
    // 1. Find all objects with matching local GUIDs;
    // 2. Create all media and try to find a relation if an existing conversation was found.
    //   * Before we continue to step 3, we need to get all of the new media IDs and data and
    //     integrate them into the conversation item arguments and such.
    // 3. Create the conversation (if it wasn't found in step 1.).
    //   * We'll search for corresponding Blrt Requests in this step;
    // 4. Search for an existing role (if a conversation was found) and/or create a new role;
    // 5. Create conversation item and relation with ACLs.
    //   * We'll also make Blrt Request TimelyActions here if necessary; and
    // 6. Mark conversation as completed and set it's ACL.
    // We'll also update all statistics in this final step.

    // All creation in a single step can be combined into a single saveAll.
    // Other queries in a single step can be performed simultaneously (i.e. no dependency).

    var blrtRequest = params.requestId
        ? new Parse.Object("BlrtRequest")
        : false;
    if (blrtRequest)
        blrtRequest.id = params.requestId;

    // var mp = new Mixpanel({
    //     id: user.id,
    //     batchMode: true
    // });

    // ### Step 1: Matching local GUIDs
    var convoGuid = params.localGuid;                   // Conversation GUID
    var itemGuid = params.item.localGuid;               // Blrt GUID

    var createMediaPromise = MediaAsset.createMediaForBlrt(params.item.media, params.item.arg, user, savedObjects, device)

    return Parse.Promise.when(
        new Parse.Query(constants.convo.keys.className)
            .equalTo(constants.base.keys.localGuid, convoGuid)
            .first(),
        new Parse.Query(constants.convoItem.keys.className)
            .equalTo(constants.base.keys.localGuid, itemGuid)
            .first()
        // mediaQuery
    ).then(function (convo, item) {
        var convoExisted = convo != null;
        if (convo != null && convo.get(constants.convo.keys.incomplete) != true) {
            // It's probably a duplicate creation attempt if the conversation was found completed.
            // Unfortunately we now have to look for the relation so we can return all of the
            // objects that the client may need :(
            return new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.conversation, convo)
                .equalTo(constants.convoUserRelation.keys.user, user)
                .first()
                .then(function (relation) {
                    l.log(true, "Completed by finding existing completed conversation: " + convo.id).despatch();
                    return {success: true, objects: _.responsify(_.union([convo, item, relation], mediaArray))};
                }, function (error) {
                    l.log(false, "Error when finding relation to return:", error).despatch();
                    return unsuccessful(error.subqueryError);
                });
        }

        // ### Step 2: Create all media and find relation
        var toSave = [];
        var query = null;

        // * Look for a relation if conversation already exists.
        if (convoExisted) {
            query = new Parse.Query(constants.convoUserRelation.keys.className)
                .equalTo(constants.convoUserRelation.keys.conversation, convo)
                .equalTo(constants.convoUserRelation.keys.user, user);
        }

        var relationPromise
        if (query != null)
            relationPromise = query.first();

        return Parse.Promise.when(createMediaPromise, relationPromise).then(function (createdMediaResult, relation) {
            // ### Step 3: Create conversation
            var promise;

            if (convo == null) {
                var obj = new Parse.Object(constants.convo.keys.className)
                    .set(constants.base.keys.version, constants.convo.version)
                    .set(constants.base.keys.appVersion, req.device.version)
                    .set(constants.base.keys.device, req.device.device)
                    .set(constants.base.keys.os, req.device.os)
                    .set(constants.base.keys.osVersion, req.device.osVersion)
                    .set(constants.base.keys.localGuid, convoGuid)
                    .set(constants.base.keys.creator, user)
                    .set(constants.convo.keys.ThumbnailFile, params.thumbnail)
                    .set(constants.convo.keys.Name, params.name)
                    .set(constants.convo.keys.BondCount, 1)
                    .set(constants.convo.keys.generalCount, 1)
                    .set(constants.convo.keys.readableCount, 1)
                    .set(constants.convo.keys.blrtCount, 1)
                    .set(constants.convo.keys.ContentUpdate, now)
                    .set(constants.convo.keys.CreationType, 0)
                    .set(constants.convo.keys.StatusCode, 100)
                    .set(constants.convo.keys.LastMedia, createdMediaResult.foundAndCreatedMedia)
                    .set(constants.convo.keys.LastMediaArgument, JSON.stringify(createdMediaResult.dbArg))
                    .set(constants.convo.keys.incomplete, true);

                // if (blrtRequest) {
                //     callWebhookForBlrtRequest(blrtRequest, blrtRequest.get('customWebhook'), convo)
                // }


                promise = obj.save();
            } else
                promise = Parse.Promise.as(convo);

            return Parse.Promise.when(
                promise,
                // We'll also take this opportunity to fetch the BlrtRequest if it's needed.
                blrtRequest ?
                    blrtRequest.fetch() :
                    Parse.Promise.as(blrtRequest)
            ).then(function (convo, blrtRequest) {

                // If we've found a Blrt Request but it's already been answered, just pretend
                // we've never found it >.> We don't want to do anything with answered requests.
                if (blrtRequest && (blrtRequest.get("answered") || blrtRequest.get("allowMutipleReply")))
                    blrtRequest = null;

                // ### Step 4: Search for an existing role (if a conversation was found) and/or create a new role
                var roleSearchPromise = Parse.Promise.as();
                if (convoExisted) {
                    roleSearchPromise = new Parse.Query(Parse.Role)
                        .equalTo("name", "Conversation-" + convo.id)
                        .first();
                }

                return roleSearchPromise.then(function (role) {
                    var newRolePromise = Parse.Promise.as(role);
                    if (role == null) {
                        var newRole = new Parse.Role("Conversation-" + convo.id, new Parse.ACL());
                        newRole.getUsers().add(user);
                        newRolePromise = newRole.save();
                    }

                    return newRolePromise.then(function (role) {
                        // ### Step 5: Create item, relation and TimelyActions if necessary
                        toSave = [];
                        var promise;

                        var roleACL = new Parse.ACL();
                        roleACL.setReadAccess(role, true);
                        roleACL.setWriteAccess(role, true);

                        // * Create item if it wasn't found in the GUID check.
                        if (item == null) {
                            toSave.push(
                                item = new Parse.Object(constants.convoItem.keys.className)
                                    .set(constants.base.keys.version, constants.convoItem.version)
                                    .set(constants.base.keys.appVersion, req.device.version)
                                    .set(constants.base.keys.device, req.device.device)
                                    .set(constants.base.keys.os, req.device.os)
                                    .set(constants.base.keys.osVersion, req.device.osVersion)
                                    .set(constants.base.keys.localGuid, itemGuid)
                                    .set(constants.base.keys.creator, user)
                                    .set(constants.base.keys.encryptType, params.item.files.encryptType)
                                    .set(constants.base.keys.encryptValue, params.item.files.encryptValue)
                                    .set(constants.convoItem.keys.conversation, convo)
                                    .set(constants.convoItem.keys.Media, createdMediaResult.foundAndCreatedMedia)
                                    .set(constants.convoItem.keys.Argument, JSON.stringify(createdMediaResult.dbArg))
                                    .set(constants.convoItem.keys.type, 0)
                                    .set(constants.convoItem.keys.AudioFile, params.item.files.audio)
                                    .set(constants.convoItem.keys.TouchDataFile, params.item.files.touch)
                                    .set(constants.convoItem.keys.generalIndex, 0)
                                    .set(constants.convoItem.keys.readableIndex, 0)
                                    .set(constants.convoItem.keys.psuedoId, convo.id + "-" + 0)
                                    .setACL(roleACL)
                            );
                        }

                        savedObjects.push(item);

                        // * Create relation if it wasn't found.
                        if (relation == null) {
                            toSave.push(
                                relation = new Parse.Object(constants.convoUserRelation.keys.className)
                                    .set(constants.base.keys.version, constants.convoUserRelation.version)
                                    .set(constants.base.keys.creator, user)
                                    .set(constants.convoUserRelation.keys.conversation, convo)
                                    .set(constants.convoUserRelation.keys.convoCreator, user)
                                    .set(constants.convoUserRelation.keys.user, user)
                                    .set(constants.convoUserRelation.keys.indexViewedList, "0")
                                    .set(constants.convoUserRelation.keys.lastViewed, now)
                                    .set(constants.convoUserRelation.keys.lastEdited, now)
                                    .set(constants.convoUserRelation.keys.QueryType, "_creator")
                                    .set("contentUpdate", now)
                                    .setACL(roleACL)
                            );
                        } else {
                            relation.set("contentUpdate", now);
                            toSave.push(relation);
                        }


                        savedObjects.push(relation);

                        // Do we need to save anything?
                        if (toSave.length != 0)
                            promise = Parse.Object.saveAll(toSave);
                        else
                            promise = Parse.Promise.as();

                        return promise.then(function () {
                            // ### Step 6: Mark conversation as completed and set it's ACL
                            return convo
                                .unset(constants.convo.keys.incomplete)
                                .setACL(roleACL)
                                .set("conversationRole", role)
                                .save()
                                .then(function () {
                                    savedObjects.push(convo);

                                    // Before we finish, we'll also do some Mixpanel event tracking and BlrtRequest maintenance.
                                    var finalPromises = [];

                                    // finalPromises.push(pioneerJS.upgradeIfPioneerInviteeMakeBlrt(user, false));

                                    // We'll also increment the user's statistical counters here.
                                    finalPromises.push(
                                        user
                                            .increment(constants.user.keys.conversationCreationCounter)
                                            .increment(constants.user.keys.blrtCreationCounter)
                                            .save()
                                    );

                                    savedObjects.push(user);

                                    // finalPromises.push(mp.despatch());

                                    return Parse.Promise.when(finalPromises).always(function () {

                                        // * Create Blrt Request linking TimelyAction if necessary.
                                        if (blrtRequest) {
                                            l.log("Looks like a Blrt Request is attached. Creating a TimelyAction to link stuff up!");
                                            // console.log(JSON.stringify(blrtRequest))
                                            // console.log(JSON.stringify(blrtRequest.get('customWebhook')))
                                            // console.log(JSON.stringify(convo))



                                            callWebhookForBlrtRequest(blrtRequest, blrtRequest.get('customWebhook'), convo)
                                                .fail(function (requestError) {
                                                    console.log("ALERT: Webhook request failed: 1111", requestError)
                                                    l.log("ALERT: Webhook request failed: 1111", requestError);

                                                    // If the webhook request failed, we'll create another TimelyAction
                                                    // which can keep trying again (skipping all the other stuff).
                                                    return new Parse.Object("TimelyAction")
                                                        .set("executionTime", new Date())
                                                        .set("type", "linkBlrtRequest_retryWebhook")
                                                        .set("blrtRequest", request)
                                                        .save()
                                                        .fail(function (promiseError) {
                                                            l.log(false, "Failed to save retryWebhook TimelyAction:", promiseError).despatch();
                                                            return Parse.Promise.error("Failed to save retryWebhook TimelyAction: " + JSON.stringify(promiseError));
                                                        });
                                                })
                                        }
                                        return {success: true, objects: _.responsify(savedObjects)};
                                    });
                                }, function (error) {
                                    l.log(false, "Error when marking convo as complete:", error).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                        }, function (error) {
                            l.log(false, "Error when creating item/relation:", error).despatch();
                            return unsuccessful(error.subqueryError);
                        });
                    }, function (promiseError) {
                        l.log(false, "Error creating new role:", error).despatch();
                        return unsuccessful(error.subqueryError);
                    });
                }, function (promiseError) {
                    l.log(false, "Error searching for existing role:", error).despatch();
                    return unsuccessful(error.subqueryError);
                });
            }, function (error) {
                // console.log(error)
                l.log(false, "Error when creating convo:", error).despatch();
                return unsuccessful(error.subqueryError);
            });
        }, function (errors) {
            l.log(false, "Error(s) when creating media or finding relation:", errors).despatch();
            return unsuccessful(error.subqueryError);
        });
    }, function (errors) {
        l.log(false, "Error(s) when searching for matching local GUIDs:", errors).despatch();
        return unsuccessful(error.subqueryError);
    });
};


function callWebhookForBlrtRequest(request, webhookUrl, convo) {
    console.log("call callWebhookForBlrtRequest !!!!")
    console.log(webhookUrl)
    var waitPromises = [];
    waitPromises.push(convo.get("creator").fetch());
    var itemQuery = new Parse.Query("ConversationItem")
        .equalTo("conversation", convo)
        .ascending("createdAt")
        .first({useMasterKey:true});
    waitPromises.push(itemQuery);

    return Parse.Promise.when(waitPromises)
        .then(function ([creator, blrt]) {
            console.log("Blrt is imporantne")
            console.log(JSON.stringify(blrt))
            console.log("convo")
            console.log(JSON.stringify(convo))
            console.log("creatooooo")
            console.log(JSON.stringify(creator))
            if(!blrt){
                return Parse.Promise.error("blrt not found");
            }

            var duration;

            // try {
            //     var tmp = JSON.parse(convo.get("lastMediaArgument"));
            //     duration = tmp["AudioLength"];
            // } catch (e) {
            // }
            console.log("something before post request")

            try{
                console.log(JSON.stringify({
                    // endpoint: "/conv/request",
                    // title: convo.get("blrtName"),
                    // thumbnail: convo.get("thumbnailFile").url(),
                    url: constants.protocol + constants.siteAddress + "/conv/" + _.encryptId(convo.id) + "/blrt/" + _.encryptId(blrt.id),
                    // duration: duration,
                    // id: _.encryptId(request.id),
                    // authorName: creator.get("name"),
                    // authorEmail: creator.get("email")
                }))
            }catch (e){
                console.log("qqqqqqqqqq")
                console.log(JSON.stringify(e))
            }


            return Parse.Cloud.httpRequest({
                method: "POST",
                url: webhookUrl,
                body: {
                    // endpoint: "/conv/request",
                    // title: convo.get("blrtName"),
                    // thumbnail: convo.get("thumbnailFile").url(),
                    url: constants.protocol + constants.siteAddress + "/conv/" + _.encryptId(convo.id) + "/blrt/" + _.encryptId(blrt.id),
                    // duration: duration,
                    // id: _.encryptId(request.id),
                    // authorName: creator.get("name"),
                    // authorEmail: creator.get("email")
                }
            });
        }).fail(function (e) {
            console.log("!!!!!!!!!!!~!~!!!!!")
            console.log(JSON.stringify(e))
        });
}


