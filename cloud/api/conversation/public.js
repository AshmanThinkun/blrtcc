var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var ParseMutex = require("cloud/ParseMutex");
var curJS = require("cloud/conversationUserRelationLogic");


// # v1: conversationPublic
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationPublic

    // Subclasses APIError.
    var conversationPublicError = _.extend(api.error, { 
        conversationIdNotFound:              	{ code: 611, message: "The conversation id cannot be found." },
        notCreator:              		{ code: 612, message: "The requesting user is not the conversation creator." },
        publicChildExists:            { code: 613, message: "This conversation already has public Blrt" }
    });

   
    // Aliased as error for simplicity of internal use.
    var error = conversationPublicError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        var conversationId = params.conversationId;
        var disallowPublicBlrt = !params.allowPublicBlrt;

        //get conversation
        return new Parse.Query("Conversation")
            .equalTo("objectId", conversationId)
            //.include("item") not in use any
            .first({useMasterKey:true})
            .then(function (convo) {
                if(!convo) {
                    l.log(false, "Conversation id not found: ", conversationId).despatch();
                    return unsuccessful(error.conversationIdNotFound);
                }

                console.log('\n\nconvo disallowPublicBlrt==='+convo.get("disallowPublicBlrt"));
                console.log('disallowPublicBlrt disallowPublicBlrt==='+disallowPublicBlrt);

                // if the value matches what user wants to do, there is no need to update again
                if((convo.get("disallowPublicBlrt") && disallowPublicBlrt) || (!convo.get("disallowPublicBlrt") && !disallowPublicBlrt)){
                    console.log('\n\n\n===Allow Public Blrt===');
                    l.log(true, "Allow/Disallow Public Blrt: ", convo.id).despatch();
                    return {success: true, message:"Allow Public Blrt"}; 
                }

                if(user.id != convo.get("creator").id){
                    return unsuccessful(error.notCreator);
                }
				
				return ParseMutex.fnWithLock(convo, "publicBlrtManagement", function (convo, isDirty) {
                    var dirtyPromise = Parse.Promise.as();
                    if(isDirty) {
                        l.log("It was dirty! Cleaning before proceeding");
                        dirtyPromise = curJS.cleanDirtyConvos([convo.id]);
                    }

                    return dirtyPromise.then(function () {
                        //check whether there are public blrt child if disallowPublicBlrt is true
                        if(disallowPublicBlrt) {
                            return new Parse.Query("ConversationItem")
                                .equalTo("isPublicBlrt", true)
                                .equalTo("conversation", convo)
                                .count({useMasterKey:true})
                                .then(function(count){
                                    console.log('\n\npublic blrt count==='+count);
                                    if(count && count>0)
                                        return unsuccessful(error.publicChildExists);
                                    return convo.set("disallowPublicBlrt", disallowPublicBlrt)
                                        .save(null,{useMasterKey:true})
                                        .then(function () {
                                            l.log(true, "Allow/Disallow Public Blrt: ", convo.id).despatch();
                                            return {success: true, message:"Allow/Disallow Public Blrt"}; 
                                        }, function(error){
                                            l.log(false, "Error save conversation", error).despatch();
                                            return unsuccessful(error.subqueryError);
                                        });
                                }, function(error){
                                    l.log(false, "Error find conversation item", error).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                        }else {
                            console.log('\n\ndisallowPublicBlrt==='+disallowPublicBlrt);
                            return convo.set("disallowPublicBlrt", disallowPublicBlrt)
                                .save(null,{useMasterKey:true})
                                .then(function () {
                                    l.log(true, "Allow/Disallow Public Blrt: ", convo.id).despatch();
                                    return {success: true, message:"Allow/Disallow Public Blrt"}; 
                                }, function(error){
                                    l.log(false, "Error save conversation", error).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                        }
                            
                    }, function (promiseError) {
                        l.log(false, "Couldn't clean dirty convo:", promiseError).despatch();
                        return Parse.Promise.as({ success: false, error: error.subqueryError });
                    });
                }, function (promiseError) {
                    l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();
                    
                    if(_.isDate(promiseError)) {
                        // The mutual exclusion was in use by another calling function - we'll have to try again later.
                        return Parse.Promise.as({ success: false, error: error.subqueryError, tryAgainIn: promiseError });
                    }

                    return Parse.Promise.as({ success: false, error: error.subqueryError });
                });
            }, function(error){
                l.log(false, "Error finding conversation item: " + blrtId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();
