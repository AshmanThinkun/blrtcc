var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var ParseMutex = require("cloud/ParseMutex");
var convoUserRelJS = require("cloud/api/conversationUserRelation/create");

(function () {
    var params;
    var user;


    // Subclasses APIError.
    var error = _.extend(api.error, { 
        conversationIdNotFound:         { code: 611, message: "The conversation id cannot be found." },
        eventIdNotFound:                { code: 612, message: "The event id cannot be found." },
        alreadyTookAction:              { code: 613, message: "User already took an action"}
    });

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;



        params = req.params;
        user = req.user;
        

        var conversationId = _.decryptId(params.conversationId);
        var eventId = _.decryptId(params.eventId)



        var accept = params.accept;
        var token = params.token;

        var convo = false;

        var itemQuery =  new Parse.Query("ConversationItem")
            .equalTo("objectId",eventId)
            .include("creator");
        if(!user){
            //[Guichi]
            itemQuery.include("conversation")
                .include("conversation.creator");
        }
        return itemQuery.first({useMasterKey:true})
            .then(function(eventItem){
                if(!eventItem){
                    l.log(false, "Conversation item not found: ", eventId).despatch();
                    return unsuccessful(error.eventIdNotFound);
                }

                if(eventItem.get("status")!=0){
                    return unsuccessful(error.alreadyTookAction);
                }

                if(!user){
                    //token check
                    var now = new Date();
                    if(eventItem.get("requestTokenRetryCounter")>5){
                        return unsuccessful(error.invalidParameters);
                    }

                    if(eventItem.get("requestToken")!=token || eventItem.get("requestTokenCreateAt") < now.setDate(now.getDate()-5)){
                        return eventItem.increment("requestTokenRetryCounter")
                            .save()
                            .then(function (argument) {
                                l.log(false, "Invalid Token", token).despatch();
                                return unsuccessful(error.invalidParameters);
                            },function(error){
                                l.log(false, "Error saving Object", eventItem).despatch();
                                return unsuccessful(error.subqueryError);
                            });
                    }

                    convo = eventItem.get("conversation");
                    user = convo.get("creator");
                }

                var creator = eventItem.get("creator");

                var preparePromise;

                if(accept){
                    //first add user relation
                    var requestVars = {
                        user: user,
                        params: {
                            conversationId: conversationId,
                            relations: [{
                                type: "email",
                                value: creator.get("email").toLowerCase(),
                                name: creator.get("name")
                            }],
                            ignoreBlock: true
                        }
                    };
                    preparePromise = convoUserRelJS[1](requestVars);
                }else{
                    preparePromise = new Parse.Promise.as({success:true});
                } 

                return preparePromise
                .then(function (result){
                    //if coming from email, we should mark as viewed.
                    if(req.user){
                        return result;
                    }
                    return new Parse.Query("ConversationUserRelation")
                        .equalTo("conversation", convo)
                        .equalTo("user", user)
                        .first({useMasterKey:true})
                        .then(function(rel){
                            rel.remove("unreadMessages", eventItem.get(constants.convoItem.keys.generalIndex))
                            return rel.save(null,{useMasterKey:true});
                        }).then(function(){
                            return result;
                        });

                }).then(function (result) {
                    if(!result.success){
                        return result;
                    }

                    //2nd update eventItems
                    //change all the event status for this requester
                    //currently, the convo item created by this requester are all request event items
                    return new Parse.Query("Conversation")
                    .equalTo("objectId", conversationId)
                    .first({useMasterKey:true})
                    .then(function (convo) {
                        var eventsToSave = [];
                        return new Parse.Query("ConversationItem")
                            .equalTo("creator", creator)
                            .equalTo("conversation", convo)
                            .find({useMasterKey:true})
                            .then(function(requestEvents){
                                var status = accept? 1: 2;
                                _.each(requestEvents, function(preEvent){
                                    preEvent.set("status", status);
                                    eventsToSave.push(preEvent);
                                });

                                //add accept/deny event
                                if(!accept){
                                    convo.addUnique("requestAccessBlacks", creator);
                                }
                                return convo.increment(constants.convo.keys.generalCount)
                                    .set("contentUpdate", new Date())
                                    .save(null,{useMasterKey:true})
                                    .then(function(convo){
                                        return require("cloud/beforeSave").saveContentUpdateForConvo(convo)
                                            .then(function(){
                                                return convo;
                                            });
                                    }).then(function(convo){
                                        var update = _.map(requestEvents, function(theEvent){
                                            return {
                                                objectId: theEvent.id,
                                                status: status
                                            };
                                        });

                                        //add event
                                        var message = accept? 
                                            constants.convoItem.boldPrefix + "{0}"+constants.convoItem.boldPrefix+" granted " + creator.get("name") + " access to the conversation":
                                            "";


                                        var noteItem = new Parse.Object(constants.convoItem.keys.className)
                                            .set(constants.base.keys.version, constants.convoItem.version)
                                            .set(constants.base.keys.creator, user)
                                            .set(constants.convoItem.keys.conversation, convo)
                                            .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) -1)
                                            .set(constants.convoItem.keys.readableIndex, -1)
                                            .set(constants.convoItem.keys.type, 5)
                                            .set(constants.convoItem.keys.Argument, JSON.stringify({
                                                "Message" : message,
                                                "MessageType" : 4, // type 4 for update conversation item
                                                "Update": update,
                                                "Fallback" : {
                                                    "Message" : message,
                                                    "MessageType" : 1,
                                                }
                                            }));
                                        eventsToSave.push(noteItem);
                                        return Parse.Object.saveAll(eventsToSave,{useMasterKey:true}).then(function(){
                                            var message = "Access request " + (accept? "accepted" : "denied");
                                            return {success:true, message: message};
                                        },function (error) {
                                            l.log(false, "Error saving objects").despatch();
                                            return unsuccessful(error.subqueryError);
                                        });
                                    },function(error){
                                        l.log(false, "Error saving convo", convo).despatch();
                                        return unsuccessful(error.subqueryError);
                                    });
                            },function (error){
                                l.log(false, "requestEventsQuery failure ").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                    },function (error){
                        l.log(false, "Error finding conversation:", conversationId).despatch();
                        return unsuccessful(error.subqueryError);
                    });
                }, function (error) {
                    l.log(false, "error add relation:", error).despatch();
                    return unsuccessful(error.subqueryError);
                });
            },function (error){
                l.log(false, "error find conversation item:", error).despatch();
                return unsuccessful(error.subqueryError);
            });
    }
})();
