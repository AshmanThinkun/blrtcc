var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
// var utilJS = require("cloud/util");
var ParseMutex = require("cloud/ParseMutex");
// var Mixpanel = require("cloud/mixpanel");
// var curJS = require("cloud/conversationUserRelationLogic");
var permissionsJS = require("cloud/permissions");
var emailJS = require("cloud/email");

(function () {
    var params;
    var user;


    // Subclasses APIError.
    var requestAccessError = _.extend(api.error, { 
        conversationIdNotFound:         { code: 611, message: "The conversation id cannot be found." },
        userInBlackList:              	{ code: 612, message: "The requesting user is blocked by the conversation owner" },
        conversationFull:               { code: 613, message: "This conversation is full" }
    });

    // api success
    // 555: relation exist
    // 556: request sent
   
    // Aliased as error for simplicity of internal use.
    var error = requestAccessError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        var conversationId = _.decryptId(params.conversationId);

        //get conversation
        return new Parse.Query("Conversation")
            .equalTo("objectId", conversationId)
            .include("creator")
            .first({useMasterKey:true})
            .then(function (convo) {
                if(! convo){
                    return unsuccessful(error.conversationIdNotFound);
                }

                var creator = convo.get("creator");

                //check blackList
                var blackList = convo.get("requestAccessBlacks");
                if(blackList && _.find(blackList, function(tempUser){ return tempUser.id == user.id; })){
                    l.log(false, "User in black list: ", user.id).despatch();
                    return unsuccessful(error.userInBlackList);
                }

                //check userRelation existance
                return new Parse.Query("ConversationUserRelation")
                    .equalTo("conversation", convo)
                    .equalTo("user", user)
                    .first({useMasterKey:true})
                    .then(function(relationExist){
                        if(relationExist){
                            l.log(true, "user relation already exist, relation: ", relationExist.id).despatch();
                            return {
                                success: true, 
                                status: {
                                    code: "555",
                                    message: "user relation exist."
                                }
                            };
                        }

                        //check conversation full
                        return new Parse.Query("ConversationUserRelation")
                            .equalTo("conversation", convo)
                            .equalTo("user", convo.get("creator"))
                            .first({useMasterKey:true})
                            .then(function(creatorRel){
                                return permissionsJS.getSinglePermissions({ user: user, other: creatorRel }, true)
                                    .then(function (permissions) {
                                        if(
                                            convo.get("bondCount") +1 >
                                            permissions[constants.permissions.values.maxConvoUsers]
                                        ) {
                                            l.log(false, "There would be too many relations in this conversation if we added them").despatch();
                                            return unsuccessful(error.conversationFull);
                                        }

                                        //now we can add the request
                                        //create event
                                        return convo.increment(constants.convo.keys.generalCount)
                                            .set("contentUpdate", new Date())
                                            .set("hasConversationBegan", true)
                                            .save(null,{useMasterKey:true})
                                            .then(function(convo){
                                                return require("cloud/beforeSave").saveContentUpdateForConvo(convo)
                                                    .then(function(){
                                                        return convo;
                                                    });
                                            }).then(function(convo){
                                                //add event

                                                //generate token
                                                var buf = require('crypto').randomBytes(48);
                                                var token = buf.toString('hex');

                                                var saveNoteNRel = [];

                                                let noteItem = new Parse.Object(constants.convoItem.keys.className)
                                                    .set(constants.base.keys.version, constants.convoItem.version)
                                                    .set(constants.base.keys.creator, user)
                                                    .set(constants.convoItem.keys.conversation, convo)
                                                    .set(constants.convoItem.keys.generalIndex, convo.get(constants.convo.keys.generalCount) -1)
                                                    .set(constants.convoItem.keys.readableIndex, -1)
                                                    .set(constants.convoItem.keys.type, 5)
                                                    //[Guichi]mean can be displayed
                                                    .set("status", 0)
                                                    .set("requestToken", token)
                                                    .set("requestTokenCreateAt", new Date())
                                                    .set("requestTokenRetryCounter", 0)
                                                    .set(constants.convoItem.keys.Argument, JSON.stringify({
                                                        "Message" : user.get("name") + " has requested access to this conversation.",
                                                        "MessageType": -999,
                                                        "Fallback" : {
                                                            "Message" : constants.convoItem.boldPrefix + "{0}"+constants.convoItem.boldPrefix+" ({1}) has requested access to this conversation.",
                                                            "MessageType" : 3,// if message type 3, you need to show accept / deny buttons for convo creator
                                                            "Fallback" : {
                                                                "Message" : constants.convoItem.boldPrefix + "{0}"+constants.convoItem.boldPrefix+" has requested access to this conversation.",
                                                                "MessageType" : 1,
                                                            }
                                                        }
                                                    }));

                                                creatorRel.addUnique("unreadMessages", noteItem.get(constants.convoItem.keys.generalIndex));


                                                saveNoteNRel.push(noteItem);
                                                saveNoteNRel.push(creatorRel); 
                                                
                                                return Parse.Object.saveAll(saveNoteNRel,{useMasterKey:true}).then(function(){
                                                    //2nd send push
                                                    var message = emailJS.GeneratePush("PM", {
                                                        t: 1, 
                                                        bid: convo.id, 
                                                    }, [
                                                        user.get("name") + " requested access to your conversation"
                                                    ]);
                                                    
                                                    // We send it to all devices owned by receiving users.
                                                    var p = Parse.Push.send({
                                                        where: new Parse.Query(Parse.Installation)
                                                            .equalTo("owner", 
                                                                convo.get("creator")),
                                                        data: message
                                                    }, { useMasterKey: true }).then(function(pu){
                                                        l.log("sent push successfully", pu).despatch();
                                                    },function (error) {
                                                        l.log("ALERT: Error sending relation push notifications:", error).despatch();
                                                    });

                                                    //send email
                                                    var processLink = constants.protocol + constants.siteAddress + "/processConvoRequest?token="+encodeURIComponent(token)
                                                        + "&e=" + _.encryptId(noteItem.id) + "&c=" + _.encryptId(convo.id); 

                                                    var e =  emailJS.SendTemplateEmail({
                                                        template:       "convo-request-access",
                                                        convo:          convo,
                                                        content:        null,
                                                        recipients:     [{
                                                            id:         creator.id, 
                                                            name:       creator.get("name"), 
                                                            email:      creator.get("email"), 
                                                            isUser:     true,
                                                            user:       creator
                                                        }],
                                                        sender:         user,
                                                        customMergeVars: {
                                                            "CONVO_LINK": constants.protocol+ constants.siteAddress + "/conv/" + _.encryptId(convo.id),
                                                            "REQUESTER_NAME":  user.get("name"),
                                                            "REQUESTER_EMAIL": user.get("email"),
                                                            "BLRT_TITLE": convo.get("blrtName"),
                                                            "BLRT_THUMBNAIL": convo.get("thumbnailFile")? convo.get("thumbnailFile").url(): "",
                                                            "ACCEPT_LINK": processLink + "&accept=1",
                                                            "DENY_LINK": processLink + "&accept=2",
                                                        }
                                                    });
                                                    
                                                    return Parse.Promise.when([p,e]).then(function(){
                                                        return {
                                                            success: true, 
                                                            status: {
                                                                code: "556",
                                                                message: "request sent."
                                                            }
                                                        };
                                                    },function (error) {
                                                        l.log("ALERT: Error send existing user emails:", error).despatch();
                                                        return unsuccessful(error.subqueryError);
                                                    });
                                                },function(error){
                                                    l.log(false, "Error save event: ", noteItem).despatch();
                                                    return unsuccessful(error.subqueryError);
                                                });
                                            },function(error){
                                                l.log(false, "Error save convo:", convo.id).despatch();
                                                return unsuccessful(error.subqueryError);
                                            });
                                    },function(error){
                                        l.log(false, "Error getting permissions:", creatorRel).despatch();
                                        return unsuccessful(error.subqueryError);
                                    });
                            },function(error){
                                l.log(false, "Error finding conversation user relation").despatch();
                                return unsuccessful(error.subqueryError);
                            });
                    },function(error){
                        l.log(false, "Error getting realtion").despatch();
                        return unsuccessful(error.subqueryError);
                    });                
            }, function(error){
                l.log(false, "Error finding conversation: " + conversationId).despatch();
                return unsuccessful(error.subqueryError);
            });
        }
})();
