var _ = require("underscore");

var api = require("cloud/api/util");
var requestGenerator = require("cloud/requestGenerator");

// # v1: conversationRequest
(function () {
    var user;
    var params;

    // ## Enums

    // We'll just use the standard API errors here - no special cases.
    var error = api.error;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        user = req.user;
        params = req.params;

        if(params.type == "api.blrt.com") {
            if(!params.developerId) {
                l.log(false, "api.blrt.com request received without a developerId:", req).despatch();
                return Parse.Promise.as({ success: false, error: error.invalidParameters });
            }

            params.user = params.developerId;
            delete params.developerId;
        }

        return requestGenerator.generate({ params: params }, params.type).then(function (result) {
            if(result.success)
                return {
                    success: true,
                    url: result.link,
                    id: _.encryptId(result.requestId)
                };
            else
                return result;
        }, function (functionError) {
            l.log(false, "An error occurred when generating the request:", functionError).despatch();
            return Parse.Promise.as({ success: false, error: error.subqueryError });
        });
    };
})();