const error = require("cloud/api/util").error;
const constants = require("cloud/constants");

module.exports[1] = (req)=> {
    const {params, user}=req;
    const {convoId, name}=params;
    const now=new Date();
    let oldName;

    // console.log(convoId +" : "+name);
    // console.log(now);
    // console.log('user: '+user);

    return new Parse.Query('Conversation')
        .get(convoId,{useMasterKey:true})
        .then((convo)=> {
            oldName=convo.get('blrtName');
            console.log('oldName==='+oldName);
            if (convo.get('creator').id !== user.id) {
                return error.authenticationRequired
            } else {
                const convoItemCount = convo.get(constants.convo.keys.generalCount);
                convo.set('blrtName', name)
                    .set(constants.base.keys.version, constants.convo.version)
                    .increment(constants.convo.keys.generalCount)
                    .set(constants.convo.keys.ContentUpdate, now)

                var messageItem = new Parse.Object('ConversationItem')
                    .set(constants.base.keys.version, constants.convoItem.version)
                    .set(constants.base.keys.creator, user)
                    .set(constants.convoItem.keys.conversation, convo)
                    .set(constants.convoItem.keys.generalIndex, convoItemCount)
                    .set(constants.convoItem.keys.readableIndex, -1)
                    .set(constants.convoItem.keys.type, 5)
                    .set(constants.convoItem.keys.Argument, JSON.stringify({
                    	MessageType:6,
                        OldName:oldName,
                        NewName:name,
                        ChangingInitiator:user.get('name')||user.get('username'),
                        Fallback:{
                            "Message": `rename conversation to ${params.name}`,
                            "MessageType": -1
                        }

                    }))

                new Parse.Query('ConversationUserRelation')
                         .equalTo('conversation', convo)
                         .equalTo('user', user)
                         .first({useMasterKey: true})
                         .then(relation=>{
                            // console.log('relation relation length==='+relation.length);
                            // console.log('relation before contentUpdate==='+relation.get('contentUpdate'));
                            relation.set('contentUpdate', now);
                            // console.log('\nrelation after contentUpdate==='+relation.get('contentUpdate'));
                            return Parse.Promise.when(relation.save(null, {useMasterKey: true}));
                         }).fail(e=>{
                            console.log(e);
                         })

                return Parse.Promise.when(convo.save(null,{useMasterKey:true}),messageItem.save(null,{useMasterKey:true}))
                    .then((save)=>{
                        return {
                            success:true,
                            message:'rename success'
                        }
                    })
            }

        })

        .catch((e)=>{
            // console.log("I want to know the error")
            // console.log(e)
                return error.subqueryError
            })
}