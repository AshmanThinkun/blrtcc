// var emailJS = require("cloud/email");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
// var pioneerJS = require("cloud/pioneer");
// var Mixpanel = require("cloud/mixpanel");

// # v1: conversationDuplicateFromBlrt
(function () {
    var params;
    var user;
    var now;

    // ## Enums

    // ### conversationDuplicateFromBlrtError

    // Subclasses APIError.
    var conversationDuplicateFromBlrtError = _.extend(api.error, { 
        itemNotFound:               { code: 404, message: "No conversationItem was found with the provided ID." },
        invalidItemArg:             { code: 410, message: "The provided item does not reference a Blrt or Blrt Reply." },
        invalidName:                { code: 411, message: "The provided name exceeds the " + constants.convo.nameCharLimit + " character limit." },
        invalidOwner:               { code: 412, message: "The creator of the Blrt or Blrt Reply is not the user who made this request." }
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationDuplicateFromBlrtError;

    // ## Request examples

    // } TODO: Provide examples

    // ## Response examples

    // } TODO: Provide examples

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        var now = new Date();

        params = req.params;
        user = req.user;
        now = new Date();


        userPromise = Parse.Promise.as(user);
        if(req.master){
            userPromise = new Parse.Query(Parse.User)
                .equalTo("objectId", params.userId)
                .first();
        }else if(!user){
            // client call needs auth
            return Parse.Promise.as({
                success: false,
                error: error.invalidOwner
            });
        }


        Parse.Cloud.useMasterKey();

        // The flow of creation can be split into 6 major steps:
        // 0.5. Ensure names are valid;
        // 1. Find conversationItem and verify;
        // 2. Create all conversations, marked as incomplete;
        // 3. Create all roles;
        // 4. Create all conversationItems and relations using the roles;
        // 5. Apply roles to conversations and mark them as completed; and
        // 6. Mixpanel track!

        // ### Step 0.5: Ensure names are valid
        // * No names? Guess we've successfully duplicated then! Hehe >.>
        if(params.names.length == 0) {
            return Parse.Promise.as({ success: true });
        }

        // * Make sure they all obey the name character limit.
        if(_.some(params.names, function (name) { return name.length == 0 || name.length > constants.convo.nameCharLimit; })) {
            l.log(false, "Found a conversation name that exceeds the character limit (or has 0 length)!").despatch();
            return Parse.Promise.as({
                success: false,
                error: error.invalidName
            });
        }

        //client only allow 1 name
        if(!req.master && params.names.length>1){
            return Parse.Promise.as({
                success: false,
                error: error.invalidName
            });
        }


    return userPromise
        .then(function (user) {
        // ### Step 1: Find conversation and verify
        return new Parse.Query("ConversationItem")
            .equalTo("objectId", params.conversationItemId)
            .include("conversation")
            .first()
            .then(function (convoItem) {
                // * Didn't find it?
                if(!convoItem) {
                    l.log(false, "Requested convoItem not found!").despatch();
                    return {
                        success: false,
                        error: error.itemNotFound
                    };
                }

                // * Not a Blrt or Blrt Reply?
                if(convoItem.get("type") != constants.convoItem.types.blrt && convoItem.get("type") != constants.convoItem.types.reBlrt) {
                    l.log(false, "Requested item is not a Blrt or Blrt Reply!").despatch();
                    return {
                        success: false,
                        error: error.invalidItemArg
                    }
                }

                // * Created by someone else?
                if(convoItem.get("creator").id != user.id) {
                    l.log(false, "convoItem creator is NOT requesting user!").despatch();
                    return {
                        success: false,
                        error: error.invalidOwner
                    };
                }

                // * Nope? All good! Lets continue!
                // ### Step 2: Create all conversations, marked as incomplete
                var media = convoItem.get("media");
                var mediaArgument = convoItem.get("argument");
                var thumbnailFile = convoItem.get("thumbnailFile");
                var audioFile = convoItem.get("audioFile");
                var touchDataFile = convoItem.get("touchDataFile");
                var encryptType = convoItem.get("encryptType");
                var encryptValue = convoItem.get("encryptValue");

                if(!thumbnailFile)
                    thumbnailFile = convoItem.get("conversation").get("thumbnailFile");

                var convosToCreate = _.map(params.names, function (name) {
                    return new Parse.Object("Conversation")
                        .set("blrtCount", 1)
                        .set("blrtName", name)
                        .set("bondCount", 1)
                        .set("contentCount", 1)
                        .set("contentUpdate", now)
                        .set("creationType", constants.convo.creationTypes.duplicate)
                        .set("creator", user)
                        .set("dbversion", constants.convo.version)
                        .set("device", req.device.device)
                        .set("lastMedia", media)
                        .set("lastMediaArgument", mediaArgument)
                        .set("os", req.device.os)
                        .set("osVersion", req.device.osVersion)
                        .set("appVersion", req.device.version)
                        .set("readableCount", 1)
                        .set("statusCode", constants.convo.statusCodes.normal)
                        .set("thumbnailFile", thumbnailFile)
                        // Initially incomplete!
                        .set("incomplete", true);
                });

                return Parse.Object.saveAll(convosToCreate).then(function (convos) {
                    // ### Step 3: Create all roles
                    var roles = _.map(convos, function (convo) {
                        var newRole = new Parse.Role("Conversation-" + convo.id, new Parse.ACL());
                        newRole.getUsers().add(user);
                        return newRole;
                    });

                    return Parse.Object.saveAll(roles).then(function (roles) {
                        roles = _.reduce(roles, function (memo, role) {
                            memo[role.getName()] = role;
                            return memo;
                        }, {});

                        // ### Step 4: Create all conversationItems and relations using the roles

                        // * ConversationItems first (for no particular reason).
                        var convoItems = _.map(convos, function (convo) {
                            var roleACL = new Parse.ACL();
                            roleACL.setReadAccess(roles["Conversation-" + convo.id], true);
                            roleACL.setWriteAccess(roles["Conversation-" + convo.id], true);

                            return new Parse.Object("ConversationItem")
                                .set("argument", mediaArgument)
                                .set("audioFile", audioFile)
                                .set("conversation", convo)
                                .set("creator", user)
                                .set("dbversion", constants.convoItem.version)
                                .set("encryptType", encryptType)
                                .set("encryptValue", encryptValue)
                                .set("index", 0)
                                .set("media", media)
                                .set("psuedoId", convo.id + "-0")
                                .set("readableIndex", 0)
                                .set("thumbnailFile", thumbnailFile)
                                .set("touchDataFile", touchDataFile)
                                .set("type", constants.convoItem.types.blrt)
                                .setACL(roleACL);
                        });

                        // * Now ConversationUserRelations!
                        var convoUserRelations = _.map(convos, function (convo) {
                            var roleACL = new Parse.ACL();
                            roleACL.setReadAccess(roles["Conversation-" + convo.id], true);
                            roleACL.setWriteAccess(roles["Conversation-" + convo.id], true);

                            return new Parse.Object("ConversationUserRelation")
                                .set("conversation", convo)
                                .set("convoCreator", user)
                                .set("creator", user)
                                .set("dbversion", constants.convoUserRelation.version)
                                .set("indexViewedList", "0")
                                .set("lastEdited", now)
                                .set("lastViewed", now)
                                .set("queryType", "_creator")
                                .set("user", user)
                                .set("contentUpdate", now)
                                .setACL(roleACL);
                        });

                        // * Save everything!
                        return Parse.Object.saveAll(_.union(convoItems, convoUserRelations)).then(function () {
                            // ### Step 5: Apply roles to conversations and mark them as completed
                            var resultConvoIds = [];

                            _.each(convos, function (convo) {
                                var roleACL = new Parse.ACL();
                                roleACL.setReadAccess(roles["Conversation-" + convo.id], true);
                                roleACL.setWriteAccess(roles["Conversation-" + convo.id], true);

                                convo
                                    .set("incomplete", false)
                                    .set("conversationRole", roles["Conversation-" + convo.id])
                                    .setACL(roleACL);
                                resultConvoIds.push(convo.id);
                            });

                            return Parse.Object.saveAll(convos).then(function () {
                                // ### Step 6: Mixpanel track!
                                // return new Mixpanel()
                                //     .track({
                                //         event: "Conversation Duplicated",
                                //         properties: {
                                //             "# Duplicates": params.names.length
                                //         }
                                //     }, { id: user.id })
                                //     .fail(function (trackError) {
                                //         l.log("ALERT: Failed to track Conversation Duplicated:", trackError).despatch();
                                //     })
                                //     .always(function () {

                                //         return { success: true, convoIds: resultConvoIds };
                                //     })

                                return { success: true, convoIds: resultConvoIds }

                            }, function (promiseError) {
                                l.log(false, "Failed to complete conversations and set roles:", promiseError).despatch();
                                return Parse.Promise.as({
                                    success: false,
                                    error: error.subqueryError
                                });
                            });
                        }, function (promiseError) {
                            l.log(false, "Failed to convoItems and convoUserRelations:", promiseError).despatch();
                            return Parse.Promise.as({
                                success: false,
                                error: error.subqueryError
                            });
                        });
                    }, function (promiseError) {
                        l.log(false, "Failed to save roles:", promiseError).despatch();
                        return Parse.Promise.as({
                            success: false,
                            error: error.subqueryError
                        });
                    });
                }, function (promiseError) {
                    l.log(false, "Failed to create conversations:", promiseError).despatch();
                    return Parse.Promise.as({
                        success: false,
                        error: error.subqueryError
                    });
                });
            }, function (promiseError) {
                l.log(false, "Failed to retrieve convoItem:", promiseError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });

}); // TODO: REMOVE THIS LINE
    };
})();
