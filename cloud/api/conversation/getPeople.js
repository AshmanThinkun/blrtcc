/******************************
 * fetch all the people in a conversation
 ******************************/

var _ = require("underscore");
var api = require("cloud/api/util");


var error = api.error;
exports[1] = function (req) {
    var params = req.params;
    var requestUser = req.user;

    var convoStub =new Parse.Object("Conversation");
    convoStub.id = params.conversationId;


    var peopleInThisConversation = [];


    return new Parse.Query("ConversationUserRelation")
        .equalTo("conversation", convoStub)
        .include("user")
        .find({useMasterKey:true})
        .then(function (relations) {
            var hasAccess = false;
            relations.forEach(function (relation) {
                var person = {};
                person.relationId=_.encryptId(relation.id);
                var user = relation.get("user");
                // console.log("\n\n\n\n");
                // console.log('user==='+JSON.stringify(user));
                //we require type and value because we need to start new convo with those relation 
                person['type']=relation.get('queryType');

                // console.log('========================================');
            
                if (!user) {
                    person['value']=relation.get('queryValue');
                    // console.log('queryValue==='+relation.get('queryValue'));
                    person["name"] = relation.get("queryValue");
                    person["status"] = "Not signed up";
                }
                else {
                    //email should be got from user.email not queryValue...
                    person['value']=user.get('email');
                    // console.log('email==='+user.get('email'));
                    person.id=user.id;
                    if (relation.get("user").id == requestUser.id) {
                        hasAccess = true;
                    }
                    person["name"] = relation.get("user").get("name") || relation.get("user").get("username");
                    person["avatar"] = relation.get("user").get("avatar");
                    if (relation.get("user").get('accountStatus')==='deleted'){
                        person['deletedUser'] = true;
                    } else {
                        person['deletedUser'] = false;
                    }
                    if (relation.get("queryType") == "_creator") {
                        person["status"] = "Creator";
                    }
                    else {
                        if (relation.get("indexViewedList")) {
                            // person["id"] = relation.get("user").id
                            person["status"] = "Viewed"
                        }
                        else {
                            person["status"] = "Not viewed"
                        }
                    }
                }

                // console.log("\n\n\n\n");
                // console.log(JSON.stringify(person));
                peopleInThisConversation.push(person);

            })

            if (!hasAccess)
                return new Parse.Promise.error({errorType: error.noAccess})


            var conversationItemPromises = [];
            peopleInThisConversation.forEach(function (person) {
                if (person.id) {
                    var userStub = new Parse.Object("_User");
                    userStub.id = person.id;
                    conversationItemPromises.push(
                        new Parse.Query("ConversationItem")
                            .equalTo("creator", userStub)
                            .equalTo("conversation",convoStub )
                            .find({useMasterKey:true})
                            .then(
                                function (result) {
                                    if (result.length>0 && person.status != "Creator") {
                                        person.status = "Contributor";
                                    }
                                }
                            )
                    )
                }
            })
            return Parse.Promise.when(conversationItemPromises)
                .then(function () {
                    return peopleInThisConversation;
                })
        })


        .fail(function (error) {
            return Parse.Promise.as({success: false, error: error})
        })



}


