var constants = require("cloud/constants");
var api = require("cloud/api/util");
var _ = require("underscore");

// # v1: conversationCreate
// ## Enums

// ### conversationCreateWithBlrtError
var error = _.extend(api.error, {
    invalidItemArg: {
        code: 410,
        message: "The provided item argument is invalid or does not cover each provided media asset."
    },
    invalidName: {
        code: 411,
        message: "The provided name exceeds the " + constants.convo.nameCharLimit + " character limit."
    }
});

// ## Request examples

// } TODO: Provide examples

// ## Response examples

// } TODO: Provide examples

// ## Flow
exports[1] = function (req) {
    //[Guichi] return to client ,the client need this to calculate the success part,i.e. replacing some
    //localGuid with real mediaId
    var savedObjects = [];

    function unsuccessful(error) {
        var response = {
            success: false,
            error: error
        };

        if (savedObjects.length != 0)
            response.objects = _.responsify(savedObjects);

        return Parse.Promise.as(response);
    }

    var l = api.loggler;
    var now = new Date();


    const {params, user}=req;

    // The flow of creation can be split into 6 major steps:
    // 1. Find all objects with matching local GUIDs;
    // 2. Try to find a relation if an existing conversation was found.
    // 3. Create the conversation (if it wasn't found in step 1.).
    // 4. Search for an existing role (if a conversation was found) and/or create a new role;
    // 5. Create conversation item and relation with ACLs.
    // 6. Mark conversation as completed and set it's ACL.
    // We'll also update all statistics in this final step.

    // All creation in a single step can be combined into a single saveAll.
    // Other queries in a single step can be performed simultaneously (i.e. no dependency).


    // ### Step 1: Matching local GUIDs
    var convoGuid = params.localGuid;                   // Conversation GUID
    return new Parse.Query(constants.convo.keys.className)
        .equalTo(constants.base.keys.localGuid, convoGuid)
        .first({useMasterKey:true})
        .then(function (convo) {
            var convoExisted = convo != null;
            if (convo != null && convo.get(constants.convo.keys.incomplete) != true) {
                // It's probably a duplicate creation attempt if the conversation was found completed.
                // Unfortunately we now have to look for the relation so we can return all of the
                // objects that the client may need :(
                return new Parse.Query(constants.convoUserRelation.keys.className)
                    .equalTo(constants.convoUserRelation.keys.conversation, convo)
                    .equalTo(constants.convoUserRelation.keys.user, user)
                    .first({useMasterKey:true})
                    .then(function (relation) {
                        l.log(true, "Completed by finding existing completed conversation: " + convo.id).despatch();
                        return {success: true, objects: _.responsify(_.union([convo, relation]))};
                    }, function (error) {
                        l.log(false, "Error when finding relation to return:", error).despatch();
                        return unsuccessful(error.subqueryError);
                    });
            }

            // ### Step 2: Create all media and find relation
            var toSave = [];
            var query = null;

            // * Look for a relation if conversation already exists.
            if (convoExisted) {
                query = new Parse.Query(constants.convoUserRelation.keys.className)
                    .equalTo(constants.convoUserRelation.keys.conversation, convo)
                    .equalTo(constants.convoUserRelation.keys.user, user);
            }

            var relationPromise=new Parse.Promise.as(false)
            if (query != null)
                relationPromise = query.first({useMasterKey:true});

            return relationPromise.then(function (relation) {
                // ### Step 3: Create conversation
                var convoCreationPromise;

                console.log("show me relation")
                console.log(JSON.stringify(relation))

                if (convo == null) {
                    console.log("try to create new object")
                    var obj = new Parse.Object(constants.convo.keys.className)
                        .set(constants.base.keys.version, constants.convo.version)
                        .set(constants.base.keys.appVersion, req.device.version)
                        .set(constants.base.keys.device, req.device.device)
                        .set(constants.base.keys.os, req.device.os)
                        .set(constants.base.keys.osVersion, req.device.osVersion)
                        .set(constants.base.keys.localGuid, convoGuid)
                        .set(constants.base.keys.creator, user)
                        .set(constants.convo.keys.Name, params.name)
                        .set(constants.convo.keys.BondCount, 1)
                        .set(constants.convo.keys.generalCount, 0)
                        .set(constants.convo.keys.readableCount, 0)
                        .set(constants.convo.keys.blrtCount, 0)
                        .set(constants.convo.keys.ContentUpdate, now)
                        .set(constants.convo.keys.CreationType, 0)
                        .set(constants.convo.keys.StatusCode, 100)
                        .set(constants.convo.keys.incomplete, true);

                
                    if(params.ctags){
                        const tags=params.ctags.split('.')
                        obj.set('tag', tags);
                    }
                    convoCreationPromise = obj.save(null,{useMasterKey:true,success:function (r) {
                        console.log(r)
                    },error:function (e) {
                        console.log("+++++++")
                    }});

                } else
                    convoCreationPromise = Parse.Promise.as(convo);


                return convoCreationPromise.then(function (convo) {
                    console.log("new convo here")
                    console.log(JSON.stringify(convo))
                    // If we've found a Blrt Request but it's already been answered, just pretend
                    // we've never found it >.> We don't want to do anything with answered requests.

                    // ### Step 4: Search for an existing role (if a conversation was found) and/or create a new role
                    var roleSearchPromise = Parse.Promise.as();
                    if (convoExisted) {
                        roleSearchPromise = new Parse.Query(Parse.Role)
                            .equalTo("name", "Conversation-" + convo.id)
                            .first({useMasterKey:true});
                    }

                    return roleSearchPromise.then(function (role) {
                        var newRolePromise = Parse.Promise.as(role);
                        if (role == null) {
                            var newRole = new Parse.Role("Conversation-" + convo.id, new Parse.ACL());
                            newRole.getUsers().add(user);
                            newRolePromise = newRole.save(null,{useMasterKey:true});
                        }

                        return newRolePromise.then(function (role) {
                            // ### Step 5: Create item, relation and TimelyActions if necessary
                            toSave = [];
                            var promise;

                            var roleACL = new Parse.ACL();
                            roleACL.setReadAccess(role, true);
                            roleACL.setWriteAccess(role, true);


                            // * Create relation if it wasn't found.
                            if (!relation) {
                                toSave.push(
                                    relation = new Parse.Object(constants.convoUserRelation.keys.className)
                                        .set(constants.base.keys.version, constants.convoUserRelation.version)
                                        .set(constants.base.keys.creator, user)
                                        .set(constants.convoUserRelation.keys.conversation, convo)
                                        .set(constants.convoUserRelation.keys.convoCreator, user)
                                        .set(constants.convoUserRelation.keys.user, user)
                                        .set(constants.convoUserRelation.keys.indexViewedList, "0")
                                        .set(constants.convoUserRelation.keys.lastViewed, now)
                                        .set(constants.convoUserRelation.keys.lastEdited, now)
                                        .set(constants.convoUserRelation.keys.QueryType, "_creator")
                                        .set("contentUpdate", now)
                                        .setACL(roleACL)
                                );
                            } else {
                                relation.set("contentUpdate", now);
                                toSave.push(relation);
                            }


                            savedObjects.push(relation);

                            // Do we need to save anything?
                            if (toSave.length != 0)
                                promise = Parse.Object.saveAll(toSave);
                            else
                                promise = Parse.Promise.as();

                            return promise.then(function () {
                                // ### Step 6: Mark conversation as completed and set it's ACL
                                return convo
                                    .unset(constants.convo.keys.incomplete)
                                    .setACL(roleACL)
                                    .set("conversationRole", role)
                                    .save(null,{useMasterKey:true})
                                    .then(function () {
                                        savedObjects.push(convo);

                                        // Before we finish, we'll also do some Mixpanel event tracking and BlrtRequest maintenance.
                                        var finalPromises = [];


                                        // We'll also increment the user's statistical counters here.
                                        finalPromises.push(
                                            user
                                                .increment(constants.user.keys.conversationCreationCounter)
                                                .save(null,{useMasterKey:true})
                                        );

                                        savedObjects.push(user);

                                        // finalPromises.push(mp.despatch());

                                        return Parse.Promise.when(finalPromises).always(function () {
                                            return {success: true, objects: _.responsify(savedObjects)};
                                        });
                                    }, function (error) {
                                        l.log(false, "Error when marking convo as complete:", error).despatch();
                                        return unsuccessful(error.subqueryError);
                                    });
                            }, function (error) {
                                l.log(false, "Error when creating item/relation:", error).despatch();
                                return unsuccessful(error.subqueryError);
                            });
                        }, function (promiseError) {
                            l.log(false, "Error creating new role:", error).despatch();
                            return unsuccessful(error.subqueryError);
                        });
                    }, function (promiseError) {
                        l.log(false, "Error searching for existing role:", error).despatch();
                        return unsuccessful(error.subqueryError);
                    });
                }, function (error) {
                    console.log("Got it ")
                    console.log(error)
                    l.log(false, "Error when creating convo:", error).despatch();
                    return unsuccessful(error.subqueryError);
                });
            }, function (errors) {
                l.log(false, "Error(s) when creating media or finding relation:", errors).despatch();
                return unsuccessful(error.subqueryError);
            });
        }, function (errors) {
            l.log(false, "Error(s) when searching for matching local GUIDs:", errors).despatch();
            return unsuccessful(error.subqueryError);
        });
};


