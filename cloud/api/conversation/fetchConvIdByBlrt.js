var _ = require("underscore");
exports[1] = function fetchConvIdByBlrt(req) {
    Parse.Cloud.useMasterKey();
    var params=req.params;
    var blrtId=params.blrtId;
    return new Parse.Query("ConversationItem")
        .get(blrtId)
        .then(function (item) {
            var convoId=item.get("conversation").id
            return {convoId:_.encryptId(convoId)}
        })
    
}






