var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var ParseMutex = require("cloud/ParseMutex");
var curJS = require("cloud/conversationUserRelationLogic");

// # v1: conversationDelete
(function () {
    var params;
    var user;

    // ## Enums

    // ### conversationDelete

    // Subclasses APIError.
    var conversationDeleteError = _.extend(api.error, { 
        invalidConversationId:       { code: 416, message: "The conversation id is invalid" },
        mutipleRelations:            { code: 417, message: "The conversation has relation to other users"},
        publicBlrtsExist:            { code: 418, message: "The conversation has public Blrts"},
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationDeleteError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;
        var now = new Date();

        params = req.params;
        user = req.user;

        var conversationId = params.conversationId;

        var conversation = new Parse.Object("Conversation");
        conversation.id = conversationId;
        //lock the convo before deleting

        return ParseMutex.fnWithLock(conversation, "relationManagement", function (convo, isDirty) {
            var dirtyPromise = Parse.Promise.as();
            if(isDirty) {
                l.log("It was dirty! Cleaning before proceeding");
                dirtyPromise = curJS.cleanDirtyConvos([convo.id]);
            }

            return dirtyPromise.then(function () {
                //check if the conversation only has relation to current user
                return new Parse.Query("ConversationUserRelation")
                    .equalTo("conversation", conversation)
                    .include("conversation")
                    .include("user")
                    // .notEqualTo("user", user)
                    // .count()
                    // .then(function(count){
                    .find({useMasterKey:true})
                    .then(function(relations){
                        if ((!relations) || (0 == relations.length)) {
                            return unsuccessful(error.invalidConversationId);
                        }
                        let count = 0;
                        relations.map(relation=>{
                            // consider the case where nonuser in the conversation
                            if(relation.get('user')) {
                                if(relation.get('user').get('accountStatus') !== 'deleted') { // exclude the deleted user
                                    count++;    
                                }
                            }else { // count the number of a conversation member includes nonuser
                                count++;
                            }
                        })
                        console.log('\n\ncount count count==='+count);
                        if(count > 1){
                            return unsuccessful(error.mutipleRelations);
                        }

                        //we should not delete a public Blrt
                        return new Parse.Query("ConversationItem")
                            .equalTo("conversation", conversation)
                            .equalTo("isPublicBlrt", true)
                            .count({useMasterKey:true})
                            .then(function(publicCounter){
                                if(publicCounter>0)
                                    return unsuccessful(error.publicBlrtsExist);

                                var deletedAt = conversation.get("deletedAt");
                                if(deletedAt) {
                                    l.log(true, "Conversation already deleted at: ", deletedAt).despatch();
                                    return {success: true, message:"Conversation already deleted at: " + deletedAt.toString() };
                                }

                                // Update the conversation object incase it's been cleaned
                                conversation = relations[0].get("conversation");
                                conversation.set("deletedAt", now)

                                var objectsToSave = [conversation];

                                //save each relation as deleted
                                _.each(relations,function(relation){
                                    objectsToSave.push(relation.set("deletedAt", now));
                                });

                                return Parse.Object.saveAll(objectsToSave,{useMasterKey:true}).then(function () {
                                    l.log(true, "Conversation deleted: ", conversation.id).despatch();
                                    return {success: true, message:"Conversation deleted"};
                                }, function (promiseError) {
                                    l.log(false, "Error saving all objects: ", relation.id);
                                    return unsuccessful(error.subqueryError);
                                });

                            },function(error){
                                l.log(false, "Error finding conversation Item" + conversation.id).despatch();
                                return unsuccessful(error.subqueryError);
                            });
                    },function(error){
                        l.log(false, "Error finding conversation relation: " + conversation.id).despatch();
                        return unsuccessful(error.subqueryError);
                    });
            }, function (promiseError) {
                l.log(false, "Couldn't clean dirty convo:", promiseError).despatch();
                return Parse.Promise.as({ success: false, error: error.subqueryError });
            });
        }, function (promiseError) {
            l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();
            
            if(_.isDate(promiseError)) {
                // The mutual exclusion was in use by another calling function - we'll have to try again later.
                return Parse.Promise.as({ success: false, error: error.subqueryError, tryAgainIn: promiseError });
            }

            return Parse.Promise.as({ success: false, error: error.subqueryError });
        });
    };
})();
