
// var permissionsJS = require("cloud/permissions");
// var emailJS = require("cloud/email");
// var curJS = require("cloud/conversationUserRelationLogic");

var constants = require("cloud/constants");
var api = require("cloud/api/util");
var ParseMutex = require("cloud/ParseMutex");
// var Mixpanel = require("cloud/mixpanel");
// var NonUser = require("cloud/nonUser");
var _ = require("underscore");
// var pioneerJS = require("cloud/pioneer");

// # v1: conversationUserRelationLink
(function () {
    var params;
    var user;

    // ## Enums

    // ### Relation link result
    var conversationUserRelationLinkStatus = {
        relationLinked:         { code: 100, message: "The relation was linked to the user as requested." },
        linkAlreadyMade:        { code: 110, message: "The requesting user is already linked to the relation." }
    };

    // ### conversationUserRelationLinkError

    // Subclasses APIError.
    var conversationUserRelationLinkError = _.extend(api.error, { 
        relationNotFound:       { code: 404, message: "The requested relation was not found." },
        relationAlreadyLinked:  { code: 410, message: "The requested relation is already linked to a different user." },
        userAlreadyInConvo:     { code: 411, message: "The requesting user is already in the conversation, using a different relation." }
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationUserRelationLinkError;

    // ## Request examples

    // } TODO: Provide examples

    // ## Response examples

    // } TODO: Provide examples

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        var now = new Date();

        // The flow of creation can be split into 7 major steps:

        // 1. Get the requested relation;
        // 2. Acquire mutual exclusion (gets conversation for us);
        // 3. Get all other relations in the conversation;
        //   - Check if the user is already inside the conversation;
        // 4. Add user to conversation role;
        // 5. Add user and role to relation;
        // 6. Send Mixpanel events.

        // ### Step 1: Get the requested relation
        return new Parse.Query("ConversationUserRelation")
            .equalTo("objectId", params.relationId)
            .include("conversation")
            .first()
            .then(function (relation) {
                if(!relation) {
                    l.log(false, "Requested relation not found").despatch();
                    return {
                        success: false,
                        error: error.relationNotFound
                    };
                }

                if(relation.get("user") != null) {
                    if(relation.get("user").id == user.id) {
                        l.log(true, "Relation was already linked to requesting user").despatch();
                        return {
                            success: true,
                            status: conversationUserRelationLinkStatus.linkAlreadyMade
                        };
                    } else {
                        l.log(false, "Relation is already linked to a different user").despatch();
                        return {
                            success: false,
                            error: error.relationAlreadyLinked
                        };
                    }
                }

                var convo = relation.get("conversation");

                // ### Step 2: Acquire mutual exclusion

                // Using our custom made mutex manager for this :D
                return ParseMutex.fnWithLock(convo, "relationManagement", function (convo, isDirty) {
                    // Normally we'd clean the conversation if it was found dirty, but in this
                    // case we don't actually care. We only claimed the mutex so the user that's
                    // being linked can't also be invited at the same time.

                    // However in some cases, we may want to explicitly keep this conversation
                    // dirty - for instance if it didn't have it's role set.
                    var stayDirty = false;

                    // ### Step 3: Get all other relations in the conversation
                    return new Parse.Query("ConversationUserRelation")
                        .equalTo("conversation", convo)
                        .find()
                        .then(function (allRelations) {
                            // We need to do this so we can make sure that the user being linked isn't
                            // already in the conversation.
                            var userAlreadyInConvoError = "USER ALREADY IN CONVERSATION";
                            try {
                                _.each(allRelations, function (convoRelation) {
                                    if(convoRelation.get("user") && convoRelation.get("user").id == user.id)
                                        throw userAlreadyInConvoError;
                                });
                            } catch(e) {
                                if(e == userAlreadyInConvoError) {
                                    l.log(false, "The requesting user was already in the conversation, using a different relation").despatch();
                                    return {
                                        success: false,
                                        error: error.userAlreadyInConvo
                                    };
                                } else throw e;
                            }

                            // ### Step 4: Add users to conversation role
                            var role = convo.get("conversationRole");
                            var rolePromise = Parse.Promise.as(role);

                            if(role != null) {
                                role.getUsers().add(user);
                                rolePromise = role.save();
                            } else {
                                l.log("A problem with the role was detected, keeping this convo dirty");
                                stayDirty = true;
                            }

                            return rolePromise.then(function (role) {
                                // ### Step 5: Add user and role to relation
                                var roleACL;
                                if(role) {
                                    roleACL = new Parse.ACL();
                                    roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                                    roleACL.setRoleWriteAccess("Conversation-" + convo.id, true);

                                    relation.setACL(roleACL);
                                }

                                relation.set("user", user).set("allowInInbox", true);

                                return relation.save().then(function () {
                                    var followUpPromises = [];

                                    if(!isDirty && stayDirty)
                                        followUpPromises.push(
                                            convo.increment("dirtyCounter_relationManagement")
                                                .save()
                                                .fail(function (promiseError) {
                                                    l.log("ALERT: Failed to increment dirtyCounter_relationManagement:", promiseError);
                                                })
                                        );

                                    // followUpPromises.push(
                                    //     new Mixpanel().track("Conversation Linked", { id: user.id })
                                    //         .fail(function (promiseError) {
                                    //             l.log("ALERT: Failed to Mixpanel track:", promiseError);
                                    //         })
                                    // );

                                    return Parse.Promise.when(followUpPromises).always(function () {
                                        l.log(true).despatch();
                                        return {
                                            success: true,
                                            status: conversationUserRelationLinkStatus.relationLinked
                                        };
                                    });
                                }, function (promiseError) {
                                    l.log(false, "Failed to save relation:", promiseError).despatch();
                                    return Parse.Promise.as({ success: false, error: error.subqueryError });
                                });
                            }, function (promiseError) {
                                l.log(false, "Failed to update the conversation's role:", promiseError).despatch();
                                return Parse.Promise.as({ success: false, error: error.subqueryError });
                            });
                        }, function (promiseError) {
                            l.log(false, "Failed to search for all of the conversation's relations:", promiseError).despatch();
                            return Parse.Promise.as({ success: false, error: error.subqueryError });
                        });
                }).fail(function (promiseError) {
                    l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();
                    if(_.isDate(promiseError)) {
                        // The mutual exclusion was in use by another calling function - we'll have to try again later.
                        return Parse.Promise.as({ success: false, error: error.subqueryError, tryAgainIn: promiseError });
                    }

                    return Parse.Promise.as({ success: false, error: error.subqueryError });
                });
        }, function (promiseError) {
            l.log(false, "Failed to search for the requested relation:", promiseError).despatch();
            return Parse.Promise.as({ success: false, error: error.subqueryError });
        });
    };

})();
