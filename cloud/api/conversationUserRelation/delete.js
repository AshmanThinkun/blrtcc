var api = require("cloud/api/util");
var constants = require("cloud/constants");
var _ = require("underscore");
var emailJS = require("cloud/email");
const error = _.extend(api.error, {
    ownerWantToLeave: {code: 411, message: "The conversation owner should not leave"},
    tendToRemoveOtherPeople: {
        code: 453, message: "Anyone except conversation owner can not remove others from the relation"
    },
});

function unsuccess(reason) {
    return {
        success: false,
        error: reason
    };
}
exports[1] = function (req) {
    const {params, user}=req;

    console.log('params.relationIds: '+params.relationIds);
    
    //fetch all the relations from the request params
    return new Parse.Query('ConversationUserRelation')
        .containedIn('objectId', params.relationIds)
        .include('user')
        .include('conversation') //we need conversation role
        .find({useMasterKey:true})
        .then(function (relations) {
            // contain all the registered users to be removed ,'user' property of relation can be empty due to nonUser case
            const usersToLeave = _.chain(relations).map( r=> r.get('user')).compact().value()
            // used to display in client ,including nonUser
            const usernamesToProvide = _.chain(relations).map(r=>r.get('user')? r.get('user').get('username'):r.get('queryValue')).value()
            //some info such as convo and creator of the convo is shared by all the relations in the result,just use the first one
            const firstRelation = relations[0]
            const conversation = firstRelation.get('conversation')
            const convoCreatorId = firstRelation.get('convoCreator').id


            //we take advantage of generalIndex as a way of concurrent control
            //we need to send push notification to all the users in the conversation except req.user
            const allRelationInExceptSenderPromise= new Parse.Query("ConversationUserRelation")
                .equalTo("conversation", conversation)
                .notEqualTo('user',user)
                .include("user")
                .find({useMasterKey:true})
            
            const convoIncreaseGeneralCountPromise =conversation.increment(constants.convo.keys.generalCount).save(null,{useMasterKey:true})

            var messageArgumet={}

            console.log("convoCreatorId : "+convoCreatorId)
            console.log("usersToLeave")
            console.log(JSON.stringify(usersToLeave.map(u=>u.id)))

            if(_.contains(usersToLeave.map(u=>u.id),convoCreatorId)) //convo creator can never be removed
            // if (convoCreatorId in usersToLeave.map(u=>u.id)) //convo creator can never be removed
            {
                console.log("where you return ???")
                return Parse.Promise.error(unsuccess(error.ownerWantToLeave));
            } else if (convoCreatorId === user.id) // owner remove user
            {
                messageArgumet.MessageType=5;
                messageArgumet.RemoveType='remove';
                messageArgumet.RemoveInitiator=user.get('name')||user.get('username');
                messageArgumet.RemovedUserContactDetail=usernamesToProvide;
                messageArgumet.Fallback={
                    "MessageType": 1,
                    "Message": `@#$${messageArgumet.RemoveInitiator}@#$ removed  @#$${usernamesToProvide}@#$ from the conversation`
                }
            } else if (usersToLeave[0].id === user.id)  //user leave conversation
            {
                messageArgumet.MessageType=5;
                messageArgumet.RemoveType='leave';
                messageArgumet.RemovedUserDisplayName=user.get('name')||user.get('username')
            }
            
            var roleStub = conversation.get("conversationRole");
            //usersToLeave need to be refered in the closure
            return Parse.Promise.when(roleStub.fetch({useMasterKey:true}),convoIncreaseGeneralCountPromise)
                .then(function (role, convo) {
                    usersToLeave.forEach(u=> {
                        role.getUsers().remove(u);

                    })
                    //convo need to be refered in the closure
                    return Parse.Promise.when(role.save(), Parse.Object.destroyAll(relations,{useMasterKey:true}))
                        .then(function (newRole) {
                            var deleteItemACL = new Parse.ACL();
                            deleteItemACL.setRoleReadAccess("Conversation-" + conversation.id, true);
                            deleteItemACL.setRoleWriteAccess("Conversation-" + conversation.id, true);
                            const deleteNoteItem = new Parse.Object(constants.convoItem.keys.className)
                                .set(constants.base.keys.version, constants.convoItem.version)
                                .set(constants.base.keys.creator, user)
                                .set(constants.convoItem.keys.conversation, conversation)
                                .set(constants.convoItem.keys.generalIndex, conversation.get(constants.convo.keys.generalCount)-1)
                                .set(constants.convoItem.keys.readableIndex, -1)
                                .set(constants.convoItem.keys.type, 5)
                                .set(constants.convoItem.keys.Argument, JSON.stringify(messageArgumet))
                                .setACL(deleteItemACL);
                            convo
                                .set(constants.convo.keys.ContentUpdate, new Date())
                                .increment(constants.convo.keys.BondCount, -(usersToLeave.length))
                            
                            return Parse.Promise.when(allRelationInExceptSenderPromise,Parse.Object.saveAll([deleteNoteItem, convo],{useMasterKey:true}))
                        })
                        .then(function (relationsToPushNotification) {
                            console.log('relationsToPushNotification: '+JSON.stringify(relationsToPushNotification)); 
                            let recipients = []
                            relationsToPushNotification.forEach(relation=>{
                                if(relation.get('user')) 
                                    recipients.push(relation.get('user'));
                            })
                            console.log(typeof recipients);
                            console.log('usersToLeave: '+JSON.stringify(recipients));
                            Parse
                                .Push
                                .send({
                                    where: new Parse
                                        .Query(Parse.Installation)
                                        .containedIn("owner", usersToLeave),
                                    data: emailJS.GeneratePush('PRM',{t: 3, bid: conversation.id, s: 0},[convo.get(constants.convo.keys.Name)])
                                    // JSON.stringify({t: 1, bid: conversation.id, s: 0})

                                },{useMasterKey: true})
                                .fail(function (error) {
                                    console.log("ALERT: Error sending unhidden push notifications:", error);
                                });

                            console.log('usersToLeave name: '+usersToLeave[0].get("name"));

                            Parse
                                .Push
                                .send({
                                    where: new Parse
                                        .Query(Parse.Installation)
                                        .containedIn("owner", recipients),
                                    data: emailJS.GeneratePush("PLC", {t: 1, bid: convo.id, s: 1}, [usersToLeave[0].get("name"), convo.get(constants.convo.keys.Name)])
                                    // JSON.stringify({t: 1, bid: conversation.id, s: 0})
                                },{useMasterKey: true})
                                .fail(function (error) {
                                    console.log("ALERT: Error sending unhidden push notifications:", error);
                                });
                            return Parse.Promise.as({
                                success: true,
                                message: "successfully leave/remove from the conversation "
                            })
                        })
                });
        })

        .fail(function (err) {
            console.error(err)
            return err
        })


}