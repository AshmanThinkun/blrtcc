
/******************************
 * set conversationUserRelation property:
 *          flag
 *          archive
 ******************************/

var _ = require("underscore");
var api = require("cloud/api/util");
//just for encryptId
require("cloud/util")



var error = api.error;
exports[1] = function (req) {
    var params = req.params;
    var user = req.user;


    var convoUserRelationStub =new Parse.Object("ConversationUserRelation");
    convoUserRelationStub.id = params.relationId;

    var settings=params.settings;

    console.log("params.relationId : "+params.relationId+" settings : "+JSON.stringify(settings));

    return convoUserRelationStub.fetch()
        .then(function (relation) {
            settings.forEach(function (setting) {

                if(setting.hasOwnProperty("archive"))
                {
                    relation.set("archived",setting.archive);
                }else if(setting.hasOwnProperty("flag"))
                {
                    relation.set("flagged",setting.flag)
                }

            })

            return relation.save()
        }, function () {
            return new Parse.Promise.error({errorType:error.noAccess})
        })
        .then(function (relation) {
            console.log("Full relation : "+JSON.stringify(relation))
            return Parse.Promise.as({success:true})
        })
        .fail(function (error) {
            return Parse.Promise.as({success:false,error:error.errorType||error})
        })


}

