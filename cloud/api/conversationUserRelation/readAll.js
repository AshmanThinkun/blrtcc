module.exports[1] = ({user, params}) => {
    const {relationId} = params
    return new Parse.Query('ConversationUserRelation')
        .include('conversation')
        .get(relationId).then((relation) => {
        const readableCount = relation.get('conversation').get('readableCount');
        return relation.set('indexViewedList', `0-${readableCount - 1}`).save()
    }).then((relation) => {
        const viewListAfterSet = relation.get('indexViewedList')
        console.log("Are you still good ???")
        return Parse.Promise.as({viewListAfterSet})

    }).fail((e) => {
        console.log("Any thing wrong ?")
        console.log(JSON.stringify(e))
        return Parse.Promise.as({hello: "world"})
    })

}