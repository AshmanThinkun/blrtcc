/**
 * Created by test on 26/10/2017.
 */

/***************************************
 * Fetch all the conversations of a user
 ***************************************/

var _ = require("underscore");
var constants = require("cloud/constants");
var CONVO_PER_REQUEST = 20;
var api = require("cloud/api/util");
var l = api.loggler;

//util function the construct the error json
function unsuccessful(error) {
    var response = {
        success: false,
        error: error
    };
    // if(savedObjects.length != 0)    response.objects =
    // _.responsify(savedObjects);

    return Parse
        .Promise
        .as(response);
}

class ResultObject {
    constructor(relation) {
        try {
            var convo = relation.get("conversation");
            if (convo === undefined) {
                console.log("convo is undefined");
            } else {
                if (!convo.get("incomplete") && !convo.get('deletedAt')) {
                    var lastMedia = convo.get('lastMedia');
                    if (lastMedia) {
                        lastMedia = lastMedia.map(function (m) {
                            return {
                                id: m.id,
                                encryptType: m.get('encryptType'),
                                encryptValue: m.get('encryptValue'),
                                mediaFile: m.get("mediaFile") && m
                                    .get("mediaFile")
                                    .url(),
                                mediaFormat: m.get('mediaFormat'),
                                pageArgs: m.get('pageArgs'),
                                name: m.get('name')
                            }
                        })
                    }
    
                    this.className = constants.convoUserRelation.keys.className;
                    this.encryptedId = _.encryptId(relation.id);
    
                    // Assign the default value to false by double pipe if these two properties is
                    // null or undifined in parse
                    this.flagged = relation.get("flagged") || false;
                    this.archived = relation.get("archived") || false;
                    this.contentUpdate = relation.get("contentUpdate") || convo.get("contentUpdate");
                    var convoCreator = relation.get("convoCreator")
                    this.convoCreator = {
                        className: "Parse.User",
                        id: convoCreator.id,
                        encryptedId: _.encryptId(convoCreator.id),
                        name: convoCreator.get("name")
                    }
    
                    this.conversation = {
                        className: "Conversation",
                        lastMediaArgument: convo.get('lastMediaArgument'),
                        lastMedia: lastMedia,
                        specialSlug: convo.get('specialSlug') || false,
                        encryptedId: _.encryptId(convo.id),
                        plainId: convo.id,
                        blrtName: convo.get("blrtName"),
                        //todo some conversation do not have a thumbnail
                        thumbnail: convo.get("thumbnailFile") && convo
                            .get("thumbnailFile")
                            .url(),
                        disallowPublicBlrt: convo.get("disallowPublicBlrt"),
                        disableOthersToAdd: convo.get("disableOthersToAdd"),
                        localGuid: convo.get('localGuid'),
                        unusedMedia: convo.get('unusedMedia') || false,
                        conversationRelaxedStatus: convo.get('conversationRelaxedStatus') || 0,
                    }
    
                    //read count and unread count for the user-conversation
                    var indexViewedList = relation.get("indexViewedList");
                    // console.log('\n\n\nconvo blrtName==='+convo.get("blrtName"));
                    // console.log('webappquery indexViewedList===');
                    // console.log(indexViewedList);
                    var viewedArr = _.expand(indexViewedList);
    
                    var totalCount = convo.get("readableCount");
                    var readCount = viewedArr.length;
                    this.unreadCount = totalCount - readCount;
                    this.itemCount = totalCount;
                    this.tag = relation.get("tags") || []
                }
            }
        } catch (e) {
            console.error(e)
        }

    }
}

exports[1] = function (req) {
    const {params, user} = req
    // const pageNo = parseInt(params.page);
    const ctags = params.ctags;

    // Both main query and flagged query should build on this. And the two queries do
    // not influence each other by using different instance
    function getBaseQuery() {
        const query = new Parse
            .Query("ConversationUserRelation")
            .equalTo("user", user)
            .equalTo("deletedAt", null)
            .descending("contentUpdate")
            .include("conversation")
            .include("conversation.lastMedia")
            .include("convoCreator");
        if (ctags) {
            const tags = ctags.split('.')
            query.matchesQuery("conversation", new Parse.Query("Conversation").containsAll('tag', tags))
        }
        return query;
    }

    let convoId = params.convoId;
    let direction = params.direction;
    let pageNoPromise;
    let skipConvoNo = 0;
    let finalConvoQuery = false;
    
    console.log('type type type==='+params.type);
    console.log("convoId convoId convoId===" + convoId);
    console.log("direction direction direction==="+direction);
    /*direction:
    ** bottom -- scroll down to load more older conversations
    ** top -- click Newer button to loader newer converations
    ** first -- start running the web app and switch between Archive and Inbox
    */

    if (convoId) {
        // const decryptId = _.decryptId(convoId);
        const convo = new Parse.Object('Conversation');
        convo.id = convoId;
        pageNoPromise = new Parse
            .Query('ConversationUserRelation')
            .equalTo('conversation', convo) //according to the convo.id find certain convo
            .equalTo('user', user)
            .first({useMasterKey: true})
            .then((relation) => {
                if(params.type=='inbox') {
                    return new Parse
                    .Query('ConversationUserRelation')
                    .equalTo('user', user)
                    .greaterThan('contentUpdate', relation.get('contentUpdate')) // dont include itself
                    .equalTo("deletedAt", null)
                    .notEqualTo('archived', true)
                    .include("conversation")
                    .count({useMasterKey: true})
                }else if(params.type == "archive") {
                    return new Parse
                    .Query('ConversationUserRelation')
                    .equalTo('user', user)
                    .greaterThan('contentUpdate', relation.get('contentUpdate'))
                    .equalTo("deletedAt", null)
                    .equalTo("archived", true)
                    .include("conversation")
                    .count({useMasterKey: true})
                }
            })
            //calculate the number of skipped conversations 
            .then((count) => {
                console.log('count count count: '+count);
                let calculatedPageNo;
                //load not overlapped convos
                if(direction === "top") {
                    if(count <= 20){
                        calculatedPageNo = 0;
                        CONVO_PER_REQUEST = count;
                        skipConvoNo = 0;
                        if(count === 0) {
                            finalConvoQuery = true;
                        }
                    }else {
                        skipConvoNo = count-CONVO_PER_REQUEST;
                        calculatedPageNo = Math.floor((skipConvoNo-1) / CONVO_PER_REQUEST)+1;
                    }
                    console.log('skipConvoNo skipConvoNo skipConvoNo: '+skipConvoNo);
                }else {
                    skipConvoNo = direction==='first' ? count : count+1; 
                    calculatedPageNo = Math.floor((skipConvoNo-1) / CONVO_PER_REQUEST)+1; 
                }
                console.log('==========calculatedPageNo: '+calculatedPageNo);
                return calculatedPageNo;
            })
            .fail((e) => {
                console.log(e)
            })
    } else {
        pageNoPromise = Parse
            .Promise
            .as(0);
    }

    return pageNoPromise.then((pn) => {
        console.log("pn pn pn: " + pn);
        if (pn < 0 || finalConvoQuery) {
            return {data: [], pn: 0};
        }
     
        try {
            let conversationDataPromise;
            const pageNo = Math.max(pn, 0)
            var getInboxOrArchiveQuery;
            if (params.type == "inbox") {
                getInboxOrArchiveQuery = function () {
                    return getBaseQuery().notEqualTo("archived", true);
                }     

            } else if (params.type == "archive") {
                getInboxOrArchiveQuery = function () {
                    return getBaseQuery().equalTo("archived", true);
                }
            } else {
                //exit the endpoint and give back error
                return Parse
                    .Promise
                    .as({success: false, error: api.error.invalidParameters});
            }     

            console.log('skipConvoNo==='+skipConvoNo); // skip all the newer conversations
            console.log("CONVO_PER_REQUEST==="+CONVO_PER_REQUEST);
            var mainQueryPromise = getInboxOrArchiveQuery()
                .skip(skipConvoNo)
                .limit(CONVO_PER_REQUEST)
                .find({useMasterKey: true})
                .then(function (result) {
                    var convoInfo = [];
                    // console.log('\n'+'mainQueryPromise result: '+JSON.stringify(result));
                    console.log('result.length: '+result.length+'\n');
                    result.forEach(function (relation) {
                        // console.log('\n\nrelation conversation==='+relation.get('conversation').get('blrtName'));
                        convoInfo.push(new ResultObject(relation))
                    })
                    CONVO_PER_REQUEST = 20;
                    return convoInfo
                })
            //if pageNo === 0, need to include the flagged convos as well
            if (pageNo === 0) { 
                var flagQueryPromise = getInboxOrArchiveQuery()
                    .equalTo("flagged", true)
                    .find({useMasterKey: true})
                    .then(function (result) {
                        var convoInfo = [];
                        // console.log('flagged result: '+JSON.stringify(result));
                        // console.log('flagged flagged flagged: '+result.length);
                        result.forEach(function (relation) {
                            convoInfo.push(new ResultObject(relation))
                        })
                        return convoInfo
                    })     

                conversationDataPromise = Parse
                    .Promise
                    .when(flagQueryPromise, mainQueryPromise)
                    .then(function (flag, main) {
                        //incomplete conversation will introduce empty value
                        return flag
                            .concat(main)
                            .filter(function (i) {
                                return Object
                                    .keys(i)
                                    .length > 0
                            })
                    }, function () {
                        return unsuccessful(api.error.subqueryError)
                    })     
            } else {
                conversationDataPromise = mainQueryPromise.then(function (main) {
                    console.log("main.length : " + main.length)
                    return main.filter(function (i) {
                        return Object
                            .keys(i)
                            .length > 0
                    })
                }, function () {
                    return unsuccessful(api.error.subqueryError)
                })
            }

            return conversationDataPromise.then((data) => {
                console.log("pppnnn " + pn + ' ' + data.length);
                return {data, pn}
            });

        } catch (e) {
            console.log(e)
        }

    })

}
