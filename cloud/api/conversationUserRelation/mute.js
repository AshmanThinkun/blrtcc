var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
var ParseMutex = require("cloud/ParseMutex");
var curJS = require("cloud/conversationUserRelationLogic");
var loggler = require("cloud/loggler");

// ## Enums
// if it's all, it will ignore all the other flags
// all the flags can be combined such as 2|4
var MuteEnum = {
        all:        1,
        email:      2,
        push:       4,
    };

exports.MuteEnum = MuteEnum;

// # v1: conversationUserRelationMute
(function () {
    var params;
    var user;


    // ### conversationUserRelationMute

    // Subclasses APIError.
    var conversationUserRelationMuteError = _.extend(api.error, { 
        invalidRelationId:           { code: 431, message: "The conversation user relation id is invalid" },
        invalidMuteFlag:            { code: 432, message: "The mute flag is invalid"},
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationUserRelationMuteError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        var relationId = params.relationId;
        var muteFlag = params.muteFlag;

        var conversationUserRelation = new Parse.Object("ConversationUserRelation");
        conversationUserRelation.id = relationId;

        //check if params are valid
        if(muteFlag<0 || muteFlag> 7){
            l.log(false, "invalid mute flag:", muteFlag).despatch();
            return unsuccessful(error.invalidMuteFlag);
        }

        //we fetch it so that we can check if the relation exist
        return conversationUserRelation.fetch().then(function(){
            return conversationUserRelation.set("muteFlag", muteFlag)
                .save()
                .then(function(){
                    l.log(true, "Mute conversationUserRelation:", relationId).despatch();
                    return {success: true, message:"Mute conversationUserRelation success"};
                },function(error){
                    l.log(false, "Could not save conversationUserRelation:", relationId).despatch();
                    return unsuccessful(error.subqueryError);
                });
        },function(error){
            l.log(false, "Couldn fetch conversationUserRelation:", relationId).despatch();
            return unsuccessful(error.invalidRelationId);
        });

    };
})();

// check if the type of muteFlag is selected
// type should be MuteEnum, it can be a combination of MuteEnum
exports.shouldMute = function(muteFlag, type){
    if(!muteFlag)
        return false;
    if(muteFlag%2 == MuteEnum.all) //odd number means mute both push and email notification
        return true;
    return (muteFlag&type) == type;
};


// we use this to getMuteFlags for array of users, so that we don't need to copy paste the code every where
exports.getMuteFlags = function(users, convoId){
    var muteFlags = {};
    var convo = new Parse.Object("Conversation");
    convo.id = convoId;
    
    // console.log('\n\ngetMuteFlags convoId==='+JSON.stringify(convoId));
    // console.log('\nusers==='+JSON.stringify(users));

    return new Parse.Query("ConversationUserRelation")
        .equalTo("conversation", convo)
        .containedIn("user", users)
        .each(function(relation){
            // console.log('\n===relation==='+relation.get('muteFlag')+' length==='+relation.length);
            var muteFlag = relation.get("muteFlag");
            return muteFlags[relation.get("user").id] = muteFlag;
        }).then(function(){
            return muteFlags;
        },function(error){
            loggler.global.log(false, "Failed to get conversationUserRelation", error).despatch();
            return Parse.Promise.as(muteFlags);
        });
};
