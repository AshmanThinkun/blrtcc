/**
 * Created by test on 2/12/2015.
 */

/***************************************
 * Fetch all the conversations of a user
 ***************************************/

var _ = require("underscore");
var constants = require("cloud/constants");
const CONVO_PER_REQUEST = 20;
var api = require("cloud/api/util");
var l = api.loggler;

//util function the construct the error json
function unsuccessful(error) {
    var response = {
        success: false,
        error: error
    };
    // if(savedObjects.length != 0)    response.objects =
    // _.responsify(savedObjects);

    return Parse
        .Promise
        .as(response);
}

class ResultObject {
    constructor(relation) {
        try {
            var convo = relation.get("conversation");
            if (!convo.get("incomplete") && !convo.get('deletedAt')) {
                var lastMedia = convo.get('lastMedia');
                if (lastMedia) {
                    lastMedia = lastMedia.map(function (m) {
                        return {
                            id: m.id,
                            encryptType: m.get('encryptType'),
                            encryptValue: m.get('encryptValue'),
                            mediaFile: m.get("mediaFile") && m
                                .get("mediaFile")
                                .url(),
                            mediaFormat: m.get('mediaFormat'),
                            pageArgs: m.get('pageArgs'),
                            name: m.get('name')
                        }
                    })
                }

                this.className = constants.convoUserRelation.keys.className;
                this.encryptedId = _.encryptId(relation.id);

                // Assign the default value to false by double pipe if these two properties is
                // null or undifined in parse
                this.flagged = relation.get("flagged") || false;
                this.archived = relation.get("archived") || false;
                this.contentUpdate = relation.get("contentUpdate") || convo.get("contentUpdate");
                var convoCreator = relation.get("convoCreator")
                this.convoCreator = {
                    className: "Parse.User",
                    id: convoCreator.id,
                    encryptedId: _.encryptId(convoCreator.id),
                    name: convoCreator.get("name")
                }

                this.conversation = {
                    className: "Conversation",
                    lastMediaArgument: convo.get('lastMediaArgument'),
                    lastMedia: lastMedia,
                    specialSlug: convo.get('specialSlug') || false,
                    encryptedId: _.encryptId(convo.id),
                    plainId: convo.id,
                    blrtName: convo.get("blrtName"),
                    //todo some conversation do not have a thumbnail
                    thumbnail: convo.get("thumbnailFile") && convo
                        .get("thumbnailFile")
                        .url(),
                    disallowPublicBlrt: convo.get("disallowPublicBlrt"),
                    disableOthersToAdd: convo.get("disableOthersToAdd"),
                    localGuid: convo.get('localGuid'),
                    unusedMedia: convo.get('unusedMedia') || false
                }

                //read count and unread count for the user-conversation
                var indexViewedList = relation.get("indexViewedList");
                var viewedArr = _.expand(indexViewedList);

                var totalCount = convo.get("readableCount");
                var readCount = viewedArr.length;
                this.unreadCount = totalCount - readCount;
                this.itemCount = totalCount;
                this.tag = relation.get("tags") || []
            }
        } catch (e) {
            console.error(e)
        }
    }
}

exports[1] = function (req) {
    const {params, user} = req
    // const pageNo = parseInt(params.page);
    const ctags = params.ctags;
    // Both main query and flagged query should build on this. And he two queries do
    // not influence each other by using different instance
    function getBaseQuery() {
        const query = new Parse
            .Query("ConversationUserRelation")
            .equalTo("user", user)
            .equalTo("deletedAt", null)
            .descending("contentUpdate")
            .include("conversation")
            .include("conversation.lastMedia")
            .include("convoCreator");
        if (ctags) {
            const tags = ctags.split('.')
            query.matchesQuery("conversation", new Parse.Query("Conversation").containsAll('tag', tags))
        }
        return query;
    }

    let convoId = params.convoId;
    let pageNoPromise;

    console.log("convoId : " + convoId)
    if (convoId) {
        const decryptId = _.decryptId(convoId);
        const convo = new Parse.Object('Conversation')
        convo.id = decryptId
        pageNoPromise = new Parse
            .Query('ConversationUserRelation')
            .equalTo('conversation', convo)
            .equalTo('user', user)
            .first({useMasterKey: true})
            .then((relation) => {
                return new Parse
                    .Query('ConversationUserRelation')
                    .greaterThan('contentUpdate', relation.get('contentUpdate'))
                    .equalTo('user', user)
                    .notEqualTo("flagged", true)
                    .equalTo("deletedAt", null)
                    .count({useMasterKey: true})

            })
            .then((count) => {
                const calculatedPageNo = Math.floor(count / CONVO_PER_REQUEST)
                console.log("======== : " + count + " : " + calculatedPageNo)
                return calculatedPageNo
            })
            .fail((e) => {
                console.log(e)
            })

    } else {
        pageNoPromise = Parse
            .Promise
            .as(parseInt(params.page))
    }

    return pageNoPromise.then((pn) => {
        console.log("pn : " + pn)
        if (pn < 0) {
            return {data: [], pn: -1}
        }

        try {
            const pageNo = Math.max(pn, 0)
            var getInboxOrArchiveQuery;
            if (params.type == "inbox") {
                getInboxOrArchiveQuery = function () {
                    return getBaseQuery().notEqualTo("archived", true);
                }

            } else if (params.type == "archive") {
                getInboxOrArchiveQuery = function () {
                    return getBaseQuery().equalTo("archived", true);
                }
            } else {
                //exit the endpoint and give back error
                return Parse
                    .Promise
                    .as({success: false, error: api.error.invalidParameters});
            }

            var mainQuery = getInboxOrArchiveQuery().notEqualTo("flagged", true);
            console.log("pageNo * CONVO_PER_REQUEST : " + pageNo * CONVO_PER_REQUEST)
            var mainQueryPromise = mainQuery
                .skip(pageNo * CONVO_PER_REQUEST)
                .limit(CONVO_PER_REQUEST)
                .find({useMasterKey: true})
                .then(function (result) {
                    var convoInfo = [];
                    result.forEach(function (relation) {
                        convoInfo.push(new ResultObject(relation))
                    })
                    return convoInfo
                })
            let conversationDataPromise;
            if (pageNo === 0) {
                var flagQuery = getInboxOrArchiveQuery().equalTo("flagged", true);
                var flagQueryPromise = flagQuery
                    .find({useMasterKey: true})
                    .then(function (result) {
                        var convoInfo = [];
                        result.forEach(function (relation) {
                            convoInfo.push(new ResultObject(relation))
                        })
                        return convoInfo
                    })

                conversationDataPromise = Parse
                    .Promise
                    .when(flagQueryPromise, mainQueryPromise)
                    .then(function (flag, main) {
                        //incomplete conversation will introduce empty value
                        return flag
                            .concat(main)
                            .filter(function (i) {
                                return Object
                                    .keys(i)
                                    .length > 0
                            })
                    }, function () {
                        return unsuccessful(api.error.subqueryError)
                    })

            } else {
                conversationDataPromise = mainQueryPromise.then(function (main) {
                    console.log("main.length : " + main.length)
                    return main.filter(function (i) {
                        return Object
                            .keys(i)
                            .length > 0
                    })
                }, function () {
                    return unsuccessful(api.error.subqueryError)
                })
            }

            return conversationDataPromise.then((data) => {
                console.log("pppnn " + pn)
                return {data, pn}
            });

        } catch (e) {
            console.log(e)
        }

    })

}