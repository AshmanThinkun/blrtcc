var api = require("cloud/api/util");

var _ = require("underscore");

// # v1: conversationUserRelationPublicInfo
(function () {
    // ## Enums

    // ### conversationUserRelationPublicInfoError

    // Subclasses APIError.
    var conversationUserRelationCreateError = _.extend(api.error, { 
        relationNotFound:           { code: 404, message: "The requested relation was not found." }
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationUserRelationCreateError;

    // ## Request examples

    // } TODO: Provide examples

    // ## Response examples

    // } TODO: Provide examples

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;

        return new Parse.Query("ConversationUserRelation")
            .equalTo("objectId", params.relationId)
            .first()
            .then(function (relation) {
                if(!relation) {
                    l.log(false, "Requested relation not found").despatch();
                    return {
                        success: false,
                        error: error.relationNotFound
                    };
                }

                if(relation.get("user"))
                    return {
                        success: true,
                        isUser: true
                    };
                else
                    return {
                        success: true,
                        isUser: false,
                        contactType: relation.get("queryType"),
                        contactValue: relation.get("queryValue")
                    };
            }, function (promiseError) {
                l.log(false, "Failed to fetch ConversationUserRelation:", promiseError).despatch();
                return Parse.Promise.as({ success: false, error: error.subqueryError });
            });
    };
})();