var permissionsJS = require("cloud/permissions");
var emailJS = require("cloud/email");
var curJS = require("cloud/conversationUserRelationLogic");
const convertBlrtToVideo = require("cloud/api/shared/convertVideo");
var constants = require("cloud/constants");
var api = require("cloud/api/util");
var ParseMutex = require("cloud/ParseMutex");
var trackBatch = require('cloud/analytics-node').trackBatch;
var NonUser = require("cloud/nonUser");
var _ = require("underscore");
const config=require('../../../config/serverConfig')

function generateAccessTokens(numOfTokens) {
    var result = [];
    if (('number' == typeof numOfTokens) && (numOfTokens > 0)) {
        for (var i = 0; i < numOfTokens; ++i) {
            result.push(_.randomString(constants.accessToken.tokenLength));
        }
    }
    return result;
}

function saveAccessTokens(tokens, convo, infoObjs) {
    if (tokens && (tokens.length > 0)) {
        var objects = [];
        for (var i = 0; i < tokens.length; ++i) {
            var object = new Parse.Object(constants.accessToken.keys.className)
                .set(constants.accessToken.keys.token, tokens[i])
                .set(constants.accessToken.keys.conversation, convo)
                .set(constants.accessToken.keys.accessCountdown, constants.convoItem.maxPrivateViewCounter)
                .set(constants.accessToken.keys.conversationUserRelation, infoObjs[i].relation)
                .set(constants.accessToken.keys.isEnabled, true);
            // if (infoObjs && infoObjs[i]) {
            //     object.set(constants.accessToken.keys.info, JSON.stringify(infoObjs[i]));
            // }
            objects.push(object);
        }
        return Parse.Object.saveAll(objects,{useMasterKey:true});
    } else {
        return Parse.Promise.as(true);
    }
}

// # v1: conversationUserRelationCreate
(function () {
    var params;
    var user;

    // ## Enums

    // ### Relation creation result
    var relationCreationResult = {
        sentUser: 0,
        sentReachable: 1,
        sentUnreachable: 2,
        alreadySent: 5,
        invalid: 10,
        blockedByUser: 15,
    };

    // ### conversationUserRelationCreateError

    // Subclasses APIError.
    var conversationUserRelationCreateError = _.extend(api.error, {
        invalidRelationQuery: {
            code: 410,
            message: "Relation(s) could not be created due to detection of an invalid relation query."
        },
        tooManyRelations: {
            code: 450,
            message: "Creating the requested relation(s) would cause the conversation to breach it's current relation limit."
        },
        disabledByCreator: {code: 451, message: "The conversation add function is disabled by the creator."}
    });

    // Aliased as error for simplicity of internal use.
    var error = conversationUserRelationCreateError;

    // ## Request examples

    // } TODO: Provide examples

    // ## Response examples

    // } TODO: Provide examples

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;
        params = req.params;
        user = req.user;

        var now = new Date();
        var ignoreBlock = req.params.ignoreBlock;
        // console.log('\n\n===req.user==='+user);
        console.log('\n\n===req.params===');
        console.log(req.params);

        // The flow of creation can be split into 7 major steps:

        // 1. Acquire mutual exclusion (gets conversation for us);
        // 2. Get:
        //   * All existing relations; and
        //   * Related users/contact details.
        //   - Process found information; and
        // 3. Check permissions with the inviter's relation;
        // 4. Add users to conversation role;
        // 5. Create relations with role;
        // 6. Update conversation's relation counter and create events/notes;
        // 6.5. Create TimelyActions for non-user invites; and
        // 7. Send push notifications,and emails.

        // ### Step 1: Acquire mutual exclusion

        // Which object to we want to hold the mutual exclusion of?
        var convo = new Parse.Object(constants.convo.keys.className);//Conversation
        convo.id = params.conversationId;

        // Unfortunately we need to fetch the conversation or it'll be missing the fields we want to see.
        // Even though we'll be saving it and receiving responses regarding it, those responses will only
        // show information about the fields that were saved instead of about the entire conversation.
        return convo.fetch({useMasterKey:true}).then(function () {
            // Using our custom made mutex manager for this :D
            return ParseMutex.fnWithLock(convo, "relationManagement", function (convo, isDirty) {
                var dirtyPromise = Parse.Promise.as();
                if (isDirty) {
                    l.log("It was dirty! Cleaning before proceeding");
                    dirtyPromise = curJS.cleanDirtyConvos([convo.id]);
                }

                return dirtyPromise.then(function () {
                    console.log('\n\n===dirtyPromise===');
                    // If something goes wrong we may want to keep this conversation flagged as dirty so it can be cleaned.
                    var stayDirty = false;

                    // ### Step 2: Get everything!

                    // * Existing relations for this conversation.
                    // It's necessary to get all of them (even with different types or values) because we need to check for more
                    // complicated cases like where an existing relation using a user's secondary email already exists.
                    var relationPromise = new Parse.Query(constants.convoUserRelation.keys.className)
                        .equalTo(constants.convoUserRelation.keys.conversation, convo)
                        .find({useMasterKey:true});

                    // * Contact details matching the requested relations (if they exist).
                    // This query may be slightly more complicated as we have to match contact details of different types -
                    // so we'll build up an or query for each type.
                    var contactTypesAndValues = {};
                    _.each(params.relations, function (relation) {
                        relation.value = relation.value.toLowerCase();
                        if (!_.has(contactTypesAndValues, relation.type))
                            contactTypesAndValues[relation.type] = [];
                        contactTypesAndValues[relation.type].push(relation.value);
                    });



                    //fetch all the existing UserContactDetails objects in Parse based on the params.relation,
                    var contactDetailPromise =
                        Parse.Query.or(...(_.map(contactTypesAndValues, (values, type) =>
                            new Parse.Query(constants.contactTable.keys.className)
                                .equalTo(constants.contactTable.keys.type, type)
                                .containedIn(constants.contactTable.keys.Value, values))))
                            .include(constants.contactTable.keys.user)
                            .find({useMasterKey:true});

                    //get blrt request
                    var blrtRequest = convo.get("blrtRequest");
                    var requestPromise = Parse.Promise.as();
                    if (blrtRequest) {
                        requestPromise = blrtRequest.fetch({useMasterKey:true});
                    }


                    return Parse.Promise.when(relationPromise, contactDetailPromise, requestPromise)
                        .then(function (foundRelations, contacts) {
                            console.log("Are you still OK here ???  start");
                            console.log('\n\n\n\n===foundRelations: '+JSON.stringify(foundRelations));
                            // the existing relation in conversation which is also requested to be added i.e. clashing relation
                            var relatedRelations = [];

                                // #### Step 2.5: Process found information
                                var relationsToCreate = 0;
                                var relationUsers = []; //for adding to role
                                var numBlockingUsers = 0;

                                // We'll hold onto which relation belongs to the inviter because that's who's permissions we'll
                                // need to check (the conversation and it's owner's permissions will cascade into it nicely).
                                var invitersRelation;
                                var creatorsRelation;
                                _.each(foundRelations, function (relation) {
                                    if (
                                        relation.get(constants.convoUserRelation.keys.user) !== undefined &&
                                        relation.get(constants.convoUserRelation.keys.user).id === user.id
                                    )
                                        invitersRelation = relation;

                                    if (relation.get(constants.convoUserRelation.keys.QueryType) === "_creator")
                                        creatorsRelation = relation;
                                });

                                // Our goal here is to link each requested relation with any contact details that were found and use
                                // those to check for any existing clashing relations.
                                _.each(params.relations, function (relation) {
                                    // * Look through found contacts for a matching user.
                                    // attach UserContactDetails object to "params.relations" individually
                                    _.each(contacts, function (contact) {
                                        if (
                                            contact.get(constants.contactTable.keys.type) === relation.type &&
                                            contact.get(constants.contactTable.keys.Value) === relation.value
                                        ) {
                                            relation.contact = contact;
                                            relation.name = contact.get(constants.contactTable.keys.user).get(constants.user.keys.FullName);

                                            var userBlocked = _.find(relation.contact.get("user").get("blockedUsers"), function (blockedUser) {
                                                return blockedUser.id === user.id;
                                            });

                                            if (userBlocked && !ignoreBlock) {
                                                ++numBlockingUsers;
                                                relation.blocked = true;
                                                return;
                                            } else
                                                relationUsers.push(contact.get("user"));

                                            // We need a special check for _creator (i.e. when the person is inviting themselves...).
                                            // We're doing it in here instead of at the beginning of the function because this will also
                                            // catch cases with tokens and other types of contact details elegantly for us.
                                            if (contact.get(constants.contactTable.keys.user).id === user.id) {
                                                relatedRelations.push(creatorsRelation);
                                                relation.clash = creatorsRelation;
                                            }
                                        }
                                    });



                                    // * Look through found relations for a clashing relation.
                                    //attach ConversationUserRelation object to "params.relation" individually
                                    _.each(foundRelations, function (foundRelation) {
                                        if ((
                                                //handle blrt user
                                                foundRelation.get("user") !== undefined && relation.contact &&
                                                foundRelation.get("user").id === relation.contact.get("user").id
                                            ) || (
                                                //handle nonUser
                                                foundRelation.get("user") === undefined &&
                                                foundRelation.get(constants.convoUserRelation.keys.QueryType) === relation.type &&
                                                foundRelation.get(constants.convoUserRelation.keys.QueryValue) === relation.value
                                            )) {
                                            relatedRelations.push(foundRelation);
                                            relation.clash = foundRelation;
                                        }
                                    });

                                    // * Look through relationUsers that are going to create, if we found match, we should mark this one as clash
                                    var duplicatedRelations = 0;
                                    _.each(relationUsers, function (relationUserToRole) {
                                        if (
                                            relation.contact && relation.contact.get("user") && relationUserToRole.id == relation.contact.get("user").id
                                        ) {
                                            duplicatedRelations++;
                                            if (duplicatedRelations > 1)
                                                relation.clash = "Will Clash";
                                        }
                                    });


                                    if (relation.clash === undefined && !relation.blocked)
                                        relationsToCreate++;

                                    if (relation.clash) {
                                        relation.result = relationCreationResult.alreadySent;
                                        if (relation.clash === "Will Clash" && relation.blocked) {
                                            relation.result = relationCreationResult.blockedByUser;
                                        }
                                    } else if (relation.blocked)
                                        relation.result = relationCreationResult.blockedByUser;
                                    else if (relation.contact)
                                        relation.result = relationCreationResult.sentUser;
                                    else {
                                        switch (relation.type) {
                                            case api.contactTypes.email:
                                                relation.result = relationCreationResult.sentReachable;
                                                relation.email = relation.value;
                                                break;
                                            default:
                                                relation.result = relationCreationResult.sentUnreachable;
                                        }
                                    }
                                });

                            // If there's nothing to create here, then call success!
                            if(relationsToCreate === 0) {
                                console.log('\n\n\n===nothing to create===');
                                return succeed(relatedRelations);
                            }
                            console.log("Are you still OK here ???");

                            // #### Step 3: Check permissions
                            return permissionsJS.getSinglePermissions({user: user, other: invitersRelation}, true)
                                .then(function (permissions) {
                                    if (
                                        relationsToCreate + convo.get(constants.convo.keys.BondCount) >
                                        permissions[constants.permissions.values.maxConvoUsers]
                                    ) {
                                        l.log(false, "There would be too many relations in this conversation if we added them").despatch();
                                        return {success: false, error: error.tooManyRelations};
                                    }

                                    //is the add others function is disabled by the creator, others can't add people
                                    if (convo.get("disableOthersToAdd") && (user.id !== convo.get("creator").id)) {
                                        l.log(false, "Add function disabled by creator").despatch();
                                        return Parse.Promise.as({success: false, error: error.disabledByCreator});
                                    }

                                    // ### Step 4: Add users to conversation role
                                    var role = convo.get("conversationRole");
                                    var rolePromise = Parse.Promise.as(role);

                                    if (relationUsers.length !== 0) {
                                        if (role != null) {
                                            role.getUsers().add(relationUsers);
                                            rolePromise = role.save(null,{useMasterKey:true});
                                        } else {
                                            l.log("A problem with the role was detected, keeping this convo dirty");
                                            stayDirty = true;
                                        }
                                    }

                                    return rolePromise.then(function (role) {
                                        console.log('\n===rolePromise===');
                                        var roleACL;
                                        if (role) {
                                            roleACL = new Parse.ACL();
                                            roleACL.setRoleReadAccess("Conversation-" + convo.id, true);
                                            roleACL.setRoleWriteAccess("Conversation-" + convo.id, true);
                                        }

                                        // ### Step 5: Create relations with roles
                                        var pushRecipients = [];
                                        var nonUserRelations = [];
                                        var numFacebookContacts = 0;
                                        var toSave = [];
                                        _.each(params.relations, function (relation) {
                                            if (relation.clash !== undefined || relation.blocked)
                                                return;

                                            relation.object = new Parse.Object(constants.convoUserRelation.keys.className)
                                                .set(constants.base.keys.version, constants.convoUserRelation.version)
                                                .set(constants.base.keys.creator, user)
                                                .set(constants.convoUserRelation.keys.allowInInbox, true)
                                                .set(constants.convoUserRelation.keys.conversation, convo)
                                                .set(constants.convoUserRelation.keys.convoCreator, convo.get(constants.base.keys.creator))
                                                .set(constants.convoUserRelation.keys.QueryType, relation.type)
                                                .set(constants.convoUserRelation.keys.QueryValue, relation.value)
                                                .set(constants.convoUserRelation.keys.QueryName, relation.name);

                                            if (roleACL)
                                                relation.object.setACL(roleACL);

                                            if (relation.type === "facebook") ++numFacebookContacts;
                                            if (relation.contact) {
                                                pushRecipients.push(relation.contact.get("user"));
                                                relation.object.set(constants.convoUserRelation.keys.user, relation.contact.get(constants.contactTable.keys.user));
                                            } else
                                                nonUserRelations.push(relation);

                                            toSave.push(relation.object);
                                        });

                                        // We may also need to update the inviting user's relation, to state that they have invited people.
                                        if (invitersRelation.get(constants.convoUserRelation.keys.HasInvitedPeople) != true)
                                            toSave.push(invitersRelation.set(constants.convoUserRelation.keys.HasInvitedPeople, true));


                                        return Parse.Promise.when([Parse.Object.saveAll(toSave,{useMasterKey:true})]).then(function () {
                                            var relatedRelationIds = _.pluck(relatedRelations, "id");
                                            var savedObjects = _.union(relatedRelations, _.reject(toSave, function (object) {
                                                return _.contains(relatedRelationIds, object.id);
                                            }));

                                            // ### Step 6: Update conversation's relation counter and create events/notes.
                                            var convoItemCount = convo.get(constants.convo.keys.generalCount);

                                            var eventItem;
                                            var noteItem;

                                            if (params.conversationName && params.conversationName.length > 0
                                                && convo.get("bondCount") && convo.get("bondCount") < 2) {
                                                convo.set("blrtName", params.conversationName);
                                            }
                                            var moreToSave = [
                                                // * Update the conversation's relation and item counter (since we're creating 1 or 2 new items too).
                                                convo
                                                    .set(constants.convo.keys.ContentUpdate, new Date())
                                                    .increment(constants.convo.keys.BondCount, relationsToCreate)
                                                    .increment(constants.convo.keys.generalCount, (params.note && !params.dontAddNoteInConvo) ? 2 : 1),
                                                // * Create a new event detailing the users that were added to this conversation.
                                                eventItem = new Parse.Object(constants.convoItem.keys.className)
                                                    .set(constants.base.keys.version, constants.convoItem.version)
                                                    .set(constants.base.keys.creator, user)
                                                    .set(constants.convoItem.keys.conversation, convo)
                                                    .set(constants.convoItem.keys.generalIndex, convoItemCount)
                                                    .set(constants.convoItem.keys.readableIndex, -1)
                                                    .set(constants.convoItem.keys.type, 4)
                                                    .set(constants.convoItem.keys.Argument, JSON.stringify({
                                                        items: _.map(
                                                            _.reject(params.relations, function (relation) {
                                                                return relation.clash !== undefined || relation.blocked;
                                                            }),
                                                            function (relation) {
                                                                console.log('\n\n===create convo item event===');
                                                                return {
                                                                    userId: user.contact ? user.contact.get(constants.contactTable.keys.user).id : null,
                                                                    type: relation.type,
                                                                    value: relation.value,
                                                                    name: relation.name,
                                                                    relationId: relation.object.id
                                                                };
                                                            })
                                                    })),
                                                // We'll also increment the user's invitation counter here.
                                                user
                                                    .increment(constants.user.keys.convoInvitationCounter, relationsToCreate)
                                            ];

                                            // * Create a new conversation item for the note, if one was specified.
                                            if (params.note && !params.dontAddNoteInConvo)
                                                moreToSave.push(
                                                    noteItem = new Parse.Object(constants.convoItem.keys.className)
                                                        .set(constants.base.keys.version, constants.convoItem.version)
                                                        .set(constants.base.keys.creator, user)
                                                        .set(constants.convoItem.keys.conversation, convo)
                                                        .set(constants.convoItem.keys.generalIndex, convoItemCount + 1)
                                                        .set(constants.convoItem.keys.readableIndex, -1)
                                                        .set(constants.convoItem.keys.type, 5)
                                                        .set(constants.convoItem.keys.Argument, JSON.stringify({
                                                            "Message": params.note,
                                                            "MessageType": 0
                                                        }))
                                                );

                                            // Don't forget ACLs!
                                            if (roleACL) {
                                                eventItem.setACL(roleACL);
                                                if (noteItem)
                                                    noteItem.setACL(roleACL);
                                            }

                                            // If something went wrong (i.e. missing ACLs) then we might want to make sure
                                            // that the conversation remains dirty even after finishing up here.
                                            if (stayDirty)
                                                convo.increment("dirtyCounter_relationManagement");

                                            // ### Step 6.5: Create TimelyActions for non-user invites
                                            var timelyActions = [];
                                            _.each(nonUserRelations, function (relation) {
                                                if (relation.type !== "email")
                                                    return;

                                                timelyActions.push(
                                                    new Parse.Object("TimelyAction")
                                                        .set("type", "email_nonUserInvited_spawner")
                                                        .set("context", relation.value.toLowerCase())
                                                        .set("executionTime", now)
                                                        .setACL(new Parse.ACL())
                                                );
                                            });

                                            // ## Step 6.6: Create TimelyActions For blrtRequest AfterSend action
                                            if (blrtRequest && blrtRequest.get("afterSendAction")) {
                                                var afterSendUser = _.find(pushRecipients, function (rep) {
                                                    return blrtRequest.get("afterSendAction").userId == rep.id;
                                                });
                                                if (afterSendUser) {
                                                    timelyActions.push(
                                                        new Parse.Object("TimelyAction")
                                                            .set("type", blrtRequest.get("afterSendAction").type)
                                                            .set("data", blrtRequest.get("afterSendAction").data)
                                                            .set("convo", convo)
                                                            .set("user", afterSendUser)
                                                            .set("executionTime", now)
                                                            .setACL(new Parse.ACL())
                                                    );
                                                }
                                            }

                                            return Parse.Object.saveAll(_.union(moreToSave, timelyActions),{useMasterKey:true})
                                                .then(function () {
                                                    return require("cloud/beforeSave").saveContentUpdateForConvo(convo);
                                                },function (e) {
                                                    console.log("what is errorrrr")
                                                    console.log(JSON.stringify(e))
                                                }).then(function () {
                                                    var followUpPromises = [];
                                                    var preEmailPromises = [];

                                                    // ### Step 7: Send push notifications, emails and Mixpanel events
                                                    // var mp = new Mixpanel({batchMode: true});


                                                    // * Ship off our Segment event!
                                                    var sProperties = {
                                                        "# Total": nonUserRelations.length + pushRecipients.length,
                                                        "# NonUsers": nonUserRelations.length,
                                                        "# Existing Users": pushRecipients.length,
                                                        "# Email Invitees": nonUserRelations.length + pushRecipients.length - numFacebookContacts,
                                                        "# Facebook Invitees": numFacebookContacts,
                                                        "# Blocking Users": numBlockingUsers,
                                                        "With Note": params.note !== undefined,
                                                    };
                                                    if (params.note)
                                                        sProperties["Note Length"] = params.note.length;
                                                    if (nonUserRelations.length !== 0 && user.get("pioneerStatus") === "approved")
                                                        sProperties["Invited By Pioneer"] = true;


                                                    // We'll also handle non-user events here. We want to keep a hold
                                                    // of the non-user story.
                                                    var nonUserDetails = [];
                                                    _.each(params.relations, function (relation) {
                                                        if (
                                                            relation.result === relationCreationResult.sentUnreachable ||
                                                            relation.result === relationCreationResult.sentReachable
                                                        )
                                                            nonUserDetails.push({
                                                                type: relation.type,
                                                                value: relation.value,
                                                                referredBy: user.id
                                                            });

                                                        else if (relation.contact)
                                                            trackBatch.track({
                                                                userId: relation.contact.get("user").id,
                                                                event: 'Conversation Invitation Received',
                                                                properties: sProperties
                                                            });

                                                        else
                                                            l.log("ALERT: Somehow non-nonUser relation is detected as having no contact?");
                                                    });

                                                    var invitedNonUser = [];
                                                    if (nonUserDetails.length !== 0) {
                                                        preEmailPromises.push(
                                                            NonUser.get(nonUserDetails).then(function (nonUsers) {
                                                                _.each(nonUsers, function (nonUser) {
                                                                    trackBatch.track({
                                                                        userId: "nonuser-" + nonUser.id,
                                                                        event: 'Conversation Invitation Received',
                                                                        properties: sProperties
                                                                    });
                                                                });
                                                                //[Guichi] we need put nonUser id in email for tracking purpose
                                                                var nonUserResult = {}
                                                                _.each(nonUsers, (n) => {
                                                                    nonUserResult[n.parseObject.get('value')] = n.id;
                                                                })
                                                                return nonUserResult;
                                                            })
                                                        );
                                                    }

                                                    // * Build and send the push notification.
                                                    // We build the push giving it the related conversation and the relation creator's information.
                                                    var message = emailJS.GeneratePush("PN", {
                                                        t: 1,
                                                        bid: convo.id,
                                                    }, [
                                                        user.get("name"),
                                                        convo.get(constants.convo.keys.Name)
                                                    ]);
                                                    // We send it to all devices owned by receiving users.
                                                    new Parse
                                                        .Query(Parse.Installation)
                                                        .containedIn("owner", pushRecipients)
                                                        .find({useMasterKey: true})
                                                        .then(owners=>{
                                                            // console.log('\n\n\n\nowners length==='+owners.length);
                                                            // console.log('Parse Installation owners: '+JSON.stringify(owners));
                                                            // console.log('\n\n\nmessageShow: '+JSON.stringify(messageShow));
                                                            // console.log('\n\n\nmessage: '+JSON.stringify(message));
                                                            Parse.Push.send({
                                                                where: new Parse.Query(Parse.Installation)
                                                                    .containedIn("owner",
                                                                        pushRecipients),
                                                                data: message
                                                            }, {useMasterKey: true}).fail(function (error) {
                                                                l.log("ALERT: Error sending relation push notifications:", error).despatch();
                                                            });
                                                        });
                                                    
                                                    // * Next send out emails.
                                                    // We'll organise the users into their respective templates so we can minimise the requests.
                                                    var existingUsers = [];
                                                    var nonUserEmailRecipients = []; //not in use
                                                    var nonUserEmailRecipientsPioneerInvited = [];
                                                    var nonUserEmailRecipientsNonInvited = [];


                                                    // Next, do we send treat these emails as for a new or existing conversation?
                                                    // A "new conversation" email is only sent if the conversation hasn't begun and the person inviting
                                                    // is the creator of the conversation.
                                                    var newConvo = !convo.get("hasConversationBegan") && user.id == convo.get("creator").id;

                                                    return Parse.Promise.when(preEmailPromises).always(function (nonUserIds) {
                                                        nonUserIds = _.flatten(nonUserIds)
                                                        _.each(params.relations, function (relation) {
                                                            // Which template will we use?
                                                            switch (relation.result) {
                                                                case relationCreationResult.alreadySent:
                                                                case relationCreationResult.sentUnreachable:
                                                                case relationCreationResult.blockedByUser:
                                                                    // } Nothing for this dude
                                                                    break;
                                                                case relationCreationResult.sentUser:
                                                                    //this is the simplest way I can think of to let the disable email new blrt working.
                                                                    if (relation.contact.get("user").get("allowEmailNewBlrt") === false)
                                                                        break;
                                                                    existingUsers.push({
                                                                        id: relation.contact.get("user").id,
                                                                        isUser: true,
                                                                        email: relation.contact.get("user").get("email"),
                                                                        name: relation.contact.get("user").get("name"),
                                                                        // is the relation used some where else?
                                                                        relation: relation.object,
                                                                        user: relation.contact.get("user")
                                                                    });
                                                                    break;
                                                                case relationCreationResult.sentReachable:
                                                                    var sendingObj = {
                                                                        isUser: false,
                                                                        email: relation.email,
                                                                        name: relation.name,
                                                                        relation: relation.object,
                                                                        nonUserId: nonUserIds[0][relation.email]

                                                                    };

                                                                    nonUserEmailRecipients.push(sendingObj);
                                                                    if (_.indexOf(invitedNonUser, relation.email) === -1) {
                                                                        nonUserEmailRecipientsNonInvited.push(sendingObj);
                                                                    } else {
                                                                        nonUserEmailRecipientsPioneerInvited.push(sendingObj);
                                                                    }
                                                                    break;
                                                            }
                                                        });

                                                        // Ship 'em out!
                                                        if (existingUsers.length > 0) {
                                                            userEmailSend(existingUsers,user,convo,params.note)
                                                        }

                                                        if(nonUserEmailRecipientsNonInvited.length > 0) {
                                                            var tokens = generateAccessTokens(nonUserEmailRecipientsNonInvited.length);
                                                            followUpPromises.push(
                                                                saveAccessTokens(tokens, convo, nonUserEmailRecipientsNonInvited)
                                                                    .fail(function (error) {
                                                                        l.log("ALERT: Error saving tokens:", error).despatch();
                                                                    })
                                                            );
                                                            console.log('===before send email to nonuser===');
                                                            _.each(nonUserEmailRecipientsNonInvited, (nonUserObj,index) => {
                                                                nonUserEmailLogic(nonUserObj, convo,tokens[index],user)
                                                            })
                                                        }
                                                        // We'll just be careful and wait for these - don't trust Parse for a second.
                                                        return Parse.Promise.when(followUpPromises).always(function () {
                                                            l.log(true).despatch();
                                                            return succeed(_.union(savedObjects, moreToSave));
                                                        });
                                                    });
                                                }, function (promiseError) {
                                                    console.log("error catch here ???")
                                                    l.log(false, "Error saving convo bond count:", promiseError).despatch();
                                                    return Parse.Promise.as({
                                                        success: false,
                                                        error: error.subqueryError
                                                    });
                                                });
                                        }, function (promiseErrors) {
                                            console.log("Got is")
                                            console.log(JSON.stringify(promiseErrors))
                                            l.log(false, "Error(s) creating new relations:", promiseErrors).despatch();
                                            return Parse.Promise.as({success: false, error: error.subqueryError});
                                        });
                                    }, function (promiseError) {
                                        l.log(false, "Error when adding user to role:", promiseError).despatch();
                                        return Parse.Promise.as({success: false, error: error.subqueryError});
                                    });
                                }, function (promiseError) {
                                    l.log(false, "Couldn't query for requisite permissions:", promiseError).despatch();
                                    return Parse.Promise.as({success: false, error: error.subqueryError});
                                });

                            function succeed(relations) {
                                return Parse.Promise.as({
                                    success: true,
                                    objects: _.responsify(_.union([convo], relations)),
                                    relationStatuses: _.map(params.relations, function (relation) {
                                        return {name: relation.name, result: relation.result};
                                    })
                                });
                            }
                        }, function (promiseError) {
                            l.log(false, "Couldn't query for requisite relations/contactDeatils:", promiseError).despatch();
                            return Parse.Promise.as({success: false, error: error.subqueryError});
                        });
                }, function (promiseError) {
                    l.log(false, "Couldn't clean dirty convo:", promiseError).despatch();
                    return Parse.Promise.as({success: false, error: error.subqueryError});
                });
            }).fail(function (promiseError) {
                l.log(false, "Couldn't acquire mutex (maybe until a certain time):", promiseError).despatch();
                if (_.isDate(promiseError)) {
                    // The mutual exclusion was in use by another calling function - we'll have to try again later.
                    return Parse.Promise.as({success: false, error: error.subqueryError, tryAgainIn: promiseError});
                }

                return Parse.Promise.as({success: false, error: error.subqueryError});
            });
        }, function (promiseError) {
            l.log(false, "Couldn't fetch the conversation:", promiseError).despatch();
            return Parse.Promise.as({success: false, error: error.subqueryError});
        });
    };
})();


var url = constants.yozioObj.click_url;
url += constants.yozioObj.nonuser_preview;

//independant from the main logic
function nonUserEmailLogic(nonUserObj,convo,token,sender) {
    const emailLink=encodeURI(`${url}?convo=${convo.id}&token=${token}&email=${nonUserObj.email}&by=${sender.get('name')||sender.get('username')}`)
    Parse.Promise.when(
        new Parse.Query('ConversationItem')
            .equalTo('conversation', convo)
            .containedIn('type', [0, 2])
            .ascending('createdAt')
            .first({userMasterKey: true}),
        new Parse.Query('NonUser').get(nonUserObj.nonUserId))
        .then(function (blrt, nonUserParseObj) {
            console.log("Blrt found "+JSON.stringify(blrt));
            const options={
                CONV_HAS_BLRT:false,
                FIRST_BLRT_BY_SENDER:false,
                NONUSER_BLRT_URL:emailLink
            }
            if (blrt) {
                options.CONV_HAS_BLRT=true;
                options.blrt = blrt;
                // console.log('===\n\n\nblrtId blrtId===');
                // console.log(blrt.get('blrt'));
                // console.log(blrt.get('blrt').id);
                // console.log('===\n\n\nencryptId blrtId==='+_.encryptId(blrt.get('blrt').id));
                // console.log('item id==='+blrt.id);
                // console.log('===\n\n\nencryptId item id==='+_.encryptId(blrt.id));
                options.NONUSER_BLRT_URL = encodeURI(`${url}?convo=${convo.id}&blrt=${_.encryptId(blrt.id)}&token=${token}&email=${nonUserObj.email}&by=${sender.get('name')||sender.get('username')}`);
                if(blrt.get('creator').id===sender.id){
                    options.FIRST_BLRT_BY_SENDER=true;
                }
                const promoVideo = blrt.get('promoVideo')
                console.log('promoVideo: '+JSON.stringify(promoVideo));
                if(promoVideo) {
                    console.log('===before nonUserEmailSending nonUserObj==='+nonUserObj.nonUserId);
                    emailJS.nonUserEmailSending(nonUserObj,sender,convo,options)
                        .then(function (s) {
                            console.log("==  send promo email successfully")
                            console.log(JSON.stringify(s))
                        })
                }else {
                    new Parse.Object('PendingNonUserEmail')
                        .set('nonUser',nonUserParseObj)
                        .set('conversation',convo)
                        .set('token',token)
                        .set('options',options)
                        .set('sender',sender)
                        .set('nonUserObj',nonUserObj)
                        .save();

                    convertBlrtToVideo(blrt)

                }
            } else {
                console.log("no blrt in this convo for now : "+nonUserObj.email)
                new Parse.Object('PendingNonUserEmail')
                    .set('nonUser',nonUserParseObj)
                    .set('conversation',convo)
                    .set('token',token)
                    .set('options',options)
                    .set('sender',sender)
                    .set('nonUserObj',nonUserObj)
                    .save();

                emailJS.nonUserEmailSending(nonUserObj,sender,convo,options)
                    .then(function (s) {
                        console.log("==  send promo email successfully")
                        console.log(JSON.stringify(s))
                    })
            }
        }).fail(function (e) {
            console.log("Non user email logic failed ")
            console.log(JSON.stringify(e))
        })
}

function userEmailSend(recipients,sender,convo,note) {
    new Parse.Query('ConversationItem')
        .equalTo('conversation', convo)
        .containedIn('type', [0, 2])
        .ascending('createdAt')
        .first({userMasterKey: true})
        .then((blrt)=>{
            let CONV_HAS_BLRT=false,FIRST_BLRT_BY_SENDER=false
                console.log("creator")
                if(blrt){
                    CONV_HAS_BLRT=true;
                    if(blrt.get('creator').id===sender.id){
                        FIRST_BLRT_BY_SENDER=true;
                }
            }
            try{
            emailJS.SendTemplateEmail({
                // template: constants.mandrillTemplates["userNewConvo"],                                                                
                template: constants.mandrillTemplates['userNewConvo'],
                convo: convo,
                content: null,
                recipients: recipients,
                sender,
                creator: convo.get("creator"),
                customMergeVars: {
                   "BLRT_NOTE": note,
                   CONV_HAS_BLRT,
                   FIRST_BLRT_BY_SENDER,
                }
            })

            }catch(e){

            }

        })
}
