/**
 * Created by test on 2/12/2015.
 */

/***************************************
 * Fetch all the conversations of a user
 ***************************************/

var _ = require("underscore");
var constants = require("cloud/constants");
const CONVO_PER_REQUEST = 20;
var api = require("cloud/api/util");
var l = api.loggler;

//util function the construct the error json
function unsuccessful(error) {
    var response = {
        success: false,
        error: error
    };
    // if(savedObjects.length != 0)    response.objects =
    // _.responsify(savedObjects);

    return Parse
        .Promise
        .as(response);
}

class ResultObject {
    constructor(relation) {
        try {
            var convo = relation.get("conversation");
            if (!convo.get("incomplete") && !convo.get('deletedAt')) {
                var lastMedia = convo.get('lastMedia');
                if (lastMedia) {
                    lastMedia = lastMedia.map(function (m) {
                        return {
                            id: m.id,
                            encryptType: m.get('encryptType'),
                            encryptValue: m.get('encryptValue'),
                            mediaFile: m.get("mediaFile") && m
                                .get("mediaFile")
                                .url(),
                            mediaFormat: m.get('mediaFormat'),
                            pageArgs: m.get('pageArgs'),
                            name: m.get('name')
                        }
                    })
                }

                this.className = constants.convoUserRelation.keys.className;
                this.encryptedId = _.encryptId(relation.id);

                // Assign the default value to false by double pipe if these two properties is
                // null or undifined in parse
                this.flagged = relation.get("flagged") || false;
                this.archived = relation.get("archived") || false;
                this.contentUpdate = relation.get("contentUpdate") || convo.get("contentUpdate");
                var convoCreator = relation.get("convoCreator")
                this.convoCreator = {
                    className: "Parse.User",
                    id: convoCreator.id,
                    encryptedId: _.encryptId(convoCreator.id),
                    name: convoCreator.get("name")
                }

                this.conversation = {
                    className: "Conversation",
                    lastMediaArgument: convo.get('lastMediaArgument'),
                    lastMedia: lastMedia,
                    specialSlug: convo.get('specialSlug') || false,
                    encryptedId: _.encryptId(convo.id),
                    plainId: convo.id,
                    blrtName: convo.get("blrtName"),
                    //todo some conversation do not have a thumbnail
                    thumbnail: convo.get("thumbnailFile") && convo
                        .get("thumbnailFile")
                        .url(),
                    disallowPublicBlrt: convo.get("disallowPublicBlrt"),
                    disableOthersToAdd: convo.get("disableOthersToAdd"),
                    localGuid: convo.get('localGuid'),
                    unusedMedia: convo.get('unusedMedia') || false

                }

                //read count and unread count for the user-conversation
                var indexViewedList = relation.get("indexViewedList");
                var viewedArr = _.expand(indexViewedList);

                var totalCount = convo.get("readableCount");
                var readCount = viewedArr.length;
                this.unreadCount = totalCount - readCount;
                this.itemCount = totalCount;
                this.tag = relation.get("tags") || []
            }
        } catch (e) {
            console.error(e)
        }
    }
}

exports[1] = function (req) {
    const {params, user} = req
    // const pageNo = parseInt(params.page);
    const inputText = params.input;
    
    const regex = new RegExp(inputText, "i");
    console.log('regex regex regex: '+regex);
    console.log('type type type==='+params.type);
    // Both main query and flagged query should build on this. And the two queries do
    // not influence each other by using different instance

    if(params.type === 'inbox') {
        function getBaseQuery() {
            const query = new Parse
                .Query("ConversationUserRelation")
                .equalTo("user", user)
                .equalTo("deletedAt", null)
                .notEqualTo("archived", true)
                .descending("contentUpdate")
                .include("conversation") // Conversation
                .include("conversation.lastMedia") 
                .include("convoCreator"); //nested Parse.Object -- User
            return query;
        }
    }else {
        function getBaseQuery() {
            const query = new Parse
                .Query("ConversationUserRelation")
                .equalTo("user", user)
                .equalTo("deletedAt", null)
                .equalTo("archived", true)
                .descending("contentUpdate")
                .include("conversation") // Conversation
                .include("conversation.lastMedia") 
                .include("convoCreator"); //nested Parse.Object -- User
            return query;
        }
    }

    let convosPromise = getBaseQuery()
        .find({useMasterKey: true})
        .then(relations=>{
            let convos = [];
            relations.map(relation=>{
                if(relation) {
                    convos.push(relation.get('conversation').id);
                }
            });
            return convos;
        });

    let skipConvoPromise;
    let convoId = params.bottomConvoId; //for loading more search results, skip previous matched convos

    if (convoId) {
        const decryptId = _.decryptId(convoId);
        const convo = new Parse.Object('Conversation')
        convo.id = decryptId
        skipConvoPromise = new Parse
            .Query('ConversationUserRelation')
            .equalTo('conversation', convo)
            .equalTo('user', user)
            .first({useMasterKey: true})
            .then((relation) => {
                return relation
            })
            .fail((e) => {
                console.log(e)
            })
    } else {
        skipConvoPromise = new Parse
            .Query('ConversationUserRelation')
            .equalTo('user', user)
            .descending("contentUpdate")
            .first({useMasterKey: true})
            .then((relation) => {
                return relation
            })
    }
    
    return Parse.Promise.when(convosPromise, skipConvoPromise).then((convos, relation) => {
        console.log("relation.get(\'contentUpdate\'): " +relation.get('contentUpdate'));
        if (!relation.get('contentUpdate')) {
            console.log('return []');
            return {data: []}
        }
        try {
            if(convoId) {
                var mainQueryPromise = getBaseQuery()
                    .lessThan('contentUpdate', relation.get('contentUpdate'))
                    .matchesQuery("conversation", new Parse.Query("Conversation").containedIn('objectId', convos).matches('blrtName', regex))
                    .limit(CONVO_PER_REQUEST)
                    .find({useMasterKey: true})
                    .then(function (result) {
                        console.log('result length==='+result.length);
                        // console.log('result: '+JSON.stringify(result));
                        var convoInfo = [];
                        result.forEach(function (relation) {
                            convoInfo.push(new ResultObject(relation));
                        })
                        return convoInfo;
                    })    

                return mainQueryPromise.then((data) => {
                    return {data}
                });                
            }else {
                var mainQueryPromise = getBaseQuery()
                    .lessThanOrEqualTo('contentUpdate', relation.get('contentUpdate'))
                    .matchesQuery("conversation", new Parse.Query("Conversation").containedIn('objectId', convos).matches('blrtName', regex))
                    .limit(CONVO_PER_REQUEST)
                    .find({useMasterKey: true})
                    .then(function (result) {
                        console.log('result length==='+result.length);
                        // console.log('result: '+JSON.stringify(result));
                        var convoInfo = [];
                        result.forEach(function (relation) {
                            convoInfo.push(new ResultObject(relation));
                        })
                        return convoInfo;
                    })    

                return mainQueryPromise.then((data) => {
                    return {data}
                });
            }


        } catch (e) {
            console.log(e)
        }
    })
}
