/**
 * @public createMedia process is required by creating conversationItem (Blrt) and creating conversation with Blrt
 * @param {Object} conversationItem - part of req.params provided directly from client side
 * @param {Object} user - req.user
 * @param {Object} savedObjects - we need to keep track of saved object to return to client
 * @param {object} device - we need this for some extra info such as device ,app version etc
 * @return {Object} - an object contain dbArgs denoting lastMediaArgument in Conversation and argument in
 * arguments in ConversationItem (they are shared)
 * */
// const Immutable =require('immutable');
const constants = require("cloud/constants");
const _ = require("underscore");
function createMedia(conversationItem, user, savedObjects, device) {
    console.log("====== original conversationItem =====")
    console.log(JSON.stringify(conversationItem))

    // console.log("====== after rejection ====")
    // console.log(JSON.stringify(conversationItem))

    //media arg
    var media = conversationItem.media;
    var hiddenItems = conversationItem.arg.hiddenItems;
    //hold all the media id in this conversation ,including the hidden ones
    const mediaIds = [];
    //hold the local ids (i.e. not in DB before) in this conversation,excluding the hidden ones
    const mediaGuids = [];

    _.each(media, function (m) {
        // If a mediaId was provided then the client realises that a particular
        // media was already created and we can use that to directly find it.
        if (m.mediaId)
            mediaIds.push(m.mediaId);
        else
            mediaGuids.push(m.localGuid);           // Media GUID
    });

    // As well as going through the Blrt's media, we need to go through it's hidden pages.
    // Hidden media only gives us one media ID which could be either a local GUID or template.
    _.each(hiddenItems, function (hiddenMedia) {
        if (
            hiddenMedia.mediaId.lastIndexOf("local+=", 0) !== 0 &&
            hiddenMedia.mediaId.lastIndexOf("_template", 0) !== 0
        )
            mediaIds.push(hiddenMedia.mediaId);
    });
    var mediaQuery;
    if (mediaGuids.length != 0)
        mediaQuery = new Parse.Query(constants.mediaAsset.keys.className)
            .containedIn(constants.base.keys.localGuid, mediaGuids);

    if (mediaIds.length != 0) {
        var idQuery = new Parse.Query(constants.mediaAsset.keys.className)
            .containedIn(constants.base.keys.objectId, mediaIds);

        if (mediaQuery)
            mediaQuery = Parse.Query.or(mediaQuery, idQuery);
        else
            mediaQuery = idQuery;
    }

    if (mediaQuery != null)
        mediaQuery = mediaQuery.find()
    else
        mediaQuery = Parse.Promise.as([]);
    

    return mediaQuery.then(function createMedia(foundMedia) {
        var foundMediaGuids = [];
        var foundMediaIds = [];

        _.each(foundMedia, function (media) {
            foundMediaGuids.push(media.get(constants.base.keys.localGuid));
            foundMediaIds.push(media.id);
        });


        // We've got a problem if a provided mediaId wasn't found :(
        var missingException = "Missing mediaId!";
        try {
            var remainingMedia = _.reject(media, function (m) {
                if (m.mediaId) {
                    if (_.contains(foundMediaIds, m.mediaId))
                        return true;
                    throw missingException;
                } else
                    return _.contains(foundMediaGuids, m.localGuid);
            });
        } catch (e) {

            if (e == missingException) {
            } else
                throw e;
        }
        // ### Step 2: Create all media
        var newMediaObjects = remainingMedia.map((media)=>new Parse.Object(constants.mediaAsset.keys.className)
            .set(constants.base.keys.version, constants.mediaAsset.version)
            .set(constants.base.keys.appVersion, device.version)
            .set(constants.base.keys.device, device.device)
            .set(constants.base.keys.os, device.os)
            .set(constants.base.keys.osVersion, device.osVersion)
            .set(constants.base.keys.localGuid, media.localGuid)
            .set(constants.base.keys.creator, user)
            .set(constants.base.keys.encryptType, media.encryptType)
            .set(constants.base.keys.encryptValue, media.encryptValue)
            .set(constants.mediaAsset.keys.mediaFile, media.file)
            .set(constants.mediaAsset.keys.mediaFormat, media.format)
            .set(constants.mediaAsset.keys.name, media.name)
            .set(constants.mediaAsset.keys.pageArgs, media.pageArgs));

        var newMediaPromise = new Parse.Promise.as([])
        if (newMediaObjects.length > 0) {
            newMediaPromise = Parse.Object.saveAll(newMediaObjects)
        }

        return newMediaPromise
            .then(function createDbArg(createdMedia) {

                var errorToCheckFor = "Invalid arg error";
                var dbArg = {
                    "AudioLength": conversationItem.arg.audioLength
                };

                var foundAndCreatedMedia = _.union(foundMedia, createdMedia)

                // We need to go through the provided convoItem args and swap out all localGUIDs
                // for the actual created media's ID.
                // To do this we have to go through the `items` and `hiddenItems` arrays in the
                // args, find all mediaIds which map to a localGUID and reverse map it to point
                // to the actual media's ID. There may be templates in here - just ignore them.

                // I'm providing the parameter name and the database name together because I'd like
                // to share all of this code - they behave exactly the same.
                try {
                    _.each([["items", "Items"], ["hiddenItems", "HiddenItems"]], function (targetArray) {
                        if (conversationItem.arg[targetArray[0]] != null) {
                            dbArg[targetArray[1]] = [];

                            _.each(conversationItem.arg[targetArray[0]], function (page) {
                                var mediaId = page.mediaId;
                                var isMediaId = false;

                                // There are four cases we need to account for:
                                // 1. The mediaId specifies a template: no substitution.
                                if (mediaId.lastIndexOf("_template:", 0) !== 0) {
                                    if (mediaId.lastIndexOf("local+=", 0) !== 0)
                                        isMediaId = true;

                                    var accountedFor = false;
                                    _.each(foundAndCreatedMedia, function (savedMedia) {
                                        // 2. The mediaId matches an existing mediaId: no substitution.
                                        if (mediaId == savedMedia.id)
                                            accountedFor = true;

                                        // 3. The mediaId matches a localGUID - replace with mediaId.
                                        if (mediaId == savedMedia.get(constants.base.keys.localGuid)) {
                                            mediaId = savedMedia.id;
                                            accountedFor = true;
                                        }
                                    });

                                    // 4. None of the above? Catastrophic error!
                                    if (!accountedFor) {
                                        // However, if it's a hiddenItem, then instead of exploding horrendously,
                                        // simply ignore that item :D Schmick! Nice one Kris.
                                        if (targetArray[0] == "hiddenItems" && !isMediaId)
                                            return;

                                        throw errorToCheckFor;
                                    }
                                }

                                dbArg[targetArray[1]].push({
                                    "PageString": page.pageString,
                                    "MediaId": mediaId
                                });
                            });
                        }
                    });
                } catch (e) {
                    if (e == errorToCheckFor) {
                        // If we're in here then something went wrong when going through the convoItem
                        // args (other than a programming error >.>).
                    }
                    else
                        throw e;
                }


                if (savedObjects) {
                    savedObjects.push(...foundAndCreatedMedia)
                }

                return {dbArg, foundAndCreatedMedia}

            });
    })

}

exports['createMedia']=createMedia