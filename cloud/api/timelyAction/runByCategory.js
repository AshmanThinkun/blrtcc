var timelyActionsJS = require("cloud/timelyActions");

var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");

// # v1: timelyActionRunByCategory
(function () {
    var params;

    // ## Enums

    // ### timelyActionRunByCategoryError

    // Subclasses APIError.
    var timelyActionRunByCategoryError = _.extend(api.error, { 
        invalidCategory:             { code: 412, message: "The provided category is not support" },
        userIdRequired:              { code: 413, message: "UserId is require for this category"},
    });

    // Aliased as error for simplicity of internal use.
    var error = timelyActionRunByCategoryError;

    // ## Flow
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        var l = api.loggler;
        var now = new Date();

        Parse.Cloud.useMasterKey();

        params = req.params;
        var category = params.category;

        switch(category){
            case "userLogin":
                if(!params.userId)
                    return unsuccessful(error.userIdRequired);
                break;
            default:
                return unsuccessful(error.invalidCategory);
        }


        var queriedUser = new Parse.Object("User");
        queriedUser.id = params.userId;                 
        var now = new Date();
        var query  = new Parse.Query("TimelyAction")
                        .lessThan("executionTime", now)
                        .equalTo("category", category)
                        .equalTo("user", queriedUser);


        return timelyActionsJS.processAction(query,[queriedUser])
            .then(function(result){
                if(result.success){ 
                    return result;
                }else{
                    return unsuccessful(error.subqueryError);
                }
        });
    };
})();
