var api = require("cloud/api/util");
var apply = require("cloud/api/developer/apply");
var resetKeys = require("cloud/api/developer/resetKeys");
var updateSettings = require("cloud/api/developer/updateSettings");

// # Developer
var define=require('../../lib/cloud').define()
Parse.Cloud.define=define
// ## Apply
Parse.Cloud.define("developerApply", function (req, res) {
    var supported = api.supports(req, "developerApply");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true
            }).then(function (processed) {
                if(processed.success) {
                    return apply[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Reset Keys
Parse.Cloud.define("developerResetKeys", function (req, res) {
    var supported = api.supports(req, "developerResetKeys");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true
            }).then(function (processed) {
                if(processed.success) {
                    return resetKeys[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Update Settings
Parse.Cloud.define("developerUpdateSettings", function (req, res) {
    var supported = api.supports(req, "developerUpdateSettings");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true,
                params: {
                    requestWebhookUrl: {
                        type: api.varTypes.text,
                        allowBlank: true
                    }
                }
            }).then(function (processed) {
                if(processed.success) {
                    return updateSettings[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});