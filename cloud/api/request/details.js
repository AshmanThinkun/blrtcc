var _ = require("underscore");
var api = require("cloud/api/util");

// # v1: requestDetails
(function () {
    var user;
    var params;

    // ## Enums

    // ### requestDetailsError

    // Subclasses APIError.
    var requestDetailsError = _.extend(api.error, { 
        noRequest:           { code: 404, message: "A BlrtRequest with the provided ID was not found or was not permissable for the requesting user." }
    });

    var error = requestDetailsError;

    // ## Flow
    exports[1] = function (req) {
        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        user = req.user;
        params = req.params;

        return new Parse.Query("BlrtRequest")
        	.equalTo("objectId", params.requestId)
        	.first()
        	.then(function (blrtRequest) {
        		if(!blrtRequest ||
        			// We don't let users other than the requestee access this
        			// BlrtRequest if it's marked as "strict".
        			( 
        				blrtRequest.get("strict") &&
        				blrtRequest.get("requestee") &&
        				blrtRequest.get("requestee").id != user.id
        			)
        		) {
        			if(!blrtRequest) l.log(false, "BlrtRequest not found: " + params.requestId).despatch();
        			else 			 l.log(true, "Invalid user requesting strict BlrtRequest details: " + params.requestId).despatch();
        			return { success: false, error: error.noRequest };
        		}

        		var result = {
        			success: true,
        			id: _.encryptId(blrtRequest.id)
        		};

        		var name = blrtRequest.get("name");
        		if(name) result.name = name;

        		var url = blrtRequest.get("url");
        		if(url) result.url = url;

        		var mediaUrls = blrtRequest.get("mediaUrls");
        		if(mediaUrls) result.media = mediaUrls;

        		var recipients = blrtRequest.get("recipients");
        		if(recipients) result.recipients = recipients;

                result.makeBlrtPublic = blrtRequest.get("makeBlrtPublic");

        		return result;
        	}, function (promiseError) {
        		l.log(false, "Error when searching for BlrtRequest:", promiseError).despatch();
        		return Parse.Promise.as({ success: false, error: error.subqueryError });
        	});

        // We need to check the type just incase they provided an empty string.
        if(typeof params.requestWebhookUrl != "undefined")
            user.set("developerRequestWebhookUrl", params.requestWebhookUrl);

        l.log(typeof params.requestWebhookUrl);

        if(!user.dirty()) {
            l.log(true, "ALERT: Developer just updated settings without anything changing, legit?").despatch();

            return { success: true };
        }

        return user
            .save()
            .then(function () {
                l.log(true).despatch();
                return { success: true };
            }, function (promiseError) {
                l.log(false, "Failed to save user:", promiseError).despatch();
                return Parse.Promise.as({
                    success: false,
                    error: error.subqueryError
                });
            });
    };
})();