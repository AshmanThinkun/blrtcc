var api = require("cloud/api/util");

var createWithBlrt = require("cloud/api/conversation/createWithBlrt");
var createWithoutBlrt = require("cloud/api/conversation/createWithoutBlrt");
var deleteJS = require("cloud/api/conversation/delete");
var duplicateFromBlrt = require("cloud/api/conversation/duplicateFromBlrt");
var request = require("cloud/api/conversation/request");
var publicJS = require("cloud/api/conversation/public");
var requestAccess = require("cloud/api/conversation/requestAccess");
var processRequest = require("cloud/api/conversation/processRequest");
var define = require('../../lib/cloud').define()
Parse.Cloud.define = define
// # Conversation ## CreateWithBlrt
Parse
    .Cloud
    .define("conversationCreateWithBlrt", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationCreateWithBlrt");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        // console.log(req.params)

        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        name: {
                            type: api.varTypes.text,
                            required: true
                            // Although I'd like to use a max char limit, I can't because I want to return a
                            // more specific error in this case... so the endpoint logic needs to handle it
                            // and return the proper error. } required: { presence: true, max:
                            // constants.convo.nameCharLimit }
                        },
                        localGuid: {
                            type: api.varTypes.text,
                            required: true
                        },
                        thumbnail: {
                            type: api.varTypes.file,
                            required: true
                        },
                        item: {
                            type: api.varTypes.object,
                            required: true,
                            subtype: {
                                arg: {
                                    type: api.varTypes.object,
                                    required: true,
                                    subtype: {
                                        audioLength: {
                                            type: api.varTypes.int,
                                            required: {
                                                presence: true,
                                                min: 0
                                            }
                                        },
                                        items: {
                                            type: api.varTypes.array,
                                            required: {
                                                presence: true,
                                                min: 1
                                            },
                                            subtype: {
                                                pageString: {
                                                    type: api.varTypes.text,
                                                    required: true
                                                },
                                                mediaId: {
                                                    type: api.varTypes.text,
                                                    required: true
                                                }
                                            }
                                        },
                                        hiddenItems: {
                                            type: api.varTypes.array,
                                            subtype: {
                                                pageString: {
                                                    type: api.varTypes.text,
                                                    required: true
                                                },
                                                mediaId: {
                                                    type: api.varTypes.text,
                                                    required: true
                                                }
                                            }
                                        }
                                    }
                                },
                                localGuid: {
                                    type: api.varTypes.text,
                                    required: true
                                },
                                files: {
                                    type: api.varTypes.object,
                                    required: true,
                                    subtype: {
                                        encryptType: {
                                            type: api.varTypes.int,
                                            required: true
                                        },
                                        encryptValue: {
                                            type: api.varTypes.text
                                        },
                                        audio: {
                                            type: api.varTypes.file,
                                            required: true
                                        },
                                        touch: {
                                            type: api.varTypes.file,
                                            required: true
                                        }
                                    }
                                },
                                media: {
                                    // This may not be provided if the conversation is created solely from
                                    // templates.
                                    type: api.varTypes.array,
                                    subtype: {
                                        mediaId: {
                                            type: api.varTypes.text
                                        },
                                        name: {
                                            type: api.varTypes.text,
                                            // All requirements start from the root of the parameters so we need to build a
                                            // path back here. "_" tells the requirement to reference the object sobject to
                                            // the variable being checked.
                                            required: "!item.media._.mediaId",
                                            strict: true
                                        },
                                        localGuid: {
                                            type: api.varTypes.text,
                                            required: "!item.media._.mediaId",
                                            strict: true
                                        },
                                        file: {
                                            type: api.varTypes.file,
                                            required: "!item.media._.mediaId",
                                            strict: true
                                        },
                                        format: {
                                            type: api.varTypes.int,
                                            required: "!item.media._.mediaId",
                                            strict: true
                                        },
                                        encryptType: {
                                            type: api.varTypes.int,
                                            required: "!item.media._.mediaId",
                                            strict: true
                                        },
                                        encryptValue: {
                                            type: api.varTypes.text,
                                            // This strictly cannot be provided if mediaId is provided, however if mediaId
                                            // is not provided then this either can or cannot be provided. Therefore, when
                                            // !item.media._.mediaId then required = null. In JavaScript, if you typed `var
                                            // x = 5 && null` x would evaluate to equal null. } TODO: support this! (don't
                                            // forget to uncomment pageArgs after) } required: "!item.media._.mediaId &&
                                            // null", A strict null requirement doesn't check anything. } strict: true
                                        },
                                        pageArgs: {
                                            type: api.varTypes.text,
                                            // } required: "!item.media._.mediaId && null", } strict: true
                                        }
                                    }
                                }
                            }
                        },
                            // A requestId can be specified. If it is, then this conversation has been
                            // created following the opening on a Blrt Request, so we need to perform some
                            // additional tracking.
                            requestId: {
                                type: api.varTypes.id
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return createWithBlrt[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

Parse
    .Cloud
    .define("conversationCreate", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationCreateWithBlrt");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        name: {
                            type: api.varTypes.text,
                            required: true
                            // Although I'd like to use a max char limit, I can't because I want to return a
                            // more specific error in this case... so the endpoint logic needs to handle it
                            // and return the proper error. } required: { presence: true, max:
                            // constants.convo.nameCharLimit }
                        },
                        localGuid: {
                            type: api.varTypes.text,
                            required: true
                        },
                        ctags: {
                            type: api.varTypes.text,
                            required: false
                            // Although I'd like to use a max char limit, I can't because I want to return a
                            // more specific error in this case... so the endpoint logic needs to handle it
                            // and return the proper error. } required: { presence: true, max:
                            // constants.convo.nameCharLimit }
                        },
                            // thumbnail: {     type: api.varTypes.file,     required: true },

                        }
                    })
                    .then(function (processed) {
                        console.log("processed :: ")
                        console.log(processed)
                        if (processed.success) {
                            return createWithoutBlrt[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

const renameConvo = require("cloud/api/conversation/rename.js");
Parse
    .Cloud
    .define("conversationRename", function (req, res) {
        var supported = api.supports(req, "conversationCreateWithBlrt");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api.process(req, {
                    auth: true,
                    params: {
                        name: {
                            type: api.varTypes.text,
                            required: true
                        },
                        convoId: {
                            type: api.varTypes.id,
                            required: true
                        }
                    }
                }).then(function (processed) {
                    console.log("processed :: ")
                    console.log(processed)
                    if (processed.success) {
                        return renameConvo[1](processed.req).then(function (result) {
                            return res.success(JSON.stringify(result));
                        }, function (error) {
                            api
                                .loggler
                                .log(false, "ALERT: Big error, caught outside function:", error)
                                .despatch();
                            return res.error({success: false, error: api.error.subqueryError});
                        });
                    } else 
                        return res.success(JSON.stringify(processed));
                    }
                , function (error) {
                    console.log(JSON.stringify(error))
                    return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                });
            }
        };

        return v[supported.version]();
    });

// ## Delete
Parse
    .Cloud
    .define("conversationDelete", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationDelete");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api
                    .process(req, {
                        auth: true,
                        params: {
                            conversationId: {
                                type: api.varTypes.id,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return deleteJS[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## DuplicateFromBlrt This endpoint exists so the owner of a particular Blrt
// can decide to create a new conversation using that Blrt, instead of having to
// record another Blrt with similar media and information. The endpoint is being
// extended to support duplicating to multiple new conversations to support our
// own administrative efforts.
Parse
    .Cloud
    .define("conversationDuplicateFromBlrt", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationDuplicateFromBlrt");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    params: {
                        userId: { // only for admin
                            type: api.varTypes.text
                        },
                        conversationItemId: {
                            type: api.varTypes.text,
                            required: true
                        },
                        names: {
                            type: api.varTypes.array, //client duplication only support 1 name
                            required: {
                                presence: true,
                                min: 1
                            },
                                subtype: api.varTypes.text
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return duplicateFromBlrt[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## Request A conversation can be programmatically requested via api.blrt.com,
// or manually requested by a user from request.blrt.com. Unfortunately we don't
// have time to properly shift the old generate code over to this cloud
// function, so instead we'll make this cloud function simply point to the old
// generation code. When more time avails we can properly implement this.
Parse
    .Cloud
    .define("conversationRequest", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationRequest");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    params: {
                        // This should either be request.blrt.com or api.blrt.com.
                        type: {
                            type: api.varTypes.text,
                            required: true
                        },
                        // This is required if the request is coming from api.blrt.com.
                        developerId: {
                            type: api.varTypes.text
                        },
                        name: {
                            type: api.varTypes.text
                        },
                        media: {
                            type: api.varTypes.array,
                            subtype: api.varTypes.text
                        },
                        message: {
                            type: api.varTypes.text
                        },
                        requestee: {
                            type: api.varTypes.email
                        },
                        recipients: {
                            type: api.varTypes.array,
                            subtype: api.varTypes.email
                        },
                            webhook: {
                                type: api.varTypes.text
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return request[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## Public
Parse
    .Cloud
    .define("conversationPublic", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationPublic");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, { // An authenticated user is expected for client app side api call
                    auth: true,
                    params: {
                        allowPublicBlrt: {
                            type: api.varTypes.bool,
                            required: true
                        },
                        conversationId: {
                            type: api.varTypes.text,
                            required: true
                        }
                    }
                }).then(function (processed) {
                    if (processed.success) {
                        return publicJS[1](processed.req).then(function (result) {
                            return res.success(JSON.stringify(result));
                        }, function (error) {
                            api
                                .loggler
                                .log(false, "ALERT: Big error, caught outside function:", error)
                                .despatch();
                            return res.error({success: false, error: api.error.subqueryError});
                        });
                    } else 
                        return res.success(JSON.stringify(processed));
                }, function (error) {
                    return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                });
            }
        };

        return v[supported.version]();
    });

// ## Request Access
Parse
    .Cloud
    .define("conversationRequestAccess", function (req, res) {
        req.start();
        var supported = api.supports(req, "conversationRequestAccess");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api
                    .process(req, { // An authenticated user is expected for client app side api call
                        auth: true,
                        params: {
                            conversationId: {
                                type: api.varTypes.text,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return requestAccess[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

// ## Process Request
Parse
    .Cloud
    .define("conversationProcessRequest", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "conversationProcessRequest");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        
        var v = {
            1: function () {
                api.process(req, {
                    // An authenticated user is expected for client app side api call [Guichi] this
                    // function can also be called by clicking a link in email,that is why
                    // authentication is not essential auth : true,
                    params: {
                        conversationId: {
                            type: api.varTypes.text,
                            required: true
                        },
                        eventId: {
                            type: api.varTypes.text,
                            required: true
                        },
                        accept: {
                            type: api.varTypes.bool,
                            required: true
                        },
                            token: {
                                type: api.varTypes.text
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return processRequest[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

var peopleInConversation = require("cloud/api/conversation/getPeople");
Parse
    .Cloud
    .define("getPeopleInConversation", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "alwaysAvailable");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api
                    .process(req, {
                        auth: true,
                        params: {
                            conversationId: {
                                type: api.varTypes.id,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return peopleInConversation[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });

var fetchConvIdByBlrt = require("cloud/api/conversation/fetchConvIdByBlrt");
Parse
    .Cloud
    .define("fetchConvIdByBlrt", function (req, res) {
        req.start(); //start tracking time
        var supported = api.supports(req, "alwaysAvailable");
        if (!supported.success) 
            return res.success(JSON.stringify(supported));
        var v = {
            1: function () {
                api
                    .process(req, {
                        auth: true,
                        params: {
                            blrtId: {
                                type: api.varTypes.id,
                                required: true
                            }
                        }
                    })
                    .then(function (processed) {
                        if (processed.success) {
                            return fetchConvIdByBlrt[1](processed.req).then(function (result) {
                                return res.success(JSON.stringify(result));
                            }, function (error) {
                                api
                                    .loggler
                                    .log(false, "ALERT: Big error, caught outside function:", error)
                                    .despatch();
                                return res.error({success: false, error: api.error.subqueryError});
                            });
                        } else 
                            return res.success(JSON.stringify(processed));
                        }
                    , function (error) {
                        return res.success(JSON.stringify({success: false, error: api.apiError.subqueryError}))
                    });
            }
        };

        return v[supported.version]();
    });