var constants = require("cloud/constants");
var api = require("cloud/api/util"); 
var _ = require("underscore");
// var utilJS = require("cloud/util");
// var Mixpanel = require("cloud/mixpanel");


// # v1: urlInfo
(function () {
    var params;
    var user;

    // ## Enums

    // ### urlInfo

    // Subclasses APIError.
    var urlInfoError = _.extend(api.error, { 
        invalidUrl:              	    { code: 620, message: "This is an invalid url." },
        authenticationFailure:          { code: 621, message: "User is not allowed for accessing this Blrt" },
        playCounterLimit:              	{ code: 622, message: "Play counter limit reached for this private blrt" },
        notBlrt:                        { code: 624, message: "This is not a Blrt"},
        readOnly:                       { code: 625, message: "This conversation is readonly" }
    });

   
    // Aliased as error for simplicity of internal use.
    var error = urlInfoError;
    exports[1] = function (req) {
        function unsuccessful(error) {
            var response = {
                success: false,
                error: error
            };

            return Parse.Promise.as(response);
        }

        function parseUrl (url) {
            var match,
                pl     = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
                query  = url;

            var urlParams = {};
            while (match = search.exec(query)){
                var key =  decode(match[1]);
                if(key.length>2 && key.substr(key.length-2)=="[]"){
                    key = key.substring(0,key.length-2);
                    if(! urlParams[key]){
                        urlParams[key]=[];
                    }
                    urlParams[key].push(decode(match[2]));
                }else{
                    urlParams[key] = decode(match[2]);
                }
            }
           return urlParams;
        }

        var l = api.loggler;

        Parse.Cloud.useMasterKey();

        params = req.params;
        user = req.user;
        var url = params.url;
        var includeMedia = params.includeMedia? true: false;

        //parse url to get the conversation item id
        if(!_.isUrl(url)){
            l.log(false, "Invalid url: ", url).despatch();
            return unsuccessful(error.invalidUrl);
        }

        //detecting params from url
        var pre = "/blrt/";
        var index = url.indexOf(pre);
        if(index==-1){
            pre = "/conv/";
            index = url.indexOf(pre);
            if(index==-1){
                pre = "/make/";
                index = url.indexOf(pre);
            }
        }
        
        switch(pre){
            case "/blrt/":
                return processBlrtUrl();
            case "/conv/":
                return processConvUrl();
            default:
                return processRequestUrl();
        }


        function processBlrtUrl(){
            var blrtId = url.substr(index + pre.length);

            var queryString = _.singleSplit(blrtId, '?');
            if(queryString.length == 2) {
                blrtId = queryString[0];
                queryString = queryString[1];
            } else
                queryString = '';

            blrtId = blrtId.replace(/\/$/, '');

            blrtId = _.decryptId(blrtId);

            //get conversationItem
            return new Parse.Query("ConversationItem")
                .equalTo("objectId", blrtId)
                .include("conversation")
                .include("creator")
                .include("media")
                .first()
                .then(function (blrtItem) {
                    if(! blrtItem){
                        l.log(false, "Conversation item id not found: ", blrtId).despatch();
                        return unsuccessful(error.invalidUrl);
                    }
                    
                    //check whether this is a blrt
                    if(!blrtItem.get("media")){
                        l.log(false, "Requesting blrt id is not a blrt: ", blrtId).despatch();
                        return unsuccessful(error.notBlrt);
                    }

                    var conversation = blrtItem.get("conversation");
                    var isPublicBlrt = blrtItem.get("isPublicBlrt");

                    //if conversation is deleted
                    if(conversation.get("deletedAt")){
                        l.log(false, "Requesting blrt deleted: ", blrtId).despatch();
                        return unsuccessful(error.notBlrt);
                    }

                    var argData;
                    try {
                        argData = JSON.parse(blrtItem.get("argument"));
                    } catch(error) {
                        l.log(false, "Can't get arg from json string", blrtItem.get("argument")).despatch();
                        return unsuccessful(error.subqueryError);
                    }

                    var thumbnailFile  =  blrtItem.get("thumbnailFile");
                    if(!thumbnailFile){
                        thumbnailFile = conversation.get("thumbnailFile");
                    }
                    var embed = {
                        title:                  isPublicBlrt? blrtItem.get(constants.convoItem.keys.PublicName): conversation.get(constants.convo.keys.Name),
                        creator:                blrtItem.get("creator").get("name"),
                        createdAt:              blrtItem.createdAt,
                        duration:               argData.AudioLength,
                        thumbnail:              thumbnailFile? thumbnailFile.url(): "",
                        pages:                  blrtItem.get("media").length,
                        "public":               isPublicBlrt? true:false,
                        blrtId:                 _.encryptId(blrtItem.id),
                        conversationId:         _.encryptId(conversation.id),
                    };

                    if(!includeMedia){
                        l.log(true, "Return Blrt item from url: ", url).despatch();
                        return {
                            success: true,
                            type: "blrt",
                            data: embed
                        };
                    }

                    // get the meidas 
                    var media ={};
                    var canPlay = false;
                    var waitPromises = [];

                    var playCounter = blrtItem.get("privateViewCounter") || 0;
                    if(!isPublicBlrt){
                        //user is the conversation member, then they can play
                        if(user){
                            var query = new Parse.Query("ConversationUserRelation")
                                .equalTo("conversation", conversation)
                                .equalTo("user", user)
                                .count()
                                .then(function(relationCount){
                                    if(relationCount>0)
                                        canPlay = true;
                                    return;
                                }, function(error){
                                    l.log(false, "Error find conversation user relation", user.id ).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                            waitPromises.push(query);
                        }else if(playCounter < constants.convoItem.maxPrivateViewCounter){
                            canPlay = true;
                        }
                    }else{
                        canPlay = true;
                    }

                    return Parse.Promise.when(waitPromises).then(function(){
                        embed.accessible = canPlay;
                        if(!canPlay){
                            l.log(false, "user doesn't allowed to view this blrt" ).despatch();
                            resError = user? error.authenticationFailure: error.playCounterLimit;

                            return {
                                success: false,
                                error: resError,
                                data: embed
                            };
                        }
                        var playData = {
                            touchDataFile:      blrtItem.get("touchDataFile").url(),
                            audioFile:          blrtItem.get("audioFile").url(),
                            argument:           blrtItem.get("argument"),
                            encryptValue:       blrtItem.get("encryptValue"),
                            encryptType:        blrtItem.get("encryptType"),
                            media:              _.map(blrtItem.get("media"), function(asset){
                                return {
                                    id: asset.id,
                                    encryptType: asset.get("encryptType"),
                                    encryptValue: asset.get("encryptValue"),
                                    mediaFile: asset.get("mediaFile").url(),
                                    mediaFormat: asset.get("mediaFormat"),
                                    pageArgs: asset.get("pageArgs")
                                };
                            })
                        };



                        l.log(true, "Return Blrt item from url: ", url).despatch();
                        var toReturn =  {
                            success: true,
                            type: "blrt",
                            data: embed
                        };

                        toReturn.data.playData = playData;

                        return toReturn;

                    },function (error){
                        return unsuccessful(error.subqueryError);
                    });
                    

                }, function(error){
                    l.log(false, "Error finding conversation item: " + blrtId).despatch();
                    return unsuccessful(error.subqueryError);
                });
        }

        function processConvUrl(){
            var convoId = url.substr(index + pre.length);

            var queryString = _.singleSplit(convoId, '?');
            if(queryString.length == 2) {
                convoId = queryString[0];
                queryString = queryString[1];
            } else
                queryString = '';

            convoId = convoId.replace(/\/$/, '');

            convoId = _.decryptId(convoId);

            //get conversation
            return new Parse.Query("Conversation")
                .equalTo("objectId", convoId)
                .include("creator")
                .first()
                .then(function (conversation) {
                    if(! conversation){
                        l.log(false, "Conversation id not found: ", convoId).despatch();
                        return unsuccessful(error.invalidUrl);
                    }
                    

                    //if conversation is deleted
                    if(conversation.get("deletedAt")){
                        l.log(false, "Requesting blrt deleted: ", convoId).despatch();
                        return unsuccessful(error.invalidUrl);
                    }
                    if (conversation.get("readonly")) {
                        l.log(false, "Requesting conversation is readonly: ", convoId).despatch();
                        return unsuccessful(error.readOnly);
                    }

                    var thumbnailFile  =  conversation.get("thumbnailFile");
                    var embed = {
                        title:                  conversation.get(constants.convo.keys.Name),
                        creator:                conversation.get("creator").get("name"),
                        createdAt:              conversation.createdAt,
                        thumbnail:              thumbnailFile? thumbnailFile.url(): "",
                        conversationId:         _.encryptId(conversation.id),
                    };

                    var waitPromises = [];
                    var accessible = false;
                    if(user){
                        var query = new Parse.Query("ConversationUserRelation")
                                .equalTo("conversation", conversation)
                                .equalTo("user", user)
                                .count()
                                .then(function(relationCount){
                                    if(relationCount>0)
                                        accessible = true;
                                    return;
                                }, function(error){
                                    l.log(false, "Error find conversation user relation", user.id ).despatch();
                                    return unsuccessful(error.subqueryError);
                                });
                        waitPromises.push(query);
                    }

                    //get first Blrt Id
                    var firstBlrtId;
                    var BlrtIdQuery = new Parse.Query("ConversationItem")
                        .equalTo("conversation",conversation)
                        .equalTo("type", 0)
                        .first()
                        .then(function(firstBlrt){
                            if(!firstBlrt){
                                l.log(false, "Error find first Blrt");
                                return;
                            }
                            firstBlrtId = firstBlrt.id;

                        }, function(error){
                            l.log(false, "Error find firstBlrt").despatch();
                            return unsuccessful(error.subqueryError);
                        });
                    waitPromises.push(BlrtIdQuery);
                    
                    return Parse.Promise.when(waitPromises).then(function(){
                        embed.accessible = accessible;
                        embed.blrtId = _.encryptId(firstBlrtId);

                        l.log(true, "Return conversation from url: ", url).despatch();
                        var toReturn =  {
                            success: true,
                            type: "conv",
                            data: embed
                        };

                        return toReturn;

                    },function (error){
                        return unsuccessful(error.subqueryError);
                    });
                    

                }, function(error){
                    l.log(false, "Error finding conversation: " + convoId).despatch();
                    return unsuccessful(error.subqueryError);
                });
        }

        function processRequestUrl(){
            var queryString = _.singleSplit(url,"?");
            if(queryString.length<2){
                return unsuccessful(error.invalidUrl);
            }
            var requestParams = {};
            var title;
            try{
                requestParams = parseUrl(queryString[1]);
                var secondSlash = queryString[1].lastIndexOf("/");
                var firstSlash = queryString[1].lastIndexOf("/", secondSlash-1);
                title = queryString[1].substring(firstSlash+1, secondSlash);

                if(title.toLowerCase()=="send"){
                    throw "invalid";
                }
            }catch(e){
                return unsuccessful(error.invalidUrl);
            }
            var embed = {};

            if(requestParams.req){
                var requestId = _.decryptId(requestParams.req);
                return new Parse.Query("BlrtRequest")
                    .equalTo("objectId",requestId)
                    .include("creator")
                    .include("conversation")
                    .first()
                    .then(function(requestItem){
                        if(!requestItem){
                            l.log(false, "request id not found: ", requestId).despatch();
                            return unsuccessful(error.invalidUrl);
                        }

                        var requestor = requestItem.get("creator").get("name");
                        var webhook = requestItem.get("customWebhook");
                        if(webhook && (webhook.indexOf("https://blrt-jira.herokuapp.com/")!=-1 ||webhook.indexOf("https://jira.blrt.com/")!=-1)){
                            requestor = "Blrt JIRA Plugin";
                        }

                        embed = {
                            title:                  requestItem.get("name"),
                            requestor:              requestor,
                            message:                requestItem.get("message"),
                            requestId:              _.encryptId(requestItem.id),
                            createdAt:              requestItem.createdAt, 
                            recipients:             requestItem.get("recipients")? requestItem.get("recipients").length:0,
                            answered:               requestItem.get("answered"),
                            fileCount:{
                                wwws:               requestItem.get("wwwwCount")?requestItem.get("wwwwCount"):0,
                                images:             requestItem.get("imageCount")?requestItem.get("imageCount"):0,
                                pdfs:               requestItem.get("pdfCount")?requestItem.get("pdfCount"):0
                            }
                        };

                        embed.fileCount.total = embed.fileCount.wwws + embed.fileCount.images + embed.fileCount.pdfs;

                        if(embed.fileCount.total==0){
                            calcuteFileCount();
                        }


                        if(embed.fileCount.images>0) {
                            embed.thumbnail = requestItem.get("media")[0];
                            if(embed.thumbnail.indexOf("//")==-1){
                                embed.thumbnail = "//"+embed.thumbnail;
                            }
                        } else if(embed.fileCount.wwws>0) {
                            embed.thumbnail = "//s3.amazonaws.com/blrt-emails/www-thumbnail.png";
                        } else{
                            embed.thumbnail = "//s3.amazonaws.com/blrt-emails/pdf-thumbnail.png";
                        }
                        

                        if(embed.answered){
                            var conversation = requestItem.get("conversation");
                            embed.conversationId = conversation.id;
                            embed.thumbnail = conversation.get("thumbnailFile").url();
                        }

                        var toReturn =  {
                            success: true,
                            type: "request",
                            data: embed
                        };

                        return toReturn;

                    },function(error){
                        l.log(false, "Error finding requestItem: " + requestId).despatch();
                        return unsuccessful(error.subqueryError);
                    })
            }else{
                embed = {
                    title:                  title,
                    message:                requestParams.message,
                    recipients:             requestParams.e.length,
                    fileCount:{
                        wwws:       0,
                        pdfs:       0,
                        images:     0
                    }
                };

                calcuteFileCount();

                var toReturn =  {
                    success: true, 
                    type: "request",
                    data: embed
                };

                return Parse.Promise.as(toReturn);
            }

            function calcuteFileCount(){
                embed.fileCount.wwws = requestParams.url? 1:0;
                if(requestParams.m){
                    embed.fileCount.pdfs = _.find(requestParams.m, function(item){
                        if(item.length>3 && item.substring(item.length-4).toLowerCase()==".pdf"){
                            return true;
                        }
                        return false;
                    });
                    embed.fileCount.pdfs = embed.fileCount.pdfs? embed.fileCount.pdfs:0;

                    embed.fileCount.images = requestParams.m.length - embed.fileCount.pdfs;
                }

                embed.fileCount.total = embed.fileCount.wwws + embed.fileCount.images + embed.fileCount.pdfs;
            }
        }
    }

})();
