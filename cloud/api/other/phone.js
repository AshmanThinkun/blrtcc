var _ = require("underscore");
var constants = require('cloud/constants.js');
var twilio = require('twilio')(constants.twilioKey, constants.twilioToken);
var api = require("cloud/api/util"); 

const Errors = _.extend(api.error, { 
    detailInUse: {code: 410, message: "Another user owns a contact detail which the request would've clashed with." }
});

const PhoneNumberClassName = "PhoneNumberVerification";

/*
request.param: {
	phoneNumber: "429292929",
	countryCode" "+61"
}
*/
// # v1: RequestPhoneVerificationCode
(function () {
	exports[1] = function (req) {
		var phoneNumber = req.params.phoneNumber;
		var countryCode = req.params.countryCode;
		// console.log('countryCode==='+countryCode);
		// console.log('phoneNumber==='+phoneNumber);

		const regex1 = new RegExp('^0?');
		let phone = phoneNumber.replace(regex1, ''); // remove leading zero from phoneNumber
		let cc = countryCode.replace(/^[+]?/, ''); // "+" is special regex character, should be removed
		console.log('countryCode==='+cc);
		console.log('phone==='+phone);

		const regex2 = new RegExp(cc+'0?'+phone);
		console.log('regex2==='+regex2);

		phoneNumber = countryCode+phoneNumber; // exactly same as user's input
		
		var res = new Parse.Promise();

		Parse.Cloud.useMasterKey();
		new Parse.Query(constants.contactTable.keys.className)
        .equalTo(constants.contactTable.keys.type, "phoneNumber")
        .matches(constants.contactTable.keys.Value, regex2)
        .include('user')
        .find({useMasterKey: true})
        .then(function(result){
        	console.log('\n\n\nresult user===');
        	console.log('result.length==='+result.length);
        	// console.log('\nresult: '+JSON.stringify(result));
            
            // the same phoneNumber from deletedUser is allowed to be added to new account
        	if(result.length > 0 && result[0].get('user').get('accountStatus') !== 'deleted') {
        		console.log('\n\nuser: '+result[0].get('user').get('accountStatus'));
        		return res.resolve({ success: false, error: Errors.detailInUse });
        	}

        	var validUntil = new Date();
			validUntil.setTime(validUntil.getTime() + 15*60*1000);

			//generate new code 
			var verificationCode = Math.floor(1000 + Math.random() * 9000).toString();


			Parse.Cloud.useMasterKey();
			var savePromise = new Parse.Query(PhoneNumberClassName)
				.equalTo("phoneNumber", phoneNumber)
				.first()
				.then(function(existingRecord){
					if(existingRecord) {
						return existingRecord.set("verificationCode", verificationCode)
							.set("verificationCodeValidUntil", validUntil)
							.save();
					}else {
						var PhoneNumberClass = Parse.Object.extend(PhoneNumberClassName);
						var newRecord = new PhoneNumberClass();
						return newRecord.set("phoneNumber", phoneNumber)
							.set("verificationCode", verificationCode)
							.set("verificationCodeValidUntil", validUntil)
							.save();
					}
				});

			var sendSMSPromise = new Parse.Promise();

			console.log('\n\n\n===phoneNumber==='+phoneNumber);

			twilio.sendSms({
			    to: phoneNumber,
			    from: constants.twilioNumber["US"],
			    body: "Blrt verification code: "+verificationCode
			  }, function(err, responseData) {
			    if (err) {
			      console.log(err);
			      sendSMSPromise.reject(err);
			    } else {
			      console.log('\n\n\n===responseData===');
			      console.log(responseData);
			      sendSMSPromise.resolve();
			    }
			  }
			);

			Parse.Promise.when(savePromise, sendSMSPromise).then(function() {
				return res.resolve({ success: true });
			},function(error){
				console.log('===error2===');
				return res.resolve({ success: false, error: Errors.subqueryError });
			});
        },function(error){
        	console.log('===error1===');
			return res.resolve({ success: false, error: Errors.subqueryError });
		});

		return res;	
	}
})();

/*
input{
	phoneNumber, verificationCode
}

response true, false in promise
*/
exports.VerifyPhoneNumber = function(phoneNumber, verificationCode){
	return new Parse.Query(PhoneNumberClassName)
		.equalTo("phoneNumber", phoneNumber)
		.first()
		.then(function(existingRecord){
			var now = new Date();
			if(existingRecord && existingRecord.get("verificationCode")==verificationCode && existingRecord.get("verificationCodeValidUntil")>now){
				return true;
			}
			return false;
		});
}