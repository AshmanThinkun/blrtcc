var api = require("cloud/api/util");

var runByCategory = require("cloud/api/timelyAction/runByCategory");
var define=require('../../lib/cloud').define()
Parse.Cloud.define=define
// # TimelyAction

// ## RunByCategory
Parse.Cloud.define("timelyActionRunByCategory", function (req, res) {
    var supported = api.supports(req, "timelyActionRunByCategory");
    if(!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    category: {
                        type: api.varTypes.text,
                        required: true
                    },
                    userId: {
                        type: api.varTypes.text,
                    },
                }
            }).then(function (processed) {
                if(processed.success) {
                    return runByCategory[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "ALERT: Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});