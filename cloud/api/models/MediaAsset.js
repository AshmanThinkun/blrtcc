const constants = require("cloud/constants");
const _ = require("underscore");
class MediaAsset extends Parse.Object {
    constructor(id) {
        super('MediaAsset')
        if (id) {
            this.id = id
        }
    }

    static createMediaForBlrt(mediaArray, blrtArg, user, savedObjects, device) {
        //media arg
        var media = mediaArray
        var hiddenItems = blrtArg.hiddenItems;
        //hold all the media id in this conversation ,including the hidden ones
        let mediaIds = [];
        // hold the local ids (i.e. not in DB before) in this conversation,excluding the
        // hidden ones
        const mediaGuids = [];

        _.each(media, function (m) {
            // If a mediaId was provided then the client realises that a particular media
            // was already created and we can use that to directly find it.
            if (m.mediaId) 
                mediaIds.push(m.mediaId);
            else 
                mediaGuids.push(m.localGuid); // Media GUID
            }
        );

        // As well as going through the Blrt's media, we need to go through it's hidden
        // pages. Hidden media only gives us one media ID which could be either a local
        // GUID or template.
        _.each(hiddenItems, function (hiddenMedia) {
            if (hiddenMedia.mediaId.lastIndexOf("local+=", 0) !== 0 && hiddenMedia.mediaId.lastIndexOf("_template", 0) !== 0) 
                mediaIds.push(hiddenMedia.mediaId);
            }
        );
        var mediaQuery;

        if (mediaGuids.length !== 0) 
            mediaQuery = new Parse.Query(constants.mediaAsset.keys.className).containedIn(constants.base.keys.localGuid, mediaGuids);
        

        /**
         * Parse allow maximum 100 items in containedIn query, if there are more than 100 items to "contain" and we
         * put all of them in single query, problem happen
         */
        let idQuerys = []; //if the mediaIds is really long, we need to split it 
        if (mediaIds.length !== 0) {
            mediaIds = _.uniq(mediaIds, x => x) //dedepulicate
            let current = 0;
            let tmp_ids = [];
            for (let i = 0; i < mediaIds.length; i++) {
                tmp_ids.push(mediaIds[i])
                if (i > 0 && (i % 50 === 0)) { //each batch comtains maximum 50 items
                    idQuerys.push(new Parse.Query(constants.mediaAsset.keys.className).containedIn(constants.base.keys.objectId, mediaIds.slice(current, i)))
                    current = i;
                    tmp_ids = []
                }
            }
            idQuerys.push(new Parse.Query(constants.mediaAsset.keys.className).containedIn(constants.base.keys.objectId, mediaIds.slice(current)))
        }
        const allQuerys = [
            mediaQuery, ...idQuerys
        ]
        const allQueryPromise = _
            .compact(allQuerys) //remove "mediaQuery" if there is no new media
            .map(q => q.find({useMasterKey: true})) //run the query and get the promise

        return Parse
            .Promise
            .when(allQueryPromise)
            // return mediaQuery
            .then(function createMedia(foundMedia) {
                foundMedia = _.flatten(foundMedia)
                var foundMediaGuids = [];
                var foundMediaIds = [];

                _.each(foundMedia, function (media) {
                    foundMediaGuids.push(media.get(constants.base.keys.localGuid));
                    foundMediaIds.push(media.id);
                });

                // We've got a problem if a provided mediaId wasn't found :(
                var missingException = "Missing mediaId!";
                try {
                    var remainingMedia = _.reject(media, function (m) {
                        if (m.mediaId) {
                            if (_.contains(foundMediaIds, m.mediaId)) 
                                return true;
                            throw missingException;
                        } else 
                            return _.contains(foundMediaGuids, m.localGuid);
                        }
                    );
                } catch (e) {
                    console.log("exceptopn here")
                    console.log(JSON.stringify(e))
                    if (e === missingException) {} else 
                        throw e;
                    }
                
                // ### Step 2: Create all media
                var newMediaObjects = remainingMedia.map((media) => new Parse.Object(constants.mediaAsset.keys.className).set(constants.base.keys.version, constants.mediaAsset.version).set(constants.base.keys.appVersion, device.version).set(constants.base.keys.device, device.device).set(constants.base.keys.os, device.os).set(constants.base.keys.osVersion, device.osVersion).set(constants.base.keys.localGuid, media.localGuid).set(constants.base.keys.creator, user).set(constants.base.keys.encryptType, media.encryptType).set(constants.base.keys.encryptValue, media.encryptValue).set(constants.mediaAsset.keys.mediaFile, media.file).set(constants.mediaAsset.keys.mediaFormat, media.format).set(constants.mediaAsset.keys.name, media.name).set('thumbnailFile', media.thumbnail).set(constants.mediaAsset.keys.pageArgs, media.pageArgs));

                var newMediaPromise = new Parse
                    .Promise
                    .as([])
                if (newMediaObjects.length > 0) {
                    newMediaPromise = Parse
                        .Object
                        .saveAll(newMediaObjects)
                }

                return newMediaPromise.then(function createDbArg(createdMedia) {
                    console.log("Create media")
                    console.log(JSON.stringify(createdMedia))
                    var errorToCheckFor = "Invalid arg error";
                    var dbArg = {
                        "AudioLength": blrtArg.audioLength
                    };

                    var foundAndCreatedMedia = _.union(foundMedia, createdMedia)

                    // We need to go through the provided convoItem args and swap out all localGUIDs
                    // for the actual created media's ID. To do this we have to go through the
                    // `items` and `hiddenItems` arrays in the args, find all mediaIds which map to
                    // a localGUID and reverse map it to point to the actual media's ID. There may
                    // be templates in here - just ignore them. I'm providing the parameter name and
                    // the database name together because I'd like to share all of this code - they
                    // behave exactly the same.
                    try {
                        _
                            .each([
                                [
                                    "items", "Items"
                                ],
                                ["hiddenItems", "HiddenItems"]
                            ], function (targetArray) {
                                if (blrtArg[targetArray[0]] !== null) {
                                    dbArg[targetArray[1]] = [];

                                    _.each(blrtArg[targetArray[0]], function (page) {
                                        var mediaId = page.mediaId;
                                        var isMediaId = false;

                                        // There are four cases we need to account for:
                                        // 1. The mediaId specifies a template: no substitution.
                                        if (mediaId.lastIndexOf("_template:", 0) !== 0) {
                                            if (mediaId.lastIndexOf("local+=", 0) !== 0) 
                                                isMediaId = true;
                                            
                                            var accountedFor = false;
                                            _.each(foundAndCreatedMedia, function (savedMedia) {
                                                // 2. The mediaId matches an existing mediaId: no substitution.
                                                if (mediaId === savedMedia.id) 
                                                    accountedFor = true;
                                                
                                                // 3. The mediaId matches a localGUID - replace with mediaId.
                                                if (mediaId === savedMedia.get(constants.base.keys.localGuid)) {
                                                    mediaId = savedMedia.id;
                                                    accountedFor = true;
                                                }
                                            });

                                            // 4. None of the above? Catastrophic error!
                                            if (!accountedFor) {
                                                // However, if it's a hiddenItem, then instead of exploding horrendously, simply
                                                // ignore that item :D Schmick! Nice one Kris.
                                                if (targetArray[0] == "hiddenItems" && !isMediaId) 
                                                    return;
                                                
                                                throw errorToCheckFor;
                                            }
                                        }

                                        dbArg[targetArray[1]].push({"PageString": page.pageString, "MediaId": mediaId});
                                    });
                                }
                            });
                    } catch (e) {
                        console.log("!!!!!!!")
                        console.log(JSON.stringify(e))
                        if (e === errorToCheckFor) {
                            // If we're in here then something went wrong when going through the convoItem
                            // args (other than a programming error >.>).
                        } else 
                            throw e;
                        }
                    
                    if (savedObjects) {
                        savedObjects.push(...foundAndCreatedMedia)
                    }

                    return {dbArg, foundAndCreatedMedia}

                });
            }, (e) => {
                console.log("Error found")
                console.log(JSON.stringify(e))
            })
    }

    static createMediaForImageItem(mediaArray, user, device) {
        const mediaGuids = mediaArray.map(m => m.localGuid);
        return new Parse
            .Query('MediaAsset')
            .containedIn('localGuid', mediaGuids)
            .find()
            .then(function (foundMediaAsset) {
                const foundGuids = foundMediaAsset.map(m => m.localGuid);
                const remainingMediaToCreate = _.reject(mediaArray, m => {
                    return _.contains(foundGuids, m.localGuid)
                })

                const mediaAssetObjs = remainingMediaToCreate.map(media => (new MediaAsset().set(constants.base.keys.version, constants.mediaAsset.version).set(constants.base.keys.appVersion, device.version).set(constants.base.keys.device, device.device).set(constants.base.keys.os, device.os).set(constants.base.keys.osVersion, device.osVersion).set(constants.base.keys.localGuid, media.localGuid).set(constants.base.keys.creator, user).set(constants.base.keys.encryptType, media.encryptType).set(constants.base.keys.encryptValue, media.encryptValue).set(constants.mediaAsset.keys.mediaFile, media.file).set(constants.mediaAsset.keys.mediaFormat, media.format).set(constants.mediaAsset.keys.name, media.name).set(constants.mediaAsset.keys.pageArgs, media.pageArgs).set('thumbnailFile', media.thumbnail).save()))

                return Parse
                    .Promise
                    .when(mediaAssetObjs)

            })
    }

}
Parse
    .Object
    .registerSubclass('MediaAsset', MediaAsset);
module.exports = MediaAsset;