const constants = require("cloud/constants");
class Blrt extends Parse.Object {
    constructor(id) {
        super('Blrt')
        if (id) {
            this.id = id
        }
        // console.log(this)
    }

    static createBlrt(encryptType, encryptValue, media, args, audio, touch, thumbnail) {
        var masterACL = new Parse.ACL();
        return new Blrt()
            .set(constants.base.keys.encryptType, encryptType)
            .set(constants.base.keys.encryptValue, encryptValue)
            .set(constants.convoItem.keys.Media, media)
            .set(constants.convoItem.keys.Argument, args)
            .set(constants.convoItem.keys.AudioFile, audio)
            .set(constants.convoItem.keys.TouchDataFile, touch)
            .set(constants.convoItem.keys.thumbnail, thumbnail)
            .setACL(masterACL)
            .save()
    }

}
Parse
    .Object
    .registerSubclass('Blrt', Blrt);
module.exports = Blrt