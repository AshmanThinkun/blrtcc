//node module
// var gm = require('gm')
var gm = require('gm').subClass({imageMagick: true});

var api = require("cloud/api/util");
var changePrimary = require("cloud/api/user/changePrimary");
var block = require("cloud/api/user/block");
var getTipBanners = require("cloud/api/user/getTipBanners");
var changeAccountStatus = require("cloud/api/user/changeAccountStatus");
var getUserInfo = require("cloud/api/user/fetchOtherUserInfo");
var define = require('../../lib/cloud').define()
Parse.Cloud.define = define
// # User
var userRegister = require("cloud/api/user/register");
// ## Register
Parse.Cloud.define("userRegister", function (req, res) {
    req.start();
    var supported = api.supports(req, "userRegister");
    if (!supported.success)
    // } We can't run this endpoint under the requested version!
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {
                    email: {
                        type: api.varTypes.email,
                        // required: true
                    },
                    password: {
                        type: api.varTypes.password
                    },
                    token: {
                        type: api.varTypes.authToken,
                        required: "!password"           // Only required if password is NOT provided
                    },
                    name: {
                        type: api.varTypes.text,
                        // required: true
                    },
                    extra: {                            // Extra is for things like industry or gender, etc
                        type: api.varTypes.dictionary
                    },
                    isQuickSignUp: {
                        type: api.varTypes.bool
                    },
                    isForcedSignUp: {
                        type: api.varTypes.bool
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return userRegister[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        api.loggler.log(false, "Big error, caught outside function:", error).despatch();
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Change Primary
Parse.Cloud.define("userChangePrimary", function (req, res) {
    var supported = api.supports(req, "userChangePrimary");
    if (!supported.success)
    // } We can't run this endpoint under the requested version!
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                auth: true, // An authenticated user is expected
                params: {
                    email: {
                        type: api.varTypes.email,
                        required: true
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return changePrimary[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## Block
//  used for blocking the sender to send "add conversation" emails to recevicer
Parse.Cloud.define("userBlock", function (req, res) {
    var supported = api.supports(req, "userBlock");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {                     // An authenticated user is expected for client app side api call
                params: {
                    blockUserId: {
                        type: api.varTypes.text,
                        //required: true
                    },
                    blockUserEmail: {
                        type: api.varTypes.email,
                    },
                    requestUserId: {                //if no auth provided, then the requestUserId is required
                        type: api.varTypes.text,
                    },
                    blockFlag: {
                        type: api.varTypes.int
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return block[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

// ## TipBanners
//  The banner shows at the bottom of the screen
Parse.Cloud.define("userGetTipBanners", function (req, res) {
    var supported = api.supports(req, "userGetTipBanners");
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                params: {}
            }).then(function (processed) {
                if (processed.success) {
                    return getTipBanners[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


// ## Change Account status
// the user can only be able to change its own status
Parse.Cloud.define("userChangeAccountStatus", function (req, res) {
    // console.log('\n\n\n===function called==='+req);
    var supported = api.supports(req, "userChangeAccountStatus");
    // console.log('\n\n===supported.success==='+supported.success);
    if (!supported.success)
        return res.success(JSON.stringify(supported));

    var v = {
        1: function () {
            api.process(req, {
                // auth: true,    //the user can only be able to change its own status, todo...
                params: {
                    from: {
                        type: api.varTypes.text,
                        required: true
                    },
                    accountStatus: {
                        type: api.varTypes.text,
                        required: true
                    },
                    username: {
                        type: api.varTypes.text,
                        required: false // required = true means this param must be provided, if required = false, there is no necessary to provide this param
                    }
                }
            }).then(function (processed) {
                // console.log('\n\n\n===processed.success==='+processed.success);
                if(processed.success) {
                    // console.log('\n\n\n===function called accountStatus==='+processed.req.params.accountStatus)
                    //client app can only delete account, check:
                    if (processed.req.params.accountStatus !== "deleted") {
                        return res.success(JSON.stringify({
                            success: false,
                            error: api.error.invalidParameters
                        }));
                    }

                    return changeAccountStatus[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});


Parse.Cloud.define("fetchUserInfo", function (req, res) {
    var supported = api.supports(req, "alwaysAvailable");
    if (!supported.success)
        return res.success(JSON.stringify(supported));
    var v = {
        1: function () {
            api.process(req, {
                params: {
                    userId: {
                        type: api.varTypes.text,
                        required: true
                    }
                }
            }).then(function (processed) {
                if (processed.success) {
                    return getUserInfo[1](processed.req).then(function (result) {
                        return res.success(JSON.stringify(result));
                    }, function (error) {
                        return res.error({
                            success: false,
                            error: api.error.subqueryError
                        });
                    });
                } else
                // } API processing error, easy to return
                    return res.success(JSON.stringify(processed));
            }, function (error) {
                return res.success(JSON.stringify({
                    success: false,
                    error: api.apiError.subqueryError
                }))
            });
        }
    };

    return v[supported.version]();
});

const avatarSize = 480;
//todo error happened in callback can crash the server,we need a consistent way to prevent this
//todo I think wrap everything in a Promise is good way,especially for third-party library
Parse.Cloud.define("uploadAvatar", function (req, res) {
    var params = req.params;
    var user = req.user;
    var base64Data = params.base64Data;
    var type = params.type;
    req.start();

    // console.log('\n\nbase64Data: '+base64Data);

    //read input base64 string and generate a resized base64 string
    new Promise(function (resolve, reject) {
        gm(new Buffer(base64Data, 'base64'))
            .resize(avatarSize, avatarSize, '^')
            .gravity('Center')
            .crop(avatarSize, avatarSize)
            .stream(function (err, stdout) {
                if(err) reject(err);
                
                console.log('\n\n\n\n\n');
                console.log(stdout);
                
                // var buf = new Buffer(0);
                var buf = new Buffer(base64Data, 'base64');

                // stdout.on('data', function (d) {
                //     buf = Buffer.concat([buf, d]);
                // });

                stdout.on('end', function () {
                    console.log('\n\n\n\n\n===end===');
                    resolve(buf.toString('base64'))
                });
            })
    })
    //create file with Parse sdk
        .then(function (base64) {
            console.log('\n\nParse file base64: '+base64);
            var file = new Parse.File('avatar', {base64: base64}, type)
            return file.save()
        })
        //set user thumbnail
        .then(function (fileResult) {
            user.set('avatar', fileResult)
            return user.save(null,{useMasterKey: true});
        })
        .then(function (savedUser) {
            res.success({success: true})
        }, function (e) {
            res.error({success: false, message: JSON.stringify(e)})
        })


});
