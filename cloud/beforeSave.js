///*
var constants = require("cloud/constants");
var _ = require("underscore");

// Parse.Cloud.beforeDelete(constants.mediaAsset.keys.className, function (req, res) {
//     return res.error("I can't let you do that, fox.");
// });

// Parse.Cloud.beforeSave(Parse.User, function (req, res) {
//
// 	//request by master key
//     if (req.master) {
//     	return res.success();
//     }
//
//     //client key can't change account status
//     if(req.object.dirty("accountStatus")|| req.object.dirty("pioneerStatus") ||
//         req.object.dirty("accountTypeExpirationDate") || req.object.dirty("accountTypeStartDate") || req.object.dirty("activeAccountTypeId")){
//     	return res.error("Sorry, you can't change it.");
//     }
//
//     return res.success();
// });
//
exports.saveContentUpdateForConvo = function(convo){
        var objToSave = [];
        return new Parse.Query("ConversationUserRelation")
            .equalTo("conversation", convo)
            .find({useMasterKey:true})
            .then(function(rels){
                _.each(rels, function(rel){
                    rel.set("contentUpdate", convo.get("contentUpdate"));
                    objToSave.push(rel);
                });
                return 1;
            }).then(function(){
                if(objToSave.length>0){
                    return Parse.Object.saveAll(objToSave,{useMasterKey:true});
                }
            }).then(function(){
                return true;
            });
    }
