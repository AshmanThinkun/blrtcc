using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using BlrtData.ExecutionPipeline;
using Parse;

namespace BlrtData
{
	public static class AppLinkHandler
	{
		public enum AppLinkVersion
		{
			None,
			Unknown,

			Version_1_0,

		}

		private class AppLink
		{

			public bool Valid { get { return !string.IsNullOrWhiteSpace (type); } }

			public AppLinkVersion version;
			public AppLink parent;
			public string type;
			public string value;
			public string leftovers;
			public string subdomain;

			public Dictionary<string, string> arguments;



			public AppLink (AppLinkVersion version, string link, AppLink parent, string subdomain = null)
			{

				this.version = version;
				this.parent = parent;
				this.subdomain = subdomain;

				arguments = new Dictionary<string, string> ();

				var match = Regex.Match (link, @"(?:\/([^/?]+))(?:\/([^/?]+))?([^?]*)(?:\?(\S*))?");

				if (match.Success) {
					type = match.Groups [1].Value.Trim ();
					value = System.Web.HttpUtility.UrlDecode (match.Groups [2].Value.Trim ());
					leftovers = match.Groups [3].Value.Trim ();
					var a = System.Web.HttpUtility.UrlDecode (match.Groups [4].Value.Trim ());


					foreach (var arg in a.Split ('&')) {
						var split = arg.Split (new char [] { '=' }, 2);

						if (split.Length > 1) {
							var k = split [0];
							var v = split [1];

							if (k.Contains ("[]")) {
								for (int i = 0; ; ++i) {
									var str = k.Replace ("[]", "[" + i + "]");
									if (!arguments.ContainsKey (str)) {
										k = str;
										break;
									}
								}
							}

							if (arguments.ContainsKey (k))
								arguments [k] = v;
							else
								arguments.Add (k, v);
						}
					}

				}
			}

			public AppLink (AppLink link) : this (link.version, link.leftovers, link)
			{
				arguments = new Dictionary<string, string> (link.arguments);
			}
		}




		public static bool BlrtParseUrl (string url, bool inApp)
		{
			// First remove any URLs like d.blrt.co
			string subdomain = null;
			if (Regex.Match (url, @"^\/([a-zA-Z0-9-_]*\.[a-zA-Z0-9-_]*)+\/").Success) {
				subdomain = url.Substring (1, url.IndexOf ('.', 1) - 1);

				//if that's from yozio, we need to convert it to standard link
				var urlObj = new Uri ("http://" + url.Substring (1));
				if (urlObj.Segments.Length > 0) {
					const string CONVERSATION = BlrtUtil.URLConstants.Paths.CONVERSATION;
					const string BLRT = BlrtUtil.URLConstants.Paths.BLRT;

					switch (urlObj.Segments [urlObj.Segments.Length - 1]) {
					case CONVERSATION:
					case CONVERSATION + "/":
						var convo = System.Web.HttpUtility.ParseQueryString (urlObj.Query).Get ("convo");
						url = "/" + urlObj.Authority + "/" + CONVERSATION + "/" + convo + urlObj.Query;
						break;
					case BLRT:
					case BLRT + "/":
						var blrtId = System.Web.HttpUtility.ParseQueryString (urlObj.Query).Get (BLRT);
						var convId = System.Web.HttpUtility.ParseQueryString (urlObj.Query).Get ("convo");
						url = "/" + urlObj.Authority;

						if (!string.IsNullOrEmpty (convId)) {
							url += "/" + CONVERSATION + "/" + convId;
						}

						if (!string.IsNullOrEmpty (blrtId)) {
							url += "/" + BLRT + "/" + blrtId;
						}

						url += urlObj.Query;
						break;
					case "oclist":
					case "oclist/":
						var listId = System.Web.HttpUtility.ParseQueryString (urlObj.Query).Get ("oclist");
						url = "/" + urlObj.Authority + "/oclist/" + listId + urlObj.Query;
						break;
					}
				}


				url = url.Substring (url.IndexOf ('/', 1));
			}


			var match = Regex.Match (url, @"\/([0-9]+\.[0-9]+)\/");

			AppLinkVersion version = AppLinkVersion.Unknown;

			if (match.Success) {

				switch (match.Groups [1].Value) {
				case "1.0":
					version = AppLinkVersion.Version_1_0;
					break;
				}


			} else {
				version = AppLinkVersion.None;
			}

			var source = new AppLinkExecutor (inApp);



			var instr = BlrtParseUrl (source, version, url, subdomain);
			if (instr != null && instr.Ready) {

				//				if ((source.Cause & ExecutionSource.InApp) == ExecutionSource.InApp) {	
				//
				//					var _helper = Xamarin.Forms.DependencyService.Get<IPlatformHelper>();;
				//
				//					var alert = new ShowAlertPerformer (source, 
				//						new SimpleAlert(
				//							_helper.Translate("open_conversation_warning_title", ""),
				//							_helper.Translate("open_conversation_warning_message", ""),
				//							_helper.Translate("open_conversation_warning_cancel", ""),
				//							_helper.Translate("open_conversation_warning_accept", "")
				//						), (int arg) => {
				//						instr.RunAction ();
				//					});
				//
				//					alert.RunAction ();
				//
				//				} else {

				instr.RunAction ();
				//				}

				return true;
			}
			return false;
		}

		static ExecutionPerformer BlrtParseUrl (AppLinkExecutor source, AppLinkVersion version, string link, string subdomain = null)
		{
			var appLink = new AppLink (version, link, null, subdomain);

			if (appLink.Valid) {
				return BlrtParseUrl (source, appLink);
			}

			return null;
		}


		static ExecutionPerformer BlrtParseUrl (AppLinkExecutor source, AppLink applink)
		{
			try {
				switch (applink.type.ToLower ()) {
				case "conversation":
				case "c":
					return DeprecatedBlrtParseUrl (source, applink);
				case "make":
					return OpenMake (source, applink);
				case BlrtUtil.URLConstants.Paths.CONVERSATION:
					return OpenConversation (source, applink);
				case BlrtUtil.URLConstants.Paths.BLRT:
					if ((null == applink.parent) || (null == ParseUser.CurrentUser)) {
						return OpenIndependentBlrt (source, applink);
					} else {
						return OpenBlrt (source, applink);
					}
				case "share":
					return OpenShare ();
				case "oclist":
					return OpenOCList (source, applink);
				case "support":
					return OpenSupport ();
				case "news":
					return OpenNews ();
				case "upgrade":
					return OpenUpgrade ();
				case "archive":
					return OpenArchive ();
				}
			} catch (Exception e) {
				Console.Out.WriteLine (e.ToString ());
			}

			return null;
		}

		static ExecutionPerformer OpenNews ()
		{
			return new OpenNewsPerformer (Executor.System ());
		}

		static ExecutionPerformer OpenArchive ()
		{
			return new OpenArchivePerformer (Executor.System ());
		}

		static ExecutionPerformer OpenUpgrade ()
		{
			return new OpenUpgradeAccountPerformer (Executor.System ());
		}

		static ExecutionPerformer OpenMake (AppLinkExecutor source, AppLink applink)
		{
			var name = applink.value;
			var url = "";
			var recipients = "";
			var media = new List<string> ();
			var emails = new List<string> ();

			string meta = "";
			string requestId = "";


			if (applink.arguments.Count > 0) {

				foreach (var arg in applink.arguments) {

					var key = arg.Key;

					if (key.Contains ("["))
						key = key.Remove (key.IndexOf ('['));

					switch (key.ToLower ()) {
					case "r":
						recipients = arg.Value;
						break;
					case "url":
						url = arg.Value;
						break;
					case "m":
						media.Add (arg.Value);
						break;
					case "e":
						emails.Add (arg.Value);
						break;
					case "meta":
						meta = arg.Value;
						break;
					case "req":
						requestId = arg.Value;
						break;
					}
				}
			}

#if __ANDROID__
			return new CreateNewBlrtPerfomer (
#elif __IOS__
			return new ExternalMediaUrlHandler (
#endif
				source,
				new BlrtRequestArgs () {
					BlrtName = name,
					CaptureUrl = url,
					RecipientsId = recipients,
					Emails = emails.ToArray (),
					DownloadUrls = media.ToArray (),

					AttachedMeta	= meta,
					RequestId = requestId,
				}
			);
		}

		static ExecutionPerformer OpenIndependentBlrt (AppLinkExecutor source, AppLink applink)
		{
			var objectId = applink.value;

			string accessToken = null;
			if (!applink.arguments.TryGetValue (BlrtConstants.UrlQueryKey_AccessToken, out accessToken)) {
				accessToken = null;
			}
			if (!String.IsNullOrWhiteSpace (objectId)) {
				return new OpenIndependentBlrtPerformer (source, objectId, accessToken);
			}

			return null;
		}

		static ExecutionPerformer OpenBlrt (AppLinkExecutor source, AppLink applink)
		{
			var objectId = applink.value;

			if (!String.IsNullOrWhiteSpace (objectId)) {

				var conversationId = ConversationIdFromApplink (applink.parent);
				objectId = BlrtEncode.DecodeId (objectId.Trim ());

				string accessToken = null;
				if (!applink.arguments.TryGetValue (BlrtConstants.UrlQueryKey_AccessToken, out accessToken)) {
					accessToken = null;
				}
				return new OpenConversationPerformer (source, conversationId, accessToken){
					ContentId = objectId
				};
			}

			return null;
		}

		static ExecutionPerformer OpenOCList (AppLinkExecutor source, AppLink applink)
		{
			var ocListId = applink.value;
			if (!String.IsNullOrWhiteSpace (ocListId)) {
				const string AutoSubscribeKey = "auto_subscribe";
				var autoSubscribe = false;
				string autoSubStr;
				if (applink.arguments.TryGetValue (AutoSubscribeKey, out autoSubStr)) {
					autoSubscribe = BlrtUtil.BetterParseBoolean (autoSubStr);
				}
				return new OpenOCListPerformer (source, ocListId, autoSubscribe);
			}
			return null;
		}


		static ExecutionPerformer OpenShare()
		{
			return new OpenSharePerformer (Executor.System());
		}

		static ExecutionPerformer OpenSupport()
		{
			return new OpenSupportPerformer (Executor.System());
		}

		static ExecutionPerformer OpenConversation (AppLinkExecutor source, AppLink applink)
		{
			var objectId = ConversationIdFromApplink (applink);

			if (!string.IsNullOrWhiteSpace(objectId)) {
				if (!string.IsNullOrWhiteSpace (applink.leftovers)) {
					var instr = BlrtParseUrl(source, new AppLink(applink));
					if (instr != null)
						return instr;
				}

				string accessToken = null;
				if (!applink.arguments.TryGetValue (BlrtConstants.UrlQueryKey_AccessToken, out accessToken)) {
					accessToken = null;
				}
				var gotoConvo = new OpenConversationPerformer (source, objectId, accessToken){
				};

				foreach (var arg in applink.arguments) {
					switch (arg.Key.ToLower()) {
						case "rel":
							gotoConvo.RelationId = BlrtEncode.DecodeId (arg.Value);
							break;
						case "sendtophone":
							gotoConvo.WithSMSLink = true;
							break;
					}
				}

				return gotoConvo;
			}

			return null;
		}




		static string ConversationIdFromApplink(AppLink applink){
			return BlrtEncode.DecodeId (applink.value);
		}




		static ExecutionPerformer DeprecatedBlrtParseUrl (AppLinkExecutor source, AppLink applink)
		{
			switch (applink.type.ToLower()) {
				case "conversation":
				case "c":
					if (BlrtManager.Responder.CanOpenEvent (BlrtEventType.OpenConversation)) {
						var json = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(applink.value);
						return new OpenConversationPerformer (source, json [BlrtUtil.URLConstants.Paths.BLRT]);
					}
					return null;
			}

			return null;
		}

		static ExecutionPerformer OpenEmbededBlrt (AppLinkExecutor source, AppLink applink)
		{
			string accessToken = null;
			if (!applink.arguments.TryGetValue (BlrtConstants.UrlQueryKey_AccessToken, out accessToken)) {
				accessToken = null;
			}
			return new OpenIndependentBlrtPerformer (source, applink.value, accessToken); // TODO
		}

		class AppLinkExecutor : IExecutor
		{
#region IExecutor implementation
			public ExecutionSource Cause {
				get { return (_inApp ? ExecutionSource.InApp | ExecutionSource.AppLinks : ExecutionSource.AppLinks);}
			}
#endregion

			bool _inApp;

			public AppLinkExecutor (bool inApp)
			{
				this._inApp = inApp;
			}
		}
	}


	public class BlrtRequestArgs : IRequestArgs {

		public string BlrtName {
			get;
			set;
		}
		public string CaptureUrl {
			get;
			set;
		}
		public string RecipientsId {
			get;
			set;
		}
		public string[] Emails {
			get;
			set;
		}
		public string[] DownloadUrls {
			get;
			set;
		}
		public string AttachedMeta {
			get;
			set;
		}
		public string RequestId {
			get;
			set;
		}

		public bool MakeBlrtPublic {
			get;
			set;
		}
	}
}

