﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace BlrtData
{
	public static partial class BlrtUtil
	{
		public static class Debug
		{
			public static void ReportException
				(
					object exceptionObj, 
					IDictionary<string, string> additionalData = null, 
					Xamarin.Insights.Severity severity = Xamarin.Insights.Severity.Warning, 
					[CallerMemberName] string methodName = "", 
					[CallerFilePath] string filePath = "", 
					[CallerLineNumber] int lineNum = 0
				)
			{
				Exception fatal = null;
				try {
					Exception ex = (exceptionObj as Exception) ?? new NotImplementedException ("Reporting a non-exception object: [" + exceptionObj.GetType ().FullName + "] [" + exceptionObj.ToString () + "]");
					var data = new Dictionary<string, string> () {
						{"_Method", methodName},
						{"_File", filePath},
						{"_Line", ("" + lineNum)},
						{"_ExceptionMessage", ex.Message},
						{"_ExceptionDetail", ex.ToString ()}
					};
					if (null != additionalData) {
						foreach (var kvp in additionalData) {
							if (null != kvp.Key) {
								data[kvp.Key] = kvp.Value;
							}
						}
					}
					Xamarin.Insights.Report (
						ex, 
						data, 
						Xamarin.Insights.Severity.Warning
					);
					LLogger.WriteObject (data);
				} catch (Exception otex) {
					fatal = otex;
				}
				if (null != fatal) {
					try {
						Xamarin.Insights.Report (fatal, Xamarin.Insights.Severity.Critical);
					} catch {}
				}
			}
		}
	}
}

