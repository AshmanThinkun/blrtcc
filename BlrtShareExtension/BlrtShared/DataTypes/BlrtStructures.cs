using System;
using System.Collections.Generic;

namespace BlrtData
{
	public class DeviceDetails {
		public string AppVersion { get; private set; }
		public string Device { get; private set; }
		public string OS { get; private set; }
		public string OSVersion { get; private set; }
		public string[] LanguageArray { get; private set; }
		public string Locale { get; private set; }

		public DeviceDetails (string appVersion, string device, string os, string osVersion, string[] languageArray, string locale) {
			AppVersion = appVersion;
			Device = device;
			OS = os;
			OSVersion = osVersion;
			LanguageArray = languageArray;
			Locale = locale;
		}

		public Dictionary<string, object> ToDictionary () {
			return new Dictionary<string, object> () {
				{ "version", AppVersion },
				{ "device", Device },
				{ "os", OS },
				{ "osVersion", OSVersion },
				{ "langArray", LanguageArray },
				{ "locale", Locale }
			};
		}
	}

	// Used by the Blrt API, but can be used elsewhere too!
	public class AuthTokenInfo {
		public string ID { get; private set; }
		public string Token { get; private set; }
		public string Expiration { get; private set; }
		public AuthTarget Target { get; private set; }

		public AuthTokenInfo (string id, string token, DateTime expiration, AuthTarget target)
			: this (id, token, expiration.ToString ("O"), target) { }

		public AuthTokenInfo (string id, string token, string expiration, AuthTarget target) {
			ID = id;
			Token = token;
			Expiration = expiration;
			Target = target;
		}

		public Dictionary<string, string> ToDictionary() {
			return new Dictionary<string, string> () {
				{ "id", ID },
				{ "token", Token },
				{ "expiration", Expiration },
				{ "target", TypeFromAuthTarget (Target) }
			};
		}

		public static string TypeFromAuthTarget (AuthTarget type) {
			switch (type) {
				case AuthTarget.Facebook:
					return "facebook";
				default:
					return "unknown";
			}
		}
	}
}