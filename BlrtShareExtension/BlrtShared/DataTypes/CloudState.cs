using System;

namespace BlrtData
{

	public enum CloudState {

		NotOnCloud,

		DownloadingContent,
		UploadingContent,

		ContentMissing,
		ChildrenNotOnCloud,

		Synced,

		UploadingFailed,
	}
}

