using System;

namespace BlrtData
{


	public enum BlrtBrush : int {

		Move 			= 0,
		Pen				= 2,
		Pointer			= 1,

		ShapeLine		= 16,
		ShapeCircle		= 18,
		ShapeRectangle	= 17,

		StampSmile		= 20,
		StampFrown		= 21,
		StampTick		= 22,
		StampCross		= 23,
		StampQuestion	= 24
	}

	public enum BlrtBrushColor : int {
		Red 	= 0x00,
		White 	= 0x01,
		Black 	= 0x02,
		Blue	= 0x04,
		Yellow	= 0x08,
		Green	= 0x10
	}

	public enum BlrtBrushWidth : int {
		Thin	= 2,
		Meduim	= 5,
		Thick	= 10
	}



	[Flags]
	public enum BlrtClassState : byte {
		Blank 				= 0x00,
		HasMedia			= 0x01,
		HasBlrt				= 0x02
	}

	/*public enum BlrtMediaType : byte {
		ImagesAndTemplates	= 0,
		PDF					= 10,
	}*/

	public enum BlrtMediaFormat : int {
		Image				= 0,
		PDF					= 1,
		Audio				= 2,
		Template			= 10,
		Any					= 100,
		Invalid				= -999
	}
	public enum CreationType : byte {
		StandardBlrt		= 0,
		Request				= 10,
		SpecialCloud		= 100,
	}
	public enum BlrtStatusCode : int {
		Valid 				= 100,
		Blocked				= 400,
	}



	public enum BlrtContentType {

		Blrt				= 0,
		Request				= 1,
		BlrtReply			= 2,
		Comment				= 3,
		Media				= 8,
		Audio				= 7,
		Date				= 21,


		AddEvent			= 4,
		MessageEvent		= 5,

		SendFailureEvent	= 11,



		UnknownEvent		= -999,
	}

	[Flags]
	public enum BlrtBaseIssues{
	
		None					= 0x000,
		UnsupportedDBVersion	= 0x001,
		UnsupportedEncryption	= 0x002,
		UnknownStatusCode		= 0x004,
		UnknownMediaFormat		= 0x008,
		UnknownContentType		= 0x010,
	}

	public enum GenericError {
		NoError 			= 0,
		Unknown 			= -1,
		AlreadyDone 		= 1,
		AlreadyUsed 		= 5,
		ObjectNotFound 		= 10,
		ObjectInvalid 		= 11,
		PermissionDenied 	= 50
	}

	public enum UpdatePrimaryEmailError {
		NoError 			= 0,
		Unknown 			= -1,
		Taken 				= 5
	}

	public enum AddSecondaryEmailError {
		NoError 			= 0,
		Unknown 			= -1,
		AlreadyAdded 		= 1,
		Taken 				= 5,
		TooMany 			= 50
	}

	public enum ResendVerificationEmailError {
		NoError 			= 0,
		Unknown 			= -1,
		AlreadyVerified 	= 1,
		ContactNotFound 	= 10,
		ContactInvalid 		= 11
	}

	public enum RemoveContactError {
		NoError 			= 0,
		Unknown 			= -1,
		ContactNotFound 	= 10
	}



	// Created according to the Enums wiki document and used by the Blrt API, but can be used elsewhere too!
	public class Error {
		public const int Unknown 	= 0;
	}

	public enum AuthTarget {
		Facebook
	}

	public enum MessageEventMessageType : int {

		// message created by old version code, or create for compatibility of old versions.
		// those versions don't support fallbacks, so this type is needed
		// TODO: as we have disable all versions before 2.2.0 / A1.4.5, we can do migration and delete this type
		OldVersion = -999,

		// plain text without any format marks, it will be pre-fixed with bold author name, e.g. 
		// author = "Kevin", message = "flied away" ==>
		// *Kevin* flied away
		Action = -1,

		// note added in "Add People" screen. It will be displayed as
		// [color:gray] *Kevin* added a note
		// [color:black] *XXXXXXXXXXXXXXX*
		Note = 0,

		// formatted text; only support bold now; no further change by the app
		FormattedText = 1,

		// a message event for public blrt
		// info is included in the arguments, but not used in showing
		// app the show the message directly, so it is actually not handled in the app now
		PublicBlrt = 2,

		// conversation access request
		// the app need to render an "Allow" button and a "Deny" button
		RequestAccess = 3,

		// the result of an access request
		// the app need to update the specified RequestAccess events with the specified results
		AccessDetermined = 4,

		// someone left the convo, or was removed from the convo
		LeaveConvo = 5,

		// the name of the convo was changed
		ConvoNameChanged = 6
	}
}