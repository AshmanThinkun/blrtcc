using System;
using BlrtData.Contacts;

namespace BlrtData
{
	[Serializable]
	public class BlrtUserContactDetail
	{
		public static string ClassName = "UserContactDetail";

		public string ObjectId 		{ get; private set; }
		public string User 			{ get; set; }
		public BlrtContactInfoType Type 	{ get; set; }
		public string Value 		{ get; set; }
		public bool Verified 		{ get; set; }
		public bool Placeholder 	{ get; set; }

		public BlrtUserContactDetail (string objectId)
		{
			this.ObjectId 	= objectId;

			User = Value = "";
			Verified = Placeholder = false;
			Type = BlrtContactInfoType.Email;
		}

		public static BlrtContactInfoType TypeFromString (string type)
		{
			switch (type) {
				case "email":
					return BlrtContactInfoType.Email;
				case "facebook":
					return BlrtContactInfoType.FacebookId;
				case "phoneNumber":
					return BlrtContactInfoType.PhoneNumber;
				default:
					return BlrtContactInfoType.BlrtUserId;
			}
		}



		public bool Equals (BlrtUserContactDetail other) {
			return this == other || (
				User == other.User &&
				Type == other.Type && 
				Value == other.Value &&
				Verified == other.Verified && 
				Placeholder == other.Placeholder
			);
		}
	}
}

