#if __IOS__

namespace BlrtData
{
	public static partial class BlrtUtil
	{
		private static int _stackCounter = 0;

		public static void PushDownloadStack()
		{
			_stackCounter++;

			if(_stackCounter > 0)
				UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
		}

		public static void PopDownloadStack()
		{
			_stackCounter--;

			if (_stackCounter <= 0) {
				_stackCounter = 0;
				UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			}
		}
	}
}
#endif