using System;
using System.Collections.Generic;
using System.IO;
using Parse;

namespace BlrtData
{
	public static partial class BlrtConstants
	{
		//public static readonly string[] FacebookExtendedPermissions = new [] {"email", "user_about_me", "user_friends"};
		public static readonly string [] FacebookExtendedPermissions = new [] { "email", "public_profile", "user_friends" };

		public const string AdWordConversationBaseUrl = "https://www.googleadservices.com/pagead/conversion/";
		public const string AdWordConversationID = "949024334";
		public const string AdWordInstallLabel = "QLMcCLGA92AQzuzDxAM";
		public const string AdWordSignupLabelIOS = "fYd_COKf8GAQzuzDxAM";
		public const string AdWordSignupLabelDroid = "6Uc0CMSr-mAQzuzDxAM";

		// Change this to force the cached data to reset
		internal const string CacheToken = "3l2Xq33l8rywmDOKLAY0fB7oCXOVsAI2Xq3mGfZIv3lydfZ0Fhp88rywmDOKLAY0fB7oCXOVsmGmGAIAaZbOmiYpWmPuMpyEu02IfZIv3lydfZ0Fhp8mGAI2Xq32Xq3";

		// These must be consistent with the running version of CC - otherwise this version of the app should be disabled
		internal const string ObjectIdChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		internal const string SharedObjectIdCryptToken = "gGuskwfEOAJl0zTKvMNpYHPDab513qjdQyCe6iRXrth28oI4c7nFW9UZSBLmxV";

		public const string UserVoiceSite = "blrt.uservoice.com";
		public const int UserVoiceForumId = 245400;

		public const string UrlProtocolWithSlashes = UrlProtocol + "://";

		public const string CrashReprtKey = "453wsgJLKfdGUYioudfFGDgfGFG564hui8idsfg";

		public const string MrGesturesKey = "4MG9-GK29-K4HL-R3ZE-F9F3-UYAX-MBY9-EMME-R3BE-S2YA-TWC6-7GJC-3ESB";

		public const string HelpOverlayMailboxViewed = "HelpOverlayInboxViewed";
		public const string HelpOverlayConversationViewed = "HelpOverlayConversationViewed";
		public const string HelpOverlayConversationWithNonWelcomeBlrtViewed = "HelpOverlayConversationWithNonWelcomeBlrtViewed";
		public const string HelpOverlayEmptyConversationViewed = "HelpOverlayEmptyConversationViewed";
		public const string HelpOverlayConversationWithWelcomeBlrtViewed = "HelpOverlayConversationWithWelcomeBlrtViewed";
		public const string HelpOverlayCanvasDrawViewed = "HelpOverlayCanvasDrawViewed";
		public const string HelpOverlayCanvasNavViewed = "HelpOverlayCanvasNavViewed";
		public const string HelpOverlayCanvasStartViewed = "HelpOverlayCanvasStartViewed";
		public const string HelpOverlayCanvasFinishViewed = "HelpOverlayCanvasFinishViewed";
		public const string UrlQueryKey_AccessToken = "tk";

#if DEBUG

		internal const string OSP_ServerUrl = "https://d.blrt.co/parse/";
		internal const string OSP_AppId = "blrtDev";

		internal const string ParseAppID = "btEvYlbdW0pSlDv4Nqfqq80t0TpNXpFFs8ABG4JR";
		internal const string ParseCSharpApiKey = "aZbOm8rywmDOKLAY0fB7oCXOVsiYpWmPuMpyEu02";
		internal const string ParseAndroidiOSApiKey = "238vxKrrTzBDSJ5yDppRWIOiljfZgt91ziWGYXsC";

		public const string LogglyToken = "db2de934-f547-4c6c-9e6b-5be312a5768d";
		public const string AppPrefix = "blrtdev";

		internal const string GAKey = "UA-45564248-2";

		public const string FacebookAppName = "BlrtDev";
		public const string FacebookAppId = "1417078645178637";

		public const string UrlProtocol = "blrtdev";
		public const string TestFlightKey = "c55af2e1-9a66-48fe-9808-2be7a826835d";

		public const string ServerUrl = "https://d.blrt.co";
		public const string EmbedUrl = "http://blrt-embed-dev-thinkun.c9.io";

		public const string WebappUrl = "https://blrt-web-interface-thinkun.c9users.io";

		public const string DefaultIntroBlrtId = "VY59kENJZL";
		public const string InsightApiKey = "caae8a5fa16ee6d4d30f5d7818de6fad51b24b32";



		public const string YozioKey = "86878035-9559-401b-9451-fc9173f0a201";
		public const string YozioSecret = "579f1aca-7367-4ceb-b2bd-db95b1d7bc22";
		public const string SegmentKey = "Gac37VwihZ9G9WYphBLpEZvwlpsRoPFh";

#if __ANDROID__
		public const string GCMSenderId = "911760092001";
#endif

#elif QA
		internal const string OSP_ServerUrl = "https://q.blrt.co/parse/";
		internal const string OSP_AppId = "blrtQa";

		internal const string ParseAppID				= "QIMCw35biDACwVYwJZedvo5MQrspAVBdQlBW5OPX";
		internal const string ParseCSharpApiKey			= "zoFBq8pPnWuINanuLRXK8cC90CCtwGGPrzY2mKSd";
		internal const string ParseAndroidiOSApiKey		= "T6qe5DzytqlW25ZJUBkBJLURy0qxJE23KhE01vLr";

		public const string LogglyToken					= "db2de934-f547-4c6c-9e6b-5be312a5768d";
		public const string AppPrefix					= "blrtqa";

		internal const string GAKey						= "UA-45564248-6";

		public const string FacebookAppName				= "BlrtDev";
		public const string FacebookAppId				= "1417078645178637";

		public const string UrlProtocol		 			= "blrtqa";
		public const string TestFlightKey				= "c55af2e1-9a66-48fe-9808-2be7a826835d";

		public const string ServerUrl					= "https://q.blrt.co";
		public const string EmbedUrl 					= "http://blrt-embed-qa.herokuapp.com";

		public const string WebappUrl                     = "https://blrt-webapp-qa.herokuapp.com";

		public const string DefaultIntroBlrtId			= "k3nHt8cVbg";
		public const string InsightApiKey				= "d5f5393b0fdee0926b82523e91a65ed5cc36854b";



		public const string YozioKey = "95bc5706-aa5a-4010-998e-1728a37f3986";
		public const string YozioSecret = "3cf1da18-9076-482b-89e8-6d2ebb476497";
		public const string SegmentKey = "Gac37VwihZ9G9WYphBLpEZvwlpsRoPFh";

#if __ANDROID__
		public const string GCMSenderId = "494185331879";
#endif
#elif STAGING
		
		internal const string ParseAppID				= "xwcQLBxg0Hb5C2HOtRj4cHFbzS1QUtnWMxz55OzQ";
		internal const string ParseCSharpApiKey			= "Iuh91kWba9bUXpJzTIIlmp7JdOVASaOKwiD1Qq8F";

		public const string LogglyToken					= "db2de934-f547-4c6c-9e6b-5be312a5768d";
		public const string AppPrefix					= "blrtstaging";

		public const string GAKey						= "UA-45564248-4";

		public const string FacebookAppName				= "BlrtDev";
		public const string FacebookAppId				= "1417078645178637";

		public const string UrlProtocol		 			= "blrtstaging";
		public const string TestFlightKey				= "c55af2e1-9a66-48fe-9808-2be7a826835d";

		public const string ServerUrl					= "https://s.blrt.co";
		public const string EmbedUrl 					= "http://e.blrt.com";

		public const string WebappUrl                     = "web.blrt.com";

		public const string DefaultIntroBlrtId			= "PDbw16fogM";



		public const string YozioKey = "d92c37cf-93d7-4a7a-a395-0bda0f24bcaf";
		public const string YozioSecret = "eeb4e71c-dbfd-4714-91f3-7a23f002d7f9";
		public const string SegmentKey = "Gac37VwihZ9G9WYphBLpEZvwlpsRoPFh";

		
#elif CE || LIVE || SUPPORT
		internal const string OSP_ServerUrl 			= "https://m.blrt.co/parse/";
		internal const string OSP_AppId 				= "blrt";

		internal const string ParseAppID				= "Jcau0RObxeLbFEJi3rqSdGy2BbpktyLjOtwjbPJG";
		internal const string ParseCSharpApiKey			= "sVYzLaxNx27INBcFHklEoFri24aRQVq2Bh0OsqfS";
		internal const string ParseAndroidiOSApiKey		= "ZYmaLCjg3lMbmzKkHEVfpUYCHt9e9VKMAcelZYiv";

		public const string LogglyToken					= "693fde69-f9e4-4d71-b507-c573ce514eec";

		internal const string GAKey						= "UA-45564248-5";

		public const string FacebookAppName				= "Blrt";
		public const string FacebookAppId				= "1451170328452442";

		public const string UrlProtocol		 			= "blrt";

		public const string ServerUrl					= "https://m.blrt.co";
		public const string EmbedUrl 					= "http://e.blrt.com";

		public const string WebappUrl                     = "web.blrt.com";

		public const string DefaultIntroBlrtId			= "PDbw16fogM";
		public const string InsightApiKey				= "7d9e197f5d6a670d0772dfb481b3209cca515f18";



		public const string YozioKey = "d92c37cf-93d7-4a7a-a395-0bda0f24bcaf";
		public const string YozioSecret = "eeb4e71c-dbfd-4714-91f3-7a23f002d7f9";
		public const string SegmentKey = "2MnbEcPdqWKMa9EAOIjG01x5FUIl0SfZ";

#if __ANDROID__
		public const string GCMSenderId = "227338088772";
#endif

#if CE
		public const string AppPrefix					= "blrtce";
		public const string TestFlightKey				= "c55af2e1-9a66-48fe-9808-2be7a826835d";
#endif
		
		
#if LIVE
		public const string AppPrefix					= "blrt";
		public const string TestFlightKey				= "c81fce59-5b2b-4ddf-aca2-162ad1fda356";
#endif
		
		
#if SUPPORT
		public const string AppPrefix					= "blrtsupport";
		public const string TestFlightKey				= "c55af2e1-9a66-48fe-9808-2be7a826835d";
#endif
		
#endif

#if __ANDROID__
		public const string NonActionIntentDataKey_Type = "type";
		public const string NonActionIntentDataType_PushNotification = "push";
		public const string NonActionIntentDataKey_Data = "data";
#endif
		private const string ContainingDirectory = "blrt";

		private const string DataDirectory = "data";
		private const string ThumbnailDirectory = "thumbnail";
		private const string AvatarDirectory = "avatar";
		private const string FilesDirectory = "files";
		private const string CacheDirectory = "cached";
		private const string DecryptedDirectory = "temp";
		private const string DownloaderLocation = "downloader-cache";
		private const string RecentDirectory = "recent";

		public const string IndependentPublicPath = "public";
		public const string IndependentPrivatePath = "private";

		private static string SystemDocumentPath {
			get {
#if __IOS__
				if (UIKit.UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
					return Foundation.NSFileManager.DefaultManager.GetUrls (
						Foundation.NSSearchPathDirectory.DocumentDirectory,
						Foundation.NSSearchPathDomain.User
					) [0].Path;
				}
#endif

				return Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			}
		}

		private static string AppPath {
			get {
				return Path.Combine (SystemDocumentPath, ContainingDirectory);
			}
		}

		private static string UserPath {
			get {
				return ParseUser.CurrentUser != null ? AppPath + "/" + ParseUser.CurrentUser.ObjectId : AppPath;
			}
		}


		public static void Thesf (string d)
		{
		#if DEBUG
			foreach (var path in Directory.GetFiles (d))
				Console.WriteLine (path);
		#endif

			foreach (var dir in System.IO.Directory.GetDirectories (d)) {
				Thesf (dir);
			}
		}

		private static bool IsParseInitialized = false;

		public static void InitParse ()
		{
			// in Android, this method will be called before Forms init; don't use Forms in this method
			if (IsParseInitialized) {
				return;
			}
			//ParseClient.Initialize (ParseAppID, ParseCSharpApiKey);

			ParseClient.Initialize (new ParseClient.Configuration {
				ApplicationId = OSP_AppId,
				Server = OSP_ServerUrl
			});
			ParsePush.SubscribeAsync ("");
			IsParseInitialized = true;
		}

		public static void Setup ()
		{
			InitParse ();
			Directories.CreateAll ();

			LLogger.Initialize (BlrtData.BlrtConstants.LogglyToken);
			LLogger.AddTag (BlrtData.BlrtConstants.AppPrefix);
			AppDomain.CurrentDomain.UnhandledException += async (sender, e) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread(
					async () => {
						if (await BlrtManager.HandlePotentialInvalidSessionException (e.ExceptionObject)) {
							return;
						}
						BlrtUtil.Debug.ReportException (e.ExceptionObject, new Dictionary<string, string> () { { "message", "global unhandled exception" }, { "isTerminating", e.IsTerminating.ToString () } });
						LLogger.WriteEvent ("Global", "Exception", "Unhandled", e.ExceptionObject.ToString ());
					}
				);
			};
		}

		public static readonly string [] IndustryOptions = new string [] {
			"accounting", "advertising", "agriculture", "architecture", "consumerservices", "design", "education",
			"engineering", "fashion", "financialservices", "government", "health", "hospitality", "it", "legal",
			"manufacturing", "media", "nonprofit", "publishing", "recreation", "retail", "software",
			"telecommunications"
		};

		public static readonly string IndustriesOther = "industry_other";


		public static event EventHandler OnDeleteEverything;

		public static void DeleteEverything ()
		{
			// Can't actually delete EVERYTHING without exiting the app - it'll destroy Parse and maybe other 3PS
			// DeleteDirectory (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments));

			Directories.Delete (AppPath);
			Directories.CreateAll ();

			if (OnDeleteEverything != null)
				OnDeleteEverything (new object (), EventArgs.Empty);
		}

		public const int MinPasswordLength = 8;

		#region directories

		public static class Directories
		{
			static ILocalDirectories _userDir;
			static ILocalDirectories _publicDir;
			static ILocalDirectories _privateDir;

			public static void CreateAll ()
			{
				_userDir = new UserDirGetter ();
				_publicDir = new GlobalIndependentDirGetter (IndependentPublicPath);
				_privateDir = new GlobalIndependentDirGetter (IndependentPrivatePath);
			}

			public static void Delete (string directory)
			{
				if (Directory.Exists (directory)) {
					Directory.Delete (directory, true);
				}
			}

			public static string AppRoot { get { return SystemDocumentPath; } }

			public static ILocalDirectories Default { get { return User; } }

			public static ILocalDirectories User {
				get {
					if (null == _userDir) {
						_userDir = new UserDirGetter ();
					}
					return _userDir;
				}
			}

			public static ILocalDirectories IndependentPublic {
				get {
					if (null == _publicDir) {
						_publicDir = new GlobalIndependentDirGetter (IndependentPublicPath);
					}
					return _publicDir;
				}
			}

			public static ILocalDirectories IndependentPrivate {
				get {
					if (null == _privateDir) {
						_privateDir = new GlobalIndependentDirGetter (IndependentPrivatePath);
					}
					return _privateDir;
				}
			}


			private abstract class DirGetterBase : ILocalDirectories
			{
				public DirGetterBase ()
				{
					MakeDirectory (GetDownloaderPath (null));
				}

				public string GetDataPath (string fileName)
				{
					return InnerGetPath (DataDirectory, fileName);
				}

				public string GetFilesPath (string fileName)
				{
					return InnerGetPath (FilesDirectory, fileName);
				}

				public string GetThumbnailPath (string fileName)
				{
					return InnerGetPath (ThumbnailDirectory, fileName);
				}

				public string GetAvatarPath (string fileName)
				{
					return InnerGetPath (AvatarDirectory, fileName);
				}

				public string GetRecentPath (string fileName)
				{
					return InnerGetPath (RecentDirectory, fileName);
				}

				public string GetCachedPath (string fileName)
				{
					return InnerGetPath (CacheDirectory, fileName);
				}

				public string GetDecryptedPath (string fileName)
				{
					return InnerGetPath (DecryptedDirectory, fileName);
				}

				public string GetDownloaderPath (string fileName)
				{
					var path = Path.Combine (AppPath, DownloaderLocation);
					if (fileName != null)
						return Path.Combine (path, fileName);
					else
						return path;
				}

				protected void MakeDirectories ()
				{
					MakeDirectory (GetDataPath (null));
					MakeDirectory (GetFilesPath (null));
					MakeDirectory (GetThumbnailPath (null));
					MakeDirectory (GetCachedPath (null));
					MakeDirectory (GetDecryptedPath (null));
					MakeDirectory (GetAvatarPath (null));
					MakeDirectory (GetRecentPath (null));
				}

				protected abstract string InnerGetPath (string namedDir, string fileName);
			}

			private class UserDirGetter : DirGetterBase
			{
				public UserDirGetter () : base ()
				{
					MakeDirectories ();
				}

				protected override string InnerGetPath (string namedDir, string fileName)
				{
					var path = Path.Combine (UserPath, namedDir);
					if (fileName != null)
						return Path.Combine (path, fileName);
					else
						return path;
				}
			}

			private class GlobalIndependentDirGetter : DirGetterBase
			{
				string _subDir;

				public GlobalIndependentDirGetter (string subDir = null) : base ()
				{
					_subDir = subDir;
					MakeDirectories ();
				}

				protected override string InnerGetPath (string namedDir, string fileName)
				{
					var path = AppPath;
					if (null != _subDir) {
						path = Path.Combine (AppPath, _subDir);
					}
					path = Path.Combine (path, namedDir);
					if (fileName != null)
						return Path.Combine (path, fileName);
					else
						return path;
				}

			}

			private static void MakeDirectory (string directory)
			{
				if (!Directory.Exists (directory)) {
					Directory.CreateDirectory (directory);
				}
			}
		}

		public static class FormsMessageId
		{
			public const string BlrtPlayedOnce = "00_blrt_played_once";
		}

		#endregion
	}
}

