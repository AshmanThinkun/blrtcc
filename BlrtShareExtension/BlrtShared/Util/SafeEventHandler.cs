﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace BlrtShared
{
	/// <summary>
	/// An event handler should not throw exceptions. This helper class will help to do so.
	/// </summary>
	public static class SafeEventHandler
	{
		/// <summary>
		/// Create an event handler from an Action (i.e. void function without param).
		/// The action will be wrapped into try-catch, so the event handler will not throw any exceptions.
		/// However on debug, the app will be terminated is the Action throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the action.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler FromAction (Action handler, Type[] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return (sender, e) => {
				try {
					handler ();
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}


		/// <summary>
		/// Create an event handler from an Action (i.e. void function with default event argument).
		/// The action will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Action throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the action.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler FromAction (Action<object, EventArgs> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return (sender, e) => {
				try {
					handler (sender, e);
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}


		/// <summary>
		/// Create an event handler from an Action (i.e. void function with generic event argument).
		/// The action will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Action throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the action.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler<T> FromAction<T> (Action<object, T> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return (sender, e) => {
				try {
					handler (sender, e);
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}


		/// <summary>
		/// Create an event handler from a function that returns a Task.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler FromTaskFunc (Func<Task> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return async (sender, e) => {
				try {
					await handler ();
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}


		/// <summary>
		/// Create an event handler from a function that returns a Task and accept default EventHandler argument.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler FromTaskFunc (Func<object, EventArgs, Task> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return async (sender, e) => {
				try {
					await handler (sender, e);
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}


		/// <summary>
		/// Create an event handler from a function that returns a Task and accept generic EventHandler argument.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler<T> FromTaskFunc<T> (Func<object, T, Task> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return async (sender, e) => {
				try {
					await handler (sender, e);
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}

		/// <summary>
		/// Create an Action type a function that returns a Task and accept a generic argument.
		/// This function is for some special APIs that requires action as the handler.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The Action.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static Action<T> Actionize<T> (Func<T, Task> handler, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return async (T param) => {
				try {
					await handler (param);
				} catch (Exception ex) {
					ExceptionAssert (ex, acceptableExceptions, methodName, filePath, lineNum);
				}
			};
		}

		/// <summary>
		/// Create an event handler from a function that returns a Task.
		/// The function will not be executed if there is a event handling task still running.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="token">token to control the single execution of the event handler task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler PreventMulti (Func<Task> handler, SingleExecutionToken token = null, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return new NoParamEventHandlerAdapter (handler, token, acceptableExceptions, methodName, filePath, lineNum).Handler;
		}


		/// <summary>
		/// Create an event handler from a function that returns a Task and accept default EventHandler argument.
		/// The function will not be executed if there is a event handling task still running.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="token">token to control the single execution of the event handler task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler PreventMulti (Func<object, EventArgs, Task> handler, SingleExecutionToken token = null, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return new EventHandlerAdapter (handler, token, acceptableExceptions, methodName, filePath, lineNum).Handler;
		}


		/// <summary>
		/// Create an event handler from a function that returns a Task and accept generic EventHandler argument.
		/// The function will not be executed if there is a event handling task still running.
		/// The task execution will be wrapped into try-catch, so will not throw any exceptions.
		/// However on debug, the app will be terminated is the Task throws an excpetion not inside the acceptableExceptions
		/// </summary>
		/// <returns>The EventHandler.</returns>
		/// <param name="handler">the function that returns a Task.</param>
		/// <param name="token">token to control the single execution of the event handler task.</param>
		/// <param name="acceptableExceptions">Acceptable exceptions that can happen in the Action.</param>
		/// <param name="methodName">compiler provided Method name; leave it unspecified if you don't understand</param>
		/// <param name="filePath">compiler provided File path; leave it unspecified if you don't understand</param>
		/// <param name="lineNum">compiler provided Line number; leave it unspecified if you don't understand</param>
		public static EventHandler<T> PreventMulti<T> (Func<object, T, Task> handler, SingleExecutionToken token = null, Type [] acceptableExceptions = null, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			Assert (null != handler, nameof (handler) + "is null!", filePath, methodName, lineNum);
			return new EventHandlerAdapter<T> (handler, token, acceptableExceptions, methodName, filePath, lineNum).Handler;
		}

		static void Assert (bool isTrue, string msg, string methodName, string filePath, int lineNum)
		{
			if (!isTrue) {
				BlrtData.BlrtUtil.Debug.ReportException (
					new NotImplementedException ("event handler assert"),
					new Dictionary<string, string> {
						{ "error", "event handler assert failed" },
						{ "message", msg ?? "null"}
					},
					Xamarin.Insights.Severity.Warning,
					methodName,
					filePath,
					lineNum
				);
			}
			System.Diagnostics.Debug.Assert (isTrue, msg + DebugInfo (methodName, filePath, lineNum));
		}


		/// <summary>
		/// create a new SingleExecutionToken
		/// </summary>
		/// <returns>The new SingleExecutionToken.</returns>
		public static SingleExecutionToken NewToken ()
		{
			return new SingleExecutionToken ();
		}

		static string DebugInfo (string methodName, string filePath, int lineNum)
		{
			return string.Format ("\n[{0}]\n[{1}]\n[{2}]\n", filePath, methodName, lineNum);
		}

		static void ExceptionAssert(Exception ex, Type [] acceptableExceptions, string methodName, string filePath, int lineNum)
		{
			try {
				if (null != acceptableExceptions) {
					foreach (var type in acceptableExceptions) {
						if (ex.GetType ().IsSubclassOf (type)) {
							return;
						}
					}
				}
			} catch {}
			Assert (false, "Event handler exception happens!!!\n" + ex + "\n", filePath, methodName, lineNum);
		}

		private class EventHandlerAdapter
		{
			Func<object, EventArgs, Task> _handler;
			SingleExecutionToken _token;
			string _methodName, _filePath;
			int _lineNum;
			Type [] _acceptableExceptions;

			public EventHandlerAdapter (Func<object, EventArgs, Task> handler, SingleExecutionToken token, Type [] acceptableExceptions, string methodName, string filePath, int lineNum)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken ();
				}
				_token = token;
				_methodName = methodName;
				_filePath = filePath;
				_lineNum = lineNum;
				_acceptableExceptions = acceptableExceptions;
			}

			public async void Handler (object sender, EventArgs e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						try {
							await _handler (sender, e);
						} catch (Exception ex) {
							ExceptionAssert (ex, _acceptableExceptions, _methodName, _filePath, _lineNum);
						}
					}
#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
#endif
				}
			}
		}

		private class NoParamEventHandlerAdapter
		{
			Func<Task> _handler;
			SingleExecutionToken _token;
			string _methodName, _filePath;
			int _lineNum;
			Type [] _acceptableExceptions;

			public NoParamEventHandlerAdapter (Func<Task> handler, SingleExecutionToken token, Type [] acceptableExceptions, string methodName, string filePath, int lineNum)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken ();
				}
				_token = token;
				_methodName = methodName;
				_filePath = filePath;
				_lineNum = lineNum;
				_acceptableExceptions = acceptableExceptions;
			}

			public async void Handler (object sender, EventArgs e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						try {
							await _handler ();
						} catch (Exception ex) {
							ExceptionAssert (ex, _acceptableExceptions, _methodName, _filePath, _lineNum);
						}
					}
#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
#endif
				}
			}
		}

		private class EventHandlerAdapter<T>
		{
			Func<object, T, Task> _handler;
			SingleExecutionToken _token;
			string _methodName, _filePath;
			int _lineNum;
			Type [] _acceptableExceptions;

			internal EventHandlerAdapter (Func<object, T, Task> handler, SingleExecutionToken token, Type [] acceptableExceptions, string methodName, string filePath, int lineNum)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken ();
				}
				_token = token;
				_methodName = methodName;
				_filePath = filePath;
				_lineNum = lineNum;
				_acceptableExceptions = acceptableExceptions;
			}

			public async void Handler (object sender, T e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						try {
							await _handler (sender, e);
						} catch (Exception ex) {
							ExceptionAssert (ex, _acceptableExceptions, _methodName, _filePath, _lineNum);
						}
					}
#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler<T> MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
#endif
				}
			}
		}


		public class SingleExecutionToken
		{
			bool _locked = false;
			object _flagLock = new object ();

			public SingleExecutionToken () { }

			public ILock Lock ()
			{
				lock (_flagLock) {
					if (!_locked) {
						_locked = true;
						return new Locker (this);
					}
				}
				return null;
			}

			public interface ILock : IDisposable
			{
				bool Release ();
			}

			private class Locker : ILock
			{
				SingleExecutionToken _ins;
				bool _released;
				public Locker (SingleExecutionToken instance)
				{
					_ins = instance;
					_released = false;
				}

				public bool Release ()
				{
					return InnerRelease ();
				}

				#region IDisposable implementation

				public void Dispose ()
				{
					InnerRelease ();
				}

				#endregion

				private bool InnerRelease ()
				{
					if (!_released) {
						_released = true;
						lock (_ins._flagLock) {
							_ins._locked = false;
						}
						return true;
					}
					return false;
				}
			}
		}

	}
}
