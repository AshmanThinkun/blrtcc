#if __ANDROID__
using System;
using System.Net;
using Android.Net;
using Android.Content;
using Java.Net;


namespace BlrtData
{

public enum NetworkStatus {
	NotReachable,
	ReachableViaCarrierDataNetwork,
	ReachableViaWiFiNetwork
}

public static class Reachability {
	public static string HostName = "www.google.com";


	// 
	// Raised every time there is an interesting reachable event, 
	// we do not even pass the info as to what changed, and 
	// we lump all three status we probe into one
	//
	public static event EventHandler ReachabilityChanged;

	static void OnChange (object sender, EventArgs args){
		OnChange ();
	}

	static void OnChange ()
	{
		var h = ReachabilityChanged;
		if (h != null)
			h (null, EventArgs.Empty);
	}

	static Context				_context;
	static BroadcastReceiver	_broadcastReceiver;

	static void Create(){
		if(_broadcastReceiver == null){
			_broadcastReceiver = new ReachabilityBroadcastReceiver ();
		}
	}



	public static NetworkStatus InternetConnectionStatus ()
	{
		Create ();

		if(_context == null)
			return NetworkStatus.ReachableViaWiFiNetwork;

		var connectivityManager = (ConnectivityManager)_context.GetSystemService(Context.ConnectivityService);

		var mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Ethernet).GetState();
		if (mobileState == NetworkInfo.State.Connected)
			return NetworkStatus.ReachableViaWiFiNetwork;

		mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi).GetState();
		if (mobileState == NetworkInfo.State.Connected)
			return NetworkStatus.ReachableViaWiFiNetwork;

		mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Mobile).GetState();
		if (mobileState == NetworkInfo.State.Connected)
			return NetworkStatus.ReachableViaCarrierDataNetwork;

		return NetworkStatus.NotReachable;
	}

	public static NetworkStatus LocalWifiConnectionStatus ()
	{
		Create ();

		if(_context == null)
			return NetworkStatus.ReachableViaWiFiNetwork;


		var connectivityManager = (ConnectivityManager)_context.GetSystemService(Context.ConnectivityService);

		var mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Ethernet).GetState();
		if (mobileState == NetworkInfo.State.Connected)
			return NetworkStatus.ReachableViaWiFiNetwork;

		mobileState = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi).GetState();
		if (mobileState == NetworkInfo.State.Connected)
			return NetworkStatus.ReachableViaWiFiNetwork;

		return NetworkStatus.NotReachable;
	}


	private class ReachabilityBroadcastReceiver : BroadcastReceiver {
		public override void OnReceive (Context context, Intent intent)
		{
			_context = context.ApplicationContext;
			OnChange ();
		}


	}
}
}
#endif