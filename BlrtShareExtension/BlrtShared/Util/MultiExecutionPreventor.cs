using System;
using System.Threading.Tasks;

namespace BlrtData
{
	public static class MultiExecutionPreventor
	{
		public delegate Task EventHandlingTask(object sender, EventArgs e);
		public delegate Task TaskFunc ();
		public delegate Task EventHandlingTask<T> (object sender, T e);

		public static EventHandler NewEventHandler (EventHandlingTask handler, SingleExecutionToken token = null)
		{
			if (null == handler) {
				throw new ArgumentNullException ("handler");
			}
			return new EventHandlerAdapter (handler, token).Handler;
		}

		public static EventHandler NewEventHandler (TaskFunc handler, SingleExecutionToken token = null)
		{
			if (null == handler) {
				throw new ArgumentNullException ("handler");
			}
			return new NoParamEventHandlerAdapter (handler, token).Handler;
		}

		public static EventHandler<T> NewEventHandler<T> (EventHandlingTask<T> handler, SingleExecutionToken token = null)
		{
			if (null == handler) {
				throw new ArgumentNullException ("handler");
			}
			return new EventHandlerAdapter<T> (handler, token).Handler;
		}

		public static Action NewAction (TaskFunc func, SingleExecutionToken token = null)
		{
			if (null == func) {
				throw new ArgumentNullException ("func");
			}
			return new ActionAdapter (func, token).Action;
		}

		// execute the function, with the guard of the token
		public async static Task GuardedInvoke (TaskFunc func, SingleExecutionToken token)
		{
			if (null == func) {
				return;
			}
			if (token == null) {
				await func ();
				return;
			}
			using (var locker = token.Lock ()) {
				if (null != locker) {
					await func ();
				}
				#if DEBUG
				else {
					Console.WriteLine ("===[DEBUG]===GuardedInvoke MULTIPLE EXECUTION BLOCKED!!!!!=====");
				}
				#endif
			}
		}

		private class EventHandlerAdapter
		{
			EventHandlingTask _handler;
			SingleExecutionToken _token;

			public EventHandlerAdapter (EventHandlingTask handler, SingleExecutionToken token = null)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken();
				}
				_token = token;
			}

			public async void Handler (object sender, EventArgs e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						await _handler (sender, e);
					}
					#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
					#endif
				}
			}
		}

		private class NoParamEventHandlerAdapter
		{
			TaskFunc _handler;
			SingleExecutionToken _token;

			public NoParamEventHandlerAdapter (TaskFunc handler, SingleExecutionToken token = null)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken();
				}
				_token = token;
			}

			public async void Handler (object sender, EventArgs e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						await _handler ();
					}
					#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
					#endif
				}
			}
		}

		private class EventHandlerAdapter<T>
		{
			EventHandlingTask<T> _handler;
			SingleExecutionToken _token;

			internal EventHandlerAdapter (EventHandlingTask<T> handler, SingleExecutionToken token = null)
			{
				_handler = handler;
				if (null == token) {
					token = NewToken();
				}
				_token = token;
			}

			public async void Handler (object sender, T e)
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _handler) {
						await _handler (sender, e);
					}
					#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===EventHandler<T> MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
					#endif
				}
			}
		}


		private class ActionAdapter
		{
			TaskFunc _func;
			SingleExecutionToken _token;
			public Action Action { get; private set; }
			public ActionAdapter(TaskFunc func, SingleExecutionToken token = null)
			{
				_func = func;
				if (null == token) {
					token = NewToken();
				}
				_token = token;
				Action = new Action(DoAction);
			}

			private async void DoAction()
			{
				using (var locker = _token.Lock ()) {
					if (null != locker && null != _func) {
						await _func ();
					}
					#if DEBUG
					else {
						Console.WriteLine ("===[DEBUG]===Action MULTIPLE EXECUTION BLOCKED!!!!!=====");
					}
					#endif
				}
			}
		}
			
		// token
		public static SingleExecutionToken NewToken()
		{
			return new SingleExecutionToken ();
		}
			
		public class SingleExecutionToken {
			bool _locked = false;
			object _flagLock = new object();

			public SingleExecutionToken() {}

			public ILock Lock ()
			{
				lock (_flagLock) {
					if (!_locked) {
						_locked = true;
						return new Locker (this);
					}
				}
				return null;
			}

			public interface ILock : IDisposable
			{
				bool Release ();
			}

			private class Locker : ILock
			{
				SingleExecutionToken _ins;
				bool _released;
				public Locker (SingleExecutionToken instance)
				{
					_ins = instance;
					_released = false;
				}

				public bool Release ()
				{
					return InnerRelease ();
				}

				#region IDisposable implementation

				public void Dispose ()
				{
					InnerRelease ();
				}

				#endregion

				private bool InnerRelease ()
				{
					if (!_released) {
						_released = true;
						lock (_ins._flagLock) {
							_ins._locked = false;
						}
						return true;
					}
					return false;
				}
			}
		}

	}
}

