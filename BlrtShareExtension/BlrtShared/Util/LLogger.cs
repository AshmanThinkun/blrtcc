using System;       
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Globalization;

/// <summary>
/// Parse and send string, json and event logs to Loggly.
/// </summary>
 public static class LLogger
{
	#region Constants

	#if DEBUG
	private const bool LogInConsole = true;
	#else
	private const bool LogInConsole = false;
	#endif

	private const long MAX_LOG_SIZE = 1 * 1024 * 1024;						// 1MB
	private const long SAFE_REQUEST_SIZE = 5 * 1024 * 1024 - MAX_LOG_SIZE;	// 5MB - 1MB
	private const int BYTE_PER_CHAR = sizeof(char);							// used to calculate log size
	private const string LOGGLY_URL = "https://logs-01.loggly.com";
	private const string JSON_CONTENT_TYPE = "application/json";
	private const string NULL_USER_ID_STRING = "(nil)";						// the value of "user_id" field in log when the UserId is not set
	private const string LOG_FIELD_NAME_STRING = "msg";						// the field name of the string message in log
	private const string LOG_FIELD_NAME_JSON = "obj";						// the field name of the json object message in log
	private const string LOG_FIELD_NAME_EVENT = "event";					// the field name of the json object message in log
	private const double CHECKING_PERIOD_SECOND = 1;

	private const string USER_ID_FIELD = "user_id";
	private const string EVENT_CATEGORY_FIELD = "category";
	private const string EVENT_ACTION_FIELD = "action";
	private const string EVENT_LABEL_FIELD = "label";
	private const string EVENT_VALUE_FIELD = "value";
	#endregion

	#region Fields
	private static string _key = null;
	private static double _flushPeriod;
	private static int _maxLineBeforePush;
	private static List<string> _tags = null;
	private static string _userId = null;
	private static string _appVersion = null;
	private static string _deviceModel = null;
	private static DateTime _nextFlushTime;
	private static ConcurrentQueue<string> _buffer;
	private static object _sendingTaskLock;
	private static object _waitingTaskLock;
	private static Task _waitingTask = null;
	private static Task _sendingTask = null;
	#endregion

	#region Configuration

	/// <summary>
	/// Initialize this <see cref="LLogger"/>
	/// </summary>
	/// <param name="key">Key (Loggly Customer Token).</param>
	public static void Initialize (string key)
	{
		if (Initialized)
		{
			if (_key != key)
			{
				throw new MultiInitilizationException ();
			}
			return;
		}

		_key = key;

		_buffer = new ConcurrentQueue<string> ();
		_tags = new List<string> ();

		_sendingTaskLock = new object ();
		_waitingTaskLock = new object ();

		_flushPeriod = 30;
		_maxLineBeforePush = 512;
	}

	/// <summary>
	/// Gets or sets the flush period.
	/// If the logs can not be sent to Loggly in one request, the rest logs will be sent in a new request after this period
	/// </summary>
	/// <value>The flush period.</value>
	public static double FlushPeriod
	{ 
		get
		{
			return _flushPeriod;
		}

		set
		{
			_flushPeriod = value;
		}
	}

	/// <summary>
	/// Gets or sets the max line number before push.
	/// </summary>
	/// <value>The max line number before push.</value>
	public static int MaxLineBeforePush
	{ 
		get
		{
			return _maxLineBeforePush;
		}

		set
		{
			_maxLineBeforePush = value;
		}
	}

	/// <summary>
	/// Gets or sets the user id.
	/// </summary>
	/// <value>The user id.</value>
	public static string UserId 
	{
		get
		{
			return _userId;
		}

		set
		{
			_userId = value;
		}
	}

	/// <summary>
	/// Gets or sets the app version.
	/// log tags will include app version if it is set.
	/// </summary>
	/// <value>The app version.</value>
	public static string AppVersion 
	{
		get
		{
			return _appVersion;
		}

		set
		{
			_appVersion = value;
		}
	}

	/// <summary>
	/// Gets or sets the type of the device.
	/// log tags will include device type if it is set.
	/// </summary>
	/// <value>The type of the device.</value>
	public static string DeviceModel 
	{
		get
		{
			return _deviceModel;
		}

		set
		{
			_deviceModel = value;
		}
	}

	/// <summary>
	/// Gets the key (Loggly Custom Token).
	/// </summary>
	/// <value>The key.</value>
	public static string Key
	{ 
		get
		{
			return _key;
		}
	}

	/// <summary>
	/// Gets a value indicating whether this <see cref="LLogger"/> is initialized.
	/// </summary>
	/// <value><c>true</c> if initialized; otherwise, <c>false</c>.</value>
	public static bool Initialized
	{ 
		get
		{
			return (!string.IsNullOrWhiteSpace (_key));
		}
	}

	#region Tag Logic

	public static string[] Tags{
		get{
			return _tags.ToArray ();
		}
	}

	public static void AddTag(string tag){
		if (!_tags.Contains (tag) && Regex.IsMatch(tag, @"^[\w][\w/._-]*$"))
			_tags.Add (tag);



		//
	}
	public static void RemoveTag(string tag){
		if (_tags.Contains (tag))
			_tags.Remove (tag);
	}




	#endregion



	#endregion

	#region Logging Interfaces
	static bool IsLocalErrorReported = false; // we send a lot of loggly logs, but we don't want to send too many Xamarin Insight reports, so we use this to control we only send 1 locale exception report per run.
	/// <summary>
	/// Writes a line of string log, and send it to Loggly in the format of
	/// {"timestamp":XXX, "user_id":UserId, "message":<paramref name="message"/>}
	/// </summary>
	/// <param name="message">the string log</param>
	public static void WriteLine (string message)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif

		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}
		constructJsonLogFromString (timestamp, message);
	}

	/// <summary>
	/// Writes a line of string log, which is returned by processing a composite format string, and send it to Loggly in the format of
	/// {"timestamp":XXX, "user_id":UserId, "message":message}
	/// </summary>
	/// <param name="format">A composite format string.</param>
	/// <param name="args">An array of objects to format.</param>
	public static void WriteLine (string format, params object[] args)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif

		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}
		StringBuilder sb = new StringBuilder ();
		sb.AppendFormat (ParseString(format), args);
		constructJsonLogFromString (timestamp, sb.ToString ());
	}

	/// <summary>
	/// Parses the dictionary to a json string, and send it as a log to Loggly in the format of
	/// {"timestamp":XXX, "user_id":UserId, "message":{<paramref name="json"/>}}
	/// </summary>
	/// <param name="json">the dictionary representing a set of json field-value pairs</param>
	public static void WriteObject (IDictionary<string, object> json)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif

		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}
		if ((null != json) && (json.Count > 0)) 
		{
			StringBuilder sb = new StringBuilder ();
			sb.Append ('{');
			foreach (var kvp in json) 
			{
				sb.AppendFormat ("\"{0}\":\"{1}\",", ParseString(kvp.Key), ParseString(kvp.Value.ToString ()));
			}
			sb.Replace (',', '}', sb.Length - 1, 1);
			constructJsonLogFromJson (timestamp, sb.ToString ());
		}
	}

	/// <summary>
	/// Parses the <paramref name="jsonObj"/> to a json string, and send it as a log to Loggly in the format of
	/// {"timestamp":XXX, "user_id":UserId, "message":{<paramref name="jsonObj"/>}}
	/// </summary>
	/// <param name="jsonObj">object to be parse and send</param>
	public static void WriteObject (Object jsonObj)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif

		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}
		if (null != jsonObj) 
		{
			var jsonStr = JsonConvert.SerializeObject (jsonObj);
			if (jsonStr.Length > 2)
			{
				constructJsonLogFromJson (timestamp, jsonStr);
			}
		}
	}

	/// <summary>
	/// Puts <paramref name="args"/> into json field-value pairs, where even arguments are the field names, and odd are the values; 
	/// then the json will be sent to Loggly as a log in the format of
	/// {"timestamp":XXX, "user_id":UserId, "message":{json}}.
	/// if the length of <paramref name="args"/> is not even, the last one will be ignored.
	/// </summary>
	/// <param name="args">Arguments to make json string</param>
	public static void WriteObject (params object[] args)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif

		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}
		if ((null != args) && (args.Length > 1)) 
		{
			StringBuilder sb = new StringBuilder ();
			sb.Append ('{');
			for(int i = 0; i < (args.Length / 2); ++i)  
			{
				sb.AppendFormat ("\"{0}\":\"{1}\",", ParseString(args[i * 2 + 0].ToString()), ParseString(args[i * 2 + 1].ToString()));
			}
			sb.Replace (',', '}', sb.Length - 1, 1);
			constructJsonLogFromJson (timestamp, sb.ToString ());
		}
	}

	/// <summary>
	/// Writes a event as a log, and send it to Loggly in the format of
	/// {"timestamp":XXX, "user_id":UserId, "event":{"category":<paramref name="category"/>,"action":<paramref name="action"/>,"label":<paramref name="label"/>,"value":<paramref name="value"/>}}.
	/// </summary>
	/// <param name="category">Category.</param>
	/// <param name="action">Action.</param>
	/// <param name="label">Label.</param>
	/// <param name="value">Value.</param>
	public static void WriteEvent (string category, string action, string label = null, object value = null)
	{
		#if !DEBUG
		if (!Initialized) return;
		#endif
		string timestamp = null;
		try {
			timestamp = DateTime.UtcNow.ToString ("o", CultureInfo.InvariantCulture);
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			if (!IsLocalErrorReported) {
				Xamarin.Insights.Report (ex, "Reporter", "LLogger::WriteEvent::DataTime_To_String", Xamarin.Insights.Severity.Warning);
				IsLocalErrorReported = true;
			}
			return;
		}

		if ((null != category) && (category.Length > 0) && (null != action) && (action.Length > 0)) 
		{
			StringBuilder sb = new StringBuilder ();
			sb.Append ('{');

			sb.AppendFormat ("\"{0}\":\"{1}\",", EVENT_CATEGORY_FIELD, ParseString(category));
			sb.AppendFormat ("\"{0}\":\"{1}\",", EVENT_ACTION_FIELD, ParseString (action));

			if (!string.IsNullOrEmpty(label))
			{
				sb.AppendFormat ("\"{0}\":\"{1}\",", EVENT_LABEL_FIELD, ParseString(label));
			}
			if (value != null)
			{
				sb.AppendFormat ("\"{0}\":\"{1}\",", EVENT_VALUE_FIELD, ParseString(value.ToString()));
			}

			sb.Replace (',', '}', sb.Length - 1, 1);
			constructJsonLogFromJson (timestamp, sb.ToString (), LOG_FIELD_NAME_EVENT);
		}
	}

	private static string ParseString(string str){
		return str.Replace ("\"", "\\\"");
	}

	#region Unimplemented Interfaces
	/*
	// {timestamp:XXX, user_id:UserId, message:messages[0]} \n {timestamp:XXX, user_id:UserId, message:messages[1]} \n ...
	public static void WriteLines (string[] messages)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:messages[0]} \n {timestamp:XXX, user_id:UserId, message:messages[1]} \n ...
	public static void WriteLines (string[] formats, params object[] args)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:message, json:{json}}
	public static void WriteJson (IDictionary<string, object> json, string format, params object[] args)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:message, json:{jsonObj}}
	public static void WriteJson (Object jsonObj, string format, params object[] args)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:messages[0], json:{jsons[0]}} \n {timestamp:XXX, user_id:UserId, message:messages[1], json:{jsons[1]}} \n ...
	public static void WriteJsons (IDictionary<string, object>[] jsons, string[] messages)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:messages[0], json:{jsonObjs[0]}} \n {timestamp:XXX, user_id:UserId, message:messages[1], json:{jsonObjs[1]}} \n ...
	public static void WriteJsons (Object[] jsonObjs, string[] messages)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:messages[0], json:{jsons[0]}} \n {timestamp:XXX, user_id:UserId, message:messages[1], json:{jsons[1]}} \n ...
	public static void WriteJsons (IDictionary<string, object>[] jsons, string[] formats, params object[] args)
	{

	}

	// {timestamp:XXX, user_id:UserId, message:messages[0], json:{jsonObjs[0]}} \n {timestamp:XXX, user_id:UserId, message:messages[1], json:{jsonObjs[1]}} \n ...
	public static void WriteJsons (Object[] jsonObjs, string[] formats, params object[] args)
	{

	}
	*/
	#endregion Unimplemented Interfaces
	#endregion Logging Interfaces

	#region Flushing Interfaces

	/// <summary>
	/// start the flush (send) task, but do NOT wait for it
	/// </summary>
	/// <returns>A task that represents the asynchronous flush (send) operation.</returns>
	public static Task FlushNow ()
	{
		return FlushNowAsync ();
	}

	/// <summary>
	/// Asynchronously flush (send) all buffered logs to Loggly
	/// </summary>
	/// <returns>A task that represents the asynchronous flush (send) operation.</returns>
	public static async Task FlushNowAsync()
	{
		if (!Initialized) return;

		if (!_buffer.IsEmpty)
		{
			await createSendingTaskIfNotExist ();
		}
	}

	#endregion

	#region Sending Logic

	/// <summary>
	/// Constructs the json-style log from string.
	/// </summary>
	/// <param name="timestamp">Timestamp of the log.</param>
	/// <param name="strMsg">content of the log.</param>
	private static void constructJsonLogFromString(string timestamp, string strMsg) 
	{
		if (string.IsNullOrEmpty (strMsg))
		{
			return;
		}
		StringBuilder sb = new StringBuilder ();
		sb.AppendFormat ("{{\"timestamp\":\"{0}\",\"{1}\":\"{2}\",\"{3}\":\"{4}\"}}", timestamp, USER_ID_FIELD, _userId ?? NULL_USER_ID_STRING, LOG_FIELD_NAME_STRING, strMsg);
		sb.Replace ('\n'.ToString(), "\\n");
		sb.Replace ('\r'.ToString(), "\\r");
		sendLog (sb.ToString ());
	}

	/// <summary>
	/// Constructs the json-style log from a json string.
	/// </summary>
	/// <param name="timestamp">Timestamp of the log.</param>
	/// <param name="jsonString">the log json string.</param>
	/// <param name="jsonFieldName">the field name of the log value in the final sent json-style log.</param>
	private static void constructJsonLogFromJson(string timestamp, string jsonString, string jsonFieldName = LOG_FIELD_NAME_JSON) 
	{
		StringBuilder sb = new StringBuilder ();
		if (string.IsNullOrWhiteSpace (jsonFieldName))
		{
			jsonFieldName = LOG_FIELD_NAME_JSON;
		}
		sb.AppendFormat ("{{\"timestamp\":\"{0}\",\"{1}\":\"{2}\",\"{3}\":{4}}}", timestamp, USER_ID_FIELD, _userId ?? NULL_USER_ID_STRING, jsonFieldName, jsonString);
		sb.Replace ('\n'.ToString(), "\\n");
		sb.Replace ('\r'.ToString(), "\\r");
		sendLog (sb.ToString ());
	}

	/// <summary>
	/// put the log into the sending buffer.
	/// print to console if in DEBUG
	/// </summary>
	/// <param name="log">Log.</param>
	private static void sendLog(string log) 
	{
		if(LogInConsole)
			Console.WriteLine (string.Format ("LLogger: {0}", log));


		#if DEBUG
		if(!Initialized) return;		
		#endif

		if ((log.Length + 1) * BYTE_PER_CHAR >= MAX_LOG_SIZE) // each log should be smaller than 1MB
		{
			throw new LogIsTooLongException ();
		}
		_buffer.Enqueue (log);
		runWaitingTask ();
	}

	/// <summary>
	/// The method that the sending task excutes
	/// </summary>
	/// <returns>The sending task</returns>
	private static async Task sendingTask()
	{
		try {
			if (Initialized && (!_buffer.IsEmpty)) 
			{
				StringBuilder sbUrl = new StringBuilder (LOGGLY_URL);
				sbUrl.AppendFormat ("/bulk/{0}/", _key);	// https://logs-01.loggly.com/bulk/<Customer_Token>/ 
				// add tags to the bulk
				addTags (sbUrl);							// https://logs-01.loggly.com/bulk/<Customer_Token>/tag/<appVerion>,<DeviceType>,t1,t2,t3.../
				var request = HttpWebRequest.Create (sbUrl.ToString ());
				request.Method = "POST";
				request.Timeout = 10000;
				request.ContentType = JSON_CONTENT_TYPE;

				int lineCount = 0;
				using (Stream requestData = await request.GetRequestStreamAsync ()) 
				{
					using (var writer = new StreamWriter (requestData)) 
					{
						string outString = null;
						long totalRequestSize = 0;
						while ((totalRequestSize <= SAFE_REQUEST_SIZE) && _buffer.TryDequeue (out outString)) // stop looping when the queue is empty, or when the bulk size is larger than 4MB
						{
							totalRequestSize += (outString.Length + 1) * BYTE_PER_CHAR;
							await writer.WriteLineAsync (outString);

							lineCount++;
						}
						// Asynchronously flush the stream to request body 
						await writer.FlushAsync ();
					}
				}
				if(LogInConsole) Console.Out.WriteLine("LLogger Console: Sending log, {0} lines", lineCount);

				using (HttpWebResponse response = await request.GetResponseAsync () as HttpWebResponse)
				{
					if (LogInConsole) {
						// print response info if debugging
						if (response.StatusCode != HttpStatusCode.OK) {
							Console.Out.WriteLine ("LLogger Console: Error!! Loggly returned status code: {0}", response.StatusCode);
						}
						using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
							var content = reader.ReadToEnd ();
							if (string.IsNullOrWhiteSpace (content)) {
								Console.Out.WriteLine ("LLogger Console: Loggly Response contained empty body...");
							} else {
								Console.Out.WriteLine ("LLogger Console: Loggly Response Body: {0}", content);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			// we are not using BlrtUtil.Debug.ReportException () in this file because that function might also call functions in this class, which might make a infinite loop
			Xamarin.Insights.Report (ex, "Reporter", "LLogger", Xamarin.Insights.Severity.Warning);
		}
		_nextFlushTime = DateTime.Now + TimeSpan.FromSeconds (_flushPeriod);
	}

	/// <summary>
	/// The method that the waiting task excutes
	/// </summary>
	/// <returns>the waiting task.</returns>
	private static Task runWaitingTask()
	{

		lock(_waitingTaskLock){
			if (_waitingTask != null) {
				return _waitingTask;
			}

			_waitingTask = Task.WhenAll(waitingTaskAction ());
			_waitingTask.ContinueWith ((Task arg) => {
				lock(_waitingTaskLock){
					_waitingTask = null;
				}
			});
		}
		return _waitingTask;
	}


	private static async Task waitingTaskAction()
	{
		_nextFlushTime = DateTime.Now + TimeSpan.FromSeconds (_flushPeriod);
		while (true)
		{
			await Task.Delay ((int)(CHECKING_PERIOD_SECOND * 1000));
			if (isSendingTaskRunning || (DateTime.Now >= _nextFlushTime) || _maxLineBeforePush < _buffer.Count)
			{
				if (!_buffer.IsEmpty) {
					await createSendingTaskIfNotExist ();
				} else {
					return; // stop waiting task when all the logs have been sent
				}
			}
		}
	}

	/// <summary>
	/// Creates the sending task if it is not running.
	/// </summary>
	/// <returns>The sending task.</returns>
	private static Task createSendingTaskIfNotExist()
	{
		lock (_sendingTaskLock) {
			if (null == _sendingTask){
				_sendingTask = sendingTask ();
				_sendingTask.ContinueWith ((Task arg) => {
					lock (_sendingTaskLock) 
						_sendingTask = null;
				});
			}
		}
		return _sendingTask;
	}

	/// <summary>
	/// Gets a value indicating whether the sending task is running.
	/// </summary>
	/// <value><c>true</c> if sending task is running; otherwise, <c>false</c>.</value>
	private static bool isSendingTaskRunning
	{
		get
		{
			lock (_sendingTaskLock) 
			{
				return (null != _sendingTask);
			}
		}
	}

	/// <summary>
	/// Adds tags to Loggly REST API url.
	/// </summary>
	/// <param name="sbUrl">The <seealso cref="StringBuilder"/> that contains the current Loggly REST API URL</param>
	private static void addTags(StringBuilder sbUrl)
	{
		bool addTags = false; // prevent tag array full of empty/null tags
		StringBuilder sbTags = new StringBuilder ("tag/");
		if (!string.IsNullOrWhiteSpace (_appVersion)) {
			addTags = true;
			var tmp = Regex.Replace (_appVersion, "[^\\w/._-]+", "_");
			sbTags.AppendFormat ("AppVerison_{0},", tmp.Trim('_'));
		}
		if (!string.IsNullOrWhiteSpace (_deviceModel)) {
			addTags = true;
			var tmp = Regex.Replace (_deviceModel, "[^\\w/._-]+", "_");
			sbTags.AppendFormat ("Device_{0},", tmp.Trim('_'));
		}
		if ((null != _tags) && (_tags.Count > 0)) 
		{
			for (int i = 0; i < _tags.Count; i++) 
			{
				if (!string.IsNullOrWhiteSpace (_tags[i])) 
				{
					addTags = true;
					sbTags.AppendFormat("{0},", _tags[i]);
				}
			}
		}
		if (addTags) {
			sbTags.Replace (',', '/', sbTags.Length - 1, 1); // replace the last ',' with '/'
			sbUrl.Append (sbTags);
		}
	}

	#endregion

	#region LogglyLogger Exceptions

	/// <summary>
	/// Represents the error that occurs when the user initializes <see cref="LLogger"/> with for more than once with different keys
	/// </summary>
	public class MultiInitilizationException : InvalidOperationException
	{
		public MultiInitilizationException () : base ("Can not initialize LogglyLogger more than once") {}
		public MultiInitilizationException (string message) : base (message) {}
	}

	/// <summary>
	/// Represents the error that occurs when the user logs without initializing <see cref="LLogger"/>
	/// </summary>
	public class NullOrEmptyLogglyKeyException : InvalidOperationException
	{
		public NullOrEmptyLogglyKeyException () : base ("Loggly key is null! Need Initialization!") {}
		public NullOrEmptyLogglyKeyException (string message) : base (message) {}
	}

	/// <summary>
	/// Represents the error that occurs when the user write a log that is bigger than 1MB
	/// </summary>
	public class LogIsTooLongException : InvalidOperationException
	{
		public LogIsTooLongException () : base ("Log is longer than 1MB.") {}
		public LogIsTooLongException (string message) : base (message) {}
	}

	#endregion
}
