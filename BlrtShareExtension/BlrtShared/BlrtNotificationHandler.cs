using System;
using System.Collections.Generic;

using BlrtData.Notification;
using Newtonsoft.Json;

namespace BlrtData
{
	public static class BlrtNotificationHandler
	{


		public static bool Valid(BlrtNotificationOption option)
		{
			switch (option.type) {
				case "url":
					return true;
				case "accountupgrademodal":
					return true;
			}

			return false;
		}


		public static BlrtNotificationOption GetValid(BlrtNotificationOption option)
		{
			while (option != null) {
				if (Valid (option))
					return option;

				option = option.fallback;
			}

			return null;
		}

		public static void ProformAction(BlrtNotificationOption option){
		
			if (!Valid (option))
				return;

			switch (option.type) {
				case "url":
					if (ProformUrl (option))
						return;
					break;
				case "accountupgrademodal":
					if (ProformAccountUpgradeModal (option))
						return;
					break;
			}


			if (option.fallback != null)
				ProformAction (option.fallback);

		}

		static bool ProformUrl (BlrtNotificationOption option)
		{
			try{
				var dic = JsonConvert.DeserializeObject<Dictionary<string, object>> (option.arg);

				if (dic.ContainsKey ("url")) {
					BlrtManager.Responder.OpenWebUrl (dic["url"].ToString(), false);
					return true;
				}

				//return UIApplication.SharedApplication.OpenUrl (NSUrl.FromString (opt.args));
			} catch{
			}
			return false;
		}

		public class UpgradesAction{
			public int[] thresholds;
		}

		static bool ProformAccountUpgradeModal (BlrtNotificationOption option)
		{
			var upgrade = JsonConvert.DeserializeObject<UpgradesAction> (option.arg);

			BlrtPermissions.BlrtAccountThreshold threshold = BlrtPermissions.BlrtAccountThreshold.Unknown;

			foreach (int j in upgrade.thresholds) {
				if (Enum.IsDefined (typeof(BlrtPermissions.BlrtAccountThreshold), j)) {
					threshold = (BlrtPermissions.BlrtAccountThreshold)j;
					break;
				}
			}

			return BlrtManager.Responder.ShowUpgradeModal (threshold);

			/*

			BlrtAccountUpgradeModalViewController modalController = null;

			switch(threshold) {
				case BlrtPermissions.BlrtAccountThreshold.Trial:
					modalController = new BlrtFreeTrialModalViewController ();

					break;
				case BlrtPermissions.BlrtAccountThreshold.Premium:
					modalController = new BlrtPremiumModalViewController ();

					break;
			}


			if (modalController != null) {
				var _modalController = new UINavigationController (modalController);
				_modalController.ModalPresentationStyle = modalController.ModalPresentationStyle;


				modalController.OnDismiss += (s1, a1) => {
					DismissViewController (true, null);
				};


				PresentViewController (_modalController, true, null);
			}
			*/
		}
	}
}

