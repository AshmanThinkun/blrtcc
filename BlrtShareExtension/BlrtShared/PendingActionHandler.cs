﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlrtData
{
	public static class PendingActionHandler
	{
		/// <summary>
		/// Constants used to handles actions related to the pending actions
		/// </summary>
		public static class Constants
		{
			/// <summary>
			/// For an action to be considered a pendingAction, its type value must be within the following range:
			/// 	MIN_PENDING_ACTION_TYPE_VALUE =< pending_action_type_value <= MAX_PENDING_ACTION_TYPE_VALUE
			/// </summary>
			public static readonly int MIN_PENDING_ACTION_TYPE_VALUE = 100;
			public static readonly int MAX_PENDING_ACTION_TYPE_VALUE = 999;

			public static readonly string PENDING_ACTION_KEY = "pid";
			public static readonly string PENDING_ACTION_TYPE_KEY = "t";
			public static readonly string USER_ID_KEY = "user";
			public static readonly string ARGUMENTS_KEY = "args";

			public static readonly string PENDING_ACTIONS_RESULTS_KEY = "results";
			public static readonly string APP_VERSION_KEY = "version";

			public static readonly string SERVER_ENDPOINT_FOR_PENDING_ACTION_RESULTS = "pendingActionsHandled";
			public static readonly string SERVER_ENDPOINT_TO_GET_PENDING_ACTIONS = "getPendingActions";
		}

		/// <summary>
		/// Pending action types.
		/// </summary>
		enum PendingActionType
		{
			UserDeleted = 101,
			ForceLogout = 102,
			Unknown = -1
		}

		/// <summary>
		/// Determines whether the type is one of the types reserved for pending actions
		/// </summary>
		/// <returns><c>true</c>, if pending action type was ised, <c>false</c> otherwise.</returns>
		/// <param name="type">Type.</param>
		public static bool IsPendingActionType (int type)
		{
			return type >= Constants.MIN_PENDING_ACTION_TYPE_VALUE
				&& type <= Constants.MAX_PENDING_ACTION_TYPE_VALUE;
		}
		/// <summary>
		/// Handles the pending actions.
		/// </summary>
		/// <param name="pendingActions">Pending actions.</param>
		internal static void HandlePendingActions (IDictionary<string, object> [] pendingActions)
		{
			var results = new Dictionary<string, bool> (pendingActions.Length);

			foreach (var pendingAction in pendingActions) {

				var pendingActionId = pendingAction [Constants.PENDING_ACTION_KEY].ToString ();
				PendingActionType pendingActionType = GetPendingActionType (pendingAction);

				var handledSuccessfully = false;
				switch (pendingActionType) {
				case PendingActionType.UserDeleted:
					handledSuccessfully = OnUserDeleted (pendingAction);
					break;
				case PendingActionType.ForceLogout:
					handledSuccessfully = OnForceLogoutRequested (pendingAction).Result;
					break;
				case PendingActionType.Unknown:
					break;
				}

				results.Add (pendingActionId, handledSuccessfully);
			}

			OnPendingActionsHandled (results);
		}

		/// <summary>
		/// Triggered when the force logout is requested.
		/// </summary>
		/// <returns><c>true</c>, if force logout requested was oned, <c>false</c> otherwise.</returns>
		/// <param name="pendingAction">Pending action.</param>
		static async Task<bool> OnForceLogoutRequested (IDictionary<string, object> pendingAction)
 		{
			try {
				PushHandler.ForceUserLogout (ExecutionPipeline.Executor.System (), BlrtUser.DELETED_ACCOUNT_STATUS_STRING, BlrtPushState.InApp);
				return true;
			} catch {
				return false;
			}
 		}


		/// <summary>
		/// Called when the pending actions have been handled.
		/// </summary>
		/// <param name="results">Results.</param>
		static void OnPendingActionsHandled (Dictionary<string, bool> results)
		{
			var responseDic = new Dictionary<string, object> (2); // version and results
			responseDic.Add (Constants.APP_VERSION_KEY, BlrtUtil.PlatformHelper.GetAppVersion ());
			responseDic.Add (Constants.PENDING_ACTIONS_RESULTS_KEY, results);

			//var responseJSON = Newtonsoft.Json.JsonConvert.SerializeObject (responseDic);
			SendPendingActionsResultsToServer (responseDic);
			Console.WriteLine ("===SendPendingActionsResultsToServer");
		}

		/// <summary>
		/// Sends the pending actions results to server.
		/// </summary>
		/// <param name="responseDic">Response dic.</param>
		static void SendPendingActionsResultsToServer (Dictionary<string, object> responseDic)
		{
			BlrtUtil.BetterParseCallFunctionAsync<object> (
				Constants.SERVER_ENDPOINT_FOR_PENDING_ACTION_RESULTS,
				responseDic,
				new System.Threading.CancellationToken ()
			).FireAndForget ();
		}

		/// <summary>
		/// Gets the type of the pending action.
		/// </summary>
		/// <returns>The pending action type.</returns>
		/// <param name="pendingAction">Pending action.</param>
		static PendingActionType GetPendingActionType (IDictionary<string, object> pendingAction)
		{
			var pendingActionType = Convert.ToInt32 (pendingAction [Constants.PENDING_ACTION_TYPE_KEY]);
			if (Enum.IsDefined (typeof (PendingActionType), pendingActionType)) {
				return (PendingActionType)pendingActionType;
			}

			return PendingActionType.Unknown;
		}

		/// <summary>
		/// Called when a user is deleted.
		/// </summary>
		/// <returns><c>true</c>, if user deleted was oned, <c>false</c> otherwise.</returns>
		/// <param name="pendingAction">Pending action.</param>
		static bool OnUserDeleted (IDictionary<string, object> pendingAction)
		{
			var args = pendingAction [Constants.ARGUMENTS_KEY] as Dictionary<string, object>;

			var userId = args [Constants.USER_ID_KEY].ToString ();

			if (!string.IsNullOrWhiteSpace (userId)) {
				var user = BlrtManager.DataManagers.Default.GetUser (userId);
				BlrtUtil.UpdateUserAccountStatus (user, BlrtUser.DELETED_ACCOUNT_STATUS_STRING).ContinueWith (task => {

					var relations = BlrtManager.DataManagers.Default.GetConversationUserRelations (userId);
					if (relations != null) {
						foreach (var rel in relations) {
							BlrtUtil.UpdateConversationReadonlyStatus (rel.Conversation, rel.Conversation.CreatorId == userId).ContinueWith (t => {
								// Update UI
								Xamarin.Forms.MessagingCenter.Send<object, string> (rel, string.Format ("{0}.ConverationUserDeleted", rel.Conversation.LocalGuid), userId);
							});
						}
					}
				});
				return true;
			}

			return false;
		}

		/// <summary>
		/// Called when the user is forced to logout.
		/// </summary>
		/// <returns><c>true</c>, if forced logout was oned, <c>false</c> otherwise.</returns>
		static bool OnForcedLogout ()
		{
			Xamarin.Forms.Device.BeginInvokeOnMainThread (
				() => {
                    Console.WriteLine ("===OnForcedLogout start");
					PushHandler.ForceUserLogout (ExecutionPipeline.Executor.System (), BlrtUserHelper.AccountStatus, BlrtPushState.InApp);
					Console.WriteLine ("===OnForcedLogout end");
				}
			);
			return true;
		}

		/// <summary>
		/// Handles the pending actions. It makes a request to the server to provide pending actions that need 
		/// to be handled.
		/// </summary>
		internal static async Task HandlePendingActions ()
		{
			var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
				Constants.SERVER_ENDPOINT_TO_GET_PENDING_ACTIONS,
				null,
				new System.Threading.CancellationToken ()
			);

			if (result != null) {
				var pendingActions = (result as List<object>).Select (r => r as Dictionary<string, object>).ToArray ();
				HandlePendingActions (pendingActions);
			}
		}
	}
}