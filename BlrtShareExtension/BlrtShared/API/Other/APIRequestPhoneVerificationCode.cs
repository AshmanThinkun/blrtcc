﻿using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIRequestPhoneVerificationCodeError : APIError {
		public const int detailInUse = 410;
	}

	public class APIRequestPhoneVerificationCode: APIBase<APIResponse>
	{
		public APIRequestPhoneVerificationCode (Parameters parameters)
			: base (1, "RequestPhoneVerificationCode", parameters) { }

		public class Parameters : IAPIParameters {
			public string phoneNumber;
			public string countryCode;

			public Parameters () {
				phoneNumber = null;
				countryCode = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "phoneNumber", phoneNumber },
					{ "countryCode", countryCode }
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (phoneNumber) && !string.IsNullOrWhiteSpace (countryCode);
			}
		}
	}
}

