﻿using Newtonsoft.Json;
using BlrtData;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIUrlInfoError : APIError {
		public const int invalidUrl = 620;
		public const int authenticationFailure = 621;
		public const int playCounterLimit = 622;
		public const int notBlrt = 624;
		public const int readOnly = 625;
	}

	public class APIUrlInfoResponse : APICreationResponse {
		[JsonProperty("data")]
		public ResponseData Data{ get;private set;}

		private APIUrlInfoResponse () { }

		public class ResponseData{

			[JsonProperty("type")]
			public string Type{ get; private set;}
			[JsonProperty("accessible")]
			public bool Accessible{ get; private set;}
			[JsonProperty("title")]
			public string Title{ get; private set;}
			[JsonProperty("creator")]
			public string Creator{ get; private set;}
			[JsonProperty("duration")]
			public int Duration{ get; private set;}
			[JsonProperty("thumbnail")]
			public string Thumbnail{ get; private set;}
			[JsonProperty("pages")]
			public int Pages{ get; private set;}
			[JsonProperty("isPublicBlrt")]
			public bool IsPublicBlrt{ get; private set;}
			[JsonProperty("conversationId")]
			public string ConversationId{ get; private set;}
			[JsonProperty("blrtId")]
			public string BlrtId{ get; private set;}


			[JsonProperty("playData")]
			public PlayDataCl PlayData{get;private set;}

			public class PlayDataCl{
				[JsonProperty("touchDataFile")]
				public string TouchDataFile{ get; private set;}
				[JsonProperty("audioFile")]
				public string AudioFile{ get; private set;}
				[JsonProperty("argument")]
				public string Argument{ get; private set;}
				[JsonProperty("encryptValue")]
				public string EncryptValue{ get; private set;}
				[JsonProperty("encryptType")]
				public int EncryptType{ get; private set;}
				[JsonProperty("media")]
				public MediaData[] Media{ get; private set;}
			}

			public class MediaData{
				[JsonProperty("id")]
				public string Id{ get; private set;}
				[JsonProperty("mediaFile")]
				public string MediaFile{ get; private set;}
				[JsonProperty("mediaFormat")]
				public BlrtMediaFormat MediaFormat{ get; private set;}
				[JsonProperty("encryptValue")]
				public string EncryptValue{ get; private set;}
				[JsonProperty("encryptType")]
				public int EncryptType{ get; private set;}
				[JsonProperty("pageArgs")]
				public string PageArgs{ get; private set;}
			}
		}
	}

	public class APIUrlInfo : APIBase<APIUrlInfoResponse>
	{
		public APIUrlInfo (Parameters parameters)
			: base (1, "urlInfo", parameters) { }

		public class Parameters : IAPIParameters {
			public string url;
			public bool includeMedia;

			public Parameters () {
				url = null;
				includeMedia = false;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "url", url },
					{ "includeMedia", includeMedia }
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (url);
			}
		}
	}
}