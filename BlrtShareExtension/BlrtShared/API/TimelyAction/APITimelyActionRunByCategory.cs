using System.Collections.Generic;
using Parse;

namespace BlrtAPI
{
	// Codes for success
	public class APITimelyActionRunByCategoryStatus
	{
	}

	public class APITimelyActionRunByCategoryError : APIError
	{
	}

	public class APITimelyActionRunByCategory : APIBase<APIResponse>
	{
		public APITimelyActionRunByCategory (Parameters parameters)
			: base (1, "timelyActionRunByCategory", parameters) { }

		public class Parameters : IAPIParameters
		{

			public string Category { get; set; }

			public Parameters ()
			{
				Category = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "category", Category },
					{ "userId", ParseUser.CurrentUser.ObjectId }
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return ParseUser.CurrentUser != null && !string.IsNullOrWhiteSpace (Category);
			}
		}
	}
}

