﻿using Parse;
using System.Collections.Generic;
using BlrtData;
using System.Linq;

namespace BlrtAPI
{
	public class APIConversationUserRelationDeleteError : APIError
	{
		public const int invalidConversationId = 416;
		public const int mutipleRelations = 417;
		public const int publicBlrtsExist = 418;
	}

	public class APIConversationUserRelationDeleteResponse : APICreationResponse
	{
		private APIConversationUserRelationDeleteResponse () { }
	}

	public class APIConversationUserRelationDelete : APIBase<APIConversationDeleteResponse>
	{
		public APIConversationUserRelationDelete (Parameters parameters)
			: base (1, "conversationUserRelationDelete", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string[] RelationIds;

			public Parameters ()
			{
				RelationIds = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;
				
				var result = new Dictionary<string, object> () {
					{ "relationIds", RelationIds },
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return (RelationIds.Length != 0) && ParseUser.CurrentUser != null;
			}
		}
	}
}

