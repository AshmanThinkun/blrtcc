﻿using Parse;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationDuplicateFromBlrtError : APIError
	{
	}

	public class APIConversationDuplicateFromBlrtResponse : APICreationResponse
	{
		[JsonProperty ("convoIds")]
		public string [] ConversationIds { get; set; }
	}

	public class APIConversationDuplicateFromBlrt : APIBase<APIConversationDuplicateFromBlrtResponse>
	{
		public APIConversationDuplicateFromBlrt (Parameters parameters)
			: base (1, "conversationDuplicateFromBlrt", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationItemId;
			public string ConversationName;

			public Parameters ()
			{
				ConversationItemId = null;
				ConversationName = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationItemId", ConversationItemId },
					{ "names", new string[]{ConversationName}}
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationItemId) && ParseUser.CurrentUser != null
					&& !string.IsNullOrWhiteSpace (ConversationName)
					&& ConversationName.Length > 0
					&& ConversationName.Length < 56;
			}
		}
	}
}

