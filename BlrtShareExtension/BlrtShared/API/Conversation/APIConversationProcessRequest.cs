﻿using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationProcessRequestError : APIConversationUserRelationCreateError
	{
		public const int conversationIdNotFound = 611;
		public const int eventIdNotFound = 612;
		public const int alreadyTookAction = 613;
	}

	public class APIConversationProcessRequestResponse : APICreationResponse
	{
		private APIConversationProcessRequestResponse () { }
	}

	public class APIConversationProcessRequest : APIBase<APIConversationProcessRequestResponse>
	{
		public APIConversationProcessRequest (Parameters parameters)
			: base (1, "conversationProcessRequest", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationId;
			public string EventId;
			public bool Accept;

			public Parameters ()
			{
				ConversationId = null;
				EventId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationId", ConversationId },
					{ "eventId", EventId },
					{ "accept", Accept }
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationId) && !string.IsNullOrWhiteSpace (EventId) && ParseUser.CurrentUser != null;
			}
		}
	}
}