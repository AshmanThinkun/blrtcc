using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationPublicError : APIError
	{
		public const int conversationIdNotFound = 611;
		public const int notCreator = 612;
		public const int publicChildExists = 613;
	}

	public class APIConversationPublicResponse : APICreationResponse
	{
		private APIConversationPublicResponse () { }
	}

	public class APIConversationPublic : APIBase<APIConversationPublicResponse>
	{
		public APIConversationPublic (Parameters parameters)
			: base (1, "conversationPublic", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationId;
			public bool AllowPublicBlrt;

			public Parameters ()
			{
				ConversationId = null;
				AllowPublicBlrt = false;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationId", ConversationId },
					{ "allowPublicBlrt", AllowPublicBlrt },
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationId) && ParseUser.CurrentUser != null;
			}
		}
	}
}