using Parse;
using System.Collections.Generic;
using BlrtData;

namespace BlrtAPI
{
	public class APIConversationDeleteError : APIError
	{
		public const int invalidConversationId = 416;
		public const int mutipleRelations = 417;
		public const int publicBlrtsExist = 418;
	}

	public class APIConversationDeleteResponse : APICreationResponse
	{
		APIConversationDeleteResponse () { }
	}

	public class APIConversationDelete : APIBase<APIConversationDeleteResponse>
	{
		public APIConversationDelete (Parameters parameters)
			: base (1, "conversationDelete", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationId;

			public Parameters ()
			{
				ConversationId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationId", BlrtEncode.EncodeId (ConversationId) }
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationId) && ParseUser.CurrentUser != null;
			}
		}
	}
}

