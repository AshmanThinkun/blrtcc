﻿using Parse;
using System.Collections.Generic;
using BlrtData;

namespace BlrtAPI
{
	public class APIConversationRenameError : APIError
	{
	}

	public class APIConversationRenameResponse : APICreationResponse
	{
		APIConversationRenameResponse () { }
	}

	public enum APIConversationRenameRequestStatus
	{
		Success,
		Failed,
		Cancelled
	}

	public class APIConversationRename : APIBase<APIConversationRenameResponse>
	{
		public APIConversationRename (Parameters parameters)
					: base (1, "conversationRename", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationId;
			public string ConversationName;

			public Parameters ()
			{
				ConversationId = null;
				ConversationName = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "name" , ConversationName}, 
					{"convoId", BlrtEncode.EncodeId (ConversationId) }
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationId) && ParseUser.CurrentUser != null
					&& !string.IsNullOrWhiteSpace (ConversationName)
					&& ConversationName.Length > 0
					&& ConversationName.Length < 56;
			}
		}
	}
}

