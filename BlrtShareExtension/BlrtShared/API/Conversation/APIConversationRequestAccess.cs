﻿using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationRequestAccessError : APIError
	{
		public const int conversationIdNotFound = 611;
		public const int userInBlackList = 612;
		public const int conversationFull = 613;
	}

	public class APIConversationRequestAccessResponse : APICreationResponse
	{
		private APIConversationRequestAccessResponse () { }
	}

	public class APIConversationRequestAccess : APIBase<APIConversationRequestAccessResponse>
	{
		public const int RelationExist = 555;
		public const int RequestSent = 556;

		public APIConversationRequestAccess (Parameters parameters)
			: base (1, "conversationRequestAccess", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string ConversationId;

			public Parameters ()
			{
				ConversationId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationId", ConversationId }
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (ConversationId) && ParseUser.CurrentUser != null;
			}
		}
	}
}