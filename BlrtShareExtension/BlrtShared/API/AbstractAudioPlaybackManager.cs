﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlrtShared
{
	public abstract class AbstractAudioPlaybackManager 
	{ 
		public static string CurrentContentId { get; set; }
		public static object LastAudioCell { get; set;}
		public static string MaxDurationOfLastAudio { get; set;}


#if __ANDROID__
		public static bool IsProcessingRequest { get; protected set; }
		public static object UINotificationObserver { get; set; }
#endif

		public static bool IsAudioPlaying {
			get;
			set;
		}

		protected BlrtCanvas.IBCAudioPlayer AudioPlayer;

		public abstract Task Play (string contentId, string audioFilePath);
		public abstract void Pause ();
		public abstract void Seek (float time);
		public abstract float CurrentTime ();


#if __ANDROID__
		public abstract Task StartNotificationLoop ();
#endif

		public event EventHandler AudioFinishedCallback {
			add { AudioPlayer.Finished += value; }
			remove { AudioPlayer.Finished -= value; }
		}
	}
}
