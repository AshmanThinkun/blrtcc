﻿using System.Collections.Generic;
using Parse;

namespace BlrtAPI
{
	/// <summary>
	/// API user change account status.
	/// client side should only support account deletion
	/// </summary>
	public class APIUserChangeAccountStatus : APIBase<APIResponse>
	{
		public APIUserChangeAccountStatus (Parameters parameters)
			: base (1, "userChangeAccountStatus", parameters) { }

		public class Parameters : IAPIParameters {

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "from", "app" },
					{ "accountStatus", BlrtData.BlrtUser.DELETED_ACCOUNT_STATUS_STRING}
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return ParseUser.CurrentUser != null;
			}
		}
	}
}