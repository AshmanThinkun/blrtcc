using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIUserBlockError : APIError
	{
		public const int blockUserNotExist = 420;
		public const int requestUserRequired = 421;
		public const int blockUserEmailNotExist = 422;
	}

	public class APIUserBlockResponse : APICreationResponse
	{
		private APIUserBlockResponse () { }
	}

	public class APIUserBlock : APIBase<APIUserBlockResponse>
	{
		public APIUserBlock (Parameters parameters)
			: base (1, "userBlock", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string BlockUserId;
			public bool UnBlock;
			public string BlockUserEmail;
			public Parameters ()
			{
				BlockUserId = null;
				UnBlock = false;
				BlockUserEmail = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "blockUserId", BlockUserId },
					{ "blockFlag", UnBlock ? 2 : 1},
					{ "blockUserEmail", BlockUserEmail},
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				if (BlockUserId != null)
					return !(string.IsNullOrWhiteSpace (BlockUserId) && ParseUser.CurrentUser != null);
				else if (BlockUserEmail != null)
					return !(string.IsNullOrWhiteSpace (BlockUserEmail) && ParseUser.CurrentUser != null);
				else
					return false;
			}
		}
	}
}