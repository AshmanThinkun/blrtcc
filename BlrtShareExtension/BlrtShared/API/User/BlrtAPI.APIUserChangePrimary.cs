using System.Collections.Generic;

namespace BlrtAPI
{
	// Codes for success
	public class APIUserChangePrimaryStatus {
		public const int PrimaryChanged 					= 100;
		public const int PrimaryChangedNoVerificationEmail 	= 101;
		public const int SwitchedWithSecondary   			= 102;
		public const int AlreadyUsingTargetEmail 			= 110;
	}

	public class APIUserChangePrimaryError : APIError {
		public const int EmailInUse 				= 410;
	}

	public class APIUserChangePrimary : APIBase<APIResponse>
	{
		public APIUserChangePrimary (Parameters parameters)
			: base (1, "userChangePrimary", parameters) { }

		public class Parameters : IAPIParameters {
			public string Email;

			public Parameters () {
				Email = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "email", Email }
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return Email != null && BlrtData.BlrtUtil.IsValidEmail (Email);
			}
		}
	}
}

