using System;
using System.Collections.Generic;

namespace BlrtAPI
{
	// Codes for success
	public class APIUserRegisterStatus {
		public const int AccountCreated 		= 100;
		public const int TokenAccountExists 	= 110;
	}

	public class APIUserRegisterError : APIError {
		public const int EmailInUse 			= 410;
	}

	public class APIUserRegister : APIBase<APIResponse>
	{
		public APIUserRegister (Parameters parameters)
			: base (1, "userRegister", parameters) { }

		public class Parameters : IAPIParameters {
			public string Name;
			public string Email;
			public string Password;
			public BlrtData.AuthTokenInfo Token;
			public IDictionary<string, object> Extra;
			public bool IsQuickSignUp;

			public Parameters () {
				Name = null;
				Email = null;
				Password = null;
				Token = null;
				Extra = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "name", Name },
					{ "email", Email }
				};

				if (Extra != null)
					result ["extra"] = Extra;

				if (Token != null)
					result ["token"] = Token.ToDictionary ();
				if (Password != null)
					result ["password"] = Password;
				result ["isQuickSignUp"] = IsQuickSignUp;

				return result;
			}

			bool IAPIParameters.IsValid () {
				return (String.IsNullOrWhiteSpace (Email)|| BlrtData.BlrtUtil.IsValidEmail (Email)) &&
					(
						Password != null ||
						Token != null
					);
			}
		}
	}
}

