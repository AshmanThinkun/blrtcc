using System.Collections.Generic;

namespace BlrtAPI
{
	// Codes for success
	public class APIUserGetTipBannersStatus {
	}

	public class APIUserGetTipBannersError : APIError {
	}

	public class APIUserGetTipBannersResponse : APIResponse{
		public BlrtData.TipBanner[] Data { get; set; }
	}


	public class APIUserGetTipBanners : APIBase<APIUserGetTipBannersResponse>
	{
		public APIUserGetTipBanners (Parameters parameters)
			: base (1, "userGetTipBanners", parameters) { }

		public class Parameters : IAPIParameters {


			public Parameters () {
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> ();

				return result;
			}

			bool IAPIParameters.IsValid () {
				return true;
			}
		}
	}
}

