﻿using System;
using System.IO;
using System.Threading.Tasks;
using BlrtData.Models.ConversationScreen;

namespace BlrtShared
{
	public abstract class AbstractAudioRecordingManager
	{
		public static string LocalAudioPath { get; set; }

		protected BlrtCanvas.IBCAudioRecorder AudioRecorder;

		public abstract void Record ();

		public async Task<bool> RequestPermission()
		{
			return await AudioRecorder.RequestPermissions ();
		}

		public float CurrentVolume {
			get {
				try {
					var vol = AudioRecorder.CurrentVolume;
					return vol;
				}catch {
					return 0;
				}
			}
		}

		public float Duration { 
			get {
				return AudioRecorder.Duration;
			}
		}

		public async Task<bool> OnAudioRecordingEnded ()
		{
			if (AudioRecorder.IsRecording) {
				float recordingDuration = 0.0f;
				try {
					Console.WriteLine ("=========================Stop requested========================");
					recordingDuration = await AudioRecorder.Stop ();
				} catch(Exception e){
					Console.WriteLine ("Stop audio exception: " + e);
				}
				// Discard recordings of length less than 1 second
				if (recordingDuration < 1.0f) {
					DeleteRecordedAudio ();
					return false;
				}

				return true;
			}
			return false;
		}

		void DeleteAudioFile()
		{
			if (!string.IsNullOrWhiteSpace (LocalAudioPath)) {
				try {
					File.Delete (LocalAudioPath);
				} catch { }

				LocalAudioPath = string.Empty;
			}
		}

		internal void DeleteRecordedAudio()
		{
			DeleteAudioFile ();
			Reset ();
		}

		protected void Reset ()
		{
			try {
				AudioRecorder.Reset ();
				GC.Collect ();
			} catch {
				DeleteAudioFile ();
			} finally {
				LocalAudioPath = string.Empty;

			}
		}
	}
}
