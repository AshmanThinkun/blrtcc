using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using BlrtData;

namespace BlrtAPI
{
	public abstract class APIBase<T> : IBlrtAPI
	{
		int _version;
		string _endpoint;
		IAPIParameters _parameters;

		public T Response { get; private set; }
		public Exception Exception { get; private set; }

		protected APIBase (int version, string endpoint, IAPIParameters parameters)
		{
			_version = version;
			_endpoint = endpoint;

			_parameters = parameters;
		}

		public async Task<bool> Run (CancellationToken cancellationToken) {
			if (!_parameters.IsValid ()) {
				Exception = new InvalidOperationException ("Provided parameters are not valid");
				return false;
			}

			var parameters = new Dictionary<string, object> () {
				{ "api", new Dictionary<string, object> () {
					{ "version", _version },
					{ "endpoint", _endpoint }
				} },
				{ "device", BlrtData.BlrtManager.Responder.DeviceDetails.ToDictionary () },
				{ "data", _parameters.ToDictionary () }
			};

			try {
				var jsonResult = await BlrtUtil.BetterParseCallFunctionAsync<string> (_endpoint, parameters, cancellationToken);
				Response = JsonConvert.DeserializeObject<T> (jsonResult);
				return true;
			} catch (Exception e) {
				Exception = e;
				return false;
			}
		}
	}

	public interface IAPIParameters {
		Dictionary<string, object> ToDictionary ();
		bool IsValid ();
	}

	public class APIError : BlrtData.Error {
		public const int InvalidVersion 			= 300;
		public const int AuthenticationRequired 	= 301;

		public const int InvalidParameters 			= 400;

		public const int SubqueryError 				= 500;
	}

	public class APIResponse {
		[JsonProperty("success")]
		public bool Success { get; private set; }

		[JsonProperty("status")]
		public APIResponseStatus Status { get; private set; }

		[JsonProperty("error")]
		public APIResponseStatus Error { get; private set; }

		protected APIResponse () { }
	}

	public class APICreationResponse : APIResponse {
		[JsonProperty("objects")]
		public IEnumerable<APIResponseObject> Objects { get; private set; }

		protected APICreationResponse () { }
	}

	public class APIResponseObject {
		[JsonProperty("type")]
		public string ClassName { get; set; }

		[JsonProperty("id")]
		public string ObjectId { get; set; }

		[JsonProperty("localGuid")]
		public string LocalGuid { get; set; }

		public APIResponseObject () { }
	}

	public class APIResponseStatus {
		[JsonProperty("code")]
		public int Code { get; private set; }

		[JsonProperty("message")]
		public string Message { get; private set; }

		private APIResponseStatus () { }
	}
}

