using System.Collections.Generic;
using BlrtData;

namespace BlrtAPI
{
	public class APIUserContactDetailCreateStatus {
		public const int DetailCreated 						= 100;
		public const int DetailCreatedNoVerificationEmail 	= 101;
		public const int AlreadyAdded 	 					= 110;
	}

	public class APIUserContactDetailCreateError : APIError {
		public const int DetailInUse 						= 410;
		public const int TooManyContacts 	 				= 450;
	}

	public class APIUserContactDetailCreate : APIBase<APIResponse>
	{
		public APIUserContactDetailCreate (Parameters parameters)
			: base (1, "userContactDetailCreate", parameters) { }

		public class Parameters : IAPIParameters {
			public AuthTokenInfo Token;
			public string Email;
			public string PhoneNumber;
			public IDictionary<string, object> Extra;

			public Parameters () {
				Token = null;
				Email = null;
				Extra = null;
				PhoneNumber = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> ();
				if (Email != null)
					result ["email"] = Email;
				if (Token != null)
					result ["token"] = Token.ToDictionary ();
				if (PhoneNumber != null)
					result ["phoneNumber"] = PhoneNumber;
				if (Extra != null)
					result ["extra"] = Extra;

				return result;
			}

			bool IAPIParameters.IsValid () {
				if(!(Token!=null || PhoneNumber!=null || Email!=null)){
					return false;
				}
				if(Email!=null && !BlrtUtil.IsValidEmail(Email)){
					return false;
				}
				return true;
			}
		}
	}
}

