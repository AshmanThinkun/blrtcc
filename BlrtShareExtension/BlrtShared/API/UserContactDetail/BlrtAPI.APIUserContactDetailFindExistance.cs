using System.Collections.Generic;
using BlrtData;
using Newtonsoft.Json;


namespace BlrtAPI
{
	public class APIFindExistanceResponse : APICreationResponse {
		[JsonProperty("existedEmails")]
		public string[] Emails{ get;private set;}
		private APIFindExistanceResponse () { }
	}


	public class APIUserContactDetailFindExistance : APIBase<APIFindExistanceResponse>
	{
		public APIUserContactDetailFindExistance (Parameters parameters)
			: base (1, "userContactDetailFindExistance", parameters) { }

		public class Parameters : IAPIParameters {

			//public IDictionary<string, object> Extra;
			public string[] Emails;
			public Parameters () {
				Emails = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "emails", Emails }
				};			

				return result;
			}

			bool IAPIParameters.IsValid () {
				if(Emails == null)
					return false;
				foreach(string email in Emails){
					if (!(email==null||BlrtUtil.IsValidEmail (email)))
						return false;
				}
				return true;
			}
		}
	}
}

