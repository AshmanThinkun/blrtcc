using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIUserContactDetailDeleteError : APIError {
		public const int RecordNotFound 		= 404;
		public const int DetailIsPrimary 		= 410;
	}

	public class APIUserContactDetailDelete : APIBase<APIResponse>
	{
		public APIUserContactDetailDelete (Parameters parameters)
			: base (1, "userContactDetailDelete", parameters) { }

		public class Parameters : IAPIParameters {
			public string Id;

			public Parameters () {
				Id = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "id", Id },
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return Id != null;
			}
		}
	}
}

