using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIUserContactDetailResendVerificationError : APIError {
		public const int RecordNotFound 	= 404;
		public const int AlreadyVerified 	= 410;
	}

	public class APIUserContactDetailResendVerification : APIBase<APIResponse>
	{
		public APIUserContactDetailResendVerification (Parameters parameters)
			: base (1, "userContactDetailResendVerification", parameters) { }

		public class Parameters : IAPIParameters {
			public string Id;

			public Parameters () {
				Id = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "id", Id }
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return Id != null;
			}
		}
	}
}

