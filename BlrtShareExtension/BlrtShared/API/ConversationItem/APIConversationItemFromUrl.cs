using Newtonsoft.Json;
using BlrtData;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationItemFromUrlError : APIError {

		public const int InvalidUrl 			= 620;
		public const int AuthenticationFailure 	= 621;
		public const int PlayCounterLimit 		= 622;
		public const int NotBlrt 				= 623;

	}

	public class APIConversationItemFromUrlResponse : APICreationResponse {

		[JsonProperty("data")]
		public ResponseData Data{ get;private set;}

		private APIConversationItemFromUrlResponse () { }

		public class ResponseData{

			[JsonProperty("title")]
			public string Title{ get; private set;}
			[JsonProperty("creator")]
			public string Creator{ get; private set;}
			[JsonProperty("duration")]
			public int Duration{ get; private set;}
			[JsonProperty("thumbnail")]
			public string Thumbnail{ get; private set;}
			[JsonProperty("pages")]
			public int Pages{ get; private set;}
			[JsonProperty("isPublicBlrt")]
			public bool IsPublicBlrt{ get; private set;}
			[JsonProperty("conversationId")]
			public string ConversationId{ get; private set;}


			[JsonProperty("playData")]
			public PlayDataCl PlayData{get;private set;}

			public class PlayDataCl{
				[JsonProperty("touchDataFile")]
				public string TouchDataFile{ get; private set;}
				[JsonProperty("audioFile")]
				public string AudioFile{ get; private set;}
				[JsonProperty("argument")]
				public string Argument{ get; private set;}
				[JsonProperty("encryptValue")]
				public string EncryptValue{ get; private set;}
				[JsonProperty("encryptType")]
				public int EncryptType{ get; private set;}
				[JsonProperty("media")]
				public MediaData[] Media{ get; private set;}
			}

			public class MediaData{
				[JsonProperty("id")]
				public string Id{ get; private set;}
				[JsonProperty("mediaFile")]
				public string MediaFile{ get; private set;}
				[JsonProperty("mediaFormat")]
				public BlrtMediaFormat MediaFormat{ get; private set;}
				[JsonProperty("encryptValue")]
				public string EncryptValue{ get; private set;}
				[JsonProperty("encryptType")]
				public int EncryptType{ get; private set;}
				[JsonProperty("pageArgs")]
				public string PageArgs{ get; private set;}
			}
		}
	}

	public class APIConversationItemFromUrl : APIBase<APIConversationItemFromUrlResponse>
	{
		public APIConversationItemFromUrl (Parameters parameters)
			: base (1, "conversationItemFromUrl", parameters) { }

		public class Parameters : IAPIParameters {
			public string Url;
			public bool IncludeMedia;

			public Parameters () {
				Url = null;
				IncludeMedia = false;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "url", Url },
					{ "includeMedia", IncludeMedia}
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (Url);
			}
		}
	}
}

