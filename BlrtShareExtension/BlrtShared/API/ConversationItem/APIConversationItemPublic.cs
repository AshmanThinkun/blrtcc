using System.Collections.Generic;
using Parse;

namespace BlrtAPI
{
	public class APIConversationItemPublicError : APIError {
		public const int blrtIdNotFound = 620;
		public const int requestUserRequired = 621;
		public const int notCreator = 622;
		public const int conversationPrivate = 623;
		public const int notBlrt = 624;
	}

	public class APIConversationItemPublicResponse : APICreationResponse {
		private APIConversationItemPublicResponse () { }
	}

	public class APIConversationItemPublic : APIBase<APIConversationItemPublicResponse>
	{
		public APIConversationItemPublic (Parameters parameters)
			: base (1, "conversationItemPublic", parameters) { }

		public class Parameters : IAPIParameters {
			public string BlrtId;
			public string Name;

			public Parameters () {
				BlrtId = null;
				Name = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "blrtId", BlrtId },
					{ "name", Name}
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (BlrtId) && !string.IsNullOrWhiteSpace (Name) && ParseUser.CurrentUser!=null;
			}
		}
	}
}

