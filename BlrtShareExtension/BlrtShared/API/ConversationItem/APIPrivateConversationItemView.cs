using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIPrivateConversationItemViewError : APIError {
		public const int isPublicBlrt = 620;
	}

	public class APIConversationItemViewResponse : APICreationResponse {
		private APIConversationItemViewResponse () { }
	}

	public class APIPrivateConversationItemView : APIBase<APIConversationItemViewResponse>
	{
		public APIPrivateConversationItemView (Parameters parameters)
			: base (1, "conversationItemView", parameters) { }

		public class Parameters : IAPIParameters {
			public string BlrtId;

			public Parameters () {
				BlrtId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "blrtId", BlrtId }
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (BlrtId);
			}
		}
	}
}

