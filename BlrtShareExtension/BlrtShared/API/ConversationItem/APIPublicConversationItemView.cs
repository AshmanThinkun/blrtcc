using System;
using System.Threading.Tasks;
using System.Threading;
using BlrtData;
using System.Collections.Specialized;

namespace BlrtAPI
{
	public class APIPublicConversationItemView : IBlrtAPI
	{
		private string _encodedBlrtId;
		public APIPublicConversationItemView (string encodedBlrtId)
		{
			_encodedBlrtId = encodedBlrtId;
		}

		#region IBlrtAPI implementation

		public async Task<bool> Run (CancellationToken cancellationToken)
		{
			try {
				await BlrtUtil.Http.Post (ApiUrl (), new NameValueCollection {
					{"BlrtId", _encodedBlrtId ?? "(null)"}
				});
				return true;
			} catch {}
			return false;
		}

		string ApiUrl ()
		{
			return BlrtEncode.EncodePublicBlrtUrl (_encodedBlrtId, true) + "/play";
		}
		#endregion
	}
}

