using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtAPI
{
	public class APIRequestDetailsError : APIError {
	}

	public class APIRequestDetailsResponse : APICreationResponse, BlrtData.ExecutionPipeline.IRequestArgs {

		[JsonProperty("name")]
		public string BlrtName { get; set; }
		[JsonProperty("url")]
		public string CaptureUrl { get; set; }
		[JsonProperty("recipients")]
		public string[] Emails { get; set; }
		[JsonProperty("media")]
		public string[] DownloadUrls { get; set; }
		[JsonProperty("id")]
		public string RequestId { get; set; }
		[JsonProperty("makeBlrtPublic")]
		public bool MakeBlrtPublic{ get; set;}

		public string AttachedMeta  { get; set; }
		public string RecipientsId { get; set; }

		private APIRequestDetailsResponse () { }
	}

	public class APIRequestDetails : APIBase<APIRequestDetailsResponse>
	{
		public APIRequestDetails (Parameters parameters)
			: base (1, "requestDetails", parameters) { }

		public class Parameters : IAPIParameters {
			public string RequestId;

			public Parameters () {
				RequestId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "requestId", RequestId },
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (RequestId);
			}
		}
	}
}

