using System;

namespace BlrtAPI
{
	public interface IBlrtAPI
	{
		System.Threading.Tasks.Task<bool> Run (System.Threading.CancellationToken cancellationToken);
	}
}

