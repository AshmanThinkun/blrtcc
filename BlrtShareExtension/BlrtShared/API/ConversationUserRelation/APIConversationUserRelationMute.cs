using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationUserRelationMuteError : APIError
	{
		public const int invalidRelationId = 431;
		public const int invalidMuteFlag = 432;
	}

	public class APIConversationUserRelationMuteResponse : APICreationResponse
	{
		private APIConversationUserRelationMuteResponse () { }
	}

	public class APIConversationUserRelationMute : APIBase<APIConversationUserRelationMuteResponse>
	{
		public APIConversationUserRelationMute (Parameters parameters)
			: base (1, "conversationUserRelationMute", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string RelationId;
			public int MuteFlag;

			public enum MuteEnum
			{
				None = 0,
				All = 1,
				Email = 2,
				Push = 4
			};

			public Parameters ()
			{
				RelationId = null;
				MuteFlag = -1;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "relationId", RelationId },
					{ "muteFlag", MuteFlag}
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				bool valid = !string.IsNullOrWhiteSpace (RelationId) && ParseUser.CurrentUser != null;
				if (MuteFlag < 0 || MuteFlag > 7) {
					return false;
				}
				return valid;
			}

			public static bool ShouldMute (int muteFlag, int type)
			{
				if (muteFlag % 2 == (int)(MuteEnum.All))
					return true;
				var a = muteFlag & type;
				return a == type;
			}
		}
	}
}

