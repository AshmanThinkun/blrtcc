using Parse;
using System.Collections.Generic;

namespace BlrtAPI
{
	public class APIConversationUserRelationLinkStatus
	{
		public const int RelationLinked = 100;
		public const int LinkAlreadyMade = 110;
	}

	public class APIConversationUserRelationLinkError : APIError
	{
		public const int relationNotFound = 404;
		public const int RelationAlreadyLinked = 410;
		public const int userAlreadyInConvo = 411;
	}

	public class APIConversationUserRelationLinkResponse : APICreationResponse
	{
		private APIConversationUserRelationLinkResponse () { }
	}

	public class APIConversationUserRelationLink : APIBase<APIConversationUserRelationLinkResponse>
	{
		public APIConversationUserRelationLink (Parameters parameters)
			: base (1, "conversationUserRelationLink", parameters) { }

		public class Parameters : IAPIParameters
		{
			public string RelationId;

			public Parameters ()
			{
				RelationId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary ()
			{
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "relationId", RelationId },
				};

				return result;
			}

			bool IAPIParameters.IsValid ()
			{
				return !string.IsNullOrWhiteSpace (RelationId) && ParseUser.CurrentUser != null;
			}
		}
	}
}