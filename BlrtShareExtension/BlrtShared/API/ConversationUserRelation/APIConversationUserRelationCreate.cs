using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace BlrtAPI
{
	public class APIConversationUserRelationCreateError : APIError {
		public const int TooManyRelations = 450;
		public const int disabledByCreator = 451;
	}

	public class APIConversationUserRelationCreateResponse : APICreationResponse {
		[JsonProperty("relationStatuses")]
		public APIConversationUserRelationCreate.RelationStatus[] RelationStatuses { get; private set; }

		private APIConversationUserRelationCreateResponse () { }
	}

	public class APIConversationUserRelationCreate : APIBase<APIConversationUserRelationCreateResponse>
	{
		public APIConversationUserRelationCreate (Parameters parameters)
			: base (1, "conversationUserRelationCreate", parameters) { }

		public class Relation {
			public string Type { get; private set; }
			public string Value { get; private set; }
			public string Name { get; private set; }

			public Relation (string type, string value, string name) {
				Type = type;
				Value = value;
				Name = name;
			}

			public IDictionary<string, string> ToDictionary () {
				return new Dictionary<string, string> () {
					{ "type", Type },
					{ "value", Value },
					{ "name", Name }
				};
			}
		}

		public class RelationStatus {
			public enum ResultEnum {
				SentUser 			= 0,
				SentReachable 		= 1,
				SentUnreachable 	= 2,

				AlreadySent 		= 5,

				Invalid 			= 10,

				BlockedByUser 		= 15,
			}

			[JsonProperty("result")]
			public ResultEnum Result { get; private set; }

			[JsonProperty("name")]
			public string Name { get; private set; }

			private RelationStatus () { }
		}

		public class Parameters : IAPIParameters {
			public string ConversationID;
			public string ConversationName;
			public string Note;
			public bool DontAddNoteInConvo;
			public Relation[] Relations;

			public Parameters () {
				ConversationID = null;
				ConversationName = null;
				Note = null;
				Relations = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "conversationId", ConversationID },
					{ "relations", (from rel in Relations select rel.ToDictionary ()).ToArray () },
					{ "dontAddNoteInConvo", DontAddNoteInConvo}
				};

				if (Note != null)
					result ["note"] = Note;
				if (ConversationName != null)
					result ["conversationName"] = ConversationName;

				return result;
			}

			bool IAPIParameters.IsValid () {
				// We either want a token or an email address
				return ConversationID != null && Relations != null && Relations.Count () != 0;
			}
		}
	}
}