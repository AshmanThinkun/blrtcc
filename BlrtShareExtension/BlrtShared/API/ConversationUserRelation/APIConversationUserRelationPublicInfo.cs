using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtAPI
{
	public class APIConversationUserRelationPublicInfoError : APIError {
	}

	public class APIConversationUserRelationPublicInfoResponse : APICreationResponse {

		[JsonProperty("isUser")]
		public bool IsUser { get; private set; }

		[JsonProperty("contactType")]
		public string ContactType { get; private set; }
		[JsonProperty("contactValue")]
		public string ContactValue { get; private set; }

		private APIConversationUserRelationPublicInfoResponse () { }
	}

	public class APIConversationUserRelationPublicInfo : APIBase<APIConversationUserRelationPublicInfoResponse>
	{
		public APIConversationUserRelationPublicInfo (Parameters parameters)
			: base (1, "conversationUserRelationPublicInfo", parameters) { }

		public class Parameters : IAPIParameters {
			public string RelationId;

			public Parameters () {
				RelationId = null;
			}

			Dictionary<string, object> IAPIParameters.ToDictionary () {
				if (!(this as IAPIParameters).IsValid ())
					return null;

				var result = new Dictionary<string, object> () {
					{ "relationId", RelationId },
				};

				return result;
			}

			bool IAPIParameters.IsValid () {
				return !string.IsNullOrWhiteSpace (RelationId);
			}
		}
	}
}

