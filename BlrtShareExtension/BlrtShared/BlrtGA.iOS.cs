//#if __IOS__
//using System;
//using GoogleAnalytics.iOS;
//using UIKit;
//using System.Drawing;
//using System.Collections.Generic;
//using System.Linq;
//using Foundation;
//using CoreGraphics;
//
//namespace BlrtData
//{
//	public static partial class BlrtGA
//	{
//		private class BlrtGAiOS : IBlrtGA
//		{
//
//			string _lastPage;
//
//			public BlrtGAiOS ()
//			{
//				GAI.SharedInstance.DispatchInterval = DISPATCH_INTERVAL;
//				GAI.SharedInstance.TrackUncaughtExceptions = TRACK_UNCAUGHT_EXCEPTIONS;
//
//#if DEBUG
//				GAI.SharedInstance.DispatchInterval = 1;
//#endif
//
//			}
//
//			public void TrackNewSession ()
//			{
//				GAI.SharedInstance.DefaultTracker.Set (GAIConstants.SessionControl, "start");
//				SetAppSettings ();
//			}
//
//			public void TrackPageView (string viewName, bool force = false, Dictionary<string,string> extraParam = null)
//			{
//				if (_lastPage != viewName || force) {
//					GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, viewName);
//					var builder = GAIDictionaryBuilder.CreateScreenView ();
//
//					if(extraParam!=null){
//						foreach(var kv in extraParam){
//							builder.Set (kv.Value, kv.Key);
//						}
//					}
//
//					var objtoSend = builder.Build ();
//
//					GAI.SharedInstance.DefaultTracker.Send (objtoSend);
//
//					_lastPage = viewName;
//
//					DebugListener ("PageView: " + viewName);
//				}
//			}
//
//			public void SetAppSettings ()
//			{
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIFields.CustomDimension (GADimensionUserId),
//					ParseUser.CurrentUser != null ? ParseUser.CurrentUser.ObjectId : ""
//				);
//
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIFields.CustomDimension (GADimensionIndusty),
//					ParseUser.CurrentUser != null ? ParseUser.CurrentUser.Get<string> (BlrtUserHelper.IndustryKey, "") : ""
//				);
//
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIFields.CustomDimension (GADimensionAccountType),
//					BlrtPermissions.AccountName
//				);
//
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIFields.CustomDimension (GADimensionPlatform),
//					BlrtUtil.PlatformHelper.GetPlatformName ()
//				);
//
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIConstants.AppVersion,
//					BlrtManager.Responder.DeviceDetails.AppVersion
//				);
//			}
//
//			public void SetConversationId (string blrtId)
//			{
//				GAI.SharedInstance.DefaultTracker.Set (
//					GAIFields.CustomDimension (GADimensionBlrtId),
//					blrtId
//				);
//
//				GAI.SharedInstance.Dispatch ();
//			}
//
//			public void RemoveBlrtId ()
//			{
//				SetConversationId ("");
//			}
//
//			public void SetTrackerValue (string name, string value)
//			{
//				GAI.SharedInstance.DefaultTracker.Set (name, value);
//			}
//
//			public void SetDimensionValue (int dimension, string value)
//			{
//				GAI.SharedInstance.DefaultTracker.Set (GAIFields.CustomDimension ((nuint)dimension), value);
//			}
//
//			public void TrackTiming (string category, float seconds, string label, string name)
//			{
//				GAI.SharedInstance.DefaultTracker.Send (
//					GAIDictionaryBuilder.CreateTiming (category, NSNumber.FromFloat (seconds * 0.001f), label, name).Build ()
//				);
//			}
//
//			public void TrackEvent (string category, string action, string label, float value)
//			{
//				GAI.SharedInstance.DefaultTracker.Send (
//					GAIDictionaryBuilder.CreateEvent (category, action, label, NSNumber.FromFloat (value)).Build ()
//				);
//
//				DebugListener (string.Format ("E: [{0},{1},{2},{3}]", category, action, label, value));
//			}
//
//
//
//
//
//
//			#if DEBUG && true
//
//
//			class GALabel : IComparable<GALabel>
//			{
//				public UILabel lbl;
//				public DateTime stamp;
//
//				public DateTime HideAt { get { return stamp.AddSeconds (5); } }
//
//				public bool Visable { get { return HideAt > DateTime.Now; } }
//
//				public readonly float Height = 16;
//
//				public GALabel ()
//				{
//					lbl = new UILabel () {
//						Font = UIFont.SystemFontOfSize (12),
//						TextColor = UIColor.FromRGBA (0, 0, 0, 0.8f), 
//						BackgroundColor = UIColor.FromRGBA (1, 1, 1, 0.8f) 
//					};
//				}
//
//				public void SetText (string text)
//				{
//					lbl.Text = " " + text;
//					stamp = DateTime.Now;
//				}
//
//
//				public int CompareTo (GALabel other)
//				{
//					return -stamp.CompareTo (other.stamp);
//				}
//			}
//
//			UIView _parentView;
//			List<GALabel> _viewList;
//
//
//
//			void DebugListener (string message)
//			{
//				CreateDebugListener ();
//
//				if (_parentView == null)
//					return;
//
//				_parentView.BeginInvokeOnMainThread (() => {
//
//					var first = _viewList.FirstOrDefault (view => !view.Visable);
//					if (first == null) {
//						first = new GALabel ();
//						_viewList.Add (first);
//						_parentView.AddSubview (first.lbl);
//					}
//					first.SetText (message);
//
//
//					StartHideThread (first.HideAt);
//
//					_viewList.Sort ();
//				});
//
//				Layout ();
//			}
//
//			void Layout ()
//			{
//				_parentView.BeginInvokeOnMainThread (() => {
//					for (int i = 0; i < _viewList.Count; ++i) {
//						var v = _viewList [i];
//					
//						if (v.Visable) {
//							v.lbl.Hidden = false;
//							v.lbl.Frame = new CGRect (0, 20 + i * v.Height, _parentView.Bounds.Width, v.Height);
//						} else {
//							v.lbl.Hidden = true;
//						}
//					}
//				});			
//			}
//
//			void CreateDebugListener ()
//			{
//				if (_parentView != null) {
//					if (_parentView.Superview != null)
//						_parentView.RemoveFromSuperview ();
//					UIApplication.SharedApplication.Windows [0].AddSubview (_parentView);
//					return;
//				}
//
//				var window = UIApplication.SharedApplication.Windows [0];
//
//				window.InvokeOnMainThread (() => {
//
//					_viewList = new List<GALabel> ();
//					_parentView = new UIView (window.Bounds) {
//						AutoresizingMask = UIViewAutoresizing.FlexibleDimensions,
//						UserInteractionEnabled = false
//					};
//					window.AddSubview (_parentView);
//				});
//			}
//
//			async void StartHideThread (DateTime time)
//			{
//				while (time > DateTime.Now)
//					await System.Threading.Tasks.Task.Delay ((int)Math.Max ((time - DateTime.Now).TotalMilliseconds, 100) + 100);
//
//				Layout ();
//			}
//			#else
//			void DebugListener(string message){}
//#endif
//
//		}
//	}
//}
//#endif