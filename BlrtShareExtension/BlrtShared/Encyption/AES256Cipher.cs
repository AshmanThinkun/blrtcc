﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BlrtData.Encryptor
{
	public class AES256Cipher : IBlrtCipher
	{
		const int BlockSizeByte = 128 / 8;
		const int KeySizeBit = 256;
		const int KeySizeByte = KeySizeBit / 8;
		string _srcPath;
		string _dstPath;
		byte[] _iv = null;
		byte[] _key = null;

		private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

		public AES256Cipher()
		{
		}

		#region IBlrtCipher implementation

		public void Setup(string src, string dst)
		{
			if (null == src || src.Length <= 0)
			{
				throw new ArgumentNullException("src");
			}
			if (null == dst || dst.Length <= 0)
			{
				throw new ArgumentNullException("dst");
			}
			_srcPath = src;
			_dstPath = dst;
		}

		public void SetSender(BlrtBase sender)
		{
			if (BlrtEncryptorManager.AES256 != sender.EncryptType)
			{
				throw new BlrtEncryptorManagerException(BlrtEncryptorManagerException.ExceptionCodes.Unknown);
			}
			if (string.IsNullOrEmpty(sender.EncryptValue))
			{
				sender.EncryptValue = GenerateEncryptionString(out _iv, out _key);
			}
			else
			{
				ParseEncryptionString(sender.EncryptValue, out _iv, out _key);
			}
		}

		// generate the encryptValue of a Parse object based on iv and key. 
		static private string GenerateEncryptionString(out byte[] outIV, out byte[] outKey)
		{
			outIV = new byte[BlockSizeByte];
			rngCsp.GetBytes(outIV);
			outKey = new byte[KeySizeByte];
			rngCsp.GetBytes(outKey);
			var mixed = Mix(outIV, outKey);
			return Convert.ToBase64String(mixed);
		}

		// parse the encryptValue of a Parse object to get iv and key
		static bool ParseEncryptionString(string encryptValue, out byte[] outIV, out byte[] outKey)
		{
			var mixed = Convert.FromBase64String(encryptValue);
			return Unmix(mixed, out outIV, out outKey);
		}

		static readonly uint[] Mixer = { 1, 5, 3, 2, 5, 4, 7, 6, 9, 3, 12, 9, 2, 20, 25 }; // mix the iv and key into a single buffer
		static readonly byte[] SecretKey1 = { // unused array; added to obfuscate the binary
			0x71, 0xEC, 0x91, 0x24, 0x5A, 0x98, 0x36, 0x29, 0x4E, 0x63, 0xCC, 0xB7, 0xCC, 0x49, 0x76, 0xAC,
			0x22, 0xE4, 0x61, 0x1D, 0xD4, 0x3E, 0x3E, 0x96, 0xA5, 0xCB, 0x79, 0x3D, 0xC5, 0x26, 0x44, 0x28,
			0x25, 0x06, 0xD8, 0xB3, 0x3F, 0xD3, 0xFD, 0x8A, 0x51, 0x64, 0x4B, 0xD6, 0x03, 0xED, 0x4C, 0x65,
			0x5A, 0xA8, 0x15, 0x07, 0x7B, 0xD2, 0xC6, 0xDC, 0xBA, 0x20, 0x1D, 0xDF, 0x87, 0xB4, 0x9F, 0x70
		};
		static readonly byte[] SecretKey2 = { // secret key to generate the real encryption / decryption key
			0xCF, 0xDB, 0xE0, 0x5F, 0xF2, 0x71, 0x2E, 0xDB, 0x3E, 0xBB, 0x03, 0xC0, 0x10, 0xC4, 0xCF, 0x23,
			0x5C, 0x93, 0x62, 0x30, 0x03, 0x0D, 0xF0, 0x20, 0x49, 0xAB, 0x02, 0x09, 0x82, 0x7A, 0xF7, 0x30,
			0x5D, 0x95, 0xAC, 0x50, 0x0A, 0xA3, 0x31, 0x57, 0xC7, 0x2C, 0xAF, 0xFF, 0x6A, 0x8F, 0x3F, 0x32,
			0xE5, 0x16, 0xF3, 0xFC, 0x74, 0x9A, 0x8E, 0x58, 0x61, 0x8D, 0xAD, 0x5E, 0xFB, 0x87, 0xDA, 0xFF
		};
		static readonly byte[] SecretKey3 = { // unused array; added to obfuscate the binary
			0x46, 0x58, 0xB7, 0xE5, 0xA8, 0xEE, 0xD9, 0xD5, 0x3D, 0x37, 0x4B, 0xC0, 0x6D, 0xA3, 0x38, 0x27,
			0xFA, 0x3D, 0xD1, 0x0E, 0xC2, 0xAE, 0x14, 0xCE, 0x4D, 0xCF, 0xDE, 0x67, 0x35, 0xFA, 0x3C, 0x2E,
			0xB7, 0xC3, 0x56, 0x13, 0x29, 0x38, 0xE1, 0x13, 0xCB, 0xA7, 0x28, 0xC8, 0x27, 0xA1, 0x4C, 0xE1,
			0x29, 0x10, 0x79, 0x26, 0x85, 0x8B, 0x91, 0xCA, 0x31, 0x2B, 0x3A, 0x9C, 0x9A, 0x82, 0x40, 0xE7
		};
		static readonly byte[] SecretKey4 = { // secret key for generating encrypted string
			0x04, 0x75, 0x8A, 0x94, 0xC7, 0xE5, 0x0E, 0xAD, 0xA9, 0x8B, 0x1F, 0x0E, 0xEB, 0xBF, 0x34, 0xE9,
			0x45, 0xDF, 0xD0, 0x60, 0x8B, 0xA6, 0xDF, 0x09, 0xFF, 0xF4, 0xA6, 0xA5, 0xC0, 0x19, 0x20, 0xC7,
			0x7A, 0x5E, 0x6A, 0xBC, 0xA5, 0xB6, 0x0E, 0xC6, 0xEB, 0xB4, 0xA3, 0x28, 0xC8, 0x63, 0xE1, 0x51,
			0x3D, 0x42, 0xBD, 0x8C, 0x49, 0x2E, 0xF7, 0xA5, 0x1F, 0xD1, 0x28, 0xA4, 0x5E, 0xDC, 0x80, 0xB7
		};
		static readonly byte[] EncryptionStringXor = {	// xor key for mixed buffer; some of the data are not used
			0x64, 0x15, 0x5D, 0x9A, 0x83, 0x65, 0xB3, 0x56, 0x57, 0x4D, 0xB5, 0x10, 0x35, 0x62, 0x4A, 0xC1,
			0x91, 0x56, 0x5F, 0x45, 0x25, 0xB3, 0xFC, 0xF0, 0xA4, 0x72, 0x19, 0x3E, 0x2E, 0xDA, 0x38, 0x90,
			0xFE, 0x86, 0xE4, 0x67, 0x6C, 0xFB, 0x06, 0xF1, 0xAE, 0xC7, 0x74, 0x9B, 0x56, 0x47, 0x8D, 0xBF,
			0x65, 0xA2, 0xA2, 0xBC, 0x03, 0x7C, 0xBB, 0x97, 0x82, 0xD9, 0x70, 0x05, 0x58, 0x39, 0x09, 0x01
		};
		const uint XorStart = 8; // DO NOT CHANGE IT!!! all the byte in EncryptionStringXor before this value will not be used

		// mix the iv and key into a single buffer
		static byte[] Mix(byte[] iv, byte[] key)
		{
			if ((EncryptionStringXor.Length + XorStart) < (BlockSizeByte + KeySizeByte)
				|| iv.Length != BlockSizeByte
				|| key.Length != KeySizeByte)
			{
				throw new NotImplementedException();
			}

			uint ivIndex = 0;
			uint keyIndex = 0;
			byte[] returnBytes = new byte[BlockSizeByte + KeySizeByte];
			uint returnBytesIndex = 0;

			// mix and xor
			for (uint i = 0; i < Mixer.Length; ++i)
			{
				if (i % 2 == 0)
				{ // iv
					for (uint j = 0; j < Mixer[i] && ivIndex < iv.Length; ++j, ++ivIndex, ++returnBytesIndex)
					{
						returnBytes[returnBytesIndex] = (byte)(iv[ivIndex] ^ EncryptionStringXor[returnBytesIndex + XorStart]);
					}
				}
				else
				{ // key
					for (uint j = 0; j < Mixer[i] && keyIndex < key.Length; ++j, ++keyIndex, ++returnBytesIndex)
					{
						returnBytes[returnBytesIndex] = (byte)(key[keyIndex] ^ EncryptionStringXor[returnBytesIndex + XorStart]);
					}
				}
				if (returnBytesIndex >= returnBytes.Length)
				{
					break;
				}
			}


			if (ivIndex == iv.Length && keyIndex == key.Length && returnBytesIndex == returnBytes.Length)
			{
				return returnBytes;
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		// get iv and key from the mixed buffer
		static bool Unmix(byte[] mixed, out byte[] outIV, out byte[] outKey)
		{
			if ((EncryptionStringXor.Length + XorStart) < (BlockSizeByte + KeySizeByte)
				|| mixed.Length != (BlockSizeByte + KeySizeByte))
			{
				throw new NotImplementedException();
			}

			var iv = new byte[BlockSizeByte];
			var key = new byte[KeySizeByte];
			uint ivIndex = 0;
			uint keyIndex = 0;
			uint mixedBytesIndex = 0;

			// xor and unmix
			for (uint i = 0; i < Mixer.Length; ++i)
			{
				if (i % 2 == 0)
				{ // iv
					for (uint j = 0; j < Mixer[i] && ivIndex < iv.Length; ++j, ++ivIndex, ++mixedBytesIndex)
					{
						iv[ivIndex] = (byte)(mixed[mixedBytesIndex] ^ EncryptionStringXor[mixedBytesIndex + XorStart]);
					}
				}
				else
				{ // key
					for (uint j = 0; j < Mixer[i] && keyIndex < key.Length; ++j, ++keyIndex, ++mixedBytesIndex)
					{
						key[keyIndex] = (byte)(mixed[mixedBytesIndex] ^ EncryptionStringXor[mixedBytesIndex + XorStart]);
					}
				}
				if (mixedBytesIndex >= mixed.Length)
				{
					break;
				}
			}


			if (ivIndex == iv.Length && keyIndex == key.Length && mixedBytesIndex == mixed.Length)
			{
				outIV = iv;
				outKey = key;
				return true;
			}
			else
			{
				throw new NotImplementedException();
			}
			return false;
		}

		public void SetPassword(string argument)
		{
			// do not need "password"; use encryptionValue
		}

		public async Task RunDecrypt()
		{
			using (FileStream srcStream = new FileStream(_srcPath, FileMode.Open, FileAccess.Read))
			{
				try
				{
					var k = MixStream(_key); // check the function comment
#if __IOS__
					using (Stream csDecrypt = DecryptStream(srcStream, _iv, k))
					{
						using (FileStream dstStream = new FileStream (_dstPath, FileMode.Create, FileAccess.Write)) {
							// copy all the bytes from the decryption stream to the destination file stream
							await csDecrypt.CopyToAsync (dstStream);
							// flush before return
							await dstStream.FlushAsync ();
							dstStream.Close ();
						}
						csDecrypt.Close ();
					}
#elif __ANDROID__
					using (Stream csDecrypt = DecryptStream(srcStream, _iv, k))
					{
						try
						{
							using (var memorystream = new MemoryStream())
							{
								await csDecrypt.CopyToAsync(memorystream);
								File.WriteAllBytes(_dstPath, memorystream.ToArray());
							}
						}
						catch (Exception e)
						{
							Console.WriteLine("======Copy async exception=====: " + e);
						}
						csDecrypt.Dispose();
					}
#endif
				}
				catch (CryptographicException exxx)
				{ // padding error, would be thrown when the key is wrong
					DeleteFileNoThrow (_dstPath);
					throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.DecryptionFailed);
				} finally {
					srcStream.Close ();
				}
			}
		}

		void DeleteFileNoThrow(string path)
		{
			try {
				File.Delete (path);
			} catch {}
		}

		public async Task RunEncrypt ()
		{
			const uint RetryTime = 2;
			string srcMd5 = null;
			for (var retries = RetryTime; retries > 0; --retries) {
				byte[] k = null;
				try {
					using (FileStream srcStream = new FileStream(_srcPath, FileMode.Open, FileAccess.Read))
					{
						k = MixStream(_key); // check the function comment
						using (Stream csEncrypt = EncryptStream(srcStream, _iv, k))
						{
							using (FileStream dstStream = new FileStream(_dstPath, FileMode.Create, FileAccess.Write))
							{
								// copy all the bytes from the encryption stream to the destination file stream
								await csEncrypt.CopyToAsync (dstStream);
								// flush before return
								await dstStream.FlushAsync ();
								dstStream.Close ();
							}
							csEncrypt.Close ();
						}
						srcStream.Close ();
					}
				} catch (Exception ex) {
					DeleteFileNoThrow (_dstPath);
					continue;
				}
				var srcInfo = new FileInfo(_srcPath);
				var dstInfo = new FileInfo(_dstPath);

				// check whether the size of the encrypted file is correct.
				if (dstInfo.Length != (int)(Math.Ceiling((float)(srcInfo.Length + 1) / BlockSizeByte) * BlockSizeByte)) {
					DeleteFileNoThrow (_dstPath);
					continue;
				}

				// check whether the decrypted file is the same with encrypted file
				bool succeeded = false;
				var tempFilePath = _dstPath + "-crctmp";
				try {
					// decrypt the encrypted file first
					using (FileStream encryptedFileStream = new FileStream (_dstPath, FileMode.Open, FileAccess.Read))
					{
						using (Stream csDecrypt = DecryptStream (encryptedFileStream, _iv, k))
						{
							using (FileStream dstStream = new FileStream (tempFilePath, FileMode.Create, FileAccess.Write))
							{
								// copy all the bytes from the decryption stream to the destination file stream
								await csDecrypt.CopyToAsync (dstStream);
								// flush before return
								await dstStream.FlushAsync ();
								dstStream.Close ();
							}
							csDecrypt.Close ();
						}
						encryptedFileStream.Close ();
					}

					// check whether the md5 of the decrypted file and the source file are the same
					string ex_md5_src_error = null;
					if (null == srcMd5) {
						try {
							srcMd5 = BlrtUtil.FileHexMd5 (_srcPath);
						} catch (Exception ex_md5_src) {
							ex_md5_src_error = ex_md5_src.ToString ();
						}
					}
					if (null == srcMd5) {
						// cannot get src file md5. This should not happen.
						succeeded = true;
						LLogger.WriteEvent ("encrypt", "error", "cannot get src file md5", ex_md5_src_error);
					} else {
						var resultMd5 = BlrtUtil.FileHexMd5 (tempFilePath);
						succeeded = resultMd5.Equals (srcMd5);
					}
				} catch {
					succeeded = false;
				}
				DeleteFileNoThrow (tempFilePath);
				if (succeeded) {
					return;
				} else {
					DeleteFileNoThrow (_dstPath);
					continue;
				}
			}
			throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.EncryptionFailed);
		}

		// used to generate the real key...its name is obfuscation...i don't think the binary will contain the function name, but...
		static private byte[] MixStream(byte[] key)
		{
			var secret = new byte[KeySizeByte];
			Array.Copy (SecretKey2, (int)(XorStart * 1.3), secret, 0, KeySizeByte);
			using (var hmac = new HMACSHA256(secret)) {
				return hmac.ComputeHash (key);
			}
		}

		static private Stream DecryptStream (Stream stream, byte[] iv, byte[] key)
		{
			if (null == stream)
			{
				throw new ArgumentNullException("stream");
			}
			var aes = new AesManaged () {
				KeySize = KeySizeBit,
				Padding = PaddingMode.PKCS7,
				Mode = CipherMode.CBC,
				Key = key,
				IV = iv
			};
			// create a decryptor based on current AesManaged
			ICryptoTransform decryptor = aes.CreateDecryptor();
			return new CryptoStream (stream, decryptor, CryptoStreamMode.Read);
		}

		static private Stream EncryptStream (Stream stream, byte[] iv, byte[] key)
		{
			if (null == stream)
			{
				throw new ArgumentNullException("stream");
			}
			var aes = new AesManaged () {
				KeySize = KeySizeBit,
				Padding = PaddingMode.PKCS7,
				Mode = CipherMode.CBC,
				Key = key,
				IV = iv
			};
			// create an encryptor based on current AesManaged
			ICryptoTransform encryptor = aes.CreateEncryptor();
			return new CryptoStream (stream, encryptor, CryptoStreamMode.Read);
		}
#endregion

		public static async Task<string> EncryptString(string plainText)
		{
			var buffer = System.Text.Encoding.Unicode.GetBytes (plainText);
			var srcStream = new MemoryStream (buffer);
			var iv = new byte[BlockSizeByte];
			rngCsp.GetBytes (iv);
			var k = new byte[KeySizeByte];
			Array.Copy (SecretKey4, (int)(XorStart * 1.6), k, 0, KeySizeByte);
			using (Stream csEncrypt = EncryptStream(srcStream, iv, k))
			{
				using (var dstStream = new MemoryStream ())
				{
					// copy all the bytes from the encryption stream to the destination file stream
					await csEncrypt.CopyToAsync (dstStream);
					// flush before return
					await dstStream.FlushAsync ();
					var resultArray = new byte[iv.Length + dstStream.Length];
					Array.Copy (iv, 0, resultArray, 0, iv.Length);
					Array.Copy (dstStream.GetBuffer (), 0, resultArray, iv.Length, dstStream.Length);
					return Convert.ToBase64String (resultArray);
				}
			}
		}

		public static async Task<string> DecryptString(string encryptedText)
		{
			var mixedData = Convert.FromBase64String (encryptedText);
			if (null != mixedData 
				&& mixedData.Length > BlockSizeByte * 2 
				&& (mixedData.Length % BlockSizeByte == 0))
			{
				var iv = new byte[BlockSizeByte];
				Array.Copy (mixedData, 0, iv, 0, iv.Length);
				var encryptedData = new byte[mixedData.Length - iv.Length];
				Array.Copy (mixedData, iv.Length, encryptedData, 0, encryptedData.Length);
				var srcStream = new MemoryStream (encryptedData);
				var k = new byte[KeySizeByte];
				Array.Copy (SecretKey4, (int)(XorStart * 1.6), k, 0, KeySizeByte);
				using (Stream csDecrypt = DecryptStream(srcStream, iv, k))
				{
					using (var dstStream = new MemoryStream ())
					{
						// copy all the bytes from the decryption stream to the destination file stream
						await csDecrypt.CopyToAsync (dstStream);
						// flush before return
						await dstStream.FlushAsync ();
						var resultStringArray = new byte[dstStream.Length];
						Array.Copy (dstStream.GetBuffer (), 0, resultStringArray, 0, resultStringArray.Length);
						return System.Text.Encoding.Unicode.GetString (resultStringArray);
					}
				}
			}
			throw new InvalidOperationException ("bad encrypted text");
		}
	}
}

