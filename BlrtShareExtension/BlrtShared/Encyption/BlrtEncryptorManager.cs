using System;
using System.IO;
using System.Threading.Tasks;

using BlrtData.Encryptor;

namespace BlrtData
{
	public static class BlrtEncryptorManager
	{
		public const int None = 0;
		public const int BasicXor = 1;
		public const int BasicWithKeyXor = 2;
		public const int BasicWithKeyImprovedXor = 3;
		public const int AES256 = 4;

		#if DEBUG
		public static int RecommendedEncyption{get{ return BlrtManager.Settings.encryption_type;}}
		#else
		public static int RecommendedEncyption{get{ return BlrtManager.Settings.encryption_type;}}
		#endif


		public static bool CanDecypt (BlrtBase sender)
		{
			switch (sender.EncryptType) {
				case None:
				case BasicXor:
				case BasicWithKeyXor:
				case BasicWithKeyImprovedXor:
				case AES256:
					return true;
			}

			return false;
		}



		public static bool EncryptionRequiresPassword (BlrtBase sender)
		{
			switch (sender.EncryptType) {
				case None:
				case BasicXor: 					// use creator ID as password
				case BasicWithKeyXor: 			// use encryptValue as password
				case BasicWithKeyImprovedXor:	// use encryptValue as password
				case AES256: 					// use encryptValue as password
					return false;
			}

			return false;
		}

		public static Task Encrypt (string srcPath, string dstPath, BlrtBase sender, string password = null)
		{
			if (EncryptionRequiresPassword (sender) && password == null)
				throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.RequiresPassword);

			IBlrtCipher cipher = null;

			switch (sender.EncryptType) {
				case None:
					cipher = new CopyCipher ();
					break;
				case BasicXor:
				case BasicWithKeyXor:
				case BasicWithKeyImprovedXor:
					cipher = new XorCipher ();
					break;
				case AES256:
					cipher = new AES256Cipher ();
					break;
			}

			if (cipher != null) {

				cipher.Setup (srcPath, dstPath);
				cipher.SetSender (sender);
				cipher.SetPassword (password);

				return cipher.RunEncrypt ();
			}

			throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.Unknown);
		}


		public static Task Decrypt (string srcPath, string dstPath, BlrtBase sender, string password = null)
		{
			if (!CanDecypt (sender))
				throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.CannotDecryptType);
			if (EncryptionRequiresPassword (sender) && password == null)
				throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.RequiresPassword);

			IBlrtCipher cipher = null;

			switch (sender.EncryptType) {
				case None:
					cipher = new CopyCipher ();
					break;
				case BasicXor:
				case BasicWithKeyXor:
				case BasicWithKeyImprovedXor:
					cipher = new XorCipher ();
					break;
				case AES256:
					cipher = new AES256Cipher ();
					break;
			}

			if (cipher != null) {
			
				cipher.Setup (srcPath, dstPath);
				cipher.SetSender (sender);
				cipher.SetPassword (password);
			
				return cipher.RunDecrypt ();
			}


			throw new BlrtEncryptorManagerException (BlrtEncryptorManagerException.ExceptionCodes.Unknown);
		}
	}

	public class BlrtEncryptorManagerException : Exception
	{
		public enum ExceptionCodes
		{
			Unknown,
			RequiresPassword,
			CannotDecryptType,
			EncryptionFailed,
			DecryptionFailed
		}


		public ExceptionCodes Code {
			get;
			private set;
		}

		public BlrtEncryptorManagerException (ExceptionCodes code) : base ()
		{
			this.Code = code;
		}
	}



	public interface IBlrtCipher
	{
		void Setup (string src, string dst);

		void SetSender (BlrtBase sender);

//		void SetValues (int type, string value);

		void SetPassword (string argument);

		Task RunDecrypt ();

		Task RunEncrypt ();
	}
}

