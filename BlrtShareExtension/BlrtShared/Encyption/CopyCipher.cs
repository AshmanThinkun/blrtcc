using System;
using System.Threading.Tasks;
using System.IO;

namespace BlrtData.Encryptor
{
	public class CopyCipher : IBlrtCipher
	{
		string srcPath, dstPath;



		public CopyCipher ()
		{
		}

		public void Setup (string src, string dst)
		{
			srcPath = src;
			dstPath = dst;
		}

		public void SetSender (BlrtBase sender)
		{
		}

		//		public void SetValues (int type, string value)
		//		{
		//		}

		public void SetPassword (string argument)
		{
			// do not need "password"
		}

		public System.Threading.Tasks.Task RunDecrypt ()
		{
			return RunEncrypt ();
		}

		private static bool firstRun = true; // [TODO] temporary fix the welcome Blrt issue. Check https://thinkun.atlassian.net/browse/BLRT-1944

		public System.Threading.Tasks.Task RunEncrypt ()
		{
			return Task.Run (async () => {
				if (firstRun) {
					firstRun = false;
					await Task.Delay (500);
				}
				File.Copy (srcPath, dstPath, true);
			});
		}

	}
}

