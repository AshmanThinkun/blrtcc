using System;
using System.Threading.Tasks;
using System.IO;

namespace BlrtData.Encryptor
{
	public class XorCipher : IBlrtCipher
	{
		string srcPath, dstPath;

		int _type;
		string _value;


		public XorCipher ()
		{
		}

		public void Setup (string src, string dst)
		{
			srcPath = src;
			dstPath = dst;
		}

		public void SetSender (BlrtBase sender)
		{
			switch (sender.EncryptType) {
				case BlrtEncryptorManager.BasicXor:
					SetValues (sender.EncryptType, sender.CreatorId);
					break;
				case BlrtEncryptorManager.BasicWithKeyXor:
				case BlrtEncryptorManager.BasicWithKeyImprovedXor:

					if (string.IsNullOrEmpty (sender.EncryptValue) && !sender.OnParse)
						sender.EncryptValue = Guid.NewGuid ().ToString ();

					SetValues (sender.EncryptType, sender.EncryptValue);
					break;
			}
		}

		private void SetValues (int type, string value)
		{
			_type = type;
			_value = value;
		}

		public void SetPassword (string argument)
		{
			// do not need "password"; use encryptionValue
		}

		public Task RunDecrypt ()
		{
			return Task.Run (async () => {

				int seedCount = _type == BlrtEncryptorManager.BasicWithKeyImprovedXor ? _value.Length : 16;

				byte[] seedNums = new byte[seedCount];

				for (int i = 0; i < seedNums.Length; ++i) {
					seedNums [i] =	(byte)(i + (byte)_value [i % _value.Length]);
				}

				File.Copy (srcPath, dstPath, true);

				using (var fileStream = new FileStream (srcPath, FileMode.Open, FileAccess.Read)) {

					using (var cloneStream = new FileStream (dstPath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {

						fileStream.Position = cloneStream.Position = 0;
						byte[] buffer = new byte[256 * 32];
						int length = 0;

						ulong read = 0;


						while ((length = fileStream.Read (buffer, 0, buffer.Length)) > 0) {
							for (int i = 0; i < length; ++i) {
								buffer [i] = (byte)(buffer [i] ^ GenerateRand (((uint)i + read) ^ seedNums [i % seedNums.Length]));
							}

							cloneStream.Write (buffer, 0, length);

							read += (uint)length;
						}

						buffer = null;

						await cloneStream.FlushAsync ();
						cloneStream.Close ();
					}
					fileStream.Close ();
				}

				seedNums = null;
			});
		}

		public Task RunEncrypt ()
		{
			return RunDecrypt ();
		}

		byte GenerateRand (ulong i)
		{
			return (byte)((i * 8646)
			^ (i * i * 5486516)
			^ (i * i * i * 158919));									
		}
	}
}

