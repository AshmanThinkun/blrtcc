//using System;
//using System.IO;
//using System.Threading.Tasks;
//using System.Linq;
//using System.Collections.Generic;
//
//using BlrtData.IO;
//using BlrtData.Contents;
//
//
//using Parse;
//
//namespace BlrtData.Media
//{
//	// not used now
//	public abstract class BlrtMediaManager
//	{
//		public int Pages { get { return _list.Count; } }
//		public int HiddenPages { get { return _hiddenList.Count; } }
//
//
//		private List<IBlrtPage> _list;
//		private List<IBlrtPage> _hiddenList;
//
//		public BlrtMediaManager () { 
//			_list = new List<IBlrtPage> ();
//			_hiddenList = new List<IBlrtPage> ();
//		}
//		public BlrtMediaManager (IBlrtPage[] pages, IBlrtPage[] hiddenPages)  { 
//			_list = new List<IBlrtPage> (pages);
//			_hiddenList = new List<IBlrtPage> (hiddenPages);
//		}
//
//		public BlrtMediaManager (BlrtMediaPagePrototype pageIndex)
//		{
//			_list = new List<IBlrtPage> ();
//			_hiddenList = new List<IBlrtPage> ();
//
//			for (int i = 0; i <pageIndex.UncompressedItems.Length; ++i) {
//				var item = pageIndex.UncompressedItems [i];
//
//				var mediaAsset = BlrtManager.DataManagers.Default.GetMediaAsset(item.objectId);
//
//				if(mediaAsset != null)
//					Add (CreateMediaObject (mediaAsset, item.mediaPage)); 
//				else 
//					Add (CreateTemplateObject(item.objectId));
//
//			}
//
//			for (int i = 0; i < pageIndex.UncompressedHiddenItems.Length; ++i) {
//				var item = pageIndex.UncompressedHiddenItems [i];
//
//				var mediaAsset = BlrtManager.DataManagers.Default.GetMediaAsset(item.objectId);
//
//
//				if(mediaAsset != null)
//					AddHidden (CreateMediaObject (mediaAsset, item.mediaPage)); 
//				else 
//					AddHidden (CreateTemplateObject(item.objectId)); 
//
//			}
//		}
//
//		protected abstract IBlrtPage CreateTemplateObject (string templateId);
//		protected abstract IBlrtPage CreateMediaObject (MediaAsset mediaAssets, int page);
//
//
//		public void Add (IBlrtPage mediaObject) {
//
//			if (mediaObject == null)
//				throw new NullReferenceException ();
//
//
//			_list.Add (mediaObject);
//		}
//		public void AddHidden (IBlrtPage mediaObject) {
//
//			if (mediaObject == null)
//				throw new NullReferenceException ();
//
//			_hiddenList.Add (mediaObject);
//		}
//
//
//		public void Replace (IBlrtPage media, IBlrtPage newMedia)
//		{
//			int index = _list.IndexOf (media);
//
//			if (index >= 0) {
//				_list [index] = newMedia;
//			}
//		}
//
//
//		public void RemovePage (int page) {
//			_list.RemoveAt (page - 1);
//		}
//
//		public IBlrtPage GetPage(int page) {
//			return _list [page - 1];
//		}
//
//		public IBlrtPage GetHiddenPage(int page) {
//			return _hiddenList [page - 1];
//		}
//
//
//
//		public static Task DecyptAndOpenFiles (IEnumerable<IBlrtPage> media)
//		{
//			var list = new List<IBlrtPage>(media);
//			var taskList = new List<Task>();
//
//			for(int i = 0; i <list.Count; ++i){
//
//				var page = list[i];
//
//				if(!page.MediaOpenned && page.HasMediaAsset){
//
//					list.Remove (page);
//					i--;
//
//					var task = page.DecyptAndOpenMedia();
//
//					for(int j = i + 1; j < list.Count; ++j){
//						if (!list[j].MediaOpenned && list[j].HasMediaAsset && list[j].SameMedia(page)) {
//
//
//							var page2 = list [j];
//
//							task = task.ContinueWith((Task<IBlrtPage> t) => {
//								page2.OpenMedia(page);							
//								return t.Result;
//							});
//
//							list.Remove (page2);
//							j--;
//						}
//					}
//
//					taskList.Add (task);
//				}
//			}
//
//			return Task.WhenAll (taskList);
//		}
//
//
//
//
//		public Task DecyptAndOpenFiles ()
//		{
//			return DecyptAndOpenFiles(_list);
//		}
//
//
//
//
//		public BlrtMediaPagePrototype ToPageIndex() {
//			var usedPageIndexes = new List<BlrtMediaPagePrototype.PageIndex> ();
//			var unusedPageIndexes = new List<BlrtMediaPagePrototype.PageIndex> ();
//
//			for (int i = 0; i < _list.Count; ++i) {
//				if (!_list[i].HasMediaAsset)
//					throw new Exception ("Object needs to be on Parse!");
//
//				usedPageIndexes.Add (
//					new BlrtMediaPagePrototype.PageIndex (
//						_list [i].ObjectId, 
//						_list [i].MediaPage
//					)
//				);
//			}
//			for (int i = 0; i < _hiddenList.Count; ++i) {
//				if (!_hiddenList[i].HasMediaAsset)
//					throw new Exception ("Object needs to be on Parse!");
//
//				unusedPageIndexes.Add (
//					new BlrtMediaPagePrototype.PageIndex (
//						_hiddenList [i].ObjectId, 
//						_hiddenList [i].MediaPage
//					)
//				);
//			}
//
//			return new BlrtMediaPagePrototype () {
//				UncompressedItems 		= usedPageIndexes.ToArray(),
//				UncompressedHiddenItems = unusedPageIndexes.ToArray()
//			};
//		}
//
//
//		public async Task CreateMediaAssets ()
//		{
//			int mediaCache = 0;
//
//			var totalList = _list.Concat (_hiddenList);
//			var _listMediaAsset = new List<MediaAsset> ();
//
//			foreach(var page in totalList){
//
//				if (!page.IsTemplate && !page.HasMediaAsset) {
//
//					for(; mediaCache < int.MaxValue; ++mediaCache) {
//
//						var filename = string.Format("media-cache{0}.{1}", mediaCache, page.FileExtention);
//						var srcFilePath = BlrtConstants.Directories.Default.GetCachedPath (filename + ".tmp");
//						var dstFilePath = BlrtConstants.Directories.Default.GetCachedPath (filename);
//
//						if (!File.Exists (dstFilePath) && !File.Exists (srcFilePath)) {
//
//							var mediaAsset = new MediaAsset(){
//								Name 			= page.Name,
//								Format 			= page.Format,
//								LocalMediaName 	= filename,
//								EncryptType 	= BlrtEncryptorManager.RecommendedEncyption,
//								ShouldUpload 	= true
//							};
//
//							var pageArgs = new int[page.TotalMediaPages];
//
//							for(int i = 0; i < page.TotalMediaPages; ++i)
//								pageArgs[i] = i + 1;
//
//							mediaAsset.PageArgs = pageArgs;
//
//							page.SaveMedia (srcFilePath);
//							await BlrtEncryptorManager.Encrypt(
//								srcFilePath,
//								dstFilePath,
//								mediaAsset
//							);
//							File.Delete (srcFilePath);
//							page.SetMediaAsset (mediaAsset);
//
//							foreach (var dup in totalList) {
//								if (dup.SameMedia (page)) {
//									dup.SetMediaAsset (mediaAsset);
//								}
//							}
//
//							_listMediaAsset.Add (mediaAsset);
//							BlrtManager.DataManagers.Default.AddUnsynced (mediaAsset);
//
//							break;
//						}
//					}
//				}
//			}
//
//			foreach (var m in _listMediaAsset) {
//				m.TryUploadAsset ();
//			}
//		}
//
//		public IEnumerable<ParseObject> AsParseObjects ()
//		{
//			foreach (var m in _list) {
//
//				if (m.Format == BlrtMediaFormat.Template)
//					continue;
//
//				if(m.HasMediaAsset)
//					throw new Exception ("Object needs to be on Parse!");
//
//				yield return ParseObject.CreateWithoutData (MediaAsset.CLASS_NAME, m.ObjectId);
//			}
//		}
//
//		public string[] GetObjectIds ()
//		{
//			return (from doop in _list
//				where !doop.IsTemplate
//				select doop.ObjectId
//			).Distinct().ToArray();
//		}
//	}
//
//	public interface IBlrtPage
//	{
//		string Name 			{ get; }
//		string FileExtention 	{ get; }
//		object Media 			{ get; }
//		BlrtMediaFormat Format 	{ get; }
//		int MediaPage			{ get; }
//		int TotalMediaPages		{ get; }
//		bool IsTemplate		 	{ get; }
//		bool MediaOpenned		{ get; }
//
//		int PixelWidth 			{ get; }
//		int PixelHeight 		{ get; }
//
//		string ObjectId			{ get; }
//		bool HasMediaAsset		{ get; }
//
//		System.Threading.Tasks.Task<IBlrtPage> DecyptAndOpenMedia ();
//		void OpenMedia (string filepath);
//		void OpenMedia (IBlrtPage mediaPage);
//
//
//		void SetMediaAsset(MediaAsset asset);
//		bool IsMediaAsset (MediaAsset asset);
//
//
//		bool SameMedia (IBlrtPage iMediaPage);
//
//		void SaveMedia (string filepath);
//	}
//}
//
