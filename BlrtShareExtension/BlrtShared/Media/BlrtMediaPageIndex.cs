using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtData.Media
{
	public class BlrtMediaPageIndex
	{
		public PageIndex[] Items 	{ get; set; }

		[JsonIgnore]
		public int TotalPages { get { 
				int r = 0;
				for (int i = 0; i < Items.Length; ++i)
					r += Items [i].Pages;
				return r;
			} 
		}

		public BlrtMediaPageIndex()
		{
		}

		public BlrtMediaPageIndex(BlrtMediaPageIndex other)
		{
			Items = new PageIndex[other.Items.Length];
			for (int i = 0; i < Items.Length; ++i) {
				Items [i] = new PageIndex () {
					PageString = other.Items[i].PageString,
					MediaId = other.Items[i].MediaId
				};
			}
		}

		public int GetMediaPage(int index)
		{
			for (int i = 0; i < Items.Length; ++i) {

				if (index < Items [i].Pages) {
					return Items [i].PageList[index];
				} else
					index -= Items[i].Pages;
			}
			return 0;
		}

		public string GetMediaId(int index)
		{
			for (int i = 0; i < Items.Length; ++i) {

				if (index < Items [i].Pages) {
					return Items [i].MediaId;
				} else
					index -= Items[i].Pages;
			}
			return null;
		}





		public static BlrtMediaPageIndex FromString(string str){
			return JsonConvert.DeserializeObject<BlrtMediaPageIndex> (str);		
		}
	}


	public class PageIndex
	{
		private string _pagesArg;
		private int[] _pageList ;

		public string PageString 	{ get{ return _pagesArg; } set { 
				if (value!= _pagesArg) {
					SetPagesArg (value);
				}
		} }
		[JsonIgnore]
		public int[] PageList 		{ get { return _pageList;} set { 
				if (value!= _pageList) {
					SetPages (value);
				}
			}
		}
		[JsonIgnore]
		public int Pages 		{ get { return _pageList.Length; } } 

		public string MediaId 	{ get; set; }


		private void SetPagesArg (string value)
		{
			_pagesArg = value;
			PageList = BlrtUtil.PagesToList (value).ToArray ();
		}

		private void SetPages (int[] value)
		{
			_pageList = value;
			PageString = BlrtUtil.ListToPages (value);
		}
	}
}