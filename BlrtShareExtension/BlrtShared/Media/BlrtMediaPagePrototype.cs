using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtData.Media
{
	[Serializable]
	public class BlrtMediaPagePrototype
	{

		private CompressedPageIndex [] _items;
		private CompressedPageIndex [] _unusedItems;

		private PageIndex [] _uncompressedItems;
		private PageIndex [] _uncompressedUnusedItems;



		public CompressedPageIndex [] Items {
			get {
				if (_items == null)
					Recalulate ();
				return _items;
			}
			set {
				if (value != null) _uncompressedItems = null;
				_items = value;
			}
		}

		public CompressedPageIndex [] HiddenItems {
			get {
				if (_unusedItems == null)
					Recalulate ();
				return _unusedItems;
			}
			set {
				if (value != null) _uncompressedUnusedItems = null;
				_unusedItems = value;
			}
		}

		[JsonIgnore]
		public PageIndex [] UncompressedItems {
			get {
				if (_uncompressedItems == null)
					Recalulate ();
				return _uncompressedItems;
			}
			set {
				if (value != null) _items = null;
				_uncompressedItems = value;
			}
		}

		[JsonIgnore]
		public PageIndex [] UncompressedHiddenItems {
			get {
				if (_uncompressedUnusedItems == null)
					Recalulate ();
				return _uncompressedUnusedItems;
			}
			set {
				if (value != null) _unusedItems = null;
				_uncompressedUnusedItems = value;
			}
		}

		[JsonIgnore]
		public int TotalPages {
			get {
				return UncompressedItems.Length;
			}
		}

		[JsonIgnore]
		public int TotalHiddenPages {
			get {
				return UncompressedHiddenItems.Length;
			}
		}

		[JsonIgnore]
		public bool Usable {
			get {
				return (_uncompressedItems != null || _items != null)
					&& (_uncompressedUnusedItems != null || _unusedItems != null);
			}
		}

		public BlrtMediaPagePrototype ()
		{
			Items = new CompressedPageIndex [0];
			HiddenItems = new CompressedPageIndex [0];
		}

		public BlrtMediaPagePrototype (BlrtMediaPagePrototype other)
		{
			if (other.UncompressedItems != null) {
				_uncompressedItems = new PageIndex [other.UncompressedItems.Length];

				for (int i = 0; i < _uncompressedItems.Length; ++i) {
					_uncompressedItems [i] = new PageIndex (
						other.UncompressedItems [i].objectId,
						other.UncompressedItems [i].mediaPage
					);
				}
			}

			if (other.UncompressedItems != null) {
				_uncompressedUnusedItems = new PageIndex [other.UncompressedHiddenItems.Length];

				for (int i = 0; i < _uncompressedUnusedItems.Length; ++i) {
					_uncompressedUnusedItems [i] = new PageIndex (
						other.UncompressedHiddenItems [i].objectId,
						other.UncompressedHiddenItems [i].mediaPage
					);
				}
			}
		}

		public int GetMediaPage (int index)
		{
			if (index >= 0 && index < UncompressedItems.Length)
				return UncompressedItems [index].mediaPage;
			return 0;
		}

		public string GetMediaId (int index)
		{
			if (index >= 0 && index < UncompressedItems.Length)
				return UncompressedItems [index].objectId;
			return null;
		}

		void Recalulate ()
		{
			if (_items == null && _uncompressedItems != null)
				_items = CompressedPageIndex.FromPageIndex (_uncompressedItems);
			if (_unusedItems == null && _uncompressedUnusedItems != null)
				_unusedItems = CompressedPageIndex.FromPageIndex (_uncompressedUnusedItems);


			if (_uncompressedItems == null && _items != null)
				_uncompressedItems = PageIndex.FromCompressedPageIndex (_items);
			if (_uncompressedUnusedItems == null && _unusedItems != null)
				_uncompressedUnusedItems = PageIndex.FromCompressedPageIndex (_unusedItems);
		}

		public static BlrtMediaPagePrototype GenerateUnused (BlrtMediaPagePrototype current, BlrtMediaPagePrototype past)
		{
			if (past == null) {
				return new BlrtMediaPagePrototype () {
					UncompressedItems = current.UncompressedItems,
					UncompressedHiddenItems = current.UncompressedHiddenItems,
				};
			}

			var pastList = ExtractUnique (
				new List<PageIndex> (past.UncompressedItems.Concat (past.UncompressedHiddenItems ?? new PageIndex [0])),
				new List<PageIndex> (current.UncompressedItems)
			);

			pastList = ExtractUnique (
				pastList,
				new List<PageIndex> (current.UncompressedHiddenItems)
			);
			pastList.AddRange (current.UncompressedHiddenItems);

			return new BlrtMediaPagePrototype () {
				UncompressedItems = current.UncompressedItems,
				UncompressedHiddenItems = pastList.ToArray (),
			};
		}

		static List<PageIndex> ExtractUnique (List<PageIndex> pastList, List<PageIndex> currentList)
		{

			for (int i = 0; i < pastList.Count; ++i) {

				int otherIndex = currentList.IndexOf (pastList [i]);

				if (otherIndex >= 0) {
					pastList.RemoveAt (i);
					currentList.RemoveAt (otherIndex);
					--i;
				}
			}

			return pastList;
		}


		public static BlrtMediaPagePrototype FromsString (string str)
		{
			if (!string.IsNullOrEmpty (str)) {

				try {

					return JsonConvert.DeserializeObject<BlrtMediaPagePrototype> (str);

				} catch {
				}

			}
			return null;
		}

		[Serializable]
		public class PageIndex : IEquatable<PageIndex>
		{
			public int mediaPage;
			public string objectId;


			public PageIndex ()
			{
				objectId = null;
				mediaPage = -1;
			}

			public PageIndex (string objectId, int mediaPage)
			{

				this.objectId = objectId;
				this.mediaPage = mediaPage;
			}

			public bool Equals (PageIndex other)
			{
				return other != null
					&& other.mediaPage == mediaPage
					&& other.objectId == objectId;
			}

			public static PageIndex [] FromCompressedPageIndex (CompressedPageIndex [] pages)
			{

				var uncompressedList = new List<PageIndex> ();
				for (int i = 0; i < pages.Length; ++i) {

					foreach (var index in BlrtUtil.PagesToList (pages [i].PageString))
						uncompressedList.Add (new PageIndex (pages [i].MediaId, index));
				}

				return uncompressedList.ToArray ();
			}
		}

		[Serializable]
		public class CompressedPageIndex
		{
			public string PageString;
			public string MediaId;

			public CompressedPageIndex ()
			{
				this.MediaId = null;
				PageString = null;
			}

			public CompressedPageIndex (string objectId, IEnumerable<int> pages)
			{

				this.MediaId = objectId;
				this.PageString = BlrtUtil.ListToPages (pages);

			}


			public static CompressedPageIndex [] FromPageIndex (PageIndex [] pages)
			{

				PageIndex last = null;

				var list = new List<int> ();
				var compressedList = new List<CompressedPageIndex> ();

				for (int i = 0; i < pages.Length; ++i) {


					if (last != null && !(last.objectId == pages [i].objectId && pages [i].mediaPage > last.mediaPage)) {

						compressedList.Add (new CompressedPageIndex (last.objectId, list));
						list.Clear ();
					}

					list.Add (pages [i].mediaPage);
					last = pages [i];
				}

				if (last != null) {
					compressedList.Add (new CompressedPageIndex (last.objectId, list));
				}

				return compressedList.ToArray ();
			}

			public object ToDictionary ()
			{
				return new Dictionary<string, object> () {
					{ "pageString",     PageString      },
					{ "mediaId",        MediaId         }
				};
			}
		}
	}
}