using System;

namespace BlrtData.Media
{
	public static class BlrtTemplateInfo
	{
		private const string NAME_KEY					= "name";
		private const string USABLE_KEY					= "usable";

		private const string IMAGE_KEY					= "image";
		private const string IMAGE_LOCAL_KEY			= "imageLocal";

		private const string THUMBNAIL_KEY				= "thumbnail";
		private const string THUMBNAIL_LOCAL_KEY		= "thumbnailLocal";
	}
}

