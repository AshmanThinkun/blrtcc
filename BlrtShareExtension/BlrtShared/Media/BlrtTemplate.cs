using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace BlrtData.Media
{

	public class BlrtTemplate{

		public string Name { get; set; }
		public int Id { get; set; }
		public int ThumbnailId { get; set; }
		public string LegacyUrl { get; set; }
		public string Extension { get; set; }

		[JsonIgnore]
		public bool Legacy { get { return LegacyUrl != null; } }

		[JsonIgnore]
		public string FilePath { get { return GetImagePath(this); } }

		private static BlrtTemplate[] _templateData = null;

		public static BlrtTemplate[] GetTemplateData()
		{
			if (_templateData == null) {
				string json = null;

				#if __IOS__
				json = File.ReadAllText ("templates/template-data.json");
				#else
				using (var reader = new StreamReader (BlrtApp.PlatformHelper.GetAssetFileStream("templates/template-data.json"))) {
					json = reader.ReadToEnd ();
				}
				#endif
				_templateData = JsonConvert.DeserializeObject<BlrtTemplate[]> (json);

				if (_templateData == null)
					_templateData = new BlrtTemplate[0];

				for(int i = 0; i < _templateData.Length; ++i)
					_templateData[i].Id = i;
			}
			return _templateData;
		}

		public static string GetImagePath(int id)
		{
			var d = GetTemplateData ();


			if (id > d.Length || id < -1)
				throw new Exception ("Boom!  Template Id not supported.");





			return GetImagePath (d [id]);
		}

		public static string GetImagePath(BlrtTemplate data)
		{
			return string.Format("templates/template-{0}.{1}",					data.Id, data.Extension);
		}

		public static string GetThumbnailPath(int id)
		{
			var d = GetTemplateData ();


			if (id > d.Length || id < -1)
				throw new Exception ("Boom!  Template Id not supported.");





			return GetThumbnailPath (d [id]);
		}

		public static string GetThumbnailPath(BlrtTemplate data)
		{
			return string.Format("templates/thumbnails/template-{0}-thumb.{1}",	data.ThumbnailId, data.Extension);
		}

		public static string GetTemplateName (int id)
		{
			var d = GetTemplateData ();


			if (id > d.Length || id < -1)
				throw new Exception ("Boom!  Template Id not supported.");





			return (d [id]).Name;
		}

		public static bool IsLocal (int templateId)
		{
			var data = GetTemplateData ();

			foreach (var d in data)
				if (d.Id == templateId)
					return !d.Legacy;
			return false;
		}






		public const string TemplatePrefix = "_template:";

		public static string AsTempleteId(int id){
			return string.Format("{0}{1}", TemplatePrefix, id);
		}


		public static bool IsTempleteId(string objectId){
			return Regex.IsMatch(objectId, string.Format("^{0}[0-9]+$", TemplatePrefix));
		}

		public static int GetTempleteId (string objectId) {

			string str = objectId.Remove (0, TemplatePrefix.Length);

			return Convert.ToInt32 (str);
		}
	}
}

