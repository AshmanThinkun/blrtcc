using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Parse;

namespace BlrtData
{
	public static class BlrtEventManager{


		public const string OverlayEventPrefix = "overlay+=";



		private static EventData _eventData;

		private static Task _waitingTask;
		private static bool _dirty;
		private static string _filepath;
		private static string _currentEvent;

		private static string Filepath {
			get{ return BlrtConstants.Directories.Default.GetDataPath("event-data.dat"); }
		}
		private static string OldFilepath {
			get{ return BlrtConstants.Directories.Default.GetDataPath(string.Format("event-data-{0}.dat", ParseUser.CurrentUser == null ? "blank" : ParseUser.CurrentUser.ObjectId)); }
		}



		public static bool ShouldAskForRating {
			get{ return _eventData.fireSignificantEvent; }
			set{
				if (value != _eventData.fireSignificantEvent) {
					_eventData.fireSignificantEvent = value;
					SaveData ();

					if(ParseUser.CurrentUser.Get<DateTime?>(BlrtUserHelper.GAVE_FEEDBACK_AT, null) == null)
						ParseUser.CurrentUser[BlrtUserHelper.GAVE_FEEDBACK_AT] = DateTime.Now;
				}

			}
		}

		public static void ResetCounters()
		{
			_eventData.Clear ();
			SaveData ();
		}

		public static void FailLastEvent ()
		{
			if (_currentEvent == null)
				return;

			var firedEvent = _eventData.events;

			if(firedEvent.Contains(_currentEvent))
				firedEvent.Remove (_currentEvent);
			_currentEvent = null;
		}

		public static bool HasFiredEvent (string name) {
			return !String.IsNullOrWhiteSpace (name) && _eventData.events.Contains (name);
		}

		//fire normal event
		public static void FireEvent(string name, Action first = null, Action other = null) {
			_currentEvent = name;
			var firedEvent = _eventData.events;

			if (!String.IsNullOrWhiteSpace(name)) {

				bool firstTime = firedEvent.Contains (name);

				if (_currentEvent != null) {
					if(firedEvent.Contains(name))
						firedEvent.Remove (name);
					firedEvent.Add (name);
				}

				if (firstTime) {
					LLogger.WriteEvent ("Event Manager", "firing event", name, string.Format("other: {0}", first == null));

					if (null != other)
						other ();			
				} else {
					LLogger.WriteEvent ("Event Manager", "firing event", name, string.Format("first: {0}", first == null));
					if (null != first)
						first ();
				}

				SaveData ();
			}

			_currentEvent = null;
		}

		// don't run checkRate if it's the first time the event is being run.
		public static void FireSignificantEvent(string name, int weight, Action first = null, Action other = null)
		{
			if (!String.IsNullOrEmpty(name)) {
				LLogger.WriteEvent ("Event Manager", "firing significant", name, string.Format("current: {0}, weight: {1}", _eventData.significantEventCounter, weight));
				_eventData.significantEventCounter += weight;
				FireEvent (name, first ?? CheckRate, other ?? CheckRate);
			}
		}

		public static void RemoveEventsStartingWith (string prefix)
		{
			for (int i = 0; i <  _eventData.events.Count; ++i) {
				if (_eventData.events [i].StartsWith (prefix)) {
					_eventData.events.RemoveAt (i);
					--i;
				}
			}
		}

		public static void Reload() {

			ForceSave ();
			_filepath = Filepath;

			LLogger.WriteEvent ("Event Manager", "loading data");

			try {
				if (File.Exists (_filepath)) {
					using (var reader = File.OpenRead(_filepath)) {
						_eventData = BlrtUtil.DeserializeFromStream<EventData> (reader);
						reader.Close ();
						return;
					}
				} else if (File.Exists (OldFilepath)) {
					using (var reader = File.OpenRead(_filepath)) {
						_eventData = BlrtUtil.DeserializeFromStream<EventData> (reader);
						reader.Close ();
						return;
					}
				}
			} catch {
			}
			_eventData = new EventData ();
		}


		//it will store the eventdata to local file every 10 seconds(if there is a store request);
		private static void SaveData()
		{
			_dirty = true;
			if (_waitingTask != null && !_waitingTask.IsCompleted)
				return;
			_waitingTask = Task.Run (async () => {
				await Task.Delay (2000);
				ForceSave();
				_waitingTask = null;			
			});
		}


		public static void ForceSave(){
			if(_dirty){
				LLogger.WriteEvent ("Event Manager", "saving data");
				_dirty = false;

				var stream = BlrtUtil.SerializeToStream (_eventData);
				BlrtUtil.StreamToFile (_filepath, stream);

				LLogger.WriteEvent ("Event Manager", "successfully saved data");
			}
		}



		private static void CheckRate()
		{
			LLogger.WriteEvent ("Event Manager", "should ask for rating");
			if (ParseUser.CurrentUser == null)
				return;


			int sigEvents 	= BlrtSettings.RateAppSignificantEvents;
			int days 		= BlrtSettings.RateAppDays;
			bool fireEvent = true && (sigEvents > 0 || days > 0);


			if (false 
				|| (ParseUser.CurrentUser.Get<DateTime?>(BlrtUserHelper.GAVE_FEEDBACK_AT, null) == null
					&& _eventData.fireSignificantEvent 
					&& fireEvent
					&& (_eventData.significantEventCounter >= sigEvents || sigEvents <= 0)
					&& (_eventData.createAt > DateTime.Now.AddDays(days) || days <= 0))) 
			{
				LLogger.WriteEvent ("Event Manager", "Rating ap event fired");

				ResetCounters ();
				BlrtManager.Responder.ShowFeedbackRatingDialog ();
			}
		}

		[System.Serializable]
		public class EventData {
			public List<string> events;

			public int significantEventCounter;
			public bool fireSignificantEvent;
			public DateTime createAt;


			public EventData()
			{
				fireSignificantEvent = true;
				events = new List<string>();

				Clear();
			}


			public void Clear(){

				significantEventCounter = 0;
				createAt = DateTime.Now;			
			}

			public bool IsEmpty{
				get{
					return significantEventCounter <= 0
						&& events.Count <= 0;
				}
			}
		}
	}


}

