using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BlrtData.Query
{
	public class BlrtQueryManager
	{
		private DateTime _infoLastUpdated;
		public void InvalidateSettings (){_infoLastUpdated = new DateTime ();}
		public bool ValidateSettings{ get { return DateTime.Now - BlrtSettings.SettingsUpdateFrequency < _infoLastUpdated; }}


		private List<BlrtQueryItem> _queryList;
		private Task _queryTask;

		private Task _waitingTask;

		public bool QueryRunning { get; private set; }


		public EventHandler<BlrtQueryItem> OnQueryAdded { get; set; }
		public EventHandler<BlrtQueryItem> OnQueryCompletion { get; set; }


		internal BlrtQueryManager()
		{
			_queryList = new List<BlrtQueryItem> ();

			Reset ();
		}

		public void Reset ()
		{
			TerminateQueries();
			lock (_queryList) {
				_queryList.Clear ();
			}
			InvalidateSettings();
		}


		#region query Logic

		public void TerminateQueries () {
			lock (_queryList) {
				var listCopy = new List<BlrtQueryItem> (_queryList);
				foreach (var item in listCopy) {
					item.Cancel ();
				}
				listCopy.Clear ();
			}
		}

		public void TryRunQuery()
		{
			Task.Factory.StartNew(
				() => {
					if (_queryTask != null && (_queryTask.IsCompleted || _queryTask.IsFaulted)) {
						QueryRunning = false;
						_queryTask = null;
					}

					if (!QueryRunning) {
						//make the query 
						_queryTask = RunManager ();
					}
				},
				CancellationToken.None,
				TaskCreationOptions.None,
				TaskScheduler.Default
			);
		}

		private async Task RunManager ()
		{
			if (QueryRunning) {
				BlrtUtil.Debug.ReportException (new Exception("Query is already running"));
				return;
			}

			QueryRunning = true;

			if (!ValidateSettings || BlrtSettings.MaintenanceMode || BlrtSettings.InvalidVersion) {
				BlrtSettingsQuery.RunQuery ();
			}

			SortAndCull ();

			LLogger.WriteEvent("Query Manager", "started");
			BlrtUtil.PushDownloadStack ();

			bool failed = false;

			bool dirty = true;

			while (dirty) {

				dirty = false;

				SortAndCull ();
				var target = _queryList.FirstOrDefault (q => q.Running);

				if (target != null) {
					dirty = true;
					await RunQuery (target);

					try {
						var c = OnQueryCompletion;
						if (c != null) c (this, target);
					} catch (Exception ex) {
						LLogger.WriteEvent ("Query Manager", "failed to run event", ex.ToString ());			
					}

					//if target query has failed, fail all other queries
					if (target.Failed && target is BlrtSettingsQuery) {
						failed = true;
						LLogger.WriteEvent ("Query Manager", "error");
						lock (_queryList) {
							for (int i = 0; i < _queryList.Count; ++i) {
								_queryList [i].Exception = new BlrtQueryFailedException (target);

								try {
									if (OnQueryCompletion != null)
										OnQueryCompletion (_queryList [i], target);
								} catch (Exception ex) {
									LLogger.WriteEvent ("Query Manager", "Failed to run event post query event.", ex.ToString ());			
								}
							}
						}
						break;
					}
				}
			}
			BlrtUtil.PopDownloadStack ();


			if (!failed) {
				LLogger.WriteEvent ("Query Manager", "finished");

				BlrtManager.RecalulateAppBadge ();
			}
			QueryRunning = false;

			if (_waitingTask == null || _waitingTask.IsCompleted || _waitingTask.IsCanceled || _waitingTask.IsFaulted) {

				_waitingTask = Task.Delay (1000 * 60)
					.ContinueWith ((Task t) => {
						TryRunQuery ();
					}
				);
			}
		}


		private async Task RunQuery(BlrtQueryItem item)
		{
			var start = DateTime.Now;
			try{
				LLogger.WriteEvent("Query", "started", item.GetType().ToString());

				// if the current query isnt the settings and the deivce isnt allowed to run queries, cancel this query
				if(!BlrtSettings.CanQuery && !(item is BlrtSettingsQuery)) {
					InvalidateSettings();

					if(BlrtSettings.MaintenanceMode)
						LLogger.WriteEvent("Query", "Cannot run: MaintenanceMode", item.GetType().ToString());
					if(BlrtSettings.InvalidVersion)
						LLogger.WriteEvent("Query", "Cannot run: InvalidVersion", item.GetType().ToString());


					throw new Exception("Cannot run Query at the moment");
				}

				if(item is BlrtSettingsQuery)	InvalidateSettings();


				//try to run the query 4 times, if it is successful break out of the loop
				for(int i = 0; i < 4 && !item.Done; ++i) {

					if(!BlrtUtil.IsOnline)			
						throw new System.Net.WebException();

					if (item.Exception != null)
						throw item.Exception;

					Exception e = null;
					try {
						await item.Action();

						break;

					} catch(Exception ex) {
						e = ex;
					}

					//if it failed, check if it was a connection problem, if so chekc the connect and try the query again
					if(!item.Done && e != null) {
						if(BlrtUtil.IsConnectionIssueException(e)){

							if(!await BlrtParseActions.HasConnectionProblem())
								LLogger.WriteEvent("Query Manager", "Failed, but will rerun", e.ToString());
							else
								throw e;

						} else {
							throw e;
						}
					}
				}

				if(item.Failed) throw item.Exception;
				if(item is BlrtSettingsQuery)	_infoLastUpdated = DateTime.Now;


				try{
					item.Finished = !item.Failed;
				} catch (Exception ex){
					LLogger.WriteEvent("Query Manager", "Failed to run event post query event.", ex.ToString());			
				}


				LLogger.WriteEvent("Query", "finished", item.GetType().ToString(), (DateTime.Now - start).TotalSeconds);

				item.LocalStorage.DataManager.Save ();

				BlrtUtil.SetConnectionHasProblem(false);

			} catch (Exception queryManagerRunQueryExcepiton){
				try{
					if(BlrtUtil.IsConnectionIssueException(queryManagerRunQueryExcepiton))
						BlrtUtil.SetConnectionHasProblem(true);

					item.Exception = queryManagerRunQueryExcepiton;
				} catch (Exception ex) {
					LLogger.WriteEvent("Query Manager", "Failed to run event post query event.", ex.ToString());
					BlrtUtil.Debug.ReportException (ex);
				}
					

				LLogger.WriteEvent("Query", "error", item.GetType().ToString(), queryManagerRunQueryExcepiton.ToString());
				BlrtUtil.Debug.ReportException (queryManagerRunQueryExcepiton);
			}



		}
		#endregion





		#region query list Management
		public void SortAndCull()
		{
			lock (_queryList) {
				_queryList.RemoveAll (q => q.Done);
				_queryList.Sort ();
			}
		}


		public BlrtQueryItem AddQuery( BlrtQueryItem item )
		{
			if (item == null)
				return null;

			SortAndCull ();

			lock (_queryList) {
				foreach (var q in _queryList) {
					if (!q.Done && q.Equals (item)) {
						q.UpdateLastRequest ();
						TryRunQuery ();
						return q;
					}	
				}
			}

			ForceAddQuery (item);

			return item;
		}

		public bool HasQuery (BlrtQueryItem item)
		{
			if (item == null)
				return false;

			SortAndCull ();

			lock (_queryList) {
				foreach (var q in _queryList) {
					if (!q.Done && q.Equals(item)) {
						return true;
					}			
				}
			}

			return false;
		}


		internal void ForceAddQuery(BlrtQueryItem item )
		{
			lock (_queryList) {
				_queryList.Add (item);
			}
			item.UpdateLastRequest ();
			TryRunQuery ();

			var c = OnQueryAdded;
			if (c != null) c (this, item);
		}
		#endregion
	}
}

