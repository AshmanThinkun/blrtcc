using System;

namespace BlrtData.Query
{
	public class BlrtQueryFailedException : Exception
	{
		public BlrtQueryItem Cause { get; private set;}


		public BlrtQueryFailedException (BlrtQueryItem cause) : base ()
		{
			Cause = cause;
		}
	}
}

