using System;
using System.Linq;
using System.Collections.Generic;
using Parse;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BlrtData.Query
{
	public static class BlrtParseActions
	{

		public static Task<bool> HasConnectionProblem(int milliseconds = 15000) {
			return HasConnectionProblem (new CancellationTokenSource (milliseconds).Token);
		}

		public static async Task<bool> HasConnectionProblem(CancellationToken token)
		{
			try{
				await BlrtUtil.BetterParseCallFunctionAsync<object> (
					"connectionCheck", 
					new Dictionary<string, object> (), 
					token
				);
				return false;
			} catch (Exception e) {
				return true;
			}
		}

		static CancellationTokenSource _cancellationTokenSource;

		public static async Task<object> SendConversationToContactInfo(string objectId, IEnumerable<Contacts.ConversationNonExistingContact> contacts, string message)
		{
			const string CLOUD_FUNCTION = "createRelation";

			try{

				var recipents = new List<Dictionary<string, object>>();

				foreach(var c in contacts){

					recipents.Add(new Dictionary<string, object>(){
						{"name", c.Contact.UsableName },
						{"queryType", c.SelectedInfo.ParseContactType},
						{"queryValue", c.SelectedInfo.ContactValue.ToLower() }
					});
				}


				var parameters = new Dictionary<string, object>();
				parameters.Add("conversationId", objectId);
				parameters.Add("queries", recipents);

				if(!string.IsNullOrWhiteSpace(message)){
					parameters.Add("message", message);
				}

				var token = new CancellationTokenSource(TimeSpan.FromSeconds(15)).Token;
				var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (CLOUD_FUNCTION, parameters,token);	

				if(result is string) {
					return JsonConvert.DeserializeObject<BlrtData.Contents.Event.BlrtAddEventData>(result as string);
				}

				return null;
			} catch (Exception e){
				LLogger.WriteLine ("Failed to run cloud code \"{0}\", with the exception: {1}.", CLOUD_FUNCTION, e.ToString());
				return e;
			}
		}


		public static void Cancel () {
			if (_cancellationTokenSource != null)
				_cancellationTokenSource.Cancel ();
		}


		/// <summary>
		/// Fetchs the user and check relations.
		/// </summary>
		/// <returns>true if findPendingRelation has been run</returns>
		/// <param name="token">Token.</param>
		public static async Task<bool> FetchUserAndCheckRelations(CancellationToken token)
		{
			const string CLOUD_FUNCTION = "findPendingRelation";

			if (ParseUser.CurrentUser == null)
				return false;


			try {
				await ParseUser.CurrentUser.BetterFetchAsync(token);
				//suspended user should not be able to login
				if(!string.IsNullOrWhiteSpace(BlrtUserHelper.AccountStatus)){
					Xamarin.Forms.Device.BeginInvokeOnMainThread (
						() => {
							PushHandler.ForceUserLogout (ExecutionPipeline.Executor.System (), BlrtUserHelper.AccountStatus, BlrtPushState.InApp);
						}
					);
				}

				if (ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.RelationsDirtyKey, false)) {

					await BlrtUtil.BetterParseCallFunctionAsync<object> (
						CLOUD_FUNCTION, 
						new Dictionary<string, object> (), 
						token
					);
					return true;
				}
			} catch(Exception e){
				LLogger.WriteLine ("Failed to run cloud code \"{0}\", with the exception: {1}.", CLOUD_FUNCTION, e.ToString());
				throw;
			}
			return false;
		}

		public static Task FindUsersQuery(string[] userIds) {
			return FindUsersQuery (userIds, new CancellationTokenSource (TimeSpan.FromSeconds (15)).Token);
		}

		public static async Task FindUsersQuery(IEnumerable<string> userIds, CancellationToken token)
		{
			var ids = userIds.Concat(BlrtManager.DataManagers.Default.GetUnkownUserId (true)).Distinct().ToList();

			if(ids.Count() == 0)
				return;

			var encrypted = await Encrypt (ids);

			var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
				"findUserInfo",
				new Dictionary<string, object>(){
					{ "userIds", encrypted },
					{ "version", 2}
				},
				token
			);

			IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType> resultUsers = null;
			try{
				var decrypted = await BlrtData.Encryptor.AES256Cipher.DecryptString(result as string);
				resultUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType>> (decrypted);
			}catch{}
			
			var _contactManager = Contacts.BlrtContactManager.SharedInstanced;

			var contactListToAdd = new List<BlrtData.Contacts.BlrtContact> ();

			if (resultUsers != null) {
				foreach (var d in resultUsers) {
					if (d != null) {
						string id = d.id;
						var blrtUser = BlrtUser.FromParseObject (d);

						BlrtManager.DataManagers.Default.AddUser (blrtUser);
						ids.Remove (blrtUser.ObjectId);

						contactListToAdd.Add (
							Contacts.BlrtContact.FromBlrtUser (blrtUser)
						);
					}
				}
			}

			_contactManager.Add (contactListToAdd);
			_contactManager.Save ();

			foreach (var u in ids) {
				var blrtUser = BlrtUser.CreateBlank (u);
				BlrtManager.DataManagers.Default.AddUser(blrtUser);
			}
		}

		//put it here for now
		static async Task<string> Encrypt(Object target){
			var json = JsonConvert.SerializeObject(target);
			//encrypt string
			return await Encryptor.AES256Cipher.EncryptString(json);
		}

		public static async Task<IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType>> FindUsersFromContacts(IEnumerable<IDictionary<string,string>> contacts, CancellationToken token)
		{
			if (contacts == null || contacts.Count() < 1)
				return null;

			var encrypted = await Encrypt (contacts);

			var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
				"findUserInfoFromContacts", 
				new Dictionary<string, object>(){
					{ "contacts", encrypted},
					{ "version", 2}
				},
				token
			);

			IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType> resultUsers = null;
			try{
				var decrypted = await BlrtData.Encryptor.AES256Cipher.DecryptString(result as string);
				resultUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<BlrtData.BlrtUser.BlrtUserReturnType>> (decrypted);
			}catch{}

			if (resultUsers == null)
				return null;
		
			var _contactManager = Contacts.BlrtContactManager.SharedInstanced;
			var contactListToAdd = new List<BlrtData.Contacts.BlrtContact> ();

			foreach(var d in resultUsers)
			{
				if(d != null) {
					var blrtUser 	= BlrtUser.FromParseObject(d);
					BlrtManager.DataManagers.Default.AddUser(blrtUser);
					contactListToAdd.Add (
						Contacts.BlrtContact.FromBlrtUser (blrtUser)
					);
				}
			}
			_contactManager.Add (contactListToAdd);
			_contactManager.Save ();
			return resultUsers;
		}

		public static Task GetContactDetails () {
			return GetContactDetails (new CancellationTokenSource (TimeSpan.FromSeconds (15)).Token);
		}

		public static async Task GetContactDetails (CancellationToken token)
		{
			if(ParseUser.CurrentUser == null)
				return;

			IEnumerable<object> result = null;

			result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
				"getContactDetails", 
				new Dictionary<string, object>(),
				token
			) as IEnumerable<object>;

			var details = new List<BlrtUserContactDetail> ();


			foreach (var contact in result) {
				var d = contact as Dictionary<string, object>;

				if (d != null) {
					if (!(d.ContainsKey ("id") && d.ContainsKey ("user") && d.ContainsKey ("type") && d.ContainsKey ("value")))
						continue;

					string id;
					string user;
					string type;
					string value;
					bool verified;
					bool placeholder;

					try {
						id 				= d ["id"].ToString ();
						user 			= d ["user"].ToString ();
						type			= d ["type"].ToString ();
						value 			= d ["value"].ToString ();
						verified 		= d.ContainsKey ("verified") && Convert.ToBoolean (d ["verified"]);
						placeholder 	= d.ContainsKey ("placeholder") && Convert.ToBoolean (d ["placeholder"]);
					} catch (Exception) {
						continue;
					}

					var record	 	= new BlrtUserContactDetail (id) {
						User 		= user,
						Type 		= BlrtUserContactDetail.TypeFromString (type),
						Value 		= value,
						Verified 	= verified,
						Placeholder = placeholder
					};

					details.Add (record);
				}
			}

			BlrtManager.DataManagers.Default.SetUserContactDetails (details);
		}

		public static async Task<bool?> CheckPassword (string password, bool animated = true) {
			return await CheckPassword (password, animated, new CancellationTokenSource (TimeSpan.FromSeconds (15)).Token);
		}

		/// <summary>
		/// Checks that the provided password matches the password of the logged in user.
		/// </summary>
		/// <returns>True if the password matched. False if it was invalid. Null if checking failed.</returns>
		public static async Task<bool?> CheckPassword (string password, bool animated, CancellationToken token) {
			BlrtManager.Responder.PushLoading (animated);

			bool? result;
			try {
				result = await BlrtUserHelper.CheckPassword(password, token);
			} catch {
				result = null;
			}

			BlrtManager.Responder.PopLoading (animated);

			return result;
		}

		public static async Task ForceGetSettings(CancellationToken token)
		{
			const string CLOUD_FUNCTION = "getSettings";

			var worked = false;
			BlrtUtil.PushDownloadStack ();

			var oldSettings = BlrtManager.settingManager;

			try {
				var param = new Dictionary<string, object>() {
					{"device", BlrtManager.Responder.DeviceDetails.ToDictionary() }
				};

				var currentUser = ParseUser.CurrentUser;

				var str = await BlrtUtil.BetterParseCallFunctionAsync<string> (CLOUD_FUNCTION, param, token);

				if(currentUser != ParseUser.CurrentUser)
					throw new Exception("User changed during the setting query running.");


				worked = BlrtSettingsManager.FromStringOrCache(str, out BlrtManager.settingManager);

				if(worked){
					BlrtManager.Settings.lastInvalidVersion 
						= BlrtManager.Settings.invalidVersion ? BlrtManager.Responder.DeviceDetails.AppVersion : null;				
				}


				if(BlrtSettings.MaintenanceMode)
					BlrtManager.Responder.ActivateLockout(BlrtLockoutMode.Maintance, true);
				else
					BlrtManager.Responder.DeactivateLockout(BlrtLockoutMode.Maintance, true);


				if(BlrtSettings.InvalidVersion)
					BlrtManager.Responder.ActivateLockout(BlrtLockoutMode.InvalidVersion, true);
				else
					BlrtManager.Responder.DeactivateLockout(BlrtLockoutMode.InvalidVersion, true);


				/*
				if(BlrtSettings.Warning != null){
					BlrtManager.Responder.ShowNotification(
						BlrtSettings.Warning, true
					);
				}
				*/




			} catch (Exception e) {
 				LLogger.WriteLine ("Failed to run cloud code \"{0}\", with the exception: {1}.", CLOUD_FUNCTION, e.ToString());
				BlrtManager.settingManager = BlrtSettingsManager.FromCache();
				throw e;
			} finally {
				BlrtUtil.PopDownloadStack ();
			}




			if(!worked)
				throw new Exception("Failed to get settings");


			// POST GET SETTINGS ACTIONS
			BlrtUserHelper.InitilizeUser ();	

			BlrtSettingsManager.CompareSettings (BlrtManager.settingManager, oldSettings);
			BlrtGlobalEventManager.GotSettings (BlrtManager.settingManager, new EventArgs ());
		}
	}
}

