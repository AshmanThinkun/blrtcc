using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlrtData.Query
{
	public interface IBlrtJobItem : IEquatable<IBlrtJobItem>
	{
		int Priority { get; }


		Task RunQuery(CancellationToken token);
	}
}

