using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace BlrtData.Query
{
	public class BlrtQueryJobItem : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(IBlrtJobItem jobItem){
			var output =  BlrtManager.Query.AddQuery (new BlrtQueryJobItem (jobItem));

			if (!output.Running) output.Run ();
			return output;
		}


		public override int Priority { get { return _jobItem.Priority; } }

		IBlrtJobItem _jobItem;

		private BlrtQueryJobItem (IBlrtJobItem jobItem)
		{
			_jobItem = jobItem;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			var o = other as BlrtQueryJobItem;

			return (o != null) && (_jobItem.Equals(o._jobItem));
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			await _jobItem.RunQuery (CancellationToken);
		}
	}

}
