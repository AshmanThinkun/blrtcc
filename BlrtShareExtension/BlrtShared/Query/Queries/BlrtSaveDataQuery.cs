using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public class BlrtSaveDataQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(ILocalStorageParameter localStorage = null) {
			var output =  BlrtManager.Query.AddQuery (new BlrtSaveDataQuery (localStorage));


			if (!output.Running) output.Run ();
			return output;
		}


		public override int Priority { get { return 12; } }

		DateTime _started;

		private BlrtSaveDataQuery (ILocalStorageParameter localStorage = null) : base (localStorage)
		{
		}

		public override bool Equals (BlrtQueryItem other)
		{
			var o = other as BlrtSaveDataQuery;

			return (o != null) && (_started == o._started);
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			if (ParseUser.CurrentUser == null)
				throw new NullReferenceException ();


			_started = DateTime.Now;
			var saveables = LocalStorage.DataManager.GetNeedSave ();



			if ((ParseUser.CurrentUser != null && ParseUser.CurrentUser.IsDirty) || saveables.Any()) {
				saveables = saveables.Concat (new []{ await BlrtUserHelper.GetUserForSave()});
			}


			if (saveables.Any()) {
				try {
					await saveables.BetterSaveAllAsync (CreateTokenWithTimeout (15 * 1000));
					await Fetcher (saveables);
					LocalStorage.DataManager.AddParse (saveables);
				} catch (Exception e) {
					throw e;
				}

			}

			LocalStorage.DataManager.Save ();
		}


		//fetch in bulk
		async Task Fetcher (IEnumerable<ParseObject> saveables)
		{
			var ts = new List<Task> ();
			foreach (var c in saveables) {
				ts.Add(c.BetterFetchAsync (this.CancellationToken));
			}

			await Task.WhenAll (ts);
		}
	}
}

