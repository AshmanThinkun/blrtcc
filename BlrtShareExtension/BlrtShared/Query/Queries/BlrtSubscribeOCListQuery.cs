﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Parse;

namespace BlrtData.Query
{
	public class BlrtSubscribeOCListQuery : BlrtQueryItem
	{
		#region implemented abstract members of BlrtQueryItem

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;
			if (ParseUser.CurrentUser == null) {
				return;
			}
			if (null != UserOrganisationRelation.Current 
			 && null != UserOrganisationRelation.Current.SubscribedOCListIds
			 && UserOrganisationRelation.Current.SubscribedOCListIds.Contains (_listId))
			{
				return;
			}

			try {
				var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
					"subscribeOCList",
					new Dictionary<string, object> () {
						{ "listId", BlrtEncode.EncodeId (_listId) },
					},
					CreateTokenWithTimeout (15000)
				);
				await UserOrganisationRelation.UpdateCurrent ();
			} catch (Exception exex) {
				LLogger.WriteEvent ("BlrtSubscribeOCListQuery", "Query", "Exception", exex.ToString ());
			}
		}

		public override bool Equals (BlrtQueryItem other)
		{
			var otherSubscribeQuery = other as BlrtSubscribeOCListQuery;
			if (null != otherSubscribeQuery) {
				return this._listId == otherSubscribeQuery._listId;
			}
			return false;
		}

		public override int Priority {
			get {
				return 30;
			}
		}

		#endregion

		string _listId;
		private BlrtSubscribeOCListQuery (string ocListId)
		{
			_listId = ocListId;
		}

		public static BlrtQueryItem RunQuery(string ocListId) {
			if (string.IsNullOrWhiteSpace (ocListId)) {
				throw new ArgumentNullException ("ocListId");
			}
			var output = BlrtManager.Query.AddQuery (new BlrtSubscribeOCListQuery (ocListId));
			if (!output.Running) 
				output.Run ();
			return output;
		}
	}
}

