using System.Threading.Tasks;

namespace BlrtData.Query
{
	public class BlrtUserQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(){
			var output =  BlrtManager.Query.AddQuery (new BlrtUserQuery ());


			if (!output.Running) output.Run ();
			return output;
		}



		public override int Priority { get { return 8; } }



		internal BlrtUserQuery ()
		{
		}

		public override bool Equals (BlrtQueryItem other)
		{
			return (other is BlrtUserQuery);
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			await BlrtParseActions.FindUsersQuery (LocalStorage.DataManager.GetUnkownUserId(false), CreateTokenWithTimeout(15000));
		}
	}
}

