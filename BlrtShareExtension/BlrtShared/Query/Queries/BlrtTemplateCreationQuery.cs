using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;
using BlrtAPI;


namespace BlrtData.Query
{
	public class BlrtTemplateCreationQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(){
			var output =  BlrtManager.Query.AddQuery (new BlrtTemplateCreationQuery ());


			if (!output.Running) output.Run ();
			return output;
		}


		public override int Priority { get { return 999; } }

		private BlrtTemplateCreationQuery ()
		{
		}

		public override bool Equals (BlrtQueryItem other)
		{
			return other == this;
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			if (ParseUser.CurrentUser == null)
				return;

			var cu = ParseUser.CurrentUser;

			var queued = cu.Get<IList<string>>(BlrtUserHelper.QueuedConvoTemplates, null);

			if (queued != null && queued.Count > 0) {

				await BlrtUtil.BetterParseCallFunctionAsync<object> (
					"processQueuedConvoTemplates",
					new Dictionary<string, object> (),
					CreateTokenWithTimeout (30000)
				);

				BlrtManager.Query.AddQuery (new BlrtTemplateCreationQuery ());
			} else if (cu.Get<int> ("userLoginActions", 0) > 0) {

				var api = new APITimelyActionRunByCategory (
					new APITimelyActionRunByCategory.Parameters () {
						Category = "userLogin"
					}
				);

				if (!await api.Run (CreateTokenWithTimeout (15000)))
					throw api.Exception;

				if(!api.Response.Success)
					throw new Exception(api.Response.Error.Message);

				BlrtManager.Query.AddQuery (new BlrtTemplateCreationQuery ());
			}
		}
	}
}

