﻿using System;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.Query
{
	public class BlrtOCListQuery : BlrtQueryItem
	{
		#region implemented abstract members of BlrtQueryItem

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			try {
				var theList = new ParseObject(OCList.CLASS_NAME);
				theList.ObjectId = _listId;
				theList = await theList.BetterFetchAsync (CreateTokenWithTimeout(15000));

				if ((theList.ClassName == OCList.CLASS_NAME) && (theList.ObjectId == _listId)) {
					LocalStorage.DataManager.AddParse (theList);
				} else {
					throw new NotImplementedException ("OCList's class name or object id get changed after fetch");
				}
			} catch (Exception exex) {
				LLogger.WriteEvent ("BlrtOCListQuery", "Query", "Exception", exex.ToString ());
			}
		}

		public override bool Equals (BlrtQueryItem other)
		{
			var otherOCListQuery = other as BlrtOCListQuery;
			if (null != otherOCListQuery) {
				return this._listId == otherOCListQuery._listId;
			}
			return false;
		}

		public override int Priority {
			get {
				return 12;
			}
		}

		#endregion

		string _listId;
		private BlrtOCListQuery (string ocListId)
		{
			_listId = ocListId;
		}

		public static BlrtQueryItem RunQuery(string ocListId) {
			if (string.IsNullOrWhiteSpace (ocListId)) {
				throw new ArgumentNullException ("ocListId");
			}
			var output = BlrtManager.Query.AddQuery (new BlrtOCListQuery (ocListId));
			if (!output.Running) 
				output.Run ();
			return output;
		}
	}
}

