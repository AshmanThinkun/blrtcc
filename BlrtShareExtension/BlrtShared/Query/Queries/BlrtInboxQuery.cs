using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public enum BlrtInboxQueryType
	{
		Inbox,
		Archive
	}


	public class BlrtInboxQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery (BlrtInboxQueryType type, int skip = 0)
		{
			var output = BlrtManager.Query.AddQuery (new BlrtInboxQuery (type) {
				_skip = skip
			});


			if (!output.Running)
				output.Run ();
			return output;
		}

		public static bool HasQuery (BlrtInboxQueryType type, int skip = 0)
		{
			return BlrtManager.Query.HasQuery (new BlrtInboxQuery (type) {
				_skip = skip
			});
		}

		public override int Priority { get { return 10; } }

#if SUPPORT
		public const int ItemsPerQuery = 100;
#else
		public const int ItemsPerQuery = 30;
#endif

		private int _skip;
		private BlrtInboxQueryType _type;


		private BlrtInboxQuery (BlrtInboxQueryType type)
		{
			_type = type;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is BlrtInboxQuery) {
				var o = other as BlrtInboxQuery;

				return (o._type == _type) && (o._skip == _skip);
			}
			return false;
		}

		public static bool IsEmptyOrNull (Array array)
		{
			return (array == null) || (array.Length == 0);
		}


		internal override Task Action ()
		{
			return run ();
		}


		/// <summary>
		/// Run inbox query, this query might fail by the first time as userFetchQueryT runs concurrently, see FetchUserAndCheckRelations for details
		/// </summary>
		/// <param name="reFetchMailBox">If set to <c>true</c>, then we should not FetchUserAndCheckRelations again</param>
		private async Task run (bool reFetchMailBox = false)
		{
			if (Exception != null)
				throw Exception;

			var d = DateTime.Now;

			ParseQuery<ParseObject> mainQuery, flagedItemQuery = null;
			Task<IEnumerable<ParseObject>> mainQueryT, flagedItemQueryT = null;
			Task<bool> userFetchQueryT = null;

			// <param name="reFetchMailBox">If set to <c>true</c>, then we should not FetchUserAndCheckRelations again</param>
			if (!reFetchMailBox)
				userFetchQueryT = BlrtParseActions.FetchUserAndCheckRelations (CreateTokenWithTimeout (15000));

			try {
				mainQuery = ParseObject.GetQuery (ConversationUserRelation.CLASS_NAME)
					.WhereEqualTo (ConversationUserRelation.USER_KEY, ParseUser.CurrentUser)
					.WhereEqualTo (BlrtBase.DELETED_AT_KEY, null)
					.WhereNotEqualTo (ConversationUserRelation.FLAGGED_KEY, true);

				// fileter by query type
				switch (_type) {
				case BlrtInboxQueryType.Inbox:
					mainQuery = mainQuery.WhereNotEqualTo (ConversationUserRelation.ARCHIVED_KEY, true);
					break;
				case BlrtInboxQueryType.Archive:
					mainQuery = mainQuery.WhereEqualTo (ConversationUserRelation.ARCHIVED_KEY, true);
					break;
				}

				// we only fetch 30 items each main query
				mainQuery = mainQuery.Include (ConversationUserRelation.CONVERSATION_KEY)
					.Include (string.Format ("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.LAST_MEDIA_KEY))
#if __IOS__
					.Include (string.Format ("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.READONLY_KEY))
#endif
					.OrderByDescending (ConversationUserRelation.CONVERSATION_CONTENTUPDATE_KEY)
					.Limit (ItemsPerQuery)
					.Skip (Math.Max (0, _skip));
				mainQueryT = mainQuery.BetterFindAsync (CreateTokenWithTimeout (15000));

				// get flaged items
				if (_skip <= 0) {
					flagedItemQuery = ParseObject.GetQuery (ConversationUserRelation.CLASS_NAME)
						.WhereEqualTo (ConversationUserRelation.USER_KEY, ParseUser.CurrentUser)
						.WhereEqualTo (BlrtBase.DELETED_AT_KEY, null)
						.WhereEqualTo (ConversationUserRelation.FLAGGED_KEY, true);

					//fileter by query type
					switch (_type) {
						case BlrtInboxQueryType.Inbox:
							flagedItemQuery = flagedItemQuery.WhereNotEqualTo (ConversationUserRelation.ARCHIVED_KEY, true);
							break;
						case BlrtInboxQueryType.Archive:
							flagedItemQuery = flagedItemQuery.WhereEqualTo (ConversationUserRelation.ARCHIVED_KEY, true);
							break;
					}

					//we need to find all flagged item
					flagedItemQuery = flagedItemQuery.Include (ConversationUserRelation.CONVERSATION_KEY)
						.Include (string.Format ("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.LAST_MEDIA_KEY))
                        .Include (string.Format ("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.UNUSED_MEDIA_KEY))
						.OrderByDescending (ConversationUserRelation.CONVERSATION_CONTENTUPDATE_KEY);
					flagedItemQueryT = flagedItemQuery.BetterFindAsync (CreateTokenWithTimeout (15000));
				}

				var results = new List<ParseObject> ();
				var tasks = new List<Task> ();

				tasks.Add (mainQueryT);

				if (flagedItemQueryT != null)
					tasks.Add (flagedItemQueryT);

				if (userFetchQueryT != null)
					tasks.Add (userFetchQueryT);

				await Task.WhenAll (tasks);

				//re run mainQuery and flagedItemQuery if user info dirty.
				if (userFetchQueryT != null && userFetchQueryT.Result && !reFetchMailBox) {
					await run (true);
					return;
				}

				if (flagedItemQueryT != null) {
					results.AddRange (flagedItemQueryT.Result);
				}
				results.AddRange (mainQueryT.Result);

				//get rid of any incomplete results locally (much faster, and a user should never have more than 5)
				int incompleteItemCount = 0;
				results = results.Where ((ParseObject arg) => {
					var c = arg.Get<ParseObject> (ConversationUserRelation.CONVERSATION_KEY, null);
					var complete = (c != null && !c.Get<bool> ("incomplete", false));
					if (!complete) {
						incompleteItemCount++;
					}
					return complete;
				}).ToList ();
				LocalStorage.DataManager.AddParse (results);
				if (incompleteItemCount > 0) {
					var page = _skip / ItemsPerQuery;
					var mailboxFilter = BlrtData.Models.Mailbox.MailboxFilter.CurrentInstance;
					if (mailboxFilter != null && (
					       (_type == BlrtInboxQueryType.Inbox && mailboxFilter is BlrtData.Models.Mailbox.MailboxInboxFilter)
					       || (_type == BlrtInboxQueryType.Archive && mailboxFilter is BlrtData.Models.Mailbox.MailboxArchiveFilter)
					   )) {
						mailboxFilter.IncompleteItemCountDic [page] = incompleteItemCount;
					}
				}


				//if some reason the conversation, this fetch the conversation
				foreach (var result in results) {
					if (!result.ContainsKey (ConversationUserRelation.CONVERSATION_KEY)
					    || result.Get<ParseObject> (ConversationUserRelation.CONVERSATION_KEY, null) == null) {
						BlrtMarkConversationDirtyQuery.RunQuery (_type, result.ObjectId);
					}
				}
				await BlrtParseActions.FindUsersQuery (
					new string[]{ }, 
					CreateTokenWithTimeout (15000)
				);

				BlrtConversationMetaQuery.RunQuery ();
				BlrtPerrmissionQuery.RunQuery (null, LocalStorage);
				BlrtRelationFetchItemQuery.RunQuery (LocalStorage);
				BlrtManager.RecalulateAppBadge ();
			} catch (Exception ex) { // TODO: Parse bug here: parse will throw null reference bug if the internet is down; change this part of code when this parse bug is fixed
#if DEBUG
				Console.WriteLine ("========ex: " + ex.Message);
#endif
			}
		}
	}
}

