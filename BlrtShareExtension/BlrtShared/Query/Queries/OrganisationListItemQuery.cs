﻿using System;
using Parse;
using System.Threading.Tasks;
using System.Linq;

namespace BlrtData.Query
{
	public class OrganisationListItemQuery: BlrtQueryItem
	{
		public bool HasContent;
		#region implemented abstract members of BlrtQueryItem

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			try{
				var mainQuery = ParseObject.GetQuery (OCListItem.CLASS_NAME)
					.WhereEqualTo ("oclist", new ParseObject ("OCList"){ ObjectId = this._listId })
					.WhereNotContainedIn ("objectId", this._ignoreItems)
					.OrderByDescending ("index")
					.Limit(20)
					.Include ("blrt");

				var results = await mainQuery.BetterFindAsync(CreateTokenWithTimeout(15000));
				if(results!=null && results.Count()>0)
					HasContent = true;

				LocalStorage.DataManager.AddParse (results);
				await BlrtParseActions.FindUsersQuery (
					new string[]{}, 
					CreateTokenWithTimeout(15000)
				);
			}catch(Exception xxsdfd){
				LLogger.WriteEvent ("OrganisationListItemQuery", "Query", "Exception", xxsdfd.ToString ());
				if(xxsdfd is TaskCanceledException){
					throw xxsdfd;
				}
			}
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is OrganisationListItemQuery) {
				var o = other as OrganisationListItemQuery;

				if (o._listId != this._listId)
					return false;

				if (o._ignoreItems.Length != this._ignoreItems.Length)
					return false;

				for(int i = 0 ; i< _ignoreItems.Length; i++){
					if (o._ignoreItems [i] != _ignoreItems [i])
						return false;
				}
				return true;
			
			} else {
				return false;
			}
		}

		public override int Priority {
			get {
				return 11;
			}
		}

		#endregion


		string _listId;
		string[] _ignoreItems;
		private OrganisationListItemQuery (string OClistID, string[] ignoreItems)
		{
			this._listId = OClistID;

			if (ignoreItems == null)
				ignoreItems = new string[]{ };
			this._ignoreItems = ignoreItems;
		}


		public static BlrtQueryItem RunQuery(string OClistID, string[] ignoreItems=null) {
			var output =  BlrtManager.Query.AddQuery (new OrganisationListItemQuery (OClistID, ignoreItems));
			if (!output.Running) 
				output.Run ();
			return output;
		}
	}
}

