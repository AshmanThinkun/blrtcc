using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.Query
{
	public class BlrtSettingsQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(){
			var output =  BlrtManager.Query.AddQuery (new BlrtSettingsQuery ());


			if (!output.Running) output.Run ();
			return output;
		}


		public override int Priority { get { return 999999999; } }



		ParseUser _parseUser;


		private BlrtSettingsQuery ()
		{
		}

		public override bool Equals (BlrtQueryItem other)
		{
			return (other is BlrtSettingsQuery)
				&& (Running || _parseUser == ParseUser.CurrentUser);
		}


		internal override async Task Action ()
		{
			Task userFetchT = null, getSettingsT = null;
			_parseUser = ParseUser.CurrentUser;

			if (_parseUser != null) {
				userFetchT = BlrtParseActions.FetchUserAndCheckRelations (CreateTokenWithTimeout(15000));
			}

			getSettingsT = BlrtParseActions.ForceGetSettings (CreateTokenWithTimeout (15000)); 

			var ts = new List<Task> ();
			ts.Add (getSettingsT);
			if(userFetchT!=null)
				ts.Add (userFetchT);
			await Task.WhenAll (ts);

			if (null != ParseUser.CurrentUser)
				BlrtTemplateCreationQuery.RunQuery ();
		}
	}
}

