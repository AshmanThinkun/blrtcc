using System;
using System.Linq;
using System.Threading.Tasks;
using Parse;
using BlrtAPI;

namespace BlrtData.Query
{
	public class BlrtUserGetTipBannersQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(){
			var output =  BlrtManager.Query.AddQuery (new BlrtUserGetTipBannersQuery ());

			if (!output.Running) output.Run ();
			return output;
		}


		public override int Priority { get { return 1; } }

		private BlrtUserGetTipBannersQuery ()
		{
		}

		public override bool Equals (BlrtQueryItem other)
		{
			return other == this;
		}

		#if __IOS__
		const string platform = "ios";
		#else
		const string platform = "android";
		#endif

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;



			var api = new APIUserGetTipBanners (
				new APIUserGetTipBanners.Parameters ()
			);

			if (!await api.Run (CreateTokenWithTimeout (15000)))
				throw api.Exception;

			if(!api.Response.Success)
				throw new Exception(api.Response.Error.Message);

			BlrtData.TipBannerModel.BannerList = (
				from tip in api.Response.Data
				where (null == tip.Filter || tip.Filter.Length <= 0 || Array.IndexOf (tip.Filter, platform) >= 0)
				select tip
			).ToList ();
		}
	}
}

