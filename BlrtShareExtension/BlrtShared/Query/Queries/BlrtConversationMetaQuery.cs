using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.Query
{
	public class BlrtConversationMetaQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(IEnumerable<ConversationUserRelation> relations = null){
			var r = relations ?? new ConversationUserRelation[0];
			var output =  BlrtManager.Query.AddQuery (new BlrtConversationMetaQuery (r, GetLocalStorageParameter(r)));


			if (!output.Running) 
				output.Run ();
			return output;
		}

		public static BlrtQueryItem RunQuery(ConversationUserRelation relation){
			var r = new ConversationUserRelation[]{relation};
			return RunQuery (r);
		}


		public static bool HasQuery (IEnumerable<ConversationUserRelation> relation = null) {

			var r = new BlrtConversationMetaQuery(relation, GetLocalStorageParameter(relation));
			return BlrtManager.Query.HasQuery (r);
		}

		public static bool HasQuery (ConversationUserRelation relation){

			return  HasQuery (new []{ relation });
		}

		public override int Priority { get { return _base.Any() ? 15 : 5; } }


		private IEnumerable<ConversationUserRelation> _base;

		private IEnumerable<ConversationUserRelation> _totalRelations;
		private IEnumerable<ConversationUserRelation> _relations;



		private BlrtConversationMetaQuery (IEnumerable<ConversationUserRelation> relations, ILocalStorageParameter localStorageParam = null) : base (localStorageParam)
		{
			_base = relations;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is BlrtConversationMetaQuery) {
				var o = other as BlrtConversationMetaQuery;

				return _base == o._base || (_base != null && o._base != null && o._base.SequenceEqual ( _base ));
			}
			return false;
		}

		public static bool IsEmptyOrNull(Array array){
			return array == null || array.Length == 0;
		}

		void CreateList ()
		{
			if (_relations != null)
				return;

			_totalRelations = _base.Concat(from rel in LocalStorage.DataManager.GetConversationUserRelations ((null == ParseUser.CurrentUser) ? "" : ParseUser.CurrentUser.ObjectId) 
				where rel.Conversation != null && (rel.Conversation.NeedsMetaUpdate)
				select rel).Distinct().ToArray();


			_relations = _totalRelations.Take (10).ToArray();
		}

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			_relations = null;
			CreateList ();

			if ((null == _relations) || (!_relations.Any ()))
				return;


			var convoParseObjects = from rel in _relations
					where ((null != rel) && (rel.Conversation != null)) 
					select LocalStorage.DataManager.GetParseObject(rel.ConversationId, Conversation.CLASS_NAME, true);

			var t1 = GetRelations (convoParseObjects);
			var t2 = GetUnread (convoParseObjects);

			await Task.WhenAll(t1, t2);

			foreach (var c in _relations) {
				if ((null != c) && (null != c.Conversation)) {
					c.Conversation.NeedsMetaUpdate = false;
				}
			}


			await BlrtParseActions.FindUsersQuery (LocalStorage.DataManager.GetUnkownUserId(false), CreateTokenWithTimeout (15000)); 

			if ((null != _relations) && (null != _totalRelations) && (null != _base) && (_relations.Count () != _totalRelations.Count ())) {
				var needUpdate = _base.Where (rel => ((null != rel) && (null != rel.Conversation) && rel.Conversation.NeedsMetaUpdate));
				var q = new BlrtConversationMetaQuery(needUpdate, GetLocalStorageParameter (needUpdate));
				BlrtManager.Query.ForceAddQuery(q);
				q.Run();
			}
		}

		private async Task GetRelations (IEnumerable<ParseObject> parseConversations)
		{
			var query = ParseObject.GetQuery(ConversationUserRelation.CLASS_NAME)
				.WhereContainedIn(ConversationUserRelation.CONVERSATION_KEY, parseConversations)
				.WhereNotEqualTo(ConversationUserRelation.USER_KEY, ParseUser.CurrentUser)
				.WhereEqualTo(BlrtBase.DELETED_AT_KEY, null)
				.Limit(500);
			var results = await query.BetterFindAsync (CreateTokenWithTimeout (15000)); 
			LocalStorage.DataManager.AddParse ( results );
		}



		private async Task GetUnread (IEnumerable<ParseObject> parseConversations) {

			return;
			/*
			var list = new List<string>();

			foreach (var c in parseConversations) {

				var r = BlrtManager.Data.GetConversationUserRelation (c.ObjectId, ParseUser.CurrentUser.ObjectId);

				if (r != null) {
					for (int i = 0; i < r.Conversation.ReadableCount; ++i) {
						if (!r.ViewedList.Contains (i)) {
							list.Add (CreatePseudoId(c.ObjectId, i));
						}
					}
				}
			}


			if(list.Count > 0) {
				var query = ParseObject.GetQuery(ConversationItem.CLASS_NAME)
					.WhereContainedIn(ConversationItem.PSUEDO_ID_KEY, list)
					.WhereEqualTo(BlrtBase.DELETED_AT_KEY, null)
					.Limit (900);

				var results = await query.BetterFindAsync (CreateTokenWithTimeout (15000));
				BlrtManager.Data.AddParse ( results );
			}
			*/
		}

		public static string CreatePseudoId(string conversationId, int index){

			return string.Format ("{0}-{1}", conversationId, index);
		}

		public static ILocalStorageParameter GetLocalStorageParameter (IEnumerable<ConversationUserRelation> relations)
		{
			ILocalStorageParameter localParam = null;
			if (null != relations) {
				var first = relations.FirstOrDefault ();
				if (null != first) {
					localParam = first.LocalStorage;
				}
			}
			return localParam;
		}
	}
}

