using System;
using System.Linq;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public class BlrtRelationFetchItemQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery (ILocalStorageParameter localStorage = null) {
			var output = BlrtManager.Query.AddQuery (new BlrtRelationFetchItemQuery ());

			if (!output.Running) 
				output.Run ();
			return output;
		}

		public override int Priority { get { return -4; } }

		private bool _started;

		private BlrtRelationFetchItemQuery (ILocalStorageParameter localStorage = null) : base (localStorage)
		{
			_started = false;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (_started) {
				return other == this;
			}
			return (other is BlrtRelationFetchItemQuery);
		}

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			_started = true;

			var rels = LocalStorage.DataManager.GetConversationUserRelations (
	        	ParseUser.CurrentUser?.ObjectId
           	);
			if (rels.Count () > 0) {
				var last = rels.Select (rel => rel.SessionSyncedAt ?? new DateTime())
					.Max();

				rels = rels.Where (rel => rel.OnParse && (!rel.SessionSynced || rel.SessionSyncedAt.Value.AddMinutes (15) < last));

				if (!rels.Any ())
					return;

				var objs = rels.Select(rel => rel.ObjectId).ToArray();




				var result = await ParseObject.GetQuery(ConversationUserRelation.CLASS_NAME).WhereContainedIn("objectId", objs).BetterFindAsync(CreateTokenWithTimeout(15000)); 
				LocalStorage.DataManager.AddParse (result);

				BlrtRelationFetchItemQuery.RunQuery (LocalStorage);
			}
		}
	}
}

