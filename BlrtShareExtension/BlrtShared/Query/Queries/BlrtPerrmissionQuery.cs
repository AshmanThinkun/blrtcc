using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;
using Newtonsoft.Json;


namespace BlrtData.Query
{
	public class BlrtPerrmissionQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(IEnumerable<ConversationUserRelation> relations = null, ILocalStorageParameter localStorage = null) {
			var r = relations ?? new ConversationUserRelation[0];
			var output =  BlrtManager.Query.AddQuery (new BlrtPerrmissionQuery (r, localStorage));


			if (!output.Running) output.Run ();
			return output;
		}

		public static BlrtQueryItem RunQuery(ConversationUserRelation relation){
			var r = new ConversationUserRelation[]{relation};
			return RunQuery (r, relation.LocalStorage);
		}

		public override int Priority { get { return _base.Any() ? 20 : 1; } }


		private IEnumerable<ConversationUserRelation> _base;

		private IEnumerable<ConversationUserRelation> _totalRelations;
		private IEnumerable<ConversationUserRelation> _relations;



		private BlrtPerrmissionQuery (IEnumerable<ConversationUserRelation> relations, ILocalStorageParameter localStorage = null) : base(localStorage)
		{
			_base = relations;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is BlrtPerrmissionQuery) {
				var o = other as BlrtPerrmissionQuery;

				return _base == o._base || (_base != null && o._base != null && o._base.SequenceEqual ( _base ));
			}
			return false;
		}

		public static bool IsEmptyOrNull(Array array){
			return array == null || array.Length == 0;
		}

		void CreateList ()
		{
			if (_relations != null)
				return;

			_totalRelations = _base.Concat(
				from rel in LocalStorage.DataManager.GetConversationUserRelations ((null == ParseUser.CurrentUser) ? "" : ParseUser.CurrentUser.ObjectId) 
				where rel.Conversation != null && (rel.PermissionsNeedsUpdate)
				orderby rel.PermissionsValid, rel.Conversation.ContentUpdatedAt descending
				select rel
			).Distinct();


			_relations = _totalRelations.Take (10);
		}

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			_relations = null;
			CreateList ();

			if (!_relations.Any ())
				return;


			await GetRelationPermissions (_relations);

			foreach (var c in _relations) {
				c.Conversation.NeedsMetaUpdate = false;
			}


			await BlrtParseActions.FindUsersQuery (LocalStorage.DataManager.GetUnkownUserId(false), CreateTokenWithTimeout (15000)); 

			if (_relations.Count () != _totalRelations.Count ()) {
				var q = new BlrtPerrmissionQuery(_base.Where(rel => (rel.PermissionsNeedsUpdate)));
				BlrtManager.Query.ForceAddQuery(q);
				q.Run();
			}
		}

		private async Task GetRelationPermissions (IEnumerable<ConversationUserRelation> relations) {

			var list = new List<object> ();

			foreach (var p in relations) {

				if (p.PermissionsNeedsUpdate) {
					list.Add (new Dictionary<string, object> () {
						{ "otherClassName", p.ClassName },
						{ "otherObjectId", 	p.ObjectId },
						{ "user", 			false },
					});
				}
			}

			if (list.Count == 0)
				return;

			var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
				"getPermissions", 
				new Dictionary<string, object> () {
					{ "objects", list },
				},
				CreateTokenWithTimeout (15000)
			);

			var resultDic = result as IDictionary<string, object>;

			foreach (var kvp in resultDic) {
				var match = relations.FirstOrDefault((ConversationUserRelation arg) => { return arg.MatchKey(kvp.Key); });

				if(match != null) {
					try{

						var dic = kvp.Value as IDictionary<string, object>;

						if (dic != null || dic.ContainsKey("permissions")) {

							var output = JsonConvert.SerializeObject(dic ["permissions"]);
							var records = JsonConvert.DeserializeObject<List<BlrtPermissions.Record>> (output);

							match.Permissions = new ConversationUserRelation.RelationPermissions (records);
						}

					} catch(Exception e) {
						throw e;
					}

				} else {
					LLogger.WriteEvent("Unknown Value", "Parse Function", "getPermissions returned " + kvp.Key, kvp.Value);
				}
			}
		}

	}
}

