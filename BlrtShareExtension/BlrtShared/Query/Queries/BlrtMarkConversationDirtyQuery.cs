using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.Query
{
	public class BlrtMarkConversationDirtyQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(BlrtInboxQueryType query, string objectId){
			var output = BlrtManager.Query.AddQuery ( new BlrtMarkConversationDirtyQuery (query, objectId) );


			if (!output.Running) output.Run ();
			return output;
		}

		public override int Priority { get { return 50; } }


		List<string> _objectIds;

		bool _started;

		BlrtInboxQueryType _query;

		private BlrtMarkConversationDirtyQuery (BlrtInboxQueryType query, string objectId)
		{
			_query = query;
			this._objectIds = new List<string>(){
				objectId
			};

			_started = false;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (this == other)
				return true;


			if (other is BlrtMarkConversationDirtyQuery) {
				var o = other as BlrtMarkConversationDirtyQuery;

				if (!(_started || o._started) && o._query == _query) {

					_objectIds.AddRange ((from tmp in o._objectIds
					                    where !_objectIds.Contains (tmp)
						select tmp).ToArray());
					o._objectIds.AddRange ((from tmp in _objectIds
					                      where !o._objectIds.Contains (tmp)
						select tmp).ToArray());
					return true;
				}
			}
			return false;
		}

		internal override async Task Action ()
		{
			_started = true;

			if (Exception != null)
				throw Exception;

			BlrtUtil.BetterParseCallFunctionAsync<object> (
				"cleanDirtyConvosFromRelations",
				new Dictionary<string, object> () {
					{ "relations", _objectIds.ToArray() }
				},
				CreateTokenWithTimeout (15000)
			);

			BlrtInboxQuery.RunQuery (_query);
		}
	}
}

