using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public class BlrtConversationItemQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery (string objectId, ILocalStorageParameter localStorageParam = null) {
			var output = BlrtManager.Query.AddQuery (new BlrtConversationItemQuery (objectId, localStorageParam));

			if (!output.Running) 
				output.Run ();
			return output;
		}

		public override int Priority { get { return 20; } }


		string _objectId;


		private BlrtConversationItemQuery (string objectId, ILocalStorageParameter localStorageParam = null) : base (localStorageParam)
		{
			this._objectId = objectId;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is BlrtConversationItemQuery) {
				var o = other as BlrtConversationItemQuery;

				return o._objectId == _objectId;
			}
			return false;
		}

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			var query = ParseObject.GetQuery (ConversationItem.CLASS_NAME)
				.WhereEqualTo ("objectId", _objectId)
				.WhereEqualTo(BlrtBase.DELETED_AT_KEY, null)
				.Include (ConversationItem.MEDIA_KEY);

			var result = await query.BetterFirstAsync(CreateTokenWithTimeout (15000)); 
			LocalStorage.DataManager.AddParse (result);
		}
	}
}

