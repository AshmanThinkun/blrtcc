using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public class BlrtConversationQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery (string conversationId, int start = -1, int length = 0, ILocalStorageParameter localStorageParam = null)
		{
			var output =  BlrtManager.Query.AddQuery (new BlrtConversationQuery (conversationId) {
				_start = start,
				_length = length
			});

			if (!output.Running) output.Run ();
			return output;
		}

		public static BlrtQueryItem RunQuery (Conversation conversation) {
			return RunQuery (conversation.ObjectId, -1, 0, conversation.LocalStorage);
		}

		public static bool HasQuery (string conversationId, int start = -1, int length = 0, ILocalStorageParameter localStorageParam = null)
		{
			return BlrtManager.Query.HasQuery (new BlrtConversationQuery (conversationId, localStorageParam) {
				_start = start,
				_length = length,
			});
		}

		public override int Priority { get { return 20; } }

		public const int MaxLength = 50;


		private int _start;
		private int _length;
		private string _conversationId;
		private bool GeneralRefresh { get { return _start < 0; }}



		private BlrtConversationQuery (string conversationId, ILocalStorageParameter localStorageParam = null) : base (localStorageParam)
		{
			this._conversationId = conversationId;

			_start = _length = 11;
		}

		public override bool Equals (BlrtQueryItem other)
		{
			if (other is BlrtConversationQuery) {
				var o = other as BlrtConversationQuery;

				return o._conversationId == _conversationId
					&& GeneralRefresh == o.GeneralRefresh
					&& ((GeneralRefresh)
						|| (!GeneralRefresh
							&& o._start == _start
							&& o._length == _length));
			}
			return false;
		}

		public static bool IsEmptyOrNull(Array array){
			return array == null || array.Length == 0;
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			var parseConversation = LocalStorage.DataManager.GetParseObject (_conversationId, Conversation.CLASS_NAME, true);

			var refreshQuery = ParseObject.GetQuery(ConversationUserRelation.CLASS_NAME)
				.WhereEqualTo(ConversationUserRelation.USER_KEY, ParseUser.CurrentUser)
				.WhereEqualTo(ConversationUserRelation.CONVERSATION_KEY, parseConversation)
				.WhereEqualTo(BlrtBase.DELETED_AT_KEY, null)
				.Include(ConversationUserRelation.CONVERSATION_KEY)
                .Include (string.Format("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.UNUSED_MEDIA_KEY))
				.Include(string.Format("{0}.{1}", ConversationUserRelation.CONVERSATION_KEY, Conversation.LAST_MEDIA_KEY));

			var result = await refreshQuery.BetterFirstAsync(CreateTokenWithTimeout(15000));
			var t = System.Threading.Thread.CurrentThread.ManagedThreadId;
			LocalStorage.DataManager.AddParse (result);

			var query = ParseObject.GetQuery(ConversationItem.CLASS_NAME)
				.WhereEqualTo(ConversationItem.CONVERSATION_KEY, parseConversation)
				.WhereEqualTo(BlrtBase.DELETED_AT_KEY, null)
				.Include 					(ConversationItem.MEDIA_KEY)
				.OrderByDescending 			("updatedAt")
				.Limit(MaxLength);

			if (!GeneralRefresh) {
				var s = Math. Max(0, Math.Min(_start, _start + (_length - MaxLength) / 2));

				query = query.WhereGreaterThanOrEqualTo 	(ConversationItem.INDEX_KEY, s)
					.WhereLessThan (ConversationItem.INDEX_KEY, s + MaxLength);
			}
				
			Results = await query.BetterFindAsync (CreateTokenWithTimeout (15000));
			LocalStorage.DataManager.AddParse (Results);

			await BlrtParseActions.FindUsersQuery (LocalStorage.DataManager.GetUnkownUserId(false), CreateTokenWithTimeout(15000));

			var convo = LocalStorage.DataManager.GetConversation (_conversationId);

			convo.NeedsRefresh = false;

			if (_length < MaxLength) {

				var items = LocalStorage.DataManager.GetConversationItems (convo.ObjectId);
				var indexes = new List<int> ();
				var unknownIndexes = new List<int> ();

				int high 	= int.MinValue;
				int low 	= int.MaxValue;


				foreach(var item in items){
					
					low		= Math.Min(low, item.Index);
					high	= Math.Max(high, item.Index);

					indexes.Add (item.Index);
				}

				if (GeneralRefresh) {

					if (high < MaxLength) {

						high = Math.Max(high, convo.GeneralCount); 

						for (int i = 0; i < high; ++i) {
							if(!indexes.Contains(i))
								unknownIndexes.Add (i);
						}
					}

				} else {
					for (int i = _start; i < _start + _length; ++i) {
						if(!indexes.Contains(i))
							unknownIndexes.Add (i);
					}
				}

				foreach(var index in unknownIndexes) {
					if (!convo.DamagedIndexes.Contains (index)) {
						convo.DamagedIndexes.Add (index);
						convo.Dirty = true;
					}
				}
			}
		}
	}
}

