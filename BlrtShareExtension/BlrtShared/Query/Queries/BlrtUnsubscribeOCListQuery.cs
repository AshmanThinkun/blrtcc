﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Parse;

namespace BlrtData.Query
{
	public class BlrtUnsubscribeOCListQuery : BlrtQueryItem
	{
		#region implemented abstract members of BlrtQueryItem

		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;
			if (ParseUser.CurrentUser == null) {
				return;
			}
			if (null == UserOrganisationRelation.Current 
				|| null == UserOrganisationRelation.Current.SubscribedOCListIds
				|| (!UserOrganisationRelation.Current.SubscribedOCListIds.Contains (_listId)))
			{
				return;
			}

			try {
				var result = await BlrtUtil.BetterParseCallFunctionAsync<object> (
					"unsubscribeOCList",
					new Dictionary<string, object> () {
						{ "listId", BlrtEncode.EncodeId (_listId) },
					},
					CreateTokenWithTimeout (15000)
				);
				await UserOrganisationRelation.UpdateCurrent ();
			} catch (Exception exex) {
				LLogger.WriteEvent ("BlrtUnsubscribeOCListQuery", "Query", "Exception", exex.ToString ());
			}
		}

		public override bool Equals (BlrtQueryItem other)
		{
			var otherUnsubscribeQuery = other as BlrtUnsubscribeOCListQuery;
			if (null != otherUnsubscribeQuery) {
				return this._listId == otherUnsubscribeQuery._listId;
			}
			return false;
		}

		public override int Priority {
			get {
				return 29;
			}
		}

		#endregion

		string _listId;
		private BlrtUnsubscribeOCListQuery (string ocListId)
		{
			_listId = ocListId;
		}

		public static BlrtQueryItem RunQuery(string ocListId) {
			if (string.IsNullOrWhiteSpace (ocListId)) {
				throw new ArgumentNullException ("ocListId");
			}
			var output = BlrtManager.Query.AddQuery (new BlrtUnsubscribeOCListQuery (ocListId));
			if (!output.Running) 
				output.Run ();
			return output;
		}
	}
}

