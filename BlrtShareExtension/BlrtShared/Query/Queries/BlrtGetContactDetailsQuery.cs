using System;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Query
{
	public class BlrtGetContactDetailsQuery : BlrtQueryItem
	{
		public static BlrtQueryItem RunQuery(){
			var output =  BlrtManager.Query.AddQuery (new BlrtGetContactDetailsQuery ());

			if (!output.Running) output.Run ();
			return output;
		}

		public override int Priority { get { return 25; } }

		private BlrtGetContactDetailsQuery () { }

		public override bool Equals (BlrtQueryItem other) {
			return (other is BlrtGetContactDetailsQuery);
		}


		internal override async Task Action ()
		{
			if (Exception != null)
				throw Exception;

			if (ParseUser.CurrentUser == null)
				throw new NullReferenceException ();

			await BlrtParseActions.GetContactDetails(this.CreateTokenWithTimeout(15 * 1000));
		}

	}
}

