using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace BlrtData.Query
{
	public abstract class BlrtQueryItem : IComparable<BlrtQueryItem>, IEquatable<BlrtQueryItem>
	{

		public abstract int Priority { get; }
		public DateTime LastRequested { get; private set; }

		public IEnumerable<Parse.ParseObject> Results;

		public bool Failed { get { return _exception != null; } }
		public bool Running { get; private set; }

		public bool Done { get { return Finished || Failed; } }



		public bool Finished { get { return _finished; }
			internal set{ 
				if (value && !Done) {
					_finished = value;
					Running = false;
					_signal.TrySetResult (true);
				}
			}
		}
		public Exception Exception { get{ return _exception;}
			internal set{ 
				if (value != null && !Done) {
					_exception = value;
					Running = false;
					_signal.TrySetException (_exception);
				}
			}
		}

		public ILocalStorageParameter LocalStorage
		{ 
			get {
				if (null == _localStorage) {
					return LocalStorageParameters.Default;
				}
				return _localStorage;
			}

			set {
				_localStorage = value;
			}
		}

		private TaskCompletionSource<bool> _signal;
		private Task _awaitingTask;
		private bool _finished;
		private Exception _exception;
		private ILocalStorageParameter _localStorage;

		CancellationTokenSource _cancelationSourceToken;

		protected CancellationToken CancellationToken { get { return _cancelationSourceToken.Token; } } 


		protected BlrtQueryItem (ILocalStorageParameter localStorageParam = null) {
			_signal = new TaskCompletionSource<bool>();
			_finished = Running = false;
			_exception = null;
			_awaitingTask = null;

			_cancelationSourceToken = new CancellationTokenSource ();
			_localStorage = localStorageParam;
		}

		protected virtual void Reset() { 

			_finished = Running = false;
			_exception = null;
			_awaitingTask = null;

			_cancelationSourceToken = new CancellationTokenSource ();
		}

		public void Rerun(){
		
			if (!Done) {
				return;
			}

			Reset ();
			Run ();
		}




		public void UpdateLastRequest() { 
			LastRequested = DateTime.Now; 
		}



		public void Cancel (){

			Exception = new TaskCanceledException ();
			_cancelationSourceToken.Cancel ();
		}

		internal abstract Task Action ();
		public abstract bool Equals (BlrtQueryItem other);

		protected CancellationToken CreateTokenWithTimeout(int millisecondsDelay) {
			return CancellationTokenSource.CreateLinkedTokenSource(
				CancellationToken, new CancellationTokenSource(millisecondsDelay).Token
			).Token;
		}

		protected CancellationToken CreateTokenWithTimeout(TimeSpan delay) {
			return CancellationTokenSource.CreateLinkedTokenSource(
				CancellationToken, new CancellationTokenSource(delay).Token
			).Token;
		}


		public void Run () {

			if(Running || Done) return;

			Running = true;
			BlrtManager.Query.ForceAddQuery (this);
			UpdateLastRequest ();
			WaitAsync ();
		}



		public int CompareTo (BlrtQueryItem other) {
			return Compare (this, other);
		}

		public static int Compare (BlrtQueryItem x, BlrtQueryItem y)
		{
			int compare = x.Priority.CompareTo(y.Priority);
			if (compare != 0)	return -compare;

			return x.LastRequested.CompareTo(y.LastRequested);;
		}



		public Task WaitAsync () {
			if (_awaitingTask == null) {
				_awaitingTask = Wait ();
			}
			return _awaitingTask;
		}


		Task Wait() {
			return _signal.Task;
		}
	}
}
