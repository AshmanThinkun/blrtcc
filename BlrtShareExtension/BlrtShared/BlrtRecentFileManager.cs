using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace BlrtData
{
	public static class BlrtRecentFileManager
	{
		public static string Folder{
			get{
				return BlrtConstants.Directories.Default.GetFilesPath ("recent/");
			}
		}

		const int MaxFiles = 128;

		static List<string> _used;

		public static void RefreshFileInfo(){

			if (!Directory.Exists (Folder)) {
				Directory.CreateDirectory (Folder);
			}
		
			var files = Directory.GetFiles (Folder);

			if (files.Length > MaxFiles) {
			
				int delete = MaxFiles - files.Length;
			
				var info = (from f in files
				            select new FileInfo (f))
					.OrderBy((FileInfo arg) => {return arg.LastWriteTime;});

				foreach (var i in info) {

					if (delete <= 0)
						break;

					try {
						i.Delete();
						delete--;
					} catch {
					}
				}
			}
		}

		public static string GetUniqueFile(string extension, string prefix = null){

			string output = "";

			if (!extension.StartsWith ("."))
				extension = "." + extension;


			if (_used == null)
				_used = new List<string> ();

			do {
				output = Folder + (prefix ?? "") + Guid.NewGuid ().ToString () + extension;

			} while (File.Exists (output) || _used.Contains(output));

			_used.Add (output);

			return output;
		}

	}
}

