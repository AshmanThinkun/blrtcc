using System;

namespace BlrtData.Instructions
{
	public interface ITabbedInstruction : IBlrtInstruction {
	}

	public interface IMailboxInstruction : ITabbedInstruction{
		MailboxInstructionTypes GotoMailbox { get; }
	}

	public enum MailboxInstructionTypes {
		Any,
		Archive,
		Inbox,
		Sent
	}



	public interface IOpenConversationInstruction : IMailboxInstruction {
		string ConversationId { get; }
		string RelationId { get; }
	}

	public interface IOpenBlrtInstruction : IOpenConversationInstruction{
		string BlrtId { get; }
	}

	public interface IShowMessageInstruction: IBlrtInstruction {
		string Title { get; }
		string Message { get; }
		string Button { get; }

		string TitleType { get; }
		string MessageType { get; }
		string ButtonType { get; }
		string[] Args { get; }
	}

	public interface IOpenUrlInstruction : IBlrtInstruction{
		string Url { get; }
		bool InApp { get; }
	}

	public interface IUpdateAppInstruction : IBlrtInstruction{
	}

	public interface IOpenSupportInstruction : IBlrtInstruction{
		bool StraightToMessage { get; }
	}

	public interface IOpenAccountUpgradeInstruction : IBlrtInstruction{
		bool ForcePremium { get; }
	}


	public interface IMakeBlrtInstruction : IMailboxInstruction {
		string BlrtName { get; }
		string CaptureUrl { get; }
		string Recipients { get; }
		string[] DownloadUrls { get; }
		string[] Emails { get; }

		string AttachedMeta { get; }
		string RequestId { get; }
	}
}

