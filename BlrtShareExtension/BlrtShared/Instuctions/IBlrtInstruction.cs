using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlrtData.Instructions {


	public interface IInstructionHandler{
	
		void ExecuteInstruction (IBlrtInstruction instruction, bool selfFired);

	}


	public interface IBlrtInstruction{
		BlrtInstructionSource Source { get; }
		IBlrtInstruction Next { get; }

		bool WaitOnCanvas { get; }

		bool Finished { get; }
		bool Cancelled { get; }


		void Finish ();
		void Cancel ();

		bool ShouldFinish (Type instruction);



		CancellationToken GetCancellationToken ();
		Task WaitAsync ();
	}



	public abstract class BlrtInstruction : IBlrtInstruction {
		bool _finished;
		CancellationTokenSource _cts;
		SemaphoreSlim _sema;
		Task _awaiter;
		IBlrtInstruction _next;

		public BlrtInstructionSource Source { get; private set; }

		public IBlrtInstruction Next { get { return _next; } set{ _next = value;} }

		//public abstract string Action		{ get; }
		public bool Finished		{ get { return _finished; } }
		public virtual bool WaitOnCanvas		{ get { return true; } }
		public bool Cancelled		{ get { return _cts.IsCancellationRequested; } }
		public virtual bool StraightToMessage { get { return false; } }


		public void Finish(){
			if (Cancelled)
				throw new BlrtInstructionException (BlrtInstructionException.ErrorCode.AlreadyCancelled);
			if (Finished)
				throw new BlrtInstructionException (BlrtInstructionException.ErrorCode.AlreadyFinished); 

			_finished = true;
			_sema.Release ();
		}
		public void Cancel(){
			if (Cancelled)
				throw new BlrtInstructionException (BlrtInstructionException.ErrorCode.AlreadyCancelled);
			if (Finished)
				throw new BlrtInstructionException (BlrtInstructionException.ErrorCode.AlreadyFinished); 
				
			_cts.Cancel ();
			_sema.Release ();
		}

		public CancellationToken GetCancellationToken(){
			return _cts.Token;
		}

		public Task WaitAsync(){
			if (_awaiter == null) {
				_awaiter = _sema.WaitAsync ();
			}
			return _awaiter;
		}

		public abstract bool ShouldFinish (Type instruction);


		public BlrtInstruction(BlrtInstructionSource source) { 
			this.Source = source;

			_sema	= new SemaphoreSlim (0);
			_cts	= new CancellationTokenSource ();
		}
	}

	public class BlrtInstructionException : Exception{
	
		public enum ErrorCode {
			InstructionCancelled,
			InvalidInstruction,

			Unauthorised,

			AlreadyCancelled,
			AlreadyFinished

		}

		public ErrorCode Code { get; private set; }
	
		public BlrtInstructionException(ErrorCode code) : base() 
		{
			this.Code = code;
		}
	}



	public class BlrtInstructionSource {
	
		public enum Source {

			InAppSystem,
			InAppPush,
			InAppAppLink,
			InAppUserAction,
			InAppScheduled,

			System,
			Push, 
			AppLink, 
		}

		public Source Type { get; private set; }

		public bool FromSystem { 
			get { return Type == Source.InAppSystem || Type == Source.InAppScheduled || Type == Source.System;} 
		}

		public bool FromUser { 
			get { return Type == Source.InAppUserAction;} 
		}

		public bool FromPush { 
			get { return Type == Source.Push || Type == Source.InAppPush; } 
		}

		public bool InApp { 
			get { return Type == Source.InAppSystem 
				|| Type == Source.InAppPush 
				|| Type == Source.InAppAppLink 
				|| Type == Source.InAppUserAction
				|| Type == Source.InAppScheduled; 
			} 
		}

		public bool UserAction { 
			get { return Type == Source.InAppUserAction; } 
		}

		public bool AppLink { 
			get { return Type == Source.InAppAppLink || Type == Source.InAppUserAction; } 
		}


		public BlrtInstructionSource(Source type){
			this.Type = type;
		}
	}

}