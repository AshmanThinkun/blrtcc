using System;
using System.Linq;
using System.Collections.Generic;

namespace BlrtData.Instructions
{
	public class OpenConversationInstruction : BlrtInstruction, IOpenConversationInstruction
	{
		public string ConversationId { get { return _conversationId; } }
		public MailboxInstructionTypes GotoMailbox { get { return _mailbox; } }

		public string RelationId { get; set; }

		string _conversationId;
		MailboxInstructionTypes _mailbox;

		public OpenConversationInstruction (BlrtInstructionSource source, string conversationId) 
			: base (source)
		{
			_conversationId = conversationId;
			_mailbox = MailboxInstructionTypes.Any;
		}

		public OpenConversationInstruction (BlrtInstructionSource source, string conversationId, MailboxInstructionTypes mailbox) 
			: base (source)
		{
			_conversationId = conversationId;
			_mailbox = mailbox;
		}


		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IOpenConversationInstruction)
				|| instruction == typeof(OpenConversationInstruction);
		}
	}


	public class OpenBlrtInstruction : OpenConversationInstruction, IOpenBlrtInstruction
	{
		public string BlrtId { get { return _blrtId; } }

		string _blrtId;

		public OpenBlrtInstruction (BlrtInstructionSource source, string conversationId, string blrtId) 
			: base (source, conversationId)
		{
			_blrtId = blrtId;
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IOpenBlrtInstruction)
				|| instruction == typeof(OpenBlrtInstruction);
		}
	}



	public class OpenAccountUpgradeInstruction : BlrtInstruction, IOpenAccountUpgradeInstruction
	{
		public bool ForcePremium { get; set; }

		public override bool WaitOnCanvas { get { return false; } }

		public OpenAccountUpgradeInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IOpenAccountUpgradeInstruction)
				|| instruction == typeof(OpenAccountUpgradeInstruction);
		}
	}

	public class MakeBlrtInstruction : BlrtInstruction, IMakeBlrtInstruction
	{
		public string BlrtName { get; set; }
		public string CaptureUrl { get; set; }
		public string Recipients { get; set; }
		public string[] DownloadUrls { get; set; }
		public string[] Emails { get; set; }

		public string AttachedMeta { get; set; }
		public string RequestId { get; set; }


		public MailboxInstructionTypes GotoMailbox { get { return MailboxInstructionTypes.Any; } }

		public MakeBlrtInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IMakeBlrtInstruction)
				|| instruction == typeof(MakeBlrtInstruction);
		}
	}

	public class ShowMessageInstruction : BlrtInstruction, IShowMessageInstruction
	{
		public string Title { get; set; }
		public string Message { get; set; }
		public string Button { get; set; }

		public string TitleType { get; set; }
		public string MessageType { get; set; }
		public string ButtonType { get; set; }
		public string[] Args { get; set; }



		public ShowMessageInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}

		public ShowMessageInstruction (BlrtInstructionSource source, IDictionary<string, object> pushData) 
			: base(source) {

			if (pushData.ContainsKey ("aps")) {

				var aps = pushData ["aps"] as IDictionary<string, object>;

				if(aps != null && aps.ContainsKey("alert")) {
					var alertInfo = aps ["alert"];

					if (alertInfo is IDictionary<string, object>) {
						var alert = alertInfo as IDictionary<string, object>;

						TitleType 		= alert ["loc-key"].ToString();
						ButtonType 		= alert ["action-loc-key"].ToString();

						var tmp = alert ["loc-args"] as IEnumerable<object>;

						if (tmp != null) {

							Args = (from t in tmp
								where t != null
								select t.ToString ()).ToArray();
						}



					} else {
						Title = alertInfo.ToString ();

						if (pushData.ContainsKey ("bn")) 
							Button = pushData["bn"].ToString();	
						else
							Button = "OK";	
					}
				}
			}


		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IShowMessageInstruction)
				|| instruction == typeof(ShowMessageInstruction);
		}
	}
}

