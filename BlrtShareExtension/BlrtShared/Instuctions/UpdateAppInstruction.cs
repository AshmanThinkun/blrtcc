using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;


namespace BlrtData.Instructions
{


	public class UpdateAppInstruction : BlrtInstruction, IUpdateAppInstruction
	{
		public UpdateAppInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IUpdateAppInstruction)
				|| instruction == typeof(UpdateAppInstruction);
		}
	}
	public class OpenUrlInstruction : BlrtInstruction, IOpenUrlInstruction
	{
		public string Url {	get;set;}
		public bool InApp {	get;set;}

		public OpenUrlInstruction (BlrtInstructionSource source) 
			: base (source)
		{
			InApp = true;
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IOpenUrlInstruction)
				|| instruction == typeof(OpenUrlInstruction);
		}
	}
	public class OpenSupportInstruction : BlrtInstruction, IOpenSupportInstruction
	{
		public override bool WaitOnCanvas { get { return false; } }

		public OpenSupportInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}

		public override bool ShouldFinish (Type instruction) {
			return instruction == typeof(IOpenSupportInstruction)
				|| instruction == typeof(OpenSupportInstruction);
		}
	}
	public class OpenSupportMessageInstruction : OpenSupportInstruction
	{
		public override bool StraightToMessage { get { return true; } }

		public OpenSupportMessageInstruction (BlrtInstructionSource source) 
			: base (source)
		{
		}
	}
	
}

