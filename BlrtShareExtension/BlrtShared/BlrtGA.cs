//using System;
//using System.Collections.Generic;
//
//namespace BlrtData
//{
//	public interface IBlrtGA
//	{
//		// user has logged in, we need a new session
//		void TrackNewSession ();
//
//		void TrackPageView (string viewName, bool force, Dictionary<string,string> extraParam);
//		//let it know what user is signed in
//		void SetAppSettings ();
//		//sets session based properties
//		void SetTrackerValue (string name, string value);
//		void SetDimensionValue (int dimension, string value);
//
//		void TrackTiming (string category, float seconds, string label, string name);
//
//		void TrackEvent (string category, string action, string label = "", float value = 0);
//
//
//		void SetConversationId (string blrtId);
//		void RemoveBlrtId ();
//	}
//
//	public static partial class BlrtGA
//	{
//		public const int GADimensionType 			= 1;
//		public const int GADimensionBlrtId 		= 2;
//		public const int GADimensionUserId 		= 3;
////		public const int GADimensionUserEmail	 	= 4;
//		public const int GADimensionSignUpSource 	= 5;
//		public const int GADimensionAccountType 	= 6;
//		public const int GADimensionIndusty 		= 7;
//		public const int GADimensionPlatform 		= 8;
//
//
//
//		private const int DISPATCH_INTERVAL = 30;
//
//		private const bool TRACK_UNCAUGHT_EXCEPTIONS = false;
//
//		private static IBlrtGA ga;
//
//		static BlrtGA ()
//		{
//			ga = null;
//
//			if (BlrtConstants.GAKey != null) {
//				#if __IOS__
//				ga = new BlrtGAiOS ();
//				GoogleAnalytics.iOS.GAI.SharedInstance.GetTracker (BlrtConstants.GAKey);
//				#endif
//			}
//		}
//
//		public static void TrackNewSession ()
//		{
//			if (ga != null)
//				ga.TrackNewSession ();
//		}
//		public static void TrackPageView (string viewName, bool force = false, Dictionary<string,string> extraParam = null)
//		{
//			if (ga != null)
//				ga.TrackPageView (viewName, force, extraParam);
//			LLogger.WriteEvent ("GA_PageView", viewName);
//		}
//		public static void SetAppValues ()
//		{
//			if (ga != null)
//				ga.SetAppSettings ();
//		}
//		public static void SetTrackerValue (string name, string value)
//		{
//			if (ga != null)
//				ga.SetTrackerValue (name, value);
//		}
//		public static void SetDimensionValue (int dimension, string value)
//		{
//			if (ga != null)
//				ga.SetDimensionValue (dimension, value);
//		}
//		public static void TrackTiming (string category, float seconds, string label, string name)
//		{
//			if (ga != null)
//				ga.TrackTiming (category, seconds, label, name);
//
//			LLogger.WriteEvent ("GA_Timing" + category, name, label, seconds);
//		}
//		public static void TrackEvent (string category, string action, string label = "", float value = 0f)
//		{
////			if (ga != null)
////				ga.TrackEvent (category, action, label, value);
////
////			LLogger.WriteEvent ("GA_Event: " + category, action, label, value);
//		}
//
//
//
//		public static void SetConversationId (string blrtId)
//		{
//			if (ga != null)
//				ga.SetConversationId (blrtId);
//		}
//
//		public static void RemoveBlrtId ()
//		{
//
//			if (ga != null)
//				ga.RemoveBlrtId ();
//		}
//
//	}
//}
//
