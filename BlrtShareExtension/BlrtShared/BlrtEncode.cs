using System;
using System.Collections.Generic;
using System.Linq;

namespace BlrtData
{
	public static class BlrtEncode
	{
		public static string EncodeId (string id)
		{
			string result = "";

			foreach (var letter in id)
				result += BlrtConstants.SharedObjectIdCryptToken [BlrtConstants.ObjectIdChars.IndexOf (letter)];

			return result;
		}

		public static string DecodeId(string id)
		{
			string result = "";

			foreach (var letter in id) {
				result += BlrtConstants.ObjectIdChars [BlrtConstants.SharedObjectIdCryptToken.IndexOf (letter)];
			}

			return result;
		}

		public static string UrlEncode (string str)
		{
			return System.Web.HttpUtility.UrlEncode(EncodeToBase64 (str));
		}

		public static string DecodeUrl (string url)
		{
			string[] parts = url.Split (new []{'/'}, 2);
	
			if (parts.Length == 1)
				return "";


			string p2 = parts [1];
			p2 = DecodeFromBase64 (System.Web.HttpUtility.UrlDecode(p2));

			return string.Format("{0}/{1}", parts[0], p2);
		}


		static public string EncodeToBase64(string toEncode)
		{
			byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
			string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
			return returnValue;
		}

		static public string DecodeFromBase64(string encodedData)
		{
			try {
				byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
				string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

				return returnValue;
			} catch (FormatException e) {
				return "";
			}
		}


		public static long Pow(int number, int power)
		{
			long output = 1;

			while (power > 0) {
				power--;
				output *= number;
			}

			return output;
		}

		public static string EncodePrivateBlrtUrl (string blrtId, bool idAlreadyEncrypted = false, Dictionary<string, string> queries = null)
		{
			if (string.IsNullOrWhiteSpace (blrtId)) {
				return null;
			}

			if (!idAlreadyEncrypted) {
				blrtId = EncodeId (blrtId);
			}

			if (string.IsNullOrWhiteSpace (blrtId)) {
				return null;
			}

			var resultUrl = BlrtConstants.ServerUrl + "/blrt/" + blrtId;

			if ((null != queries) && (queries.Count > 0)) {
				var queryStrings = 
					from query in queries
					select string.Format ("{0}={1}", Uri.EscapeUriString (query.Key), Uri.EscapeUriString (query.Value));
				resultUrl += "?" + string.Join ("&", queryStrings);
			}

			return resultUrl;
		}

		public static string EncodePublicBlrtUrl (string blrtId, bool idAlreadyEncrypted = false)
		{
			if (string.IsNullOrWhiteSpace (blrtId)) {
				return null;
			}

			if (!idAlreadyEncrypted) {
				blrtId = EncodeId (blrtId);
			}

			if (string.IsNullOrWhiteSpace (blrtId)) {
				return null;
			}

			return BlrtConstants.EmbedUrl + "/blrt/" + blrtId;
		}
	}
}

