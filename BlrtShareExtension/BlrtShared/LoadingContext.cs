using System;
using BlrtData;
using System.Collections.Generic;

namespace BlrtData
{
	public class LoadingContext
	{
		static Dictionary<string, LoadingContext> _contexts;
		static int _globalCount;

		static LoadingContext () {
			_contexts = new Dictionary<string, LoadingContext> ();
			_globalCount = 0;
		}

		LoadingContext (string context) {
			_count = 0;
		}

		public static LoadingContext Get (string context) {
			if (!_contexts.ContainsKey (context))
				_contexts [context] = new LoadingContext (context);

			return _contexts [context];
		}


		int _count;
		public int Count {
			get {
				return _count;
			}
			set {
				var difference = value - _count;

				_globalCount += difference;
				_count = value;

				if (difference != 0) {
					if (_globalCount <= 0)
						BlrtManager.Responder.PopLoading (true);
					else
						BlrtManager.Responder.PushLoading (true);
				}
			}
		}
	}
}

