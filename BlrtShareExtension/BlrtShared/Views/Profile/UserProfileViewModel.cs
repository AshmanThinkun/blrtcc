﻿using System;
using System.Collections.Generic;
using BlrtData;
using BlrtData.Contacts;

namespace BlrtData.Views.Profile
{
	public class UserProfileViewModel: BlrtNotifyProperty
	{
		string _profileImage;
		public string AvatarImage{ get{ return _profileImage;} set{ OnPropertyChanged (ref _profileImage, value);}}
		public string DisplayName{ get; set;}
		public string PrimaryEmail{ get; set;}
		public string Gender{ get; set;}
		public string Organization{ get; set;}
		public IEnumerable<BlrtContactInfo> ContactInfoList{ get; set;}

		public bool IsPremium{ get; set;}
		public bool IsFBLinked{ get; set;}
		public bool IsPhoneLinked{ get; set;}
		public bool IsCurrentUser{ get; set;}
		public bool IsBlrtUser{ get; set;}

		public bool ShowBlrtContact{ get; set;}

		public string BottomButtonText{ get; set;}

		public List<ProfileCellModal> OperationList{get;set;}

		public UserProfileViewModel ()
		{
			ShowBlrtContact = true;
		}
	}
}

