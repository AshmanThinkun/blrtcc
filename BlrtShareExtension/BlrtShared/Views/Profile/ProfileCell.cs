﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.Profile
{
	public class ProfileCellModal{
		public string Title{get;set;}
		public string Detail{get;set;}
		public Color TextColor{get;set;}
		public string Icon{get;set;}
		public string SelectIcon{ get; set;}
		public Action Action{get;set;}
		public Color BackgroundColor{get;set;}
		public Color SeparatorColor{ get; set;}

		public ProfileCellModal(){
			SeparatorColor = BlrtStyles.BlrtGray4;
			SelectIcon = "settings_arrow.png";
		}
	}

	public class ProfileCell: ViewCell{
		Image _icon;
		Label _title, _detail;
		Image _selectIcon;
		ContentView _separator;
		BoxView _innerSeparator;

		public ProfileCell():base()
		{
			_icon = new Image(){
				VerticalOptions = LayoutOptions.Center,
			};

			_title = new Label(){
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				VerticalOptions = LayoutOptions.Fill,
				YAlign = TextAlignment.Center
			};

			_detail = new Label(){
				FontSize = BlrtStyles.CellContentFont.FontSize,
				VerticalOptions = LayoutOptions.Center,
				YAlign = TextAlignment.Center,
			};

			_selectIcon = new Image(){
				VerticalOptions = LayoutOptions.Center
			};

			_separator = new ContentView () {
				HeightRequest = 0.5,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness (50, 0, -50, 0),
				Content = _innerSeparator = new BoxView () {
				}
			};

			Thickness padding;
			if(Device.Idiom == TargetIdiom.Tablet && Device.OS == TargetPlatform.Android){
				padding = new Thickness (15, 0, 20, 0);
			}else{
				padding = new Thickness (15, 0);
			}

			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},

				RowSpacing = 0,
				ColumnSpacing = 10,
				Padding = padding,

				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{_icon,0,0},
					{new StackLayout(){
							Orientation = StackOrientation.Vertical,
							Spacing = 0,
							VerticalOptions = LayoutOptions.Center,
							Children = {
								_title,_detail
							}
						},1,0
					},
					{_selectIcon, 2,0},
					{_separator, 0,1}
				}
			};




			Grid.SetColumnSpan (_separator, 3);

			_icon.SetBinding(Image.SourceProperty, "Icon");
			_title.SetBinding(Label.TextProperty, "Title");
			_detail.SetBinding(Label.TextProperty, "Detail");
			_title.SetBinding(Label.TextColorProperty, "TextColor");
//			_detail.SetBinding(Label.TextColorProperty, "TextColor");
			View.SetBinding(Xamarin.Forms.View.BackgroundColorProperty, "BackgroundColor");
			_innerSeparator.SetBinding (BoxView.BackgroundColorProperty, "SeparatorColor");
			_selectIcon.SetBinding (Image.SourceProperty, "SelectIcon");
		}
	}
}

