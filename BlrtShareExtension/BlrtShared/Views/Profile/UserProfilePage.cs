﻿using System;
using Xamarin.Forms;
using BlrtData;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using System.Linq;

namespace BlrtData.Views.Profile
{
	public class UserProfilePage: BlrtContentPage
	{
		public const int RowHeight = 65;
		MR.Gestures.Image _profileImage;
		ListView _listView;
		Button _changeAvatar, _bottomBtn, _editProfileBtn, _premiumBadge, _googleBadge, _fbBadge, _phoneBadge;
		SwitchView _showBlrtContact;
		StackLayout _isBlrtContactStack;

		public event EventHandler OnPremiumIconTapped, OnFBIconTapped, OnGoogleIconTapped, OnPhoneIconTapped, OnChangeAvatarIconTapped, OnBottomButtonTapped, OnEditProfileTapped, OnShowBlrtContactTapped;

		Label _alertLabel;

		public override string ViewName {
			get {
				return (_model!=null && _model.IsCurrentUser)? "MyProfile": "OtherProfile";
			}
		}

		public override void TrackPageView ()
		{
			var dic = new Dictionary<string,object> ();
			if(_model!=null){
				dic.Add ("accountType", _model.IsPremium ? "Blrt Premium" : "Blrt Basic");
				dic.Add ("servicesConnected", _model.IsFBLinked ? new string[]{ "Facebook" } : new string[]{ });
				if(!_model.IsCurrentUser){
					dic.Add ("isInDeviceContacts", !_model.OperationList.Any (x => x.Title == BlrtUtil.PlatformHelper.Translate ("add_contact_title", "profile")));
				}
			}
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, dic);
		}

		public UserProfilePage ():base()
		{
			_profileImage = new MR.Gestures.Image (){
				HeightRequest = 195,
				WidthRequest = 195,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Aspect = Aspect.AspectFill,
				BackgroundColor = BlrtStyles.BlrtGray4
			};

			_premiumBadge = new Button () {
				Image = "badge_b.png",
				WidthRequest = 40,
				HeightRequest = 30,
				BackgroundColor = Color.Transparent,
			};

			_googleBadge = new Button () {
				Image = "icon_google_off.png",
				WidthRequest = 40,
				HeightRequest = 30,
				BackgroundColor = Color.Transparent,
				IsVisible = false
			};

			_fbBadge = new Button () {
				Image = "icon_facebook_off.png",
				WidthRequest = 40,
				HeightRequest = 30,
				BackgroundColor = Color.Transparent,
			};

			_phoneBadge = new Button () {
				Image = "icon_phone_off.png",
				WidthRequest = 40,
				HeightRequest = 30,
				BackgroundColor = Color.Transparent,
			};

			_changeAvatar = new FormsBlrtButton () {
				Image = "icon_edit.png",
				WidthRequest = 40,
				HeightRequest = 30,
				IsVisible = false,
				BackgroundColor = Color.Transparent,
				Padding = 0
			};

			_premiumBadge.Clicked += (sender, e) => {
				OnPremiumIconTapped?.Invoke(this, EventArgs.Empty);	
			};

			_fbBadge.Clicked += (sender, e) => {
				OnFBIconTapped?.Invoke(this, EventArgs.Empty);	
			};

			_phoneBadge.Clicked += (sender, e) => {
				OnPhoneIconTapped?.Invoke(this, EventArgs.Empty);
			};

			_changeAvatar.Clicked += (sender, e) => {
				OnChangeAvatarIconTapped?.Invoke(sender,e);
			};

			var badges = new StackLayout () {
				Padding = new Thickness(0,0,0,5),
				Orientation = StackOrientation.Vertical,
				Spacing = 10,
				Children = {
					_googleBadge,
					_fbBadge,
					_phoneBadge
				}
			};

			if(Device.OS == TargetPlatform.Android && Device.Idiom == TargetIdiom.Tablet){
				_googleBadge.WidthRequest = _fbBadge.WidthRequest = _phoneBadge.WidthRequest = _premiumBadge.WidthRequest = 50;
			}


			AbsoluteLayout.SetLayoutFlags (_profileImage, AbsoluteLayoutFlags.PositionProportional);
			AbsoluteLayout.SetLayoutBounds (_profileImage, new Rectangle(0.5,0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
			AbsoluteLayout.SetLayoutFlags (_changeAvatar, AbsoluteLayoutFlags.None);
			AbsoluteLayout.SetLayoutBounds (_changeAvatar, new Rectangle(5,195-35, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

			var profileImageSection = new AbsoluteLayout () {
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = {
					_profileImage,
					_changeAvatar,
				}
			};

			AbsoluteLayout.SetLayoutFlags (profileImageSection, AbsoluteLayoutFlags.PositionProportional);
			AbsoluteLayout.SetLayoutBounds (profileImageSection, new Rectangle(0.5,0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

			AbsoluteLayout.SetLayoutFlags (badges, AbsoluteLayoutFlags.PositionProportional);
			AbsoluteLayout.SetLayoutBounds (badges, new Rectangle(1,1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

			AbsoluteLayout.SetLayoutFlags (_premiumBadge, AbsoluteLayoutFlags.XProportional);
			AbsoluteLayout.SetLayoutBounds (_premiumBadge, new Rectangle(1,5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));


			Xamarin.Forms.View profileSec;
			if (Device.Idiom != TargetIdiom.Phone) {
				profileSec = new AbsoluteLayout () {
					Padding = new Thickness (7, 5),
					VerticalOptions = LayoutOptions.Fill,	
					HorizontalOptions = LayoutOptions.Fill,
					Children = {
						profileImageSection,
						badges,
						_premiumBadge
					}
				};
			}else{
				badges.Padding = new Thickness (0, 0, -12, 5);
				profileSec = new Grid () {
					Padding = new Thickness (0, 5),
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) }
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					},
					ColumnSpacing = 0,
					Children = {
						{profileImageSection,1,0},
						{badges, 2,1},
						{new ContentView(){
								Padding = new Thickness(0,5,-12,0),
								VerticalOptions = LayoutOptions.Start,
								Content = _premiumBadge
							},2,0}
					}
				};

				Grid.SetRowSpan (profileImageSection, 2);
			}

			_listView = new FormsListView () {
				ItemTemplate = new DataTemplate (typeof (ProfileCell)),
				HeightRequest = 0,
				RowHeight = RowHeight,
				DisableScrolling = true,
				SeparatorVisibility = SeparatorVisibility.None,
				SeparatorColor = Color.Transparent,
				BackgroundColor = BlrtStyles.BlrtWhite
			};

			_listView.ItemSelected += _listView_ItemSelected;

			//hide contact section
			_showBlrtContact = new SwitchView () {
				Text = BlrtUtil.PlatformHelper.Translate("show_in_contact_title", "profile"),
				BackgroundColor = BlrtStyles.BlrtWhite,
				Padding = new Thickness (75, 10, 10, 10),
				IsVisible = false
			};
			_showBlrtContact.Toggled += (sender, e) => {
				if(!_ignoreSwitch){
					OnShowBlrtContactTapped?.Invoke(sender,e);
				}
			};

			_editProfileBtn = new Button () {
				Image = "icon_edit2.png",
				WidthRequest = 40,
				BackgroundColor = Color.Transparent,
			};
			_editProfileBtn.Clicked+= (sender, e) => {
				OnEditProfileTapped?.Invoke(sender,e);
			};

			_isBlrtContactStack = new StackLayout () {
				Padding = new Thickness(15,10, 30, 10),
				HorizontalOptions = LayoutOptions.Start,
				Spacing = 2,
				Orientation = StackOrientation.Vertical
			};

			var grid = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},

				RowSpacing = 0,
				ColumnSpacing = 5,
				Padding = new Thickness (0, 0),
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{profileSec,0,0},
					{_listView,0,1},
					{_showBlrtContact,0,2},
					{_isBlrtContactStack, 0,3},
					{new ContentView(){
							Padding = new Thickness(5,5),
							HorizontalOptions = LayoutOptions.EndAndExpand,
							VerticalOptions = LayoutOptions.Start,
							Content = _editProfileBtn
						}, 0,3}
				}
			};

			_bottomBtn = new Button () {
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtBlueGray,
				Font = BlrtStyles.CellTitleBoldFont,
				IsVisible = false,
				HorizontalOptions = LayoutOptions.Center
			};
			_bottomBtn.Clicked+= (sender, e) => {
				OnBottomButtonTapped?.Invoke(sender,e);
			};

			var scrollView = new ScrollView () {
				VerticalOptions = LayoutOptions.Fill,
				Orientation = ScrollOrientation.Vertical,
				Content = grid
			};

			_alertLabel = new FormsLabel () {
				BackgroundColor = Color.FromRgba(0,0,0,200),
				TextColor = BlrtStyles.BlrtWhite,
				CornerRadius = 14,
				IsVisible = false,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				HeightRequest = 50,
				WidthRequest = 200,
			};

			this.Content = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},

				RowSpacing = 0,
				ColumnSpacing = 0,
				Padding = new Thickness (0, 0),
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{scrollView,0,0},
					{_alertLabel,0,0},
					{_bottomBtn,0,1}
				}
			};

			this.BindingContextChanged += (sender, e) => {
				var b = BindingContext as UserProfileViewModel;
				if(b!=null)
					this.Bind(b);
			};

			this.BackgroundColor = BlrtStyles.BlrtCharcoal;

			_profileImage.SetBinding(Image.SourceProperty, "AvatarImage");
		}


		private async void ShowMessage(string text, int duration=2000){
			_alertLabel.Opacity = 0;
			_alertLabel.IsVisible = true;
			_alertLabel.Text = text;
			_alertLabel.FadeTo (1);
			await System.Threading.Tasks.Task.Delay (duration);
			await _alertLabel.FadeTo(0);
			_alertLabel.IsVisible = false;
		}

		UserProfileViewModel _model;

		private void Bind(UserProfileViewModel model){
			_model = model;
			GroupContactDetails ();

			this.Title = model.DisplayName;

			_changeAvatar.IsVisible = model.IsCurrentUser;
			_editProfileBtn.IsVisible = model.IsCurrentUser;

			if(model.IsPremium){
				_premiumBadge.Image = "badge_b.png";
			}else if(model.IsBlrtUser || model.IsCurrentUser){
				_premiumBadge.Image = "badge_b_off.png";
			}else{
				_premiumBadge.Image = "";
			}

			if(model.IsFBLinked){
				_fbBadge.Image = "icon_facebook_on.png";
			}else{
				_fbBadge.Image = "icon_facebook_off.png";
			}

			if(model.IsPhoneLinked){
				_phoneBadge.Image="icon_phone_on.png";
			}else{
				_phoneBadge.Image="icon_phone_off.png";
			}

			_listView.ItemsSource = model.OperationList;
			_listView.HeightRequest = model.OperationList.Count * RowHeight;

			RenderContacts (model);

			if(!string.IsNullOrEmpty(model.BottomButtonText)){
				_bottomBtn.IsVisible = true;
				_bottomBtn.Text = model.BottomButtonText;
			}else{
				_bottomBtn.IsVisible = false;
			}

			if(!model.IsCurrentUser){
				_showBlrtContact.IsVisible = true;
				_ignoreSwitch = true;
				_showBlrtContact.On = model.ShowBlrtContact;
				_ignoreSwitch = false;
			}
		}
		bool _ignoreSwitch;

		IEnumerable<string> blrtEmails, blrtPhones, nonBlrtEmails, nonBlrtPhones;
		void GroupContactDetails(){
			//This is not efficient, but it should be fine as it only has a few records
			blrtEmails = new List<string> ();
			if (!string.IsNullOrWhiteSpace (_model.PrimaryEmail)) {
				(blrtEmails as List<string>).Add (_model.PrimaryEmail);
			}
			(blrtEmails as List<string>).AddRange (_model.ContactInfoList.Where (x =>
				x.ContactType == BlrtData.Contacts.BlrtContactInfoType.Email &&
				x.IsLinkToBlrtUser
			).Select (x => x.ContactValue));
			blrtEmails = blrtEmails.Distinct ().ToList();

			blrtPhones = _model.ContactInfoList.Where (x =>
				x.ContactType == BlrtData.Contacts.BlrtContactInfoType.PhoneNumber &&
				x.IsLinkToBlrtUser
			).Select (x => x.ContactValue);

			nonBlrtEmails = _model.ContactInfoList.Where (x =>
				x.ContactType == BlrtData.Contacts.BlrtContactInfoType.Email &&
				!x.IsLinkToBlrtUser &&
				!blrtEmails.Contains(x.ContactValue)
			).Select (x => x.ContactValue);

			nonBlrtPhones = _model.ContactInfoList.Where (x =>
				x.ContactType == BlrtData.Contacts.BlrtContactInfoType.PhoneNumber &&
				!x.IsLinkToBlrtUser &&
				!blrtPhones.Contains(x.ContactValue)
			).Select (x => x.ContactValue);
		}

		Line GetDefaultLine(int index){
			Line myline = null;
			if(index<_isBlrtContactStack.Children.Count){
				myline = _isBlrtContactStack.Children [index] as Line;
			}else{
				myline = new Line ();
				_isBlrtContactStack.Children.Add (myline);
			}
			myline.ResetValue ();

			return myline;
		}

		class Line: ContentView
		{
			public Image Icon{get; set;}
			public Label TextLabel{ get; set;}
			public Line():base(){
				Icon = new Image(){
					Source = "b_user_icon.png",
					Opacity = 0,
					WidthRequest = 12
				};

				TextLabel = new Label(){
					FontSize = BlrtStyles.CellTitleFont.FontSize,
					TextColor = BlrtStyles.BlrtWhite
				};

				this.Content = new StackLayout(){
					Orientation = StackOrientation.Horizontal,
					Spacing = 5,
					Children = {Icon, TextLabel}
				};
			}

			public void ResetValue(){
				TextLabel.FontSize = BlrtStyles.CellTitleFont.FontSize;
				TextLabel.TextColor = BlrtStyles.BlrtWhite;
				TextLabel.Text = "";
			}
		}

		void RenderContacts(UserProfileViewModel model){
			int i = 0;

			var emails = blrtEmails.Concat (nonBlrtEmails);
			if(emails.Count()>0){
				foreach(var str in emails){
					var line = GetDefaultLine (i);
					var isBlrtContact = blrtEmails.Contains (str);
					line.TextLabel.Text = str;
					line.Icon.Opacity = isBlrtContact? 1:0;
					i++;
				}
				var line2 = GetDefaultLine (i);
				line2.TextLabel.Text = "";
				line2.Icon.Opacity = 0;
				i++;
			}

			var phones = blrtPhones.Concat (nonBlrtPhones);
			if(phones.Count()>0){
				foreach(var str in phones){
					var line = GetDefaultLine (i);
					var isBlrtContact = blrtPhones.Contains (str);
					line.TextLabel.Text = str;
					line.Icon.Opacity = isBlrtContact? 1:0;
					i++;
				}
				var line2 = GetDefaultLine (i);
				line2.TextLabel.Text = "";
				line2.Icon.Opacity = 0;
				i++;
			}

			if(!string.IsNullOrWhiteSpace(model.Organization)){
				var line = GetDefaultLine (i);
				line.TextLabel.Text = "Organization";
				line.TextLabel.TextColor = BlrtStyles.BlrtGray4;
				line.TextLabel.FontSize = BlrtStyles.CellContentFont.FontSize;
				line.Icon.Opacity = 0;
				i++;
				var line2 = GetDefaultLine (i);
				line2.TextLabel.Text = model.Organization;
				line2.Icon.Opacity = 0;
				i++;
			}

			for(; i< _isBlrtContactStack.Children.Count;i++){
				GetDefaultLine (i).Icon.Opacity = 0;
			}
		}

		void _listView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var modal = e.SelectedItem as ProfileCellModal;
			if(modal!=null){
				modal.Action?.Invoke ();
			}
			this._listView.SelectedItem = null;
		}

		class SwitchView:Grid{
			public string Text{
				get{ 
					return _textLabel.Text;
				} set{ 
					_textLabel.Text = value;
				}
			}
			public bool On{
				get{
					return _switch.IsToggled;
				}set{
					_switch.IsToggled = value;
				}
			}
			public event EventHandler Toggled;

			Label _textLabel;
			Switch _switch;
			public SwitchView():base(){
				_textLabel = new Label(){
					FontSize = BlrtStyles.CellTitleFont.FontSize,
					VerticalOptions = LayoutOptions.Center
				};
				_switch = new Switch();
				_switch.Toggled += (sender, e) => {
					Toggled?.Invoke(sender,e);
				};

				ColumnDefinitions = new ColumnDefinitionCollection{
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				};
				RowDefinitions = new RowDefinitionCollection{
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				};

				Children.Add(_textLabel,0,0);
				Children.Add(_switch, 1,0);
			}
		}
	}
}

