﻿using System;
using Xamarin.Forms;
using Parse;
using Acr.UserDialogs;
using System.Threading.Tasks;

#if __iOS__
using BlrtiOS;
#elif __ANDROID__
using BlrtApp.Droid;
using BlrtApp;
#endif


namespace BlrtData.Views.Profile
{
	public class FacebookLinkPage: BlrtContentPage
	{
		Button _linkBtn;
		Label _connectionLabel;
		StackLayout _unlinkArea;

		public event EventHandler OnOpenPasswordPage;

		public override string ViewName {
			get {
				return "MyProfile_ConnectService_Facebook";
			}
		}

		public FacebookLinkPage () : base ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_facebook_title", "profile");

			var message = new Label {
				Text = BlrtUtil.PlatformHelper.Translate("profile_fb_message","profile"),
				LineBreakMode = LineBreakMode.WordWrap,
				Font = BlrtStyles.CellTitleFont,
				TextColor = BlrtStyles.BlrtGray5
			};

			_linkBtn = new Button () {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				BackgroundColor = BlrtStyles.FacebookBlue,
				Image = "btn_fb.png",
				Text = BlrtUtil.PlatformHelper.Translate ("Connect with Facebook"),
				TextColor = BlrtStyles.BlrtWhite,
				FontSize = 18,
				WidthRequest = BlrtData.Views.Login.HomeLoginPage.BUTTON_WIDTH,
				HeightRequest = 42,
				IsVisible = false
			};

			var unLinkBtn = new Button () {
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtAccentRed,
				Text = BlrtUtil.PlatformHelper.Translate ("Disconnect"),
				FontSize = 18,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			_connectionLabel = new Label {
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			_unlinkArea = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Spacing = 10,
				Children = {
					new Image {
						Source = ImageSource.FromFile ("sendto_fb.png"),
						VerticalOptions = LayoutOptions.CenterAndExpand
					},
					_connectionLabel,
					unLinkBtn
				}
			};

			this.Content = new StackLayout () {
				Children = { message, _linkBtn, _unlinkArea },
				Padding = 20,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Spacing = 10,
				Orientation = StackOrientation.Vertical
			};

			unLinkBtn.Clicked += _unLinkBtn_Clicked;
			_linkBtn.Clicked += _linkBtn_Clicked;
			_unlinkArea.IsVisible = false;

			Xamarin.Forms.MessagingCenter.Subscribe<Object> (this, "FacebookLinkPage.UpdatePage", (o) => {
				UpdatePage ();
			});
			UpdatePage ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			UpdatePage ();
		}

		void _linkBtn_Clicked (object sender, EventArgs e)
		{
			#if __ANDROID__
			Xamarin.Facebook.Login.LoginManager.Instance.LogInWithReadPermissions (PlatformHelper.GetActivity (), BlrtData.BlrtConstants.FacebookExtendedPermissions);
			#elif __iOS__
			FBManager.LoginManagerInstance.LogInWithReadPermissions(BlrtData.BlrtConstants.FacebookExtendedPermissions, new BlrtFacebookLoginHandler().SystemCallback);
			#endif
		}

		void _unLinkBtn_Clicked (object sender, EventArgs e)
		{
			UnlinkFacebook ();
		}

		public async Task UnlinkFacebook ()
		{
			if (!FBManager.IsLinked) {
				using (UserDialogs.Instance.Loading (BlrtUtil.PlatformHelper.Translate ("Loading..."))) {
					try {
						await ParseUser.CurrentUser.BetterFetchAsync (new System.Threading.CancellationTokenSource (5000).Token);
					} catch {}
					UpdatePage ();
				}
				return;
			}

			if (BlrtUserHelper.HasSetPassword) {
				var alert = await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_confirm_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_confirm_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_confirm_accept", "profile"),
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_confirm_cancel", "profile")
				);

				if (alert) {
					try {
						using (UserDialogs.Instance.Loading (BlrtUtil.PlatformHelper.Translate ("Disconnecting..."))) {
							await FBManager.DisconnectWithFacebook ();
							BlrtData.Helpers.BlrtTrack.ProfileDisconnectService ("Facebook");
						}
					} catch (Exception fbEx) {
						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("facebook_unlink_failed_unknown_title", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_unlink_failed_unknown_message", "profile"),
							BlrtUtil.PlatformHelper.Translate ("facebook_unlink_failed_unknown_cancel", "profile")
						);
					}
				}
			} else {
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_password_set_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_password_set_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("facebook_unlink_password_set_accept", "profile")
				);

				//now we need get password

				#if __ANDROID__
				var profilePage = new BlrtApp.Views.Profile.EditProfileNavigation ();
				var passwordPage = profilePage.ShowEditPage (BlrtData.Views.Settings.Profile.ProfileMainPage.NavText.Password.ToString ());
				this.Navigation.PushAsync (passwordPage, true);

				passwordPage.Disappearing += (sender, e) => {
					if (BlrtUserHelper.HasSetPassword) {
						UnlinkFacebook ();
					}
				};
				#else
				OnOpenPasswordPage?.Invoke(this,EventArgs.Empty);
				#endif
			}
			UpdatePage ();
		}


		public void UpdatePage ()
		{
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				if (FBManager.IsLinked) {
					#if __ANDROID__
					var username = ((Device.Idiom == TargetIdiom.Phone) ? "\n" : "") 
						+ ((FBManager.FBConnected() && (null != Xamarin.Facebook.Profile.CurrentProfile))
							? Xamarin.Facebook.Profile.CurrentProfile.Name 
							: ParseUser.CurrentUser.Get<string>(BlrtUserHelper.FacebookMetaName, ""));
					#elif __iOS__
					var username = ((Device.Idiom == TargetIdiom.Phone) ? "\n" : "") 
						+ ((FBManager.FBConnected() && (null != Facebook.CoreKit.Profile.CurrentProfile))
							? Facebook.CoreKit.Profile.CurrentProfile.Name 
							: ParseUser.CurrentUser.Get<string>(BlrtUserHelper.FacebookMetaName, ""));
					#endif
					var labelTxt = BlrtUtil.AttributeString (
						new Span { 
							Text = string.Format (BlrtUtil.PlatformHelper.Translate ("profile_fb_unlink_title", "profile"), username),
							Font = Font.SystemFontOfSize (16d)
						},
						"#",
						new Span { Font = Font.SystemFontOfSize (16, FontAttributes.Bold)}
					);
					_connectionLabel.FormattedText = labelTxt;
					_linkBtn.IsVisible = false;
					_unlinkArea.IsVisible = true;
				} else {
					_linkBtn.IsVisible = true;
					_unlinkArea.IsVisible = false;
				}
			});
		}
	}
}

