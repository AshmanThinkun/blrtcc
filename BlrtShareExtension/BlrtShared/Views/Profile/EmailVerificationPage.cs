﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.Views.Profile
{
	public class EmailVerificationPage: BlrtContentPage
	{
		public event EventHandler OnEditProfileTapped;

		public override string ViewName {
			get {
				return "MyProfile_VerifyEmail";
			}
		}

		public EmailVerificationPage (): base()
		{
#if __ANDROID__
			BackgroundColor = BlrtStyles.BlrtGray2;
#endif
			this.Title = BlrtUtil.PlatformHelper.Translate("email_verify_screen_title", "profile");
			var icon = new Image () {
				HorizontalOptions = LayoutOptions.Center,
				Source = "icon_email.png",
			};

			var lbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("verify_email_when_you_signed", "profile") + "\n"+ ParseUser.CurrentUser.Email,
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = 280,
			};

			var boldLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("verify_email_click_link", "profile"),
				FontAttributes = FontAttributes.Bold,
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = 220,
			};

			var titleSection = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				Padding = new Thickness(0,20,0,35),
				Children = {
					icon, lbl, boldLbl
				}
			};

			var verificationBtn = new ActionRow () {
				Title = BlrtUtil.PlatformHelper.Translate("verify_email_find_email", "profile"),
				ButtonText = BlrtUtil.PlatformHelper.Translate("verify_email_resend", "profile")
			};
			verificationBtn.Clicked += SendVerificationEmail;

			var editProfileBtn = new ActionRow () {
				Title = BlrtUtil.PlatformHelper.Translate("verify_email_address_incorrect", "profile"),
				ButtonText = BlrtUtil.PlatformHelper.Translate("verify_email_edit_profile", "profile")
			};
			editProfileBtn.Clicked += (sender, e) => {
				OnEditProfileTapped?.Invoke(sender, e);
			};




			this.Content = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				RowSpacing = 0,
				Padding = 0,
				Children = {
					{ titleSection,0,0 },
					{ verificationBtn,0,1 },
					{ editProfileBtn,0,2 },
					{ new BoxView(){
							BackgroundColor = BlrtStyles.BlrtGray4,
							HeightRequest = 0.5,
							VerticalOptions = LayoutOptions.Start
						},0,1},
					{ new BoxView(){
							BackgroundColor = BlrtStyles.BlrtGray4,
							HeightRequest = 0.5,
							VerticalOptions = LayoutOptions.Start
						},0,2}
				}
			};
		}

		async void SendVerificationEmail (object sender, EventArgs e)
		{
			var alert = await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("confirmation_alert_title"),
				BlrtUtil.PlatformHelper.Translate ("confirmation_alert_message"),
				BlrtUtil.PlatformHelper.Translate ("confirmation_alert_accept"),
				BlrtUtil.PlatformHelper.Translate ("confirmation_alert_cancel")
			);

			// We may have to re-save the user if their primary email contact hasn't been linked yet
			Task<string> task = null;

			if (BlrtUserHelper.PrimaryEmail == null) {
				task = GetPrimaryContact ();

				try {
					task.Start ();
				} catch {
				}
			}

			if(alert){
				string contactId;

				if (task != null) {
					if (task.IsCompleted)
						contactId = task.Result;
					else
						contactId = await task;
				} else
					contactId = BlrtUserHelper.PrimaryEmail.ObjectId;

				BlrtUserHelper.ResendConfirmation (contactId);
			}
		}

		async Task<string> GetPrimaryContact ()
		{
			var currentUser = ParseUser.CurrentUser;

			var primaryContact = BlrtUserHelper.PrimaryEmail;

			if (primaryContact == null) {
				try {
					await BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery ().WaitAsync ();
				} catch {
					return null;
				}
			} else
				return primaryContact.ObjectId;

			primaryContact = BlrtUserHelper.PrimaryEmail;

			if (primaryContact == null)
				return null;

			return primaryContact.ObjectId;
		}


		class ActionRow: Grid{
			public string Title{
				get{
					return _lbl.Text;
				}set{
					_lbl.Text = value;
				}
			}
			public string ButtonText{
				get{
					return _btn.Text;
				}set{
					_btn.Text = value;
				}
			}

			public event EventHandler Clicked;
			Label _lbl;
			Button _btn;
			public ActionRow():base(){
				_lbl = new Label(){
					XAlign = TextAlignment.Center,
					FontSize = BlrtStyles.CellTitleFont.FontSize,
					FontAttributes = FontAttributes.Bold
				};

				_btn = new FormsBlrtButton(){
					HorizontalOptions = LayoutOptions.Fill,
					FontSize = BlrtStyles.CellTitleFont.FontSize,
					TextColor = BlrtStyles.BlrtAccentRed,
					IsUnderline = true,
					HeightRequest = Device.OS == TargetPlatform.iOS? 15: 20 ,
					Padding = 0,
					BackgroundColor = Color.Transparent
				};
				_btn.Clicked += (sender, e) => {
					Clicked?.Invoke(sender,e);
				};

				ColumnDefinitions = new ColumnDefinitionCollection(){
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				};
				RowDefinitions = new RowDefinitionCollection(){
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				};
				Padding = new Thickness(20,15);
				RowSpacing = 0;

				Children.Add(_lbl,0,0);
				Children.Add(_btn,0,1);
			}
		}
	}
}

