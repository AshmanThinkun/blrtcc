﻿using System;
using Xamarin.Forms;
using BlrtData;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using System.Linq;

namespace BlrtData.Views.Profile
{
	public class ConnectServicePage: BlrtContentPage
	{
		public const int RowHeight = 65;
		ListView _listView;
		public override string ViewName {
			get {
				return "MyProfile_ConnectService";
			}
		}

		public ConnectServicePage ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("connect_service_screen_title", "profile");
			var topSection = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				Padding = new Thickness (30, 15),
				Spacing = 10,
				Children = {
					new Image () {
						Source = "icon_service"
					},
					new Label () {
						Text = BlrtUtil.PlatformHelper.Translate ("connect_service_screen_description", "profile"),
						XAlign = TextAlignment.Center,
					}
				}
			};


			_listView = new FormsListView () {
				ItemTemplate = new DataTemplate (typeof (ProfileCell)),
				HeightRequest = 0,
				RowHeight = RowHeight,
				DisableScrolling = true,
				SeparatorVisibility = SeparatorVisibility.None,
				SeparatorColor = Color.Transparent
			};

			_listView.ItemSelected += _listView_ItemSelected;;

			var grid = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},

				RowSpacing = 0,
				ColumnSpacing = 0,
				Padding = new Thickness (0, 0),
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{topSection,0,0},
					{new BoxView(){
							HeightRequest = 0.5,
							BackgroundColor = BlrtStyles.BlrtGray4,
							VerticalOptions = LayoutOptions.End
						}, 0,0},
					{_listView,0,1}
				}
			};


			var scrollView = new ScrollView () {
				VerticalOptions = LayoutOptions.Fill,
				Orientation = ScrollOrientation.Vertical,
				Content = grid
			};

			this.Content = scrollView;

			this.BindingContextChanged += (sender, e) => {
				if(BindingContext==null)
					return;
				var b = BindingContext as List<ProfileCellModal>;
				if(b!=null)
					this.Bind(b);
			};
		}

		private void Bind(List<ProfileCellModal> model){
			_listView.ItemsSource = model;
			_listView.HeightRequest = model.Count * RowHeight;
		}

		void _listView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var modal = e.SelectedItem as ProfileCellModal;
			if(modal!=null){
				modal.Action?.Invoke ();
			}
			this._listView.SelectedItem = null;
		}
	}
}

