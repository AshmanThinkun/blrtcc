using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace BlrtData.Views.Share
{
	public class ShareController : BlrtContentPage
	{
		private Button fbInvite;
		private Image fbShare, twShare, emailShare, smsShare;
		private Label _introLbl;
		public EventHandler<string> fbShareClicked, twShareClicked, emailShareClicked, smsShareClicked, fbInviteClicked;

		public override string ViewName {
			get {
				return "Share";
			}
		}

		public ShareController () : base ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("share_screen", "screen_titles");


			var logoImg = new Image () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Source = "logo_green.png",
				WidthRequest = 90
			};

			_introLbl = new Label () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				FontSize = 18,
				Text = BlrtUtil.PlatformHelper.Translate ("share_message_new", "share"),
				TextColor = BlrtStyles.BlrtWhite,
				XAlign = TextAlignment.Center,
			};


			var shareLbl = new Label () {
				FontSize = 18,
				Text = BlrtUtil.PlatformHelper.Translate ("share_to", "share"),
				XAlign = TextAlignment.Center,
			};


			fbShare = new Image () {
				Source = "share_fb.png",
			};
			twShare = new Image () {
				Source = "share_tw.png",
			};
			emailShare = new Image () {
				Source = "share_email_sharepage.png",
			};
			smsShare = new Image () {
				Source = "share_sms.png",
			};

			var fbTap = new TapGestureRecognizer ();
			fbTap.Tapped += async (s, e) => {
				await GenerateLink("share_facebook");
				fbShareClicked?.Invoke (this, _url_share_facebook);
			};
			fbShare.GestureRecognizers.Add(fbTap);

			var twTap = new TapGestureRecognizer ();
			twTap.Tapped += async (s, e) => {
				await GenerateLink("share_twitter");
				twShareClicked?.Invoke (this, _url_share_twitter);
			};
			twShare.GestureRecognizers.Add(twTap);

			var emailTap = new TapGestureRecognizer ();
			emailTap.Tapped += async (s, e) => {
				await GenerateLink("share_email");
				emailShareClicked?.Invoke (this, _url_share_email);
			};
			emailShare.GestureRecognizers.Add(emailTap);

			var smsTap = new TapGestureRecognizer ();
			smsTap.Tapped += async (s, e) => {
				await GenerateLink("share_sms");
				smsShareClicked?.Invoke (this, _url_share_sms);
			};
			smsShare.GestureRecognizers.Add (smsTap);


			var shareIconsSection = new StackLayout () {
				Spacing = 10,
				Padding = new Thickness (0, -5, 0, 10),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Orientation = StackOrientation.Horizontal,
				Children = {
					fbShare,
					twShare,
					emailShare,
					smsShare
				},
			};

			var inviteLbl = new Label () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = 18,
				Text = BlrtUtil.PlatformHelper.Translate ("share_invite_fb", "share"),
				XAlign = TextAlignment.Center,
			};

			fbInvite = new Button () {
				Image = "btn_fb.png",
				Text = "invite",
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = BlrtStyles.FacebookBlue,
				TextColor = BlrtStyles.BlrtWhite,
				WidthRequest = 100,
				HorizontalOptions = LayoutOptions.Center
			};

			fbInvite.Clicked += async (sender, e) => {
				await GenerateLink ("share_facebook");
				if (fbInviteClicked != null)
					fbInviteClicked (this, _url_share_facebook);
			};



			var blackSection = new StackLayout () {
				Spacing = 25,
				VerticalOptions = LayoutOptions.End,
				HorizontalOptions = LayoutOptions.Fill,
				BackgroundColor = BlrtStyles.BlrtCharcoalShadow,
				Children = {
					logoImg,
					_introLbl,
				},
			};

			var whiteSection = new StackLayout () {
				Spacing = 30,
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.Center,

				Children = {
					shareLbl,
					shareIconsSection,
					inviteLbl,
					fbInvite,
				},
			};

			Content = new ScrollView () {
				Content = new Grid () {

					ColumnDefinitions = {
						new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					},
					RowDefinitions = {
						new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
						new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
					},
					RowSpacing = 0,
					Padding = 0,
					Children = {
						{
							new ContentView(){
								VerticalOptions = LayoutOptions.Fill,
								HorizontalOptions = LayoutOptions.Fill,

								Padding = new Thickness(30,38,30,40),
								BackgroundColor = BlrtStyles.BlrtCharcoalShadow,

								Content = blackSection
							},0, 0
						},
						{
							new ContentView(){
								VerticalOptions = LayoutOptions.Fill,
								HorizontalOptions = LayoutOptions.Fill,

								Padding = new Thickness(20,42,20,42),
								BackgroundColor = BlrtStyles.BlrtWhite,

								Content = whiteSection
							}, 0, 1
						},
					},
				},
			};
		}

		protected override void LayoutChildren (double x, double y, double width, double height)
		{
			int introWidth = 300;
			var deviceWidth = BlrtUtil.PlatformHelper.GetDeviceWidth ();
			if (deviceWidth >= 768 && deviceWidth < 1024) {
				if (_introLbl.WidthRequest != introWidth) {
					_introLbl.WidthRequest = introWidth;
				}
			} else if (_introLbl.WidthRequest != -1) {
				_introLbl.WidthRequest = -1;
			}
			base.LayoutChildren (x, y, width, height);
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			PrepareUrl ();
		}

		string _url_share_sms, _url_share_facebook, _url_share_twitter, _url_share_email, _url_share_other = "";
		public void PrepareUrl ()
		{
			// we can hook this with the bubble on appearing method as well
			GenerateLink ("share_sms").FireAndForget ();
			GenerateLink ("share_facebook").FireAndForget ();
			GenerateLink ("share_twitter").FireAndForget ();
			GenerateLink ("share_email").FireAndForget ();
			GenerateLink ("share_other").FireAndForget ();
		}

		async Task GenerateLink (string urlType)
		{
			switch (urlType) {
				case "share_sms":
					if (string.IsNullOrEmpty (_url_share_sms) || !_url_share_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_share_sms = await BlrtUtil.GenerateYozioShareUrl (urlType);
					}
					break;
				case "share_facebook":
					if (string.IsNullOrEmpty (_url_share_facebook) || !_url_share_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_share_facebook = await BlrtUtil.GenerateYozioShareUrl (urlType);
					}
					break;
				case "share_twitter":
					if (string.IsNullOrEmpty (_url_share_twitter) || !_url_share_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_share_twitter = await BlrtUtil.GenerateYozioShareUrl (urlType);
					}
					break;
				case "share_email":
					if (string.IsNullOrEmpty (_url_share_email) || !_url_share_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_share_email = await BlrtUtil.GenerateYozioShareUrl (urlType);
					}
					break;
				case "share_other":
					if (string.IsNullOrEmpty (_url_share_other) || !_url_share_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_share_other = await BlrtUtil.GenerateYozioShareUrl (urlType);
					}
					break;
			}
		}
	}
}

