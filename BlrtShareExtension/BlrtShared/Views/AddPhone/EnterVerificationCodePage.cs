﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlrtData.Views.AddPhone
{
	public class EnterVerificationCodePage: BlrtContentPage
	{
		public override string ViewName {
			get {
				return "MyProfile_ConnectService_Phone";
			}
		}

		Label _titleLbl;
		Entry _phoneCodeEntry;
		Label[] digits;

		const int NumberOfDigits = 4;

		public string PhoneNumber{ get; set;}
		public event EventHandler Verified;
		public EnterVerificationCodePage ():base()
		{
			this.BackgroundColor = BlrtStyles.BlrtWhite;

			_titleLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("phone_verify_title", "profile"),
				XAlign = TextAlignment.Center
			};

			_phoneCodeEntry = new FormsEntry () {
				Keyboard = Keyboard.Numeric,
				CornerRadious = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				WidthRequest = 0,
				HeightRequest = 0
			};

			var digitLayout = new StackLayout () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Orientation = StackOrientation.Horizontal,
				GestureRecognizers = {
					new TapGestureRecognizer(){
						Command = new Command((o)=>{
							Device.BeginInvokeOnMainThread(()=>{
								if(!_phoneCodeEntry.IsFocused)
									_phoneCodeEntry.Focus();
							});
						})
					}
				}
			};


			digits = new Label[NumberOfDigits];
			for(int i=0 ; i<NumberOfDigits; i++){
				digits [i] = new Label () {
					Text = "-",
					XAlign = TextAlignment.Center,
					WidthRequest = 30,
					FontSize = 30,
				};
				digitLayout.Children.Add (digits [i]);
			}

			var stack = new StackLayout () {
				Padding = new Thickness (10, 0, 0, 0),
				Children = {
					new ContentView(){
						Padding = new Thickness(5, 50, 15, 40),
						Content = _titleLbl
					},
					digitLayout,
					new BoxView(){
						BackgroundColor = BlrtStyles.BlrtGray4,
						HeightRequest = 0.5f
					},
					new BoxView () {
						HeightRequest = 30,
						GestureRecognizers = {
							new TapGestureRecognizer(){
								Command = new Command((o)=>{
									Device.BeginInvokeOnMainThread(()=>{
										if(!_phoneCodeEntry.IsFocused)
											_phoneCodeEntry.Focus();
									});
								})
							}
						}
					},
					new BoxView(){
						BackgroundColor = BlrtStyles.BlrtGray4,
						HeightRequest = 0.5f
					},
					_phoneCodeEntry
				}
			};
			this.Content = stack;

			_phoneCodeEntry.TextChanged += _phoneCodeEntry_TextChanged;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			Device.BeginInvokeOnMainThread (() => {
				_phoneCodeEntry.Focus ();
			});
		}

		async void _phoneCodeEntry_TextChanged (object sender, TextChangedEventArgs e)
		{
			var chars = new List<char>();
			foreach(var c in _phoneCodeEntry.Text){
				if(Char.IsDigit(c) && chars.Count< NumberOfDigits){
					chars.Add (c);
				}
			}

			_phoneCodeEntry.Text = string.Join ("", chars);

			for(int i= 0 ;i<NumberOfDigits; i++){
				if(chars.Count>i){
					digits [i].Text = chars [i].ToString();
				}else{
					digits [i].Text = "-";
				}
			}


			if(chars.Count==NumberOfDigits){
				//do verification
				if(Device.OS == TargetPlatform.Android){
					_phoneCodeEntry.Unfocus ();
				}
				if(await AddNumber (PhoneNumber, _phoneCodeEntry.Text)){
					Verified?.Invoke (this, EventArgs.Empty);
				}
			}
		}

		async Task<bool> AddNumber(string phoneNumber, string code){
			using (Acr.UserDialogs.UserDialogs.Instance.Loading ("")) {
				var request = new BlrtAPI.APIUserContactDetailCreate (new BlrtAPI.APIUserContactDetailCreate.Parameters () {
					PhoneNumber = phoneNumber,
					Extra = new Dictionary<string,object> () {
						{ "verificationCode", code }
					}
				});

				try {
					var result = await request.Run (new System.Threading.CancellationTokenSource (15000).Token);
					if(!result){
						throw new Exception("connection error");
					}

					if (request.Response.Success) {
						var t1 =  BlrtData.Query.BlrtSettingsQuery.RunQuery ().WaitAsync ();
						var t2 =  BlrtData.Query.BlrtParseActions.GetContactDetails(new System.Threading.CancellationTokenSource(15 * 1000).Token);
						var t3 =  BlrtData.Query.BlrtInboxQuery.RunQuery(BlrtData.Query.BlrtInboxQueryType.Inbox); //dont wait for this query
						await Task.WhenAll(t1,t2);

						BlrtData.Helpers.BlrtTrack.ProfileConnectService("Phone");
						BlrtUserHelper.IdentifyUser();
						return true;
					} else if (request.Response.Error.Code == BlrtAPI.APIUserContactDetailCreateError.DetailInUse) {
						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("phone_inuse_title", "profile"), 
							BlrtUtil.PlatformHelper.Translate ("phone_inuse_message", "profile"), 
							BlrtUtil.PlatformHelper.Translate ("OK")
						);
					} else if (request.Response.Error.Code == BlrtAPI.APIError.InvalidParameters){
						DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("phone_invalid_code_title", "profile"), 
							BlrtUtil.PlatformHelper.Translate (""), 
							BlrtUtil.PlatformHelper.Translate ("OK")
						);	
					}else {
						throw request.Exception;
					}
				} catch(Exception ee) {
					BlrtUtil.Debug.ReportException (request.Exception);
					var retry = await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);

					if(retry){
						return await AddNumber (phoneNumber, code);
					}
				}
				return false;
			}
		}
	}
}

