﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using BlrtData.Views.Settings.Profile;

namespace BlrtData.Views.AddPhone
{
	public class ChooseCountryPage: BlrtContentPage
	{
		public override string ViewName {
			get {
				return "MyProfile_ConnectService_Phone";
			}
		}

		public event EventHandler<string> CountrySelected;
		IEnumerable<string> _supportCountries;
		List<DataItem> _source;
		List<DataItem> _currentsource;


		SearchBar _search;
		ListView _listView;

		public ChooseCountryPage(IEnumerable<string> supportCountries, string selectCountry):base(){
			_supportCountries = supportCountries;
			_source = new List<DataItem> ();

			this.BackgroundColor = BlrtStyles.BlrtWhite;

			_listView = new ListView () {
				ItemTemplate = new DataTemplate(typeof(ProfileMainPageCell)),
			};

			_search = new FromsSearchBar () {
				Placeholder = "Search",
				BackgroundColor = BlrtStyles.BlrtWhite,
			};

			_search.TextChanged += Search_TextChanged;
			_listView.ItemSelected += ListView_ItemSelected;
			_listView.Header = _search;

			this.Content = _listView;

			InitSource (selectCountry);
		}

		void InitSource(string selectCountry){
			DataItem selectIndex = null;
			foreach(var code in _supportCountries){
				var data = new DataItem () {
					Code = code,
					CountryName = BlrtConstants.CountryNameDic [code],
					PhonePrefix = BlrtUtil.GetPhoneCountryPrefix (code),
					Title = ProfileMainPageCell.GetFormattedString (BlrtConstants.CountryNameDic [code]),
					Text = ProfileMainPageCell.GetFormattedString (BlrtUtil.GetPhoneCountryPrefix (code), ProfileMainPageCell.CellTextGrayColor, BlrtStyles.CellTitleFont)
				};

				_source.Add (data);

				if(code==selectCountry){
					selectIndex = data;
				}
			}

			_listView.ItemsSource = _currentsource = _source;
			_listView.SelectedItem = selectIndex;
			_listView.ScrollTo (selectIndex, ScrollToPosition.MakeVisible, Device.OS != TargetPlatform.Android);
		}

		void Search_TextChanged (object sender, TextChangedEventArgs e)
		{
			var searchText = (_search.Text ?? "").Trim ().ToLower ();
			var newSoucre = new List<DataItem> ();

			foreach(var data in _source){
				if(data.CountryName.ToLower().IndexOf(searchText)!=-1){
					newSoucre.Add (data);
				}
			}
			_listView.ItemsSource = _currentsource = newSoucre;
		}

		void ListView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var data = e.SelectedItem as DataItem;
			if(data !=null){
				CountrySelected?.Invoke (sender, data.Code);
			}
		}

		class DataItem{
			public string Code{get;set;}
			public string CountryName{get;set;}
			public string PhonePrefix{get;set;}

			public FormattedString Title{ get; set;}
			public FormattedString Text{ get; set;}

			public bool ShowSwitch{get{return false;}}
		}
	}

	public class FromsSearchBar:SearchBar{
		
	}
}

