﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtData.Views.AddPhone
{
	public class EnterNumberPage : BlrtContentPage
	{
		public override string ViewName {
			get {
				return "MyProfile_ConnectService_Phone";
			}
		}

		Label _titleLbl;
		Picker _countryPicker;
		Label _countryNameLbl;
		Label _countryCodeLbl;
		FormsEntry _phoneNumberEntry;
		Button _skipBtn;

		public Button SkipButton {
			get {
				return _skipBtn;
			}
		}

		public event EventHandler<string> TitleChanged;
		public event EventHandler SkipPressed;

		public new string Title {
			get {
				return base.Title;
			}
			set {
				base.Title = value;
				TitleChanged?.Invoke (this, base.Title);
			}
		}

		public string PhoneNumber {
			get {
				return BlrtUtil.ToPhoneNumberE164 (_phoneNumberEntry.Text, SelectedCountryCode);
			}
		}

		public EnterNumberPage () : base ()
		{
			this.BackgroundColor = BlrtStyles.BlrtWhite;
			_titleLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("enter_number_page_label_title", "profile"),
				XAlign = TextAlignment.Center,
				FontSize = 17,
			};

			_countryPicker = new FormsPicker () {
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			_countryNameLbl = new Label () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = 17,
				TextColor = new Color (0, 0.478431, 1)
			};

			_countryCodeLbl = new Label () {
				FontSize = 17,
			};

			_phoneNumberEntry = new FormsEntry () {
				Placeholder = BlrtUtil.PlatformHelper.Translate ("phone_placeholder", "profile"),
				Keyboard = Keyboard.Numeric,
				CornerRadious = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HideBottomDrawable = true,
				FontSize = 17
			};

			if (Device.OS == TargetPlatform.Android) {
				_phoneNumberEntry.ContentEdgeInsets = 0;
			}

			_skipBtn = new Button () {
				TextColor = BlrtStyles.BlrtGray4,
				FontSize = BlrtStyles.CellContentFont.FontSize,
				Text = BlrtUtil.PlatformHelper.Translate ("addPhone_skip", "profile"),
				IsVisible = false,
				BackgroundColor = Color.Transparent
			};
			_skipBtn.Clicked += (sender, e) => {
				SkipPressed?.Invoke (sender, e);
			};

			var stack = new StackLayout () {
				Padding = new Thickness (10, 0, 0, 0),
				Children = {
					new ContentView(){
						Padding = new Thickness(5, 50, 15, 50),
						Content = _titleLbl
					},
					new BoxView(){
						BackgroundColor = BlrtStyles.BlrtGray4,
						HeightRequest = 0.5f
					},
					new StackLayout(){
						Orientation = StackOrientation.Horizontal,
						Padding = new Thickness(0,5, 3, 5),
						Children = {
							_countryNameLbl,
							new Image(){
								Source = "settings_arrow.png",
								HorizontalOptions = LayoutOptions.End
							}
						}
					},
					new BoxView(){
						BackgroundColor = BlrtStyles.BlrtGray4,
						HeightRequest = 0.5f
					},
					new StackLayout () {
						Orientation = StackOrientation.Horizontal,
						Padding = new Thickness(0,5),
						Children = {
							_countryCodeLbl, _phoneNumberEntry
						}
					},
					new BoxView(){
						BackgroundColor = BlrtStyles.BlrtGray4,
						HeightRequest = 0.5f
					},
					_skipBtn
				}
			};
			this.Content = stack;

			_countryPicker.SelectedIndexChanged += _countryPicker_SelectedIndexChanged;
			_phoneNumberEntry.TextChanged += _phoneNumberEntry_TextChanged;
			_countryNameLbl.GestureRecognizers.Add (new TapGestureRecognizer () {
				Command = new Command ((o) => {
					OpenCountryPage?.Invoke (this, EventArgs.Empty);
				})
			});

			Reload ();
		}

		public void Unfocus ()
		{
			base.Unfocus ();
			_phoneNumberEntry.Unfocus ();
		}

		public event EventHandler OpenCountryPage;

		public IEnumerable<string> SupportCountryCodes {
			get {
				return _supportCountryCode;
			}
		}

		public string SelectedCountryCode {
			get {
				return BlrtConstants.CountryCodeDic [_countryPicker.Items.ElementAt (_countryPicker.SelectedIndex)];
			}
			set {
				_countryPicker.SelectedIndex = Array.IndexOf (_countryPicker.Items.ToArray (), BlrtConstants.CountryNameDic [value]);
				_countryNameLbl.Text = BlrtConstants.CountryNameDic [value];
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			Device.BeginInvokeOnMainThread (() => {
				_phoneNumberEntry.Focus ();
			});
		}

		void _phoneNumberEntry_TextChanged (object sender, TextChangedEventArgs e)
		{
			this.Title = _countryCodeLbl.Text + " " + _phoneNumberEntry.Text;
		}

		void _countryPicker_SelectedIndexChanged (object sender, EventArgs e)
		{
			//get code
			var code = BlrtConstants.CountryCodeDic [_countryPicker.Items.ElementAt (_countryPicker.SelectedIndex)];

			//set phone prefix
			var countryPrefix = BlrtUtil.GetPhoneCountryPrefix (code);
			_countryCodeLbl.Text = countryPrefix;

			_phoneNumberEntry_TextChanged (sender, new TextChangedEventArgs (_phoneNumberEntry.Text, _phoneNumberEntry.Text));
		}


		string [] _supportCountryCode;

		public void Reload ()
		{
			//get country list
			_supportCountryCode = BlrtSettings.PhoneNumberSupportedCountries;
			if (_supportCountryCode.Length == 0) {
				_supportCountryCode = new string [] { "AU" };
			}

			foreach (var code in _supportCountryCode) {
				_countryPicker.Items.Add (BlrtConstants.CountryNameDic [code]);
			}


			var selectIndex = Array.IndexOf (_countryPicker.Items.ToArray (), BlrtConstants.CountryNameDic [BlrtUtil.PlatformHelper.CountryCode]);
			if (selectIndex == -1) {
				selectIndex = 0;
			}
			_countryPicker.SelectedIndex = selectIndex;
			_countryPicker_SelectedIndexChanged (this, EventArgs.Empty);
			_countryNameLbl.Text = BlrtConstants.CountryNameDic [BlrtUtil.PlatformHelper.CountryCode];
		}


		public async Task<bool> Done ()
		{
			_phoneNumberEntry.Unfocus ();

			//validate phone number
			var country = _countryPicker.Items.ElementAt (_countryPicker.SelectedIndex);
			var code = BlrtConstants.CountryCodeDic [country];
			if (!BlrtUtil.IsValidPhoneNumber (_phoneNumberEntry.Text, code)) {
				//alert
				await DisplayAlert (this.Title, BlrtUtil.PlatformHelper.Translate ("invalid_phone_message", "profile"), BlrtUtil.PlatformHelper.Translate ("OK"));
				return false;
			}

			//confirmation alert
			var result = await DisplayAlert (
				this.Title,
				BlrtUtil.PlatformHelper.Translate ("phone_confirm_message", "profile"),
				BlrtUtil.PlatformHelper.Translate ("phone_confirm_accept", "profile"),
				BlrtUtil.PlatformHelper.Translate ("phone_confirm_cancel", "profile")
			);

			if (!result) {
				_phoneNumberEntry.Focus ();
				return false;
			}

			//send request
			var number = BlrtUtil.ToPhoneNumberE164Tuple (_phoneNumberEntry.Text, code);
			return await RequestPhoneCode (number.Item2, number.Item1);
		}

		public async Task<bool> RequestPhoneCode (string phoneNumber, string countryCode)
		{
			try {
				using (Acr.UserDialogs.UserDialogs.Instance.Loading ("")) {
					var request = new BlrtAPI.APIRequestPhoneVerificationCode (new BlrtAPI.APIRequestPhoneVerificationCode.Parameters () {
						phoneNumber = phoneNumber,
						countryCode = countryCode
					});

					var result = await request.Run (new System.Threading.CancellationTokenSource (15000).Token);

					if (!result)
						throw new Exception ("connection error");
					if (request.Response.Success) {
						return true;
					}
					if (request.Response.Error.Code == BlrtAPI.APIRequestPhoneVerificationCodeError.detailInUse) {
						await DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("phone_inuse_title", "profile"),
							BlrtUtil.PlatformHelper.Translate ("phone_inuse_message", "profile"),
							BlrtUtil.PlatformHelper.Translate ("phone_inuse_cancel", "profile")
						);
						_phoneNumberEntry.Focus ();
					} else {
						throw new Exception ("connection error");
					}
				}
			} catch (Exception) {
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK")
				);
			}
			return false;
		}
	}

	public enum OpenFrom
	{
		Profile,
		Signup,
		SingupWithSMS
	}

	public class FormsPicker : Picker { }
}

