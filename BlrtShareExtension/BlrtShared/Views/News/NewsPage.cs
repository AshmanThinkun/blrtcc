using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.News
{
	#if __ANDROID__
	using BlrtApp.Views;
	public class NewsPage: BlrtContentPage, IBackStatePage
	{
		public BackState BackState { get { return BackState.Finish; } }
	#else
	public class NewsPage: BlrtContentPage
	{
	#endif
		static readonly int rowHeight = NewsPageCell.ImageHeight + 1;
		public const int contentMaxLength = 90;
		const int loadingHeight = 34;

		static readonly Font CellContentFont = BlrtStyles.CellContentFont;
		private ActivityIndicator _loading;

		public EventHandler<string> ReadMoreClicked{ get; set; }

		private readonly TimeSpan	ProfileNewsUpdateFrequency = new TimeSpan (0, 5, 0);
		private static DateTime _lastTimeNewsUpdated;

		private ListView _listView;

		public override string ViewName {
			get {
				return "News";
			}
		}

		public NewsPage () : base ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("news_screen", "screen_titles");

			_loading = new ActivityIndicator () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				IsRunning = true,
				IsEnabled = false,
				IsVisible = false,
				HeightRequest = loadingHeight,
			};

			_listView = new FormsListView {
				RowHeight = rowHeight,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				SeparatorInset = new Thickness (NewsPageCell.ImageWidth + 10, 0, 0, 0),
				HasUnevenRows = true
			};
			_listView.ItemsSource = null;

			_listView.ItemTemplate = new DataTemplate (typeof(NewsPageCell));

			_listView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null) {
					return;
				}
				_listView.SelectedItem = null;
				var url = (e.SelectedItem as News).URL;
				if (ReadMoreClicked != null && !string.IsNullOrEmpty (url))
					ReadMoreClicked (this, url);
			};


			Content = new Grid () {
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
				},
				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) }
				},
				Children = {
					{ _listView, 0, 0 },
					{ _loading, 0, 0 }
				}
			};
		}

		#if __IOS__
		const string platform = "ios";
		#else
		const string platform = "android";
		#endif

		public async Task<bool> RefreshItemsSource ()
		{
			List<BlrtNewsItemModel> models;
			_loading.IsEnabled = true;
			_loading.IsVisible = true;
			if (_lastTimeNewsUpdated < DateTime.Now - ProfileNewsUpdateFrequency) {
				try {
					models = await BlrtNewsItemModel.FetchFromUrlAsync (BlrtSettings.NewsFeedUrl, "profile_news");
					_lastTimeNewsUpdated = DateTime.Now;
				} catch {
					//diplay error
					DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("request_get_failed_title"),
						BlrtUtil.PlatformHelper.Translate ("request_get_failed_message"),
						BlrtUtil.PlatformHelper.Translate ("request_get_failed_cancel")
					);
					models = new List<BlrtNewsItemModel> ();
					_lastTimeNewsUpdated = new DateTime ();
				}
			} else {
				models = BlrtNewsItemModel.FetchFromCache ("profile_news");
			}
			var result = new List<News> ();
			foreach (var item in models) {
				if (null == item.filter || item.filter.Length <= 0 || Array.IndexOf (item.filter, platform) >= 0) {
					var convertedNews = new News (item.title, item.image, item.text, item.url, item.readMoreText);
					result.Add (convertedNews);
				}
			}
			_listView.ItemsSource = result.ToArray ();
			_loading.IsEnabled = false;
			_loading.IsVisible = false;
			return true;
		}

		public class News : BlrtNotifyProperty
		{

			string _title, _imageUri, _url;
			FormattedString _text, _readMore;

			public string Title {
				get { return _title; }
				set {
					OnPropertyChanged (ref _title, value);
				}
			}

			public FormattedString Text {
				get { return _text; }
				set {
					OnPropertyChanged (ref _text, value);
				}
			}

			public FormattedString ReadMore {
				get { return _readMore; }
				set {
					OnPropertyChanged (ref _readMore, value);
				}
			}

			public string ImageUri {
				get { return _imageUri; }
				set {
					OnPropertyChanged (ref _imageUri, value);
				}
			}

			public string URL {
				get { return _url; }
				set {
					OnPropertyChanged (ref _url, value);
				}
			}

			public News (string title, string imageUri, string text, string url, string readMoreText)
			{

				setUp (title, imageUri, text, url, readMoreText);
			}

			public News (string title, string imageUri, string text)
			{
				setUp (title, imageUri, text, "");
			}

			private void setUp (string title, string imageUri, string text, string url, string readMoreText = "")
			{

				Title = title;
				ImageUri = imageUri;
				var fs = new FormattedString ();
				fs.Spans.Add (
					new Span {
						Text = text, 
						FontFamily = CellContentFont.FontFamily,
						FontAttributes = CellContentFont.FontAttributes,
						FontSize = CellContentFont.FontSize
					}
				);
				if (!String.IsNullOrEmpty (url)) {
					var read = new FormattedString ();
					read.Spans.Add (
						new Span {
							Text = string.IsNullOrEmpty(readMoreText)? BlrtUtil.PlatformHelper.Translate ("news_read_more"): readMoreText,
							ForegroundColor = BlrtStyles.BlrtAccentBlue,
							FontFamily = CellContentFont.FontFamily,
							FontAttributes = CellContentFont.FontAttributes,
							FontSize = CellContentFont.FontSize
						}
					);
					ReadMore = read;
				}
				Text = fs;
				URL = url;
			}
		}
	}
}

