using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.News
{
	public class NewsPageCell : ViewCell
	{
		Image _image;
		Label _titleLabel, _textLabel, _readMoreLabel;

		public static readonly int ImageHeight = 118;
		public static readonly int ImageWidth = ImageHeight;
		static readonly Font TitleFont = BlrtStyles.CellTitleFont;
		static readonly Font TextFont = BlrtStyles.CellContentFont;


		public NewsPageCell ()
		{
			_image = new Image () {
				HorizontalOptions = LayoutOptions.Start
			};
			_image.WidthRequest = ImageWidth;
			_image.HeightRequest = ImageHeight;

			_titleLabel = new Label {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontFamily = TitleFont.FontFamily,
				FontAttributes = TitleFont.FontAttributes,
				FontSize = TitleFont.FontSize
			};

			_textLabel = new FormsLabel {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Fill,
				FontFamily = TextFont.FontFamily,
				FontAttributes = TextFont.FontAttributes,
				FontSize = TextFont.FontSize,
				#if __IOS__
				LineBreakMode = LineBreakMode.TailTruncation
				#else
				LineBreakMode = LineBreakMode.CharacterWrap
				#endif
			};

			_readMoreLabel = new Label () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				FontFamily = TextFont.FontFamily,
				FontAttributes = TextFont.FontAttributes,
				FontSize = TextFont.FontSize,
			};


			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},
				RowSpacing = 0,
				ColumnSpacing = 0,
				Padding = 0,

				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{ _image, 0,0 }, { 
						new Grid () {
							ColumnDefinitions = {
								new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
							},
							RowDefinitions = {
								new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
								new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
								new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
							},
							HorizontalOptions = LayoutOptions.Fill,
							VerticalOptions = LayoutOptions.Fill,
							Padding = new Thickness (10),
							RowSpacing = 4,
							Children = { 
								{ _titleLabel,0,0 },
								{ _textLabel,0,1 },
								{ _readMoreLabel,0,2 }
							}
						},1,0
					}, 
				}
			};

			_image.SetBinding (Image.SourceProperty, "ImageUri");
			_titleLabel.SetBinding (Label.TextProperty, "Title");
			_textLabel.SetBinding (Label.FormattedTextProperty, "Text");
			_readMoreLabel.SetBinding (Label.FormattedTextProperty, "ReadMore");
		}
	}
}

