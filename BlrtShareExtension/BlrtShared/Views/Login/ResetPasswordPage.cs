﻿using System;
using Xamarin.Forms;
using Acr.UserDialogs;
using System.Threading.Tasks;
using BlrtData;
using BlrtData.Views.CustomUI;
using BlrtData.Helpers;

namespace BlrtData.Views.Login
{
	public class ResetPasswordPage : BaseLoginPage
	{
		Entry		_username;
		StackLayout _bottomSection;

		public override string ViewName {
			get {
				return "Login_Email_ForgotPassword";
			}
		}

		public ResetPasswordPage ()
		{
			NavigationPage.SetHasNavigationBar (this, false);
			var logo = new Image () {
				Source					= "blrt_tpd.png",
				HorizontalOptions		= LayoutOptions.Center,
				VerticalOptions			= LayoutOptions.End,
			};

			_username = new FormsEntry () {
				HideBottomDrawable = true,
				Keyboard = Keyboard.Email,
				Placeholder = BlrtUtil.PlatformHelper.Translate("Email"),
				HorizontalOptions		= LayoutOptions.Center,
				WidthRequest = HomeLoginPage.BUTTON_WIDTH,
				BackgroundColor = BlrtStyles.BlrtWhite,
				ReturnKey = ReturnKeyType.Done
			};

			var resetPasswordBtn = new Button () {
				HorizontalOptions = LayoutOptions.Center,
				BackgroundColor = BlrtStyles.BlrtGreen,
				Text = BlrtUtil.PlatformHelper.Translate ("Reset password"),
				TextColor = Device.OS == TargetPlatform.Android? BlrtStyles.BlrtWhite: BlrtStyles.BlrtCharcoal,
				FontSize = 18,
				WidthRequest = HomeLoginPage.BUTTON_WIDTH,
				HeightRequest = 42,
			};

			//Layout stuff
			_bottomSection = new StackLayout () {
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness (0, 12, 0 , 16),
				BackgroundColor = BlrtStyles.BlrtGray2,
				Spacing = 12,
				MinimumHeightRequest = 220,
				Children = {
					_username,
					resetPasswordBtn
				}
			};

			var logoBotSpace = Device.Idiom == TargetIdiom.Phone ? 60 : 30;
			if(Device.OS == TargetPlatform.iOS){
				logoBotSpace = Device.Idiom == TargetIdiom.Phone ? 50 : 30;
				if (Device.Idiom == TargetIdiom.Phone && BlrtUtil.PlatformHelper.GetDeviceHeight () < 481)
					logoBotSpace = 15;
			}

			var logoSection = new ContentView () {
				Padding = new Thickness (0, 0, 0, logoBotSpace),
				Content = logo
			};

			var content = new Grid () {
				ColumnDefinitions = new ColumnDefinitionCollection () {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
				},
				RowDefinitions = new RowDefinitionCollection () {
					new RowDefinition () { Height = new GridLength (5, GridUnitType.Star) },
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Auto) },
				},
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				BackgroundColor = BlrtStyles.BlrtGreen,
				Padding = 0,
				RowSpacing = 0,
				Children = {
					{logoSection,0,0},
					{_bottomSection, 0, 1},
				}
			};

			this.Content = new ScrollView () {
				Content = content
			};

			this.KeyboardAppeared += async (sender,e) =>{
				await Task.Delay(10);
				var s = this.Content as ScrollView;
				if(this.KeyboardAppearing){
					var offset = logoSection.Height +  resetPasswordBtn.Bounds.Bottom + 10 + e  -this.Height;
					if(offset>0){
						await s.ScrollToAsync(0,offset,true);
					}
				}else{
					await s.ScrollToAsync(0,0,true);
				}
			};

			_username.Completed += OpenResetPassword;
			resetPasswordBtn.Clicked	+= OpenResetPassword;
		}

		protected override void OnAppearing ()
		{
			#if __ANDROID__
			if (Device.Idiom == TargetIdiom.Phone) {
				BlrtApp.PlatformHelper.LockScreenOrientation (BlrtApp.PlatformHelper.ScreenOrientation.UserPortrait);
			}
			#endif
			base.OnAppearing ();
		}

		protected override void OnSizeAllocated (double width, double height)
		{
			_bottomSection.HeightRequest = Math.Max(height * 0.45f, 220);
			base.OnSizeAllocated (width, height);
		}

		async void OpenResetPassword (object sender, EventArgs e) {

			//hide keyboard
			_username.Unfocus ();

			using (UserDialogs.Instance.Loading(BlrtUtil.PlatformHelper.Translate ("Resetting..."))) {
				var loginDetail = new LoginHelper.LoginDetails (_username.Text, "");

				if (await ResetPasswordCheck (loginDetail)) {
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate("alert_reset_success_title","login"),
						BlrtUtil.PlatformHelper.Translate("alert_reset_success_message","login"),
						BlrtUtil.PlatformHelper.Translate("alert_reset_success_accept", "login")
					);
					#if __ANDROID__
					Navigation.PopAsync ();
					#elif __IOS__
					var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
					app.LoginNav.PopViewController(true);
					#endif
				}
			}
		}
			

		/// <summary>
		/// Register the user, and handles showing alerts and refiring the register if it fails.
		/// </summary>
		/// <param name="details">Details.</param>
		async Task<bool> ResetPasswordCheck(LoginHelper.LoginDetails details)
		{
			var result = await LoginHelper.ResetPassword (details);

			bool retryRegister = false;

			switch (result) {

				case LoginHelper.Result.Success:
					return true;

					break;
				case LoginHelper.Result.InvalidParameters:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Email Invalid"), 
						BlrtUtil.PlatformHelper.Translate ("Please make sure you have entered the correct email address."), 
						BlrtUtil.PlatformHelper.Translate ("OK")
					);
					break;


				case LoginHelper.Result.InternetProblem:
				case LoginHelper.Result.ParseProblem:
				case LoginHelper.Result.UnknownProblem:
				default:
					retryRegister = await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Problem connecting to server"), 
						BlrtUtil.PlatformHelper.Translate ("Check your connected to Internet and try again."), 
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);
					break;
			}
			if (retryRegister) {
				return await ResetPasswordCheck (details);
			}
			return false;
		}
	}
}

