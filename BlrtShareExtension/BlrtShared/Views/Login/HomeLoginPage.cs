﻿using System;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using BlrtAPI;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using BlrtData.Events;
using Acr.UserDialogs;
using BlrtData.Views.CustomUI;
using BlrtData.Helpers;
using Parse;

namespace BlrtData.Views.Login
{
	public class HomeLoginPage : BaseLoginPage
	{
		public static readonly float StackPadding = 20;
		public static readonly float StackSpacing = 20;
		public static readonly float StackWidthRequest = 360;

		public static readonly Font TitleFont = Font.SystemFontOfSize (24, FontAttributes.Bold);
		public static readonly Font DetailFont = Font.SystemFontOfSize (NamedSize.Medium, FontAttributes.None);


		public static readonly Color ButtonBackgroundColor = BlrtStyles.BlrtCharcoal;
		public static readonly Color ButtonTextColor = BlrtStyles.BlrtGreen;

		public static readonly Color TextColor = BlrtStyles.BlrtCharcoal;

		public const int EntryHInset = 5;


		public override string ViewName {
			get {
				return "Login_Email_SignUpLogin";
			}
		}

		public List<string> SystemEmails { get; private set; }

		public static readonly int BUTTON_WIDTH = 300;
		StackLayout _bottomSection;
		Label _primaryEmailLbl;
		Picker _emailPicker;
		Label _bytappingOn, _tos, _privacy;
		Button _signupBtn;
		Entry _email, _password;

		public HomeLoginPage (List<string> systemEmails = null) : base ()
		{
			SystemEmails = systemEmails ?? new List<string> ();
			NavigationPage.SetHasNavigationBar (this, false);

			var logo = new Image () {
				Source = "blrt_tpd.png",
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};

			_email = new FormsEntry () {
				HideBottomDrawable = true,
				Keyboard = Keyboard.Email,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("Email"),
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = BUTTON_WIDTH,
				BackgroundColor = BlrtStyles.BlrtWhite,
				ReturnKey = ReturnKeyType.Next
			};

			_password = new FormsEntry () {
				HideBottomDrawable = true,
				IsPassword = true,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("Password"),
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = BUTTON_WIDTH,
				BackgroundColor = BlrtStyles.BlrtWhite,
				ReturnKey = ReturnKeyType.Done
			};

			_primaryEmailLbl = new Label () {
				HorizontalOptions = LayoutOptions.Center,
				Text = BlrtUtil.PlatformHelper.Translate ("Primary email address"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
			};

			_emailPicker = new Picker () {
				HorizontalOptions = LayoutOptions.Center,
				BackgroundColor = BlrtStyles.BlrtWhite,
				WidthRequest = BUTTON_WIDTH,
				IsVisible = false
			};

			_bytappingOn = new Label () {
				HorizontalOptions = LayoutOptions.Center,
				Text = BlrtUtil.PlatformHelper.Translate ("By tapping on \"Start using Blrt\""),
				TextColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			};

			var agreeTo = new Label () {
				HorizontalOptions = LayoutOptions.Center,
				Text = BlrtUtil.PlatformHelper.Translate (", you are agreeing to"),
				TextColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			};

			var tos = new FormattedString ();
			tos.Spans.Add (new Span () {
				Text = "our ",
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			});
			tos.Spans.Add (new Span () {
				Text = "Terms of Service",
				ForegroundColor = BlrtStyles.BlrtCharcoal,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			});
			_tos = new Label () {
				FormattedText = tos,
				VerticalOptions = LayoutOptions.Fill,
				YAlign = TextAlignment.Start,
				XAlign = TextAlignment.End,
			};

			var privacy = new FormattedString ();
			privacy.Spans.Add (new Span () {
				Text = " and ",
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			});
			privacy.Spans.Add (new Span () {
				Text = "Privacy Policy",
				ForegroundColor = BlrtStyles.BlrtCharcoal,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			});
			privacy.Spans.Add (new Span () {
				Text = ".",
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
			});

			_privacy = new Label () {
				FormattedText = privacy,
				VerticalOptions = LayoutOptions.Fill,
				YAlign = TextAlignment.Start,
				XAlign = TextAlignment.Start,
			};

			_signupBtn = new Button () {
				HorizontalOptions = LayoutOptions.Center,
				BackgroundColor = BlrtStyles.BlrtGreen,
				Text = BlrtUtil.PlatformHelper.Translate ("Start using Blrt"),
				TextColor = BlrtStyles.BlrtCharcoal,
				FontSize = 18,
				WidthRequest = BUTTON_WIDTH,
				HeightRequest = 42,
#if __ANDROID__
				Margin = new Thickness (0, -2, 0, 0)
#endif
			};

			var signin = new FormattedString ();
			signin.Spans.Add (new Span () {
				Text = "Already have an account? ",
				FontSize = BlrtStyles.CellContentFont.FontSize,
				ForegroundColor = BlrtStyles.BlrtGray5
			});
			signin.Spans.Add (new Span () {
				Text = "Sign in",
				FontSize = BlrtStyles.CellContentFont.FontSize,
				ForegroundColor = BlrtStyles.BlrtGreen
			});

			var tosNprivacy = new StackLayout () {
				Padding = 0,
				Spacing = 0,
				HorizontalOptions = LayoutOptions.Center,
				Orientation = StackOrientation.Horizontal,
				Children = {
					_tos,
					_privacy
				},
				HeightRequest = 30,
			};

			var tappingNagree = new StackLayout () {
				Padding = 0,
				Spacing = 0,
				HorizontalOptions = LayoutOptions.Center,
				Orientation = StackOrientation.Horizontal,
				Children = {
					_bytappingOn,
					agreeTo
				}
			};

			var resetPasswordBtn = new Button () {
				HorizontalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtGreen,
				Text = BlrtUtil.PlatformHelper.Translate ("Forgot Password"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
#if __IOS__
				HeightRequest = 35,
#endif
			};

			var tosSec = new StackLayout () {
				Padding = new Thickness (0, 0, 0, -12),
				Spacing = 0,
				HorizontalOptions = LayoutOptions.Fill,
				Children = {
					tappingNagree,
					tosNprivacy
				}
			};

			_bottomSection = new StackLayout () {
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness (0, 12, 0, 16),
				BackgroundColor = BlrtStyles.BlrtGray2,
				Spacing = 12,
				MinimumHeightRequest = 220,
				Children = {
					_primaryEmailLbl,
					_emailPicker,
					_email,
					_password,
					tosSec,
					_signupBtn,
					resetPasswordBtn
				}
			};

			var logoBotSpace = Device.Idiom == TargetIdiom.Phone ? 60 : 30;
			if (Device.OS == TargetPlatform.iOS) {
				logoBotSpace = Device.Idiom == TargetIdiom.Phone ? 50 : 30;
				if (Device.Idiom == TargetIdiom.Phone && BlrtUtil.PlatformHelper.GetDeviceHeight () < 481)
					logoBotSpace = 15;
			}

			var logoSection = new ContentView () {
				Padding = new Thickness (0, 0, 0, logoBotSpace),
				Content = logo
			};

			var content = new Grid () {
				ColumnDefinitions = new ColumnDefinitionCollection () {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
				},
				RowDefinitions = new RowDefinitionCollection () {
					new RowDefinition () { Height = new GridLength (5, GridUnitType.Star) },
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Auto) },
				},
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				BackgroundColor = BlrtStyles.BlrtGreen,
				Padding = 0,
				RowSpacing = 0,
				Children = {
					{logoSection,0,0},
					{_bottomSection, 0, 1},
				}
			};

			this.Content = new ScrollView () {
				Content = content,
			};

			_emailPicker.SelectedIndexChanged += EmailPickerSelected;

			_tos.GestureRecognizers.Add (new TapGestureRecognizer ((v, o) => {
				OpenURL (string.Format (BlrtData.BlrtSettings.TermOfServiceUrl, 0), "Terms & Conditions of Use", "Terms");
			}));

			_privacy.GestureRecognizers.Add (new TapGestureRecognizer ((v, o) => {
				OpenURL (string.Format (BlrtData.BlrtSettings.PrivacyUrl, 0), "Privacy", "Privacy");
			}));

			_bytappingOn.GestureRecognizers.Add (new TapGestureRecognizer ((v, o) => {
				OpenURL (string.Format (BlrtData.BlrtSettings.TermOfServiceUrl, 0), "Terms & Conditions of Use", "Terms");
			}));

			agreeTo.GestureRecognizers.Add (new TapGestureRecognizer ((v, o) => {
				OpenURL (string.Format (BlrtData.BlrtSettings.PrivacyUrl, 0), "Privacy", "Privacy");
			}));

			_email.Completed += (sender, e) => {
				_password.Focus ();
			};

			this.KeyboardAppeared += async (sender, e) => {
#if __IOS__
				Task.Delay (10);
#endif

				var s = this.Content as ScrollView;
				if (this.KeyboardAppearing) {
					var offset = logoSection.Height + _signupBtn.Bounds.Bottom + 10 + e - this.Height;
					if (offset > 0) {
						await s.ScrollToAsync (0, offset, true);
					}
				} else {
					await s.ScrollToAsync (0, 0, true);
				}
			};

			_password.Completed += (a, b) => {
				SignupBtnClicked ();
			};

			_signupBtn.Clicked += (a, b) => {
				SignupBtnClicked ();
			};

			resetPasswordBtn.Clicked += MultiExecutionPreventor.NewEventHandler ((async (sender, e) => {
#if __IOS__
				await OpenResetPassword(sender,e);
#else
				await Navigation.PushAsync (new ResetPasswordPage (), true);
#endif
			}));

			PopQuickSignUpResult ();
		}

		async Task<bool> ConfirmInternetError()
		{
			return await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("Internet problem"),
				BlrtUtil.PlatformHelper.Translate ("Check your internet connection or visit blrt.com for help."),
				BlrtUtil.PlatformHelper.Translate ("Retry"),
				BlrtUtil.PlatformHelper.Translate ("Cancel")
			);
		}

		async Task<bool> DoExistedUserLogIn(LoginHelper.LoginDetails details)
		{
			using (UserDialogs.Instance.Loading ()) {
				if (await LoginHelper.Login (this, details)) {
					BlrtTrack.AccountSignIn ("email");
#if __IOS__
				//login the linked fb account
				await BlrtiOS.FBManager.LoginWithParse ();
#else
					BlrtApp.Droid.FBManager.LoginWithParse ();
#endif
					if (BlrtData.BlrtManager.Login (ParseUser.CurrentUser, false, false)) {
						var evnt = new BlrtLoginEvent (BlrtLoginEvent.LoginType.LoginWithEmail);
						BlrtManager.Event.Fire (evnt);
					}
					return true;
				} else {
					return false;
				}
			}
		}

		async Task<bool> DoRegisterAndLogIn (LoginHelper.LoginDetails details)
		{
			var accept = await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("signup_confirm_title", "login"),
				details.Username,
				BlrtUtil.PlatformHelper.Translate ("signup_confirm_accept", "login"),
				BlrtUtil.PlatformHelper.Translate ("signup_confirm_cancel", "login")
			);
			if (accept) {
				var signupWithSMSLink = BlrtUserHelper.InstallMetaData?.ContainsKey ("sendToPhone") ?? false;
				using (UserDialogs.Instance.Loading ()) {
					if (await Register (details)) {
						if (await LoginHelper.Login (this, details)) {
							_password.Unfocus ();
							BlrtTrack.AccountCreate ("email");
							BlrtTrack.AccountSignIn ("email");
							
							if (BlrtData.BlrtManager.Login (ParseUser.CurrentUser, true, signupWithSMSLink)) {
								BlrtData.BlrtManager.Event.Fire<BlrtLoginEvent> (new BlrtLoginEvent (BlrtLoginEvent.LoginType.RegisterWithEmail));
							}
							return true;
						}
					}
				}
			}
			return false;
		}

		async Task<bool> DoQuickFlow(string email, bool emailExist)
		{
			if (emailExist) {
				// quick log in
				string password = null;

				while (true) {
					var passwordResult = await UserDialogs.Instance.PromptAsync (new PromptConfig () {
						Title = BlrtUtil.PlatformHelper.Translate ("login_please_password_title", "login"),
						Message = BlrtUtil.PlatformHelper.Translate ("login_please_password_message", "login"),
						InputType = InputType.Password,
						CancelText = "Cancel",
						Placeholder = "Password",
						OkText = "Sign in"
					});
					if (passwordResult.Ok) {
						password = passwordResult.Text;
						if (SignUpCheckFieldsValid (email, password)) {
							var loginDetail = new LoginHelper.LoginDetails (email, password, false, false);
							return await DoExistedUserLogIn (loginDetail);

						}
					} else {
						return false;
					}
				}

			} else {
				// quick sign up
				var password = CreateRandPassword (8);
				var loginDetail = new LoginHelper.LoginDetails (email, password, true, false);
				return await DoRegisterAndLogIn (loginDetail);
			}
		}

		async Task<bool> DoNormalFlow (string email, string password, bool emailExist)
		{
			var loginDetail = new LoginHelper.LoginDetails (email, password, false, false);
			if (emailExist) {
				return await DoExistedUserLogIn (loginDetail);
			} else {
				return await DoRegisterAndLogIn (loginDetail);
			}
		}

		async Task SignupBtnClicked ()
		{
			// hide keyboard
			_email.Unfocus ();
			_password.Unfocus ();

			string email = null;
			if (_emailPicker.IsVisible) {
				email = _emailPicker.Items [_emailPicker.SelectedIndex];
			} else {
				if (!SignUpCheckFieldsValid(_email.Text, _password.Text)) {
					return;
				}
				email = _email.Text;
			}

			bool emailExist = false;
			while (true) {
				try {
					using (UserDialogs.Instance.Loading ()) {
						emailExist = await FindExistance (email);
						break;
					}
				} catch {
					//internet error
					var internetRetry = await ConfirmInternetError ();
					if (internetRetry) {
						continue;
					} else {
						return;
					}
				}
			}

			if (_emailPicker.IsVisible) {
				await DoQuickFlow(email, emailExist);
			} else {
				await DoNormalFlow (email, _password.Text, emailExist);
			}


		}

		public async Task<bool> FindExistance (string email)
		{
			var api = new APIUserContactDetailFindExistance (
				new APIUserContactDetailFindExistance.Parameters () {
					Emails = new string [] { email }
				});

			if (!await api.Run (new CancellationTokenSource (15000).Token))
				throw api.Exception;

			if (!api.Response.Success)
				throw new Exception (api.Response.Error.Message);

			var result = api.Response.Emails;
			if (result != null && result.Count () > 0) {
				return true;
			} else {
				return false;
			}
		}

		bool SignUpCheckFieldsValid (string email, string password)
		{
			if (!LoginHelper.IsValidEmail (email)) {
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("Invalid email"),
					BlrtUtil.PlatformHelper.Translate ("Check your email address and try again."),
					BlrtUtil.PlatformHelper.Translate ("OK")
				);
				return false;
			}

			if (!LoginHelper.IsValidPassword (password)) {
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("Password too short"),
					BlrtUtil.PlatformHelper.Translate ("Your password must be at least 8 characters long."),
					BlrtUtil.PlatformHelper.Translate ("OK")
				);
				return false;
			}
			return true;
		}

		void OpenURL(string url, string title, string viewName){
			var web = new SimpleWebPage(){
				Source = new UrlWebViewSource() {
					Url = url
				},
				Title = title,
				ViewName = viewName
			};
#if __ANDROID__
			var nav = new NavigationPage (web);
			this.Navigation.PushModalAsync(nav);
#else
			var con = web.CreateViewController();
			con.Title = title;
			var nav = new BlrtiOS.Views.Util.BaseUINavigationController (con);
			var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
			app.LoginNav.PresentViewController (nav, true, null);
#endif
		}

		protected override void OnAppearing ()
		{
#if __ANDROID__
			if (Device.Idiom == TargetIdiom.Phone) {
				BlrtApp.PlatformHelper.LockScreenOrientation (BlrtApp.PlatformHelper.ScreenOrientation.UserPortrait);
			}
#endif
			base.OnAppearing ();
		}	

		protected override void OnSizeAllocated (double width, double height)
		{
			_bottomSection.HeightRequest = Math.Max(height * 0.45f, 220);
			base.OnSizeAllocated (width, height);
		}


		void EmailPickerSelected(object sender, EventArgs e){
			if(_emailPicker.SelectedIndex== _emailPicker.Items.Count()-1){
				_emailPicker.IsVisible = false;
				_email.IsVisible = true;
				_password.IsVisible = true;
				_primaryEmailLbl.IsVisible = false;
			}
		}

		protected override bool OnBackButtonPressed ()
		{
			if (this._emailPicker.IsVisible || null == SystemEmails || SystemEmails.Count == 0 || null != LastSignedInUserEmail ()) {
				return base.OnBackButtonPressed ();
			}else{
				_emailPicker.IsVisible = true;
				_emailPicker.SelectedIndex = 0;
				_email.IsVisible = false;
				_password.IsVisible = false;
				_primaryEmailLbl.IsVisible = true;
				return true;
			}
		}

		string LastSignedInUserEmail() {
			object lastEmailObj = null;
			if (BlrtPersistant.Properties.TryGetValue ("lastSignInAs", out lastEmailObj) && BlrtUtil.IsValidEmail (lastEmailObj as string)) {
				return lastEmailObj as string;
			}
			return null;
		}

		public void PopQuickSignUpResult ()
		{
#if __ANDROID__
			bool isQuickSignup = false;
			var lastEmail = LastSignedInUserEmail();
			if (null != lastEmail) {
				_email.Text = lastEmail;
				isQuickSignup = false;
			} else {
				var emails = SystemEmails ?? new List<string> ();
				if (emails.Count > 0) {
					_emailPicker.Items.Clear ();
					foreach (var email in emails) {
						_emailPicker.Items.Add (email);
					}
					_emailPicker.Items.Add (BlrtUtil.PlatformHelper.Translate ("+ add email"));
					_emailPicker.SelectedIndex = 0;
					isQuickSignup = true;
				}
			}
			_emailPicker.IsVisible = isQuickSignup;
			_primaryEmailLbl.IsVisible = isQuickSignup;
			_email.IsVisible = !isQuickSignup;
			_password.IsVisible = !isQuickSignup;
#else
			_primaryEmailLbl.IsVisible = false;
#endif
		}


		async Task<bool> Register(LoginHelper.LoginDetails details)
		{
			var result = await LoginHelper.Register (details);

			bool retryRegister = false;

			switch (result) {

				case LoginHelper.Result.Success:
					return true;

				case LoginHelper.Result.EmailInUse:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("You've already registered"), 
						BlrtUtil.PlatformHelper.Translate ("A Blrt account already exists for that email address."), 
						BlrtUtil.PlatformHelper.Translate ("OK")
					);
					break;

				case LoginHelper.Result.InternetProblem:
					retryRegister = await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Internet problem"), 
						BlrtUtil.PlatformHelper.Translate ("Check your internet connection or visit blrt.com for help."), // TODO error message
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);
					break;


				case LoginHelper.Result.InvalidParameters:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Invalid parameters"), 
						BlrtUtil.PlatformHelper.Translate ("Invalid parameters"), 
						BlrtUtil.PlatformHelper.Translate ("OK")
					);
					break;


				case LoginHelper.Result.ParseProblem:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Server issue"), 
						BlrtUtil.PlatformHelper.Translate ("Server issue"), 
						BlrtUtil.PlatformHelper.Translate ("OK")
					);
					break;


				case LoginHelper.Result.UnknownProblem:
				default:
					retryRegister = await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Registration failed"), 
						BlrtUtil.PlatformHelper.Translate ("Check your internet connection or visit blrt.com for help."), // TODO error message
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);
					break;
			}
			if (retryRegister) {
				return await Register (details);
			}
			return false;
		}


		public string CreateRandPassword(int length)
		{
			const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			StringBuilder res = new StringBuilder();
			Random rnd = new Random();
			while (0 < length--)
			{
				res.Append(valid[rnd.Next(valid.Length)]);
			}
			return res.ToString();
		}

#if __ANDROID__
		public Task OpenResetPassword (object sender, EventArgs e) {
		return Navigation.PushAsync (new ResetPasswordPage (), true);
		}
#elif __IOS__
		public static Task OpenResetPassword (object sender, EventArgs e) {
			var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
			app.LoginNav.PushViewController(new ResetPasswordPage().CreateViewController(),true);
			return Task.Run (() => {});
		}
#endif

	}
}

