﻿using System;
using Xamarin.Forms;
using System.Threading;
using System.Threading.Tasks;


namespace BlrtData.Views.Login
{
	public class SimpleWebPage : BlrtContentPage
	{
		WebView _webView;
		ActivityIndicator _loading;
		Grid _content;

		public WebViewSource Source{ get{
				if (_webView != null)
					return _webView.Source;
				else
					return null;
			} set{
				if(_webView!=null){
					_webView.Source = value;
				}	
			}
		}

		public SimpleWebPage (bool hideOkButton = false) : base ()
		{
			_loading = new ActivityIndicator () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				IsRunning = true,
				IsEnabled = true,
				IsVisible = true,
				HeightRequest = 32,
			};


			// instantiate member views
			_webView = new CustomWebView {
				HorizontalOptions = LayoutOptions.Fill,
				Source = Source
			};


			_webView.Navigated += (sender, e) => {
				_loading.IsVisible = false;
				_loading.IsEnabled = false;
			};

			var okBtn = new Button () {
				Text = "OK",
				FontSize = BlrtStyles.CellContentFont.FontSize,
				BackgroundColor = BlrtStyles.BlrtCharcoal,
				TextColor = BlrtStyles.BlrtGreen,
				BorderRadius = 3,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = 70,
				HeightRequest = 35
			};
			okBtn.Clicked+= (sender, e) => {
				#if __ANDROID__
				this.Navigation.PopModalAsync();
				#else
				var app = BlrtManager.App as BlrtiOS.Views.Root.RootAppController;
				app.LoginNav.DismissViewController(true,null);
				#endif
			};

			// layout member views
			var Star = new GridLength (1, GridUnitType.Star);
			_content = new Grid() {
				HorizontalOptions = LayoutOptions.Fill,
				RowDefinitions = {
					new RowDefinition {Height = Star},
					new RowDefinition {Height = GridLength.Auto}
				},
				ColumnDefinitions = {
					new ColumnDefinition {Width = Star},
				},
				Children = {
					{_webView, 0, 0},
					{_loading, 0, 0},
				}
			};
			if(!hideOkButton){
				_content.Children.Add (
					new ContentView () {
						VerticalOptions = LayoutOptions.Fill,
						HorizontalOptions = LayoutOptions.Fill,
						BackgroundColor = BlrtStyles.BlrtGray2,
						HeightRequest = 60,
						Content = okBtn
					}, 0, 1
				);
			}

			Content = _content;
		}
	}

	public class CustomWebView: WebView
	{
	}
}

