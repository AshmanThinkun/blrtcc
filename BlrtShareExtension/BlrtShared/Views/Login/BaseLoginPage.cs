﻿using System;

namespace BlrtData.Views.Login
{
	public class BaseLoginPage : BlrtContentPage
	{
		public EventHandler<double> KeyboardAppeared{ get; set;}

		public bool KeyboardAppearing{get;set;}
	}
}

