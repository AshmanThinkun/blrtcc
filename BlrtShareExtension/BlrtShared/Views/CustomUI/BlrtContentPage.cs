using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BlrtData
{
	public class BlrtContentPage : ContentPage
	{
		public BlrtContentPage () { }

		public virtual string ViewName { get; set; }

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			TrackPageView ();
		}

		public virtual void TrackPageView ()
		{
			if (!string.IsNullOrWhiteSpace (ViewName))
				BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName);
		}
	}
}

