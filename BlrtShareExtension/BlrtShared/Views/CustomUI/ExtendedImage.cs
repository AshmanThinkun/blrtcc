﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI 
{
	public class ExtendedImage : Image
	{
		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create<ExtendedImage, Color> (p => p.BorderColor, Color.Default);

		public Color BorderColor {
			get { return (Color)base.GetValue (BorderColorProperty); }
			set { base.SetValue (BorderColorProperty, value); }
		}

		public static readonly BindableProperty BorderWidthProperty =
			BindableProperty.Create<ExtendedImage, double> (p => p.BorderWidth, 0);

		public double BorderWidth {
			get { return (double)base.GetValue (BorderWidthProperty); }
			set { base.SetValue (BorderWidthProperty, value); }
		}

	}
}

