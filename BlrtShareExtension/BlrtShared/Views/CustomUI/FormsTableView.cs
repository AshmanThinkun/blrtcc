using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsTableView: TableView
	{
		public bool DisableSelectionStyle{ get; set;}
		public bool ShowSectionHeader{ get; set;}

		public FormsTableView ():base()
		{
			ShowSectionHeader = true;
		}
	}
}

