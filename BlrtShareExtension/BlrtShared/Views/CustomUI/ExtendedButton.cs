﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class ExtendedButton : Button
	{
		public ExtendedButton () : base ()
		{
		}

		public event EventHandler Pressed;
		public event EventHandler DraggedEnter;
		public event EventHandler DraggedExit;
		public event EventHandler ReleasedInside;
		public event EventHandler ReleasedOutside;

		public void NotifyPressed ()
		{
			if (null != Pressed) {
				Pressed (this, EventArgs.Empty);
			}
		}

		public void NotifyDraggedEnter ()
		{
			if (null != DraggedEnter) {
				DraggedEnter (this, EventArgs.Empty);
			}
		}

		public void NotifyDraggedExit ()
		{
			if (null != DraggedExit) {
				DraggedExit (this, EventArgs.Empty);
			}
		}

		public void NotifyReleasedInside ()
		{
			if (null != Pressed) {
				ReleasedInside (this, EventArgs.Empty);
			}
		}

		public void NotifyReleasedOutside ()
		{
			if (null != ReleasedOutside) {
				ReleasedOutside (this, EventArgs.Empty);
			}
		}
	}
}

