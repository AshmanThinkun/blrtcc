using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsListView : ListView
	{
		public Thickness SeparatorInset { get; set; }
		public bool DisplayDivider { get; set; }

		public bool DisableSelectionStyle { get; set; }
		public bool DisableScrolling { get; set; }

#if __ANDROID__
		public FormsListView (ListViewCachingStrategy strategy = ListViewCachingStrategy.RetainElement) : base (strategy)
#endif
#if __IOS__
		public FormsListView () : base()
#endif
		{
		}
	}
}

