using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsEntry:Entry
	{
		public Thickness ContentEdgeInsets{ get; set;} // don't use
		public bool HideBottomDrawable{ get; set;}
		public ReturnKeyType ReturnKey{ get; set;}
		public int CornerRadious{ get; set;}
		public bool DisableAutoCap{ get; set;}

		public float FontSize{ get; set;}

		public Color PlaceholderTextColor{ get; set;}

		public FormsEntry ()
		{
			ContentEdgeInsets = new Thickness(-999);
			ReturnKey = ReturnKeyType.Default;
			CornerRadious = -999;

			PlaceholderTextColor = BlrtStyles.BlrtGray5;

			FontSize = -999;

			HideBottomDrawable = true;
		}

		public Action SelectAllAction{ get; set;}
		public void SelectAll(){
			SelectAllAction?.Invoke ();
		}
	}

	public enum ReturnKeyType{
		Default,
		Done,
		Next
	}
}

