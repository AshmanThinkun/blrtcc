using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace BlrtData.Views.CustomUI
{
	public class BlrtSwitchImageButton : Grid
	{
		public event EventHandler Clicked {
			add {
				_btn.Clicked += value;
			}
			remove {
				_btn.Clicked -= value;
			}
		}

		public event EventHandler Pressed {
			add {
				_btn.Pressed += value;
			}
			remove {
				_btn.Pressed -= value;
			}
		}

		public event EventHandler DraggedEnter {
			add {
				_btn.DraggedEnter += value;
			}
			remove {
				_btn.DraggedEnter -= value;
			}
		}

		public event EventHandler DraggedExit {
			add {
				_btn.DraggedExit += value;
			}
			remove {
				_btn.DraggedExit -= value;
			}
		}

		public event EventHandler ReleasedInside {
			add {
				_btn.ReleasedInside += value;
			}
			remove {
				_btn.ReleasedInside -= value;
			}
		}

		public event EventHandler ReleasedOutside {
			add {
				_btn.ReleasedOutside += value;
			}
			remove {
				_btn.ReleasedOutside -= value;
			}
		}

		ExtendedButton _btn;
		TintableImage[] _images;
		BoxView	_background;
		int _currentShowing;

		public BlrtSwitchImageButton ()
		{
			base.BackgroundColor = Color.Transparent;
			_currentShowing = -1;
			_btn = new ExtendedButton {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
			};
			_images = null;
			_background = new BoxView {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill
			};

			ColumnSpacing = 0;

			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) }
			};
			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = new GridLength (1, GridUnitType.Star) }
			};

			Children.Add (_background, 0, 0);
			Children.Add (_btn, 0, 0);
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == IsEnabledProperty.PropertyName) {

				_btn.IsEnabled = IsEnabled;
				Opacity = IsEnabled ? 1 : 0.5f;
			}
		}

		public int ImageCount 
		{
			get {
				if (null == _images) {
					throw new InvalidOperationException ("set images before get image count");
				}
				return _images.Length;
			}
		}

		public ImageSource[] ImageSource
		{
			get {
				return (from view in _images select view.Source).ToArray ();
			}

			set {
				if (!ImageSourceEqual (value)) {
					if (_images != null) {
						foreach (var item in _images) {
							Children.Remove (item);
						}
					}
					if (null != value && value.Length > 0) {
						_images = new TintableImage[value.Length];
						_currentShowing = 0;
						for (int i = 0; i < value.Length; i++) {
							_images[i] = new TintableImage () {
								BackgroundColor = Color.Transparent,
								VerticalOptions = LayoutOptions.Center,
								HorizontalOptions = LayoutOptions.Center,
								Source = value [i],
								Opacity = (i == _currentShowing) ? 1 : 0
							};
							Children.Add (_images[i], 0, 0);
						}
						this.RaiseChild (_btn);
					} else {
						_currentShowing = -1;
					}
				}
			}
		}

		bool ImageSourceEqual (ImageSource[] newValue) 
		{
			if (_images == null) {
				if (null != newValue && newValue.Length > 0) {
					return false;
				}
				return true;
			} else {
				if (null != newValue && newValue.Length > 0 && newValue.Length == _images.Length) {
					for (int i = 0; i < newValue.Length; i++) {
						if (_images[i].Source != newValue[i]) {
							return false;
						}
					}
					return true;
				}
				return false;
			}
		}

		public int Current
		{
			get {
				return _currentShowing;
			}

			set {
				if (_currentShowing != value) {
					if (null == _images) {
						throw new InvalidOperationException ("set image source before showing image");
					}
					if (value < 0 || value > _images.Length) {
						throw new IndexOutOfRangeException (string.Format ("image index [{0}] is out of range [0, {1})", value, _images.Length));
					}
					BatchBegin ();
					_images [value].Opacity = 1;
					_images [_currentShowing].Opacity = 0;
					_currentShowing = value;
					BatchCommit ();
				}
			}
		}

		public void SetTintColor (int index, Color tintColor)
		{
			if (null == _images) {
				throw new InvalidOperationException ("set image source before setting tint color");
			}
			if (index < 0 || index > _images.Length) {
				throw new IndexOutOfRangeException (string.Format ("image index [{0}] is out of range [0, {1})", index, _images.Length));
			}
			_images [index].TintColor = tintColor;
		}

		public Color GetTintColor (int index)
		{
			if (null == _images) {
				throw new InvalidOperationException ("set image source before setting tint color");
			}
			if (index < 0 || index > _images.Length) {
				throw new IndexOutOfRangeException (string.Format ("image index [{0}] is out of range [0, {1})", index, _images.Length));
			}
			return _images [index].TintColor;
		}

		public new Color BackgroundColor {
			get { return _background.BackgroundColor; }
			set { _background.BackgroundColor = value; }
		}

		bool _isTouchFeedbackColorSet = false;
		Color _touchFeedbackColor;
		Color _normalColor;
		uint _touchFeedbackDuration;
		public void SetTouchFeedbackColor (Color highlightColor, Color normalColor, uint duration = 50)
		{
			_isTouchFeedbackColorSet = true;
			_touchFeedbackColor = highlightColor;
			_normalColor = normalColor;
			_touchFeedbackDuration = duration;
			_btn.Pressed += (sender, e) => {
				TouchFeedbackHighlight (true);
			};
			_btn.DraggedEnter += (sender, e) => {
				TouchFeedbackHighlight (true);
			};
			_btn.DraggedExit += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
			_btn.ReleasedInside += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
			_btn.ReleasedOutside += (sender, e) => {
				TouchFeedbackHighlight (false);
			};
		}

		bool _isHighlighted = false;
		const string AnimationName_Highlight = "BlrtSwitchImageButton_highlight";
		const string AnimationName_UnHighlight = "BlrtSwitchImageButton_unhighlight";
		Color _startColor;
		void TouchFeedbackHighlight (bool isHighlight)
		{
			if (_isHighlighted != isHighlight) {
				_isHighlighted = isHighlight;
				if (isHighlight) {
					if (this.AnimationIsRunning (AnimationName_UnHighlight)) {
						this.AbortAnimation (AnimationName_UnHighlight);
					}
					_startColor = BackgroundColor;
					this.Animate (
						AnimationName_Highlight,
						(d) => {
							BackgroundColor = new Color (
								_startColor.R + (_touchFeedbackColor.R - _startColor.R) * d,
								_startColor.G + (_touchFeedbackColor.G - _startColor.G) * d,
								_startColor.B + (_touchFeedbackColor.B - _startColor.B) * d,
								_normalColor.A
							);
						},
						24,
						_touchFeedbackDuration
					);
				} else {
					if (this.AnimationIsRunning (AnimationName_Highlight)) {
						this.AbortAnimation (AnimationName_Highlight);
					}
					_startColor = BackgroundColor;
					this.Animate (
						AnimationName_UnHighlight,
						(d) => {
							BackgroundColor = new Color (
								_startColor.R + (_normalColor.R - _startColor.R) * d,
								_startColor.G + (_normalColor.G - _startColor.G) * d,
								_startColor.B + (_normalColor.B - _startColor.B) * d,
								_normalColor.A
							);
						},
						24,
						_touchFeedbackDuration
					);
				}
			}
		}
	}
}