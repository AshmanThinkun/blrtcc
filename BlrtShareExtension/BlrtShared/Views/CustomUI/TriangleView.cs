﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class TriangleView : BoxView
	{
		public enum TriangleOrientation
		{
			Left,
			Right,
			Up,
			Down
		};

		public static readonly BindableProperty OrientationProperty =
			BindableProperty.Create<TriangleView, TriangleOrientation> (p => p.Orientation, TriangleOrientation.Right);

		public TriangleOrientation Orientation {
			get { return (TriangleOrientation)base.GetValue (OrientationProperty); }
			set { base.SetValue (OrientationProperty, value); }
		}

		public Point[] GetTrianglePoints ()
		{
			var pts = new Point [3];
			switch (Orientation) {
				case TriangleView.TriangleOrientation.Up:
					pts[0]	= new Point (0, 			Height);
					pts[1] 	= new Point (Width * 0.5f,	0);
					pts[2] 	= new Point (Width, 		Height);
					break;
				case TriangleView.TriangleOrientation.Down:
					pts[0]	= new Point (0, 			0);
					pts[1] 	= new Point (Width * 0.5f,	Height);
					pts[2] 	= new Point (Width, 		0);
					break;
				case TriangleView.TriangleOrientation.Left:
					pts[0]	= new Point (Width, 		0);
					pts[1] 	= new Point (0, 			Height * 0.5f);
					pts[2] 	= new Point (Width, 		Height);
					break;
				case TriangleView.TriangleOrientation.Right:
				default:
					pts[0]	= new Point (0, 			0);
					pts[1] 	= new Point (Width, 		Height * 0.5f);
					pts[2] 	= new Point (0, 			Height);
					break;
			}
			return pts;
		}
	}
}

