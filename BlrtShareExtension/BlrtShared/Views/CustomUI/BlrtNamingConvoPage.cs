﻿
//using System;
//using Xamarin.Forms;
//using BlrtData.Views.CustomUI;
////using BlrtApp;
//using BlrtData.ExecutionPipeline;

//namespace BlrtData
//{
//	public class BlrtNamingConvoPage : BlrtContentPage
//	{
//		DashedBorderEntry _entry;
//		//For the background, look at the EmptyConvoPageRenderer
//		public BlrtNamingConvoPage ()
//		{
//			Title = BlrtUtil.PlatformHelper.Translate ("conversation_screen", "screen_titles");
//			BackgroundColor = BlrtStyles.BlrtBlackAlpha;

//			var label = new Label {
//				Text = "Name your conversation",
//				TextColor = BlrtStyles.BlrtCharcoal,
//				FontSize = 16
//			};
//			_entry = new DashedBorderEntry () {
//				BackgroundColor = BlrtStyles.BlrtGray2,
//				VerticalOptions = LayoutOptions.Fill,
//				Placeholder = PlatformHelper.Translate ("Conversation name"),
//				Keyboard = Keyboard.Text,
//				FontSize = (float)BlrtStyles.CellTitleFont.FontSize,

//			};
//			var cancelBtn = new FormsBlrtButton () {
//				Text = "Cancel",
//				TextColor = BlrtStyles.BlrtWhite,
//				FontSize = 15,
//				BackgroundColor = BlrtStyles.BlrtBlueGray,
//				BorderRadius = 5,
//				HeightRequest = 40,
//				WidthRequest = 90,

//			};
//			var submitBtn = new FormsBlrtButton () {
//				Text = "Create",
//				TextColor = BlrtStyles.BlrtBlackBlack,
//				FontSize = 15,
//				BackgroundColor = BlrtStyles.BlrtGreen,
//				BorderRadius = 5,
//				HeightRequest = 40,
//				WidthRequest = 90,

//			};
//			var btnContentView = new ContentView () {
//				HorizontalOptions = LayoutOptions.Center,
//				Padding = new Thickness (0, 10, 0, 0),
//				Content = new StackLayout () {
//					Spacing = 10,
//					Orientation = StackOrientation.Horizontal,
//					Children = {
//						cancelBtn,
//						submitBtn
//					}
//				}
//			};

//			var mainView = new ContentView () {
//				Padding = new Thickness (15, 20, 15, 20),
//				BackgroundColor = BlrtStyles.BlrtWhite,
//				Content = new StackLayout () {
//					Children = {
//						label,
//						_entry,
//						btnContentView
//					}
//				}
//			};

//			Content = new StackLayout () {
//				BackgroundColor = BlrtStyles.BlrtBlackAlpha,
//				Children = { mainView }
//			};

//			cancelBtn.Clicked += (sender, e) => cancelCreateNewConvo ();
//			submitBtn.Clicked += (sender, e) => createNewEmptyConvo();
//		}


//		protected override void OnAppearing ()
//		{
//			base.OnAppearing ();
//			InitialiseEntry ();
//		}

//		protected override void OnDisappearing ()
//		{
//			base.OnDisappearing ();

//		}
//		public override string ViewName {
//			get {
//				return BlrtUtil.PlatformHelper.Translate ("conversation_screen", "screen_titles");
//			}
//		}

//		void InitialiseEntry ()
//		{
//			_entry.Text = BlrtUtil.NewConversationDefaultName;
//			_entry.Focus ();
//		}

//		async void cancelCreateNewConvo ()
//		{
//			await Navigation.PopAsync (true);
//		}

//		async void createNewEmptyConvo ()
//		{
//			var text = _entry.Text;
//			if (string.IsNullOrEmpty(text)) {//if name is empty do nothing
//				_entry.PlaceholderColor = BlrtStyles.BlrtAccentRedHighlight;

//			} else {//create the new empty convo with the given name and navigate to that new convo
//				try {
//					//first step, we create a new convo and get the returned new convo id
//					var convoId = await BlrtUtil.CreateConversation (text);

//					//then we need to refresh the list of convo in mailbox
//					var executor = new Executor (ExecutionSource.System);
//					var mailbox = await BlrtManager.App.OpenMailbox (executor, BlrtData.Models.Mailbox.MailboxType.Inbox);
//					if (!mailbox.Success)
//						return;
//					mailbox.Result.Refresh (executor);

//					//finally we navigate to the new empty convo
//					await mailbox.Result.OpenConversation (executor, convoId, false, true);
//				} catch (Exception ex) {
//					System.Diagnostics.Debug.WriteLine (@"          ERROR: ", ex.Message);
//				}
//			}
//		}

//	}
//}
