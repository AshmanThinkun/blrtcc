using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class ExtendedScrollView : ScrollView
	{
		public new event Action<ScrollView, Rectangle> Scrolled;
		public event EventHandler DraggingStarted;
		public event EventHandler DraggingEnded;

		public void UpdateBounds(Rectangle bounds)
		{
			Position = bounds.Location;
			if (Scrolled != null)
				Scrolled (this, bounds);
		}

		public void NotifyDraggingStarted ()
		{
			if (null != DraggingStarted) {
				DraggingStarted (this, EventArgs.Empty);
			}
		}

		public void NotifyDraggingEnded ()
		{
			if (null != DraggingEnded) {
				DraggingEnded (this, EventArgs.Empty);
			}
		}

		public static readonly BindableProperty PositionProperty = 
			BindableProperty.Create<ExtendedScrollView,Point>(
				p => p.Position, default(Point));

		public Point Position {
			get { return (Point)GetValue(PositionProperty); }
			set { SetValue(PositionProperty, value); }
		}

		public static readonly BindableProperty AnimateScrollProperty = 
			BindableProperty.Create<ExtendedScrollView,bool>(
				p => p.AnimateScroll,true);

		public bool AnimateScroll {
			get { return (bool)GetValue (AnimateScrollProperty); }
			set { SetValue (AnimateScrollProperty, value); }
		}

	}
}

