using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsLabel:Label
	{
		public float CornerRadius{ get; set;}
		public FormsLabel ():base()
		{
			CornerRadius = 0;
		}
	}
}

