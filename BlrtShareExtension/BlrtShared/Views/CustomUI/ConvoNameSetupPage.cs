﻿#if __ANDROID__
using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtApp;
using BlrtApp.Views;
using System.Threading.Tasks;
using BlrtAPI;
using System.Collections.Generic;
using Acr.UserDialogs;
using System.Threading;

namespace BlrtData
{
	public class ConvoNameSetupPage : BetterContentPage
	{
		public DashedBorderEntry _entry;
		TaskCompletionSource<string> _waitConvoNameConfirmation;

		public Task<string> WaitConvoNameConfirmation {
			get {
				_waitConvoNameConfirmation = new TaskCompletionSource<string> ();
				return _waitConvoNameConfirmation.Task;
			}
		}

		/// <summary>
		/// Used to identify whether page is being used to edit existing convo name. 
		/// It is used later on to decide whether to call submit button handler.
		/// </summary>
		/// <value><c>true</c> if to edit existing convo name; otherwise, <c>false</c>.</value>
		bool ToEditExistingConvoName { get; set; }

		/// <summary>
		/// Gets or sets the conversation id of existing conversation.
		/// </summary>
		/// <value>The conversation id.</value>
		string conversationId { get; set; }

		/// <summary>
		/// Gets or sets the name of the current conversation.
		/// </summary>
		/// <value>The name of the current convo.</value>
		string currentConvoName { get; set; }

		//For the background, look at the EmptyConvoPageRenderer

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BlrtData.ConvoNameSetupPage"/> class.
		/// It is a page containing views to rename or create conversation.
		/// </summary>
		/// <param name="submitButtonText">Submit button text.</param>
		/// <param name="submitButtonHandler">Submit button handler.</param>
		/// <param name="toEditExistingConvoName">If set to <c>true</c> to edit existing convo name.</param>
		/// <param name="prePopulatedConvoName">text to be shown initially in the text field</param>
		public ConvoNameSetupPage (string submitButtonText,
								   bool toEditExistingConvoName,
								   string prePopulatedConvoName,
								   string _conversationId = null)
		{
			ToEditExistingConvoName = toEditExistingConvoName;
			conversationId = _conversationId;
			Title = BlrtUtil.PlatformHelper.Translate ("conversation_screen", "screen_titles");

			var label = new Label {
				Text = "Name your conversation",
				TextColor = BlrtStyles.BlrtCharcoal,
				FontSize = 16
			};
			_entry = new DashedBorderEntry {
				BackgroundColor = BlrtStyles.BlrtGray2,
				VerticalOptions = LayoutOptions.Fill,
				Placeholder = PlatformHelper.Translate ("Conversation name"),
				Keyboard = Keyboard.Text,
				FontSize = (float)BlrtStyles.CellTitleFont.FontSize,
				Text = prePopulatedConvoName
			};

			var cancelBtn = new FormsBlrtButton () {
				Text = "Cancel",
				TextColor = BlrtStyles.BlrtWhite,
				FontSize = 15,
				BackgroundColor = BlrtStyles.BlrtBlueGray,
				BorderRadius = 5,
				HeightRequest = 40,
				WidthRequest = 90,

			};
			var submitBtn = new FormsBlrtButton () {
				Text = submitButtonText,
				TextColor = BlrtStyles.BlrtBlackBlack,
				FontSize = 15,
				BackgroundColor = BlrtStyles.BlrtGreen,
				BorderRadius = 5,
				HeightRequest = 40,
				WidthRequest = 90,

			};
			var btnContentView = new ContentView () {
				HorizontalOptions = LayoutOptions.Center,
				Padding = new Thickness (0, 10, 0, 0),
				Content = new StackLayout () {
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {
						cancelBtn,
						submitBtn
					}
				}
			};

			var mainView = new ContentView () {
				Padding = new Thickness (15, 20, 15, 20),
				BackgroundColor = BlrtStyles.BlrtWhite,
				Content = new StackLayout () {
					Children = {
						label,
						_entry,
						btnContentView
					}
				}
			};

			Content = new StackLayout () {
				BackgroundColor = BlrtStyles.BlrtBlackAlpha,
				Children = { mainView }
			};

			cancelBtn.Clicked += (sender, e) => TrySetResult (string.Empty);
			submitBtn.Clicked += OnSubmitButtonClicked;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			SetFocusToInputField ();
		}

		public void SetFocusToInputField ()
		{
			_entry.Focus ();
			currentConvoName = _entry.Text;
		}

		/// <summary>
		/// Tries to return the convo name as result.
		/// </summary>
		/// <param name="convoName">Convo name.</param>
		async void TrySetResult (string convoName)
		{
			if (_waitConvoNameConfirmation != null) {
				_waitConvoNameConfirmation.TrySetResult (convoName);
				if (ToEditExistingConvoName && !string.IsNullOrEmpty (convoName) && string.Compare (convoName, currentConvoName) != 0) {
					var apiCall = new APIConversationRename (
						new APIConversationRename.Parameters () {
							ConversationId = conversationId,
							ConversationName = convoName
						});
					var token = new CancellationTokenSource ();
					var cancelOnPurpose = false;
					try {
						using (UserDialogs.Instance.Loading ("", delegate {
							token.Cancel ();
							cancelOnPurpose = true;
						}, "Cancel")) {
							
							var result = await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (token.Token, 5000));

							if (apiCall.Response.Success) {
								await Navigation.PopAsync (true);
							} else {
								BlrtUtil.Debug.ReportException (new Exception ("Conversation name update failure"), new Dictionary<string, string> {
									{"error code", apiCall.Response.Error.Code.ToString()},
									{"conversation id", conversationId},
									{"conversation name", convoName}
								}, Xamarin.Insights.Severity.Error);
								DisplayAlert (
									BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
									BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
									BlrtUtil.PlatformHelper.Translate ("OK")).FireAndForget ();
							}
						}
					} catch (Exception) {//blame Internet for any kinds of exception!
						if (!cancelOnPurpose) {
							DisplayAlert (
									BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
									BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
									BlrtUtil.PlatformHelper.Translate ("OK")).FireAndForget ();
						}
					}
				} else {
					await Navigation.PopAsync (true);
				}
			}

		}

		/// <summary>
		/// On submit button clicked.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void OnSubmitButtonClicked (object sender, EventArgs e)

		{
			var text = _entry.Text;
			if (string.IsNullOrEmpty (text)) {//if name is empty do nothing
				_entry.PlaceholderColor = BlrtStyles.BlrtAccentRedHighlight;
				return;
			}

			TrySetResult (_entry.Text);
		}
	}
}
#endif