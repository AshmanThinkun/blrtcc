﻿#if __ANDROID__
using System;
using System.Collections.Generic;
using BlrtApp;
using BlrtApp.Adapters;
using BlrtApp.Views.MediaPicker.Selection;
using BlrtCanvas.Pages;
using BlrtData.ExecutionPipeline;
using Xamarin.Forms;

namespace BlrtData
{
	public class MenuPicker : Picker
	{
		/// <summary>
		/// The menu list.
		/// </summary>
		Dictionary<int, string> menuList = new Dictionary<int, string> {
			{0, "Blrt"},
			{1, "Camera"},
			{2, "Photo"},
			{3, "Document"},
		};
		//This is the event to pass open request to the renderer
		public event EventHandler OpenRequested;

		public event EventHandler OpenBlrtRequested;

		public event EventHandler Execute;

		public bool IsNotFirst { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:BlrtData.MenuPicker"/> class.
		/// </summary>
		public MenuPicker ()
		{
			foreach (string item in menuList.Values) {
				Items.Add (item);
			}
		}

		/// <summary>
		/// Opens the picker.
		/// </summary>
		public void OpenPicker ()
		{
			if (OpenRequested != null) {
				OpenRequested (this, EventArgs.Empty);
			}

		}

		/// <summary>
		/// Performs the action.
		/// </summary>
		/// <param name="index">Index.</param>
		public async void PerformAction (int index)
		{
#if __IOS__
			var page = new SelectionMediaPage (true);
#endif
			switch (index) {
				case 0://Open Blrt creation
					if (OpenBlrtRequested != null & IsNotFirst) {
						OpenBlrtRequested (this, EventArgs.Empty);
					} else {
						IsNotFirst = true;
					}
					break;
				case 1://Open Camera
					var s = await PlatformHelper.GetCameraSelector ();
					var camera = await s.RunAction ();
					Execute (camera, EventArgs.Empty);
					break;
				case 2://Open Photo
					var p = await PlatformHelper.GetImageSelector ();
					var photo = await p.RunAction ();
					Execute (photo, EventArgs.Empty);
					break;
				case 3://Open Document
					var d = await PlatformHelper.GetDocumentSelector ();
					var document = await d.RunAction ();

#if __ANDROID__
				Execute (document, EventArgs.Empty);
#endif
				break;
			}
		}

	}
}
#endif