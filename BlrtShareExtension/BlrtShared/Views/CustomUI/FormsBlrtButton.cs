using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsBlrtButton : Button
	{
		public Thickness Padding{ get; set; }
		public bool IsUnderline{ get; set;}
		public bool RightImage { get; set;}
#if __ANDROID__
		public bool NeedsLayout { get; set; }
#endif
		public FormsBlrtButton () : base ()
		{
			Padding = -999;
#if __ANDROID__
			NeedsLayout = true;
#endif
		}
	}
}

