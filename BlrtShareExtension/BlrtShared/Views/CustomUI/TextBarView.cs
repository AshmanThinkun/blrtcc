using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class TextBarView : ContentView
	{
		public Entry _entry;
		public Button _sendBtn;

		public Grid _grid;

		public event EventHandler SendClicked;


		public string Placeholder {
			get { return _entry.Placeholder; }
			set { _entry.Placeholder = value; }
		}
		public string Text {
			get { return (_entry.Text ?? "").Trim(); }
			set { _entry.Text = (value ?? "").Trim(); }
		}
		public string SendButtonText {
			get { return _sendBtn.Text; }
			set { _sendBtn.Text = value; }
		}

		public int SendButtonWidth {
			get { return (int)_grid.ColumnDefinitions [1].Width.Value; }
			set { _grid.ColumnDefinitions [1].Width = new GridLength(value); }
		}

		public TextBarView() 
		{
			HorizontalOptions = LayoutOptions.Fill;

			_entry = new FormsEntry () {
				VerticalOptions = LayoutOptions.Fill,
				FontSize = (float)BlrtStyles.CellTitleFont.FontSize
			};

			_sendBtn = new Button(){
				Text			= "s",
				Font			= BlrtStyles.CellTitleBoldFont,

				VerticalOptions = LayoutOptions.End,
				HorizontalOptions = LayoutOptions.Center
			};


			Content = new StackLayout(){
				Spacing = 0,
				Orientation = StackOrientation.Vertical,

				Children = {
					new BoxView(){

						HeightRequest		= 1,
						HorizontalOptions	= LayoutOptions.Fill,

						BackgroundColor		= BlrtStyles.BlrtGray5,
						Color				= BlrtStyles.BlrtGray5,						
						
					},
					(_grid = new Grid(){

						BackgroundColor		= BlrtStyles.BlrtGray1,

						ColumnDefinitions = new ColumnDefinitionCollection(){
							new ColumnDefinition(){	Width = new GridLength(1, GridUnitType.Star) },
							new ColumnDefinition(){	Width = new GridLength(95, GridUnitType.Absolute) },
						},
						RowDefinitions = new RowDefinitionCollection(){
							new RowDefinition(){	Height = new GridLength(1, GridUnitType.Star) },
						},

						Padding			= 4,
						ColumnSpacing	= 1,
						RowSpacing		= 6,

						Children = {
							{ _entry,	0,	0 },
							{ _sendBtn,	1,	0 },
						}
					})
				}
			};


			_entry.TextChanged += (object sender, TextChangedEventArgs e) => {
				RecalulateState(e.NewTextValue);
			};
			RecalulateState (null);


			_entry.Completed += EnterPressed;
			_sendBtn.Clicked +=  EnterPressed;
		}

		public void FocusTextField()
		{
			_entry.Focus ();
		}

		public void RecalulateState(string text) {
		
			_sendBtn.IsEnabled = !string.IsNullOrWhiteSpace(text ?? Text );

			if(_sendBtn.IsEnabled){
				_sendBtn.TextColor			= BlrtStyles.BlrtAccentBlue;
			} else {
				_sendBtn.TextColor			= BlrtStyles.BlrtGray5;
			}

			InvalidateLayout();
		}


		void EnterPressed (object sender, EventArgs e)
		{
			var sc = SendClicked;
			if (_sendBtn.IsEnabled && sc != null)
				sc (this, e);
		}
	}
}

