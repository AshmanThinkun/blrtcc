using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class BlrtImageButton : Grid
	{
		public event EventHandler Clicked {
			add {
				_btn.Clicked += value;
			}

			remove {
				_btn.Clicked -= value;
			}
		}

		Button _btn;

		TintableImage _image;

		BoxView	_background;

		public BlrtImageButton ()
		{
			base.BackgroundColor = Color.Transparent;
			_btn = new Button {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
			};
			_image = new TintableImage () {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
			};

			_background = new BoxView {
				BackgroundColor = Color.Transparent,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill
			};

			ColumnSpacing = 0;

			ColumnDefinitions = new ColumnDefinitionCollection {
				new ColumnDefinition { Width = new GridLength (1, GridUnitType.Star) }
			};
			RowDefinitions = new RowDefinitionCollection {
				new RowDefinition { Height = new GridLength (1, GridUnitType.Star) }
			};

			Children.Add (_background, 0, 0);
			Children.Add (_image, 0, 0);
			Children.Add (_btn, 0, 0);
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == IsEnabledProperty.PropertyName) {

				_btn.IsEnabled = IsEnabled;
				Opacity = IsEnabled ? 1 : 0.5f;
			}
		}

		public ImageSource ImageSource
		{
			get {
				return _image.Source;
			}

			set {
				_image.Source = value;
			}
		}

		public Aspect Aspect
		{
			get {
				return _image.Aspect;
			}

			set {
				_image.Aspect = value;
			}
		}

		public Color TintColor {
			get { return _image.TintColor; } 
			set { _image.TintColor = value; }
		}

		public new Color BackgroundColor {
			get { return _background.BackgroundColor; }
			set { _background.BackgroundColor = value; }
		}

	}
}

