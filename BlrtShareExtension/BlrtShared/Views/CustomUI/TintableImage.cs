using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class TintableImage : Image
	{
		public static readonly BindableProperty TintColorProperty =
			BindableProperty.Create<TintableImage, Color>(cell => cell.TintColor, Color.Transparent);

		public Color TintColor { get { return (Color)GetValue(TintColorProperty); } set { SetValue (TintColorProperty, value); } }

		public static readonly BindableProperty IsVerticallyFlippedProperty =
			BindableProperty.Create<TintableImage, bool> (cell => cell.IsVerticallyFlipped, false);

		public bool IsVerticallyFlipped { get { return (bool)GetValue (IsVerticallyFlippedProperty); } set { SetValue (IsVerticallyFlippedProperty, value); } }

		public TintableImage ()
		{
		}
	}
}

