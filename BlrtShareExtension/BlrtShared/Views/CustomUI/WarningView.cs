﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Timers;
using System.Linq;

namespace BlrtData.Views.CustomUI
{
	public class WarningView: ContentView
	{
		Label _lbl;

		public double FontSize{
			get{
				return _lbl.FontSize;
			}set{
				_lbl.FontSize = value;
			}
		}

		public List<WarningAction> WarningList{ get; private set;}

		public bool Visible{get{return IsVisible;}
			set{ 
				IsVisible = value;
				if (!_timer.Enabled && value) {
					TimerElapsed (this, EventArgs.Empty);
					_timer.Enabled = true;
				}
				_timer.Enabled = value;
			}}

		Timer _timer;
		WarningAction _currentWarning;
		public WarningView ():base()
		{
			_lbl = new Label (){
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				TextColor = BlrtStyles.BlrtWhite,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};

			WarningList = new List<WarningAction> ();

			this.Content = _lbl;

			_timer = new Timer () {
				Interval = 10000
			};

			_timer.Elapsed += TimerElapsed;
					
			this.GestureRecognizers.Add (new TapGestureRecognizer ((v, o) => {
				if(_currentWarning!=null && _currentWarning.Action!=null){
					_currentWarning.Action.Invoke();
				}
			}));
		}

		void TimerElapsed(object sender, EventArgs e){
			if(WarningList.Count>0){
				if(_currentWarning==null){
					_currentWarning = WarningList.ElementAt(0);
				}else{
					var currentIndex = WarningList.IndexOf(_currentWarning);
					var newIndex = (currentIndex+1)% WarningList.Count;
					_currentWarning = WarningList.ElementAt(newIndex);
				}
				Device.BeginInvokeOnMainThread(()=>{
					_lbl.Text = _currentWarning.Title;
					this.BackgroundColor = _currentWarning.BackgroundColor;
					this.IsVisible = true;
				});
			}else{
				_currentWarning = null;
				this.IsVisible = false;
				_timer.Stop();
			}
		}
	}

	public class WarningAction
	{
		public string Title{ get; set;}
		public string Identifer{get;set;}
		public Action Action{get;set;}
		public Color BackgroundColor{get;set;}
	}
}

