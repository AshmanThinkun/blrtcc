﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomViews
{
	public class ExtendedEditor : Editor
	{
		string _placeholder;
		FormattedString _attrStr;
		public bool HideBottomDrawable{ get; set;}
		public Color PlaceholderTextColor{ get; set;}

		public ExtendedEditor ()
		{
			this.FontSize = (float)BlrtStyles.CellTitleFont.FontSize;
			this.PlaceholderTextColor = BlrtStyles.BlrtGray5;
		}

		public float FontSize{ get; set;}

		public string Placeholder
		{
			get {
				return _placeholder;
			}

			set {
				_placeholder = value;
				if (null != SetPlaceholderDelegate) {
					SetPlaceholderDelegate (_placeholder);
				}
			}
		}

		public FormattedString FormattedText
		{ 
			get {
				return _attrStr;
			}

			set {
				if (value != _attrStr) {
					if (null != _attrStr) {
						_attrStr.PropertyChanged -= OnAttrStrPropertyChanged;
					}
					_attrStr = value;
					if (null != _attrStr) {
						_attrStr.PropertyChanged += OnAttrStrPropertyChanged;
					}
					if (null != SetFormattedTextDelegate) {
						SetFormattedTextDelegate (_attrStr);
					}
				}
			}
		}

		public int CursorPosition
		{
			get {
				if (null != GetCursorDelegate) {
					return GetCursorDelegate ();
				}
				return -1;
			}

			set {
				if (null != MoveCursorDelegate) {
					Device.BeginInvokeOnMainThread (() => {
						MoveCursorDelegate (value);
					});
				}
			}
		}

		public int Lines
		{
			get {
				if (null != GetLinesDelegate) {
					return GetLinesDelegate ();
				}
				return -1;
			}

			set {
				if (null != SetLinesDelegate) {
					SetLinesDelegate (value);
				}
			}
		}

		public int ContentLineCount
		{
			get {
				if (null != GetContentLineCountDelegate) {
					return GetContentLineCountDelegate ();
				}
				return -1;
			}
		}

		public void Post(Action action)
		{
			if (null != PostDelegate) {
				PostDelegate (action);
			} else {
				action ();
			}
		}

		void OnAttrStrPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (null != SetFormattedTextDelegate) {
				SetFormattedTextDelegate (FormattedText);
			}
		}

		// delegate for custom renderer
		public Action<FormattedString>		SetFormattedTextDelegate;
		public Action<int>					MoveCursorDelegate;
		public Func<int>					GetCursorDelegate;
		public Action<string>				SetPlaceholderDelegate;
		public Func<int>					GetLinesDelegate;
		public Action<int>					SetLinesDelegate ;
		public Func<int>					GetContentLineCountDelegate;
		public Action<Action>				PostDelegate;
	}
}

