using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace BlrtData.Views.CustomUI
{
	public class BlrtPageControl : StackLayout
	{
		private const float DefaultIndicatorSpacing = 10f;
		private const float DefaultIndicatorRadius = 3f;
		private int _currentPage;
		private int _totalPages;
		private Color _currentPageIndicatorTintColor;
		private Color _pageIndicatorTintColor;
		private float _indicatorRadius;
		private List<RoundedBoxView> _indicators;

		public BlrtPageControl (int totalPages)
		{
			if (totalPages <= 0) {
				throw new ArgumentOutOfRangeException ("totalPages");
			}
			VerticalOptions = LayoutOptions.Center;
			HorizontalOptions = LayoutOptions.Center;
			Orientation = StackOrientation.Horizontal;
			Spacing = DefaultIndicatorSpacing;
			_indicatorRadius = DefaultIndicatorRadius;
			_currentPage = 0;
			_totalPages = totalPages;
			_indicators = new List<RoundedBoxView> (_totalPages);

			_currentPageIndicatorTintColor = Color.Black;
			_pageIndicatorTintColor = Color.White;

			for (int i = 0; i < _totalPages; i++) {
				var item = new RoundedBoxView () {
					Color = _pageIndicatorTintColor,
					WidthRequest = _indicatorRadius * 2,
					HeightRequest = _indicatorRadius * 2,
					CornerRadius = _indicatorRadius,
					HasShadow = false,
					Stroke = Color.Transparent,
					StrokeThickness = 0
				};
				if (i == _currentPage) {
					item.Color = _currentPageIndicatorTintColor;
				}
				_indicators.Add (item);
				Children.Add (item);
			}
		}



		/// <summary>
		/// Gets or sets the current page.
		/// </summary>
		/// <value>The current page.</value>
		public virtual int CurrentPage
		{
			get
			{
				return _currentPage;
			}
			set
			{
				if (_totalPages > 0) {
					if (value < 0) {
						ChangePage (0);
					} else if (value >= _totalPages) {
						ChangePage (_totalPages - 1);
					} else {
						ChangePage (value);
					}
				}
			}
		}

		/// <summary>
		/// Changes the current page.
		/// </summary>
		/// <param name="toPage">destination page to which should be change</param>
		private void ChangePage (int toPage)
		{
			if (toPage != _currentPage) {
				_indicators [_currentPage].Color = _pageIndicatorTintColor;
				_indicators [toPage].Color = _currentPageIndicatorTintColor;
				_currentPage = toPage;
			}
		}

		/// <summary>
		/// Gets or sets the color of the current page indicator.
		/// </summary>
		/// <value>The color of the current page indicator.</value>
		public virtual Color CurrentPageIndicatorTintColor
		{
			get
			{
				return _currentPageIndicatorTintColor;
			}
			set
			{
				if (value != _currentPageIndicatorTintColor) {
					_currentPageIndicatorTintColor = value;
					_indicators [_currentPage].Color = _currentPageIndicatorTintColor;
				}
			}
		}

		/// <summary>
		/// Gets or sets the color of the other page indicators (except the current one).
		/// </summary>
		/// <value>The color of the other page indicators.</value>
		public virtual Color PageIndicatorTintColor
		{
			get
			{
				return _pageIndicatorTintColor;
			}
			set
			{
				if (value != _pageIndicatorTintColor) {
					_pageIndicatorTintColor = value;
					for (int i = 0; i < _indicators.Count; i++) {
						if (i != _currentPage) {
							_indicators [i].Color = _pageIndicatorTintColor;
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the indicator radius.
		/// </summary>
		/// <value>The indicator radius.</value>
		public virtual float IndicatorRadius
		{
			get
			{
				return _indicatorRadius;
			}
			set
			{
				if (value != _indicatorRadius) {
					_indicatorRadius = value;
					for (int i = 0; i < _indicators.Count; i++) {
						_indicators [i].WidthRequest = _indicatorRadius * 2;
						_indicators [i].HeightRequest = _indicatorRadius * 2;
						_indicators [i].CornerRadius = _indicatorRadius;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the total page count.
		/// </summary>
		/// <value>The total page count.</value>
		public virtual int Pages
		{
			get
			{
				return _totalPages;
			}
		}
	}
}

