﻿using System;
using BlrtData.Views.CustomUI;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class FormsMicButton : Button
	{
		public event EventHandler Pressed;
		public event EventHandler Released;

		public virtual void OnPressed ()
		{
			Pressed?.Invoke (this, EventArgs.Empty);
		}

		public virtual void OnReleased ()
		{
			Released?.Invoke (this, EventArgs.Empty);
		}
	}
}
