﻿using System;
using Xamarin.Forms;

namespace BlrtData.Views.CustomUI
{
	public class ConvoSaveBtnEventArgs : EventArgs
	{
		public string ConvoName { get; private set; }

		public ConvoSaveBtnEventArgs (string convoName)
		{
			ConvoName = convoName;
		}
	}

	public class EditConvoNameView : ContentView
	{
		public EventHandler ConvoCancelBtnPressed;
		public EventHandler<ConvoSaveBtnEventArgs> ConvoSaveBtnPressed;

		public string CurrentConvoName { get; set; }

		public EditConvoNameView(string currentConvoName) {
			CurrentConvoName = currentConvoName;
		}

		public void OnConvoCancelBtnPressed (object sender, EventArgs e)
		{
			if (ConvoCancelBtnPressed != null)
			{
				ConvoCancelBtnPressed (null, null);
			}
		}

		public void OnConvoSaveBtnPressed (object sender, ConvoSaveBtnEventArgs e) 
		{
			if (ConvoSaveBtnPressed != null)
			{
                ConvoSaveBtnPressed (null, e);
			}
		}

	}
}