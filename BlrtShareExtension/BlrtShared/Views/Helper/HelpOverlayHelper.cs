﻿using System.Collections.Generic;
using Parse;
using System;

#if __iOS__
using BlrtiOS.UI;

#elif __ANDROID__
using BlrtAndroid.UI;
#endif

namespace BlrtData.Views.Helpers
{
	public static class HelpOverlayHelper
	{
		static HelpOverlay HelpOverlayWithNextBtn {
			get {
				var btnTtile = BlrtUtil.PlatformHelper.Translate ("canvas_help_overlay_next_button_title", "helpoverlay");
				return new HelpOverlay (btnTtile);
			}
		}

		static HelpOverlay HelpOverlayWithOkBtn {
			get {
				var btnTtile = BlrtUtil.PlatformHelper.Translate ("canvas_help_overlay_ok_button_title", "helpoverlay");
				return new HelpOverlay (btnTtile);
			}
		}

		/// <summary>
		/// Gets the base help overlay instruction rows for non empty convo. This is used by non-empty 
		/// convos both with and without welcome blrt
		/// </summary>
		/// <value>The base help overlay instruction rows for non empty convo.</value>
		static List<HelpOverlay.InstructionRowModel> BaseHelpOverlayInstructionRowsForNonEmptyConvos {
			get {
				var list = new List<HelpOverlay.InstructionRowModel> ();

				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_play", "helpoverlay"),
					Image = "help_screen_convo.png",
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_volume", "helpoverlay"),
					Image = "help_screen_volume.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_reply", "helpoverlay"),
					Image = "help_screen_reply.png"
				});

				return list;
			}
		}

		/// <summary>
		/// Gets the help overlay for conversation with welcome blrt.
		/// </summary>
		/// <value>The help overlay for conversation with welcome blrt.</value>
		static HelpOverlay HelpOverlayForConvo_WithWelcomeBlrt {
			get {
				var helpOverlay = HelpOverlayWithOkBtn;
				//pop the content
				helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("convo_title", "helpoverlay");
				var list = BaseHelpOverlayInstructionRowsForNonEmptyConvos;

				list.Insert (0, new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_welcome_blrt_help_msg", "helpoverlay"),
					SeparatorHidden = true
				});
				helpOverlay.InstructionsList = list;
				helpOverlay.ConfirmButtonTapped += (sender, e) => {
					BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed] = true;
					BlrtPersistant.SaveProperties ();
				};

				return helpOverlay;
			}
		}

#if __IOS__
		/// <summary>
		/// Adds the conversation help overlay.
		/// </summary>
		/// <returns>The conversation help overlay.</returns>
		/// <param name="convoContainsBlrt">If set to <c>true</c> convo contains blrt.</param>
		/// <param name="convoContainsWelcomeBlrt">If set to <c>true</c> convo contains welcome blrt.</param>
		public static object AddConversationHelpOverlay (bool convoContainsBlrt, bool convoContainsWelcomeBlrt)
		{
			var helpOverlay = !convoContainsBlrt ? HelpOverlayForConvo_WithoutBlrt :
				(
				convoContainsWelcomeBlrt ? HelpOverlayForConvo_WithWelcomeBlrt : HelpOverlayForConvo_WithNonWelcomeBlrt
				);
			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Conversation");
			return helpOverlay;
		}

		/// <summary>
		/// Gets the help overlay for convo with non welcome blrt.
		/// </summary>
		/// <value>The help overlay for convo with non welcome blrt.</value>
		static HelpOverlay HelpOverlayForConvo_WithNonWelcomeBlrt {
			get {
				var helpOverlay = HelpOverlayWithOkBtn;
				//pop the content
				helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("convo_title", "helpoverlay");
				var list = BaseHelpOverlayInstructionRowsForNonEmptyConvos;

				list.Insert (2, new HelpOverlay.InstructionRowModel {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_add", "helpoverlay"),
					Image = "help_screen_add.png"
				});

				helpOverlay.InstructionsList = list;
				helpOverlay.ConfirmButtonTapped += (sender, e) => {
					BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed] = true;
					BlrtPersistant.SaveProperties ();
				};

				return helpOverlay;
			} 
		}

		/// <summary>
		/// Gets the help overlay for convo without blrt.
		/// </summary>
		/// <value>The help overlay for convo without blrt.</value>
		static HelpOverlay HelpOverlayForConvo_WithoutBlrt {
			get {
				var helpOverlay = HelpOverlayWithOkBtn;
				//pop the content
				helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("convo_title", "helpoverlay");
								var list = new List<HelpOverlay.InstructionRowModel> ();

				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_newblrt", "helpoverlay"),
					Image = "help_screen_newblrt.png",
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_camera", "helpoverlay"),
					Image = "media_camera.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_audio", "helpoverlay"),
					Image = "media_audio_dark.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_media_add", "helpoverlay"),
					Image = "media_add.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_people_add", "helpoverlay"),
					Image = "help_screen_add.png"
				});

				helpOverlay.InstructionsList = list;
				helpOverlay.ConfirmButtonTapped += (sender, e) => {
					BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed] = true;
					BlrtPersistant.SaveProperties ();
				};

				return helpOverlay;
			}
		}
#endif
		/// <summary>
		/// Gets the help overlay for non empty convo.
		/// </summary>
		/// <value>The help overlay for non empty convo.</value>
		static HelpOverlay HelpOverlayForNonEmptyConvo {
			get {
				var helpOverlay = HelpOverlayWithOkBtn;
				//pop the content
				helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("convo_title", "helpoverlay");
				var list = BaseHelpOverlayInstructionRowsForNonEmptyConvos;

				list.Insert (2, new HelpOverlay.InstructionRowModel {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_add", "helpoverlay"),
					Image = "help_screen_add.png"
				});

				helpOverlay.InstructionsList = list;
				helpOverlay.ConfirmButtonTapped += (sender, e) => {
					BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed] = true;
					BlrtPersistant.SaveProperties ();
				};

				return helpOverlay;
			}
		}

		/// <summary>
		/// Gets the help overlay for empty convo.
		/// </summary>
		/// <value>The help overlay for empty convo.</value>
		static HelpOverlay HelpOverlayForEmptyConvo {
			get {
				var helpOverlay = HelpOverlayWithOkBtn;
				//pop the content
				helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("convo_title", "helpoverlay");
				var list = new List<HelpOverlay.InstructionRowModel> ();

				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_newblrt", "helpoverlay"),
					Image = "help_screen_newblrt.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_camera", "helpoverlay"),
					Image = "media_camera.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_audio", "helpoverlay"),
					Image = "media_audio_dark.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_media_add", "helpoverlay"),
					Image = "media_add.png"
				});
				list.Add (new HelpOverlay.InstructionRowModel () {
					Title = BlrtUtil.PlatformHelper.Translate ("convo_people_add", "helpoverlay"),
					Image = "help_screen_add.png"
				});

				helpOverlay.InstructionsList = list;
				helpOverlay.ConfirmButtonTapped += (sender, e) => {
					BlrtPersistant.Properties [BlrtConstants.HelpOverlayEmptyConversationViewed] = true;
					BlrtPersistant.SaveProperties ();
				};

				return helpOverlay;
			}
		}

#if __ANDROID__
		/// <summary>
		/// Adds the conversation help overlay.
		/// </summary>
		/// <returns>The conversation help overlay.</returns>
		/// <param name="isEmptyConvo">If set to <c>true</c> is empty convo.</param>
		/// <param name="isConvoWithWelcomeBlrt">If set to <c>true</c> is convo with welcome blrt.</param>
		public static object AddConversationHelpOverlay (bool isEmptyConvo, bool isConvoWithWelcomeBlrt)
		{
			var helpOverlay = isEmptyConvo ? HelpOverlayForEmptyConvo :
				(
					isConvoWithWelcomeBlrt ? HelpOverlayForConvo_WithWelcomeBlrt : HelpOverlayForNonEmptyConvo
				);
			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Conversation");
			return helpOverlay;
		}

#endif
		public static object AddMailboxHelpOverlay ()
		{
			var helpOverlay = HelpOverlayWithOkBtn;
			//pop the content
			helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("mailbox_title", "helpoverlay");
			var list = new List<HelpOverlay.InstructionRowModel> ();
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = Xamarin.Forms.Device.Idiom ==
#if __ANDROID__
					Xamarin.Forms.TargetIdiom.Phone ? BlrtUtil.PlatformHelper.Translate ("mailbox_newblrt_android", "helpoverlay") : BlrtUtil.PlatformHelper.Translate ("mailbox_newblrt_tablet_android", "helpoverlay")
#else
					Xamarin.Forms.TargetIdiom.Phone ? BlrtUtil.PlatformHelper.Translate ("mailbox_newConvo", "helpoverlay") : BlrtUtil.PlatformHelper.Translate ("mailbox_newblrt_ipad", "helpoverlay")
#endif
				, Image = "help_screen_newconvo.png"
#if __ANDROID__
				, Padding = 2
#endif
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("mailbox_menu", "helpoverlay"),
				Image = "help_screen_menu.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("mailbox_refresh", "helpoverlay"),
				Image = "help_screen_tap.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
#if __IOS__
					Title = BlrtUtil.PlatformHelper.Translate ("mailbox_convo_options", "helpoverlay"),
#elif __ANDROID__
				Title = BlrtUtil.PlatformHelper.Translate ("mailbox_convo_options_android", "helpoverlay"),
#endif
				Image = "help_screen_convo_options.png"
			});
			helpOverlay.InstructionsList = list;
			helpOverlay.ConfirmButtonTapped += (sender, e) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayMailboxViewed] = true;
				BlrtPersistant.SaveProperties ();
			};

			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Mailbox");
			return helpOverlay;
		}

		public static void AddCanvasDrawHelpOverlay ()
		{
			var helpOverlay = HelpOverlayWithNextBtn;
			//pop the content
			helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_title", "helpoverlay");

			//pop the content
			var list = new List<HelpOverlay.InstructionRowModel> ();
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_colour", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_colour.png",
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_pencil", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_pencil.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_point", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_point.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_line", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_line.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_shape", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_circ_rect.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_draw_thickness", "helpoverlay"),
				LeftAlign = true,
				Image = "help_screen_stroke.png"
			});
			helpOverlay.InstructionsList = list;

			helpOverlay.ConfirmButtonTapped += (sender, e) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed] = true;
				BlrtPersistant.SaveProperties ();
				try {
					if (ParseUser.CurrentUser == null) {

					} else if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasNavViewed)
								|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed]) {
						AddCanvasNavHelpOverlay ();
					}
				} catch {
				}
			};

			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Recording1");
		}

		public static void AddCanvasNavHelpOverlay ()
		{
			var helpOverlay = HelpOverlayWithOkBtn;
			//pop the content
			helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("canvas_nav_title", "helpoverlay");
			var list = new List<HelpOverlay.InstructionRowModel> ();
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_nav_move", "helpoverlay"),
				Image = "help_screen_move.png",
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_nav_zoom", "helpoverlay"),
				Image = "help_screen_pinch.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_nav_page", "helpoverlay"),
				Image = "help_screen_page.png"
			});
			helpOverlay.InstructionsList = list;

			helpOverlay.ConfirmButtonTapped += (sender, e) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed] = true;
				BlrtPersistant.SaveProperties ();
			};

			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Recording2");
		}

		public static void AddCanvasStartHelpOverlay ()
		{
			var helpOverlay = HelpOverlayWithNextBtn;
			//pop the content
			helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("canvas_start_title", "helpoverlay");
			var list = new List<HelpOverlay.InstructionRowModel> ();
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_start_record", "helpoverlay"),
				Image = "help_screen_record.png",
			});
#if __IOS__
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate("canvas_start_pause","helpoverlay"),
				Image = "help_screen_pause.png"
			});
#elif __ANDROID__
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("Don't worry! You can play back your Blrt before saving"),
				Image = "help_screen_pause.png"
			});
#endif

			helpOverlay.InstructionsList = list;
			helpOverlay.ConfirmButtonTapped += (sender, e) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed] = true;
				BlrtPersistant.SaveProperties ();

				try {
					if (ParseUser.CurrentUser == null) {

					} else if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasDrawViewed)
							   || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed]) {
						AddCanvasDrawHelpOverlay ();
					}
				} catch {
				}
			};

			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Recording3");
		}

		public static void AddCanvasFinishHelpOverlay ()
		{
			var helpOverlay = HelpOverlayWithOkBtn;
			//pop the content
			helpOverlay.Title = BlrtUtil.PlatformHelper.Translate ("canvas_finish_title", "helpoverlay");
			var list = new List<HelpOverlay.InstructionRowModel> ();
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_finish_playback", "helpoverlay"),
				Image = "help_screen_play.png",
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_finish_startover", "helpoverlay"),
				Image = "help_screen_cross.png"
			});
			list.Add (new HelpOverlay.InstructionRowModel () {
				Title = BlrtUtil.PlatformHelper.Translate ("canvas_finish_save", "helpoverlay"),
				Image = "help_screen_save.png"
			});
			helpOverlay.InstructionsList = list;
			helpOverlay.ConfirmButtonTapped += (sender, e) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasFinishViewed] = true;
				BlrtPersistant.SaveProperties ();
			};

			helpOverlay.Open ();
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("HelpOverlay_Recording4");
		}


		public static bool AtLeastOneOverlayViewed ()
		{
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayMailboxViewed)
				&& (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayMailboxViewed])
				return true;
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationViewed)
				&& (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed])
				return true;
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed)
				&& (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed])
				return true;
			
#if __ANDROID__
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayEmptyConversationViewed)
				&& (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayEmptyConversationViewed])
#elif __IOS__
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed)
			    && (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed])
#endif
				return true;
			
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasDrawViewed)
			    && (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed])
				return true;
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasNavViewed)
			    && (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed])
				return true;
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasStartViewed)
			    && (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed])
				return true;
			if (BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasFinishViewed)
			    && (bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasFinishViewed])
				return true;
			return false;
		}
	}
}

