using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlrtData.Views.Helpers
{
	public interface ITableModelCell
		: System.ComponentModel.INotifyPropertyChanged
	{
		string CellIdentifer { get; }

		string Title { get; }
		ICellAction Action { get; }
	}


	public abstract class TableModelSection<T>
		: ObservableCollection<T>, System.ComponentModel.INotifyPropertyChanged 
			where T : ITableModelCell
	{
		public abstract string HeaderIdentifer { get; }
		public abstract string FooterIdentifer { get; }
		public abstract string Title { get; set; }

		public abstract ICellAction Action { get; set; }
	}

	public interface ICellAction {
		void Action(object sender);
	}


	public interface IBindable<T>{
		void Bind(T data);
	}
}

