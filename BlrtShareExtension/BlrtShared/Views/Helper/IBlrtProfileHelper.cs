using System;
using System.Threading.Tasks;

namespace BlrtData.Views.Helpers
{
	public interface IBlrtProfileHelper
	{
		Task<bool> AddSecondaryEmail (object controller, string email = null);
		Task<bool> ReloadContactDetails (bool fetchUserObject = false);
		Task<bool> CheckPassword (object controller);
		string GetUpgradeButtonText ();
	}
}

