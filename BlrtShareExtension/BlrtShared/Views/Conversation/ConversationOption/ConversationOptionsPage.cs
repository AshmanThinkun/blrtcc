using System;
using System.Linq;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtData.ExecutionPipeline;
using BlrtData.Views.CustomUI;
using System.Threading;
using BlrtAPI;
using BlrtData.Models.ConversationScreen;
using Parse;
using System.Runtime.CompilerServices;

#if __ANDROID__
using Acr.UserDialogs;
#endif

#if __IOS__
using System.IO;
using BlrtShared;
using BlrtData.Views.Send;
using UIKit;
using BlrtiOS;
#endif

namespace BlrtData.Views.Conversation.ConversationOptions
{
	public class ConversationOptionsPage : BlrtContentPage, IExecutor
	{
		static Color READONLY_CONVO_TEXT_COLOR = BlrtStyles.BlrtGray4;
		static float READONLY_CONVO_OPACITY = 0.4f;

		#region IExecutor implementation

		ExecutionSource IExecutor.Cause {
			get {
				return ExecutionSource.System;
			}
		}

		#endregion

		public override string ViewName {
			get {
				return "Conversation_Options";
			}
		}

		/// <summary>
		/// Gets the remove people button text. This button shows a cross next to each person in the convo.
		/// </summary>
		/// <value>The remove people button text.</value>
		string RemovePeopleBtnText {
			get {
				return BlrtUtil.PlatformHelper.Translate ("remove_people", "conversation_option");
			}
		}

		/// <summary>
		/// Gets the remove-people-cancel button text. This button hides a cross next to each person in the convo.
		/// </summary>
		/// <value>The remove people cancel button text.</value>
		string RemovePeopleCancelBtnText {
			get {
				return BlrtUtil.PlatformHelper.Translate ("remove_people_cancel", "conversation_option");
			}
		}


		/// <summary>
		/// Gets a value indicating whether current user is a premium or trial, or free
		/// </summary>
		/// <value><c>true</c> if user is either premium or trial; otherwise, <c>false</c>.</value>
		bool IsPremiumOrTrialUser {
			get {
				return (BlrtPermissions.BlrtAccountThreshold.Premium == BlrtPermissions.AccountThreshold ||
					   BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Trial);
			}
		}

		/// <summary>
		/// Gets a value indicating whether current user is the creator of currently open conversation
		/// </summary>
		/// <value><c>true</c> if is current user convo creator; otherwise, <c>false</c>.</value>
		bool IsConvoCreator {
			get {
				return _relation.CreatorId == ParseUser.CurrentUser.ObjectId;
			}
		}

		/// <summary>
		/// Gets a value indicating whether remove botton is visible and flips the state of this button
		/// </summary>
		/// <value><c>true</c> if flip and return remove people button text state; otherwise, <c>false</c>.</value>
		bool FlipAndReturnRemovePeopleBtnTextState {
			get {
				var removePeopleBtnTextHidden = RemovePeopleButtonTextHidden;

				// change text
				_removeBtn.Text = removePeopleBtnTextHidden ? RemovePeopleBtnText : RemovePeopleCancelBtnText;
				return !removePeopleBtnTextHidden; // state has been flipped as text has been flipped
			}
		}

		/// <summary>
		/// Gets a value indicating whether remove-people-button text is hidden.
		/// </summary>
		/// <value><c>true</c> if remove people button text hidden; otherwise, <c>false</c>.</value>
		bool RemovePeopleButtonTextHidden {
			get {
				var removePeopleCancelBtnText = RemovePeopleCancelBtnText;
				return _removeBtn.Text == removePeopleCancelBtnText;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether user is editing convo name. 
		/// It is used to decide wheteher to set result for task completion source that lauched options page.
		/// </summary>
		/// <value><c>true</c> if is editing convo name; otherwise, <c>false</c>.</value>
		bool IsEditingConvoName { get; set; }

		/// <summary>
		/// Sets a value indicating whether loading indicator is hidden.
		/// </summary>
		/// <value><c>true</c> if loading indicator hidden; otherwise, <c>false</c>.</value>
		bool LoadingIndicatorHidden {
			set {
				if (value == true) {
					if (HideLoading != null) {
						HideLoading (this, EventArgs.Empty);
					}
				} else {
					if (ShowLoading != null) {
						ShowLoading (this, EventArgs.Empty);
					}
				}
			}
		}

		/// <summary>
		/// Gets a value indicating whether the user of user relation is creator.
		/// </summary>
		/// <value><c>true</c> if is creator; otherwise, <c>false</c>.</value>
		bool IsCreator {
			get {
				return _relation.CreatorId == ParseUser.CurrentUser.ObjectId;
			}
		}

		/// <summary>
		/// Notifies whether mute enum provided is mute
		/// </summary>
		/// <returns><c>true</c>, if enum is mute, <c>false</c> otherwise.</returns>
		/// <param name="muteType">Mute type.</param>
		bool IsMute (APIConversationUserRelationMute.Parameters.MuteEnum muteType)
		{
			return APIConversationUserRelationMute.Parameters.ShouldMute (_relation.MuteFlag, (int)muteType);
		}

		public override void TrackPageView ()
		{
			if (_relation == null)
				return;

			var dic = new Dictionary<string, object> ();
			var rel = _relation;

			dic.Add ("isOwner", rel.CreatorId == ParseUser.CurrentUser.ObjectId);
			dic.Add ("convoId", rel.ConversationId);

			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, dic);
		}

		int _ignoreNavigationStack;

		TaskCompletionSource<ConversationOptionResult> _tcs;
		private List<string> _filteredIds;

		public event EventHandler ShowLoading, HideLoading;

		public async Task<ConversationOptionResult> WaitTask ()
		{
			return await _tcs.Task;
		}

		public class ConversationOptionResult
		{
			public string [] FilterUsers { get; set; }

			public string ConversationId { get; set; }

			public bool Navigated { get; set; }
		}

		const int ThumbHeight = 72;
		const int SectionHeaderHeight = 40;
		const int rowHeight = 44;

		ConversationUserRelation _relation;
		ConversationHandler _handler;

		Image _convoThumb;
		Label _convoTitle;

#if __ANDROID__
		SwitchCellWithPrimAndSecLabels _allowPublicBlrt;
#endif

		Button _editConvoNameBtn;
		SwitchCell _flag, _archive, _allowAdd, _muteEmail, _mutePush;
		TableSection _filterSec;
		TableView _tableView;

#if __IOS__
		EditConvoNameView _editConvoNameView;
		SwitchCell _allowPublicBlrt;
#endif

		AbsoluteLayout _mainLayout;
		ViewCell _filterTitleCell;
		IConversationScreen _conversationScreen;
		ActivityIndicator _filterLoader;


		public ConversationOptionsPage (IConversationScreen conversationScreen, ConversationUserRelation relation, string [] filteredIds
#if __IOS__
										, bool readonlyConversation
#endif
									   )
		{
			_conversationScreen = conversationScreen;
			_relation = relation;
			var convo = relation.Conversation;
			_filteredIds = filteredIds == null ? new List<string> () : filteredIds.ToList ();

			_tcs = new TaskCompletionSource<ConversationOptionResult> ();

			Title = BlrtUtil.PlatformHelper.Translate ("conversation_option_screen", "screen_titles");
			var titleCell = GetPageTitleCell (
#if __IOS__
				readonlyConversation
#endif
			);
#if __IOS_
			titleCell.IsEnabled = !readonlyConversation;
#endif
			var addPeopleCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("add_people", "conversation_option")
#if __IOS__
												 , readonlyConversation
#endif
												);
			addPeopleCell.Tapped += (sender, e) => {
				OpenSendingPage ().FireAndForget ();
			};

#if __ANDROID__
			var openSubPageToken = MultiExecutionPreventor.NewToken ();
			var shareLinkCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("Share link to this conversation"));
			shareLinkCell.Tapped += MultiExecutionPreventor.NewEventHandler (OpenShareLinkPage, openSubPageToken);
#endif

			var tagsCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("add_tags", "conversation_option")
#if __IOS__
											, false
#endif
										   );
			tagsCell.Tapped += (sender, e) => {
				OpenTagPage ().FireAndForget ();
			};

			_flag = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("flag", "conversation_option"),
				On = relation.Flagged
			};

			_archive = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("archive", "conversation_option"),
				On = relation.Archive,
				ShowSeparator = false,
			};

			_allowAdd = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("allow_add", "conversation_option"),
				On = !_relation.Conversation.DisableOthersToAdd,
				IsSwitchEnabled = IsConvoCreator && IsPremiumOrTrialUser,
				ShowSeparator = true
			};


			_muteEmail = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("mute_email", "conversation_option"),
				On = APIConversationUserRelationMute.Parameters.ShouldMute (relation.MuteFlag, (int)(APIConversationUserRelationMute.Parameters.MuteEnum.Email))
			};


			_mutePush = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("mute_push", "conversation_option"),
				On = APIConversationUserRelationMute.Parameters.ShouldMute (relation.MuteFlag, (int)(APIConversationUserRelationMute.Parameters.MuteEnum.Push))
			};

#if __ANDROID__
			_allowPublicBlrt = new SwitchCellWithPrimAndSecLabels (IsConvoCreator && IsPremiumOrTrialUser,
							  !_relation.Conversation.DisAllowPublicBlrt,
							  BlrtUtil.PlatformHelper.Translate ("allowPublic", "conversation_option"),
							  BlrtUtil.PlatformHelper.Translate ("allow_public_description", "conversation_option"));
#endif

#if __IOS__
			_allowAdd.IsEnabled = !readonlyConversation;
			_muteEmail.IsEnabled = !readonlyConversation;
			_mutePush.IsEnabled = !readonlyConversation;

			_allowPublicBlrt = new FormsSwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("allowPublic", "conversation_option"),
				IsSwitchEnabled = IsConvoCreator && IsPremiumOrTrialUser,
				On = !_relation.Conversation.DisAllowPublicBlrt,
				ShowSeparator = false
			};
			_allowPublicBlrt.IsEnabled = !readonlyConversation;
#endif
			_muteEmail.OnChanged += MuteEmailSwitched;
			_mutePush.OnChanged += MutePushSwitched;
			_allowAdd.OnChanged += AllowAddSwitched;
			_allowPublicBlrt.OnChanged += AllowPublicBlrtSwitched;
			_allowPublicBlrt.Tapped += PremiumAndCreatorCellTapped;
			_allowAdd.Tapped += PremiumAndCreatorCellTapped;

#if __ANDROID__
			if (BlrtApp.PlatformHelper.AndroidResource.DisplayMetrics.Density <= 1) {
				_muteEmail.Height = rowHeight * 1.2f;
				_mutePush.Height = rowHeight * 1.2f;
				_flag.Height = rowHeight * 1.2f;
				_archive.Height = rowHeight * 1.2f;
				_allowAdd.Height = rowHeight * 1.2f;
				_allowPublicBlrt.Height = rowHeight * 1.2f;
			}
#endif
			_filterSec = new TableSection ();

			_filterLoader = new ActivityIndicator () {
				HorizontalOptions = LayoutOptions.Start,
				IsRunning = true,
				Color = BlrtStyles.BlrtBlackBlack,
				IsVisible = false
			};

			_filterTitleCell = GetFilterTitleCell ();

			var premiumFeatureTitle = GetPremiumFeatureTitleCell ();

			LoadRelations ();
			Xamarin.Forms.MessagingCenter.Subscribe<Object, bool> (this, string.Format ("{0}.RelationChanged", _relation.Conversation.LocalGuid), (o, refreshFailed) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					LoadRelations ();
				});
			});

			var normalSection = new TableSection () {
				titleCell,
				addPeopleCell,
#if __ANDROID__
				shareLinkCell,
#endif
				tagsCell
			};

			if (IsConvoCreator) {
				var deleteCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("delete_convo", "conversation_option")
#if __IOS__
												  , readonlyConversation
#endif
												 );
				deleteCell.Tapped += (sender, e) => {
#if __IOS__
					DeleteCell ();
#endif
#if __ANDROID__
					DeleteConversation ().FireAndForget ();
#endif
				};
				normalSection.Add (deleteCell);
			} else {
				var leaveCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("leave_convo", "conversation_option")
#if __IOS__
												 , false
#endif
												);
				leaveCell.Tapped += (sender, e) => {
#if __IOS__
					LeaveCell ().FireAndForget ();
#endif
#if __ANDROID__
					LeaveConversation ().FireAndForget ();
#endif
				};
				normalSection.Add (leaveCell);
			}

#if __IOS__
			normalSection.Add (GetShareLinkToConvoCell (readonlyConversation));
			var allowPublicDescription = GetPublicDescriptionCell ();
#endif
			normalSection.Add (_muteEmail);
			normalSection.Add (_mutePush);
			normalSection.Add (_flag);
			normalSection.Add (_archive);

			_tableView = new FormsTableView () {
				ShowSectionHeader = false,
				HasUnevenRows = true,
				RowHeight = rowHeight,
				VerticalOptions = LayoutOptions.Fill,
				Intent = TableIntent.Data,
				BackgroundColor = BlrtStyles.BlrtWhite,
				Root = new TableRoot {
					normalSection,
					new TableSection {
						premiumFeatureTitle,
						_allowAdd,
						_allowPublicBlrt,
						#if __IOS__
						allowPublicDescription
						#endif
					},
					_filterSec
				}
			};

			SetMainLayout ();
			Content = _mainLayout;

			_flag.OnChanged += (object sender, ToggledEventArgs e) => {
				Task.Run (delegate {
					if (BlrtUtil.FlagConversationUserRelation (_relation, _flag.On)) {
#if __ANDROID__
						ReloadMailboxFilteredList ();
#endif
#if __IOS__
						BlrtManager.RecalulateAppBadge ();
#endif
					}
				});
			};

			_archive.OnChanged += (object sender, ToggledEventArgs e) => {
				Task.Run (delegate {
					if (BlrtUtil.ArchiveConversationUserRelation (_relation, _archive.On)) {
#if __ANDROID__
						ReloadMailboxFilteredList ();
#endif
#if __IOS__
						BlrtManager.RecalulateAppBadge ();
#endif
					}
				});
			};

			_ignoreNavigationStack = 0;
		}

		/// <summary>
		/// Reloads the mailbox filtered list. Filtered list orders/ displays the 
		/// conversations based on the applied filters.
		/// </summary>
		void ReloadMailboxFilteredList ()
		{
			BlrtManager.RecalulateAppBadge ();
			Models.Mailbox.MailboxFilter.CurrentInstance.ReloadList ().FireAndForget ();
		}

#if __ANDROID__
		/// <summary>
		/// Triggered when the convo save button is pressed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		string ConversationTitle {
			set {
				if (!string.IsNullOrEmpty (value) && !value.Equals (_convoTitle.Text)) {
					_convoTitle.Text = value;

					Task.Run (async delegate {
						BlrtUtil.SaveConversationName (_conversationScreen.CurrentConversation, value);
						await (_conversationScreen as BlrtApp.Views.ConversationView.ConversationPage).Refresh ();
					});
				}
			}
		}
#endif
		void OnEditConvoNameBtnClicked (object s, EventArgs e)
		{
			IsEditingConvoName = true;
#if __IOS__
			ShowEditConvoNameViewOniOS (true);
#elif __ANDROID__
			ShowEditConvoNameViewOnAndroid ().FireAndForget ();
#endif
		}

#if __ANDROID__
		async Task ShowEditConvoNameViewOnAndroid ()
		{
			var page = new ConvoNameSetupPage ("Save",
											   true,
											   _conversationScreen.CurrentConversation.Name,
											   _conversationScreen.CurrentConversation.ObjectId);
			await (_conversationScreen as Page).Navigation.PushAsync (page);
			var newConvoName = await page.WaitConvoNameConfirmation;
			IsEditingConvoName = false;
			ConversationTitle = newConvoName;
		}
#endif

#if __IOS__
		/// <summary>
		/// Called when the share-link-to-convo cell is tapped
		/// </summary>
		async Task OnShareLinkToConvoCell ()
		{
			_ignoreNavigationStack++;
			await _conversationScreen.OpenSharePage ();

			_ignoreNavigationStack--;
			RunRelationQuery ().FireAndForget ();
		}

		/// <summary>
		/// Gets the share-link-to-convo cell.
		/// </summary>
		/// <value>The share link to convo cell.</value>
		Cell GetShareLinkToConvoCell (bool readonlyConversation)
		{
			var shareLinkToConvoCell = GetTextViewCell (BlrtUtil.PlatformHelper.Translate ("share_link_to_convo", "conversation_option"), readonlyConversation);
			shareLinkToConvoCell.Tapped += MultiExecutionPreventor.NewEventHandler (OnShareLinkToConvoCell, MultiExecutionPreventor.NewToken ());
			return shareLinkToConvoCell;
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		async Task DeleteLocalConversation ()
		{
			//tracking
			BlrtData.Helpers.BlrtTrack.ConvoDelete (_relation.ConversationId);

			if (HideLoading != null)
				HideLoading (this, EventArgs.Empty);

			await BlrtUtil.DeleteConversationFromLocal (_relation.LocalStorage, _relation.Conversation, this, _relation);

			_tcs.TrySetResult (new ConversationOptionResult {
				ConversationId = null,
				FilterUsers = new string [0],
				Navigated = true
			});
		}

		async Task LeaveCell ()
		{
			var shouldContinue = await DisplayAlert (
				 BlrtUtil.PlatformHelper.Translate ("leave_convo_alert_title", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("leave_convo_alert_msg", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("OK"),
					BlrtUtil.PlatformHelper.Translate ("Cancel"));

			if (!shouldContinue)
				return;
			if (ShowLoading != null)
				ShowLoading (this, EventArgs.Empty);
			string [] relationId = new string [] {
				BlrtData.BlrtEncode.EncodeId(_relation.ObjectId)
			};
			var apiCall = new BlrtAPI.APIConversationUserRelationDelete (
							  new APIConversationUserRelationDelete.Parameters () {
								  RelationIds = relationId
							  }
			);

			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {

				if (apiCall.Response.Success) {
					//tracking
					//BlrtData.Helpers.BlrtTrack.ConvoDelete (_relation.ConversationId);

					if (HideLoading != null)
						HideLoading (this, EventArgs.Empty);
					var slave = BlrtManager.App.GetCurrentSlave ();

					if (Device.OS == TargetPlatform.Android) {
						_relation.LocalStorage.DataManager.Remove (_relation);
						await BlrtManager.App.OpenMailbox (this, BlrtData.Models.Mailbox.MailboxType.Any);
					} else if (slave is IMailboxScreen) {
						var mailbox = slave as IMailboxScreen;
						_relation.LocalStorage.DataManager.Remove (_relation);
						mailbox.Reload (this);
						mailbox.CloseConversation (this);
						_tcs.TrySetResult (new ConversationOptionResult {
							ConversationId = null,
							FilterUsers = new String [0],
							Navigated = true
						});
					}
					return;
				}
				if (HideLoading != null)
					HideLoading (this, EventArgs.Empty);
				switch (apiCall.Response.Error.Code) {
				case APIConversationDeleteError.invalidConversationId:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("unknown_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("unknown_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.mutipleRelations:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.publicBlrtsExist:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("delete_public_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("delete_public_message", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				default:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				}

			} else {
				if (HideLoading != null)
					HideLoading (this, EventArgs.Empty);
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
		}

		async void DeleteCell ()
		{

			var shouldContinue = await DisplayAlert (
									 BlrtUtil.PlatformHelper.Translate ("delete_convo_alert_title", "conversation_option"),
									 BlrtUtil.PlatformHelper.Translate ("delete_convo_alert_msg", "conversation_option"),
									 BlrtUtil.PlatformHelper.Translate ("OK"),
									 BlrtUtil.PlatformHelper.Translate ("Cancel")
								 );

			if (!shouldContinue)
				return;
			if (ShowLoading != null)
				ShowLoading (this, EventArgs.Empty);

			var apiCall = new BlrtAPI.APIConversationDelete (
							  new APIConversationDelete.Parameters () {
								  ConversationId = _relation.ConversationId
							  }
			);

			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {

				if (apiCall.Response.Success) {
					await DeleteLocalConversation ();
					return;
				}
				if (HideLoading != null)
					HideLoading (this, EventArgs.Empty);
				switch (apiCall.Response.Error.Code) {
				case APIConversationDeleteError.invalidConversationId:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("unknown_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("unknown_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.mutipleRelations:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.publicBlrtsExist:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("delete_public_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("delete_public_message", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				default:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				}

			} else {
				if (HideLoading != null)
					HideLoading (this, EventArgs.Empty);
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
		}

		/// <summary>
		/// Triggered when a filter cell property changes.
		/// </summary>
		/// <param name="filterCell">Filter cell.</param>
		/// <param name="eventArgs">Event arguments.</param>
		/// <param name="firstPerson">First person.</param>
		/// <param name="peopleMoreThanOne">If set to <c>true</c> people more than one.</param>
		void OnFilterCellPropertyChanged (FilterCell filterCell,
										  PropertyChangingEventArgs eventArgs,
										  ConversationUserRelation firstPerson,
										  bool peopleMoreThanOne)
		{
			if (eventArgs.PropertyName == FilterCell.IsFilteredProperty.PropertyName) {
				if (filterCell.IsFiltered) {
					_filteredIds.Remove (filterCell.Relation.UserId);
				} else if (filterCell.Relation != null && !_filteredIds.Contains (filterCell.Relation.UserId)) {
					_filteredIds.Add (filterCell.Relation.UserId);
				}
				_clearBtn.IsVisible = _filteredIds.Count > 0;
				_removeBtn.IsVisible = _clearBtn.IsVisible ? false :
				(IsCreator && peopleMoreThanOne ? !_clearBtn.IsVisible : false);
				// count must be greater than one as creator can't remove itself from the conversation. Creator should delete the conversation instead.

				if (!peopleMoreThanOne) {
					var cell = _filterCellsList.FirstOrDefault ((ce) => ce is FilterCell && (ce as FilterCell).Relation == firstPerson) as FilterCell;
					cell.SetBinding (firstPerson, false, _filteredIds.Contains (firstPerson.UserId));
				}
			}
		}

		void ShowEditConvoNameViewOniOS (bool show)
		{
			if (show) {
				_editConvoNameView = new EditConvoNameView (_convoTitle.Text);
				_editConvoNameView.ConvoCancelBtnPressed += OnConvoCancelBtnPressed;
				_editConvoNameView.ConvoSaveBtnPressed += OnConvoSaveBtnPressed;

				AbsoluteLayout.SetLayoutFlags (_editConvoNameView,
				   AbsoluteLayoutFlags.All);

				AbsoluteLayout.SetLayoutBounds (_editConvoNameView,
				   new Rectangle (0, 0, 1, 1));

				_mainLayout.Children.Add (_editConvoNameView);
			} else {
				_mainLayout.Children.Remove (_editConvoNameView);
				_editConvoNameView.ConvoCancelBtnPressed -= OnConvoCancelBtnPressed;
				_editConvoNameView.ConvoSaveBtnPressed -= OnConvoSaveBtnPressed;
				_editConvoNameView = null;
			}
		}

		void OnConvoCancelBtnPressed (object sender, EventArgs e)
		{
			ShowEditConvoNameViewOniOS (false);
		}

		async void OnConvoSaveBtnPressed (object sender, ConvoSaveBtnEventArgs e)
		{
			var newName = e.ConvoName.Trim ();
			if (!BlrtCanvas.CanvasPageView.PlatformHelper.TryValidateConversationName (newName)) {
				return;
			}

			var status = await BlrtUtil.SaveConversationName (_conversationScreen.CurrentConversation, newName);
			switch (status) {
			case APIConversationRenameRequestStatus.Success:
				_convoTitle.Text = newName;
				ShowEditConvoNameView (false);
				Xamarin.Forms.MessagingCenter.Send<Object> (this, "ConversationNameChanged");
				break;
			case APIConversationRenameRequestStatus.Failed:
				await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
				break;
			case APIConversationRenameRequestStatus.Cancelled:
				break;
			}

		}

		void ShowEditConvoNameView (bool show)
		{
			if (show) {
				_editConvoNameView = new EditConvoNameView (_convoTitle.Text);
				_editConvoNameView.ConvoCancelBtnPressed += OnConvoCancelBtnPressed;
				_editConvoNameView.ConvoSaveBtnPressed += OnConvoSaveBtnPressed;

				AbsoluteLayout.SetLayoutFlags (_editConvoNameView,
				   AbsoluteLayoutFlags.All);

				AbsoluteLayout.SetLayoutBounds (_editConvoNameView,
				   new Rectangle (0, 0, 1, 1));

				_mainLayout.Children.Add (_editConvoNameView);
			} else {
				_mainLayout.Children.Remove (_editConvoNameView);
				_editConvoNameView.ConvoCancelBtnPressed -= OnConvoCancelBtnPressed;
				_editConvoNameView.ConvoSaveBtnPressed -= OnConvoSaveBtnPressed;
				_editConvoNameView = null;
			}
		}
#endif

		void SetMainLayout ()
		{
			_mainLayout = new AbsoluteLayout () {
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			AbsoluteLayout.SetLayoutFlags (_tableView,
										   AbsoluteLayoutFlags.All);

			AbsoluteLayout.SetLayoutBounds (_tableView,
				new Rectangle (0.0,
							   0.0, 1, 1));

			_mainLayout.Children.Add (_tableView);
		}

		ViewCell GetPageTitleCell (
#if __IOS__
			bool readonlyConversation
#endif
		)
		{
			var convo = _relation.Conversation;

			var convoThumbSrc = convo.LocalThumbnailPath;
			if (!System.IO.File.Exists (convoThumbSrc)) {
				convoThumbSrc = "logo-placeholder.png";
			}

			_convoThumb = new Image () {
				Source = convoThumbSrc,
				WidthRequest = ThumbHeight,
				HeightRequest = ThumbHeight
			};
			_convoTitle = new Label () {
				Text = convo.Name,
				VerticalOptions = LayoutOptions.Center
#if __IOS__
				, TextColor = readonlyConversation ? READONLY_CONVO_TEXT_COLOR : BlrtStyles.BlrtCharcoal
#endif
			};

			_editConvoNameBtn = new Button () {
				Image = "convo_option_editpencil.png",
				WidthRequest = 44,
				HeightRequest = 44,
				IsEnabled = IsConvoCreator,
#if __ANDROID__
				BackgroundColor = IsConvoCreator ? BlrtStyles.BlrtWhite : BlrtStyles.BlrtGray3,
				Opacity = IsConvoCreator ? 1 : 0.4,
#endif
#if __IOS__
				Opacity = readonlyConversation ? READONLY_CONVO_OPACITY : 1,
#endif
				VerticalOptions = LayoutOptions.Center,
			};
			_editConvoNameBtn.Clicked += OnEditConvoNameBtnClicked;

			return new FormsViewCell () {
				ShowSelection = false,
				Height = ThumbHeight,
				SeparatorInset = new Thickness (ThumbHeight + 15, 0, 0, 0),
				View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Auto) }
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = new Thickness (0,
											 0,
#if __ANDROID__
											 10,
#endif
#if __IOS__
											 0,
#endif
											 0),
					ColumnSpacing = 20,
					Children = {
						{ _convoThumb,0,0 },
						{ _convoTitle,1,0 },
						{ _editConvoNameBtn,2,0}
					}
				}
			};
		}

		ViewCell GetTextViewCell (string title
#if __IOS__
			, bool readonlyConversation
#endif
		)
		{
			var cell = new ViewCell () {
				View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},

					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = new Thickness (15, 0),
					Children = { {new Label () {
								Text = title,
#if __ANDROID__
								TextColor = BlrtStyles.BlrtAccentBlue,
#endif
#if __IOS__
								TextColor = readonlyConversation ? READONLY_CONVO_TEXT_COLOR : BlrtStyles.BlrtAccentBlue,
#endif
								FontFamily = BlrtStyles.CellTitleFont.FontFamily,
								FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
								FontSize = BlrtStyles.CellTitleFont.FontSize,
								VerticalOptions = LayoutOptions.Center
							},0,0
						}, {new Image () {
								Source = "settings_arrow.png",
#if __IOS__
								Opacity  = readonlyConversation? READONLY_CONVO_OPACITY : 1,
#endif
								HorizontalOptions = LayoutOptions.End
							},1,0
						}
					}
				}
			};

			if (Device.OS == TargetPlatform.Android) {
				cell.Height = rowHeight;
			}

#if __IOS__
			cell.IsEnabled = !readonlyConversation;
#endif
			return cell;
		}

		ViewCell GetPublicDescriptionCell ()
		{
			Thickness padding;
			if (BlrtUtil.PlatformHelper.IsPhone && Math.Max (BlrtUtil.PlatformHelper.GetDeviceWidth (), BlrtUtil.PlatformHelper.GetDeviceHeight ()) > 720)
				padding = new Thickness (15, -5, 15, 5);
			else
				padding = new Thickness (15, -5, 15, 5);

			return new FormsViewCell () {
				ShowSeparator = false,
				ShowSelection = false,
				Height = 23,
				View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = padding,
					Children = { {new Label () {
								Text = BlrtUtil.PlatformHelper.Translate("allow_public_description", "conversation_option"),
								FontFamily = BlrtStyles.CellContentFont.FontFamily,
								FontAttributes = BlrtStyles.CellContentFont.FontAttributes,
								FontSize = BlrtStyles.CellContentFont.FontSize,
								TextColor = BlrtStyles.BlrtGray5,
								VerticalOptions = LayoutOptions.Center
							},0,0
						},
					}
				}
			};
		}

		ViewCell GetPremiumFeatureTitleCell ()
		{
			return new FormsViewCell () {
				ShowSeparator = false,
				ShowSelection = false,
				Height = SectionHeaderHeight,
				View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					BackgroundColor = BlrtStyles.BlrtGray3,
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = new Thickness (15, 0),
					Children = { {new Label () {
								Text = BlrtUtil.PlatformHelper.Translate ("premium_feature", "conversation_option"),
								FontFamily = BlrtStyles.CellTitleFont.FontFamily,
								FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
								FontSize = BlrtStyles.CellTitleFont.FontSize,
								VerticalOptions = LayoutOptions.Center
							},0,0
						},
					}
				}
			};
		}

		Button _clearBtn, _removeBtn;
		ViewCell GetFilterTitleCell ()
		{
			_clearBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("clear_all", "conversation_option"),
				TextColor = BlrtStyles.BlrtAccentBlue,
				Font = BlrtStyles.CellTitleFont,
				IsVisible = false
			};

			_removeBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("remove_people", "conversation_option"),
				TextColor = BlrtStyles.BlrtAccentBlue,
				Font = BlrtStyles.CellTitleFont,
				IsVisible = _relation.CreatorId == ParseUser.CurrentUser.ObjectId,
				IsEnabled = _relation.CreatorId == ParseUser.CurrentUser.ObjectId
			};

			_removeBtn.Clicked += RemovePeopleClicked;

			_clearBtn.Clicked += ClearAllClicked;



			return new FormsViewCell () {
				ShowSeparator = false,
				ShowSelection = false,
				Height = SectionHeaderHeight,
				View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					BackgroundColor = BlrtStyles.BlrtGray3,
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = new Thickness (15, 0),
					ColumnSpacing = 10,
					Children = { {new Label () {
								Text = BlrtUtil.PlatformHelper.Translate ("People in conversation"),
								FontFamily = BlrtStyles.CellTitleFont.FontFamily,
								FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
								FontSize = BlrtStyles.CellTitleFont.FontSize,
								VerticalOptions = LayoutOptions.Center
							},0,0
						},
						{ _filterLoader,1,0 },
						{ _clearBtn,2,0 },
						{ _removeBtn,2,0 }
					}
				}
			};
		}

		public async Task<ExecutionResult<object>> OpenSendingPage ()
		{
			if (_relation.Conversation.IsReadonly) {
				var platforHelper = DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (),
					new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("option_disabled_title", "conversation_action"),
						BlrtUtil.PlatformHelper.Translate ("option_disabled_msg", "conversation_action"),
						BlrtUtil.PlatformHelper.Translate ("Ok"))).FireAndForget ();
			} else {
				_ignoreNavigationStack++;
				await _conversationScreen.OpenSendingPage (null, null);
				_ignoreNavigationStack--;
				RunRelationQuery ().FireAndForget ();
			}
			return new ExecutionResult<object> (new object ());
		}

#if __ANDROID__
		public async Task<ExecutionResult<object>> OpenShareLinkPage ()
		{
			_ignoreNavigationStack++;
			await OpenSharePage (null, null);

			_ignoreNavigationStack--;
			RunRelationQuery ().FireAndForget ();

			return new ExecutionResult<object> (new object ());
		}

		async Task OpenSharePage (object sendera, EventArgs ea)
		{
			await GenerateLink ("convo_other");
			var url = _url_convo_other;

			var sharePage = new BlrtApp.Views.Send.BackSharePage (url);
			var shareSubPageToken = MultiExecutionPreventor.NewToken ();
			sharePage.FbShareClicked += MultiExecutionPreventor.NewEventHandler (OnFacebookSend, shareSubPageToken);
			sharePage.MessageShareClicked += MultiExecutionPreventor.NewEventHandler (OnMessageSend, shareSubPageToken);
			sharePage.EmailShareClicked += MultiExecutionPreventor.NewEventHandler (OnEmailSend, shareSubPageToken);
			sharePage.ShareWithClicked += MultiExecutionPreventor.NewEventHandler (OnShareWithSend, shareSubPageToken);

			sharePage.DonePressed += async (se, ev) => await Navigation.PopAsync ();

			await this.Navigation.PushAsync (sharePage);
		}

		async Task OnFacebookSend (object sender, EventArgs e)
		{
			if (_relation == null || !_relation.OnParse)
				return;
			PushLoading ("url", true);
			await GenerateLink ("convo_facebook");
			var url = _url_convo_facebook;
			PopLoading ("url", true);

			BlrtApp.Droid.FBManager.TrySendFBMessenger (url, this);
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Facebook");
		}

		async Task OnEmailSend (object sendera, EventArgs ea)
		{
			if (_relation == null || !_relation.OnParse)
				return;
			PushLoading ("url", true);
			await GenerateLink ("convo_email");
			var url = _url_convo_email;
			PopLoading ("url", true);
			try {

				var title = string.Format (BlrtUtil.PlatformHelper.Translate ("email_convo_url_subject"), _relation.Conversation.Name);
				var message = string.Format (BlrtUtil.PlatformHelper.Translate ("email_convo_url_message_body_html"), url, _relation.Conversation.Name);
				//Create an Intent with an ActionSend action.
				var email = new global::Android.Content.Intent (global::Android.Content.Intent.ActionSend);

				//Add email extras to the Intent’s payload.
				email.PutExtra (global::Android.Content.Intent.ExtraSubject,
									title);

				email.PutExtra (global::Android.Content.Intent.ExtraText,
						global::Android.Text.Html.FromHtml (
						message));
				email.SetType ("text/html");

				//Start the Activity with the Intent;
				Forms.Context.StartActivity (global::Android.Content.Intent.CreateChooser (email, "Send email"));
				BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Email");
			} catch {
				var platforHelper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (),
					new SimpleAlert (
						platforHelper.Translate ("cannot_send_email_error_title"),
						platforHelper.Translate ("cannot_send_email_error_message"),
						platforHelper.Translate ("cannot_send_email_error_cancel"))).FireAndForget ();
			}
		}

		async Task OnShareWithSend (object sendera, EventArgs ea)
		{
			if (_relation == null || !_relation.OnParse)
				return;
			PushLoading ("url", true);
			await GenerateLink ("convo_other");
			var url = _url_convo_other;
			PopLoading ("url", true);

			global::Android.Content.Intent i = new global::Android.Content.Intent (global::Android.Content.Intent.ActionSend);
			i.SetType ("text/plain");
			i.PutExtra (global::Android.Content.Intent.ExtraSubject, "Blrt Conversation");
			i.PutExtra (global::Android.Content.Intent.ExtraText, url);
			Forms.Context.StartActivity (global::Android.Content.Intent.CreateChooser (i, "Blrt Conversation"));
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("Other");
		}

		async Task OnMessageSend (object sender, EventArgs e)
		{
			await SendSms ();
			BlrtData.Helpers.BlrtTrack.AddPeopleShare ("SMS");
		}


		async Task SendSms (string [] recipients = null, string url = null)
		{
			if (_relation == null || !_relation.OnParse)
				return;
			if (url == null) {
				PushLoading ("url", true);
				await GenerateLink ("convo_sms");
				url = _url_convo_sms;
				PopLoading ("url", true);
			}
			var message = string.Format (BlrtUtil.PlatformHelper.Translate ("message_blrt_url_sms_body", "conversation_screen"), _relation.Conversation.Name, url);

			try {
				string uri = "sms:";
				if (recipients != null) {
					uri = "smsto:";
					uri += string.Join (";", recipients);
				}
				var intent = new global::Android.Content.Intent (global::Android.Content.Intent.ActionView, global::Android.Net.Uri.Parse (uri));
				intent.PutExtra ("sms_body", message);

				Forms.Context.StartActivity (intent);
			} catch {
			}
		}

		public void PushLoading (string cause, bool animate)
		{
			UserDialogs.Instance.ShowLoading (null, MaskType.None);
		}

		public void PopLoading (string cause, bool animate)
		{
			UserDialogs.Instance.HideLoading ();
		}

#endif
		string _url_convo_sms, _url_convo_facebook, _url_convo_email, _url_convo_other, _url_convo_addphone = "";

		async Task GenerateLink (string urlType)
		{
			switch (urlType) {
			case "convo_sms":
				if (string.IsNullOrEmpty (_url_convo_sms) || !_url_convo_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal)) {
					_url_convo_sms = await BlrtUtil.GenerateYozioUrl (_relation.Conversation.ObjectId, urlType);
				}
				break;
			case "convo_addphone":
				if (string.IsNullOrEmpty (_url_convo_addphone) || !_url_convo_addphone.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal)) {
					_url_convo_addphone = await BlrtUtil.GenerateYozioUrl (_relation.Conversation.ObjectId, "convo_sms", new Dictionary<string, string> { { "sendToPhone", "true" } });
				}
				break;
			case "convo_facebook":
				if (string.IsNullOrEmpty (_url_convo_facebook) || !_url_convo_facebook.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal)) {
					_url_convo_facebook = await BlrtUtil.GenerateYozioUrl (_relation.Conversation.ObjectId, urlType);
				}
				break;
			case "convo_email":
				if (string.IsNullOrEmpty (_url_convo_email) || !_url_convo_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal)) {
					_url_convo_email = await BlrtUtil.GenerateYozioUrl (_relation.Conversation.ObjectId, urlType);
				}
				break;
			case "convo_other":
				if (string.IsNullOrEmpty (_url_convo_other) || !_url_convo_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal)) {
					_url_convo_other = await BlrtUtil.GenerateYozioUrl (_relation.Conversation.ObjectId, urlType);
				}
				break;
			}
		}

		public async Task<ExecutionResult<object>> OpenTagPage ()
		{
			if (_relation.Conversation.IsReadonly) {
				var platforHelper = DependencyService.Get<IPlatformHelper> ();
				BlrtManager.App.ShowAlert (Executor.System (),
									new SimpleAlert (
										BlrtUtil.PlatformHelper.Translate ("option_disabled_title", "conversation_action"),
										BlrtUtil.PlatformHelper.Translate ("option_disabled_msg", "conversation_action"),
										BlrtUtil.PlatformHelper.Translate ("Ok"))).FireAndForget ();
			} else {
				_ignoreNavigationStack++;

				await _conversationScreen.OpenTagPage (null);
				_ignoreNavigationStack--;
			}
			return new ExecutionResult<object> (new object ());
		}

		public void ClearAllClicked (object sender, EventArgs e)
		{
			_filteredIds.Clear ();
			LoadRelations ();
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();
			if (_ignoreNavigationStack <= 0 && !IsEditingConvoName) {
				IsEditingConvoName = false;
				var result = new ConversationOptionResult () {
					ConversationId = _relation.ConversationId,
					FilterUsers = _filteredIds.ToArray (),
					Navigated = true
				};
				_tcs.TrySetResult (result);
			}
		}

		bool _shouldDecreaseCounter;
		protected override void OnAppearing ()
		{
			if (_shouldDecreaseCounter) {
				_ignoreNavigationStack--;
			}
#if __ANDROID__
			BlrtApp.PlatformHelper.ConversationOptionLayoutFlag = true;
#endif
			base.OnAppearing ();
			RunRelationQuery ().FireAndForget ();

		}

		List<FilterCell> _filterCellsList;
		ConversationRelationStatus.Comparer _comparer;

		public void RemovePeopleClicked (object sender, EventArgs e)
		{
#if __IOS__
			var removePeopleBtnTextHidden = FlipAndReturnRemovePeopleBtnTextState;
#endif
#if __ANDROID__
			_removeBtn.Text = _removeBtn.Text == BlrtUtil.PlatformHelper.Translate ("remove_people", "conversation_option") ?
				BlrtUtil.PlatformHelper.Translate ("remove_people_cancel", "conversation_option") : BlrtUtil.PlatformHelper.Translate ("remove_people", "conversation_option");
#endif
			for (int i = 0; i < _filterCellsList.Count; i++) {
				_filterCellsList [i].ChangeImage (
#if __IOS__
					removePeopleBtnTextHidden
#endif
				);
				if (i > 0) {
					if (_removeBtn.Text == BlrtUtil.PlatformHelper.Translate ("remove_people", "conversation_option")) {
						_filterCellsList [i].Tapped -= RemoveSelected;
					} else {
						_filterCellsList [i].Tapped += RemoveSelected;
					}
				}
			}
		}

		private void RemoveSelected (object sender, EventArgs e)
		{
			if (sender is FilterCell) {
#if __ANDROID__
				RemovePeople (((FilterCell)sender).Relation).FireAndForget ();
#endif
#if __IOS__
				RemovePeople ((FilterCell)sender).FireAndForget ();
#endif
			}
		}


		void LoadRelations ()
		{
			if (_comparer == null) {
				_comparer = new ConversationRelationStatus.Comparer ();
			}

			var rels = GetUserRelationsForCurrentConversation ();

			if (_filterCellsList == null)
				_filterCellsList = new List<FilterCell> ();

			var tableSecWithUsers = GetTableSectionForUserRelations (ref rels);

			//reload the section
			for (int i = 0; i < tableSecWithUsers.Count; i++) {
				if (_filterSec.Count - 1 < i) {
					_filterSec.Add (tableSecWithUsers [i]);
				} else if (_filterSec [i] != tableSecWithUsers [i]) {
					_filterSec [i] = tableSecWithUsers [i];
				}
			}

			var extra = _filterSec.Count - tableSecWithUsers.Count;
			if (extra > 0) {
				for (int i = 0; i < extra; i++) {
					_filterSec.RemoveAt (_filterSec.Count - 1);
				}
			}

			_clearBtn.IsVisible = _filteredIds.Count > 0;
		}

		TableSection GetTableSectionForUserRelations (ref ConversationUserRelation [] rels)
		{
			var tempSec = new TableSection ();
			tempSec.Add (_filterTitleCell);

#if __ANDROID__
			foreach (var rel in rels) {
				var c = _filterCellsList.FirstOrDefault ((cell) => cell is FilterCell && (cell as FilterCell).Relation == rel) as FilterCell;
				if (c == null) {
					c = new FilterCell ();
					if (Device.RuntimePlatform == Device.iOS) {
						c.Height = rowHeight;
					}
					c.OnOpenProfile += OpenProfile;
					c.PropertyChanged += (object sender, System.ComponentModel.PropertyChangedEventArgs e) => {
						if (e.PropertyName == FilterCell.IsFilteredProperty.PropertyName) {
							if (!c.IsFiltered) {
								_filteredIds.Remove (c.Relation.UserId);
							} else if (c.Relation != null && !_filteredIds.Contains (c.Relation.UserId)) {
								_filteredIds.Add (c.Relation.UserId);
							}

							_removeBtn.IsVisible = _relation.CreatorId == ParseUser.CurrentUser.ObjectId ? !_clearBtn.IsVisible : false;
							_clearBtn.IsVisible = _filteredIds.Count > 0;
						}
					};
					_filterCellsList.Add (c);
				}
				c.SetBinding (rel, _filteredIds.Contains (rel.UserId));
				tempSec.Add (c);
			}
#endif

#if __IOS__

			var peopleMoreThanOne = rels.Count () > 1;
			var firstPerson = rels [0];

			foreach (var rel in rels) {

				var user = BlrtManager.DataManagers.Default.GetUser (rel.UserId);

				var filterCell = _filterCellsList.FirstOrDefault ((cell) => cell is FilterCell && (cell as FilterCell).Relation == rel) as FilterCell;
				if (filterCell == null) {
					filterCell = new FilterCell ();

					if (Device.OS != TargetPlatform.iOS) {
						filterCell.Height = rowHeight;
					}

					filterCell.OnOpenProfile += OpenProfile;

					filterCell.PropertyChanging += (sender, eventArgs) => { 
						OnFilterCellPropertyChanged (filterCell, eventArgs, firstPerson, peopleMoreThanOne);
					};

					_filterCellsList.Add (filterCell);
				}
				filterCell.SetBinding (rel, RemovePeopleButtonTextHidden, _filteredIds.Contains (rel.UserId));
				tempSec.Add (filterCell);
			}
#endif
			return tempSec;
		}

		ConversationUserRelation [] GetUserRelationsForCurrentConversation ()
		{
			return _relation.LocalStorage.DataManager
				.GetConversationUserRelationsFromConversation (_relation.ConversationId)
				.OrderBy (rel => rel, _comparer)
				.ToArray ();
		}

		async void OpenProfile (object sender, object e)
		{
			ExecutionResult<object> result = null;
			_ignoreNavigationStack++;
			if (e is BlrtUser) {
				var user = e as BlrtUser;
				if (!user.IsDeleted)
					result = await BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), e as BlrtUser);
				else {
					_ignoreNavigationStack--;
				}
			} else {
				var contact = e as BlrtData.Contacts.BlrtContact;
				if (contact.InfoList.Count < 1)
					return;
				result = await BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), contact);
			}
			if (result != null) {
				var nav = result.Result as BlrtData.Views.Profile.IProfileNav;
				nav.OnDismissed += (a, v) => {
					_ignoreNavigationStack--;
				};
			}
		}

		async Task RunRelationQuery ()
		{
			try {
				Device.BeginInvokeOnMainThread (() => {
					_filterLoader.IsVisible = true;
					_filterLoader.IsRunning = true;
					_filterLoader.IsEnabled = true;
				});
				var query = BlrtData.Query.BlrtConversationMetaQuery.RunQuery (_relation);
				await query.WaitAsync ();
			} catch {
			}

			Device.BeginInvokeOnMainThread (() => {
				LoadRelations ();
				_filterLoader.IsVisible = false;
			});
		}

#if __ANDROID__
		/// <summary>
		/// Deletes the conversation from cloud and local
		/// </summary>
		/// <returns>The conversation.</returns>
		async Task DeleteConversation ()
		{
			var apiCall = new APIConversationDelete (
				  new APIConversationDelete.Parameters () {
					  ConversationId = _relation.ConversationId
				  }
			);

			var isConvoRemoved = await Remove (DisplayDeleteConvoAlert, apiCall, RemoveConversationFromLocal);

			if (isConvoRemoved) {
				//tracking
				BlrtData.Helpers.BlrtTrack.ConvoDelete (_relation.ConversationId);
			}
		}

		/// <summary>
		/// Removes the people from conversation
		/// </summary>
		/// <param name="relation">User relation</param>
		async Task RemovePeople (ConversationUserRelation relation)
		{
			string [] relationIds = { BlrtEncode.EncodeId (relation.ObjectId) };

			var apiCall = new APIConversationUserRelationDelete (
				  new APIConversationUserRelationDelete.Parameters {
					  RelationIds = relationIds
				  }
			);

			await Remove (DisplayRemovePeopleAlert, apiCall, async delegate {
				await RemoveConversationUserRelationFromLocal (relation);
				LoadRelations ();
				_handler = new ConversationHandler (_relation.ConversationId);
				await _handler.Refresh ();
			});
		}

		/// <summary>
		/// Removes the conversation from local.
		/// </summary>
		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		async Task RemoveConversationFromLocal ()
		{
			var slave = BlrtManager.App.GetCurrentSlave ();

			var isMailBoxScreen = slave is IMailboxScreen;
			var isAndroidPlatform = Device.RuntimePlatform == Device.Android;

			if (isMailBoxScreen || isAndroidPlatform) {
				await RemoveConversationUserRelationFromLocal (_relation);
			}

			if (isMailBoxScreen) {
				var mailbox = slave as IMailboxScreen;
				mailbox.Reload (this);
				mailbox.CloseConversation (this);

				_tcs.TrySetResult (new ConversationOptionResult {
					ConversationId = null,
					FilterUsers = new string [0],
					Navigated = true
				});
			}

			if (isAndroidPlatform) {
				BlrtManager.App.OpenMailbox (this, Models.Mailbox.MailboxType.Any).FireAndForget ();
			}
		}

		/// <summary>
		/// Leaves the conversation. Deletes conversation user relation
		/// </summary>
		async Task LeaveConversation ()
		{
			string [] relationId = { _relation.ObjectId };

			var apiCall = new APIConversationUserRelationDelete (
				  new APIConversationUserRelationDelete.Parameters {
					  RelationIds = relationId
				  }
			);

			await Remove (DisplayLeaveConvoAlert, apiCall, RemoveConversationFromLocal);
		}

#endif
#if __IOS__
		async Task RemovePeople (FilterCell cell)
		{
			var shouldContinue = await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("remove_people_convo_alert_title", "conversation_option"),
				BlrtUtil.PlatformHelper.Translate ("remove_people_convo_alert_msg", "conversation_option"),
				BlrtUtil.PlatformHelper.Translate ("OK"),
				BlrtUtil.PlatformHelper.Translate ("Cancel")
			);

			if (!shouldContinue) {
				return;
			}

			cell.RemoveIndicatorHidden = false;

			var relation = cell.Relation;
			string [] relationId = new string [] {
				BlrtData.BlrtEncode.EncodeId(relation.ObjectId)
			};
			var apiCall = new BlrtAPI.APIConversationUserRelationDelete (
				  new APIConversationUserRelationDelete.Parameters () {
					  RelationIds = relationId
				  }
			);
			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {

				cell.RemoveIndicatorHidden = true;

				var result = apiCall.Response;
				if (result.Success) {
					//tracking
					//BlrtData.Helpers.BlrtTrack.ConvoDelete (_relation.ConversationId);

					BlrtUtil.UpdateConversationBondCount (_conversationScreen.CurrentConversation, -1);
					relation.LocalStorage.DataManager.Remove (relation);
					LoadRelations ();
					_handler = new ConversationHandler (_relation.ConversationId);
					await _handler.LoadContent (relation.Conversation.GeneralCount, 1);
					MessagingCenter.Send<object> (this, string.Format ("{0}.PersonRemoved", _relation.Conversation.LocalGuid));
					return;
				}

				switch (apiCall.Response.Error.Code) {
				case APIConversationDeleteError.invalidConversationId:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("unknown_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("unknown_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.mutipleRelations:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				case APIConversationDeleteError.publicBlrtsExist:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("delete_public_title", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("delete_public_message", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				default:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("failed_to_remove", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
						BlrtUtil.PlatformHelper.Translate ("OK"));
					break;
				}
			} else {
				cell.RemoveIndicatorHidden = true;
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("failed_to_remove", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
		}
#endif

		/// <summary>
		/// Removes the provided entity from both cloud and local. Shows confirmation alert and, upon confirmation, 
		/// removes the provided entity from both cloud and local.
		/// </summary>
		/// <returns>True if entity is removed; otherwise False</returns>
		/// <param name="removalAlert">Method to show relevant alert</param>
		/// <param name="removeFromCloud">Relevant cloud function to remove entity</param>
		/// <param name="removeFromCloud">Action to perform after removing entity from cloud</param>
		async Task<bool> Remove (Func<Task<bool>> removalAlert,
								 APIBase<APIConversationDeleteResponse> removeFromCloud,
								 Func<Task> postRemovalAction)
		{
			var isConfirmed = await removalAlert ();

			if (!isConfirmed) {
				return false;
			}

			LoadingIndicatorHidden = false;

			var callSucces = await removeFromCloud.Run (BlrtUtil.CreateTokenWithTimeout (15000));
			var callResponseSuccess = removeFromCloud.Response.Success;
			var responseError = removeFromCloud.Response.Error;
			var callResponseErrorCode = responseError == null ? -1 : responseError.Code;

			return await HandleCloudAPICallResponse (callSucces, callResponseSuccess, callResponseErrorCode, postRemovalAction);
		}

		/// <summary>
		/// Removes the conversation user relation from local.
		/// </summary>
		async Task RemoveConversationUserRelationFromLocal (ConversationUserRelation relation)
		{
			relation.LocalStorage.DataManager.Remove (relation);
			await relation.LocalStorage.DataManager.SaveLocal ();
		}

		/// <summary>
		/// Handles a switch change. Given the params, it handles the appropriate switch.
		/// </summary>
		/// <param name="muteToggled">If set to <c>true</c> switch is toggled</param>
		/// <param name="enumType">type of switch toggled</param>
		void MuteSwitched (bool muteToggled, APIConversationUserRelationMute.Parameters.MuteEnum enumType)
		{
			var currentMuteFlag = _relation.MuteFlag;
			var isMute = IsMute (enumType);
			var shouldMute = muteToggled && !isMute;
			var shouldUnMute = !muteToggled && isMute;

			if (shouldMute || shouldUnMute) {
				currentMuteFlag ^= (int)enumType;
			}

			if (shouldUnMute) {
				if (currentMuteFlag % 2 == 1) {
					currentMuteFlag -= 1;
				}
			}

			_relation.MuteFlag = currentMuteFlag;
			SaveRelation ();
		}

		/// <summary>
		/// Sets the relation dirty and saves it. 
		/// </summary>
		void SaveRelation ()
		{
			_relation.Dirty = true;
			_relation.LocalStorage.DataManager.Save ();
		}

		/// <summary>
		/// Handles the mute-email switch change
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void MuteEmailSwitched (object sender, EventArgs e)
		{
			MuteSwitched (_muteEmail.On, APIConversationUserRelationMute.Parameters.MuteEnum.Email);
		}

		/// <summary>
		/// Handles the mute-push switch change
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void MutePushSwitched (object sender, EventArgs e)
		{
			MuteSwitched (_mutePush.On, APIConversationUserRelationMute.Parameters.MuteEnum.Push);
		}

		void AllowAddSwitched (object sender, EventArgs e)
		{
			_relation.Conversation.DisableOthersToAdd = !_allowAdd.On;
			_relation.Conversation.Dirty = true;
			_relation.LocalStorage.DataManager.Save ();
		}

		async void PremiumAndCreatorCellTapped (object sender, EventArgs e)
		{
			//check permission
			if (!IsConvoCreator) {
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("creator_only_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("creator_only_message", "conversation_option"),
					null,
					BlrtUtil.PlatformHelper.Translate ("creator_only_cancel", "conversation_option")
				).FireAndForget ();
			} else if (!IsPremiumOrTrialUser) {
				BlrtPermissions.PermissionEnum permission;

				permission = (sender == _allowAdd) ?
					BlrtPermissions.PermissionEnum.CanChangeAllowAdd :
					BlrtPermissions.PermissionEnum.CanChangeAllowPublic;

				BlrtUpgradeHelper.ShowUpgradeDialog (permission).FireAndForget ();
			}
		}

		int _ignoreNextAllowPublicSwitch = 0;

		async void AllowPublicBlrtSwitched (object sender, ToggledEventArgs e)
		{
			if (_ignoreNextAllowPublicSwitch > 0) {
				_ignoreNextAllowPublicSwitch--;
				return;
			}

#if __ANDROID__
			LoadingIndicatorHidden = true;

			var apiCall = new BlrtAPI.APIConversationPublic (
				new APIConversationPublic.Parameters {
					ConversationId = _relation.ConversationId,
					AllowPublicBlrt = e.Value
				}
			);

			var callSucces = await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000));
			var callResponseSuccess = apiCall.Response.Success;
			var callResponseErrorCode = apiCall.Response.Error.Code;

			await HandleCloudAPICallResponse (callSucces, callResponseSuccess, callResponseErrorCode, async delegate {
				try {
					var query = Query.BlrtConversationQuery.RunQuery (
						_relation.ConversationId, -1, 0
					);
					await query.WaitAsync ();
				} catch { }
			});
#endif
#if __IOS__
			if(ShowLoading!=null)
				ShowLoading (this, EventArgs.Empty);
			var apiCall = new BlrtAPI.APIConversationPublic (
				new APIConversationPublic.Parameters () {
					ConversationId = _relation.ConversationId,
					AllowPublicBlrt = _allowPublicBlrt.On
				}
			);

			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout(15000))) {

				if (apiCall.Response.Success) {
//					fetch conversation
					try {
						var query = BlrtData.Query.BlrtConversationQuery.RunQuery(
							_relation.ConversationId, -1, 0
						);
						await query.WaitAsync();
					} catch {}

					if (HideLoading!=null)
						HideLoading (this, EventArgs.Empty);
					return;
				}
				if(HideLoading!=null)
					HideLoading (this, EventArgs.Empty);
				switch (apiCall.Response.Error.Code) {
					case APIConversationPublicError.publicChildExists:
						await DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("publicChildExists_title", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("publicChildExists_message", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate ("publicChildExists_accept", "conversation_option"));
						break;
					default:
						await DisplayAlert (
							BlrtUtil.PlatformHelper.Translate("internet_error_title", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate("internet_error_msg", "conversation_option"), 
							BlrtUtil.PlatformHelper.Translate("OK"));
						break;
				}

			} else {
				if(HideLoading!=null)
					HideLoading (this, EventArgs.Empty);
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate("OK"));
			}
#endif
			_ignoreNextAllowPublicSwitch++;
			_allowPublicBlrt.On = !_allowPublicBlrt.On;
		}

		/// <summary>
		/// Handles the cloud API call response.
		/// </summary>
		/// <returns>The cloud APIC all response.</returns>
		/// <param name="cloudApiCallSuccess">If set to <c>true</c> cloud API call success.</param>
		/// <param name="cloudApiCallResponseSuccess">If set to <c>true</c> cloud API call response success.</param>
		/// <param name="errorCode">api call response error code</param>
		/// <param name="postCloudApiCallAction">Action to perform after receiving response from API call</param>
		async Task<bool> HandleCloudAPICallResponse (bool cloudApiCallSuccess, bool cloudApiCallResponseSuccess, int errorCode, Func<Task> postCloudApiCallAction)
		{
			if (!cloudApiCallSuccess) {
				LoadingIndicatorHidden = true;
				await DisplayInternetErrorAlert ();
				return false;
			}

			if (cloudApiCallResponseSuccess) {
				await postCloudApiCallAction ();
				LoadingIndicatorHidden = true;
				return true;
			}

			LoadingIndicatorHidden = true;

			switch (errorCode) {
			case APIConversationDeleteError.invalidConversationId:
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("unknown_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("unknown_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
				break;
			case APIConversationDeleteError.mutipleRelations:
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("mutiple_relation_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
				break;
			case APIConversationDeleteError.publicBlrtsExist:
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("delete_public_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("delete_public_message", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
				break;
			case APIConversationPublicError.publicChildExists:
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("publicChildExists_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("publicChildExists_message", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("publicChildExists_accept", "conversation_option"));
				break;
			default:
				await DisplayInternetErrorAlert ();
				break;
			}
			return false;
		}

		/// <summary>
		/// Displays the remove people alert.
		/// </summary>
		/// <returns>True if user confirmed to remove selected people; otherwise False</returns>
		Task<bool> DisplayRemovePeopleAlert ()
		{
			return DisplayAlert (
				 BlrtUtil.PlatformHelper.Translate ("remove_people_convo_alert_title", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("remove_people_convo_alert_msg", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("OK"),
				 BlrtUtil.PlatformHelper.Translate ("Cancel")
			 );
		}
		/// <summary>
		/// Displays the leave cell alert.
		/// </summary>
		/// <returns>True if user confirmed to leave conversation; otherwise False</returns>
		Task<bool> DisplayLeaveConvoAlert ()
		{
			return DisplayAlert (
				 BlrtUtil.PlatformHelper.Translate ("leave_convo_alert_title", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("leave_convo_alert_msg", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("OK"),
				 BlrtUtil.PlatformHelper.Translate ("Cancel")
			);
		}

		/// <summary>
		/// Displays alrt to confirm convo deletion
		/// </summary>
		/// <value>True if user confirmed conversation deletion; otherwise False</value>
		Task<bool> DisplayDeleteConvoAlert ()
		{
			return DisplayAlert (
				 BlrtUtil.PlatformHelper.Translate ("delete_convo_alert_title", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("delete_convo_alert_msg", "conversation_option"),
				 BlrtUtil.PlatformHelper.Translate ("OK"),
				 BlrtUtil.PlatformHelper.Translate ("Cancel")
			 );
		}

		/// <summary>
		/// Displays the internet error alert.
		/// </summary>
		Task DisplayInternetErrorAlert ()
		{
			return DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
				BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
				BlrtUtil.PlatformHelper.Translate ("OK"));
		}
	}
}

