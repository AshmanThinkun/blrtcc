using System;
using Xamarin.Forms;

namespace BlrtData.Views.Conversation.ConversationOptions
{
	public class FormsViewCell: ViewCell
	{
		public bool ShowSeparator{ get; set;}
		public bool ShowSelection{ get; set;}
		public Thickness SeparatorInset{ get; set;}

		public FormsViewCell ():base()
		{
			ShowSeparator = true;
			ShowSelection = true;
		}
	}
}

