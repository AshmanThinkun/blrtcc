using System;
using Xamarin.Forms;

namespace BlrtData.Views.Conversation.ConversationOptions
{
	public class FormsSwitchCell: SwitchCell
	{
		public bool ShowSeparator { get; set; } = true;
		public float TextSize{ get; set;}
		public bool IsSwitchEnabled { get; set; } = true;

		public FormsSwitchCell ():base()
		{
		}
	}
}

