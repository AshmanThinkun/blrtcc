using System;
using Xamarin.Forms;
using BlrtData.Models.ConversationScreen;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Conversation.ConversationOptions
{
	public class FilterCell : FormsViewCell
	{


		public static readonly BindableProperty IsFilteredProperty =
			BindableProperty.Create<FilterCell, bool> (cell => cell.IsFiltered, false);
		public bool IsFiltered { get { return (bool)GetValue (IsFilteredProperty); } set { SetValue (IsFilteredProperty, value); } }

		public ConversationUserRelation Relation { get; private set; }

#if __IOS__
		ActivityIndicator _removeIndicator; // to indicate person is being removed

		/// <summary>
		/// Sets a value indicating whether _removeIndicator is hidden
		/// </summary>
		/// <value><c>true</c> if remove indicator hidden; otherwise, <c>false</c>.</value>
		public bool RemoveIndicatorHidden {
			set {
				RemovingUser = (!value);
				_removeIndicator.IsVisible = _removeIndicator.IsRunning = !value;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="T:BlrtData.Views.Conversation.ConversationOptions.FilterCell"/> is creator.
		/// </summary>
		/// <value><c>true</c> if is creator; otherwise, <c>false</c>.</value>
		bool IsCreator {
			get {
				return ConversationRelationStatus.GetTypeOfRelation (Relation) == ConversationRelationStatus.RoleEnum.Creator;
			}
		}

		/// <summary>
		/// Sets a value indicating whether user is being removed
		/// </summary>
		/// <value><c>true</c> if removing user; otherwise, <c>false</c>.</value>
		bool RemovingUser {
			set {
				_typeLbl.TextColor = BlrtStyles.BlrtGray6;

				if (value) {
					_typeLbl.Text = "Removing";
					_iconViewRemove.IsEnabled = _iconViewRemove.IsVisible = false;
					_iconViewRemove.Opacity = 0;
					return;
				}

				_iconViewRemove.IsEnabled = _iconViewRemove.IsVisible = true;
				_iconViewRemove.Opacity = 1;
				if (!IsFiltered) {
					try {
						var item = this.BindingContext as BlrtUtil.ConversationExistingContact;
						if (item != null)
							_typeLbl.Text = BlrtUtil.GetStringForConversationRelationType (item.RelationType);
					} catch { }
				} else {
					_typeLbl.Text = BlrtUtil.PlatformHelper.Translate ("filter_applied", "conversation_option");
					_typeLbl.TextColor = BlrtStyles.BlrtGreen;
				}
			}
		}

		Label _accountRemovedLabel;
#endif



		public static int CellHeight = 50;
		const int IconHeight = 30;
		const int IconWidth = 34;

		ContentView _iconView, _iconViewRemove;

#if __ANDROID__
		ContentView _nameView;
		Label _accountStatus;
#endif
		Label _nameLbl, _typeLbl, _initialLbl;
		RoundedImageView _roundBox;

		public event EventHandler<object> OnOpenProfile;

		public FilterCell () : base ()
		{
			ShowSelection = false;
			_iconView = new ContentView () {
				HeightRequest = IconHeight,
				WidthRequest = IconWidth,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,

				Content = new Image () {
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Source = "filter_icon.png"
				}
			};
			_iconViewRemove = new ContentView () {
				HeightRequest = IconHeight,
				WidthRequest = IconWidth,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
#if __ANDROID__
				BackgroundColor = BlrtStyles.BlrtBlueGray,
#endif
#if __IOS__
				BackgroundColor = Color.Transparent,
#endif
				Opacity = 0,
				Content = new Image () {
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
#if __ANDROID__
					Source = "cross.png"
#endif
#if __IOS__
					Source = "removeperson_cross.png"
#endif
				}
			};
			_initialLbl = new Label () {
				YAlign = TextAlignment.Center,
				XAlign = TextAlignment.Center,
				TextColor = BlrtStyles.BlrtWhite,
				FontSize = BlrtStyles.FontSize14.FontSize
			};

			_roundBox = new RoundedImageView () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				CornerRadius = (CellHeight - 10) * 0.5f,
				HeightRequest = CellHeight - 10,
				WidthRequest = CellHeight - 10,
				Source = "cross.png",
				Aspect = Aspect.AspectFill
			};

			_roundBox.GestureRecognizers.Add (new TapGestureRecognizer () {
				Command = new Command ((o) => {
					OpenProfile ();
				})
			});

#if __IOS__
			_removeIndicator = new ActivityIndicator {
				IsVisible = false,
				IsRunning = false,
				Color = BlrtStyles.BlrtGray6
			};
#endif

			_initialLbl.GestureRecognizers.Add (new TapGestureRecognizer () {
				Command = new Command ((o) => {
					OpenProfile ();
				})
			});

			_nameLbl = new Label () {
				LineBreakMode = LineBreakMode.TailTruncation,
				FontFamily = BlrtStyles.CellTitleFont.FontFamily,
				FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
			};

#if __ANDROID__
			_accountStatus = new Label {
				LineBreakMode = LineBreakMode.TailTruncation,
				FontFamily = BlrtStyles.CellDetailFont.FontFamily,
				FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				Text = "Account deleted",
				TextColor = BlrtStyles.BlrtAccentRed,
				IsVisible = false
			};

			_nameView = new ContentView {
				VerticalOptions = LayoutOptions.Center,
				Content = new StackLayout {
					Orientation = StackOrientation.Vertical,
					Spacing = -3,
					Children = {
						_nameLbl,
						_accountStatus
					}
				}
			};
#endif
			_typeLbl = new Label () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.End,
				FontFamily = BlrtStyles.CellTitleFont.FontFamily,
				FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
			};

			this.Height = CellHeight;
			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
#if __IOS__
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)}
#endif
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
				},
				HeightRequest = CellHeight,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				ColumnSpacing = 5,
				Padding = new Thickness (15, 0),
				Children = {
					{_iconView, 0,0},
					{_iconViewRemove, 0,0},
					{_roundBox, 1,0},
					{_initialLbl, 1,0},
#if __IOS__
					/*
    				 * _nameLbl & _accountRemovedLabel share the height of the cell equally. One label aligns its text
    				 * to its top edge while the other aligns it to its bottom edge. This way both the labels are displayed
    				 * centered vertically.
					*/
					{_nameLbl = new Label (){
							VerticalOptions = LayoutOptions.StartAndExpand, // align to the top of the cell
							HeightRequest = CellHeight * 0.5, // consume half of the cell's height
							YAlign = TextAlignment.End, // align text to the bottom of the label
							LineBreakMode = LineBreakMode.TailTruncation,
							FontFamily = BlrtStyles.CellTitleFont.FontFamily,
							FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
							FontSize = BlrtStyles.CellTitleFont.FontSize
						},2,0},
					{_accountRemovedLabel = new Label (){
							VerticalOptions = LayoutOptions.EndAndExpand, // align to the bottom of the cell
							HeightRequest = CellHeight * 0.5, // consume half of the cell's height
							YAlign = TextAlignment.Start, // align text to the top of the label
							Text = BlrtUtil.PlatformHelper.Translate ("deleted_user_string", "profile"),
							TextColor = BlrtStyles.BlrtAccentRed,
							LineBreakMode = LineBreakMode.TailTruncation,
							FontFamily = BlrtStyles.CellDetailFont.FontFamily,
							FontAttributes = FontAttributes.Italic,
							FontSize = BlrtStyles.CellTitleFont.FontSize,
						},2,0},
					{_removeIndicator, 3, 0},
					{_typeLbl = new Label (){
							VerticalOptions = LayoutOptions.Center,
							HorizontalOptions = LayoutOptions.End,
							FontFamily = BlrtStyles.CellTitleFont.FontFamily,
							FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
							FontSize = BlrtStyles.CellTitleFont.FontSize,
						},4,0}, 
#endif
#if __ANDROID__
					{_nameView ,2,0},
					{_typeLbl ,3,0},
#endif
				}
			};

			_roundBox.SetBinding (RoundedImageView.BackgroundColorProperty, "InitialBackgroundColor");
			_roundBox.SetBinding (RoundedImageView.SourceProperty, "AvatarPath");
			_initialLbl.SetBinding (Label.TextProperty, "Initials");
			_typeLbl.SetBinding (Label.TextProperty,
#if __ANDROID__
								"RelationType"
#endif
#if __IOS__
			                    "RelationTypeString"
#endif
			);
			_nameLbl.SetBinding (Label.TextProperty, "DisplayName");

			OnPropertyChanged (IsFilteredProperty.PropertyName);
		}

		void OpenProfile ()
		{
			var item = this.BindingContext as
#if __ANDROID__
						   BlrtUtil.ConversationsOptionsUserItem;
#endif
#if __IOS__
			               BlrtUtil.ConversationExistingContact;
#endif
			if (item.Model != null) {
				OnOpenProfile?.Invoke (this, item.Model);
			}
		}

#if __ANDROID__
		public void ChangeImage ()
		{
			if (_iconViewRemove.Opacity == 0) {
				if (ConversationRelationStatus.GetTypeOfRelation (Relation) == ConversationRelationStatus.RoleEnum.Creator) {
					_iconView.Opacity = (_iconView.Opacity + 1) % 2;
				} else {
					_iconView.Opacity = 0;
					_iconViewRemove.Opacity = 1;
				}
			} else {
				_iconViewRemove.Opacity = 0;
				_iconView.Opacity = string.IsNullOrEmpty (Relation.UserId) ? 0 : 1;
			}
		}
#endif
#if __IOS__
		/// <summary>
		/// Changes the image.
		/// </summary>
		/// /// <param name="removeButtonHidden">If set to <c>true</c> user should be able to remove people. 
		/// This remove button appears only to indicate user can remove people. Once pressed, 
		/// this button is removed and cross is displayed next to each person in a convo</param>
		public void ChangeImage (bool removeButtonHidden)
		{
			/*if (string.IsNullOrEmpty (Relation.UserId)) 
			{
				_iconView.Opacity = _iconViewRemove.Opacity = 0;
				return;
			} */

			var isCreator = IsCreator;
			if (removeButtonHidden) 
			{
				_iconView.Opacity = 0;
				_iconViewRemove.Opacity = isCreator ? 0 : 1;

			} 
			else 
			{
				_iconViewRemove.Opacity = 0;
				_iconView.Opacity = 1;
			}
		}
#endif
		protected override void OnTapped ()
		{
			base.OnTapped ();
			if (_iconViewRemove.Opacity == 0) {
				if (ConversationRelationStatus.GetTypeOfRelation (Relation) != ConversationRelationStatus.RoleEnum.NotSignedUp) {
					IsFiltered = !IsFiltered;
				}
			}
		}

		public void SetBinding (ConversationUserRelation relation,
		                        #if __IOS__
								bool removeButtonHidden,
							    #endif
		                        bool filtered = false)
		{
			IsFiltered = filtered;
			Relation = relation;

			if (relation == null)
				return;

#if __ANDROID__
			var displayItem = BlrtUtil.CreateConversationsOptionsUserItem (relation);

			if (!string.IsNullOrWhiteSpace (relation.UserId)) {
				var user = BlrtManager.DataManagers.Default.GetUser (relation.UserId);
				displayItem.Model = user;
				if (user.IsDeleted) {
					//disable avatar and initial display of deleted user.
					//https://thinkun.atlassian.net/wiki/spaces/BLRTDOC/pages/245301280/Blrt+deleted+user+logic+and+architecture
					_accountStatus.IsVisible = true;
					displayItem.Initials = "";
					displayItem.AvatarPath = "profile_deleted_user";
				} else {
					if (string.IsNullOrWhiteSpace (user.LocalAvatarPath)) {
						displayItem.Initials = BlrtUtil.GetInitials (displayItem.DisplayName);
					} else {
						displayItem.Initials = "";
						displayItem.AvatarPath = user.LocalAvatarPath;
					}
				}
			} else {
				var c = new BlrtData.Contacts.BlrtContact ();
				c.Add (new BlrtData.Contacts.BlrtContactInfo (BlrtData.Contacts.BlrtContactInfoType.Email, relation.GetUserEmail ()));
				displayItem.Model = BlrtData.Contacts.BlrtContactManager.SharedInstanced.GetMatching (c);
				if (displayItem.Model == null) {
					displayItem.Model = c;
				}
			}

			this.BindingContext = displayItem;

			_iconView.Opacity = string.IsNullOrEmpty (Relation.UserId) ? 0 : 1;
#endif
#if __IOS__
			var result = BlrtUtil.CreateConversationExistingContactFromRelation (relation);
			var displayItem = result.Item1;

			this.BindingContext = displayItem;
			_accountRemovedLabel.IsVisible = displayItem.AccountRemoved;

			// _nameLbl is centered vertically if user's account is deleted; it is aligned to the top edge otherwise
			_nameLbl.VerticalOptions = _accountRemovedLabel.IsVisible ? LayoutOptions.Start : LayoutOptions.Center;
           	ChangeImage (removeButtonHidden);
#endif
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			if (_iconViewRemove.Opacity == 0 || (_iconView.Opacity == 1 && ConversationRelationStatus.GetTypeOfRelation (Relation) != ConversationRelationStatus.RoleEnum.Creator)) {
				base.OnPropertyChanged (propertyName);

				if (propertyName == IsFilteredProperty.PropertyName) {
					_iconView.BackgroundColor = IsFiltered ? BlrtStyles.BlrtGreen : BlrtStyles.BlrtGray3;
#if __IOS__
					if (!IsFiltered) {
						_typeLbl.TextColor = BlrtStyles.BlrtGray5;
						try {
							var item = this.BindingContext as BlrtUtil.ConversationsOptionsUserItem;
							if (item != null)
								_typeLbl.Text = item.RelationType;
						} catch { }
					} else {
						_typeLbl.Text = BlrtUtil.PlatformHelper.Translate ("filter_applied", "conversation_option");
						_typeLbl.TextColor = BlrtStyles.BlrtGreen;
					}
#endif
				}
			}
		}
	}
}

