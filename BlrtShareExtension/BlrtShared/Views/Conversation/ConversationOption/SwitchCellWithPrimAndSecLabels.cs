﻿#if __ANDROID__
using System;
using Xamarin.Forms;

namespace BlrtData.Views.Conversation.ConversationOptions
{
	/// <summary>
	/// Switch cell with primary and secondary labels. Use this switch cell if primary and secondary text is required.
	/// </summary>
	public class SwitchCellWithPrimAndSecLabels : ViewCell
	{
		// vertical spacing between primary and secondary label
		static int ROW_MARGIN = 3;

		// static width for switch
		static int SWITCH_WIDTH = 43;

		Switch _switch;
		Label _primaryLabel;
		Label _secondaryLabel;

		Thickness _layoutMargin;

		/// <summary>
		/// Occurs when switch is toggled
		/// </summary>
		public event EventHandler<ToggledEventArgs> OnChanged {
			add {
				_switch.Toggled += value;
			}
			remove {
				_switch.Toggled -= value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether switch is on.
		/// </summary>
		/// <value><c>true</c> if on; otherwise, <c>false</c>.</value>
		public bool On {
			get {
				return _switch.IsToggled;
			}
			set {
				_switch.IsToggled = value;
			}
		}

		/// <summary>
		/// Constructs and returns the view for this cell.
		/// </summary>
		View GetView {
			get {
				var relativeLayout = new RelativeLayout ();

				// set layout margin
				relativeLayout.Margin = _layoutMargin;

				relativeLayout.Children.Add (_primaryLabel,
							 Constraint.Constant (0), // "0" with respect to relative layout
							 Constraint.Constant (0)); // "0" with respect to relative layout

				relativeLayout.Children.Add (_secondaryLabel,
							 Constraint.Constant (0), // "0" with respect to relative layout
							 Constraint.RelativeToView (_primaryLabel, (parent, sibling) => {
								 return sibling.Height + ROW_MARGIN;
							 }));

				relativeLayout.Children.Add (_switch,
							 Constraint.RelativeToParent ((parent) => {
								 return parent.Width - SWITCH_WIDTH;
							 }),
							 Constraint.Constant (0)); // "0" with respect to relative layout

				return relativeLayout;
			}
		}

		public SwitchCellWithPrimAndSecLabels (bool isEnabled, bool isToggled, string primaryText, string secondaryText)
		{
			_switch = new Switch {
				IsEnabled = isEnabled,
				IsToggled = isToggled
			};

			_primaryLabel = new Label {
				Text = primaryText
			};

			_secondaryLabel = new Label {
				Text = secondaryText,
				FontFamily = BlrtStyles.CellContentFont.FontFamily,
				FontAttributes = BlrtStyles.CellContentFont.FontAttributes | FontAttributes.Italic,
				FontSize = BlrtStyles.FontSize13.FontSize,
				TextColor = BlrtStyles.BlrtGray4,
				VerticalOptions = LayoutOptions.Center
			};


			if (BlrtUtil.PlatformHelper.IsPhone && Math.Max (BlrtUtil.PlatformHelper.GetDeviceWidth (),
															 BlrtUtil.PlatformHelper.GetDeviceHeight ()) > 720) {
				_layoutMargin = new Thickness (20, 12, 15, 12);
			} else {
				_layoutMargin = new Thickness (15, 12, 15, 12);
			}

			View = GetView;
		}
	}
}
#endif