﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Conversation
{
	public class EmbedPreviewPage: BlrtContentPage
	{
		string _blrtId;
		bool _isPublic;
		string _embedUrl;

		public string EmailShareLink{ get; set;}
		public string EmailImpressionLink{ get; set;}

		double _pageWidth = 320d;

		ScrollView _scrollView;
		Label _browserLbl, _playerSizeLbl;
		Button _copyPlayer, _emailPlayer, _copySnippet, _emailSnippet;
		Image _chromeImg, _nonChromeImg, _snippetImg;
		BlrtImageButton _prePage;
		BlrtImageButton _nextPage;
		Slider _slider;
		Switch _switch;

		ExtendedScrollView _carouselView;
		StackLayout _pageContainer;
		BlrtPageControl _pageControl;
		List<View> _pages;

		public event EventHandler LearnMorePressed;

		public event EventHandler<string> CopyPlayerPressed, EmailPlayerPressed, CopySnippetPressed, EmailSnippetPressed;

		public enum SnippetTheme 
		{
			Light,
			Dark
		};


		public SnippetTheme Theme{
			get{
				return _switch.IsToggled ? SnippetTheme.Dark : SnippetTheme.Light;
			}
		}

		public override string ViewName {
			get {
				return "Conversation_EmbedPreview";
			}
		}

		public override void TrackPageView ()
		{
			var dic = new Dictionary<string,object>();
			dic.Add("blrtId", _blrtId);
			dic.Add("type", _isPublic? "public": "private");
			if(BlrtData.Helpers.BlrtTrack.LastConvoDic!=null){
				foreach(var kv in BlrtData.Helpers.BlrtTrack.LastConvoDic){
					dic.Add(kv.Key,kv.Value);
				}
			}
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, dic);
		}

		public EmbedPreviewPage (string blrtId, bool isPublic):base()
		{
			_blrtId = blrtId;
			_isPublic = isPublic;
			_embedUrl = BlrtEncode.EncodePublicBlrtUrl (_blrtId, false);

			Title = "Select embed code";

			//layouts
			var playerSection = InitPlayerSection ();

			var snippetSection = InitSnippetSection ();

			_scrollView = new ScrollView () {
				Padding = 0,
				Content = new StackLayout(){
					Spacing = 0,
					Children = {
						playerSection,snippetSection
					}
				}
			};

			this.Content = _scrollView;

			_slider_ValueChanged (this, new ValueChangedEventArgs (0, 1));

			//button press event
			var multiExeToken = MultiExecutionPreventor.NewToken ();

			_copyPlayer.Clicked += MultiExecutionPreventor.NewEventHandler (_copyPlayer_Clicked, multiExeToken);
			_emailPlayer.Clicked += MultiExecutionPreventor.NewEventHandler (_emailPlayer_Clicked, multiExeToken);
			_copySnippet.Clicked += MultiExecutionPreventor.NewEventHandler (_copySnippet_Clicked, multiExeToken);
			_emailSnippet.Clicked += MultiExecutionPreventor.NewEventHandler (_emailSnippet_Clicked, multiExeToken);
		}

		async Task _emailSnippet_Clicked (object sender, EventArgs e)
		{
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading (null, Acr.UserDialogs.MaskType.Gradient);
			var html = await GetEmbeddedSnippetCode( _embedUrl, Theme);
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
			if (string.IsNullOrEmpty (html))
				return;
			EmailSnippetPressed?.Invoke (this, html);
		}

		async Task _copySnippet_Clicked (object sender, EventArgs e)
		{
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading (null, Acr.UserDialogs.MaskType.Gradient);
			var html = await GetEmbeddedSnippetCode( _embedUrl, Theme);
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
			if (string.IsNullOrEmpty (html))
				return;
			CopySnippetPressed?.Invoke (this, html);
		}

		async Task _emailPlayer_Clicked (object sender, EventArgs e)
		{
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading (null, Acr.UserDialogs.MaskType.Gradient);
			var html = await GetPlayerHtml ();
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
			if (string.IsNullOrEmpty (html))
				return;
			EmailPlayerPressed?.Invoke(this, html);
		}

		async Task _copyPlayer_Clicked (object sender, EventArgs e)
		{
			Acr.UserDialogs.UserDialogs.Instance.ShowLoading (null, Acr.UserDialogs.MaskType.Gradient);
			var html = await GetPlayerHtml ();
			Acr.UserDialogs.UserDialogs.Instance.HideLoading ();
			if (string.IsNullOrEmpty (html))
				return;
			CopyPlayerPressed?.Invoke(this, html);
		}


		Task<string> GetPlayerHtml(){
			uint width, height;
			switch(_currentSize){
				case 0:
					width = 426;
					height = 320;
					break;
				case 1:
					width = 640;
					height = 480;
					break;
				case 2:
					width = 854;
					height = 640;
					break;
				default:
					width = 1280;
					height = 960;
					break;
			}

			return GetEmbeddedPlayerCode (_embedUrl, width, height);
		}

		View InitSnippetSection(){
			var snippetLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("snippetLbl", "embedpreview"),
				TextColor = BlrtStyles.BlrtBlackBlack,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				XAlign = TextAlignment.Center
			};

			_snippetImg = new Image () {
				WidthRequest = 227,
				HeightRequest = 68,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Source = _isPublic? (Device.OS== TargetPlatform.iOS? "public_snippet@3x.png":"public_snippet.png") : (Device.OS== TargetPlatform.iOS? "private_snippet@3x.png":"private_snippet.png")
			};

			_switch = new NoTextSwitch ();
			var lightTheme = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("light_theme", "embedpreview"),
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				YAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.End
			};
			var darkTheme = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("dark_theme", "embedpreview"),
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				YAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start
			};

			var switchSection = new Grid () {
				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) },
				},
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) },
				},
				ColumnSpacing = 15,
				Children = {
					{lightTheme, 0,0},
					{_switch,1,0},
					{darkTheme, 2,0}
				}
			};

			_copySnippet = new FormsBlrtButton () {
				Text = BlrtUtil.PlatformHelper.Translate("item_options_copy_embed", "conversation-screen").ToUpper(),
				Image = "copy_icon.png",
				BorderRadius = 4,
				Padding = Device.OS == TargetPlatform.iOS? new Thickness(5,0): -999,
				BackgroundColor = BlrtStyles.BlrtGreen,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				TextColor = BlrtStyles.BlrtBlackBlack,
				WidthRequest = 120,
				HeightRequest = 30,
			};
			_emailSnippet = new FormsBlrtButton () {
				Text = BlrtUtil.PlatformHelper.Translate("item_options_email_embed", "conversation-screen").ToUpper(),
				Image = "email_icon.png",
				BorderRadius = 4,
				Padding = Device.OS == TargetPlatform.iOS? new Thickness(5,0): -999,
				BackgroundColor = BlrtStyles.BlrtGreen,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				TextColor = BlrtStyles.BlrtBlackBlack,
				WidthRequest = 120,
				HeightRequest = 30,
			};

			var playerBtns = new StackLayout () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(0,10),
				Spacing = 20,
				Children = {
					_copySnippet, _emailSnippet
				}
			};

			var orlbl = new FormsBlrtButton () {
				Padding = 1,
				BorderRadius = 4,
				Text = "OR",
				BackgroundColor = BlrtStyles.BlrtAccentBlueShadow,
				WidthRequest = 40,
				HeightRequest = 26,
				TextColor = BlrtStyles.BlrtWhite,
				FontAttributes = FontAttributes.Bold,
				FontSize = BlrtStyles.CellContentFont.FontSize,
				HorizontalOptions = LayoutOptions.Center
			};

			var snippetSection = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				Spacing = 20,
				BackgroundColor = BlrtStyles.BlrtWhite,
				Padding = new Thickness(0,-13,0,20),
				Children = {
					orlbl,
					snippetLbl,
					_snippetImg,
					switchSection,
					playerBtns
				}
			};

			_switch.Toggled += (sender, e) => {
				if(_switch.IsToggled){
					snippetLbl.TextColor
						= lightTheme.TextColor
						= darkTheme.TextColor
						= BlrtStyles.BlrtWhite;
					snippetSection.BackgroundColor = BlrtStyles.BlrtGray7;
					_snippetImg.Source = _isPublic? (Device.OS== TargetPlatform.iOS? "public_snippet_dark@3x.png":"public_snippet_dark.png") : (Device.OS== TargetPlatform.iOS? "private_snippet_dark@3x.png":"private_snippet_dark.png");
				}else{
					snippetLbl.TextColor
						= lightTheme.TextColor
						= darkTheme.TextColor
						= BlrtStyles.BlrtBlackBlack;
					snippetSection.BackgroundColor = BlrtStyles.BlrtWhite;
					_snippetImg.Source = _isPublic? (Device.OS== TargetPlatform.iOS? "public_snippet@3x.png":"public_snippet.png") : (Device.OS== TargetPlatform.iOS? "private_snippet@3x.png":"private_snippet.png");
				}
			};

			return snippetSection;
		}


		View InitPlayerSection(){
			var alertText = new FormattedString ();
			alertText.Spans.Add (new Span () {
				Text = _isPublic ? BlrtUtil.PlatformHelper.Translate("public_learnmore", "embedpreview") : 
					BlrtUtil.PlatformHelper.Translate("private_learnmore", "embedpreview"),
				ForegroundColor = BlrtStyles.BlrtWhite,
				FontSize = BlrtStyles.CellContentFont.FontSize,
			});
			alertText.Spans.Add (new Span () {
				Text = " " + "Learn More",
				ForegroundColor = BlrtStyles.BlrtAccentBlue,
				FontSize = BlrtStyles.CellContentFont.FontSize,
			});
			var alertLbl = new Label (){
				XAlign = TextAlignment.Center,
				FormattedText = alertText
			};

			var alertView = new ContentView () {
				Padding = new Thickness (20, 10),
				Content = alertLbl,
				BackgroundColor = BlrtStyles.BlrtCharcoal
			};

			alertView.GestureRecognizers.Add (new TapGestureRecognizer () {
				Command = new Command (() => {
					LearnMorePressed?.Invoke(this, EventArgs.Empty);
				})
			});

			var webPlayerLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("webPlayerLbl", "embedpreview"),
				TextColor = BlrtStyles.BlrtBlackBlack,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				XAlign = TextAlignment.Center
			};


			_pageContainer = new StackLayout () {
				VerticalOptions = LayoutOptions.Fill,
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Padding = 0
			};
			CreatePages ();

			_carouselView = new ExtendedScrollView () {
				Padding = 0,
				AnimateScroll = true,
				Orientation = ScrollOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Content = _pageContainer,
				HeightRequest = 166
			};
			_carouselView.DraggingStarted += OnDraggingStarted;
			_carouselView.DraggingEnded += OnDraggingEnded;
			_carouselView.Scrolled += OnScrolled;

			_browserLbl = new Label () {
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				Text = BlrtUtil.PlatformHelper.Translate("view_in_chrome", "embedpreview")
			};

			_pageControl = new BlrtPageControl (TotalPageCount) {
				Padding = new Thickness(0,-5,0,0),
				PageIndicatorTintColor = BlrtStyles.BlrtGray4,
				CurrentPageIndicatorTintColor = BlrtStyles.BlrtCharcoal,
				VerticalOptions = LayoutOptions.Center,
				IndicatorRadius = 4
			};

			_prePage = new BlrtImageButton {
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center,
				ImageSource = ImageSource.FromFile ("canvas_page_prev.png"),
				TintColor = BlrtStyles.BlrtCharcoal,
				Opacity = 0
			};
			_nextPage = new BlrtImageButton {
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center,
				ImageSource = ImageSource.FromFile ("canvas_page_next.png"),
				TintColor = Color.Black
			};
			_nextPage.Clicked += (sender, e) => {
				CurrentPage++;
			};
			_prePage.Clicked += (sender, e) => {
				CurrentPage--;
			};

			var playerView = new Grid () {
				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) },
				},
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) },
				},
				Children = {
					_carouselView, _nextPage, _prePage
				}
			};

			_slider = new FormsSlider () {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Minimum = 0,
				Maximum = 3,
				Value = 0,
				WidthRequest = 235
			};
			_slider.ValueChanged += _slider_ValueChanged;

			var sliderView = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Padding = new Thickness (20, 0, 20, -5),
				Spacing = 10,
				Children = {
					new Label () {
						Text = BlrtUtil.PlatformHelper.Translate("size", "embedpreview"),
						YAlign = TextAlignment.Center,
						FontSize = BlrtStyles.CellDetailFont.FontSize,
						FontAttributes = FontAttributes.Bold
					},
					_slider
				}
			};

			_playerSizeLbl = new Label () {
				XAlign = TextAlignment.Center
			};

			_copyPlayer = new FormsBlrtButton () {
				Text = BlrtUtil.PlatformHelper.Translate("item_options_copy_embed", "conversation-screen").ToUpper(),
				Image = "copy_icon.png",
				BorderRadius = 4,
				Padding = Device.OS == TargetPlatform.iOS? 0: -999,
				BackgroundColor = BlrtStyles.BlrtGreen,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				TextColor = BlrtStyles.BlrtBlackBlack,
				WidthRequest = 120,
				HeightRequest = 30,
			};
			_emailPlayer = new FormsBlrtButton () {
				Text = BlrtUtil.PlatformHelper.Translate("item_options_email_embed", "conversation-screen").ToUpper(),
				Image = "email_icon.png",
				BorderRadius = 4,
				Padding = Device.OS == TargetPlatform.iOS? new Thickness(5,0): -999,
				BackgroundColor = BlrtStyles.BlrtGreen,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				TextColor = BlrtStyles.BlrtBlackBlack,
				WidthRequest = 120,
				HeightRequest = 30,
			};

			var playerBtns = new StackLayout () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(0,10),
				Spacing = 20,
				Children = {
					_copyPlayer, _emailPlayer
				}
			};

			var playerSection = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				BackgroundColor = BlrtStyles.BlrtGray3,
				Padding = new Thickness(0,0,0,33),
				Children = {
					alertView,
					new BoxView(){HeightRequest = 20},
					webPlayerLbl,
					new BoxView(){HeightRequest = 20},
					playerView,
					new BoxView(){HeightRequest = 10},
					_browserLbl,
					new BoxView(){HeightRequest = 15},
					_pageControl,
					new BoxView(){HeightRequest = 10},
					sliderView,
					new BoxView(){HeightRequest = 10},
					_playerSizeLbl,
					new BoxView(){HeightRequest = 20},
					playerBtns
				}
			};
			return playerSection;
		}


		bool _ignoreSliderChange;
		int _currentSize = -1;
		void _slider_ValueChanged (object sender, ValueChangedEventArgs e)
		{
			if (_ignoreSliderChange)
				return;
			_ignoreSliderChange = true;
			var rounded = (int)(Math.Round (e.NewValue));
			string csource, ncsource, sizeTitle, sizeSize = "";
			switch(rounded){
				case 0:
					csource = (Device.OS== TargetPlatform.iOS? "webplayer_320@3x.png":"webplayer_320.png");
					ncsource = (Device.OS== TargetPlatform.iOS? "non_webplayer_320@3x.png":"non_webplayer_320.png");
					sizeTitle = "Small";
					sizeSize = "426x320";
					break;
				case 1:
					csource = (Device.OS== TargetPlatform.iOS? "webplayer_480@3x.png":"webplayer_480.png");
					ncsource = (Device.OS== TargetPlatform.iOS? "non_webplayer_480@3x.png":"non_webplayer_480.png");
					sizeTitle = "Medium";
					sizeSize = "640x480";
					break;
				case 2:
					csource = (Device.OS== TargetPlatform.iOS? "webplayer_640@3x.png":"webplayer_640.png");
					ncsource = (Device.OS== TargetPlatform.iOS? "non_webplayer_640@3x.png":"non_webplayer_640.png");
					sizeTitle = "Large";
					sizeSize = "854x640";
					break;
				default:
					csource = (Device.OS== TargetPlatform.iOS? "webplayer_960@3x.png":"webplayer_960.png");
					ncsource = (Device.OS== TargetPlatform.iOS? "non_webplayer_960@3x.png":"non_webplayer_960.png");
					sizeTitle = "Extra Large";
					sizeSize = "1280x960";
					break;
			}

			//if value changed, then change the content
			if (_currentSize != rounded) {
				_chromeImg.Source = csource;
				_nonChromeImg.Source = ncsource;
				_playerSizeLbl.FormattedText = new FormattedString () {
					Spans = {
						new Span () {
							Text = sizeTitle + "\n",
							FontSize = BlrtStyles.CellContentFont.FontSize,
							FontAttributes = FontAttributes.Bold
						},
						new Span () {
							Text = sizeSize,
							FontSize = BlrtStyles.CellContentFont.FontSize,
						}
					}
				};
				_currentSize = rounded;
			}

			_slider.Value = rounded;
			_ignoreSliderChange = false;
		}

		protected override void OnSizeAllocated (double width, double height)
		{
			base.OnSizeAllocated (width, height);
			if (null != _pages) {
				foreach (var item in _pages) {
					item.WidthRequest = width;
				}
			}
			_pageWidth = width;
		}


		public const int TotalPageCount = 2;
		void CreatePages ()
		{
			_chromeImg = new Image () {
				WidthRequest = 235,
				HeightRequest = 166,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Source = "webplayer_480.png"
			};
			_nonChromeImg = new Image () {
				WidthRequest = 235,
				HeightRequest = 166,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Source = "non_webplayer_480.png"
			};

			_pages = new List<View> ();
			for (int i = 0; i < 2; i++) {
				var page = new ContentView () {
					Padding = 0,
					VerticalOptions = LayoutOptions.Fill,
					WidthRequest = _pageWidth
				};
				var img = i == 0 ? _chromeImg : _nonChromeImg;
				page.Content = img;
				_pages.Add (page);
				_pageContainer.Children.Add (page);
			}
		}

		int _currentPage;
		public int CurrentPage {
			get { 
				return _currentPage; 
			}

			set {
				ChangePage (value);
			}
		}

		void ChangePage (int toPage) {
			if (toPage < 0) {
				toPage = 0;
			} else if (toPage >= TotalPageCount) {
				toPage = TotalPageCount - 1;
			}
			_carouselView.Position = new Point (_pageWidth * toPage, 0);
		}

		DateTime _dragStartTime;
		int _dragStartPage;
		Point _dragStartPosition;
		void OnDraggingStarted (object sender, EventArgs e)
		{
			_dragStartTime = DateTime.Now;
			_dragStartPosition = _carouselView.Position;
			_dragStartPage = CurrentPage;
		}

		void OnDraggingEnded (object sender, EventArgs e)
		{
			if ((DateTime.Now - _dragStartTime).TotalSeconds < 2) {
				var delta = _carouselView.Position.X - _dragStartPosition.X;
				if (delta > 15) {
					CurrentPage = _dragStartPage + 1;
				} else if (delta < -15) {
					CurrentPage = _dragStartPage - 1;
				} else {
					ChangePage (CurrentPage);
				}
			} else {
				CurrentPage = GetPageNumFromPosition(_carouselView.Position.X);
			}
		}

		void OnScrolled (ScrollView view, Rectangle viewPort)
		{
			var nowPage = GetPageNumFromPosition(viewPort.X);
			if (nowPage != _currentPage) {
				_browserLbl.Text = nowPage==0? BlrtUtil.PlatformHelper.Translate("view_in_chrome", "embedpreview"): BlrtUtil.PlatformHelper.Translate("view_in_nonchrome", "embedpreview");
				if (_currentPage == TotalPageCount - 1) {
					_nextPage.FadeTo (1);
				} else if (nowPage == TotalPageCount - 1) {
					_nextPage.FadeTo (0);
				} 
				if (_currentPage == 0) {
					_prePage.FadeTo (1);
				} else if (nowPage == 0) {
					_prePage.FadeTo (0);
				}
				_currentPage = nowPage;
				_pageControl.CurrentPage = _currentPage;
			}
		}

		int GetPageNumFromPosition (double x)
		{
			var num = (int)(x / _pageWidth + 0.5d);
			if (num < 0) {
				num = 0;
			} else if (num >= TotalPageCount) {
				num = TotalPageCount - 1;
			}
			return num;
		}

		//getting embed code logic

		Task<string> GetEmbeddedPlayerCode(string url, uint width, uint height)
		{
			return GetEmbeddedHtml (url, ("maxwidth=" + width), ("maxheight" + height));
		}

		Task<string> GetEmbeddedSnippetCode(string url, SnippetTheme theme)
		{
			List<string> queries = new List<string>() { "player=0" };
			if (SnippetTheme.Dark == theme) {
				queries.Add ("theme=dark");
			}
			return GetEmbeddedHtml (url, queries.ToArray ());
		}

		async Task<string> GetEmbeddedHtml (string url, params string[] otherQueries)
		{
			string html = "";
			string requestUrl = BlrtConstants.EmbedUrl + "/oembed?url=" + HttpUtility.UrlEncode (url);
			if (otherQueries.Length > 0) {
				requestUrl += ("&" + String.Join ("&", otherQueries));
			}
			bool retry = true;

			while (retry) {
				try {
					var t = Task.Factory.StartNew(() =>{
						HttpWebRequest request = (HttpWebRequest)WebRequest.Create (requestUrl);
						using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
							if (response.StatusCode != HttpStatusCode.OK) {
								throw new Exception("http error");
							} else {
								var stream = new StreamReader (response.GetResponseStream ());
								var responseText = stream.ReadToEnd ();
								var result = JsonConvert.DeserializeObject<EmbedResponse> (responseText);
								html = result.Html;
								retry = false;
							}
						}
					});
					await BlrtUtil.AwaitOrCancelTask(t, new System.Threading.CancellationTokenSource(10000).Token);
				} catch (Exception e) {
					BlrtUtil.Debug.ReportException (e, new Dictionary<string,string>(){{"message", "Error Get Embed Html"}});
					retry = await HtmlFailureAlert ();
					if (!retry)
						return "";
				}
			}
			return html;
		}

		async Task<bool> HtmlFailureAlert ()
		{
			return await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("embed_server_error_title", "conversation_screen"),
				BlrtUtil.PlatformHelper.Translate ("embed_server_error_message", "conversation_screen"),
				BlrtUtil.PlatformHelper.Translate ("embed_server_error_accept", "conversation_screen"),
				BlrtUtil.PlatformHelper.Translate ("embed_server_error_cancel", "conversation_screen")
			);
		}

		public class EmbedResponse
		{
			[JsonProperty ("html")]
			public string Html{ get; set; }
		}
	}
}

