﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Threading.Tasks;
using BlrtData;
using System.Collections.Generic;
using BlrtData.ExecutionPipeline;
using BlrtData.Views.CustomUI;

using System.Threading;
using BlrtAPI;

namespace BlrtData.Views.Conversation
{
	public static class ConversationFunctions
	{

		public static IExecutor Executor {
			get {
				return new Executor (ExecutionSource.System);
			}
		}

		public static async Task<bool> ProcessAccessRequest(string conversationId, string eventId, bool accept){
			var apiCall = new APIConversationProcessRequest (new APIConversationProcessRequest.Parameters() {
				ConversationId = BlrtEncode.EncodeId(conversationId),
				EventId = BlrtEncode.EncodeId(eventId),
				Accept = accept
			});

			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout(15000))) {
				if(apiCall.Response.Success){
					return true;
				}else{
					var code = apiCall.Response.Error.Code;
					if(code == APIConversationProcessRequestError.TooManyRelations){
						BlrtManager.App.ShowAlert (Executor, new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("grant_access_full_title", "conversation_screen"), 
							BlrtUtil.PlatformHelper.Translate ("grant_access_full_message", "conversation_screen"), 
							BlrtUtil.PlatformHelper.Translate ("grant_access_full_cancel", "conversation_screen"))
						);
						return false;
					}
					return true;
				}
			}else{
				BlrtManager.App.ShowAlert (Executor, new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
					BlrtUtil.PlatformHelper.Translate ("OK"))
				);
				return false;
			}
		}
	}
}

