using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using BlrtData.Views.Helpers;
using BlrtData.Views.Settings.Profile;
using Parse;

namespace BlrtData.Views.Settings
{
	public class EmailPage : BlrtContentPage
	{
		const int rowHeight = 68;

		public EventHandler<SelectedItemChangedEventArgs> RowSelected;

		private ListView _listView;
		private Button _addSecondaryBtn;
		private List<EmailCellItem> _secondaryEmailList = new List<EmailCellItem> ();

		public EventHandler<BlrtUserContactDetail> EditPrimaryRequested, SendVerificationEmailRequested, MakePrimaryRequested, RemoveRequested;
		public EventHandler OnAddSecondary;

		public override string ViewName {
			get {
				return "MyProfile_Edit_Email";
			}
		}

		public EmailPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_email_title", "profile");
			_listView = new ListView {
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
				IsPullToRefreshEnabled = true,
			};

			RefreshItemSource (false);
			_listView.ItemTemplate = new DataTemplate (typeof (EmailPageCell));

			_listView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null) {
					return;
				}
				_listView.SelectedItem = null;

				RowSelected?.Invoke (sender, e);
			};

			RowSelected += VerifyOrChange;

			_addSecondaryBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("profile_editemail_secondary_add", "profile"),
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent
			};
			if (Device.OS == TargetPlatform.Android) {
				_addSecondaryBtn.TextColor = BlrtStyles.BlrtAccentBlue;
				_addSecondaryBtn.FontSize = BlrtStyles.CellTitleFont.FontSize;
			}

			_addSecondaryBtn.Clicked += AddSecondaryEmail;
			_listView.Footer = _addSecondaryBtn;


			Content = _listView;

			_listView.Refreshing += (sender, e) => {
				RefreshItemSource (true);
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			PushHandler.OnContactInfoRefresh += OnUserContactInfoChanged;
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();
			PushHandler.OnContactInfoRefresh -= OnUserContactInfoChanged;
		}

		void OnUserContactInfoChanged (object sender, EventArgs e)
		{
			this.RefreshItemSource (true);
		}

		void AddSecondaryEmail (object sender, EventArgs e)
		{
			OnAddSecondary?.Invoke (this, EventArgs.Empty);
		}

		private async void VerifyOrChange (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as EmailCellItem;

			var itemSource = new List<EmailCellItem> (_listView.ItemsSource as IEnumerable<EmailCellItem>);
			var index = itemSource.IndexOf (e.SelectedItem as EmailCellItem);


			var primary = (index == 0);


			string cancel, action;

			string editPrimary = null;
			string verifyPrimary = null;
			string verifySecondary = null;
			string makePrimary = null;
			string remove = null;

			cancel = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_cancel", "profile");

			string title = null;

			if (primary) {
				editPrimary = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_primary_edit", "profile");
				if (!BlrtUserHelper.EmailVerified) {
					verifyPrimary = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_primary_verify", "profile");
					action = await DisplayActionSheet (title, cancel, null, editPrimary, verifyPrimary);
				} else {
					action = await DisplayActionSheet (title, cancel, null, editPrimary);
				}

			} else {
				string edit;
				if (item.Text == null) {
					edit = makePrimary = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_secondary_makeprimary", "profile");
				} else {
					edit = verifySecondary = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_secondary_verify", "profile");
				}

				remove = BlrtUtil.PlatformHelper.Translate ("profile_editemail_popup_secondary_remove", "profile");

				action = await DisplayActionSheet (title, cancel, null, edit, remove);
			}

			if (action == editPrimary) {
				EditPrimaryStart ();
			} else if (action == verifyPrimary) {
				SendVerificationEmail ();
			} else {
				var contact = BlrtUserHelper.SecondaryEmails.FirstOrDefault (a =>
					a.Value == item.Email.ToString ()
				);
				if (action == verifySecondary) {
					SendVerificationEmail (contact);
				} else if (action == makePrimary) {
					MakePrimary (contact);
				} else if (action == remove) {
					RemoveStart (contact);
				}
			}
		}

		void EditPrimaryStart ()
		{
			EditPrimaryRequested?.Invoke (this, BlrtUserHelper.PrimaryEmail);
		}

		void SendVerificationEmail (BlrtUserContactDetail contact = null)
		{
			if (contact == null) {
				contact = BlrtUserHelper.PrimaryEmail;
			}
			SendVerificationEmailRequested?.Invoke (this, contact);
		}

		void MakePrimary (BlrtUserContactDetail contact)
		{
			MakePrimaryRequested?.Invoke (this, contact);
		}

		void RemoveStart (BlrtUserContactDetail contact)
		{
			RemoveRequested?.Invoke (this, contact);
		}

		private EmailCellItem [] ParseToCellItems ()
		{
			var primary = new EmailCellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_email_primary_field", "profile")),
				Email = ProfileMainPageCell.GetFormattedString (ParseUser.CurrentUser.Email, ProfileMainPageCell.CellTextGrayColor),
				Image = "settings_arrow.png",
			};
			if (!BlrtUserHelper.EmailVerified) {
				primary.Text = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_editemail_notverified", "profile"), BlrtStyles.BlrtAccentRed);
			}

			var list = new List<EmailCellItem> ();
			list.Add (primary);
			list.AddRange (_secondaryEmailList);
			return list.ToArray ();
		}

		public async Task<bool> RefreshItemSource (bool shouldFetchUser = false)
		{
			Device.BeginInvokeOnMainThread (() => {
				_listView.IsRefreshing = true;
			});
			SetSecondaryEmails ();
			_listView.ItemsSource = ParseToCellItems ();

			if (shouldFetchUser) {
				await Xamarin.Forms.DependencyService.Get<IBlrtProfileHelper> ().ReloadContactDetails (true);
				RefreshItemSource (false);
			}
			Device.BeginInvokeOnMainThread (() => {
				_listView.IsRefreshing = false;
			});
			return true;
		}

		void SetSecondaryEmails ()
		{
			if (ParseUser.CurrentUser == null) {
				return;
			}

			_secondaryEmailList = new List<EmailCellItem> ();
			if (BlrtUserHelper.SecondaryEmails != null && BlrtUserHelper.SecondaryEmails.Count () != 0) {
				foreach (var contact in BlrtUserHelper.SecondaryEmails) {
					var item = new EmailCellItem ();
					item.Email = ProfileMainPageCell.GetFormattedString (contact.Value, ProfileMainPageCell.CellTextGrayColor);
					item.Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_email_secondary_field_new", "profile"));
					item.Image = "settings_arrow.png";
					if (!contact.Verified) {
						item.Text = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_editemail_notverified", "profile"), BlrtStyles.BlrtAccentRed);
					}
					_secondaryEmailList.Add (item);
				}
			}
		}

		public class EmailCellItem : CellItem
		{
			public FormattedString Email { get; set; }
		}

	}
}

