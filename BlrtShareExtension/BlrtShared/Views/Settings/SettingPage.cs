using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using BlrtData.Views.CustomUI;
using BlrtData.Views.Settings.Profile;
using System.Collections.Generic;

namespace BlrtData.Views.Settings
{
#if __ANDROID__
	using BlrtApp.Views;
	public class SettingPage : BlrtContentPage, IBackStatePage
	{
		public BackState BackState { get { return BackState.Finish; } }
#else
	public class SettingPage: BlrtContentPage
	{
#endif
		static readonly int rowHeight = 43;

		public event EventHandler<SelectedItemChangedEventArgs> RowSelected;
		private ListView _listView;

		public override string ViewName {
			get {
				return "Settings";
			}
		}

		public SettingPage () : base ()
		{

			Title = BlrtUtil.PlatformHelper.Translate ("profile_setting_title", "profile");

			_listView = new FormsListView {
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
				ItemTemplate = new DataTemplate (typeof (ProfileMainPageCell)),
				ItemsSource = new List<CellItem> ()
			};

			//			RefreshItemSource ();

			_listView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null) {
					return;
				}

				if (RowSelected != null)
					RowSelected (sender, e);
				_listView.SelectedItem = null;
			};

			Content = _listView;

		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			RefreshItemSource ();
		}

		public void DeSelectRow ()
		{
			_listView.SelectedItem = null;
		}

		public void RefreshItemSource ()
		{
			if (null == _listView)
				return;
			var items = new List<CellItem> ();
			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (string.IsNullOrWhiteSpace (BlrtPermissions.AccountName) ? "Account" : "Account (" + BlrtPermissions.AccountName + ")", ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "account",
			});
			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_notifications_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "notifications",
			});
			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_block_user_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "block",
			});
			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_device_settings_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "device",
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_update_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "update",
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_review_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "review",
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_privacy_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "privacy",
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("profile_tos_title", "profile"), ProfileMainPageCell.CellTextBlueColor),
				Image = "settings_arrow.png",
				NavText = "tos",
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("device_reshow_help_tooltips", "profile"), ProfileMainPageCell.CellTextBlueColor),
				NavText = "help_overlay",
				ShowSwitch = true,
				SwitchOn = !BlrtData.Views.Helpers.HelpOverlayHelper.AtLeastOneOverlayViewed (),
				SwitchCellAction = new Action<bool> ((isOn) => {
				BlrtPersistant.Properties [BlrtConstants.HelpOverlayMailboxViewed]
				= BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed]
					= BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed]
#if __ANDROID__
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayEmptyConversationViewed]
#elif __IOS__
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithNonWelcomeBlrtViewed]
#endif
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed]
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed]
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed]
						= BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasFinishViewed]
						= !isOn;
					BlrtPersistant.SaveProperties();
					BlrtData.Helpers.BlrtTrack.SettingsShowOverlays();
				})
			});

			items.Add (new CellItem {
				Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_version_title", "profile"),  ProfileMainPageCell.CellTextBlueColor),
				Text = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.GetAppVersion(),  ProfileMainPageCell.CellTextGrayColor, Font.SystemFontOfSize (15)),
				NavText = "version_detail",
			});

			_listView.ItemsSource = items;
		}
	}
}

