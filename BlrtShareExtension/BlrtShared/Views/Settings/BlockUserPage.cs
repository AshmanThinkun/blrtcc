﻿using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Collections.Generic;
using BlrtAPI;
using Parse;
using System.Threading.Tasks;
using Acr.UserDialogs;
using BlrtData.Query;
using System.Threading;
using System.Linq;
using BlrtShared;

namespace BlrtData.Views.Settings
{
	public class BlockUserPage : BlrtContentPage
	{
		const int rowHeight = 68;
		const int loadingHeight = 34;
		readonly int blockBarHeight = Device.OS == TargetPlatform.iOS ? 30 : 50;
		ActivityIndicator _loading;
		ListView _listView;
		Entry _blockEntry;
		// number of running thread. if it is bigger than 0 the loading indicator will show and run  
		static int _indicatorCount = 0;
		// tempary store the user IDs which to be unblocked 
		static List<string> unblockedLocalUserIds = new List<string> ();

		public override string ViewName {
			get {
				return "Settings_BlockUser";
			}
		}
		public BlockUserPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_block_user_title", "profile");
			_loading = new ActivityIndicator () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				IsRunning = true,
				IsEnabled = false,
				IsVisible = false,
				HeightRequest = loadingHeight,
			};

			_listView = new FormsListView {
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
			};
			_listView.ItemTemplate = new DataTemplate (typeof (BlockUserPageCell));
			// fetch the data for the _listView
			Refresh (true).FireAndForget ();
			_listView.ItemSelected += SafeEventHandler.PreventMulti<SelectedItemChangedEventArgs> (
				async (sender, e) => {
					if (e.SelectedItem == null) {
						return;
					}
					var sle = e.SelectedItem as BlrtUser;
					var action = await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("unblock_user_title", "profile"),
						string.Format (BlrtUtil.PlatformHelper.Translate ("unblock_user_message", "profile"), sle.Email),
						BlrtUtil.PlatformHelper.Translate ("unblock_user_accept", "profile"),
						BlrtUtil.PlatformHelper.Translate ("unblock_user_cancel", "profile")
					);
					if (action) {
						//  unblock user function to remove the select user from the blockuser list
						unblockedLocalUserIds.Add (sle.ObjectId);
						Reload ();
						await Block (null, true, sle.ObjectId);
						// if there is no block thread, clear unblockedLocalUserIds
						unblockedLocalUserIds.Remove (sle.ObjectId);
					}
					_listView.SelectedItem = null;
				}
			);

			_blockEntry = new Entry () {
				Placeholder = BlrtUtil.PlatformHelper.Translate ("block_entry_placeholder", "profile"),
				HeightRequest = blockBarHeight,
				Keyboard = Keyboard.Email
			};

			_blockEntry.Completed += SafeEventHandler.PreventMulti (OnBlockClicked);

			var blockButton = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("block_btn_text", "profile"),
				HeightRequest = blockBarHeight,
				BackgroundColor = Color.Transparent
			};
#if __ANDROID__
			blockButton.TextColor = BlrtStyles.BlrtAccentBlue;	
			blockButton.FontSize = BlrtStyles.CellTitleFont.FontSize;
			_blockEntry.FontSize = BlrtStyles.CellTitleFont.FontSize;
			_blockEntry.BackgroundColor = BlrtStyles.BlrtWhite;
			_blockEntry.VerticalOptions = LayoutOptions.Center;
			_blockEntry.HeightRequest = -1;
#endif

			// block the user through the email entered in _blockEntry
			blockButton.Clicked += SafeEventHandler.PreventMulti (OnBlockClicked);

			var expLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("block_explanation_text", "profile"),
				TextColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellDetailFont.FontSize
			};

			var blockEdit = new Grid () {
				Padding = new Thickness (10, 10, 10, 0),
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
				},
				RowDefinitions = {
					new RowDefinition (){Height= new GridLength (1,GridUnitType.Auto)},
					new RowDefinition (){Height= new GridLength (1,GridUnitType.Auto)}
				},
			};

			blockEdit.Children.Add (expLbl, 0, 1);
			Grid.SetColumnSpan (expLbl, 2);
			blockEdit.Children.Add (_blockEntry, 0, 0);
			blockEdit.Children.Add (blockButton, 1, 0);

			Content = new Grid () {
				RowSpacing = 0,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition () { Width = new GridLength(1, GridUnitType.Star) },
				},
				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition () { Height = new GridLength(1, GridUnitType.Auto) },
					new RowDefinition () { Height = new GridLength(1, GridUnitType.Auto) },
					new RowDefinition () { Height = new GridLength(1, GridUnitType.Star) },
				},
				Children = {
					{_loading, 0, 0},
					{blockEdit,0, 1},
					{_listView, 0, 2},

				}
			};
			GetMyEmails ();
		}

		List<string> _currentUserEmails;
		void GetMyEmails ()
		{
			_currentUserEmails = new List<string> ();
			_currentUserEmails.Add (ParseUser.CurrentUser.Email.ToLower ());

			var secondaries = BlrtUserHelper.SecondaryEmails.Select (t => t.Value.ToLower ());
			_currentUserEmails.AddRange (secondaries);
		}

		async Task OnBlockClicked (object sender, EventArgs e)
		{
			String blockEmail = _blockEntry.Text;
			if (!BlrtUtil.IsValidEmail (blockEmail) || _currentUserEmails.Contains (blockEmail.ToLower ())) {
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_cancel", "profile")
				);
				return;
			}
			_blockEntry.Text = "";
			using (UserDialogs.Instance.Loading ("Loading...")) {
				await Block (blockEmail);
			}
		}

		async Task Refresh (bool reloadFirst = false)
		{
			openIndicator ();
			if (reloadFirst) {
				Reload ();
			}
			//always fetch users from cloud
			await ParseUser.CurrentUser.BetterFetchAsync (BlrtUtil.CreateTokenWithTimeout (15000));

			var blockUserIds = GetBlockedUserIds ();
			await BlrtParseActions.FindUsersQuery (blockUserIds, new CancellationTokenSource (15000).Token);
			Reload ();

			closeIndicator ();
		}

		void Reload ()
		{
			var blockUserIDs = GetBlockedUserIds ();
			List<BlrtUser> blockedUsers = new List<BlrtUser> ();
			for (int i = 0; i < blockUserIDs.Length; i++) {
				var user = BlrtManager.DataManagers.Default.GetUser (blockUserIDs [i]);
				blockedUsers.Add (user);
			}
			_listView.ItemsSource = blockedUsers;
		}


		public async Task Block (string email = null, bool unblock = false, string id = null)
		{
			openIndicator ();
			var apiCall = new APIUserBlock (
				new APIUserBlock.Parameters () {
					BlockUserEmail = email,
					UnBlock = unblock,
					BlockUserId = id
				}
			);
			if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
				if (apiCall.Response.Success) {
					BlrtData.Helpers.BlrtTrack.SettingsBlockUser (email ?? id, !unblock);
					await Refresh ();
					closeIndicator ();
					return;
				}
				if (null != apiCall.Response.Error) {
					switch (apiCall.Response.Error.Code) {
						case APIUserBlockError.blockUserNotExist:
							// user unblock this user from other device, we only need to refresh
							await Refresh ();
							break;
						case APIUserBlockError.blockUserEmailNotExist:
							await DisplayAlert (
								BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_title", "profile"),
								BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_message", "profile"),
								BlrtUtil.PlatformHelper.Translate ("profile_editemail_not_email_cancel", "profile")
							);
							break;
					}
				}
			} else {
				await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
					BlrtUtil.PlatformHelper.Translate ("OK"));
			}
			closeIndicator ();
		}

		public void openIndicator ()
		{
			_indicatorCount++;
			if (_indicatorCount == 1) {
				Device.BeginInvokeOnMainThread (() => {
					_loading.IsVisible = true;
					_loading.IsEnabled = true;
				});
			}
		}

		public void closeIndicator ()
		{
			_indicatorCount--;
			if (_indicatorCount == 0) {
				Device.BeginInvokeOnMainThread (() => {
					_loading.IsVisible = false;
					_loading.IsEnabled = false;
				});
			}

		}

		string [] GetBlockedUserIds ()
		{
			try {
				var dic = ParseUser.CurrentUser.Get<IDictionary<string, int>> (BlrtUserHelper.BlockedUserFlagsKey);
				var blockedKeys = dic.Where (t => t.Value == 1 && !unblockedLocalUserIds.Contains (t.Key)).Select (u => u.Key).ToArray ();
				return blockedKeys;
			} catch (Exception e) {
				return new string [0] { };
			}

		}
	}
}