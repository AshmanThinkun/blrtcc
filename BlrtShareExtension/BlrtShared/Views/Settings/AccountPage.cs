using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;
using BlrtData.Views.Settings.Profile;
using BlrtData.Views.Helpers;
using Parse;

namespace BlrtData.Views.Settings
{
	public class AccountPage:BlrtContentPage
	{
		static readonly int rowHeight = 43;

		public EventHandler<SelectedItemChangedEventArgs> RowSelected;
		public EventHandler OnUpgrade{ get; set;}

		public Account UserAccount{ get; set;}

		private ListView _listView;
		private Button _upgradeBtn;
		const int TRIAL_SEC_HEIGHT = 70;

		public override string ViewName {
			get {
				return null;  //we can't use default screen tracking method, because on settings screen, it will be shown as default(first row), but iphone 6 or smaller device will not be able to see that 
			}
		}

		public AccountPage () :base()
		{

			var source = ParseToCellItems (GetUserAccount ());

			Title = UserAccount.Name;

			_listView = new FormsListView{
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
			};

			_listView.ItemsSource = source;
			_listView.ItemTemplate = new DataTemplate(typeof(ProfileMainPageCell));


			_listView.ItemSelected +=  (sender, e) => {
				if(e.SelectedItem==null){
					return;
				}
				_listView.SelectedItem = null;
				var itemSource = new List<CellItem>(_listView.ItemsSource as IEnumerable<CellItem>);


				if(RowSelected!=null)
					RowSelected(sender,e);
			};

			_upgradeBtn = getUpgradeBtn ();

			Content = new Grid(){
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
				},
				Padding = 0,
				RowSpacing = 0,
				ColumnSpacing = 0,


				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { 
					{_listView,0,0},
					{new ContentView (){
							VerticalOptions = LayoutOptions.Fill,
							HorizontalOptions = LayoutOptions.Fill,
							BackgroundColor = BlrtStyles.BlrtGray3,
							HeightRequest = TRIAL_SEC_HEIGHT,
							Content = _upgradeBtn,
						},0,1}
				}
			};
		}


		Button getUpgradeBtn(){
			var profileHelper = Xamarin.Forms.DependencyService.Get<IBlrtProfileHelper> ();
			string actionText;
			actionText = profileHelper.GetUpgradeButtonText();

			var heightRequest = BlrtUtil.PlatformHelper.GetPlatformName () == "android" ? 40 : 30;

			var btn = new FormsBlrtButton () {
				Text = actionText,
				BackgroundColor = BlrtStyles.BlrtCharcoal,
				TextColor = BlrtStyles.BlrtGreenHighlight,
				HeightRequest = heightRequest,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Padding = new Thickness(10,4),
			};



			if (String.IsNullOrWhiteSpace (actionText))
				btn.IsVisible = false;

			btn.Clicked+= (sender, e) => {
				ShowUpgrade();
			};
			return btn;
		}

		async Task ShowUpgrade ()
		{

			var result = await BlrtData.BlrtManager.App.OpenUpgradeAccount (new BlrtData.ExecutionPipeline.Executor(BlrtData.ExecutionPipeline.ExecutionSource.InApp));
			await result.Result.WaitAsyncCompletion ();

			RefreshItemSource ();
			if(OnUpgrade!=null)
				OnUpgrade(this,EventArgs.Empty);
		}

		private Account GetUserAccount ()
		{
			DateTime? date = ParseUser.CurrentUser.Get<DateTime?> (BlrtUserHelper.AccountTypeExpiration, null);
			if (date != null) {
				date = BlrtUtil.ToLocalTime (date.Value);
			}

			return UserAccount = new Account {
				Name 				= BlrtPermissions.AccountName,
				Duration 			= BlrtUtil.ToMinutes (BlrtPermissions.MaxBlrtDuration),
				MaxUser 			= BlrtPermissions.MaxConversationUsers + "",
				MediaSize 			= (BlrtPermissions.MaxPDFSizeKB / 1025f).ToString ("F0") + " MB",
				ImageResolution 	= BlrtPermissions.MaxImageResolution + "x" + BlrtPermissions.MaxImageResolution + " px",
				MaxConvoSize 		= (BlrtPermissions.MaxConvoSizeKb / 1025f).ToString ("F0") + " MB",
				MaxBlrtPage 		= BlrtPermissions.MaxBlrtPageCount + "",
				MaxBlrtSize 		= (BlrtPermissions.MaxBlrtSizeKb / 1025f).ToString ("F0") + " MB",
				ExpiryDate 			= (date != null) ? ((DateTime)date).ToString ("ddd MMM d, yyyy") : ""
			};
		}


		private CellItem [] ParseToCellItems (Account userAccount)
		{
			var textFont = BlrtStyles.CellTitleFont;

			var rtn = new CellItem [] {
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_account_blrt_duration_field", "profile")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.Duration,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_account_max_conversation_users_field", "profile")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.MaxUser,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_account_media_size_field", "profile")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.MediaSize,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_account_resolution_field", "profile")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.ImageResolution,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("Max conversation size")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.MaxConvoSize,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("Max Blrt size")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.MaxBlrtSize,  ProfileMainPageCell.CellTextGrayColor, textFont),
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("Max active pages in Blrt")),
					Text = ProfileMainPageCell.GetFormattedString(userAccount.MaxBlrtPage,  ProfileMainPageCell.CellTextGrayColor, textFont),
				}
			};

			if (userAccount.Name != "Free" && !string.IsNullOrWhiteSpace (userAccount.ExpiryDate)) {
				var list = new List<CellItem> (rtn);
				list.Add (new CellItem {
					Title = ProfileMainPageCell.GetFormattedString (BlrtUtil.PlatformHelper.Translate ("Account expiry date")),
					Text = ProfileMainPageCell.GetFormattedString (userAccount.ExpiryDate, ProfileMainPageCell.CellTextGrayColor, textFont),
				});
				rtn = list.ToArray ();
			}

			return rtn;
		}

		public void RefreshItemSource ()
		{
			_listView.ItemsSource = ParseToCellItems (GetUserAccount ());
			Title = UserAccount.Name;
			if (_upgradeBtn != null)
				_upgradeBtn.Text = DependencyService.Get<IBlrtProfileHelper> ().GetUpgradeButtonText ();
		}


		public class Account
		{
			public string Duration { get; set; }
			public string MaxUser { get; set; }
			public string MediaSize { get; set; }
			public string ImageResolution { get; set; }
			public string MaxConvoSize { get; set; }
			public string MaxBlrtPage { get; set; }
			public string MaxBlrtSize { get; set; }
			public string Name { get; set; }
			public string ExpiryDate { get; set; }
		}

	}
}

