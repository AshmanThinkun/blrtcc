using System;
using Xamarin.Forms;
using BlrtData;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Settings
{
	public class DeviceSettingsPage: BlrtContentPage
	{

		private DeleteDataCell _deleteCell;

		public override string ViewName {
			get {
				return "Settings_TroubleShooting";
			}
		}

		public DeviceSettingsPage ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_device_settings_title", "profile");



			_deleteCell = new DeleteDataCell ();
			_deleteCell.Tapped += OnDeleteData;

			var enableReportCell = new EnableReportCell () {
			};
			enableReportCell.Switch.IsToggled = !Xamarin.Insights.DisableCollection;

			enableReportCell.Switch.Toggled += (sender, e) => {
				Xamarin.Insights.DisableCollection = !enableReportCell.Switch.IsToggled;

				#if __iOS__
				var ds = Foundation.NSUserDefaults.StandardUserDefaults;
				ds.SetValueForKey(new Foundation.NSString(Xamarin.Insights.DisableCollection.ToString()), new Foundation.NSString("DisableCrashReport"));
				ds.Synchronize();
				#elif __ANDROID__
				var activity = BlrtApp.PlatformHelper.GetActivity();
				var preferences = activity.GetSharedPreferences("MyPreferences", Android.Content.FileCreationMode.Private);  
				var editor = preferences.Edit();
				editor.PutString("DisableCrashReport", Xamarin.Insights.DisableCollection.ToString());
				editor.Commit();
				#endif

				if(!Xamarin.Insights.DisableCollection){
					BlrtUserHelper.IdentifyUser();
				}
			};


			var tableView = new FormsTableView () {
				ShowSectionHeader = false,
				VerticalOptions = LayoutOptions.Fill,
				Intent = TableIntent.Data,
				RowHeight = DeleteDataCell.rowHeight,
				BackgroundColor = BlrtStyles.BlrtWhite,
				Root = new TableRoot {
					new TableSection () {
						_deleteCell,
						enableReportCell
					},
				}
			};

			Content = tableView;
		}			

		private async void OnDeleteData (object sender, EventArgs e)
		{
			var result = await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate("device_ts_delete_local_alert_title", "profile"),
				BlrtUtil.PlatformHelper.Translate("device_ts_delete_local_alert_message", "profile"),
				BlrtUtil.PlatformHelper.Translate("device_ts_delete_local_alert_okay", "profile"),
				BlrtUtil.PlatformHelper.Translate("device_ts_delete_local_alert_cancel", "profile")
			);

			if(result){
				BlrtData.Helpers.BlrtTrack.SettingsDeleteLocalData ();
				BlrtManager.Logout (true);
				BlrtManager.App.OpenMailbox (BlrtData.ExecutionPipeline.Executor.System (), BlrtData.Models.Mailbox.MailboxType.Inbox);
				BlrtConstants.DeleteEverything ();
			}
		}
	}
}

