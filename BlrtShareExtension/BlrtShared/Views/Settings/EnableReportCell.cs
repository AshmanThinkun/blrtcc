﻿using System;
using Xamarin.Forms;
using BlrtData;

namespace BlrtData.Views.Settings
{
	public class EnableReportCell : ViewCell
	{
		public const int rowHeight = 85;
		private Label _title, _detail;
		public Switch Switch{ get; set;}
		public EnableReportCell ():base()
		{
			_title = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("Send crash reporting data to help improve the quality of the app"),
				FontSize = 16,
			};

//			_detail = new Label () {
//				Text = BlrtUtil.PlatformHelper.Translate ("device_ts_footer", "profile"),
//				TextColor = BlrtStyles.BlrtGray5,
//				FontSize = 11,
//			};

			Switch = new Switch () {
				
			};

			View = new Grid() {

				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
//					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
				},

				RowSpacing = 10,
				Padding = new Thickness(15,10),


				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{_title,0,0},
//					{_detail,0,1}
					{Switch,1,0}
				}
			};
		}
	}
}

