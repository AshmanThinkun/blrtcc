﻿using System;
using Xamarin.Forms;
using BlrtData.Views.Settings.Profile;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Settings
{
	public class BlockUserPageCell:ViewCell
	{
	

		public BlockUserPageCell ()
		{	



			Label _nameLabel = new Label (){
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			Label _emailLabel = new Label (){
				VerticalOptions = LayoutOptions.CenterAndExpand,
				LineBreakMode = LineBreakMode.CharacterWrap,
				TextColor = Color.Gray,
			};
			Image _image = new Image() {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				Source = "settings_arrow.png",
			};

			_nameLabel.SetBinding (Label.TextProperty, "DisplayName");	
			_emailLabel.SetBinding (Label.FormattedTextProperty, "Email");
			//_emailLabel.SetBinding (Label.TextProperty, "Email");

			View = new Grid() {
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
				},

				RowSpacing = 5,
				ColumnSpacing = 5,
				Padding = new Thickness(15,0),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{ new StackLayout(){
							Orientation = StackOrientation.Vertical,
							HorizontalOptions = LayoutOptions.Start,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							Spacing = 5,
							Children = {
								_nameLabel,
								_emailLabel
							}
						},0,0},
					{
						 new StackLayout(){
							Orientation = StackOrientation.Horizontal,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							HorizontalOptions = LayoutOptions.End,
							Spacing = 10,
							Children = {
								//_stateLabel,
								_image,
								//new Label (){Text = "image"},
							}
						},1,0
					},
				}
			};


//			View = new StackLayout {
//				Padding = 0,
//				Spacing = 0,
//				Children = {
//					nameLabel
//				}
//			};	

		}
	}
}

