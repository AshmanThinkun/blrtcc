using System;
using Xamarin.Forms;
using Parse;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Settings
{
	public class NotificationPage: BlrtContentPage
	{
		private SwitchCell _newBlrtSwitch, _blrtReplySwitch, _newCommentSwitch;

		private bool 	_newBlrtBool;
		private bool 	_reblrtBool;
		private bool 	_commentBool;

		public override string ViewName {
			get {
				return "Settings_Notification";
			}
		}

		public NotificationPage ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_notifications_title", "profile");

			_newBlrtSwitch = new SwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("notification_email_on_blrt_field_new","profile"),
			};

			_blrtReplySwitch = new SwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("notification_email_on_reblrt_field","profile"),
			};

			_newCommentSwitch = new SwitchCell () {
				Text = BlrtUtil.PlatformHelper.Translate ("notification_email_on_comment_field","profile"),
			};

			var titleText = new ViewCell () {
				View = new ContentView () {
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Fill,
					Padding = new Thickness(15,0),
					Content = new Label () {
						VerticalOptions = LayoutOptions.Center,
						Text = BlrtUtil.PlatformHelper.Translate ("notification_email_section_title_new", "profile"),
						FontSize = 16,
						FontAttributes = FontAttributes.Bold
					}
				}
			};

			var tableView = new FormsTableView () {
				ShowSectionHeader = false,
				VerticalOptions = LayoutOptions.Fill,
				Intent = TableIntent.Data,
				BackgroundColor = BlrtStyles.BlrtWhite,
				Root = new TableRoot {
					new TableSection () {
						titleText,
						_newBlrtSwitch,
						_blrtReplySwitch,
						_newCommentSwitch
					},
				}
			};

			Content = tableView;
		}

		public async Task<bool> SavePressed(object sender, EventArgs e)
		{

			if (_newBlrtBool != _newBlrtSwitch.On) {
				ParseUser.CurrentUser [BlrtUserHelper.ALLOW_EMAIL_NEW_BLRT] = _newBlrtSwitch.On;
				BlrtData.Helpers.BlrtTrack.SettingsNotificationsChange ("NewBlrt");
			}
			if (_reblrtBool != _blrtReplySwitch.On) {
				ParseUser.CurrentUser [BlrtUserHelper.ALLOW_EMAIL_REBLRT] = _blrtReplySwitch.On; 
				BlrtData.Helpers.BlrtTrack.SettingsNotificationsChange ("ReBlrt");
			}
			if (_commentBool != _newCommentSwitch.On) {
				ParseUser.CurrentUser [BlrtUserHelper.ALLOW_EMAIL_COMMENTS] = _newCommentSwitch.On; 
				BlrtData.Helpers.BlrtTrack.SettingsNotificationsChange ("Comment");
			}
			try {
				await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
			} catch (Exception ex) {

				var result = await DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("edit_fail_title", "profile"),
					BlrtUtil.PlatformHelper.Translate ("edit_fail_message", "profile"),
					BlrtUtil.PlatformHelper.Translate ("edit_fail_retry", "profile"),
					BlrtUtil.PlatformHelper.Translate ("edit_fail_cancel", "profile")
				);

				if(result){
					SavePressed(this,EventArgs.Empty);
				} else {
					ParseUser.CurrentUser.Revert();
					return false;
				}
			}
			return true;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			_newBlrtSwitch.On = _newBlrtBool = ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_NEW_BLRT);
			_blrtReplySwitch.On = _reblrtBool = ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_REBLRT);
			_newCommentSwitch.On = _commentBool = ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_COMMENTS);
		}

	}
}

