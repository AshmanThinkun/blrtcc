using System;
using Xamarin.Forms;
using BlrtData;
using BlrtData.Views.Settings.Profile;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Settings
{
	public class VersionPage: BlrtContentPage
	{
		public override string ViewName {
			get {
				return "Settings_Version";
			}
		}
		static readonly int rowHeight = 43;
		private ListView _listView;
		public VersionPage ():base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_version_title", "profile");
			_listView = new FormsListView{
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
			};
			RefreshItemSource ();
			_listView.ItemTemplate = new DataTemplate(typeof(ProfileMainPageCell)); 
			_listView.ItemSelected +=  (sender, e) => {
				if(e.SelectedItem==null){
					return;
				}
				_listView.SelectedItem = null;
			};
			Content = _listView;
		}

		public void RefreshItemSource(){
			if (null == _listView)
				return;
			var textFont = Font.SystemFontOfSize (15);

			_listView.ItemsSource = new CellItem[]
			{
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_version_title", "profile"),  ProfileMainPageCell.CellTextBlueColor),
					Text  = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.GetAppVersion(),  ProfileMainPageCell.CellTextGrayColor, textFont),
					NavText = "version",
				},
			};
		}


	}
}

