using System;
using Xamarin.Forms;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using Parse;

namespace BlrtData.Views.Settings.Profile
{
	public class GenderPage: BlrtContentPage
	{
		private ListView _listView;

		public Gender SelectedGender{ get; set;}
		public const string ImageBoudle  = "tag_tick.png";

		public override string ViewName {
			get {
				return "MyProfile_Edit_Gender";
			}
		}

		public GenderPage (): base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("Gender");
			var genderInt = ParseUser.CurrentUser.Get<int> (BlrtUserHelper.UserGenderKey);

			var genders = new List<Gender> ();
			genders.Add (new Gender (){ ParseName = 0, Name = BlrtUtil.PlatformHelper.Translate ("gender_na", "profile") });
			genders.Add (new Gender (){ ParseName = 1, Name = BlrtUtil.PlatformHelper.Translate ("gender_male", "profile") });
			genders.Add (new Gender (){ ParseName = 2, Name = BlrtUtil.PlatformHelper.Translate ("gender_female", "profile") });
			SelectedGender = genders [genderInt];
			SelectedGender.Image = ImageBoudle;

			_listView = new FormsListView () {
				VerticalOptions = LayoutOptions.Fill,
				ItemsSource = genders,
				ItemTemplate = new DataTemplate(typeof(GenderCell)),
			};

			_listView.ItemSelected += (sender, e) => {
				if(e.SelectedItem==null){
					return;
				}
				SelectedGender.Image = null;
				SelectedGender = e.SelectedItem as Gender;
				SelectedGender.Image = ImageBoudle;

				_listView.SelectedItem = null;
			};

			Content = _listView;
		}

		public async System.Threading.Tasks.Task< bool> SavePressed(object sender, EventArgs e){
			var currentUser = ParseUser.CurrentUser;
			currentUser[BlrtUserHelper.UserGenderKey] = SelectedGender.ParseName;
			currentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
			return true;
		}


		public class Gender: BlrtNotifyProperty
		{
			private string _name, _image;

			public string Name{
				get{return _name;}
				set{
					OnPropertyChanged (ref _name, value);
				}
			}
			public string Image{
				get{return _image;}
				set{
					OnPropertyChanged (ref _image, value);
				}
			}

			public int ParseName{get; set;}
		}


		public class GenderCell: IndustryPage.IndustryCell
		{
			public GenderCell():base()
			{
				_nameLabel.SetBinding(Label.TextProperty,"Name");
				_img.SetBinding(Xamarin.Forms.Image.SourceProperty,"Image");
			}
		}
	}
}

