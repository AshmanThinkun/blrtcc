 using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using Parse;
using System.Threading.Tasks;

namespace BlrtData.Views.Settings.Profile
{
	public class PasswordPage : BlrtContentPage
	{
		public MyPasswordEntryCell Current, New, Confirm;
		private TableSection _currentSection;
		private TableView _tableView;

		public override string ViewName {
			get {
				return "MyProfile_Edit_Password";
			}
		}
		public PasswordPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_password_title", "profile");
			Current = new MyPasswordEntryCell {
				Label = BlrtUtil.PlatformHelper.Translate ("edit_password_old_field", "profile"),
			};

			New = new MyPasswordEntryCell {
				Label = BlrtUtil.PlatformHelper.Translate ("edit_password_new_field", "profile"),
			};
			Confirm = new MyPasswordEntryCell {
				Label = BlrtUtil.PlatformHelper.Translate ("edit_password_new_confirm_field", "profile"),
			};

			_currentSection = new TableSection () {
				Current
			};

			Content = _tableView = new TableView {
				Intent = TableIntent.Form,
				Root = new TableRoot () {

					new TableSection () {
						New,
						Confirm
					}
				}
			};

			//if(BlrtUserHelper.HasSetPassword){
			if (BlrtUserHelper.HasSetPassword) {
				_tableView.Root.Insert (0, _currentSection);
			}
		}


		public class MyPasswordEntryCell : ViewCell
		{
			public const int horizentalPadding = 0;
			private Entry _entry;
			public string Text {
				get {
					return _entry.Text;
				}
				set {
					_entry.Text = value;
				}
			}
			public string Label {
				get {
					return _entry.Placeholder;
				}
				set {
					_entry.Placeholder = value;
				}
			}
			public MyPasswordEntryCell ()
			{
				_entry = new FormsEntry () {
					IsPassword = true,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					ContentEdgeInsets = new Thickness (15, 0),
					FontSize = (float)BlrtStyles.CellTitleFont.FontSize,
					HideBottomDrawable = true
				};

				View = new StackLayout () {
					Padding = new Thickness (horizentalPadding, 0),
					Orientation = StackOrientation.Horizontal,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Children = {
						_entry,
					}
				};

				if (Device.OS == TargetPlatform.Android) {
					_entry.VerticalOptions = LayoutOptions.CenterAndExpand;
					View.BackgroundColor = BlrtStyles.BlrtWhite;
				}
			}
		}

		enum PasswordCheckResult
		{
			OldPasswordInvalid,
			NewPasswordInvalid,
			NewPasswordMismatch,
			OK
		}

		async Task<PasswordCheckResult> IsEntryValid ()
		{
			if (_tableView.Root.Contains (_currentSection)) {
				// user has set password
				if (string.IsNullOrEmpty (Current.Text)) {
					return PasswordCheckResult.OldPasswordInvalid;
				}
				if (!await BlrtUserHelper.CheckPassword (Current.Text)) {
					return PasswordCheckResult.OldPasswordInvalid;
				}
			}
			if (null == New.Text || New.Text.Length < BlrtConstants.MinPasswordLength) {
				return PasswordCheckResult.NewPasswordInvalid;
			}
			if (New.Text != Confirm.Text) {
				return PasswordCheckResult.NewPasswordMismatch;
			}
			return PasswordCheckResult.OK;
		}

		public async Task<bool> SavePasswordAsync ()
		{
			var result = await IsEntryValid ();
			switch (result) {
				case PasswordCheckResult.OldPasswordInvalid:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("edit_profile_password_check_again_title", "profile"),
						BlrtUtil.PlatformHelper.Translate ("edit_profile_password_check_again_message", "profile"),
						"OK"
					);
					return false;
				case PasswordCheckResult.NewPasswordInvalid:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("edit_password_length_title", "profile"),
						string.Format (BlrtUtil.PlatformHelper.Translate ("edit_password_length_message", "profile"), BlrtConstants.MinPasswordLength),
						"OK");
					return false;
				case PasswordCheckResult.NewPasswordMismatch:
					await DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("edit_password_match_error_title", "profile"),
						BlrtUtil.PlatformHelper.Translate ("edit_password_match_error_message", "profile"),
						"OK"
					);
					return false;
				case PasswordCheckResult.OK:
					ParseUser.CurrentUser.Password = New.Text;
					ParseUser.CurrentUser [BlrtData.BlrtUserHelper.HasSetPasswordKey] = true;
					await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
					return true;
			}
			return true;
		}
	}
}

