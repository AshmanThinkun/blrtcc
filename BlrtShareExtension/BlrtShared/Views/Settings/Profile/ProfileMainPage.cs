using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using BlrtData.Views.Helpers;
using System.Linq;
using Parse;
using System.Threading.Tasks;

namespace BlrtData.Views.Settings.Profile
{
	public class ProfileMainPage : BlrtContentPage
	{
		static readonly int rowHeight = 43;

		public EventHandler<SelectedItemChangedEventArgs> RowSelected;

		public UserProfile Profile { get; set; }

		private ListView _listView;

		public override string ViewName {
			get {
				return "MyProfile_Edit";
			}
		}

		public ProfileMainPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_profile_title", "profile");

			_listView = new FormsListView {
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight
#if __ANDROID__
				, BackgroundColor = BlrtStyles.BlrtWhite
#endif
			};

			_listView.ItemsSource = ParseToCellItems (GetUserProfile ());
			_listView.ItemTemplate = new DataTemplate (typeof (ProfileMainPageCell));


			_listView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null) {
					return;
				}
				_listView.SelectedItem = null;
				if (RowSelected != null)
					RowSelected (sender, e);

				//this code can be shared, we don't want to put it into platform specific class
				if ((e.SelectedItem as CellItem).NavText == NavText.DeleteAccount.ToString ()) {
					DeleteAccount ().FireAndForget ();
				}
			};

			Content = _listView;
		}

		async Task DeleteAccount (bool passwordCorrect = false)
		{
			var delete = await DisplayAlert (
				BlrtUtil.PlatformHelper.Translate ("account_status_user_delete_title", "profile"),
				BlrtUtil.PlatformHelper.Translate ("account_status_user_delete_message", "profile"),
				BlrtUtil.PlatformHelper.Translate ("account_status_user_delete_accept", "profile"),
				BlrtUtil.PlatformHelper.Translate ("account_status_user_delete_cancel", "profile")
			);

			if (delete) {
				//check password, this method considers the has set password param
				var profileHelper = Xamarin.Forms.DependencyService.Get<IBlrtProfileHelper> ();

				var hasSetPasswordBefore = BlrtUserHelper.HasSetPassword;

				if (!passwordCorrect) {
#if __IOS__
					passwordCorrect = await profileHelper.CheckPassword (BlrtManager.Responder);
#else
					passwordCorrect = await profileHelper.CheckPassword (this);
#endif
				}

				if (passwordCorrect) {
					if (hasSetPasswordBefore) {
						var apiCall = new BlrtAPI.APIUserChangeAccountStatus (
										 new BlrtAPI.APIUserChangeAccountStatus.Parameters ()
									 );

						if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
							BlrtData.Helpers.BlrtTrack.AccountDelete ();

						}
					} else {
						await DeleteAccount (true);
					}
				}
			}
		}

		bool _appeared;
		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			if (_appeared)
				RefreshItemSource ();
			_appeared = true;
		}

		private UserProfile GetUserProfile ()
		{
			var user = ParseUser.CurrentUser;

			var gender = user.Get<int> (BlrtUserHelper.UserGenderKey, 0);
			string genderStr = null;
			if (gender == 1) {
				genderStr = "Male";
			} else if (gender == 2) {
				genderStr = "Female";
			}

			var industry = user.Get<string> (BlrtUserHelper.IndustryKey, "");
			if (string.IsNullOrWhiteSpace (industry) || industry.IndexOf (BlrtConstants.IndustriesOther) == 0) {
				try {
					industry = industry.Substring (BlrtConstants.IndustriesOther.Length + 1);
					if (string.IsNullOrEmpty (industry))
						throw new Exception ("other");
				} catch {
					industry = BlrtUtil.PlatformHelper.Translate (BlrtConstants.IndustriesOther, "industry");
				}
			} else
				industry = BlrtUtil.PlatformHelper.Translate (industry, "industry");

			return Profile = new UserProfile () {
				DisplayName = user.Get<string> (BlrtUserHelper.DisplayNameKey, ""),
				FirstName = user.Get<string> (BlrtUserHelper.FirstNameKey, ""),
				LastName = user.Get<string> (BlrtUserHelper.LastNameKey, ""),
				Gender = genderStr,
				Organization = user.Get<string> (BlrtUserHelper.ORGANISATION_NAME, ""),
				Industry = industry,
				Phone = BlrtUserHelper.PhoneNumber,
				Emails = string.Join (", ", (new string [] { ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, "") }).Concat (BlrtUserHelper.EmailAddresses.Select (v => v.Value)).Distinct ())
			};
		}


		private CellItem [] ParseToCellItems (UserProfile profile)
		{
			var textFont = BlrtStyles.CellTitleFont;

			return new CellItem [] {
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("edit_profile_display_name_field_new", "profile")),
					Text = ProfileMainPageCell.GetFormattedString(profile.DisplayName,  ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = "settings_arrow.png",
					NavText = NavText.DisplayName.ToString()
					},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("Email address"+"              ")),
					Text = ProfileMainPageCell.GetFormattedString(profile.Emails, ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = "settings_arrow.png",
					NavText = NavText.Email.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("Phone")),
					Text = String.IsNullOrEmpty(profile.Phone) ? ProfileMainPageCell.GetFormattedString("Add",  ProfileMainPageCell.CellTextBlueColor, textFont): ProfileMainPageCell.GetFormattedString(profile.Phone,  ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = String.IsNullOrEmpty(profile.Phone) ? null : "settings_arrow.png",
					NavText = NavText.Phone.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_profile_gender_field", "profile")),
					Text = String.IsNullOrEmpty(profile.Gender) ? ProfileMainPageCell.GetFormattedString("Add",  ProfileMainPageCell.CellTextBlueColor, textFont): ProfileMainPageCell.GetFormattedString(profile.Gender,  ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = String.IsNullOrEmpty(profile.Gender) ? null : "settings_arrow.png",
					NavText = NavText.Gender.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_profile_organisation_field", "profile")),
					Text = String.IsNullOrEmpty(profile.Organization) ? ProfileMainPageCell.GetFormattedString("Add",  ProfileMainPageCell.CellTextBlueColor, textFont): ProfileMainPageCell.GetFormattedString(profile.Organization,  ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = String.IsNullOrEmpty(profile.Organization) ? null : "settings_arrow.png",
					NavText = NavText.Organization.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_profile_industry_field", "profile")),
					Text = String.IsNullOrEmpty(profile.Industry) ? ProfileMainPageCell.GetFormattedString("Add",  ProfileMainPageCell.CellTextBlueColor, textFont): ProfileMainPageCell.GetFormattedString(profile.Industry,  ProfileMainPageCell.CellTextGrayColor, textFont),
					Image = String.IsNullOrEmpty(profile.Industry) ? null : "settings_arrow.png",
					NavText = NavText.Industry.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(BlrtUtil.PlatformHelper.Translate ("profile_profile_password_field", "profile")),
					Text = ProfileMainPageCell.GetFormattedString("Change",  ProfileMainPageCell.CellTextBlueColor, textFont),
					NavText = NavText.Password.ToString()
				},
				new CellItem {
					Title = ProfileMainPageCell.GetFormattedString(
						BlrtUtil.PlatformHelper.Translate("account_status_user_delete_title", "profile"),
						BlrtStyles.BlrtAccentRed
					),
					NavText = NavText.DeleteAccount.ToString()
				}
			};
		}

		public void RefreshItemSource ()
		{
			_listView.ItemsSource = ParseToCellItems (GetUserProfile ());
		}

		public enum NavText
		{
			DisplayName = 0,
			FirstName = 1,
			LastName = 2,
			Gender = 3,
			Email = 4,
			Industry = 5,
			Organization = 6,
			DeleteAccount = 7,
			Password = 8,
			Phone = 9
		}

		public class UserProfile
		{
			public string DisplayName { get; set; }
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string Gender { get; set; }
			public string Organization { get; set; }
			public string Industry { get; set; }
			public string Password { get; set; }
			public string Emails { get; set; }
			public string Phone { get; set; }
		}

	}
}

