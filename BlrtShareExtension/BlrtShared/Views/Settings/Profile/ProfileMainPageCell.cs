using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;

namespace BlrtData.Views.Settings.Profile
{
	public class ProfileMainPageCell : ViewCell
	{
		public static readonly BindableProperty CellAccessoryImageProperty =
			BindableProperty.Create<ProfileMainPageCell, ImageSource> (cell => cell.CellAccessoryImage, null);

		public ImageSource CellAccessoryImage { get { return (ImageSource)GetValue (CellAccessoryImageProperty); } set { SetValue (CellAccessoryImageProperty, value); } }

		public static readonly Color CellTextGrayColor = BlrtStyles.BlrtGray6;
		public static readonly Color CellTextBlueColor = BlrtStyles.BlrtAccentBlue;

		Image _image;
		Label _titleLabel, _textLabel;
		Switch _switch;


		public ProfileMainPageCell () : base ()
		{
			_image = new Image () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				IsVisible = false
			};

			_titleLabel = new Label () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
			};
			_textLabel = new Label () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			};
			_textLabel.LineBreakMode = LineBreakMode.TailTruncation;

			_switch = new Switch (){
				IsVisible = false,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End
			};

			_switch.Toggled += (sender, e) => {
				if(!_switch.IsVisible || this.BindingContext==null)
					return;

				var bind = this.BindingContext as CellItem;
				if(bind==null || bind.SwitchCellAction == null)
					return;
				bind.SwitchCellAction.Invoke(_switch.IsToggled);
			};

			View = new Grid () {


				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},

				RowSpacing = 0,
				ColumnSpacing = 5,
				Padding = new Thickness (15, 0),

				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{ _titleLabel,0,0 }, {
						new StackLayout () {
							Orientation = StackOrientation.Horizontal,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							HorizontalOptions = LayoutOptions.End,
							Spacing = 10,
							Children = {
								_textLabel,
								_switch,
								_image,
							}
						},1,0
					}
				}
			};


			_titleLabel.SetBinding (Label.FormattedTextProperty, "Title");
			_textLabel.SetBinding (Label.FormattedTextProperty, "Text");
			_switch.SetBinding (Switch.IsToggledProperty, "SwitchOn");
			_switch.SetBinding (Switch.IsVisibleProperty, "ShowSwitch");
			this.SetBinding (ProfileMainPageCell.CellAccessoryImageProperty, "Image");
		}

		public static FormattedString GetFormattedString (string text)
		{
			var fs = new FormattedString ();
			fs.Spans.Add (
				new Span { Text = text }
			);

			return fs;
		}

		public static FormattedString GetFormattedString (string text, Color color)
		{
			var fs = new FormattedString ();
			fs.Spans.Add (
				new Span { Text = text,  ForegroundColor = color }
			);

			return fs;
		}

		public static FormattedString GetFormattedString (string text, Color color, Font font)
		{
			var fs = new FormattedString ();
			fs.Spans.Add (
				new Span { 
					Text = text, 
					ForegroundColor = color, 
					FontFamily = font.FontFamily, 
					FontSize = font.FontSize,
					FontAttributes = font.FontAttributes 
				}
			);
			return fs;
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == CellAccessoryImageProperty.PropertyName) {
			
				if (CellAccessoryImage == null) {
					_image.IsVisible = false;
				} else {
					_image.IsVisible = true;
					_image.Source = CellAccessoryImage;

				}
			}
		}
	}

	public class CellItem
	{
		public FormattedString Title{ get; set; }

		public FormattedString Text{ get; set; }

		public string Image{ get; set; }

		public string NavText{ get; set; }

		public bool ShowSwitch{get;set;}

		public bool SwitchOn{ get; set;}

		public Action<bool> SwitchCellAction{get;set;}
	}
}

