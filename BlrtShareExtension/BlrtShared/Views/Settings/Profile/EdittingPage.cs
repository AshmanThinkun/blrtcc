using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Linq;
using Parse;

namespace BlrtData.Views.Settings.Profile
{
	public class EdittingPage: BlrtContentPage
	{
		public Keyboard EntryKeyboard{
			get{
				return _entry.Keyboard;
			}
			set{
				_entry.Keyboard = value;
			}
		}
		public bool DisableAutoCap{
			get{
				return _entry.DisableAutoCap;
			}
			set{
				_entry.DisableAutoCap = value;
			}
		}

		public bool EntryIsPassword{
			get{
				return _entry.IsPassword;
			}
			set{
				_entry.IsPassword = value;
			}
		}

		public override string ViewName {
			get {
				try{
					var titlewords = this.Title.Split (new string[]{" "}, StringSplitOptions.RemoveEmptyEntries);
					var title = string.Join("", titlewords.Select(x=>{
						return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(x.ToLower());
					}));
					return "MyProfile_Edit_" + title;
				}catch{
					return null;
				}
			}
		}

		private FormsEntry _entry;
		public EdittingPage (string title, string placeholder, string text):base()
		{
			Title = title;
			_entry = new FormsEntry () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				HeightRequest = 43,
				Placeholder = placeholder,
				Text = text,
				BackgroundColor = BlrtStyles.BlrtWhite,
				ContentEdgeInsets = new Thickness(15,0),
				FontSize = (float)BlrtStyles.CellTitleFont.FontSize
			};
				

			Content = new StackLayout {
				Padding = new Thickness (0, 20),
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = BlrtStyles.BlrtGray2,
				Children = {
					_entry,
				},
			};
		}

		public string EdittingString{get{return _entry.Text;}}


		public void SavePressed (string field, string value, string alertTitle, string alertMessage){
			var currentUser = ParseUser.CurrentUser;
			if(string.IsNullOrWhiteSpace (value)){
				DisplayAlert (alertTitle,alertMessage,"OK");
			}else{
				currentUser[field] = value.Trim ();
				currentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
			}
		}
	}
}

