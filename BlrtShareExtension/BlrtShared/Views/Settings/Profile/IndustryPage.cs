using System;
using Xamarin.Forms;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using Parse;
using System.Threading.Tasks;

namespace BlrtData.Views.Settings.Profile
{
	public class IndustryPage : BlrtContentPage
	{
		private Entry _otherEntry;
		const int rowHeight = 44;
		public IndustryCell SelectedIndustry { get; set; }
		public const string ImageBoudle = "tag_tick.png";
		public string OtherName { get { return _otherEntry.Text; } }

		public override string ViewName {
			get {
				return "MyProfile_Edit_Industry";
			}
		}

		public IndustryPage () : base ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("profile_industry_title", "profile");
			var industryString = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.IndustryKey, "");
			if (string.IsNullOrWhiteSpace (industryString)) {
				industryString = BlrtConstants.IndustriesOther + ":";
			}


			var industries = new List<Cell> ();
			IndustryCell itIndus;
			foreach (var name in BlrtConstants.IndustryOptions) {
				industries.Add (itIndus = new IndustryCell () { Name = name });
				if (industryString == name) {
					itIndus.Image = ImageBoudle;
					SelectedIndustry = itIndus;
				}
				itIndus.Selected += RowSelected;
			}
			industries.Add (itIndus = new IndustryCell () { Name = BlrtConstants.IndustriesOther });
			if (industryString.IndexOf (BlrtConstants.IndustriesOther) == 0) {
				itIndus.Image = ImageBoudle;
				SelectedIndustry = itIndus;
			}
			itIndus.Selected += RowSelected;

			_otherEntry = new Entry () {
				HorizontalOptions = LayoutOptions.Fill,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("industry_other", "industry"),
			};
			_otherEntry.Focused += (sender, e) => {
				SelectedIndustry.Image = null;
				itIndus.Image = ImageBoudle;
				SelectedIndustry = itIndus;
			};

			if (SelectedIndustry.ParseName == BlrtConstants.IndustriesOther) {
				_otherEntry.Text = industryString.Remove (0, BlrtConstants.IndustriesOther.Length + 1);
			}


			var _industrySec = new TableSection ();
			_industrySec.Add (industries);

			var otherCell = new ViewCell () {
				Height = 60,
				View = new ContentView () {
					Padding = new Thickness (15, 10),
					Content = _otherEntry
				},
			};
			_industrySec.Add (otherCell);

			var _tableView = new FormsTableView () {
				ShowSectionHeader = false,
				HasUnevenRows = true,
				RowHeight = rowHeight,
				VerticalOptions = LayoutOptions.Fill,
				Intent = TableIntent.Data,
				BackgroundColor = BlrtStyles.BlrtWhite,
				Root = new TableRoot {
					_industrySec,
				}
			};

			Content = _tableView;
		}


		void RowSelected (object sender, EventArgs e)
		{
			SelectedIndustry.Image = null;
			if (sender is IndustryCell) {
				SelectedIndustry = sender as IndustryCell;
				SelectedIndustry.Image = ImageBoudle;
			}

			if (_otherEntry != null)
				_otherEntry.Unfocus ();
		}

		public async Task<bool> SaveIndustryAsync ()
		{
			string industry = SelectedIndustry.ParseName;
			if (SelectedIndustry.ParseName == BlrtConstants.IndustriesOther) {
				industry = BlrtConstants.IndustriesOther + ":" + OtherName;
			}

			var currentUser = ParseUser.CurrentUser;
			currentUser [BlrtUserHelper.IndustryKey] = industry.Trim ();
			await currentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
			return true;
		}


		public class IndustryCell : ViewCell
		{

			private string _name, _image;

			protected Label _nameLabel;
			protected Image _img;

			public string Name {
				get { return _name; }
				set {
					ParseName = value;
					_name = BlrtUtil.PlatformHelper.Translate (value, "industry");
					_nameLabel.Text = _name;
				}
			}
			public string Image {
				get { return _image; }
				set {
					_image = value;
					_img.Source = _image;
				}
			}

			public string ParseName { get; private set; }

			public event EventHandler Selected;
			public IndustryCell () : base ()
			{
				_nameLabel = new Label () {
					VerticalOptions = LayoutOptions.CenterAndExpand,
				};


				_img = new Image () {
					HorizontalOptions = LayoutOptions.EndAndExpand,
				};


				this.Tapped += (object sender, EventArgs e) => {
					if (Selected != null)
						Selected (this, e);
				};

				Height = rowHeight;
				View = new Grid () {

					ColumnDefinitions = {
						new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
						new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
					},
					RowDefinitions = {
						new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
					},

					RowSpacing = 0,
					ColumnSpacing = 5,
					Padding = new Thickness (15, 0),

					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.Fill,
					Children = {
						{ _nameLabel,0,0},
						{
							_img,1,0
						}
					}
				};
			}
		}
	}
}

