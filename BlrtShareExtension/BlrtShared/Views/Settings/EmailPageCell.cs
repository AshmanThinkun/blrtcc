using System;
using Xamarin.Forms;
using BlrtData.Views.Settings.Profile;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Settings
{
	public class EmailPageCell: ViewCell
	{
		public static readonly BindableProperty CellAccessoryImageProperty =
			BindableProperty.Create<ProfileMainPageCell, ImageSource> (cell => cell.CellAccessoryImage, null);

		public ImageSource CellAccessoryImage { get { return (ImageSource)GetValue (CellAccessoryImageProperty); } set { SetValue (CellAccessoryImageProperty, value); } }

		Image _image;
		Label _titleLabel, _stateLabel, _emailLabel;

		public EmailPageCell () : base ()
		{
			_image = new Image () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				IsVisible = false
			};

			_titleLabel = new Label () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
			};
			_stateLabel = new Label () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				LineBreakMode = LineBreakMode.NoWrap,
			};
			_emailLabel = new Label () {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
				LineBreakMode = LineBreakMode.CharacterWrap,
			};

			StackLayout stacklayout = null;
			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},

				RowSpacing = 5,
				ColumnSpacing = 5,
				Padding = new Thickness (15, 0),



				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { { new StackLayout () {
							Orientation = StackOrientation.Vertical,
							HorizontalOptions = LayoutOptions.Start,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							Spacing = 5,
							Children = {
								_titleLabel,
								_emailLabel
							}
						},0,0
					}, {
						new StackLayout () {
							Orientation = StackOrientation.Horizontal,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							HorizontalOptions = LayoutOptions.End,
							Spacing = 10,
							Children = {
								_stateLabel,
								_image,
							}
						},1,0
					},
				}
			};


			_titleLabel.SetBinding (Label.FormattedTextProperty, "Title");
			_stateLabel.SetBinding (Label.FormattedTextProperty, "Text");
			_emailLabel.SetBinding (Label.FormattedTextProperty, "Email");
			this.SetBinding (EmailPageCell.CellAccessoryImageProperty, "Image");
		}

		protected override void OnPropertyChanged (string propertyName)
		{
			base.OnPropertyChanged (propertyName);

			if (propertyName == CellAccessoryImageProperty.PropertyName) {

				if (CellAccessoryImage == null) {
					_image.IsVisible = false;
				} else {
					_image.IsVisible = true;
					_image.Source = CellAccessoryImage;

				}
			}
		}

	}
}

