using System;
using Xamarin.Forms;
using BlrtData;

namespace BlrtData.Views.Settings
{
	public class DeleteDataCell : ViewCell
	{
		public const int rowHeight = 85;
		private Label _title, _detail;
		public DeleteDataCell ():base()
		{
			_title = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("device_ts_delete_local_button", "profile"),
				FontSize = 16,
			};

			_detail = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("device_ts_footer", "profile"),
				TextColor = BlrtStyles.BlrtGray5,
				FontSize = 11,
			};

			View = new Grid() {

				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
				},

				RowSpacing = 10,
				Padding = new Thickness(15,10),


				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				Children = { 
					{_title,0,0},
					{_detail,0,1}
				}
			};
		}
	}
}

