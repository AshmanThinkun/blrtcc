using System;
using Xamarin.Forms;
using BlrtData.Views.CustomUI;
using System.Threading.Tasks;
using Acr.UserDialogs;
using System.Windows.Input;
using BlrtData.ExecutionPipeline;

namespace BlrtData.Views.Intro
{
	public class IntroPage : BlrtContentPage
	{
		public event EventHandler StartClicked;
		private const double DefaultPageWidth = 320d;
		private const double TextAreaHeight = 155d;
		private const double ImageSize = 250d;
		private const int TotalPageCount = 5;

		private RelativeLayout[] _pages;
		private Label[] _titles;
		private Label[] _messages;
		private StackLayout _pageContainer;
		private int _currentPage;
		ExtendedScrollView _carouselView;
		private double _pageWidth;
		private BlrtPageControl _pageControl;
		DateTime _dragStartTime;
		Point _dragStartPosition;
		int _dragStartPage;
		Button _startBtn;
		IPlatformHelper _platformHelper;
		BlrtImageButton _prePage;
		BlrtImageButton _nextPage;

		public IntroPage ():base()
		{
			BlrtUserHelper.SetTimeStamp ();
			NavigationPage.SetHasNavigationBar (this, false);
			Padding = 0;
			BackgroundColor = BlrtStyles.BlrtGreen;
			_pageWidth = DefaultPageWidth;
			_platformHelper = DependencyService.Get<IPlatformHelper> ();
			_startBtn = new Button {
				Text = _platformHelper.Translate ("intro_button_start", "intro"),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				TextColor = BlrtStyles.BlrtBlackBlack,
				BorderColor = BlrtStyles.BlrtBlackBlack,
				BackgroundColor = Color.Transparent,
#if __ANDROID__
				Margin = new Thickness(0, -2, 0, 0),
#endif
				FontSize = 15,
				BorderWidth = 1,
				WidthRequest = 100,
				HeightRequest = 40,
				BorderRadius = 3
			};
			_prePage = new BlrtImageButton {
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center,
				ImageSource = ImageSource.FromFile ("canvas_page_prev.png"),
				TintColor = BlrtStyles.BlrtCharcoal,
				Opacity = 0
			};
			_nextPage = new BlrtImageButton {
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center,
				ImageSource = ImageSource.FromFile ("canvas_page_next.png"),
				TintColor = Color.Black
			};

			CreatePages ();
			_carouselView = new ExtendedScrollView () {
				Padding = 0,
				AnimateScroll = true,
				Orientation = ScrollOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Content = _pageContainer
			};
			_pageControl = new BlrtPageControl (TotalPageCount) {
				Padding = 0,
				PageIndicatorTintColor = BlrtStyles.BlrtWhite,
				CurrentPageIndicatorTintColor = BlrtStyles.BlrtCharcoal,
				VerticalOptions = LayoutOptions.Center,
				IndicatorRadius = 6
			};
			var carouselArea = new Grid () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = BlrtStyles.BlrtGreen,
				RowSpacing = 0,
				ColumnSpacing = 0,
				#if __ANDROID__
				Padding = new Thickness(0, 20, 0, 0),
				#elif __IOS__
				Padding = 0,
				#endif
				ColumnDefinitions = new ColumnDefinitionCollection () {
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
				},
				RowDefinitions = new RowDefinitionCollection () {
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) },
					new RowDefinition () { Height = new GridLength (50, GridUnitType.Absolute) }
				},
				Children = {
					{ _carouselView , 0, 1, 0, 1}, 
					{ _pageControl, 0, 1, 1, 2 }
				}
			};

			var textArea = CreateTextArea ();
			_currentPage = 0;
			Content = new StackLayout () {
				Padding = 0,
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Children = { carouselArea, textArea }
			};

			_carouselView.DraggingStarted += OnDraggingStarted;
			_carouselView.DraggingEnded += OnDraggingEnded;
			_carouselView.Scrolled += OnScrolled;
			_startBtn.Clicked += (sender, e) => {
				if (null != StartClicked) {
					StartClicked (this, EventArgs.Empty);
				}
			};
			_nextPage.Clicked += (sender, e) => {
				CurrentPage++;
			};
			_prePage.Clicked += (sender, e) => {
				CurrentPage--;
			};
		}


		public override string ViewName {
			get{
				return "Login_Intro" + (CurrentPage+1);
			}
		}

		void CreatePages ()
		{
			_pageContainer = new StackLayout () {
				VerticalOptions = LayoutOptions.Fill,
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Padding = 0
			};
			_pages = new RelativeLayout [TotalPageCount];
			string imgFilrFmt = null;
			if (Device.Idiom == TargetIdiom.Phone) {
				imgFilrFmt = "intro_screen_{0}.png";
			} else {
				imgFilrFmt = "large_intro_screen_{0}.png";
			}
			for (int i = 0; i < _pages.Length; i++) {
				_pages [i] = new RelativeLayout () {
					Padding = 0,
					VerticalOptions = LayoutOptions.Fill,
					WidthRequest = DefaultPageWidth
				};
				var grid = new Grid () {
					Padding = 0,
					RowSpacing = 0,
					ColumnSpacing = 0,
					WidthRequest = ImageSize,
					HeightRequest = ImageSize,
					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Center,
					ColumnDefinitions = new ColumnDefinitionCollection {
						new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) }
					},
					RowDefinitions = new RowDefinitionCollection {
						new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) }
					},
					Children = { {
							new RoundedBoxView () {
								Color = BlrtStyles.BlrtWhite,
								VerticalOptions = LayoutOptions.Fill,
								HorizontalOptions = LayoutOptions.Fill,
								CornerRadius = 20
							}, 0, 0
						}, {
							new Image () {
								VerticalOptions = LayoutOptions.Fill,
								HorizontalOptions = LayoutOptions.Fill,
								Source = string.Format (imgFilrFmt, (i + 1))
							}, 0, 0
						}
					}
				};
				_pages [i].Children.Add (
					grid,
					Constraint.RelativeToParent ((parent) => {
						var size = Math.Min (parent.Height, _pageWidth * 0.7f);
						return (_pageWidth - size) * 0.5f;
					}),
					Constraint.RelativeToParent ((parent) => {
						var size = Math.Min (parent.Height, _pageWidth * 0.7f);
						return (parent.Height - size) * 0.5f;
					}),
					Constraint.RelativeToParent ((parent) => {
						var size = Math.Min (parent.Height, _pageWidth * 0.7f);
						return size;
					}),
					Constraint.RelativeToParent ((parent) => {
						var size = Math.Min (parent.Height, _pageWidth * 0.7f);
						return size;
					})
				);
				if (0 == i || 4 == i) {
					grid.GestureRecognizers.Add (new TapGestureRecognizer () {
						Command = new OpenIntroBlrtCommand (i, this)
					});
				}
				_pageContainer.Children.Add (_pages [i]);
			}
		}

		protected override void OnAppearing ()
		{
			ChangePage (0);
			base.OnAppearing ();

			Task.Run (async () => {
				try {
					await IntroBlrtSettingManager.GetAsync ();
				} catch {}
			});
		}

		Grid CreateTextArea() 
		{
			var grid = new Grid () {
				BackgroundColor = BlrtStyles.BlrtWhite,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.End,
				Padding = new Thickness (20, 10, 20, 30),
				HeightRequest = TextAreaHeight - 10,
				RowSpacing = 0,
				ColumnSpacing = 0,
				RowDefinitions = new RowDefinitionCollection {
					new RowDefinition () { Height = new GridLength (30, GridUnitType.Absolute) },
					new RowDefinition () { Height = new GridLength (10, GridUnitType.Absolute) },
					new RowDefinition () { Height = new GridLength (1, GridUnitType.Star) },
					new RowDefinition () { Height = new GridLength (40, GridUnitType.Absolute) }
				},
				ColumnDefinitions = new ColumnDefinitionCollection {
					new ColumnDefinition () { Width = GridLength.Auto },
					new ColumnDefinition () { Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition () { Width = GridLength.Auto }
				}
			};

			_titles = new Label [TotalPageCount];
			_messages = new Label [TotalPageCount];
			for (int i = 0; i < TotalPageCount; i++) {
				_titles [i] = new Label () {
					FontAttributes = FontAttributes.Bold,
					Text = _platformHelper.Translate ("intro_screen_title_" + (i + 1), "intro"),
					FontSize = 15,
					Opacity = 0,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Center
				};
				_messages [i] = new Label () {
					Text = _platformHelper.Translate ("intro_screen_text_" + (i + 1), "intro"),
					Opacity = 0,
					FontSize = 13,
					XAlign = TextAlignment.Center,
					YAlign = TextAlignment.Start
				};
				grid.Children.Add (_titles [i], 0, 3, 0, 1);
				grid.Children.Add (_messages [i], 0, 3, 2, 3);
			}
			_titles [0].Opacity = 1;
			_messages [0].Opacity = 1;
			grid.Children.Add (_startBtn, 1, 2, 3, 4);
			grid.Children.Add (_prePage, 0, 1, 3, 4);
			grid.Children.Add (_nextPage, 2, 3, 3, 4);
			return grid;
		}

		public int CurrentPage {
			get { 
				return _currentPage; 
			}

			set {
				ChangePage (value);
			}
		}

		void ChangePage (int toPage) {
			if (toPage < 0) {
				toPage = 0;
			} else if (toPage >= TotalPageCount) {
				toPage = TotalPageCount - 1;
			}
			_carouselView.Position = new Point (_pageWidth * toPage, 0);
		}

		void OnDraggingStarted (object sender, EventArgs e)
		{
			_dragStartTime = DateTime.Now;
			_dragStartPosition = _carouselView.Position;
			_dragStartPage = CurrentPage;
		}

		void OnDraggingEnded (object sender, EventArgs e)
		{
			if ((DateTime.Now - _dragStartTime).TotalSeconds < 2) {
				var delta = _carouselView.Position.X - _dragStartPosition.X;
				if (delta > 15) {
					CurrentPage = _dragStartPage + 1;
				} else if (delta < -15) {
					CurrentPage = _dragStartPage - 1;
				} else {
					ChangePage (CurrentPage);
				}
			} else {
				CurrentPage = GetPageNumFromPosition(_carouselView.Position.X);
			}
		}

		string lastTrackedScreenView;
		void OnScrolled (ScrollView view, Rectangle viewPort)
		{
			var nowPage = GetPageNumFromPosition(viewPort.X);
			if (nowPage != _currentPage) {
				_titles [_currentPage].FadeTo (0);
				_messages [_currentPage].FadeTo (0);
				if (_currentPage == TotalPageCount - 1) {
					_startBtn.TextColor = BlrtStyles.BlrtBlackBlack;
					_startBtn.BorderColor = BlrtStyles.BlrtBlackBlack;
					_startBtn.BackgroundColor = Color.Transparent;
					_nextPage.FadeTo (1);
				} else if (nowPage == TotalPageCount - 1) {
					_startBtn.TextColor = BlrtStyles.BlrtCharcoal;
					_startBtn.BorderColor = BlrtStyles.BlrtGreen;
					_startBtn.BackgroundColor = BlrtStyles.BlrtGreen;
					_nextPage.FadeTo (0);
				} else if (_currentPage == 0) {
					_prePage.FadeTo (1);
				} else if (nowPage == 0) {
					_prePage.FadeTo (0);
				}
				_currentPage = nowPage;
				_pageControl.CurrentPage = _currentPage;
				// todo change text and button
				_titles [nowPage].FadeTo (1);
				_messages [nowPage].FadeTo (1);
			}
			if (lastTrackedScreenView != ViewName) {
				lastTrackedScreenView = ViewName;
				TrackPageView ();
			}
		}

		int GetPageNumFromPosition (double x)
		{
			var num = (int)(x / _pageWidth + 0.5d);
			if (num < 0) {
				num = 0;
			} else if (num >= TotalPageCount) {
				num = TotalPageCount - 1;
			}
			return num;
		}

		protected override void OnSizeAllocated (double width, double height)
		{
			_pageWidth = width;
			if (null != _pages) {
				foreach (var item in _pages) {
					item.WidthRequest = width;
				}
			}
			base.OnSizeAllocated (width, height);
		}

		class OpenIntroBlrtCommand : ICommand
		{
			int _page;
			IntroPage _parent;
			public OpenIntroBlrtCommand (int page, IntroPage parent)
			{
				_page = page;
				_parent = parent;
			}
			#region ICommand implementation
			public event EventHandler CanExecuteChanged;
			public bool CanExecute (object parameter)
			{
				return true;
			}
			public async void Execute (object parameter)
			{
				IntroBlrtSettingManager.Settings setting = null;
				using (UserDialogs.Instance.Loading ("Loading...")) {
					await Task.Run (async () => {
						while (true) {
							try {
								setting = await IntroBlrtSettingManager.GetAsync ();
								break;
							} catch (Exception ex) {
								if (await AlertInternetErrorRetry ()) {
									continue;
								} else {
									break;
								}
							}
						}
					});
				}
				if (null == setting) {
					return;
				}
				for (int i = 0; i < setting.intro.Length; i++) {
					if (_page == setting.intro[i].page) {
						new OpenIndependentBlrtPerformer (Executor.System (), setting.intro[i].blrt).RunAction ();
					}
				}
			}

			async Task<bool> AlertInternetErrorRetry ()
			{
				var tcs = new TaskCompletionSource<bool> ();
				Device.BeginInvokeOnMainThread (() => {
					BlrtManager.App.ShowAlert (Executor.System (), 
						new SimpleAlert(
							BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_title"),
							BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_message"),
							BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_cancel"),
							BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_retry")
						)
					).ContinueWith ((t) => {
						var alert = t.Result;
						if (!alert.Success || alert.Result.Cancelled) {
							tcs.TrySetResult (false);
						} else {
							tcs.TrySetResult (true);
						}
					});
				});
				return await tcs.Task;
			}
			#endregion
			
		}
	}
}

