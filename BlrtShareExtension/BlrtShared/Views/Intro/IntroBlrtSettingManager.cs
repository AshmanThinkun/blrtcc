﻿using System;
using System.IO;
using BlrtData;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Generic;

namespace BlrtData
{
	public class IntroBlrtSettingManager
	{
		public class Intro
		{
			public int page;
			public string blrt;
		}

		public class Settings
		{
			public Intro[] intro;
		}

		private static string FileName = "intro-setting";
		private const string EncryptionKey = "oPFakNLocI1ITLwOR3DkrJtC2yB3cZ";
		const int SettingValidDuration = 24 * 60 * 60;

		public DateTime UpdatedAt { get; set; }
		public Settings IntroSettings { get; set; }
		public string DefaultId { get; set; }

		public IntroBlrtSettingManager ()
		{
			UpdatedAt = DateTime.MinValue;
			IntroSettings = null;
			DefaultId = null;
		}

		static bool _requestingSettings = false;
		static IntroBlrtSettingManager _instance;
		static Task<IntroBlrtSettingManager> _getSettingTask;
		public static async Task<Settings> GetAsync ()
		{
			if (null == _instance) {
				_instance = GetFromCache ();
			}

			if ((null == _instance) || ((DateTime.UtcNow - _instance.UpdatedAt).TotalSeconds > SettingValidDuration)) {
				_requestingSettings = true;
				if (_getSettingTask == null
					|| _getSettingTask.IsFaulted
					|| _getSettingTask.IsCanceled) 
				{
					_getSettingTask = GetFromCloud ();
				}
				await _getSettingTask.ContinueWith ((t) => {
					_requestingSettings = false;
				});
				return (await _getSettingTask).IntroSettings;
			} else {
				return _instance.IntroSettings;
			}
		}

		static IntroBlrtSettingManager GetFromCache ()
		{
			try {
				var json = File.ReadAllText(
					BlrtConstants.Directories.IndependentPublic.GetDataPath (FileName)
				);
				json = BlrtUtil.XorString(json, EncryptionKey);
				return JsonConvert.DeserializeObject<IntroBlrtSettingManager>(json);
			} catch (Exception ex1) {
				return null;
			}
		}

		const int DefaultPageNum = -1;
		static async Task<IntroBlrtSettingManager> GetFromCloud ()
		{
			var settings = await GetFromCloudRetried ();

			if (null == _instance) {
				_instance = new IntroBlrtSettingManager ();
			}
			if (IsSettingsValid (settings)) {
				for (int i = 0; i < settings.intro.Length; i++) {
					if (DefaultPageNum == settings.intro[i].page) {
						_instance.DefaultId = settings.intro [i].blrt;
						break;
					}
				}
				string defaultId = _instance.DefaultId ?? BlrtConstants.DefaultIntroBlrtId;
				_instance.IntroSettings = ValidateSettings (settings, defaultId);
				_instance.UpdatedAt = DateTime.UtcNow;
			} else {
				string defaultId = _instance.DefaultId ?? BlrtConstants.DefaultIntroBlrtId;
				_instance.IntroSettings = ValidateSettings (settings, defaultId);
				_instance.UpdatedAt = DateTime.MinValue;
				_instance.DefaultId = defaultId;
			}
			try {
				var json = BlrtUtil.XorString(JsonConvert.SerializeObject(_instance), EncryptionKey);
				File.WriteAllText(BlrtConstants.Directories.IndependentPublic.GetDataPath (FileName), json);
			} catch (Exception ex2) {
				LLogger.WriteEvent ("IntroBlrtSettingManager", "GetFromCloud-WriteToFile", "Exception", ex2.ToString ());
			}
			return _instance;
		}

		const int MaxRetry = 5;
		const int RetryDelay = 10;
		static async Task<Settings> GetFromCloudRetried ()
		{
			int retryCounter = 0;
			while (true) {
				try {
					var settings = await GetFromCloudSingle ();
					if (_requestingSettings || IsSettingsValid (settings)) {
						return settings;
					}
				} catch (Exception ex) {
					
				}
				if (_requestingSettings) {
					throw new Exception ("cannot get settings");
				}
				if (retryCounter >= MaxRetry) {
					return null;
				} else {
					await Task.Delay (RetryDelay);
					++retryCounter;
				}
			}
			return null;
		}

		const string IntroSettingUrl = BlrtConstants.ServerUrl + "/intro.json";

		static async Task<Settings> GetFromCloudSingle ()
		{
			var request = HttpWebRequest.Create (IntroSettingUrl);
			using (var response = (await request.GetResponseAsync ()) as HttpWebResponse) {
				if (response.StatusCode == HttpStatusCode.OK) {
					var stream = new StreamReader (response.GetResponseStream ());
					var responseText = stream.ReadToEnd ();
					return JsonConvert.DeserializeObject<Settings> (responseText);
				}
				throw new Exception ("Cannot get setting, http status code: " + response.StatusCode);
			}
		}

		static bool IsSettingsValid (Settings settings)
		{
			if ((null != settings) && (null != settings.intro)) {
				var intro = settings.intro;
				bool hasDefault = false;
				bool has0 = false;
				bool has4 = false;
				for (int i = 0; i < intro.Length; i++) {
					switch (intro[i].page) {
						case DefaultPageNum:
							hasDefault = true;
							break;
						case 0:
							has0 = true;
							break;
						case 4:
							has4 = true;
							break;
						default:
							break;
					}
					if (hasDefault || (has0 && has4)) {
						return true;
					}
				}
			}
			return false;
		}


		static Settings ValidateSettings (Settings settings, string defaultId = BlrtConstants.DefaultIntroBlrtId)
		{
			if (null == settings) {
				settings = new Settings ();
			}
			if (null == settings.intro || settings.intro.Length == 0) {
				settings.intro = new Intro[] {
					new Intro() { page = 0, blrt = defaultId },
					new Intro() { page = 4, blrt = defaultId }
				};
			}
			var intro = settings.intro;
			bool hasDefault = false;
			bool has0 = false;
			bool has4 = false;
			string defaultInSetting = null;
			for (int i = 0; i < intro.Length; i++) {
				switch (intro[i].page) {
					case DefaultPageNum:
						hasDefault = true;
						defaultInSetting = intro [i].blrt;
						break;
					case 0:
						has0 = true;
						break;
					case 4:
						has4 = true;
						break;
					default:
						break;
				}
			}
			if ((!has0) || (!has4)) {
				var list = new List<Intro> (settings.intro);
				if (!has0) {
					list.Add (new Intro() { page = 0, blrt = defaultInSetting ?? defaultId });
				}
				if (!has4) {
					list.Add (new Intro() { page = 4, blrt = defaultInSetting ?? defaultId });
				}
			}
			return settings;
		}
	}
}

