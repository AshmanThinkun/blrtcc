using System;
using Xamarin.Forms;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Models.Mailbox;
using BlrtData.Views.CustomUI;
using System.Linq;

namespace BlrtData.Views.Mailbox.UserFilter
{
	public class FilterPage: BlrtContentPage
	{
		static readonly int rowHeight = 44;

		static FilterState _state;

		private List<string> _filteredUserIds;

		private List<string> _filteredTagIds;

		private ListView _listView;
		private IMailboxFilter _mailBoxFilter;

		private Button _tagBtn, _userBtn;

		public override string ViewName {
			get {
				return "Mailbox_Filter";
			}
		}

		public FilterPage (IMailboxFilter mailBoxFilter):base()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("inbox_filter_screen","screen_titles");
			_mailBoxFilter = mailBoxFilter;

			_tagBtn = new Button(){
				Text = BlrtUtil.PlatformHelper.Translate ("Tags"),
				BorderRadius = 0,
				VerticalOptions = LayoutOptions.Center
			};

			_userBtn = new Button(){
				Text = BlrtUtil.PlatformHelper.Translate ("People"),
				BorderRadius = 0,
				VerticalOptions = LayoutOptions.Center
			};

			_tagBtn.Clicked += StateBtnClicked;
			_userBtn.Clicked += StateBtnClicked;

			_listView = new FormsListView{
				VerticalOptions = LayoutOptions.Fill,
				RowHeight = rowHeight,
			};


			_listView.ItemTemplate = new DataTemplate(typeof(UserFilterCell));

			_listView.ItemSelected += (sender, e) => {
				if(e.SelectedItem is FilterItem){
					RowSelected (e.SelectedItem as FilterItem);
				}
				_listView.SelectedItem = null;
			};

			if (_state == FilterState.Users)
				LoadUserFilter ();
			else
				LoadTagFilter ();

			Content = new Grid () {
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,

				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Auto)},
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnSpacing		= 0,
				RowSpacing			= 0,
				Padding				= 0,
				HeightRequest		= rowHeight,
				Children = {
					{_tagBtn, 1, 0 },
					{_userBtn, 0, 0 },
					{_listView, 0, 1 },
				}			
					
			};

			Grid.SetColumnSpan(_listView, 2);

			StateBtnClicked (null, null);
		}

		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();

			if (Device.OS == TargetPlatform.iOS) {
				_mailBoxFilter.ApplyUserFilters (_filteredUserIds==null ? _mailBoxFilter.UserFilters: _filteredUserIds.ToArray ());
				_mailBoxFilter.ApplyTagFilters (_filteredTagIds==null? _mailBoxFilter.TagFilters : _filteredTagIds.ToArray());
				_mailBoxFilter.ReloadList ();
			}
		}

		void StateBtnClicked(object sender, EventArgs e){
			if (sender != null) {
				if (sender == _tagBtn) {
					LoadTagFilter ();
				} else {
					LoadUserFilter ();
				}
			}
			Device.BeginInvokeOnMainThread (() => {
				_tagBtn.BackgroundColor = _state == FilterState.Tags ? Color.Transparent : BlrtStyles.BlrtGray4;
				_userBtn.BackgroundColor = _state == FilterState.Users ? Color.Transparent : BlrtStyles.BlrtGray4;

				_tagBtn.TextColor = BlrtStyles.BlrtCharcoal;
				_userBtn.TextColor = BlrtStyles.BlrtCharcoal;
			});
		}

		async void RowSelected(FilterItem item){
			item.IsFiltered = !item.IsFiltered;
			var filteredIds = _state == FilterState.Users ? _filteredUserIds : _filteredTagIds;

			if(!item.IsFiltered){
				filteredIds.Remove (item.FilterId);
			} else if(!filteredIds.Contains (item.FilterId)){
				filteredIds.Add (item.FilterId);
			}

			if (Device.OS == TargetPlatform.Android) {
				if (_state == FilterState.Users)
					_mailBoxFilter.ApplyUserFilters (_filteredUserIds.ToArray ());
				else
					_mailBoxFilter.ApplyTagFilters (_filteredTagIds.ToArray ());
				_mailBoxFilter.ReloadList ();
			}
		}

		private void LoadTagFilter(){
			_state = FilterState.Tags;
			_filteredTagIds = new List<string>(_mailBoxFilter.TagFilters);

			var tags = _mailBoxFilter.GetAllTags ()
				.Concat(_filteredTagIds)
				.Select(tag => tag.ToLower())
				.Distinct()
				.OrderBy(str => str);

			List<FilterItem> data = new List<FilterItem> ();
			foreach(var tag in tags)
			{
				var item = new FilterItem () {
					Title = tag,
					FilterId = tag.ToLower (),
					IsFiltered = _filteredTagIds.Contains (tag),
				};
				data.Add (item);
			}
			Device.BeginInvokeOnMainThread (() => {
				_listView.ItemsSource = data;
			});
		}

		private void LoadUserFilter () {
			_state = FilterState.Users;
			_filteredUserIds = new List<string>(_mailBoxFilter.UserFilters);

			var conversations = new List<BlrtData.Conversation> ();
			foreach(var convoId in _mailBoxFilter.GetAllConversations ()){
				conversations.Add (BlrtManager.DataManagers.Default.GetConversation (convoId));
			}

			var userIds = BlrtBaseHelpers.GetUserInConversations (conversations);

			List<FilterItem> data = new List<FilterItem> ();
			foreach(var userId in userIds)
			{
				var user = BlrtManager.DataManagers.Default.GetUser (userId);

				var item = new FilterItem () {
					Title = user.DisplayName,
					FilterId = userId,
					IsFiltered = _filteredUserIds.Contains (userId),
					Detail = user.ContactInfo
				};
				data.Add (item);
			}
			data.Sort(new Comparison<FilterItem> ((x,y)=>{
				return string.Compare (x.Title,y.Title);
			}));

			Device.BeginInvokeOnMainThread (() => {
				_listView.ItemsSource = data;
			});
		}

		public async void ClearAll(object sender, EventArgs e){
			_mailBoxFilter.ApplyUserFilters (new string[0]); 
			_mailBoxFilter.ApplyTagFilters (new string[0]); 
			_mailBoxFilter.ReloadList ();

			if(_state == FilterState.Tags)
				LoadTagFilter ();
			else
				LoadUserFilter ();
		}


		public class FilterItem: BlrtNotifyProperty
		{
			public string Title { get; set; }
			public string FilterId { get; set; }
			public string Detail { get; set; }

			private bool _isFiltered;
			public bool IsFiltered{get{ return _isFiltered;}
				set{
					OnPropertyChanged (ref _isFiltered, value);
				}
			}
		}

		public enum FilterState{
			Users,
			Tags
		}
	}
}

