using System;
using Xamarin.Forms;

namespace BlrtData.Views.Mailbox.UserFilter
{
	public class UserFilterCell : ViewCell
	{
		public static readonly BindableProperty IsFilteredProperty =
			BindableProperty.Create<UserFilterCell, bool>(cell => cell.IsFiltered, false);

		public bool IsFiltered { get { return (bool)GetValue(IsFilteredProperty); } set { SetValue (IsFilteredProperty, value); } }

		const int IconHeight = 30;
		const int IconWidth = 32;
		private Label _nameLbl, _filterLbl, _emailLbl;
		private ContentView _icon;
		public UserFilterCell ():base()
		{
			_icon = new ContentView () {

				HeightRequest = IconHeight,
				WidthRequest = IconWidth,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,

				Content = new Image () {
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center,
					Source = "filter_icon.png",
				}
			};


			_nameLbl = new Label () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Start,
				FontFamily = BlrtStyles.CellTitleFont.FontFamily,
				FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
				FontSize = BlrtStyles.CellTitleFont.FontSize
			};
			_emailLbl = new Label () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Start,
				FontFamily = BlrtStyles.CellDetailFont.FontFamily,
				FontAttributes = BlrtStyles.CellDetailFont.FontAttributes,
				FontSize = BlrtStyles.CellDetailFont.FontSize
			};

			_filterLbl = new Label () {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.End,
				TextColor = BlrtStyles.BlrtGreen,
				IsVisible = false,
				FontFamily = BlrtStyles.CellTitleFont.FontFamily,
				FontAttributes = BlrtStyles.CellTitleFont.FontAttributes,
				FontSize = BlrtStyles.CellTitleFont.FontSize
			};

			View = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
				},
				RowDefinitions = {
					new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
				},

				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Padding = new Thickness(15,0),


				RowSpacing = 0,
				ColumnSpacing = 12,

				Children = {
					{_icon, 0,0},
					{
						new StackLayout(){

							Spacing = 1,
							VerticalOptions = LayoutOptions.Center,

							Children = {
								_nameLbl,
								//_emailLbl
							}
						}, 1,0
					},
					{_filterLbl, 2,0}
				}
			};

			_nameLbl.SetBinding (Label.TextProperty, "Title");
			_emailLbl.SetBinding (Label.TextProperty, "Detail");

			this.SetBinding (IsFilteredProperty, "IsFiltered");

			OnPropertyChanged (IsFilteredProperty.PropertyName);
		}


		protected override void OnPropertyChanged (string propertyName)
		{
			if (propertyName == IsFilteredProperty.PropertyName) {
				_filterLbl.IsVisible = IsFiltered;
				_icon.BackgroundColor = IsFiltered ? BlrtStyles.BlrtGreen : BlrtStyles.BlrtGray3;
			}
			base.OnPropertyChanged (propertyName);
		}
	}
}

