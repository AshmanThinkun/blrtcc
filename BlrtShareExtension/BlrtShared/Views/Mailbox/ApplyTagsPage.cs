using System;
using System.Linq;
using Xamarin.Forms;
using BlrtData.Models.Mailbox;
using BlrtData.Views.CustomUI;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Parse;

namespace BlrtData.Views.Mailbox
{
	public class ApplyTagsPage : BlrtContentPage
	{
		public const int RowHeight = 44;

		ConversationUserRelation _relation;

		ObservableCollection<TagItem> _tagsList;

		ListView _listView;
		TextBarView _tagBarView;

		public override string ViewName {
			get {
				return "Conversation_Tags";
			}
		}

		public override void TrackPageView ()
		{
			if (_relation == null)
				return;
			var dic = new Dictionary<string, object> ();
			var rel = _relation;

			dic.Add ("isOwner", rel.CreatorId == ParseUser.CurrentUser.ObjectId);
			dic.Add ("convoId", rel.ConversationId);

			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, dic);
		}

		public ApplyTagsPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("Tags");

			_tagsList = new ObservableCollection<TagItem> ();


			_listView = new FormsListView () {
				RowHeight = RowHeight,

				ItemsSource = _tagsList,
				ItemTemplate = new DataTemplate(CreateCell),
			};
			_tagBarView = new TextBarView () {
				SendButtonText = BlrtUtil.PlatformHelper.Translate ("tags_add_btn", "tags"),
				Placeholder = BlrtUtil.PlatformHelper.Translate("tags_add_placeholder", "tags")
			};



			Content = new ContentView() {
				Content = new Grid(){

					ColumnDefinitions = new ColumnDefinitionCollection{
						new ColumnDefinition(){ Width = new GridLength(1, GridUnitType.Star) },
					},
					RowDefinitions = new RowDefinitionCollection{
						new RowDefinition(){ Height = new GridLength(1, GridUnitType.Star) },
						new RowDefinition(){ Height = new GridLength(1, GridUnitType.Auto) },
					},

					RowSpacing		= 0,
					ColumnSpacing	= 0,
					Padding			= 0,

					Children = {
						{	_listView,		0,	0	},
						{	_tagBarView,	0,	1	}
					}
				}
			};

			_listView.ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
				if(e.SelectedItem is TagItem)
					Check(e.SelectedItem as TagItem);
				_listView.SelectedItem = null;
			};
			_tagBarView.SendClicked += (object sender, EventArgs e) => {
				if(IsTagValid(_tagBarView.Text)) {
					AddTag(_tagBarView.Text, true, 0);
					_tagBarView.Text = "";
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			_tagBarView.FocusTextField ();
		}

		public void SetRelation(string conversationId, string[] otherTags)
		{
			_relation = BlrtManager.DataManagers.Default.GetConversationUserRelation(conversationId, ParseUser.CurrentUser.ObjectId);

			foreach (var t in _relation.Tags.OrderBy(str => str))
				AddTag (t, true, -1, false);

			foreach (var t in otherTags.Distinct(new MailboxFilter.TagsStringComparer()).OrderBy(str => str).Except(_relation.Tags))
				AddTag (t, false, -1, false);
		}


		public static bool IsTagValid(string tag) {
			return !string.IsNullOrWhiteSpace (tag);
		}

		void AddTag(string tag, bool select, int index = -1, bool sync = true){

			tag = tag.ToLower ();

			var c = _tagsList.FirstOrDefault(cell => cell.Name == tag);

			if(c == null){
				c = new TagItem () {
					Name = tag,
				};

				if(index < 0)
					_tagsList.Add (c);
				else
					_tagsList.Insert (0, c);
			}

			c.IsFilter = select;
			if (sync){
				Check (c, true);
			}
		}


		void Check(TagItem item, bool ignoreFilter = false){

			if(!ignoreFilter)
				item.IsFilter = !item.IsFilter;

			_relation.Tags = _tagsList.Where (tag => tag.IsFilter)
				.Select (tag => tag.Name)
				.Distinct(new MailboxFilter.TagsStringComparer())
				.ToArray ();

			_relation.Dirty = true;
			BlrtManager.DataManagers.Default.Save ();

			//track
			BlrtData.Helpers.BlrtTrack.ConvoAddTag (_relation.ConversationId, _relation.Tags);
		}

		object CreateCell(){

			var lbl = new Label () {
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center,
			};

			var tick = new Image () {
				Source = "tag_tick.png",

				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
			};


			var cell = new ViewCell(){

				View = new Grid(){
					ColumnDefinitions = {
						new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)},
						new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Auto)},
					},
					RowDefinitions = {
						new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
					},


					ColumnSpacing = 4,
					Padding = new Thickness(15, 4),

					Children = {
						{new ContentView(){

							HorizontalOptions = LayoutOptions.Center,
							VerticalOptions = LayoutOptions.Center,

							HeightRequest = RowHeight - 4 * 2,
							WidthRequest = RowHeight - 4 * 2,

							Content = tick,
							}, 1, 0},
						{lbl, 0, 0}
					}
				}
			};

			lbl.SetBinding (Label.TextProperty, "Name");
			tick.SetBinding (View.IsVisibleProperty, "IsFilter");

			return cell;
		}

		private class TagItem : BlrtNotifyProperty {
		
			public string _name;
			public bool _isFilter;

			public string Name { get { return _name; } set { this.OnPropertyChanged (ref _name, value); } }
			public bool IsFilter { get { return _isFilter; } set { this.OnPropertyChanged (ref _isFilter, value); } }

		}
	}
}

