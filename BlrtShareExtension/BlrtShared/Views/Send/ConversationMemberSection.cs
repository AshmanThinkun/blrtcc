﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;
using System.Linq;
using BlrtData.Query;
using System.Threading;
using BlrtData.Models.ConversationScreen;

namespace BlrtData.Views.Send
{
	public class ConversationMemberSection : ContentView
	{
		FormsListView _listView;
		ConversationRelationStatus.Comparer _comparer;
		ActivityIndicator _loading;
		public const int HeaderHeight = 28;

		public ConversationMemberSection () : base ()
		{
			_comparer = new ConversationRelationStatus.Comparer ();
			_listView = new FormsListView () {
				RowHeight = MemberCell.CellHeight,
				SeparatorInset = new Thickness (MemberCell.CellHeight + 10, 0, 0, 0),
				SeparatorColor = BlrtStyles.BlrtGray3,
				BackgroundColor = BlrtStyles.BlrtGray2,
				DisableScrolling = true,
			};

			_loading = new ActivityIndicator () {
				IsRunning = true,
				IsEnabled = true,
				IsVisible = false
			};

			var titleSec = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				BackgroundColor = BlrtStyles.BlrtGreen,
				Padding = new Thickness (10, 0, 15, 0),
				HeightRequest = HeaderHeight,
				Children = {
					{ new Label(){
							HeightRequest = HeaderHeight,
							YAlign = TextAlignment.Center,
							Text = "People in conversation",
							FontSize = BlrtStyles.CellContentFont.FontSize,
							TextColor = BlrtStyles.BlrtGray6,
						},0,0},
					{
						_loading, 1,0
					}
				}
			};

			_listView.ItemTemplate = new DataTemplate (typeof (MemberCell));
			_listView.ItemSelected += _listView_ItemSelected;

			Content = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				Padding = new Thickness (0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				Children = {
					{ titleSec,0,0},
					{ _listView,0, 1 },
				}
			};
		}

		void _listView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null) {
#if __ANDROID__
				var item = e.SelectedItem as BlrtUtil.ConversationsOptionsUserItem;
#elif __IOS__
				var item = e.SelectedItem as BlrtUtil.ConversationExistingContact;
#endif
				_listView.SelectedItem = null;
				if (item.Model != null) {
					if (item.Model is BlrtUser)
						BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), item.Model as BlrtUser);
					else {
						var contact = item.Model as BlrtData.Contacts.BlrtContact;
						if (contact.InfoList.Count < 1)
							return;
						BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), contact);
					}
				}
			}
		}

		BlrtData.Conversation _conversation;
		public void SetConversation (BlrtData.Conversation conversation)
		{
			_conversation = conversation;
			ReloadList ();
			RunConversationRelationQuery ();
		}


		async void RunConversationRelationQuery ()
		{
			try {
				var query = BlrtData.Query.BlrtConversationMetaQuery.RunQuery (BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId));
				await query.WaitAsync ();
				ReloadList ();
			} catch {

			}
		}

#if __ANDROID__
		public void ReloadList ()
		{
			if (_conversation == null)
				return;
			var relations = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId);

			var unfetchedUsers = new List<string> ();

			var dataList = relations.OrderBy (t => t, _comparer).Select ((userRelation) => {
				var displayItem = BlrtUtil.CreateConversationsOptionsUserItem (userRelation);

				if (!string.IsNullOrWhiteSpace (userRelation.UserId)) {
					var user = BlrtManager.DataManagers.Default.GetUser (userRelation.UserId);
					displayItem.Model = user;
					if (!user.Found) {
						displayItem.DisplayName = "...";
						displayItem.Initials = "";
						displayItem.RelationType = "";
						unfetchedUsers.Add (userRelation.UserId);
					} else {
						if (string.IsNullOrWhiteSpace (user.LocalAvatarPath)) {
							displayItem.Initials = BlrtUtil.GetInitials (displayItem.DisplayName);
						} else {
							displayItem.Initials = "";
							displayItem.AvatarPath = user.LocalAvatarPath;
						}
					}
				} else {
					var c = new BlrtData.Contacts.BlrtContact ();
					c.Add (new BlrtData.Contacts.BlrtContactInfo (BlrtData.Contacts.BlrtContactInfoType.Email, userRelation.GetUserEmail ()));
					displayItem.Model = BlrtData.Contacts.BlrtContactManager.SharedInstanced.GetMatching (c);
					if (displayItem.Model == null) {
						displayItem.Model = c;
					}
				}

				return displayItem;
			});

			_listView.ItemsSource = dataList;

			_listView.HeightRequest = MemberCell.CellHeight * dataList.Count ();
			this.Content.HeightRequest = _listView.HeightRequest + HeaderHeight;

			if (unfetchedUsers.Count > 0) {
				FetchUsers (unfetchedUsers);
			}
		}
#elif __IOS__
		public void ReloadList(){
			if (_conversation == null)
				return;

			try {
				var result = BlrtUtil.CreateConversationExistingContactsForConversation (_conversation.ObjectId);
				var existingContacts = result.Item1;
				var unfetchedUsers = result.Item2;

				_listView.ItemsSource = existingContacts;

				_listView.HeightRequest = MemberCell.CellHeight * existingContacts.Count ();
				this.Content.HeightRequest = _listView.HeightRequest + HeaderHeight;

				if(unfetchedUsers.Count>0){
					FetchUsers (unfetchedUsers);
				}
			} catch {
				
			}
		}
#endif

		List<string> _latestfetchingUsers;
		async void FetchUsers(List<string> userIds){
			if(_latestfetchingUsers!=null && _latestfetchingUsers.SequenceEqual(userIds)){
				return;
			}

			_latestfetchingUsers = userIds;
			//show loading
			_loading.IsVisible = true;
			try{
				await BlrtParseActions.FindUsersQuery (userIds, new CancellationTokenSource (15000).Token);
			}catch{
			}
			ReloadList ();

			if(_latestfetchingUsers == userIds){
				_latestfetchingUsers = null;
				//hide loading
				_loading.IsVisible = false;
			}
		}

		public class MemberCell: ViewCell{
			public static int CellHeight = 50;

			Label _nameLbl, _typeLbl, _initialLbl;
			RoundedImageView _roundBox;

			public MemberCell():base(){
				_nameLbl = new Label(){
					XAlign = TextAlignment.Start,
					YAlign = TextAlignment.Center,
					FontSize = BlrtStyles.FontSize14.FontSize,
					LineBreakMode = LineBreakMode.TailTruncation,
					TextColor = BlrtStyles.BlrtGray6
				};

				_typeLbl = new Label(){
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.End,
					TextColor = BlrtStyles.BlrtGray5,
					FontSize = BlrtStyles.FontSize14.FontSize
				};

				_initialLbl = new Label(){
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.Center,
					TextColor = BlrtStyles.BlrtWhite,
					FontSize = BlrtStyles.FontSize14.FontSize
				};

				_roundBox = new RoundedImageView(){
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					CornerRadius = (CellHeight-10)*0.5f,
					HeightRequest = CellHeight-10,
					WidthRequest = CellHeight-10,
					Source = "cross.png",
					Aspect = Aspect.AspectFill
				};

				var bottomPadding = 0;

				this.View = new Grid(){
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					Padding = new Thickness(10, 10, 10, bottomPadding),
					ColumnSpacing = 10,
					RowSpacing = 0,
					Children = {
						{_roundBox, 0,0},
						{_initialLbl, 0,0},
						{_nameLbl, 1, 0},
						{_typeLbl, 2,0}
					}
				};

				if(Device.OS ==  TargetPlatform.Android){
					this.View.BackgroundColor = BlrtStyles.BlrtGray2;
				}

				_roundBox.SetBinding(RoundedImageView.BackgroundColorProperty, "InitialBackgroundColor");
				_roundBox.SetBinding(RoundedImageView.SourceProperty, "AvatarPath");
				_initialLbl.SetBinding(Label.TextProperty, "Initials");
				_typeLbl.SetBinding(Label.TextProperty, "RelationType");
				_nameLbl.SetBinding(Label.TextProperty, "DisplayName");

			}
		}
	}
}

