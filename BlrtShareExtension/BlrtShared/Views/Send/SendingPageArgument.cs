﻿using System;
using System.Linq;
using Xamarin.Forms;
using BlrtData;
using BlrtData.ExecutionPipeline;

namespace BlrtData.Views.Send
{
	public class SendingPageArgument
	{
		public BlrtData.Conversation Conversation	{ get; private set; }


		public bool IsSpecial				{ get { return Conversation.IsSpecialConversation; } }
		public string ConversationId		{ get { return Conversation.ObjectId; } }
		public bool CanSend					{ get { return Conversation.OnParse; } }
		public int ConversationUserCount	{ get { return Conversation.BondCount; } }

		public IRequestArgs RequestArgs { get; set; }

		public SendingPageArgument (string conversationId)
		{
			this.Conversation = BlrtManager.DataManagers.Default.GetConversation (conversationId);
		}
	}
}

