﻿using System;
using BlrtData;
using System.Collections;
using System.Collections.Generic;
using BlrtData.Contacts;

namespace BlrtData.Views.Send
{
	public class SelectContactListModel
	{
		public List<ConversationNonExistingContact> Contacts{
			get{
				return _contactList;
			}
		}

		private List<ConversationNonExistingContact> _contactList;
		public SelectContactListModel ()
		{
			_contactList = new List<ConversationNonExistingContact> ();
		}

		public void AddContact(ConversationNonExistingContact e){
			if (_contactList.Contains (e)) {
				return;
			}else{
				_contactList.Add (e);
			}
		}

		public void DeleteAtIndex(int i) {
			try {
				_contactList.RemoveAt (i);
			} catch {}
		}
	}
}

