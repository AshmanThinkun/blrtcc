﻿#if __IOS__

using System.Collections.Generic;
using BlrtData.Contacts;
using System.Linq;

namespace BlrtData.Views.Send
{
	/// <summary>
	/// Conversation contact list mel for all types of conatcts: contacts that have been added successfully, failed, in-progress, 
	/// and ones selected on the first add-people screen to send invitation to 
	/// </summary>
	public class ConversationContactListModel
	{
		List<ConversationNonExistingContact> _nonExistingContacts;

		/// <summary>
		/// Gets the existing contacts. It includes all the contacts that have been added successfully to a conversation
		/// </summary>
		/// <value>The existing contacts.</value>
		public List<BlrtUtil.ConversationExistingContact> ExistingContacts { get; internal set; }

		/// <summary>
		/// Gets all contacts.
		/// </summary>
		/// <value>All contacts.</value>
		public List<List<BlrtUtil.IConversationContact>> AllContacts {
			get {
				var contacts = ExistingContacts?.Select(ec => ec as BlrtUtil.IConversationContact).ToList ();
				var nonExistingContacts = _nonExistingContacts.Select (nec => nec as BlrtUtil.IConversationContact).ToList ();
				return new List<List<BlrtUtil.IConversationContact>> { contacts, nonExistingContacts };
			}
		}

		/// <summary>
		/// Gets the non existing contacts. It includes all the unsent contacts: failed, in-progress 
		/// and ones selected on the first add-people screen to send invitation to 
		/// </summary>
		/// <value>The non existing contacts.</value>
		public List<ConversationNonExistingContact> NonExistingContacts {
			get {
				return _nonExistingContacts;
			}
		}

		public ConversationContactListModel ()
		{
			_nonExistingContacts = new List<ConversationNonExistingContact> ();
		}

		public void AddContact(ConversationNonExistingContact e) {
			_nonExistingContacts.Add (e);
		}

		public void DeleteAtIndex(int i) {
			try {
				_nonExistingContacts.RemoveAt (i);
			} catch {}
		}
	}
}

#endif