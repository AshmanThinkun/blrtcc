﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using BlrtData.Contacts;
using System.Linq;
using System.Collections.ObjectModel;
using BlrtData.ExecutionPipeline;

#if __ANDROID__
using BlrtApp.Droid;
#endif

namespace BlrtData.Views.Send
{
	public class ContactTablePage : BlrtContentPage
	{
		protected const int RecentContectCounterDiameter = 16;
		ListView _listView;
		SearchBar _searchBar;
		protected Button _allBtn, _recentBtn, _blrtBtn;
		protected AbsoluteLayout _recentJoinCounter;
		protected Label _recentJoinCounterTxt;
		View _recentCounterView;
		WarningView _warningView;

		List<BlrtContact> _contacts;
		IEnumerable<TableCellData> _dataSource;

#if __ANDROID__
		public event EventHandler<ConversationNonExistingContact> ContactSelected;
		public event EventHandler<BlrtAddedContact> ContactCheckedSelected;
		public event EventHandler<bool> SendReminder;
#elif __IOS__
		public event EventHandler<ConversationNonExistingContact> ContactSelected;
#endif

		BlrtContactForm _currentFilter;

		public override string ViewName {
			get {
				return "Conversation_Send_Add_Contacts";
			}
		}

		public ContactTablePage ()
		{

			Title = BlrtUtil.PlatformHelper.Translate ("Select contact");
			_listView = new FormsListView () {
				BackgroundColor = BlrtStyles.BlrtGray2,
				HasUnevenRows = true,
				ItemTemplate = new DataTemplate (typeof (ContactTableCell)),
				IsGroupingEnabled = true,
				GroupHeaderTemplate = new DataTemplate (typeof (ContactTableHeaderCell)),
				GroupDisplayBinding = new Binding ("Text"),
				GroupShortNameBinding = new Binding ("Short"),
				SeparatorColor = BlrtStyles.BlrtGray3,
				SeparatorInset = new Thickness (20, 0, 0, 0)
			};
			_listView.ItemSelected += ListViewItemSelected;

			_searchBar = new SearchBar () {
				Placeholder = "Search",
				BackgroundColor = BlrtStyles.BlrtGray6,
				// setting a default width to fix a bug in iOS11. It will be calculated by system to match the screen width.
				// [BUG] https://forums.xamarin.com/discussion/103579/search-bar-crash-on-new-version-of-xamarin-with-latest-xcode
				WidthRequest = 5
			};

			_searchBar.TextChanged += (sender, e) => {
				if (_contacts != null)
					ReloadTable (_dataSource);
			};

			_listView.Header = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},
				Padding = new Thickness (0, -1),
				Children = {
					{_searchBar,0,0}
					//{new BoxView(){BackgroundColor = BlrtStyles.BlrtGray6, HeightRequest =3, VerticalOptions = LayoutOptions.Start},0,0},
				}
			};

			_allBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("All"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtBlackBlack
			};
			_recentBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("filter_recent", "send_screen"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtBlackBlack
			};
			_blrtBtn = new Button () {
				Text = BlrtUtil.PlatformHelper.Translate ("Blrt"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtBlackBlack
			};

			_recentJoinCounterTxt = new Label {
				FontSize = BlrtStyles.CellDetailFont.FontSize,
				TextColor = BlrtStyles.BlrtWhite,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent,
				WidthRequest = RecentContectCounterDiameter,
				HeightRequest = RecentContectCounterDiameter
			};

			var recentJoinCounterBackground = new RoundedBoxView {
				Color = BlrtStyles.BlrtAccentRed,
				CornerRadius = RecentContectCounterDiameter * 0.5f,
				WidthRequest = RecentContectCounterDiameter,
				HeightRequest = RecentContectCounterDiameter
			};

			_recentJoinCounter = new AbsoluteLayout () {
				WidthRequest = RecentContectCounterDiameter,
				HeightRequest = RecentContectCounterDiameter,
				Padding = 0,
				BackgroundColor = Color.Transparent
			};

			_recentJoinCounter.Children.Add (recentJoinCounterBackground, Point.Zero);
			_recentJoinCounter.Children.Add (_recentJoinCounterTxt, Point.Zero);

			var blrtBtnGrid = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				Padding = 0,
				Children = {
					{_recentCounterView = new ContentView(){
							HorizontalOptions = LayoutOptions.End,
							VerticalOptions = LayoutOptions.Start,
							Padding = new Thickness(40,7,0,0),
							IsVisible = false,
							Content = _recentJoinCounter
						},0,0},
					{_blrtBtn,0,0},
				}
			};

			_allBtn.Clicked += (sender, e) => {
				ApplyFilter (_allBtn, BlrtContactForm.None);
			};
			_recentBtn.Clicked += (sender, e) => {
				ApplyFilter (_recentBtn, BlrtContactForm.Recent);
			};
			_blrtBtn.Clicked += (sender, e) => {
				ApplyFilter (_blrtBtn, BlrtContactForm.Blrt);
			};

			var filterStack = new StackLayout () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = BlrtStyles.BlrtGray3,
				Orientation = StackOrientation.Horizontal,
				Spacing = Device.OS == TargetPlatform.Android ? 10 : 20,
				Padding = Device.OS == TargetPlatform.Android ? new Thickness (0, -5) : new Thickness (20, -5),
				Children = {
					blrtBtnGrid, _recentBtn, _allBtn
				}
			};

			_warningView = new WarningView () {
				Padding = new Thickness (5, 10),
				FontSize = BlrtStyles.CellContentFont.FontSize,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				IsVisible = false
			};

			//			this.Content = new StackLayout(){
			//				Orientation = StackOrientation.Vertical,
			//				VerticalOptions = LayoutOptions.Fill,
			//				HorizontalOptions = LayoutOptions.Fill,
			//				Padding = 0,
			//				Spacing = 0,
			//				Children ={filterStack, _warningView, _listView}
			//			};

			this.Content = new Grid () {
				Padding = 0,
				ColumnSpacing = 0,
				RowSpacing = 0,
				VerticalOptions = LayoutOptions.Fill,
				HorizontalOptions = LayoutOptions.Fill,
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},
				Children = {
					{filterStack, 0,0},
					{_warningView, 0,1},
					{_listView, 0,2},
				}
			};


			ApplyFilter (_blrtBtn, BlrtContactForm.Blrt);
		}


		void ApplyFilter (Button sender, BlrtContactForm form)
		{
			_allBtn.TextColor = _blrtBtn.TextColor = _recentBtn.TextColor = BlrtStyles.BlrtBlackBlack;
			sender.TextColor = BlrtStyles.BlrtAccentRed;

			_currentFilter = form;
			ReloadTable (_dataSource);
			Device.BeginInvokeOnMainThread (() => {
				_listView.ScrollTo (_firstItem, ScrollToPosition.End, true);
			});
		}

		async void ShowAllowSendingContactsAlert ()
		{
			var allow = await DisplayAlert ("allow sending data to cloud", "securely", "Accept", "Deny");
			BlrtPersistant.Properties ["disableSendingContacts"] = !allow;
			BlrtPersistant.SaveProperties ();
			UpdateWarningView ();
			if (allow) {
				BlrtContactManager.FindBlrtUserFromParse ();
			}
		}

		async void ListViewItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var data = e.SelectedItem as TableCellData;
			if (data == null)
				return;
			_listView.SelectedItem = null;

			ConversationNonExistingContact sel;

#if __ANDROID__
			var checkedSel = new BlrtAddedContact ();
			checkedSel.isAdded = data.LabelAddedVisible;
			checkedSel.isPending = data.TickVisible;
			checkedSel.contact = data.Contact;
			if (data.LabelAddedVisible && !data.Image2Visible) {
				checkedSel.isReminder = true;
				if (BlrtUtil.IsValidEmail (data.Email)) {
					checkedSel.isEmailReminder = true;
				}
			}

			ContactCheckedSelected?.Invoke (this, checkedSel);
#endif

			var info = data.Contact.InfoList.Where (t => (t.ContactType != BlrtContactInfoType.FacebookId) && (t.ContactType != BlrtContactInfoType.BlrtUserId)).ToList ();

			if (info.Count == 1 || (info.Count > 0 && this is BlrtData.Views.Contacts.ContactsPage)) {
				sel = new ConversationNonExistingContact (data.Contact.InfoList [0], data.Contact);

			} else {
				var ps = info.Select (x => x.ContactValue).ToArray ();
				if (ps == null || ps.Count () == 0)
					return;
				var selectValue = await DisplayActionSheet ("Select", "Cancel", null, ps);
				BlrtContactInfo selectInfo;
				try {
					selectInfo = info.First (t => t.ContactValue == selectValue);
				} catch { selectInfo = null; }

				if (selectInfo != null) {
					sel = new ConversationNonExistingContact (selectInfo, data.Contact);
				} else {
					return;
				}
			}

			ContactSelected?.Invoke(this, sel);


		}

		void UpdateWarningView ()
		{
			var list = new List<WarningAction> ();

#if __iOS__
			if (!BlrtiOS.FBManager.IsLinked) {
				list.Add (new WarningAction () {
					Title = BlrtUtil.PlatformHelper.Translate ("warning_connect_to_facebook", "profile"),
					BackgroundColor = BlrtStyles.BlrtAccentBlue,
					Action = new Action (async () => {
						var result = await BlrtManager.App.OpenProfile (Executor.System ());
						var controller = result.Result as BlrtiOS.Views.Profile.ProfileNavigationPage;
						if (controller != null) {
							controller.OpenFacebookPage ();
							controller.OnDismissed += (x, y) => {
								UpdateWarningView ();
							};
						}
					})
				});
			}
			if (AddressBook.ABAddressBook.GetAuthorizationStatus () != AddressBook.ABAuthorizationStatus.Authorized) {
				list.Add (new WarningAction () {
					Title = BlrtUtil.PlatformHelper.Translate ("warning_access_contacts", "profile"),
					BackgroundColor = BlrtStyles.BlrtAccentBlue,
				});
			}
			if (BlrtPersistant.Properties.ContainsKey ("disableSendingContacts") && ((bool)BlrtPersistant.Properties ["disableSendingContacts"]) == true) {
				list.Add (new WarningAction () {
					Title = "Enable sending contacts to server",
					BackgroundColor = BlrtStyles.BlrtAccentBlue,
					Action = new Action (() => {
						ShowAllowSendingContactsAlert ();
					})
				});
			}

#endif

			Device.BeginInvokeOnMainThread (() => {
				if (list.Count > 0) {
					_warningView.WarningList.Clear ();
					_warningView.WarningList.AddRange (list);
					_warningView.Visible = true;
					var stack = this.Content as Grid;
					stack.ForceLayout ();
				} else {
					_warningView.WarningList.Clear ();
					_warningView.Visible = false;
					var stack = this.Content as Grid;
					stack.ForceLayout ();
				}
			});
		}


#if __ANDROID__
		public void AssignContacts (IEnumerable<BlrtApp.Droid.BlrtAddedContact> contacts)
		{
			_contacts = contacts.Select(t=> t.contact).ToList();

			UpdateWarningView ();
			_dataSource = contacts.Select (t => {
				var c = t.contact;
				var cellData = new TableCellData () {
					DisplayName = c.UsableName,
					Email = c.ContactInfoToString (),
					Contact = c,
					BlrtUserCreatedAt = c.BlrtUserCreatedAt,
					Removed = c.Hide,
					LabelAddedVisible = t.isAdded,
					TickVisible = t.isPending
				};
				if (c.Avatar != null && System.IO.File.Exists (c.Avatar.Path)) {
					cellData.AvatarPath = c.Avatar.Path;
				} else {
					cellData.Initials = BlrtUtil.GetInitials (c.UsableName);
				}
				if (t.isPending) {
					cellData.Image1Visible = false;
					cellData.Image2Source = "addpeople_tick";
					cellData.Image2Visible = true;
				} else {
					bool facebook = false, blrtUser = false;
					foreach (var info in c.InfoList) {
						if (info.ContactType == BlrtContactInfoType.FacebookId)
							facebook = true;
						else if (info.ContactType == BlrtContactInfoType.BlrtUserId)
							blrtUser = true;
					}
					if (blrtUser) {
						cellData.Image2Source = "sendto_blrt.png";
					}
					if (facebook) {
						cellData.Image1Source = "sendto_fb.png";
					}
					cellData.Image2Visible = blrtUser;
					cellData.Image1Visible = facebook;
				}
				return cellData;
			});

			//check recently joined user counter
			var recentCount = _dataSource.Count (x => x.BlrtUserCreatedAt > DateTime.Now.AddDays (-7));
			Device.BeginInvokeOnMainThread (() => {
				if (recentCount == 0) {
					_recentCounterView.IsVisible = false;
				} else {
					_recentCounterView.IsVisible = true;
					_recentJoinCounterTxt.Text = recentCount.ToString ();
				}
			});

			ReloadTable (_dataSource);
		}
#endif

		public void AssignContacts (List<BlrtContact> contacts)
		{
			_contacts = contacts;
			UpdateWarningView ();
			_dataSource = _contacts.Select (c => {
				var cellData = new TableCellData () {
					DisplayName = c.UsableName,
					Email = c.ContactInfoToString (),
					Contact = c,
					BlrtUserCreatedAt = c.BlrtUserCreatedAt,
					Removed = c.Hide
				};
				if (c.Avatar != null && System.IO.File.Exists (c.Avatar.Path)) {
					cellData.AvatarPath = c.Avatar.Path;
				} else {
					cellData.Initials = BlrtUtil.GetInitials (c.UsableName);
				}
				bool facebook = false, blrtUser = false;
				foreach (var info in c.InfoList) {
					if (info.ContactType == BlrtContactInfoType.FacebookId)
						facebook = true;
					else if (info.ContactType == BlrtContactInfoType.BlrtUserId)
						blrtUser = true;
				}
				if (blrtUser) {
					cellData.Image2Source = "sendto_blrt.png";
				}
				if (facebook) {
					cellData.Image1Source = "sendto_fb.png";
				}
				cellData.Image2Visible = blrtUser;
				cellData.Image1Visible = facebook;

				return cellData;
			});

			//check recently joined user counter
			var recentCount = _dataSource.Count (x => x.BlrtUserCreatedAt > DateTime.Now.AddDays (-7));
			Device.BeginInvokeOnMainThread (() => {
				if (recentCount == 0) {
					_recentCounterView.IsVisible = false;
				} else {
					_recentCounterView.IsVisible = true;
					_recentJoinCounterTxt.Text = recentCount.ToString ();
				}
			});

			ReloadTable (_dataSource);
		}

		object _firstItem;
		protected virtual void ReloadTable (IEnumerable<TableCellData> dataSource)
		{
			if (dataSource == null)
				return;

			dataSource = Search (dataSource);
			Console.WriteLine (dataSource.Count ());
			//group the datasource
			var incluedRemoved = !string.IsNullOrEmpty (_searchBar.Text);
			var rtn = dataSource.Where (t => t.Contact.State.HasFlag (_currentFilter) && !t.Contact.Invalid && (incluedRemoved || !t.Removed))
				.GroupBy (x => {
					if (_currentFilter == BlrtContactForm.Blrt && x.BlrtUserCreatedAt > DateTime.Now.AddDays (-7)) {
						return BlrtUtil.PlatformHelper.Translate ("Recently joined");
					}
					var indexKey = x.DisplayName.ToUpper () [0];
					if (!char.IsLetter (indexKey)) {
						indexKey = '#';
					}
					return indexKey.ToString ();
				}).Select (g => {
					var sec = new TableSectionData () {
						Text = g.Key,
						Short = g.Key == BlrtUtil.PlatformHelper.Translate ("Recently joined") ? "" : g.Key
					};

					foreach (var o in g) {
						sec.Add (o);
					}
					return sec;
				}).ToList ();

			rtn.Sort (delegate (TableSectionData x, TableSectionData y) {
				if (x.Text == BlrtUtil.PlatformHelper.Translate ("Recently joined"))
					return -1;
				else if (y.Text == BlrtUtil.PlatformHelper.Translate ("Recently joined")) {
					return 1;
				}
				if (x.Text == "#" || y.Text == "#")
					return y.Text.CompareTo (x.Text);
				return x.Text.CompareTo (y.Text);
			});

			Device.BeginInvokeOnMainThread (() => {
				_listView.ItemsSource = rtn;
			});

			try {
				_firstItem = rtn.FirstOrDefault ().FirstOrDefault ();
			} catch { }
		}

		public IEnumerable<TableCellData> Search (IEnumerable<TableCellData> dataSource)
		{
			var keyword = _searchBar.Text;
			if (string.IsNullOrEmpty (keyword)) {
				keyword = "";
			}
			keyword = keyword.Trim ().ToLower ();
			if (string.IsNullOrWhiteSpace (keyword)) {
				return dataSource;
			}

			var d = dataSource.Where (t => {
				if (t.DisplayName.ToLower ().IndexOf (keyword) != -1 || t.Email.ToLower ().IndexOf (keyword) != -1)
					return true;
				return false;
			});
			return d;
		}

		public class ContactTableCell : SendingResultPage.MemberCell
		{
			public ContactTableCell () : base ()
			{
				_nameLbl.FontSize = _initialLbl.FontSize = BlrtStyles.CellTitleFont.FontSize;
				_nameLbl.TextColor = BlrtStyles.BlrtBlackBlack;
				//_reminder.GestureRecognizers.Add (new TapGestureRecognizer ((v) => {
				//	var bind = BindingContext as TableCellData;
    //                ShowReminder (BlrtUtil.IsValidEmail (bind.Email));
				//}));

			}
		}
		public class ContactTableHeaderCell : SendingResultPage.MemberHeaderCell
		{
			public ContactTableHeaderCell () : base ()
			{
				this.View.BackgroundColor = BlrtStyles.BlrtGray3;
			}
		}

		public class TableSectionData : ObservableCollection<TableCellData>
		{
			public string Text { get; set; }
			public string Short { get; set; }
		}

		public class TableCellData
		{
			public bool Removed { get; set; }
			public string DisplayName { get; set; }
			public string Email { get; set; }
			public string Initials { get; set; }
			public Color InitialBackgroundColor { get; set; }
			public BlrtContact Contact { get; set; }
			public string AvatarPath { get; set; }
			public string Image1Source { get; set; }
			public string Image2Source { get; set; }
			public bool Image1Visible { get; set; }
			public bool Image2Visible { get; set; }
#if __ANDROID__
			public bool LabelAddedVisible { get; set; }
			public bool TickVisible { get; set; }
#endif
			public DateTime BlrtUserCreatedAt { get; set; }

			public TableCellData ()
			{
				InitialBackgroundColor = BlrtStyles.BlrtGreen;
			}
		}
	}
}

