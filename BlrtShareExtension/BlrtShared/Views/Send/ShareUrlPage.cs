﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Send
{
	public class ShareUrlPage : BlrtContentPage
	{
		public const int IconSize = 63;
		public event EventHandler FbShareClicked, MessageShareClicked, EmailShareClicked, ShareWithClicked, DonePressed;

		public override string ViewName {
			get {
				return "Conversation_Send_Share";
			}
		}

		public override void TrackPageView ()
		{
			BlrtData.Helpers.BlrtTrack.TrackScreenView (ViewName, false, BlrtData.Helpers.BlrtTrack.LastConvoDic);
		}

		public ShareUrlPage (string conversationUrl) : base ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("send_url_title", "send_screen");
			this.BackgroundColor = BlrtStyles.BlrtGray2;
			this.ToolbarItems.Add (new ToolbarItem ("Done", null, () => {
				if (DonePressed != null) {
					DonePressed (this, EventArgs.Empty);
				}
			}));

#if __IOS__
			_progressSection = new ProgressSection ();
			_progressArea = _progressSection.ProgressArea;
#endif

			var convoLbl = new Label () {
				Font = BlrtStyles.CellTitleFont,
				Text = "Conversation URL",
				YAlign = TextAlignment.Center
			};

			var convoUrlLbl = new Label () {
				Font = BlrtStyles.CellDetailFont,
				TextColor = BlrtStyles.BlrtGray6,
				Text = conversationUrl
			};

			var copyBtn = new Button () {
				HeightRequest = 27,
				BackgroundColor = Color.Transparent,
				TextColor = BlrtStyles.BlrtAccentBlueHighlight,
				Text = "Copy   "
			};

			if (Device.OS == TargetPlatform.Android) {
				copyBtn.FontSize = BlrtStyles.CellTitleFont.FontSize;
				copyBtn.HeightRequest = -1;
			}

			copyBtn.Clicked += (sender, e) => {
#if __IOS__
				UIKit.UIPasteboard.General.String = conversationUrl;
#elif __ANDROID__
				Android.Content.ClipData clip = Android.Content.ClipData.NewPlainText ("url", conversationUrl);
				Android.Content.ClipboardManager clipboard = (Android.Content.ClipboardManager)global::Android.App.Application.Context.GetSystemService (Android.Content.Context.ClipboardService);
				clipboard.PrimaryClip = clip;
#endif
				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("copy_url_title", "send_screen"),
					BlrtUtil.PlatformHelper.Translate ("copy_url_message", "send_screen"),
					BlrtUtil.PlatformHelper.Translate ("copy_url_cancel", "send_screen")
				);
			};

			var sep = new BoxView () { BackgroundColor = BlrtStyles.BlrtGray3, HeightRequest = 0.5f };

			var convoSec = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},

				Padding = 0,
				RowSpacing = 5,
				ColumnSpacing = 0,
				Children = {
					{ convoLbl,0, 0 },
					{ copyBtn,1, 0 },
					{ convoUrlLbl,0, 1 },
					{ sep,0, 2 }
				}
			};

			Grid.SetColumnSpan (sep, 2);

			var convoDescriptionLbl = new Label () {
				Font = BlrtStyles.CellTitleFont,
				TextColor = BlrtStyles.BlrtGray5,
				Text = BlrtUtil.PlatformHelper.Translate ("share_detail_sharewith", "send_screen")
			};

			var messenger = new ButtonWihtText () {
				Text = BlrtUtil.PlatformHelper.Translate ("fb_icon_share", "send_screen"),
				Image = "share_fbmessenger.png"
			};

			var sms = new ButtonWihtText () {
				Text = BlrtUtil.PlatformHelper.Translate ("message_icon_share", "send_screen"),
				Image = "share_messenger.png"
			};

			var email = new ButtonWihtText () {
				Text = BlrtUtil.PlatformHelper.Translate ("email_icon_share", "send_screen"),
				Image = "share_email.png"
			};

			var share = new ButtonWihtText () {
				Text = BlrtUtil.PlatformHelper.Translate ("share_icon_share", "send_screen"),
				Image = "share_others.png"
			};

			messenger.Clicked += delegate {
				if (FbShareClicked != null)
					FbShareClicked (this, EventArgs.Empty);
			};
			sms.Clicked += delegate {
				if (MessageShareClicked != null)
					MessageShareClicked (this, EventArgs.Empty);
			};
			email.Clicked += delegate {
				if (EmailShareClicked != null)
					EmailShareClicked (this, EventArgs.Empty);
			};
			share.Clicked += delegate {
				if (ShareWithClicked != null)
					ShareWithClicked (this, EventArgs.Empty);
			};

			var icons = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				Spacing = Device.Idiom == TargetIdiom.Phone ? 5 : 10,
				Children = {
					messenger,
					sms,
					email,
					share
				}
			};

			var contentSec = new StackLayout () {
				BackgroundColor = BlrtStyles.BlrtWhite,
				Orientation = StackOrientation.Vertical,
				Padding = new Thickness (15, 10, 0, 10),
				Spacing = 15,
				Children = {
					convoSec,
					convoDescriptionLbl,
					icons
				}
			};

			Content = new StackLayout () {
				Orientation = StackOrientation.Vertical,
				Children = {
					//_progressArea,
					new BoxView(){HeightRequest = 25},
					contentSec
				}
			};

#if __IOS__
			this.SetProgress (1, false);
			this._progressSection.IsTitleVisible = false;
#endif
		}



#if __IOS__
		ProgressSection _progressSection;
		StackLayout _progressArea;
		public bool IsProgressVisible {
			get { return _progressArea.IsVisible; }
			set { _progressArea.IsVisible = value; }
		}

		public void SetProgress(float progress, bool animated = true)
		{
			_progressSection.SetProgress (progress, animated);
		}
#endif
		public class ButtonWihtText: ContentView
		{
			public string Text{
				get{
					return _label.Text;
				}set{
					_label.Text = value;
				}
			}

			public string Image{
				set{
					_image.Source = value;
				}
			}

			public event EventHandler Clicked;
			Image _image;
			Label _label;
			public ButtonWihtText(): base(){

				_image = new Image();
				_image.GestureRecognizers.Add(new TapGestureRecognizer((v,o)=>{
					if(Clicked!=null)
						Clicked(this,EventArgs.Empty);
				}));

				_label = new Label(){
					TextColor = BlrtStyles.BlrtGray6,
					FontSize = 12.0d,
					LineBreakMode = LineBreakMode.WordWrap,
					XAlign = TextAlignment.Center,
					WidthRequest = ShareUrlPage.IconSize
				};

				var layout = new StackLayout(){
					Orientation = StackOrientation.Vertical,
					Padding = 0,
					Children = {
						_image,
						_label
					}
				};

				Content = layout;
			}
		}
	}
}

