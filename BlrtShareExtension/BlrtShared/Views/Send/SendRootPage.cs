﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using BlrtData.Views.CustomUI;
using System.Linq;
using Parse;

namespace BlrtData.Views.Send
{
	public class SendRootPage : BlrtContentPage
	{
		public event EventHandler DonePressed;

#if __ANDROID__
		public event EventHandler LoadUsers;
		public bool isDismissed;
#elif __IOS__
		public event EventHandler AddClicked;
#endif

		public override void TrackPageView ()
		{
			if (_conversation == null)
				return;
			var dic = new Dictionary<string, object> ();
			var rel = _conversation.LocalStorage.DataManager.GetConversationUserRelation (
				_conversation.ObjectId,
				ParseUser.CurrentUser.ObjectId
			);

			if (rel != null) {
				dic.Add ("isOwner", rel.CreatorId == ParseUser.CurrentUser.ObjectId);
			} else if (!_conversation.OnParse) {
				dic.Add ("isOwner", true);
			} else {
				Console.WriteLine ("No relation found");
			}
			dic.Add ("convoId", _conversation.ObjectId);
			BlrtData.Helpers.BlrtTrack.TrackScreenView ("Conversation_Send", false, dic);
		}

#if __ANDROID__
		public EventHandler GetSelectedContact;
		public EventHandler<bool> SendReminder;
		public IEnumerable<BlrtData.Contacts.ConversationNonExistingContact> SelectedContacts {
			get; set;
		}
#elif __IOS__
		View _convoNameSec;
		FormsEntry _convoNameEdit;
		ConversationMemberSection _convoMemberSec;
		ConversationOptionsActivitySection _convoOptionsActivitySec;
		public const int PaddingLeft = 19;
		public event EventHandler OnRetrySend;
		public event EventHandler OnFacebookSend, OnPhoneNumberSend;
#endif

		public SendRootPage () : base ()
		{
#if __ANDROID__
			Title = BlrtUtil.PlatformHelper.Translate ("Add people");
			BackgroundColor = BlrtStyles.BlrtGray4;

			ToolbarItems.Add (new ToolbarItem ("Next", null, () => {
				if (DonePressed != null) {
					DonePressed (this, EventArgs.Empty);
				}
			}));
#elif __IOS__
			Title = BlrtUtil.PlatformHelper.Translate ("send_root_title", "send_screen");
			this.BackgroundColor = BlrtStyles.BlrtGray2;

			this.ToolbarItems.Add(new ToolbarItem("Done", null, ()=>{
				if(DonePressed!=null){
					DonePressed(this, EventArgs.Empty);
				}
			}));

			var addPeople = GetAddpeopleGrid ();

			addPeople.GestureRecognizers.Add(new TapGestureRecognizer(){
				Command = new Command(()=>{
					OpenSend();
				})	
			});

			//progress View
			_progressSection = new ProgressSection ();
			_progressArea = _progressSection.ProgressArea;

			//conversation name
			_convoNameEdit = new FormsEntry () {
				BackgroundColor = BlrtStyles.BlrtWhite,
				CornerRadious = 0,
				HeightRequest = 40,
				FontSize = (float)BlrtStyles.CellContentFont.FontSize,
				Keyboard = Keyboard.Text
			};
			var nameContentView = new ContentView(){
				BackgroundColor = BlrtStyles.BlrtWhite,
				Padding = new Thickness(PaddingLeft,0),
				Content = _convoNameEdit
			};

			var conNameLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("convo_name_lbl", "send_screen"),
				Font = BlrtStyles.CellTitleFont
			};

			_convoNameSec = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				Padding = new Thickness(0,20,0,0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				Children = {
					{ new ContentView{Padding = new Thickness(PaddingLeft,0,PaddingLeft,10), Content = conNameLbl},0,0},
					{ new BoxView(){BackgroundColor = BlrtStyles.BlrtGray3, HeightRequest=0.5f, VerticalOptions = LayoutOptions.End},0, 1 },
					{nameContentView,0,2},
					{ new BoxView(){BackgroundColor = BlrtStyles.BlrtGray3, HeightRequest=0.5f, VerticalOptions = LayoutOptions.End},0, 2 },
				}
			};

			_convoMemberSec = new ConversationMemberSection();
			_convoOptionsActivitySec = new ConversationOptionsActivitySection ();
			_convoOptionsActivitySec.OnRetrySend += (sender, e) => {
				OnRetrySend?.Invoke(sender,e);
			};
			_convoOptionsActivitySec.OnFacebookSend += (sender, e) => {
				OnFacebookSend?.Invoke(sender,e);
			};
			_convoOptionsActivitySec.OnPhoneNumberSend += (sender, e) => {
				OnPhoneNumberSend?.Invoke(sender,e);
			};

			//page layout
			var contentGrid = new Grid () {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.Fill,
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},

				Padding = 0,
				RowSpacing = 0,
				ColumnSpacing = 0,
				Children = { 
					{ _progressArea,0, 0 },
					{ _convoNameSec,0, 1 },
					{ new BoxView(){HeightRequest=20},0, 2 },
					{ new BoxView(){BackgroundColor = BlrtStyles.BlrtGray5, HeightRequest=0.5f, VerticalOptions = LayoutOptions.End},0, 2 },
					{ addPeople,0, 3 },
					{ new BoxView(){HeightRequest=20},0, 4 },
					{ new BoxView(){BackgroundColor = BlrtStyles.BlrtGray5, HeightRequest=0.5f, VerticalOptions = LayoutOptions.Start},0, 4 },
					{ new BoxView(){BackgroundColor = BlrtStyles.BlrtGray5, HeightRequest=0.5f, VerticalOptions = LayoutOptions.End},0, 4 },
					{ _convoOptionsActivitySec,0, 7 },
					{ _convoMemberSec,0, 8 }
				}
			};

			var scroll = new ScrollView () {
				Padding = 0,
				Content = contentGrid
			};
			scroll.Scrolled += (sender, e) => {
				addPeople.ForceUpdateBackgroundColor();
			};

			this.Content = scroll;

			_convoNameEdit.Focused += _convoNameEdit_Focused;
#endif
		}

#if __IOS__
		public void UnFocusNameField(){
			_convoNameEdit.Unfocus ();
		}

		void _convoNameEdit_Focused (object sender, FocusEventArgs e)
		{
			if(!string.IsNullOrEmpty(_convoNameEdit.Text)){
				_convoNameEdit.SelectAll ();
			}
		}

		public void Reload(){
			_convoOptionsActivitySec.ReloadList ();
			_convoMemberSec.ReloadList ();
			RecalculateViewAppear ();
		}

		protected override void OnAppearing ()
		{
			RecalculateViewAppear ();
			if(ConversatonVisible){
				_convoNameEdit.Focus ();
			}
			base.OnAppearing ();
		}

		public void RecalculateViewAppear(){
			if (_conversation == null)
				return;

			if(_conversation.BondCount>1|| _conversation.SendingContacts?.Count(x=>x.Status== ContactState.Sending)>0){
				Device.BeginInvokeOnMainThread (() => {
					ConversatonVisible = false;
					ConversatonMemberVisible = true;
					_convoMemberSec.ReloadList ();
				});
			}else{
				ConversatonVisible = !ForceHideConversationName;
				ConversatonMemberVisible = false;
			}
		}

		public bool ForceHideConversationName;

		public string ConversationName{ 
			get{
				if(_convoNameEdit!=null){
					return _convoNameEdit.Text;
				}
				return "";
			}
			set{
				if(_convoNameEdit!=null){
					_convoNameEdit.Text = value;
				}		
			}
		}

		public bool ConversatonVisible{
			get{
				return _convoNameSec.IsVisible;
			}set{
				_convoNameSec.IsVisible = value;
			}
		}

		public bool ConversatonMemberVisible{
			get{
				return _convoMemberSec.IsVisible;
			}set{
				_convoMemberSec.IsVisible = value;
			}
		}

		ProgressSection _progressSection;
		StackLayout _progressArea;

		public bool IsProgressVisible {
			get { return _progressArea.IsVisible; }
			set { _progressArea.IsVisible = value; }
		}

		public void SetProgress(float progress, bool animated = true)
		{
			_progressSection.SetProgress (progress, animated);
		}

		void OpenSend(){
			if(AddClicked!=null){
				AddClicked(this, EventArgs.Empty);
			}
		}

		ShareRootGrid GetAddpeopleGrid(){
			var titleLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("add_people_title_lbl", "send_screen"),
				FontSize = BlrtStyles.CellTitleFont.FontSize,
			};

			var detailLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate("add_people_detail_lbl", "send_screen"),
				LineBreakMode = LineBreakMode.WordWrap,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				TextColor = BlrtStyles.BlrtGray7,
				YAlign = TextAlignment.Center
			};

			var image = new Image () {
				Source = "convo_addpeople.png"
			};

			var btn = new Label () {
				Text = "Add",
				TextColor = BlrtStyles.BlrtAccentBlueHighlight,
				YAlign = TextAlignment.Center,
				FontSize = BlrtStyles.CellTitleFont.FontSize
			};

			var grid = new ShareRootGrid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) }
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				BackgroundColor = BlrtStyles.BlrtGreenLightLight,
				Padding = new Thickness(PaddingLeft,10,10,10),
				RowSpacing = 10,
				ColumnSpacing  = 15,
				Children = { 
					{
						titleLbl,0, 0
					}, {
						detailLbl, 1, 1
					}, {
						image, 0,1
					},{
						btn, 2,0
					}
				}
			};

			Grid.SetColumnSpan (titleLbl, 2);
			Grid.SetRowSpan (btn, 2);

			return grid;
		}
#endif

		BlrtData.Conversation _conversation;
		public void SetConversation (BlrtData.Conversation conversation)
		{
			if (conversation == _conversation)
				return;
			_conversation = conversation;

#if __IOS__
			_convoOptionsActivitySec.SetConversation (_conversation);
			_convoMemberSec.SetConversation (_conversation);

			//register subscriber
			MessagingCenter.Subscribe<Object,bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), (o,b) => {
				Reload();
			});
			RecalculateViewAppear ();
#endif
		}

		public BlrtData.Conversation GetConversation ()
		{
			return _conversation;
		}

#if __ANDROID__
		public void Reload ()
		{
			LoadUserList ();
		}

		public void LoadUserList ()
		{
			LoadUsers?.Invoke (this, EventArgs.Empty);
		}

		public void SendReminders (bool s)
		{
			SendReminder?.Invoke (this, s);
		}
#endif

	}

#if __IOS__
	public class ShareRootGrid: MR.Gestures.Grid {

		public event EventHandler BackgroundColorChanged;

		Color _backgroundColor;
		public new Color BackgroundColor{
			get{
				return _backgroundColor;
			}
			set{
				if(value!=_backgroundColor){
					_backgroundColor = value;
					base.BackgroundColor = value;
					if (BackgroundColorChanged != null)
						BackgroundColorChanged (this, EventArgs.Empty);
				}
			}
		}

		public void ForceUpdateBackgroundColor(){
			base.BackgroundColor = this.BackgroundColor;
		}

		public ShareRootGrid():base(){
			this.Down += (sender, e) => {
				//if it's disabled, dont change color
				if(this.BackgroundColor == BlrtStyles.BlrtGray2)
					return;
				base.BackgroundColor = BlrtStyles.BlrtGreen;
			};

			this.Up += (sender, e) => {
				if(this.BackgroundColor == BlrtStyles.BlrtGray2)
					return;
				base.BackgroundColor = this.BackgroundColor;
			};
		}
 	}

#endif

}

