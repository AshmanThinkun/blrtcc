using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using BlrtAPI;
using BlrtData.Views.CustomUI;

#if __IOS__
using System.Threading.Tasks;
using BlrtData;
using System.Threading;
using BlrtData.Views.Send;
#endif


namespace BlrtData.Views.Send
{
	public class SendingResultPage : ContentPage
	{
		public event EventHandler DonePressed;

#if __ANDROID__
		public BlrtData.Conversation _conversation { get; set; }
		public event EventHandler<bool> LoadResult;
		public event EventHandler<BlrtData.Contacts.ConversationNonExistingContact []> ReSend;

		public SendingResultPage ()
		{
			BackgroundColor = BlrtStyles.BlrtGray2;
			Title = "Send Results";

			ToolbarItems.Add (new ToolbarItem ("Done", "", () => {
				if (DonePressed != null)
					DonePressed (this, EventArgs.Empty);
			}));
		}

		public void Resend (BlrtData.Contacts.ConversationNonExistingContact [] e)
		{
			ReSend?.Invoke (this, e);
		}

		public void Reload (bool show = false)
		{
			LoadResult?.Invoke (this, show);
		}

#elif __IOS__
		ListView _listView;
		public SendingResultPage (string[] emails, BlrtAPI.APIConversationUserRelationCreateResponse result)
		{
			this.BackgroundColor = BlrtStyles.BlrtGray2;
			Title = "Send Conversation Results";
			var progressBar = new ContentView () {
				Padding = new Thickness (0, -5, 0, 0),
				Content = new ProgressBar (){ Progress = 1 }
			};

			ToolbarItems.Add(new ToolbarItem("Done", "", ()=>{
				if(DonePressed != null)
					DonePressed ( this, EventArgs.Empty );
			}));


			_listView = new ListView () {
				BackgroundColor		= BlrtStyles.BlrtGray2,
				HasUnevenRows		= true,
				ItemsSource			= GetData(emails, result),
				ItemTemplate		= new DataTemplate (typeof (MemberCell)),
				IsGroupingEnabled	= true,
				GroupHeaderTemplate = new DataTemplate (typeof(MemberHeaderCell)),
				SeparatorColor = BlrtStyles.BlrtGray3
			};

			_listView.ItemSelected += (sender, e) => {
				_listView.SelectedItem = null;
			};

			this.Content = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
				},
				Padding = 0,
				ColumnSpacing = 0,
				RowSpacing = 0,
				Children = {
					{progressBar, 0,0},
					{_listView, 0,1},
				}
			};
		}

		public IEnumerable<ResultSection> GetData(string[] emails, BlrtAPI.APIConversationUserRelationCreateResponse result){
			var list = new List<ResultObject> ();

			for (int i = 0; i < emails.Length && i < result.RelationStatuses.Length; ++i) {
				list.Add(new ResultObject(
					result.RelationStatuses[i].Name,
					emails[i],
					result.RelationStatuses[i].Result
				));
			}

			var rtn = list.GroupBy (t => t.Result).Select(g=>{
				var sec = new ResultSection(){
					ResultType = g.Key
				};

				foreach(var o in g){
					sec.Add(o);
				}

				string title="";
				switch(g.Key){
					case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.AlreadySent:
						title = BlrtUtil.PlatformHelper.Translate("Already in conversation");
						break;
					case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentReachable:
						title = BlrtUtil.PlatformHelper.Translate("Blrt invitation sent to email");
						break;
					case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUnreachable:
						//title = PlatformHelper.Translate("Facebook contacts not on Blrt", "send result screen");
						//break;
					case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUser:
						title = BlrtUtil.PlatformHelper.Translate("Blrt sent to existing users");
						break;
				}	
				sec.Text = title;
				return sec;
			});

			var listRtn = rtn.ToList ();
			listRtn.Sort (( x,  y) => x.ResultType.CompareTo (y.ResultType));

			return listRtn;
		}

		public class ResultSection: ObservableCollection<ResultObject>{
			public string Text{get;set;}
			public APIConversationUserRelationCreate.RelationStatus.ResultEnum ResultType{get;set;} 
		}

		public class ResultObject {
		
			public string DisplayName{get;set;}
			public string Email{get;set;}
			public string Initials{get;set;}
			public Color InitialBackgroundColor{get;set;}
			public BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum Result{get;set;}
			public bool ImageVisible{ get; set;}

			public ResultObject(string name, string email, BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum result){
			
				this.DisplayName = !string.IsNullOrWhiteSpace(name) ? name : email;
				this.Email = email;
				this.Result = result;

				if(result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentReachable 
					|| result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUnreachable 
					|| result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.Invalid){
					this.InitialBackgroundColor = BlrtStyles.BlrtGray6;
				}else{
					this.InitialBackgroundColor = BlrtStyles.BlrtGreen;
				}

				this.Initials = BlrtUtil.GetInitials(this.DisplayName);
				this.ImageVisible = false;
			}
		
			public override string ToString () {

				if(DisplayName != Email && !string.IsNullOrWhiteSpace(DisplayName))
					return string.Format ("{0} <{1}>", DisplayName, Email).Trim();

				return string.Format ("{1}", DisplayName, Email).Trim();
			}
		}

#endif

		public class MemberHeaderCell : ViewCell
		{
			Label _nameLbl;
			public MemberHeaderCell () : base ()
			{
				_nameLbl = new Label () {
					XAlign = TextAlignment.Start,
					YAlign = TextAlignment.Center,
					FontSize = BlrtStyles.FontSize14.FontSize,
					LineBreakMode = LineBreakMode.TailTruncation,
					TextColor = BlrtStyles.BlrtGray6
				};

				this.View = new ContentView () {
					Padding = new Thickness (10, 3),
					Content = _nameLbl,
					BackgroundColor = BlrtStyles.BlrtGray2,
				};

				_nameLbl.SetBinding (Label.TextProperty, "Text");
			}
		}

		public class MemberCell : ViewCell
		{
			public static readonly int CellHeight = 60;

			protected Label _nameLbl, _valueLbl, _initialLbl;

#if __ANDROID__
			protected Label _addedLbl, _reminder;
			StackLayout images;
#elif __IOS__
			GestureRecognizer _avatarTapGesture;
#endif

			protected RoundedImageView _roundBox;
			protected Image _img1, _img2;

			//GestureRecognizer _avatarTapGesture;

			public MemberCell () : base ()
			{
				_nameLbl = new Label () {
					XAlign = TextAlignment.Start,
					YAlign = TextAlignment.End,
					FontSize = BlrtStyles.FontSize14.FontSize,
					LineBreakMode = LineBreakMode.TailTruncation,
					TextColor = BlrtStyles.BlrtGray6
				};

				_valueLbl = new Label () {
					YAlign = TextAlignment.Start,
					XAlign = TextAlignment.Start,
					TextColor = BlrtStyles.BlrtGray5,
#if __ANDROID__
					LineBreakMode = LineBreakMode.TailTruncation,
#endif
					FontSize = BlrtStyles.CellContentFont.FontSize
				};

#if __ANDROID__
				_reminder = new BlrtApp.UnderlinedLabel () {
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.Start,
					TextColor = BlrtStyles.BlrtAccentBlue,
					FontSize = BlrtStyles.CellContentFont.FontSize,
					Text = "Send reminder email",
					IsVisible = false,
				};

				_addedLbl = new Label () {
					YAlign = TextAlignment.Start,
					XAlign = TextAlignment.Start,
					TextColor = BlrtStyles.BlrtGreen,
					Text = "Already added",
					FontSize = BlrtStyles.CellContentFont.FontSize,
					IsVisible = false
				};
#endif
				_initialLbl = new Label () {
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.Center,
					TextColor = BlrtStyles.BlrtWhite,
					FontSize = BlrtStyles.FontSize14.FontSize
				};

				_roundBox = new RoundedImageView () {
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
#if __ANDROID__
					CornerRadius = CellHeight * 0.5f,
					HeightRequest = CellHeight,
					WidthRequest = CellHeight,
#elif __IOS__
					CornerRadius = (CellHeight-10)*0.5f,
					HeightRequest = CellHeight-10,
					WidthRequest = CellHeight-10,

#endif
					Aspect = Aspect.AspectFill
				};

				_img1 = new Image () {
				};
				_img2 = new Image () {
				};

#if __IOS__
				_img2.GestureRecognizers.Add(new TapGestureRecognizer((v)=>{
					if(!_img2.IsVisible)
						return;
					if(this.BindingContext is SendingPage.SelectContactSource){
						var bind = this.BindingContext as SendingPage.SelectContactSource;
						bind.ImageClick();
					}
				}));
#endif

				var images = new StackLayout () {
					Padding = 0,
					Orientation = StackOrientation.Horizontal,
					Spacing = 5,
					Children ={
						_img1,_img2
						#if __ANDROID__
						, _reminder
						#endif
					}
				};

				this.View = new Grid () {
					VerticalOptions = LayoutOptions.Center,
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) }
						#if __ANDROID__
						,new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) }
						#endif
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
						new RowDefinition () { Height = new GridLength (1, GridUnitType.Auto) }
					},
					Padding = new Thickness (20, 5, 20, 5),
					ColumnSpacing = Device.Idiom == TargetIdiom.Tablet ? 20 : 10,
					RowSpacing = 0,
					Children = {
						{_roundBox, 0,0},
						{_initialLbl, 0,0},
						{_nameLbl, 1, 0},
						{_valueLbl, 1,1},
						#if __ANDROID__
						{_addedLbl, 1,2},
						#endif
						{images,2,0}
					},
					BackgroundColor = BlrtStyles.BlrtGray2,
					HeightRequest = CellHeight
				};

#if __IOS__
				Grid.SetRowSpan(_roundBox,2);
				Grid.SetRowSpan(_initialLbl,2);
				Grid.SetRowSpan(images,2);

				_avatarTapGesture = (new TapGestureRecognizer(){
					Command = new Command(()=>{
						var bind = this.BindingContext as BlrtData.Views.Send.SendingPage.SelectContactSource;
						if(bind!=null){
							bind.OpenProfile();
						}
					})	
				});

#endif

				_roundBox.SetBinding (RoundedImageView.BackgroundColorProperty, "InitialBackgroundColor");
				_roundBox.SetBinding (RoundedImageView.SourceProperty, "AvatarPath");
				_initialLbl.SetBinding (Label.TextProperty, "Initials");
				_valueLbl.SetBinding (Label.TextProperty, "Email");
				_nameLbl.SetBinding (Label.TextProperty, "DisplayName");
				_valueLbl.SetBinding (Label.FormattedTextProperty, "FormattedDetail");
				_img1.SetBinding (Image.SourceProperty, "Image1Source");
				_img2.SetBinding (Image.SourceProperty, "Image2Source");
				_img1.SetBinding (Image.IsVisibleProperty, "Image1Visible");
				_img2.SetBinding (Image.IsVisibleProperty, "Image2Visible");
#if __ANDROID__
				_addedLbl.SetBinding (Label.IsVisibleProperty, "LabelAddedVisible");
#endif

			}

			protected override void OnBindingContextChanged ()
			{
				base.OnBindingContextChanged ();

#if __IOS__
				if(this.BindingContext is BlrtData.Views.Send.SendingPage.SelectContactSource){
					_roundBox.GestureRecognizers.Clear ();
					_roundBox.GestureRecognizers.Add (_avatarTapGesture);
				}
#elif __ANDROID__
				if (BindingContext is ContactTablePage.TableCellData) {
					var bind = BindingContext as ContactTablePage.TableCellData;
					_reminder.IsVisible = false;
					if (bind.LabelAddedVisible) {
						Grid.SetRowSpan (_roundBox, 3);
						Grid.SetRowSpan (_initialLbl, 3);
						Grid.SetRowSpan (images, 3);
						if (!bind.Image2Visible) {
							if (BlrtUtil.IsValidEmail (bind.Email))
								_reminder.Text = "Send reminder email";
							else
								_reminder.Text = "Send reminder sms";
							_reminder.IsVisible = true;
						}
					} else {
						Grid.SetRowSpan (_roundBox, 2);
						Grid.SetRowSpan (_initialLbl, 2);
						Grid.SetRowSpan (images, 2);

					}
				}
#endif
			}
		}
	}
}

