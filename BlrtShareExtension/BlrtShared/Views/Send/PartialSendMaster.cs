﻿#if __ANDROID__
using System;
using System.Linq;
using BlrtData.Contacts;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;
using Xamarin.Forms;
using Parse;

namespace BlrtData.Views.Send
{
	/// <summary>
	/// /* This partial class contains the share code of send master controller*/
	/// </summary>
#if __iOS__
	public partial class BlrtSendMasterController
#else
	public partial class SendMasterNavPage
#endif
	{

		public BlrtData.Conversation BlrtConversation {
			get { return _conversation; }
		}

		void OnRootPageRetrySend (object sender, EventArgs e)
		{
			var selectedList = _conversation.SendingContacts?.Where (x => x.Status == ContactState.FailedToSend).Select (y => {
				var contactInfo = y.SelectedInfo;
				var newcontact = new BlrtContact () {
					Name = null,
				};
				newcontact.Add (contactInfo);
				var match = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
				if (match == null) {
					match = newcontact;
				}
				var selectInfo = contactInfo;
				foreach (var info in match.InfoList) {
					if (info.Equals (contactInfo)) {
						selectInfo = info;
						break;
					}
				}
				return new ConversationNonExistingContact (selectInfo, match);
			});

			if (selectedList.Count () < 1)
				return;

			foreach (var i in selectedList) {
				_sender.AddContactToField (i);
			}
			OpenSendPage (sender, e);
		}

		void OnSenderContactRemoved (object sender, ConversationNonExistingContact e)
		{
			if (_conversation.UnSendContacts != null &&
				_conversation.UnSendContacts.Length > 0 &&
				_conversation.UnSendContacts.Contains (e.SelectedInfo.ContactValue)) {
				_conversation.UnSendContacts = _conversation.UnSendContacts.Except (new string [] { e.SelectedInfo.ContactValue }).ToArray ();
				_conversation.LocalStorage.DataManager.SaveLocal ();
			}
		}

		public async Task ParseAction (ConversationNonExistingContact [] contacts, string conversationName = null)
		{
			// add sending contacts
			if (_conversation.SendingContacts == null) {
				_conversation.SendingContacts = new List<ContactStatus> ();
			}
			var now = DateTime.Now;
			foreach (var co in contacts) {
				var coStatus = new ContactStatus () {
					SelectedInfo = co.SelectedInfo,
					UpdateAt = now,
					Status = ContactState.Sending
				};

				var match = _conversation.SendingContacts.FirstOrDefault (x => x.Equals (coStatus));
				if (match != null) {
					match.Status = coStatus.Status;
					match.UpdateAt = coStatus.UpdateAt;
				} else {
					_conversation.SendingContacts.Add (coStatus);
				}
			}
			_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.InProgress;

#if __ANDROID__
			_sendResult.Reload ();
#elif __IOS__
			_sendRootPage.Reload ();
#endif
			_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
			Xamarin.Forms.MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), false);

			try {
				var param = new BlrtAPI.APIConversationUserRelationCreate.Parameters () {
					ConversationID = _conversation.ObjectId,
#if __ANDROID__
					Note = _sender.DescriptionFieldText,
					DontAddNoteInConvo = _sender.DontAddNoteInConvo,
#endif
					Relations = (from c in contacts
								 select new BlrtAPI.APIConversationUserRelationCreate.Relation (
									 c.SelectedInfo.ParseContactType,
									 c.SelectedInfo.ContactValue,
									 c.Contact.UsableName
								 )).ToArray ()
				};

				if (conversationName != null)
					param.ConversationName = conversationName;

				var request = new BlrtAPI.APIConversationUserRelationCreate (param);

				if (!await request.Run (new CancellationTokenSource (10000).Token))
					throw request.Exception ?? new Exception ("Request failed :(");
				if (!request.Response.Success)
					throw request.Exception ?? new Exception (request.Response.Error.Code.ToString ());

				var result = request.Response;

				try {
					await FetchObjects (result);
				} catch (Exception e) {
					LLogger.WriteEvent ("BlrtParseActions.SendConversationToContactInfo.FetchObjects", "failed", e.ToString ());
					Xamarin.Insights.Report (e);
					_refreshFailed = true;
				}
				_isSending = false;
				ShowResults (contacts, result);

#if __IOS__
				BlrtManager.Query.TryRunQuery ();
#endif
			} catch (Exception e) {

				LLogger.WriteEvent ("BlrtParseActions.SendConversationToContactInfo", "failed", e.ToString ());
				Xamarin.Insights.Report (e);
				foreach (var contact in contacts) {
					var match = _conversation.SendingContacts.FirstOrDefault (x => x.SelectedInfo.Equals (contact.SelectedInfo));
					if (match != null) {
						match.Status = ContactState.FailedToSend;
						match.UpdateAt = now;
					} else {
						_conversation.SendingContacts.Add (new ContactStatus () {
							Status = ContactState.FailedToSend,
							UpdateAt = now,
							SelectedInfo = contact.SelectedInfo
						});
					}
				}
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Failed;

				_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
				//update conversation screen
				_isSending = false;

				Device.BeginInvokeOnMainThread (() => {
					if (e.Message == BlrtAPI.APIConversationUserRelationCreateError.disabledByCreator.ToString ()) {
						BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("conversation_lock_title", "conversation_screen"),
							BlrtUtil.PlatformHelper.Translate ("conversation_lock_msg", "conversation_screen"),
							BlrtUtil.PlatformHelper.Translate ("conversation_lock_accept", "conversation_screen")
						)).FireAndForget ();

					} else {
						BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("failed_to_send_title", "send_screen"),
							BlrtUtil.PlatformHelper.Translate ("failed_to_send_message", "send_screen"),
							BlrtUtil.PlatformHelper.Translate ("failed_to_send_accept", "send_screen")
						)).FireAndForget ();
					}
				});

			}
#if __ANDROID__
			_sendResult.Reload (true);
			Query.BlrtConversationMetaQuery.RunQuery (BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId));
#endif
			Xamarin.Forms.MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), _refreshFailed);
		}

		public void ShowResults (ConversationNonExistingContact [] contacts, BlrtAPI.APIConversationUserRelationCreateResponse results)
		{
			//show result on send root page
			var list = new List<BlrtContact> ();
			var dictionary = new Dictionary<BlrtContactInfo, string> ();
			var nonUserPhoneNumbers = new List<BlrtContactInfo> ();

			int i = -1;
			var now = DateTime.Now;
			foreach (var contact in contacts) {
				i++;
				var result = results.RelationStatuses [i];

				if (contact.Contact.Name != null && !string.IsNullOrWhiteSpace (result.Name))
					contact.Contact.Name = result.Name;

				ContactState state;

				switch (result.Result) {
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentReachable:
					state = ContactState.ConversationSentReachable;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUnreachable:
					state = ContactState.ConversationSentUnreachable;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.SentUser:
					state = ContactState.ConversationSentUser;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.AlreadySent:
					state = ContactState.ConversationAlreadySent;
					break;
				case BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.BlockedByUser:
					state = ContactState.Blocked;
					break;
				default:
					state = ContactState.InvalidContact;
					break;
				}

				var match = _conversation.SendingContacts.FirstOrDefault (x => x.SelectedInfo.Equals (contact.SelectedInfo));
				if (match != null) {
					match.Status = state;
					match.UpdateAt = now;
				} else {
					_conversation.SendingContacts.Add (match = new ContactStatus () {
						Status = state,
						UpdateAt = now,
						SelectedInfo = contact.SelectedInfo
					});
				}

				contact.Contact.LastUsed = now;

				list.Add (contact.Contact);
				dictionary.Add (match.SelectedInfo, state.ToString ());

				if (match.SelectedInfo.ContactType == BlrtContactInfoType.PhoneNumber && state == ContactState.ConversationSentUnreachable) {
					nonUserPhoneNumbers.Add (match.SelectedInfo);
				}
			}

			BlrtContactManager.SharedInstanced.Add (list);
			BlrtContactManager.SharedInstanced.Save ();


			if (results.RelationStatuses.Any (
				x => x.Result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.BlockedByUser
				|| x.Result == BlrtAPI.APIConversationUserRelationCreate.RelationStatus.ResultEnum.Invalid
			)) {
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Failed;
			} else {
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
			}

			if (nonUserPhoneNumbers.Count > 0) {
				// TODO: show an alert. phone number not on blrt, send them a message

#if __ANDROID__
				OnPhoneNumberSend (this, EventArgs.Empty).FireAndForget ();
#endif
			}

#if __ANDROID__
			_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
#endif
			BlrtData.Helpers.BlrtTrack.AddPeopleSend (dictionary);

			//update conversation screen
			MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), _refreshFailed);
		}


		async Task FetchObjects (BlrtAPI.APIConversationUserRelationCreateResponse result)
		{
			//HACK, need conversation to set to dirty
			BlrtUtil.UpdateConversationBondCount (_conversation, 1);

			//BLRT-1263: one email address will need to fetch 4 objects, so may be we need more time on this task
			if (result.Objects != null) {
				var v = await BlrtUtil.FetchAllAsync (result.Objects, new CancellationTokenSource (5000).Token);
				_conversation.LocalStorage.DataManager.AddParse (v);
				if (v == null || v.Count () != result.Objects.Count ()) {
					throw new Exception ("Fetch Objects incomplete!");
				}
			}
		}



		string _url_convo_sms, _url_convo_facebook, _url_convo_email, _url_convo_other, _url_convo_addphone = "";

		public void PrepareUrl ()
		{

			if (BlrtConversation == null || !BlrtConversation.OnParse) {
				return;
			}

			Task.Factory.StartNew (() => {
				GenerateLink ("convo_sms");
				GenerateLink ("convo_facebook");
				GenerateLink ("convo_email");
				GenerateLink ("convo_other");
				GenerateLink ("convo_addphone");
			});
		}

		async Task GenerateLink (string urlType)
		{
			switch (urlType) {
			case "convo_sms":
				if (string.IsNullOrEmpty (_url_convo_sms) || !_url_convo_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_sms = await BlrtUtil.GenerateYozioUrl (BlrtConversation.ObjectId, urlType);
				}
				break;
			case "convo_addphone":
				if (string.IsNullOrEmpty (_url_convo_addphone) || !_url_convo_addphone.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_addphone = await BlrtUtil.GenerateYozioUrl (BlrtConversation.ObjectId, "convo_sms", new Dictionary<string, string> { { "sendToPhone", "true" } });
				}
				break;
			case "convo_facebook":
				if (string.IsNullOrEmpty (_url_convo_facebook) || !_url_convo_facebook.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_facebook = await BlrtUtil.GenerateYozioUrl (BlrtConversation.ObjectId, urlType);
				}
				break;
			case "convo_email":
				if (string.IsNullOrEmpty (_url_convo_email) || !_url_convo_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_email = await BlrtUtil.GenerateYozioUrl (BlrtConversation.ObjectId, urlType);
				}
				break;
			case "convo_other":
				if (string.IsNullOrEmpty (_url_convo_other) || !_url_convo_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
					_url_convo_other = await BlrtUtil.GenerateYozioUrl (BlrtConversation.ObjectId, urlType);
				}
				break;
			}
		}


		private void SetResult (bool success)
		{
			if (_tcs.Task.IsCompleted)
				return;
			_tcs.SetResult (success);
		}

		/// <summary>
		/// Closes the send.
		/// </summary>
		void CloseSendMaster ()
		{
			if (_conversation != null) {
				//var contacts = _sender.GetContacts ();
				//if(contacts==null){
				//	contacts = new List<BlrtSelectedContact> ();
				//}
				////caching data
				/// 
#if __IOS__
				if(_conversation.UnSendContacts == null){
					_conversation.UnSendContacts = new string[]{};
				}
#endif

				//var sendingContacts = contacts.Where(t=>t.SelectedInfo.ContactType != BlrtContactInfoType.FacebookId).Select (t => t.SelectedInfo.ContactValue).ToArray();
				//_conversation.UnSendContacts = _conversation.UnSendContacts.Concat (sendingContacts).Distinct ().ToArray ();
				_conversation.LocalStorage.DataManager.SaveLocal ();
			}

#if __IOS__
			BlrtContactManager.SharedInstanced.Changed -= ContactsChanged;
#endif
			SetResult (_refreshFailed);

#if __iOS__
            this.DismissViewController (true, null);
#else
			//BlrtApp.PlatformHelper.GetActivity ().OptionsItemSelected -= OptionItemClicked;
			ExecutionPerformer.UnregisterBlocker(this);
#endif
		}

		void LoadUnSendContacts(){

#if __ANDROID__
			//reset sending contacts list
			_conversation.SendingContacts?.Clear ();
			_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
#endif
			if (_conversation.UnSendContacts != null && _conversation.UnSendContacts.Length > 0) {
				//if the email already in convo, we should just remove it
				var relations = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId);
				var existEmails = new List<string> ();
				foreach(var rel in relations){
					existEmails.Add (rel.GetUserEmail ());
					existEmails.Add (rel.QueryValue);
				}

				_conversation.UnSendContacts = _conversation.UnSendContacts.Except (existEmails).ToArray();
				_conversation.LocalStorage.DataManager.SaveLocal ();

				if(_conversation.UnSendContacts.Length>0){ //I dont have time to modify the add contacts function
#if __iOS__
					AddContactString (_conversation.UnSendContacts);
#else
					_sender.AddContacts(_conversation.UnSendContacts);
#endif
				}
			}
		}

		bool CheckAddPeoplePermission()
		{
			var convo = _conversation;
			if (ParseUser.CurrentUser.ObjectId != convo.CreatorId && convo.DisableOthersToAdd) {
				BlrtManager.App.ShowAlert(Executor.System(), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_title", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_msg", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_accept", "conversation_screen")
				));

				return false;
			}


			if (!BlrtPermissions.CanAddUsers) {

				BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_title"),
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_message"),
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_accept")
				));

				return false;
			}

			if (BlrtPermissions.MaxConversationUsers <= convo.BondCount) {

				BlrtManager.App.ShowAlert(Executor.System(), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_title", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_message", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_accept", "conversation_screen")
				)); 
				BlrtData.Helpers.BlrtTrack.AccountHitLimit (BlrtData.BlrtPermissions.PermissionEnum.MaxConversationUsers.ToString());
				return false;
			}
			return true;
		}
	}
}
#endif
