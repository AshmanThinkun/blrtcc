﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;

namespace BlrtData.Views.Send
{
	public class ProgressSection{
		public StackLayout ProgressArea{get{return _progressArea;}}
		public const uint ProgressAnimationDuration = 200; //ms

		BlrtProgressBar _progressBar;
		Label _progressTitle;
		StackLayout _progressArea;

		public ProgressSection(){
			GetProgressSec ();
		}

		public StackLayout GetProgressSec(){
			_progressBar = new BlrtProgressBar {
				HorizontalOptions = LayoutOptions.Fill,
				Progress = 0d,
			};

			_progressTitle = new Label {
				FontSize = BlrtStyles.CellContentFont.FontSize,
				TextColor = BlrtStyles.BlrtGray5,
			};

			var progressTitleArea = new StackLayout {
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Fill,
				Padding = new Thickness(20, 0),
				Spacing = 0d,
				Children = {_progressTitle}
			};
			_progressArea = new StackLayout {
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Fill,
				Padding = new Thickness(0, 0, 0, 0),	
				Spacing = 3,
				Children = {_progressBar, progressTitleArea}
			};

			if(Device.OS == TargetPlatform.Android){//android move up progress bar
				_progressArea.Padding = new Thickness (0, -5, 0, 0);

				progressTitleArea.Padding = new Thickness (12, 0);
			}
			return _progressArea;
		}

		public void SetProgress(float progress, bool animated = true)
		{
			Device.BeginInvokeOnMainThread (() => {
				if (progress >= 0.99999999f) {
					_progressTitle.Text = string.Format (BlrtUtil.PlatformHelper.Translate ("progress_title_finished", "send_screen"), (int)Math.Round (progress * 100)); // TRANSLATORS: {0}: current upload progress; will be 100 here
				} else {
					_progressTitle.Text = string.Format (BlrtUtil.PlatformHelper.Translate ("progress_title_active", "send_screen"), (int)Math.Round (progress * 100)); // TRANSLATORS: {0}: current upload progress
				}
				if (animated) {
					_progressBar.ProgressTo (progress, ProgressAnimationDuration, Easing.Linear);
				} else {
					_progressBar.Progress = progress;
				}
			});
		}

		public bool IsTitleVisible{
			get{
				return _progressTitle.IsVisible;
			}set{
				_progressTitle.IsVisible = value;
			}
		}
	}
}

