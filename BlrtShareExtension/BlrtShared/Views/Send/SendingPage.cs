﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Collections.Generic;
using BlrtData.Contacts;
using System.Windows.Input;
using System.Threading.Tasks;
using BlrtData.Views.CustomViews;

#if __ANDROID__
using BlrtApp.Views;
#elif __IOS__
using BlrtData.Views.Send;
using BlrtData;
using BlrtData.Views.CustomUI;
#endif

namespace BlrtData.Views.Send
{
	public class SendingPage : BlrtContentPage
	{
		public event EventHandler SendPressed;
		public event EventHandler CancelPressed;

#if __IOS__
		Entry _contactEditor;
		FormsEntry _convoNameField;
		Label _errorLbl, _requestInfoLbl, _convoNameLbl;
		ListView _listView;
		ContentView _listViewContainer;
		ContactTablePage _contactPage;
		ConversationContactListModel _selectContacts;
#elif __ANDROID__
		SelectContactListModel _selectContacts;
#endif

		ExtendedEditor _conversationNote;
		Label _addNoteInConvoLbl;
		Switch _addNoteInConvo;
		ScrollView _mainScrollView;
		SendCommand _sendCommand;

		public void ClearSendContacts ()
		{
#if __IOS__
			_selectContacts.ExistingContacts.Clear ();
			ReloadTableView ();
#elif __ANDROID__
			_selectContacts.Contacts.Clear ();
#endif
		}

		public IEnumerable<ConversationNonExistingContact> GetContacts ()
		{
#if __IOS__
			return null;
#else
			return _selectContacts.Contacts;
#endif
		}

		public void SetContacts (IEnumerable<ConversationNonExistingContact> contacts)
		{
			foreach (var contact in contacts) {
				_selectContacts.AddContact (contact);
			}
		}

		public override string ViewName {
			get {
				return "Conversation_Send_Add";
			}
		}

		public void HideKeyboard ()
		{
#if __ANDROID__
			_conversationNote.Unfocus ();
#elif __IOS__
			_convoNameField.Unfocus ();
			_contactEditor.Unfocus ();
#endif
		}

		public SendingPage ()
		{
			Title = BlrtUtil.PlatformHelper.Translate ("Add people");
			this.BackgroundColor = BlrtStyles.BlrtGray2;

#if __IOS__
			_selectContacts = new ConversationContactListModel ();

			//progress View
			_progressSection = new ProgressSection ();
			_progressArea = _progressSection.ProgressArea;

			_contactEditor = new BlrtData.Views.CustomUI.FormsEntry {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("send_to_placeholder", "send_screen"),
				PlaceholderTextColor = BlrtStyles.BlrtGray6,
				FontSize = (float)BlrtStyles.CellTitleFont.FontSize,
				HideBottomDrawable = true,
				BackgroundColor = BlrtStyles.BlrtWhite,
			};
			_contactEditor.Completed += EmailEntryComplete;
			_contactEditor.Unfocused += EmailEntryComplete;
			_contactEditor.TextChanged += EmailEntryTextChanged;


			_contactPage = new ContactTablePage ();
			_contactPage.ContactSelected += MultiExecutionPreventor.NewEventHandler<ConversationNonExistingContact> (OnContactSelected);

			var _icon = new Image {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Center,
				Source = ImageSource.FromFile ("add_person.png"),
				WidthRequest = 50
			};
			_icon.GestureRecognizers.Add (new TapGestureRecognizer () {
				Command = new Command ((o) => {
					OpenContactPage ();
				})
			});

			_errorLbl = new Label () {
				Text = "   " + BlrtUtil.PlatformHelper.Translate ("error_invalid_contact_info", "send_screen"), // Invalid email addressasd
				TextColor = BlrtStyles.BlrtAccentRed,
				FontSize = BlrtStyles.FontSize14.FontSize,
				IsVisible = false
			};

			_requestInfoLbl = new Label () {
				Text = "    " + BlrtUtil.PlatformHelper.Translate ("requests_contact_lbl", "send_screen"),
				TextColor = BlrtStyles.BlrtGray6,
				FontSize = BlrtStyles.CellContentFont.FontSize,
				IsVisible = false
			};

			_convoNameLbl = new Label () {
				Text = "   " + BlrtUtil.PlatformHelper.Translate ("convo_name_lbl", "send_screen"),
				TextColor = BlrtStyles.BlrtGray6,
				FontSize = BlrtStyles.CellContentFont.FontSize,
				IsVisible = false
			};

			_convoNameField = new FormsEntry () {
				FontSize = (float)BlrtStyles.CellTitleFont.FontSize,
				IsVisible = false,
				HideBottomDrawable = true,
				BackgroundColor = BlrtStyles.BlrtWhite,
				PlaceholderTextColor = BlrtStyles.BlrtGray6,
			};

			_convoNameField.Focused += (sender, e) => {
				_convoNameField.SelectAll ();
			};

			_listView = new ListView () {
				BackgroundColor = BlrtStyles.BlrtGray2,
				ItemTemplate = new DataTemplate (typeof (SendingResultPage.MemberCell)),
				SeparatorColor = BlrtStyles.BlrtGray3,
				RowHeight = SendingResultPage.MemberCell.CellHeight,
			};


			_listView.ItemSelected += (sender, e) => {
				_listView.SelectedItem = null;
			};

			_listViewContainer = new ContentView () {
				Padding = 0,
				VerticalOptions = LayoutOptions.Start,
				HeightRequest = 0,
				Content = _listView
			};

			var emailEntry = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) }
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) }
				},
				Padding = new Thickness (0, 0, 10, 0),
				BackgroundColor = BlrtStyles.BlrtWhite,
				ColumnSpacing = 0,
				RowSpacing = 0,
				Children = {
					{_contactEditor, 0,0},
					{_icon, 1,0}
				}
			};

#endif

#if __ANDROID__
			_selectContacts = new SelectContactListModel ();
#endif

			_conversationNote = new ExtendedEditor {
				HorizontalOptions = LayoutOptions.Fill,
				HeightRequest = 100,
				Placeholder = BlrtUtil.PlatformHelper.Translate ("send_description_placeholder", "send_screen"),
				HideBottomDrawable = true,
				BackgroundColor = BlrtStyles.BlrtWhite
			};

			_conversationNote.TextChanged += (sender, e) => {
				_addNoteInConvo.IsEnabled = !string.IsNullOrEmpty (_conversationNote.Text);
				_addNoteInConvoLbl.TextColor = _addNoteInConvo.IsEnabled ? BlrtStyles.BlrtBlackBlack : BlrtStyles.BlrtGray5;
			};

			_addNoteInConvo = new Switch () {
				IsToggled = true,
				IsEnabled = false
			};

			_addNoteInConvoLbl = new Label () {
				Text = BlrtUtil.PlatformHelper.Translate ("add_note_in_convo", "send_screen"),
				TextColor = BlrtStyles.BlrtGray5,
				FontSize = BlrtStyles.CellTitleFont.FontSize,
				XAlign = TextAlignment.Start,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			_addNoteInConvo.Toggled += (sender, e) => {
				if (_addNoteInConvo.IsToggled) {
					_addNoteInConvoLbl.Text = BlrtUtil.PlatformHelper.Translate ("add_note_in_convo", "send_screen");
				} else {
					_addNoteInConvoLbl.Text = BlrtUtil.PlatformHelper.Translate ("add_note_in_convo_off", "send_screen");
				}
			};

			var noteSwitchView = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) }
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) }
				},
				Padding = new Thickness (12, 5, 10, 5),
				BackgroundColor = BlrtStyles.BlrtWhite,
				ColumnSpacing = 10,
				RowSpacing = 0,
				Children = {
					{_addNoteInConvoLbl, 0,0},
					{_addNoteInConvo, 1,0}
				}
			};

			_mainScrollView = new ScrollView () {
				Orientation = ScrollOrientation.Vertical,
				Padding = 0,
				Content = new StackLayout () {
					Orientation = StackOrientation.Vertical,
					Spacing = 0,
					Children = {
#if __IOS__
						_progressArea,
						_convoNameLbl,
						_convoNameField,
						_requestInfoLbl,
						_listViewContainer,
						new BoxView {HeightRequest = 10},
						emailEntry,
						_errorLbl,
#endif
						new BoxView {HeightRequest = 10},
						_conversationNote,
						new BoxView {HeightRequest = 10},
						noteSwitchView
					}
				}
			};

			this.Content = _mainScrollView;

			_sendCommand = new SendCommand (this);
#if __IOS__
			_sendAllowed = false;
			_sendPossible = false;
#elif __ANDROID__
			_sendAllowed = true;
			_sendPossible = true;
#endif
			this.ToolbarItems.Add (new ToolbarItem {
				Text = BlrtUtil.PlatformHelper.Translate ("Send"),
				Command = _sendCommand
			});

#if __IOS__
			ReloadTableView ();
#endif
		}

		bool _sendAllowed;
		bool _sendPossible;

		public bool SendAllowed {
			get {
				return _sendAllowed;
			}

			set {
				_sendAllowed = value;
				RefreshSendButton ();
			}
		}

		void RefreshSendButton ()
		{
			_sendCommand.Enabled = _sendAllowed && _sendPossible;
		}

		private class SendCommand : ICommand
		{
			SendingPage _parent;
			bool _enabled;
			public SendCommand (SendingPage parent)
			{
				_parent = parent;
#if __ANDROID__
				_enabled = true;
#elif __IOS__
				_enabled = false;
#endif
			}

			public bool Enabled {
				get {
					return _enabled;
				}

				set {
					if (value != _enabled) {
						_enabled = value;
						CanExecuteChanged?.Invoke (this, EventArgs.Empty);
					}
				}
			}

			public event EventHandler CanExecuteChanged;

			public bool CanExecute (object parameter)
			{
				return _enabled;
			}

			public void Execute (object parameter)
			{
				if (_enabled) {
					_parent.OnSend ();
				}
			}
		}

#if __IOS__
		protected override void OnDisappearing ()
		{
			base.OnDisappearing ();
			this.ConversationNameHidden = true;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			if (this.GetContacts ()?.Count () < 1) {
				_contactEditor.Focus ();
			}
		}

		void OpenContactPage ()
		{
			//this.Navigation.SafePushAsync (_contactPage); // for android
			this.Navigation.PushAsync (_contactPage);
		}

		void EmailEntryComplete (object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty (_contactEditor.Text) && BlrtUtil.IsValidEmail (_contactEditor.Text)) {
				//add contact
				AddContactString (_contactEditor.Text.Trim ());
				_contactEditor.Text = "";
			} else if (!string.IsNullOrEmpty (_contactEditor.Text) && BlrtUtil.IsValidPhoneNumber (_contactEditor.Text.Trim ())) {
				//add contact
				AddContactString (BlrtUtil.ToPhoneNumberE164 (_contactEditor.Text.Trim ()));
				_contactEditor.Text = "";

			} else if (!string.IsNullOrEmpty (_contactEditor.Text)) {
				_errorLbl.IsVisible = true;
			} else {
				_errorLbl.IsVisible = false;
			}
		}

		void EmailEntryTextChanged (object sender, EventArgs e)
		{
			_errorLbl.IsVisible = false;
		}

		void ReloadTableView ()
		{
			/*var source = null*/ /*_selectContacts.ExistingContacts.Select(c => c as ConversationNonExistingContact).Select ((v, i) => {
				
				var rtn = new SelectContactSource (i) {
					DisplayName = v.Contact.UsableName,
					Email = v.SelectedInfo.ContactValue,
					Image2Source = "cross.png",
					InitialBackgroundColor = BlrtStyles.BlrtGreen,
					Initials = BlrtUtil.GetInitials (v.Contact.UsableName),
					Model = v.Contact
				};

				if (v.Contact.Avatar != null && !string.IsNullOrEmpty (v.Contact.Avatar.Path)) {
					rtn.AvatarPath = v.Contact.Avatar.Path;
					rtn.Initials = "";
				} else {
					rtn.AvatarPath = "";
				}

				rtn.ImageClicked += ContactRowDeleted;
				return rtn;
			});*/
								  //_listView.ItemsSource = source;
								  //_sendPossible = source.Count () > 0;
			RefreshSendButton ();
			//reset table view frame
			if (_selectContacts.ExistingContacts.Count > 0) {
				_listViewContainer.HeightRequest = _selectContacts.ExistingContacts.Count * SendingResultPage.MemberCell.CellHeight + 3;
				_listViewContainer.IsVisible = true;
			} else {
				_listViewContainer.HeightRequest = 0;
				_listViewContainer.IsVisible = false;
			}
		}

		public bool RequestLabelHidden {
			get {
				return !_requestInfoLbl.IsVisible;
			}
			set {
				if (_requestInfoLbl.IsVisible == value) {
					_requestInfoLbl.IsVisible = !value;
				}
			}
		}

		public bool ConversationNameHidden {
			get {
				return !_convoNameLbl.IsVisible;
			}
			set {
				if (_convoNameLbl.IsVisible == value) {
					_convoNameLbl.IsVisible = !value;
					_convoNameField.IsVisible = !value;
				}
			}
		}

		public string ConversationName {
			get {
				return _convoNameField.Text;
			}
			set {
				_convoNameField.Text = value;
			}
		}

		ProgressSection _progressSection;
		StackLayout _progressArea;
		public bool IsProgressVisible {
			get { return _progressArea.IsVisible; }
			set { _progressArea.IsVisible = value; }
		}

		public void SetProgress (float progress, bool animated = true)
		{
			_progressSection.SetProgress (progress, animated);
		}

		public void clearAddressField ()
		{
			_errorLbl.IsVisible = false;
			_contactEditor.Text = "";
		}

		public void AssignContacts (List<BlrtContact> contacts)
		{
			_contactPage.AssignContacts (contacts);
		}


#endif
		async Task OnContactSelected (object sender, ConversationNonExistingContact e)
		{
			AddContactToField (e);
#if __ANDROID__
			await this.Navigation.SafePopAsync ();
			await _mainScrollView.ScrollToAsync (_addNoteInConvo, ScrollToPosition.End, true);
#else
			await this.Navigation.PopAsync ();
#endif
		}

		void ContactRowDeleted (object sender, int e)
		{
			_selectContacts.DeleteAtIndex (e);
#if __IOS__
			ReloadTableView ();
#endif
		}

		public string DescriptionFieldText {
			get { return _conversationNote.Text; }
			set {
				_conversationNote.Text = value;
				_addNoteInConvo.IsEnabled = !string.IsNullOrEmpty (_conversationNote.Text);
				_addNoteInConvoLbl.TextColor = _addNoteInConvo.IsEnabled ? BlrtStyles.BlrtBlackBlack : BlrtStyles.BlrtGray5;
			}
		}

		public bool DontAddNoteInConvo {
			get {
				return !_addNoteInConvo.IsToggled;
			}
		}

		void OnCancel ()
		{
			if (CancelPressed != null)
				CancelPressed (this, EventArgs.Empty);
		}

		void OnSend ()
		{

#if __IOS__
			EmailEntryComplete (this, EventArgs.Empty);

			var emails = GetRecipientEmails ();

			if (emails.Length == 0) {

				DisplayAlert (
					BlrtUtil.PlatformHelper.Translate ("Invalid recipients"),
					BlrtUtil.PlatformHelper.Translate ("You must enter at least one valid email address."),
					BlrtUtil.PlatformHelper.Translate ("OK")
				);

				return;
			}
#endif

			if (SendPressed != null)
				SendPressed (this, EventArgs.Empty);
		}

		public string [] GetRecipientEmails ()
		{
#if __IOS__
			return null;
#else
			return _selectContacts.Contacts.Where (x => x.SelectedInfo.ContactType != BlrtContactInfoType.FacebookId).Select (t => t.SelectedInfo.ContactValue).ToArray ();
#endif
		}

		public void AddContactToField (ConversationNonExistingContact e)
		{
			_selectContacts.AddContact (e);
#if __IOS__
			ReloadTableView ();
#endif
		}

		public void AddContacts (string [] emails)
		{
			for (int i = 0; i < emails.Length; ++i) {
				AddContactString (emails [i]);
			}
		}

		public void AddContactString (string contactString)
		{

			var str = contactString.Trim ();

			var contactInfo = new BlrtContactInfo (BlrtUtil.IsValidEmail (str) ? BlrtContactInfoType.Email : BlrtContactInfoType.PhoneNumber, str);
			var newcontact = new BlrtContact () {
				Name = null,
			};
			newcontact.Add (contactInfo);
			var otherContact = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
			ConversationNonExistingContact newSelected;
			if (otherContact == null) {
				newSelected = new ConversationNonExistingContact (contactInfo, newcontact);
			} else {
				var selectInfo = otherContact.InfoList.First (info => info.Equals (contactInfo));
				newSelected = new ConversationNonExistingContact (selectInfo, otherContact);
			}

			this.AddContactToField (newSelected);
		}

		public class SelectContactSource
		{
			public string DisplayName { get; set; }
			public string Email { get; set; }
			public string Initials { get; set; }
			public Color InitialBackgroundColor { get; set; }
			public string AvatarPath { get; set; }
			public string Image2Source { get; set; }
			public FormattedString FormattedDetail { get; set; }
			public BlrtContact Model { get; set; }

			public void OpenProfile ()
			{
				if (Model != null)
					BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), Model);
			}

			public event EventHandler<int> ImageClicked;

			readonly int _index;
			public SelectContactSource (int index)
			{
				_index = index;
			}

			public void ImageClick ()
			{
				if (ImageClicked != null)
					ImageClicked (this, _index);
			}
		}
	}
}

