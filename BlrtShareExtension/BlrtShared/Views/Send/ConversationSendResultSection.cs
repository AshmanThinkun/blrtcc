﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;
using System.Linq;
using BlrtData.Query;
using System.Threading;
using BlrtData.Models.ConversationScreen;
using BlrtData.Contacts;
#if __iOS__
using Foundation;
using UIKit;
#endif

namespace BlrtData.Views.Send
{
	public class ConversationSendResultSection : ContentView
	{
		FormsListView _listView;
		MR.Gestures.Label _clearAllBtn;
		public const int HeaderHeight = 28;

		public ConversationSendResultSection () : base ()
		{
			_listView = new FormsListView () {
				RowHeight = MemberCell.CellHeight,
				SeparatorInset = new Thickness (MemberCell.CellHeight + 10, 0, 0, 0),
				SeparatorColor = BlrtStyles.BlrtGray3,
				BackgroundColor = BlrtStyles.BlrtGray2,
				DisableScrolling = true,
			};

			_clearAllBtn = new MR.Gestures.Label () {
				HeightRequest = HeaderHeight,
				YAlign = TextAlignment.Center,
				Text = "Clear",
				FontSize = BlrtStyles.CellContentFont.FontSize,
				FontAttributes = FontAttributes.Bold,
				TextColor = BlrtStyles.BlrtAccentBlueHighlight,
			};

			_clearAllBtn.Down += (sender, e) => {
				_clearAllBtn.Opacity = 0.5f;
			};
			_clearAllBtn.Up += (sender, e) => {
				_clearAllBtn.Opacity = 1;

				bool cancelled = false;
				if (e.Touches.Length > 0) {
					var increment = 5;
					if (e.Touches [0].X > e.ViewPosition.Width + increment
						|| e.Touches [0].X < 0 - increment
						|| e.Touches [0].Y > e.ViewPosition.Height + increment
						|| e.Touches [0].Y < 0 - increment
					) {
						cancelled = true;
					}
				}

				if (cancelled)
					return;
				_conversation.SendingContacts?.Clear ();
				_conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
				Xamarin.Forms.MessagingCenter.Send<Object, bool> (this, string.Format ("{0}.RelationChanged", _conversation.LocalGuid), false);

				ReloadList ();
			};

			var titleSec = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				BackgroundColor = BlrtStyles.BlrtGreen,
				Padding = new Thickness (10, 0, 15, 0),
				HeightRequest = HeaderHeight,
				Children = {
					{ new Label(){
							HeightRequest = HeaderHeight,
							YAlign = TextAlignment.Center,
							Text = "Send results",
							FontSize = BlrtStyles.CellContentFont.FontSize,
							TextColor = BlrtStyles.BlrtGray6,
						},0,0},
					{
						_clearAllBtn, 1,0
					}
				}
			};

			_listView.ItemTemplate = new DataTemplate (typeof (MemberCell));
			_listView.ItemSelected += _listView_ItemSelected;

			Content = new Grid () {
				ColumnDefinitions = {
					new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
				},
				RowDefinitions = {
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
					new RowDefinition (){ Height = new GridLength (1, GridUnitType.Auto) },
				},
				Padding = new Thickness (0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				Children = {
					{ titleSec,0,0},
					{ _listView,0, 1 },
					{ new BoxView(){HeightRequest=20},0, 2 },
				}
			};
		}

		void _listView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null) {
				var item = e.SelectedItem as DisplayItem;
				if (item.Model != null) {
					if (item.Contact.SelectedInfo.ContactType == BlrtContactInfoType.FacebookId && item.Contact.Status == ContactState.ConversationSentUnreachable) {
						OnFacebookSend?.Invoke (sender, e);
					} else if (item.Contact.SelectedInfo.ContactType == BlrtContactInfoType.PhoneNumber && item.Contact.Status == ContactState.ConversationSentUnreachable) {
						OnPhoneNumberSend?.Invoke (sender, e);
					} else if (item.Contact.Status == ContactState.FailedToSend) {
						RetrySend ();
					} else if (item.Model is BlrtUser)
						BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), item.Model as BlrtUser);
					else
						BlrtManager.App.OpenProfile (BlrtData.ExecutionPipeline.Executor.System (), item.Model as BlrtData.Contacts.BlrtContact);
				}
			}
			_listView.SelectedItem = null;
		}

		BlrtData.Conversation _conversation;
		public void SetConversation (BlrtData.Conversation conversation)
		{
			_conversation = conversation;
			ReloadList ();
		}


		public void ReloadList ()
		{
			if (_conversation == null)
				return;
			var relations = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId);
			if (_conversation.SendingContacts == null) {
				_conversation.SendingContacts = new List<ContactStatus> ();
			}


			//if relation already exist and we haven't get a response after 20 seconds, we should just say success.
			foreach (var relation in relations) {
				var match = _conversation.SendingContacts.FirstOrDefault (x => x.SelectedInfo.ContactValue.ToLower () == relation.QueryValue);
				if (match != null && (match.Status == ContactState.Sending) && match.UpdateAt < DateTime.Now.AddSeconds (-20)) {
					match.UpdateAt = DateTime.Now;
					if (!string.IsNullOrEmpty (relation.UserId)) {
						match.Status = ContactState.ConversationSentUser;
					} else {
						match.Status = ContactState.ConversationSentReachable;
					}
				}
			}

			//set other timeout status as send failure
			foreach (var sta in _conversation.SendingContacts) {
				if (sta.Status == ContactState.Sending && sta.UpdateAt < DateTime.Now.AddSeconds (-20)) {
					sta.Status = ContactState.FailedToSend;
					sta.UpdateAt = DateTime.Now;
				}
			}

			//now we can build the data source
			var dataList = _conversation.SendingContacts.OrderByDescending (t => t.UpdateAt).Select ((infoStatus) => {
				var displayItem = new DisplayItem ();

				var contactInfo = infoStatus.SelectedInfo;
				var newcontact = new BlrtContact () {
					Name = null,
				};
				newcontact.Add (contactInfo);
				var match = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
				if (match == null) {
					match = newcontact;
				}
				var selectInfo = contactInfo;
				foreach (var info in match.InfoList) {
					if (info.Equals (contactInfo)) {
						selectInfo = info;
						break;
					}
				}
				var x = new ConversationNonExistingContact (selectInfo, match);

				displayItem.DisplayName = x.Contact.UsableName;
				displayItem.Initials = BlrtUtil.GetInitials (displayItem.DisplayName);
				if (x.Contact.Avatar != null && !string.IsNullOrWhiteSpace (x.Contact.Avatar.Path)) {
					displayItem.Initials = "";
					displayItem.AvatarPath = x.Contact.Avatar.Path;
				}
				displayItem.Contact = infoStatus;

				var type = infoStatus.Status;
				switch (type) {
				case ContactState.Blocked:
					displayItem.SendResult = "Blocked";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.ConversationAlreadySent:
					displayItem.SendResult = "Already sent";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.ConversationSentReachable:
					displayItem.SendResult = "Invitation sent";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.ConversationSentUnreachable:
					displayItem.SendResult = "Not on Blrt";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.ConversationSentUser:
					displayItem.SendResult = "Sent to user";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.FailedToSend:
					displayItem.SendResult = "Failed to send";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					displayItem.ResultTextColor = BlrtStyles.BlrtAccentRed;
					break;
				case ContactState.InvalidContact:
					displayItem.SendResult = "Invalid contact";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				case ContactState.Sending:
					displayItem.SendResult = "Sending";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					displayItem.ShowLoader = true;
					break;
				default:
					displayItem.SendResult = "";
					displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
					break;
				}
				displayItem.Model = x.Contact;

				return displayItem;
			});

			_listView.ItemsSource = dataList;

			if (dataList.Count () > 0) {
				this.IsVisible = true;
			} else {
				this.IsVisible = false;
			}

			_listView.HeightRequest = MemberCell.CellHeight * dataList.Count ();
			this.Content.HeightRequest = _listView.HeightRequest + HeaderHeight + 20;
		}


		private void SendFacebookSuggetions (bool showAlert = true)
		{
			List<BlrtContactInfo> _list = _conversation.SendingContacts
				.Where (c => c.Status == ContactState.ConversationSentUnreachable && c.SelectedInfo.ContactType == BlrtContactInfoType.FacebookId)
				.Select (x => x.SelectedInfo).ToList ();


			if (_list.Count == 0) {
				return;
			}

			if (showAlert) {
				BlrtManager.App.ShowAlert (
					ExecutionPipeline.Executor.System (),
					new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_alert_title", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_alert_message", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_alert_accept", "send_screen")
					)
				);
			}

			SendFacebookSuggetions (_list.Select (c => c.ContactValue).ToArray ());

		}

#if __iOS__
		class SuggestionsDel : Facebook.ShareKit.GameRequestDialogDelegate
		{
			#region implemented abstract members of GameRequestDialogDelegate
			public override void DidCancel (Facebook.ShareKit.GameRequestDialog gameRequestDialog)
			{

			}
			public override void DidComplete (Facebook.ShareKit.GameRequestDialog gameRequestDialog, NSDictionary results)
			{
				//				var dic = BlrtData.BlrtUtil.GetParameters (resultUrl.AbsoluteString);
				//				foreach (var kvp in dic) {
				//					if (kvp.Key.StartsWith ("to")) {
				//						if (_suggesstions.Contains (kvp.Value)) {
				//							_suggesstions.Remove (kvp.Value);
				//						}
				//					}
				//				}
				//				try {
				//					//save the fbreqeust record to cloud
				//					var parameters = new Dictionary<string, object>(){
				//						{ "conversationId", _conversation.ObjectId },
				//						{ "requestId", dic["?request"]}
				//					};
				//					BlrtUtil.BetterParseCallFunctionAsync<string> ("fbNotification", parameters, new System.Threading.CancellationTokenSource(1500).Token);
				//				} catch {}

				if (_suggesstions.Count > 0) {

					var alert = new UIAlertView (
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_failed_alert_title", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_failed_alert_message", "send_screen"),
						null,
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_failed_alert_cancel", "send_screen"),
						BlrtUtil.PlatformHelper.Translate ("facebook_send_request_failed_alert_accept", "send_screen")
					);
					alert.Clicked += (object sender, UIButtonEventArgs e) => {
						if (e.ButtonIndex != alert.CancelButtonIndex)
							_parent.SendFacebookSuggetions (_suggesstions.ToArray ());
					};

					alert.Show ();
				}
			}
			public override void DidFail (Facebook.ShareKit.GameRequestDialog gameRequestDialog, NSError error)
			{
				Console.WriteLine (error.ToString ());
			}
			#endregion

			ConversationSendResultSection _parent;
			List<string> _suggesstions = new List<string> ();
			public SuggestionsDel (string [] suggestions, ConversationSendResultSection parent) : base ()
			{
				_suggesstions = suggestions?.ToList ();
				_parent = parent;
			}

		}
#endif
		private void SendFacebookSuggetions (string [] suggestions)
		{
#if __iOS__
			var content = new Facebook.ShareKit.GameRequestContent ();
			content.Message = string.Format (BlrtUtil.PlatformHelper.Translate ("facebook_send_request_message", "send_screen"), BlrtData.BlrtUserHelper.CurrentUserName);
			content.Title = BlrtUtil.PlatformHelper.Translate ("facebook_send_request_title", "send_screen");
			content.Recipients = suggestions;
			//			content.ObjectID = BlrtEncode.EncodeId(_conversation.ObjectId);

			var del = new SuggestionsDel (suggestions, this);

			Facebook.ShareKit.GameRequestDialog.Show (content, del);
#endif
		}

		public event EventHandler OnRetrySend;
		public event EventHandler OnFacebookSend, OnPhoneNumberSend;
		public void RetrySend ()
		{
			OnRetrySend?.Invoke (this, EventArgs.Empty);
		}


		class DisplayItem
		{
			public object Model { get; set; }
			public string Initials { get; set; }
			public string DisplayName { get; set; }
			public string SendResult { get; set; }
			public string AvatarPath { get; set; }
			public Color InitialBackgroundColor { get; set; }
			public Color ResultTextColor { get; set; }
			public ContactStatus Contact { get; set; }
			public bool ShowLoader { get; set; }

			public DisplayItem ()
			{
				ResultTextColor = BlrtStyles.BlrtGray5;
			}
		}

		public class MemberCell : ViewCell
		{
			public static int CellHeight = 50;

			Label _nameLbl, _typeLbl, _initialLbl;
			RoundedImageView _roundBox;
			ActivityIndicator _loader;

			public MemberCell () : base ()
			{
				_nameLbl = new Label () {
					XAlign = TextAlignment.Start,
					YAlign = TextAlignment.Center,
					FontSize = BlrtStyles.FontSize14.FontSize,
					LineBreakMode = LineBreakMode.TailTruncation,
					TextColor = BlrtStyles.BlrtGray6
				};

				_typeLbl = new Label () {
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.End,
					TextColor = BlrtStyles.BlrtGray5,
					FontSize = BlrtStyles.FontSize14.FontSize
				};

				_initialLbl = new Label () {
					YAlign = TextAlignment.Center,
					XAlign = TextAlignment.Center,
					TextColor = BlrtStyles.BlrtWhite,
					FontSize = BlrtStyles.FontSize14.FontSize
				};

				_roundBox = new RoundedImageView () {
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					CornerRadius = (CellHeight - 10) * 0.5f,
					HeightRequest = CellHeight - 10,
					WidthRequest = CellHeight - 10,
					Source = "cross.png",
					Aspect = Aspect.AspectFill
				};

				_loader = new ActivityIndicator () {
					IsVisible = false,
				};

				var bottomPadding = 0;

				this.View = new Grid () {
					ColumnDefinitions = {
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Star) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
						new ColumnDefinition (){ Width = new GridLength (1, GridUnitType.Auto) },
					},
					RowDefinitions = {
						new RowDefinition (){ Height = new GridLength (1, GridUnitType.Star) },
					},
					Padding = new Thickness (10, 10, 10, bottomPadding),
					ColumnSpacing = 10,
					RowSpacing = 0,
					Children = {
						{_roundBox, 0,0},
						{_initialLbl, 0,0},
						{_nameLbl, 1, 0},
						{_loader, 2,0},
						{_typeLbl, 3,0}
					}
				};

				if (Device.OS == TargetPlatform.Android) {
					this.View.BackgroundColor = BlrtStyles.BlrtGray2;
				}

				_roundBox.SetBinding (RoundedImageView.BackgroundColorProperty, "InitialBackgroundColor");
				_roundBox.SetBinding (RoundedImageView.SourceProperty, "AvatarPath");
				_initialLbl.SetBinding (Label.TextProperty, "Initials");
				_typeLbl.SetBinding (Label.TextProperty, "SendResult");
				_typeLbl.SetBinding (Label.TextColorProperty, "ResultTextColor");
				_nameLbl.SetBinding (Label.TextProperty, "DisplayName");
				_loader.SetBinding (ActivityIndicator.IsRunningProperty, "ShowLoader");
				_loader.SetBinding (ActivityIndicator.IsVisibleProperty, "ShowLoader");
			}
		}
	}
}
