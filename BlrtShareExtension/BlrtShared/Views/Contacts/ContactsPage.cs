﻿using Xamarin.Forms;
using BlrtData;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Views.CustomUI;
using BlrtData.Contacts;
using System.Linq;
using System.Collections.ObjectModel;
using BlrtData.Views.Send;
using System;
using BlrtData.ExecutionPipeline;

namespace BlrtData.Views.Contacts
{
	public class ContactsPage: ContactTablePage
	{
		public ContactsPage (): base()
		{
			Title = BlrtUtil.PlatformHelper.Translate("contact_screen_title", "profile");
		}

		public override string ViewName {
			get {
				return "Contacts";
			}
		}
	}
}

