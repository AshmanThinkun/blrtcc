using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Models.Mailbox;

#if __IOS__
using BlrtiOS.Views.Mailbox;
#endif

namespace BlrtData.ExecutionPipeline
{

	public interface IAppController
	{
		Task<ExecutionResult<IAlert>> ShowAlert (IExecutor e, IAlert alert);
		Task<ExecutionResult<IProgressAlert>> ShowProgressAlert (IExecutor e, IProgressAlert alert);

		Task<ExecutionResult<IMailboxScreen>> OpenMailbox (IExecutor e, MailboxType type);

		Task<ExecutionResult<ISignUpScreen>> OpenSignUp (IExecutor e);
		Task<ExecutionResult<IUpgradeScreen>> OpenUpgradeAccount (IExecutor e);
		Task<ExecutionResult<object>> OpenShare (IExecutor e);
		Task<ExecutionResult<object>> OpenContactsPage (IExecutor e);
		Task<ExecutionResult<object>> OpenSettings (IExecutor e);
		Task<ExecutionResult<IHelpScreen>> OpenSupport (IExecutor e);
		Task<ExecutionResult<object>> OpenNews (IExecutor e);
		Task<ExecutionResult<object>> OpenSendSupportMsg (IExecutor e, string additionalMsg = null);

		Task<ExecutionResult<ICanvasResult>> OpenBlrtCreation (IExecutor e, IRequestArgs args);
		Task<ExecutionResult<ICanvasResult>> OpenBlrtCreation (IExecutor e,
															 object args,
															 bool mediaHidden = false,
															 Tuple<IEnumerable<MediaAsset>, HashSet<string>> blrtThisMediaModel = null);


		Task<ExecutionResult<ICanvasResult>> OpenCanvas (IExecutor e, object args);
		Task<ExecutionResult<ICanvasResult>> OpenCanvas (IExecutor e, ILocalBlrtArgs args);

		Task<ExecutionResult<object>> OpenProfile (IExecutor e, BlrtUser blrtUser = null);
		Task<ExecutionResult<object>> OpenProfile (IExecutor e, BlrtData.Contacts.BlrtContact blrtContact);
		Task<ExecutionResult<object>> OpenOrganisationList (IExecutor e, string ocListId);
		Task<ExecutionResult<object>> OpenAddPhone (IExecutor e, BlrtData.Views.AddPhone.OpenFrom openfrom);

		object GetCurrentSlave ();

		event EventHandler SystemMemoryWarning;
		event EventHandler MoveToBackground;
		event EventHandler MoveToForeground;


#if __IOS__
		Task<ExecutionResult<IMailboxScreen>> OpenMailboxToCreateNewConvoItemsUsingExternalMedia (IExecutor executor,
																											MailboxType inbox,
																											IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia);
		void AddPushNotification (IDictionary<string, object> pushData);
		Task<bool> CreateConversationWithContacts (IExecutor executor, IRequestArgs args);
		Task CreateConversationToCreateNewConvoItemsUsingExternalMedia (IExecutor executor, IRequestArgs argsToCreateNewConvoItemsUsingExternalMedia);
		void SettleHighPriorityPushNotification (Dictionary<string, object> dic);
#endif
#if __ANDROID__
		Task<ExecutionResult<ICanvasResult>> OpenBlrtCreation (IExecutor e, object args, string path, IEnumerable<MediaAsset> item, BlrtMediaFormat type);
		void HandleActionSend (string filePath, string fileName);
		Task<bool> HandleBlrtSendProcess ();
#endif

	}

	public interface IMailboxScreen
	{

		Task Refresh (IExecutor e);
		void Reload (IExecutor e);
		Task<ExecutionResult<IConversationScreen>> OpenConversation (IExecutor e,
																	 string conversationId,
																	 bool openWtihSMSLink = false,
																	 bool IsNewConversation = false);
		void CloseConversation (IExecutor e);
		IConversationScreen ConversationScreen { get; }


#if __ANDROID__
		Task<Tuple<bool, string>> CreateNewConversation ();
#endif
#if __IOS__
		bool MailboxListInSeparateContainer { get; set; }
#endif

	}

	public interface IConversationScreen
	{
		Task<ExecutionResult<object>> OpenContentItem (IExecutor e, string contentId);
		Task<ExecutionResult<object>> OpenSendingPage (IExecutor e, IRequestArgs args);
		Task<ExecutionResult<object>> OpenTagPage (IExecutor e);

		Task<ExecutionResult<object>> OpenReBlrtCreation (IExecutor e,
															   IRequestArgs args,
															   bool lastBlrtMediaHidden = false,
															   Tuple<IEnumerable<MediaAsset>, HashSet<string>> blrtThisMediaModel = null);


		Task<ExecutionResult<object>> OpenConversationOption (IExecutor e);
		MultiExecutionPreventor.SingleExecutionToken GetSubscreenExecutionToken ();
		Conversation CurrentConversation { get; }


#if __IOS__
		Task OpenSharePage ();
		void CreateConversationItemsUsingExternalMedia (object args);
		Task RefreshAsync ();
		Task BlrtThisMedia (string [] mediaIds);
#endif


	}

	public interface IUpgradeScreen {
		Task WaitAsyncCompletion();
	}



	public interface IHelpScreen{
		Task<ExecutionResult<object>>					OpenMessageSupport	();
		Task<ExecutionResult<object>>					OpenMessageSupport (string additionalInformation);

		Task<ExecutionResult<object>>					OpenKnowledge ();
	}

	public interface ISignUpScreen
	{
		Task<ExecutionResult<object>>					OpenSignUpMenu (IExecutor e, bool animated);
	}
}

