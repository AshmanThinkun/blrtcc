using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	


	public interface ICanvasResult {

		string CreatedConversationId	{ get; }
		string CreatedContentId			{ get; }

		bool CreatedContent				{ get; }
		bool ShowMediaPicker			{ get; }
	}

	public interface IRequestArgs {

		string BlrtName			{ get; }
		string CaptureUrl		{ get; set; }
		string RecipientsId		{ get; set; }
		string[] Emails			{ get; set; }
		string[] DownloadUrls	{ get; set; }

		string AttachedMeta		{ get; }
		string RequestId		{ get; }
		bool MakeBlrtPublic	{ get; set;}

	}

	public class LocalBlrtPage
	{
		public uint MediaIndex;
		public int PageNumInMedia;
	};

	public class LocalMedia
	{
		public BlrtMediaFormat Format;
		public string Location;
	};

	public interface ILocalBlrtArgs
	{
		LocalMedia[] Media 		{ get; }
		LocalBlrtPage[] Pages 	{ get; }
		string AudioPath		{ get; }
		string TouchPath		{ get; }
	}

	public interface IAlert
	{
		string Title { get; }
		string Message { get; }
		string Cancel { get; }
		int OptionCount { get; }

		int Result { get; }

		bool Cancelled { get; }

		string GetOption (int index);


		void SetResult (int result);
	}

	public interface IProgress
	{
		event EventHandler OnProgress;
		event EventHandler OnFinished;
		event EventHandler OnFailed;

		bool IsFailed 		{ get; }
		bool IsFinished		{ get; }

		long Total 			{ get; }
		long Current 		{ get; }
		float Progress 		{ get; }
	}
	
	public interface IProgressAlert
	{
		IProgress Source	{ get; }
		string Title 		{ get; }
		string Message 		{ get; }
		string Cancel 		{ get; }
		bool Finished		{ get; }
		void SetResult (bool finished);
	}
	


	

	





	

	

	

	
}

