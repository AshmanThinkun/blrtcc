using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace BlrtData.ExecutionPipeline
{
	public abstract class ExecutionPerformer
	{
		Task<bool> _task;
		IExecutor _executor;

		public bool Ready {
			get { return !Ran; }
		}

		public bool Ran {
			get { return _task != null; }
		}

		public IExecutor Executor {
			get { return _executor; }
		}

		public ExecutionPerformer (IExecutor executor) {

			if (executor == null)
				throw new ArgumentNullException ();

			this._executor = executor;
		}


		public Task<bool> RunAction () {

			if (Ran || !Ready)
				throw new Exception ();
			_task = DoRunAction ();
			return _task;
		}

		private async Task<bool> DoRunAction ()
		{
			if (await SolveBlockers (this)) {
				await Action ();
				return true;
			}
			return false;
		}

		public Task WaitAsync() {
			return _task;
		}

		protected abstract Task Action();


		static List<IExecutionBlocker> _blockers;
		static ConcurrentQueue<IExecutionBlocker> _unregisteringBlockers;
		static object _blockerLock = new object ();
		static bool _isResolving = false;

		public static bool IsResolving{
			get{
				return _isResolving;
			}
		}

		/// <summary>
		/// Registers a blocker.
		/// </summary>
		/// <returns><c>true</c>, if blocker was registered, <c>false</c> this blocker has already registered.</returns>
		/// <param name="b">The blocker.</param>
		public static bool RegisterBlocker (IExecutionBlocker b)
		{
			if (_isResolving)
				throw new NotImplementedException ();
			lock (_blockerLock) {
				if (null == _blockers) {
					_blockers = new List<IExecutionBlocker> ();
				}
				if (!_blockers.Exists (ib => (ib == b))) {
					_blockers.Add (b);
					return true;
				}
				return false;
			}
		}

		/// <summary>
		/// Unregisters a blocker.
		/// </summary>
		/// <returns><c>true</c>, if blocker was unregistered, <c>false</c> the blocker was not registered.</returns>
		/// <param name="b">The blocker.</param>
		public static bool UnregisterBlocker (IExecutionBlocker b)
		{
			if (_isResolving) {
				if (null == _unregisteringBlockers) {
					_unregisteringBlockers = new ConcurrentQueue<IExecutionBlocker> ();
				}
				_unregisteringBlockers.Enqueue (b);
				return true;
			} else {
				return DoUnregisterBlocker (b);
			}
		}

		/// <summary>
		/// Solves all the blockers.
		/// </summary>
		/// <returns><c>true</c>, if blockers were all solved successfully, <c>false</c> at least 1 blocker cannot be solved.</returns>
		/// <param name="e">he execution that need to be done after all the blockers are solved</param>
		private static async Task<bool> SolveBlockers (ExecutionPerformer e)
		{
			if (_isResolving)
				throw new NotImplementedException ();
			_isResolving = true;
			if (null == _blockers) {
				return FinishBlockerSolving (true);
			}
			for (int i = _blockers.Count - 1; i >= 0; --i) {
				bool solved = await _blockers [i].Resolve (e);
				if (!solved) {
					return FinishBlockerSolving (false);
				}
			}
			return FinishBlockerSolving (true);
		}

		private static bool FinishBlockerSolving (bool allSolved)
		{
			if (!_isResolving)
				throw new NotImplementedException ();
			IExecutionBlocker result;
			try {
				while ((null != _unregisteringBlockers) && _unregisteringBlockers.TryDequeue (out result)) {
					DoUnregisterBlocker (result);
				}
			} catch (Exception ex) {
				LLogger.WriteEvent ("ExecutionBlocker", "FinishBlockerSolving", "Exception", ex.ToString ());
			}
			_isResolving = false;
			return allSolved;
		}

		private static bool DoUnregisterBlocker (IExecutionBlocker b)
		{
			lock (_blockerLock) {
				if (null == _blockers) {
					return false;
				}
				return _blockers.Remove (b);
			}
		}
	}

	public interface IExecutionBlocker {
		/// <summary>
		/// Resolve this blocker.
		/// </summary>
		/// <returns><c>true</c>, if blocker was solved successfully, <c>false</c> the blocker cannot be solved.</returns>
		/// <param name="e">the execution that need to be done after all the blockers are solved</param>
		Task<bool> Resolve(ExecutionPerformer e);
	}

	public class SimpleBlocker : IExecutionBlocker
	{
		Func<Task<bool>> _action;
		public SimpleBlocker (Func<Task<bool>> resolveAction)
		{
			_action = resolveAction;
		}

		public async Task<bool> Resolve (ExecutionPerformer e)
		{
			if (null != _action) {
				return await _action ();
			}
			return true;
		}
	}
}

