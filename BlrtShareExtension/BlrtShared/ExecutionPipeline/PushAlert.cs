using System;
using System.Linq;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;
using Xamarin.Forms;
using System.Collections.Generic;

namespace BlrtData
{
	public class PushAlert : IAlert
	{
		IPlatformHelper _platformHelper;

		int? _result;

		string _title;
		string _cancel;

		string _option;

		string[] _args;

		public string Title {
			get { return (null == _title) ? null : ((null == _args) ? _platformHelper.Translate (_title) : string.Format (_platformHelper.Translate (_title), _args)); }
		}

		public string Message {
			get { return ""; }
		}

		public string Cancel {
			get { return _cancel; }
		}

		public int OptionCount {
			get { return string.IsNullOrEmpty(_option) ? 0 : 1;}
		}

		public int Result {
			get { return _result ?? -1; }
		}

		public bool Cancelled {
			get { return _result != null && Result < 0; }
		}

		public PushAlert ( IDictionary<string, object> pushData ) {

			_platformHelper = DependencyService.Get<IPlatformHelper>();

			if (pushData.ContainsKey ("aps")) {

				var aps = pushData ["aps"] as IDictionary<string, object>;

				if(aps != null && aps.ContainsKey("alert")) {
					var alertInfo = aps ["alert"];

					if (alertInfo is IDictionary<string, object>) {
						var alert = alertInfo as IDictionary<string, object>;

						_title 			= alert ["loc-key"].ToString();
						_option 		= alert ["action-loc-key"].ToString();
						_cancel 		= "Cancel";

						var tmp = alert.ContainsKey ("loc-args") ? alert ["loc-args"] as IEnumerable<object> : null;

						if (tmp != null) {

							_args = (from t in tmp
								where t != null
								select t.ToString ()
							).ToArray();
						}



					} else {
						_title = alertInfo.ToString ();

						if (pushData.ContainsKey ("bn")) 
							_cancel = pushData["bn"].ToString();	
						else
							_cancel = "OK";	
					}
				}
			}
		}

		public string GetOption (int index)
		{
			return (null == _option) ? null : ((null == _args) ? _platformHelper.Translate (_option) : string.Format (_platformHelper.Translate (_option), _args));
		}

		public void SetResult (int result)
		{
			_result = result;
		}
	}
}

