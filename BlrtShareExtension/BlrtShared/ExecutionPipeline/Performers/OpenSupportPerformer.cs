using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenSupportPerformer : ExecutionPerformer
	{

		public OpenSupportPerformer(IExecutor executor) : base(executor) {

		}


		protected override async Task Action ()
		{
			var support = await BlrtData.BlrtManager.App.OpenSupport (Executor);

			if (!support.Success)
				return;

		}
	}
}

