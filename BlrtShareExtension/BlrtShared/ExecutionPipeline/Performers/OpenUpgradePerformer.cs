using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenUpgradeAccountPerformer : ExecutionPerformer
	{
		public OpenUpgradeAccountPerformer(IExecutor executor) : base(executor) 
		{

		}


		protected override async Task Action ()
		{
			await BlrtData.BlrtManager.App.OpenUpgradeAccount (Executor);
		}
	}
}

