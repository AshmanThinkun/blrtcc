﻿#if __ANDROID__
using System;
using System.Threading.Tasks;
#if __iOS__
using UIKit;
#endif

namespace BlrtData.ExecutionPipeline
{
	public class CreateNewBlrtPerfomer : ExecutionPerformer
	{
		IRequestArgs _requestArgs;


		public CreateNewBlrtPerfomer(IExecutor executor) : base(executor) {
			_requestArgs = null;
		}
		public CreateNewBlrtPerfomer(IExecutor executor, IRequestArgs requestArgs) : base(executor) {

			_requestArgs = requestArgs;
		}


		protected override async Task Action ()
		{
			//we will not await the permission to make it faster
			BlrtData.BlrtManager.ApplyPermission (
				new BlrtData.UserPermissionHandler (),
				false
			).FireAndForget();

			var _helper = BlrtUtil.PlatformHelper;

			if (!BlrtData.BlrtPermissions.CanCreateConversation) {

				//TODO make an alert here

				return;
			}

			if (_requestArgs != null && !string.IsNullOrWhiteSpace (_requestArgs.RequestId)) {
			
				var id = _requestArgs.RequestId;
			
				var api = new BlrtAPI.APIRequestDetails(new BlrtAPI.APIRequestDetails.Parameters(){
					RequestId = id
				});

				BlrtManager.Responder.PushLoading (true);

				while (!await api.Run(BlrtUtil.CreateTokenWithTimeout(15000))) {	

					BlrtManager.Responder.PopLoading (true);			

					var alert = await BlrtData.BlrtManager.App.ShowAlert(Executor,
						new SimpleAlert(
							_helper.Translate("request_get_failed_title", ""),
							_helper.Translate("request_get_failed_message", ""),
							_helper.Translate("request_get_failed_cancel", ""),
							_helper.Translate("request_get_failed_retry", "")
						)
					);

					if (!alert.Success || alert.Result.Cancelled)
						return;


					BlrtManager.Responder.PushLoading (true);
				}

				BlrtManager.Responder.PopLoading (true);		

				if(api.Response.Success) {

					_requestArgs = api.Response;

				} else {

					var alert = await BlrtData.BlrtManager.App.ShowAlert(Executor,
						new SimpleAlert(
							_helper.Translate("request_get_permissions_title", ""),
							_helper.Translate("request_get_permissions_message", ""),
							_helper.Translate("request_get_permissions_cancel", ""),
							_helper.Translate("request_get_permissions_support", "")
						)
					);

					if (!alert.Success || alert.Result.Cancelled)
						return;

					await BlrtData.BlrtManager.App.OpenSendSupportMsg (Executor, "Failed to open RequestId: " + id);

					return;
				}
			}

			var result = await BlrtData.BlrtManager.App.OpenBlrtCreation (Executor, _requestArgs);
			//if (!(result.Success && result.Result.CreatedContent))
			//	return;

			//var mailbox = await BlrtData.BlrtManager.App.OpenMailbox (Executor, BlrtData.Models.Mailbox.MailboxType.Inbox);
			//if (!mailbox.Success)
			//	return;

			//var convo = await mailbox.Result.OpenConversation (Executor, result.Result.CreatedConversationId);
			//if (!convo.Success)
			//	return;

			//fix ios sometime can't hide keyboard automatically
#if __iOS__
			try{
				var convoScreen =  convo.Result as UIViewController;
				convoScreen.View.EndEditing(true);
			}catch{}
#endif
			//await convo.Result.OpenSendingPage (
			//	Executor,
			//	_requestArgs
			//);
		}
	}
}
#endif
