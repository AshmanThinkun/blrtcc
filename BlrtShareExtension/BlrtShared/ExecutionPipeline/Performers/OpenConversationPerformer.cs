using System;
using System.Threading.Tasks;
using BlrtData.Models.Mailbox;
using BlrtAPI;
using Acr.UserDialogs;
using Xamarin.Forms;
using Parse;

namespace BlrtData.ExecutionPipeline
{
	public class OpenConversationPerformer : ExecutionPerformer
	{
		string _conversationId;
		readonly string _accessToken;

		public MailboxType Mailbox { get; set; }

		public string ContentId { get; set; }

		public string RelationId { get; set; }

		public bool WithSMSLink{ get; set;}

		public OpenConversationPerformer (IExecutor executor, string conversationId, string accessToken = null) : base (executor)
		{

			_conversationId = conversationId;
			_accessToken = accessToken;

			Mailbox = MailboxType.Any;
		}


		protected override async Task Action ()
		{
			if (null == ParseUser.CurrentUser) {
				if (string.IsNullOrWhiteSpace (ContentId)) {
					var alert = await BlrtData.BlrtManager.App.ShowAlert (Executor,
						new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate ("convo_url_access_without_log_in_title"),
							BlrtUtil.PlatformHelper.Translate ("convo_url_access_without_log_in_message"),
							BlrtUtil.PlatformHelper.Translate ("convo_url_access_without_log_in_cancel"),
							BlrtUtil.PlatformHelper.Translate ("convo_url_access_without_log_in_view_blrt"),
							BlrtUtil.PlatformHelper.Translate ("convo_url_access_without_log_in_login")
						)
					);
					if (!alert.Success || alert.Result.Cancelled) {
						return;
					} else {
						switch (alert.Result.Result) {
							case 0: // play 1st
								await ViewFirstBlrt ();
								return;
							case 1: // sign up
								await OpenSignUp ();
								return;
							default:
								return;
						}
					}
				} else {
					await new OpenIndependentBlrtPerformer (Executor, BlrtEncode.EncodeId (ContentId), _accessToken).RunAction ();
				}
				return;
			}

			var mailbox = await BlrtData.BlrtManager.App.OpenMailbox (Executor, Mailbox);

			if (!mailbox.Success)
				return;

			var convo = await mailbox.Result.OpenConversation (Executor, _conversationId, this.WithSMSLink);

			if (!convo.Success)
				return;

			if (!string.IsNullOrEmpty (ContentId)) {
				var watch = await convo.Result.OpenContentItem (Executor, ContentId);

				if (!watch.Success)
					return;
			}
		}

		async Task ViewFirstBlrt ()
		{
			string encodedBlrtId = null;
			using (UserDialogs.Instance.Loading ("Loading...")) {
				while (true) {
					try {
						var apiCall = new APIUrlInfo (new APIUrlInfo.Parameters () {
							url = BlrtConstants.ServerUrl + "/conv/" + BlrtEncode.EncodeId (_conversationId)
						});
						if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
							if (apiCall.Response.Data != null) {
								encodedBlrtId = apiCall.Response.Data.BlrtId;
							}
						}
						if (!string.IsNullOrWhiteSpace (encodedBlrtId)) {
							break;
						}
						throw new Exception ("cannot get correct blrt id");
					} catch (Exception ex) {
						if (await AlertInternetErrorRetry ()) {
							continue;
						} else {
							break;
						}
					}
				}
			}

			if (string.IsNullOrWhiteSpace (encodedBlrtId)) {
				return;
			}
			await new OpenIndependentBlrtPerformer (Executor, encodedBlrtId, _accessToken).RunAction ();
		}

		async Task<bool> AlertInternetErrorRetry ()
		{
			var tcs = new TaskCompletionSource<bool> ();
			Device.BeginInvokeOnMainThread (() => {
				BlrtManager.App.ShowAlert (Executor, 
					new SimpleAlert(
						BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_title"),
						BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_message"),
						BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_cancel"),
						BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_retry")
					)
				).ContinueWith ((t) => {
					var alert = t.Result;
					if (!alert.Success || alert.Result.Cancelled) {
						tcs.TrySetResult (false);
					} else {
						tcs.TrySetResult (true);
					}
				});
			});
			return await tcs.Task;
		}

		async Task OpenSignUp ()
		{
			var openResult = await BlrtManager.App.OpenSignUp (Executor);
			if (openResult.Success && openResult.Result != null) {
				openResult.Result.OpenSignUpMenu (Executor, true);
			}
		}

		public static async Task RequestAccess (string EncodedConversationId, IExecutor executor)
		{
			if (ParseUser.CurrentUser == null)
				return;

			var alert = await BlrtData.BlrtManager.App.ShowAlert (executor,
	            new SimpleAlert (
		            BlrtUtil.PlatformHelper.Translate ("request_access_title", "conversation_screen"),
		            BlrtUtil.PlatformHelper.Translate ("request_access_message", "conversation_screen"),
		            BlrtUtil.PlatformHelper.Translate ("request_access_cancel", "conversation_screen"),
		            BlrtUtil.PlatformHelper.Translate ("request_access_accept", "conversation_screen")
	            )
            );
			if (alert.Success && (alert.Result.Result == 0)) {
				//send api request
				var apiCall = new APIConversationRequestAccess (new APIConversationRequestAccess.Parameters () {
					ConversationId = EncodedConversationId
				});
				BlrtManager.Responder.PushLoading (true);
				if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {
					BlrtManager.Responder.PopLoading (true);

					if (apiCall.Response.Success) {
						if (apiCall.Response.Status.Code == APIConversationRequestAccess.RelationExist) {
							BlrtManager.App.ShowAlert (executor, new SimpleAlert (
								BlrtUtil.PlatformHelper.Translate ("request_access_refresh_title", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_refresh_message", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_refresh_cancel", "conversation_screen"))
							);
						}else{
							BlrtManager.App.ShowAlert (executor, new SimpleAlert (
								BlrtUtil.PlatformHelper.Translate ("request_access_success_title", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_success_message", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_success_cancel", "conversation_screen"))
							);
						}
						return;
					} else {
						var code = apiCall.Response.Error.Code;
						if (code == APIConversationRequestAccessError.conversationFull) {
							BlrtManager.App.ShowAlert (executor, new SimpleAlert (
								BlrtUtil.PlatformHelper.Translate ("request_access_full_title", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_full_message", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_full_cancel", "conversation_screen"))
							);
						} else if (code == APIConversationRequestAccessError.userInBlackList) {
							BlrtManager.App.ShowAlert (executor, new SimpleAlert (
								BlrtUtil.PlatformHelper.Translate ("request_access_blacklist_title", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_blacklist_message", "conversation_screen"), 
								BlrtUtil.PlatformHelper.Translate ("request_access_blacklist_cancel", "conversation_screen"))
							);
						} else {
							BlrtManager.App.ShowAlert (executor, new SimpleAlert (
								BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
								BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
								BlrtUtil.PlatformHelper.Translate ("OK"))
							);
						}
					}
				} else {
					BlrtManager.Responder.PopLoading (true);

					BlrtManager.App.ShowAlert (executor, new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"), 
						BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"), 
						BlrtUtil.PlatformHelper.Translate ("OK"))
					);
				}
			}
		}
	}
}

