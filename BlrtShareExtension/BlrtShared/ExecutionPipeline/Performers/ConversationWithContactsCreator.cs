﻿
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class ConversationWithContactsCreator : ExecutionPerformer
	{
		IRequestArgs _requestArgs;

		public ConversationWithContactsCreator (IExecutor executor, IRequestArgs requestArgs) : base (executor)
		{
			_requestArgs = requestArgs;
		}

		protected override async Task Action ()
		{
			if (_requestArgs != null && !string.IsNullOrWhiteSpace (_requestArgs.RequestId)) {

				var apiResponse = await BlrtUtil.CallRequestedAPI (Executor, _requestArgs.RequestId);

				if (apiResponse == null) {
					return;
				}
			}

			var conversationCreated = await CreateNewConversationWithContact ();

			if (conversationCreated) {
				await BlrtManager.App.OpenBlrtCreation (Executor, _requestArgs);
			}
		}

		async Task<bool> CreateNewConversationWithContact ()
		{
#if __IOS__
			return await BlrtManager.App.CreateConversationWithContacts (Executor, _requestArgs);
#endif
#if __ANDROID__
			return false;
#endif
		}
	}
}
