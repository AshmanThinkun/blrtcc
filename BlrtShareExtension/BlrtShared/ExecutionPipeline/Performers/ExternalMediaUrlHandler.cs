#if __IOS__
using System;
using System.Threading.Tasks;
#if __iOS__
using UIKit;
#endif

namespace BlrtData.ExecutionPipeline
{
#if __IOS__
	public class ExternalMediaUrlHandler : ExecutionPerformer
	{
		IRequestArgs _requestArgs;


		public ExternalMediaUrlHandler (IExecutor executor) : base (executor)
		{
			_requestArgs = null;
		}
		public ExternalMediaUrlHandler (IExecutor executor, IRequestArgs requestArgs) : base (executor)
		{

			_requestArgs = requestArgs;
		}


		protected override async Task Action ()
		{

			if (_requestArgs != null && !string.IsNullOrWhiteSpace (_requestArgs.RequestId)) {

				var apiResponse = await BlrtUtil.CallRequestedAPI (Executor, _requestArgs.RequestId);

				if (apiResponse == null) {
					return;
				}
			}

			/*var result = await BlrtData.BlrtManager.App.OpenBlrtCreation (Executor, _requestArgs);
			if (!(result.Success && result.Result.CreatedContent))
				return;*/

			ShowPopupToChooseBetweenBlrtAndMedia ();


			/*var convo = await mailbox.Result.OpenConversation (Executor, result.Result.CreatedConversationId);
			if (!convo.Success)
				return;*/

			//fix ios sometime can't hide keyboard automatically
			try {
				/*var convoScreen =  convo.Result as UIViewController;
				convoScreen.View.EndEditing(true);*/
			} catch { }
			/*await convo.Result.OpenSendingPage (
				Executor,
				_requestArgs
			);*/
		}

		void ShowPopupToChooseBetweenBlrtAndMedia ()
		{

			var alert = UIAlertController.Create (BlrtUtil.PlatformHelper.Translate ("import_to_blrt_title", "import_to_blrt"),
												  BlrtUtil.PlatformHelper.Translate ("existing_or_new_convo_msg", "import_to_blrt"),
												  UIAlertControllerStyle.Alert);

			alert.View.TintColor = BlrtStyles.iOS.AccentBlue;

			var blrt = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("existing_convo_btn_title", "import_to_blrt"),
											 UIAlertActionStyle.Default,
											 OnSelectExistingConversationSelected);
			var media = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("create_new_convo_btn_title", "import_to_blrt"),
											  UIAlertActionStyle.Default,
											  OnCreateNewConversationSelected);
			var cancel = UIAlertAction.Create (BlrtUtil.PlatformHelper.Translate ("import_to_blrt_cancel_btn_title", "import_to_blrt"),
											   UIAlertActionStyle.Destructive,
											   null);


			alert.AddAction (blrt);
			alert.AddAction (media);
			alert.AddAction (cancel);

			UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (alert, true, null);

		}

		/// <summary>
		/// Triggered when the create new conversation is selected to create brt or send external media in.
		/// </summary>
		/// <param name="obj">Object.</param>
		async void OnCreateNewConversationSelected (UIAlertAction obj)
		{
			await BlrtManager.App.CreateConversationToCreateNewConvoItemsUsingExternalMedia (Executor, _requestArgs);
		}

		/// <summary>
		/// Triggered when the existing conversation is selected to create brt or send external media in.
		/// </summary>
		/// <param name="obj">Object.</param>
		async void OnSelectExistingConversationSelected (UIAlertAction obj)
		{
			await BlrtManager.App.OpenMailboxToCreateNewConvoItemsUsingExternalMedia (Executor,
																					  Models.Mailbox.MailboxType.Inbox,
																					  _requestArgs);
		}
	}
#endif

}
#endif
