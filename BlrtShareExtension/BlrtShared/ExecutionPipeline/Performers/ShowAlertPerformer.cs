using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class ShowAlertPerformer : ExecutionPerformer
	{
		IAlert _alert;
		Action<int> _action;

		public ShowAlertPerformer(IExecutor executor, IAlert alert, Action<int> action) : base(executor) {

			_alert = alert;
			_action = action;
		}


		protected override async Task Action ()
		{
			var result = await BlrtData.BlrtManager.App.ShowAlert (
				Executor,
				_alert
			);



			if (result.Success && !result.Result.Cancelled) {
				if(_action != null)
					_action.Invoke(result.Result.Result);
			}
		}
	}
}

