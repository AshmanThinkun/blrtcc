﻿using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenArchivePerformer : ExecutionPerformer
	{

		public OpenArchivePerformer (IExecutor executor) : base (executor)
		{

		}


		protected override async Task Action ()
		{
			var share = await BlrtData.BlrtManager.App.OpenMailbox (Executor, Models.Mailbox.MailboxType.Archive);

			if (!share.Success)
				return;

		}
	}
}