﻿using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenNewsPerformer : ExecutionPerformer
	{

		public OpenNewsPerformer (IExecutor executor) : base (executor)
		{

		}


		protected override async Task Action ()
		{
			var share = await BlrtData.BlrtManager.App.OpenNews (Executor);

			if (!share.Success)
				return;

		}
	}
}
