using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenWebpagePerformer : ExecutionPerformer
	{
		string _url;

		public OpenWebpagePerformer (IExecutor executor, string url) : base (executor)
		{
			_url = url;
		}

		protected override Task Action ()
		{
			return BlrtManager.OpenHyperlink (_url);
		}
	}
}

