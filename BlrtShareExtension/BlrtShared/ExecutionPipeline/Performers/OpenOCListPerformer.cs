﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using BlrtData.Query;
using Parse;

namespace BlrtData.ExecutionPipeline
{
	public class OpenOCListPerformer : ExecutionPerformer
	{
		readonly string DecodedOCListId;
		readonly bool AutoSubscribe;
		public OpenOCListPerformer (IExecutor executor, string encodedOCListId, bool autoSubscribe = false) : base (executor)
		{
			if (null == encodedOCListId) {
				throw new ArgumentNullException ("encodedOCListId");
			}
			var trimmedEncodedOCListId = encodedOCListId.Trim ();
			if (string.IsNullOrWhiteSpace (trimmedEncodedOCListId)) {
				throw new InvalidOperationException ("emply oclist id");
			}
			DecodedOCListId = BlrtEncode.DecodeId (trimmedEncodedOCListId);
			if (string.IsNullOrWhiteSpace (DecodedOCListId)) {
				throw new InvalidOperationException ("cannot decode the oclist id");
			}
			AutoSubscribe = autoSubscribe;
		}

		protected override async Task Action ()
		{
			if (BlrtManager.Responder.InMaintanceMode) {
				return;
			}

			if (null == ParseUser.CurrentUser) {
				return;
			}

			OCList ocList = LocalStorageParameters.User.DataManager.GetOCList (DecodedOCListId);
			if (null == ocList || null == ocList.Name) {
				using (UserDialogs.Instance.Loading ("Loading...")) {
					try {
						var queryList = BlrtOCListQuery.RunQuery (DecodedOCListId);
						await queryList.WaitAsync ();
						ocList = LocalStorageParameters.User.DataManager.GetOCList (DecodedOCListId);
					} catch (Exception e) {
						BlrtUtil.Debug.ReportException (e);
					}
				}
			}
			
			if (null == ocList || null == ocList.Name) {
				await BlrtData.BlrtManager.App.ShowAlert (
					Executor,
					new SimpleAlert("Error", "Cannot find this Blrt list", "OK")
				);
			} else {
				// open list page
				await BlrtData.BlrtManager.App.OpenOrganisationList (Executor, DecodedOCListId);
				if (AutoSubscribe) {
					// subscribe
					using (UserDialogs.Instance.Loading ("Subscribing...")) {
						try {
							var query = BlrtSubscribeOCListQuery.RunQuery (DecodedOCListId);
							await query.WaitAsync ();
						} catch (Exception e) {
							BlrtUtil.Debug.ReportException (e);
							BlrtData.BlrtManager.App.ShowAlert (
								Executor,
								new SimpleAlert("Error", "Failed subscribe this channel", "OK")
							);
						}

					}
				}
			}
		}
	}
}

