﻿using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class OpenSharePerformer : ExecutionPerformer
	{

		public OpenSharePerformer(IExecutor executor) : base(executor) {

		}


		protected override async Task Action ()
		{
			var share = await BlrtData.BlrtManager.App.OpenShare (Executor);

			if (!share.Success)
				return;

		}
	}
}

