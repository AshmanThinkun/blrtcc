using System;
using System.Threading.Tasks;
using BlrtData.IO;
using System.Collections.Generic;
using System.Linq;
using BlrtAPI;
using BlrtData.Media;
using BlrtData.Contents;
using Xamarin.Forms;
using Acr.UserDialogs;
using Parse;

namespace BlrtData.ExecutionPipeline
{
	public class OpenIndependentBlrtPerformer : ExecutionPerformer
	{


		readonly string EncodedBlrtId;
		readonly string DecodedBlrtId;
		readonly string _accessToken;
		bool _isPublic;
		public OpenIndependentBlrtPerformer (IExecutor executor, string encodedBlrtId, string accessToken = null) : base(executor)
		{
			if (null == encodedBlrtId) {
				throw new ArgumentNullException ("encodedBlrtId");
			}
			EncodedBlrtId = encodedBlrtId.Trim ();
			if (string.IsNullOrWhiteSpace (EncodedBlrtId)) {
				throw new InvalidOperationException ("emply blrt id");
			}
			DecodedBlrtId = BlrtEncode.DecodeId (EncodedBlrtId);
			if (string.IsNullOrWhiteSpace (DecodedBlrtId)) {
				throw new InvalidOperationException ("cannot decode the blrt id");
			}
			_accessToken = accessToken;
			_isPublic = false;
		}

		#region implemented abstract members of ExecutionPerformer

		protected override async Task Action ()
		{
			// 0. the app should not be in maintance mode
			if (BlrtManager.Responder.InMaintanceMode) {
				return;
			}

			UserDialogs.Instance.ShowLoading ("Loading...");
			Dictionary<string, string> queries = null;
			if (null != _accessToken) {
				queries = new Dictionary<string, string> ();
				queries.Add (BlrtConstants.UrlQueryKey_AccessToken, _accessToken);
			}
			var blrtUrl = BlrtEncode.EncodePrivateBlrtUrl (EncodedBlrtId, true, queries);
			var apiCall = new APIConversationItemFromUrl (new APIConversationItemFromUrl.Parameters() {
				Url = blrtUrl,
				IncludeMedia = true
			});

			while (!await apiCall.Run(BlrtUtil.CreateTokenWithTimeout(15000))) {	

				UserDialogs.Instance.HideLoading ();			

				if (!(await AlertInternetErrorRetry ())) {
					return;
				}

				UserDialogs.Instance.ShowLoading ("Loading...");
			}

			UserDialogs.Instance.HideLoading ();
			if (!apiCall.Response.Success) {
				switch (apiCall.Response.Error.Code) {
					case APIConversationItemFromUrlError.AuthenticationFailure:
					case APIConversationItemFromUrlError.InvalidUrl: // url invalid of blrt deleted
					case APIConversationItemFromUrlError.NotBlrt:
						InvalidUrlError (blrtUrl, apiCall.Response);
						return;
					case APIConversationItemFromUrlError.PlayCounterLimit:
						await BlrtData.BlrtManager.App.ShowAlert(Executor,
							new SimpleAlert(
								BlrtUtil.PlatformHelper.Translate ("independent_reach_play_counter_limit_title"),
								BlrtUtil.PlatformHelper.Translate ("independent_reach_play_counter_limit_message"),
								BlrtUtil.PlatformHelper.Translate ("independent_reach_play_counter_limit_cancel")
							)
						);
						return;
					default:
						UnknownError (blrtUrl, apiCall.Response.Error);

						return;
				}
			} else {
				var playData = apiCall.Response.Data;
				var playArg = BlrtMediaPagePrototype.FromsString (playData.PlayData.Argument);
				if (null == playArg) {
					return;
				}
				_isPublic = playData.IsPublicBlrt;

				var localStorageType = LocalStorageType.Default;
				BlrtData.Data.BlrtDataManagerBase dataManager = BlrtManager.DataManagers.Default;


				if (playData.IsPublicBlrt) {

					localStorageType = LocalStorageType.IndependentPublic;
					dataManager = BlrtManager.DataManagers.IndependentPublic;

				} else {
					if (null != ParseUser.CurrentUser) {
						if (!string.IsNullOrWhiteSpace (playData.ConversationId)) {
							var itr = new OpenConversationPerformer (Executor, BlrtEncode.DecodeId (playData.ConversationId)) {
								ContentId = DecodedBlrtId
							};
							await itr.RunAction ();
							return;
						} else {
							localStorageType = LocalStorageType.User;
							dataManager = BlrtManager.DataManagers.User;
						}
					} else {
						localStorageType = LocalStorageType.IndependentPrivate;
						dataManager = BlrtManager.DataManagers.IndependentPrivate;
					}
				}
				var media = playData.PlayData.Media;
				var mediaIds = new string [media.Length];

				for (int i = 0; i < media.Length; i++) {

					var item = media [i];

					dataManager.AddMediaAsset (
						new MediaAsset (localStorageType) {
							OnParse = true,
							CloudMediaPath = new Uri (item.MediaFile),
							EncryptType = item.EncryptType,
							EncryptValue = item.EncryptValue,
							ObjectId = item.Id,
							Format = item.MediaFormat,
							IsIndependent = true,
							LocalMediaName = BlrtUtil.FileUrlFileName (item.MediaFile),
							PageArgs = BlrtUtil.PagesToList (item.PageArgs).ToArray ()
						}
					);

					mediaIds [i] = item.Id;
				}

				var blrtItem = new ConversationBlrtItem (localStorageType) {
					CloudAudioPath = new Uri (playData.PlayData.AudioFile),
					CloudTouchDataPath = new Uri (playData.PlayData.TouchDataFile),
					LocalAudioName = BlrtUtil.FileUrlFileName (playData.PlayData.AudioFile),
					LocalTouchDataName = BlrtUtil.FileUrlFileName (playData.PlayData.TouchDataFile),
					MediaData = new BlrtRecordData(playArg),
					EncryptType = playData.PlayData.EncryptType,
					EncryptValue = playData.PlayData.EncryptValue,
					Media = mediaIds,
					OnParse = true,
					IsIndependent = true,
					LocalThumbnailNames = new string [1],
					CloudThumbnailPaths = new Uri [1]
				};

				dataManager.AddConversationItem (blrtItem);

				while (true) {
					var mediaAssets = dataManager.GetMediaAssets(blrtItem);
					var downloaders = BlrtUtil.Concat (
						BlrtUtil.Concat (
							from m in mediaAssets
							where !m.IsDataLocal ()
							select m.DownloadContent (1000)
						), 
						blrtItem.IsDataLocal () ? null : blrtItem.DownloadContent (1000)
					);
					downloaders = (
						from d in downloaders
						where null != d
						select d
					).ToArray ();
					bool needRetry = false;
					var download = new BlrtDownloaderList (downloaders);
					if (download.Count > 0) {
						var result = await BlrtManager.App.ShowProgressAlert (Executor, 
							new SimpleProgressAlert (
								download,
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_title"),
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_message"),
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_cancel")
							)
						);
						if (result.Success) {
							if (result.Result.Finished) {
								needRetry = false;
							} else {
								// user pressed cancel
								return;
							}
						} else {
							needRetry = true;
						}
					
					}
					// check file validation even if download failed or no file downloaded
					using (UserDialogs.Instance.Loading ("Loading...")) {
						foreach (var m in mediaAssets) {
							if (!m.IsDecypted) {
								try {
									await m.DecyptData ();
								} catch (BlrtEncryptorManagerException e1) {
									needRetry = true;
									m.DeleteData ();
								} catch (InvalidOperationException e2) {
									needRetry = true;
									m.DeleteData ();
								}
							}
						}
						if (!blrtItem.IsAudioDecrypted) {
							try {
								await blrtItem.DecyptAudioData ();
							} catch (BlrtEncryptorManagerException e3) {
								needRetry = true;
								blrtItem.DeleteAudioData ();
							} catch (InvalidOperationException e4) {
								needRetry = true;
								blrtItem.DeleteAudioData ();
							}
						}
						if (!blrtItem.IsTouchDecrypted) {
							try {
								await blrtItem.DecyptAudioData();
							} catch (BlrtEncryptorManagerException e5) {
								needRetry = true;
								blrtItem.DeleteTouchData ();
							} catch (InvalidOperationException e6) {
								needRetry = true;
								blrtItem.DeleteTouchData ();
							}
						}
					}

					if (needRetry) {
						if (await AlertInternetErrorRetry ()) {
							continue;
						} else {
							return;
						}
					} else {
						break;
					}
				}
				MessagingCenter.Subscribe (this, BlrtConstants.FormsMessageId.BlrtPlayedOnce, (object sender) => {
					IncreasePlayCounter ();
				});

				BlrtData.Helpers.BlrtTrack.TrackViewedContent (blrtItem.IsPublicBlrt ? "Public" : "Private", blrtItem.ObjectId);
				var dic = new Dictionary<string, object>();
				dic.Add("type", blrtItem.IsPublicBlrt? "public": "private");
				dic.Add("blrtLength", blrtItem.AudioLength);
				dic.Add("blrtId", blrtItem.ObjectId);
				dic.Add("numberOfPages",blrtItem.Pages);
				BlrtData.Helpers.BlrtTrack.TrackScreenView("Canvas_Playback", false,dic);
				BlrtManager.App.OpenCanvas (Executor, blrtItem).ContinueWith ((t) => {
					MessagingCenter.Unsubscribe<object> (this, BlrtConstants.FormsMessageId.BlrtPlayedOnce);
				});
			}

		}

		async Task InvalidUrlError (string blrtUrl, APIConversationItemFromUrlResponse response)
		{
			if (ParseUser.CurrentUser != null) {
				if(response.Error.Code == APIConversationItemFromUrlError.AuthenticationFailure){
					OpenConversationPerformer.RequestAccess (response.Data.ConversationId, this.Executor);
					return;
				}
				var invalidUrlAlert = await BlrtData.BlrtManager.App.ShowAlert(Executor,
					new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_title"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_message"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_cancel"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_support")
					)
				);
				if (invalidUrlAlert.Success && (invalidUrlAlert.Result.Result == 0)) {
					BlrtManager.App.OpenSendSupportMsg (
						Executor, 
						string.Format (
							"Invalid Blrt url: {0}, error code: {1}, error message: {2}.\n", 
							blrtUrl, 
							response.Error.Code, 
							response.Error.Message
						)
					);
				}
			} else {
				await BlrtData.BlrtManager.App.ShowAlert(Executor,
					new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_title"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_message"),
						BlrtUtil.PlatformHelper.Translate ("checking_conversation_to_open_failed_cancel")
					)
				);
			}
			return;
		}

		async Task UnknownError (string blrtUrl, APIResponseStatus error)
		{
			if (ParseUser.CurrentUser != null) {
				var alert = await BlrtData.BlrtManager.App.ShowAlert(Executor,
					new SimpleAlert(
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_title"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_message"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_cancel"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_update"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_support")
					)
				);

				if (!alert.Success || alert.Result.Cancelled)
					return;

				switch (alert.Result.Result) {
					case 0:
						BlrtManager.Responder.OpenWebUrl (BlrtManager.Settings.app_store_url, false);
						return;
					case 1:
						BlrtManager.App.OpenSendSupportMsg (
							Executor, 
							string.Format (
								"Unknow error. Blrt url: {0}, error code: {1}, error message: {2}.\n", 
								blrtUrl, 
								error.Code, 
								error.Message
							)
						);
						return;
					default:
						return;
				}
			} else {
				var alert = await BlrtData.BlrtManager.App.ShowAlert(Executor,
					new SimpleAlert(
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_title"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_message"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_cancel"),
						BlrtUtil.PlatformHelper.Translate ("independent_blrt_info_unknown_error_update")
					)
				);

				if (!alert.Success || alert.Result.Cancelled)
					return;
				if (alert.Result.Result == 0) {
					BlrtManager.Responder.OpenWebUrl (BlrtManager.Settings.app_store_url, false);
				}
			}
		}

		const int MaxRetry = 5;
		async Task IncreasePlayCounter ()
		{
			if (_isPublic) {
				IncreasePublicBlrtPlayCounter ();
			} else {
				if (null == ParseUser.CurrentUser) {
					IncreasePrivateBlrtPlayCounter ();
				}
			}
		}

		async Task IncreasePrivateBlrtPlayCounter ()
		{
			var apiCall = new APIPrivateConversationItemView (new APIPrivateConversationItemView.Parameters () {
				BlrtId = DecodedBlrtId
			});
			int counter = 0;
			while (!await apiCall.Run(BlrtUtil.CreateTokenWithTimeout(15000))) {	
				++counter;
				if (MaxRetry == counter) {
					return;
				}
			}
			if (!apiCall.Response.Success) {
				if (apiCall.Response.Error.Code == APIPrivateConversationItemViewError.isPublicBlrt) {
					IncreasePublicBlrtPlayCounter ();
				}
			}
		}

		async Task IncreasePublicBlrtPlayCounter ()
		{
			var apiCall = new APIPublicConversationItemView (EncodedBlrtId);
			apiCall.Run (BlrtUtil.CreateTokenWithTimeout(15000));
		}
		#endregion

		async Task<bool> AlertInternetErrorRetry ()
		{
			var alert = await BlrtManager.App.ShowAlert (Executor, 
				new SimpleAlert(
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_title"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_message"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_cancel"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_retry")
				)
			);
			if (!alert.Success || alert.Result.Cancelled) {
				return false;
			} else {
				return true;
			}
		}

		public class IndependentBlrtData : ILocalBlrtArgs
		{
			#region ILocalBlrtArgs implementation

			public LocalMedia[] Media { get; set; }

			public LocalBlrtPage[] Pages { get; set; }

			public string AudioPath { get; set; }

			public string TouchPath { get; set; }

			#endregion


		}
	}
}

