using System;
using System.Threading.Tasks;

namespace BlrtData.ExecutionPipeline
{
	public class ExecutionResult<T> where T : class{
	
		public bool Success { get; private set; }
		public T Result { get; private set; }
		public Exception Exception { get; private set; }

		public ExecutionResult(T result){
			if (result == null) throw new ArgumentNullException ();

			this.Result = result;
			this.Success = true;
		}

		public ExecutionResult(Exception exception){
			if (exception == null) throw new ArgumentNullException ();

			this.Exception = exception;
			this.Success = false;
		}
	}
	[Flags]
	public enum ExecutionSource
	{
		Unknown		= 0,

		System		= 1 << 0,
		AppLinks	= 1 << 1,
		Push		= 1 << 2,
		InApp		= 1 << 3
	}

	public interface IExecutor {
		ExecutionSource Cause { get; }
	}

	public class Executor : IExecutor {
		public ExecutionSource Cause { get; private set; }

		public Executor (ExecutionSource cause){
			this.Cause = cause;
		}

		public static Executor System(){
			return new Executor (ExecutionSource.System);
		}
	}


}

