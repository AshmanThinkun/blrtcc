using System;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;
using Xamarin.Forms;

namespace BlrtData
{
	public class SimpleAlert : IAlert
	{
		int? _result;

		string _title;
		string _message;
		string _cancel;

		string[] _options;


		public string Title {
			get { return _title; }
		}

		public string Message {
			get { return _message; }
		}

		public string Cancel {
			get { return _cancel; }
		}

		public int OptionCount {
			get { return _options.Length;}
		}

		public int Result {
			get { return _result ?? -1; }
		}

		public bool Cancelled {
			get { return _result != null && Result < 0; }
		}

		public SimpleAlert (
			string title, 
			string message, 
			string cancel, 
			params string[] options
		) {
			_title		= title;
			_message	= message;
			_cancel		= cancel;
			_options	= options;
		}

		public string GetOption (int index)
		{
			return _options [index];
		}

		public void SetResult (int result)
		{
			_result = result;
		}
	}
}

