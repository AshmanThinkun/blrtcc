using System;
using BlrtData.ExecutionPipeline;

namespace BlrtData
{
	public class SimpleProgressAlert : IProgressAlert
	{
		#region IProgressAlert implementation

		public IProgress Source { get; protected set; }

		public string Title { get; protected set; }

		public string Message { get; protected set; }

		public string Cancel { get; protected set; }


		public void SetResult (bool finished)
		{
			Finished = finished;
		}

		public bool Finished { get; protected set; }
		#endregion

		public SimpleProgressAlert (
			IProgress source,
			string title,
			string message,
			string cancel
		) {
			Source = source;
			Title = title;
			Message = message;
			Cancel = cancel;
		}
	}
}

