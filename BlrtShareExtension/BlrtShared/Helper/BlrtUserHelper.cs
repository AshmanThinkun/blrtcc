using System;
using System.Linq;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace BlrtData
{
	public static class BlrtUserHelper
	{
		public const string EmailVerified2Key		 	= "emailVerified2";
		public const string AccountStatusKey		 	= "accountStatus";

		public const string LastActiveKey 				= "lastActive";
		public const string RelationsDirtyKey 			= "relationsDirty";

		public const string EmailKey					= "email";
		public const string PhoneNumberKey				= "phoneNumber";

		public const string HasnotSetDisplaynameKey		= "hasnotSetDisplayname";

		public const string QueuedConvoTemplates		= "queuedConvoTemplates";


		public const string HasDoneFreeTrialKey			= "hasDoneFreeTrial";

		public const string HasAgreedToTOSKey 			= "hasAgreedToTOS";
		public const string ReferredByKey 					= "referredBy";

		public const string HasSetPasswordKey			= "hasSetPassword";

		public const string ExpiredActionKey	 		= "expiredAction";
		public const string ExpiresInPrefixKey 			= "expirationIn";

		public const string FacebookMetaId 				= "fb_id";
		public const string FacebookMetaLink 			= "fb_link";
		public const string FacebookMetaLocale 			= "fb_locale";
		public const string FacebookMetaUsername 		= "fb_username";
		public const string FacebookMetaFirstName 		= "fb_first_name";
		public const string FacebookMetaLastName 		= "fb_last_name";
		public const string FacebookMetaName	 		= "fb_name";
		public const string FacebookMetaVerified		= "fb_verified";

		public const string FirstNameKey 				= "first_name";
		public const string LastNameKey 				= "last_name";
		public const string DisplayNameKey				= "name";

		public const string ORGANISATION_NAME 			= "organisation";
		public const string IndustryKey		 			= "industry";

		public const string UserGenderKey 				= "gender";

		public const string GAVE_FEEDBACK_AT 			= "gaveFeedbackAt";

		public const string ALLOW_EMAIL_NEW_BLRT		= "allowEmailNewBlrt";
		public const string ALLOW_EMAIL_REQUEST			= "allowEmailRequest";
		public const string ALLOW_EMAIL_REBLRT			= "allowEmailReBlrt";
		public const string ALLOW_EMAIL_COMMENTS		= "allowEmailComments";

		public const string ALLOW_PUBLIC_EMAIL			= "allowPublicEmail"; // Allow other people to share blrts with you (When your only contact has been blrt)

		public const string AccountTypeExpiration		= "accountTypeExpirationDate";

		public const string DEVICE_LOCAL				= "deviceLocal";
		public const string DEVICE_LANG					= "deviceLangArray";

		public const string DisabledKey 				= "disabled";

		public const string BlockedUserFlagsKey 		= "blockedUserFlags";


		public const string ConversationCreationCounter = "conversationCreationCounter";

		public const string AVATAR_KEY 					= "avatar";
		public const string AVATAR_FROM_KEY 			= "avatarFrom";
		public const string PUBLIC_BLRT_COUNT_KEY   	= "publicBlrtCount";
		public const string HAS_ZOOOMED_KEY   			= "hasZoomed";
		public const string HAS_DRAWN_KEY   			= "hasDrawn";

		public static string TIMESTAMP = DateTime.Now.ToFileTime ().ToString ();

		public static void GenerateUnsignedId ()
		{
			TIMESTAMP = DateTime.Now.ToFileTime ().ToString ();
		}

		static Dictionary<string, string> _installMetaData;

		public static Dictionary<string, string> InstallMetaData {
			get {
				return _installMetaData;
			}
			set {
				if (value != _installMetaData) {	
					_installMetaData = value;
					if (value != null && !BlrtPersistant.Properties.ContainsKey ("HasSignedUpWithSignupUrl")) {
						BlrtData.Helpers.BlrtTrack.ExtraMetaData = value;
					}
				}
			}
		}

		public static void SetTimeStamp ()
		{
			TimeStamp = DateTime.Now.ToBinary().ToString ();
		}

		public static string TimeStamp { get; set;}

		public static int PublicBlrtCount {
			get {
				if (ParseUser.CurrentUser == null)
					return 0;
				return ParseUser.CurrentUser.Get<int> (PUBLIC_BLRT_COUNT_KEY, 0);
			}
		}

		public static string PhoneNumber {
			get {
				if (ParseUser.CurrentUser == null)
					return "";
				return ParseUser.CurrentUser.Get<string> (PhoneNumberKey, "");
			}
		}

		public static string CurrentUserName {
			get {
				if (ParseUser.CurrentUser == null)
					return "";
				return ParseUser.CurrentUser.Get<string> (DisplayNameKey, "");
			}
		}
		public static bool HasDoneFreeTrial {
			get {
				if (ParseUser.CurrentUser == null)
					return false;
				return ParseUser.CurrentUser.Get<bool> (HasDoneFreeTrialKey, false);
			}
		}

		public static IEnumerable<BlrtUserContactDetail> ContactDetails {
			get { return BlrtManager.DataManagers.User.GetUserContactDetails (); }
		}
		public static IEnumerable<BlrtUserContactDetail> EmailAddresses {
			get {
				if (ContactDetails == null)
					return null;

				var emails = new List<BlrtUserContactDetail> ();

				foreach (var contact in ContactDetails) {
					if (contact.Type != BlrtData.Contacts.BlrtContactInfoType.Email)
						continue;

					emails.Add (contact);
				}

				return emails;
			}
		}
		public static BlrtUserContactDetail PrimaryEmail {
			get {
				if (ContactDetails == null || ParseUser.CurrentUser == null)
					return null;

				foreach (var contact in EmailAddresses) {
					if (contact.Value == ParseUser.CurrentUser.Username)
						return contact;
				}

				return null;
			}
		}
		public static IEnumerable<BlrtUserContactDetail> SecondaryEmails {
			get {
				if (ContactDetails == null || ParseUser.CurrentUser == null)
					return new BlrtUserContactDetail [0];

				var emails = new List<BlrtUserContactDetail> ();
				var primaryEmail = PrimaryEmail;

				foreach (var contact in EmailAddresses) {
					if (contact == primaryEmail)
						continue;

					emails.Add (contact);
				}

				return emails;
			}
		}
		public static BlrtUserContactDetail FacebookContact {
			get {
				if (ContactDetails == null || ParseUser.CurrentUser == null)
					return null;

				foreach (var contact in ContactDetails) {
					if (contact.Type == BlrtData.Contacts.BlrtContactInfoType.FacebookId)
						return contact;
				}

				return null;
			}
		}

		public static string AccountStatus {
			get {
				if (ParseUser.CurrentUser == null)
					return "";
				return ParseUser.CurrentUser.Get<string> (AccountStatusKey, "");
			}
		}

		public static void InitilizeUser ()
		{
			if (ParseUser.CurrentUser == null)
				return;

			var u = ParseUser.CurrentUser;

			if (u.Username == null || u.Username == null
				|| u.Email == null || u.Email == null) {
				BlrtManager.Logout (true);
				return;
			}

			if (!u.Keys.Contains (UserGenderKey))
				u.Add (UserGenderKey, (int)BlrtUserGender.Unknown);

			if (!u.Keys.Contains (ALLOW_EMAIL_NEW_BLRT))
				u.Add (ALLOW_EMAIL_NEW_BLRT, true);
			if (!u.Keys.Contains (ALLOW_EMAIL_REQUEST))
				u.Add (ALLOW_EMAIL_REQUEST, true);
			if (!u.Keys.Contains (ALLOW_EMAIL_REBLRT))
				u.Add (ALLOW_EMAIL_REBLRT, true);
			if (!u.Keys.Contains (ALLOW_EMAIL_COMMENTS))
				u.Add (ALLOW_EMAIL_COMMENTS, true);

			if (!u.Keys.Contains (ALLOW_PUBLIC_EMAIL))
				u.Add (ALLOW_PUBLIC_EMAIL, true);


			if (!u.Keys.Contains (ORGANISATION_NAME))
				u.Add (ORGANISATION_NAME, "");


			if (!u.Keys.Contains (DisplayNameKey))
				u.Add (DisplayNameKey, "");

			if (!u.Keys.Contains (FirstNameKey))
				u.Add (FirstNameKey, "");
			if (!u.Keys.Contains (LastNameKey))
				u.Add (LastNameKey, "");

			if (!u.Keys.Contains (DEVICE_LOCAL))
				u.Add (DEVICE_LOCAL, "");


			return;
		}

		public static void AutoSetDisplayName (string displayname)
		{
			var names = displayname.Trim ().Split (' ');
			ParseUser.CurrentUser [BlrtData.BlrtUserHelper.DisplayNameKey] = displayname;
			ParseUser.CurrentUser [BlrtData.BlrtUserHelper.HasnotSetDisplaynameKey] = false;
			if (names.Length > 1) {
				ParseUser.CurrentUser [BlrtData.BlrtUserHelper.LastNameKey] = names [names.Length - 1];
			}
			if (names.Length > 0) {
				ParseUser.CurrentUser [BlrtData.BlrtUserHelper.FirstNameKey] = names [0];
			}
		}

		public static string Gender {
			get {
				var genderInt = ParseUser.CurrentUser.Get<int> (BlrtUserHelper.UserGenderKey, 0);
				switch (genderInt) {
					case 0:
						return "Undisclosed";
					case 1:
						return "Male";
					case 2:
						return "Female";
					default:
						return "Undisclosed";
				}
			}
		}

		public static string Organization {
			get {
				return ParseUser.CurrentUser.Get<string> (BlrtUserHelper.ORGANISATION_NAME, "");
			}
		}

		public static async Task<ParseUser> GetUserForSave ()
		{

			var u = ParseUser.CurrentUser;

			if (u != null) {

#if __IOS__
				u [DEVICE_LOCAL] = Foundation.NSLocale.CurrentLocale.CountryCode;
				u [DEVICE_LANG] = Foundation.NSLocale.PreferredLanguages;
#elif __ANDROID__
				u [DEVICE_LOCAL]	= BlrtApp.PlatformHelper.GetCountryCode();
				u [DEVICE_LANG]		= BlrtApp.PlatformHelper.GetLanguages();
#endif


				var ip = await BlrtUtil.GetDeviceIPAddress ();
				if (!string.IsNullOrEmpty (ip))
					u ["last_ip"] = ip;

				u [BlrtUserHelper.LastActiveKey] = BlrtUtil.ToCloudTime (DateTime.Now);
			}

			return u;
		}


		public static bool HasnotSetDisplayname {
			get {
				if (ParseUser.CurrentUser == null)
					return false;

				return ParseUser.CurrentUser.Get<bool> (HasnotSetDisplaynameKey, false);
			}
		}

		public static bool HasSetPassword {
			get {
				if (ParseUser.CurrentUser == null)
					return false;

				return ParseUser.CurrentUser.Get<bool> (HasSetPasswordKey, false);
			}
		}


		public static Task<bool> CheckPassword (string password)
		{
			return CheckPassword (password, CancellationToken.None);
		}

		public static async Task<bool> CheckPassword (string password, CancellationToken token)
		{
			var result = await BlrtUtil.BetterParseCallFunctionAsync<Dictionary<string, object>> (
				"passwordCheck147",
				new Dictionary<string, object> () {
					{ "password", password },
				},
				token
			);

			if (result != null && result.ContainsKey ("result")) {
				return (result ["result"] as bool?) ?? false;
			}

			return false;
		}

		public static bool HasCreatedConversation {
			get {
				if (ParseUser.CurrentUser == null)
					return false;

				return ParseUser.CurrentUser.Get<int> (ConversationCreationCounter, 0) > 0;
			}
		}

		public static TimeSpan? TimeToAccountTypeExpiration ()
		{
			if (ParseUser.CurrentUser != null) {
				DateTime? date = ParseUser.CurrentUser.Get<DateTime?> (AccountTypeExpiration, null);


				if (date != null) {
					date = BlrtUtil.ToLocalTime (date.Value);

					TimeSpan time = date.Value - DateTime.Now;
					if (time < new TimeSpan ())
						return new TimeSpan ();


					return time;
				}
			}

			return null;
		}

		public static bool EmailVerified {
			get {
				if (ParseUser.CurrentUser != null)
					return ParseUser.CurrentUser.Get<bool> (EmailVerified2Key, false);

				return false;
			}
		}

		public static async Task<bool> ResendConfirmation (BlrtUserContactDetail contact)
		{
			if (contact.Verified)
				return true;

			return await ResendConfirmation (contact.ObjectId);
		}

		public static async Task<bool> ResendConfirmation (string contactId)
		{
			LLogger.WriteEvent ("resendConfirmation", "started");

			var request = new BlrtAPI.APIUserContactDetailResendVerification (new BlrtAPI.APIUserContactDetailResendVerification.Parameters () {
				Id = contactId
			});

			if (await request.Run (new CancellationTokenSource (15000).Token)) {
				if (!request.Response.Success) {
					switch (request.Response.Error.Code) {
					case BlrtAPI.APIUserContactDetailResendVerificationError.AlreadyVerified:
						return true;
					default:
						LLogger.WriteEvent ("resendConfirmation", "failed", request.Response.Error.Message);
						return false;
					}
				}
			} else {
				LLogger.WriteEvent ("resendConfirmation", "failed", request.Exception.Message);
				return false;
			}

			return true;
		}

		public static BlrtUserGender GenderFromString (string str)
		{
			if (null != str) {
				str = str.ToLower ().Trim ();

				switch (str) {
				case "male":
				case "man":
				case "boy":
					return BlrtUserGender.Male;
				case "female":
				case "woman":
				case "girl":
					return BlrtUserGender.Female;
				}
			}

			return BlrtUserGender.Unknown;
		}

		public enum BlrtUserGender : int
		{
			Unknown,
			Male,
			Female,
		}

		public static string UnsignedUserId {
			get {
#if __iOS__
				return "UnsignedUser: " + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString ()+ "-" + TIMESTAMP;
#else
				return "UnsignedUser: "+Android.Provider.Settings.Secure.GetString(
					BlrtApp.PlatformHelper.GetActivity().ContentResolver, Android.Provider.Settings.Secure.AndroidId) + "-" + TIMESTAMP;
#endif
			}
		}

		public static string TrackingId {
			get {
				return Segment.Analytics.Client.AliasCreatedThisSession ?
					          UnsignedUserId : ParseUser.CurrentUser?.ObjectId ?? UnsignedUserId;
			}
		}

		public static async void IdentifyUser (bool logout = false)
		{
			if (ParseUser.CurrentUser != null) {
				var currentUser = await BlrtUserHelper.GetUserForSave ();
				Xamarin.Insights.Identify (currentUser.ObjectId, new Dictionary<string, string>{
					{Xamarin.Insights.Traits.Email, currentUser.Email},
					{"verified", BlrtUserHelper.EmailVerified.ToString()},
					{"device_local", currentUser.Get<string>(DEVICE_LOCAL,"")},
					{"device_lang", currentUser.Get<string[]>(DEVICE_LANG,new string[]{}).ToString()}
				});
			} else {
				Xamarin.Insights.Identify (Xamarin.Insights.Traits.GuestIdentifier, null);
			}


			//segment
			if (ParseUser.CurrentUser != null) {
				var currentUser = await BlrtUserHelper.GetUserForSave ();
				var traits = new Segment.Model.Traits ();
				var avatar = currentUser.Get<ParseFile> (AVATAR_KEY, null);
				if (avatar != null)
					traits.Add ("avatar", avatar.Url.ToString ());
				else
					traits.Add ("avatar", "unset");
				traits.Add ("userId", TrackingId);
				traits.Add ("createdAt", currentUser.CreatedAt);
				traits.Add ("email", currentUser.Email);
				traits.Add ("name", CurrentUserName);
				traits.Add ("firstName", currentUser.Get<string> (FirstNameKey, "unset"));
				if (currentUser.Get<string> (LastNameKey, "") == "") {
					traits.Add ("lastName", CurrentUserName);
				}
				else
					traits.Add ("lastName", currentUser.Get<string> (LastNameKey, ""));
				traits.Add ("gender", Gender);
				if (PhoneNumber != "")
					traits.Add ("phoneNumber", PhoneNumber);
				else
					traits.Add ("phoneNumber", "unset");
				
				var accountType = currentUser.Get<ParseObject> ("activeAccountTypeId", null);
				if (accountType != null) {
					traits.Add ("accountTypeId", accountType.ObjectId);
					if (!string.IsNullOrEmpty (BlrtPermissions.AccountName))
						traits.Add ("accountType", BlrtPermissions.AccountName);
				}

				traits.Add ("allowsBlrtReplyEmails", currentUser.Get<bool> (ALLOW_EMAIL_REBLRT, true));
				traits.Add ("allowsBlrtRequestEmails", currentUser.Get<bool> (ALLOW_EMAIL_REQUEST, true));
				traits.Add ("allowsCommentEmails", currentUser.Get<bool> (ALLOW_EMAIL_COMMENTS, true));
				traits.Add ("allowsConversationEmails", currentUser.Get<bool> (ALLOW_EMAIL_NEW_BLRT, true));
				traits.Add ("allowsPublicEmails", currentUser.Get<bool> (ALLOW_PUBLIC_EMAIL, true));

				traits.Add ("industry", currentUser.Get<string> (IndustryKey, ""));
				traits.Add ("lastAppVersion", BlrtUtil.PlatformHelper.GetAppVersion ());
				traits.Add ("lastDevice", BlrtManager.Responder.DeviceDetails.Device);

				var languageArray = BlrtManager.Responder.DeviceDetails.LanguageArray;
				if (languageArray != null && languageArray.Length > 0) {
					traits.Add ("lastLanguage", languageArray [0]);
				}
				traits.Add ("lastLocale", BlrtManager.Responder.DeviceDetails.Locale);
				traits.Add ("lastOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
				traits.Add ("lastOS", BlrtManager.Responder.DeviceDetails.OS);
				traits.Add ("performedFreeTrial", HasDoneFreeTrial);
				traits.Add ("receivedBlrtRequestCount", currentUser.Get<int> ("receivedBlrtRequestCount", 0));
				traits.Add ("sentBlrtRequestCount", currentUser.Get<int> ("sentBlrtRequestCount", 0));
				traits.Add ("timezone", "UTC " + TimeZone.CurrentTimeZone.GetUtcOffset (DateTime.Now));
				traits.Add ("unansweredReceivedBlrtRequestCount", currentUser.Get<int> ("unansweredReceivedBlrtRequestCount", 0));
				traits.Add ("unansweredSentBlrtRequestCount", currentUser.Get<int> ("unansweredSentBlrtRequestCount", 0));
				traits.Add ("usesFacebook", ParseFacebookUtils.IsLinked (currentUser));
				traits.Add ("hasZoomed", currentUser.Get<bool> (HAS_ZOOOMED_KEY, false));
				traits.Add ("hasDrawn", currentUser.Get<bool> (HAS_DRAWN_KEY, false));

				var option = new Segment.Model.Options ();
				option.Context.Add ("app", new Dictionary<string, string> () {
					{"name", "Blrt"},
					{"version", BlrtManager.Responder.DeviceDetails.AppVersion}
				});

				Segment.Analytics.Client.Identify (TrackingId, traits, option);
			} else {
				//Generate a new unsigned user id every time the user logout. 
				//This is to make sure the same unsigned user id with not be used to alias with
				//two different user ids.
				if (logout) {
					GenerateUnsignedId ();
				}
				var traits = new Segment.Model.Traits ();
				traits.Add ("$name", TrackingId);
				traits.Add ("lastAppVersion", BlrtUtil.PlatformHelper.GetAppVersion ());
				traits.Add ("lastDevice", BlrtManager.Responder.DeviceDetails.Device);

				var languageArray = BlrtManager.Responder.DeviceDetails.LanguageArray;
				if (languageArray != null && languageArray.Length > 0) {
					traits.Add ("lastLanguage", languageArray [0]);
				}
				traits.Add ("lastLocale", BlrtManager.Responder.DeviceDetails.Locale);
				traits.Add ("lastOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
				traits.Add ("lastOS", BlrtManager.Responder.DeviceDetails.OS);

				var option = new Segment.Model.Options ();
				option.Context.Add ("app", new Dictionary<string, string> () {
					{"name", "Blrt"},
					{"version", BlrtManager.Responder.DeviceDetails.AppVersion}
				});

				Segment.Analytics.Client.Identify (TrackingId, traits, option);
			}
		}

		#region avatar
		public const int FileNameLength = 47;
		public static string CreateUniqueUserAvatarName ()
		{
			string cloudFileName = "";
			try {
				var file = ParseUser.CurrentUser.Get<ParseFile> (BlrtUserHelper.AVATAR_KEY, null);
				if (file != null) {
					cloudFileName = file.Name;
					if (cloudFileName.Length > FileNameLength) {
						cloudFileName = cloudFileName.Substring (cloudFileName.Length - FileNameLength);
					}
				}
			} catch { }

			string filename;
			while (true) {
				var guid = Guid.NewGuid ();
				if (guid == Guid.Empty)
					continue;
				filename = guid.ToString () + "-avatar.jpg";
				foreach (var character in System.IO.Path.GetInvalidFileNameChars ())
					filename = filename.Replace (character, '_');
				if (filename == cloudFileName)
					continue;
				var path = BlrtConstants.Directories.Default.GetAvatarPath (filename);
				if (File.Exists (path))
					continue;
				break;
			}
			return filename;
		}


		static string _uploadPath;
		/// <summary>
		/// Saves the avatar to parse for current user.
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="name">Name.</param>
		/// <param name="avatarFrom">Avatar from.</param>
		public static async void SaveAvatarToParse (string path, string name, string avatarFrom, string service = null)
		{
			//stream
			_uploadPath = path;
			using (var s = File.OpenRead (path)) {
				var parseFile = new ParseFile (name, s);
				await parseFile.SaveAsync ();
				ParseUser.CurrentUser [BlrtUserHelper.AVATAR_KEY] = parseFile;
				ParseUser.CurrentUser [BlrtUserHelper.AVATAR_FROM_KEY] = avatarFrom;
				await ParseUser.CurrentUser.BetterSaveAsync (BlrtUtil.CreateTokenWithTimeout (15000));
				string newpath = BlrtConstants.Directories.Default.GetAvatarPath (name);
				if (!File.Exists (newpath))
					File.Copy (path, newpath);
				_uploadPath = null;
				if (service == null) {
					BlrtData.Helpers.BlrtTrack.ProfileChangePicture (avatarFrom);
				} else {
					BlrtData.Helpers.BlrtTrack.ProfileChangePicture (service);
				}
				var traits = new Segment.Model.Traits ();
				var avatar = parseFile;
				traits.Add ("avatar", avatar.Url.ToString ());
				Segment.Analytics.Client.Identify (TrackingId, traits);
				s.Close ();
			}
		}

		public const string DefaultBasicAvatar = "avatar_basic_large.png";
		public const string DefaultPremiumAvatar = "avatar_basic_large.png";

		public static string DefaultAvatar {
			get {
				if (BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free
					|| BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Unknown) {
					return DefaultBasicAvatar;
				} else {
					return DefaultPremiumAvatar;
				}
			}
		}

		public static string GetLocalAvatar ()
		{
			if (ParseUser.CurrentUser == null)
				return "";
			if (_uploadPath != null)
				return _uploadPath;
			string cloudFileName = "";
			ParseFile file = null;
			try {
				file = ParseUser.CurrentUser.Get<ParseFile> (BlrtUserHelper.AVATAR_KEY, null);
				if (file != null) {
					cloudFileName = file.Name;
					if (cloudFileName.Length > FileNameLength) {
						cloudFileName = cloudFileName.Substring (cloudFileName.Length - FileNameLength);
					}
				}
			} catch { }

			if (!string.IsNullOrEmpty (cloudFileName)) {
				var path = BlrtConstants.Directories.Default.GetAvatarPath (cloudFileName);
				if (File.Exists (path)) {
					return path;
				}
			}

			return DefaultAvatar;
		}


		/// <summary>
		/// Gets the latest avatar.
		/// </summary>
		/// <returns>The latest avatar path.</returns>
		public static async Task<string> GetLatestAvatar ()
		{
			if (_uploadPath != null) {
				return _uploadPath;
			}

			string cloudFileName = "";
			ParseFile file = null;
			try {
				file = ParseUser.CurrentUser.Get<ParseFile> (BlrtUserHelper.AVATAR_KEY, null);
				if (file != null) {
					cloudFileName = file.Name;
					if (cloudFileName.Length > FileNameLength) {
						cloudFileName = cloudFileName.Substring (cloudFileName.Length - FileNameLength);
					}
				}
			} catch { }

			if (string.IsNullOrEmpty (cloudFileName)) {
				//get from other service
				string filename, path;

				//1st create file path
				filename = BlrtUserHelper.CreateUniqueUserAvatarName ();
				path = BlrtConstants.Directories.Default.GetAvatarPath (filename);


				//2nd check from fb
#if __iOS__
				if (BlrtiOS.FBManager.IsLinked) {
					var url = await BlrtiOS.FBManager.FetchUserAvatar ();
#elif __ANDROID__
				if(BlrtApp.Droid.FBManager.IsLinked){
					var url = await BlrtApp.Droid.FBManager.FetchUserAvatar ();
#endif
					if (!string.IsNullOrEmpty (url) && url != "blrt_error") {
						var downloader = BlrtData.Contacts.BlrtContact.AvatarDownloader.Download (10, file.Url, path);
						await downloader.WaitAsync ();
						if (File.Exists (path)) {
							BlrtUserHelper.SaveAvatarToParse (path, filename, "facebook");
							return path;
						}
					}
				}

				//3rd check from gravatar
				var tempFileName = await RefreshFromGravatar ();
				if (!string.IsNullOrEmpty (tempFileName) && tempFileName != "blrt_error") {
					File.Copy (BlrtConstants.Directories.Default.GetRecentPath (tempFileName), path);
					//if any match, save to cloud
					BlrtUserHelper.SaveAvatarToParse (path, filename, "gravatar");
					return path;
				} else {
					return "";
				}
			} else {
				var path = BlrtConstants.Directories.Default.GetAvatarPath (cloudFileName);
				if (File.Exists (path)) {
					return path;
				} else {
					//download from parse
					var downloader = BlrtData.Contacts.BlrtContact.AvatarDownloader.Download (10, file.Url, path);
					await downloader.WaitAsync ();
					if (File.Exists (path)) {
						return path;
					}
				}
			}

			return "";
		}

		/// <summary>
		/// Refreshs from gravatar.
		/// </summary>
		/// <returns>The file name downloaded from gravatar.</returns>
		public static async Task<string> RefreshFromGravatar ()
		{
			var emails = BlrtUserHelper.EmailAddresses.Select (v => v.Value).Concat (new string [] { ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, "") }).Distinct ();

			if (emails == null || emails.Count () == 0)
				return "blrt_error";
			else {
				var ts = new List<Task<string>> ();
				foreach (var e in emails) {
					var t = BlrtData.Contacts.BlrtContactManager.LoadAvatarFromGravatar (e);
					ts.Add (t);
				}

				await Task.WhenAll (ts.ToArray ());

				try {
					var fileName = (ts.FirstOrDefault (re => !string.IsNullOrEmpty (re.Result)))?.Result;
					if (!string.IsNullOrEmpty (fileName)) {
						return fileName;
					}
				} catch (Exception ex) {
					return "Err: " + ex;
				}
			}
			return "";
		}
		#endregion


		public static int UnreadUserMessageCount {
			get {
				if (ParseUser.CurrentUser == null)
					return 0;
				var unreadMessagesCount = 0;
				if (BlrtUserHelper.HasnotSetDisplayname)
					unreadMessagesCount++;
				if (!BlrtUserHelper.EmailVerified)
					unreadMessagesCount++;
				if (!BlrtUserHelper.HasSetPassword)
					unreadMessagesCount++;
				if (string.IsNullOrEmpty (BlrtUserHelper.PhoneNumber))
					unreadMessagesCount++;

				return unreadMessagesCount;
			}
		}

		public static string [] BlockedUsers {
			get {
				var dic = ParseUser.CurrentUser.Get<IDictionary<string, int>> (BlrtUserHelper.BlockedUserFlagsKey, new Dictionary<string, int> ());
				return dic.Where (t => t.Value == 1).Select (u => u.Key).ToArray ();
			}
		}


		public async static Task<string> GetSessionToken ()
		{
			if (ParseUser.CurrentUser == null)
				return "";
			try {
				var res = await BlrtUtil.BetterParseCallFunctionAsync<string> (
					"getSessionTokenForCurrentUser",
					new Dictionary<string, object> (),
					new CancellationTokenSource (15000).Token
				);
				return res;
			} catch {
				return "";
			}

		}
	}
}

