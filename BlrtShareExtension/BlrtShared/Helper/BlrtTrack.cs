﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using Parse;

#if __iOS__
using Foundation;
using Facebook.CoreKit;
#endif

namespace BlrtData.Helpers
{
	public static class BlrtTrack
	{
		static DateTime _extraDataUpdateAt;
		static Dictionary<string,string> _extraMetaData;
		public static Dictionary<string,string> ExtraMetaData{
			get{
				if (_extraMetaData != null && _extraDataUpdateAt < DateTime.Now.AddMinutes (-30)) {
					_extraMetaData = null;
				}
				return _extraMetaData;
			}set{
				if (_extraMetaData != value) {
					_extraMetaData = value;
					TrackUtms ();
				}
				_extraDataUpdateAt = DateTime.Now;
			}
		}

		static void TrackUtms(){
			if(_extraMetaData!=null){
				//track utm_*
				var traits = new Segment.Model.Traits ();
				bool utm = false;
				string utm_source = "";
				string utm_medium = "";
				string utm_campaign = "";
				string utm_content = "";
				foreach(var kv in _extraMetaData){
					if (kv.Key == "utm_source") {
						utm_source = kv.Value;
						utm = true;
					} else if (kv.Key == "utm_medium") {
						utm_medium = kv.Value;
						utm = true;
					} else if (kv.Key == "utm_campaign") {
						utm_campaign = kv.Value;
						utm = true;
					} else if (kv.Key == "utm_content") {
						utm_content = kv.Value;
						utm = true;
					} else {
						continue;
					}
				}
				if (utm) {
					traits.Add ("utm_source", utm_source);
					traits.Add ("utm_medium", utm_medium);
					traits.Add ("utm_campaign", utm_campaign);
					traits.Add ("utm_content", utm_content);

					Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
				}
			}
		}

		static void MySegmentTrack(string trackingId, string trackingName, Segment.Model.Properties properties){
			if(properties==null){
				properties = new Segment.Model.Properties ();
			}

			try{
				//extra meta data from last 30 mins url click
				if (ExtraMetaData != null) {
					foreach (var kv in ExtraMetaData) {
						if(!properties.ContainsKey(kv.Key) && kv.Key.StartsWith("utm"))
							properties.Add(kv.Key,kv.Value);
					}
				}
			} catch{
				
			}

			try {
				//all event tracking should have these params
				if (!properties.ContainsKey ("platform"))
					properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName ());
				if (!properties.ContainsKey ("fromDevice"))
					properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
				if (!properties.ContainsKey ("fromOS"))
					properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
				if (!properties.ContainsKey ("fromOSVersion"))
					properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
				if (!properties.ContainsKey ("fromBlrtVersion"))
					properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			} catch {

			}
			var option = new Segment.Model.Options ();
			option.Context.Add ("app", new Dictionary<string, string> () {
				{"name", "Blrt"},
				{"version", BlrtManager.Responder.DeviceDetails.AppVersion}
			});
			option.Context.Add ("screen", new Dictionary<string, float> () {
				{"width", BlrtUtil.PlatformHelper.GetDeviceWidth()},
				{"height", BlrtUtil.PlatformHelper.GetDeviceHeight()},
			});
			string locale = "en";
			var languageArray = BlrtManager.Responder.DeviceDetails.LanguageArray;
			if (languageArray != null && languageArray.Length > 0) {
				locale = languageArray [0];
			}
			locale += '-' + BlrtManager.Responder.DeviceDetails.Locale;


			//construct the userAgent string for GA, for example:
			//userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X; en-us) BlrtApp/2.1 (KHTML, like Gecko)";
			//userAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) BlrtApp/2.1 (KHTML, like Gecko) Mobile";
			string userAgent = "Mozilla/5.0 ";
			if (BlrtUtil.PlatformHelper.GetPlatformName () == "iOS") {
				userAgent += "(" + BlrtManager.Responder.DeviceDetails.Device + ";CPU ";
				if (BlrtManager.Responder.DeviceDetails.Device == "iPhone") {
					userAgent += "iPhone ";
				}
				userAgent += "OS " + BlrtManager.Responder.DeviceDetails.OSVersion.Replace ('.', '_');
				userAgent += " like Mac OS X; ";
				userAgent += locale;
				userAgent += ") ";
				userAgent += "BlrtApp/2.1 (KHTML, like Gecko)";
				option.Context.Add ("locale", locale);
			} else {
				userAgent += "(Linux; ";
				if (Xamarin.Forms.Device.Idiom == Xamarin.Forms.TargetIdiom.Phone) {
					userAgent += "U;";
				}
				userAgent += " Android " + BlrtManager.Responder.DeviceDetails.OSVersion + "; " + BlrtManager.Responder.DeviceDetails.Device + "; " + languageArray [0].Replace ("_", "-") + ") ";
				userAgent += "BlrtApp/2.1 (KHTML, like Gecko)";
				if (Xamarin.Forms.Device.Idiom == Xamarin.Forms.TargetIdiom.Phone) {
					userAgent += " Mobile";
				}
				option.Context.Add ("locale", languageArray [0].Replace ("_", "-"));
			}
			option.Context.Add ("userAgent", userAgent);

			Segment.Analytics.Client.Track (trackingId, trackingName, properties, option);
		}

		public static void TrackURLClick(string url){
			var properties = new Segment.Model.Properties ();
			properties.Add ("url", url);

			var dic = BlrtUtil.GetParameters (url);
			foreach (var kv in dic) {
				if(kv.Key!="url")
					properties.Add(kv.Key,kv.Value);
			}

			Segment.Analytics.Client.Track(BlrtUserHelper.TrackingId, "TrackURLClick", properties);

			BlrtData.Helpers.BlrtTrack.ExtraMetaData = dic;
		}

		public static async void TrackInstall(){
			if(BlrtPersistant.Properties.ContainsKey("InstallationTracked")){
				return;
			}

			//adword tracking
			#if __IOS__
			var url = BlrtConstants.AdWordConversationBaseUrl + BlrtConstants.AdWordConversationID+"/?"+"label="+BlrtConstants.AdWordInstallLabel;
			url += "&rdid=" + AdSupport.ASIdentifierManager.SharedManager.AdvertisingIdentifier.AsString ();
			url += "&bundleid=" + NSBundle.MainBundle.BundleIdentifier;
			url += "&idtype=idfa";
			url += "&lat=" + (AdSupport.ASIdentifierManager.SharedManager.IsAdvertisingTrackingEnabled ? "1" : "0");
			var success = await SendGetRequest (url);
			if(success){
				BlrtPersistant.Properties ["InstallationTracked"] = true;
				BlrtPersistant.SaveProperties ();
			}
			#endif
		}

		static async Task<bool> SendGetRequest(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (url);
			try {
				using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse) {
					if (response.StatusCode != HttpStatusCode.OK) {
						throw new Exception("get request falure "+ url);
					}
				}
			} catch (Exception e) {
				BlrtUtil.Debug.ReportException (e);
				return false;
			}
			return true;
		}


		public static void TrackViewedContent(string contentType, string id){
			#if __iOS__
			var dic = new NSDictionary (AppEvents.ContentTypeParameterNameKey, new NSString (contentType));
			AppEvents.LogEvent (AppEvents.ViewedContentEventNameKey, dic);
			#elif __ANDROID__
			using (var logger = Xamarin.Facebook.AppEvents.AppEventsLogger.NewLogger (BlrtApp.PlatformHelper.GetActivity ())) {
			using (var parameter = new Android.OS.Bundle()) {
			parameter.PutString(Xamarin.Facebook.AppEvents.AppEventsConstants.EventParamContentType, contentType);
			logger.LogEvent (Xamarin.Facebook.AppEvents.AppEventsConstants.EventNameViewedContent, parameter);
			}
			}
			#endif
		}

		public static void TrackCreatedBlrt(){
			#if __iOS__
			AppEvents.LogEvent(new NSString("createdBlrt"));
			#elif __ANDROID__
			using (var logger = Xamarin.Facebook.AppEvents.AppEventsLogger.NewLogger (BlrtApp.PlatformHelper.GetActivity ())) {
			logger.LogEvent ("createdBlrt");
			}
			#endif
		}

		public static void AccountCreate(string registrationMethod){
			
			#if __iOS__
			Yozio.Yozio.TrackSignup ();
			var dic = new NSDictionary (AppEvents.RegistrationMethodParameterNameKey, new NSString (registrationMethod));
			AppEvents.LogEvent (AppEvents.CompletedRegistrationEventNameKey, dic);


#elif __ANDROID__
			using (var logger = Xamarin.Facebook.AppEvents.AppEventsLogger.NewLogger (BlrtApp.PlatformHelper.GetActivity ())) {
				using (var parameter = new Android.OS.Bundle()) {
					parameter.PutString(Xamarin.Facebook.AppEvents.AppEventsConstants.EventParamRegistrationMethod, registrationMethod);
					logger.LogEvent (Xamarin.Facebook.AppEvents.AppEventsConstants.EventNameCompletedRegistration, parameter);
				}
			}
#endif
			var trackingId = BlrtUserHelper.TrackingId;
			var currentUserId = ParseUser.CurrentUser?.ObjectId ?? "null";

			if (trackingId != currentUserId) {
				LLogger.WriteLine ("AccountCreate WARN, about to create alias, BlrtUserHelper.TrackingId {0} != ParseUser.CurrentUser.ObjectId {1}", trackingId, currentUserId);
			}

			Segment.Analytics.Client.Alias (BlrtUserHelper.UnsignedUserId, trackingId);

			//now identify the user
			try {
				var traits = new Segment.Model.Traits ();
				string utm_extra = null;
				string utm_source = null;
				string utm_medium = null;
				string utm_campaign = null;
				string utm_content = null;
				if (ExtraMetaData != null) {
					foreach (var kv in _extraMetaData) {
						if (kv.Key == "utm_source") {
							utm_source = kv.Value;
						} else if (kv.Key == "utm_medium") {
							utm_medium = kv.Value;
						} else if (kv.Key == "utm_campaign") {
							utm_campaign = kv.Value;
						} else if (kv.Key == "utm_content") {
							utm_content = kv.Value;
						} else {
							if (utm_extra == null) {
								utm_extra = "";
							}
							utm_extra += kv;
						}
					}
				}
				traits.Add ("signup_extraparameters", utm_extra);
				traits.Add ("signup_utm_source", utm_source);
				traits.Add ("signup_utm_medium", utm_medium);
				traits.Add ("signup_utm_campaign", utm_campaign);
				traits.Add ("signup_utm_content", utm_content);
				traits.Add ("signupPlatform", BlrtUtil.PlatformHelper.GetPlatformName ());
				traits.Add ("signupDevice", BlrtManager.Responder.DeviceDetails.Device);
				traits.Add ("signupOS", BlrtManager.Responder.DeviceDetails.OS);
				traits.Add ("signupOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
				traits.Add ("signupAppVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
				traits.Add ("secondaryEmails", "unset");

				Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);


			} catch { }


			var properties = new Segment.Model.Properties ();
			properties.Add ("service", registrationMethod);
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName ());
			properties.Add ("category", "Account");
			properties.Add ("label", registrationMethod);

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountCreate", properties);


			//unsigneUser tracking
			//var usigninp = new Segment.Model.Properties ();
			//usigninp.Add ("userId", BlrtUserHelper.TrackingId);
			//MySegmentTrack (BlrtUserHelper.UnsignedUserId, "UnsignedNewSignup", usigninp);

			//adword tracking
			#if __IOS__
			var url = BlrtConstants.AdWordConversationBaseUrl + BlrtConstants.AdWordConversationID+"/?"+"label="+BlrtConstants.AdWordSignupLabelIOS;
			url += "&rdid=" + AdSupport.ASIdentifierManager.SharedManager.AdvertisingIdentifier.AsString ();
			url += "&bundleid=" + NSBundle.MainBundle.BundleIdentifier;
			url += "&idtype=idfa";
			url += "&lat=" + (AdSupport.ASIdentifierManager.SharedManager.IsAdvertisingTrackingEnabled ? "1" : "0");
			SendGetRequest (url);
			#elif __ANDROID__
			if((int)(Android.OS.Build.VERSION.SdkInt) >= 16){
				//advertising component only support version 4.1 and above
				Task.Factory.StartNew(()=>{
					try{
						var pInfo = Xamarin.Forms.Forms.Context.PackageManager.GetPackageInfo (Xamarin.Forms.Forms.Context.PackageName, global::Android.Content.PM.PackageInfoFlags.MetaData);
						var adInfo = Android.Gms.Ads.Identifier.AdvertisingIdClient.GetAdvertisingIdInfo(Xamarin.Forms.Forms.Context);

						var url = BlrtConstants.AdWordConversationBaseUrl + BlrtConstants.AdWordConversationID+"/?"+"label="+BlrtConstants.AdWordSignupLabelDroid;
						url += "&rdid=" + adInfo.Id;
						url += "&bundleid=" + pInfo.PackageName;
						url += "&idtype=advertisingid";
						url += "&lat=" + (adInfo.IsLimitAdTrackingEnabled ? "1" : "0");
						url += "&appversion=" + pInfo.VersionName;
						url += "&osversion=" + Android.OS.Build.VERSION.Release;
						url += "&sdkversion=" + "blrt-sdk-a-v1.0.0";

						SendGetRequest (url);
					}catch(Exception adworldExp){
						Xamarin.Insights.Report (adworldExp, Xamarin.Insights.Severity.Warning);
					}
				});

			}
			#endif
		}

		public static void AccountSignIn (string method)
		{
			
			var traits = new Segment.Model.Traits ();
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
			var properties = new Segment.Model.Properties ();
			properties.Add ("service", method);
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName ());
			properties.Add ("category", "Account");
			properties.Add ("label", method);

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountSignIn", properties);

			//unsignedUser tracking
			//var traits = new Segment.Model.Traits ();
			//traits.Add ("lastLoginUserId", BlrtUserHelper.TrackingId);
			//Segment.Analytics.Client.Identify (BlrtUserHelper.UnsignedUserId, traits);

			//var uloginp = new Segment.Model.Properties ();
			//uloginp.Add ("userId", BlrtUserHelper.TrackingId);
			//MySegmentTrack (BlrtUserHelper.UnsignedUserId, "UnsignedLogin", uloginp);
		}

		public static void AccountSignOut ()
		{
			if (ParseUser.CurrentUser == null)
				return;

			var properties = new Segment.Model.Properties ();
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName ());
			properties.Add ("category", "Account");

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountSignOut", properties);
		}

		public static void AccountDelete ()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Account");

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountDelete", properties);
		}

		public static void AccountStartTrial ()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Account");

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountStartTrial", properties);
		}

		public static void AccountHitLimit(string limitType)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("accontType", BlrtPermissions.AccountName);
			properties.Add ("category", "Account");
			properties.Add ("limitType", limitType);
			properties.Add ("label", limitType);

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountHitLimit", properties);
		}



		public static void AccountUpgrade (Decimal price, string currency)
		{

			var properties = new Segment.Model.Properties ();
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName());
			properties.Add ("revenue", price);
			properties.Add ("currency", currency);
			properties.Add ("upgradedTo", "Premium");
			properties.Add ("subscriptionPeriod", "365");
			properties.Add ("category", "Account");
			properties.Add ("label", "Premium");
			properties.Add ("value", price);

			MySegmentTrack (BlrtUserHelper.TrackingId, "AccountUpgrade", properties);
			//update user traits
			var traits = new Segment.Model.Traits ();
			var accountType = ParseUser.CurrentUser.Get<ParseObject> ("activeAccountTypeId", null);
			traits.Add ("accountTypeId", accountType.ObjectId);
			traits.Add ("accountType", BlrtPermissions.AccountName);
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);

			#if __iOS__
			// do nothing
			#elif __ANDROID__
			using (var logger = Xamarin.Facebook.AppEvents.AppEventsLogger.NewLogger (BlrtApp.PlatformHelper.GetActivity ())) {
				logger.LogPurchase (new Java.Math.BigDecimal (Convert.ToDouble (price)), Java.Util.Currency.GetInstance (currency));
			}
			#endif
		}

		public static void CanvasStartRecording()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Canvas");

			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasStartRecording", properties);
		}

		public static void CanvasStopRecording()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Canvas");

			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasStopRecording", properties);
			var traits = new Segment.Model.Traits ();
			traits.Add ("hasZoomed", ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HAS_ZOOOMED_KEY, false));
			traits.Add ("hasDrawn", ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.HAS_DRAWN_KEY, false));
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void CanvasResumeRecording()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Canvas");

			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasResumeRecording", properties);
		}

		public static void CanvasSelectTool(string tool, string colour)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("tool", tool);
			properties.Add ("colour", colour);
			properties.Add ("category", "Canvas");
			properties.Add ("label", tool + ","+ colour);

			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasSelectTool", properties);
		}

		public static void CanvasChangePage(string direction)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("direction", direction);
			properties.Add ("category", "Canvas");
			properties.Add ("label", direction);

			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasChangePage", properties);
		}

		public static void CanvasClear()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Canvas");
			MySegmentTrack (BlrtUserHelper.TrackingId, "CanvasClear", properties);
		}

		public static void AddPeopleShare(string service)
		{
			//change Other to Share
			if (service == "Other") {
				service = "Share";
			}
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "AddPeople");
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "AddPeopleShare", properties);
		}

		public static void AddPeopleSend(Dictionary<BlrtData.Contacts.BlrtContactInfo, string> recipients )
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "AddPeople");
			properties.Add ("numberRecipients", recipients.Count);
			properties.Add ("sendResults", recipients.Values.ToArray());
			properties.Add ("service", recipients.Keys.Select(x=>x.ContactType.ToString()).ToArray());
			properties.Add ("label", recipients.Keys.Select(x=>x.ContactType.ToString()).ToArray());

			MySegmentTrack (BlrtUserHelper.TrackingId, "AddPeopleSend", properties);


			#if __iOS__
			AppEvents.LogEvent(new NSString("sentBlrt"));
			#elif __ANDROID__
			using (var logger = Xamarin.Facebook.AppEvents.AppEventsLogger.NewLogger (BlrtApp.PlatformHelper.GetActivity ())) {
			logger.LogEvent ("sentBlrt");
			}
			#endif
		}

		public static void ProfileConnectService(string service)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileConnectService", properties);

			var traits = new Segment.Model.Traits ();
			traits.Add ("usesFacebook", true);
			traits.Add ("lastName", ParseUser.CurrentUser.Get<string> (BlrtUserHelper.LastNameKey, ""));
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void ProfileDisconnectService(string service)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileDisconnectService", properties);

			var traits = new Segment.Model.Traits ();
			traits.Add ("usesFacebook", false);
			traits.Add ("lastName", BlrtUserHelper.CurrentUserName);
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void ProfileChangePicture(string service)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileChangePicture", properties);

		}

		public static void ProfileEditDisplayname(string name)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("name", name);
			properties.Add ("label", name);
			properties.Add ("length", name.Length);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileEditDisplayname", properties);
			//update user traits
			var traits = new Segment.Model.Traits ();
			traits.Add ("name", name);
			//traits.Add ("lastName", name);
			//Add firstName here because Segement auto translate $name into $firstname and $lastname and send to Mixpanel
			//traits.Add ("firstName", ParseUser.CurrentUser.Get<string> (BlrtUserHelper.FirstNameKey, "unset"));
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void ProfileEditEmail(string email, int secondaries)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("email", email);
			properties.Add ("secondaries", secondaries);

			var rrr = email.Split ('@');
			if(rrr.Length>1)
				properties.Add ("domain", rrr [1]);

			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileEditEmail", properties);
			//update user traits
			var traits = new Segment.Model.Traits ();
			traits.Add ("email", email);
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void ProfileEditPassword()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileEditPassword", properties);
		}

		public static void ProfileEditOther(string field)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Profile");
			properties.Add ("field", field);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ProfileEditOther", properties);
			//update user traits
			var traits = new Segment.Model.Traits ();
			if (field == "Industry") {
				traits.Add ("industry", ParseUser.CurrentUser.Get<string> (BlrtUserHelper.IndustryKey, ""));
				Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
			} else if (field == "Gender") {
				traits.Add ("gender", BlrtUserHelper.Gender);
				Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
			}

		}

		public static void OtherProfileSendBlrt(bool blrtUser, string service)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "OtherProfile");
			properties.Add ("blrtUser", blrtUser);
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "OtherProfileSendBlrt", properties);
		}

		public static void OtherProfileAddContact()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "OtherProfile");
			MySegmentTrack (BlrtUserHelper.TrackingId, "OtherProfileAddContact", properties);
		}

		public static void OtherProfileHideShow()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "OtherProfile");
			MySegmentTrack (BlrtUserHelper.TrackingId, "OtherProfileHideShow", properties);
		}

		public static void Share(string service)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Share");
			properties.Add ("service", service);
			properties.Add ("label", service);
			MySegmentTrack (BlrtUserHelper.TrackingId, "Share", properties);
		}

		public static void SettingsNotificationsChange(string setting)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Settings");
			properties.Add ("setting", setting);
			properties.Add ("label", setting);
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsNotificationsChange", properties);
			var traits = new Segment.Model.Traits ();
			if (setting == "NewBlrt") {
				traits.Add ("allowsConversationEmails", ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_NEW_BLRT, true));
			} else if (setting == "ReBlrt") {
				traits.Add ("allowsBlrtReplyEmails", ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_REBLRT, true));
			} else {
				traits.Add ("allowsCommentEmails", ParseUser.CurrentUser.Get<bool> (BlrtUserHelper.ALLOW_EMAIL_COMMENTS, true));
			}
			Segment.Analytics.Client.Identify (BlrtUserHelper.TrackingId, traits);
		}

		public static void SettingsBlockUser(string userBlockedID, bool block)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Settings");
			properties.Add ("userBlocked", userBlockedID);
			properties.Add ("blocked", block);
			properties.Add ("label", userBlockedID);
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsBlockUser", properties);
		}

		public static void SettingsDeleteLocalData()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
			properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
			properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("label", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("category", "Settings");
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsDeleteLocalData", properties);
		}

		public static void SettingsCheckUpdate()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Settings");
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("label", BlrtManager.Responder.DeviceDetails.AppVersion);
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsCheckUpdate", properties);
		}

		public static void SettingsLeaveReview()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Settings");
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("label", BlrtManager.Responder.DeviceDetails.AppVersion);
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsLeaveReview", properties);
		}

		public static void SettingsShowOverlays()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("category", "Settings");
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("label", BlrtManager.Responder.DeviceDetails.AppVersion);
			MySegmentTrack (BlrtUserHelper.TrackingId, "SettingsShowOverlays", properties);
		}

		public static void ConvoCreate(int blrtLength, string convoId, string title, int numberOfPages, IEnumerable<string> pageTypes)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("blrtLength", blrtLength);
			properties.Add ("convId", convoId);
			properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
			properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
			properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("title", title);
			properties.Add ("numberPages", numberOfPages);
			properties.Add ("pageTypes", pageTypes);
			properties.Add ("category", "Conversation");
			properties.Add ("label", convoId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoCreate", properties);
		}

		public static void ConvoCreateBlrtReply(int blrtLength, string convoId, string blrtId)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("blrtLength", blrtLength);
			properties.Add ("convId", convoId);
			properties.Add ("blrtId", blrtId);
			properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
			properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
			properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("category", "Conversation");
			properties.Add ("label", convoId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoCreateBlrtReply", properties);
		}

		public static void ConvoCreateComment(int commentLength, string convoId)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("commentLength", commentLength);
			properties.Add ("convId", convoId);
			properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
			properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
			properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);
			properties.Add ("category", "Conversation");
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoCreateComment", properties);
		}

		public static void ConvoMakeBlrtPublic(string convoId, string blrtId, string title)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("blrtId", blrtId);
			properties.Add ("title", title);
			properties.Add ("category", "Conversation");
			properties.Add ("label", blrtId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoMakeBlrtPublic", properties);
		}

		public static void ConvoSharePublicBlrt(string convoId, string blrtId, string title)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("blrtId", blrtId);
			properties.Add ("title", title);
			properties.Add ("category", "Conversation");
			properties.Add ("label", blrtId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoSharePublicBlrt", properties);
		}

		public static void ConvoAddTag(string convoId, IEnumerable<string> tags)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("tags", tags);
			properties.Add ("category", "Conversation");
			properties.Add ("label", convoId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoAddTag", properties);
		}

		public static void ConvoFlag(string convoId, bool flagged)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("flagged", flagged);
			properties.Add ("category", "Conversation");
			properties.Add ("label", flagged);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoFlag", properties);
		}

		public static void ConvoArchive(string convoId, bool archived)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("archived", archived);
			properties.Add ("category", "Conversation");
			properties.Add ("label", archived);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoArchive", properties);
		}

		public static void ConvoDelete(string convoId)
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("convId", convoId);
			properties.Add ("category", "Conversation");
			properties.Add ("label", convoId);
			MySegmentTrack (BlrtUserHelper.TrackingId, "ConvoDelete", properties);
		}

		public static void AppActivate()
		{
			var properties = new Segment.Model.Properties ();
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName());
			properties.Add ("category", "App");
			properties.Add ("label", "Activate");
			MySegmentTrack (BlrtUserHelper.TrackingId, "AppActivate", properties);
			BlrtUserHelper.IdentifyUser ();
		}


		static string _lastScreen;
		static Dictionary<string, object> _lastDic;
		public static Dictionary<string, object> LastConvoDic{ get; private set;}

		public static void TrackScreenView(string screenName, bool force=false, Dictionary<string,object> extraParam = null )
		{
			bool shouldTrack = false;
			if(_lastScreen== screenName){
				if(_lastDic==extraParam){
					shouldTrack = false;
				}else if(_lastDic==null || extraParam==null || _lastDic.Count!= extraParam.Count){
					// they are different,we should track
					shouldTrack = true;
				}else{
					for(int i = 0; i<_lastDic.Count;i++){
						var keyA = _lastDic.Keys.ElementAt (i);
						var keyB = extraParam.Keys.ElementAt (i);
						var valueA = _lastDic.Values.ElementAt (i);
						var valueB = extraParam.Values.ElementAt (i);
						if((keyA != keyB) ||
							(!valueA.Equals(valueB))
						){
							shouldTrack = true;
							break;
						}
					}
				}
			}else{
				shouldTrack = true;
			}


			if (!shouldTrack && !force) {
				Console.WriteLine ("\nIn TrackScreenView shouldTrack = {0}, force = {1}\n", shouldTrack, force);
				return;
			}

			_lastScreen = screenName;
			_lastDic = extraParam;

			if(_lastScreen.StartsWith("Conversation") && _lastDic!=null && !_lastScreen.StartsWith("Conversation_EmbedPreview")){
				LastConvoDic = _lastDic;
			}

			var properties = new Segment.Model.Properties ();
			properties.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName());
			properties.Add ("fromDevice", BlrtManager.Responder.DeviceDetails.Device);
			properties.Add ("fromOS", BlrtManager.Responder.DeviceDetails.OS);
			properties.Add ("fromOSVersion", BlrtManager.Responder.DeviceDetails.OSVersion);
			properties.Add ("fromBlrtVersion", BlrtManager.Responder.DeviceDetails.AppVersion);

			if(extraParam!=null){
				foreach(var a in extraParam){
					properties.Add (a.Key, a.Value);
				}
			}

			//construct the option package, specifically for GA. GA required all this info to display correctly.
			//The user agent string is customed to make GA realise the operating system, and device of this session,
			//and not affect the display in Mixpanel.
			var option = new Segment.Model.Options();
			option.Context.Add ("app", new Dictionary<string, string> () {
				{"name", "Blrt"},
				{"version", BlrtManager.Responder.DeviceDetails.AppVersion}
			});
			option.Context.Add ("screen", new Dictionary<string, float> () {
				{"width", BlrtUtil.PlatformHelper.GetDeviceWidth()},
				{"height", BlrtUtil.PlatformHelper.GetDeviceHeight()},
			});
			string locale = "en";
			var languageArray = BlrtManager.Responder.DeviceDetails.LanguageArray;
			if (languageArray != null && languageArray.Length > 0) {
				locale =  languageArray[0];                       
			}
			locale += '-' + BlrtManager.Responder.DeviceDetails.Locale;


			//construct the userAgent string for GA, for example:
			//userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X; en-us) BlrtApp/2.1 (KHTML, like Gecko)";
			//userAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) BlrtApp/2.1 (KHTML, like Gecko) Mobile";
			string userAgent = "Mozilla/5.0 ";
			if (BlrtUtil.PlatformHelper.GetPlatformName () == "iOS") {
				userAgent += "(" + BlrtManager.Responder.DeviceDetails.Device + ";CPU ";
				if (BlrtManager.Responder.DeviceDetails.Device == "iPhone") {
					userAgent += "iPhone ";
				}
				userAgent += "OS " + BlrtManager.Responder.DeviceDetails.OSVersion.Replace ('.', '_');
				userAgent += " like Mac OS X; ";
				userAgent += locale;
				userAgent += ") ";
				userAgent += "BlrtApp/2.1 (KHTML, like Gecko)";
				option.Context.Add ("locale", locale);
			} else {
				userAgent += "(Linux; ";
				if (Xamarin.Forms.Device.Idiom == Xamarin.Forms.TargetIdiom.Phone) {
					userAgent += "U;";
				}
				userAgent += " Android " + BlrtManager.Responder.DeviceDetails.OSVersion + "; " + BlrtManager.Responder.DeviceDetails.Device + "; " + languageArray[0].Replace("_", "-") + ") ";
				userAgent += "BlrtApp/2.1 (KHTML, like Gecko)";
				if (Xamarin.Forms.Device.Idiom == Xamarin.Forms.TargetIdiom.Phone) {
					userAgent += " Mobile";
				}
				option.Context.Add ("locale", languageArray [0].Replace ("_", "-"));
			}
			option.Context.Add ("userAgent", userAgent);
			Segment.Analytics.Client.Screen (BlrtUserHelper.TrackingId, screenName, properties, option);	
		}
	}
}

