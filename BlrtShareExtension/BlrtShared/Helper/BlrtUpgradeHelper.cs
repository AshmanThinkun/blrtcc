using System;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;

namespace BlrtData
{
	public static class BlrtUpgradeHelper
	{
		static string KBIntToMBString (this int i)
		{
			return (i / 1024f).ToString ("##.#") + " MB";
		}

		/// <summary>
		/// Shows the upgrade dialog.
		/// </summary>
		/// <returns>true: the user tapped "upgrade"; false: the user tapped "OK / Cancel"</returns>
		/// <param name="permission">Permission.</param>
		/// <param name="value">Value.</param>
		public static async Task<bool> ShowUpgradeDialog(BlrtPermissions.PermissionEnum permission, long value = -1)
		{
			string valueStr 		= "";
			string currentLimit 	= "";
			string premiumLimit 	= "";

			bool upgrade 	= true;

			string message = "permission_limit";


			LLogger.WriteEvent("Blrt Permission", "Limit Reached", null, permission.ToString());

			switch (permission) {
				case BlrtPermissions.PermissionEnum.MaxBlrtDuration:
					message 		= "max_blrt_duration";
					valueStr	 	= (BlrtPermissions.MaxBlrtDuration / 60).ToString();
					currentLimit 	= (BlrtPermissions.MaxBlrtDuration / 60).ToString();
					premiumLimit	= (BlrtSettings.AdvertisedPremiumValues.MaxBlrtDuration / 60).ToString();
					break;

				case BlrtPermissions.PermissionEnum.MaxBlrtPageCount:
					message 		= "max_pages";
					valueStr 		= value.ToString ();
					currentLimit	= BlrtPermissions.MaxBlrtPageCount.ToString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxBlrtPageCount.ToString ();
					break;

				case BlrtPermissions.PermissionEnum.MaxConvoSizeKB:
					message 		= "max_convo_size";
					valueStr 		= value.ToString ();
					currentLimit 	= BlrtPermissions.MaxConvoSizeKb.KBIntToMBString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxConvoSizeKB.KBIntToMBString ();
					break;

				case BlrtPermissions.PermissionEnum.MaxBlrtSizeKB:
					message 		= "max_blrt_size";
					valueStr 		= value.ToString ();
					currentLimit 	= BlrtPermissions.MaxBlrtSizeKb.KBIntToMBString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxBlrtSizeKB.KBIntToMBString ();
					break;

				case BlrtPermissions.PermissionEnum.MaxPDFSizeKB:
					message 		= "max_pdf_size";
					valueStr 		= (value / 1024f).ToString ("F2");
					currentLimit 	= BlrtPermissions.MaxPDFSizeKB.KBIntToMBString ();
					premiumLimit 	= BlrtSettings.AdvertisedPremiumValues.MaxPdfSizeKb.KBIntToMBString ();
					break;

				case BlrtPermissions.PermissionEnum.CanMediaAddOnReblrt:
					message 		= "can_add_on_reblrt";
					break;

				case BlrtPermissions.PermissionEnum.CanMediaReorder:
					message 		= "can_reorder";
					break;

				case BlrtPermissions.PermissionEnum.CanMediaShowHide:
					message 		= "can_showhide";
					break;

				case BlrtPermissions.PermissionEnum.MaxImageResolution:
					message 		= "max_resolution";
					valueStr		= value.ToString();
					currentLimit 	= BlrtPermissions.MaxImageResolution.ToString();
					premiumLimit	= BlrtSettings.AdvertisedPremiumValues.MaxImageResolution.ToString();
					upgrade 		= false;
					break;

				case BlrtPermissions.PermissionEnum.CanChangeAllowAdd:
					message 		= "can_allowAdd";
					break;

				case BlrtPermissions.PermissionEnum.CanChangeAllowPublic:
					message 		= "can_allowPublic";
					break;

				case BlrtPermissions.PermissionEnum.MaxConversationUsers:
					message = "failed_too_many_recipents";
					valueStr = BlrtPermissions.MaxConversationUsers.ToString ();
					break;

				case BlrtPermissions.PermissionEnum.CanCreateConversation:
				case BlrtPermissions.PermissionEnum.CanCreateComment:
				case BlrtPermissions.PermissionEnum.CanCreateReblrt:
				case BlrtPermissions.PermissionEnum.CanAddUsers:
				case BlrtPermissions.PermissionEnum.CanUseWebViewer:
				case BlrtPermissions.PermissionEnum.CanUseDesktopViewer:

					return false;
			} 

			LLogger.WriteEvent("Blrt Permissions", "Limit reached alert", null, permission.ToString());

			BlrtData.Helpers.BlrtTrack.AccountHitLimit (permission.ToString ());

			bool trial 		= !BlrtUserHelper.HasDoneFreeTrial;
			bool free 		= BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free;

			if (free) {
				message += "_free";
			}

			SimpleAlert alertArgs = null;
			if (upgrade && free) {
				alertArgs = new SimpleAlert (
					string.Format (BlrtUtil.PlatformHelper.Translate (message + "_title", "upgrade"), valueStr, currentLimit, premiumLimit),
					string.Format (BlrtUtil.PlatformHelper.Translate (message + "_message", "upgrade"), valueStr, currentLimit, premiumLimit),
					BlrtUtil.PlatformHelper.Translate (message + "_cancel", "upgrade"),
					BlrtUtil.PlatformHelper.Translate (message + "_upgrade" + (trial ? "_trial" : ""), "upgrade")
				);
			} else {
				alertArgs = new SimpleAlert (
					string.Format (BlrtUtil.PlatformHelper.Translate (message + "_title", "upgrade"), valueStr, currentLimit, premiumLimit),
					string.Format (BlrtUtil.PlatformHelper.Translate (message + "_message", "upgrade"), valueStr, currentLimit, premiumLimit),
					BlrtUtil.PlatformHelper.Translate (message + "_cancel", "upgrade")
				);
			}
			var executor = BlrtData.ExecutionPipeline.Executor.System ();
			var alert = await BlrtManager.App.ShowAlert (executor, alertArgs);
			if (!alert.Success || alert.Result.Cancelled) {
				return false;
			} else {
				BlrtManager.App.OpenUpgradeAccount(executor);
				return true;
			}

			return false;
		}
	}
}

