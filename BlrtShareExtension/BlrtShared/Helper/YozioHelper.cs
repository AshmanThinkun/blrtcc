﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using Newtonsoft.Json;
using Parse;

namespace BlrtData.Helpers
{
	public static class YozioHelper
	{
		public static async Task<string> CreateSublink (string superLinkId, Dictionary<string, string> metaData)
		{
			var defaultDic = new Dictionary<string, object> ();
			if (ParseUser.CurrentUser != null)
				defaultDic.Add ("inviter", BlrtEncode.EncodeId (ParseUser.CurrentUser.ObjectId));
			defaultDic.Add ("platform", BlrtUtil.PlatformHelper.GetPlatformName ());

			foreach (var kv in metaData) {
				defaultDic [kv.Key] = kv.Value;
			}

			try {
				var str = await YozioCreateSublinkAsync (superLinkId, defaultDic, new CancellationTokenSource (5000).Token);
				return str;

			} catch (Exception e) {
				return "";
			}
		}


		static async Task<string> YozioCreateSublinkAsync (string superlink, Dictionary<string, object> data, CancellationToken token = default(CancellationToken))
		{
			var param = new Dictionary<string, object> ();
			param ["app_key"] = BlrtConstants.YozioKey;
			param ["method"] = "sub.link.create";
			param ["short_url"] = superlink;
			param ["meta_data"] = data;

			var paramsString = serialize (param);

			var root_url = BlrtSettings.YozioDictionary ["root_url"];
			var url = root_url + "?" + paramsString;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (url);
			request.UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0";
			token.ThrowIfCancellationRequested ();

			using (token.Register (() => request.Abort (), useSynchronizationContext: false)) {
				using (HttpWebResponse response = await request.GetResponseAsync () as HttpWebResponse) {
					token.ThrowIfCancellationRequested ();
					if (response.StatusCode != HttpStatusCode.OK) {
						return "";
					} else {
						var stream = new System.IO.StreamReader (response.GetResponseStream ());
						var responseText = stream.ReadToEnd ();
						var result = JsonConvert.DeserializeObject<Dictionary<string, object>> (responseText);
						var body = JsonConvert.DeserializeObject<Dictionary<string, object>> (result ["body"].ToString ());
						return BlrtSettings.YozioDictionary ["click_url"] + body ["sub_link"].ToString ();
					}
				}
			}
		}

		static string serialize (Dictionary<string, object> obj, string prefix = "")
		{
			var str = new List<string> ();
			foreach (var p in obj) {
				var k = prefix != "" ? prefix + "[" + p.Key + "]" : p.Key;
				var v = p.Value;
				var strToAdd = (v is Dictionary<string, object> ?
					serialize (v as Dictionary<string, object>, k) :
					k + "=" + v.ToString ());

				str.Add (strToAdd);
			}

			return string.Join ("&", str);
		}
	}
}

