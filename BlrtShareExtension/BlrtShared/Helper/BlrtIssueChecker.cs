﻿using System;
using System.Collections.Generic;
using BlrtData;

namespace BlrtData
{
	public static class BlrtIssueChecker
	{
		public static bool TryIssueCheck (IEnumerable<BlrtBase> blrt)
		{
			foreach (var b in blrt)
				if (!TryIssueCheck (b))
					return false;

			return true;
		}

		public static bool TryIssueCheck (BlrtBase blrt)
		{
			var issue = blrt.GetIssues ();

			if (issue != BlrtBaseIssues.None) {

				string prefix = "alert";

				if(blrt is Conversation)		prefix = "alert_conversation";
				if(blrt is ConversationItem)	prefix = "alert_conversationitem";
				if(blrt is MediaAsset)			prefix = "alert_mediaasset";

				string type = null;


				if ((issue & BlrtBaseIssues.UnsupportedDBVersion)	 		== BlrtBaseIssues.UnsupportedDBVersion) {
					type = "unsupported_db_verison";
				} else if ((issue & BlrtBaseIssues.UnsupportedEncryption) 	== BlrtBaseIssues.UnsupportedEncryption) {
					type = "unsupported_encryption";
				} else if ((issue & BlrtBaseIssues.UnknownMediaFormat) 		== BlrtBaseIssues.UnknownMediaFormat) {
					type = "unknown_media_format";
				} else if ((issue & BlrtBaseIssues.UnknownContentType) 		== BlrtBaseIssues.UnknownContentType) {
					type = "unknown_content_type";
				} else if ((issue & BlrtBaseIssues.UnknownStatusCode) 		== BlrtBaseIssues.UnknownStatusCode) {
					type = "unknown_status_code";
				}

				if (string.IsNullOrEmpty (type))
					return false;

				SimpleAlert alertArgs = new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate (string.Format("{0}_{1}_title", prefix, type)),
					BlrtUtil.PlatformHelper.Translate (string.Format("{0}_{1}_message", prefix, type)),
					BlrtUtil.PlatformHelper.Translate (string.Format("{0}_{1}_cancel", prefix, type)),
					BlrtUtil.PlatformHelper.Translate ("alert_issues_appstore")
				);
				var executor = BlrtData.ExecutionPipeline.Executor.System ();
				BlrtManager.App.ShowAlert (executor, alertArgs).ContinueWith ((alertTask) => {
					var alert = alertTask.Result;
					if (!alert.Success || alert.Result.Cancelled) {
						// do nothing
					} else {
						BlrtUtil.PlatformHelper.OpenAppStore ();
					}
				});
				return false;
			} 

			return true;
		}
	}
}

