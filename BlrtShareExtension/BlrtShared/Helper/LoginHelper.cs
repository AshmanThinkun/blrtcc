﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using BlrtAPI;
using Parse;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace BlrtData.Helpers
{
	public static class LoginHelper
	{
		const int SignInTimeout = 15 * 1000;
		const int RegisterTimeout = 15 * 1000;

		public enum Result
		{

			Success,

			InvalidParameters,

			InternetProblem,
			ParseProblem,
			UnknownProblem,

			EmailInUse,

			UnmatchedEmailPassword
		}

		public class LoginDetails
		{

			public bool ValidLogin { get { return IsValidEmail (Username); } }
			public bool ValidSignUp { get { return ValidLogin && IsValidPassword (Password); } }

			public string Username { get; set; }
			public string Password { get; set; }

			public string Name { get; set; }
			public string Industry { get; set; }
			public bool HasAgreeTOS { get; set; }
			public bool IsQuickSignUp { get; set; }
			public string ReferredBy { get; set; }


			public LoginDetails (string username, string password)
			{
				this.Username = username;
				this.Password = password;
			}



			public LoginDetails (string username, string password, string name, string industry)
			{
				this.Username = username;
				this.Password = password;
				this.Name = name;
				this.Industry = industry;
			}

			public LoginDetails (string username, string password, bool isQuickSignup, bool hasAgreeTOS)
			{
				this.Username = username;
				this.Password = password;
				this.IsQuickSignUp = isQuickSignup;
				this.HasAgreeTOS = hasAgreeTOS;
			}

			public LoginDetails (string username, string password, string name, string industry, bool hasAgreeTOS)
			{
				this.Username = username;
				this.Password = password;
				this.Name = name;
				this.Industry = industry;
				this.HasAgreeTOS = hasAgreeTOS;
			}
		}


		public static async Task<Result> ResetPassword (LoginDetails user)
		{

			if (!IsValidEmail (user.Username))
				return Result.InvalidParameters;

			try {
				var cts = new CancellationTokenSource (SignInTimeout);

				await ParseUser.RequestPasswordResetAsync (user.Username, cts.Token);

				return Result.Success;

			} catch (ParseException ex) {

				switch (ex.Code) {
				case ParseException.ErrorCode.EmailMissing:
				case ParseException.ErrorCode.EmailNotFound:
				case ParseException.ErrorCode.InvalidEmailAddress:
				case ParseException.ErrorCode.PasswordMissing:
				case ParseException.ErrorCode.UsernameMissing:
				case ParseException.ErrorCode.MustCreateUserThroughSignup:
					return Result.InvalidParameters;

				case ParseException.ErrorCode.ObjectNotFound:
					return Result.UnmatchedEmailPassword;

				case ParseException.ErrorCode.InternalServerError:
				case ParseException.ErrorCode.NotInitialized:
				case ParseException.ErrorCode.UnsupportedService:
				case ParseException.ErrorCode.InvalidLinkedSession:
				case ParseException.ErrorCode.OperationForbidden:
					return Result.ParseProblem;

				case ParseException.ErrorCode.Timeout:
				case ParseException.ErrorCode.ConnectionFailed:
					return Result.InternetProblem;
				}

				return Result.UnknownProblem;

			} catch (WebException ex) {
				return Result.InternetProblem;
			} catch (TaskCanceledException ex) {
				return Result.InternetProblem;
			} catch (Exception ex) {
				return Result.UnknownProblem;
			}
		}

		private static async Task<Result> SignIn (LoginDetails user)
		{
			if (user == null || !user.ValidLogin)
				return Result.InvalidParameters;

			try {
				var cts = new CancellationTokenSource (SignInTimeout);

				await ParseUser.LogInAsync (user.Username.ToLower (), user.Password, cts.Token);

				return Result.Success;

			} catch (ParseException ex1) {

				switch (ex1.Code) {
				case ParseException.ErrorCode.EmailMissing:
				case ParseException.ErrorCode.EmailNotFound:
				case ParseException.ErrorCode.InvalidEmailAddress:
				case ParseException.ErrorCode.PasswordMissing:
				case ParseException.ErrorCode.UsernameMissing:
				case ParseException.ErrorCode.MustCreateUserThroughSignup:
					return Result.InvalidParameters;

				case ParseException.ErrorCode.ObjectNotFound:
					return Result.UnmatchedEmailPassword;

				case ParseException.ErrorCode.InternalServerError:
				case ParseException.ErrorCode.NotInitialized:
				case ParseException.ErrorCode.UnsupportedService:
				case ParseException.ErrorCode.InvalidLinkedSession:
				case ParseException.ErrorCode.OperationForbidden:
					return Result.ParseProblem;

				case ParseException.ErrorCode.Timeout:
				case ParseException.ErrorCode.ConnectionFailed:
					return Result.InternetProblem;
				}

				return Result.UnknownProblem;

			} catch (WebException ex2) {
				return Result.InternetProblem;
			} catch (TaskCanceledException ex3) {
				return Result.InternetProblem;
			} catch (Exception ex4) {
				return Result.UnknownProblem;
			}
		}


		public static string GetRefererId ()
		{
			try {
				if (BlrtPersistant.Properties.ContainsKey ("HasSignedUpWithReferrerId"))
					return null;
				if (BlrtUserHelper.InstallMetaData == null || !BlrtUserHelper.InstallMetaData.ContainsKey ("inviter"))
					return null;
				var str = BlrtUserHelper.InstallMetaData ["inviter"];
				if (string.IsNullOrEmpty (str))
					return null;
				var id = BlrtEncode.DecodeId (str);


#if __IOS__
				if (!(BlrtUserHelper.InstallMetaData.ContainsKey ("yozio_probability")))
					return id;
				else {
					var pos = float.Parse (BlrtUserHelper.InstallMetaData ["yozio_probability"]);
					if (pos > 0.5)
						return id;
					else
						return null;
				}
#endif

				return id;

			}catch(Exception e){
				BlrtUtil.Debug.ReportException (e);
				return null;
			}
		}

		public static string GetSignupUrl(){
			if (BlrtPersistant.Properties.ContainsKey ("HasSignedUpWithSignupUrl"))
				return null;
			if (BlrtUserHelper.InstallMetaData == null || BlrtUserHelper.InstallMetaData.Count==0)
				return null;
			var str = BlrtUserHelper.InstallMetaData.Select(x => x.Key + "=" + Uri.EscapeDataString(x.Value)).Aggregate((s1, s2) => s1 + "&" + s2);;
			if (string.IsNullOrEmpty (str))
				return null;
			return str;
		}


		public static async Task<Result> Register(LoginDetails user) {
			if (user == null || !user.ValidSignUp)
				return Result.InvalidParameters;

			var _params = new APIUserRegister.Parameters () {
				Email		= user.Username,
				Name		= user.Name,
				Password	= user.Password,
				IsQuickSignUp = user.IsQuickSignUp,
				Extra		= new Dictionary<string, object> () {
					{ BlrtUserHelper.IndustryKey,			user.Industry },//////
					{ BlrtUserHelper.HasAgreedToTOSKey,		user.HasAgreeTOS }
				}
			};

			var referer = GetRefererId ();
			if(!string.IsNullOrEmpty(referer)){
				_params.Extra [BlrtUserHelper.ReferredByKey] = referer;
			}

			var signupUrl = GetSignupUrl ();
			if(!string.IsNullOrEmpty(signupUrl)){
				_params.Extra ["signupUrl"] = signupUrl;
			}

			APIUserRegister request = new APIUserRegister (_params);

			if (!await request.Run (new CancellationTokenSource (15000).Token)) {
				return Result.InternetProblem;
			}


			if (!request.Response.Success) {
				// UserRegister failed --> parse error result and return
				switch(request.Response.Error.Code) {
					case APIUserRegisterError.EmailInUse:
						return Result.EmailInUse;
					case APIUserRegisterError.InvalidParameters:
						return Result.InvalidParameters;
					case APIUserRegisterError.InvalidVersion:
						return Result.ParseProblem;
					case APIUserRegisterError.SubqueryError:
						return Result.InternetProblem;
				}
				return Result.UnknownProblem;
			}

			BlrtPersistant.Properties ["HasSignedUpWithReferrerId"] = true;
			BlrtPersistant.Properties ["HasSignedUpWithSignupUrl"] = true;
			BlrtPersistant.SaveProperties();

			return Result.Success;
		}



#region IsValid Functions

		public static bool IsValidPassword(string password) {
			return (null != password) && (password.Length > 7);
		}

		public static bool IsValidEmail(string email) {
			return (null != email) && BlrtUtil.IsValidEmail (email);
		}

		public static bool IsValidIndustry(string industry) {
			return !string.IsNullOrWhiteSpace(industry);
		}

		public static bool IsValidName(string name) {
			return !string.IsNullOrEmpty(name);
		}

#endregion


#region UI
		public static async Task<bool> Login(Page page, LoginHelper.LoginDetails details)
		{
			var result = await LoginHelper.SignIn (details);
			bool retryLogin = false;
			switch (result) {

				case LoginHelper.Result.Success:
					// success --> navigate to mail box
					
					return true;

					// Errors that might happen
				case LoginHelper.Result.InternetProblem:
				case LoginHelper.Result.ParseProblem:
					retryLogin = await page.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Unable to log in"), 
						BlrtUtil.PlatformHelper.Translate ("Check your internet connection or visit blrt.com for help."),
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);
					break;
				case LoginHelper.Result.UnmatchedEmailPassword:
					if (Device.OS == TargetPlatform.iOS) {
						//wrong password or email exist
						await page.DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("alert_login_signup_title",	"login"),  
							BlrtUtil.PlatformHelper.Translate ("alert_login_signup_message",	"login"), 
							BlrtUtil.PlatformHelper.Translate ("alert_login_signup_cancel", "login")
						);
					} else {
						await page.DisplayAlert (
							BlrtUtil.PlatformHelper.Translate ("alert_login_wrong_details_title",	"login"),  
							BlrtUtil.PlatformHelper.Translate ("alert_login_wrong_details_message",	"login"), 
							BlrtUtil.PlatformHelper.Translate ("alert_login_wrong_details_accept", "login")
						);
					}
					retryLogin = false;
					break;
					// Errors that should not happen
				default:
					retryLogin = await page.DisplayAlert (
						BlrtUtil.PlatformHelper.Translate ("Unable to log in"), 
						BlrtUtil.PlatformHelper.Translate ("Check your internet connection or visit blrt.com for help."),
						BlrtUtil.PlatformHelper.Translate ("Retry"),
						BlrtUtil.PlatformHelper.Translate ("Cancel")
					);
					break;
			}
			if (retryLogin) {
				return await Login (page, details);
			}

			return false;
		}
#endregion
	}
}

