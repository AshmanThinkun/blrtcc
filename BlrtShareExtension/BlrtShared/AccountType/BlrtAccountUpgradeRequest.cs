using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.Query;
using System.Threading;
using Parse;

namespace BlrtData
{
	public abstract class BlrtAccountUpgradeRequest
	{
		public enum BlrtAccountUpgradeRequestErrorCode {
			Unknown = 0,

			ServerConnectionError = 1,
			PaymentCancelled = 2,
			RecentDuplicateRequest = 3,
			DuplicateRequest = 4,

			TrialAlreadyUsed = 101,
			CurrentAccountTypeSuperior = 102,

			iOSUnknownPaymentError = 200,
			iOSAppStoreConnectionFailed = 201,
			iOSPaymentFailed = 202,
			iOSCannotMakePayments = 203,

			AndroidPaymentFailed = 300,
		};


		public class BlrtAccountUpgradeRequestResponse
		{
			public static BlrtAccountUpgradeRequestResponse DefaultResponse ()
			{
				return new BlrtAccountUpgradeRequestResponse () {
					success = false,
					error_code_enum = BlrtAccountUpgradeRequestErrorCode.Unknown
				};
			}


			// Coming in from CC
			public bool Success {
				get {
					return success || error_code_enum == BlrtAccountUpgradeRequestErrorCode.RecentDuplicateRequest;
				}
			}
			public bool success;

			public BlrtAccountUpgradeRequestErrorCode? ErrorCode {
				get {
					// Recent duplicate upgrade requests are displayed as "success" (as they often happen following a "Try again")
					if (error_code_enum == BlrtAccountUpgradeRequestErrorCode.RecentDuplicateRequest)
						return null;

					return error_code_enum;
				}
			}
			public BlrtAccountUpgradeRequestErrorCode? error_code_enum;
			public int? error_code;


			public BlrtAccountUpgradeRequestResponse ()
			{
				success	= false;

				error_code_enum = null;
				error_code = null;
			}


			public void Unpack ()
			{
				if (error_code != null) {
					if (Enum.IsDefined (typeof(BlrtAccountUpgradeRequestErrorCode), error_code))
						error_code_enum = (BlrtAccountUpgradeRequestErrorCode)error_code;
					else
						error_code_enum = BlrtAccountUpgradeRequestErrorCode.Unknown;
				}
			}
		}


		public BlrtAccountUpgradeRequestResponse RequestResponse { get; private set; }


		public async Task<BlrtAccountUpgradeRequestResponse> DespatchToCC (string requestFor, IDictionary<string, object> additionalRequestData = null)
		{
			RequestResponse = BlrtAccountUpgradeRequestResponse.DefaultResponse ();


			string serverResponse = null;

			try {
				var data = new Dictionary<string, object> ();
				data.Add ("requestFor", requestFor);


				if (additionalRequestData != null)
					data.Add ("data", additionalRequestData);


				serverResponse = await BlrtUtil.BetterParseCallFunctionAsync<string> ("requestAccountUpgrade", data, new CancellationTokenSource (15000).Token);
			} catch (TaskCanceledException exception) {
				RequestResponse.error_code_enum = BlrtAccountUpgradeRequestErrorCode.ServerConnectionError;
			} catch (ParseException exception) {
				RequestResponse.error_code_enum = BlrtAccountUpgradeRequestErrorCode.ServerConnectionError;
			} catch (System.Net.WebException exception) {
				RequestResponse.error_code_enum = BlrtAccountUpgradeRequestErrorCode.ServerConnectionError;
			}

			if (serverResponse == null)
				return RequestResponse;

			try {
				var settingsQuery = BlrtSettingsQuery.RunQuery();
				await settingsQuery.WaitAsync ();
			} catch {}


			try {
				RequestResponse = JsonConvert.DeserializeObject<BlrtAccountUpgradeRequestResponse> (serverResponse);
				RequestResponse.Unpack ();
			} catch (JsonException exception) {
				RequestResponse.error_code_enum = BlrtAccountUpgradeRequestErrorCode.ServerConnectionError;
			}


			return RequestResponse;
		}


		public abstract Task<BlrtAccountUpgradeRequestResponse> Start ();
	}
}

