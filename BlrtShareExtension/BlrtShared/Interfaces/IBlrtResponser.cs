using System;
using System.Threading.Tasks;

namespace BlrtData
{
	public interface IBlrtResponser
	{
		DeviceDetails DeviceDetails { get; }

		bool InMaintanceMode { get; }

		string AppName { get; }

		void ActivateLockout(BlrtLockoutMode mode, bool animated);
		void DeactivateLockout(BlrtLockoutMode mode, bool animated);

		void ShowNotification(IBlrtNotification notification, bool animated);

		void ShowFeedbackRatingDialog ();

		bool CanOpenEvent(BlrtEventType eventType);

		void PushLoading (bool animated);
		void PopLoading (bool animated);

		void Logout(bool animated);
		void Login(bool isNewUser, bool animated, bool signupViaSMSLink);

		void OpenMediaFromUrl (string url);
		void OpenMediaFromUrl (string[] urls);

		void AppReceivedPush ();


		void RecalulateAppBadge();

		void OpenWebUrl (string str, bool openInApp);

		bool ShowUpgradeModal (BlrtPermissions.BlrtAccountThreshold threshold);
	}


	public enum BlrtEventType
	{
		OpenPDF,

		OpenConversation,
		OpenImage,

	}


	[Flags]
	public enum BlrtLockoutMode {
		None				= 0x00,
		Maintance			= 0x01,
		InvalidVersion		= 0x02
	}
}

