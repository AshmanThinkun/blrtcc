using System;

namespace BlrtData
{
	public enum LocalStorageType : int {
		Default = 0,
		User = 1,
		IndependentPublic = 2,
		IndependentPrivate = 3
	};

	public interface ILocalStorageParameter
	{
		ILocalDirectories LocalDirectories { get; }
		BlrtData.Data.BlrtDataManagerBase DataManager { get; }
	}

	public static class LocalStorageParameters
	{
		static ILocalStorageParameter _default;
		static ILocalStorageParameter _user;
		static ILocalStorageParameter _public;
		static ILocalStorageParameter _private;

		public static ILocalStorageParameter Default 
		{ 
			get {
				if (null == _default) {
					_default = new LocalStorageParam () {
						LocalDirectories = BlrtConstants.Directories.Default,
						DataManager = BlrtManager.DataManagers.Default
					};
				}
				return _default; 
			} 
		}

		public static ILocalStorageParameter User 
		{ 
			get {
				if (null == _user) {
					_user = new LocalStorageParam () {
						LocalDirectories = BlrtConstants.Directories.User,
						DataManager = BlrtManager.DataManagers.User
					};
				}
				return _user; 
			} 
		}

		public static ILocalStorageParameter IndependentPublic 
		{ 
			get {
				if (null == _public) {
					_public = new LocalStorageParam () {
						LocalDirectories = BlrtConstants.Directories.IndependentPublic,
						DataManager = BlrtManager.DataManagers.IndependentPublic
					};
				}
				return _public; 
			} 
		}

		public static ILocalStorageParameter IndependentPrivate 
		{ 
			get {
				if (null == _private) {
					_private = new LocalStorageParam () {
						LocalDirectories = BlrtConstants.Directories.IndependentPrivate,
						DataManager = BlrtManager.DataManagers.IndependentPrivate
					};
				}
				return _private; 
			} 
		}

		public static ILocalStorageParameter Get (LocalStorageType type)
		{
			switch (type) {
				case LocalStorageType.User:
					return User;
				case LocalStorageType.IndependentPublic:
					return IndependentPublic;
				case LocalStorageType.IndependentPrivate:
					return IndependentPrivate;
				case LocalStorageType.Default:
				default:
					return Default;
			}
		}

		private class LocalStorageParam : ILocalStorageParameter
		{
			public ILocalDirectories LocalDirectories { get; set; }
			public BlrtData.Data.BlrtDataManagerBase DataManager { get; set; }
		}
	}
}

