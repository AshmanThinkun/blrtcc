using System;
using System.Threading.Tasks;
using System.Drawing;

namespace BlrtData
{
	public interface IPlatformHelper
	{
		string Translate(string field);
		string Translate(string field, string context);

		string TranslateDate(DateTime date);
		string TranslateDateLong(DateTime date);
		Xamarin.Forms.FormattedString TranslateTime(DateTime date);

		float GetDeviceWidth();
		float GetDeviceHeight();
		string GetAppVersion();
		string GetPlatformName();
		string CountryCode{get;}

		bool IsPhone{ get;}
		Task OpenAppStore();
		Task<bool> ShowWaitAlert (Task task, string title, string message, string cancel);

		Size GetImageSize (string filePath);
		Task<string> CompressLargeImageToJpeg (string path, int maxSize);
		Task CopyAsJpeg (string src, string dst);
	}
}

