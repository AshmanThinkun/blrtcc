using System;

namespace BlrtData
{
	public interface ILocalDirectories
	{
		string GetDataPath 			(string fileName);
		string GetFilesPath 		(string fileName);
		string GetThumbnailPath		(string fileName);
		string GetCachedPath 		(string fileName);
		string GetDecryptedPath 	(string fileName);
		string GetDownloaderPath	(string fileName);
		string GetAvatarPath		(string fileName);
		string GetRecentPath		(string fileName);
	}
}

