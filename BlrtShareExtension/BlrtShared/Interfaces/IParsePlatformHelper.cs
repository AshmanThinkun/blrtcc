using System;
using System.Threading;
using System.Threading.Tasks;
using Parse;

namespace BlrtData
{
	public interface IParsePlatformHelper
	{
		Task<ParseFile> UploadFile (string cloudFileName, string filePath, CancellationToken token, IProgress<float> progressCallback = null);
	}
}

