using System;
using System.Collections.Generic;
using System.IO;

namespace BlrtData
{
	/// <summary>
	/// This is used to remember the last state of the canvas, such as what color for what tools
	/// </summary>
	public class CanvasStateManager
	{
		static CanvasStateManager _last;
		static string Path {get{ return BlrtData.BlrtConstants.Directories.Default.GetFilesPath ("canvas.json");}}


		public static CanvasStateManager Last {
			get {
				if (_last == null)
					_last = Load ();

				return _last;
			}
		}

		static CanvasStateManager Load ()
		{
			try{
				var str = File.ReadAllText(Path);
				return Newtonsoft.Json.JsonConvert.DeserializeObject<CanvasStateManager>(str);
			} catch { }

			return new CanvasStateManager ();
		}

		public void Save(){

			try{
				var str = Newtonsoft.Json.JsonConvert.SerializeObject (this);
				File.WriteAllText (Path, str);
			} catch { }
		}

		public BlrtData.BlrtBrush brush;
		public BlrtData.BlrtBrushWidth width;

		public Dictionary<BlrtData.BlrtBrush, BlrtData.BlrtBrushColor> colors;

		public CanvasStateManager ()
		{
			brush = BlrtData.BlrtBrush.Pointer;
			width = BlrtData.BlrtBrushWidth.Meduim;

			colors = new Dictionary<BlrtData.BlrtBrush, BlrtData.BlrtBrushColor> ();
		}



		public BlrtData.BlrtBrushColor GetColor(BlrtData.BlrtBrush brush){
			if(colors.ContainsKey(brush))
				return colors[brush];
			return BlrtData.BlrtBrushColor.Red;
		}

		public void SetColor(BlrtData.BlrtBrush brush, BlrtData.BlrtBrushColor color){
			if(colors.ContainsKey(brush))
				colors[brush] = color;
			else
				colors.Add(brush, color);
		}
	}
}

