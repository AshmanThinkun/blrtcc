#if __ANDROID__

namespace BlrtData
{
	public static partial class BlrtUtil
	{
		static int _stackCounter = 0;

		public static void PushDownloadStack()
		{
			_stackCounter++;
		}

		public static void PopDownloadStack()
		{
			_stackCounter--;
		}
	}
}
#endif