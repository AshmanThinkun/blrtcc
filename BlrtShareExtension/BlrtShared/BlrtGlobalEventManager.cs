using System;
using System.Threading.Tasks;

namespace BlrtData
{
	public static class BlrtGlobalEventManager
	{
		public static EventHandler<EventArgs> OnGotSettings;
		public static EventHandler<EventArgs> OnAccountTypeChanged;
		public static EventHandler<EventArgs> OnAvailableIAPsChanged;

		public static void GotSettings (object sender, EventArgs args) {
			if (OnGotSettings != null) {
				Task.Run (() => {
					var handler = OnGotSettings;
					if (handler != null)
						handler (sender, args);
				});
			}
		}

		public static void AccountTypeChanged (object sender, EventArgs args)
		{
			if (OnAccountTypeChanged != null) {
				Task.Run (() => {
					var handler = OnAccountTypeChanged;
					if (handler != null)
						handler (sender, args);
				});
			}
		}

		public static void AvailableIAPsChanged (object sender, EventArgs args)
		{
			if (OnAvailableIAPsChanged != null) {
				Task.Run (() => {
					var handler = OnAvailableIAPsChanged;
					if (handler != null)
						handler (sender, args);
				});
			}
		}
	}
}

