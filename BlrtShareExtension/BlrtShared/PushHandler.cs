﻿using System;
using System.Collections.Generic;
using BlrtData.ExecutionPipeline;
using Parse;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BlrtData
{
	public enum BlrtPushState
	{

		InApp,
		OpenFromPush,
		ResumeFromPush,
		InBackground,
		ResumeFromBackground,
		Background

	}

	public static class PushHandler
	{

		public enum BlrtPushType
		{

			Message = -1,

			SettingRefresh = 0,
			BlrtRefresh = 1,

			UpdateApp = 2,

			RemovedFromConvo = 3,

			ContactInfoRefresh = 5,

			Logout = 6,

			OpenUrl = 10,
			OpenSupport = 11,
			OpenNewBlrt = 12,

			UpgradeAccount = 20,
			UpgradeAccountPremium = 21,

		}


		public static event EventHandler<string> OnBlrtRefresh;
		public static event EventHandler OnContactInfoRefresh;

		public static async Task<bool> ParsePush(BlrtPushState state, IDictionary<string, object> pushData)
		{
			var type = BlrtPushType.Message;

			var source = new PushExecutor(state == BlrtPushState.InApp);

			if (pushData.ContainsKey(BlrtUtil.PushNotificationConstants.NOTIFICATION_TYPE))
			{
				int value = Convert.ToInt32(pushData[BlrtUtil.PushNotificationConstants.NOTIFICATION_TYPE]);

				if (PendingActionHandler.IsPendingActionType(value))
				{
					PendingActionHandler.HandlePendingActions(new IDictionary<string, object>[] { pushData });
					return true;
				}

				if (Enum.IsDefined(typeof(BlrtPushType), value))
					type = (BlrtPushType)value;
			}


#if __ANDROID__
			//with these types of push, allow the actions to be done in the background
			if (type == BlrtPushType.BlrtRefresh || type == BlrtPushType.SettingRefresh || type == BlrtPushType.ContactInfoRefresh)
			{
				source = new PushExecutor(state == BlrtPushState.InApp || state == BlrtPushState.InBackground);
			}
#endif

			ExecutionPerformer instr = null;

			try
			{
				switch (type)
				{
					case BlrtPushType.SettingRefresh:
						BlrtData.Query.BlrtSettingsQuery.RunQuery();
						break;

					case BlrtPushType.BlrtRefresh:
						if (pushData.ContainsKey(BlrtUtil.PushNotificationConstants.CONVERSATION_ID))
						{
							instr = PerformRefresh(pushData[BlrtUtil.PushNotificationConstants.CONVERSATION_ID].ToString(), source, state);
						}
						break;

					case BlrtPushType.ContactInfoRefresh:
						BlrtData.Query.BlrtSettingsQuery.RunQuery();
						BlrtData.Query.BlrtGetContactDetailsQuery.RunQuery();

						BlrtData.Query.BlrtInboxQuery.RunQuery(
							BlrtData.Query.BlrtInboxQueryType.Inbox, 0
						);
						if (OnContactInfoRefresh != null)
							OnContactInfoRefresh(new object(), EventArgs.Empty);
						break;

					case BlrtPushType.UpdateApp:
						instr = UpdatePush(source, pushData);
						break;

					case BlrtPushType.OpenUrl:
						instr = OpenUrl(source, pushData);
						break;

					case BlrtPushType.OpenSupport:
						instr = OpenSupport(source, pushData);
						break;

					case BlrtPushType.OpenNewBlrt:
						instr = OpenNewBlrt(source, pushData);
						break;

					case BlrtPushType.UpgradeAccount:
					case BlrtPushType.UpgradeAccountPremium:
						instr = UpgradeAccount(source, type, pushData);
						break;
					case BlrtPushType.Logout:
						ForceUserLogout(source, BlrtUserHelper.AccountStatus, state);
						break;

					case BlrtPushType.RemovedFromConvo:
						instr = await RemovedFromConvo(source, pushData);
						break;

				}
			}
			catch (Exception ex)
			{
				BlrtUtil.Debug.ReportException(ex);
			}

			bool showMessage = state == BlrtPushState.InApp;

			if (instr != null)
			{
				switch (type)
				{
					case BlrtPushType.UpdateApp:
					case BlrtPushType.OpenUrl:
					case BlrtPushType.OpenSupport:
						showMessage = true;
						break;
				}
			}

			if ((showMessage && instr != null) || type == BlrtPushType.Message)
			{

				var alert = new PushAlert(pushData);

				if (!string.IsNullOrEmpty(alert.Title))
				{

					var performer = new ShowAlertPerformer(source,
						alert, (int arg) =>
						{
							if (instr != null)
								instr.RunAction();
						}
					);

					performer.RunAction();
					return true;
				}
			}

			if (instr != null)
			{
				instr.RunAction();

				return true;
			}

			LLogger.WriteObject(pushData);
			return false;
		}

		/// <summary>
		/// Performs the refresh. This method does nothing and returns null if is called while app is in background state
		/// </summary>
		/// <returns>The refresh.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="source">Source.</param>
		/// <param name="pushState">Push state.</param>
		public static ExecutionPerformer PerformRefresh(string conversationId, PushExecutor source, BlrtPushState pushState)
		{
			if (pushState == BlrtPushState.Background)
			{
				return null;
			}

			var conversation = LocalStorageParameters.Default.DataManager.GetConversation(conversationId);
			var conversationLocal = conversation != null;
			if (conversationLocal)
			{
				conversation.NeedsRefresh = true;
			}
			var instr = ParseConversationPush(source, conversationId, conversationLocal, pushState);
			FetchConversationUserRelation(conversationId);

			return instr;
		}

		/// <summary>
		/// Parses the conversation push notification and run cloud queries if needed to fetch new data.
		/// </summary>
		/// <returns>The conversation push.</returns>
		/// <param name="source">Source.</param>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="conversationLocal">If set to <c>true</c> conversation local.</param>
		/// <param name="pushState">Push state.</param>
		static ExecutionPerformer ParseConversationPush(IExecutor source,
														 string conversationId,
														 bool conversationLocal,
														 BlrtPushState pushState)
		{
			if (!conversationLocal)
			{
				Query.BlrtConversationQuery.RunQuery(conversationId, -1, 0, LocalStorageParameters.Default);
			}

#if __IOS__
			if (pushState == BlrtPushState.ResumeFromBackground) {
				return null;
			}
#elif __ANDROID__

			if (OnBlrtRefresh != null) {
			  OnBlrtRefresh (source, conversationId);
		  	}
#endif

			if ((source.Cause & ExecutionSource.InApp) != ExecutionSource.InApp)
				return new OpenConversationPerformer (source, conversationId);
			return null;
		}

		static async Task<ExecutionPerformer> RemovedFromConvo (IExecutor source, IDictionary<string, object> pushData)
		{
			if (pushData.ContainsKey (BlrtUtil.PushNotificationConstants.CONVERSATION_ID)) {
				var id = pushData [BlrtUtil.PushNotificationConstants.CONVERSATION_ID].ToString ();
				ILocalStorageParameter localStorage = LocalStorageParameters.Default;
				var convo = localStorage.DataManager.GetConversation (id);

				if (convo != null) {
					Device.BeginInvokeOnMainThread (async () => {
						var alert = new SimpleAlert (
							BlrtUtil.PlatformHelper.Translate (""),
							BlrtUtil.PlatformHelper.Translate ("You were removed from a conversation: " + convo.Name),
							BlrtUtil.PlatformHelper.Translate ("Ok")
						);
						BlrtManager.App.ShowAlert (source, alert).FireAndForget ();
						var rel = BlrtManager
								.DataManagers
								.Default
								.GetConversationUserRelation (id, ParseUser.CurrentUser.ObjectId);

						await BlrtUtil.DeleteConversationFromLocal (convo.LocalStorage, convo, source, rel);


#if __ANDROID__
						if (OnBlrtRefresh != null) {
							OnBlrtRefresh (source, id);
						}
#endif

					});
				}
			}
			return null;
		}

		static ExecutionPerformer UpdatePush (IExecutor source, IDictionary<string, object> pushData)
		{

			var output = new OpenWebpagePerformer (source, BlrtSettings.AppStoreUrl);
			return output;
		}

		static ExecutionPerformer OpenUrl (IExecutor source, IDictionary<string, object> pushData)
		{

			if (pushData.ContainsKey ("u")) {
				var output = new OpenWebpagePerformer (
					source,
					pushData ["u"].ToString ()
				);
				//if(pushData.ContainsKey("i")) output.InApp 		= Convert.ToInt32(pushData ["i"].ToString()) > 0;

				return output;
			}
			return null;
		}

		static ExecutionPerformer OpenSupport (IExecutor source, IDictionary<string, object> pushData)
		{

			var output = new OpenSupportPerformer (source);
			return output;
		}

		static ExecutionPerformer OpenNewBlrt (IExecutor source, IDictionary<string, object> pushData)
		{

			var output = new BlrtRequestArgs ();

			if (pushData.ContainsKey ("n")) output.BlrtName = pushData ["n"].ToString ();
			if (pushData.ContainsKey ("c")) output.CaptureUrl = pushData ["c"].ToString ();
			if (pushData.ContainsKey ("m")) output.DownloadUrls = new [] { pushData ["m"].ToString () };
#if __ANDROID__
			return new CreateNewBlrtPerfomer (source, output);
#else
			return new ExternalMediaUrlHandler (source, output);
#endif
		}

		static ExecutionPerformer UpgradeAccount (IExecutor source, BlrtPushType type, IDictionary<string, object> pushData)
		{

			return new OpenUpgradeAccountPerformer (source) {
			};
		}

		public static void ForceUserLogout (IExecutor source, string accountStatus, BlrtPushState state)
		{
			//var status = pushData ["status"].ToString ();
			//Console.WriteLine ("===ForceUserLogout status: " + status);
			//BlrtManager.Logout (true);
			//if (status == "deleted") {
			//	BlrtConstants.DeleteEverything ();
			//}
			//Console.WriteLine ("===ForceUserLogout: DeleteEverything");

			BlrtManager.Logout (true);
			if (accountStatus == BlrtUser.DELETED_ACCOUNT_STATUS_STRING) {
				BlrtConstants.DeleteEverything ();
			}

			if (state == BlrtPushState.InApp) {
				IAlert alert;
				if (accountStatus == BlrtUser.DELETED_ACCOUNT_STATUS_STRING || accountStatus == BlrtUser.SUSPENDED_ACCOUNT_STATUS_STRING) {
					alert = new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("account_status_title_" + accountStatus, "profile"),
						BlrtUtil.PlatformHelper.Translate ("account_status_message_" + accountStatus, "profile"),
						BlrtUtil.PlatformHelper.Translate ("account_status_cancel_" + accountStatus, "profile")
					);
					BlrtManager.App.ShowAlert (source, alert);
				}
			}
		}


		static void FetchConversationUserRelation (string convoId)
		{
			if (null == ParseUser.CurrentUser) {
				return;
			}
			var rel = BlrtManager.DataManagers.Default.GetConversationUserRelation (
				convoId,
				ParseUser.CurrentUser.ObjectId
			);
			try {
				BlrtData.Query.BlrtConversationMetaQuery.RunQuery (rel).Run ();
			} catch { }
		}



		public class PushExecutor : IExecutor
		{
#region IExecutor implementation
			public ExecutionSource Cause {
				get { return (_inApp ? ExecutionSource.InApp | ExecutionSource.Push : ExecutionSource.Push); }
			}
#endregion

			bool _inApp;

			public PushExecutor (bool inApp)
			{
				this._inApp = inApp;
			}
		}
	}
}

