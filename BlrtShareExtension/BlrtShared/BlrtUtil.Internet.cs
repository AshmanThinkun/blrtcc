using System;
using System.Net.NetworkInformation;
using System.IO;
using System.Threading;
using BlrtData.IO;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using BlrtData.ExecutionPipeline;
using BlrtAPI;

#if __ANDROID__
using BlrtAndroid.UI;
#endif

#if __IOS__
using System.Linq;
using BlrtiOS.Views.Conversation;
using Acr.UserDialogs;
using Xamarin.Forms;
using BlrtData.Contacts;
#endif

namespace BlrtData
{
	public static partial class BlrtUtil
	{
		/// <summary>
		/// URL constants. Used in all types of blrt URLs
		/// </summary>
		public static class URLConstants
		{
			public static class Paths
			{
				public const string CONVERSATION = "conv";
				public const string BLRT = "blrt";
			}
		}

		/// <summary>
		/// Push notification constants.
		/// </summary>
		public static class PushNotificationConstants
		{
			public const string NOTIFICATION_TYPE = "t";
			public const string S = "s"; //TODO: Don't really know what it is. We don't really use it.
			public const string CONVERSATION_ID = "bid";
			public const string USER_ID = "uid";
			public const string PENDING_ACTION_ID = "pid";
		}

		public static event EventHandler ReachabilityChanged;


		public static bool HasConnectionIssue {
			get { return !IsOnline || (_connectionIssues); }
		}
		public static bool IsOnline { get { return Reachability.InternetConnectionStatus () != NetworkStatus.NotReachable; } }

		public static void SetConnectionHasProblem (bool hasIssue)
		{

			SetupIfNeeded ();

			if (hasIssue != _connectionIssues) {
				InternetChangedState ();
			}
		}



		private static bool _connectionIssues = false;
		private static bool _setup = false;
		private static bool _runningCheck = false;

		private static string _ip;

		static void SetupIfNeeded ()
		{
			if (_setup) return;
			_setup = true;
			Reachability.ReachabilityChanged += InternetChangedState;
		}

		static void InternetChangedState (object sender, EventArgs e)
		{
			_ip = null;
			InternetChangedState ();
		}

		static async void InternetChangedState ()
		{
			if (!_runningCheck) {
				_runningCheck = true;

				bool prev = _connectionIssues;
				_connectionIssues = await BlrtData.Query.BlrtParseActions.HasConnectionProblem ();

				_runningCheck = false;

				if (_connectionIssues != prev) {
					try {
						var e = ReachabilityChanged;
						if (e != null)
							e.Invoke (ReachabilityChanged, EventArgs.Empty);
					} catch (Exception e) {
						LLogger.WriteEvent ("Has Connection Issue", "Failed to change state", e.ToString ());
					}
				}

				if (!_connectionIssues)
					BlrtManager.Query.TryRunQuery ();
			}
		}





		public static string GetDeviceMacAddress ()
		{
			foreach (var netInterface in NetworkInterface.GetAllNetworkInterfaces ()) {
				if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
					netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {
					var address = netInterface.GetPhysicalAddress ();
					return BitConverter.ToString (address.GetAddressBytes ());

				}
			}

			return "NoMac";
		}

		static Task<string> _getIPTask = null;
		public static async Task<string> GetDeviceIPAddress ()
		{
			if (!string.IsNullOrWhiteSpace (_ip))
				return _ip ?? "";

			if (_getIPTask == null || _getIPTask.Status == TaskStatus.Canceled || _getIPTask.Status == TaskStatus.Faulted || _getIPTask.Status == TaskStatus.RanToCompletion) {
				_getIPTask?.Dispose ();
				_getIPTask = InnerGetIPTask ();
			}

			return await _getIPTask;
		}

		static Task<string> InnerGetIPTask ()
		{
			return Task.Run<string> (async () => {
				try {
					string str = null;
					WebRequest request = WebRequest.Create (BlrtConstants.EmbedUrl + "/util/ip");
					request.Timeout = 8 * 1000;
					using (WebResponse response = await request.GetResponseAsync ()) {
						using (StreamReader stream = new StreamReader (response.GetResponseStream ())) {
							str = stream.ReadToEnd ();
						}
					}

					if (!string.IsNullOrWhiteSpace (str)) {
						var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, object>> (str);
						object successObj = null;
						if (dic.TryGetValue ("success", out successObj) && (successObj is bool) && ((bool)successObj)) {
							object ipObj = null;
							if (dic.TryGetValue ("ip", out ipObj)) {
								var ipStr = ipObj as string;
								if (!string.IsNullOrWhiteSpace (ipStr)) {
									_ip = ipStr;
								}
							}
						}
					}
					_getIPTask = null;
					return _ip ?? "";

				} catch {
					_getIPTask = null;
					return _ip ?? "";
				}
			});
		}



		public static string GetFileSafeString (string path)
		{
			Array.ForEach (
				Path.GetInvalidFileNameChars (),
				c => path = path.Replace (c.ToString (), String.Empty)
			);

			return path;
		}

		public static CancellationToken CreateTokenWithTimeout (int millisecondsDelay)
		{
			return new CancellationTokenSource (millisecondsDelay).Token;
		}
		public static CancellationToken CreateTokenWithTimeout (CancellationToken token, int millisecondsDelay)
		{
			return CancellationTokenSource.CreateLinkedTokenSource (
				token,
				new CancellationTokenSource (millisecondsDelay).Token
			).Token;
		}

		public static async Task<string> GetMimeType (Uri url)
		{
			var request = HttpWebRequest.Create (url) as HttpWebRequest;

			HttpWebResponse response = null;
			try {
				response = await request.GetResponseAsync () as HttpWebResponse;

				return response.ContentType;

			} finally {

				if (response != null) {
					response.Close ();
					response.Dispose ();
					response = null;
				}
			}
		}

		public static BlrtMediaFormat MimeTypeToMediaFormat (string mimeType)
		{

			switch (mimeType.ToLower ()) {
			case "application/pdf":
				return BlrtMediaFormat.PDF;
			case "image/gif":
			case "image/jpeg":
			case "image/pjpeg":
			case "image/png":
				return BlrtMediaFormat.Image;
			default:
				return BlrtMediaFormat.Invalid;
			}
		}

		public static string MimeTypeToFileExtention (string mimeType)
		{
			switch (mimeType.ToLower ()) {
			case "application/pdf":
				return ".pdf";
			case "image/gif":
				return ".gif";
			case "image/jpeg":
			case "image/pjpeg":
				return ".jpg";
			case "image/png":
				return ".png";
			default:
				return ".txt";
			}
		}

		private static readonly string [] MD5EtagDomains = {
			"files.parsetfss.com",
			"s3.amazonaws.com"
		};

		private static bool IsEtagMD5 (string host)
		{
			var comparingHost = host.ToLower ();
			foreach (var etagHost in MD5EtagDomains) {
				if (comparingHost.EndsWith (etagHost)) {
					return true;
				}
			}
			return false;
		}

		private const int MD5StrLength = 32;

		public static async Task<string> RequestFileHexMD5 (Uri uri)
		{
			if (!IsEtagMD5 (uri.Host)) {
				return null;
			}
			var request = HttpWebRequest.Create (uri) as HttpWebRequest;
			request.Method = "HEAD";
			using (HttpWebResponse response = await request.GetResponseAsync () as HttpWebResponse) {
				return GetHexMD5FromETag (uri, response.GetResponseHeader ("ETag"));
			}
		}

		public static string GetHexMD5FromETag (Uri uri, string eTag)
		{
			if ((!string.IsNullOrWhiteSpace (eTag)) && IsEtagMD5 (uri.Host)) {
				Regex rgx = new Regex ("[^0-9,a-f]");
				var hash = rgx.Replace (eTag.ToLower (), "");
				if ((null != hash) && (MD5StrLength == hash.Length)) {
					return hash;
				}
			}
			return null;
		}

		public static bool BetterParseBoolean (string value, bool defaultValue = false)
		{
			if (!string.IsNullOrWhiteSpace (value)) {
				var v = value.ToLower ();
				switch (v) {
				case "true":
				case "t":
					return true;
				case "false":
				case "f":
					return false;
				}
				int intValue;
				if (int.TryParse (v, out intValue)) {
					return (0 != intValue);
				}
			}
			return defaultValue;
		}

		public static class Http
		{
			public static async Task<byte []> Post (string uri, NameValueCollection pairs)
			{
				using (WebClient client = new WebClient ()) {
					return await client.UploadValuesTaskAsync (uri, pairs);
				}
			}
		}

		internal static IEnumerable<MediaAsset> GetMediaAssets (Conversation conversation, string [] mediaIds)
		{
			return conversation.LocalStorage.DataManager.GetMediaAssets (mediaIds);
		}

		async static Task<bool> AlertInternetErrorRetry ()
		{
			var alert = await BlrtManager.App.ShowAlert (Executor.System (),
				new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_title"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_message"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_cancel"),
					BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_failed_retry")
				)
			);
			if (!alert.Success || alert.Result.Cancelled) {
				return false;
			} else {
				return true;
			}
		}

		public static IO.BlrtDownloaderList DownloadMedia (int priority,
														   IEnumerable<MediaAsset> assets,
														   Conversation associatedConversation,
														   EventHandler fileDowloadSuccessHandler = null,
														   UnhandledExceptionEventHandler fileDowloadFailureHandler = null,
														   EventHandler fileAssocStatusHandler = null)
		{
			var obj = new object ();

			foreach (var m in assets) {
				if (!m.IsDataLocal ()) {
					var d = IO.BlrtDownloader.Download (priority, m.CloudMediaPath, m.LocalMediaPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (m, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (associatedConversation, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (obj, d);

					if (fileDowloadFailureHandler != null) {
						d.OnDownloadFailed += fileDowloadFailureHandler;
					}
					if (fileDowloadSuccessHandler != null) {
						d.OnDownloadFinished += fileDowloadSuccessHandler;
					}
				}
			}

			var assoc = BlrtDownloaderAssociation.GetAssociationsAsList (obj); ;

			if (fileAssocStatusHandler != null) {
				assoc.OnFailed += fileAssocStatusHandler;
				assoc.OnFinished += fileAssocStatusHandler;
			}

			return assoc;
		}

		/// <summary>
		/// Uploads the item asynchronously. Waits for uploading to finish.
		/// </summary>
		/// <returns>Completed task</returns>
		/// <param name="item">Item.</param>
		/// <param name="onProgress">Uploading progress handler</param>
		/// <param name="onFinish">Uploading finish handler (true means success)</param>
		internal static async Task UploadItemAsync (BlrtBase item, EventHandler onProgress = null, EventHandler<bool> onFinish = null)
		{
			await BlrtManager.Upload.RunAsync (GetItemUploader (item, onProgress, onFinish));
		}

		static BlrtUploader GetItemUploader (BlrtBase item, EventHandler onProgress = null, EventHandler<bool> onFinish = null)
		{
			item.ShouldUpload = true;
			var uploader = BlrtManager.Upload.GetUploader (item);
			uploader.OnFinish += onFinish;
			uploader.ProgressChanged += onProgress;
			return uploader;
		}

		/// <summary>
		/// Calls the requested API.
		/// </summary>
		/// <returns>The requested API.</returns>
		/// <param name="executor">Executor.</param>
		/// <param name="apiRequestId">API request identifier.</param>
		internal static async Task<APIRequestDetailsResponse> CallRequestedAPI (IExecutor executor, string apiRequestId)
		{
			//we will not await the permission to make it faster
			BlrtData.BlrtManager.ApplyPermission (
				new BlrtData.UserPermissionHandler (),
				false
			).FireAndForget ();

			if (!BlrtPermissions.CanCreateConversation) {
				//TODO make an alert here
				return null;
			}

			var id = apiRequestId;

			var api = new APIRequestDetails (new APIRequestDetails.Parameters () {
				RequestId = id
			});

			BlrtManager.Responder.PushLoading (true);

			while (!await api.Run (CreateTokenWithTimeout (15000))) {

				BlrtManager.Responder.PopLoading (true);

				var alert = await BlrtData.BlrtManager.App.ShowAlert (executor,
					new SimpleAlert (
						PlatformHelper.Translate ("request_get_failed_title", ""),
						PlatformHelper.Translate ("request_get_failed_message", ""),
						PlatformHelper.Translate ("request_get_failed_cancel", ""),
						PlatformHelper.Translate ("request_get_failed_retry", "")
					)
				);

				if (!alert.Success || alert.Result.Cancelled)
					return null;


				BlrtManager.Responder.PushLoading (true);
			}

			BlrtManager.Responder.PopLoading (true);

			if (api.Response.Success) {
				return api.Response;

			} else {

				var alert = await BlrtData.BlrtManager.App.ShowAlert (executor,
					new SimpleAlert (
						PlatformHelper.Translate ("request_get_permissions_title", ""),
						PlatformHelper.Translate ("request_get_permissions_message", ""),
						PlatformHelper.Translate ("request_get_permissions_cancel", ""),
						PlatformHelper.Translate ("request_get_permissions_support", "")
					)
				);

				if (!alert.Success || alert.Result.Cancelled)
					return null;

				await BlrtData.BlrtManager.App.OpenSendSupportMsg (executor, "Failed to open RequestId: " + id);

				return null;
			}
		}

		/// <summary>
		/// Updates the conversation bond count.
		/// </summary>
		/// <param name="conversation">Conversation.</param>
		/// <param name="count">Value by which to increase or decrease count. It can either be positive or negative</param>
		internal static void UpdateConversationBondCount (Conversation conversation, int count)
		{
			if (conversation != null) {
				conversation.BondCount += count;
				conversation.NeedsRefresh = conversation.NeedsMetaUpdate = true;
				conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
			}
		}

#if __IOS__
		internal static async Task<BlrtUploader> UploadItem (BlrtBase item, bool isEmptyConversation = false)
		{
			var uploader = BlrtManager.Upload.GetUploader (item, isEmptyConversation);
			item.ShouldUpload = true;
			await BlrtManager.Upload.RunAsync (uploader);
			return uploader;
		}

		/// <summary>
		/// Saves the conversation name to cloud.
		/// </summary>
		/// <returns>The conversation name on cloud.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="newName">New name.</param>
		static async Task<APIConversationRenameRequestStatus> SaveConversationNameToCloud (string conversationId, string newName)
		{
			var parameters = new APIConversationRename.Parameters {
				ConversationId = conversationId,
				ConversationName = newName
			};
			var apiCall = new APIConversationRename (parameters);
			var cancelled = false;
			using (var token = new CancellationTokenSource ())
			using (UserDialogs.Instance.Loading ("Saving",
												 delegate {
													 token.Cancel ();
													 cancelled = true;
												 }, "Cancel")) {
				if (await apiCall.Run (CreateTokenWithTimeout (token.Token, 5000))) {
					return apiCall.Response.Success ? APIConversationRenameRequestStatus.Success : APIConversationRenameRequestStatus.Failed;
				}
				return cancelled ? APIConversationRenameRequestStatus.Cancelled : APIConversationRenameRequestStatus.Failed;
			}

		}

		/// <summary>
		/// Generates the convo shareable link.
		/// </summary>
		/// <returns>The link.</returns>
		/// <param name="conversationId">Conversation identifier.</param>
		/// <param name="linkType">Link type.</param>
		/// <param name="existingLink">Existing link.</param>
		internal static async Task<string> GenerateLink (string conversationId, string linkType, string existingLink)
		{
			using (UserDialogs.Instance.Loading ("Generating link...")) {
				var isExistingLinkInvalid = string.IsNullOrEmpty (existingLink) ||
								!existingLink.StartsWith (BlrtSettings.YozioDictionary ["click_url"], StringComparison.Ordinal);

				if (isExistingLinkInvalid) {
					switch (linkType) {
					case "convo_addphone":
						return await GenerateYozioUrl (conversationId, "convo_sms", new Dictionary<string, string> { { "sendToPhone", "true" } });
					case "convo_sms":
					case "convo_facebook":
					case "convo_email":
					case "convo_other":
						return await GenerateYozioUrl (conversationId, linkType);
					}
				}
				return existingLink;
			}
		}

		/// <summary>
		/// Tries to create user relation and upload it.
		/// </summary>
		/// <returns>The create uer relation.</returns>
		public static async Task<APIConversationUserRelationCreateResponse> TryCreateUserRelation (string conversationId,
																						   string noteText,
																						   bool addNoteToConvo,
																						   IEnumerable<ConversationNonExistingContact> contacts)
		{
			var param = new APIConversationUserRelationCreate.Parameters {
				ConversationID = conversationId,
				Note = noteText,
				DontAddNoteInConvo = !addNoteToConvo,
				Relations = (from c in contacts
							 select new APIConversationUserRelationCreate.Relation (
								 c.SelectedInfo.ParseContactType,
								 c.SelectedInfo.ContactValue,
								 c.Contact.UsableName
							 )).ToArray ()
			};

			var request = new APIConversationUserRelationCreate (param);

			if (!await request.Run (new CancellationTokenSource (15000).Token)) {
				throw request.Exception ?? new Exception ("Request failed :(");
			}
			if (!request.Response.Success) {
				throw request.Exception ?? new Exception (request.Response.Error.Code.ToString ());
			}

			return request.Response;
		}

		/// <summary>
		/// Fetchs the new objects from parse.
		/// </summary>
		/// <returns>The new objects from parse.</returns>
		/// <param name="result">Result.</param>
		internal static async Task<IEnumerable<Parse.ParseObject>> FetchNewObjectsFromParse (
													APIConversationUserRelationCreateResponse userRelationCreateResponse)
		{
			//BLRT-1263: one email address will need to fetch 4 objects, so may be we need more time on this task
			if (userRelationCreateResponse?.Objects != null) {
				return await FetchAllAsync (userRelationCreateResponse.Objects, new CancellationTokenSource (5000).Token);
			}
			return Enumerable.Empty<Parse.ParseObject> ();
		}
#endif
#if __ANDROID__
		/// <summary>
		/// Uploads the item. Queues the item for uploading and returns immediately.
		/// </summary>
		/// <param name="item">Item.</param>
		/// <param name="onProgress">Uploading progress handler</param>
		/// <param name="onFinish">Uploading finish handler (true means success)</param>
		internal static void UploadItem (BlrtBase item, EventHandler onProgress = null, EventHandler<bool> onFinish = null)
		{
			BlrtManager.Upload.RunAsync (GetItemUploader (item, onProgress, onFinish)).FireAndForget ();
		}
#endif
	}
}

