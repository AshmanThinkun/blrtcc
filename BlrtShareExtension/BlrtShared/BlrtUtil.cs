using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Parse;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using BlrtData.ExecutionPipeline;
using BlrtData.Models.ConversationScreen;
using Acr.UserDialogs;
using BlrtCanvas;
using BlrtData.IO;

#if __IOS__
using BlrtData.Contacts;
using System.Web;
#endif

using BlrtCanvas.Pages;
using BlrtAPI;

namespace BlrtData
{
	/// <summary>
	/// This class contains a bunch of useful static function
	/// </summary>
	public static partial class BlrtUtil
	{
		// 0.0: worst quality, 1.0: best quality
		public static readonly float JPEG_COMPRESSION_QUALITY = 0.4f;

		static IParsePlatformHelper _parseHelper;
		static readonly string NEW_CONVERSATION_DEFAULT_NAME_DATE_FORMAT = "dd MMMM yyyy";
		static readonly string NEW_CONVERSATION_DEFAULT_NAME_PREFIX = "Blrt";

		public static readonly string SHORT_DATE_FORMAT = "ddd, d MMM";
		public static readonly string LONG_DATE_FORMAT = "ddd, d MMM yyyy";

		public static readonly string TODAY_STRING = "Today";
		public static readonly string YESTERDAY_STRING = "Yesterday";


#if __IOS__
		public static readonly string TIME_FORMAT = "h:mm tt";
		public static readonly string DELETED_ACCOUNT_AVATAR_PATH = "deleted_account_avatar.png";
#endif

		public static string NewConversationDefaultName {
			get {
				return string.Format ("{0} {1}",
									  NEW_CONVERSATION_DEFAULT_NAME_PREFIX,
									  DateTime.Now.ToString (NEW_CONVERSATION_DEFAULT_NAME_DATE_FORMAT));
			}
		}

		/// <summary>
		/// Gets the date string.
		/// </summary>
		/// <returns>The date string.</returns>
		/// <param name="date">Date.</param>
		/// <param name="dateFormat">Date format. If none is provided default format is used.</param>
		public static string GetDateString (DateTime date, string dateFormat = "")
		{
			var today = DateTime.Now.Date;

			if (today.CompareTo (date.Date) == 0) { return TODAY_STRING; }
			if (today.AddDays (-1) == date.Date) { return YESTERDAY_STRING; }

			if (string.IsNullOrWhiteSpace (dateFormat)) {
				dateFormat = SHORT_DATE_FORMAT;
				if (today.Year != date.Year) {
					dateFormat = LONG_DATE_FORMAT;
				}
			}
			return string.Format (date.ToString (dateFormat, System.Globalization.CultureInfo.InvariantCulture));
		}

		public static List<ICanvasPage> GetPDFCanvasPages (MediaAsset asset)
		{
			var pdfPath = asset.DecyptedPath;
			var pageCount = CanvasPageView.PlatformHelper.PDFPageCount (pdfPath);
			var returnList = new List<ICanvasPage> (pageCount);
			for (int i = 0; i < pageCount; i++) {
				returnList.Add (new BlrtPDFPage (pdfPath, i + 1, asset.Name));
			}
			return returnList;
		}

		public static bool TryMarkingConversationItemAsRead (ConversationItem item)
		{
			if (item == null) {
				return false;
			}

			return item.TryMarkRead ();
		}


#if __ANDROID__
		public static async Task<object> DecryptAudioItem (BlrtData.Contents.ConversationAudioItem audio)
		{
			try {
				if (!audio.IsAudioDecrypted) {
					using (UserDialogs.Instance.Loading ("Loading...")) {
						try {
							await audio.DecyptAudioData ();
						} catch (BlrtEncryptorManagerException e) {
							audio.DeleteAudioData ();
							return e;
						} catch (InvalidOperationException e1) {
							audio.DeleteAudioData ();
							return e1;
						}
						return audio.DecryptedAudioPath;
					}
				}
				return audio.DecryptedAudioPath;
			} catch (Exception e2) {
				return e2;
			}
		}

		public static bool SaveConversationName (Conversation _conversation, string newName)
		{
			if (!CanvasPageView.PlatformHelper.TryValidateConversationName (newName.Trim ())) {
				return false;
			}

			//save
			if (_conversation != null &&
				!string.IsNullOrEmpty (newName) &&
				_conversation.Name != newName.Trim ()) {
				_conversation.Dirty = true;
				_conversation.Name = newName.Trim ();
				_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
			}
			return true;
		}

		internal static ConversationsOptionsUserItem CreateConversationsOptionsUserItem (ConversationUserRelation userRelation)
		{
			var displayItem = new ConversationsOptionsUserItem ();

			var type = ConversationRelationStatus.GetTypeOfRelation (userRelation);
			switch (type) {
			case ConversationRelationStatus.RoleEnum.Creator:
				displayItem.RelationType = "Creator";
				displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
				break;
			case ConversationRelationStatus.RoleEnum.Contributor:
				displayItem.RelationType = "Contributor";
				displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
				break;
			case ConversationRelationStatus.RoleEnum.Viewed:
				displayItem.RelationType = "Viewed";
				displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
				break;
			case ConversationRelationStatus.RoleEnum.NotViewed:
				displayItem.RelationType = "Not viewed";
				displayItem.InitialBackgroundColor = BlrtStyles.BlrtGreen;
				break;
			case ConversationRelationStatus.RoleEnum.NotSignedUp:
				displayItem.RelationType = "Not signed up";
				displayItem.InitialBackgroundColor = BlrtStyles.BlrtGray4;
				break;
			}

			displayItem.DisplayName = GetUserDisplayName (userRelation);
			displayItem.Initials = GetInitials (displayItem.DisplayName);

			return displayItem;
		}
#endif

		/// <summary>
		/// This method is suppose to find whether media has been used to create blrt.
		/// </summary>
		/// <returns>The media used for blrt.</returns>
		/// <param name="objectId">Object identifier.</param>
		/// <param name="unusedConversationMediaItems">Unused conversation media items.</param>
		internal static bool IsMediaUsedForBlrt (string objectId,
												 HashSet<string> unusedConversationMediaItemIds,
												 ConversationHandler conversationHandler)
		{
			int count = 0;
			bool mediaAlreadyUsed = true;

			if (unusedConversationMediaItemIds != null) {
				count = unusedConversationMediaItemIds.Count;
			}

			if (count > 0) {
				if (unusedConversationMediaItemIds.Contains (objectId) ||
					IdsBelongToSameMediaAsset (conversationHandler.LocalStorage.DataManager.GetMediaAsset (objectId),
											  unusedConversationMediaItemIds)
				   ) {
					mediaAlreadyUsed = false;
				}
			}

			return (count == 0 || mediaAlreadyUsed);
		}

		/// <summary>
		/// This method is suppose to find whether the Ids provided are for same media asset. Every media asset has 
		/// parseId (or objectId) and localGUID.
		/// </summary>
		/// <returns><c>true</c>, if belong to same media asset was identified, <c>false</c> otherwise.</returns>
		/// <param name="dataManager">Data manager.</param>
		/// <param name="unusedMediaAssetId">Unused media asset identifier.</param>
		/// <param name="objectId">Object identifier.</param>
		static bool IdsBelongToSameMediaAsset (MediaAsset asset, HashSet<string> unusedConversationMediaItemIds)
		{
			return (unusedConversationMediaItemIds.Contains (asset.ObjectId) ||
					unusedConversationMediaItemIds.Contains (asset.LocalGuid));
		}

		public static BlrtData.Conversation CreateConversation (string conversationName)
		{
			BlrtData.BlrtManager.ApplyPermission (
					new BlrtData.UserPermissionHandler (),
					false
				).FireAndForget ();

			var conversation = new BlrtData.Conversation {
				Name = conversationName
			};

#if __ANDROID__
			//conversation.LocalThumbnailName = string.Format ("{0}-thumb.jpg", conversation.ObjectId);
			conversation.ContentUpdatedAt = DateTime.Now;
			conversation.ShouldUpload = true;
#endif

			conversation.LastMediaArgument = "{}";
			conversation.LocalStorage.DataManager.AddUnsynced (conversation);

			return conversation;
		}

		public static async Task OpenConversation (string conversationId)
		{
			var executor = new Executor (ExecutionSource.System);
			var mailbox = await BlrtManager.App.OpenMailbox (executor, BlrtData.Models.Mailbox.MailboxType.Inbox);

			if (!mailbox.Success) {
				return;
			}

			await mailbox.Result.OpenConversation (executor, conversationId, false, true);
		}

		public static IParsePlatformHelper ParsePlatformHelper {
			get {
				if (null == _parseHelper) {
					_parseHelper = Xamarin.Forms.DependencyService.Get<IParsePlatformHelper> ();
				}
				return _parseHelper;
			}
		}

		private static IPlatformHelper _helper;

		public static IPlatformHelper PlatformHelper {
			get {
				if (null == _helper) {
					_helper = Xamarin.Forms.DependencyService.Get<IPlatformHelper> ();
				}
				return _helper;
			}
		}

		public static int [] GetRangeArray (int min, int max)
		{

			if (min > max)
				return new int [0];

			//count max, you need to increase by 1
			int [] range = new int [max - min + 1];

			for (int i = 0; i < range.Length; ++i)
				range [i] = i + min;

			return range;
		}

		public static T [] Concat<T> (params T [] [] array)
		{
			return Concat (array as IEnumerable<T []>);
		}

		public static T [] Concat<T> (IEnumerable<T []> array)
		{

			int length = 0;

			foreach (var t in array) {
				if (t != null)
					length += t.Length;
			}

			var output = new T [length];
			length = 0;

			foreach (var t in array) {
				if (t != null) {
					t.CopyTo (output, length);
					length += t.Length;
				}
			}

			return output;
		}


		public static string GenerateUrl (string conversationId)
		{
			return string.Format ("{0}conv/{1}",
				BlrtSettings.ParseWebsite,
				BlrtEncode.EncodeId (conversationId)
			);
		}

		public static async Task<string> GenerateYozioUrl (string conversationId, string urlType, Dictionary<string, string> extraParams = null)
		{
			string url = "";
			var dic = new Dictionary<string, string> ();
			dic.Add ("convo", BlrtEncode.EncodeId (conversationId));
			if (extraParams != null) {
				foreach (var kv in extraParams) {
					dic [kv.Key] = kv.Value;
				}
			}
			url = BlrtUtil.GenerateUrl (conversationId);

			return url;
		}

		public static async Task<string> GenerateYozioUrl (ConversationItem content, string urlType)
		{
			string url = "";
			var dic = new Dictionary<string, string> ();
			dic.Add (URLConstants.Paths.BLRT, BlrtEncode.EncodeId (content.ObjectId));
			if (!content.IsPublicBlrt) {
				dic.Add ("convo", BlrtEncode.EncodeId (content.ConversationId));
				dic.Add ("type", "private");
			} else {
				dic.Add ("type", "public");
			}


#if __ANDROID__
			url = GenerateUrl (content);
#endif
#if __IOS__
			url = await BlrtData.Helpers.YozioHelper.CreateSublink (BlrtSettings.YozioDictionary [urlType], dic);
			if (string.IsNullOrEmpty (url)) {
				url = BlrtUtil.GenerateUrl (content);
			}
#endif
			BlrtData.Helpers.BlrtTrack.AccountCreate ("");
			return url;
		}

		public static async Task<string> GenerateYozioUrl (OCList content, string urlType)
		{
			string url = "";
			var dic = new Dictionary<string, string> ();
			dic.Add ("oclist", BlrtEncode.EncodeId (content.ObjectId));


#if __ANDROID__
			url = BlrtUtil.GenerateUrl (content);
#endif
#if __IOS__
			url = await BlrtData.Helpers.YozioHelper.CreateSublink (BlrtSettings.YozioDictionary [urlType], dic);
			if (string.IsNullOrEmpty (url)) {
				url = BlrtUtil.GenerateUrl (content);
			}

#endif

			return url;
		}

		public static async Task<string> GenerateYozioShareUrl (string urlType, bool longlink = false)
		{
			string url = "";
			var dic = new Dictionary<string, string> ();


#if __ANDROID__
			string param = "";
			param += "platform=" + BlrtUtil.PlatformHelper.GetPlatformName ();
			param += "&utm_source=" + urlType.Replace ("share_", "");
			param += "&utm_medium=app";
			if (ParseUser.CurrentUser != null)
				param += "&inviter=" + BlrtEncode.EncodeId (ParseUser.CurrentUser.ObjectId);

			url = String.Format (BlrtSettings.ShareUrl, param);
#endif
#if __IOS__
			if (!longlink)
				url = await BlrtData.Helpers.YozioHelper.CreateSublink (BlrtSettings.YozioDictionary [urlType], dic);
			if (string.IsNullOrEmpty (url) || longlink) {
				string param = "";
				param += "platform=" + BlrtUtil.PlatformHelper.GetPlatformName ();
				param += "&utm_source=" + urlType.Replace ("share_", "");
				param += "&utm_medium=app";
				if (ParseUser.CurrentUser != null)
					param += "&inviter=" + BlrtEncode.EncodeId (ParseUser.CurrentUser.ObjectId);
				if (!string.IsNullOrEmpty (BlrtSettings.YozioDictionary ["click_url"]))
					url = BlrtSettings.YozioDictionary ["click_url"] + BlrtSettings.YozioDictionary [urlType] + "?" + param;
				else
					url = String.Format (BlrtSettings.ShareUrl, param);
			}

#endif

			return url;
		}

		static string GenerateUrl (ConversationItem content)
		{
			if (!content.IsPublicBlrt) {
				return string.Format ("{0}conv/{1}/blrt/{2}",
					BlrtSettings.ParseWebsite,
					BlrtEncode.EncodeId (content.ConversationId),
					BlrtEncode.EncodeId (content.ObjectId)
				);
			} else {
				return string.Format ("{0}/blrt/{1}",
					BlrtConstants.EmbedUrl,
					BlrtEncode.EncodeId (content.ObjectId)
				);
			}
		}

		static string GenerateUrl (OCList content)
		{
			return string.Format ("{0}/oclist/{1}?auto_subscribe=true",
				BlrtConstants.EmbedUrl,
				BlrtEncode.EncodeId (content.ObjectId)
			);
		}

		public static async Task<bool> TrackImpression (string urlType)
		{
			var url = BlrtSettings.YozioDictionary ["impression_url"] + BlrtSettings.YozioDictionary [urlType];
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (url);
			request.UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0";
			try {
				using (HttpWebResponse response = await request.GetResponseAsync () as HttpWebResponse) {
					if (response.StatusCode != HttpStatusCode.OK) {
						return false;
					} else {
						var stream = new System.IO.StreamReader (response.GetResponseStream ());
						var responseText = stream.ReadToEnd ();
					}
				}
			} catch (Exception e) {
				return false;
			}
			return true;
		}

		public static string GetImpressionHtml (string urlType)
		{
			var html = string.Empty;
#if __IOS__
			var url = BlrtSettings.YozioDictionary ["impression_url"] + BlrtSettings.YozioDictionary [urlType];
			html = string.Format ("<img src='{0}' width='1px' style='display:none'>", url);
#endif
			return html;
		}


		/// <summary>
		/// Lightly encrypts/decrypts a string.  This is for security reasons.
		/// </summary>
		/// <returns>The string.</returns>
		/// <param name="s">The string that will be encryped, or decryped</param>
		/// <param name="version">The version of this method used to create it.</param>
		public static string XorString (string s, string token)
		{
			char [] c = s.ToCharArray ();
			char [] t = token.ToCharArray ();

			for (int i = 0; i < s.Length; i++) {
				int v1 = c [i];
				int v2 = t [(i % t.Length)];

				int output = v1 ^ v2 ^ (i % 128);

				c [i] = (char)(output);
			}

			return new string (c);
		}


		public static bool IsValidEmail (string email)
		{
			if (string.IsNullOrWhiteSpace (email))
				return false;
			// Return true if strIn is in valid e-mail format.
			return Regex.IsMatch (email,
				@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
				RegexOptions.IgnoreCase
			);

			//version 1.3
			//			return Regex.IsMatch (email, 
			//				@"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
			//				@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z][a-zA-Z]+))$"
			//			); 

			/*original, removed ' support
			 *@"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + 
			 *@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z][a-zA-Z]+))$"); 
			 */
		}

		public static bool IsValidPhoneNumber (string number, string countryCode = null)
		{
			var util = PhoneNumbers.PhoneNumberUtil.GetInstance ();
			if (countryCode == null)
				countryCode = PlatformHelper.CountryCode;
			try {
				var phoneNumber = util.Parse (number, countryCode);
				return util.IsValidNumber (phoneNumber);
			} catch {
				return false;
			}
		}

		public static Tuple<string, string> ToPhoneNumberE164Tuple (string number, string countryCode = null)
		{
			var util = PhoneNumbers.PhoneNumberUtil.GetInstance ();
			if (countryCode == null)
				countryCode = PlatformHelper.CountryCode;
			try {
				var phoneNumber = util.Parse (number, countryCode);
				if (!util.IsValidNumber (phoneNumber))
					throw new Exception ("invalid phone number");
				return new Tuple<string, string> (string.Format("+{0}", phoneNumber.CountryCode), phoneNumber.NationalNumber.ToString());
				//return util.Format (phoneNumber, PhoneNumbers.PhoneNumberFormat.E164);
			} catch {
				return new Tuple<string, string>(string.Empty, string.Empty);
			}
		}

		public static string ToPhoneNumberE164 (string number, string countryCode = null)
		{
			var util = PhoneNumbers.PhoneNumberUtil.GetInstance ();
			if (countryCode == null)
				countryCode = PlatformHelper.CountryCode;
			try {
				var phoneNumber = util.Parse (number, countryCode);
				if (!util.IsValidNumber (phoneNumber))
					throw new Exception ("invalid phone number");
				return util.Format (phoneNumber, PhoneNumbers.PhoneNumberFormat.E164);
			} catch {
				return "";
			}
		}

		public static string GetPhoneCountryPrefix (string countryCode)
		{
			var util = PhoneNumbers.PhoneNumberUtil.GetInstance ();
			var num = util.GetCountryCodeForRegion (countryCode);
			return "+" + num.ToString ();
		}

		public static bool CanUseFile (string filename, bool allowEmpty = true)
		{
			if (string.IsNullOrWhiteSpace (filename) || !File.Exists (filename))
				return false;


			try {
				using (var stream = new FileStream (filename, FileMode.Open, FileAccess.Read)) {
					// File/Stream manipulating code here
					if (!allowEmpty) {
						return stream.Length > 0;
					}
					stream.Close ();
				}
			} catch (Exception e) {
				return false;
			}
			return true;
		}

		public static void DeleteDecryptedFiles ()
		{
			ClearDirectory (new DirectoryInfo (BlrtConstants.Directories.User.GetDecryptedPath ("")));
			ClearDirectory (new DirectoryInfo (BlrtConstants.Directories.IndependentPublic.GetDecryptedPath ("")));
			ClearDirectory (new DirectoryInfo (BlrtConstants.Directories.IndependentPrivate.GetDecryptedPath ("")));
		}

		public static void ClearDirectory (DirectoryInfo dir)
		{
			foreach (FileInfo file in dir.GetFiles ()) {
				try {
					file.Delete ();
				} catch {
				}
			}
		}

		public static Stream SerializeToStream (object o)
		{
			MemoryStream stream = new MemoryStream ();
			IFormatter formatter = new BinaryFormatter ();
			formatter.Serialize (stream, o);
			return stream;
		}

		public static T DeserializeFromStream<T> (Stream stream) where T : class
		{
			BinaryFormatter formatter = new BinaryFormatter ();
			stream.Seek (0, SeekOrigin.Begin);
			T o = null;

			try {
				o = (T)formatter.Deserialize (stream);
			} catch (Exception ess) {
				throw ess;
			}
			if (o == null)
				throw new SerializationException ("Deserialisation result is null");

			return o;
		}

		public static void StreamToFile (string file, Stream input)
		{
			using (var fs = File.Create (file)) {
				if (input.CanSeek)
					input.Position = fs.Position = 0;
				else
					fs.Position = 0;

				byte [] buffer = new byte [256 * 32];
				int bytesRead;

				while ((bytesRead = input.Read (buffer, 0, buffer.Length)) > 0) {
					fs.Write (buffer, 0, bytesRead);
				}

				if (input.CanSeek)
					input.Position = fs.Position = 0;
				else
					fs.Position = 0;

				fs.Close ();
			}
		}

		public static async Task StreamToFileAsync (string file, Stream input)
		{
			using (var fs = File.Create (file)) {
				if (input.CanSeek)
					input.Position = fs.Position = 0;
				else
					fs.Position = 0;

				byte [] buffer = new byte [256 * 32];
				int bytesRead;

				while ((bytesRead = await input.ReadAsync (buffer, 0, buffer.Length)) > 0) {
					await fs.WriteAsync (buffer, 0, bytesRead);
				}

				if (input.CanSeek)
					input.Position = fs.Position = 0;
				else
					fs.Position = 0;

				fs.Close ();
			}
		}


		public static string ParseUrl (string url)
		{
			url = url.Trim ();

			url = url.Replace (" ", "%20");

			if (!url.Contains ("://"))
				url = "http://" + url;

			return url;
		}

		public static string Zip (string text)
		{
			byte [] buffer = System.Text.Encoding.Unicode.GetBytes (text);

			buffer = Zip (buffer);

			return Convert.ToBase64String (buffer);
		}


		public static async Task<T> AwaitOrCancelTask<T> (Task<T> task, CancellationToken token)
		{

			while (!token.IsCancellationRequested
				   && !(task.IsCanceled || task.IsFaulted || task.IsCompleted)) {
				await Task.Delay (100);
			}

			if (task.Exception != null) {

				if (task.Exception is AggregateException) {
					var e = task.Exception as AggregateException;
					if (e.InnerExceptions.Count == 1)
						throw e.InnerExceptions [0];
				}

				throw task.Exception;
			}
			token.ThrowIfCancellationRequested ();

			return task.Result;
		}

		public static async Task AwaitOrCancelTask (Task task, CancellationToken token)
		{

			while (!token.IsCancellationRequested
				   && !(task.IsCanceled || task.IsFaulted || task.IsCompleted)) {
				await Task.Delay (100);
			}

			if (task.Exception != null) {

				if (task.Exception is AggregateException) {
					var e = task.Exception as AggregateException;
					if (e.InnerExceptions.Count == 1)
						throw e.InnerExceptions [0];
				}

				throw task.Exception;
			}
			if (token.IsCancellationRequested)
				throw new TaskCanceledException ();
		}

		public static float SmoothClamp (float value, float max)
		{

			// y = x / (1 + |x / 100|)

			return value / (1 + Math.Abs (value / max));
		}

		public static string RemoveControlCharaters (this string input)
		{

			StringBuilder newString = new StringBuilder ();
			char ch;

			for (int i = 0; i < input.Length; i++) {

				ch = input [i];

				if (!char.IsControl (ch)) {
					newString.Append (ch);
				}
			}
			return newString.ToString ();
		}

		public static bool IsConnectionIssueException (Exception ex)
		{

			if (ex == null)
				return false;

			if (ex is ParseException) {
				var parseException = ex as ParseException;

				switch (parseException.Code) {
				case ParseException.ErrorCode.ConnectionFailed:
				case ParseException.ErrorCode.ExceededQuota:
				case ParseException.ErrorCode.InternalServerError:
				case ParseException.ErrorCode.NotInitialized:
				case ParseException.ErrorCode.OtherCause:
				case ParseException.ErrorCode.SessionMissing:
				case ParseException.ErrorCode.Timeout:
				case ParseException.ErrorCode.UnsupportedService:
					return true;
				}
			}

			if (ex is WebException) {
				return true;
			}

			if (ex is TaskCanceledException) {
				return true;
			}

			return IsConnectionIssueException (ex.InnerException);
		}




		public static bool BetterEquals (object val1, object val2)
		{
			if (val1 == val2)
				return true;
			return val1 != null && val1.Equals (val2);
		}

		public static string FileHexMd5 (string path)
		{
			using (var md5 = MD5.Create ()) {
				using (var stream = File.OpenRead (path)) {
					Regex rgx = new Regex ("[^0-9,a-f]");
					var result = rgx.Replace (BitConverter.ToString (md5.ComputeHash (stream)).ToLower (), "");
					stream.Close ();
					return result;
				}
			}
		}

		public static byte [] Zip (byte [] data)
		{

			MemoryStream ms = new MemoryStream ();
			using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream (ms, System.IO.Compression.CompressionMode.Compress, true)) {
				zip.Write (data, 0, data.Length);
			}

			ms.Position = 0;
			//MemoryStream outStream = new MemoryStream();

			byte [] compressed = new byte [ms.Length];
			ms.Read (compressed, 0, compressed.Length);

			byte [] gzBuffer = new byte [compressed.Length + 4];
			System.Buffer.BlockCopy (compressed, 0, gzBuffer, 4, compressed.Length);
			System.Buffer.BlockCopy (BitConverter.GetBytes (data.Length), 0, gzBuffer, 0, 4);
			return gzBuffer;
		}

		public static string UnZip (string compressedText)
		{
			byte [] gzBuffer = Convert.FromBase64String (compressedText);
			using (MemoryStream ms = new MemoryStream ()) {
				int msgLength = BitConverter.ToInt32 (gzBuffer, 0);
				ms.Write (gzBuffer, 4, gzBuffer.Length - 4);

				byte [] buffer = new byte [msgLength];

				ms.Position = 0;
				using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream (ms, System.IO.Compression.CompressionMode.Decompress)) {
					zip.Read (buffer, 0, buffer.Length);
				}

				return System.Text.Encoding.Unicode.GetString (buffer, 0, buffer.Length);
			}
		}


		public static List<int> PagesToList (string strPage)
		{
			List<int> rtn = new List<int> ();
			var page = strPage.Trim ();
			foreach (string s in page.Split (',')) {
				string str = s.Replace (" ", "");

				// try and get the number
				int num;
				if (int.TryParse (str, out num)) {
					if (!rtn.Contains (num))
						rtn.Add (num);
					continue; // skip the rest
				}

				// otherwise we might have a range
				// split on the range delimiter
				string [] subs = str.Split ('-');
				int start, end;

				// now see if we can parse a start and end
				if (subs.Length > 1 &&
				   int.TryParse (subs [0], out start) &&
				   int.TryParse (subs [1], out end) &&
				   end >= start) {
					for (int i = start; i < end + 1; ++i) {
						if (!rtn.Contains (i))
							rtn.Add (i);
					}
				}
			}

			return rtn;
		}

		public static string ListToPages (IEnumerable<int> numbers)
		{
			if (!numbers.Any ())
				return "";

			int startNumber = 0;
			int endNumber = 0;
			bool inRange = false;
			string rtn = "";


			foreach (int number in numbers.OrderBy (arg => arg)) {
				if (!inRange) {
					inRange = true;
					startNumber = number;
					endNumber = number;
				} else if (number == endNumber)
					continue;
				else if (number == endNumber + 1)
					endNumber++;
				else if (startNumber != endNumber) {
					rtn += startNumber.ToString () + "-" + endNumber.ToString () + ",";
					startNumber = number;
					endNumber = number;
				} else {
					rtn += startNumber.ToString () + ",";
					startNumber = number;
					endNumber = number;
				}
			}

			if (startNumber != endNumber) {
				rtn += startNumber.ToString () + "-" + endNumber.ToString ();
			} else {
				rtn += startNumber.ToString ();
			}

			return rtn;
		}

		public static DateTime ToLocalTime (DateTime time)
		{
			return time + DateTimeOffset.Now.Offset;
		}

		public static DateTime ToCloudTime (DateTime time)
		{
			return time - DateTimeOffset.Now.Offset;
		}


		public static string ToMinutes (float seconds)
		{
			return ((int)(seconds / 60)) + ":" + ((int)(seconds % 60)).ToString ("D2");
		}



		public static Dictionary<string, string> GetParameters (string absoluteString)
		{
			absoluteString = System.Web.HttpUtility.UrlDecode (absoluteString);

			var output = new Dictionary<string, string> ();


			if (absoluteString.Contains ("?")) {
				var args = absoluteString.Remove (0, absoluteString.IndexOf ("?") + 1);

				foreach (var str in args.Split ('&')) {

					var split = str.Split ("=".ToCharArray (), 2);

					if ((split.Length > 0) && !output.ContainsKey (split [0])) {
						if (split.Length > 1)
							output.Add (split [0], split [1]);
						else
							output.Add (split [0], split [0]);
					}
				}
			}


			return output;
		}

		public static string StringOrNull (object obj)
		{
			if (obj == null)
				return null;

			return obj.ToString ();
		}

		public static Dictionary<string, object> BetterJsonToDictionary (string json)
		{
			Dictionary<string, object> dic = null;
			try {
				dic = JsonConvert.DeserializeObject<Dictionary<string, object>> (json);
			} catch (Exception ex) {
				return null;
			}

			if (null != dic) {
				ParseJsonDic (dic);
			}
			return dic;
		}

		static void ParseJsonDic (Dictionary<string, object> dic)
		{
			if (null == dic || 0 == dic.Count) {
				return;
			}

			var keys = new string [dic.Keys.Count];
			dic.Keys.CopyTo (keys, 0);
			foreach (var key in keys) {
				object ov = dic [key];
				ParseJsonDicValue (ref ov);
				dic [key] = ov;
			}
		}

		static void ParseJsonDicValue (ref object value)
		{
			if (value is JContainer) {
				var jcon = value as JContainer;
				switch (jcon.Type) {
				case JTokenType.Object:
					value = jcon.ToObject<Dictionary<string, object>> ();
					break;
				case JTokenType.Array:
					value = jcon.ToObject<object []> ();
					break;
				default:
					break;
				}
			}
			if (value is Dictionary<string, object>) {
				ParseJsonDic (value as Dictionary<string, object>);
			} else if (value is object []) {
				var arr = value as object [];
				for (int i = 0; i < arr.Length; i++) {
					ParseJsonDicValue (ref arr [i]);
				}
			}
		}

		public static string FileUrlFileName (string url)
		{
			if (string.IsNullOrWhiteSpace (url)) {
				return null;
			}
			var index = url.LastIndexOfAny (new char [] { '/', '\\' });
			if (index >= 0 && index < url.Length - 1) {
				var extension = url.Substring (index + 1);
				if (extension.Length > 0) {
					return extension;
				}
			}
			return null;
		}

		public static string GetInitials (string displayName)
		{
			var s = displayName.Trim ().Split (' ');

			if (s.Length == 0 || s [0].Length == 0)
				return "";

			if (s.Length == 1)
				return s [0] [0] + "";

			return (s [0] [0] + "" + s [s.Length - 1] [0]).ToUpper ();
		}

		public enum WordSize
		{
			Unknown,
			Bit_32,
			Bit_64
		}

		public const WordSize DefaultWordSize = WordSize.Unknown;

		public static WordSize DeviceWordSize {
			get {
#if __IOS__
				return IntPtr.Size == 4 ? WordSize.Bit_32 : WordSize.Bit_64;
#else
				return WordSize.Bit_32;
#endif
			}
		}

		public static void BetterDispose (IDisposable obj)
		{
			if (null != obj) {
				obj.Dispose ();
			}
		}

		public static void BetterDispose (object obj)
		{
			if (obj is IEnumerable<IDisposable>) {
				var arr = (obj as IEnumerable<IDisposable>).ToArray ();
				for (int i = 0; i < arr.Length; i++) {
					BetterDispose (arr [i]);
				}
			}
			if (obj is IDisposable) {
				var dis = obj as IDisposable;
				BetterDispose (dis);
			}
		}

		public static Xamarin.Forms.Color CombineColor (Xamarin.Forms.Color colorA, Xamarin.Forms.Color colorB)
		{
			colorA = NormaliseColor (colorA);
			colorB = NormaliseColor (colorB);
			var resultAlpha = colorA.A + colorB.A * (1 - colorA.A);
			return new Xamarin.Forms.Color (
				CalculateCombineColor (colorA.R, colorA.A, colorB.R, colorB.A, resultAlpha),
				CalculateCombineColor (colorA.G, colorA.A, colorB.G, colorB.A, resultAlpha),
				CalculateCombineColor (colorA.B, colorA.A, colorB.B, colorB.A, resultAlpha),
				resultAlpha
			);
		}

		static double CalculateCombineColor (double colorA, double alphaA, double colorB, double alphaB, double alphaResult)
		{
			return (colorA * alphaA + colorB * alphaB * (1 - alphaA)) / alphaResult;
		}

		static Xamarin.Forms.Color NormaliseColor (Xamarin.Forms.Color color)
		{
			return new Xamarin.Forms.Color (
				Math.Max (0d, color.R),
				Math.Max (0d, color.G),
				Math.Max (0d, color.B),
				Math.Max (0d, color.A)
			);
		}

		public static IExecuteAtLast ExecuteAtLast (Action action)
		{
			return new ExecuteAtLastImpl (action);
		}

		public interface IExecuteAtLast : IDisposable
		{
			void Invoke ();
		}

		private class ExecuteAtLastImpl : IExecuteAtLast
		{
			Action _action;
			public ExecuteAtLastImpl (Action action)
			{
				_action = action;
			}
			public void Invoke ()
			{
				_action?.Invoke ();
			}
			public void Dispose ()
			{
				try {
					_action?.Invoke ();
				} catch { }
			}
		}

		private class DisposableLockAdapter : IDisposable
		{
			ReaderWriterLockSlim _lock;
			bool _isReadLock;
			public DisposableLockAdapter (ReaderWriterLockSlim rwlock, bool isReadLock)
			{
				_lock = rwlock;
				_isReadLock = isReadLock;
			}

			#region IDisposable implementation
			public void Dispose ()
			{
				if (_isReadLock) {
					_lock?.ExitReadLock ();
				} else {
					_lock?.ExitWriteLock ();
				}
				_lock = null;
			}
			#endregion
		}

		public static IDisposable DisposableReadLock (this ReaderWriterLockSlim rwlock)
		{
			if (null == rwlock) {
				throw new ArgumentNullException ("rwlock");
			}
			rwlock.EnterReadLock ();
			return new DisposableLockAdapter (rwlock, true);
		}

		public static IDisposable DisposableWriteLock (this ReaderWriterLockSlim rwlock)
		{
			if (null == rwlock) {
				throw new ArgumentNullException ("rwlock");
			}
			rwlock.EnterWriteLock ();
			return new DisposableLockAdapter (rwlock, false);
		}

		public static FormattedString AttributeString (string plainStr, string prefix, Span formatTemplate)
		{
			return AttributeString (new Span { Text = plainStr }, prefix, formatTemplate);
		}

		public static FormattedString AttributeString (Span plainStr, string prefix, Span formatTemplate)
		{
			if (string.IsNullOrEmpty (prefix)) {
				throw new InvalidOperationException ("prefix cannot be null or empty");
			}
			int start, end, searchStart;
			start = end = searchStart = 0;
			var formattedStr = new FormattedString ();
			while ((start = plainStr.Text.IndexOf (prefix, searchStart)) >= 0) {
				formattedStr.Spans.Add (new Span {
					Text = plainStr.Text.Substring (searchStart, start - searchStart),
					Font = plainStr.Font,
					BackgroundColor = plainStr.BackgroundColor,
					FontAttributes = plainStr.FontAttributes,
					FontFamily = plainStr.FontFamily,
					FontSize = plainStr.FontSize,
					ForegroundColor = plainStr.ForegroundColor
				});
				searchStart = start + 1;
				end = plainStr.Text.IndexOf (prefix, searchStart);
				if (end >= 0) {
					formattedStr.Spans.Add (new Span {
						Text = plainStr.Text.Substring (searchStart, end - searchStart),
						Font = formatTemplate.Font,
						BackgroundColor = formatTemplate.BackgroundColor,
						FontAttributes = formatTemplate.FontAttributes,
						FontFamily = formatTemplate.FontFamily,
						FontSize = formatTemplate.FontSize,
						ForegroundColor = formatTemplate.ForegroundColor
					});
					searchStart = end + 1;
				}
			}
			if (searchStart < plainStr.Text.Length) {
				formattedStr.Spans.Add (new Span {
					Text = plainStr.Text.Substring (searchStart),
					Font = plainStr.Font,
					BackgroundColor = plainStr.BackgroundColor,
					FontAttributes = plainStr.FontAttributes,
					FontFamily = plainStr.FontFamily,
					FontSize = plainStr.FontSize,
					ForegroundColor = plainStr.ForegroundColor
				});
			}
			return formattedStr;
		}

		public static async void FireAndForget (this Task task, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			if (null != task) {
				try {
					await task;
				} catch (Exception e) {
					Debug.ReportException (e, new Dictionary<string, string> () { { "message", "fire and forget task_n exception" } }, Xamarin.Insights.Severity.Warning, methodName, filePath, lineNum);
				}
			}
		}

		public static async void FireAndForget<T> (this Task<T> task, [CallerMemberName] string methodName = "", [CallerFilePath] string filePath = "", [CallerLineNumber] int lineNum = 0)
		{
			if (null != task) {
				try {
					await task;
				} catch (Exception e) {
					Debug.ReportException (e, new Dictionary<string, string> () { { "message", "fire and forget task_t exception" } }, Xamarin.Insights.Severity.Warning, methodName, filePath, lineNum);
				}
			}
		}

		public static async Task<MediaAsset> CreateAsset (string src, BlrtMediaFormat format)
		{
			var media = new MediaAsset () {
				EncryptType = BlrtEncryptorManager.RecommendedEncyption,
				Format = format
			};

			media.LocalMediaName = string.Format ("media-{0}.{1}", media.ObjectId,
												  format == BlrtMediaFormat.PDF ? "pdf" : "jpg");

			string dst = media.DecyptedPath;

			if (format == BlrtMediaFormat.PDF) {
				File.Copy (src, dst, true);
			} else if (format == BlrtMediaFormat.Image) {
				await PlatformHelper.CopyAsJpeg (src, dst);
			}

			return media;
		}

		/// <summary>
		/// Flags the conversation user relation.
		/// </summary>
		/// <returns><c>true</c>, if conversation user relation was flaged, <c>false</c> otherwise.</returns>
		/// <param name="relation">Relation.</param>
		/// <param name="flagged">If set to <c>true</c> flagged.</param>
		internal static bool FlagConversationUserRelation (ConversationUserRelation relation, bool flagged)
		{
			if (relation.Flagged != flagged) {
				relation.Flagged = flagged;
				relation.Dirty = true;
				relation.LocalStorage.DataManager.Save ();
				BlrtData.Helpers.BlrtTrack.ConvoFlag (relation.ConversationId, relation.Flagged);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Archives the conversation user relation.
		/// </summary>
		/// <returns><c>true</c>, if conversation user relation was archived, <c>false</c> otherwise.</returns>
		/// <param name="relation">Relation.</param>
		/// <param name="archived">If set to <c>true</c> archived.</param>
		internal static bool ArchiveConversationUserRelation (ConversationUserRelation relation, bool archived)
		{
			if (relation.Archive != archived) {
				relation.Archive = archived;
				relation.Dirty = true;
				relation.LocalStorage.DataManager.Save ();
				BlrtData.Helpers.BlrtTrack.ConvoArchive (relation.ConversationId, relation.Archive);
				return true;
			}
			return false;
		}

		public static async Task<Exception> DecryptMedia (MediaAsset [] media)
		{
			Exception lastDecryptionException = null;

			foreach (var m in media) {
				if (!m.IsDecypted) {
					try {
						await m.DecyptData ();
					} catch (BlrtEncryptorManagerException e1) {
						lastDecryptionException = e1;
						m.DeleteData ();
					} catch (InvalidOperationException e2) {
						lastDecryptionException = e2;
						m.DeleteData ();
					}
				}
			}
			return lastDecryptionException;
		}

		public delegate BlrtDownloaderList DownloadMediaDelegate (int priority);
		internal static async Task<bool> DownloadAndDecryptContent (DownloadMediaDelegate downloadMedia,
																	IEnumerable<MediaAsset> mediaAssets)
		{
			var download = downloadMedia (1000);
			while (true) {
				bool needRetry = false;
				if (download.Count > 0) {
					// download media files if needed
					var result = await BlrtManager.App.ShowProgressAlert (
						Executor.System (),
							new SimpleProgressAlert (
								download,
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_title"),
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_message"),
								BlrtUtil.PlatformHelper.Translate ("checking_blrt_to_open_cancel")
							)
						);
					if (result.Success) {
						if (result.Result.Finished) {
							needRetry = false;
						} else {
							// user pressed cancel while downloading
							return false;
						}
					} else {
						// download failed
						needRetry = true;
					}
				}
				// check file validation even if download failed or no file downloaded
				using (UserDialogs.Instance.Loading ("Loading...")) {
					needRetry = await DecryptMedia (mediaAssets.ToArray ()) != null;
				}
				if (needRetry) {
					if (await AlertInternetErrorRetry ()) {
						download = downloadMedia (1000);
						continue;
					} else {
						return false;
					}
				} else {
					return true;
				}
			}
		}

		public static readonly int THUMBNAIL_SIZE_SMALL = 256;
		public static readonly int THUMBNAIL_SIZE_MEDIUM = 512;
		public static readonly int THUMBNAIL_SIZE_LARGE = 1024;

		internal static int GetStandardThumbnailSize (int length = 0)
		{
			if (length == 0) {
				return CanvasPageView.PlatformHelper.ThumbnailLength;
			}

			// TODO: return smallest standard thubnail length that is bigger that provided length parameter.
			return THUMBNAIL_SIZE_SMALL;
		}

		internal static bool HasUserSignedUp (ConversationUserRelation conversationUserRelation)
		{
			if (conversationUserRelation == null) return false;
			return ConversationRelationStatus.GetTypeOfRelation (conversationUserRelation)
															!= ConversationRelationStatus.RoleEnum.NotSignedUp;
		}

		/// <summary>
		/// 	Method to get user's display name.
		/// </summary>
		/// <returns>The user display name.</returns>
		/// <param name="hasUserSignedUp">If set to <c>true</c> has user signed up.</param>
		internal static string GetUserDisplayName (ConversationUserRelation conversationUserRelation)
		{
			var hasUserSignedUp = HasUserSignedUp (conversationUserRelation);

			var email = conversationUserRelation.GetUserEmail ();

			if (!hasUserSignedUp) {
				return email;
			}

			var name = conversationUserRelation.GetUserDisplayName ();

			if (string.IsNullOrWhiteSpace (name)) {
				return email;
			}

			return name;
		}

		/// <summary>
		/// This method, based on the type of conversation, tells whether help overlay is needed 
		/// for the currently open conversation
		/// </summary>
		/// <returns><c>true</c>, if help overlay is needed, <c>false</c> otherwise.</returns>
		/// <param name="isEmptyConvo">If set to <c>true</c> is empty convo</param>
		static bool IsConversationHelpOverlayNeeded (bool isEmptyConvo, bool containsWelcomeBlrt)
		{
			if (isEmptyConvo) {
				return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayEmptyConversationViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayEmptyConversationViewed];
			}

			if (containsWelcomeBlrt) {
				return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed)
				  || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationWithWelcomeBlrtViewed];
			}

			return !BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayConversationViewed)
				  || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayConversationViewed];
		}

		/// <summary>
		/// Updates the user account status.
		/// </summary>
		/// <param name="user">User.</param>
		/// <param name="newAccountStatus">New account status.</param>
		internal static async Task UpdateUserAccountStatus (BlrtUser user, string newAccountStatus)
		{
			if (user != null) {
				user.AccountStatus = newAccountStatus;
				await BlrtManager.DataManagers.Default.SaveLocal ();
			}
		}

		/// <summary>
		/// Updates the conversation readonly status.
		/// </summary>
		/// <param name="conversation">Conversation.</param>
		/// <param name="isReadonly">If set to <c>true</c> is readonly.</param>
		internal static async Task UpdateConversationReadonlyStatus (Conversation conversation, bool isReadonly)
		{
			if (conversation != null) {
				conversation.IsReadonly = isReadonly;
				await conversation.LocalStorage.DataManager.SaveLocal ();
			}
		}

		/// <summary>
		/// Tries to get help overlay for convo.
		/// </summary>
		/// <returns>The get help overlay for convo.</returns>
		/// <param name="isEmptyConversation">If set to <c>true</c> is empty conversation.</param>
		/// <param name="containsWelcomeBlrt">If set to <c>true</c> contains welcome blrt.</param>
		internal static object TryShowHelpOverlayForConvo (bool isEmptyConversation, bool containsWelcomeBlrt)
		{
			if (IsConversationHelpOverlayNeeded (isEmptyConversation, containsWelcomeBlrt)) {
				return Views.Helpers.HelpOverlayHelper
									   .AddConversationHelpOverlay (isEmptyConversation, containsWelcomeBlrt);
			}
			return null;
		}

		/// <summary>
		/// Tries to show canvas help overlay.
		/// </summary>
		internal static void TryShowCanvasHelpOverlay ()
		{
			try {
				if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasStartViewed)
					|| !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasStartViewed]) {
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasStartHelpOverlay ();
				} else if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasDrawViewed)
					  || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasDrawViewed]) {
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasDrawHelpOverlay ();
				} else if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasNavViewed)
					  || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasNavViewed]) {
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasNavHelpOverlay ();
				} else if (!BlrtPersistant.Properties.ContainsKey (BlrtConstants.HelpOverlayCanvasFinishViewed)
					  || !(bool)BlrtPersistant.Properties [BlrtConstants.HelpOverlayCanvasFinishViewed]) {
					BlrtData.Views.Helpers.HelpOverlayHelper.AddCanvasFinishHelpOverlay ();
				}

			} catch { }
		}

		public class ConversationsOptionsUserItem
		{
			public object Model { get; set; }
			public string Initials { get; set; }
			public string DisplayName { get; set; }
			public string RelationType { get; set; }
			public string AvatarPath { get; set; }
			public Color InitialBackgroundColor { get; set; }
		}

		/// <summary>
		/// Deletes the conversation from local.
		/// </summary>
		/// <returns>The conversation from local.</returns>
		/// <param name="localStorage">Local storage.</param>
		/// <param name="conversation">Conversation.</param>
		internal static async Task DeleteConversationFromLocal (ILocalStorageParameter localStorage, Conversation conversation, IExecutor exec, ConversationUserRelation relation)
		{
			var slave = BlrtManager.App.GetCurrentSlave ();

			IMailboxScreen mailbox = null;
			if (slave is IMailboxScreen) {
				mailbox = slave as IMailboxScreen;
			}

			if (Device.OS == TargetPlatform.Android) {
				var result = await BlrtManager.App.OpenMailbox (exec, Models.Mailbox.MailboxType.Any);
				mailbox = result.Result;
			}

			if (relation != null) {
				localStorage.DataManager.Remove (relation);
			}
			localStorage.DataManager.Remove (conversation);
			await localStorage.DataManager.SaveLocal ();
			mailbox?.Reload (exec);


#if __IOS__
			var currentConversation = (BlrtManager.App.GetCurrentSlave () as IMailboxScreen)?.
																							 ConversationScreen?.
																							 CurrentConversation?.
																							 ObjectId ?? string.Empty;
			var currentConversationDeleted = currentConversation.Equals (conversation.ObjectId);

			if (currentConversationDeleted) {
				mailbox?.CloseConversation (exec);
			}
#endif

		}

		/// <summary>
		/// Represents the abstract contact class.
		/// </summary>
		public interface IConversationContact
		{
			bool IsRemovable { get; }
			string Name { get; }
			string Initials { get; }
			string Detail { get; }
			string Avatar { get; }
			ConversationRelationStatus.RoleEnum RelationType { get; }
			object Model { get; }
			bool IsValid { get; }
			ContactState SendingStatus { get; set; }
			bool AccountRemoved { get; }

			bool Equals (IConversationContact contact);
		}

		#if __IOS__
		public static nfloat SmoothClamp (nfloat value, nfloat max)
		{
			// y = x / (1 + |x / 100|)

			return value / (1 + NMath.Abs (value / max));
		}
		/// <summary>
		/// Saves the name of the conversation. ONLY this method must be used to save new convo name.
		/// </summary>
		/// <returns>Request status: success, failed, cancelled</returns>
		/// <param name="_conversation">Conversation.</param>
		/// <param name="newName">New name.</param>
		public static async Task<APIConversationRenameRequestStatus> SaveConversationName (Conversation _conversation, string newName)
		{
			if (_conversation != null && _conversation.Name != newName.Trim ()) {
				var requestStatus = await SaveConversationNameToCloud (_conversation.ObjectId, newName);
				if (requestStatus == APIConversationRenameRequestStatus.Success) {
					//save it to local
					_conversation.Dirty = true;
					_conversation.Name = newName.Trim ();
					_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
				}
				return requestStatus;
			}
			return APIConversationRenameRequestStatus.Success;
		}

		/// <summary>
		/// Creates the conversation existing contact from relation1.
		/// </summary>
		/// <returns>The conversation existing contact from relation1.</returns>
		/// <param name="userRelation">User relation.</param>
		/// <exception cref="Exceptiom">Throws exception</exception>
		internal static Tuple<ConversationExistingContact, bool> CreateConversationExistingContactFromRelation (ConversationUserRelation userRelation)
		{
			var displayItem = new ConversationExistingContact ();
			var userId = userRelation.UserId;
			var isUserNeedsToBeFecthed = false; // whether user's data is available locally

			displayItem.RelationType = ConversationRelationStatus.GetTypeOfRelation (userRelation);
			displayItem.InitialBackgroundColor = GetContactCellBackgroundColorForRelationType (displayItem.RelationType);
			
			// user id exists only for blrt users
			if (!string.IsNullOrWhiteSpace (userId)) {
				var user = BlrtManager.DataManagers.Default.GetUser (userId); 

				var localAvatarPath = string.Empty;

				localAvatarPath = user.IsDeleted? DELETED_ACCOUNT_AVATAR_PATH : user.LocalAvatarPath;
				displayItem.AvatarPath = localAvatarPath;
				displayItem.Model = user;
				displayItem.DisplayName = GetUserDisplayName (userRelation);
				displayItem.Initials = !user.IsDeleted && string.IsNullOrWhiteSpace(localAvatarPath) ? GetInitials (displayItem.DisplayName) : string.Empty;

				isUserNeedsToBeFecthed = !user.Found;

			} else {
				// construct contact for non user using contact data avaible in relation
				var blrtContact = CreateBlrtContactForNonUserUsingRelation (userRelation);

				displayItem.Model = blrtContact;
				displayItem.DisplayName = blrtContact.UsableName;
				displayItem.Initials = GetInitials (displayItem.DisplayName);
			}
			displayItem.DisplayName = displayItem.DisplayName + "\nAccount deleted";
			return new Tuple<ConversationExistingContact, bool> (displayItem, isUserNeedsToBeFecthed);
		}

		/// <summary>
		/// Creates the blrt contact for non user using relation.
		/// </summary>
		/// <returns>The blrt contact for non user using relation.</returns>
		/// <param name="userRelation">User relation.</param>
		static BlrtContact CreateBlrtContactForNonUserUsingRelation (ConversationUserRelation userRelation)
		{
			var c = new BlrtContact ();
			var contactInfo = new BlrtContactInfo (IsValidEmail (userRelation.GetUserEmail ()) ?
																	 BlrtContactInfoType.Email : BlrtContactInfoType.PhoneNumber,
																	 userRelation.QueryValue);
			c.Add (contactInfo);
			var blrtContact = BlrtContactManager.SharedInstanced.GetMatching (c);

			if (blrtContact == null) {
				blrtContact = c;
			}

			return blrtContact;
		}

		/// <summary>
		/// Gets the contact cell background color for a given relation type.
		/// </summary>
		/// <returns>The contact cell background color for relation type.</returns>
		/// <param name="relationType">Relation type.</param>
		static Color GetContactCellBackgroundColorForRelationType (ConversationRelationStatus.RoleEnum relationType)
		{
			switch (relationType) {
				case ConversationRelationStatus.RoleEnum.Creator:
				case ConversationRelationStatus.RoleEnum.Contributor:
				case ConversationRelationStatus.RoleEnum.Viewed:
				case ConversationRelationStatus.RoleEnum.NotViewed:
					return BlrtStyles.BlrtGreen;
				case ConversationRelationStatus.RoleEnum.NotSignedUp:
					return BlrtStyles.BlrtGray5;
				default : 
					return BlrtStyles.BlrtAccentRed;
			}
		}

		/// <summary>
		/// Gets the the string representation for the conversation relation type.
		/// </summary>
		/// <returns>The string for conversation relation type.</returns>
		/// <param name="type">Type.</param>
		public static string GetStringForConversationRelationType (ConversationRelationStatus.RoleEnum type)
		{
 			switch (type) {
	 			case ConversationRelationStatus.RoleEnum.Creator:
					return "Creator";
	 			case ConversationRelationStatus.RoleEnum.Contributor:
					return "Contributor";
	 			case ConversationRelationStatus.RoleEnum.Viewed:
					return "Viewed";
	 			case ConversationRelationStatus.RoleEnum.NotViewed:
					return "Not viewed";
	 			case ConversationRelationStatus.RoleEnum.NotSignedUp:
					return "Not signed up";
				default: return string.Empty;
			}
		}

		/// <summary>
		/// Shows the mic permission alert. Shows the user where to grant permssion from.
		/// </summary>
		/// <returns>The mic permission alert.</returns>
		internal static Task ShowMicPermissionAlert ()
		{
			return BlrtData.BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (),
				new BlrtData.SimpleAlert (
					BlrtData.BlrtUtil.PlatformHelper.Translate ("disabled_micphone_access_title"),
					BlrtData.BlrtUtil.PlatformHelper.Translate ((Device.OS == TargetPlatform.Android) ? "disabled_micphone_access_message_android" : "disabled_micphone_access_message"),
					BlrtData.BlrtUtil.PlatformHelper.Translate ("disabled_micphone_access_cancel")
				)
			);
		}

		/// <summary>
		/// Adds the sending contacts to conversation. This lets conversation view to show contacts' sending status.
		/// </summary>
		/// <param name="contacts">Contacts.</param>
		/// <param name="conversation">Conversation.</param>
		public static void AddSendingContactsToConversation (object sender, IEnumerable<ConversationNonExistingContact> contacts, Conversation conversation)
		{
			if (conversation.SendingContacts == null) {
				conversation.SendingContacts = new List<ContactStatus> ();
			}

			var now = DateTime.Now;
			foreach (var co in contacts) {
				var coStatus = new ContactStatus () {
					SelectedInfo = co.SelectedInfo,
					UpdateAt = now,
					Status = ContactState.Sending
				};

				var match = conversation.SendingContacts.FirstOrDefault (x => x.Equals (coStatus));
				if (match != null) {
					match.Status = coStatus.Status;
					match.UpdateAt = coStatus.UpdateAt;
				} else {
					conversation.SendingContacts.Add (coStatus);
				}
			}

			conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.InProgress;
			conversation.LocalStorage.DataManager.SaveLocal ().ContinueWith (delegate {
				MessagingCenter.Send<Object, bool> (sender, string.Format ("{0}.RelationChanged", conversation.LocalGuid), false);
			});
		}

		/// <summary>
		/// Represents the existing contact in a conversation
		/// </summary>
		public class ConversationExistingContact : IConversationContact
		{
			public object Model { get; set; }
			public string Initials { get; set; }
			public string DisplayName { get; set; }

			ConversationRelationStatus.RoleEnum _relationType;
			public ConversationRelationStatus.RoleEnum RelationType { 
				get {
					return _relationType;
				}
				set {
					_relationType = value;
					RelationTypeString = GetStringForConversationRelationType (value);
				} 
			}

			public string RelationTypeString { get; private set; }
			public string AvatarPath { get; set; }
			public Color InitialBackgroundColor { get; set; }


			public bool IsRemovable {
				get {
					return false;
				}
			}

			public string Name {
				get {
					return DisplayName;
				}
			}

			public string Detail {
				get {
					if (Model is BlrtUser) {
						var user = Model as BlrtUser;

						if (user.IsDeleted) {
							return PlatformHelper.Translate ("deleted_user_string", "profile");
						}

						var info = user.Email;
						if (BlrtUtil.IsValidPhoneNumber (user.ContactInfo)) {
							info = user.ContactInfo;
						}
						return info;
					}

					if (Model is BlrtContact) {
						var contact = Model as BlrtContact;
						if (contact.InfoList.Count == 0)
							return string.Empty;

						return contact.InfoList [0].ToString ();
					}

					return string.Empty;
				}
			}
			
			public string Avatar {
				get {
					return AvatarPath;
				}
			}

			public bool IsValid {
				get {
					return true;
				}
			}

			public ContactState SendingStatus {
				get {
					return ContactState.ConversationSentUser;
				}
				set { }
			}

			public bool AccountRemoved {
				get {
					if (Model is BlrtUser) {
						var user = Model as BlrtUser;

						return user.IsDeleted;
					}
					return false;
				}
			}

			public bool Equals (IConversationContact contact)
			{
				if (!(contact is ConversationExistingContact)) {
					return false;
				}

				if (!(contact.Equals (Initials) &&
					  contact.Equals (DisplayName) &&
					  contact.Equals (AvatarPath) &&
					  RelationType == contact.RelationType)) {
					return false;
				}

				if (Model is BlrtContact) {
					return (Model as BlrtContact).CompareTo (contact as BlrtContact) == 0;
				}

				if (Model is BlrtUser) {
					return (Model as BlrtUser).ObjectId.Equals (contact as BlrtUser);
				}

				return false;
			}
		}

		/// <summary>
		/// Loads the unsend contacts from local storage to conversation.
		/// </summary>
		/// <param name="_conversation">Conversation.</param>
		internal static void LoadUnsendContactsToConversation (Conversation _conversation)
		{
			if (_conversation.UnSendContacts != null && _conversation.UnSendContacts.Length > 0) {
				//if the email already in convo, we should just remove it
				var relations = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (_conversation.ObjectId);
				var existEmails = new List<string> ();
				foreach (var rel in relations) {
					existEmails.Add (rel.GetUserEmail ());
					existEmails.Add (rel.QueryValue);
				}

				_conversation.UnSendContacts = _conversation.UnSendContacts.Except (existEmails).ToArray ();
				_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
			}
		}

		/// <summary>
		/// Saves the un send contacts to local conversation.
 		/// </summary>
		/// <param name="_conversation">Conversation.</param>
		internal static void SaveUnSendContactsToLocalConversation (Conversation _conversation, IEnumerable<ConversationNonExistingContact> contacts)
 		{
			if (contacts == null) {
				return;
 			}
 
			if (_conversation != null) {

				//caching data
				if (_conversation.UnSendContacts == null) {
					_conversation.UnSendContacts = new string [] { };
				}
				var sendingContacts = contacts.Where (t => t.SelectedInfo.ContactType != BlrtContactInfoType.FacebookId).Select (t => t.SelectedInfo.ContactValue).ToArray ();
				_conversation.UnSendContacts = sendingContacts; //_conversation.UnSendContacts.Concat (sendingContacts).Distinct ().ToArray ();
				_conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
 			}
		}

		/// <summary>
		/// Converts the string to contact.
		/// </summary>
		/// <param name="contactString">Contact string.</param>
		/// <param name="addContactDelegate">Add contact delegate. It is called for each string converted to contact</param>
		public static void CreateContactsFromString (string [] contactString, Func<ConversationNonExistingContact, bool, Task<bool>> addContactDelegate)
		{
			int deletStringCount = 0;
 
			// should not add current user to list
			for (int i = 0; contactString != null && i < contactString.Length; ++i) {
				var str = contactString [i].Trim ();
				if (str == Parse.ParseUser.CurrentUser.Email) {
					deletStringCount++;
					continue;
				}
                addContactDelegate (CreateContactFromString (str), false);
			}
 		}

		/// <summary>
		/// Loads the failed contacts from conversation.
		/// </summary>
		/// <param name="_conversation">Conversation.</param>
		internal static void LoadFailedContactsFromConversation (Conversation conversation, Func<ConversationNonExistingContact, bool, Task<bool>> addContactDelegate)
		{
			var selectedList = conversation.SendingContacts?.Where (x => x.Status == ContactState.FailedToSend
																	|| (x.Status == ContactState.Sending
																		&& x.UpdateAt < DateTime.Now.AddSeconds (-20)));

			if (selectedList == null) {
				return;
			}

			foreach (var contact in selectedList) {
				var contactInfo = contact.SelectedInfo;
				var newcontact = new BlrtContact () {
					Name = null,
				};
				newcontact.Add (contactInfo);
				var match = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
				if (match == null) {
					match = newcontact;
				}
				var selectInfo = contactInfo;
				foreach (var info in match.InfoList) {
					if (info.Equals (contactInfo)) {
						selectInfo = info;
						break;
					}
				}
				addContactDelegate (new ConversationNonExistingContact (selectInfo, match, ContactState.FailedToSend), false);
			}
		}

		/// <summary>
		/// Creates the contact from string.
 		/// </summary>
		/// <returns>The contact from string.</returns>
		/// <param name="contactString">Contact string.</param>
		public static ConversationNonExistingContact CreateContactFromString (string contactString)
		{
			var str = contactString.Trim ();

			var contactInfo = new BlrtContactInfo (IsValidEmail (str) ? BlrtContactInfoType.Email : BlrtContactInfoType.PhoneNumber, str);
			var newcontact = new BlrtContact {
				Name = null,
			};
			newcontact.Add (contactInfo);
			var otherContact = BlrtContactManager.SharedInstanced.GetMatching (newcontact);
			ConversationNonExistingContact newSelected;
			if (otherContact == null) {
				newSelected = new ConversationNonExistingContact (contactInfo, newcontact);
			} else {
				var selectInfo = otherContact.InfoList.First (info => info.Equals (contactInfo));
				newSelected = new ConversationNonExistingContact (selectInfo, otherContact);
 			}

			return newSelected;
 		}

		/// <summary>
		/// Removes an un send contact from a conversation.
 		/// </summary>
 		/// <param name="conversation">Conversation.</param>
		/// <param name="contact">Contact.</param>
		internal static void RemoveUnSendContactsFromConversation (Conversation conversation, List<ConversationNonExistingContact> contacts)
		{
			var contactValues = contacts.Select (contact => contact.SelectedInfo.ContactValue);
			if (conversation.UnSendContacts != null &&
				conversation.UnSendContacts.Length > 0) {
				if (contactValues.Any (cv => conversation.UnSendContacts.Contains (cv))) {
					conversation.UnSendContacts = conversation.UnSendContacts.Except (contactValues).ToArray ();
					conversation.LocalStorage.DataManager.SaveLocal ().FireAndForget ();
				}
			}
		}

		/// <summary>
		/// Gets the string representation for a given contact sending state.
		/// </summary>
		/// <returns>The string for contact sending state.</returns>
		/// <param name="state">State.</param>
		internal static string GetStringForContactSendingState (ContactState state)
		{
			switch (state) {
			case ContactState.Blocked:
				return "Blocked";

			case ContactState.ConversationAlreadySent:
				return "Already sent";

			case ContactState.ConversationSentReachable:
				return "Invitation sent";

			case ContactState.ConversationSentUnreachable:
				return "Not on Blrt";

			case ContactState.ConversationSentUser:
				return "Sent to user";

			case ContactState.FailedToSend:
				return "Failed to send";

			case ContactState.InvalidContact:
				return "Invalid contact";

			case ContactState.Sending:
				return "Sending";

			default:
				return string.Empty;

			}
		}

		public static async Task<bool> CheckAddPeoplePermission (Conversation conversation, bool checkUserCount, int unsendContactsCount = 0)
		{
			if (ParseUser.CurrentUser.ObjectId != conversation.CreatorId && conversation.DisableOthersToAdd) {
				await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_title", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_msg", "conversation_screen"),
					BlrtUtil.PlatformHelper.Translate ("conversation_lock_accept", "conversation_screen")
				));

				return false;
			}

			if (!BlrtPermissions.CanAddUsers) {

				await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_title"),
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_message"),
					BlrtUtil.PlatformHelper.Translate ("permissions_cant_add_users_accept")
				));

				return false;
			}

			if (!checkUserCount) {
				return true;
			}

			var maxConversationUsers = BlrtPermissions.MaxConversationUsers;
			var bondCount = conversation.BondCount;
			if (maxConversationUsers <= bondCount || unsendContactsCount >= maxConversationUsers - bondCount) {

				if (conversation.CreatedByCurrentUser) {
					await ShowAddPeoplePermissionAlertForConversationCreator ();
				} else {
					await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
						BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_title", "conversation_screen"),
						BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_message", "conversation_screen"),
						BlrtUtil.PlatformHelper.Translate ("alert_cannot_add_more_people_accept", "conversation_screen")
					));
				}

				BlrtData.Helpers.BlrtTrack.AccountHitLimit (BlrtData.BlrtPermissions.PermissionEnum.MaxConversationUsers.ToString ());
				return false;
			}
			return true;
		}

		/// <summary>
		/// Shows the add people permission alert for conversation creator.
		/// </summary>
		static async Task ShowAddPeoplePermissionAlertForConversationCreator ()
 		{
			if (BlrtPermissions.AccountThreshold == BlrtPermissions.BlrtAccountThreshold.Free) {
				await BlrtUpgradeHelper.ShowUpgradeDialog (BlrtPermissions.PermissionEnum.MaxConversationUsers);
			} else {
				await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("failed_too_many_recipents_free_title", "upgrade"),
					string.Format (BlrtUtil
								   .PlatformHelper
								   .Translate ("failed_too_many_recipents_free_message", "upgrade"),
								   BlrtPermissions.MaxConversationUsers),
					BlrtUtil.PlatformHelper.Translate ("failed_too_many_recipents_accept", "send-screen")
				));
 			}
 		}

		/// <summary>
		/// Gets all contact types for a given contact.
		/// </summary>
		/// <returns>set of contact types</returns>
		/// <param name="infoList">Info list.</param>
		internal static HashSet<BlrtContactInfoType> GetAllContactTypesForInfoList (List<BlrtContactInfo> infoList)
		{
			return infoList.Select (info => info.ContactType).ToHashSet ();
		}

		/// <summary>
		/// Gets the conversation existing contacts as contact info.
		/// </summary>
		/// <returns>The conversation existing contact as contact info.</returns>
		/// <param name="_conversation">Conversation.</param>
		internal static HashSet<string> GetConversationExistingContactsAsContactInfo (Conversation conversation)
		{
			var rels = BlrtManager.DataManagers.Default.GetConversationUserRelationsFromConversation (conversation.ObjectId);
			return rels.Select (rel => rel.QueryValue).ToHashSet ();

		}

		/// <summary>
		/// Gets the email body for either blrt or a conversation.
 		/// </summary>
		/// <returns>The email body</returns>
		/// <param name="bodyTemplate">Body template to use</param>
		/// <param name="itemUrl">Item URL.</param>
		/// <param name="itemName">Item name.</param>
		/// <param name="forPublicItem">If set to <c>true</c> for public item.</param>
		/// <param name="forEmbedCode">If set to <c>true</c> for embed code.</param>
		/// <param name="encodedDataForEmbedCode">Encoded data for embed code.</param>
		internal static async Task<string> GetEmailBodyUsing (string bodyTemplate,
															  string itemUrl,
															  string itemName,
															  bool forPublicItem,
															  bool forEmbedCode,
															  string encodedDataForEmbedCode = null)
		{
			var impression = GetImpressionHtml ("blrt_email");
			impression += GetImpressionHtml ("share_email");
			var shareLink = await GenerateYozioShareUrl ("share_email", true);

			if (forEmbedCode) {
				return string.Format (bodyTemplate,
								  itemUrl,
								  itemName,
								  encodedDataForEmbedCode,
								  impression,
								  shareLink);
			}

			if (forPublicItem) {
				return string.Format (bodyTemplate,
									  itemUrl,
									  itemName,
									  HttpUtility.UrlEncode (itemName),
									  HttpUtility.UrlEncode (itemUrl),
									  impression,
									  shareLink);
			}

			return string.Format (bodyTemplate,
								  itemUrl,
								  itemName,
								  impression,
								  shareLink);
		}

		/// <summary>
		/// Removes the sending failed contacts.
		/// </summary>
		/// <param name="removeAll">If set to <c>true</c> remove all.</param>
		/// <param name="conversation">Conversation.</param>
		/// <param name="contactInfo">Contact info.</param>
		public static void RemoveSendingFailedContacts (bool removeAll, Conversation conversation, List<BlrtData.Contacts.BlrtContactInfo> contactInfo = null)
		{
			if (removeAll) {
				conversation.SendingContacts.Clear ();
				conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
			} else if (contactInfo != null) {
				conversation.SendingContacts = conversation
					.SendingContacts
					.Where (contact => !contactInfo.Contains (contact.SelectedInfo))
					.ToList ();
			}
			conversation.SendingContacts.TrimExcess ();

			if (!conversation.SendingContacts.Any (c => c.Status == ContactState.FailedToSend)) {
				conversation.CurrentActivityStatus = BlrtData.Conversation.ActivityStatus.Completed;
			}

			conversation.LocalStorage.DataManager.SaveLocal ().ContinueWith (delegate {
				//update conversation screen
				MessagingCenter.Send<object> (conversation, string.Format ("{0}.SendingFailedContactsChanged", conversation.LocalGuid));
			});
 		}

		/// <summary>
		/// Creates the conversation existing contacts for a given conversation.
 		/// </summary>
		/// <returns>Tuple <existingContacts, list of users ids that need to be fetched data for></returns>
		/// <param name="conversationId">Conversation identifier.</param>
		public static Tuple<List<ConversationExistingContact>, List<string>> CreateConversationExistingContactsForConversation (string conversationId)
		{
			var rels = BlrtManager.DataManagers
						  .Default
						  .GetConversationUserRelationsFromConversation (conversationId)
						  .OrderBy (r => r, new ConversationRelationStatus.Comparer ());

			var unfetchedUsers = new List<string> ();
			var existingContacts = new List<ConversationExistingContact> ();

			foreach (var rel in rels) {
				var result = CreateConversationExistingContactFromRelation (rel);
				var existingContact = result.Item1;

				var userNeedsToBeFecthed = result.Item2;
				if (userNeedsToBeFecthed) {
					unfetchedUsers.Add (rel.UserId);
 				}
				existingContacts.Add (existingContact);
			}
 
			return new Tuple<List<ConversationExistingContact>, List<string>> (existingContacts, unfetchedUsers);
 		}

		/// <summary>
		/// Shows the conversation readonly alert.
		/// </summary>
		public static void ShowConversationReadonlyAlert ()
		{
			BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), new SimpleAlert (
				"Readonly conversation",
				"This conversation is readonly",
				"Dismiss"//BlrtUtil.PlatformHelper.Translate ("conversation_lock_accept", "conversation_screen")
			)).FireAndForget ();
		}
#endif

	}
}
