using System;
using System.IO;
using System.Collections.Generic;
using Parse;
using BlrtData.Query;
using BlrtData.Data;
using BlrtData.Notification;
using Newtonsoft.Json;
using BlrtData.IO;
using Rivets;
using System.Threading;
using System.Threading.Tasks;
using BlrtData.Events;
using Acr.UserDialogs;

namespace BlrtData
{
	public class BlrtManager
	{
		private static BlrtManager _control;

		private BlrtQueryManager _query;
		private BlrtDataManager _userData;
		private BlrtIndependentDataManager _independentPrivateData;
		private BlrtIndependentDataManager _independentPublicData;
		private BlrtUploadManager _upload;
		private EventManager _event;

		private IBlrtResponser _responder;


		internal static IBlrtResponser Responder { get { return _control._responder; } }
		internal static BlrtSettingsManager settingManager;



		public static bool Initialized { get { return _control == null; } }
		public static BlrtQueryManager Query { get { return _control._query; } }
		public static BlrtUploadManager Upload { get { return _control._upload; } }
		public static EventManager Event { get { return _control._event; } }

		public static BlrtPermissions Permissions { get { return settingManager?.Permissions; } }
		public static BlrtSettings Settings { get { return settingManager.Settings; } }
		public static BlrtIAP IAP { get { return settingManager.IAP; } }

		public static ExecutionPipeline.IAppController App { get { return _control._responder as ExecutionPipeline.IAppController; } }



		private BlrtManager (IBlrtResponser responder)
		{
			_responder = responder;

			settingManager = BlrtSettingsManager.FromCache ();
			_userData = new BlrtDataManager ();
			_independentPrivateData = new BlrtIndependentDataManager (BlrtConstants.Directories.IndependentPrivate);
			_independentPublicData = new BlrtIndependentDataManager (BlrtConstants.Directories.IndependentPublic);
			_query = new BlrtQueryManager ();
			_upload = new BlrtUploadManager ();
			_event = new EventManager ();
		}

		public static class DataManagers
		{
			public static BlrtDataManagerBase Default { get { return User; } }
			public static BlrtDataManagerBase User { get { return _control._userData; } }
			public static BlrtDataManagerBase IndependentPublic { get { return _control._independentPublicData; } }
			public static BlrtDataManagerBase IndependentPrivate { get { return _control._independentPrivateData; } }
			public static void SaveAll ()
			{
				User.Save ();
				IndependentPublic.Save ();
				IndependentPrivate.Save ();
			}
		}

		public static void Initialize (IBlrtResponser responder)
		{

			ResetCacheWordSize ();
			BlrtPersistant.Init ();
			BlrtUtil.GetDeviceIPAddress ();
			LLogger.UserId = ParseUser.CurrentUser != null ? ParseUser.CurrentUser.ObjectId : null;
			LLogger.WriteEvent ("credentials", "App Openned");

			if (responder == null)
				throw new Exception ("Cannot supply null responder");

			if (_control != null)
				throw new Exception ("Cannot Initialize Blrt controller more than once!");

			_control = new BlrtManager (responder);

			BlrtEventManager.Reload ();


			if (BlrtSettings.MaintenanceMode)
				BlrtManager.Responder.ActivateLockout (BlrtLockoutMode.Maintance, false);
			else
				BlrtManager.Responder.DeactivateLockout (BlrtLockoutMode.Maintance, false);


			if (BlrtSettings.InvalidVersion)
				BlrtManager.Responder.ActivateLockout (BlrtLockoutMode.InvalidVersion, false);
			else
				BlrtManager.Responder.DeactivateLockout (BlrtLockoutMode.InvalidVersion, false);


			BlrtGlobalEventManager.OnGotSettings += CheckIfUserDisabledDelegate;
			BlrtGlobalEventManager.OnGotSettings += CheckForNotifications;

			Task.Factory.StartNew (() => {
				_control._independentPrivateData.LoadData ();
				_control._independentPublicData.LoadData ();
			});

#if __ANDROID__
			ShouldHandleActionSendAfterLoginFilePath = null;
			ShouldHandleActionSendAfterLoginFilePath = null;
#endif 

			return;
		}

		const int DefaultFlagFileVersion = 1;
#if __ANDROID__
		const int FlagFileVersion = 2;
#else
		const int FlagFileVersion = 1;
#endif
		static readonly Dictionary<BlrtUtil.WordSize, string> FlagFiles = new Dictionary<BlrtUtil.WordSize, string> () {
			{ BlrtUtil.WordSize.Bit_32, "_cache32_" + FlagFileVersion },
			{ BlrtUtil.WordSize.Bit_64, "_cache64_" + FlagFileVersion }
		};

		static void ResetCacheWordSize ()
		{
			var wordSize = BlrtUtil.DefaultWordSize;
			var fileVersion = DefaultFlagFileVersion;
			foreach (var item in FlagFiles) {
				if (File.Exists (Path.Combine (BlrtConstants.Directories.AppRoot, item.Value))) {
					wordSize = item.Key;
					fileVersion = FlagFileVersion;
					break;
				}
			}
			if (BlrtUtil.DeviceWordSize != wordSize || FlagFileVersion != fileVersion) {
				BlrtConstants.DeleteEverything ();
				var path = Path.Combine (BlrtConstants.Directories.AppRoot, FlagFiles [BlrtUtil.DeviceWordSize]);
				using (var file = File.Create (path)) {
					file.Close ();
				}
			}
		}

		public static void CheckIfUserDisabledDelegate (object sender, EventArgs args)
		{
			CheckIfUserDisabled ();
		}

		public static bool CheckIfUserDisabled ()
		{
			ParseUser u = ParseUser.CurrentUser;
			string disabledAction;
			if (u == null || string.IsNullOrWhiteSpace (disabledAction = u.Get<string> (BlrtUserHelper.DisabledKey, null)))
				return false;

			LLogger.WriteEvent ("credentials", "User disabled is NOT NULL");

			BlrtManager.Logout (true);

			BlrtCloudNotification notification = null;
			try {
				notification = JsonConvert.DeserializeObject<BlrtCloudNotification> (disabledAction);

				Responder.ShowNotification (notification, true);
			} catch (JsonException exception) {
				LLogger.WriteEvent ("credentials", "Failed to deserialise user disability, aborting logout: " + exception.Message);
			}

			return true;
		}

		public static void CheckForNotifications (object sender, EventArgs args)
		{
			InnerCheckForNotifications ();
		}

		const int MaxRetry = 3;

		private static void InnerCheckForNotifications (int retried = 0)
		{
			if (retried > MaxRetry) {
				return;
			}
			ParseUser u;
			if ((u = ParseUser.CurrentUser) != null) {
				string action = null;
				string expireKey = null;
				int minExpire = int.MaxValue;


				if ((action = u.Get<string> (BlrtUserHelper.ExpiredActionKey, null)) == null) {
					try {
						foreach (var key in u.Keys) {
							if (u [key] == null)
								continue;


							if (key.StartsWith (BlrtUserHelper.ExpiresInPrefixKey)) {
								try {
									int days;
									if ((days = Convert.ToInt32 (key.Substring (BlrtUserHelper.ExpiresInPrefixKey.Length))) < minExpire) {
										minExpire = days;

										action = u.Get<string> (key, null);
										expireKey = key;
									}
								} catch (FormatException) {
								}
								;
							}
						}
					} catch {
						InnerCheckForNotifications (retried + 1);
						return;
					}

				}

				if (action != null && action != "true") {
					BlrtCloudNotification notification = null;
					try {
						notification = JsonConvert.DeserializeObject<BlrtCloudNotification> (action);
					} catch (JsonException exc) {
						LLogger.WriteEvent ("credentials", "Failed to deserialise notification");
						return;
					}

					u [BlrtUserHelper.ExpiredActionKey] = null;

					if (expireKey != null)
						u [expireKey] = "true";

					Responder.ShowNotification (notification, true);
				}
			}
		}

		public static bool Login (object user, bool isNewUser, bool signupViaSMSLink)
		{
			//suspended user should not be able to login
			if (!string.IsNullOrWhiteSpace (BlrtUserHelper.AccountStatus)) {
				var status = BlrtUserHelper.AccountStatus;
				ParseUser.LogOut ();
#if __IOS__
				BlrtiOS.FBManager.Logout ();
#else
				Xamarin.Facebook.Login.LoginManager.Instance.LogOut ();
#endif
				BlrtData.ExecutionPipeline.IAlert alert;
				alert = new SimpleAlert (
					BlrtUtil.PlatformHelper.Translate ("account_status_title_" + status, "profile"),
					BlrtUtil.PlatformHelper.Translate ("account_status_message_" + status, "profile"),
					BlrtUtil.PlatformHelper.Translate ("account_status_cancel_" + status, "profile")
				);
				BlrtManager.App.ShowAlert (BlrtData.ExecutionPipeline.Executor.System (), alert);
				return false;
			}


			var parseUser = user as ParseUser;

			BlrtConstants.Directories.CreateAll ();

			LLogger.UserId = parseUser.ObjectId;
			LLogger.WriteEvent ("credentials", "Login");

			Contacts.BlrtContactManager.OpenContactManager ();

#if __ANDROID__
			Contacts.ContactManagerDroid.RefreshContacts ();
#endif

			BlrtUtil.SetConnectionHasProblem (false);

			_control._userData.LoadData ();
			_control._query.Reset ();
			_control._upload.Reset ();

			_control._independentPrivateData.ClearLocal ();
			_control._independentPrivateData.SaveLocal ();

			Query.TryRunQuery ();

			_control._responder.Login (isNewUser, true, signupViaSMSLink);
			_control._responder.RecalulateAppBadge ();

			BlrtRecentFileManager.RefreshFileInfo ();

			BlrtEventManager.Reload ();
			BlrtUserHelper.IdentifyUser ();
			UserOrganisationRelation.UpdateCurrent ().FireAndForget ();

			BlrtPersistant.Properties ["lastSignInAs"] = parseUser.Email;
			BlrtPersistant.SaveProperties ();

			BlrtHousekeeper.Start ();



			return true;
		}

		public static async Task OpenHyperlink (string url)
		{
			Responder.PushLoading (true);

			AppLink result = null;

			string protocal = url.Remove (url.IndexOf (":"));

			try {
				var timeToWait = Xamarin.Forms.Device.OS == Xamarin.Forms.TargetPlatform.iOS ? 5 : 15;
				switch (protocal.ToLower ()) {
				case "http":
				case "https":
					result = await BlrtUtil.AwaitOrCancelTask (
						new HttpClientAppLinkResolver ().ResolveAppLinks (url),
						new CancellationTokenSource (timeToWait * 1000).Token
					);
					break;
				}
			} catch {
			} finally {
				Responder.PopLoading (true);
			}

			if (result != null) {
				var navigator = new AppLinkNavigator ();
				navigator.WillNavigateToWebUrl += (object sender, WebNavigateEventArgs e) => {

					Responder.OpenWebUrl (e.WebUrl.AbsolutePath, true);
					e.Handled = true;
				};

				var status = await navigator.Navigate (result);

				if (status == NavigationResult.Failed) {
					Responder.OpenWebUrl (url, true);
				}
			} else {
				Responder.OpenWebUrl (url, false);
			}
		}


		public static async void Logout (bool animated)
		{
			BlrtConstants.Directories.CreateAll ();

			// to trigger the refresh upon next login, reset last update date
			BlrtData.Models.Mailbox.MailboxInboxFilter.ResetLastUpdatedDate (BlrtManager.App, null);

			LLogger.WriteEvent ("credentials", "Logout");
			LLogger.UserId = null;
			var tcs = new TaskCompletionSource<bool> ();
			Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
				BlrtData.Helpers.BlrtTrack.AccountSignOut ();
				tcs.SetResult (true);
			});

			await tcs.Task;

			Contacts.BlrtContactManager.CloseContactManager ();
			try {
				Query.TerminateQueries ();
				Query.Reset ();
				Query.TryRunQuery ();
			} catch { }
			_control._upload.Reset ();

			try {
				if (ParseUser.CurrentUser != null) {
					await ParseUser.LogOutAsync ();
				}
			} catch { }

			_control._responder.Logout (animated);

			_control._responder.RecalulateAppBadge ();

			BlrtDownloader.TerminateAllDownloads ();

			BlrtEventManager.Reload ();

			_control._independentPrivateData.LoadData ();

			BlrtUserHelper.IdentifyUser (true);
			UserOrganisationRelation.UpdateCurrent ();

		}


#if __ANDROID__
		public static string ShouldHandleActionSendAfterLoginFilePath;
		public static string ShouldHandleActionSendAfterLoginFileName;

		public static void CanHandleActionSend (string filePath, string fileName)
		{
			if (!string.IsNullOrEmpty (filePath)) {
				if (ParseUser.CurrentUser != null) {
					App.HandleActionSend (filePath, fileName);
					ShouldHandleActionSendAfterLoginFilePath = null;
					ShouldHandleActionSendAfterLoginFileName = null;
				} else {
					ShouldHandleActionSendAfterLoginFilePath = filePath;
					ShouldHandleActionSendAfterLoginFileName = fileName;
				}
			}
		}


		public static async Task<bool> HandleBlrtSendProcess ()
		{
			return await App.HandleBlrtSendProcess ();
		}
#endif

		public static bool ParseUrl (string url, bool fromInsideApp)
		{
			if (url.StartsWith (BlrtConstants.UrlProtocolWithSlashes)) {

				//var match = Regex.Match (url, "blrt://[\\.a-zA-Z0-9]*/([A-Za-z0-9]+)/([A-Za-z0-9]+)/?(.*)");

				string simpleUrl;
				if (url.StartsWith (BlrtConstants.UrlProtocolWithSlashes, StringComparison.OrdinalIgnoreCase)) {
					simpleUrl = url.Remove (0, BlrtConstants.UrlProtocolWithSlashes.Length - 1);
				} else {
					if (url.StartsWith ("http://", StringComparison.OrdinalIgnoreCase)) {
						simpleUrl = url.Remove (0, 6);
					} else {//https://
						simpleUrl = url.Remove (0, 7);
					}
				}

				bool worked = AppLinkHandler.BlrtParseUrl (simpleUrl, fromInsideApp);

				if (!worked) {
					simpleUrl = BlrtEncode.DecodeUrl (simpleUrl);
					worked = AppLinkHandler.BlrtParseUrl (simpleUrl, fromInsideApp);
				}

				return worked;

			} else {

#if DEBUG
				Console.WriteLine ("File type: " + Path.GetExtension (url).ToLower ());
#endif

				switch (Path.GetExtension (url).ToLower ()) {
				case ".png":
				case ".jpeg":
				case ".jpg":
				case ".gif":
				case ".pdf":
					if (Responder.CanOpenEvent (BlrtEventType.OpenImage)) {
						Responder.OpenMediaFromUrl (url);
					}
					return true;
				}
			}

			return false;
		}

		public static bool ShouldNavigateTextLink (string url)
		{
			return url.StartsWith (BlrtConstants.ServerUrl, StringComparison.OrdinalIgnoreCase) ||
			   url.StartsWith (BlrtConstants.WebappUrl, StringComparison.OrdinalIgnoreCase);
		}

		public static async Task ApplyPermission (IPermissionHandler permissinHandler, bool showLoading = true)
		{

			if (permissinHandler == null)
				throw new ArgumentNullException ();

			bool loading = permissinHandler.WillDelay;

			if (loading && showLoading)
				BlrtManager.Responder.PushLoading (true);

			await permissinHandler.ApplyPermissions ();

			if (loading && showLoading)
				BlrtManager.Responder.PopLoading (true);
		}


#if __IOS__
		public static async Task<bool> ParsePush(BlrtPushState state, IDictionary<string, object> pushData)
		{
			if (state == BlrtPushState.Background || state ==  BlrtPushState.InApp) {
				// both of these states are dealt with the same way as both of them don't explicitly open anything
				BlrtManager.App.AddPushNotification (pushData);
			} else {
				await PushHandler.ParsePush (state, pushData);
			}

			_control._responder.AppReceivedPush ();
			return false;
		}
#endif
#if __ANDROID__
		public static async Task<bool> ParsePush (BlrtPushState state, IDictionary<string, object> pushData)
		{
			await PushHandler.ParsePush (state, pushData);
			_control._responder.AppReceivedPush ();
			return false;
		}
#endif


		public static void RecalulateAppBadge(){
			_control._responder.RecalulateAppBadge ();
		}

		static DateTime _lastExpiryTime = new DateTime (1970, 1, 1);
		static int _invalidSessionHandleLock = 0;
		const int MinExpiryDurationHour = 2;
		public static async Task<bool> HandlePotentialInvalidSessionException(object exceptionObj)
		{
			if (exceptionObj is ParseException) {
				var parseException = exceptionObj as ParseException;
				if ((null != parseException) && (parseException.Code == ParseException.ErrorCode.InvalidSessionToken)) {

#if __IOS__
					var expiryDurationGraterThanMin = ((DateTime.UtcNow - _lastExpiryTime).TotalHours > MinExpiryDurationHour);
					var invalidSessionHandleLocked = (0 == Interlocked.Exchange (ref _invalidSessionHandleLock, 1));
					if (expiryDurationGraterThanMin && invalidSessionHandleLocked) {
						try {
							_lastExpiryTime = DateTime.UtcNow;
							// when session expires, we we treat is as the deleted user case as sessions expires after a long time.
							// Hence, it makes sense to remove cache and fetch everything again.
							PushHandler.ForceUserLogout (ExecutionPipeline.Executor.System (), BlrtUser.DELETED_ACCOUNT_STATUS_STRING, BlrtPushState.Background);
							/*if (!string.IsNullOrWhiteSpace (BlrtUserHelper.AccountStatus)) {
								    

							} else {
								await UserDialogs.Instance.AlertAsync (
									BlrtUtil.PlatformHelper.Translate ("Please log in again."),
									BlrtUtil.PlatformHelper.Translate ("Your session has expired"),
									BlrtUtil.PlatformHelper.Translate ("OK")
								);
								Xamarin.Forms.Device.BeginInvokeOnMainThread (
									() => {
										Logout (true);
									}
								);
							}*/
						} finally {
							Interlocked.Exchange (ref _invalidSessionHandleLock, 0);
						}

#endif

#if __ANDROID__
					if ((0 == Interlocked.Exchange (ref _invalidSessionHandleLock, 1)) && ((DateTime.UtcNow - _lastExpiryTime).TotalHours > MinExpiryDurationHour)) {
						_lastExpiryTime = DateTime.UtcNow;

						await UserDialogs.Instance.AlertAsync (
							BlrtUtil.PlatformHelper.Translate ("Please log in again."),
							BlrtUtil.PlatformHelper.Translate ("Your session has expired"),
							BlrtUtil.PlatformHelper.Translate ("OK")
						);

						Logout (true);
						Interlocked.Exchange (ref _invalidSessionHandleLock, 0);
#endif

					}
					return true;
				}
			}
			return false;
		}
	}
}
