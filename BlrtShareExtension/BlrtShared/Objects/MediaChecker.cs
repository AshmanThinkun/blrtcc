using System;
using System.Collections.Generic;
using System.Collections;
using System.Timers;
using BlrtShared;
using BlrtData.IO;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Linq;

namespace BlrtData
{
	public class MediaChecker
	{
		private Dictionary<string, Checker> _dictionary;
		ReaderWriterLockSlim _dictionaryLock = new ReaderWriterLockSlim ();

		private MediaChecker ()
		{
			_dictionary = new Dictionary<string, Checker> ();
		}

		private static MediaChecker _mediaChecker;

		public static MediaChecker GetMediaChecker ()
		{
			if (_mediaChecker == null)
				_mediaChecker = new MediaChecker ();
			return _mediaChecker;
		}

		public void Subscribe (string name, IMediaCheckerListener subscriber)
		{
			if (null == subscriber) {
				throw new ArgumentNullException ("subscriber");
			}
			Checker checker = null;
			using (_dictionaryLock.DisposableWriteLock ()) {
				if (!_dictionary.TryGetValue (name, out checker)) {
					checker = new Checker (name);
					_dictionary.Add (name, checker);
				}
				checker.Add (subscriber);
			}
		}

		public void UnSubscribe (string name, IMediaCheckerListener subscriber)
		{
			if (null == subscriber) {
				throw new ArgumentNullException ("subscriber");
			}

			Checker checker = null;
			using (_dictionaryLock.DisposableWriteLock ()) {
				if (_dictionary.TryGetValue (name, out checker)) {
					checker.Remove (subscriber);
					if (checker.IsEmpty) {
						_dictionary.Remove (name);
					}
				}
			}
		}

		public CheckerState GetState (string path)
		{
			if (string.IsNullOrWhiteSpace (path)) {
				throw new ArgumentNullException ("path");
			}

			Checker checker = null;
			using (_dictionaryLock.DisposableReadLock ()) {
				if (_dictionary.TryGetValue (path, out checker)) {
					return checker.State;
				} else {
					return GetFileState (path);
				}
			}
		}

		static CheckerState GetFileState(string path)
		{
			if (BlrtDownloader.IsDownloadingToFile (path)) {
				return CheckerState.DownloadingContent;
			} else if (BlrtUtil.CanUseFile (path)) {
				return CheckerState.Synced;
			} else {
				return CheckerState.ContentMissing;
			}
		}

		static IFileWatcherPlatformHelper _fileWatcherCreator;
		static IFileWatcher CreateFileWatcher (string path)
		{
			if (null == _fileWatcherCreator) {
				_fileWatcherCreator = Xamarin.Forms.DependencyService.Get<IFileWatcherPlatformHelper> ();
			}
			return _fileWatcherCreator.CreateWatcher (path);
		}

		class Checker : IDisposable, BlrtDownloader.IDownloadStateListener
		{
			public CheckerState State { get; private set; }
			public string Path { get; private set; }

			ConditionalHashSet<IMediaCheckerListener> _subsciberList;
			ReaderWriterLockSlim _listLock = new ReaderWriterLockSlim ();
			IFileWatcher _fileWatcher;

			public Checker (string path)
			{
				if (string.IsNullOrWhiteSpace (path)) {
					throw new ArgumentNullException("path");
				}
				Path = path;
				_subsciberList = new ConditionalHashSet<IMediaCheckerListener> ();
				_fileWatcher = CreateFileWatcher(Path);
				_fileWatcher.FileExistanceChanged += OnFileExistanceChanged;
				BlrtDownloader.Subscribe (Path, this);
				State = GetFileState (Path);
			}

			public void Add (IMediaCheckerListener listener)
			{
				using (_listLock.DisposableWriteLock ()) {
					if (!_subsciberList.Contains (listener)) {
						_subsciberList.Add (listener);
					}
				}
			}

			public void Remove (IMediaCheckerListener listener)
			{
				using (_listLock.DisposableWriteLock ()) {
					_subsciberList.Remove (listener);
				}
			}

			public bool IsEmpty { get {return _subsciberList.Count == 0;} }

			void NotifyStateChanged ()
			{
				IMediaCheckerListener[] targetList = null;
				using (_listLock.DisposableReadLock ()) {
					targetList = _subsciberList.ToArray ();
				}
				foreach (var listener in targetList) {
					listener?.PathStateChanged (Path);
				}
				targetList = null;
			}

			void OnFileExistanceChanged (object sender, bool exists)
			{
				// we ignore the file creation event, because we are using downloader's download finish event
				if (!exists) {
					State = CheckerState.ContentMissing;
					NotifyStateChanged ();
				}
			}

			#region IDownloadStateListener implementation

			public void NotifyDownloadStarted (string filePath)
			{
				if (Path == filePath) {
					State = CheckerState.DownloadingContent;
					NotifyStateChanged ();
				}
			}

			public void NotifyDownloadFinished (string filePath, bool succeeded)
			{
				if (Path == filePath) {
					State = (succeeded && BlrtUtil.CanUseFile (Path)) ? CheckerState.Synced : CheckerState.ContentMissing;
					NotifyStateChanged ();
				}
			}

			#endregion

			#region IDisposable implementation

			public void Dispose ()
			{
				if (null != _fileWatcher) {
					_fileWatcher.FileExistanceChanged -= OnFileExistanceChanged;
					_fileWatcher.Dispose ();
					_fileWatcher = null;
				}
				BlrtDownloader.UnsubscribeFile (Path, this);
			}

			#endregion
		}
	}

	public interface IMediaCheckerListener
	{
		void PathStateChanged (string path);
	}

	public enum CheckerState
	{
		DownloadingContent,
		ContentMissing,
		Synced
	}

	public sealed class ConditionalHashSet<T> where T : class
	{
		private readonly object locker = new object();
		private readonly List<WeakReference> weakList = new List<WeakReference>();
		private readonly ConditionalWeakTable<T, WeakReference> weakDictionary =
			new ConditionalWeakTable<T, WeakReference>();

		public void Add(T item)
		{
			lock (this.locker)
			{
				var reference = new WeakReference(item);
				this.weakDictionary.Add(item, reference);
				this.weakList.Add(reference);
				this.Shrink();
			}
		}

		public void Remove(T item)
		{
			lock (this.locker)
			{
				WeakReference reference;

				if (this.weakDictionary.TryGetValue(item, out reference))
				{
					reference.Target = null;
					this.weakDictionary.Remove(item);
				}
			}
		}

		public T[] ToArray()
		{
			lock (this.locker)
			{
				return (
					from weakReference in this.weakList
					let item = (T)weakReference.Target
					where item != null
					select item)
						.ToArray();
			}
		}

		public bool Contains(T item)
		{
			WeakReference reference;

			return (this.weakDictionary.TryGetValue (item, out reference));
		}

		public int Count{
			get{
				lock (this.locker)
				{
					var arr =  (
						from weakReference in this.weakList
						let item = (T)weakReference.Target
						where item != null
						select item)
							.ToArray();
					return arr.Length;
				}
			}
		}

		private void Shrink()
		{
			// This method prevents the List<T> from growing indefinitely, but 
			// might also cause  a performance problem in some cases.
			if (this.weakList.Capacity == this.weakList.Count)
			{
				this.weakList.RemoveAll(weak => !weak.IsAlive);
			}
		}
	}
}

