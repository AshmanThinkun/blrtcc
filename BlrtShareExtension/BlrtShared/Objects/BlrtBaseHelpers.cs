using System.Linq;
using System.Collections.Generic;
using Parse;

namespace BlrtData
{
	public static class BlrtBaseHelpers
	{
		public static string[] GetDisplayNames (this IEnumerable<ConversationUserRelation> relations)
		{
			List<string> output = new List<string> ();

			foreach (var r in relations) {

				if (r.BondCreated) {

					var str = r.LocalStorage.DataManager.GetUser (r.UserId);
					output.Add (str.DisplayName);

				} else {
					output.Add (r.QueryName);
				}			
			}

			output.Sort ();

			return output.ToArray ();
		}

		public static string GetNewItemsDisplayNames (this ConversationUserRelation relation)
		{
			if (relation.ViewedList.Count == relation.Conversation.ReadableCount)
				return "";
			var users = new List<string> ();
			var contents = relation.LocalStorage.DataManager.GetConversationItems (relation.Conversation);

			for (int i = 0; i < relation.Conversation.ReadableCount; ++i) {

				if (!relation.ViewedList.Contains (i)) {
					var item = contents.FirstOrDefault (u => (u.ReadableIndex == i));

					if (item != null) {

						var user = relation.LocalStorage.DataManager.GetUser (item.CreatorId);

						if (!users.Contains (user.DisplayName))
							users.Add (user.DisplayName);					
					}
				}
			}

			if (users.Count > 0)
				return string.Join (", ", users);

			relation.Conversation.NeedsRefresh = relation.Conversation.NeedsMetaUpdate = true;
			return "...";
		}



		public static ConversationProblemType HasProblem (this Conversation conversation)
		{
			if (!conversation.OnParse)
				return ConversationProblemType.ConversationUnsynced;

			if (conversation.LocalStorage.DataManager.GetUnsyncedConversationItemCount (conversation) > 0)
				return ConversationProblemType.ContentUnsynced;

			if (conversation.BondCount == 1)
				return ConversationProblemType.Unsent;

			return ConversationProblemType.None;
		}




		public static string[] GetAllKnownTags ()
		{
			if (null == ParseUser.CurrentUser) {
				return new string[0];
			}
			return BlrtManager.DataManagers.Default.GetConversationUserRelations (ParseUser.CurrentUser.ObjectId)
				.SelectMany (rel => rel.Tags)
				.Select (reltag => reltag.ToLower ())
				.Distinct ()
				.OrderBy (tag => tag)
				.ToArray ();
		}


		public static string[] GetUserInConversations (this IEnumerable<Conversation> conversations)
		{
			return conversations
				.SelectMany (c => c.GetUserInConversation ())
				.Distinct ()
				.ToArray ();
		}
	}



	public enum ConversationProblemType
	{
		None,
		ConversationUnsynced,
		ContentUnsynced,
		Unsent
	
	}
}

