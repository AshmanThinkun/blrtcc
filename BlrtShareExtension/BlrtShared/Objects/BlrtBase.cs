using System;
using System.ComponentModel;
using System.Collections.Generic;
using Parse;
using System.IO;

namespace BlrtData
{
	[Serializable]
	public abstract class BlrtBase : IEquatable<BlrtBase>, INotifyPropertyChanged
	{
		public abstract string ClassName { get; }
		public abstract int CurrentVersion { get; }
		public abstract int OldestSupportedVersion { get; }

		public const string VERSION_KEY 		= "dbversion";
		public const string VERSION_CREATED_KEY = "dbversionCreated";
		public const string CREATOR_KEY 		= "creator";
		public const string DELETED_AT_KEY 		= "deletedAt";
		public const string ENCRYPT_TYPE_KEY 	= "encryptType";
		public const string ENCRYPT_VALUE_KEY 	= "encryptValue";
		public const string LOCAL_GUID_KEY	 	= "localGuid";
		public const string PERMISSIONS 		= "permissionsIds";


		#region Boring property change stuff

		[NonSerialized]
		private PropertyChangedEventHandler _propertyChanged;
		public event PropertyChangedEventHandler PropertyChanged { add {_propertyChanged += value;} remove {_propertyChanged -= value;}}
		[NonSerialized]
		private int _pauseStack = 0;
		[NonSerialized]
		private List<string> _list;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (_pauseStack > 0) {
			
				if (!_list.Contains (e.PropertyName))
					_list.Add (e.PropertyName);
				return;
			}

			var handler = _propertyChanged;
			if (handler != null)
				handler(this, e);

		}

		protected void OnPropertyChanged(string propertyName)
		{
			OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}

		public void PushPausePropertyChanged(){
			_pauseStack++;

			if (_list == null)
				_list = new List<string> ();
		}

		public void PopPausePropertyChanged() {
			_pauseStack--;

			if (_list == null)
				_list = new List<string> ();

			if (_pauseStack <= 0) {
				if (_list.Count > 0) {
					OnPropertyChanged (new PropertyChangedEventArgs (string.Join (",", _list)));
				}
				_list.Clear ();
			}
		}

		#endregion

		private string 		_objectId;

		private int 		_version;
		private int 		_versionCreated;
		private string 		_creatorId;

		private DateTime 	_createdAt;
		private DateTime 	_updatedAt;
		private DateTime? 	_deleteAt;

		private DateTime? 	_changedAt;



		private bool 		_onParse;
		private DateTime 	_lastRetrieved;

		[NonSerialized]
		private DateTime?		_sessionSyncedAt;
		[NonSerialized]
		private bool 		_shouldUpload;


		private int 		_encryptType;
		private string 		_encryptValue;


		private string 		_localGuid;

		private LocalStorageType _localStorageType;

		public virtual string ObjectId { get { 
				if (string.IsNullOrEmpty (_objectId)) {
					return LocalGuid;
				}
				return _objectId;
			} 
			set{ 
				if (value != _objectId) {
					_objectId = value;
					OnPropertyChanged("ObjectId");
				}
			} 
		}


		public virtual bool 	Dirty				{ get { return ChangedAt != null; } set { if(value) ChangedAt = DateTime.Now; else ChangedAt = null; } } 	// Whether or not the Blrt should be updated
		 	// Whether or not the Blrt should be updated
		
		public virtual int 			Version {
			get {
				return _version;
			}
			set { 
				if (value != _version) {
					_version = value;
					OnPropertyChanged ("Version");
				}
			}
		}
		public virtual int 			VersionCreated {
			get {
				return _versionCreated;
			}
			set { 
				if (value != _versionCreated) {
					_versionCreated = value;
					OnPropertyChanged ("VersionCreated");
				}
			}
		}
		public virtual string 		CreatorId {
			get {
				return _creatorId;
			}
			set { 
				if (value != _creatorId) {
					_creatorId = value;
					OnPropertyChanged ("CreatorId");
				}
			}
		}

		public virtual DateTime		CreatedAt{
			get {
				return _createdAt;
			}
			set { 
				if (value != _createdAt) {
					_createdAt = value;
					OnPropertyChanged ("CreatedAt");
				}
			}
		}
		public virtual DateTime		UpdatedAt{
			get {
				return _updatedAt;
			}
			set { 
				if (value != _updatedAt) {
					_updatedAt = value;
					OnPropertyChanged ("UpdatedAt");
				}
			}
		}
		public virtual int		EncryptType{
			get {
				return _encryptType;
			}
			set { 
				if (value != _encryptType) {
					_encryptType = value;
					OnPropertyChanged ("EncryptType");
				}
			}
		}

		public virtual DateTime		LastRetrieved{
			get {
				return _lastRetrieved;
			}
			set { 
				if (value != _lastRetrieved) {
					_lastRetrieved = value;
					OnPropertyChanged ("LastRetrieved");
				}
			}
		}

		public string EncryptValue {
			get {
				return _encryptValue;
			}
			set { 
				if (value != _encryptValue) {
					_encryptValue = value;
					OnPropertyChanged ("EncryptValue");
				}
			}
		}

		public string LocalGuid {
			get {
				return _localGuid;
			}
			set { 
				if (value != _localGuid) {
					_localGuid = value;
					OnPropertyChanged ("LocalGuid");
				}
			}
		}
		public virtual DateTime?	DeleteAt{
			get {
				return _deleteAt;
			}
			set { 
				if (value != _deleteAt) {
					_deleteAt = value;
					OnPropertyChanged ("DeleteAt");
				}
			}
		}
		public virtual DateTime?	ChangedAt	{
			get {
				return _changedAt;
			}
			set { 
				if (value != _changedAt) {

					if (_changedAt == null ^ (value == null)) {
						_changedAt = value;
						OnPropertyChanged ("Dirty");
					} else {
						_changedAt = value;
					}

					OnPropertyChanged ("ChangedAt");
				}
			}
		}
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="BlrtData.BlrtBase"/> has been synced during the current session.
		/// </summary>
		/// <value><c>true</c> if session synced; otherwise, <c>false</c>.</value>
		public virtual bool SessionSynced		{ 
			get {
				return SessionSyncedAt != null;
			}
		} 

		public virtual DateTime? SessionSyncedAt		{ 
			get {
				return _sessionSyncedAt;
			}
			set { 
				if (value != _sessionSyncedAt) {
					_sessionSyncedAt = value;
					OnPropertyChanged ("SessionSyncedAt");
				}
			}
		} 


		public virtual bool OnParse				{ 
			get {
				return _onParse && (IsIndependent || (!string.IsNullOrEmpty (_objectId) && !LocalStorage.DataManager.IsCacheId (_objectId)));
			}
			set { 
				if (value != OnParse) {
					_onParse = value;
					OnPropertyChanged ("OnParse");
				}
			}
		}

		public virtual bool 	Deleted		{ 
			get{
				return DeleteAt != null;
			}
			set{
				if (value && DeleteAt == null) 
				{
					DeleteAt = DateTime.Now;
					Dirty = true;				
				}
			}
		}


		public virtual bool 		ShouldUpload 					{ 
			get {
				return _shouldUpload && !OnParse;
			}
			set { 
				if (value != _shouldUpload && !OnParse) {
					_shouldUpload = value;
					OnPropertyChanged ("ShouldUpload");
				}
			}
		}
			
		public bool CreatedByCurrentUser { get { return (null != ParseUser.CurrentUser) && (CreatorId == ParseUser.CurrentUser.ObjectId); } }

		[Newtonsoft.Json.JsonIgnore]
		public bool IsIndependent { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public ILocalStorageParameter LocalStorage
		{ 
			get {
				return LocalStorageParameters.Get (_localStorageType);
			}
		}

		protected BlrtBase (LocalStorageType localStorageType = LocalStorageType.Default) : base()
		{
			_shouldUpload = false;
			_onParse = false;
			_sessionSyncedAt = null;
			this._objectId = null;
			this._versionCreated = CurrentVersion;
			this._version = CurrentVersion;

			this._creatorId = (ParseUser.CurrentUser == null) ? "" : ParseUser.CurrentUser.ObjectId;

			this._createdAt = DateTime.Now;
			this._updatedAt = DateTime.Now;
			this._deleteAt = null;

			this._localGuid = LocalStorage.DataManager.CreateUniqueLocalGuid ();


			this._encryptType = BlrtEncryptorManager.None;
			this.IsIndependent = false;

			this._localStorageType = localStorageType;
		}


		protected BlrtBase (ParseObject parseObject, LocalStorageType localStorageType = LocalStorageType.Default) : this(localStorageType)
		{
			ShouldUpload = false;
			this._objectId 		= parseObject.ObjectId;

			ApplyParseObject (parseObject);
		}


		public virtual void AfterDeserization(){
			_sessionSyncedAt 		= null;
			_shouldUpload 	= false;
		}

		public virtual void BeforeSerization(){
		}

		/// <summary>
		/// Writes current information to parse
		/// </summary>
		/// <returns>The to parse.</returns>
		/// <param name="parseObject">Parse object.</param>
		public virtual void CopyToParse(ref ParseObject parseObject)
		{
			if (parseObject == null)
				return;

			if (parseObject.ObjectId != null && parseObject.ObjectId.Length > 0 && parseObject.ObjectId != this.ObjectId)
				return;

			parseObject [VERSION_KEY] 			= _version;
			parseObject [DELETED_AT_KEY] 		= _deleteAt;
		}


		/// <summary>
		/// Write to the class from a parse object
		/// </summary>
		/// <param name="parseObject">Parse object.</param>
		public virtual void ApplyParseObject(ParseObject parseObject)
		{
			if (parseObject == null)
				return;

			PushPausePropertyChanged ();
			try{
				ShouldUpload = false;
				OnParse = true;

				this.ChangedAt = null;
				this.ObjectId = parseObject.ObjectId;
				SessionSyncedAt = LastRetrieved = DateTime.Now;
				
				VersionCreated 	= parseObject.Get<int>		(VERSION_CREATED_KEY, 	CurrentVersion);
				Version 		= parseObject.Get<int>		(VERSION_KEY, 			VersionCreated);
				EncryptType 	= parseObject.Get<int>		(ENCRYPT_TYPE_KEY,		BlrtEncryptorManager.None);
				EncryptValue 	= parseObject.Get<string>	(ENCRYPT_VALUE_KEY, 	"");
				LocalGuid 		= parseObject.Get<string>	(LOCAL_GUID_KEY,		"");

				var creator		= parseObject.Get<ParseUser> (CREATOR_KEY, null);
				CreatorId 		= creator != null ? creator.ObjectId : "";
				DeleteAt 		= parseObject.Get<DateTime?> (DELETED_AT_KEY, null);
				
				this.CreatedAt = BlrtUtil.ToLocalTime(parseObject.CreatedAt ?? new DateTime ());
				this.UpdatedAt = BlrtUtil.ToLocalTime(parseObject.UpdatedAt ?? new DateTime ());


			} finally {
				PopPausePropertyChanged ();
			}
		}


		/// <summary>
		/// Shoulds the delete data on start up
		/// </summary>
		/// <returns><c>true</c>, if delete data was shoulded, <c>false</c> otherwise.</returns>
		public virtual bool ShouldDeleteData()
		{
			return LastRetrieved.AddDays (30) < DateTime.Now && OnParse;
		}


		public virtual void DeleteData() 
		{
		}


		public bool IsStable()
		{
			return Version <= CurrentVersion;
		}


		public static int CompareTo(BlrtBase x, BlrtBase y)
		{
			return x.UpdatedAt.CompareTo (y.UpdatedAt);
		}

		public virtual BlrtBaseIssues GetIssues ()
		{
			var issues = BlrtBaseIssues.None;

			if (Version > CurrentVersion)
				issues = issues | BlrtBaseIssues.UnsupportedDBVersion;


			if (!BlrtEncryptorManager.CanDecypt(this))
				issues = issues | BlrtBaseIssues.UnsupportedEncryption;

			return issues;
		}


		public bool MatchKey(string key){
			return key == string.Format ("{0}-{1}", ClassName, ObjectId);
		}



		public bool Equals (BlrtBase other)
		{
			if (other == null) return false;
			return this == other || (ObjectId == other.ObjectId && ClassName == other.ClassName);
		}


		public virtual bool ShouldUpgrade () {
			return (Version < OldestSupportedVersion);
		}



		protected void UpdateFileState (object sender, EventArgs e)
		{
			if (sender is IO.BlrtDownloader) {
				var d = sender as IO.BlrtDownloader;

				d.OnDownloadFailed -= UpdateFileState;
				d.OnDownloadFinished -= UpdateFileState;
			}

			OnPropertyChanged ("FileState");
		}

		public abstract bool TryUpgrade ();
	}

}

