﻿using System;
using Parse;

namespace BlrtData
{
	[Serializable]
	public class OCListItem:BlrtBase
	{
		#region implemented abstract members of BlrtBase
		public const string CLASS_NAME 			= "OCListItem";
		public const int CURRENT_VERSION     = 0 ;
		public override bool TryUpgrade ()
		{
			return false;
		}
		public override string ClassName {
			get {
				return CLASS_NAME;
			}
		}
		public override int CurrentVersion {
			get {
				return 0;
			}
		}
		public override int OldestSupportedVersion {
			get {
				return 0;
			}
		}
		#endregion


		public string OCListId{ get; set;}
		public string BlrtId{ get; set;}
		public float Index{ get; set;}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
			if (parseObject == null)
				return;
			this.OCListId = parseObject.Get<ParseObject> ("oclist", null)?.ObjectId;
			this.BlrtId = parseObject.Get<ParseObject> ("blrt", null)?.ObjectId;
			this.Index = parseObject.Get<float> ("index", 0);
		}
	}
}

