using System;
using System.Linq;
using System.Collections.Generic;
using Parse;

namespace BlrtData
{
	[Serializable]
	public sealed class  ConversationUserRelation : BlrtBase
	{
		public const string CLASS_NAME 				= "ConversationUserRelation";
		public override string ClassName { get { return CLASS_NAME; } }

		public const int CURRENT_VERSION = 6;
		public override int CurrentVersion { get { return CURRENT_VERSION; } }

		public const int OLDEST_SUPPORTED_VERSION = 6;
		public override int OldestSupportedVersion { get { return OLDEST_SUPPORTED_VERSION; } }
		
		public const string USER_KEY 				= "user";
		public const string CONVERSATION_KEY 		= "conversation";
		public const string CONVERSATION_CONTENTUPDATE_KEY 		= "contentUpdate";

		public const string QUERY_VALUE_KEY 		= "queryValue";
		public const string QUERY_TYPE_KEY 			= "queryType";
		public const string QUERY_NAME_KEY 			= "queryName";

		public const string TAGS_KEY				= "tags";
		public const string INDEX_VIEWED_LIST_KEY	= "indexViewedList";

		public const string LAST_VIEWED_KEY			= "lastViewed";
		public const string LAST_EDITED_KEY			= "lastEdited";

		public const string ARCHIVED_KEY			= "archived";
		public const string FLAGGED_KEY				= "flagged";
		public const string MUTEFLAG_KEY			= "muteFlag";

		public const string ALLOW_IN_INBOX_KEY			= "allowInInbox";
		public const string FLAGGED_FOR_EMAIL_KEY	= "flaggedForCommentEmailAt";



		public const string HAS_COMMENT_KEY			= "hasComment";
		public const string HAS_MADE_BLRT_KEY		= "hasMadeBlrt";
		public const string HAS_MADE_REBLRT_KEY		= "hasMadeReBlrt";
		public const string HAS_MADE_REQUEST_KEY 	= "hasMadeRequest";
		public const string HAS_INVITED_PEOPLE_KEY	= "hasInvitedPeople";

		public const string UNREAD_MESSAGES_KEY		= "unreadMessages";

		#region properties and fields
		string					_userId;
		string					_conversationId;

		List<int> 				_unreadMessages;

		List<int>				_viewedList;
		[NonSerialized]
		int[] _viewedListArray;

		string[]				_tags;

		DateTime				_lastViewed;
		DateTime				_lastEdited;

		int 					_muteFlag;
		bool					_archive;
		bool 					_flagged;

		string 					_queryValue;
		string 					_queryType;
		string					_queryName;

		bool					_hasComment;
		bool					_hasMadeBlrt;
		bool					_hasMadeReBlrt;
		bool					_hasMadeRequest;
		bool					_hasInvitedPeople;

		RelationPermissions 	_permissions;

		public string 		UserId {
			get {
				return _userId;
			}
			set { 
				if (value != _userId) {
					_userId = value;
					OnPropertyChanged ("UserId");
				}
			}
		}
		public string 		ConversationId {
			get {
				return _conversationId;
			}
			set { 
				if (value != _conversationId) {
					_conversationId = value;
					OnPropertyChanged ("ConversationId");
				}
			}
		}

		public List<int>	ViewedList {
			get {
				return _viewedList;
			}
		}

		public List<int>	UnreadMessages {
			get {
				return _unreadMessages;
			}
		}


		public string[]	Tags {
			get {
				return _tags;
			}
			set { 
				if (value != _tags) {
					_tags = value;
					OnPropertyChanged ("Tags");
				}
			}
		}

		public DateTime		LastViewed {
			get {
				return _lastViewed;
			}
			set { 
				if (value != _lastViewed) {
					_lastViewed = value;
					OnPropertyChanged ("LastViewed");
				}
			}
		}
		public DateTime		LastEdited {
			get {
				return _lastEdited;
			}
			set { 
				if (value != _lastEdited) {
					_lastEdited = value;
					OnPropertyChanged ("LastEdited");
				}
			}
		}

		public int MuteFlag{
			get{
				return _muteFlag;
			}
			set{
				if (value != _muteFlag) {
					_muteFlag = value;
					OnPropertyChanged ("MuteFlag");
				}
			}
		}

		public bool 		Archive	 {
			get {
				return _archive;
			}
			set { 
				if (value != _archive) {
					_archive = value;
					OnPropertyChanged ("Archive");
				}
			}
		}
		public bool 		Flagged {
			get {
				return _flagged;
			}
			set { 
				if (value != _flagged) {
					_flagged = value;
					OnPropertyChanged ("Flagged");
				}
			}
		}

		public string 		QueryValue {
			get {
				return _queryValue;
			}
			set { 
				if (value != _queryValue) {
					_queryValue = value;
					OnPropertyChanged ("QueryValue");
				}
			}
		}
		public string 		QueryType {
			get {
				return _queryType;
			}
			set { 
				if (value != _queryType) {
					_queryType = value;
					OnPropertyChanged ("QueryType");
				}
			}
		}
		public string 		QueryName {
			get {
				return _queryName;
			}
			set { 
				if (value != _queryName) {
					_queryName = value;
					OnPropertyChanged ("QueryName");
				}
			}
		}

		public bool 		HasComment {
			get {
				return _hasComment;
			}
			set { 
				if (value != _hasComment) {
					_hasComment = value;
					OnPropertyChanged ("HasComment");
				}
			}
		}
		public bool 		HasMadeBlrt	 {
			get {
				return _hasMadeBlrt;
			}
			set { 
				if (value != _hasMadeBlrt) {
					_hasMadeBlrt = value;
					OnPropertyChanged ("HasMadeBlrt");
				}
			}
		}
		public bool 		HasMadeReBlrt {
			get {
				return _hasMadeReBlrt;
			}
			set { 
				if (value != _hasMadeReBlrt) {
					_hasMadeReBlrt = value;
					OnPropertyChanged ("HasMadeReBlrt");
				}
			}
		}
		public bool 		HasMadeRequest {
			get {
				return _hasMadeRequest;
			}
			set { 
				if (value != _hasMadeRequest) {
					_hasMadeRequest = value;
					OnPropertyChanged ("HasMadeRequest");
				}
			}
		}
		public bool 		HasInvitedPeople {
			get {
				return _hasInvitedPeople;
			}
			set { 
				if (value != _hasInvitedPeople) {
					_hasInvitedPeople = value;
					OnPropertyChanged ("HasInvitedPeople");
				}
			}
		}
		public RelationPermissions Permissions {
			get {
				return _permissions;
			}
			set { 
				if (value != _permissions) {
					_permissions = value;
					OnPropertyChanged ("Permissions");
				}
			}
		}
		#endregion

		public bool PermissionsValid { get { return _placeHolder || ((Conversation != null && Conversation.CreatedByCurrentUser && !Conversation.HasConversationBegan) ||  (Permissions != null && Permissions.Valid)); } }
		public bool PermissionsNeedsUpdate { get { return !_placeHolder && (IsCurrentUsers && (Permissions == null || Permissions.NeedsUpdate)); } }


		private bool _placeHolder;
		public bool IsPlaceholder { get{ return _placeHolder; } }




		public bool HasTags { get { return Tags != null && Tags.Length > 0; } }

		public bool HasViewedConversation { get{ return ViewedList.Count > 0; } }

		public string TagsToString ()
		{
			return string.Join (", ", Tags); 
		}

		public string GetUserDisplayName ()
		{
			if (string.IsNullOrWhiteSpace (UserId))
				return QueryName;

			return LocalStorage.DataManager.GetUser (UserId).DisplayName;
		}

		public string GetUserContactInfo ()
		{
			if (string.IsNullOrWhiteSpace (UserId) && QueryType == "email")
				return string.Format("Email: {0}", QueryValue);

			var user = LocalStorage.DataManager.GetUser (UserId);


			return user.ContactInfo;
		}

		public string GetUserEmail ()
		{
			if (string.IsNullOrWhiteSpace (UserId) && QueryType == "email")
				return QueryValue;

			var user = LocalStorage.DataManager.GetUser (UserId);

			return user.Email;
		}

		[NonSerialized]
		private Conversation _conversation;

		public Conversation Conversation {
			get{
				if (_conversation == null)
					_conversation = LocalStorage.DataManager.GetConversation (ConversationId);

				return _conversation; 
			}
		}



		public bool 		BondCreated			{	get { return !string.IsNullOrEmpty(UserId); }	}
		public bool 		HasContributed		{	get { return HasComment || HasMadeBlrt || HasMadeReBlrt || HasMadeRequest; } }

		public bool IsCurrentUsers { get { return ParseUser.CurrentUser != null && UserId == ParseUser.CurrentUser.ObjectId; } }

		public ConversationUserRelation () : base()
		{
			this.UserId	 			= "";
			this.ConversationId	 	= "";

			this._viewedList	 	= new List<int>();
			this._unreadMessages 	= new List<int> ();
			this.Tags 				= new string[0];

			Archive = HasComment = HasMadeBlrt = HasMadeReBlrt = HasMadeRequest = HasInvitedPeople	= false;
		}

		public ConversationUserRelation (ParseObject parseObject) : base(parseObject)
		{
		}

		public override void CopyToParse(ref ParseObject parseObject)
		{
			if (parseObject == null) {
				parseObject = new ParseObject (CLASS_NAME);
				if(ObjectId != null && ObjectId.Length > 0)
					parseObject.ObjectId = ObjectId;
			}

			if (parseObject.ClassName != CLASS_NAME 
				|| (parseObject.ObjectId != null && parseObject.ObjectId.Length > 0 && ObjectId.Length > 0 && parseObject.ObjectId != this.ObjectId))
				return;

			base.CopyToParse (ref parseObject);

			parseObject [INDEX_VIEWED_LIST_KEY] 		= BlrtUtil.ListToPages(ViewedList);
			parseObject.RemoveAllFromList (UNREAD_MESSAGES_KEY, parseObject.Get<IList<int>>(UNREAD_MESSAGES_KEY,new List<int>()).Where(x => !UnreadMessages.Contains(x)));

			parseObject [ARCHIVED_KEY]					= Archive;
			parseObject [MUTEFLAG_KEY]					= MuteFlag;
			parseObject [FLAGGED_KEY]					= Flagged;
			parseObject [TAGS_KEY]						= new List<object>(Tags);
			parseObject [LAST_VIEWED_KEY] 				= BlrtUtil.ToCloudTime(LastViewed);
			parseObject [LAST_EDITED_KEY] 				= BlrtUtil.ToCloudTime(LastEdited);

			parseObject [FLAGGED_FOR_EMAIL_KEY] 		= null;

		}
		
		public override void ApplyParseObject(ParseObject parseObject)
		{
			if (parseObject == null) {
				return;
			}

			base.ApplyParseObject (parseObject);

			this.QueryType				= parseObject.Get<string>	( QUERY_TYPE_KEY, 			"" );
			this.QueryValue				= parseObject.Get<string>	( QUERY_VALUE_KEY, 			"" );
			this.QueryName				= parseObject.Get<string>	( QUERY_NAME_KEY, 			"" );

			this.Archive				= parseObject.Get<bool> 	( ARCHIVED_KEY, 			false );
			this.Flagged				= parseObject.Get<bool> 	( FLAGGED_KEY, 				false );
			this.MuteFlag 				= parseObject.Get<int> 		(MUTEFLAG_KEY, 0);
			this.LastViewed				= BlrtUtil.ToLocalTime(	parseObject.Get<DateTime> ( LAST_VIEWED_KEY, 	BlrtUtil.ToCloudTime( CreatedAt ) ));
			this.LastEdited				= BlrtUtil.ToLocalTime(	parseObject.Get<DateTime> ( LAST_EDITED_KEY, 	BlrtUtil.ToCloudTime( CreatedAt ) ));

			this.HasComment				= parseObject.Get<bool> 	( HAS_COMMENT_KEY, 			false );
			this.HasMadeBlrt			= parseObject.Get<bool> 	( HAS_MADE_BLRT_KEY, 		false );
			this.HasMadeReBlrt			= parseObject.Get<bool> 	( HAS_MADE_REBLRT_KEY, 		false );
			this.HasMadeRequest			= parseObject.Get<bool> 	( HAS_MADE_REQUEST_KEY, 	false );
			this.HasInvitedPeople		= parseObject.Get<bool> 	( HAS_INVITED_PEOPLE_KEY,	false );


			var user 				= parseObject.Get<ParseUser> 	( USER_KEY, 				null );
			this.UserId 			= user != null ? user.ObjectId : "";

			var conversation 		= parseObject.Get<ParseObject> 	( CONVERSATION_KEY, 		null );
			this.ConversationId 	= conversation != null ? conversation.ObjectId : "";

			var indexList 			= parseObject.Get<string> 		( INDEX_VIEWED_LIST_KEY, 	"" );
			this._viewedList 		= BlrtUtil.PagesToList (indexList);
			OnPropertyChanged ("ViewedList");

			this._unreadMessages = parseObject.Get<IList<int>> (UNREAD_MESSAGES_KEY, new List<int>()).ToList();
			OnPropertyChanged ("UnreadMessages");


			var tagsTemp = parseObject.Get<List<object>> (TAGS_KEY, null);

			if (tagsTemp != null)
				Tags = tagsTemp.Cast<string> ().ToArray();
			else
				Tags =new string[0];

			if ((null != ParseUser.CurrentUser) && (UserId == ParseUser.CurrentUser.ObjectId)) {
			
				var items = LocalStorage.DataManager.GetConversationItems (ConversationId);

				if (items != null) {
					foreach (var i in items) {
					
						if (i.CreatedByCurrentUser)
							MarkViewed (i);
					
					}
				}
			}
		}

		public void MarkAllRead()
		{
			if (_placeHolder)
				return;


			MarkViewed (BlrtUtil.GetRangeArray(0, Conversation.ReadableCount - 1));
			MarkAllUnreadMesaagesViewed ();
		}

		public void MarkAllUnreadMesaagesViewed()
		{
			if (this.UnreadMessages?.Count > 0) {
				this.UnreadMessages?.Clear ();

				Dirty = true;
				UpdateLastViewed ();

				OnPropertyChanged ("UnreadMessages");
				OnPropertyChanged ("ViewedList");

				LocalStorage.DataManager.Save ();
				BlrtManager.RecalulateAppBadge ();
			}
		}

		public void MarkViewed(ConversationItem content)
		{
			if (_placeHolder || content.ReadableIndex < 0 || !content.OnParse)
				return;

			if (this.ViewedList == null) {
				_viewedList = new List<int> ();
			}


			if (content.ConversationId == ConversationId) {
				MarkViewed (new []{content.ReadableIndex});
			}
		}

		public void MarkViewed(IEnumerable<int> readableIndexes)
		{
			if (_placeHolder || Conversation == null)
				return;

			bool changed = false;

			foreach (var index in readableIndexes) {

				if (index < 0 || index >= Conversation.ReadableCount)
					continue;

				if (!ViewedList.Contains (index)) {
					_viewedList.Add (index);
					changed = true;
				}
			}

			if (changed) {
				Dirty = true;
				UpdateLastViewed ();

				OnPropertyChanged ("ViewedList");

				LocalStorage.DataManager.Save ();
				BlrtManager.RecalulateAppBadge ();
			}
		}

		public void UpdateLastViewed () {
			LastViewed = DateTime.Now;
			Dirty = true;
		}

		public void UpdateLastEdited ()
		{
			LastEdited = DateTime.Now;
		}

		public IEnumerable<int> InverseViewList ()
		{
			for (int i = 0; i < Conversation.GeneralCount + 1; ++i)
				if (!ViewedList.Contains (i))
					yield return i;


		}

		public int EstimateUnread ()
		{
			if (_placeHolder)
				return 0;

			if (Conversation == null) 
				return 0;

			if (Conversation.IsDamaged) {
				var items = LocalStorage.DataManager.GetConversationItems (Conversation);
				return items.Count ( i => { 
					if(ParseUser.CurrentUser!=null){
						return i.ReadableIndex >= 0 && !ViewedList.Contains (i.ReadableIndex) && i.CreatorId != ParseUser.CurrentUser.ObjectId; 
					}
					return i.ReadableIndex >= 0 && !ViewedList.Contains (i.ReadableIndex); 
				} );
			}

			if(UnreadMessages!=null)
				return Math.Max (Conversation.ReadableCount - ViewedList.Count + UnreadMessages.Count, 0);
			else
				return Math.Max (Conversation.ReadableCount - ViewedList.Count, 0);
		}

		public bool HasViewed(ConversationItem content)
		{
			if (_placeHolder)
				return true;

			if (this.UnreadMessages.Contains (content.Index))
				return false;
			else if (content.Type == BlrtContentType.MessageEvent)
				return true;

			if (content.ReadableIndex < 0)
				return true;

			var t = content.ConversationId == ConversationId;
			var tt = this.ViewedList != null && ViewedList.Contains (content.ReadableIndex);
			var ttt = !content.OnParse;
			var tttt = content.CreatedByCurrentUser;

			return ((content.ConversationId == ConversationId) && this.ViewedList != null && ViewedList.Contains (content.ReadableIndex))
				|| !content.OnParse
				|| content.CreatedByCurrentUser;
		}

		public int GetFirstUnread ()
		{
			_viewedList.Sort ();

			for (int i = 0; i < ViewedList.Count; ++i) {
				if (ViewedList [i] != i)
					return i;
			}
			return ViewedList.Count;
		}

		public static ConversationUserRelation CreatePlaceholder (Conversation conversation)
		{
			return new ConversationUserRelation () {
				ConversationId = conversation.ObjectId,
				CreatorId = (null == ParseUser.CurrentUser) ? "" : ParseUser.CurrentUser.ObjectId,
				UserId = (null == ParseUser.CurrentUser) ? "" : ParseUser.CurrentUser.ObjectId,
				_placeHolder = true,
				Permissions = new RelationPermissions(new List<BlrtPermissions.Record>())
			};
		}

		public override bool TryUpgrade ()
		{
			return false; // No upgrades supported
		}









		[Serializable]
		public class RelationPermissions{
		
			public List<BlrtPermissions.Record> records;
			public DateTime updateAt;

			public bool Valid { get { return updateAt + BlrtSettings.PermissionsExpiryTime > DateTime.Now; } }
			public bool NeedsUpdate { 
				get { return updateAt + BlrtSettings.PermissionsUpdateFrequency < DateTime.Now; }
			}

			public RelationPermissions(IEnumerable<BlrtPermissions.Record> records) : this(records.ToList()){
			}

			public RelationPermissions(List<BlrtPermissions.Record> records){
				this.records = records;
				this.updateAt = DateTime.Now;
			}

			public RelationPermissions(){
				this.records = new List<BlrtPermissions.Record>();
				this.updateAt = new DateTime();
			}
		}
	}
}

