using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtData.Contents
{
	public class BlrtRecordData : Media.BlrtMediaPagePrototype
	{
		public int AudioLength 	{ get; set; }

		public BlrtRecordData () : base ()
		{

		}

		public BlrtRecordData (Media.BlrtMediaPagePrototype index) : base (index)
		{
			if (index is BlrtRecordData) {
				
			}
		}

		public IDictionary<string, object> ToDictionary ()
		{
			return new Dictionary<string, object> () {
				{ "audioLength",		AudioLength	}, 
				{ "items",				(from m in Items 		select m.ToDictionary ()).ToArray () }, 
				{ "hiddenItems",		(from m in HiddenItems 	select m.ToDictionary ()).ToArray () },
			};
		}
	}
}