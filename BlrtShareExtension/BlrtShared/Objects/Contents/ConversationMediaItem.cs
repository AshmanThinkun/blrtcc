﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlrtData.IO;
using Parse;


namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationMediaItem : ConversationItem
	{
		const string PAGE_COUNT_KEY = "PageCount";
		const string MEDIA_TYPE_KEY = "MediaType";

		public override BlrtContentType Type { get { return BlrtContentType.Media; } }

		public int Pages { get; set; }
		public BlrtMediaFormat MediaType { get; set; }

		public ConversationMediaItem (LocalStorageType localStorageType = LocalStorageType.Default) : base(localStorageType)
		{
		}

		public ConversationMediaItem (ParseObject parse, ILocalStorageParameter localStorageParam = null)
			: base (parse)
		{}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			PushPausePropertyChanged ();
			base.ApplyParseObject (parseObject);
			PopPausePropertyChanged ();
		}

		public override void CopyToParse (ref ParseObject parseObject)
		{
			base.CopyToParse (ref parseObject);
		}

		public override bool IsDataLocal ()
		{
			return base.IsDataLocal ();
		}

		public override void DeleteData ()
		{
			base.DeleteData ();
		}

		public override void StartFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StartFileMonitoring (listener, thumbnailIndex);
		}

		public override void StopFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StopFileMonitoring (listener, thumbnailIndex);
		}

		public override CloudState GetCloudState ()
		{
			return base.GetCloudState ();
		}

		public Dictionary<string, string> ArgDictionary () {
			return new Dictionary<string, string> () {
					{PAGE_COUNT_KEY, Pages.ToString()},
					{MEDIA_TYPE_KEY, MediaType.ToString()}
			};
		}

		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json.JsonConvert.SerializeObject (ArgDictionary());
		}

		public override void ReloadArgument ()
		{
			if (string.IsNullOrWhiteSpace (Argument))
				return;

			try {
				var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>> (Argument);
				string pages = null;
				string mediaType = null;

				if (dic.TryGetValue (PAGE_COUNT_KEY, out pages)) {
					if (!string.IsNullOrWhiteSpace (pages)) {
						Pages = int.Parse (pages);
					}
				}

				if (dic.TryGetValue (MEDIA_TYPE_KEY, out mediaType)) {
					if (!string.IsNullOrWhiteSpace (mediaType)) {
						var type = (BlrtMediaFormat)Enum.Parse (typeof (BlrtMediaFormat), mediaType);
						if (Enum.IsDefined (typeof (BlrtMediaFormat), mediaType)) {
							MediaType = type;
						}
					}
				}
			} catch { }
		}

		public override BlrtDownloader [] DownloadContent (int priority)
		{
			return new List<IO.BlrtDownloader> (base.DownloadContent (priority)).ToArray ();
		}
	}
}
