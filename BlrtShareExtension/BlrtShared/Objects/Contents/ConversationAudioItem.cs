﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BlrtData.IO;
using Parse;


namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationAudioItem : ConversationItem
	{
		const string AUDIO_LENGTH_KEY_FOR_PARSE = "audioLength";
		const string AUDIO_LENGTH_KEY_FROM_PARSE = "AudioLength";

		public override BlrtContentType Type { get { return BlrtContentType.Audio; } }

		string _localAudioName;
		Uri _cloudAudioPath;

		public string LocalAudioName {
			get {
				return _localAudioName;
			}
			set {
				if (value != _localAudioName) {
					_localAudioName = value;
					OnPropertyChanged ("LocalAudioName");
				}
			}
		}

		public Uri CloudAudioPath {
			get {
				return _cloudAudioPath;
			}
			set {
				if (value != _cloudAudioPath) {
					_cloudAudioPath = value;
					OnPropertyChanged ("CloudAudioPath");
				}
			}
		}

		public ConversationAudioItem (LocalStorageType localStorageType = LocalStorageType.Default) : base (localStorageType)
		{
			LocalAudioName = "";
			CloudAudioPath = null;
		}

		public ConversationAudioItem (ParseObject parse, ILocalStorageParameter localStorageParam = null)
			: base (parse)
		{ }

		[Newtonsoft.Json.JsonIgnore]
		public string LocalAudioPath {
			get {
				if (string.IsNullOrWhiteSpace (LocalAudioName))
					return "";
				return this.OnParse ? LocalStorage.LocalDirectories.GetFilesPath (LocalAudioName) : LocalStorage.LocalDirectories.GetCachedPath (LocalAudioName);
			}
		}

		[Newtonsoft.Json.JsonIgnore]
		public string DecryptedAudioPath {
			get {
				return LocalStorage.LocalDirectories.GetDecryptedPath (LocalAudioName);
			}
		}

		public int AudioLength { get; set; }

		public override void ApplyParseObject (ParseObject parseObject)
		{
			PushPausePropertyChanged ();

			base.ApplyParseObject (parseObject);

			ReloadArgument ();
			LocalAudioName = "";

			ParseFile audioFile = parseObject.Get<ParseFile> (AUDIO_FILE_KEY, null);

			if (audioFile != null) {
				LocalAudioName = audioFile.Name;
				CloudAudioPath = audioFile.Url;
			}

			PopPausePropertyChanged ();
		}

		public override void CopyToParse (ref ParseObject parseObject)
		{
			base.CopyToParse (ref parseObject);
		}

		public override bool IsDataLocal ()
		{
			return IsAudioLocal && base.IsDataLocal ();
		}

		public bool IsAudioLocal {
			get {
				return BlrtUtil.CanUseFile (LocalAudioPath);
			}
		}

		public void DeleteAudioData ()
		{
			try {
				File.Delete (LocalAudioPath);
				File.Delete (DecryptedAudioPath);
			} catch { }
		}

		public bool IsAudioDecrypted {
			get {
				return BlrtUtil.CanUseFile (DecryptedAudioPath, false);
			}
		}

		public async Task DecyptAudioData ()
		{
			if (IsAudioLocal) {
				await BlrtData.BlrtEncryptorManager.Decrypt (
					LocalAudioPath,
					DecryptedAudioPath,
					this
				);
			} else {
				throw new InvalidOperationException ("Cannot decrypt file before it is downloaded");
			}
		}

		public override void DeleteData ()
		{
			base.DeleteData ();
			DeleteAudioData ();
		}

		public override void StartFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StartFileMonitoring (listener, thumbnailIndex);
			var checker = MediaChecker.GetMediaChecker ();

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				checker.Subscribe (LocalAudioPath, listener ?? this);
			}
		}

		public override void StopFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StopFileMonitoring (listener, thumbnailIndex);
			var checker = MediaChecker.GetMediaChecker ();

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				checker.UnSubscribe (LocalAudioPath, listener ?? this);
			}
		}

		public override CloudState GetCloudState ()
		{
			var state = base.GetCloudState ();

			var checker = MediaChecker.GetMediaChecker ();
			var audioState = CheckerState.Synced;

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				audioState = checker.GetState (LocalAudioPath);
			}

			if (state == CloudState.Synced && (audioState == CheckerState.DownloadingContent)) {
				state = CloudState.DownloadingContent;
			} else if (state == CloudState.Synced && (audioState == CheckerState.ContentMissing)) {
				state = CloudState.ContentMissing;
			}
			return state;
		}

		public Dictionary<string, string> ArgDictionary ()
		{
			return new Dictionary<string, string> () {
				{AUDIO_LENGTH_KEY_FOR_PARSE, AudioLength.ToString()}
			};
		}

		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json.JsonConvert.SerializeObject (ArgDictionary ());
		}

		public override void ReloadArgument ()
		{
			if (string.IsNullOrWhiteSpace (Argument))
				return;

			try {
				var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>> (Argument);
				string audioLength = null;

				if (dic.TryGetValue (AUDIO_LENGTH_KEY_FROM_PARSE, out audioLength)) {
					if (!string.IsNullOrWhiteSpace (audioLength)) {
						AudioLength = int.Parse (audioLength);
					}
				}
			} catch { }
		}

		public override BlrtDownloader [] DownloadContent (int priority)
		{
			var downloaders = new List<IO.BlrtDownloader> (base.DownloadContent (priority));
			if (!this.IsDataLocal () && this.OnParse) {
				var convo = LocalStorage.DataManager.GetConversation (this.ConversationId);
					var audio = this as BlrtData.Contents.ConversationAudioItem;

				if (!string.IsNullOrEmpty (audio.LocalAudioName)) {
					var d = IO.BlrtDownloader.Download (priority, audio.CloudAudioPath, audio.LocalAudioPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (convo, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (this, d);

					d.OnDownloadFailed += UpdateFileState;
					d.OnDownloadFinished += UpdateFileState;
					downloaders.Add (d);
				}
			}
			return downloaders.ToArray ();
		}
	}
}
