using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using BlrtData.IO;

namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationBlrtItem : ConversationItem
	{

		public override BlrtContentType Type { get { return BlrtContentType.Blrt; } }

		string _localTouchDataName;
		string _localAudioName;

		string _publicBlrtName;

		Uri _cloudTouchDataPath;
		Uri _cloudAudioPath;

		[NonSerialized]
		BlrtRecordData _mediaData;


		public string 	LocalTouchDataName	{
			get {
				return _localTouchDataName;
			}
			set { 
				if (value != _localTouchDataName) {
					_localTouchDataName = value;
					OnPropertyChanged ("LocalTouchDataName");
				}
			}
		}
		public string 	LocalAudioName		 	{
			get { 
				return _localAudioName;
			}
			set { 
				if (value != _localAudioName) {
					_localAudioName = value;
					OnPropertyChanged ("LocalAudioName");
				}
			}
		}
		public Uri	 	CloudTouchDataPath		 	{
			get {
				return _cloudTouchDataPath;
			}
			set { 
				if (value != _cloudTouchDataPath) {
					_cloudTouchDataPath = value;
					OnPropertyChanged ("CloudTouchDataPath");
				}
			}
		}
		public Uri	 	CloudAudioPath		 	{
			get {
				return _cloudAudioPath;
			}
			set { 
				if (value != _cloudAudioPath) {
					_cloudAudioPath = value;
					OnPropertyChanged ("CloudAudioPath");
				}
			}
		}

		public string PublicBlrtName{
			get{
				return _publicBlrtName;
			}
			set {
				if (value != _publicBlrtName) {
					_publicBlrtName = value;
					OnPropertyChanged ("PublicBlrtName");
				}
			}
		}

		public BlrtRecordData MediaData		 	{
			get {
				return _mediaData;
			}
			set { 
				if (value != _mediaData) {
					_mediaData = value;
					OnPropertyChanged ("MediaData");
				}
			}
		}



		[Newtonsoft.Json.JsonIgnore]
		public string 	LocalTouchDataPath			{	get { 
				if (string.IsNullOrWhiteSpace (LocalTouchDataName))
					return "";
				return this.OnParse ? LocalStorage.LocalDirectories.GetFilesPath (LocalTouchDataName) 	: LocalStorage.LocalDirectories.GetCachedPath (LocalTouchDataName); 
			} 
		}
		[Newtonsoft.Json.JsonIgnore]
		public string 	LocalAudioPath				{	get { 
				if (string.IsNullOrWhiteSpace (LocalAudioName))
					return ""; 
				return this.OnParse ? LocalStorage.LocalDirectories.GetFilesPath (LocalAudioName) 		: LocalStorage.LocalDirectories.GetCachedPath (LocalAudioName);
			} 
		}

		[Newtonsoft.Json.JsonIgnore]
		public string DecryptedAudioPath {
			get {
				return LocalStorage.LocalDirectories.GetDecryptedPath (LocalAudioName);
			}
		}

		[Newtonsoft.Json.JsonIgnore]
		public string DecryptedTouchDataPath {
			get {
				return LocalStorage.LocalDirectories.GetDecryptedPath (LocalTouchDataName);
			}
		}


		public int AudioLength					{	get { return MediaData.AudioLength; }	}
		public int Pages						{	get { return MediaData.TotalPages; }	}


		public ConversationBlrtItem (LocalStorageType localStorageType = LocalStorageType.Default) : base(localStorageType)
		{
			LocalTouchDataName	= "";
			LocalAudioName		= "";
			CloudTouchDataPath	= null;
			CloudAudioPath		= null;
		}

		public ConversationBlrtItem (ParseObject parse, ILocalStorageParameter localStorageParam = null) 
			: base(parse)
		{
		}

		public override void ApplyParseObject(ParseObject parseObject)
		{
			PushPausePropertyChanged ();

			base.ApplyParseObject (parseObject);

			LocalTouchDataName = "";
			LocalAudioName = "";

			ParseFile touchDataFile = parseObject.Get<ParseFile> 	(TOUCH_DATA_FILE_KEY, null);
			ParseFile audioFile 	= parseObject.Get<ParseFile>	(AUDIO_FILE_KEY, null);

			if (touchDataFile != null) {
				LocalTouchDataName = touchDataFile.Name;
				CloudTouchDataPath = touchDataFile.Url;
			}
			if (audioFile != null) {
				LocalAudioName = audioFile.Name;
				CloudAudioPath = audioFile.Url;
			}

			PublicBlrtName = parseObject.Get<string> (PUBLICBLRTNAME_KEY, "");

			PopPausePropertyChanged ();
		}

		public override void CopyToParse (ref ParseObject parseObject)
		{
			base.CopyToParse (ref parseObject);
			parseObject [PUBLICBLRTNAME_KEY] = PublicBlrtName;
		}

		public override bool IsDataLocal()
		{
			return IsAudioLocal && IsTouchLocal && base.IsDataLocal();
		}

		public bool IsAudioLocal
		{
			get {
				return BlrtUtil.CanUseFile (LocalAudioPath);
			}
		}

		public bool IsTouchLocal
		{
			get {
				return BlrtUtil.CanUseFile (LocalTouchDataPath);
			}
		}

		public void DeleteAudioData ()
		{
			try {
				File.Delete (LocalAudioPath);
				File.Delete (DecryptedAudioPath);
			} catch {}
		}

		public void DeleteTouchData ()
		{
			try {
				File.Delete (LocalTouchDataPath);
				File.Delete (DecryptedTouchDataPath);
			} catch {}
		}

		public bool IsAudioDecrypted
		{
			get {
				return BlrtUtil.CanUseFile (DecryptedAudioPath, false);
			}
		}

		public bool IsTouchDecrypted
		{
			get {
				return BlrtUtil.CanUseFile (DecryptedTouchDataPath, false);
			}
		}

		public async Task DecyptAudioData ()
		{
			if (IsAudioLocal) {
				await BlrtData.BlrtEncryptorManager.Decrypt (
					LocalAudioPath,
					DecryptedAudioPath,
					this
				);
			} else {
				throw new InvalidOperationException ("Cannot decrypt file before it is downloaded");
			}
		}

		public async Task DecyptTouchData ()
		{
			if (IsTouchLocal) {
				await BlrtData.BlrtEncryptorManager.Decrypt (
					LocalTouchDataPath,
					DecryptedTouchDataPath,
					this
				);
			} else {
				throw new InvalidOperationException ("Cannot decrypt file before it is downloaded");
			}
		}

		public override void DeleteData ()
		{
			base.DeleteData ();
			DeleteAudioData ();
			DeleteTouchData ();
		}

		public override void StartFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StartFileMonitoring (listener, thumbnailIndex);
			var checker = MediaChecker.GetMediaChecker ();

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				checker.Subscribe (LocalAudioPath, listener ?? this);
			}
			if (!string.IsNullOrEmpty (LocalTouchDataPath)) {
				checker.Subscribe (LocalTouchDataPath, listener ?? this);
			}
		}

		public override void StopFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			base.StopFileMonitoring (listener, thumbnailIndex);
			var checker = MediaChecker.GetMediaChecker ();

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				checker.UnSubscribe (LocalAudioPath, listener ?? this);
			}
			if (!string.IsNullOrEmpty (LocalTouchDataPath)) {
				checker.UnSubscribe (LocalTouchDataPath, listener ?? this);
			}
		}

		public override CloudState GetCloudState ()
		{
			var state = base.GetCloudState ();

			var checker = MediaChecker.GetMediaChecker ();
			var audioState = CheckerState.Synced;
			var touchState = CheckerState.Synced;

			if (!string.IsNullOrEmpty (LocalAudioPath)) {
				audioState = checker.GetState (LocalAudioPath);
			}
			if (!string.IsNullOrEmpty (LocalTouchDataPath)) {
				touchState = checker.GetState (LocalTouchDataPath);
			}

			if (state == CloudState.Synced && (audioState == CheckerState.DownloadingContent || touchState == CheckerState.DownloadingContent)) {
				state = CloudState.DownloadingContent;
			} else if (state == CloudState.Synced && (audioState == CheckerState.ContentMissing || touchState == CheckerState.ContentMissing)) {
				state = CloudState.ContentMissing;
			}
			return state;
		}

		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json
				.JsonConvert.SerializeObject (
					MediaData
				);
		}

		public override void ReloadArgument ()
		{
			MediaData =  Newtonsoft.Json.JsonConvert.DeserializeObject<BlrtRecordData> ( Argument );
		}

		public override BlrtDownloader[] DownloadContent (int priority)
		{
			var downloaders = new List<IO.BlrtDownloader> (base.DownloadContent (priority));
			if (!this.IsDataLocal() && this.OnParse) {
				var convo = LocalStorage.DataManager.GetConversation (this.ConversationId);
				var blrt = this as BlrtData.Contents.ConversationBlrtItem;

				if (!string.IsNullOrEmpty(blrt.LocalAudioName) && !BlrtUtil.CanUseFile (blrt.LocalAudioPath)) {
					var d = IO.BlrtDownloader.Download (priority, blrt.CloudAudioPath, blrt.LocalAudioPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (convo,	d);
					IO.BlrtDownloaderAssociation.CreateAssociation (this,	d);

					d.OnDownloadFailed += UpdateFileState;
					d.OnDownloadFinished += UpdateFileState;
					downloaders.Add (d);
				}
				if (!string.IsNullOrEmpty(blrt.LocalTouchDataName) && !BlrtUtil.CanUseFile (blrt.LocalTouchDataPath)) {
					var d = IO.BlrtDownloader.Download (priority, blrt.CloudTouchDataPath, blrt.LocalTouchDataPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (convo, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (this, d);

					d.OnDownloadFailed += UpdateFileState;
					d.OnDownloadFinished += UpdateFileState;
					downloaders.Add (d);
				}
			}
			return downloaders.ToArray ();
		}
	}
}