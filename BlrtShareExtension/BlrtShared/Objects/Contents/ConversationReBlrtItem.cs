using System;
using Parse;

namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationReBlrtItem : ConversationBlrtItem
	{
		public override BlrtContentType Type { get { return BlrtContentType.BlrtReply; } }


		public ConversationReBlrtItem () : base()
		{
		}

		public ConversationReBlrtItem (ParseObject parse) 
			: base(parse)
		{
		}
	}
}