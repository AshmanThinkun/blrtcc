using System;
using Parse;


namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationCommentItem : ConversationItem
	{
		public override BlrtContentType Type { get { return BlrtContentType.Comment; } }

		[NonSerialized]
		ConversationCommentData _commentData;


		[Newtonsoft.Json.JsonIgnore]
		public string Comment {	
			get { return _commentData.Comment; } 
			set { _commentData.Comment = value; }	
		}

		public ConversationCommentItem () : base ()
		{
			_commentData = new ConversationCommentData ();
		}

		public ConversationCommentItem (ParseObject parse)
			: base (parse)
		{
		}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
		}

		public override void ReloadArgument ()
		{
			_commentData = Newtonsoft.Json.JsonConvert.DeserializeObject<ConversationCommentData> (Argument);

			if (_commentData == null)
				_commentData = new ConversationCommentData ();
		}

		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json
				.JsonConvert.SerializeObject (
				_commentData
			);
		}
	}


	public class ConversationCommentData
	{
		public string Comment;

		public ConversationCommentData ()
		{
			Comment = "";
		}

		public static ConversationCommentData FromString (string comment)
		{
			return new ConversationCommentData () {
				Comment = comment
			};
		}
	}
}