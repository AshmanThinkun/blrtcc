using System;
using Parse;

namespace BlrtData.Contents
{
	[Serializable]
	public class ConversationEventItem : ConversationItem
	{
		public const string BoldPrefix = "@#$";

		public const string BlackPrefix = "*%^B";


		public override BlrtContentType Type { get { return BlrtContentType.UnknownEvent; } }


		public ConversationEventItem () : base()
		{
		}

		public ConversationEventItem (ParseObject parse) 
			: base(parse)
		{

		}

		#region implemented abstract members of ConversationItem

		public override void StringifyArgument ()
		{
			throw new NotImplementedException ();
		}

		public override void ReloadArgument ()
		{
			throw new NotImplementedException ();
		}

		#endregion

		public override void ApplyParseObject(ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);


			TryMarkRead ();
		}
	}
}