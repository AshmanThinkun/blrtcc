using System;
using Parse;

namespace BlrtData.Contents.Event
{
	[Serializable]
	public class ConversationAddEventItem : ConversationEventItem
	{
		public override BlrtContentType Type { get { return BlrtContentType.AddEvent; } }


		[NonSerialized]
		BlrtAddEventData _eventData;

		public BlrtAddEventData EventData {
			get {
				return _eventData;
			}
		}


		public ConversationAddEventItem () : base()
		{
		}

		public ConversationAddEventItem (ParseObject parse) 
			: base(parse)
		{

		}

		public override void ApplyParseObject(ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
		}
			


		public override void ReloadArgument ()
		{
			_eventData =  Newtonsoft.Json.JsonConvert.DeserializeObject<BlrtAddEventData> ( Argument );
		}

		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json
				.JsonConvert.SerializeObject (
					_eventData
				);
		}
	}

	public class BlrtAddEventData
	{
		public BlrtAddEventDataItem[] items;

		public static string BoldPrefix = ConversationEventItem.BoldPrefix;
		public static string BlackPrefix = ConversationEventItem.BlackPrefix;

		public class BlrtAddEventDataItem
		{
			public string userId;
			public string relationId;
			public string type;
			public string value;
			public string name;
		}

	}
}