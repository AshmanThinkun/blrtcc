using System;
using Parse;

namespace BlrtData.Contents.Event
{
	[Serializable]
	public class ConversationMessageEventItem : ConversationEventItem
	{
		public override BlrtContentType Type { get { return BlrtContentType.MessageEvent; } }


		[NonSerialized]
		BlrtMessageEventData _eventData;

		public BlrtMessageEventData EventData {
			get {
				return _eventData;
			}
		}

		public ConversationMessageEventItem () : base ()
		{
		}

		public ConversationMessageEventItem (ParseObject parse)
			: base (parse)
		{

		}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
		}



		public override void ReloadArgument ()
		{
			_eventData = Newtonsoft.Json.JsonConvert.DeserializeObject<BlrtMessageEventData> (Argument);
		}


		public override void StringifyArgument ()
		{
			Argument = Newtonsoft.Json
				.JsonConvert.SerializeObject (
				EventData
			);
		}
	}

	public class BlrtMessageEventData
	{
		public string Info { get; set; }

		public string Message { get; set; }

		public UpdateObject[] Update{ get; set;}

		public int MessageType { get; set; }

		public BlrtMessageEventData Fallback { get; set; }

		public string RemovedUserDisplayName { get; set; }

		public string[] RemovedUserContactDetail { get; set; }

		public string RemoveType { get; set; }

		public string RemoveInitiator { get; set; }

		public string ChangingInitiator { get; set; }

		public string NewName { get; set; }

		public string OldName { get; set; }
	}

	public class UpdateObject{
		public string objectId;
		public int status;
	}
}