﻿using System;
using System.Collections.Generic;
using BlrtData;

namespace BlrtData.Contents.Event
{
	[Serializable]
	public class ConversationSendFailureEventItem: LocalConversationItem
	{
		public ConversationSendFailureEventItem ():base()
		{
		}

		#region implemented abstract members of ConversationItem

		public override void StringifyArgument ()
		{
			
		}

		public override void ReloadArgument ()
		{
			
		}

		public override BlrtContentType Type {
			get {
				return BlrtContentType.SendFailureEvent;
			}
		}




		BlrtData.Conversation.ActivityStatus _sendingStatus;
		public BlrtData.Conversation.ActivityStatus 	SendingStatus	 {
			get {
				return _sendingStatus;
			}
			set { 
				if (value != _sendingStatus) {
					_sendingStatus = value;
					OnPropertyChanged ("SendingStatus");
				}
			}
		}

		#endregion
	}
}

