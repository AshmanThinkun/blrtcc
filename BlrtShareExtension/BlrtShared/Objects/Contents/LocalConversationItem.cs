﻿using System;

namespace BlrtData
{
	[Serializable]
	public abstract class LocalConversationItem : ConversationItem
	{
		public LocalConversationItem (LocalStorageType localStorageType = LocalStorageType.Default) : base(localStorageType)
		{
			this.IsLocalItem = true;
		}
	}
}

