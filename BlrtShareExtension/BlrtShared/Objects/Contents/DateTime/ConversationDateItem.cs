﻿#if __ANDROID__
using System;
using BlrtApp.ViewModels;
using BlrtApp.Views;
using BlrtApp.Views.ConversationView;
using Xamarin.Forms;

namespace BlrtData.Contents.Date
{
	public class ConversationDateItem : LocalConversationItem, ICellConstructor
	{
		public DateTime currentDate;
		DateTime previousDate;


		public ConversationDateItem ()
		{
			//previousDate = DateTime.MinValue;
		}

#region implemented abstract members of ConversationItem

		public override void StringifyArgument ()
		{

		}

		public override void ReloadArgument ()
		{

		}

		public override BlrtContentType Type {
			get {
				return BlrtContentType.Date;
			}
		}

#endregion

		public void SetCurrentDate (DateTime time)
		{
			currentDate = time;
		}

		public bool IsIncrementing ()
		{
			return DateTime.Compare (currentDate.Date, previousDate.Date) > 0;
		}

		public void UpdatePreviousDate ()
		{
			previousDate = currentDate;
		}

		public void SetPreviousDate (DateTime pre)
		{
			previousDate = pre;
		}

		public DateTime GetCurrentDate ()
		{
			return currentDate;
		}

		public Cell GetDefaultCell ()
		{
			return new ConversationDateCell ();
		}
	}
}
#endif
