using System.IO;
using System;
using System.Linq;
using System.Collections.Generic;
using Parse;
using BlrtData.Media;


namespace BlrtData
{
	[Serializable]
	public sealed class Conversation : BlrtBase, IMediaCheckerListener
	{
		public const string CLASS_NAME 				= "Conversation";
		public override string ClassName { get { return CLASS_NAME; } }

		public const int CURRENT_VERSION = 6;
		public override int CurrentVersion { get { return CURRENT_VERSION; } }

		public const int OLDEST_SUPPORTED_VERSION = 6;
		public override int OldestSupportedVersion { get { return OLDEST_SUPPORTED_VERSION; } }

		public const string NAME_KEY 					= "blrtName";		// string used for displaying as a name

		public const string THUMBNAIL_FILE_KEY	 		= "thumbnailFile";	// the thumbnail of the entire blrt

		public const string CREATION_TYPE_KEY			= "creationType";	// The type of blrt this is, aka: request
		public const string STATUS_CODE_KEY 			= "statusCode";		// status code of the blrrt, has it been corrupted/banned
		public const string CONTENT_UPDATE_KEY 			= "contentUpdate";
		public const string CONTENT_COUNT_KEY 			= "contentCount";	// The number of content items in this conversation
		public const string BOND_COUNT_KEY 				= "bondCount";		// The number of ConversationUserRelations
		public const string READABLE_COUNT_KEY 			= "readableCount";  // The number of readable (i.e. NOT an event) content items

		public const string AWAITING_REQUEST_KEY		= "awaitingRequest";
			
		public const string LAST_MEDIA_KEY				= "lastMedia";
		public const string LAST_MEDIA_ARGUMENT_KEY 	= "lastMediaArgument";

		public const string CONVERSATION_DIRTY_KEY		= "conversationDirty";
		public const string RELOAD_NUMBER_KEYS			= "reloadNumber";

		public const string DISALLOW_PUBLIC_BLRT_KEY  = "disallowPublicBlrt";



		public const string HAS_CONVERSATION_BEGAN_KEY	= "hasConversationBegan";

		public const string SPECIAL_SLUG_KEY			= "specialSlug";
		public const string INTRO_LAUNCH 				= "intro-launch";


		public const string ATTACHED_META_KEY 			= "attachedMeta";
		public const string REQUESTED_ID_KEY			= "requestId";

		public const string DISABLE_OTEHRS_ADD 			= "disableOthersToAdd";
		public const string BLRT_COUNT_KEY 				= "blrtCount";
		public const string UNUSED_MEDIA_KEY 			= "unusedMedia";

		public const string READONLY_KEY 					= "readonly";

		//public const string CONTRIBUTOR_KEY		 	= "contributors";


		#region Properties and Fields

		string 			_name;
		CreationType 	_creationType;
		BlrtStatusCode 	_statusCode;

		DateTime 		_contentUpdatedAt;
		int 			_generalCount;
		int 			_readableCount;
		int 			_bondCount;
		int 			_blrtCount;

		bool 			_awaitingRequest;
		bool 			_hasConversationBegan;

		string[]		_lastMedia;

		string			_lastMediaArgument;
		string			_specialBlrtSlug;

		string			_attachedMeta;
		string			_requestId;


		bool 			_disableOthersToAdd;
		bool			_needsRefresh;
		bool			_needsMetaUpdate;
		bool 			_disallowPublicBlrt;
		bool 			_isEmptyConversation;
		bool 			_readonly;


		int?			_conversationReloadVerison;


		List<int>		_damagedIndexes;


		string			_localThumbnailName;
		Uri				_cloudThumbnailPath;

		// to hold unused media convo items.
		Dictionary<string, bool> _unusedConversationMediaItems;

		BlrtData.Conversation.ActivityStatus _sendingStatus;

		public BlrtData.Conversation.ActivityStatus CurrentActivityStatus{
			get{
				if(!_isSending && _sendingStatus== ActivityStatus.InProgress){ //deal with the case that the app is not sending actually
					if(SendingContacts!=null && SendingContacts.Any(x=>
						x.Status== ContactState.FailedToSend
						|| (x.Status == ContactState.Sending && x.UpdateAt < DateTime.Now.AddSeconds (-20))
					)){
						CurrentActivityStatus = ActivityStatus.Failed;
					}else{
						CurrentActivityStatus = ActivityStatus.Completed;
					}
					return _sendingStatus;
				}else{
					return _sendingStatus;
				}
			}
			set{
				if(value== ActivityStatus.InProgress){
					_isSending = true;
				}else{
					_isSending = false;
				}
				if (value != _sendingStatus) {
					_sendingStatus = value;
					OnPropertyChanged("SendingStatus");
				}
			}
		}

		[NonSerialized]
		bool _isSending;

		public string Name {
			get { 
				return _name;
			} 
			set{ 
				if (value != _name) {
					_name = value;
					OnPropertyChanged("Name");
				}
			} 
		}

		public bool IsEmpty {
			get {
				var emptyOnCloud = _isEmptyConversation;
				var emptyOnLocal = LocalStorage.DataManager.GetConversationItems (ObjectId).Count () == 0;

				if (emptyOnCloud && emptyOnLocal) {
					return true;
				}
				return false;
			}
			private set {
				if (value != _isEmptyConversation) {
					_isEmptyConversation = value;
					OnPropertyChanged ("IsEmptyConversation");
				}
			}
		}

		public int BlrtCount { 
			get { return _blrtCount; }
			private set {
				if (value != _blrtCount) {
					_blrtCount = value;
					OnPropertyChanged ("BlrtCount");
				}
			}
		}

		public bool ContainsWelcomeBlrt {
			get {
				return !string.IsNullOrEmpty (SpecialBlrtSlug);
			}
		}

		public CreationType 		CreationType {
			get { 
				return _creationType;
			} 
			set{ 
				if (value != _creationType) {
					_creationType = value;
					OnPropertyChanged("CreationType");
				}
			} 
		}
		public BlrtStatusCode 		StatusCode {
			get { 
				return _statusCode;
			} 
			set{ 
				if (value != _statusCode) {
					_statusCode = value;
					OnPropertyChanged("StatusCode");
				}
			} 
		}
		public DateTime		 		ContentUpdatedAt {
			get { 
				return _contentUpdatedAt;
			} 
			set{ 
				if (value != _contentUpdatedAt) {
					_contentUpdatedAt = value;
					OnPropertyChanged("ContentUpdatedAt");
				}
			} 
		}
		public int		 			GeneralCount {
			get { 
				return _generalCount;
			} 
			set{ 
				if (value != _generalCount) {
					_generalCount = value;
					OnPropertyChanged("GeneralCount");
				}
			} 
		}
		public int		 			ReadableCount {
			get { 
				return _readableCount;
			} 
			set{ 
				if (value != _readableCount) {
					_readableCount = value;
					OnPropertyChanged("ReadableCount");
				}
			} 
		}
		public int BondCount				{ 
			get {
				return _bondCount;
			}
			set { 
				if (value != _bondCount) {
					_bondCount = value;
					OnPropertyChanged ("BondCount");
				}
			}
		}

		public bool		 			AwaitingRequest		 {
			get { 
				return _awaitingRequest;
			} 
			set{ 
				if (value != _awaitingRequest) {
					_awaitingRequest = value;
					OnPropertyChanged("AwaitingRequest");
				}
			} 
		}
		public bool		 			HasConversationBegan {
			get { 
				return _hasConversationBegan;
			} 
			set{ 
				if (value != _hasConversationBegan) {
					_hasConversationBegan = value;
					OnPropertyChanged("HasConversationBegan");
				}
			} 
		}

		public bool DisAllowPublicBlrt{
			get {
				return _disallowPublicBlrt;
			}
			private set { 
				if (value != _disallowPublicBlrt) {
					_disallowPublicBlrt = value;
					OnPropertyChanged ("AllowPublicBlrt");
				}
			}
		}

		public string[]				LastMedia		 {
			get { 
				return _lastMedia;
			} 
			set{ 
				if (value != _lastMedia) {
					_lastMedia = value;
					OnPropertyChanged("LastMedia");
				}
			} 
		}

		/// <summary>
		/// Gets the unused conversation media items. Returns only the items that are marked as "true".
		/// True : item has NOT been used
		/// False : item has been used
		/// </summary>
		/// <value>The unused conversation media items.</value>
		public IEnumerable<string> UnusedConversationMediaItems {
			get {
				return _unusedConversationMediaItems?
					.Where (m => m.Value == true)
					.Select (m => m.Key);
			}
		}

		public string				LastMediaArgument	 {
			get { 
				return _lastMediaArgument;
			} 
			set{ 
				if (value != _lastMediaArgument) {
					_lastMediaArgument = value;
					OnPropertyChanged("LastMediaArgument");
				}
			} 
		}
		public string				SpecialBlrtSlug	 {
			get { 
				return _specialBlrtSlug;
			} 
			set{
				if (value != _specialBlrtSlug) {
					_specialBlrtSlug = value;
					OnPropertyChanged("SpecialBlrtSlug");
				}
			} 
		}


		public string				AttachedMeta	 {
			get { 
				return _attachedMeta;
			} 
			set{ 
				if (value != _attachedMeta) {
					_attachedMeta = value;
					OnPropertyChanged("AttachedMeta");
				}
			} 
		}
		public string				RequestId	 {
			get { 
				return _requestId;
			} 
			set{ 
				if (value != _requestId) {
					_requestId = value;
					OnPropertyChanged("RequestId");
				}
			} 
		}



		public bool 				NeedsRefresh 	 {
			get { 
				return _needsRefresh;
			} 
			set{ 
				if (value != _needsRefresh) {
					_needsRefresh = value;
					OnPropertyChanged("NeedsRefresh");
				}
			} 
		}
		public bool 				NeedsMetaUpdate  {
			get { 
				return _needsMetaUpdate;
			} 
			set{ 
				if (value != _needsMetaUpdate) {
					_needsMetaUpdate = value;
					OnPropertyChanged("NeedsMetaUpdate");
				}
			} 
		}

		public int?		 			ConversationReloadVerison {
			get {
				return _conversationReloadVerison;
			} 
			set{ 
				if (value != _conversationReloadVerison) {
					_conversationReloadVerison = value;
					OnPropertyChanged("ConversationReloadVerison");
				}
			} 
		}

		public List<int>			DamagedIndexes  {
			get {
				return _damagedIndexes;
			} 
			set{ 
				if (value != _damagedIndexes) {
					_damagedIndexes = value;
					OnPropertyChanged("DamagedIndexes");
				}
			} 
		}


		public string 	LocalThumbnailName		 {
			get {
				return _localThumbnailName;
			} 
			set{ 
				if (value != _localThumbnailName) {
					_localThumbnailName = value;
					OnPropertyChanged("LocalThumbnailName");
				}
			} 
		}
		public Uri	 	CloudThumbnailPath	 {
			get {
				return _cloudThumbnailPath;
			} 
			set{ 
				if (value != _cloudThumbnailPath) {
					_cloudThumbnailPath = value;
					OnPropertyChanged("CloudThumbnailPath");
				}
			} 
		}

		public bool DisableOthersToAdd{
			get {
				return _disableOthersToAdd;
			}
			set{
				if (value != _disableOthersToAdd) {
					_disableOthersToAdd = value;
					OnPropertyChanged("DisableOthersToAdd");
				}
			}
		}

		/// <summary>
		/// Gets or sets the un send contacts.
		/// </summary>
		/// <value>The un send contacts should not include facebook id.</value>
		public string[] UnSendContacts{ get; set;}

		public List<ContactStatus> SendingContacts { //this is the new one
			get;
			set;
		}

		#endregion

		[NonSerialized]
		int _damagedCount;

		public bool IsDamaged { get { return _damagedIndexes.Count > _damagedCount; } }

		[NonSerialized]
		BlrtMediaPagePrototype _lastMediaPrototype;

		public BlrtMediaPagePrototype LastMediaPrototype {
			get{
				if (_lastMediaPrototype == null && !string.IsNullOrWhiteSpace(LastMediaArgument))
					_lastMediaPrototype = BlrtMediaPagePrototype.FromsString(LastMediaArgument);
				return _lastMediaPrototype;
			}
		}
		public string 	LocalThumbnailPath				{	get { 
				if (string.IsNullOrWhiteSpace (LocalThumbnailName))
					return string.Empty; 
				return this.OnParse ? BlrtConstants.Directories.Default.GetThumbnailPath (LocalThumbnailName) : BlrtConstants.Directories.Default.GetCachedPath (LocalThumbnailName);
			} 
		}


		public bool 	IsSpecialConversation				{	get { return !string.IsNullOrWhiteSpace(SpecialBlrtSlug); } }

		[NonSerialized]
		bool _refreshing;

		public bool Refreshing {
			get {
				return _refreshing;
			}
			set {
				if (_refreshing != value) {
					_refreshing = value;
					RefreshingChanged?.Invoke (this, null);
				}
			}
		}

		[field: NonSerialized]
		public event EventHandler RefreshingChanged;

		public bool IsReadonly {
			get {
				return _readonly;
			}
			set {
				if (value != _readonly) {
					_readonly = value;
					OnPropertyChanged ("Readonly");
				}
			}
		}

		private ConversationUserRelation _relation;


		public Conversation () : base()
		{
			Init ();
		}

		public Conversation(bool allowPublicBlrt):base()
		{
			Init ();
			DisAllowPublicBlrt = !allowPublicBlrt;
		}

		private void Init()
		{
			Name 			= "";

			CreationType 	= CreationType.StandardBlrt;
			StatusCode 		= BlrtStatusCode.Valid;

			_pageCount = -1;
			IsEmpty = true;
			DamagedIndexes = new List<int> ();
		}

		public Conversation (ParseObject parseObject) : base(parseObject)
		{
			_pageCount = -1;
			DamagedIndexes = new List<int> ();
		}

		#region Parse importing/exporting stuff

		public override void CopyToParse(ref ParseObject parseObject)
		{
			if (parseObject == null) {

				if(ObjectId != null && ObjectId.Length > 0)
					parseObject = ParseObject.CreateWithoutData (CLASS_NAME, ObjectId);
				else
					parseObject = new ParseObject (CLASS_NAME);
			}

			if (IsDamaged) {
				parseObject [CONVERSATION_DIRTY_KEY] = IsDamaged;
				_damagedCount = _damagedIndexes.Count;
			}


			if (CreatedByCurrentUser) //TODO: make this not so dodgy
				parseObject [NAME_KEY] = Name;

			parseObject [DISABLE_OTEHRS_ADD] = DisableOthersToAdd;
			parseObject [DISALLOW_PUBLIC_BLRT_KEY] = DisAllowPublicBlrt;

			base.CopyToParse (ref parseObject);
		}

		public override void ApplyParseObject(ParseObject parseObject)
		{ 
			_pageCount = -1;
			if (parseObject == null)
				return;

			if (parseObject.ClassName != CLASS_NAME
			    || (OnParse && parseObject.ObjectId != null && parseObject.ObjectId.Length > 0 && ObjectId.Length > 0 && parseObject.ObjectId != this.ObjectId))
				return;

			if (!OnParse) {
				LocalStorage.DataManager.ReplaceConversationId (this.LocalGuid, parseObject.ObjectId);
			}

			base.ApplyParseObject (parseObject);

			var thumbFile = parseObject.Get<ParseFile> (THUMBNAIL_FILE_KEY, null);

			if (thumbFile != null) {
				LocalThumbnailName = thumbFile.Name;
				CloudThumbnailPath = thumbFile.Url;
			}

			this.Name = parseObject.Get<string> (NAME_KEY, "");
			this.SpecialBlrtSlug = parseObject.Get<string> (SPECIAL_SLUG_KEY, null);


			this.DisAllowPublicBlrt 		= parseObject.Get<bool> (DISALLOW_PUBLIC_BLRT_KEY, false);

			this.CreationType = (CreationType)parseObject.Get<int>	(CREATION_TYPE_KEY, 0);
			this.StatusCode = (BlrtStatusCode)parseObject.Get<int>	(STATUS_CODE_KEY, 0);

			this.ReadableCount = parseObject.Get<int>				(READABLE_COUNT_KEY, 0);

			this.AwaitingRequest	= parseObject.Get<bool> (AWAITING_REQUEST_KEY, false);
			this.HasConversationBegan	= parseObject.Get<bool> (HAS_CONVERSATION_BEGAN_KEY, false);

			this.DisableOthersToAdd = parseObject.Get<bool> (DISABLE_OTEHRS_ADD, false);

			this.ContentUpdatedAt = BlrtUtil.ToLocalTime (parseObject.Get<DateTime> (CONTENT_UPDATE_KEY,	BlrtUtil.ToCloudTime (UpdatedAt)));

			_readonly = parseObject.Get<bool> (READONLY_KEY, false);

			var mediaItems = parseObject.Get<List<object>> (LAST_MEDIA_KEY, null);
			if (mediaItems == null || mediaItems.Count == 0)
				this.LastMedia = new string[0];
			else
				this.LastMedia = (from obj in mediaItems.Cast<ParseObject> ()
					select (obj != null ? obj.ObjectId : "")).ToArray ();



			var unusedConversationMediaItems = parseObject.Get<List<object>> (UNUSED_MEDIA_KEY, null);
			if (unusedConversationMediaItems != null && unusedConversationMediaItems.Count > 0) {
				AddUnusedConversationMediaItems ((from obj in unusedConversationMediaItems.Cast<ParseObject> ()
				                                  select (obj != null ? obj.ObjectId : "")));
			}

			this.LastMediaArgument = parseObject.Get<string> (LAST_MEDIA_ARGUMENT_KEY, "");
			if (string.IsNullOrWhiteSpace (this.LastMediaArgument)) {
				this.LastMediaArgument = "{}";	
			}
			_lastMediaPrototype = null;


			var tmpContentCount = parseObject.Get<int>	(CONTENT_COUNT_KEY, 0);
			IsEmpty = tmpContentCount == 0;

			var tmpBondCount = parseObject.Get<int>	(BOND_COUNT_KEY, 0);
		
			if ((tmpContentCount != GeneralCount || tmpBondCount != BondCount)){
				NeedsRefresh = true;
				NeedsMetaUpdate = true;

				GeneralCount = tmpContentCount;
				BondCount = tmpBondCount;

				LLogger.WriteEvent("Creating Object", "Conversation", "Marked as need refresh");
			}



			var reload = parseObject.Get<int?>	(RELOAD_NUMBER_KEYS, ConversationReloadVerison);

			if (ConversationReloadVerison != reload) {
				var contents = LocalStorage.DataManager.GetConversationItems (ObjectId).Where(obj => obj.OnParse);
				LocalStorage.DataManager.Remove (contents);

				ConversationReloadVerison = reload;


				if (DamagedIndexes != null)
					DamagedIndexes.Clear ();
			}

			BlrtCount = parseObject.Get<int> (BLRT_COUNT_KEY, 0);
		}
		#endregion


		public string[] GetUserInConversation()
		{
			var rels = LocalStorage.DataManager.GetConversationUserRelationsFromConversation (
				ObjectId
			);

			if (rels == null)
				return new string[0];

			return rels
				.Where (rel => !string.IsNullOrEmpty(rel.UserId))
				.Select (rel => rel.UserId)
				.ToArray ();
		}

		private void FindConversationUserRelation()
		{
			if (_relation == null && null != ParseUser.CurrentUser) {
				_relation = LocalStorage.DataManager.GetConversationUserRelation (ObjectId, ParseUser.CurrentUser.ObjectId);
			}
		}


		public bool HasViewed (ConversationItem content)
		{
			FindConversationUserRelation ();
			if (_relation == null)
				return true;


			if (ObjectId == content.ConversationId) {
				return _relation.HasViewed (content);
			}

			return true;
		}
		public void MarkViewed(ConversationItem content)
		{
			FindConversationUserRelation ();
			if (_relation == null && ObjectId != content.ConversationId)
				return;

			_relation.MarkViewed (content);
		}

		public bool IsEverythingLocal() {

			if (LocalStorage.DataManager.GetConversationItems(ObjectId).Count((ConversationItem arg) => {return arg.OnParse;}) < GeneralCount)
				return false;

			if (!BlrtUtil.CanUseFile (LocalThumbnailPath))
				return false;



			foreach (var c in LocalStorage.DataManager.GetConversationItems(this)) {
				if (!c.IsDataLocal ())
					return false;
			}
			return true;
		}

		public bool HasLastMedia() {
			if (LastMedia == null || LastMedia.Length == 0)
				return true;

			foreach (var c in LocalStorage.DataManager.GetMediaAssets(LastMedia)) {
				if (!c.IsDataLocal ())
					return false;
			}

			return true;
		}

		public override BlrtBaseIssues GetIssues ()
		{
			var issues = base.GetIssues ();
		
			if (!Enum.IsDefined (typeof(BlrtStatusCode), StatusCode))
				issues = issues | BlrtBaseIssues.UnknownStatusCode;
		
			return issues;
		}


		public override void DeleteData ()
		{
			base.DeleteData ();

			if (File.Exists (LocalThumbnailPath)) {
				File.Delete (LocalThumbnailPath);
			}
		}

		private int _pageCount;

		public int EstimatePagesCount ()
		{
			if(LastMediaPrototype != null)
				_pageCount = LastMediaPrototype.TotalPages + LastMediaPrototype.TotalHiddenPages;

			return _pageCount;
		}

		public override bool TryUpgrade ()
		{
			return false; // No upgrades supported
		}

		public bool ChildrenOnParse {
			get{
				return OnParse && LocalStorage.DataManager.GetUnsyncedConversationItemCount(this) == 0;
			}
		}

		public bool HasIdleUnsynced ()
		{
			return LocalStorage.DataManager.GetIdleUnsyncedConversationItemCount(this) > 0;
		}

		public IO.BlrtDownloaderList DownloadUnusedConversationMedia (int priority)
		{
			return DownloadMedia (priority, UnusedConversationMediaItems.ToArray ());
		}

		public IO.BlrtDownloaderList DownloadBlrtMedia (int priority)
		{
			return DownloadMedia (priority, LastMedia);
		}

		public IO.BlrtDownloaderList DownloadMedia (int priority, string[] mediaIds = null, IEnumerable<MediaAsset> mediaAssets = null)
		{
			var obj = new object ();

			if (mediaAssets == null && mediaIds != null) {
				mediaAssets = LocalStorage.DataManager.GetMediaAssets (mediaIds);
			}

			foreach (var m in mediaAssets) {
				if (!m.IsDataLocal()) {
					var d = IO.BlrtDownloader.Download (priority, m.CloudMediaPath, m.LocalMediaPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (m, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (this, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (obj, d);

					d.OnDownloadFailed += UpdateFileState;
					d.OnDownloadFinished += UpdateFileState;
				}
			}

			var assoc = IO.BlrtDownloaderAssociation.GetAssociationsAsList(obj);;

			assoc.OnFailed += (object sender, EventArgs args) => {
				OnPropertyChanged("FileState");
			};
			assoc.OnFinished += (object sender, EventArgs args) => {
				OnPropertyChanged("FileState");
			};
			return assoc;
		}

		public IO.BlrtDownloaderList DownloadMedia (int priority, string [] mediaIds)
		{
			return BlrtUtil.DownloadMedia (priority,
										   LocalStorage.DataManager.GetMediaAssets (mediaIds),
			                               this,
										   UpdateFileState,
			                               UpdateFileState,
			                               UpdateAssocStatus);
		}

		void UpdateAssocStatus (object sender, EventArgs e)
		{
            OnPropertyChanged ("FileState");
		}

		public IO.BlrtDownloader[] DownloadThumbnail  (int priority)
		{
			if (this.OnParse){

				if (!string.IsNullOrEmpty(LocalThumbnailName) && !BlrtUtil.CanUseFile (LocalThumbnailPath)) {
					var downloader = IO.BlrtDownloader.Download (priority, CloudThumbnailPath, LocalThumbnailPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (this,	downloader);

					downloader.OnDownloadFailed += UpdateFileState;
					downloader.OnDownloadFinished += UpdateFileState;

					return new BlrtData.IO.BlrtDownloader[]{ downloader };
				}
			}
			return new BlrtData.IO.BlrtDownloader[0];
		}

		public void StartFileMonitoring ()
		{
			if (OnParse) {
				if (!string.IsNullOrEmpty (LocalThumbnailPath)) {
					MediaChecker.GetMediaChecker ()?.Subscribe (LocalThumbnailPath, this);
				}

				var items = LocalStorage.DataManager.GetConversationItems (ObjectId);
				lock (items) {
					foreach (var item in items) {
						item?.StartFileMonitoring (this, 0);
					}
				}
			}
		}

		public void StopFileMonitoring ()
		{
			MediaChecker.GetMediaChecker ()?.UnSubscribe (LocalThumbnailPath, this);

			var items = LocalStorage.DataManager.GetConversationItems (ObjectId);
			foreach (var item in items) {
				item?.StopFileMonitoring (this, 0);
			}
		}

		public CloudState GetCloudState ()
		{
			CloudState state = CloudState.Synced;

			if (OnParse) {
			
				var items = LocalStorage.DataManager.GetConversationItems (ObjectId);

				var checker = MediaChecker.GetMediaChecker ();
				if (!string.IsNullOrEmpty (LocalThumbnailPath)) {
					switch (checker.GetState (LocalThumbnailPath)) {
						case CheckerState.ContentMissing:
							state = CloudState.ContentMissing;
							break;
						case CheckerState.DownloadingContent:
							state = CloudState.DownloadingContent;
							break;
						case CheckerState.Synced:
							state = CloudState.Synced;
							break;
					}
				}

				if (items.Count (c => c.OnParse) != GeneralCount)
					state = CloudState.ContentMissing;

				foreach (var item in items) {
					var cs = item.GetCloudState ();

					switch (cs) {
						case CloudState.ContentMissing:
							if (state == CloudState.Synced)
								state = cs;
							break;
						case CloudState.DownloadingContent:
							if (state != CloudState.UploadingContent)
								state = cs;
							break;
						case CloudState.UploadingContent:
							state = cs;
							break;
						case CloudState.ChildrenNotOnCloud:
						case CloudState.NotOnCloud:
							if (state == CloudState.Synced || state == CloudState.ContentMissing)
								state = CloudState.ChildrenNotOnCloud;
							break;
					}
				}

			} else {
			
				if(ShouldUpload)
					state = CloudState.UploadingContent;
				else
					state = CloudState.NotOnCloud;
			}
			return state;
		}

		[Serializable]
		public enum ActivityStatus{
			Completed,
			InProgress,
			Failed
		}

		#region IMediaCheckerSubscriber implementation

		void IMediaCheckerListener.PathStateChanged (string path)
		{
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				OnPropertyChanged("FileState");
			});
		}

		/// <summary>
		/// Removes the media from unused media list. It marks the itms as used rather than removing them
		/// </summary>
		/// <param name="media">media ids</param>
		internal void RemoveMediaFromUnusedMediaList (IEnumerable<string> media)
		{
			if (_unusedConversationMediaItems == null || media == null) {
				return;
			}

			foreach (var m in media) {
				
				var key = string.Empty;
				if (_unusedConversationMediaItems.ContainsKey (key = m) || _unusedConversationMediaItems.ContainsKey (key = GetMediaAssetAlternativeId (m))) {
					_unusedConversationMediaItems [key] = false;
				}
			}
		}

		/// <summary>
		/// Gets the media asset alternative identifier.
		/// </summary>
		/// <returns>either localGUID or objectId depending upon the type of the id passed as a parameter</returns>
		/// <param name="id">Id</param>
		string GetMediaAssetAlternativeId (string id)
		{
			var mediaAsset = LocalStorage.DataManager.GetMediaAsset (id);
			return IsLocalGUID (id) ? mediaAsset.ObjectId : mediaAsset.LocalGuid;
		}

		/// <summary>
		/// Checks whetehr the provided id is localGUID
		/// </summary>
		/// <returns><c>true</c>, id provided is localGUID, <c>false</c> otherwise.</returns>
		/// <param name="id">Identifier.</param>
		bool IsLocalGUID (string id)
		{
			return id.Contains (BlrtBase.LOCAL_GUID_KEY);
		}

		/// <summary>
		/// Adds the unused conversation media items. Adds new "key,value" pairs
		/// </summary>
		/// <param name="media">media ids</param>
		internal void AddUnusedConversationMediaItems (IEnumerable<string> media)
		{   
			if (_unusedConversationMediaItems == null) {
				_unusedConversationMediaItems = new Dictionary<string, bool> ();
			}

			List<string> newItems = new List<string> ();

			foreach (var m in media) {
				var asset = LocalStorage.DataManager.GetMediaAsset (m);
				if (asset != null) {
					var objId = asset.ObjectId;
					var localId = asset.LocalGuid;

					if (_unusedConversationMediaItems.ContainsKey (objId) ||
						_unusedConversationMediaItems.ContainsKey (localId)) {
						continue;
					}
				}

				if (!_unusedConversationMediaItems.ContainsKey (m)) {
					newItems.Add (m);
				}
			}

			if (newItems.Count () > 0) {
				foreach (var item in newItems) {
					_unusedConversationMediaItems.Add (item, true);
				}
				OnPropertyChanged ("UnusedConversationMediaItems");
			}
		}

		internal void UpdateBlrtCount (byte blrtCount)
		{
			BlrtCount += blrtCount;
		}

		#endregion
	}

	public enum ContactState
	{
		NotSend,
		InvalidContact,
		ConversationSentUser,
		ConversationSentUnreachable,
		ConversationSentReachable,
		ConversationAlreadySent,
		Sending,
		FailedToSend,
		Blocked
	}

	[Serializable]
	public class ContactStatus
	{
		public ContactState Status{get;set;}
		public DateTime UpdateAt{get;set;}
		public BlrtData.Contacts.BlrtContactInfo SelectedInfo{get;set;}

		public override bool Equals (object obj)
		{
			if(obj is ContactStatus){
				return this.SelectedInfo.Equals((obj as ContactStatus).SelectedInfo);
			}
			return base.Equals (obj);
		}
	}
}

