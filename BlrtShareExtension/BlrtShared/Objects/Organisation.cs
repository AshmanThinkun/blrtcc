﻿using System;
using Parse;

namespace BlrtData
{
	[Serializable]
	public class Organisation: BlrtBase
	{
		#region implemented abstract members of BlrtBase
		public const string CLASS_NAME 			= "Organisation";
		public override bool TryUpgrade ()
		{
			return false;
		}
		public override string ClassName {
			get {
				return "Organisation";
			}
		}
		public override int CurrentVersion {
			get {
				return 0;
			}
		}
		public override int OldestSupportedVersion {
			get {
				return 0;
			}
		}
		#endregion


		public string Name{ get; set;}
		public string UniqueId { get; set;}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
			if (parseObject == null)
				return;
			
			this.Name = parseObject.Get<string> ("name", "");
			this.Name = parseObject.Get<string> ("uniqueId", "");
		}
	}
}

