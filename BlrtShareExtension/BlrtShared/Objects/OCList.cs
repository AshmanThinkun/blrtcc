﻿using System;
using Parse;
using System.Collections.Generic;

namespace BlrtData
{
	[Serializable]
	public class OCList:BlrtBase
	{
		#region implemented abstract members of BlrtBase
		public const string CLASS_NAME 			= "OCList";
		public const int CURRENT_VERSION     = 0 ;
		public override bool TryUpgrade ()
		{
			return false;
		}
		public override string ClassName {
			get {
				return CLASS_NAME;
			}
		}
		public override int CurrentVersion {
			get {
				return 0;
			}
		}
		public override int OldestSupportedVersion {
			get {
				return 0;
			}
		}
		#endregion


		public string ChannelId { get; set; }
		public string Name { get; set; }
		public string MenuTitle { get; set; }

		public string CloudTopBannerImagePath { get; set; }
		public string LocalTopBannerImagePath { 
			get {
				if(string.IsNullOrEmpty(CloudTopBannerImageName)){
					return null;
				}
				//image path + name
				return BlrtConstants.Directories.Default.GetCachedPath (CloudTopBannerImageName);
			}
		}

		private string CloudTopBannerImageName{ get; set;}

		public string TopBannerTargetUrl{ get; set;}

		public ActionBanner BottomBanner{ get; set;}

		public string ShareCustomText{ get; set;}

		public DateTime ContentValidAfter{ get; set;}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
			if (parseObject == null)
				return;
			this.ChannelId = parseObject.Get<ParseObject> ("channel", null)?.ObjectId;
			this.Name = parseObject.Get<string> ("name", "");
			this.MenuTitle = parseObject.Get<string> ("menu_title", "");

			var imageFile = parseObject.Get<ParseFile> ("topBannerImage", null);
			if(imageFile!=null){
				this.CloudTopBannerImageName = imageFile.Name;
				this.CloudTopBannerImagePath = imageFile.Url.ToString ();
			}

			this.TopBannerTargetUrl = parseObject.Get<string> ("topBannerTargetUrl", "");

			var bottomBanner = parseObject.Get<Dictionary<string,object>> ("bottomBanner", null);
			if(bottomBanner!=null){
				var banner = new ActionBanner ();
				banner.ButtonTitle = bottomBanner ["buttonTitle"].ToString();
				banner.Title = bottomBanner ["title"].ToString();
				banner.Detail = bottomBanner ["detail"].ToString();
				banner.ButtonUri = bottomBanner ["buttonUri"].ToString();
				this.BottomBanner = banner;
			}else{
				this.BottomBanner = null;
			}

			this.ShareCustomText = parseObject.Get<string> ("shareCustomText", "");

			this.ContentValidAfter = parseObject.Get<DateTime> ("contentValidAfter", DateTime.MinValue);
		}

		[Serializable]
		public class ActionBanner
		{
			public string Title{ get; set;}
			public string Detail{ get; set;}
			public string ButtonTitle{ get; set;}
			public string ButtonUri{ get; set;}
		}
	}
}

