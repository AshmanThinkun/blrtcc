using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.IO;
using BlrtData.Contacts;


namespace BlrtData
{
	[Serializable]
	public class BlrtUser : INotifyPropertyChanged
	{

		[NonSerialized]
		public static string DELETED_ACCOUNT_STATUS_STRING = "deleted";
		public static string SUSPENDED_ACCOUNT_STATUS_STRING = "suspended";

		[NonSerialized]
		private PropertyChangedEventHandler _changed;
		public event PropertyChangedEventHandler PropertyChanged
		{
			add { _changed += value; }
			remove { _changed -= value; }
		}

		protected bool NotifyPropertyChanged<T>(ref T current, T value,  [CallerMemberName] String propertyName = "")
		{
			if(!EqualityComparer<T>.Default.Equals(current, value)) {
				current = value;

				var pc = _changed;
				if (pc != null) pc(this, new PropertyChangedEventArgs(propertyName));

				return true;
			}

			return false;
		}

		string _displayName;
		string _email, _avatarCloudPath, _organization, _avatarName, _accountStatus;
		bool _hasSetDisplayName, _isPhoneLinked;
		long _gender, _publicBlrtCount, _accountThreshold;
		bool _known;

		[NonSerialized]
		bool _received;

		public bool NeedFetch { get { return !_received || TimeStamp.AddHours (1) < DateTime.Now;} }

		public bool IsDeleted {
			get {
				return AccountStatus?.Equals (DELETED_ACCOUNT_STATUS_STRING) ?? false;
			}
		}

		public string ObjectId 		{ get; private set; }
		public string FacebookId 	{ get; private set; }
		public DateTime TimeStamp 	{ get; set; }
		public DateTime BlrtUserCreatedAt 	{ get; set; }

		public string DisplayName 	{ get { return _displayName; } set { NotifyPropertyChanged (ref _displayName, value); } }
		public string Email 		{ get { return _email; } set { NotifyPropertyChanged (ref _email, value); } }
		public bool Found 			{ get { return _known; } set { NotifyPropertyChanged (ref _known, value); } }
		public string AvatarCloudPath 	{ get { return _avatarCloudPath; } set { NotifyPropertyChanged (ref _avatarCloudPath, value); } }
		public string AvatarName	{ get { return _avatarName; } set { NotifyPropertyChanged (ref _avatarName, value); } }
		public long Gender 		{ get { return _gender; } set { NotifyPropertyChanged (ref _gender, value); } }
		public string Organization  { get { return _organization; } set { NotifyPropertyChanged (ref _organization, value); } }
		public bool HasSetDisplayName  { get { return _hasSetDisplayName; } set { NotifyPropertyChanged (ref _hasSetDisplayName, value); } }
		public long PublicBlrtCount 		{ get { return _publicBlrtCount; } set { NotifyPropertyChanged (ref _publicBlrtCount, value); } }
		public long AccountThreshold 		{ get { return _accountThreshold; } set { NotifyPropertyChanged (ref _accountThreshold, value); } }
		public bool IsPhoneLinked { get { return _isPhoneLinked; } set { NotifyPropertyChanged (ref _isPhoneLinked, value); } }
		public string AccountStatus { get { return _accountStatus; } set { NotifyPropertyChanged (ref _accountStatus, value); } }


		[NonSerialized]
		BlrtData.Contacts.BlrtContact _localContact;

		public string LocalAvatarPath{
			get{
				if(!string.IsNullOrEmpty(AvatarName)&& File.Exists(BlrtConstants.Directories.Default.GetAvatarPath (AvatarName))){
					return BlrtConstants.Directories.Default.GetAvatarPath (AvatarName);
				}else{
					if(_localContact==null){
						_localContact = BlrtData.Contacts.BlrtContactManager.SharedInstanced.GetMatching (this);
					}
					if(_localContact!=null && _localContact.Avatar!=null && File.Exists(_localContact.Avatar.Path)){
						return _localContact.Avatar.Path;
					}

					return "";
				}
			}
		}

		public List<BlrtContactInfo> InfoList{ get; set;}

		public BlrtUser (string objectId)
		{
			this.ObjectId 	= objectId;
			DisplayName 	= Email = "";
			Found 			= true;
			InfoList = new List<BlrtContactInfo> ();
		}

		public class BlrtUserReturnType{
			public string id;
			public string name;
			public bool hasSetDisplayName;
			public string email;
			public string avatarUrl;
			public string avatarName;
			public long gender;
			public string organization;
			public long publicBlrtCount;
			public DateTime createdAt;
			public string facebookId;
			public long accountThreshold;
			public bool isPhoneLinked;
			public string accountStatus;

			[Newtonsoft.Json.JsonProperty("contactDetails")]
			public IEnumerable<IDictionary<string,string>> contactDetails;
		}

		public static BlrtUser FromParseObject(BlrtUserReturnType d){
			var b = new BlrtUser (d.id) {
				DisplayName = d.name.Trim (),
				HasSetDisplayName = d.hasSetDisplayName,
				Email = d.email,
				AvatarCloudPath = d.avatarUrl,
				AvatarName = d.avatarName,
				Gender = d.gender,
				Organization = d.organization,
				PublicBlrtCount = d.publicBlrtCount,
				TimeStamp = DateTime.Now,
				BlrtUserCreatedAt = d.createdAt,
				FacebookId = d.facebookId,
				AccountThreshold = d.accountThreshold,
				IsPhoneLinked = d.isPhoneLinked,
				Found = true,
				AccountStatus = d.accountStatus
			};
			if (d.contactDetails!=null) {
				var details = d.contactDetails;

				if (details == null)
					return b;
				
				foreach (var detail in details) {
					BlrtContactInfoType type;
					switch (detail["type"]) {
						case "facebook":
							type = BlrtContactInfoType.FacebookId;
							break;
						case "phoneNumber":
							type = BlrtContactInfoType.PhoneNumber;
							break;
						default:
							type = BlrtContactInfoType.Email;
							break;
					}
					b.InfoList.Add (new BlrtContactInfo (type, detail ["value"]){
						IsLinkToBlrtUser = true
					});
				}
			}
			return b;
		}


		public string GetInitials()
		{
			var s = DisplayName.Trim().Split(' ');

			if (s.Length == 0 || s[0].Length == 0)
				return "";

			if (s.Length == 1)
				return s [0] [0]+ "";

			return s [0][0] + "" + s [s.Length - 1][0];
		}

		public string ContactInfo{
			get{
				if (string.IsNullOrWhiteSpace (Email))
					return "";

				return string.Format("Email: {0}", Email);
			}
		}

		public void SetData (BlrtUser user) {
			DisplayName		= user.DisplayName;
			HasSetDisplayName = user.HasSetDisplayName;
			Email			= user.Email;
			AvatarCloudPath = user.AvatarCloudPath;
			AvatarName 		= user.AvatarName;
			Gender 			= user.Gender;
			Organization 	= user.Organization;
			PublicBlrtCount = user.PublicBlrtCount;
			TimeStamp		= user.TimeStamp;
			BlrtUserCreatedAt = user.BlrtUserCreatedAt;
			FacebookId      = user.FacebookId;
			AccountThreshold = user.AccountThreshold;
			InfoList 		= user.InfoList;
			IsPhoneLinked   = user.IsPhoneLinked;
			Found			= user.Found;
			AccountStatus   = user.AccountStatus;

			_received		= true;
		}


		public override string ToString ()
		{
			return DisplayName;
		}

		public static BlrtUser Default {
			get {
				return new BlrtUser(""){
					DisplayName = "...",
					Found = false
				};
			}
		}

		public static BlrtUser CreateBlank(string objectId) {
			return new BlrtUser(objectId){
				DisplayName = "...",
				Found = false,
				TimeStamp = DateTime.Now
			};
		}
	}
}

