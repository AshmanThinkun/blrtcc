using System;
using Parse;
using Newtonsoft.Json;
using BlrtData.Media;
using BlrtData.IO;
using System.IO;
using System.Threading.Tasks;

namespace BlrtData
{
	[Serializable]
	public sealed class MediaAsset : BlrtBase
	{
		public const string CLASS_NAME = "MediaAsset";
		public override string ClassName { get { return CLASS_NAME; } }

		public const int CURRENT_VERSION = 5;
		public override int CurrentVersion { get { return CURRENT_VERSION; } }

		public const int OLDEST_SUPPORTED_VERSION = 5;
		public override int OldestSupportedVersion { get { return OLDEST_SUPPORTED_VERSION; } }

		public const string NAME_KEY = "name";
		public const string MEDIA_FORMAT_KEY = "mediaFormat";
		public const string MEDIA_FILE_KEY = "mediaFile";
		public const string PAGE_ARGS_KEY = "pageArgs";
		public const string THUMBNAIL_KEY = "thumbnailFile";


		string _name;
		BlrtMediaFormat _format;
		int _templateId;

		string _localMediaName;
		string _localThumbnailName;

		Uri _cloudMediaPath;
		Uri _cloudThumbnailPath;

		int [] _pageArgs;


		public string Name {
			get {
				return _name;
			}
			set {
				if (value != _name) {
					_name = value;
					OnPropertyChanged ("Name");
				}
			}
		}
		public BlrtMediaFormat Format {
			get {
				return _format;
			}
			set {
				if (value != _format) {
					_format = value;
					OnPropertyChanged ("StatusCode");
				}
			}
		}
		public int TemplateId {
			get {
				return _templateId;
			}
			set {
				if (value != _templateId) {
					_templateId = value;
					OnPropertyChanged ("TemplateId");
				}
			}
		}

		public string LocalMediaName {
			get {
				return _localMediaName;
			}
			set {
				if (value != _localMediaName) {
					_localMediaName = value;
					OnPropertyChanged ("LocalMediaName");
				}
			}
		}
		public Uri CloudMediaPath {
			get {
				return _cloudMediaPath;
			}
			set {
				if (value != _cloudMediaPath) {
					_cloudMediaPath = value;
					OnPropertyChanged ("CloudMediaPath");
				}
			}
		}

		public string LocalThumbnailName {
			get {
				return _localThumbnailName;
			}
			set {
				if (value != _localThumbnailName) {
					_localThumbnailName = value;
					OnPropertyChanged ("LocalThumbnailName");
				}
			}
		}
		public Uri CloudThumbnailPath {
			get {
				return _cloudThumbnailPath;
			}
			set {
				if (value != _cloudThumbnailPath) {
					_cloudThumbnailPath = value;
					OnPropertyChanged ("CloudThumbnailPath");
				}
			}
		}

		public int [] PageArgs {
			get {
				return _pageArgs;
			}
			set {
				if (value != _pageArgs) {
					_pageArgs = value;
					OnPropertyChanged ("PageArgs");
				}
			}
		}

		[JsonIgnore]
		public string LocalThumbnailPath {
			get {
				return OnParse ? BlrtConstants.Directories.Default.GetThumbnailPath (LocalThumbnailName) :
								 BlrtConstants.Directories.Default.GetCachedPath (LocalThumbnailName);
			}
		}

		[JsonIgnore]
		public string LocalMediaPath {
			get {
				if (Format == BlrtMediaFormat.Template)
					return BlrtTemplate.GetImagePath (TemplateId);
				if (!OnParse)
					return LocalStorage.LocalDirectories.GetCachedPath (LocalMediaName);

				return LocalStorage.LocalDirectories.GetFilesPath (LocalMediaName);
			}
		}

		[JsonIgnore]
		public string DecyptedPath {
			get {
				return LocalStorage.LocalDirectories.GetDecryptedPath (LocalMediaName);
			}
		}

		[JsonIgnore]
		public bool IsDecypted {
			get {
				return BlrtUtil.CanUseFile (DecyptedPath, false);
			}
		}

		[JsonIgnore]
		[NonSerialized]
		long? _fileSizeByte = null;

		[JsonIgnore]
		public long FileSizeByte {
			get {
				if (!_fileSizeByte.HasValue) {
					switch (Format) {
					case BlrtMediaFormat.Image:
					case BlrtMediaFormat.PDF: {
							try {
								var fileInfo = new FileInfo (LocalMediaPath);
								var flength = fileInfo.Length;
								if (flength < 0) {
									_fileSizeByte = null;
								} else {
									_fileSizeByte = flength;
								}
							} catch { }
						}
						break;
					case BlrtMediaFormat.Template:
						_fileSizeByte = 0;
						break;
					case BlrtMediaFormat.Invalid:
					default:
						return 0;
					}
				}
				return _fileSizeByte.HasValue ? _fileSizeByte.Value : 0;
			}
		}

		public MediaAsset (LocalStorageType localStorageType = LocalStorageType.Default) : base (localStorageType)
		{
			TemplateId = -1;
			Format = BlrtMediaFormat.Template;
			_fileSizeByte = -1;
		}

		public MediaAsset (ParseObject parseObject, LocalStorageType localStorageType = LocalStorageType.Default) : base (parseObject, localStorageType)
		{
			_fileSizeByte = -1;
		}

		public override void CopyToParse (ref ParseObject parseObject)
		{
			if (parseObject == null) {
				parseObject = new ParseObject (CLASS_NAME);
				if (ObjectId != null && ObjectId.Length > 0)
					parseObject.ObjectId = ObjectId;
			}

			parseObject [NAME_KEY] = this.Name;
			parseObject [PAGE_ARGS_KEY] = BlrtUtil.ListToPages (this.PageArgs);

			base.CopyToParse (ref parseObject);
		}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			if (parseObject == null)
				return;

			if (parseObject.ClassName != CLASS_NAME
				|| (OnParse && parseObject.ObjectId != null && parseObject.ObjectId.Length > 0 && ObjectId.Length > 0 && parseObject.ObjectId != this.ObjectId))
				return;

			if (!OnParse) {
				LocalStorage.DataManager.ReplaceMediaAssetId (this.LocalGuid, parseObject.ObjectId);
			}

			base.ApplyParseObject (parseObject);


			ParseFile mediaFile = parseObject.Get<ParseFile> (MEDIA_FILE_KEY, null);

			if (mediaFile != null) {
				LocalMediaName = mediaFile.Name;
				CloudMediaPath = mediaFile.Url;
			}

			ParseFile thumbnail = parseObject.Get<ParseFile> (THUMBNAIL_KEY, null);

			if (thumbnail != null) {
				LocalThumbnailName = thumbnail.Name;
				CloudThumbnailPath = thumbnail.Url;
			}

			this.Name = parseObject.Get<string> (NAME_KEY, "");
			this.Format = (BlrtMediaFormat)parseObject.Get<int> (MEDIA_FORMAT_KEY, 0);
			this.TemplateId = -1;


			if (parseObject.ContainsKey (PAGE_ARGS_KEY)) {

				PageArgs = BlrtUtil.PagesToList (parseObject.Get<string> (PAGE_ARGS_KEY, "1")).ToArray (); ;

			} else {

				if (Format == BlrtMediaFormat.PDF) {
					PageArgs = null;
				} else {
					PageArgs = new int [] { 1 };
				}

			}
		}

		public override void DeleteData ()
		{
			base.DeleteData ();

			try {
				File.Delete (LocalMediaPath);
				File.Delete (DecyptedPath);
				File.Delete (LocalThumbnailPath);
			} catch { }
		}

		public bool IsThumbnailLocal ()
		{
			return BlrtUtil.CanUseFile (LocalThumbnailPath, false);
		}

		public bool IsDataLocal ()
		{
			switch (Format) {
			case BlrtMediaFormat.Template:
				return BlrtTemplate.IsLocal (TemplateId);
			default:
				return BlrtUtil.CanUseFile (LocalMediaPath, false);
			}
		}

		public IO.BlrtDownloader DownloadThumbnail (int priority)
		{
			var d = IO.BlrtDownloader.Download (priority, CloudThumbnailPath, LocalThumbnailPath);
			IO.BlrtDownloaderAssociation.CreateAssociation (this, d);

			return d;
		}

		public IO.BlrtDownloader [] DownloadContent (int priority)
		{
			var d = IO.BlrtDownloader.Download (priority, CloudMediaPath, LocalMediaPath);
			IO.BlrtDownloaderAssociation.CreateAssociation (this, d);

			return new BlrtDownloader [] { d };
		}

		public override BlrtBaseIssues GetIssues ()
		{
			var issues = base.GetIssues ();

			if (!Enum.IsDefined (typeof (BlrtMediaFormat), Format))
				issues = issues | BlrtBaseIssues.UnknownMediaFormat;

			return issues;
		}

		public override bool TryUpgrade ()
		{
			return false; // No upgrades supported
		}

		[NonSerialized]
		private IO.BlrtParseFileHelper _uploader;

		[NonSerialized]
		IO.BlrtParseFileHelper _thumbnailUploader;

		public bool ThumbnailOnParse { get {return CloudThumbnailPath != null;}	}

		public IO.BlrtParseFileHelper TryUploadThumbnail ()
		{
			if (CloudThumbnailPath != null) {
				throw new Exception ("Thumbnail already on parse");
			}

			if (ThumbnailUploader == null && !string.IsNullOrEmpty (LocalThumbnailName)) {
				ThumbnailUploader = BlrtParseFileHelper.FromFile ("thumb", LocalThumbnailPath);
				ThumbnailUploader.IsThumbnail = true;
			}

			if (!ThumbnailUploader.IsActive && !ThumbnailUploader.IsFinished)
				ThumbnailUploader.Retry ();

			return ThumbnailUploader;
		}

		public IO.BlrtParseFileHelper  TryUploadAsset ()
		{
			if (OnParse) {
				throw new Exception ("Media Asset already on parse");
			}

			if (_uploader == null) {
				_uploader = BlrtParseFileHelper.FromFile ("media", LocalMediaPath);
			}

			if(!_uploader.IsActive && !_uploader.IsFinished)
				_uploader.Retry();

			return _uploader;
		}

		public IO.BlrtParseFileHelper Uploader { get { return _uploader; }}

		public IO.BlrtParseFileHelper ThumbnailUploader 
		{ 
			get {return _thumbnailUploader;} 
			set {_thumbnailUploader = value;}
		}

		public void  ClearUploadAsset ()
		{
			if (_uploader != null) {
				_uploader.Dispose ();
				_uploader = null;
			}
		}

		public void ClearUploadThumbnail ()
		{
			if (ThumbnailUploader != null) {
				ThumbnailUploader.Dispose ();
				ThumbnailUploader = null;
			}
		}

		public void StartFileMonitoring (IMediaCheckerListener listener)
		{
			if (OnParse) {
				if (!string.IsNullOrEmpty (LocalMediaPath)) {
					MediaChecker.GetMediaChecker ().Subscribe (LocalMediaPath, listener);
				}
			}
		}

		public void StopFileMonitoring (IMediaCheckerListener listener)
		{
			if (!string.IsNullOrEmpty (LocalMediaPath)) {
				MediaChecker.GetMediaChecker ().UnSubscribe (LocalMediaPath, listener);
			}
		}

		public CloudState GetCloudState ()
		{
			CloudState state = CloudState.Synced;

			if (OnParse) {
				var checker = MediaChecker.GetMediaChecker ();

				if (!string.IsNullOrEmpty (LocalMediaPath)) {
					switch (checker.GetState (LocalMediaPath)) {
						case CheckerState.ContentMissing:
							state = CloudState.ContentMissing;
							break;
						case CheckerState.DownloadingContent:
							state = CloudState.DownloadingContent;
							break;
						case CheckerState.Synced:
							state = CloudState.Synced;
							break;
					}
				}

			} else {

				if (ShouldUpload)
					state = CloudState.UploadingContent;
				else
					state = CloudState.NotOnCloud;
			}
			return state;
		}

		public async Task DecyptData ()
		{
			if (IsDataLocal ()) {
				await BlrtEncryptorManager.Decrypt (
					LocalMediaPath,
					DecyptedPath,
					this
				);
			} else {
				throw new InvalidOperationException ("Cannot decrypt file before it is downloaded");
			}
		}
	}
}

