using System;
using Parse;
using BlrtData.Contents;
using BlrtData.Contents.Event;

namespace BlrtData.Builder
{
	public static class BlrtBaseBuilder
	{
		public static bool ShouldApply(BlrtBase b, ParseObject parse)
		{
			if(b != null && b.Dirty && b.OnParse){
				if( b.ChangedAt > BlrtUtil.ToLocalTime (parse.UpdatedAt ?? DateTime.Now))
					LLogger.WriteLine("{0} was dirty and is {1} than the parseobject", b.GetType().ToString(), "newer");
				else
					LLogger.WriteLine("{0} was dirty and is {1} than the parseobject", b.GetType().ToString(), "older");
			}

			return b == null 
				|| !(b.Deleted || (b.Dirty &&  b.OnParse && b.ChangedAt > BlrtUtil.ToLocalTime (parse.UpdatedAt ?? DateTime.Now)));
		}

		private static bool IsSame(ParseObject parseObject, BlrtBase bb){

			if (bb == null)
				return true;

			if (parseObject.ClassName != bb.ClassName)
				return false;
			if(parseObject.ObjectId  == bb.ObjectId)
				return true;

			var guid = parseObject.Get<string> (BlrtBase.LOCAL_GUID_KEY, null);

			return !bb.OnParse && !string.IsNullOrWhiteSpace (guid) && guid == bb.LocalGuid;
		}

		public static Conversation BuildConversation(ParseObject parseObject, Conversation conversation = null, bool forceApplied = false)
		{
			if (parseObject == null
				|| parseObject.ClassName != Conversation.CLASS_NAME
				|| !IsSame(parseObject, conversation))
				return null;


			int version = 0;

			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}


			if (version < (conversation == null ? Conversation.CURRENT_VERSION : conversation.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (conversation, parseObject)) {
				try{
					if (conversation == null) {
						conversation = new Conversation (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						conversation.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", conversation == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return conversation;
		}

		public static ConversationUserRelation BuildRelation(ParseObject parseObject, ConversationUserRelation relation = null, bool forceApplied = false)
		{
			if (parseObject == null
				|| parseObject.ClassName != ConversationUserRelation.CLASS_NAME
				|| !IsSame(parseObject, relation))
				return null;


			int version = 0;


			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}

			if (version < (relation == null ? ConversationUserRelation.CURRENT_VERSION : relation.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (relation, parseObject)) {
				try{
					if (relation == null) {
						relation = new ConversationUserRelation (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						relation.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", relation == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return relation;
		}

		public static MediaAsset BuildMediaAsset(ParseObject parseObject, MediaAsset mediaAsset = null, bool forceApplied = false, LocalStorageType localStorageType = LocalStorageType.Default)
		{
			if (parseObject == null
				|| parseObject.ClassName != MediaAsset.CLASS_NAME
				|| !IsSame(parseObject, mediaAsset))
				return null;


			int version = 0;


			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}


			if (version < (mediaAsset == null ? MediaAsset.CURRENT_VERSION : mediaAsset.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (mediaAsset, parseObject)) {
				try{
					if (mediaAsset == null) {
						mediaAsset = new MediaAsset (parseObject, localStorageType);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						mediaAsset.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", mediaAsset == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return mediaAsset;
		}

		public static OCList BuildOCList(ParseObject parseObject, OCList oclist = null, bool forceApplied = false)
		{
			if (parseObject == null
				|| parseObject.ClassName != OCList.CLASS_NAME
				|| !IsSame(parseObject, oclist))
				return null;


			int version = 0;

			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}


			if (version < (oclist == null ? OCList.CURRENT_VERSION : oclist.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (oclist, parseObject)) {
				try{
					if (oclist == null) {
						oclist = new OCList ();
						oclist.ApplyParseObject(parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						oclist.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", oclist == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return oclist;
		}


		public static OCListItem BuildOCListItem(ParseObject parseObject, OCListItem oclistItem = null, bool forceApplied = false)
		{
			if (parseObject == null
				|| parseObject.ClassName != OCListItem.CLASS_NAME
				|| !IsSame(parseObject, oclistItem))
				return null;


			int version = 0;

			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}


			if (version < (oclistItem == null ? OCListItem.CURRENT_VERSION : oclistItem.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (oclistItem, parseObject)) {
				try{
					if (oclistItem == null) {
						oclistItem = new OCListItem ();
						oclistItem.ApplyParseObject(parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						oclistItem.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", oclistItem == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return oclistItem;
		}

		public static UserOrganisationRelation BuildUserOrganisationRelation (ParseObject parseObject, UserOrganisationRelation relation = null, bool forceApplied = false)
		{
			if (parseObject == null
				|| parseObject.ClassName != UserOrganisationRelation.CLASS_NAME
				|| !IsSame(parseObject, relation))
				return null;


			int version = 0;

			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}


			if (version < (relation == null ? UserOrganisationRelation.CURRENT_VERSION : relation.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (relation, parseObject)) {
				try{
					if (relation == null) {
						relation = new UserOrganisationRelation ();
						relation.ApplyParseObject(parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						relation.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", relation == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return relation;
		}

		public static ConversationItem BuildContentItem (ParseObject parseObject, ConversationItem item, bool forceApplied)
		{
			if (parseObject == null
				|| parseObject.ClassName != ConversationItem.CLASS_NAME
				|| !IsSame(parseObject, item))
				return null;

			int type = 0;
			int version = 0;


			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version)) {
				LLogger.WriteLine ("{0}-{1} has no version value!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}



			if (!parseObject.TryGetValue<int>( ConversationItem.TYPE_KEY, out type )){
				LLogger.WriteLine ("{0}-{1} has no version content type!", parseObject.ClassName, parseObject.ObjectId);
				return null;
			}

			switch ((BlrtContentType)type) {
				case BlrtContentType.Blrt:
					if (item == null || item is ConversationBlrtItem)
						return BuildConversationBlrtItem (parseObject, item as ConversationBlrtItem, forceApplied);
					break;
				case BlrtContentType.BlrtReply:
					if(item == null || item is ConversationReBlrtItem)
						return BuildConversationReBlrtItem(parseObject, item as ConversationReBlrtItem, forceApplied);
					break;
				case BlrtContentType.Comment:
					if(item == null || item is ConversationCommentItem)
						return BuildConversationCommentItem(parseObject, item as ConversationCommentItem, forceApplied);
					break;
				case BlrtContentType.AddEvent:
					return BuildConversationAddEventItem(parseObject, item as ConversationAddEventItem, forceApplied);
				case BlrtContentType.MessageEvent:
					return BuildConversationMessageEventItem(parseObject, item as ConversationMessageEventItem, forceApplied);
				case BlrtContentType.Media:
					return BuildConversationMediaItem (parseObject, item as ConversationMediaItem, forceApplied);
				case BlrtContentType.Audio:
					return BuildConversationAudioItem (parseObject, item as ConversationAudioItem, forceApplied);
				default:
					return BuildConversationEventItem(parseObject, item as ConversationEventItem, forceApplied);
			}

			return null;
		}

		private static ConversationMediaItem BuildConversationMediaItem (ParseObject parseObject, ConversationMediaItem item, bool forceApplied, ILocalStorageParameter localStorage = null)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty) {
					dirty = false;
					switch (version) {
					default:
						break;
					}
				}

			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try {
					if (item == null) {
						item = new ConversationMediaItem (parseObject, localStorage) { 
							//LocalThumbnailNames = new string [1],
							//CloudThumbnailPaths = new Uri [1]
						};
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e) {
					LLogger.WriteEvent (string.Format ("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString ());
					throw e;
				}
			}

			return item;
		}

		private static ConversationAudioItem BuildConversationAudioItem (ParseObject parseObject, ConversationAudioItem item, bool forceApplied, ILocalStorageParameter localStorage = null)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int> (BlrtBase.VERSION_KEY, out version))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty) {
					dirty = false;
					switch (version) {
					default:
						break;
					}
				}

			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try {
					if (item == null) {
						item = new ConversationAudioItem (parseObject, localStorage) {
							//LocalThumbnailNames = new string [1],
							//CloudThumbnailPaths = new Uri [1]
						};
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e) {
					LLogger.WriteEvent (string.Format ("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString ());
					throw e;
				}
			}

			return item;
		}

		private static ConversationBlrtItem BuildConversationBlrtItem (ParseObject parseObject, ConversationBlrtItem item, bool forceApplied, ILocalStorageParameter localStorage = null)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationBlrtItem (parseObject, localStorage) { 
							LocalThumbnailNames = new string [1],
							CloudThumbnailPaths = new Uri [1] 
						};
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}

			return item;
		}

		private static ConversationReBlrtItem BuildConversationReBlrtItem (ParseObject parseObject, ConversationReBlrtItem item, bool forceApplied)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationReBlrtItem (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}
			return item;
		}

		private static ConversationCommentItem BuildConversationCommentItem (ParseObject parseObject, ConversationCommentItem item, bool forceApplied)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}
				
			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationCommentItem (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}
			return item;
		}

		static ConversationEventItem BuildConversationEventItem (ParseObject parseObject, ConversationEventItem item, bool forceApplied)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationEventItem (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}
			return item;
		}

		static ConversationAddEventItem BuildConversationAddEventItem (ParseObject parseObject, ConversationAddEventItem item, bool forceApplied)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty) {
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationAddEventItem (parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}
			return item;
		}

		private static ConversationMessageEventItem BuildConversationMessageEventItem (ParseObject parseObject, ConversationMessageEventItem item, bool forceApplied)
		{
			int version = 0;
			if (!parseObject.TryGetValue<int>( BlrtBase.VERSION_KEY, out version ))
				return null;


			if (version < (item == null ? ConversationItem.CURRENT_VERSION : item.Version)) {

				bool dirty = true;

				while (dirty){
					dirty = false;
					switch (version) {
						default:
							break;
					}
				}

			}

			if (forceApplied || ShouldApply (item, parseObject)) {
				try{
					if (item == null) {
						item = new ConversationMessageEventItem(parseObject);
						LLogger.WriteEvent ("Created object", parseObject.ClassName, parseObject.ObjectId);
					} else {
						item.ApplyParseObject (parseObject);
						LLogger.WriteEvent ("Updated object", parseObject.ClassName, parseObject.ObjectId);
					}
				} catch (Exception e){
					LLogger.WriteEvent (string.Format("Failed {0} object", item == null ? "creating" : "updating"), parseObject.ClassName, parseObject.ObjectId, e.ToString());
					throw e;
				}
			}
			return item;
		}
	}
}

