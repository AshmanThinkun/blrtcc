﻿using System;
using Parse;

namespace BlrtData
{
	[Serializable]
	public class Channel: BlrtBase
	{
		#region implemented abstract members of BlrtBase
		public const string CLASS_NAME 			= "Channel";
		public override bool TryUpgrade ()
		{
			return false;
		}
		public override string ClassName {
			get {
				return "Channel";
			}
		}
		public override int CurrentVersion {
			get {
				return 0;
			}
		}
		public override int OldestSupportedVersion {
			get {
				return 0;
			}
		}
		#endregion


		public string OrganisationId{ get; set;}
		public string Name{ get; set;}
		public string CloudIconPath{ get; set;}

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);
			if (parseObject == null)
				return;
			this.OrganisationId = parseObject.Get<ParseObject> ("organisation", null)?.ObjectId;
			this.Name = parseObject.Get<string> ("name", "");

			this.CloudIconPath = parseObject.Get<ParseFile> ("icon", null)?.Url.ToString ();
		}
		
	}
}

