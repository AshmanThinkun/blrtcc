using System;
using System.Linq;
using System.Collections.Generic;
using Parse;
using System.IO;
using System.Runtime.CompilerServices;

namespace BlrtData
{
	[Serializable]
	public abstract class ConversationItem : BlrtBase, IComparable<ConversationItem>, IMediaCheckerListener
	{
		public const string CLASS_NAME 			= "ConversationItem";
		public override string ClassName { get { return CLASS_NAME; } }

		public const int CURRENT_VERSION = 6;
		public override int CurrentVersion { get { return CURRENT_VERSION; } }

		public const int OLDEST_SUPPORTED_VERSION = 6;
		public override int OldestSupportedVersion { get { return OLDEST_SUPPORTED_VERSION; } }

		//the parent Blrt Id
		public const string CONVERSATION_KEY		= "conversation";
		public const string TYPE_KEY	 			= "type";
		public const string INDEX_KEY	 			= "index";
		public const string READABLE_INDEX_KEY 		= "readableIndex";

		public const string PSUEDO_ID_KEY 			= "psuedoId";

		public const string ARGUMENT_KEY 			= "argument";

		public const string MEDIA_KEY 				= "media";
		public const string MEDIA_NAME_KEY 			= "name";

		public const string ISPUBLICBLRT_KEY 		= "isPublicBlrt";
		public const string PUBLICBLRTNAME_KEY 		= "publicName";

		public const string TOUCH_DATA_FILE_KEY		= "touchDataFile";
		public const string AUDIO_FILE_KEY 			= "audioFile";
		public const string THUMBNAIL_KEY 			= "thumbnailFile";
		public const string STATUS_KEY 				= "status";

		string _conversationId;
		int _index;
		int _readableIndex;
		string _argument;
		bool _isPublicBlrt;

		string[] _media;
		string [] _mediaNames;

		string [] _localThumbnailNames;

		Uri[] _cloudThumbnailPaths;

		int _status;

		bool _isLocalItem;
		int _positionOffset;

		public string [] LocalThumbnailPaths {
			get {
				if (_localThumbnailNames == null)
					return null;

				var pathCount = _localThumbnailNames.Count ();
				var paths = new string [pathCount];
				for (int i = 0; i < pathCount; i++) {
					paths [i] = LocalThumbnailPath (i);
				}

				return paths;
			}
		}

		public bool 						IsLocalItem	 {
			get {
				return _isLocalItem;
			}
			set { 
				if (value != _isLocalItem) {
					_isLocalItem = value;
					OnPropertyChanged ("IsLocalItem");
				}
			}
		}

		public int 						PositionOffset	 {
			get {
				return _positionOffset;
			}
			set { 
				if (value != _positionOffset) {
					_positionOffset = value;
					OnPropertyChanged ("PositionOffset");
				}
			}
		}

		public string 						ConversationId	 {
			get {
				return _conversationId;
			}
			set { 
				if (value != _conversationId) {
					_conversationId = value;
					OnPropertyChanged ("ConversationId");
				}
			}
		}
		public int 							Index	{
			get {
				return _index;
			}
			set { 
				if (value != _index) {
					_index = value;
					OnPropertyChanged ("Index");
				}
			}
		}
		public int 							ReadableIndex	{
			get {
				return _readableIndex;
			}
			set { 
				if (value != _readableIndex) {
					_readableIndex = value;
					OnPropertyChanged ("ReadableIndex");
				}
			}
		}
		public string 						Argument	{
			get {
				return _argument;
			}
			set { 
				if (value != _argument) {
					_argument = value;
					ReloadArgument ();
					OnPropertyChanged ("Argument");
				}
			}
		}

		public string [] MediaNames {
			get {
				return _mediaNames;
			}
			set {
				if (value != _mediaNames) {
					_mediaNames = value;
					OnPropertyChanged ("MediaNames");
				}
			}
		}

		public string[] Media {
			get {
				return _media;
			}
			set { 
				if (value != _media) {
					_media = value;
					OnPropertyChanged ("Media");
				}
			}
		}

		public string[] LocalThumbnailNames		{
			get {
				return _localThumbnailNames;
			}
			set { 
				if (value != _localThumbnailNames) {
					_localThumbnailNames = value;
					OnPropertyChanged ("LocalThumbnailNames");
				}
			}
		}

		//public string [] LocalThumbnailPaths {
		//	private get {
		//		return _localThumbnailPaths;
		//	}
		//	set {
		//		if (value != _localThumbnailPaths) {
		//			_localThumbnailPaths = value;
		//			OnPropertyChanged ("LocalThumbnailPaths");
		//		}
		//	}
		//}

		public Uri[] CloudThumbnailPaths	{
			get {
				return _cloudThumbnailPaths;
			}
			set { 
				if (value != _cloudThumbnailPaths) {
					_cloudThumbnailPaths = value;
					OnPropertyChanged ("CloudThumbnailPaths");
				}
			}
		}

		public bool IsPublicBlrt{
			get {
				return _isPublicBlrt;
			}
			private set { 
				if (value != _isPublicBlrt) {
					_isPublicBlrt = value;
					OnPropertyChanged ("IsPublicBlrt");
				}
			}
		}


		public abstract BlrtContentType 	Type				{	get;	}

		public string LocalThumbnailPath(int index)	{
			/*if (LocalThumbnailPaths != null && !string.IsNullOrWhiteSpace (LocalThumbnailPaths [index]))
				return LocalThumbnailPaths [index];*/

			if (LocalThumbnailNames == null || LocalThumbnailNames.Length == 0 || string.IsNullOrWhiteSpace (LocalThumbnailNames [index]))
				return string.Empty;

			return OnParse ? BlrtConstants.Directories.Default.GetThumbnailPath (LocalThumbnailNames [index]) : BlrtConstants.Directories.Default.GetCachedPath (LocalThumbnailNames [index]);
		}

		public virtual bool HasMedia				{	get { return Media != null && Media.Length > 0; }	}

		public bool IsEvent { get { return Type == BlrtContentType.AddEvent || Type == BlrtContentType.MessageEvent || Type == BlrtContentType.SendFailureEvent || Type == BlrtContentType.UnknownEvent; } }

		public bool IsRequest { get { return Type == BlrtContentType.Request; } }

		public int Status { get{return _status;}
			private set { 
				if (value != _status) {
					_status = value;
					OnPropertyChanged ("Status");
				}
			}
		}

		bool ItemWithThumbnails { 
			get {
				return (Type == BlrtContentType.Blrt 		||
						Type == BlrtContentType.BlrtReply 	||
						Type == BlrtContentType.Media);
			}
		}

		public ConversationItem (LocalStorageType localStorageType = LocalStorageType.Default) : base(localStorageType)
		{
			ConversationId = "";
			Argument = "";
			Index = ReadableIndex = PositionOffset = -1;

			Media = null;
		}

		public override void BeforeSerization ()
		{
			base.BeforeSerization ();
			StringifyArgument ();
		}

		public override void AfterDeserization ()
		{
			base.AfterDeserization ();
			ReloadArgument ();
		}

		public ConversationItem (ParseObject parseObject, LocalStorageType localStorageType = LocalStorageType.Default) : base(parseObject, localStorageType)
		{
		}

		internal void DownloadThumbnail (int v)
		{
			throw new NotImplementedException ();
		}

		public override void CopyToParse(ref ParseObject parseObject)
		{

			if (parseObject == null) {
				parseObject = new ParseObject (CLASS_NAME);
				if(ObjectId != null && ObjectId.Length > 0)
					parseObject.ObjectId = ObjectId;
			}

			base.CopyToParse (ref parseObject);

			StringifyArgument ();
			parseObject[ARGUMENT_KEY] 			= this.Argument;


		}

		public override void DeleteData ()
		{
			base.DeleteData ();

			if (LocalThumbnailNames != null) {
				for (int i = 0; i < LocalThumbnailNames.Count (); i++) {
					var path = LocalThumbnailPath (i);
					if (File.Exists (path)) {
						File.Delete (path);
					}
				}
			}
		}

		public void DeleteMedia() {
			foreach (var m in LocalStorage.DataManager.GetMediaAssets(this)) {
				m.DeleteData ();
			}
		}


		public override void ApplyParseObject(ParseObject parseObject)
		{
			if (parseObject == null)
				return;
			
			if (parseObject.ClassName != CLASS_NAME 
				|| (OnParse && parseObject.ObjectId != null && parseObject.ObjectId.Length > 0 && ObjectId.Length > 0 && parseObject.ObjectId != this.ObjectId))
				return;

			base.ApplyParseObject (parseObject);


			var conversation = parseObject.Get<ParseObject> (CONVERSATION_KEY, null);			
			ConversationId = conversation.ObjectId;
			Argument = parseObject.Get (ARGUMENT_KEY, Argument);
			Status = parseObject.Get (STATUS_KEY, 0);
			Index = parseObject.Get (INDEX_KEY, -1);
			ReadableIndex = parseObject.Get (READABLE_INDEX_KEY, -1);
			IsPublicBlrt = parseObject.Get (ISPUBLICBLRT_KEY, false);

			if (ItemWithThumbnails) {
				
				if (Type != BlrtContentType.Media) {
					ParseFile thumbnailFile = parseObject.Get<ParseFile> (THUMBNAIL_KEY, null);
					if (thumbnailFile != null) {
						LocalThumbnailNames = new string [] { thumbnailFile.Name };
						CloudThumbnailPaths = new Uri [] { thumbnailFile.Url };
					} else {
						var convo = BlrtManager.DataManagers.Default.GetConversation (ConversationId);
						LocalThumbnailNames = new string [] { convo.LocalThumbnailName };
						CloudThumbnailPaths = new Uri [] { convo.CloudThumbnailPath };
					}
				}

				var mediaItems = parseObject.Get<List<object>> (MEDIA_KEY, null);

				if (mediaItems == null) {
					throw new Exception ("media items for blrt or media is found null");
				}

				var mediaObjects = (from obj in mediaItems.Cast<ParseObject> ()
									select obj).ToArray ();
				Media = (from obj in mediaObjects
						 select obj.ObjectId).ToArray ();

				if (Type == BlrtContentType.Media) {
					SetThumbnailPathsForMediaItem (mediaObjects);
				}

				if (MediaNames == null) {
					MediaNames = (from obj in mediaObjects
								  select obj.Get (MEDIA_NAME_KEY, "")).ToArray ();
				}
			}

			if (CreatedByCurrentUser) {
				TryMarkRead ();
			}
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		void SetThumbnailPathsForMediaItem (ParseObject [] mediaObjects)
		{
			var objectCount = mediaObjects.Length;
			LocalThumbnailNames = new string [objectCount];
			CloudThumbnailPaths = new Uri [objectCount];

			for (int i = 0; i < objectCount; i++) {
				var thumbnail = mediaObjects [i].Get<ParseFile> (THUMBNAIL_KEY, null);
				if (thumbnail != null) {
					LocalThumbnailNames [i] = thumbnail.Name;
					CloudThumbnailPaths [i] = thumbnail.Url;
				}
			}
		}

		public bool TryMarkRead()
		{
			if (null == ParseUser.CurrentUser) {
				return false;
			}
			var rel = LocalStorage.DataManager.GetConversationUserRelation (ConversationId, ParseUser.CurrentUser.ObjectId);

			if (rel != null) {
				rel.MarkViewed (this);
				return true;
			}

			return false;
		}



		public override BlrtBaseIssues GetIssues () {
			var issues = base.GetIssues ();

			if (!Enum.IsDefined (typeof(BlrtContentType), Type)) {
				issues = issues | BlrtBaseIssues.UnknownContentType;
			}

			return issues;
		}


		public abstract void StringifyArgument();

		public abstract void ReloadArgument ();

		private bool IsMediaLocal ()
		{
			if (Media != null && Media.Count() > 0) {
			
				foreach (var str in Media) {
					var id = LocalStorage.DataManager.GetReplaceCacheId (str);
					var m = LocalStorage.DataManager.GetMediaAsset (id);

					if (m == null || !m.IsDataLocal())
						return false;
				}
			}

			return true;
		}

		public virtual bool IsDataLocal ()
		{
			return IsMediaLocal ();
		}

		public IO.BlrtDownloader[] DownloadMedia (int priority)
		{
			var convo = LocalStorage.DataManager.GetConversation (this.ConversationId);

				var list = new List<IO.BlrtDownloader> ();

			foreach (var m in LocalStorage.DataManager.GetMediaAssets(this)) {
				if (!m.IsDataLocal ()) {
					var d = IO.BlrtDownloader.Download (priority, m.CloudMediaPath, m.LocalMediaPath);

					IO.BlrtDownloaderAssociation.CreateAssociation (convo, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (m, d);
					IO.BlrtDownloaderAssociation.CreateAssociation (this, d);


					d.OnDownloadFailed += UpdateFileState;
					d.OnDownloadFinished += UpdateFileState;
				
					list.Add (d);
				}
			}

			return list.ToArray ();
		}


		public virtual IO.BlrtDownloader[] DownloadContent  (int priority)
		{
			return new IO.BlrtDownloader[0];
		}

		public IO.BlrtDownloader[] DownloadThumbnails  (int priority)
		{
			var convo = LocalStorage.DataManager.GetConversation (this.ConversationId);

			if (!this.IsDataLocal() && this.OnParse){

				if (CloudThumbnailPaths == null) {
					return new BlrtData.IO.BlrtDownloader [0];
				}
				var downloaders = new BlrtData.IO.BlrtDownloader [CloudThumbnailPaths.Count()];

				for (int i = 0; i < CloudThumbnailPaths.Count (); i++) {
					var path = LocalThumbnailPath (i);

					if (!string.IsNullOrEmpty (LocalThumbnailNames [i]) && !BlrtUtil.CanUseFile (path)) {
						var downloader = IO.BlrtDownloader.Download (priority, CloudThumbnailPaths [i], path);

						IO.BlrtDownloaderAssociation.CreateAssociation (convo, downloader);
						IO.BlrtDownloaderAssociation.CreateAssociation (this, downloader);

						downloader.OnDownloadFailed += UpdateFileState;
						downloader.OnDownloadFinished += (s, e) => {
							UpdateFileState(s,e);
							ConvoShouldReload (convo);
						};
						downloaders [i] = downloader;
					}
				}
				return downloaders;
			}
			return new BlrtData.IO.BlrtDownloader[0];
		}

		void ConvoShouldReload (Conversation convo)
		{
			//fake call of relation changed to make the conversation reload
			//to update the thumbnail when download finished.
			//this is a temporary solution to fix the DataTemplateSelector in XF not updating ifself
			//when properties changes, but it doesn't comply to MVVM rule, and should be changed later.
			Xamarin.Forms.MessagingCenter.Send<Object,bool>(this, string.Format ("{0}.RelationChanged", convo.LocalGuid), true);
		}

		public int CompareTo (ConversationItem other)
		{
			if (!other.OnParse && !OnParse)
				return CreatedAt.CompareTo(other.CreatedAt);
			else if (!other.OnParse)
				return -1;
			else if(!OnParse)
				return 1;

			int tmp = Index.CompareTo (other.Index);

			if (tmp != 0)
				return tmp;

			return CreatedAt.CompareTo(other.CreatedAt);
		}

		public override bool TryUpgrade ()
		{
			return false; // No upgrades supported
		}

		public virtual void StartFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			if (OnParse) {
				var path = LocalThumbnailPath (thumbnailIndex);
				if (!string.IsNullOrEmpty (path)) {
					MediaChecker.GetMediaChecker ()?.Subscribe (path, listener ?? this);
				}

				var media = LocalStorage.DataManager.GetMediaAssets (Media);
				foreach (var item in media) {
					item.StartFileMonitoring (listener ?? this);
				}
			}
		}

		public virtual void StopFileMonitoring (IMediaCheckerListener listener, int thumbnailIndex)
		{
			var path = LocalThumbnailPath (thumbnailIndex);
			if (!string.IsNullOrEmpty (path)) {
				MediaChecker.GetMediaChecker ()?.UnSubscribe (path, listener ?? this);
			}

			var media = LocalStorage.DataManager.GetMediaAssets (Media);
			foreach (var item in media) {
				item.StopFileMonitoring (listener ?? this);
			}
		}

		public virtual CloudState GetCloudState ()
		{
			CloudState state = CloudState.Synced;

			if (OnParse) {

				var media = LocalStorage.DataManager.GetMediaAssets (Media);

				var checker = MediaChecker.GetMediaChecker ();

				if (LocalThumbnailNames != null) {
					for (int i = 0; i < LocalThumbnailNames.Count (); i++) {
						var path = LocalThumbnailPath (i);
						if (!string.IsNullOrEmpty (path)) {
							switch (checker.GetState (path)) {
							case CheckerState.ContentMissing:
								state = CloudState.ContentMissing;
								break;
							case CheckerState.DownloadingContent:
								state = (state == CloudState.ContentMissing) ? CloudState.ContentMissing : CloudState.DownloadingContent;
								break;
							case CheckerState.Synced:
								state = (state == CloudState.Synced) ? CloudState.Synced : state;
								break;
							}
						}
					}
				}

				if (Media != null && media.Count (c => c.OnParse) != Media.Length)
					state = CloudState.ContentMissing;

				foreach (var item in media) {
					var cs = item.GetCloudState ();

					switch (cs) {
						case CloudState.ContentMissing:
							if (state == CloudState.Synced)
								state = cs;
							break;
						case CloudState.DownloadingContent:
							if (state != CloudState.UploadingContent)
								state = cs;
							break;
						case CloudState.UploadingContent:
							state = cs;
							break;
						case CloudState.ChildrenNotOnCloud:
						case CloudState.NotOnCloud:
							if (state == CloudState.Synced || state == CloudState.ContentMissing)
								state = CloudState.ChildrenNotOnCloud;
							break;
					}
				}

			} else {

				if (ShouldUpload)
					state = CloudState.UploadingContent;
				else
					state = CloudState.NotOnCloud;
			}
			return state;
		}

		#region IMediaCheckerListener implementation

		void IMediaCheckerListener.PathStateChanged (string path)
		{
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				OnPropertyChanged ("FileState");
			});
		}

		#endregion
	}
}

