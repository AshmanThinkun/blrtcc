﻿using System;
using Parse;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlrtData
{
	[Serializable]
	public class UserOrganisationRelation: BlrtBase
	{
		public const string CLASS_NAME				= "UserOrganisationRelation";
		public const int CURRENT_VERSION 			= 0;
		public const int OLDEST_SUPPORTED_VERSION 	= 0;

		const LocalStorageType StorageType = LocalStorageType.User;
		static ILocalStorageParameter StorageParam {
			get {
				return LocalStorageParameters.Get (StorageType);
			}
		}
		public UserOrganisationRelation () : base(StorageType) {}
		#region implemented abstract members of BlrtBase

		public override bool TryUpgrade ()
		{
			return false;
		}

		public override string ClassName {
			get {
				return CLASS_NAME;
			}
		}

		public override int CurrentVersion {
			get {
				return CURRENT_VERSION;
			}
		}

		public override int OldestSupportedVersion {
			get {
				return OLDEST_SUPPORTED_VERSION;
			}
		}

		#endregion

		string _userId;

		public string UserId {
			get {
				return _userId;
			}
			set { 
				if (value != _userId) {
					_userId = value;
					OnPropertyChanged ("UserId");
				}
			}
		}

		string[]	_subscribedOCListIds;

		public string[]						SubscribedOCListIds		{
			get {
				return _subscribedOCListIds;
			}
			set { 
				if (value != _subscribedOCListIds) {
					_subscribedOCListIds = value;
					OnPropertyChanged ("SubscribedOCListIds");
				}
			}
		}

		#region instance
		public static UserOrganisationRelation Current {
			get;
			private set;
		}

		public static event EventHandler CurrentUpdated;

		public static async Task UpdateCurrent ()
		{
			if (ParseUser.CurrentUser == null) {
				Current = null;
			} else{
				// read from local cache
				ReadFromCache ();

				// update from cloud
				await UpdateFromCloud ();
			}

			CurrentUpdated?.Invoke (Current, EventArgs.Empty);
		}

		static void ReadFromCache ()
		{
			if (null != ParseUser.CurrentUser) {
				try {
					Current = StorageParam.DataManager.GetUserOrganisationRelationFromUser (ParseUser.CurrentUser.ObjectId);
					return;
				} catch {}
			}
			Current = null;
		}

		static async Task UpdateFromCloud ()
		{
			if (ParseUser.CurrentUser != null) {
				const int MaxRetry = 3;
				for (int i = 0; i < MaxRetry; ++i) {
					try {
						var oldUser = ParseUser.CurrentUser;

						var query = new ParseQuery<ParseObject> (CLASS_NAME)
							.WhereEqualTo("user", oldUser)
							.Include("oclists");

						var result = await query.FirstOrDefaultAsync ();
						var obj = StorageParam.DataManager.GetUserOrganisationRelationFromUser (oldUser.ObjectId);
						if (null == obj) {
							obj = StorageParam.DataManager.AddParse (result) as UserOrganisationRelation;
						} else {
							obj.ApplyParseObject (result);
						}

						if (null != obj && ParseUser.CurrentUser.ObjectId == oldUser.ObjectId) {
							await StorageParam.DataManager.SaveLocal ();
							Current = obj;
							return;
						}
						break;
					} catch (Exception ex) {
						BlrtUtil.Debug.ReportException (ex);
						continue;
					}
				}
			} else {
				Current = null;
			}
		}

		#endregion

		public override void ApplyParseObject (ParseObject parseObject)
		{
			base.ApplyParseObject (parseObject);

			if (parseObject == null) {
				this.SubscribedOCListIds = new string[0];
				return;
			}

			this.UserId = ParseUser.CurrentUser.ObjectId;

			var list = parseObject.Get<List<object>> ("oclists", null);
			if (null != list) {
				List<string> subscribedLists = new List<string> ();
				foreach (var oclistObj in list) {
					var oclist = oclistObj as ParseObject;
					if (null != oclist) {
						subscribedLists.Add (oclist.ObjectId);
					}
				}
				this.SubscribedOCListIds = subscribedLists.ToArray ();
			} else {
				this.SubscribedOCListIds = new string[0];
			}


			if ((list != null) && (list.Count > 0)) {
				BlrtManager.DataManagers.Default.AddParse (list);
			}
		}
	}
}

