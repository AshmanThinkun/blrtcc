using System;
using System.Collections.Generic;

namespace BlrtData.Events
{
	public class EventManager
	{
		Dictionary<Type, List<object>> _handlers;

		public EventManager ()
		{
			_handlers = new Dictionary<Type, List<object>> ();
		}

		public void Register<T>(BlrtEventHandler<T> handler) where T : class 
		{
			if (handler == null)
				throw new NullReferenceException ();

			var t = typeof(T);

			if (!_handlers.ContainsKey (t))
				_handlers.Add (t, new List<object> ());

			var list = _handlers [t];
			if (!list.Contains (handler))
				list.Add (handler);
		}

		public void Unregister<T>(BlrtEventHandler<T> handler)  where T : class 
		{
			if (handler == null)
				throw new NullReferenceException ();

			var t = typeof(T);

			if (_handlers.ContainsKey (t)) {
				var list = _handlers [t];

				if (list.Contains (handler))
					list.Remove (handler);
			}
		}

		public void Fire<T>(T obj)  where T : class
		{
			if (obj == null)
				throw new NullReferenceException ();

			var t = typeof(T);

			if (_handlers.ContainsKey (t)) {
				foreach (var h in _handlers[t]) {
					var handler = h as BlrtEventHandler<T>;

					try {
						handler.EventFired(obj);
					} catch {
					}
				}
			}
		}
	}

	public interface BlrtEventHandler<T> where T : class {
	
		void EventFired(T obj);
	}
}

