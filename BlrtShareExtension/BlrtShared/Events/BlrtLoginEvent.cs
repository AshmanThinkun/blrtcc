using System;

namespace BlrtData.Events
{
	public class BlrtLoginEvent
	{
		public enum LoginType {
			LoginWithEmail,
			LoginWithFacebook,
			RegisterWithEmail,
			RegisterWithFacebook,
		}

		public LoginType Login { get; private set; }

		public BlrtLoginEvent(LoginType login){
			this.Login = login;
		}
	}


	public class LoginListener: BlrtEventHandler<BlrtLoginEvent>
	{
		public event EventHandler OnUserLogin;
		#region BlrtEventHandler implementation

		void BlrtEventHandler<BlrtLoginEvent>.EventFired (BlrtLoginEvent obj)
		{
			if (OnUserLogin != null)
				OnUserLogin (this, EventArgs.Empty);
		}

		public LoginListener()
		{
			BlrtManager.Event.Register<BlrtLoginEvent> (this as BlrtEventHandler<BlrtLoginEvent>);
		}

		#endregion
	}
}

