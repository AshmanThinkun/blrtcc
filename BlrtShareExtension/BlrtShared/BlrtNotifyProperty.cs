using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlrtData
{
	public class BlrtNotifyProperty : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		public BlrtNotifyProperty ()
		{
		}

		protected void OnPropertyChanged<T> (ref T current, T value, [CallerMemberName] string propertyName = "")
		{

			if (current != null ? !current.Equals (value) : value != null) {

				current = value;

				var handler = PropertyChanged;
				if (handler != null) {
					handler (this, new PropertyChangedEventArgs (propertyName));
				}
			}
		}

	}
}

