using System;
using Xamarin.Forms;

#if __IOS__
using UIKit;
#endif


public static class BlrtStyles
{
	public static readonly Color	BlrtGreen					= Color.FromRgb( 0,		231,	168 );
	public static readonly Color	BlrtGreenHighlight			= Color.FromRgb( 0,		245,	165 );
	public static readonly Color	BlrtGreenShadow				= Color.FromRgb( 0,		185,	156 );
	public static readonly Color 	BlrtGreenLightLight = Color.FromRgb (227, 239, 237);


	public static readonly Color	BlrtCharcoal				= Color.FromRgb( 44,	53,		62 );
	public static readonly Color	BlrtCharcoalHighlight		= Color.FromRgb( 88,	105,	124 );
	public static readonly Color	BlrtCharcoalShadow			= Color.FromRgb( 23,	28,		34 );


	public static readonly Color	BlrtBlackBlack				= Color.FromRgb( 11,	13,		15 );
	public static readonly Color	BlrtWhite					= Color.FromRgb( 255,	255,	255 );


	public static readonly Color	BlrtBlueGray				= Color.FromRgb( 123,	151,	173 );
	public static readonly Color	BlrtBlueGrayHighlight		= Color.FromRgb( 148,	171,	185	);
	public static readonly Color	BlrtBlueGrayShadow			= Color.FromRgb( 94,	112,	134 );


	public static readonly Color	BlrtAccentRed				= Color.FromRgb( 226,	26,		69 );
	public static readonly Color	BlrtAccentRedHighlight		= Color.FromRgb( 255,	34,		69 );
	public static readonly Color	BlrtAccentRedShadow			= Color.FromRgb( 183,	0,		49 );


	public static readonly Color	BlrtAccentBlue				= Color.FromRgb( 79,	153,	200 );
	public static readonly Color	BlrtAccentBlueHighlight		= Color.FromRgb( 0,		160,	212 );
	public static readonly Color	BlrtAccentBlueShadow		= Color.FromRgb( 59,	113,	148 );
	public static readonly Color 	FacebookBlue 				= Color.FromRgb( 45, 	95, 	154 );
	public static readonly Color 	TwitterBlue 				= Color.FromRgb( 0, 	195, 	243 );

	public static readonly Color	BlrtWhiteAlpha				= BlrtWhite.MultiplyAlpha(0.8f);

	public static readonly Color	BlrtGray1				 	= Color.FromRgb( 253,	253,	253 );
	public static readonly Color	BlrtGray2				 	= Color.FromRgb( 248,	246,	249 );
	public static readonly Color	BlrtGray3				 	= Color.FromRgb( 227,	231,	237 );
	public static readonly Color	BlrtGray4				 	= Color.FromRgb( 178,	185,	193 );
	public static readonly Color	BlrtGray5			 		= Color.FromRgb( 139,	146,	154 );
	public static readonly Color	BlrtGray6					= Color.FromRgb( 95,	107,	125 );
	public static readonly Color	BlrtGray7					= Color.FromRgb( 66,	79,		93 );
	public static readonly Color	BlrtGray8 					= Color.FromHex("#e3e7ed");
	public static readonly Color	NavigationBarGray			= Color.FromRgb( 219,224,231 );

	public static readonly Color	CanvasPlayBarColor			= Color.FromRgb( 227,	230,	234 );
	public static readonly Color	ToolbarIconHighlightColor	= Color.FromRgb( 238,	238,	238 );
	public static readonly Color	ToolbarWidthDrawerColor		= Color.FromRgb( 62,	74,		87 );


	public static readonly Color BlrtBlackAlpha					= BlrtBlackBlack.MultiplyAlpha(0.8f);

	public static readonly Font FontSize14						= Font.SystemFontOfSize (28 * 0.5f);
	public static readonly Font FontSize13 						= Font.SystemFontOfSize (26 * 0.5f);

	public static readonly Font HugeFont						= Font.SystemFontOfSize (46 * 0.5f);

	public static readonly Font CellTitleFont					= Font.SystemFontOfSize (32 * 0.5f);
	public static readonly Font CellContentFont					= Font.SystemFontOfSize (30 * 0.5f);
	public static readonly Font CellDetailFont					= Font.SystemFontOfSize (24 * 0.5f);

	public static readonly Font CellTitleBoldFont				= Font.SystemFontOfSize (CellTitleFont.FontSize,	FontAttributes.Bold);
	public static readonly Font CellContentBoldFont				= Font.SystemFontOfSize (CellContentFont.FontSize,	FontAttributes.Bold);
	public static readonly Font CellDetailBoldFont				= Font.SystemFontOfSize (CellDetailFont.FontSize,	FontAttributes.Bold);

	#if __IOS__

	public static class iOS{

		static readonly int _screenScale = (int)BlrtCanvas.CanvasPageView.PlatformHelper.ScreenScale;
		public static readonly float ScreenScaleFactor = _screenScale < 3 ? (UIScreen.MainScreen.Bounds.Width < 360 ? _screenScale : _screenScale + 0.3f) : _screenScale;

		public static UIColor ToUIColor(Color color){
			return UIColor.FromRGBA((nfloat)color.R, (nfloat)color.G, (nfloat)color.B, (nfloat)color.A);
		}

		public static readonly UIColor	Green 					= ToUIColor(BlrtStyles.BlrtGreen);		
		public static readonly UIColor	GreenHighlight		    = ToUIColor(BlrtStyles.BlrtGreenHighlight);
		public static readonly UIColor	GreenShadow			    = ToUIColor(BlrtStyles.BlrtGreenShadow);

		public static readonly UIColor	Charcoal				= ToUIColor(BlrtStyles.BlrtCharcoal);
		public static readonly UIColor	CharcoalHighlight		= ToUIColor(BlrtStyles.BlrtCharcoalHighlight);
		public static readonly UIColor	CharcoalShadow		    = ToUIColor(BlrtStyles.BlrtCharcoalShadow);	

		public static readonly UIColor	Black				    = ToUIColor(BlrtStyles.BlrtBlackBlack);
		public static readonly UIColor	White				    = ToUIColor(BlrtStyles.BlrtWhite);

		public static readonly UIColor	BlueGray			    = ToUIColor(BlrtStyles.BlrtBlueGray);
		public static readonly UIColor	BlueGrayHighlight	    = ToUIColor(BlrtStyles.BlrtBlueGrayHighlight);
		public static readonly UIColor	BlueGrayShadow		    = ToUIColor(BlrtStyles.BlrtBlueGrayShadow);

		public static readonly UIColor	AccentRed			    = ToUIColor(BlrtStyles.BlrtAccentRed);
		public static readonly UIColor	AccentRedHighlight	    = ToUIColor(BlrtStyles.BlrtAccentRedHighlight);
		public static readonly UIColor	AccentRedShadow		    = ToUIColor(BlrtStyles.BlrtAccentRedShadow);
		public static readonly UIColor	AccentBlue			    = ToUIColor(BlrtStyles.BlrtAccentBlue);
		public static readonly UIColor	AccentBlueHighlight	    = ToUIColor(BlrtStyles.BlrtAccentBlueHighlight);	
		public static readonly UIColor	AccentBlueShadow	    = ToUIColor(BlrtStyles.BlrtAccentBlueShadow);

		public static readonly UIColor	FacebookBlue	    	= ToUIColor(BlrtStyles.FacebookBlue);
		public static readonly UIColor	TwitterBlue	    		= ToUIColor(BlrtStyles.TwitterBlue);

		public static readonly UIColor	Gray1			 	    = ToUIColor(BlrtStyles.BlrtGray1);
		public static readonly UIColor	Gray2			 	    = ToUIColor(BlrtStyles.BlrtGray2);
		public static readonly UIColor	Gray3			 	    = ToUIColor(BlrtStyles.BlrtGray3);
		public static readonly UIColor	Gray4			 	    = ToUIColor(BlrtStyles.BlrtGray4);
		public static readonly UIColor	Gray5			 	    = ToUIColor(BlrtStyles.BlrtGray5);
		public static readonly UIColor	Gray6			 	    = ToUIColor(BlrtStyles.BlrtGray6);
		public static readonly UIColor	Gray7			 	    = ToUIColor(BlrtStyles.BlrtGray7);
		public static readonly UIColor 	Gray8 					= ToUIColor (BlrtStyles.BlrtGray8);
		public static readonly UIColor  NavigationBarGray       = ToUIColor(BlrtStyles.NavigationBarGray);

		public static readonly UIColor	CharcoalAlpha			    = ToUIColor(BlrtStyles.BlrtBlackAlpha);

		public static readonly UIColor	WhiteAlpha			    = ToUIColor(BlrtStyles.BlrtWhiteAlpha);




		public static readonly UIFont FontSize14				= UIFont.SystemFontOfSize (28 * 0.5f);
		public static readonly UIFont FontSize16 				= UIFont.SystemFontOfSize (32 * 0.5f);

		public static readonly UIFont HugeFont					= UIFont.SystemFontOfSize (16 * ScreenScaleFactor * 0.5f);

		public static readonly UIFont CellTitleFont				= UIFont.SystemFontOfSize (34 * 0.5f);
		public static readonly UIFont CellContentFont			= UIFont.SystemFontOfSize (31 * 0.5f);
		public static readonly UIFont CellDetailFont			= UIFont.SystemFontOfSize (26 * 0.5f);

		public static readonly UIFont CellTitleBoldFont			= UIFont.BoldSystemFontOfSize (CellTitleFont.PointSize);
		public static readonly UIFont CellContentBoldFont		= UIFont.BoldSystemFontOfSize (CellContentFont.PointSize);
		public static readonly UIFont CellDetailBoldFont		= UIFont.BoldSystemFontOfSize (CellDetailFont.PointSize);



		public static readonly UIFont AppMenuFont = CellTitleFont;
	}
	#endif

	#if __ANDROID__
	public static readonly string PlaceholderImagePath = "blrt_placeholder.png";

	public static  global::Android.Graphics.Color ToAndroidColor(this Color color){
		return global::Android.Graphics.Color.Argb ((int)(color.A * 255), (int)(color.R * 255), (int)(color.G * 255), (int)(color.B * 255));
	}

	public static class ConversationScreenColors {

		public static readonly Color Background							= BlrtWhite;
		public static readonly Color TextColor							= BlrtGray7;



		public static readonly Color CurrentUserBlrtBackground			= BlrtAccentBlue;
		public static readonly Color OtherUserBlrtBackground			= BlrtCharcoal;

		public static readonly Color CurrentUserAudioBackground			= BlrtAccentBlue;
		public static readonly Color OtherUserAudioBackground 			= BlrtCharcoal;

		public static readonly Color CurrentUserBlrtText				= BlrtGray1;
		public static readonly Color OtherUserBlrtText					= BlrtGray1;

		public static readonly Color CurrentUserCommentBackground		= BlrtAccentBlue;
		public static readonly Color OtherUserCommentBackground			= BlrtGray8;

		public static readonly Color CurrentUserCommentText				= BlrtGray1;
		public static readonly Color OtherUserCommentText				= BlrtBlackBlack;


		public static readonly Color EventTextColor						= BlrtCharcoal;


	}
	#endif
	public static class MailboxScreenColors {

		public static readonly Color Background							= BlrtWhite;
		public static readonly Color TextColor							= BlrtCharcoal;



		public static readonly Color NewContentColor					= BlrtAccentRed;


	}



	public static Button BlrtButtonColor(){
		return new Button () {
			BackgroundColor		= BlrtCharcoal,
			TextColor			= BlrtGreen,
		};
	}


}
