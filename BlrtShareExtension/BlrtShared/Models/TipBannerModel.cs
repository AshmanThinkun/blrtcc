using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlrtData
{
	public class TipBannerModel
	{
		private static List<TipBanner> _bannerList = new List<TipBanner> ();
		public static List<TipBanner> BannerList{ 
			get{ return _bannerList;
			} 
			set{
				_bannerList = value;
				if (null != ListChanged)
					ListChanged (null, EventArgs.Empty);
			}
		}
		public static EventHandler ListChanged;

		public TipBannerModel ()
		{

		}


	}

	public class TipBanner{
		[JsonProperty("colorScheme")]
		public int ColorScheme{ get; set;}
		[JsonProperty("title")]
		public string Title{ get; set;}
		[JsonProperty("timeToStay")]
		public float TimeToStay{ get; set;}
		[JsonProperty("action")]
		public string Action{ get; set;}
		[JsonProperty("actionText")]
		public string ActionText{ get; set;}
		[JsonProperty("filter")]
		public string[] Filter{ get; set;}
	}
}

