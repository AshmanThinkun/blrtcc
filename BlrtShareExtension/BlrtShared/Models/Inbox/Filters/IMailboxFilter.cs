using System;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using BlrtData.ExecutionPipeline;

using BlrtiOS.Views.Util;
using System.ComponentModel;
using System.Threading.Tasks;
using BlrtData;
using System.Collections.Generic;

namespace BlrtiOS.Views.Mailbox
{
	public interface IMailboxFilter {
		bool CurrentUser {
			get;
		}

		int PagesLoaded { get; }
		int ItemPerPage { get; }

		bool ShouldRefresh { get; }

		string[] TagFilters		{ get; }
		string[] UserFilters	{ get; }
		string Keyword			{ get; }

		void ApplyTagFilters (string[] tags);
		void ApplyUserFilters (string[] userIds);
		void ApplyKeywordFilter (string keyword);

		MailboxCellModel GetCell (string conversationId);

		void ClearFilters ();

		Task ReloadList();
		Task RefreshList(int skip);
		Task LoadNextPage();

		string[] GetAllConversations();

		BindingList<ITableModelSection<MailboxCellModel>> GetList();

		void SetFireEvents (bool fire);
	}

	public abstract class MailboxFilter : IMailboxFilter, IComparer<MailboxCellModel>
	{
		protected MailboxSectionModel FlaggedSection { get; private set; }
		protected MailboxSectionModel DefaultSection { get; private set; }


		string[] _userId;
		string[] _tags;
		string _keyword;

		string _user;
		int _page;


		Task _task;
		System.Threading.SemaphoreSlim _semaphore;

		MailboxListController _controller;

		protected BindingList<ITableModelSection<MailboxCellModel>> List { 
			get; private set; 
		}

		public abstract bool ShouldRefresh { get; }


		public string[] UserFilters {
			get { return _userId; }
		}
		public string[] TagFilters {
			get { return _userId; }
		}
		public string UserId {
			get { return _user; }
		}

		public string Keyword{get{return _keyword;}}




		public int PagesLoaded {
			get { return _page; }
		}
		public int ItemPerPage {
			get { return BlrtData.Query.BlrtInboxQuery.ItemsPerQuery; }
		}

		public bool CurrentUser {
			get{
				return ParseUser.CurrentUser != null
					&& ParseUser.CurrentUser.ObjectId == _user;
			}
		}

		public MailboxFilter(MailboxListController controller) {

			_page = 1;

			_controller = controller;

			_semaphore = new System.Threading.SemaphoreSlim (1);

			List = new BindingList<ITableModelSection<MailboxCellModel>> ();

			if (ParseUser.CurrentUser != null)
				_user = ParseUser.CurrentUser.ObjectId;

			_tags = new string[0];
			_userId = new string[0];


			List.Add ( FlaggedSection = new MailboxSectionModel () );
			//FlaggedSection = new MailboxSectionModel ();
			List.Add ( DefaultSection = new MailboxSectionModel () );
		}


		public void ApplyTagFilters (string[] tags)
		{
			_tags = tags;
		}

		public void ApplyUserFilters (string[] userIds)
		{
			_userId = userIds;
		}

		public void ApplyKeywordFilter(string keyword)
		{
			_keyword = keyword;
		}

		public virtual void ClearFilters ()
		{
			_tags = new string[0];
			_userId = new string[0];
		}

		public string[] GetAllConversations(){

			if (ParseUser.CurrentUser == null) {
				return new string[0];
			}


			var relations = BlrtManager.Data.GetConversationUserRelations (
				_user
			);

			return GetRelations()
				.Select (rel => rel.ConversationId)
				.ToArray();
		}

		public async Task ReloadList ()
		{
			lock (_semaphore) {
				if (_task != null)
					return;

				_task = _semaphore.WaitAsync ();
			}

			await _task;
			_task = null;

			try {

				if (ParseUser.CurrentUser == null) {
					List.Clear ();
					return;
				}

				if (!List.Contains (FlaggedSection))
					List.Add (FlaggedSection);
				if (!List.Contains (DefaultSection))
					List.Add (DefaultSection);


				var relations = GetRelations ();

				foreach(var section in List){
					foreach (var ele in section.Cells.Where(m => !relations.Any(rel => m.Same(rel)))) {
						Exclude (ele);			
					}
				}

				foreach (var rel in relations) {			
					if (Filter (rel, true)) {
						var ele = GetElement (rel, true);
						if (ele != null)
							Include (ele);								
					} else {
						var ele = GetElement (rel, false);
						if (ele != null)
							Exclude (ele);			
					}
				}
			} finally {
				_semaphore.Release ();
			}
		}

		protected abstract IEnumerable<BlrtData.ConversationUserRelation> GetRelations ();

		public MailboxCellModel GetCell (string conversationId)
		{
			foreach (var section in List) {
				var ele = section.Cells.FirstOrDefault (model => model.ConversationId == conversationId);

				if (ele != null)
					return ele;
			}

			return null;
		}

		protected virtual void Include (MailboxCellModel ele)
		{
			if (ele.Flagged) {
				if (DefaultSection.Cells.Contains (ele))
					DefaultSection.Cells.Remove (ele);

				if (!FlaggedSection.Cells.Contains (ele)) {
					SortedAdd (FlaggedSection, ele);
				}
			} else {
				if (FlaggedSection.Cells.Contains (ele))
					FlaggedSection.Cells.Remove (ele);

				if (!DefaultSection.Cells.Contains (ele)) {
					SortedAdd (DefaultSection, ele);
				}
			}
		}



		void SortedAdd (MailboxSectionModel section, MailboxCellModel ele) {
			for (int i = 0; i < section.Cells.Count; ++i) {
				if (Compare(section.Cells[i], ele) < 0) {
					section.Cells.Insert (i, ele);
					return;
				}	
			}

			section.Cells.Add (ele);
		}

		public int Compare (MailboxCellModel x, MailboxCellModel y) {

			var i = -x.OnParse.CompareTo (y.OnParse);

			if (i != 0) return i;



			return x.LastUpdated.CompareTo (y.LastUpdated);
		}

		protected virtual void Exclude (MailboxCellModel ele)
		{
			if (DefaultSection.Cells.Contains (ele)) {
				DefaultSection.Cells.Remove (ele);
			}

			if (FlaggedSection.Cells.Contains (ele)) {
				FlaggedSection.Cells.Remove (ele);
			}
		}

		public abstract Task RefreshList (int skip);

		protected MailboxCellModel GetElement (ConversationUserRelation rel, bool create)
		{
			MailboxCellModel element = null;

			foreach (var section in List) {
				element = section.Cells.FirstOrDefault (model => model.Same(rel));

				if (element != null)
					break;
			}

			if(element == null) {
				if (!create)
					return null;
				element = new MailboxCellModel();
				element.MoreAction = new MailboxMoreAction(element, _controller);
			}

			element.SetRelation (rel);
			return element;
		}


		protected MailboxCellModel GetElement (BlrtData.Conversation conversation, bool create)
		{
			MailboxCellModel element = null;

			foreach (var section in List) {
				element = section.Cells.FirstOrDefault (model => model.Same(conversation));

				if (element != null)
					break;
			}

			if(element == null) {
				if (!create)
					return null;
				element = new MailboxCellModel();
				element.MoreAction = new MailboxMoreAction(element, _controller);
			}

			element.SetConversation (conversation);
			return element;
		}

		protected virtual bool Filter(ConversationUserRelation relation, bool applyBasicFilters) {
		
			if (!relation.IsCurrentUsers || relation.Conversation == null)
				return false;

			if (applyBasicFilters) {

				if (_tags != null && _tags.Length > 0) {

					bool found = false;

					for (int i = 0; i < _tags.Length; ++i) {
						if (relation.Tags.Contains (_tags [i]))
							found = true;
					}	

					if (!found)
						return false;
				}

				if (_userId != null && _userId.Length > 0) {

					var userIds = relation.Conversation.GetUserInConversation ();

					if (!userIds.Any (user => _userId.Contains (user)))
						return false;
				}

				if (!string.IsNullOrEmpty(Keyword) && !relation.Conversation.Name.ToLower ().Contains (_keyword.ToLower ()))
					return false;
			}

			return true;
		}



		public BindingList<ITableModelSection<MailboxCellModel>> GetList ()
		{
			return List;
		}



		public async Task LoadNextPage ()
		{
			int page = _page;
			await RefreshList (_page * ItemPerPage);

			_page = Math.Max(page + 1, _page);
		}


		public void SetFireEvents(bool fire){
		
			List.RaiseListChangedEvents = fire;
		
			foreach (var sec in List)
				sec.Cells.RaiseListChangedEvents = fire;
		}
	}

}

