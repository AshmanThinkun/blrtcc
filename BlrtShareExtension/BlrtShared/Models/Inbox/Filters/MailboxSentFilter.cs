﻿using System;
using System.Linq;
using System.Collections.Generic;
using BlrtData;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxSentFilter : MailboxFilter, IComparer<MailboxCellModel>
	{
		public static DateTime LastUpdated = new DateTime();

		public override bool ShouldRefresh {
			get { return BlrtData.Query.BlrtInboxQuery.HasQuery(BlrtData.Query.BlrtInboxQueryType.Sent, 0) || LastUpdated.AddMinutes (15) < DateTime.Now; }
		}

		public MailboxSentFilter (MailboxListController controller):base(controller)
		{
		}


		protected override IEnumerable<ConversationUserRelation> GetRelations(){
		
			return BlrtData.BlrtManager.Data.GetConversationUserRelations(UserId)
				.Where(rel => rel.Conversation != null && !rel.Archive && rel.Conversation.CreatedByCurrentUser)
				.Concat(
					from c in BlrtData.BlrtManager.Data.GetUnsyncedConversations ()
					select ConversationUserRelation.CreatePlaceholder(c)
				);
			
		}


		public override async System.Threading.Tasks.Task RefreshList (int skip)
		{
			var query = BlrtData.Query.BlrtInboxQuery.RunQuery (BlrtData.Query.BlrtInboxQueryType.Sent, skip);
			await query.WaitAsync();

			LastUpdated = DateTime.Now;
		}
	}
}

