﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BlrtiOS.Views.Mailbox
{
	public class MailboxArchiveFilter : MailboxFilter, IComparer<MailboxCellModel>
	{
		public static DateTime LastUpdated = new DateTime();

		public override bool ShouldRefresh {
			get { return BlrtData.Query.BlrtInboxQuery.HasQuery(BlrtData.Query.BlrtInboxQueryType.Archive, 0) || LastUpdated.AddMinutes (15) < DateTime.Now; }
		}

		public MailboxArchiveFilter (MailboxListController controller):base(controller)
		{
		}


		protected override IEnumerable<BlrtData.ConversationUserRelation> GetRelations(){

			return BlrtData.BlrtManager.Data.GetConversationUserRelations(UserId)
				.Where(rel => rel.Conversation != null && rel.Archive);

		}

		public override async System.Threading.Tasks.Task RefreshList (int skip)
		{
			var query = BlrtData.Query.BlrtInboxQuery.RunQuery (BlrtData.Query.BlrtInboxQueryType.Archive, skip);
			await query.WaitAsync();

			LastUpdated = DateTime.Now;
		}
	}
}

