using System;
using System.ComponentModel;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Linq;

namespace BlrtData.Models
{
	#if __ANDROID__
	public abstract class BasicModel : Java.Lang.Object, INotifyPropertyChanged
	#else
	public abstract class BasicModel : INotifyPropertyChanged
	#endif
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		System.Collections.Generic.List<INotifyPropertyChanged> _list;

		List<string> _events;
		bool _silence;

		public BasicModel() {

			_events = new List<string> ();
			_silence = false;
		}


		protected void FireSilencedPropertyChanges ()
		{
			if (PropertyChanged != null && _events.Count > 0) {
				while (0 < _events.Count) {
					var propertyName = _events [0];
					_events.RemoveAt (0);

					PropertyChanged.Invoke (this, new System.ComponentModel.PropertyChangedEventArgs (propertyName));
				}
			}


			lock (_events) {
				_silence = false;
			}
		}

		protected void SilencePropertyChanges ()
		{
			lock (_events) {
				_silence = true;
			}
		}

		protected void SetProperty<T> (ref T current, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
		{
			if (current != null ? !current.Equals (value) : value != null) {
				current = value;

				if (PropertyChanged != null) {
					if (!_silence) {
						PropertyChanged.Invoke (this, new PropertyChangedEventArgs (propertyName));
					} else {
						lock (_events) {
							if (!_events.Contains (propertyName))
								_events.Add (propertyName);
						}
					}
				}
			}
		}

		protected void SetProperty<T> (ref T [] current, T [] value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
		{
			if (current != null ? !current.SequenceEqual(value) : value != null) {
				current = value;

				if (PropertyChanged != null) {
					if (!_silence) {
						PropertyChanged.Invoke (this, new PropertyChangedEventArgs (propertyName));
					} else {
						lock (_events) {
							if (!_events.Contains (propertyName))
								_events.Add (propertyName);
						}
					}
				}
			}
		}

		protected void SetProperty (ref FormattedString current, FormattedString value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
		{
			bool dif = (value != null) != (current != null);

			if (!dif && current != null) {
				if (current.Spans.Count != value.Spans.Count)
					dif = true;

				for (int i = 0; !dif && i < current.Spans.Count; ++i) {
					dif = current.Spans [i].BackgroundColor		!= value.Spans [i].BackgroundColor
						|| current.Spans [i].Text 				!= value.Spans [i].Text
						|| current.Spans [i].ForegroundColor	!= value.Spans [i].ForegroundColor
						|| current.Spans [i].FontFamily			!= value.Spans [i].FontFamily
						|| current.Spans [i].FontSize			!= value.Spans [i].FontSize
						|| current.Spans [i].FontAttributes		!= value.Spans [i].FontAttributes;
				}
			}


			if (current != value && dif) {
				current = value;
				if (PropertyChanged != null) {
					if (!_silence) {
						PropertyChanged.Invoke (this, new System.ComponentModel.PropertyChangedEventArgs (propertyName));
					} else {
						lock (_events) {
							if (!_events.Contains (propertyName))
								_events.Add (propertyName);
						}
					}
				}
			}
		}

		protected void AddNotifier(INotifyPropertyChanged listener) {

			if (_list == null)
				_list = new System.Collections.Generic.List<INotifyPropertyChanged> ();

			if (listener != null && !_list.Contains (listener)) {
				_list.Add (listener);
				listener.PropertyChanged += OnPropertyChanged;

				OnPropertyChanged (listener, new PropertyChangedEventArgs(""));
			}
		}
		protected void RemoveNotifier(INotifyPropertyChanged listener) {

			if (_list != null && listener != null && _list.Contains (listener)) {
				_list.Remove (listener);
				listener.PropertyChanged -= OnPropertyChanged;
			}
		}

		public virtual void ClearNotifier(){

			while (_list != null && _list.Count > 0) {
				RemoveNotifier (_list [0]);
			}
		}


		protected abstract void OnPropertyChanged (object sender, PropertyChangedEventArgs e);

	}
}

