using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;

using BlrtData.IO;
using System.Threading;

namespace BlrtData
{
	public class BlrtNewsItemModel
	{
		private const string CacheFileNamePrefix	= "news_";
		private const string TempFileNamePrefix		= "news_temp_";

		private const int DownloadPriority			= 15;


		public static List<BlrtNewsItemModel> FetchFromCache (string slug)
		{
			string cacheFile = BlrtConstants.Directories.Default.GetFilesPath (CacheFileNamePrefix + slug);

			if (!File.Exists (cacheFile))
				return null;


			return FetchFromString (File.ReadAllText (cacheFile));
		}

		public async static Task<List<BlrtNewsItemModel>> FetchFromUrlAsync (string url, string slug)
		{
			string tempFile = BlrtConstants.Directories.Default.GetFilesPath (TempFileNamePrefix + slug);


			var download = BlrtDownloader.Download (DownloadPriority, new Uri (url), tempFile);


			await download.WaitAsync ();

			if (download.Failed)
				return null;


			var results = FetchFromString (File.ReadAllText (tempFile));

			if (results != null) {
				File.Copy (tempFile, BlrtConstants.Directories.Default.GetFilesPath (CacheFileNamePrefix + slug), true);
				File.Delete (tempFile);
			}


			return results;
		}


		public static List<BlrtNewsItemModel> FetchFromString (string input)
		{
			if (String.IsNullOrWhiteSpace (input))
				return null;


			List<BlrtNewsItemModel> results = null;


			try {
				results = JsonConvert.DeserializeObject<List<BlrtNewsItemModel>> (input);
			}
			// JSON format errors
			catch (JsonReaderException exception) {
				Console.Error.WriteLine (exception.Message);

				return null;
			}


			{
				Uri uriTryResult;

				results.RemoveAll (newsItem =>
					newsItem.title == null
					|| !(newsItem.image == null || Uri.TryCreate (newsItem.image, UriKind.Absolute, out uriTryResult)
						&& (uriTryResult.Scheme == Uri.UriSchemeHttp || uriTryResult.Scheme == Uri.UriSchemeHttps)
						)
					|| !(newsItem.url == null || Uri.TryCreate (newsItem.url, UriKind.Absolute, out uriTryResult)
						&& (uriTryResult.Scheme == Uri.UriSchemeHttp || uriTryResult.Scheme == Uri.UriSchemeHttps)
						)
				);
			}


			return results;
		}


		public string image;
		public string title;
		public string text;
		public string url;
		public string readMoreText;
		public string[] filter;


		public BlrtNewsItemModel ()
		{
			image	= null;
			title	= null;
			text	= null;
			url		= null;
			readMoreText = null;
		}
	}
}

