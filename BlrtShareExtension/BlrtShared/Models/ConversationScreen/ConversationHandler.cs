using System;
using System.Linq;
using BlrtData.ExecutionPipeline;
using BlrtData.Views.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Parse;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace BlrtData.Models.ConversationScreen
{
	public class ConversationHandler : IComparer<ConversationCellModel>
	{
		/* 
		 * DateOnLastDateCell: It is used to track the date for date cells.
		 * Using this, date decision is made whether datecell needs to be inserted or not
		 */
		//DateTime DateOnLastDateCell;
		
		public event EventHandler RefreshChanged;
		public event EventHandler CloudStateChanged;

		public event EventHandler OnOpenSend;

		public IConversationScreen ConversationScreen{ get; set;}
		private int _loading;
		public int Loading { get { return _loading; } 
			set{ 
				bool call = RefreshChanged != null && ((_loading > 0) ^ (value > 0));
				_loading = value;

				if(call)
					RefreshChanged (this, EventArgs.Empty);

			}
		}

		public bool Refreshing {
			get { return _loading > 0 || _conversation.Refreshing; }
		}

		string[] _userFilter;

		public string[] UserFilter { get { return _userFilter; } 
			set{ 
				if (_userFilter != value) {
					_userFilter = value ?? new String[0];
					//ReloadContents ();
				}
			}
		}

		ConversationUserRelation Relation { get { return _conversation.LocalStorage.DataManager.GetConversationUserRelation (_conversation.ObjectId, (ParseUser.CurrentUser == null) ? "" : ParseUser.CurrentUser.ObjectId) ?? _fake; } }

		BlrtData.Conversation _conversation;
		ObservableCollection<TableModelSection<ConversationCellModel>> _bindingList;
		TableModelSection<ConversationCellModel> _section;
		List<ConversationCellModel> _list;

		IPermissionHandler _permissionHandler;

		bool _hasContent;
		ConversationUserRelation _fake;
		LocalStorageType _localStorageType;

		public IActionBuilder ActionBuilder { get; set;}

		public int BlrtCount { get { return _conversation.BlrtCount;} }

		public bool IsEmptyConversation { get { return _conversation.IsEmpty; } }

		public bool ShouldRefreshContent {
			get { return 
				!IsEmptyConversation &&
				(_conversation.LastRetrieved.AddMinutes(15) < DateTime.Now || 
				 _conversation.NeedsRefresh); }
		}
		public string Id {
			get { return _conversation.ObjectId; }
		}

		public string LocalGuid{
			get { return _conversation.LocalGuid; }
		}

		public string Name {
			get { return _conversation.Name; }
		}

		public bool HasContent {
			get { return _hasContent; }
		}

		public bool OnParse {
			get { return _conversation.OnParse; }
		}

		public IPermissionHandler PermissionHandler {
			get { return _permissionHandler; }
		}

		public bool NotSent {
			get { return  _conversation.OnParse && _conversation.BondCount <= 1; }
		}

		public bool OnCloud {
			get { return  _conversation.OnParse || !_conversation.ShouldUpload; }
		}

		public bool OnContentCloud {
			get { return  _conversation.OnParse && _conversation.LocalStorage.DataManager.GetIdleUnsyncedConversationItemCount(_conversation) == 0; }
		}

		public ILocalStorageParameter LocalStorage
		{ 
			get { 
				return LocalStorageParameters.Get (_localStorageType);
			}
		}

		public ConversationHandler (string conversationId, LocalStorageType localStorageType = LocalStorageType.Default) {
			_localStorageType = localStorageType;
			_permissionHandler = new ConversationPermissionHandler (conversationId);

			_userFilter = new String[]{};

			_conversation = LocalStorage.DataManager.GetConversation (conversationId);

			_conversation.PropertyChanged += (sender, e) => {
				if(e.PropertyName.Contains("OnParse")){
					//fire conversation onparse change event
					if(CloudStateChanged!=null){
						CloudStateChanged(this, EventArgs.Empty);
					}
				}
			};

			_conversation.RefreshingChanged += delegate { RefreshChanged?.Invoke (this, EventArgs.Empty); };

			_fake = ConversationUserRelation.CreatePlaceholder (_conversation);

			_bindingList = new ObservableCollection<TableModelSection<ConversationCellModel>> ();
			_bindingList.Add(_section = new ConversationSectionModel());

			_list = new List<ConversationCellModel> ();
		}

		public ObservableCollection<TableModelSection<ConversationCellModel>> GetList ()
		{
			if (_list.Count == 0)
				ReloadContents ();

			return _bindingList;
		}

		bool ShouldAddDateCell (ConversationCellModel cellToAdd, int index)
		{
			if (index == 0) {
				return true;
			}

			var previousCell = _section [index - 1];

			return previousCell.ContentType != BlrtContentType.Date &&
				               previousCell.CreatedAt.Date.CompareTo (cellToAdd.CreatedAt.Date) != 0;
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		void ReloadCloudContents(ref int index, IOrderedEnumerable<ConversationItem> items, ConversationUserRelation rel)
		{
			for (int i = 0; i < _conversation.GeneralCount; ++i) {

				var model = _list.FirstOrDefault (arg => arg.Index == i && arg.Length == 0);
				var item = items.FirstOrDefault (arg => arg.Index == i);

				if (item == null) {
					//					Console.Write ("fds");
				} else {

					if (model == null) {
						model = new ConversationCellModel (this);
					}

					model.SetContent (item, rel);
					model.Action = null;

					if (index != i)
						AddLoadingGap (index, i);

					//remove any gaps
					_list.RemoveAll (arg => arg.Index == i && arg.Length > 0);

					index = i + 1;

					SortedAdd (_section, model);
				}
			}
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		void ReloadLocalContents(ref int index,IOrderedEnumerable<ConversationItem> items, ConversationUserRelation rel)
		{
			//item that are not on parse dont have an index between 0 and GeneralCount
			foreach (var item in items.Where (i => !i.OnParse)) {
				var tmpIndex = index;
				var cell = _section.FirstOrDefault (arg => arg.Index == tmpIndex && arg.Length == 0);


				if (cell == null) {
					cell = new ConversationCellModel (this);
				}

				cell.SetContent (item, rel);
				cell.Action = null;

				cell.Index = index;

				SortedAdd (_section, cell);

				var missingCell = _section.FirstOrDefault (arg => arg.Index == tmpIndex && arg.Length != 0);
				if (missingCell != null) {
					missingCell.Index++;
					missingCell.Length--;
					if (missingCell.Length == 0) {
						_section.Remove (missingCell);
					}
				}
				++index;
			}
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		void ReloadSendingStatusCells(ref int index, ConversationUserRelation rel)
		{
			//checking sending items
			Contents.Event.ConversationSendFailureEventItem item = null;
			if (_conversation.CurrentActivityStatus == BlrtData.Conversation.ActivityStatus.InProgress) {
				item = new BlrtData.Contents.Event.ConversationSendFailureEventItem ();
				item.SendingStatus = BlrtData.Conversation.ActivityStatus.InProgress;
			} else if (_conversation.CurrentActivityStatus == BlrtData.Conversation.ActivityStatus.Failed) {
				item = new BlrtData.Contents.Event.ConversationSendFailureEventItem ();
				item.SendingStatus = BlrtData.Conversation.ActivityStatus.Failed;
			}

			var tmpIndex = index;
			if (item != null) {
				var cell = _section.FirstOrDefault (arg => arg.Index == tmpIndex && arg.Length == 0);
				if (cell == null) {
					cell = new ConversationCellModel (this);
				}
				cell.Action = new OpenSendAction (() => {
					if (item.SendingStatus == Conversation.ActivityStatus.Failed)
						OnOpenSend?.Invoke (this, EventArgs.Empty);
				});

				cell.SetContent (item, rel);

				cell.Index = index;

				SortedAdd (_section, cell);

				var missingCell = _section.FirstOrDefault (arg => arg.Index == tmpIndex && arg.Length != 0);
				if (missingCell != null) {
					missingCell.Index++;
					if (missingCell.Length == 0) {
						_section.Remove (missingCell);
					}
				}
				++index;
			} else {
				var cell = _section.FirstOrDefault (arg => arg.Index == tmpIndex && arg.Length == 0);
				if ((cell != null) && (cell.CellIdentifer == ConversationCellModel.SendFailureCellIndentifer)) {
					_section.Remove (cell);
				}
			}	
		}

		bool IsUserInitiatedEvent (ConversationCellModel item) 
		{
			return item.ContentType == BlrtContentType.AddEvent
				       || item.ContentType == BlrtContentType.MessageEvent
				       || item.ContentType == BlrtContentType.UnknownEvent;
		}

		void UpdateModels ()
		{
			for (int i = 0; i < _section.Count; ++i) {

				var item = _section [i];

				if (item.ContentType == BlrtContentType.Date) {
					// no need to set anything further for date cells
					continue;
				}

				if (!_list.Contains (item))
					_list.Add (item);

				if (ActionBuilder != null && item.CloudPressed == null)
					item.CloudPressed = ActionBuilder.CloudAction (item);
				if (ActionBuilder != null && item.SettingPressed == null)
					item.SettingPressed = ActionBuilder.SettingsAction (item);
				

				var isLastConversationItem = (i == _section.Count - 1);
				var isLastConversationItemBySameAuthor = true;

				if (!isLastConversationItem) {
					var nextItem = _section [i + 1];
					var isNextItemEvent = IsUserInitiatedEvent(nextItem);

					isLastConversationItemBySameAuthor = isNextItemEvent || (item.Author != nextItem.Author);
				}

				item.IsLastConversationItemBySameAuthor = isLastConversationItemBySameAuthor;

				if (i == 0) {
					item.ShowAuthor = !item.IsAuthor;
				} else {

					var other = _section [i - 1];

					//bug, some how the content reload sisnt removed when the content is uploaded sometimes
					if (item.Index != other.Index && (item.Length > 0 || other.Length > 0)) {

						if (item.Length > 0) {
							_section.RemoveAt (i);
						} else {
							_section.RemoveAt (i - 1);
						}
						--i;

					} else {

						item.ShowAuthor = !item.IsAuthor &&
									(item.Author != other.Author
										|| IsUserInitiatedEvent (item)
										|| IsUserInitiatedEvent (other)
										|| item.Index != other.Index + 1);

						other.ShowFooter = item.ShowAuthor;
					}
				}
			}
		}

		public void ReloadContents ()
		{
			var items = LocalStorage.DataManager.GetConversationItems (_conversation)
				.OrderBy (arg => arg);

			int index = 0;

			var rel = Relation;

			ReloadCloudContents (ref index, items, rel);

			_hasContent = index != 0;

			if (index != _conversation.GeneralCount)
				AddLoadingGap (index, _conversation.GeneralCount);

			ReloadLocalContents (ref index, items, rel);
			ReloadSendingStatusCells (ref index, rel);
			UpdateModels ();
		}

		public class OpenSendAction: ICellAction
		{
			Action _action;
			public OpenSendAction(Action action){
				_action = action;
			}
			#region ICellAction implementation
			public void Action (object sender)
			{
				_action?.Invoke ();
			}
			#endregion
			
		}

		void AddLoadingGap (int index1, int index2)
		{
			var start = index1;
			var length = index2 - index1;

			var model = _section.FirstOrDefault (arg => arg.Index == start && arg.Length == length);

			if (model == null) {
				model = new ConversationCellModel (this);
			}

			if (model.ContentType == BlrtContentType.Date) {
				return;
			}

			model.SetContent (
				start, 
				length, 
				(start == 0 &&  _conversation.GeneralCount == start && _loading > 0) 
				|| Query.BlrtConversationQuery.HasQuery(_conversation.ObjectId, start, length, _conversation.LocalStorage)
			);

			model.Action = new GetContentAction (this);
			SortedAdd (_section, model);
		}

		bool RemoveDateCellIfNeeded (int currentIndex, ObservableCollection<ConversationCellModel> section)
		{
			if (currentIndex > 0) {
				var previousCell = section [currentIndex - 1];
				if (previousCell.ContentType == BlrtContentType.Date) {
					if (currentIndex + 1 == section.Count) {
						section.Remove (previousCell);
						return true;
					}

					var nextCell = section [currentIndex + 1];
					if (nextCell.ContentType == BlrtContentType.Date) {
						section.Remove (previousCell);
						return true;
					}
				}
			}
			return false;
		}

		ConversationCellModel GetDateCellModel (DateTime date)
		{
			return new ConversationCellModel (this) {
				ContentType = BlrtContentType.Date,
				CreatedAt = date,
				Index = -1
			};
		}

		void SortedAdd (ObservableCollection<ConversationCellModel> section, 
		                ConversationCellModel cell) 
		{
			bool inList = cell.Length > 0 
                || IsUserInitiatedEvent(cell)
 				|| _userFilter.Length == 0 
				|| _userFilter.Contains (cell.Author.ObjectId);

			var other = section.FirstOrDefault (c =>  
			                                    (string.IsNullOrEmpty(c.ContentId) || c.ContentId == cell.ContentId) && 
			                                    c.Index == cell.Index);

			if (other != null) 
			{
				if (!inList) 
				{
					var index = section.IndexOf (other);
					/*
					 * If the cell is the only cell between two date cell, date cell needs to be removed as well because
					 * there won't be any cell with the date of the date cell after the cell will have been removed.
					 * --------cell-------
					 * --------other-----   <----- will be removed
					 * --------cell-------
					 */
					RemoveDateCellIfNeeded (index, section);
					section.Remove (other);
				} 
				else 
				{ // inList
					if (other != cell) {
						var index = section.IndexOf (other);
						if (RemoveDateCellIfNeeded (index, section)) {
							index--;
						}

						if (ShouldAddDateCell (cell, index)) {
							var dateCellModel = GetDateCellModel (cell.CreatedAt);
							section [index] = dateCellModel;
							index++;
							section.Insert (index, cell);
						} else {
							section [index] = cell;
						}
					}
				}
			} 
			else // other == null
			{
				if (inList) {
					for (int i = 0; i < section.Count; ++i) {
						var model = section [i];

						if (Compare (model, cell) < 0) {
							if (cell.Author != null && ShouldAddDateCell (cell, Math.Max (i, 0))) {
								var dateCellModel = GetDateCellModel (cell.CreatedAt);
								section.Insert (i, dateCellModel);
								i++;
							}
							section.Insert (i, cell);
							return;
						}	
					}
					if (cell.Author != null && ShouldAddDateCell(cell, Math.Max(section.Count, 0))) {
						var dateCellModel = GetDateCellModel (cell.CreatedAt);
						section.Add (dateCellModel);
					}
					section.Add (cell);
				}
			}
		}

		[MethodImpl (MethodImplOptions.AggressiveInlining)]
		public int Compare (ConversationCellModel x, ConversationCellModel y) {
			if (x.ContentType == BlrtContentType.Date) {
				return -x.CreatedAt.Date.CompareTo (y.CreatedAt.Date);
			}
			return -x.Index.CompareTo (y.Index);
		}

		public Task Refresh () {
			return LoadContent ( -1, 0 );
		}

		public async Task LoadContent (int start, int length)
		{ 
			++Loading;
			try {
				if(_conversation.OnParse){
					var query = Query.BlrtConversationQuery.RunQuery (
						_conversation.ObjectId, start, length, _conversation.LocalStorage);
					//ReloadContents ();
					await query.WaitAsync();
				}

			} catch{
			} finally {
				--Loading;
			}
			ReloadContents ();
		}

		public async Task LoadMeta ()
		{
			++Loading;

			try {
				if(_conversation.OnParse){
					var query = Query.BlrtConversationMetaQuery.RunQuery(
						Relation
					);
					await query.WaitAsync();
				}

			} catch {
			} finally {
				--Loading;
			}
		}

		public void CleanUp(){
		
			foreach (var item in _list)
				item.ClearNotifier ();
		}


		public void ReadAllComments(){
		
			var indexes = from item in _list
			            where item.Unread && item.ContentType == BlrtContentType.Comment && item.OnParse
			            select item.ReadableIndex;
		
			if(ParseUser.CurrentUser != null)
				Relation.MarkViewed(indexes);
			Relation.MarkAllUnreadMesaagesViewed ();
		}


		public class GetContentAction : ICellAction {

			ConversationHandler parent;
		
			public GetContentAction(ConversationHandler parent){
				this.parent = parent;
			}


			public void Action (object sender) {
				var cell = parent._list.FirstOrDefault(item => item.Action == this);

				if (cell != null)
					LoadContent (cell.Index, cell.Length);
			}

			async void LoadContent (int start, int length){
				await parent.LoadContent (start, length);

			}
		}

		public interface IActionBuilder {
			ICellAction CloudAction (ConversationCellModel model);
			ICellAction SettingsAction (ConversationCellModel model);
		}

		internal void OnConversationUserDeleted (string userId)
		{
			var models = _list.Where (model =>
									  // query all items deleted user is author of
									  model.Author.ObjectId == userId ||
									  model.Author.FacebookId == userId ||

									  // query all events as deleted user might not be the author of an event
									  model.ContentType == BlrtContentType.AddEvent ||
									  model.ContentType == BlrtContentType.MessageEvent ||
									  model.ContentType == BlrtContentType.SendFailureEvent ||
									  model.ContentType == BlrtContentType.UnknownEvent);

			foreach (var model in models) {
				model.ForceUpdate ();
			}
		}
	}
}

