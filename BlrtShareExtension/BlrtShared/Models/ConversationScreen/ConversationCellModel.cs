using System;
using BlrtData.Contents;
using System.Collections.Generic;
using Xamarin.Forms;
using BlrtData.Views.Helpers;
using BlrtData.Contents.Event;
using System.Threading;
using BlrtAPI;
using System.Threading.Tasks;
using Parse;
using System.Linq;

namespace BlrtData.Models.ConversationScreen
{

	public class ConversationCellModel : BasicModel, ITableModelCell
	{

		public const string MissingContentCellIndentifer = "MissingContentCell";
		public const string BlrtCellIndentifer = "BlrtCell";
		public const string EventCellIndentifer = "EventCell";
		public const string CommentCellIndentifer = "CommentCell";
		public const string SendFailureCellIndentifer = "SendFailureCell";
		public const string MediaCellIndentifer = "MediaCell";
		public const string AudioCellIndentifer = "AudioCell";
		public const string DateCellIndentifer = "DateCell";
		public const string ImageCellIndentifer = "ImageCell";
		public const string PdfCellIndentifer = "PdfCell";


		public string Title { get { return ""; } }

		int _index;
		int _readableIndex;
		int _positionOffset;
		int _length;

		string _contentId;
		string _conversationId;
		BlrtContentType _type;
		BlrtMediaFormat _mediaType;
		string _publicName;

		bool _isAuthor;
		bool _showAuthor;
		bool _showFooter;
		bool _isPublicBlrt;
		bool _isLastConversationItemBySameAuthor;

		bool _onParse;

		BlrtUser _author;
		DateTime _date;

		Uri [] _cloudPathThumbnails;
		string [] _pathThumbnails;
		string [] _mediaNames;

		bool _unread;

		CloudState _cloudState;

		string _text;
		FormattedString _formattedText;
		FormattedString _formattedInfoText;

		int _pageCount;

		ConversationUserRelation _relation;
		ConversationItem _item;

		public string CellIdentifer {
			get {
				if (_length > 0)
					return MissingContentCellIndentifer;

				switch (ContentType) {
				case BlrtContentType.Blrt:
				case BlrtContentType.BlrtReply:
					return BlrtCellIndentifer;

				case BlrtContentType.AddEvent:
				case BlrtContentType.MessageEvent:
					return EventCellIndentifer;

				case BlrtContentType.Comment:
					return CommentCellIndentifer;

				case BlrtContentType.SendFailureEvent:
					return SendFailureCellIndentifer;
				case BlrtContentType.Media:
					if (_mediaType == BlrtMediaFormat.Image) {
						return ImageCellIndentifer;
					}
					return PdfCellIndentifer;
				case BlrtContentType.Audio:
					return AudioCellIndentifer;
				case BlrtContentType.Date:
					return DateCellIndentifer;
				default:
					return EventCellIndentifer;
				}
			}
		}

		public bool IsLastConversationItemBySameAuthor {
			get { return _isLastConversationItemBySameAuthor; }
			set { SetProperty (ref _isLastConversationItemBySameAuthor, value); }
		}

		public int Index { get { return _index; } set { SetProperty (ref _index, value); } }
		public int ReadableIndex { get { return _readableIndex; } set { SetProperty (ref _readableIndex, value); } }
		public int Length { get { return _length; } set { SetProperty (ref _length, value); } }
		public int PositionOffset { get { return _positionOffset; } set { SetProperty (ref _positionOffset, value); } }

		public string ContentId { get { return _contentId; } set { SetProperty (ref _contentId, value); } }
		public string ConversationId { get { return _conversationId; } set { SetProperty (ref _conversationId, value); } }
		public BlrtContentType ContentType { get { return _type; } set { SetProperty (ref _type, value); } }
		public BlrtMediaFormat MediaType { get { return _mediaType; } set { SetProperty (ref _mediaType, value); } }


		/// <summary>
		/// If the current user is the author of the content item
		/// </summary>
		/// <value><c>true</c> if this instance is author; otherwise, <c>false</c>.</value>
		public bool IsAuthor { get { return _isAuthor; } set { SetProperty (ref _isAuthor, value); } }
		public bool ShowAuthor { get { return _showAuthor; } set { SetProperty (ref _showAuthor, value); } }
		public bool ShowFooter { get { return _showFooter; } set { SetProperty (ref _showFooter, value); } }

		public bool OnParse { get { return _onParse; } set { SetProperty (ref _onParse, value); } }

		public bool IsPublicBlrt { get { return _isPublicBlrt; } set { SetProperty (ref _isPublicBlrt, value); } }

		public BlrtUser Author { get { return _author; } set { SetProperty (ref _author, value); } }
		public DateTime CreatedAt { get { return _date; } set { SetProperty (ref _date, value); } }

		public Uri [] CloudPathThumbnails { get { return _cloudPathThumbnails; } set { SetProperty (ref _cloudPathThumbnails, value); } }
		public string [] PathThumbnails { get { return _pathThumbnails; } set { SetProperty (ref _pathThumbnails, value); } }

		public string [] MediaNames { get { return _mediaNames; } set { SetProperty (ref _mediaNames, value); } }

		public bool Unread {
			get { return _unread; }
			set {
				SetProperty (ref _unread, value);
			}
		}
		public int PageCount { get { return _pageCount; } set { SetProperty (ref _pageCount, value); } }

		public CloudState CloudState { get { return _cloudState; } set { SetProperty (ref _cloudState, value); } }

		public string Text { get { return _text; } set { SetProperty (ref _text, value); } }
		public FormattedString FormattedText { get { return _formattedText; } set { SetProperty (ref _formattedText, value); } }
		public FormattedString FormattedInfoText { get { return _formattedInfoText; } set { SetProperty (ref _formattedInfoText, value); } }

		public string PublicName { get { return _publicName; } set { SetProperty (ref _publicName, value); } }

		Color _backgroundColor;
		public Color BackgroundColor { get { return _backgroundColor; } set { SetProperty (ref _backgroundColor, value); } }

		public ICellAction Action { get; set; }
		public ICellAction CloudPressed { get; set; }
		public ICellAction SettingPressed { get; set; }


		bool _shouldPresentInfo;
		public bool ShouldPresentInfo { get { return _shouldPresentInfo; } set { SetProperty (ref _shouldPresentInfo, value); } }
		public bool ShowInfoButton { get; set; }

		bool _showRequestButton;
		public bool ShowRequestButton { get { return _showRequestButton; } set { SetProperty (ref _showRequestButton, value); } }

		/// <summary>
		/// Gets a value indicating whether the cell model has any media. For media and blrt items, model without media is considered corrupt.
		/// <value><c>true</c> if is media data valid; otherwise, <c>false</c>.</value>
		public bool IsMediaDataValid { get; set; }

		/// <summary>
		/// Gets a value indicating whether cell model is for cell that should have media
		/// </summary>
		/// <value><c>true</c> if should have media; otherwise, <c>false</c>.</value>
		public bool ShouldHaveMedia {
			get {
				return ContentType == BlrtContentType.Blrt
					|| ContentType == BlrtContentType.BlrtReply
					|| ContentType == BlrtContentType.Media;
			}
		}

		public ConversationHandler ConversationHandle { get; set; }

		public ConversationCellModel (ConversationHandler convoHandle)
		{
			ConversationHandle = convoHandle;
		}

		public void ForceUpdate ()
		{
			OnPropertyChanged (this, null);
		}


		public void SetContent (int index, int length, bool loading)
		{
			RemoveNotifier (_item);
			RemoveNotifier (_relation);

			_item = null;
			_relation = null;


			SilencePropertyChanges ();

			ContentType = BlrtData.BlrtContentType.UnknownEvent;

			Index = index;
			Length = length;
			CloudState = loading ? BlrtData.CloudState.DownloadingContent : BlrtData.CloudState.ContentMissing;

			FireSilencedPropertyChanges ();
		}

		public override void ClearNotifier ()
		{
			base.ClearNotifier ();

			_item = null;
			_relation = null;
		}

		public void SetContent (ConversationItem item, ConversationUserRelation relation)
		{
			if (item != _item) {
				RemoveNotifier (_item);
				_item = item;
				AddNotifier (_item);
			}
			if (relation != _relation) {
				RemoveNotifier (_relation);
				_relation = relation;
				AddNotifier (_relation);
			}
		}

		void UpdateFields (bool refreshMonitoring = false)
		{
			if (_item == null || _relation == null)
				return;

			SilencePropertyChanges ();

			var user = _item.LocalStorage.DataManager.GetUser (_item.CreatorId);
			if (refreshMonitoring) {
				RefreshMonitoring ();
			}
			if (_item.OnParse)
				Index = _item.Index;
			Length = 0;

			ContentId = _item.ObjectId;
			ConversationId = _item.ConversationId;
			ContentType = _item.Type;

			ReadableIndex = _item.ReadableIndex;

			OnParse = _item.OnParse;

			IsPublicBlrt = _item.IsPublicBlrt;
			IsAuthor = _item.CreatedByCurrentUser;
			Author = user;

			if (_item is ConversationBlrtItem) {
				var item = _item as ConversationBlrtItem;
				PublicName = item.PublicBlrtName;
			}
			CreatedAt = _item.CreatedAt;
			Unread = !_relation.HasViewed (_item);
			CloudState = _item.GetCloudState ();
			MediaNames = _item.MediaNames;

			switch (ContentType) {
			case BlrtContentType.Media:
				PathThumbnails = _item.LocalThumbnailPaths;
				CloudPathThumbnails = _item.CloudThumbnailPaths;
				MediaNames = _item.MediaNames;
				MediaType = (_item as ConversationMediaItem).MediaType;

				if (_mediaType == BlrtMediaFormat.PDF) {
					PageCount = (_item as ConversationMediaItem).Pages;
				}
				IsMediaDataValid = MediaNames == null ? false : MediaNames.Length > 0;
				break;

			case BlrtContentType.Audio:
				Text = (_item as ConversationAudioItem).AudioLength.ToString ();
				break;

			case BlrtContentType.Comment:
				Text = (_item as ConversationCommentItem).Comment;
				FormattedText = new FormattedString () {
					Spans = {
							new Span(){
								Text = Text
							}
						}
				};
				break;

			case BlrtContentType.Blrt:
			case BlrtContentType.BlrtReply:
				Text = BlrtUtil.ToMinutes ((_item as ConversationBlrtItem).AudioLength);
				PageCount = (_item as ConversationBlrtItem).Pages;
				FormattedText = null;

				if (_item.LocalThumbnailNames == null || string.IsNullOrEmpty (_item.LocalThumbnailNames [0])) {
					CloudPathThumbnails = new Uri [] { _relation.Conversation.CloudThumbnailPath };
					PathThumbnails = new string [] { _relation.Conversation.LocalThumbnailPath };
				} else {
					CloudPathThumbnails = new Uri [] { _item.CloudThumbnailPaths [0] };
					PathThumbnails = new string [] { _item.LocalThumbnailPath (0) };
				}
				var items = (_item as ConversationBlrtItem).MediaData.Items;
				IsMediaDataValid = items == null ? false : items.Length > 0;
				break;
			case BlrtContentType.AddEvent:
			case BlrtContentType.MessageEvent:
			case BlrtContentType.UnknownEvent:
				PopEventVariables (_item);
				break;
			case BlrtContentType.SendFailureEvent:
				this.FormattedText = new FormattedString () {
					Spans = {
							new Span () {
								Text = (_item as ConversationSendFailureEventItem).SendingStatus.ToString (),
							}
						}
				};

#if __IOS__
				var infoStr = new Xamarin.Forms.FormattedString ();
					infoStr.Spans.Add (new Xamarin.Forms.Span ()
					{
					Text = string.Format ("{0}{1}", BlrtiOS.BlrtHelperiOS.Translate ("sending_failed_msg", "sending_convo"), Environment.NewLine),
						ForegroundColor = BlrtStyles.BlrtGray6,
						FontSize = 12,
						FontAttributes = Xamarin.Forms.FontAttributes.None
					});
					infoStr.Spans.Add (new Xamarin.Forms.Span ()
					{
						Text = BlrtiOS.BlrtHelperiOS.Translate ("sending_failed_action", "sending_convo"),
						ForegroundColor = BlrtStyles.BlrtCharcoal,
						FontSize = 12,
						FontAttributes = Xamarin.Forms.FontAttributes.Bold
					});

					this.FormattedInfoText = infoStr;
#endif
				break;
			}

			FireSilencedPropertyChanges ();
		}

		Tuple<string [], Uri []> GetThumbnailPaths (ConversationItem item)
		{
			var mediaAsets = item.LocalStorage.DataManager.GetMediaAssets (item);
			var localPaths = (from p in mediaAsets select p.LocalThumbnailPath).ToArray ();
			var cloudPaths = (from p in mediaAsets select p.CloudThumbnailPath).ToArray ();
			return new Tuple<string [], Uri []> (localPaths, cloudPaths);
		}

		protected override void OnPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			UpdateFields (true);
		}


		//variables: FormattedText, Text, ShowInfoButton, ShowRequestButton
		void PopEventVariables (ConversationItem item)
		{
			string authorName = Author.DisplayName;
			string email = Author.Email;
			if (item is BlrtData.Contents.Event.ConversationAddEventItem) {
				FormattedText = GetFormattedTextForConversationAddEventItem (item as ConversationAddEventItem);
				Text = FormattedText.ToString ();
				return;
			}

			if (item is BlrtData.Contents.Event.ConversationMessageEventItem) {
				var ev = item as BlrtData.Contents.Event.ConversationMessageEventItem;
				var output = new FormattedString ();

				var data = ev.EventData;
				var type = data.MessageType;

				bool shouldTry = true;
				while (shouldTry) {
					switch (type) {
					case (int)MessageEventMessageType.OldVersion: // this is for supporting of very old app, which doesn't have message type
						if (data.Fallback != null) {
							type = data.Fallback.MessageType;
							data = data.Fallback;
							break;
						} else {
							type = (int)MessageEventMessageType.Action;
							break;
						}
					case (int)MessageEventMessageType.LeaveConvo:
						if (data.RemoveType == "leave") {
							output = GetFormattedStringForLeaveConvoEvent (data.RemovedUserDisplayName);
							this.ShowInfoButton = true;
							shouldTry = false;
						} else if (data.RemoveType == "remove") {
							output = GetFormattedStringForRemovedFromConvoEvent (data.RemoveInitiator, data.RemovedUserContactDetail [0]);
							shouldTry = false;
						} else {
							if (data.Fallback != null) {
								type = data.Fallback.MessageType;
								data = data.Fallback;
								break;
							} else {
								type = (int)MessageEventMessageType.Action;
								break;
							}
						}
						break;
					case (int)MessageEventMessageType.ConvoNameChanged:
						var changeNameStr = new FormattedString ();
						changeNameStr.Spans.Add (new Span () {
							Text = data.ChangingInitiator,
							ForegroundColor = BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = FontAttributes.Bold
						});
						changeNameStr.Spans.Add (new Span () {
							Text = string.Format (BlrtUtil.PlatformHelper.Translate (" changed the conversation name to ")),
							ForegroundColor = BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = FontAttributes.None
						});
						changeNameStr.Spans.Add (new Span () {
							Text = data.NewName,
							ForegroundColor = BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = FontAttributes.Bold
						});
						output = changeNameStr;
						this.ShowInfoButton = false;
						shouldTry = false;
						break;
					case (int)MessageEventMessageType.AccessDetermined: //type for update event, will have taget to previous content item
						UpdateTargetConversationItem (data.Update);
						shouldTry = false;
						break;
					case (int)MessageEventMessageType.RequestAccess: //type for request access event
						var convoCreatorId = LocalStorageParameters.Default.DataManager.GetConversation (item.ConversationId).CreatorId;
						if (item.Status == 0 && ParseUser.CurrentUser.ObjectId == convoCreatorId) {
							this.ShowRequestButton = true;
						} else {
							this.ShowRequestButton = false;
						}
						this.BackgroundColor = BlrtStyles.BlrtGray2;
						output = BoldStringWithName (data.Message, authorName, email);
						shouldTry = false;
						break;
					case (int)MessageEventMessageType.FormattedText:
						output = BoldStringWithName (data.Message, authorName);
						var infoStr = new FormattedString ();
						infoStr.Spans.Add (new Span () {
							Text = data.Info,
							ForegroundColor = BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = FontAttributes.None
						});
						this.FormattedInfoText = infoStr;
						if (!string.IsNullOrWhiteSpace (data.Info))
							this.ShowInfoButton = true;

						shouldTry = false;
						break;
					case (int)MessageEventMessageType.Note:
						output = AddANote (data.Message, authorName);
						shouldTry = false;
						break;
					case (int)MessageEventMessageType.Action:
						output = SimpleNameMessage (data.Message, authorName);
						shouldTry = false;
						break;
					default:
						if (data.Fallback != null) {
							type = data.Fallback.MessageType;
							data = data.Fallback;
							break;
						} else {
							type = (int)MessageEventMessageType.Action;
							break;
						}

					}
				}
				FormattedText = output;
				Text = FormattedText.ToString ();
				return;
			}


			FormattedText = new FormattedString ();
			Text = FormattedText.ToString ();
		}

		/// <summary>
		/// Gets the formatted text for conversation add event item.
		/// </summary>
		/// <returns>The formatted text for conversation add event item.</returns>
		/// <param name="conversationAddEventItem">Conversation add event item.</param>
		/// <param name="author">Author.</param>
		FormattedString GetFormattedTextForConversationAddEventItem (ConversationAddEventItem conversationAddEventItem)
		{
			var ev = conversationAddEventItem;
			var output = FormattedStringForEvent (Author.DisplayName, string.Empty, Author.IsDeleted);

			output.Spans.Add (new Span () {
				Text = (Author.IsDeleted ? "\n" : " ") + "added",
				ForegroundColor = BlrtStyles.BlrtGray6,
				FontSize = 11,
				FontAttributes = FontAttributes.None
			});
			for (int i = 0; i < ev.EventData.items.Length; ++i) {

				var user = ev.EventData.items [i];
				output.Spans.Add (new Span () {
					Text = "\n"
				});

				var spans = AsSpan (user).Spans;
				for (int j = 0; j < spans.Count; ++j)
					output.Spans.Add (spans [j]);
			}

			return output;
		}

		/// <summary>
		/// Gets the formatted string for removed from convo event.
		/// </summary>
		/// <returns>The formatted string for removed from convo event.</returns>
		/// <param name="removeInitiator">Remove initiator.</param>
		/// <param name="removedUserContactDetail">Removed user contact detail.</param>
		FormattedString GetFormattedStringForRemovedFromConvoEvent (string removeInitiator, string removedUserContactDetail)
		{
			var leaveStr = FormattedStringForEvent (removeInitiator, string.Empty, Author.IsDeleted);
			var removedFromConvoMsg = BlrtUtil.PlatformHelper.Translate ("removed\n");

			leaveStr.Spans.Add (new Span () {
				Text = Author.IsDeleted ? "\n" + removedFromConvoMsg : " " + removedFromConvoMsg,
				ForegroundColor = BlrtStyles.BlrtGray6,
				FontSize = 11,
				FontAttributes = FontAttributes.None
			});

			leaveStr.Spans.Add (new Span () {
				Text = removedUserContactDetail,
				ForegroundColor = BlrtStyles.BlrtGray6,
				FontSize = 11,
				FontAttributes = FontAttributes.Bold
			});

			return leaveStr;
		}

		/// <summary>
		/// Gets the formatted string for leave convo event.
		/// </summary>
		/// <returns>The formatted string for leave convo event.</returns>
		/// <param name="removedUserDisplayName">Removed user display name.</param>
		FormattedString GetFormattedStringForLeaveConvoEvent (string removedUserDisplayName)
		{
			var leaveStr = FormattedStringForEvent (removedUserDisplayName, string.Empty, Author.IsDeleted);
			var leftConvoMsg = BlrtUtil.PlatformHelper.Translate ("left the conversation");

			leaveStr.Spans.Add (new Span () {
				Text = Author.IsDeleted ? "\n" + leftConvoMsg : " " + leftConvoMsg,
				ForegroundColor = BlrtStyles.BlrtGray6,
				FontSize = 11,
				FontAttributes = FontAttributes.None
			});

			return leaveStr;
		}

		/// <summary>
		/// Gets the formatted string for an event.
		/// </summary>
		/// <returns>The string for event.</returns>
		/// <param name="displayName">Display name.</param>
		/// <param name="contactDetail">Contact detail.</param>
		/// <param name="accountDeleted">If set to <c>true</c> account deleted.</param>
		static FormattedString FormattedStringForEvent (string displayName, string contactDetail, bool accountDeleted)
		{
			var formattedString = new FormattedString {
				Spans = {
					new Span {
						Text = displayName + " ",
						ForegroundColor = BlrtStyles.BlrtBlackBlack,
						FontSize = 11,
						FontAttributes = FontAttributes.Bold
					}
				}
			};

			if (accountDeleted) {
				// show account deleted status
				formattedString.Spans.Add (new Span {
					Text = BlrtUtil.PlatformHelper.Translate ("deleted_user_string", "profile"),
					ForegroundColor = BlrtStyles.BlrtAccentRed,
					FontSize = 11,
					FontAttributes = FontAttributes.Italic
				});
			} else {
				// show contact detail
				formattedString.Spans.Add (new Span {
					Text = contactDetail,
					ForegroundColor = BlrtStyles.BlrtGray6,
					FontSize = 11,
					FontAttributes = FontAttributes.None
				});
			}

			return formattedString;
		}

		async void UpdateTargetConversationItem (UpdateObject [] upInfos)
		{
			var objsNeedToFetch = new List<APIResponseObject> ();
			foreach (var info in upInfos) {
				var obj = BlrtManager.DataManagers.Default.GetConversationItem (info.objectId);
				if (obj != null && obj.Status != info.status) {
					var apiObj = new APIResponseObject ();
					apiObj.ClassName = obj.ClassName;
					apiObj.LocalGuid = obj.LocalGuid;
					apiObj.ObjectId = obj.ObjectId;
					objsNeedToFetch.Add (apiObj);
				}

			}


			if (objsNeedToFetch.Count != 0) {
				var v = await BlrtUtil.FetchAllAsync (objsNeedToFetch, new CancellationTokenSource (15000).Token);
				BlrtManager.DataManagers.Default.AddParse (v);
				try {
					ConversationHandle.ReloadContents ();
				} catch {
				}
			}
		}

		bool _isMornitoring = false;

		public void StartFileMonitoring ()
		{
			_isMornitoring = true;
			_item?.StartFileMonitoring (null, 0);
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				UpdateFields (false);
			});
		}

		public void StopFileMonitoring ()
		{
			_isMornitoring = false;
			_item?.StopFileMonitoring (null, 0);
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				UpdateFields (false);
			});
		}

		void RefreshMonitoring ()
		{
			Task.Factory.StartNew (() => {
				_item?.StartFileMonitoring (null, 0);
			});
		}

#region FormattedString
		public static FormattedString BoldStringWithName (string message, string authorName, string email = "")
		{
			var output = new FormattedString ();


			var text = string.Format (message, authorName, email);
			var parts = text.Split (new string [] { ConversationEventItem.BlackPrefix }, StringSplitOptions.None);
			bool black = false;
			foreach (var part in parts) {
				var str = part;
				bool bold = false;
				if (part.Contains (ConversationEventItem.BoldPrefix)) {
					var smallparts = part.Split (new string [] { ConversationEventItem.BoldPrefix }, StringSplitOptions.None);
					foreach (var smallpart in smallparts) {
						output.Spans.Add (new Span () {
							Text = smallpart,
							ForegroundColor = black ? BlrtStyles.BlrtBlackBlack : BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = bold ? FontAttributes.Bold : FontAttributes.None
						});
						bold = !bold;
					}

				} else {
					output.Spans.Add (new Span () {
						Text = str,
						ForegroundColor = black ? BlrtStyles.BlrtBlackBlack : BlrtStyles.BlrtGray5,
						FontSize = 12,
						FontAttributes = bold ? FontAttributes.Bold : FontAttributes.None
					});
				}
				black = !black;
			}

			return output;
		}

		public static FormattedString AddANote (string message, string authorName)
		{
			var output = new FormattedString ();

			output.Spans.Add (new Span () {
				Text = authorName,
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = 12,
				FontAttributes = FontAttributes.Bold
			});
			output.Spans.Add (new Span () {
				Text = " added a note\n",
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = 12,
				FontAttributes = FontAttributes.None
			});
			output.Spans.Add (new Span () {
				Text = message,
				ForegroundColor = BlrtStyles.BlrtBlackBlack,
				FontSize = 12,
				FontAttributes = FontAttributes.Bold
			});

			return output;
		}

		public static FormattedString SimpleNameMessage (string message, string authorName)
		{
			var output = new FormattedString ();

			output.Spans.Add (new Span () {
				Text = authorName + " ",
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = 12,
				FontAttributes = FontAttributes.Bold
			});
			output.Spans.Add (new Span () {
				Text = message,
				ForegroundColor = BlrtStyles.BlrtGray5,
				FontSize = 12,
				FontAttributes = FontAttributes.Bold
			});

			return output;
		}

		public static FormattedString AsSpan (BlrtData.Contents.Event.BlrtAddEventData.BlrtAddEventDataItem item)
		{
			var userId = item.userId;

			bool toFB = item.type == "facebook";
			bool toEmail = item.type == "email";
			bool toPhone = item.type == "phoneNumber";

			string namePostfix = "";

			if (toFB) namePostfix += "(Facebook)";
			if (toEmail) namePostfix += string.Format ("{0}", item.value);
			if (toPhone) namePostfix += "(Phone)";

			var rel = BlrtManager.DataManagers.Default.GetConversationUserRelation (item.relationId);
			if (string.IsNullOrWhiteSpace (userId)) {
				if (rel != null)
					userId = rel.UserId;
				//else {
				//	//when the user already be removed from the conversaiton
				//	//the relation is no longer exist
				//	return new FormattedString () {
				//		Spans = {
				//			new Span () {
				//				Text = item.name + " ",
				//				ForegroundColor = BlrtStyles.BlrtBlackBlack,
				//				FontSize = 12,
				//				FontAttributes = FontAttributes.Bold
				//			},
				//			new Span () {
				//				Text = item.value,
				//				ForegroundColor = BlrtStyles.BlrtGray5,
				//				FontSize = 12,
				//				FontAttributes = FontAttributes.None
				//			},
				//		}
				//	};
				//}
			}

			if (!string.IsNullOrWhiteSpace (userId)) {
				var user = BlrtManager.DataManagers.Default.GetUser (userId);
				return FormattedStringForEvent (user.DisplayName, toEmail ? user.Email : namePostfix, user.IsDeleted);
			}

			//non users
			if (toPhone) {
				return new FormattedString () {
					Spans = {
						new Span () {
							Text = BlrtUtil.PlatformHelper.Translate("someone by phone number"),
							ForegroundColor = BlrtStyles.BlrtGray5,
							FontSize = 12,
							FontAttributes = FontAttributes.None
						}
					}
				};
			}

			var formattedString = new FormattedString ();
			var name = string.IsNullOrEmpty (item.name) ? namePostfix : item.name;

			if (BlrtUtil.HasUserSignedUp (rel)) {
				formattedString.Spans.Add (new Span () {
					Text = name + " ",
					ForegroundColor = BlrtStyles.BlrtBlackBlack,
					FontSize = 12,
					FontAttributes = FontAttributes.Bold
				});

				formattedString.Spans.Add (new Span () {
					Text = name,
					ForegroundColor = BlrtStyles.BlrtBlackBlack,
					FontSize = 12,
					FontAttributes = FontAttributes.Bold
				});

				return formattedString;
			}

			if (!string.IsNullOrEmpty (name)) {
				formattedString.Spans.Add (new Span () {
					Text = name + " ",
					ForegroundColor = BlrtStyles.BlrtBlackBlack,
					FontSize = 11,
					FontAttributes = FontAttributes.Bold
				});
			}

			if (string.IsNullOrEmpty (name) || !name.Equals (namePostfix)) {
				formattedString.Spans.Add (new Span () {
					Text = namePostfix,
					ForegroundColor = BlrtStyles.BlrtGray6,
					FontSize = 11,
					FontAttributes = FontAttributes.None
				});
			}

			return formattedString;
		}

#endregion
	}
}

