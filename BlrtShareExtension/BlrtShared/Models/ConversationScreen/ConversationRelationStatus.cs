﻿using System;
using BlrtData;
using System.Collections.Generic;

namespace BlrtData.Models.ConversationScreen
{
	public static class ConversationRelationStatus
	{
		public class Comparer: IComparer<ConversationUserRelation>{
			#region IComparer implementation
			public int Compare (ConversationUserRelation x, ConversationUserRelation y)
			{
				var xIndex = GetTypeOfRelation (x).GetHashCode();
				var yIndex = GetTypeOfRelation (y).GetHashCode ();
				if(xIndex==yIndex){
					return BlrtUtil.GetUserDisplayName (x).CompareTo( BlrtUtil.GetUserDisplayName (y));
				}else{
					return xIndex - yIndex;
				}
			}
			#endregion
		}

		public enum RoleEnum{
			Creator = 0,
			Contributor =1,
			Viewed = 2,
			NotViewed = 3,
			NotSignedUp =4,
			None = 5
		}

		public static RoleEnum GetTypeOfRelation (ConversationUserRelation relation)
		{
			if (string.IsNullOrWhiteSpace (relation.UserId))
				return RoleEnum.NotSignedUp;

			if (relation.UserId == relation.Conversation.CreatorId)
				return RoleEnum.Creator;

			if (relation.HasContributed || (relation.Conversation.CreatedByCurrentUser && relation.IsCurrentUsers))
				return RoleEnum.Contributor;

			if (relation.HasViewedConversation || relation.IsCurrentUsers)
				return RoleEnum.Viewed;

			return RoleEnum.NotViewed;
		}
	}
}

