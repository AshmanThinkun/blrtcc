using System;
using BlrtData.Views.Helpers;

namespace BlrtData.Models.Mailbox
{
	public class MailboxSectionModel : TableModelSection<MailboxCellModel> {

		public override string HeaderIdentifer { get { return null; } }
		public override string FooterIdentifer { get { return null; } }
		public override string Title { get; set; }


		ICellAction _action;
		public override ICellAction Action {
			get { return _action; } set { _action = value; }
		}

		public MailboxSectionModel() {
		}
	}
}

