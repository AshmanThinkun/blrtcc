using System;
using BlrtData.Views.Helpers;
using System.Threading.Tasks;

namespace BlrtData.Models.Mailbox
{
	public class MailboxCellModel : BasicModel, ITableModelCell {

		public const string MailboxCellIndentifer = "MailboxCell";

		string _conversationId;
		string _title;
		BlrtUser _author;
		DateTime _lastUpdated;

		Uri _cloudImage;
		string _image;
		string _authorName;

		bool _archived;
		bool _flagged;
		bool _onParse;

		int _contentCount;
		int _unread;

		CloudState _cloudState;

		public string CellIdentifer { get { return MailboxCellIndentifer; } }


		public string ConversationId { get{ return _conversationId; }	set{ SetProperty (ref _conversationId, value);} }
		public string Title { get{ return _title;}						set{ SetProperty (ref _title, value);} }
		public BlrtUser Author { get{ return _author;}					set{ SetProperty (ref _author, value);} }
		public string AuthorName { get{ return _authorName;}					set{ SetProperty (ref _authorName, value);} }
		public DateTime LastUpdated { get{ return _lastUpdated;}		set{ SetProperty (ref _lastUpdated, value);} }

		public Uri CloudPathThumbnail { get { return _cloudImage;} set{ SetProperty (ref _cloudImage, value);} }
		public string PathThumbnail { 
			get { 
				return  _image;
			} 
			set{ SetProperty (ref _image, value);} 
		}

		public bool Archived { get { return _archived; } set { SetProperty (ref _archived, value); } }
		public bool Flagged { get { return _flagged; } set { SetProperty (ref _flagged, value); } }
		public bool OnParse { get { return _onParse; } set { SetProperty (ref _onParse, value); } }


		public int ContentCount { get{ return _contentCount;} set{ SetProperty (ref _contentCount, value);} }
		public int Unread { get{ return _unread;} set{ SetProperty (ref _unread, value);} }

		public CloudState CloudState { get{ return _cloudState;} set{ SetProperty (ref _cloudState, value);} }

		public ICellAction Action { get; set; }
		public ICellAction MoreAction { get; set; }
		public ICellAction TagAction { get; set; }
		public ICellAction SwipeAction{ get; set;}


		public bool Same(ConversationUserRelation rel){
			return rel.ConversationId == ConversationId || (rel.Conversation != null && rel.Conversation.LocalGuid == ConversationId);
		}
		public bool Same(BlrtData.Conversation conversation){
			return conversation.ObjectId == ConversationId || conversation.LocalGuid == ConversationId;
		}

		ConversationUserRelation _relation;
		BlrtData.Conversation _conversation;


		public void SetRelation (ConversationUserRelation rel)
		{

			if (_relation != rel) {
				RemoveNotifier (_relation);
				_relation = rel;


				if (_conversation != _relation.Conversation) {
					RemoveNotifier (_conversation);
					_conversation = _relation.Conversation;
					AddNotifier (_conversation);
				}
				AddNotifier (_relation);
			}
		}


		public void SetConversation (BlrtData.Conversation conversation)
		{
			RemoveNotifier (_relation);

			if (_conversation != conversation) {
				RemoveNotifier (_conversation);
				_conversation = conversation;
				AddNotifier (_conversation);
			}

		}

		void UpdateFields (bool refreshMonitoring = false)
		{
			if(_relation != null) {
				_conversationId	= _relation.ConversationId;

				OnParse = _relation.Conversation.OnParse;

				Archived			= _relation.Archive;
				Flagged				= _relation.Flagged;
				Unread				= _relation.EstimateUnread();
				Title				= _relation.Conversation.Name;
				LastUpdated			= _relation.Conversation.ContentUpdatedAt;
				ContentCount		= _relation.Conversation.ReadableCount;

				Author				= BlrtData.BlrtManager.DataManagers.Default.GetUser(_relation.Conversation.CreatorId);
				AuthorName = Author?.DisplayName;

				if (refreshMonitoring) {
					RefreshMonitoring ();
				}

				var cloud = _conversation.GetCloudState ();
				if(_conversation==null){

				}else if (cloud != CloudState.DownloadingContent && cloud != CloudState.UploadingContent && _conversation.CurrentActivityStatus == Conversation.ActivityStatus.InProgress)
					cloud = CloudState.UploadingContent;
				else if (_conversation.CurrentActivityStatus == Conversation.ActivityStatus.Failed)
					cloud = CloudState.UploadingFailed;
				CloudState = cloud;

				CloudPathThumbnail	= _relation.Conversation.CloudThumbnailPath;
				var path = _relation.Conversation.LocalThumbnailPath;
				PathThumbnail = string.IsNullOrEmpty (path) ? "logo-placeholder.png" : path;
			} else if(_conversation != null) {

				_conversationId		= _conversation.ObjectId;

				OnParse				= _conversation.OnParse;
				Archived			= false;
				Flagged				= false;
				Unread				= 0;
				Title				= _conversation.Name;
				LastUpdated			= _conversation.ContentUpdatedAt;
				ContentCount		= _conversation.ReadableCount;

				if (refreshMonitoring) {
					RefreshMonitoring ();
				}
				var cloud = _conversation.GetCloudState ();
				if (cloud != CloudState.DownloadingContent && cloud != CloudState.UploadingContent && _conversation.CurrentActivityStatus == Conversation.ActivityStatus.InProgress)
					cloud = CloudState.UploadingContent;
				else if (_conversation.CurrentActivityStatus == Conversation.ActivityStatus.Failed)
					cloud = CloudState.UploadingFailed;
				CloudState = cloud;
				Author				= BlrtData.BlrtManager.DataManagers.Default.GetUser(_conversation.CreatorId);
				AuthorName = Author?.DisplayName;

				CloudPathThumbnail	= _conversation.CloudThumbnailPath;
				PathThumbnail		= _conversation.LocalThumbnailPath;
			}
		}

		protected override void OnPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			UpdateFields (true);
			if (e.PropertyName == "ContentUpdatedAt") {
				Xamarin.Forms.MessagingCenter.Send<Object> (this, "ReloadMailBox");
			}
		}

		bool _isMornitoring = false;

		public void StartFileMonitoring ()
		{
			_isMornitoring = true;
			_conversation?.StartFileMonitoring ();
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				UpdateFields (false);
			});
		}

		public void StopFileMonitoring ()
		{
			_isMornitoring = false;
			_conversation?.StopFileMonitoring ();
			Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
				UpdateFields (false);
			});
		}

		void RefreshMonitoring ()
		{
			Task.Factory.StartNew (() => {
				_conversation?.StartFileMonitoring ();
			});
		}
	}

}

