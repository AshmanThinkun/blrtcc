using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtData.Views.Helpers;
using System.Collections.ObjectModel;
using Parse;

namespace BlrtData.Models.Mailbox
{

	public enum MailboxType
	{
		Any,
		Inbox,
		Archive,
	}


	public interface IMailboxFilter
	{

		MailboxType Type { get; }

		bool CurrentUser { get; }

		IMailboxActionBuilder ActionBuilder { get; set; }


		int PagesLoaded { get; }
		int ItemPerPage { get; }
		int FlaggedCount { get; }
		Dictionary<int, int> IncompleteItemCountDic { get; }
		int IncompleteItemCount { get; }

		bool ShouldRefresh { get; }

		string [] TagFilters { get; }
		string [] UserFilters { get; }
		string Keyword { get; }
		bool ShouldFilterDeletedUsers { get; set; }

		void FilterDeletedUsers ();
		void ApplyTagFilters (string [] tags);
		void ApplyUserFilters (string [] userIds);
		void ApplyKeywordFilter (string keyword);

#if __IOS__
		void ApplyReadonlyConversationHiddenFilter (bool readonlyConversationHidden);
#endif

		MailboxCellModel GetCell (string conversationId);

		void ClearFilters ();
		event EventHandler OnReloadList;
		Task ReloadList();
		Task RefreshList(int skip);
		Task LoadNextPage();

		string[] GetAllConversations();
		string[] GetAllTags();

		ObservableCollection<TableModelSection<MailboxCellModel>> GetList();
	}

	public interface IMailboxActionBuilder {
		ICellAction MoreAction (MailboxCellModel model);
		ICellAction TagAction (MailboxCellModel model);
		ICellAction SwipeAction (MailboxCellModel model);
	}

	public abstract class MailboxFilter : IMailboxFilter, IComparer<MailboxCellModel>
	{
		public abstract MailboxType Type { get; }

		protected MailboxSectionModel FlaggedSection { get; private set; }
		protected MailboxSectionModel DefaultSection { get; private set; }

		public int FlaggedCount{ 
			get{
				if (FlaggedSection == null)
					return 0;
				return FlaggedSection.Count;
			}
		}

		public Dictionary<int,int> IncompleteItemCountDic{ get; private set;}
		public int IncompleteItemCount{
			get{
				var num = 0;
				foreach(var v in IncompleteItemCountDic.Values){
					num += v;
				}
				return num;
			}
		}

		string[] _userIds;
		string[] _tags;
		string _keyword;
		bool _readonlyConversationHidden;

		bool _shouldFilterDeletedUsers;

		public bool ShouldFilterDeletedUsers {
			get {
				return _shouldFilterDeletedUsers;
			}
			set {
				_shouldFilterDeletedUsers = value;
			}
		}

		string _user;
		int _page;


		Task _task;
		System.Threading.SemaphoreSlim _reloadingSemaphore;

		public IMailboxActionBuilder ActionBuilder { get; set; }


		protected ObservableCollection<TableModelSection<MailboxCellModel>> List { 
			get; private set; 
		}

		public abstract bool ShouldRefresh { get; }


		public string[] UserFilters {
			get { return _userIds; }
		}
		public string[] TagFilters {
			get { return _tags; }
		}
		public string UserId {
			get { return _user; }
		}

		public string Keyword{get{return _keyword;}}

		public event EventHandler OnReloadList;



		public int PagesLoaded {
			get { return _page; }
		}
		public int ItemPerPage {
			get { return BlrtData.Query.BlrtInboxQuery.ItemsPerQuery; }
		}

		public bool CurrentUser {
			get{
				return ParseUser.CurrentUser != null
					&& ParseUser.CurrentUser.ObjectId == _user;
			}
		}


		public static MailboxFilter CurrentInstance{ get; private set;}
		public MailboxFilter() {
			CurrentInstance = this;
			_page = 1;

			_reloadingSemaphore = new System.Threading.SemaphoreSlim (1);

			List = new ObservableCollection<TableModelSection<MailboxCellModel>> ();
			IncompleteItemCountDic = new Dictionary<int, int> ();

			if (ParseUser.CurrentUser != null)
				_user = ParseUser.CurrentUser.ObjectId;

			_tags = new string[0];
			_userIds = new string[0];


			List.Add ( FlaggedSection = new MailboxSectionModel () );
			List.Add ( DefaultSection = new MailboxSectionModel () );
		}

		public void FilterDeletedUsers ()
		{
			var removingEle = new List<MailboxCellModel> ();
			foreach(var section in List){
				var delUsers = section.Where (m => m.Author.IsDeleted);
				foreach (var ele in delUsers) {
					removingEle.Add (ele);
				}
			}
			foreach (var ele in removingEle) {
				Remove (ele);
			}
		}

		public void ApplyTagFilters (string[] tags)
		{
			_tags = tags;
		}

		public void ApplyUserFilters (string[] userIds)
		{
			_userIds = userIds;
		}

		public void ApplyReadonlyConversationHiddenFilter (bool readonlyConversationHidden)
		{
			_readonlyConversationHidden = readonlyConversationHidden;
		}

		public void ApplyKeywordFilter(string keyword)
		{
			_keyword = keyword;
		}

		public virtual void ClearFilters ()
		{
			_tags = new string[0];
			_userIds = new string[0];
		}

		public string[] GetAllConversations(){

			if (ParseUser.CurrentUser == null) {
				return new string[0];
			}

			return GetRelations()
				.Select (rel => rel.ConversationId)
				.ToArray();
		}

		public string[] GetAllTags(){

			return GetRelations()
				.SelectMany (rel => rel.Tags)
				.Distinct(new TagsStringComparer())
				.ToArray();
		}

		public class TagsStringComparer : IEqualityComparer<string>{

#region IEqualityComparer implementation
			public bool Equals (string x, string y)
			{
				return x.Equals (y, StringComparison.CurrentCultureIgnoreCase);;
			}
			public int GetHashCode (string obj)
			{
				return obj.ToLower ().GetHashCode ();
			}
#endregion
		}


		public async Task ReloadList ()
		{
			lock (_reloadingSemaphore) {
				if (_task != null)
					return;

				_task = _reloadingSemaphore.WaitAsync ();
			}

			await _task;
			_task = null;

			try {

				if (ParseUser.CurrentUser == null) {
					List.Clear ();
					return;
				}

				if (!List.Contains (FlaggedSection))
					List.Add (FlaggedSection);
				if (!List.Contains (DefaultSection))
					List.Add (DefaultSection);


				var relations = GetRelations ();

				//checks if there is any relation that are not in relation, but are in the mailbox
				var removingEle = new List<MailboxCellModel> ();
				foreach(var section in List){
					foreach (var ele in section.Where(m => !relations.Any(m.Same))) {
						removingEle.Add (ele);
					}
				}
				foreach (var ele in removingEle) {
					Remove (ele);
				}
				foreach (var rel in relations) {			
					if (Filter (rel, true)) {
						var ele = GetMailboxModelCell (rel, true);
						if (ele != null)
							Add (ele);								
					} else {
						var ele = GetMailboxModelCell (rel, false);
						if (ele != null)
							Remove (ele);			
					}
				}


#if __ANDROID__
				if (_shouldFilterDeletedUsers) {
					FilterDeletedUsers ();
				}
#endif

				Sort();
			} finally {
				_reloadingSemaphore.Release ();
			}
			OnReloadList?.Invoke (this, EventArgs.Empty);
		}

		protected abstract IEnumerable<BlrtData.ConversationUserRelation> GetRelations ();

		public MailboxCellModel GetCell (string conversationId)
		{
			foreach (var section in List) {
				var ele = section.FirstOrDefault (model => model.ConversationId == conversationId);

				if (ele != null)
					return ele;
			}

			return null;
		}

		private void Sort()
		{
			InsertionSort (FlaggedSection);
			InsertionSort (DefaultSection);
		}

		private void InsertionSort(MailboxSectionModel section)
		{
			for (var counter = 0; counter < section.Count - 1; counter++)
			{
				var index = counter + 1;
				while (index > 0)
				{
					if(Compare(section[index - 1],section[index])<0)
					{
						var temp = section[index - 1];
						section[index - 1] = section[index];
						section[index] = temp;
					}
					index--;
				}
			}
		}

		protected virtual void Add (MailboxCellModel ele)
		{
			if (ele.Flagged) {
				if (DefaultSection.Contains (ele))
					DefaultSection.Remove (ele);

				if (!FlaggedSection.Contains (ele)) {
					SortedSectionAdd (FlaggedSection, ele);
				}
			} else {
				if (FlaggedSection.Contains (ele))
					FlaggedSection.Remove (ele);

				if (!DefaultSection.Contains (ele)) {
					SortedSectionAdd (DefaultSection, ele);
				}
			}
		}



		void SortedSectionAdd (MailboxSectionModel section, MailboxCellModel ele) {
			for (int i = 0; i < section.Count; ++i) {
				if (Compare(section[i], ele) < 0) {
					section.Insert (i, ele);
					return;
				}	
			}

			section.Add (ele);
		}

		public int Compare (MailboxCellModel x, MailboxCellModel y) {

			var i = -x.OnParse.CompareTo (y.OnParse);

			if (i != 0) return i;



			return x.LastUpdated.CompareTo (y.LastUpdated);
		}

		protected virtual void Remove (MailboxCellModel ele)
		{
			if (DefaultSection.Contains (ele)) {
				DefaultSection.Remove (ele);
			}

			if (FlaggedSection.Contains (ele)) {
				FlaggedSection.Remove (ele);
			}
		}

		public abstract Task RefreshList (int skip);

		protected MailboxCellModel GetMailboxModelCell (ConversationUserRelation rel, bool create)
		{
			MailboxCellModel element = null;

			foreach (var section in List) {
				element = section.FirstOrDefault (model => model.Same(rel));

				if (element != null)
					break;
			}

			if(element == null) {
				if (!create)
					return null;
				element = new MailboxCellModel();

			}

			if(ActionBuilder != null && element.MoreAction == null)
				element.MoreAction = ActionBuilder.MoreAction(element);
			if(ActionBuilder != null && element.TagAction == null)
				element.TagAction = ActionBuilder.TagAction(element);
			if(ActionBuilder != null && element.SwipeAction == null)
				element.SwipeAction = ActionBuilder.SwipeAction(element);

			element.SetRelation (rel);
			return element;
		}

		protected virtual bool Filter(ConversationUserRelation relation, bool applyBasicFilters) {
		
			if (!relation.IsCurrentUsers || relation.Conversation == null || (_readonlyConversationHidden && relation.Conversation.IsReadonly))
				return false;

			if (applyBasicFilters) {

				if (_tags != null && _tags.Length > 0) {

					bool found = false;

					var relTags = relation.Tags;

					for (int i = 0; i < relTags.Length; ++i) {
						if (_tags.Contains (relTags [i].ToLower()))
							found = true;
					}	

					if (!found)
						return false;
				}

				if (_userIds != null && _userIds.Length > 0) {

					var userIds = relation.Conversation.GetUserInConversation ();

					if (!userIds.Any (user => _userIds.Contains (user)))
						return false;
				}

				if (!string.IsNullOrEmpty(Keyword) && !relation.Conversation.Name.ToLower ().Contains (_keyword.ToLower ()))
					return false;
			}

			return true;
		}



		public ObservableCollection<TableModelSection<MailboxCellModel>> GetList ()
		{
			return List;
		}



		public async Task LoadNextPage ()
		{
			int page = _page;
			await RefreshList (_page * ItemPerPage);

			_page = Math.Max(page + 1, _page);
		}
	}

}

