//using System;
//using System.Linq;
//using System.Collections.Generic;
//using BlrtData;
//using System.Threading.Tasks;
//using System.Threading;
//
//namespace BlrtData.Models.Mailbox
//{
//	public class MailboxSentFilter : MailboxFilter, IComparer<MailboxCellModel>
//	{
//		public override MailboxType Type { get { return MailboxType.Sent; } }
//
//		public static DateTime LastUpdated = DateTime.MinValue;
//
//		public override bool ShouldRefresh {
//			get { return BlrtData.Query.BlrtInboxQuery.HasQuery(BlrtData.Query.BlrtInboxQueryType.Sent, 0) || LastUpdated.AddMinutes (15) < DateTime.Now; }
//		}
//
//		public MailboxSentFilter ():base()
//		{
//			BlrtConstants.OnDeleteEverything -= ResetLastUpdatedDate;
//			BlrtConstants.OnDeleteEverything += ResetLastUpdatedDate;
//		}
//
//		void ResetLastUpdatedDate(object sender, EventArgs e){
//			LastUpdated = DateTime.MinValue;
//		}
//
//		protected override IEnumerable<ConversationUserRelation> GetRelations(){
//		
//			return BlrtData.BlrtManager.DataManagers.Default.GetConversationUserRelations(UserId)
//				.Where(rel => rel.Conversation != null && !rel.Archive && rel.Conversation.CreatedByCurrentUser)
//				.Concat(
//					from c in BlrtData.BlrtManager.DataManagers.Default.GetUnsyncedConversations ()
//					select ConversationUserRelation.CreatePlaceholder(c)
//				);
//			
//		}
//
//
//		public override async System.Threading.Tasks.Task RefreshList (int skip)
//		{
//			var query = BlrtData.Query.BlrtInboxQuery.RunQuery (BlrtData.Query.BlrtInboxQueryType.Sent, skip);
//			await query.WaitAsync();
//
//			LastUpdated = DateTime.Now;
//		}
//	}
//}
//
