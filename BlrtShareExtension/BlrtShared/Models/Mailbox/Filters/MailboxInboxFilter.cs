using System;
using System.Linq;
using System.Collections.Generic;

namespace BlrtData.Models.Mailbox
{
	public class MailboxInboxFilter : MailboxFilter
	{
		public override MailboxType Type { get { return MailboxType.Inbox; } }

		static DateTime LastUpdated = DateTime.MinValue;

		public override bool ShouldRefresh {
			get { return BlrtData.Query.BlrtInboxQuery.HasQuery(BlrtData.Query.BlrtInboxQueryType.Inbox, 0) || LastUpdated.AddMinutes (15) < DateTime.Now; }
		}


		public MailboxInboxFilter ():base()
		{
			BlrtConstants.OnDeleteEverything -= ResetLastUpdatedDate;
			BlrtConstants.OnDeleteEverything += ResetLastUpdatedDate;
		}

		public static void ResetLastUpdatedDate(object sender, EventArgs e){
			LastUpdated = DateTime.MinValue;
		}

		protected override IEnumerable<BlrtData.ConversationUserRelation> GetRelations(){

			var a = BlrtData.BlrtManager.DataManagers.Default.GetConversationUserRelations(UserId)
				.Where(rel => rel.Conversation != null && !rel.Archive )
				.Concat(
					from c in BlrtData.BlrtManager.DataManagers.Default.GetUnsyncedConversations ()
					select ConversationUserRelation.CreatePlaceholder(c)
				);
			return a;
		}

		public override async System.Threading.Tasks.Task RefreshList (int skip)
		{
			var query = BlrtData.Query.BlrtInboxQuery.RunQuery (BlrtData.Query.BlrtInboxQueryType.Inbox, skip);
			try {
   				await query.WaitAsync();
				LastUpdated = DateTime.Now;
			} catch {}

		}
	}
}

