using System;
using System.Linq;
using System.Collections.Generic;
using BlrtData.Views.Helpers;

namespace BlrtData.Models.Mailbox
{
	public class MailboxArchiveFilter : MailboxFilter, IComparer<MailboxCellModel>
	{
		public override MailboxType Type { get { return MailboxType.Archive; } }

		public static DateTime LastUpdated = DateTime.MinValue;

		public override bool ShouldRefresh {
			get { return BlrtData.Query.BlrtInboxQuery.HasQuery(BlrtData.Query.BlrtInboxQueryType.Archive, 0) || LastUpdated.AddMinutes (15) < DateTime.Now; }
		}

		public MailboxArchiveFilter ():base()
		{
			BlrtConstants.OnDeleteEverything -= ResetLastUpdatedDate;
			BlrtConstants.OnDeleteEverything += ResetLastUpdatedDate;
		}

		void ResetLastUpdatedDate(object sender, EventArgs e){
			LastUpdated = DateTime.MinValue;
		}

		protected override IEnumerable<BlrtData.ConversationUserRelation> GetRelations(){

			return BlrtData.BlrtManager.DataManagers.Default.GetConversationUserRelations(UserId)
				.Where(rel => rel.Conversation != null && rel.Archive);

		}

		public override async System.Threading.Tasks.Task RefreshList (int skip)
		{
			try {
				var query = BlrtData.Query.BlrtInboxQuery.RunQuery (BlrtData.Query.BlrtInboxQueryType.Archive, skip);
				await query.WaitAsync();
				LastUpdated = DateTime.Now;
			} catch {}
		}
	}
}

