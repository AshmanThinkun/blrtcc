﻿using System;
using BlrtData.Views.Helpers;

namespace BlrtData.Models.Organisation
{
	public class OrganisationListItemSectionModel: TableModelSection<OrganisationListItemCellModel>
	{
		#region implemented abstract members of TableModelSection

		public override string HeaderIdentifer {
			get {
				return null;
			}
		}

		public override string FooterIdentifer {
			get {
				return null;
			}
		}

		public override string Title { get; set;}

		public override ICellAction Action { get; set;}

		#endregion

		public OrganisationListItemSectionModel ()
		{
		}
	}
}

