﻿using System;
using BlrtData.Views.Helpers;
using BlrtData.Contents;

namespace BlrtData.Models.Organisation
{
	public class OrganisationListItemCellModel : BasicModel, ITableModelCell
	{
		public string CellIdentifer {
			get {
				return OrganisationListItemCellIdentifer;
			}
		}

		public const string OrganisationListItemCellIdentifer = "OrganisationListItemCell";


		public ICellAction Action { get; set;}
		public ICellAction ShareAction{ get; set;}

		#region implemented abstract members of BasicModel

		protected override void OnPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if(_convoItem!=null){
				this.Title = _convoItem.PublicBlrtName;
				this.AuthorName = BlrtManager.DataManagers.Default.GetUser (_convoItem.CreatorId)?.DisplayName;
				this.Author = BlrtManager.DataManagers.Default.GetUser (_convoItem.CreatorId);
				this.CloudPathThumbnail = _convoItem.CloudThumbnailPaths[0];
				this.PathThumbnail = _convoItem.LocalThumbnailPath(0);
				this.BlrtCreatedAt = _convoItem.CreatedAt;
			}

			if(_item!=null){
				this.CreatedAt = _item.CreatedAt;
				this.ItemId = _item.ObjectId;
				this.Index = _item.Index;
			}
		}

		#endregion



		public string Title { get{ return _title;}						set{ SetProperty (ref _title, value);} }
		public string AuthorName { get{ return _authorName;}					set{ SetProperty (ref _authorName, value);} }
		public DateTime CreatedAt { get{ return _createdAt;}		set{ SetProperty (ref _createdAt, value);} }
		public DateTime BlrtCreatedAt { get{ return _blrtCreatedAt;}		set{ SetProperty (ref _blrtCreatedAt, value);} }
		public Uri CloudPathThumbnail { get { return _cloudImage;} set{ SetProperty (ref _cloudImage, value);} }
		public string PathThumbnail { 
			get { 
				return  _image;
			} 
			set{ SetProperty (ref _image, value);} 
		}
		public string ItemId { get{ return _itemId;}						set{ SetProperty (ref _itemId, value);} }
		public BlrtUser Author{ get{return _author;} 						set{SetProperty (ref _author, value);}}
		public float Index{ get{return _index;} 						set{SetProperty (ref _index, value);}}


		string _title;
		string _itemId;
		DateTime _createdAt;
		DateTime _blrtCreatedAt;

		Uri _cloudImage;
		string _image;
		string _authorName;
		BlrtUser _author;
		float _index;



		private OCListItem _item;
		private ConversationBlrtItem _convoItem;
		public void SetBinding(OCListItem oclistItem, ConversationBlrtItem blrt)
		{
			if (_item != null)
				RemoveNotifier (_item);

			_item = oclistItem;
			if (_item != null)
				AddNotifier (_item);

			if (_convoItem != null)
				RemoveNotifier (_convoItem);

			_convoItem = blrt;
			if (_convoItem != null)
				AddNotifier (_convoItem);
		}
	}
}

