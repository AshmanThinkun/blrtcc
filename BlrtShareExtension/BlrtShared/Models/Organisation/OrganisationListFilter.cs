﻿using System;
using System.Linq;
using BlrtData.ExecutionPipeline;
using System.Threading.Tasks;
using System.Collections.Generic;
using BlrtData.Views.Helpers;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using Parse;

namespace BlrtData.Models.Organisation
{
	public class OrganisationListFilter
	{
		public string OCListId {
			get;
			private set;
		}


		public bool HasMoreContent{ get; private set;}

		public OrganisationListFilter (string listId)
		{
			_reloadingSemaphore = new System.Threading.SemaphoreSlim (1);

			List = new ObservableCollection<TableModelSection<OrganisationListItemCellModel>> ();
			List.Add (new OrganisationListItemSectionModel());

			this.OCListId = listId;
			LoadOCList ();
			HasMoreContent = true;
			FetchOCList ();
		}

		#region top banner
		public event EventHandler PageModelChanged;

		OCList _oclist;

		public OCList PageModel{
			get{
				return _oclist;
			}set{
				if (value != _oclist) {
					_oclist = value;
					InvalidateUrl ();
				}
				PageModelChanged?.Invoke (this, EventArgs.Empty);
			}
		}

		#region link generation
		string _url_oclist_sms, _url_oclist_facebook, _url_oclist_email, _url_oclist_other = "";

		public string url_oclist_sms{get{return _url_oclist_sms;}}
		public string url_oclist_facebook{get{return _url_oclist_facebook;}}
		public string url_oclist_email{get{return _url_oclist_email;}}
		public string url_oclist_other{get{return _url_oclist_other;}}

		public void PrepareUrl(){  //we can hook this with the bubble on appearing method as well
			Task.Factory.StartNew (() => {
				GenerateLink ("oclist_sms");
				GenerateLink ("oclist_facebook");
				GenerateLink ("oclist_email");
				GenerateLink ("oclist_other");
			});
		}

		public void InvalidateUrl(){
			_url_oclist_sms = _url_oclist_facebook = _url_oclist_email = _url_oclist_other = "";
		}

		public async Task GenerateLink(string urlType){
			switch(urlType){
				case "oclist_sms":
					if (string.IsNullOrEmpty (_url_oclist_sms) || !_url_oclist_sms.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_oclist_sms = await BlrtUtil.GenerateYozioUrl (_oclist, urlType);
					}
					break;
				case "oclist_facebook":
					if (string.IsNullOrEmpty (_url_oclist_facebook) || !_url_oclist_facebook.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_oclist_facebook = await BlrtUtil.GenerateYozioUrl (_oclist, urlType);
					}
					break;
				case "oclist_email":
					if (string.IsNullOrEmpty (_url_oclist_email) || !_url_oclist_email.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_oclist_email = await BlrtUtil.GenerateYozioUrl (_oclist, urlType);
					}
					break;
				case "oclist_other":
					if (string.IsNullOrEmpty (_url_oclist_other) || !_url_oclist_other.StartsWith (BlrtSettings.YozioDictionary ["click_url"])) {
						_url_oclist_other = await BlrtUtil.GenerateYozioUrl (_oclist, urlType);
					}
					break;
			}
		}
		#endregion

		void LoadOCList()
		{
			PageModel = BlrtManager.DataManagers.Default.GetOCList (this.OCListId);
			if(PageModel!=null){
				//delete old data
				var items = GetRelations ().Where (i => i.LastRetrieved.ToUniversalTime() < PageModel.ContentValidAfter.ToUniversalTime()).Select(x=>x);
				if (items.Count () > 0) {
					BlrtManager.DataManagers.Default.Remove (items);
					ReloadList ();
				}
			}
		}

		async void FetchOCList(){
			var parseObj = new ParseObject ("OCList") {
				ObjectId = this.OCListId
			};

			ParseObject result = null;
			try{
				result = await parseObj.BetterFetchAsync (new CancellationTokenSource (15000).Token);
			}catch{}
			if(result!=null){
				BlrtManager.DataManagers.Default.AddParse (result);
			}
			LoadOCList ();
		}

		public async Task DownLoadFile(string fromUri, string toUri)
		{
			var downloader = BlrtData.IO.BlrtDownloader.Download (10, new Uri (fromUri), toUri);
			await downloader.WaitAsync ();
			if (File.Exists (toUri)) {
				PageModelChanged?.Invoke (this, EventArgs.Empty);
			}
		}

		#endregion


		public ObservableCollection<TableModelSection<OrganisationListItemCellModel>> List { 
			get; private set; 
		}


		Task _task;
		System.Threading.SemaphoreSlim _reloadingSemaphore;
		public async Task ReloadList ()
		{
			lock (_reloadingSemaphore) {
				if (_task != null)
					return;

				_task = _reloadingSemaphore.WaitAsync ();
			}

			await _task;
			_task = null;

			try {
				//load data
				var items = GetRelations();
				foreach(var item in items){
					if(!List[0].Any(x => x.ItemId ==  item.ObjectId)){
						//sorted add
						SortedAdd(item);
					}
				}


				Sort();
			} finally {
				_reloadingSemaphore.Release ();
			}
		}


		private void Sort()
		{
			for (var counter = 0; counter < List.Count - 1; counter++)
			{
				var index = counter + 1;
				while (index > 0)
				{
					if(List[0][index-1].Index < List[0][index].Index)
					{
						var temp = List[index - 1];
						List[index - 1] = List[index];
						List[index] = temp;
					}
					index--;
				}
			}
		}

		private void SortedAdd(OCListItem item)
		{
			//get convo Item

			var convoItem = BlrtData.BlrtManager.DataManagers.Default.GetConversationItem (item.BlrtId);
			if (convoItem == null)
				throw new Exception ("missing conversation item");

			//create model
			var model = new OrganisationListItemCellModel ();
			model.SetBinding (item, convoItem as BlrtData.Contents.ConversationBlrtItem);
			model.Action = new OpenBlrtAction (item.BlrtId);
			#if __IOS__
			model.ShareAction = new BlrtiOS.Views.Organisation.ShareAction (item.BlrtId);
			#else
			model.ShareAction = new BlrtApp.Views.Organisation.ShareAction (item.BlrtId);
			#endif

			//now add to the list
			for (int i = 0; i < List[0].Count; ++i) {
				if (List[0][i].Index < model.Index) {
					List[0].Insert (i, model);
					return;
				}	
			}

			List[0].Add (model);
		}


		class OpenBlrtAction: ICellAction
		{
			string _id;
			public OpenBlrtAction(string id)
			{
				_id = id;
			}

			#region ICellAction implementation
			public void Action (object sender)
			{
				new OpenIndependentBlrtPerformer (Executor.System (), BlrtEncode.EncodeId (_id)).RunAction();
			}
			#endregion
			
		}

		protected IEnumerable<OCListItem> GetRelations(){

			return BlrtData.BlrtManager.DataManagers.Default.GetOCListItems (this.OCListId);

		}


		public bool IgnoreRefreshError{ get; set;}
		public async System.Threading.Tasks.Task RefreshList (string[] ignoredIds = null)
		{
			try{
				var query = BlrtData.Query.OrganisationListItemQuery.RunQuery (this.OCListId, ignoredIds);
				await query.WaitAsync();
				HasMoreContent = (query as BlrtData.Query.OrganisationListItemQuery).HasContent;
			}catch(Exception queryException){
				if (!IgnoreRefreshError) {
					var result = await BlrtManager.App.ShowAlert (Executor.System (), new SimpleAlert (
						            BlrtUtil.PlatformHelper.Translate ("internet_error_title", "conversation_option"),
						            BlrtUtil.PlatformHelper.Translate ("internet_error_msg", "conversation_option"),
						            BlrtUtil.PlatformHelper.Translate ("Cancel"),
						            BlrtUtil.PlatformHelper.Translate ("Retry")
					            ));
					if (result.Success) {
						if ((!result.Result.Cancelled) && (0 == result.Result.Result)) {
							await RefreshList (ignoredIds);
							return;
						}
					}
				}
			}
			await ReloadList ();
		}

		public async System.Threading.Tasks.Task LoadNext()
		{
			var ig = List[0].Select (x => x.ItemId).ToArray ();
			await RefreshList (ig);
		}
	}
}

