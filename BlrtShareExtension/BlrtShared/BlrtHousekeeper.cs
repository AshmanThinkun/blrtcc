﻿using System.IO;
using System.Threading.Tasks;
using BlrtData.Contacts;
using Parse;

namespace BlrtData
{
	/// <summary>
	/// Clean dirty files, data maintenance, cleaning, etc.
	/// anything that should run in a background task but won't affect app running
	/// </summary>
	public static class BlrtHousekeeper
	{
		static Task _task = null;
		public static void Start()
		{
			if (_task == null || _task.Status == TaskStatus.Canceled || _task.Status == TaskStatus.Faulted || _task.Status == TaskStatus.RanToCompletion) {
				_task?.Dispose ();
				_task = CreateHousekeepingTask ();
			}
		}

		static Task CreateHousekeepingTask()
		{
			return Task.Run (async () => {
				await CompressAvatars ();
				_task = null;
			});
		}

		static async Task CompressAvatars ()
		{
			if (null == ParseUser.CurrentUser) {
				return;
			}
			try {
				var files = Directory.GetFiles (BlrtConstants.Directories.User.GetAvatarPath (""));
				foreach (var fileName in files) {
					var path = fileName.Trim ();
					if (!string.IsNullOrWhiteSpace (path)) {
						try {
							var size = BlrtUtil.PlatformHelper.GetImageSize (path);
							if (size.Width > Avatar.MaxSize || size.Height > Avatar.MaxSize) {
								var newFile = await BlrtUtil.PlatformHelper.CompressLargeImageToJpeg (path, Avatar.MaxSize);
								var newPath = newFile.Trim ();
								if (newPath != path) {
									try {
										File.Replace (newPath, path, null);
									} catch { }
									try {
										if (File.Exists (newPath)) {
											File.Delete (newPath);
										}
									} catch { }
								}
							}
						} catch {}
					}
				}
			} catch {}
		}
	}
}

