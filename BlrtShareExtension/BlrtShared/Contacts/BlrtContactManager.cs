using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.Net;

namespace BlrtData.Contacts
{
	public class BlrtContactManager
	{
		List<BlrtContact> _contactList;
		string _filepath;
		const string CONTACT_FILE = "contacts.dat";

		public bool ShouldLoadContacts { get; set; }

		public bool IgnoreChangedEvent { get; set; }

		public EventHandler Changed;

		public BlrtContact this [int key] {
			get {
				return _contactList [key];
			}
		}

		public int Count {
			get {
				return _contactList.Count;
			}
		}

		static BlrtContactManager _sharedInstanced;

		public static BlrtContactManager SharedInstanced {
			get {
				if (_sharedInstanced == null) {
					_sharedInstanced = new BlrtContactManager ();
				}
				return _sharedInstanced;
			}
		}

		public static void OpenContactManager ()
		{
			_sharedInstanced = new BlrtContactManager ();
			_sharedInstanced._filepath = BlrtConstants.Directories.Default.GetDataPath (CONTACT_FILE);
			_sharedInstanced.LoadFromFile ();
			_sharedInstanced.ShouldLoadContacts = true;
		}


		public static void CloseContactManager ()
		{
			_sharedInstanced = null;
		}


		public BlrtContactManager ()
		{
			_contactList = new List<BlrtContact> ();
		}


		public List<BlrtContact> ToList (bool includeAll = true)
		{ // if (false == includeAll) contact without email (which means we cannot use it) will not be includded
			if (includeAll) {
				return _contactList.ToList ();
			} else {
				return (
					from contact in _contactList
					where contact.InfoList.Any ((info) => (info.ContactType == BlrtContactInfoType.Email) && (!string.IsNullOrWhiteSpace (info.ContactValue)))
					select contact
				).ToList ();
			}
		}

		public static async Task FindBlrtUserFromParse (IEnumerable<BlrtContact> contacts = null)
		{
			if (!BlrtPersistant.Properties.ContainsKey ("disableSendingContacts")) {
				BlrtPersistant.Properties ["disableSendingContacts"] = false;
				BlrtPersistant.SaveProperties ();
			}
			if (((bool)BlrtPersistant.Properties ["disableSendingContacts"]) == true) {
				return;
			}

			if (contacts == null) {
				contacts = BlrtContactManager.SharedInstanced.ToList (true).Where (x => {
					var rtn = x.InfoList.Count (y => y.ContactType == BlrtContactInfoType.BlrtUserId) < 1
						&& x.NonBlrtUserUpdatedAt.AddDays (1) <= DateTime.Now;
					if (rtn) {
						x.NonBlrtUserUpdatedAt = DateTime.Now;
					}
					return rtn;
				});
			} else {
				foreach (var c in contacts) {
					c.NonBlrtUserUpdatedAt = DateTime.Now;
				}
			}
			BlrtContactManager.SharedInstanced.Save ();

			var details = contacts.SelectMany (x => x.InfoList);
			if (details.Count () < 1)
				return;
			try {
				List<IDictionary<string, string>> list = new List<IDictionary<string, string>> ();
				foreach (var detail in details) {
					list.Add (new Dictionary<string, string> (){
						{"value", detail.ContactValue},
						{"type", detail.ContactType == BlrtContactInfoType.FacebookId? "facebook":"email"}
					});
				}

				await BlrtData.Query.BlrtParseActions.FindUsersFromContacts (list, new System.Threading.CancellationTokenSource (15000).Token);
			} catch { }
		}

		private void LoadFromFile ()
		{
			if (string.IsNullOrEmpty (_filepath))
				return;

			try {
				if (File.Exists (_filepath)) {
					using (var reader = File.OpenRead (_filepath)) {
						_contactList = BlrtUtil.DeserializeFromStream<List<BlrtContact>> (reader);
						reader.Close ();
					}
				}
			} catch (Exception) {
				//data structure change may cause fail to load from caching data, so we need to catch the exception
				_contactList = new List<BlrtContact> ();
			}
		}


		public void Save ()
		{
			RemoveInvalid ();

			if (string.IsNullOrEmpty (_filepath))
				return;

			try {
				var stream = BlrtUtil.SerializeToStream (_contactList);
				BlrtUtil.StreamToFile (_filepath, stream);
			} catch (Exception e) {
				Console.Write (e.ToString ());
			}
		}

		public BlrtContact GetMatching (BlrtContact contact)
		{
			if (contact.InfoList.Count () < 1)
				return null;
			lock (_contactList) {
				if (_contactList != null && _contactList.Count > 0) {
					for (int i = 0; i < contact.InfoList.Count; i++) {
						var duplicateContact = _contactList.AsParallel ().FirstOrDefault (x => x.InfoList.Any (a => a.Equals (contact.InfoList [i])));
						if (duplicateContact != null)
							return duplicateContact;
					}
				}
			}
			return null;
		}

		public BlrtContact GetMatching (BlrtUser blrtUser)
		{
			if (string.IsNullOrEmpty (blrtUser.ObjectId)) {
				return null;
			}

			lock (_contactList) {
				if (_contactList != null && _contactList.Count > 0) {
					var duplicateContact = _contactList.AsParallel ().FirstOrDefault (x => x.InfoList.Any (a => a.ContactType == BlrtContactInfoType.BlrtUserId && a.ContactValue == blrtUser.ObjectId));
					if (duplicateContact != null) {
						return duplicateContact;
					}
				}
			}
			return null;
		}

		public void Add (IEnumerable<BlrtContact> contacts)
		{
			foreach (BlrtContact c in contacts)
				AddContact (c);

			if (!IgnoreChangedEvent)
				Changed?.Invoke (this, EventArgs.Empty);
		}


		public void Add (BlrtContact contact)
		{
			AddContact (contact);

			if (!IgnoreChangedEvent)
				Changed?.Invoke (this, EventArgs.Empty);
		}

		void AddContact (BlrtContact contact)
		{
			BlrtContact other = GetMatching (contact);
			if (other == null) {
				lock (_contactList) {
					_contactList.Add (contact);
				}
			} else {
				var currentState = other.State;
				currentState &= ~BlrtContactForm.Recent;

				if (string.IsNullOrWhiteSpace (other.Name) || currentState <= contact.State)
					other.Name = contact.Name;

				other.State = other.State | contact.State;

				if (contact.BlrtUserCreatedAt > other.BlrtUserCreatedAt) {
					other.BlrtUserCreatedAt = contact.BlrtUserCreatedAt;
				}

				if (contact.NonBlrtUserUpdatedAt > other.NonBlrtUserUpdatedAt) {
					other.NonBlrtUserUpdatedAt = contact.NonBlrtUserUpdatedAt;
				}

				foreach (var contactInfo in contact.InfoList) {
					var containsInfo = false;
					foreach (var otherInfo in other.InfoList) {
						if (otherInfo.Equals (contactInfo)) {
							containsInfo = true;
							break;
						}
					}
					if (!containsInfo) {
						other.Add (contactInfo);
					}
				}

				if (contact.State.HasFlag (BlrtContactForm.Contacts) && ShouldCopyAvatarFromLocalDevice (contact)) {
					other.DeleteAvatar ();
					other.Avatar = contact.Avatar;
				} else if (contact.State.HasFlag (BlrtContactForm.Blrt) && contact.Avatar != null) {
					if (other.Avatar == null || (other.Avatar.From < contact.Avatar.From || (other.Avatar.From == contact.Avatar.From && other.Avatar.Path != contact.Avatar.Path))) {
						other.DeleteAvatar ();
						other.Avatar = contact.Avatar;
					}
				}
			}
		}

		public void Remove (BlrtContact contact)
		{
			lock (_contactList) {
				_contactList.Remove (contact);
			}
		}

		public void RemoveInvalid ()
		{
			lock (_contactList) {
				_contactList.RemoveAll ((BlrtContact obj) => {
					return obj.Invalid;
				});
			}
		}

		public void RemoveForm (BlrtContactForm contactForm)
		{
			foreach (var contact in _contactList) {
				contact.State = (contact.State | contactForm) ^ contactForm;
			}
		}

		public void RemoveContactType (BlrtContactInfoType type)
		{
			foreach (var contact in _contactList) {

				for (int i = 0; i < contact.InfoList.Count; ++i) {
					if (contact.InfoList [i].ContactType == type) {
						contact.Remove (contact.InfoList [i]);
						--i;
					}
				}
			}
		}

		public bool ShouldCopyAvatarFromLocalDevice (BlrtContact contact)
		{
			var other = this.GetMatching (contact);
			if (other == null)
				return true;
			return other.ShouldCopyAvatarFromLocalDevice ();
		}


		public static async Task<string> LoadAvatarFromGravatar (string email)
		{
			if (string.IsNullOrEmpty (email))
				return "";
			email = email.Trim ().ToLower ();
			var hashed = GetMD5Hash (email);

			//http request to gravatar
			var requestUrl = string.Format ("http://www.gravatar.com/avatar/{0}?s={1}&d=404", hashed, Avatar.MaxSize.ToString ());

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (requestUrl);
			try {
				using (HttpWebResponse response = await request.GetResponseAsync () as HttpWebResponse) {
					if (response.StatusCode == HttpStatusCode.NotFound) {
						//not found avatar
						return "";
					} else if (response.StatusCode == HttpStatusCode.OK) {
						//save file
						string filename = BlrtRecentFileManager.GetUniqueFile (".jpg");

						var path = BlrtConstants.Directories.Default.GetRecentPath (filename);

						var stream = response.GetResponseStream ();
						BlrtUtil.StreamToFile (path, stream);
						return filename;
					}
				}
			} catch (Exception e) {
			}
			return "";
		}

		private static string GetMD5Hash (string input)
		{
			using (MD5 md5Hash = MD5.Create ()) {
				// Convert the input string to a byte array and compute the hash. 
				byte [] data = md5Hash.ComputeHash (Encoding.UTF8.GetBytes (input));

				// Create a new Stringbuilder to collect the bytes 
				// and create a string.
				StringBuilder sBuilder = new StringBuilder ();

				// Loop through each byte of the hashed data  
				// and format each one as a hexadecimal string. 
				for (int i = 0; i < data.Length; i++) {
					sBuilder.Append (data [i].ToString ("x2"));
				}

				// Return the hexadecimal string. 
				return sBuilder.ToString ();
			}
		}
	}
}


