using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.IO;
using System.IO;
using BlrtData.Models.ConversationScreen;

namespace BlrtData.Contacts
{

	public enum BlrtContactInfoType
	{
		Email,
		BlrtUserId,
		FacebookId,
		PhoneNumber,
		Invalid
	}
	[Flags]
	public enum BlrtContactForm
	{
		None = 0x000,
		Recent = 0x001,
		Contacts = 0x002,
		Facebook = 0x004,
		Blrt = 0x008,
	}

	[Serializable]
	public class Avatar
	{
		public const int MaxSize = 480;
		public string FileName { get; set; }
		public string CloudPath { get; set; }

		[field: NonSerialized]
		public string Path {
			get {
				if (string.IsNullOrEmpty (FileName))
					return "";
				return BlrtConstants.Directories.Default.GetAvatarPath (FileName);
			}
		}
		public enum AvatarFrom
		{
			Default = 0,
			Gravatar = 1,
			LocalContact = 2,
			UserSpecified = 3
		}

		public AvatarFrom From { get; set; }
		public DateTime LastUpdatedAt { get; set; }
	}


	[Serializable]
	public class BlrtContact : IComparable<BlrtContact>
	{
		private DateTime _lastUsed;
		private List<BlrtContactInfo> _infoList;

		public bool Hide { get; set; }

		private string _name;
		private BlrtContactForm _state;

		public DateTime BlrtUserCreatedAt { get; set; }
		public DateTime NonBlrtUserUpdatedAt { get; set; }

		public bool IsConnectedToBlrt { get; private set; }

		public BlrtContactForm State {
			get { return _state; }
			set { _state = value; }
		}
		public string Name {
			get { return _name; }
			set { _name = value; }
		}

		public Avatar Avatar { get; set; }

		[field: NonSerialized]
		public event EventHandler AvatarChanged;


		public DateTime LastUsed {
			get { return _lastUsed; }
			set {
				if (value > _lastUsed) {

					_lastUsed = value;

					if (value.AddDays (7) > DateTime.Now) {
						State = State | BlrtContactForm.Recent;
					}
				}
			}
		}

		public string UsableName {
			get {

				if (!string.IsNullOrWhiteSpace (Name))
					return Name.Trim ();

				for (int i = 0; i < _infoList.Count; ++i) {

					if (_infoList [i].IsValid && _infoList [i].ContactType != BlrtContactInfoType.PhoneNumber)
						return _infoList [i].ToString ();
				}

				if (_infoList.Any (x => x.ContactType == BlrtContactInfoType.PhoneNumber)) {
					return BlrtUtil.PlatformHelper.Translate ("Anonymous");
				}

				return "";
			}
		}


		public List<BlrtContactInfo> InfoList {
			get {
				return _infoList;
			}
			set {
				_infoList = value;
			}
		}

		public bool Invalid {
			get {
				if (State == BlrtContactForm.None)
					return true;

				if (InfoList == null || _infoList.Count == 0)
					return true;

				for (int i = 0; i < _infoList.Count; ++i) {

					if (_infoList [i].IsValid && _infoList [i].Implemented)
						return false;
				}

				return true;
			}
		}

		public BlrtContact ()
		{
			_infoList = new List<BlrtContactInfo> ();
		}

		public void Add (BlrtContactInfo info)
		{
			if (!info.IsValid)
				return;

			var tmpInfo = _infoList.Find (x => x.Equals (info));
			if (tmpInfo != null) {
				tmpInfo.IsLinkToBlrtUser = info.IsLinkToBlrtUser;
			} else {
				tmpInfo = info;
				_infoList.Add (tmpInfo);
			}

			if (!IsConnectedToBlrt) {
				IsConnectedToBlrt = tmpInfo.ContactType == BlrtContactInfoType.BlrtUserId;
			}
		}

		public void Remove (BlrtContactInfo info)
		{
			if (!_infoList.Contains (info))
				return;

			_infoList.Remove (info);
		}



		public string ContactInfoToString ()
		{
			return string.Join (", ", _infoList.Where ((arg) => { return arg.Implemented; }));
		}


		public static string ContactInfoTypeToString (BlrtContactInfoType type)
		{
			switch (type) {
			case BlrtContactInfoType.BlrtUserId:
				return "Blrt";
			case BlrtContactInfoType.Email:
				return "Email";
			case BlrtContactInfoType.FacebookId:
				return "Facebook";
			case BlrtContactInfoType.PhoneNumber:
				return "Phone Number";
			default:
				return "";
			}
		}

		public int CompareTo (BlrtContact other)
		{
			return UsableName.CompareTo (other.UsableName);
		}

		public static BlrtContact FromBlrtUser (BlrtUser blrtUser)
		{
			var c = new BlrtContact () {
				Name = blrtUser.DisplayName,
				State = BlrtContactForm.Blrt,
			};

			//if (!string.IsNullOrEmpty (blrtUser.AvatarCloudPath)) {
			c.Avatar = new Avatar () {
				From = Avatar.AvatarFrom.UserSpecified,
				LastUpdatedAt = blrtUser.TimeStamp,
				FileName = blrtUser.AvatarName,
				CloudPath = blrtUser.AvatarCloudPath
			};
			//}

			c.Add (new BlrtContactInfo (
				BlrtContactInfoType.Email,
				blrtUser.Email
			) {IsLinkToBlrtUser = true} );

			c.Add (new BlrtContactInfo (
				BlrtContactInfoType.BlrtUserId,
				blrtUser.ObjectId
			));

			if (!string.IsNullOrEmpty (blrtUser.FacebookId)) {
				c.Add (new BlrtContactInfo (
					BlrtContactInfoType.FacebookId,
					blrtUser.FacebookId
				));
			}
			c.BlrtUserCreatedAt = blrtUser.BlrtUserCreatedAt;

			foreach (var a in blrtUser.InfoList) {
				c.Add (a);
			}

			if (c.ShouldDownloadAvatarFromParse ()) {
				c.DownloadFromParse ();
			}

			return c;
		}

		// download the avatar; if the avatar is too large, resize it
		public class AvatarDownloader
		{
			BlrtDownloader _d;
			string _targetPath;
			string _tempPath;
			private AvatarDownloader ()
			{
			}

			static string GetTempFilePath (string path)
			{
				return Path.Combine (Path.GetDirectoryName (path), Path.GetFileNameWithoutExtension (path) + "_tmp" + (Path.GetExtension (path)));
			}

			public static AvatarDownloader Download (int priority, Uri cloudPath, string localPath)
			{
				var tempPath = GetTempFilePath (localPath);
				return new AvatarDownloader () {
					_targetPath = localPath,
					_tempPath = tempPath,
					_d = BlrtDownloader.Download (priority, cloudPath, tempPath)
				};
			}

			public async Task WaitAsync ()
			{
				await _d.WaitAsync ();
				var newPath = await BlrtUtil.PlatformHelper.CompressLargeImageToJpeg (_tempPath, Avatar.MaxSize);
				try {
					if (File.Exists (_targetPath)) {
						File.Replace (newPath, _targetPath, null);
					} else {
						File.Move (newPath, _targetPath);
					}
				} catch { }

				try {
					if (File.Exists (_tempPath)) {
						File.Delete (_tempPath);
					}
				} catch { }
			}
		}

		public async Task DownloadFromParse ()
		{
			if (_busy)
				return;
			_busy = true;
			if (!string.IsNullOrEmpty (this.Avatar.CloudPath)) {
				var d = AvatarDownloader.Download (9, new Uri (this.Avatar.CloudPath), this.Avatar.Path);
				await d.WaitAsync ();
				if (System.IO.File.Exists (this.Avatar.Path)) {
					AvatarChanged?.Invoke (this, EventArgs.Empty);
					_busy = false;
					return;
				}
			}
			_busy = false;
			return;
		}

		public bool ShouldDownloadAvatarFromParse ()
		{
			if (this.Avatar != null &&
				this.Avatar.From == Avatar.AvatarFrom.UserSpecified &&
				!string.IsNullOrEmpty (this.Avatar.CloudPath) &&
				!System.IO.File.Exists (this.Avatar.Path)) {
				return true;
			}
			return false;
		}

		public bool ShouldRefreshFromGravatar ()
		{
			//avatar has higher priority or avatar is up to date do not need to refresh
			if (this.Avatar == null
					|| this.Avatar.From < Avatar.AvatarFrom.Gravatar
					|| (this.Avatar.From == Avatar.AvatarFrom.Gravatar && this.Avatar.LastUpdatedAt.AddDays (7) < DateTime.Now)) {
				return true;
			}
			return false;
		}

		public bool ShouldCopyAvatarFromLocalDevice ()
		{
			if (this.Avatar == null
				|| this.Avatar.From < Avatar.AvatarFrom.LocalContact
				|| (this.Avatar.From == Avatar.AvatarFrom.LocalContact && this.Avatar.LastUpdatedAt.AddDays (7) < DateTime.Now)) {
				return true;
			}
			return false;
		}

		public void DeleteAvatar ()
		{
			if (Avatar != null && System.IO.File.Exists (Avatar.Path)) {
				try {
					System.IO.File.Delete (Avatar.Path);
					this.Avatar = null;
				} catch { }
			}
		}

		[NonSerialized]
		bool _busy;

		public async Task RefreshFromGravatar ()
		{
			if (_busy)
				return;
			_busy = true;
			var emails = this.InfoList.Where (x => { return (x.ContactType != BlrtContactInfoType.FacebookId && x.ContactType != BlrtContactInfoType.BlrtUserId); });

			if (emails == null || emails.Count () == 0) {
				_busy = false;
				return;
			} else {
				var ts = new List<Task<string>> ();
				foreach (var e in emails) {
					var t = BlrtContactManager.LoadAvatarFromGravatar (e.ContactValue);
					ts.Add (t);
				}

				await Task.WhenAll (ts.ToArray ());

				try {
					var fileName = ts.FirstOrDefault (re => !string.IsNullOrEmpty (re.Result)).Result;
					if (!string.IsNullOrEmpty (fileName)) {
						var newFileName = BlrtUserHelper.CreateUniqueUserAvatarName ();
						System.IO.File.Copy (BlrtConstants.Directories.Default.GetRecentPath (fileName), BlrtConstants.Directories.Default.GetAvatarPath (newFileName));
						this.DeleteAvatar ();
						this.Avatar = new Avatar () {
							FileName = newFileName,
							From = Avatar.AvatarFrom.Gravatar,
							LastUpdatedAt = DateTime.Now
						};
						AvatarChanged?.Invoke (this, EventArgs.Empty);
						_busy = false;
						return;
					}
				} catch { }
			}
			this.DeleteAvatar ();
			this.Avatar = new Avatar () {
				From = Avatar.AvatarFrom.Gravatar,
				LastUpdatedAt = DateTime.Now
			};
			AvatarChanged?.Invoke (this, EventArgs.Empty);
			_busy = false;
			return;
		}
	}


	[Serializable]
	public class BlrtContactInfo : IEquatable<BlrtContactInfo>
	{

		private string _contactValue;
		private BlrtContactInfoType _contactType;

		public string ContactValue { get { return _contactValue; } }
		public BlrtContactInfoType ContactType { get { return _contactType; } }
		public bool IsLinkToBlrtUser { get; set; }

		public bool Implemented {
			get {

				switch (ContactType) {
				case BlrtContactInfoType.Email:
				case BlrtContactInfoType.FacebookId:
				case BlrtContactInfoType.PhoneNumber:
					return true;
				}
				return false;

			}
		}

		public string ParseContactType {
			get {
				switch (ContactType) {
				case BlrtContactInfoType.Email:
					return "email";
				case BlrtContactInfoType.FacebookId:
					return "facebook";
				case BlrtContactInfoType.PhoneNumber:
					return "phoneNumber";
				}

				return "BADF00D";
			}
		}

		public bool IsValid {
			get {
				var tmp = 0L;


				switch (ContactType) {
				case BlrtContactInfoType.Email:
					return !string.IsNullOrWhiteSpace (ContactValue) && BlrtUtil.IsValidEmail (ContactValue);
				case BlrtContactInfoType.BlrtUserId:
					return !string.IsNullOrWhiteSpace (ContactValue);
				case BlrtContactInfoType.FacebookId:
					return !string.IsNullOrWhiteSpace (ContactValue) && long.TryParse (ContactValue, out tmp);
				case BlrtContactInfoType.PhoneNumber:
					return BlrtUtil.IsValidPhoneNumber (ContactValue);
				}

				return false;
			}
		}

		public int GetTypeWeight ()
		{

			switch (ContactType) {
			case BlrtContactInfoType.Email:
				return 5;
			case BlrtContactInfoType.BlrtUserId:
				return 10;
			case BlrtContactInfoType.FacebookId:
				return 1;
			case BlrtContactInfoType.PhoneNumber:
				return 3;
			}

			return -2;
		}

		public BlrtContactInfo (BlrtContactInfoType type, string value)
		{
			_contactType = type;

			switch (type) {
			case BlrtContactInfoType.Email:
				_contactValue = value.ToLower ();
				break;
			case BlrtContactInfoType.PhoneNumber:
				_contactValue = BlrtUtil.ToPhoneNumberE164 (value);
				break;
			case BlrtContactInfoType.BlrtUserId:
			case BlrtContactInfoType.FacebookId:
			default:
				_contactValue = value;
				break;
			}
		}

		public bool Equals (BlrtContactInfo other)
		{
			if (other == null || other.ContactType != ContactType)
				return false;

			return string.CompareOrdinal (ContactValue, other.ContactValue) == 0;
		}

		public string ValueToString ()
		{
			switch (ContactType) {
			case BlrtContactInfoType.Email:
				return ContactValue;
			case BlrtContactInfoType.PhoneNumber:
				return ContactValue;
			}

			return string.Empty;
		}

		public string TypeToString ()
		{
			return BlrtContact.ContactInfoTypeToString (ContactType);
		}

		public override string ToString ()
		{
			switch (ContactType) {
			case BlrtContactInfoType.Email:
			case BlrtContactInfoType.PhoneNumber:
				return ContactValue;
			default:
				return BlrtContact.ContactInfoTypeToString (ContactType);
			}
		}

		public string ToString (BlrtContact contact)
		{
			switch (ContactType) {
			case BlrtContactInfoType.Email:
			case BlrtContactInfoType.PhoneNumber:
				return ContactValue;
			default:
				return string.Format ("{0}", contact.UsableName, ToString ());
			}
		}

		/// <summary>
		/// Returns the string representation for contact type.
		/// </summary>
		/// <returns>The type to string.</returns>
		internal string ContactTypeToString ()
		{
			return BlrtContact.ContactInfoTypeToString (ContactType);
		}
	}

	public class ConversationNonExistingContact : BlrtUtil.IConversationContact, IEquatable<ConversationNonExistingContact>, IComparable<ConversationNonExistingContact>
	{
		public BlrtContactInfo SelectedInfo { get; private set; }
		public BlrtContact Contact { get; private set; }

		public bool IsRemovable {
			get {
				return SendingStatus == ContactState.NotSend || SendingStatus == ContactState.FailedToSend;
			}
		}

		public string Name {
			get {
				return Contact.UsableName;
			}
		}

		public string Detail {
			get {
				return SelectedInfo.ContactType == BlrtContactInfoType.FacebookId ? "Facebook" : SelectedInfo.ContactValue;
			}
		}

		public string Avatar {
			get {
				return Contact.Avatar != null ? Contact.Avatar.Path : string.Empty;
			}
		}

		public object Model {
			get {
				return Contact;
			}
		}

		 ConversationRelationStatus.RoleEnum BlrtUtil.IConversationContact.RelationType {
			get {
				return ConversationRelationStatus.RoleEnum.None;
			}
		}

		bool BlrtUtil.IConversationContact.IsValid {
			get {
				return IsValid ();
			}
		}

		public ContactState SendingStatus { get; set; }

		public string Initials {
			get {
				return BlrtUtil.GetInitials (Name);
			}
		}

		public bool AccountRemoved {
			get {
				return false;
			}
		}

		public ConversationNonExistingContact (BlrtContactInfo selectedInfo, BlrtContact contact, ContactState sendingState = ContactState.NotSend)
		{
			SelectedInfo = selectedInfo;
			Contact = contact;
			SendingStatus = sendingState;
		}


		public override string ToString ()
		{
			return SelectedInfo.ToString (Contact);
		}

		public bool IsValid ()
		{
			return SelectedInfo.IsValid;
		}



		public bool Equals (ConversationNonExistingContact other)
		{
			return other != null && other.SelectedInfo.Equals (SelectedInfo);
		}


		public int CompareTo (ConversationNonExistingContact other)
		{
			return Contact.CompareTo (other.Contact);
		}


		//helper function for create contact
		public static ConversationNonExistingContact FromString (string str)
		{
			var type = BlrtContactInfoType.Email;
			if (BlrtUtil.IsValidPhoneNumber (str)) {
				type = BlrtContactInfoType.PhoneNumber;
			}

			var contactInfo = new BlrtContactInfo (type, str);
			var newcontact = new BlrtContact () {
				Name = null,
			};
			newcontact.Add (contactInfo);
			return (new ConversationNonExistingContact (contactInfo, newcontact));
		}

		public bool Equals (BlrtUtil.IConversationContact contact)
		{
			if (!(contact is ConversationNonExistingContact)) {
					return false;
			}

			return (contact as ConversationNonExistingContact).Equals (this);
 		}
	}
}

