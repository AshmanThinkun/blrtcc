﻿using System;

namespace BlrtShared
{
	public interface IFileWatcher : IDisposable
	{
		bool FileExists { get; }
		event EventHandler<bool> FileExistanceChanged;
	}

	public interface IFileWatcherPlatformHelper
	{
		IFileWatcher CreateWatcher (string path);
	}
}

