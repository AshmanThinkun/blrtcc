using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlrtData.ExecutionPipeline;

namespace BlrtData.IO
{
	public class BlrtDownloaderList : IProgress
	{
		public struct BlrtDownloaderListStatus{
			public float Progress 			{ get { return current * 1f / estimatedTotal;} }
			public long total;
			public long current;
			public long estimatedTotal;
			public int count;
			public int finished;
			public int failed;
		}



		public BlrtDownloader[] Array{ get; private set;}

		public event EventHandler OnProgress;
		public event EventHandler OnFinished;
		public event EventHandler OnFailed;

		public bool IsFailed 			{ get; private set;}
		public bool IsFinished 			{ get; private set;}

		public long Total 				{ get; private set;}
		public long Current 			{ get; private set;}
		public float Progress 			{ get { return Current * 1f / Total;} }

		public int Count 				{ get { return Array.Length;} }

		TaskCompletionSource<object> _task;

		public BlrtDownloaderList (IEnumerable<BlrtDownloader> array) : this( array.ToArray() ) { }
		public BlrtDownloaderList (params BlrtDownloader[] array)
		{
			IsFailed = false;
			IsFinished = true;
			Array = (
				from t in array 
				where null != t
				select t
			).ToArray ();

			_task = new TaskCompletionSource<object> ();

			for(int i = 0; i < Array.Length; ++i) {
				var d = Array[i];
				if (null == d) {
					continue;
				}
				d.OnProgress += HadProgress;
				d.OnDownloadFinished += Finished;
				d.OnDownloadFailed += Failed;

				if (!d.IsFinished)
					IsFinished = false;
				if (d.Failed)
					IsFailed = true;
			}

			Recalulate ();
		}



		public static BlrtDownloaderListStatus GetProgress(BlrtDownloader[] downloaders)
		{
			var status = new BlrtDownloaderListStatus () {
				count = downloaders.Length		
			};

			var notStarted = 0;

			for(int i = 0; i < downloaders.Length; ++i) {

				var d = downloaders[i];

				if (d.Failed) {
					++status.failed;
				}
				if (d.IsFinished) {
					++status.finished;
				}
				if(d.TotalSize == 0)
					notStarted++;

				status.total 	+= d.TotalSize;
				status.current	+= d.ReceivedSize;
			}

			//estimated
			if (notStarted > 0 && status.count != notStarted) {	
				status.estimatedTotal = (status.total * status.count / (status.count - notStarted));
			} else {
				status.estimatedTotal = status.total;
			}

			if (status.total == 0)
				status.total = status.estimatedTotal = 1;

			return status;
		}


		private void Recalulate ()
		{
			var status = GetProgress (Array);

			Total = status.estimatedTotal;
			Current = status.current;

			if (status.failed > 0) {
				IsFailed = true;
				if (OnFailed != null)
					OnFailed (this, EventArgs.Empty);
			}
		}

		public void Retry()
		{
			if (!IsFailed)
				return;

			IsFailed = false;

			for (int i = 0; i < Array.Length; ++i) {
				if (Array[i].Failed) {
					Array [i] = BlrtDownloader.Download (Array [i].Priority, Array [i].Url, Array [i].ToFile);
					Array [i].OnProgress 			+= HadProgress;
					Array [i].OnDownloadFinished 	+= Finished;
					Array [i].OnDownloadFailed 		+= Failed;
				}
			}
		}

		private void HadProgress(object sender, EventArgs args)
		{
			if (IsFailed) return;

			Recalulate ();

			if (OnProgress != null)
				OnProgress (this, args);
		}

		private void Finished(object sender, EventArgs args)
		{
			if (IsFailed) return;

			for(int i = 0; i < Array.Length; ++i) {
				var d = Array[i];

				if (!d.IsFinished) {

					HadProgress (sender, args);
					return;
				}
			}

			SetResult(true);

			if (OnFinished != null)
				OnFinished (this, args);
		}

		private void Failed(object sender, EventArgs args)
		{
			if (IsFailed) return;

			SetResult(false);
			Recalulate ();

			if (OnFailed != null)
				OnFailed (this, args);
		}



		private void SetResult (bool success){
		
			if (IsFailed || IsFinished)
				return;

			if (success)	IsFinished = true;
			else			IsFailed = true;

			_task.SetResult (this);
		}

		public Task WaitAsync ()
		{
			return _task.Task;
		}
	}
}

