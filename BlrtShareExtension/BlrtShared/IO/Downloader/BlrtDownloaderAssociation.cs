using System;
using System.Linq;
using System.Collections.Generic;

namespace BlrtData.IO
{
	public static class BlrtDownloaderAssociation
	{
		private const int CLEAN_HOUSE = 128;

		static int _counterCleanHouse = 0;
		static List<Associate> _store;

		static BlrtDownloaderAssociation()
		{
			if (_store == null)
				_store = new List<Associate> ();
		}


		public static void CreateAssociation(object obj, params BlrtDownloader[] downloaders)
		{
			for (int i = 0; i < downloaders.Length; ++i)
				CreateAssociation (obj, downloaders[i]);
		}

		public static void CreateAssociation(object obj, IEnumerable<BlrtDownloader> downloaders)
		{
			foreach (var d in downloaders)
				CreateAssociation (obj, d);
		}

		public static void CreateAssociation(object obj, BlrtDownloaderList downloaderList)
		{
			foreach (var d in downloaderList.Array)
				CreateAssociation (obj, d);
		}

		public static void CreateAssociation(object obj, BlrtDownloader downloader)
		{
			CleanHouse ();

			if (obj == null || downloader == null)
				return;

			if (_store == null)
				_store = new List<Associate> ();
			var ass = new Associate (obj, downloader);
			if(!_store.Contains(ass))
				_store.Add(ass);
		}


		public static bool HasAssociations(object obj)
		{
			CleanHouse ();

			var results =
				from a in _store
					where (a.obj.Equals(obj) && !a.Cullable)
				select a;
			return (results != null && results.Any ());
		}

		public static IEnumerable<BlrtDownloader> GetAssociations(object obj)
		{
			CleanHouse ();

			return from a in _store
					where (a.obj != null && a.obj.Equals(obj) && !a.Cullable)
				select a.downloader;
		}



		public static BlrtDownloaderList GetAssociationsAsList(object obj)
		{
			return new BlrtDownloaderList( GetAssociations(obj) );
		}

		private static void CleanHouse()
		{
			++_counterCleanHouse;
			if (_counterCleanHouse < CLEAN_HOUSE)
				return;

			_counterCleanHouse = 0;

			_store.RemoveAll ((Associate obj) => {return obj.Cullable;});
		}

		private class Associate : IEquatable<Associate>
		{
			public object obj;
			public BlrtDownloader downloader;

			public bool Cullable {get { return ((downloader.IsFinished || downloader.Failed) && downloader.EndedAt.Value.AddSeconds(1) < DateTime.Now) 
					|| (!downloader.Exists && !(downloader.IsFinished || downloader.Failed));  } }


			public Associate (object o, BlrtDownloader d){
				obj = o;
				downloader = d;
			}

			public bool Equals (Associate other)
			{
				return obj == other.obj && downloader == other.downloader;
			}

		}
	}
}

