using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;


namespace BlrtData.IO
{
	/// <summary>
	/// An Event Based downloading class.  Should be able to handle everything i can throw at it.  
	/// </summary>
	public class BlrtDownloader : IComparable<BlrtDownloader>{


		private object _lockObject;
		TaskCompletionSource<object> _task;




		private static BlrtDownloaderSecret _downloader;

		public static void TerminateAllDownloads () {
			if (_downloader == null)
				return;

			_downloader.TerminateAllDownloads ();
		}

		public Uri Url {
			get;
			protected set;
		}

		public string ToFile{
			get;
			protected set;
		}
		public DateTime? StartedAt {
			get;
			private set;
		}

		public DateTime? EndedAt {
			get;
			private set;
		}

		public bool Started {
			get{
				return StartedAt != null;
			}
			protected set{
				if (StartedAt == null && value)
					StartedAt = DateTime.Now;
			}
		}
		Exception _exception;
		bool _isFinished;



		public bool Failed {
			get { return _exception != null; }
		}
		public Exception Exception {
			get{
				return _exception;
			}
			protected set{
				if (_exception == null && !_isFinished && value != _exception) {
					_exception = value;
					EndedAt = DateTime.Now;
				}
			}
		}
		public bool IsFinished {
			get{
				return _isFinished;
			}
			protected set{
				if (_exception == null && !_isFinished && value != _isFinished) {
					_isFinished = value;
					EndedAt = DateTime.Now;
				}
			}
		}



		public bool IsDownloading{
			get{
				Create ();
				return _downloader.IsDownloading (this);
			}
		}

		public bool Exists{
			get{
				Create ();
				return _downloader.Exists (this);
			}
		}

		public bool Cancelled {
			get;
			protected set;
		}

		public int Priority {
			get;
			protected set;
		}
		private DateTime _lastRequested;

		public long TotalSize {
			get;
			protected set;
		}

		public bool HasFileSize {
			get;
			protected set;
		}

		public long ReceivedSize {
			get;
			protected set;
		}

		public float Progress {
			get{ if(TotalSize > 0) return ReceivedSize * 1f / TotalSize; return 0; }
		}
		public float ProgressPercentage {
			get{ return Progress * 100; }
		}
		/// <summary>
		/// Gets or sets the event that is fired when the download starts.  Warning, event may be fired before object is returned
		/// </summary>
		/// <value>The on download started.</value>
		public event EventHandler OnDownloadStarted;

		/// <summary>
		/// Fires when the download has progress
		/// </summary>
		/// <value>The on progress.</value>
		public event EventHandler OnProgress;
		/// <summary>
		/// Fired when the download is successful
		/// </summary>
		/// <value>The on download finished.</value>
		public event EventHandler OnDownloadFinished;

		public event UnhandledExceptionEventHandler OnDownloadFailed;


		public Task WaitAsync () {

			return _task.Task;
		}




		private BlrtDownloader(int priority, Uri url, string toFile)
		{
			this.Priority = priority;
			this.Url = url;
			this.ToFile = toFile;
			this.IsFinished = false;

			if (url == null)
				this.Exception = new Exception ("Requires url to download.");


			_task = new TaskCompletionSource<object> ();

			_lockObject = new object ();

			_lastRequested = DateTime.Now;
		}

		public int CompareTo (BlrtDownloader obj)
		{
			var i = -(Priority.CompareTo (obj.Priority));

			if (i == 0)
				i = _lastRequested.CompareTo (obj._lastRequested);

			return i;
		}

		public void Cancel ()
		{
			if (Cancelled || IsFinished)
				return;

			Create ();
			Cancelled = true;

			_downloader.Cancel (this);
		}

		private static void Create()
		{
			if(_downloader == null)
				_downloader = new BlrtDownloaderSecret();
		}

		/// <summary>
		/// Try to start downloading the url to the given path, will create threads to do so async
		/// </summary>
		/// <param name="priority">Priority, the greater the value  the more it needs to download.</param>
		/// <param name="uri">URI.</param>
		/// <param name="pathToSave">Path to save.</param>
		public static BlrtDownloader Download(int priority, Uri uri, string pathToSave)
		{
			if (uri == null)
				throw new ArgumentNullException ();


			Create ();
			return _downloader.QueueDownload (priority, uri, pathToSave);
		}


		public static int GetMeterBytesDownloaded ()
		{
			Create ();
			return 0;//_downloader._meterBytesDownloaded;
		}

		public static bool IsDownloadingToFile (string localMediaPath)
		{
			Create ();
			return _downloader.IsDownloading (localMediaPath);
		}

		private class BlrtDownloaderSecret
		{
			private readonly string CacheLocation = Path.Combine (
				Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), 
				BlrtConstants.Directories.Default.GetDownloaderPath (null)) + "/";

			private long _meterBytesDownloaded;
			private List<BlrtDownloader> _downloadList;

			const int MaxWebClientCount = 8;

			private GzipWebClient[] 	_webClients;
			private BlrtDownloader[] 	_activeDownloaders;

			private class WebClientInfo {
				public bool IsBusy = false;
				public int CacheIndex = -1;
			}

			static object CurrentCacheLock = new object();
			static int CurrentCacheIndex = 0;
			const int MaxCacheIndex = 1000;

			private WebClientInfo[] _webClientInfo;

			bool _isFileCheckerrunning;


			public void TerminateAllDownloads () {
				foreach (var download in _activeDownloaders)
					Cancel (download);
			}


			public BlrtDownloaderSecret ()
			{
				_webClients = new GzipWebClient[MaxWebClientCount];
				_webClientInfo = new WebClientInfo[MaxWebClientCount];

				_activeDownloaders = new BlrtDownloader[_webClients.Length];

				_downloadList = new List<BlrtDownloader> ();

				_isFileCheckerrunning = false;
				_meterBytesDownloaded = 0;
			}

			public bool Exists (BlrtDownloader downloader)
			{
				return _downloadList.Contains (downloader);
			}

			/// <summary>
			/// Resorts the list of object
			/// </summary>
			public void Resort()
			{
				lock (_downloadList) {
					_downloadList.Sort ();
				}
			}

			/// <summary>
			/// Finds an item in the queue and ensures is greater than or eqaul to the current priority
			/// </summary>
			/// <returns>The download.</returns>
			/// <param name="priority">Priority.</param>
			/// <param name="uri">URI.</param>
			/// <param name="pathToSave">Path to save.</param>
			public BlrtDownloader QueueDownload(int priority, Uri uri, string pathToSave)
			{
				if (string.IsNullOrWhiteSpace (pathToSave)) {
					throw new ArgumentNullException ("pathToSave");
				}
				BlrtDownloader output = null;

				lock (_downloadList) {
					foreach (var q in _downloadList) {
						if (q != null && q.Url.Equals(uri) && pathToSave == q.ToFile) {

							if (q.Priority > priority) {
								q.Priority = priority;
							}

							q._lastRequested = DateTime.Now;
							output = q;
							break;
						}
					}
				}

				if (output == null) {
					output = new BlrtDownloader (priority, uri, pathToSave);
					lock (_downloadList) {
						_downloadList.Add (output);
					}
					NotifyFileDownloadStarted (pathToSave);
				}

				FindAndDownload ();
				return output;
			}

			/// <summary>
			/// Returns true if there is something downloading to this file
			/// </summary>
			public bool IsDownloading (string file)
			{
				lock (_downloadList) {
					foreach (var q in _downloadList) {
						if (file == q.ToFile) {
							return true;
						}
					}
				}
				return false;
			}

			/// <summary>
			/// Returns true if there is something downloading to this file
			/// </summary>
			public bool IsDownloading(BlrtDownloader downloading)
			{
				for (int i = 0; i < _activeDownloaders.Length; i++) {
					if (_activeDownloaders[i] == downloading) {
						return true;
					}
				}
				return false;
			}

			/// <summary>
			/// Search for empty thread and items that need to be downloaded, and downloads them
			/// </summary>
			public void FindAndDownload()
			{
				Resort ();

				int j = 0;


				lock (_downloadList) {
					for (int i = 0; i < _webClients.Length && j < _downloadList.Count; i++) {

						if (_webClients [i] == null) {

							var w = new GzipWebClient ();
							w.DownloadFileCompleted += OnFinished;
							w.DownloadProgressChanged += OnProgress;
							_webClientInfo [i] = new WebClientInfo () {
								IsBusy = false,
								CacheIndex = -1
							};
							_webClients [i] = w;
						}
						lock (_webClients [i]) {

							if (!(_webClientInfo [i].IsBusy || _webClients [i].IsBusy)) {

								for (; j < _downloadList.Count; j++) {

									var obj = _downloadList [j];


									lock (obj._lockObject) {
										if (!obj.IsDownloading) {

											if (obj.Cancelled || obj.Failed) {
												_downloadList.Remove (obj);
												j--;
											} else {

												_activeDownloaders [i] = obj;
												DownloadStarted (_activeDownloaders [i]);

												BlrtUtil.PushDownloadStack ();

												LLogger.WriteEvent ("Downloader", "Starting");
												try {
													_webClientInfo [i].IsBusy = true;
													lock (CurrentCacheLock) {
														bool isIndexValid = true;
														do {
															for (int wi = 0; wi < _webClientInfo.Count (); ++wi) {
																if ((null != _webClientInfo[wi]) && (CurrentCacheIndex == _webClientInfo[wi].CacheIndex)) {
																	isIndexValid = false;
																	CurrentCacheIndex = (CurrentCacheIndex + 1) % MaxCacheIndex;
																	break;
																}
															}
														} while (!isIndexValid);
														_webClientInfo [i].CacheIndex = CurrentCacheIndex;
														CurrentCacheIndex = (CurrentCacheIndex + 1) % MaxCacheIndex;
													}
													_webClients [i].DownloadFileAsync (_activeDownloaders [i].Url, CacheLocation + _webClientInfo [i].CacheIndex, (int)(1));//_list[j].ToFile);
													break;
												} catch (Exception e) {
													_webClientInfo [i].IsBusy = false;
													_webClientInfo [i].CacheIndex = -1;
													FailDownload (_activeDownloaders [i], e);
												}
											}
										}
									}
								}

								j++;
							}
						}
					}

				}

				FindFilesSize();
			}
			private void FindFilesSize()
			{
				if (_isFileCheckerrunning || true)
					return;

				_isFileCheckerrunning = true;

				Task.Factory.StartNew (() => {

					using ( var wc = new GzipWebClient () ) {
					
						for (int j = 0; j < _downloadList.Count; ++j) {
							var d = _downloadList [j];

							if (d == null || d.HasFileSize || d.Started)
								continue;

							try{
								wc.OpenRead (_downloadList [j].Url);
								long bytes_total = Convert.ToInt64 (wc.ResponseHeaders ["Content-Length"]);

								if (!(d.HasFileSize || d.Started)) {
									d.TotalSize = bytes_total;
									d.HasFileSize = true;
								}
								j = 0;

							} catch (Exception e) {
								d.HasFileSize = true;
								j = 0;
							}
						}
					}

					_isFileCheckerrunning = false;
				});

			}

			const int MaxRetryTime = 2;

			bool IsMD5Match (Uri uri, WebHeaderCollection headers, int cacheIndex)
			{
				string remoteMd5 = null;
				try {
					remoteMd5 = BlrtUtil.GetHexMD5FromETag (uri, headers.Get ("ETag"));
				} catch {}
				if (!string.IsNullOrWhiteSpace (remoteMd5)) {
					// when we can get md5 from file server
					string localMd5 = null;
					string errorStr = null;
					try {
						localMd5 = BlrtUtil.FileHexMd5 (CacheLocation + cacheIndex);
					} catch (Exception ex) {
						errorStr = ex.ToString ();
					}
					if (string.IsNullOrWhiteSpace (localMd5)) {
						LLogger.WriteEvent ("Downloader", "error", "cannot generate md5 of downloaded file", errorStr);
						return false;
					} else {
						return localMd5.Equals (remoteMd5);
					}
				} else {
					// when we cannot get md5 from file server - we asume the file is not broken
					return true;
				}
				return false;
			}

			private void OnFinished (object sender, System.ComponentModel.AsyncCompletedEventArgs args)
			{
				lock (_downloadList) {
					for (int i = 0; i < _webClients.Length; i++) {
						if (_webClients [i] == sender) {
							lock (_webClients [i]) {
								bool isDownloadFailed = true;
								bool handleResult = false; // when download is failed but we can still retry, it doesn't need to handle this download result
								if (args.Error == null && !args.Cancelled) {
									if (IsMD5Match (_activeDownloaders [i].Url, _webClients [i].ResponseHeaders, _webClientInfo [i].CacheIndex)) {
										isDownloadFailed = false;
										handleResult = true;
									} else {
										isDownloadFailed = true;
									}
								} else {
									isDownloadFailed = true;
								}
								if (isDownloadFailed) {
									int triedTime = MaxRetryTime;
									try {
										triedTime = Convert.ToInt32 (args.UserState);
									} catch {}
									if (triedTime < MaxRetryTime) {
										try {
											_webClients [i].DownloadFileAsync (_activeDownloaders [i].Url, CacheLocation + _webClientInfo [i].CacheIndex, (int)(triedTime + 1));
										} catch (Exception e) {
											FailDownload (_activeDownloaders [i], e);
										}
										handleResult = false;
									} else {
										handleResult = true;
									}
								} else {
									handleResult = true;
								}

								if (handleResult) {
									var target = _activeDownloaders [i];
									bool downloadResultInNotification = false;
									Exception lastException = args.Error;
									if (target != null) {
										try {
											lock (target._lockObject) {
												target.ReceivedSize = target.TotalSize;

												_downloadList.Remove (target);
												_activeDownloaders [i] = null;

												if (!isDownloadFailed) {
													target.IsFinished = true;

													target.ReceivedSize = target.TotalSize;
													DownloadProggress (target);
													try {
														File.Delete (target.ToFile);
													} catch {}
													File.Move (CacheLocation + _webClientInfo [i].CacheIndex, target.ToFile);
													LLogger.WriteEvent ("Downloader", "Success", string.Format ("total size: {0}, ", target.TotalSize));
													DownloadFinished (target);
													target._task.TrySetResult (target);
													BlrtUtil.PopDownloadStack ();
													BlrtUtil.SetConnectionHasProblem (false);
													downloadResultInNotification = true;
												}
											}
										} catch (Exception ex) {
											lastException = ex;
											downloadResultInNotification = false;
										} finally {
											if (!downloadResultInNotification) {
												FailDownload (target, lastException);
											}
											_webClientInfo [i].IsBusy = false;
											NotifyFileDownloadFinished (target.ToFile, downloadResultInNotification);
										}
									}
								}
								break;
							}
						}
					}
				}

				Task.Run(()=>{
					FindAndDownload ();
				});
			}

			private void FailDownload (BlrtDownloader target, Exception error)
			{
				if (null == target) {
					return;
				}

				var theException = error ?? new OperationCanceledException ();
				target.Exception = theException;							

				DownloadProggress (target);

				LLogger.WriteEvent ("Downloader", "Failed to download", "Exception is as follows: " + theException.ToString ());
				DownloadFailed (target, new UnhandledExceptionEventArgs (theException, true));
				target._task?.TrySetException (theException);
				BlrtUtil.PopDownloadStack ();
			}

			private void OnProgress(object sender, DownloadProgressChangedEventArgs args)
			{
				lock (_downloadList) {
					for (int i = 0; i < _webClients.Length; i++) {
						if (_webClients [i] == sender) {
							lock (_webClients [i]) {

								if (args.ProgressPercentage >= 100)
									_meterBytesDownloaded += args.TotalBytesToReceive;

								if (_activeDownloaders [i] != null) {

									var tmp = _activeDownloaders [i];

									lock (tmp._lockObject) {
										tmp.TotalSize = args.TotalBytesToReceive;
										tmp.ReceivedSize = args.BytesReceived;
										tmp.HasFileSize = true;

										DownloadProggress (tmp);
									}
								}

								return;
							}
						}
					}
				}
			}



			private void DownloadStarted(BlrtDownloader d)
			{
				d.Started = true;

				Task.Factory.StartNew (() => {
					if (d.OnDownloadStarted != null)
						d.OnDownloadStarted (d, EventArgs.Empty);
				});
			}

			private void DownloadProggress(BlrtDownloader d)
			{
				d.Started = true;

				Task.Factory.StartNew (() => {
					if (d.OnProgress != null)
						d.OnProgress (d, EventArgs.Empty);
				});
			}

			private void DownloadFinished(BlrtDownloader d)
			{
				d.Started = true;

				Task.Factory.StartNew (() => {
					try {
						if (d.OnDownloadFinished != null)
							d.OnDownloadFinished (d, EventArgs.Empty);
					} catch (Exception e) {
						e.ToString();
					}
				});
			}

			private void DownloadFailed(BlrtDownloader d, UnhandledExceptionEventArgs e )
			{
				Task.Factory.StartNew (() => {
					if (d.OnDownloadFailed != null)
						d.OnDownloadFailed (d, e);
				});
			}




			public void Cancel (BlrtDownloader sender)
			{
				for (int i = 0; i < _webClients.Length; i++) {
					if (_webClients [i] != null) {
						lock (_webClients [i]) {
							if (_activeDownloaders [i] != null && _activeDownloaders [i] == sender) {
								_webClients [i].CancelAsync ();
								break;
							}
						}
					}
				}
			}

		}

		public interface IDownloadStateListener
		{
			void NotifyDownloadStarted (string filePath);
			void NotifyDownloadFinished (string filePath, bool succeeded);
		}

		static Dictionary<string, List<IDownloadStateListener>> _listeners = new Dictionary<string, List<IDownloadStateListener>>();
		static ReaderWriterLockSlim _listenersLock = new ReaderWriterLockSlim ();

		public static void Subscribe (string filePath, IDownloadStateListener listener)
		{
			if (string.IsNullOrWhiteSpace (filePath)) {
				throw new ArgumentNullException ("filePath");
			}
			if (null == listener) {
				throw new ArgumentNullException ("listener");
			}
			List<IDownloadStateListener> targetList = null;
			using (_listenersLock.DisposableWriteLock ()) {
				if (_listeners.ContainsKey (filePath)) {
					targetList = _listeners [filePath];
				} else {
					targetList = new List<IDownloadStateListener> ();
					_listeners.Add (filePath, targetList);
				}
				if ((null != targetList) && (!targetList.Contains (listener))) {
					targetList.Add (listener);
				}
			}
		}

		public static void UnsubscribeFile (string filePath, IDownloadStateListener listener)
		{
			if (string.IsNullOrWhiteSpace (filePath)) {
				throw new ArgumentNullException ("filePath");
			}
			if (null == listener) {
				throw new ArgumentNullException ("listener");
			}
			using (_listenersLock.DisposableWriteLock ()) {
				if (_listeners.ContainsKey (filePath)) {
					_listeners [filePath].Remove (listener);
				}
			}
		}

		public static void UnsubscribeAllFile (IDownloadStateListener listener)
		{
			if (null == listener) {
				throw new ArgumentNullException ("listener");
			}
			using (_listenersLock.DisposableWriteLock ()) {
				foreach (var kvp in _listeners) {
					kvp.Value.Remove (listener);
				}
			}
		}

		static void NotifyFileDownloadFinished (string destPath, bool succeeded)
		{
			if (string.IsNullOrWhiteSpace (destPath)) {
				return;
			}
			using (_listenersLock.DisposableReadLock ()) {
				if (_listeners.ContainsKey (destPath)) {
					foreach (var listener in _listeners[destPath]) {
						listener.NotifyDownloadFinished (destPath, succeeded);
					}
				}
			}
		}

		static void NotifyFileDownloadStarted (string destPath)
		{
			if (string.IsNullOrWhiteSpace (destPath)) {
				return;
			}
			using (_listenersLock.DisposableReadLock ()) {
				if (_listeners.ContainsKey (destPath)) {
					foreach (var listener in _listeners[destPath]) {
						listener.NotifyDownloadStarted (destPath);
					}
				}
			}
		}
	}	
}

