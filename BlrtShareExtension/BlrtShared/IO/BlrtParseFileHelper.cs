using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Parse;

namespace BlrtData.IO
{
	///simple class used for handling the progress of a file being uploaded.  Not see by the outside world, use BlrtParseUploader.OnProgree for that
	public class BlrtParseFileHelper : IProgress<float>, IDisposable
	{
		#region IDisposable implementation

		public void Dispose ()
		{
		}

		#endregion

		const int SecondWithoutProgressDefault = 60 * 10;
		const int SecondWithoutInternetDefault = 15;

		int secondWithoutProgress, secondWithoutInternet;

		public double Progress {
			get;
			private set;
		}
		public EventHandler OnReport {
			get;
			set;
		}

		public ParseFile ParseFile {
			get;
			private set;
		}


		public bool IsFinished {
			get;
			private set;
		}
		public bool IsThumbnail {
			get;
			set;
		}

		public bool IsActive {
			get;
			private set;
		}

		public bool Cancelled {
			get;
			private set;
		}

		public string Name {
			get;
			private set;
		}

		public long SizeInBytes {
			get;
			private set;
		}


		public double SizeInMegaBytes {
			get {
				return SizeInBytes / (1024d * 1024d);
			}
		}

		private TaskCompletionSource<bool> _tcs;
		private CancellationTokenSource _cts;
		private bool spinnerOn;

		public Task WaitAsync () { return _tcs.Task; }
		private string _filePath;

		private BlrtParseFileHelper ()
		{
			IsFinished = false;
			Cancelled = false;

			spinnerOn = false;


			secondWithoutProgress = BlrtSettings.UploadProgressTimeout != -999 ? BlrtSettings.UploadProgressTimeout : SecondWithoutProgressDefault;
			secondWithoutInternet = BlrtSettings.UploadInternetTimeout != -999 ? BlrtSettings.UploadInternetTimeout : SecondWithoutInternetDefault;
		}


		public void SetIsDownloading (bool downloading)
		{
			if (!spinnerOn && downloading) {
				spinnerOn = true;
				BlrtUtil.PushDownloadStack ();
			}
			if (spinnerOn && !downloading) {
				spinnerOn = false;
				BlrtUtil.PopDownloadStack ();
			}
		}

		DateTime _timeoutProgress;
		DateTime _internetProgress;

		public bool Timeout { get { return _timeoutProgress < DateTime.Now || _internetProgress < DateTime.Now; } }

		async void TimeoutMethod ()
		{
			do {
				if (BlrtUtil.IsOnline)
					_internetProgress = DateTime.Now.AddSeconds (secondWithoutInternet);

				await Task.Delay (1000);
			} while (!Timeout && IsActive && !IsFinished);

			if (IsActive && !IsFinished) {

				LLogger.WriteLine ("Parse Helper uploader failed, after {0} progress, and {1} updates", Progress, _progressUpdated);

				Cancel ();
			}
		}

		int _progressUpdated;


		public void Report (float progress)
		{
#if DEBUG
			Console.WriteLine ("==Report==[{0}]%==of[{1}]MB", progress * 100, this.SizeInMegaBytes);
#endif
			if (progress > Progress) {
				_progressUpdated++;
				_timeoutProgress = DateTime.Now.AddSeconds (secondWithoutProgress);
			}
			Progress = progress;

			if (progress >= 1) {
				SetIsDownloading (false);
			} else {
				SetIsDownloading (true);
			}

			if (OnReport != null)
				OnReport (this, new EventArgs ());
		}

		public void Cancel ()
		{
			SetIsDownloading (false);
			IsActive = false;
			_tcs.TrySetCanceled ();
			_cts.Cancel ();

			Cancelled = true;
		}

		public void Retry ()
		{
			SetIsDownloading (true);
			IsFinished = false;
			Cancelled = false;

			_cts = new CancellationTokenSource ();
			UploadAction (_cts);
		}

		private async Task UploadAction (CancellationTokenSource cts)
		{
			_tcs = new TaskCompletionSource<bool> ();

			IsActive = true;
			try {
				SetIsDownloading (true);
				if (ParseFile == null || ParseFile.IsDirty) {

					_progressUpdated = 0;


					_timeoutProgress = DateTime.Now.AddSeconds (secondWithoutProgress);
					Progress = 0;
					TimeoutMethod ();
					await BlrtUtil.AwaitOrCancelTask (Task.Run (async () => {
						ParseFile = await BlrtUtil.ParsePlatformHelper.UploadFile (Name, _filePath, cts.Token, this);
					}), cts.Token);

					if (null == ParseFile || ParseFile.IsDirty)
						throw new IOException ();
				}
				SetIsDownloading (false);

				try {

					if (IsThumbnail)
						SaveToPath (BlrtConstants.Directories.Default.GetThumbnailPath (ParseFile.Name));
					else
						SaveToPath (BlrtConstants.Directories.Default.GetFilesPath (ParseFile.Name));


				} catch (Exception e1) {
					LLogger.WriteLine ("Failed to save {0} to path: {1}. Exception: {2}", ParseFile.Name, BlrtConstants.Directories.Default.GetFilesPath (ParseFile.Name), e1.ToString ());
				}

				IsFinished = true;
				IsActive = false;

				if (OnReport != null)
					OnReport (this, new EventArgs ());

				_tcs.TrySetResult (true);

			} catch (Exception e2) {
				BlrtUtil.Debug.ReportException (e2);
			#if DEBUG
				LLogger.WriteLine ("Failed to save {0} to path: {1}. Exception: {2}", ParseFile?.Name, BlrtConstants.Directories.Default.GetFilesPath ((null == ParseFile) ? "null_parse_file_name" : ParseFile.Name), e2?.ToString ());
			#endif
				SetIsDownloading (false);
				IsActive = false;
				OnReport?.Invoke (this, new UnhandledExceptionEventArgs (e2, true));

				_tcs?.TrySetException (e2);
			}
		}


		public void SaveToPath (string path)
		{
			if (File.Exists (_filePath) && path != _filePath) {
				File.Copy (_filePath, path);
			}
		}

		public static BlrtParseFileHelper FromFile (string name, string file)
		{
			if (name.LastIndexOf ('.') < 0)
				name = name + file.Substring (file.LastIndexOf ('.'));

			long fileLength = 0;
			try {
				fileLength = new FileInfo (file).Length;
			} catch { }
			var result = new BlrtParseFileHelper () {
				_cts = new CancellationTokenSource (),

				Name = name,
				_filePath = file,
				SizeInBytes = fileLength
			};


			result.UploadAction (result._cts);
			return result;
		}
	}
}

