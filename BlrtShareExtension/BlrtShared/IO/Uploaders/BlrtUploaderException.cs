using System;
using System.Threading.Tasks;

using BlrtAPI;

namespace BlrtData.IO
{ 

	public class BlrtUploaderException : Exception{
	
		public enum ErrorCode {
			UnknownUploadType,

			APIException,

			AlreadyOnParse,

		
			NotInQueue,

			Notrunning,
			Cancelled,
			Finished,
		}

		public ErrorCode Code { get; private set; }

		public BlrtUploaderException(ErrorCode code) : base () {
			Code = code;
		}

		public BlrtUploaderException(Exception e) : base (e.Message, e) {
			Code = ErrorCode.APIException;
		}
	
	}
}

