using System;
using System.Linq;
using System.Threading.Tasks;
using Parse;
using BlrtData.Contents;
using BlrtAPI;
using System.Collections.Generic;

namespace BlrtData.IO
{
	public class ConversationUploader : BlrtUploader, IAPIParameters
	{
		public override float Progress { 
			get { 
				return Finished ? 1 : _dataUploaded * 0.9f; 
			} 
		}

		public static BlrtUploader Create (Conversation conversation)
		{
			if (BlrtManager.Upload.HasUploader (conversation))
				return BlrtManager.Upload.GetUploader (conversation);

			var output = new ConversationUploader (conversation);
			BlrtManager.Upload.RunAsync (output);

			return output;
		}


		ConversationUploader (Conversation c) : base (c)
		{
			IsEmptyConversationUploader = c.IsEmpty;

			if (!IsEmptyConversationUploader)
				_uploaders = new List<BlrtParseFileHelper> ();
			
			_localStorage = c.LocalStorage;
		}

		float _dataUploaded;

		Conversation _conversation;
		ConversationBlrtItem _contentItem;

		IEnumerable<MediaAssetUploader> _media;
		List<BlrtParseFileHelper> _uploaders;
		ILocalStorageParameter _localStorage;

		bool IsEmptyConversationUploader { get; set;}

		internal override async Task InternalRun ()
		{

			List<Task> waitActionList = null;

			try {
				//Get all the objects setup that need to exist for this upload to work
				FindObjects ();

				_conversation.ShouldUpload = true;

				if (!IsEmptyConversationUploader) {
					
					_contentItem.ShouldUpload = true;
					RecalculateProgress ();
					waitActionList = new List<Task> ();

					lock (_uploaders) {
						//Add media uploads to a task list.
						foreach (var uploader in _uploaders) {

							if (!uploader.IsActive && !uploader.IsFinished)
								uploader.Retry ();

							waitActionList.Add (uploader.WaitAsync ());
						}
					}

					RecalculateProgress ();
				}

				if (waitActionList != null && !IsEmptyConversationUploader) {
					//wait for it to all be over, hold steady
					await Task.WhenAll (waitActionList);
					RecalculateProgress ();
				}

				await RunAPI ();

			} catch (Exception ex) {
				BlrtUtil.Debug.ReportException (ex);
				throw ex;
			} finally {
				waitActionList?.Clear ();
			}
		}


		internal override void CleanUp ()
		{
			if (IsEmptyConversationUploader)
				return;

			lock (_uploaders) {
				foreach (var uploader in _uploaders) {
					uploader.OnReport -= RecalculateProgress;
				}
				_uploaders.Clear ();
			}

			if (_audioUploader != null) {
				_audioUploader.Dispose ();
				_audioUploader = null;
			}

			if (_thumbnailUploader != null) {
				_thumbnailUploader.Dispose ();
				_thumbnailUploader = null;
			}

			if (_touchdataUploader != null) {
				_touchdataUploader.Dispose ();
				_touchdataUploader = null;
			}

			foreach (var m in _media) {
				m.CleanUp ();
			}
		}

		internal override void Error (Exception e)
		{
			base.Error (e);
			_conversation.ShouldUpload = false;

			if (!IsEmptyConversationUploader) {
				_contentItem.ShouldUpload = false;
			}
		}


		async Task RunAPI ()
		{
			APICaller api = null;

			if (IsEmptyConversationUploader) {
				api = new APICaller (this as IAPIParameters, false);

				if (await api.Run (this.GetToken ())) {
					var response = api.Response;
					var success = response.Success;
				} else {
					throw api.Exception;
				}
				return;
			}

			var publicBlrtName = _contentItem.PublicBlrtName;

			api = new APICaller (this as IAPIParameters, true);

			if (await api.Run (this.GetToken ())) {
				var response = api.Response;
				var success = response.Success;

				try {
					if (response.Objects != null) {
						var v = await BlrtUtil.FetchAllAsync (response.Objects, this.GetToken ());
						_localStorage.DataManager.AddParse (v);

						//if create with public blrt, then we should call the make public blrt api
						if (!string.IsNullOrWhiteSpace (publicBlrtName)) {
							var apiCall = new APIConversationItemPublic (
								new APIConversationItemPublic.Parameters () {
									BlrtId = _contentItem.ObjectId,
									Name = publicBlrtName
								}
							);
							if (await apiCall.Run (BlrtUtil.CreateTokenWithTimeout (15000))) {

								if (!apiCall.Response.Success) {
									throw new Exception ("error making Blrt public");
								}
								BlrtData.Helpers.BlrtTrack.ConvoMakeBlrtPublic (_conversation.ObjectId, _contentItem.ObjectId, publicBlrtName);
								var query = BlrtData.Query.BlrtConversationQuery.RunQuery (_conversation.ObjectId);
								await query.WaitAsync ();

							} else {
								throw apiCall.Exception;
							}
						}
					}
				} catch {
					success = false;
				}

				if (!success)
					throw new Exception ();

				//tracking
				var blrtLength = _contentItem.AudioLength;
				var convoId = _conversation.ObjectId;
				var title = _conversation.Name;
				var numberPages = _contentItem.Pages;
				var medias = BlrtManager.DataManagers.Default.GetMediaAssets (_contentItem);
				var pageTypes = medias.Select (x => x.Format.ToString ()).Concat (_contentItem.MediaData.Items.Any (y => y.MediaId.StartsWith ("_template")) ? new string [] { "Template" } : new string [] { }).Distinct ();

				BlrtData.Helpers.BlrtTrack.ConvoCreate (blrtLength, convoId, title, numberPages, pageTypes);
			} else {
				throw api.Exception;
			}
		}


		Dictionary<string, object> IAPIParameters.ToDictionary ()
		{
			if (IsEmptyConversationUploader) {
				return new Dictionary<string, object> () { 
					{ "name",           _conversation.Name},
					{ "localGuid",      _conversation.LocalGuid},
				};
			}

			return new Dictionary<string, object>() {
				{ "name",			_conversation.Name}, 
				{ "requestId",		_conversation.RequestId}, 
				{ "attachedMeta",	_conversation.AttachedMeta}, 
				{ "localGuid",		_conversation.LocalGuid}, 
				{ "thumbnail",		_thumbnailUploader.ParseFile}, 
				{ "item", new Dictionary<string, object>() {
						{ "arg",			_contentItem.MediaData.ToDictionary() },
						{ "localGuid",		_contentItem.LocalGuid },
						{ "files",			new Dictionary<string, object>() {
								{ "encryptType",			_contentItem.EncryptType },
								{ "encryptValue",			_contentItem.EncryptValue },
								{ "audio",					_audioUploader.ParseFile },
								{ "touch",					_touchdataUploader.ParseFile },
							}
						},
						{ "media",			(from m in _media
							select m.ToDictionary())
								.ToArray() },
					}
				}
			};
		}

		bool IAPIParameters.IsValid ()
		{
			if (_conversation == null) return false;

			if (IsEmptyConversationUploader) {
				return true;
			}

			if (_contentItem == null) return false;

			lock (_uploaders) {
				if (_uploaders.Any ((arg) => !arg.IsFinished))
					return false;
			}

			return true;
		}


		BlrtParseFileHelper _thumbnailUploader;
		BlrtParseFileHelper _audioUploader;
		BlrtParseFileHelper _touchdataUploader;

		void FindObjects ()
		{

			if (_conversation == null) {
				_conversation = Target as Conversation;
				if (_conversation.OnParse) {
					_conversation = null;
					throw new BlrtConversationUploaderException (BlrtConversationUploaderException.ErrorCode.ConversationAlreadyOnParse);
				}
			}

			if (IsEmptyConversationUploader)
				return;

			if (_contentItem == null) {
				_contentItem = _localStorage.DataManager.GetConversationItems (_conversation).OrderBy (arg => arg)
										.Where (content => content.Type == BlrtContentType.Blrt).FirstOrDefault () as ConversationBlrtItem;

				if (_contentItem == null) {
					_contentItem = null;
					throw new BlrtConversationUploaderException (BlrtConversationUploaderException.ErrorCode.NoContentItem);
				}
			}

			if (_media == null) {
				_media = from m in _localStorage.DataManager.GetMediaAssets (_contentItem)
						 select new MediaAssetUploader (m);
			}

			if (_audioUploader == null) {
				try {
					_audioUploader = BlrtParseFileHelper.FromFile ("audio", _contentItem.LocalAudioPath);
					AssignUploader (_audioUploader);
				} catch {
					_audioUploader = null;
					throw;
				}
			}

			if (_thumbnailUploader == null) {
				try {
					_thumbnailUploader = BlrtParseFileHelper.FromFile ("thumb", _conversation.LocalThumbnailPath);
					_thumbnailUploader.IsThumbnail = true;
					AssignUploader (_thumbnailUploader);
				} catch {
					_thumbnailUploader = null;
					throw;
				}
			}

			if (_touchdataUploader == null) {
				try {
					_touchdataUploader = BlrtParseFileHelper.FromFile ("touchdata", _contentItem.LocalTouchDataPath);
					AssignUploader (_touchdataUploader);
				} catch {
					_touchdataUploader = null;
					throw;
				}
			}

			foreach (var m in _media) {
				if (m.uploader != null) {
					AssignUploader (m.uploader);
				}
			}
		}


		void AssignUploader (BlrtParseFileHelper uploader)
		{
			lock (_uploaders) {
				if (uploader != null && !_uploaders.Contains (uploader)) {
					_uploaders.Add (uploader);
					uploader.OnReport += RecalculateProgress;
				}
			}
		}

		void ResignUploader (BlrtParseFileHelper uploader)
		{

			lock (_uploaders) {
				if (uploader != null && _uploaders.Contains (uploader)) {
					_uploaders.Remove (uploader);
					uploader.OnReport -= RecalculateProgress;
				}
			}
		}


		void RecalculateProgress (object sender, EventArgs args)
		{
			RecalculateProgress ();
		}

		void RecalculateProgress ()
		{
			long done = 0;
			long total = 0;
			lock (_uploaders) {
				foreach (var u in _uploaders) {
					done += u.IsFinished ? u.SizeInBytes : (long)(u.Progress * u.SizeInBytes);
					total += u.SizeInBytes;
				}
			}

			total = Math.Max (total, 1);
			_dataUploaded = (done * 1f) / total;
			Changed ();
		}


		class APICaller : APIBase<APICreationResponse> {

			public APICaller(IAPIParameters parameters, bool createWithBlrt) 
				: base(1, CloudFunctionEndPoint(createWithBlrt), parameters)
			{
			}

			static string CloudFunctionEndPoint (bool createWithBlrt)
			{
				return createWithBlrt ? "conversationCreateWithBlrt" : "conversationCreate";
			}
		}

		private class MediaAssetUploader
		{
			public MediaAsset media;
			public BlrtParseFileHelper uploader;

			public MediaAssetUploader (MediaAsset media)
			{
				this.media = media;
				uploader = media.Uploader;
				if (null == uploader && !media.OnParse) {
					uploader = media.TryUploadAsset ();
				}
			}

			public void CleanUp ()
			{
				if (uploader != null) {
					media.ClearUploadAsset ();
					uploader = null;
				}
			}

			public IDictionary<string, object> ToDictionary(){
				if (media.OnParse) {
					return new Dictionary<string, object> () {
						{ "mediaId",	media.ObjectId }
					};
				} else {
					return new Dictionary<string, object> () {
						{ "localGuid",			media.LocalGuid							},
						{ "encryptType",		media.EncryptType						},
						{ "encryptValue",		media.EncryptValue						},
						{ "pageArgs",			BlrtUtil.ListToPages(media.PageArgs)	},
						{ "format",				(int)media.Format						},
						{ "file",				uploader.ParseFile						},
						{ "name",				media.Name								},
					};
				}
			}
		}
	}

	public class BlrtConversationUploaderException:Exception{

	
		public enum ErrorCode {
			ConversationAlreadyOnParse,
			NoContentItem,
			NoResponseFromAPI,
		}
		public ErrorCode Code { get; private set; }
	
		public BlrtConversationUploaderException(ErrorCode code) : base(){
			this.Code = code;
		}
	}
}

