using System;
using System.Threading.Tasks;
using BlrtData.Contents;
using BlrtAPI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlrtData.IO
{
	public class ConversationItemUploader : BlrtUploader, IAPIParameters
	{
		public override float Progress { get { return Finished ? 1 : _dataUploaded * 0.9f; } }

		public static BlrtUploader Create (ConversationItem c)
		{
			if (BlrtManager.Upload.HasUploader (c))
				return BlrtManager.Upload.GetUploader (c);

			return new ConversationItemUploader (c);
		}


		ConversationItemUploader(ConversationItem c) : base(c) {
			_localStorage = c.LocalStorage;
		}


		float _dataUploaded;

		ConversationItem _contentItem;

		IEnumerable<MediaAssetUploader> _media;
		List<BlrtParseFileHelper> _uploaders = new List<BlrtParseFileHelper> ();
		ILocalStorageParameter _localStorage;


		internal override async Task InternalRun () {


			var waitActionList = new List<Task> ();

			try{
				//Get all the objects setup that need to exist for this upload to work
				FindObjects ();
				_contentItem.ShouldUpload = true;

				Changed();

				lock(_uploaders){
					//Add media uploads to a task list.
					foreach (var uploader in _uploaders) {

						if(!uploader.IsActive && !uploader.IsFinished)
							uploader.Retry();

						waitActionList.Add( uploader.WaitAsync() );
					}
				}

				Changed();

				//wait for it to all be over, hold steady
				await Task.WhenAll (waitActionList);

				Changed();

				await RunAPI();

			} catch (Exception ex) {
				BlrtUtil.Debug.ReportException (ex);
				throw ex;
			} finally {
				waitActionList.Clear ();
			}
		}


		internal override void CleanUp () {

			lock (_uploaders) {
				foreach (var uploader in _uploaders) {
					uploader.OnReport -= Changed;
				}
				_uploaders.Clear ();
			}

			if (_thumbnailUploaders != null) {
				
				foreach (var uploader in _thumbnailUploaders) {
					uploader.Dispose ();
				}

				_thumbnailUploaders = null;
			}

			if (_audioUploader != null) {
				_audioUploader.Dispose ();
				_audioUploader = null;
			}

			if (_touchdataUploader != null) {
				_touchdataUploader.Dispose ();
				_touchdataUploader = null;
			}

			if (_media == null) {
				foreach (var m in _media) {
					m.CleanUp ();
				}
			}
			GC.Collect ();
		}

		internal override void Error (Exception e)
		{
			base.Error (e);
			_contentItem.ShouldUpload = false;
		}



		async Task RunAPI ()
		{
			var api = new APICaller (this as IAPIParameters);

			if (await api.Run (this.GetToken ())) {
				var response = api.Response;
				var success = response.Success;

				try{
					if(response.Objects != null) {
						var objs = response.Objects;
						var newObjs = response.Objects.ToArray();
						if(objs.Count()>2){
							newObjs[1] = objs.ElementAt(2);
							newObjs[2] = objs.ElementAt(1);
						}
						var fetchResult = await BlrtUtil.FetchAllAsync(newObjs.Where (obj => !obj.ClassName.Equals (ConversationItem.CLASS_NAME)), GetToken ());

						foreach (var conversationItem in newObjs.Where (obj => obj.ClassName.Equals (ConversationItem.CLASS_NAME))) {
							var query = Query.BlrtConversationItemQuery.
							                 RunQuery (conversationItem.ObjectId, 
							                           LocalStorageParameters.Get (LocalStorageType.Default));
							await query.WaitAsync ();
						}

						_localStorage.DataManager.AddParse (fetchResult);
						_localStorage.DataManager.Save ();
					}
				} catch (Exception e) {
					success = false;
					throw;
				}

				if(!success)
					throw new Exception ();

				//tracking
				if (_contentItem.Type == BlrtContentType.BlrtReply) {
					var item = _contentItem as ConversationBlrtItem;
					var blrtLength = item.AudioLength;
					var convoId = item.ConversationId;
					var blrtId = item.ObjectId;
					BlrtData.Helpers.BlrtTrack.ConvoCreateBlrtReply (blrtLength, convoId, blrtId);
				}else if(_contentItem.Type == BlrtContentType.Comment) {
					var item = _contentItem as ConversationCommentItem;
					var commentLength = item.Comment.Length;
					var convoId = item.ConversationId;
					BlrtData.Helpers.BlrtTrack.ConvoCreateComment (commentLength, convoId);
				}

			} else {			
				throw api.Exception;
			}
		}

		Dictionary<string, object> IAPIParameters.ToDictionary ()
		{
			Dictionary<string, object> itemDic = null;

			if (_contentItem.Type == BlrtContentType.Blrt || _contentItem.Type == BlrtContentType.BlrtReply) {

				var blrt = _contentItem as ConversationBlrtItem;

				itemDic = new Dictionary<string, object> () {
					{"blrt",  new Dictionary<string, object> () {
							{ "arg",            blrt.MediaData.ToDictionary () },
							{ "localGuid",      _contentItem.LocalGuid },
							{ "thumbnail",      _thumbnailUploaders != null ? _thumbnailUploaders[0].ParseFile : null },
							{ "files",          new Dictionary<string, object> () {
									{ "encryptType",            _contentItem.EncryptType },
									{ "encryptValue",           _contentItem.EncryptValue },
									{ "audio",                  _audioUploader.ParseFile },
									{ "touch",                  _touchdataUploader.ParseFile },
								}
							}, { "media",           (from m in _media
							select m.ToDictionary())
								.ToArray()
						} }
					}
				};
			} else if (_contentItem.Type == BlrtContentType.Audio) {

				var audio = _contentItem as ConversationAudioItem;
				itemDic = new Dictionary<string, object> () {
					{"audio",  new Dictionary<string, object> () {
							{ "arg",            audio.ArgDictionary() },
							{ "localGuid",      _contentItem.LocalGuid },
							{ "files",          new Dictionary<string, object> () {
									{ "encryptType",            _contentItem.EncryptType },
									{ "encryptValue",           _contentItem.EncryptValue },
									{ "audio",                  _audioUploader.ParseFile }
								}
							} 
						}
					}
				};

			} else if(_contentItem.Type == BlrtContentType.Media) {

				var media = _contentItem as ConversationMediaItem;
				itemDic = new Dictionary<string, object> () {
					{"images",  new Dictionary<string, object> () {
							{ "arg",            media.ArgDictionary() },
							{ "localGuid",      _contentItem.LocalGuid },
							{ "media",           (from m in _media
							select m.ToDictionary())
								.ToArray()
						}, }
					}
				};
			} else if(_contentItem.Type == BlrtContentType.Comment){

				var comment = _contentItem as ConversationCommentItem;

				itemDic = new Dictionary<string, object> () {
					{ "comment",  new Dictionary<string, object> () {
						{ "text",			comment.Comment },
						{ "localGuid",		_contentItem.LocalGuid },
						{ "thumbnail",		_thumbnailUploaders != null ? _thumbnailUploaders[0].ParseFile : null }, 
					} }
				};
			} else {
			}



			return new Dictionary<string, object>() {
				{ "conversationId",	_contentItem.ConversationId }, 
				{ "items", new []{
						itemDic
					}
				}
			};
		}

		bool IAPIParameters.IsValid ()
		{
			if (_contentItem == null) return false;

			lock (_uploaders) {
				if (_uploaders.Any ((arg) => !arg.IsFinished))
					return false;
			}

			return true;
		}	


		BlrtParseFileHelper[] _thumbnailUploaders;
		BlrtParseFileHelper _audioUploader;
		BlrtParseFileHelper _touchdataUploader;

		void SetAudioUploader(string localAudioPath)
		{
			if (_audioUploader == null) {
				_audioUploader = BlrtParseFileHelper.FromFile ("audio", localAudioPath);
				AssignUploader (_audioUploader);
			}
		}

		void SetBlrtTouchUploader ()
		{
			var blrt = _contentItem as ConversationBlrtItem;
			if (_touchdataUploader == null) {
				_touchdataUploader = BlrtParseFileHelper.FromFile ("touchdata", blrt.LocalTouchDataPath);
				AssignUploader (_touchdataUploader);
			}
		}

		void SetMediaUploaders()
		{
			if (_media == null) {
				_media = from m in _localStorage.DataManager.GetMediaAssets (_contentItem)
						 select new MediaAssetUploader (m);
			}

			if (_media != null) {
				foreach (var m in _media) {
					if (m.mediaUploader != null) {
						AssignUploader (m.mediaUploader);
					}
				}
			}
		}

		void SetMediaThumbnailUploaders()
		{
			if (_media != null) 
			{
				foreach (var m in _media) {
					if (m.thumbnailUploader != null) {
						AssignUploader (m.thumbnailUploader);
					}
				}
			}
		}

		void SetBlrtConversationItemThumbnail()
		{
			if (_contentItem.LocalThumbnailNames != null) {
				var thumbnailCount = _contentItem.LocalThumbnailNames.Count ();

				if (_thumbnailUploaders == null) {
					_thumbnailUploaders = new BlrtParseFileHelper [thumbnailCount];
				}

				for (int i = 0; i < thumbnailCount; i++) {
					var path = _contentItem.LocalThumbnailPath (i);
					_thumbnailUploaders [i] = BlrtParseFileHelper.FromFile ("thumb", path);
					_thumbnailUploaders [i].IsThumbnail = true;
					AssignUploader (_thumbnailUploaders [i]);
				}
			}
		}

		void FindObjects () {

			if (_contentItem == null) {
				_contentItem	= Target as ConversationItem;
				if (_contentItem.OnParse) {
					_contentItem = null;
					throw new BlrtConversationUploaderException (BlrtConversationUploaderException.ErrorCode.ConversationAlreadyOnParse);
				}
			}

			switch (_contentItem.Type) {
				case BlrtContentType.Blrt:
				case BlrtContentType.BlrtReply: 
				{
					SetMediaUploaders ();
					SetMediaThumbnailUploaders ();
					SetBlrtConversationItemThumbnail ();
					SetAudioUploader ((_contentItem as ConversationBlrtItem).LocalAudioPath);
					SetBlrtTouchUploader ();
					break;
				}
				case BlrtContentType.Media:
				{
					SetMediaUploaders ();
					SetMediaThumbnailUploaders ();
					break;
				}
				case BlrtContentType.Audio:
				{
					SetAudioUploader ((_contentItem as ConversationAudioItem).LocalAudioPath);
					break;
				}
				case BlrtContentType.Comment:
					break;
				default:
					throw new BlrtConversationUploaderException (BlrtConversationUploaderException.ErrorCode.ConversationAlreadyOnParse);
			}
		}

		void AssignUploader(BlrtParseFileHelper uploader){
			lock (_uploaders) {
				if (uploader != null && !_uploaders.Contains (uploader)) {
					_uploaders.Add (uploader);
					uploader.OnReport += Changed;
				}
			}
		}

		void ResignUploader(BlrtParseFileHelper uploader){
			lock (_uploaders) {
				if (uploader != null && _uploaders.Contains (uploader)) {
					_uploaders.Remove (uploader);
					uploader.OnReport -= Changed;
				}
			}
		}


		void Changed (object sender, EventArgs args) {

			long done = 0;
			long total = 0;
			lock (_uploaders) {
				foreach (var u in _uploaders) {
					if (null != u) {
						done	+= u.IsFinished ? u.SizeInBytes : 0;
						total	+= u.SizeInBytes;
					}
				}
			}


			total = Math.Max (total, 1);
			_dataUploaded = (done * 1f) / total;

			Changed ();
		}





		class APICaller : APIBase<APICreationResponse> {

			public APICaller(IAPIParameters parameters) 
				: base(1, "conversationItemCreate", parameters)
			{
			}
		}


		class MediaAssetUploader {
			public MediaAsset media;
			public BlrtParseFileHelper mediaUploader;
			public BlrtParseFileHelper thumbnailUploader;

			public MediaAssetUploader(MediaAsset media){
				this.media = media;

         		mediaUploader = media.Uploader;
				thumbnailUploader = media.ThumbnailUploader;

				if (null == thumbnailUploader && !media.ThumbnailOnParse) {
					thumbnailUploader = media.TryUploadThumbnail ();
					//thumbnailUploader = BlrtParseFileHelper.FromFile ("thumb", media.LocalThumbnailPath);
					//thumbnailUploader.IsThumbnail = true;
				}
				if (null == mediaUploader && !media.OnParse) {
					mediaUploader = media.TryUploadAsset ();
				}
			}

			public void CleanUp(){
				if (mediaUploader != null) {
					media.ClearUploadAsset ();
					mediaUploader = null;
				}
				if (thumbnailUploader != null) {
					media.ClearUploadThumbnail ();
					thumbnailUploader = null;
				}
			}

			public IDictionary<string, object> ToDictionary(){
				if (media.OnParse) {
					return new Dictionary<string, object> () {
						{ "mediaId",	media.ObjectId }
					};
				} else {
					return new Dictionary<string, object> () {
						{ "localGuid",			media.LocalGuid							},
						{ "encryptType",		media.EncryptType						},
						{ "encryptValue",		media.EncryptValue						},
						{ "pageArgs",			BlrtUtil.ListToPages(media.PageArgs)	},
						{ "format",				(int)media.Format						},
						{ "file",				mediaUploader.ParseFile					},
						{ "thumbnail",          thumbnailUploader.ParseFile             },
						{ "name",				media.Name								}
					};
				}
			}
		}
	}
}

