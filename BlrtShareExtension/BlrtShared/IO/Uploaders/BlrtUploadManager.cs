using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BlrtData.Contents;

namespace BlrtData.IO
{
	public class BlrtUploadManager
	{

		List<BlrtUploader> _queue;
		bool _running;

		public BlrtUploadManager ()
		{
			_queue = new List<BlrtUploader> ();
			_running = false;
		}

		public void Reset ()
		{
			CancelAll ();
			lock (_queue)
				_queue.Clear ();
		}

		public Task RunAsync(BlrtUploader uploader) {

			if (!uploader.IsRunning && !uploader.Finished) {
				uploader.Restart ();
			}

			lock (_queue) {
				if (!_queue.Contains (uploader)) {
					_queue.Add (uploader);
				}
			}
			Run ();

			return uploader.AwaitAsync();
		}

		static bool IsRunning (BlrtUploader item)
		{
			return item != null && item.IsRunning;
		}

		public void Run ()
		{
			if (_running)
				return;

			RunQueue ();
		}


		async Task RunQueue ()
		{
			if (_running)
				return;

			_running = true;
			BlrtUtil.PushDownloadStack ();

			try{
				int skip = 0;

				while (_queue.Count > skip) { //bug found on xamarin insight, index out of range, we should use > instead of >=

					BlrtUploader item = null;

					lock(_queue)
						item = _queue[skip];


					if (IsRunning (item)) {
						try {
							await item.InternalRun();
							item.Finish();

							LLogger.WriteLine("Uploading {0} was successful", item.GetType().ToString());
						} catch (Exception e) {
							LLogger.WriteLine("Uploading {0} failed, exception: {1}", item?.GetType()?.ToString(), e.ToString());
							item.Error(e);
						}
					}

					skip++;
				}

				BlrtManager.DataManagers.SaveAll ();

				CleanUp ();
			} finally {
				BlrtUtil.PopDownloadStack ();
				_running = false; 
			}
		}

		public void CleanUp () {

			if (!_running) {
				lock (_queue) {
					var cleanUp = _queue.Where (i => i.ShouldCleanUp);

					foreach (var c in cleanUp)
						c.CleanUp ();

					_queue.RemoveAll (c => cleanUp.Contains (c));
				}
			}
		}


		public BlrtUploader GetUploader(BlrtBase b, bool isEmptyConversationUploader = false) {

			BlrtUploader c = null;

			lock (_queue) {
				c = _queue.FirstOrDefault (item => item.Target == b && !item.Finished);

				if (c == null) {

					if (b is Conversation)
						c = ConversationUploader.Create (b as Conversation);
					else if (b is ConversationBlrtItem || 
					         b is ConversationCommentItem || 
					         b is ConversationMediaItem ||  
					         b is ConversationAudioItem)
						c = ConversationItemUploader.Create (b as ConversationItem);
					else
						throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.UnknownUploadType);

					_queue.Add (c);
				}
			}

			return c;
		}


		public void UploadAll (string conversationId, ILocalStorageParameter localStorage = null) {
			if (null == localStorage) {
				localStorage = LocalStorageParameters.Default;
			}
			var convo = localStorage.DataManager.GetConversation (conversationId);
			var unsycned = localStorage.DataManager.GetConversationItems (conversationId)
				.Where(i => !i.OnParse)
				.ToArray();


			int index = 0;

			if (!convo.OnParse) {
				convo.ShouldUpload = true;
				var u = GetUploader (convo);
				RunAsync (u);
				++index;
			}

			for (; index < unsycned.Length; ++index) {
				unsycned[index].ShouldUpload = true;
				var u = GetUploader (unsycned[index]);
				RunAsync (u);
			}
		}


		public void CancelAll () {

			lock (_queue) {
				foreach (var item in _queue)
					if(IsRunning(item))
						item.Cancel ();
			}
		}

		public bool HasUploader(BlrtBase b) {

			return _queue.Any (item => item.Target == b && !item.Finished);
		}
	}
}

