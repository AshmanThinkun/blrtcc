using System;
using System.Threading.Tasks;
using System.Threading;

namespace BlrtData.IO
{
	public abstract class BlrtUploader
	{
		bool 						_running;
		bool 						_finished;
		Exception 					_exception;
		CancellationTokenSource 	_cts;

		BlrtBase					_target;

		DateTime _lastChanged;

		Task _taskAwaiter;
		SemaphoreSlim _semaphore;

		public event EventHandler ProgressChanged;
		public abstract float Progress { get; }

		public BlrtBase Target		{ get { return _target; } }
		public bool IsRunning		{ get { return _running; } }
		public bool Finished		{ get { return _finished; } }
		public Exception Expection	{ get { return _exception; } }

		public event EventHandler<bool> OnFinish;

		public bool ShouldCleanUp			{ get { return _finished || (Expection != null && _lastChanged.AddMinutes(10) < DateTime.Now ); }}

		protected BlrtUploader (BlrtBase target) {

			_target = target;


			_running = true;
			_finished = false;
			_exception = null;
			_cts = new CancellationTokenSource ();

			_semaphore = new SemaphoreSlim (0);

			_taskAwaiter = _semaphore.WaitAsync ();
		}

		protected void Changed(){

			_lastChanged = DateTime.Now;

			var c = ProgressChanged;
			if (c != null)	c (this, EventArgs.Empty);
		}

		public void Restart () {
			if (IsRunning)										throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.NotInQueue);
			if (Finished)										throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.Finished);


			_running = true;
			_finished = false;
			_exception = null;
			_cts = new CancellationTokenSource ();

			_taskAwaiter = _semaphore.WaitAsync ();

			Changed ();

			BlrtManager.Upload.Run ();
		}

		public void Cancel ()
		{
			if (!IsRunning)		throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.Notrunning);
			if (Finished)		throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.Finished);

			_running = false;
			_cts.Cancel ();
		}


		internal abstract Task InternalRun ();
		internal abstract void CleanUp ();

		public Task AwaitAsync () {
			return _taskAwaiter;
		}


		private void End(Exception e){

			if (!IsRunning)							throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.Notrunning);
			if (Finished || _exception != null )	throw new BlrtUploaderException (BlrtUploaderException.ErrorCode.Finished);


			Target.ShouldUpload = false;
			_running = false;

			if (e == null)	_finished = true;
			else			_exception =  new BlrtUploaderException (e);

			_semaphore.Release();
			Changed ();

			if(null!=OnFinish){
				OnFinish (this, Finished);
			}
		}

		internal virtual void Finish () {
			End (null);
		}

		internal virtual void Error (Exception e) {
			End (e);
		}

		protected CancellationToken GetToken ( ) { return _cts.Token; }
		protected CancellationToken GetToken ( int milliseconds ) {
			return CancellationTokenSource.CreateLinkedTokenSource (_cts.Token, new CancellationTokenSource (milliseconds).Token).Token;
		}
	}
}

