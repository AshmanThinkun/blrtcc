﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace BlrtData
{
	public static class BlrtPersistant
	{
		static IDictionary<string, object> _properties = null;
		static bool _inited = false;
		static string _filePath = null;
		static object _initLock = new object ();

		public static void Init ()
		{
			if (_inited) {
				return;
			}
			_inited = true;
			_filePath = Path.Combine (BlrtConstants.Directories.AppRoot, "Properties");
			if (File.Exists (_filePath)) {
				try {
					string text = File.ReadAllText(_filePath);
					if (!string.IsNullOrWhiteSpace (text)) {
						var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>> (text);
						_properties = new ConcurrentDictionary<string, object> (dic);
					}
				} catch (Exception e) {
					try {
						File.Delete (_filePath);
					} catch {}
					BlrtUtil.Debug.ReportException (e, new Dictionary<string,string>(){{"message", "error init persistant properties"}});
				}
				if (null == _properties) {
					_properties = new ConcurrentDictionary<string, object> ();
				}
			} else {
				_properties = new ConcurrentDictionary<string, object> (Xamarin.Forms.Application.Current.Properties);
				SaveProperties ();
			}

		}

		public static IDictionary<string, object> Properties { get { return _properties; } }

		public static void SaveProperties ()
		{
			if (_inited) {
				try {
					File.WriteAllText (_filePath, Newtonsoft.Json.JsonConvert.SerializeObject (_properties));
				} catch (Exception e) {
					BlrtUtil.Debug.ReportException (e, new Dictionary<string,string>(){{"message", "error writing persistant properties to file"}});
				}
			}
		}
	}
}

