using System;

namespace BlrtData.Data
{
	public class BlrtIndependentDataManager : BlrtDataManagerBase
	{
		ILocalDirectories _localDir;
		public BlrtIndependentDataManager (ILocalDirectories localDir) : base ()
		{
			if (null == localDir) {
				throw new ArgumentNullException ("localDir");
			}
			_localDir = localDir;
		}

		#region implemented abstract members of BlrtDataManagerBase

		protected override string GetCacheFile ()
		{
			return _localDir.GetDataPath ("independent");
		}

		protected override void SaveToCloud ()
		{
			// do nothing
		}

		protected override string GetUserId ()
		{
			return "global";
		}

		#endregion


	}
}

