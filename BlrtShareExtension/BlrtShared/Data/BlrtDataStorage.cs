using System;
using System.Linq;
using System.Collections.Generic;

using BlrtData.Contents;
using System.Threading;


namespace BlrtData.Data
{
	[System.Serializable]
	public class BlrtDataStorage : System.Runtime.Serialization.IDeserializationCallback
	{
		List<Conversation> 				ConversationList { get; set; }
		List<MediaAsset> 				MediaAssetList { get; set; }
		List<OCList> 					OCListList { get; set; }
		List<OCListItem>				OCListItemList { get; set;}
		List<UserOrganisationRelation>	UserOrganisationRelationList { get; set;}
		List<ConversationUserRelation> 	ConversationUserRelationList { get; set; }
		List<ConversationItem> 			ConversationItemList{ get; set; }
		List<BlrtUser> 					UserList { get; set; }

		[NonSerialized]
		ReaderWriterLockSlim _conversationListLock;

		[NonSerialized]
		ReaderWriterLockSlim _mediaAssetListLock;

		[NonSerialized]
		ReaderWriterLockSlim _ocListListLock;

		[NonSerialized]
		ReaderWriterLockSlim _ocListItemListLock;

		[NonSerialized]
		ReaderWriterLockSlim _userOrganisationRelationListLock;

		[NonSerialized]
		ReaderWriterLockSlim _conversationUserRelationListLock;

		[NonSerialized]
		ReaderWriterLockSlim _conversationItemListLock;

		[NonSerialized]
		ReaderWriterLockSlim _userListLock;

		IEnumerable<BlrtUserContactDetail> _userContactDetails; // This should never be null
		public IEnumerable<BlrtUserContactDetail> 		UserContactDetails {
			get {
				return _userContactDetails;
			}
			set {
				if (value == _userContactDetails && _userContactDetails != null)
					return;
				if (value == null)
					_userContactDetails = new List<BlrtUserContactDetail> ();
				else
					_userContactDetails = value;
				Xamarin.Forms.MessagingCenter.Send<Object> (this, "ContactChanged");
			}
		}

		public bool HasContent 
		{ 
			get {
				using (_conversationListLock.DisposableReadLock ()) {
					return ConversationList.Count > 0;
				}
			} 
		}

		public IEnumerable<Conversation> GetConversations ()
		{
			using (_conversationListLock.DisposableReadLock ()) {
				return ConversationList.ToArray ();
			}
		}

		public IEnumerable<string> GetRelationUserIds ()
		{
			var list = new List<string> ();

			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				foreach (var rel in ConversationUserRelationList) {
					if (rel.BondCreated && !list.Contains (rel.UserId)) {
						list.Add (rel.UserId);
					}
				}
			}

			return list;
		}

		public IEnumerable<ConversationUserRelation> GetCurrentUserUnreadRelations ()
		{
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.Where ((ConversationUserRelation arg) => {
					return !arg.Archive && arg.Conversation != null && arg.IsCurrentUsers;
				});
			}
		}

		private void InitLocks()
		{
			_conversationListLock 				= new ReaderWriterLockSlim ();
			_mediaAssetListLock 				= new ReaderWriterLockSlim ();
			_ocListListLock 					= new ReaderWriterLockSlim ();
			_ocListItemListLock 				= new ReaderWriterLockSlim ();
			_userOrganisationRelationListLock	= new ReaderWriterLockSlim ();
			_conversationUserRelationListLock	= new ReaderWriterLockSlim ();
			_conversationItemListLock 			= new ReaderWriterLockSlim ();
			_userListLock 						= new ReaderWriterLockSlim ();
		}

		public BlrtDataStorage () {
			ConversationList 				= new List<Conversation> ();
			MediaAssetList 					= new List<MediaAsset> ();
			ConversationUserRelationList 	= new List<ConversationUserRelation> ();
			ConversationItemList 			= new List<ConversationItem> ();
			UserList 						= new List<BlrtUser> ();
			UserContactDetails 				= new List<BlrtUserContactDetail> ();
			OCListItemList 					= new List<OCListItem> ();
			OCListList 						= new List<OCList> ();
			UserOrganisationRelationList	= new List<UserOrganisationRelation> ();

			InitLocks ();
		}

		#region IDeserializationCallback implementation

		public void OnDeserialization (object sender)
		{
			InitLocks ();
		}

		#endregion

		public IEnumerable<BlrtBase> GetDirty ()
		{
			using (_conversationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationList) 
					if (b.Dirty && b.OnParse)
						yield return b;
			}

			using (_mediaAssetListLock.DisposableReadLock ()) {
				foreach (var b in MediaAssetList)
					if (b.Dirty && b.OnParse)
						yield return b;
			}

			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationUserRelationList)
					if (b.Dirty && b.OnParse)
						yield return b;
			}

			using (_conversationItemListLock.DisposableReadLock ()) {
				foreach (var b in ConversationItemList)
					if (b.Dirty && b.OnParse)
						yield return b;
			}

		}

		public bool HasDirty ()
		{
			return GetAll ().Any (b => (b.Dirty && b.OnParse));
		}

		public IEnumerable<BlrtBase> GetAll ()
		{
			var rtn = new List<BlrtBase> ();
			using (_conversationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationList) {
					rtn.Add (b);
				}
			}

			using (_mediaAssetListLock.DisposableReadLock ()) {
				foreach (var b in MediaAssetList) {
					rtn.Add (b);
				}
			}

			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationUserRelationList) {
					rtn.Add (b);
				}
			}

			using (_conversationItemListLock.DisposableReadLock ()) {
				foreach (var b in ConversationItemList) {
					rtn.Add (b);
				}
			}

			using (_ocListListLock.DisposableReadLock ()) {
				foreach (var b in OCListList) {
					rtn.Add (b);
				}
			}

			using (_ocListItemListLock.DisposableReadLock ()) {
				foreach (var b in OCListItemList) {
					rtn.Add (b);
				}
			}

			using (_userOrganisationRelationListLock.DisposableReadLock ()) {
				foreach (var b in UserOrganisationRelationList) {
					rtn.Add (b);
				}
			}

			return rtn;
		}

		public void RemoveClones ()
		{
			using (_conversationListLock.DisposableWriteLock ()) {
				ConversationList = ConversationList.Distinct ().ToList ();
			}

			using (_mediaAssetListLock.DisposableWriteLock ()) {
				MediaAssetList = MediaAssetList.Distinct ().ToList ();
			}

			using (_conversationUserRelationListLock.DisposableWriteLock ()) {
				ConversationUserRelationList = ConversationUserRelationList.Distinct ().ToList ();
			}

			using (_conversationItemListLock.DisposableWriteLock ()) {
				ConversationItemList = ConversationItemList.Distinct ().ToList ();
			}

			using (_ocListListLock.DisposableWriteLock ()) {
				OCListList = OCListList.Distinct ().ToList ();
			}

			using (_ocListItemListLock.DisposableWriteLock ()) {
				OCListItemList = OCListItemList.Distinct ().ToList ();
			}

			using (_userOrganisationRelationListLock.DisposableWriteLock ()) {
				UserOrganisationRelationList = UserOrganisationRelationList.Distinct ().ToList ();
			}
		}

		public bool DoesLocalGuidExists (string id)
		{
			using (_conversationListLock.DisposableReadLock ()) {
				if (ConversationList.Exists (i => i.LocalGuid == id))
					return true;
			}

			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				if (ConversationUserRelationList.Exists (i => i.LocalGuid == id))
					return true;
			}

			using (_conversationItemListLock.DisposableReadLock ()) {
				if (ConversationItemList.Exists (i => i.LocalGuid == id))
					return true;
			}

			using (_mediaAssetListLock.DisposableReadLock ()) {
				if (MediaAssetList.Exists (i => i.LocalGuid == id))
					return true;
			}

			return false;
		}

		public string GetReplacementLocalGuid (string cacheId)
		{
			BlrtBase cache = null;

			using (_conversationListLock.DisposableReadLock ())
			using (_conversationUserRelationListLock.DisposableReadLock ())
			using (_conversationItemListLock.DisposableReadLock ())
			using (_mediaAssetListLock.DisposableReadLock ()) {
				cache = ConversationList.FirstOrDefault (i => i.LocalGuid == cacheId) as BlrtBase
					?? ConversationUserRelationList.FirstOrDefault (i => i.LocalGuid == cacheId) as BlrtBase
					?? ConversationItemList.FirstOrDefault (i => i.LocalGuid == cacheId) as BlrtBase
					?? MediaAssetList.FirstOrDefault (i => i.LocalGuid == cacheId) as BlrtBase;
			}
			if (cache != null)
				return cache.ObjectId;

			return cacheId;
		}

		public Conversation GetConversation(string id) {
			using (_conversationListLock.DisposableReadLock ()) {
				return ConversationList.FirstOrDefault (i => i.ObjectId == id || i.LocalGuid == id);
			}
		}

		public MediaAsset GetMediaAsset(string id) {
			using (_mediaAssetListLock.DisposableReadLock ()) {
				return MediaAssetList.FirstOrDefault (i => i.ObjectId == id || i.LocalGuid == id);
			}
		}

		public OCList GetOCList(string id) {
			if (OCListList == null)
				OCListList = new List<OCList> ();
			using (_ocListListLock.DisposableReadLock ()) {
				return OCListList.FirstOrDefault (i => i.ObjectId == id);
			}
		}

		public OCListItem GetOCListItem(string id) {
			if (OCListItemList == null)
				OCListItemList = new List<OCListItem> ();
			using (_ocListItemListLock.DisposableReadLock ()) {
				return OCListItemList.FirstOrDefault (i => i.ObjectId == id);
			}
		}

		public UserOrganisationRelation GetUserOrganisationRelation (string id) {
			if (UserOrganisationRelationList == null)
				UserOrganisationRelationList = new List<UserOrganisationRelation> ();
			using (_userOrganisationRelationListLock.DisposableReadLock ()) {
				return UserOrganisationRelationList.FirstOrDefault (i => i.ObjectId == id);
			}
		}

		public UserOrganisationRelation GetUserOrganisationRelationFromUser (string userId) {
			if (UserOrganisationRelationList == null)
				UserOrganisationRelationList = new List<UserOrganisationRelation> ();
			using (_userOrganisationRelationListLock.DisposableReadLock ()) {
				return UserOrganisationRelationList.FirstOrDefault (i => i.UserId == userId);
			}
		}

		public ConversationUserRelation GetConversationUserRelation(string id) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.FirstOrDefault (i => i.ObjectId == id);
			}
		}

		public IEnumerable<ConversationUserRelation> GetConversationUserRelationsFromConversation (string conversationId) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.Where (i => i.ConversationId == conversationId);
			}
		}

		public ConversationUserRelation GetConversationUserRelation(string conversationId, string userId) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.FirstOrDefault (i => (i.ConversationId == conversationId && i.UserId == userId));
			}
		}

		public ConversationItem GetConversationItem(string id) {
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.FirstOrDefault (i => i.ObjectId == id);
			}
		}


		public int GetConverationUserRelations(string conversationId) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.Count (i => i.ConversationId == conversationId);
			}
		}
		public int GetConversationItemCount (string conversationId) {
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.Count (i => i.ConversationId == conversationId);
			}
		}

		public IEnumerable<string> GetUserIds ()
		{
			using (_conversationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationList)
					yield return b.CreatorId;
			}

			using (_conversationItemListLock.DisposableReadLock ()) {
				foreach (var b in ConversationItemList)
					yield return b.CreatorId;
			}

			//foreach (var b in MediaAssetList)
			//	yield return b.CreatorId;

			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				foreach (var b in ConversationUserRelationList) {
					yield return b.CreatorId;
					if (!string.IsNullOrWhiteSpace (b.UserId))
						yield return b.UserId;
				}
			}
		}




		public IEnumerable<MediaAsset> GetMediaAssets(IEnumerable<string> id) {
			if(id == null) return new MediaAsset[0];
			using (_mediaAssetListLock.DisposableReadLock ()) {
				return MediaAssetList.Where (i => id.Contains (i.ObjectId) || id.Contains (i.LocalGuid));
			}
		}

		public IEnumerable<MediaAsset> GetMediaAssetsFromConversation(string conversationId) {

			var items = GetConversationItemsFromConversationId (conversationId);

			var l = new List<string> ();

			foreach (var item in items) {
				for (int i = 0; i < item.Media.Length; ++i)
					if (!l.Contains (item.Media [i]))
						l.Add (item.Media [i]);
			}

			return GetMediaAssets (l);
		}

		public IEnumerable<OCListItem> GetOCListItemsFromOCList(string ocListId)
		{
			using (_ocListItemListLock.DisposableReadLock ()) {
				return OCListItemList.Where (i => i.OCListId == ocListId);
			}
		}
		public IEnumerable<ConversationUserRelation> GetConversationUserRelations(IEnumerable<string> id) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.Where (i => id.Contains (i.ObjectId));
			}
		}
//		public IEnumerable<ConversationUserRelation> GetConversationUserRelations() {
//			return ConversationUserRelationList;
//		}
		public IEnumerable<ConversationUserRelation> GetConversationUserRelationsFromUser(string userId) {
			using (_conversationUserRelationListLock.DisposableReadLock ()) {
				return ConversationUserRelationList.Where (i => i.UserId == userId);
			}
		}
		public IEnumerable<ConversationItem> GetConversationItems(IEnumerable<string> id) {
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.Where (i => id.Contains (i.ObjectId));
			}
		}

		public IEnumerable<ConversationItem> GetConversationItemsFromConversationId (string conversationId) {
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.Where (i => (i.ConversationId == conversationId));
			}
		}

		public void Remove (BlrtBase b)
		{
			if (b is MediaAsset)
				RemoveMediaAsset (b as MediaAsset);
			if (b is Conversation)
				RemoveConversation (b as Conversation);
			if (b is ConversationItem)
				RemoveConversationItem (b as ConversationItem);
			if (b is ConversationUserRelation)
				RemoveConversationUserRelation (b as ConversationUserRelation);
			if (b is OCList)
				RemoveOCList (b as OCList);
			if (b is OCListItem)
				RemoveOCListItem (b as OCListItem);
		}

		public void Remove (IEnumerable<BlrtBase> bases)
		{
			RemoveMediaAssets (bases.Where (i => (i is MediaAsset)).Select((BlrtBase arg) => {return arg as MediaAsset;}));
			RemoveConversations (bases.Where (i => (i is Conversation)).Select((BlrtBase arg) => {return arg as Conversation;}));
			RemoveConversationItems (bases.Where (i => (i is ConversationItem)).Select((BlrtBase arg) => {return arg as ConversationItem;}));
			RemoveConversationUserRelations (bases.Where (i => (i is ConversationUserRelation)).Select((BlrtBase arg) => {return arg as ConversationUserRelation;}));
			RemoveOCLists (bases.Where (i => (i is OCList)).Select((BlrtBase arg) => {return arg as OCList;}));
			RemoveOCListItems (bases.Where (i => (i is OCListItem)).Select((BlrtBase arg) => {return arg as OCListItem;}));
			RemoveUserOrganisationRelations (bases.Where (i => (i is UserOrganisationRelation)).Select((BlrtBase arg) => {return arg as UserOrganisationRelation;}));
		}

		public void RemoveConversation (Conversation conversation) {
			using (_conversationListLock.DisposableWriteLock ()) {
				ConversationList.Remove (conversation);
			}
		}
		public void RemoveConversationItem (ConversationItem conversationItem) {
			using (_conversationItemListLock.DisposableWriteLock ()) {
				ConversationItemList.Remove (conversationItem);
			}
		}
		public void RemoveConversationUserRelation (ConversationUserRelation conversationUserRelation) {
			using (_conversationUserRelationListLock.DisposableWriteLock ()) {
				ConversationUserRelationList.Remove (conversationUserRelation);
			}
		}
		public void RemoveMediaAsset (MediaAsset mediaAsset) {
			using (_mediaAssetListLock.DisposableWriteLock ()) {
				MediaAssetList.Remove (mediaAsset);
			}
		}
		public void RemoveOCList (OCList ocl) {
			using (_ocListListLock.DisposableWriteLock ()) {
				OCListList.Remove (ocl);
			}
		}
		public void RemoveOCListItem (OCListItem oclitem) {
			using (_ocListItemListLock.DisposableWriteLock ()) {
				OCListItemList.Remove (oclitem);
			}
		}

		public void RemoveUserOrganisationRelation (UserOrganisationRelation relation) {
			using (_userOrganisationRelationListLock.DisposableWriteLock ()) {
				UserOrganisationRelationList.Remove (relation);
			}
		}

		public void RemoveConversations (IEnumerable<Conversation> conversations) {
			using (_conversationListLock.DisposableWriteLock ()) {
				ConversationList.RemoveAll ((Conversation obj) => {
					return conversations.Contains (obj);
				});
			}
		}
		public void RemoveConversationItems (IEnumerable<ConversationItem> conversationItems) {
			using (_conversationItemListLock.DisposableWriteLock ()) {
				ConversationItemList.RemoveAll ((ConversationItem obj) => {
					return conversationItems.Contains (obj);
				});
			}
		}
		public void RemoveConversationUserRelations (IEnumerable<ConversationUserRelation> conversationUserRelations) {
			using (_conversationUserRelationListLock.DisposableWriteLock ()) {
				ConversationUserRelationList.RemoveAll ((ConversationUserRelation obj) => {
					return conversationUserRelations.Contains (obj);
				});
			}
		}
		public void RemoveMediaAssets (IEnumerable<MediaAsset> mediaAssets) {
			using (_mediaAssetListLock.DisposableWriteLock ()) {
				MediaAssetList.RemoveAll ((MediaAsset obj) => {
					return mediaAssets.Contains (obj);
				});
			}
		}
		public void RemoveOCLists(IEnumerable<OCList> oclists) {
			using (_ocListListLock.DisposableWriteLock ()) {
				OCListList.RemoveAll (obj => oclists.Contains (obj));
			}
		}
		public void RemoveOCListItems(IEnumerable<OCListItem> oclistItems){
			using (_ocListItemListLock.DisposableWriteLock ()) {
				OCListItemList.RemoveAll (obj => oclistItems.Contains (obj));
			}
		}

		public void RemoveUserOrganisationRelations (IEnumerable<UserOrganisationRelation> relations) {
			using (_userOrganisationRelationListLock.DisposableWriteLock ()) {
				UserOrganisationRelationList.RemoveAll (obj => relations.Contains (obj));
			}
		}

		public int GetIdleUnsyncedConversationItemCount (string conversationId)
		{
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.Count ((ConversationItem obj) => {
					return obj.ConversationId == conversationId
					&& !obj.OnParse && !obj.ShouldUpload;
				});
			}
		}

		public void AddConversation(Conversation obj) {
			var other = GetConversation (obj.ObjectId);

			if (other != null){
				if (obj != other)
					return;
				return;
			}
			using (_conversationListLock.DisposableWriteLock ()) {
				ConversationList.Add (obj);
			}
		}

		public void AddMediaAsset (MediaAsset media) {

			var other = GetMediaAsset (media.ObjectId);

			if (other != null) {
				if (other.IsIndependent && (!media.IsIndependent)) {
					RemoveMediaAsset (other);
				} else {
					return;
				}
			}

			using (_mediaAssetListLock.DisposableWriteLock ()) {
				MediaAssetList.Add (media);
			}
		}

		public void AddOCList (OCList ocl) {

			var other = GetOCList (ocl.ObjectId);

			if (other != null) {
				if (other.IsIndependent && (!ocl.IsIndependent)) {
					RemoveOCList (other);
				} else {
					return;
				}
			}
			using (_ocListListLock.DisposableWriteLock ()) {
				OCListList.Add (ocl);
			}
		}

		public void AddOCListItem (OCListItem oclItem) {

			var other = GetOCListItem (oclItem.ObjectId);

			if (other != null) {
				if (other.IsIndependent && (!oclItem.IsIndependent)) {
					RemoveOCListItem (other);
				} else {
					return;
				}
			}
			using (_ocListItemListLock.DisposableWriteLock ()) {
				OCListItemList.Add (oclItem);
			}
		}

		public void AddUserOrganisationRelation (UserOrganisationRelation relation) {

			var other = GetUserOrganisationRelation (relation.ObjectId);

			if (other != null) {
				if (other.IsIndependent && (!relation.IsIndependent)) {
					RemoveUserOrganisationRelation (other);
				} else {
					return;
				}
			}
			using (_userOrganisationRelationListLock.DisposableWriteLock ()) {
				UserOrganisationRelationList.Add (relation);
			}
		}

		public void AddConversationUserRelation(ConversationUserRelation bond) {
			var other = GetConversationUserRelation (bond.ObjectId);

			if (other != null)
				return;

			using (_conversationUserRelationListLock.DisposableWriteLock ()) {
				ConversationUserRelationList.Add (bond);
			}
		}

		public void AddContentItem(ConversationItem content) {

			var other = GetConversationItem (content.ObjectId);

			if (other != null) {
				if (other.IsIndependent && !content.IsIndependent) {
					RemoveConversationItem (other);
				} else {
					return;
				}
			}

			using (_conversationItemListLock.DisposableWriteLock ()) {
				ConversationItemList.Add (content);
			}
		}

		#region User Stuff

		public BlrtUser GetBlrtUser (string id) {
			using (_userListLock.DisposableReadLock ()) {
				return UserList.FirstOrDefault (i => i.ObjectId == id);
			}
		}

		public BlrtUser GetBlrtUserFromEmail (string email) {
			using (_userListLock.DisposableReadLock ()) {
				return UserList.FirstOrDefault (i => i.Email == email);
			}
		}

		public void AddBlrtUser(BlrtUser user) {

			var other = GetBlrtUser (user.ObjectId);


			if (other != null) {
				if (other.Found && !user.Found)
					return;
				other.SetData (user);
			} else {
				using (_userListLock.DisposableWriteLock ()) {
					UserList.Add (user);
				}
			}
		}

		#endregion

		#region Unsynced

		public int GetUnsyncedConversationItemCount (string conversationId)
		{
			using (_conversationItemListLock.DisposableReadLock ()) {
				return ConversationItemList.Count ((ConversationItem arg) => (!arg.OnParse && arg.ConversationId == conversationId));
			}
		}


		public IEnumerable<Conversation> GetUnsyncedConversations ()
		{
			using (_conversationListLock.DisposableReadLock ()) {
				return ConversationList.Where (convo => !convo.OnParse);
			}
		}


		public IEnumerable<BlrtBase> GetUnsynced() {
			return GetAll().Where((BlrtBase arg) => { return !arg.OnParse; });
		}

		#endregion
	}
}