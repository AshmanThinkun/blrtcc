using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Parse;

namespace BlrtData.Data
{
	public abstract class BlrtDataManagerBase
	{
		private const float SaveTimeOut = 15;
		BlrtDataStorage _storage;
		List<ParseObject> _parseObjects;

		protected virtual BlrtDataStorage DataStorage {
			get {
				return _storage;
			}

			set {
				_storage = value;
			}
		}

		protected virtual List<ParseObject> ParseObjects {
			get {
				return _parseObjects;
			}
		}

		internal BlrtDataManagerBase ()
		{
			_parseObjects = new List<ParseObject> ();
			_storage = new BlrtDataStorage ();
		}

		protected abstract string GetCacheFile ();

		protected abstract void SaveToCloud ();

		protected abstract string GetUserId ();

		public bool HasContent { get { return DataStorage.HasContent; } }

		internal virtual void LoadData ()
		{
			lock (ParseObjects)
				ParseObjects.Clear ();


			try {
				DateTime n = DateTime.Now;
				try {
					using (var stream = File.OpenRead (GetCacheFile ())) {
						DataStorage = BlrtUtil.DeserializeFromStream<BlrtDataStorage> (stream);
						stream.Close ();
					}
				} catch (FileNotFoundException fnfex) {
					LoadJsonFile ();
				} catch (Exception nmexc) {
					BlrtUtil.Debug.ReportException (nmexc);
					LoadJsonFile ();
				}
				LLogger.WriteLine ("Loading local files took {0}", (DateTime.Now - n).TotalSeconds);


				var all = DataStorage.GetAll ();
				var removeList = new List<BlrtBase> ();

				foreach (var b in all) {
					if (b.ShouldDeleteData ()) {
						removeList.Add (b);
					} else if (b.CurrentVersion > b.Version) {

						if (b.ShouldUpgrade ()) {
							if (!b.TryUpgrade ()) {
								removeList.Add (b);
							}
						}
					}

					if (!removeList.Contains (b))
						b.AfterDeserization ();
				}

				foreach (var data in removeList) {
					data.DeleteData ();
					Remove (data);
				}

				LLogger.WriteLine ("Loading and processing local files took {0}", (DateTime.Now - n).TotalSeconds);


			} catch (Exception e) {
				LLogger.WriteEvent ("Local Load", "failed", "Load has failed because: " + e.ToString ());

				DataStorage = new BlrtDataStorage ();
			}
		}

		void LoadJsonFile ()
		{
			string str = "";

			try {
				str = File.ReadAllText (GetCacheFile ());

				str = BlrtUtil.XorString (str, BlrtConstants.CacheToken);
				DataStorage = JsonConvert.DeserializeObject<BlrtDataStorage> (str, new JsonSerializerSettings () {
					TypeNameHandling = TypeNameHandling.Objects
				});


				if (DataStorage == null)
					throw new Exception ();
			} catch {

				if (str != null && str.Length > 10) {
					File.Delete (GetCacheFile ());
				}
				throw;
			}
		}

		public void Remove (IEnumerable<BlrtBase> b)
		{
			DataStorage.Remove (b);
		}

		public void Remove (BlrtBase b)
		{
			DataStorage.Remove (b);
		}

		public void Remove (Conversation conversation)
		{
			DataStorage.RemoveConversation (conversation);
		}

		public void RemoveAndSaveLocal (Conversation conversation)
		{
			DataStorage.RemoveConversation (conversation);
			SaveLocal ().FireAndForget ();
		}

		public void Remove (ConversationUserRelation conversationUserRelation)
		{
			DataStorage.RemoveConversationUserRelation (conversationUserRelation);
		}

		public void Remove (ConversationItem conversationItem)
		{
			DataStorage.RemoveConversationItem (conversationItem);
		}

		public void Remove (MediaAsset mediaAsset)
		{
			DataStorage.RemoveMediaAsset (mediaAsset);
		}

		public MediaAsset GetMediaAsset (string id)
		{
			return DataStorage.GetMediaAsset (id);
		}

		public IEnumerable<MediaAsset> GetMediaAssets (IEnumerable<string> ids)
		{
			return DataStorage.GetMediaAssets (ids);
		}

		public IEnumerable<MediaAsset> GetMediaAssets (ConversationItem item)
		{
			return ((item != null) && (item.Media != null)) ? DataStorage.GetMediaAssets (item.Media) : new MediaAsset[0];
		}

		private SemaphoreSlim _saveLocalSemaphore;

		public virtual async Task SaveLocal ()
		{
			if (_saveLocalSemaphore == null)
				_saveLocalSemaphore = new SemaphoreSlim (1);

			await _saveLocalSemaphore.WaitAsync ();
			await Task.Run ((Action)LocalSaveAction);

			_saveLocalSemaphore.Release ();
		}


		protected virtual void LocalSaveAction ()
		{
			DateTime start = DateTime.Now;
			LLogger.WriteEvent ("Local Save", "started");

			try {
				foreach (var b in DataStorage.GetAll()) {
					b.BeforeSerization ();
				}

				using (var stream = BlrtUtil.SerializeToStream (DataStorage)) {
					BlrtUtil.StreamToFile (GetCacheFile (), stream);
				}


				/*
				var str = JsonConvert.SerializeObject (_storage, new JsonSerializerSettings () {
					TypeNameHandling = TypeNameHandling.Objects, 
					Formatting = Formatting.None
				});
				str = BlrtUtil.XorString (str, BlrtConstants.CacheToken);
				File.WriteAllText (GetCacheFile (), str);*/
			} catch (Exception e) {

				File.Delete (GetCacheFile ());
				BlrtUtil.Debug.ReportException (e);
			}

			LLogger.WriteEvent ("Local Save", "finished", null, (DateTime.Now - start).TotalSeconds);
		}


		public void Save ()
		{
			SaveLocal ();
			SaveToCloud ();
		}

		public virtual void AddMediaAsset (MediaAsset asset)
		{
			DataStorage.AddMediaAsset (asset);
		}

		public virtual void AddConversationItem (ConversationItem item)
		{
			DataStorage.AddContentItem (item);
		}

		public void ClearLocal ()
		{
			var array = DataStorage.GetAll ().ToArray ();
			foreach (var item in array) {
				item.DeleteData ();
				Remove (item);
			}
			array = null;
		}

		public IEnumerable<ParseObject> GetNeedSave ()
		{

			var saveables = new List<ParseObject> ();
			var dirtyOnes = DataStorage.GetDirty ();

			dirtyOnes = dirtyOnes.OrderBy ((arg) => arg.CreatedAt);

			foreach (var d in dirtyOnes) {
				if (d.OnParse) {
					var parse = GetParseObject (d.ObjectId, d.ClassName);
					d.CopyToParse (ref parse);

					if (parse != null) {
						saveables.Add (parse);
					}
				}
			}

			return saveables;
		}

		public OCList GetOCList (string objectId)
		{
			return DataStorage.GetOCList (objectId);
		}

		public IEnumerable<OCListItem> GetOCListItems (string oclistId)
		{
			return DataStorage.GetOCListItemsFromOCList (oclistId);
		}

		public UserOrganisationRelation GetUserOrganisationRelationFromUser (string userId)
		{
			return DataStorage.GetUserOrganisationRelationFromUser (userId);
		}

		public ConversationUserRelation GetConversationUserRelation (string objectId)
		{
			return DataStorage.GetConversationUserRelation (objectId);
		}

		public ConversationUserRelation GetConversationUserRelation (string conversationId, string userId)
		{
			return DataStorage.GetConversationUserRelation (conversationId, userId);
		}

		public IEnumerable<ConversationUserRelation> GetConversationUserRelations (string userId)
		{
			return DataStorage.GetConversationUserRelationsFromUser (userId);
		}

//		public IEnumerable<ConversationUserRelation> GetConversationUserRelations ()
//		{
//			return DataStorage.GetConversationUserRelations ();
//		}

		public ConversationItem GetConversationItem (string objectId)
		{
			return DataStorage.GetConversationItem (objectId);
		}

		public IEnumerable<ConversationUserRelation> GetConversationUserRelationsFromConversation (string conversationId)
		{

			var convo = DataStorage.GetConversation (conversationId);
			var items = DataStorage.GetConversationUserRelationsFromConversation (conversationId);

			if (convo != null && convo.BondCount != items.Count ())
				convo.NeedsMetaUpdate = true;

			////exclude the deleted users from the conversation relations
			////if the user is not the creator of the conversation.
			////https://thinkun.atlassian.net/wiki/spaces/BLRTDOC/pages/245301280/Blrt+deleted+user+logic+and+architecture
			//if (convo != null) {
			//	return items.Where (rel => {
			//		if (!string.IsNullOrWhiteSpace (rel.UserId)) {
			//			var user = BlrtManager.DataManagers.Default.GetUser (rel.UserId);
			//			if (user == null) {
			//				return false;
			//			}
			//			if (!user.Found) {
			//				return false;
			//			}
			//			if (user.IsDeleted && user.ObjectId != rel.Conversation.CreatorId) {
			//				return false;
			//			} 
			//			return true;
			//		}
			//		return true;
			//	});
			//}

			return items;
		}

		public Conversation GetConversation (string id)
		{
			return DataStorage.GetConversation (id);
		}

		public IEnumerable<Conversation> GetConversations ()
		{
			return DataStorage.GetConversations ();
		}

		public IEnumerable<ConversationItem> GetConversationItems (string conversationId)
		{
			return DataStorage.GetConversationItemsFromConversationId (conversationId);
		}

		public IEnumerable<ConversationItem> GetConversationItems (Conversation conversation)
		{
			return GetConversationItems (conversation?.ObjectId);
		}

		public IEnumerable<string> GetRelationUserIds ()
		{
			return DataStorage.GetRelationUserIds ();
		}

		public IEnumerable<BlrtBase> GetRelated (Conversation conversation)
		{
			var list = new List<BlrtBase> ();

			list.Add (conversation);//TODO: dont include yourself, that's weired

			foreach (var c in DataStorage.GetConversationItemsFromConversationId(conversation.ObjectId)) {

				if (c.Media != null && c.Media.Length > 0) {
					foreach (var m in DataStorage.GetMediaAssets(c.Media))
						if (!list.Contains (m))
							list.Add (m);
				}

				if (!list.Contains (c))
					list.Add (c);
			}

			if (conversation.LastMedia != null && conversation.LastMedia.Length > 0) {
				foreach (var m in DataStorage.GetMediaAssets(conversation.LastMedia)) {

					if (!list.Contains (m))
						list.Add (m);
				}
			}

			foreach (var r in DataStorage.GetConversationUserRelationsFromConversation(conversation.ObjectId)) {

				if (!list.Contains (r))
					list.Add (r);
			}

			return list;
		}

		public int GetLocalConversationItemCount (string conversationId)
		{
			return DataStorage.GetConversationItemCount (conversationId);
		}

		public int GetLocalConversationItemCount (Conversation conversation)
		{
			return GetLocalConversationItemCount (conversation.ObjectId);
		}

		public int GetLocalConversationItemCount (ConversationUserRelation relation)
		{
			return GetLocalConversationItemCount (relation.ConversationId);
		}

		public int GetIdleUnsyncedConversationItemCount (Conversation conversation)
		{
			return DataStorage.GetIdleUnsyncedConversationItemCount (conversation.ObjectId);
		}

		public void MarkConversationForUpload (Conversation conversation)
		{
			conversation.ShouldUpload = true;
			foreach (var item in GetConversationItems(conversation))
				item.ShouldUpload = true;
		}

		public int TotalUnreadCount ()
		{

			int counter = 0;

			foreach (var rel in DataStorage.GetCurrentUserUnreadRelations ()) {
				counter += rel.EstimateUnread ();
			}

			return counter;
		}

		public void ReplaceConversationId (string localGuid, string objectId)
		{

			foreach (var item in GetConversationItems(localGuid)) {
				item.ConversationId = objectId;
			}
			foreach (var rel in GetConversationUserRelationsFromConversation(localGuid)) {
				rel.ConversationId = objectId;
			}
		}

		public string GetReplaceCacheId (string cacheId)
		{
			return DataStorage.GetReplacementLocalGuid (cacheId);
		}

		public void ReplaceMediaAssetId (string localGuid, string objectId)
		{

			foreach (var convo in GetConversations()) {

				bool dirty = false;

				if (!convo.OnParse && convo.LastMedia != null && convo.LastMedia.Contains (localGuid)) {
					dirty = true;
					while (convo.LastMedia.Contains (localGuid))
						convo.LastMedia [Array.IndexOf<string> (convo.LastMedia, localGuid)] = objectId;
					convo.LastMediaArgument = convo.LastMediaArgument.Replace (localGuid, objectId);
				}

				foreach (var item in GetConversationItems(convo.ObjectId)) {
					if (!item.OnParse && item.Media != null && (item.Media.Contains (localGuid) || dirty)) {

						while (item.Media.Contains (localGuid))
							item.Media [Array.IndexOf<string> (item.Media, localGuid)] = objectId;

						item.StringifyArgument ();
						item.Argument = item.Argument.Replace (localGuid, objectId);

						item.ReloadArgument ();
					}
				}
			}


		}

		public const string CacheIdIntro = "local+=";

		public string CreateUniqueLocalGuid ()
		{
			while (true) {
				var id = string.Format ("{0}-{1}-{2}-{3}", 
					         CacheIdIntro, 
					         Guid.NewGuid ().ToString (),
					         GetUserId (),
					         BlrtUtil.GetFileSafeString (DateTime.Now.ToString ())
				         );
				if (!DataStorage.DoesLocalGuidExists (id))
					return id;
			}

			throw null;
		}

		public bool IsCacheId (string id)
		{
			return id.StartsWith (CacheIdIntro);
		}

		public Conversation[] GetUnsyncedConversations ()
		{
			var tmp = DataStorage.GetUnsyncedConversations ();

			if (tmp == null)
				return new Conversation[0];


			return tmp.ToArray ();
		}

		#region User Data

		public BlrtUser GetUser (string id)
		{
			if (ParseUser.CurrentUser == null)
				return BlrtUser.Default;

			if (ParseUser.CurrentUser.ObjectId == id) {
				var output = new BlrtUser (id) {
					DisplayName = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.DisplayNameKey, "").Trim (),
					Email = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.EmailKey, ""),
					HasSetDisplayName = !BlrtUserHelper.HasnotSetDisplayname,
					AvatarName = BlrtUserHelper.GetLocalAvatar().Replace(BlrtConstants.Directories.Default.GetAvatarPath ("")+"/",""),
					Organization = BlrtUserHelper.Organization,
					Gender = ParseUser.CurrentUser.Get<int> (BlrtUserHelper.UserGenderKey,0),
					PublicBlrtCount = BlrtUserHelper.PublicBlrtCount,
					Found = true,
					TimeStamp = DateTime.Now
				};
				AddUser (output);
			}

			var user = DataStorage.GetBlrtUser (id);



			if (user == null) {
				user = BlrtUser.CreateBlank (id);
				AddUser (user);

				if (!string.IsNullOrWhiteSpace (id) && BlrtUtil.IsOnline)
					BlrtManager.Query.AddQuery (new BlrtData.Query.BlrtUserQuery ());
			}

			return user;
		}

		public IEnumerable<string> GetUnkownUserId (bool includeUnfound)
		{
			List<string> wantedIds = new List<string> ();

			var userIds = DataStorage.GetUserIds ().Where ((string arg) => !string.IsNullOrWhiteSpace (arg));


			foreach (var userId in userIds) {

				var other = DataStorage.GetBlrtUser (userId);

				if (!string.IsNullOrWhiteSpace (userId)
				    && !wantedIds.Contains (userId)
					&& null != ParseUser.CurrentUser
				    && ParseUser.CurrentUser.ObjectId != userId
				    && (other == null
				    || other.NeedFetch
				    || (includeUnfound && !other.Found)
				    )) {
					wantedIds.Add (userId);
				}
			}

			return wantedIds;
		}

		public void AddUser (BlrtUser blrtUser)
		{
			DataStorage.AddBlrtUser (blrtUser);
		}

		#endregion

		#region UserContactDetails

		public IEnumerable<BlrtUserContactDetail> GetUserContactDetails ()
		{
			return DataStorage.UserContactDetails;
		}

		public void SetUserContactDetails (IEnumerable<BlrtUserContactDetail> details)
		{
			DataStorage.UserContactDetails = details;
		}

		#endregion

		#region Loading Parse Data



		public ParseObject GetParseObject (string objectId, string className, bool create = false)
		{
			lock (ParseObjects) {
				var result = ParseObjects.FirstOrDefault (p1 => (p1.ObjectId == objectId && p1.ClassName == className));

				if (result == null && create)
					result = ParseObject.CreateWithoutData (className, objectId);

				return result;
			}
		}

		public ParseObject[] GetParseObjects (string[] ids, string className)
		{
			var parseObjects = new ParseObject[ids.Length];

			for (int i = 0; i < ids.Length; ++i) {
				parseObjects [i] = GetParseObject (ids [i], className, true);

			}

			return parseObjects;
		}

		public IEnumerable<ParseObject> GetDirty ()
		{
			lock (ParseObjects) {
				return (from obj in ParseObjects
				        where (obj.IsDirty)
				        select obj).ToList ();
			}
		}


		public void AddParseCollection (IEnumerable<ParseObject> parseObjects)
		{
			foreach (var parse in parseObjects) {

				AddParseCollection (parse);
			}
		}

		public void AddParseCollection (ParseObject parseObject)
		{
			lock (ParseObjects) {
				var p = ParseObjects.FirstOrDefault (p1 => (p1.ObjectId == parseObject.ObjectId && p1.ClassName == parseObject.ClassName));

				if (p != null) {
					ParseObjects [ParseObjects.IndexOf (p)] = parseObject;
				} else {
					ParseObjects.Add (parseObject);
				}
			}
		}

		#endregion

		#region adding methods


		public void AddParse (IEnumerable<object> list)
		{
			if (null == list)
				return;

			foreach (var p in list) {			
				AddParse (p as ParseObject);			
			}
		}

		public void AddParse (IEnumerable<ParseObject> list, bool fromSave = false)
		{
			if (null == list)
				return;

			foreach (var p in list) {			
				AddParse (p, fromSave);			
			}
		}

		public BlrtBase AddParse (ParseObject obj, bool fromSave = false)
		{
			if (null == obj || string.IsNullOrEmpty (obj.ObjectId))
				return null;

			AddParseCollection (obj);

			if (obj.IsDataAvailable) {

				switch (obj.ClassName) {
					case Conversation.CLASS_NAME:
						AddParse (obj.Get<List<object>> (Conversation.LAST_MEDIA_KEY, null));
						return AddParseConversation (obj, fromSave);

					case ConversationUserRelation.CLASS_NAME:
						AddParse (obj.Get<ParseObject> (ConversationUserRelation.CONVERSATION_KEY, null));
						return AddParseRelation (obj, fromSave);

					case ConversationItem.CLASS_NAME:
						AddParse (obj.Get<List<object>> (ConversationItem.MEDIA_KEY, null));
						return AddParseConversationItem (obj, fromSave);

					case MediaAsset.CLASS_NAME:
						return AddParseMedia (obj, fromSave);

						//organisations:
					case OCList.CLASS_NAME:
						return AddOCList (obj, fromSave);
					case OCListItem.CLASS_NAME:
						AddParse (obj.Get<ParseObject> ("blrt", null));
						return AddOCListItem (obj, fromSave);
					case UserOrganisationRelation.CLASS_NAME:
						return AddUserOrganisationRelation (obj, fromSave);
				}
			}
			return null;
		}


		private Conversation AddParseConversation (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetConversation (parse.ObjectId);
			if (b == null && parse.ContainsKey (BlrtBase.LOCAL_GUID_KEY)) {
				b = DataStorage.GetConversation (parse.Get<string> (BlrtBase.LOCAL_GUID_KEY, ""));
				if (b != null && b.OnParse)
					b = null;
			}
			var tmp = Builder.BlrtBaseBuilder.BuildConversation (parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddConversation (tmp);

			return tmp;
		}

		private ConversationUserRelation AddParseRelation (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetConversationUserRelation (parse.ObjectId);
			if (b == null && parse.ContainsKey (BlrtBase.LOCAL_GUID_KEY)) {
				b = DataStorage.GetConversationUserRelation (parse.Get<string> (BlrtBase.LOCAL_GUID_KEY, ""));
				if (b != null && b.OnParse)
					b = null;
			}
			ConversationUserRelation tmp = Builder.BlrtBaseBuilder.BuildRelation (parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddConversationUserRelation (tmp);

			return tmp;
		}

		private ConversationItem AddParseConversationItem (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetConversationItem (parse.ObjectId);
			if (b == null && parse.ContainsKey (BlrtBase.LOCAL_GUID_KEY)) {
				b = DataStorage.GetConversationItem (parse.Get<string> (BlrtBase.LOCAL_GUID_KEY, ""));
				if (b != null && b.OnParse)
					b = null;
			}
			var tmp = Builder.BlrtBaseBuilder.BuildContentItem (parse, b, fromSave);

			if (tmp != null && b != tmp) {
				if (b != null) {
					DataStorage.RemoveConversationItem (b);
				}
				DataStorage.AddContentItem (tmp);
			}

			return tmp;
		}

		private MediaAsset AddParseMedia (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetMediaAsset (parse.ObjectId);
			if (b == null && parse.ContainsKey (BlrtBase.LOCAL_GUID_KEY)) {
				b = DataStorage.GetMediaAsset (parse.Get<string> (BlrtBase.LOCAL_GUID_KEY, ""));
				if (b != null && b.OnParse)
					b = null;
			}
			var tmp = Builder.BlrtBaseBuilder.BuildMediaAsset (parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddMediaAsset (tmp);

			return tmp;
		}

		private OCList AddOCList (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetOCList (parse.ObjectId);
			var tmp = Builder.BlrtBaseBuilder.BuildOCList(parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddOCList (tmp);

			return tmp;
		}

		private OCListItem AddOCListItem (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetOCListItem (parse.ObjectId);
			var tmp = Builder.BlrtBaseBuilder.BuildOCListItem(parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddOCListItem (tmp);

			return tmp;
		}

		private UserOrganisationRelation AddUserOrganisationRelation (ParseObject parse, bool fromSave)
		{
			var b = DataStorage.GetUserOrganisationRelation (parse.ObjectId);
			var tmp = Builder.BlrtBaseBuilder.BuildUserOrganisationRelation (parse, b, fromSave);

			if (b == null && tmp != null)
				DataStorage.AddUserOrganisationRelation (tmp);

			return tmp;
		}

		#endregion

		public int GetUnsyncedConversationItemCount (string conversationId)
		{
			return DataStorage.GetUnsyncedConversationItemCount (conversationId);
		}

		public int GetUnsyncedConversationItemCount (Conversation conversation)
		{
			return DataStorage.GetUnsyncedConversationItemCount (conversation.ObjectId);
		}

		public void AddUnsynced (BlrtBase b)
		{
			if (b.OnParse)
				throw new Exception ("Cannot add synced object this way.");

			if (b is Conversation)
				DataStorage.AddConversation (b as Conversation);
			else if (b is ConversationItem)
				DataStorage.AddContentItem (b as ConversationItem);
			else if (b is MediaAsset)
				DataStorage.AddMediaAsset (b as MediaAsset);
			else if (b is ConversationUserRelation)
				DataStorage.AddConversationUserRelation (b as ConversationUserRelation);

			b.Dirty = true;

			SaveLocal ();
		}
	}
}

