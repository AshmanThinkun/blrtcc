using System;
using System.Threading.Tasks;
using Parse;


namespace BlrtData.Data
{
	public class BlrtDataManager : BlrtDataManagerBase
	{

		protected override string GetCacheFile()
		{
			if (ParseUser.CurrentUser != null)
				return BlrtConstants.Directories.Default.GetDataPath (string.Format("{0}", ParseUser.CurrentUser.ObjectId));
			else
				throw new Exception ();
		}

		private string GetRecentContactsFile()
		{
			if (ParseUser.CurrentUser != null)
				return BlrtConstants.Directories.Default.GetDataPath (string.Format("{0}-{1}", ParseUser.CurrentUser.ObjectId, "recentcontact.json"));
			else
				throw new Exception ();
		}

		protected override string GetUserId ()
		{
			return (ParseUser.CurrentUser == null) ? null : ParseUser.CurrentUser.ObjectId;
		}

		internal BlrtDataManager () : base ()
		{

		}

		private bool NeedParseSave() {
			return DataStorage.HasDirty () || (ParseUser.CurrentUser != null && ParseUser.CurrentUser.IsDirty);
		}



		public override async Task SaveLocal ()
		{
			if (ParseUser.CurrentUser == null)
				return;
			await base.SaveLocal ();
		}

		protected override void LocalSaveAction ()
		{
			if (ParseUser.CurrentUser == null)
				return;
			base.LocalSaveAction ();
		}

		protected override void SaveToCloud ()
		{
			if (NeedParseSave ()) {
				try {
					Query.BlrtSaveDataQuery.RunQuery (LocalStorageParameters.User);
				} catch (Exception ex) {}
			}
		}


	}
}

