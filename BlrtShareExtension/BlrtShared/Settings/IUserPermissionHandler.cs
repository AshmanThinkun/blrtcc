using System.Threading.Tasks;
using Parse;

namespace BlrtData
{
	public interface IPermissionHandler
	{
		/// <summary>
		/// Retrieves whichever permissions are required by the implementing view controller and stores the result in the public Permissions property.
		/// </summary>
		/// <returns><c>true</c> if permissions were successfully updated; otherwise, <c>false</c>.</returns>
		Task<bool> ApplyPermissions ();

		/// <summary>
		/// Gets a value of if applying permission will take long enought to justify showing a loading screen
		/// </summary>
		/// <value><c>true</c> if will delay; otherwise, <c>false</c>.</value>
		bool WillDelay { get; }
	}

	/// <summary>
	/// Applies the user's permissions.
	/// </summary>
	public class UserPermissionHandler : IPermissionHandler {


		public async Task<bool> ApplyPermissions ()
		{
			if (WillDelay) {
				try {
					await Query.BlrtSettingsQuery.RunQuery ().WaitAsync (); 
				} catch { }
			}


			BlrtPermissions.Apply(new BlrtPermissions.Record[0], BlrtPermissions.AppPermissionsValid);
			return BlrtManager.Query.ValidateSettings;
		}

		public bool WillDelay {
			get { return !BlrtManager.Query.ValidateSettings || !BlrtPermissions.AppPermissionsValid; }
		}
	}

	/// <summary>
	/// A Permission Handler for a specific conversation. 
	/// </summary>
	public class ConversationPermissionHandler : IPermissionHandler {

		ConversationUserRelation Relation {
			get {
				return BlrtManager.DataManagers.Default.GetConversationUserRelation (
					_conversationId,
					ParseUser.CurrentUser.ObjectId
				);
			}
		}

		string _conversationId;



		public ConversationPermissionHandler(string conversationId) {

			_conversationId = conversationId;
		
		}

		public async Task<bool> ApplyPermissions ()
		{
			var rel = Relation;

			if (rel == null)
				return false;

			try {
				if( !rel.PermissionsValid ) {
					await Query.BlrtPerrmissionQuery.RunQuery(rel).WaitAsync();
				}
				BlrtPermissions.Apply(rel.Permissions==null ? null:rel.Permissions.records, rel.PermissionsValid);
				return true;
			} catch {
				return false;
			}
		}

		public bool WillDelay {
			get { var rel = Relation; return rel != null && !rel.PermissionsValid; }
		}
	}
}

