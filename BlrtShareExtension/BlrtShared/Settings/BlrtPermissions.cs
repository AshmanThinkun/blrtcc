using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace BlrtData
{
	public class BlrtPermissions
	{
		public static class Keys
		{
			public const string MaxBlrtDuration = "max_blrt_duration";
			public const string MaxImageResolution = "max_image_resolution";
			public const string MaxConversationUsers = "max_conversation_users";
			public const string MaxPdfSizeKb = "max_pdf_size_kb";
			public const string MaxSecondaryEmails = "max_secondary_emails";

			public const string MaxConvoSizeKb = "max_convo_size_kb";
			public const string MaxBlrtPageCount = "max_blrt_page_count";
			public const string MaxBlrtSizeKb = "max_blrt_size_kb";

			public const string CanCreateConversation = "can_create_conversation";
			public const string CanCreateComment = "can_create_comment";
			public const string CanCreateReblrt = "can_create_reblrt";
			public const string CanAddUsers = "can_add_users";
			public const string CanMediaAddOnReblrt = "can_media_add_on_reblrt";
			public const string CanMediaReorder = "can_media_reorder";
			public const string CanMediaShowhide = "can_media_showhide";
			public const string CanUseWebViewer = "can_use_web_viewer";
			public const string CanUseDesktopViewer = "can_use_desktop_viewer";
		}



		public enum BlrtAccountThreshold
		{
			Unknown = -1,
			Free = 0,
			Trial = 500,
			Premium = 1500
		}

		[Serializable]
		public class Record
		{
			public Dictionary<string, object> values;

			public int stackLevel;

			public Record ()
			{
				values = new Dictionary<string, object> ();
				stackLevel = int.MinValue;
			}

			public static Record Default {
				get {
					return new Record () {
						values = new Dictionary<string, object> () {
							{ Keys.MaxBlrtDuration,         0 },
							{ Keys.MaxImageResolution,      0 },
							{ Keys.MaxConversationUsers,    0 },
							{ Keys.MaxPdfSizeKb,            0 },
							//{ Keys.MaxPdfPageCount, 		0 },
							{ Keys.MaxSecondaryEmails,      0 },

							//{ Keys.MaxImagePageCount, 	0 },
							//{ Keys.MaxPdfFileCount, 		0 },
							//{ Keys.MaxFileCount, 			0 },

							{ Keys.MaxConvoSizeKb,          0 },
							{ Keys.MaxBlrtPageCount,        0 },
							{ Keys.MaxBlrtSizeKb,           0 },

							{ Keys.CanCreateConversation,   false },
							{ Keys.CanCreateComment,        false },
							{ Keys.CanCreateReblrt,         false },
							{ Keys.CanAddUsers,             false },

							{ Keys.CanMediaAddOnReblrt,     false },
							{ Keys.CanMediaReorder,         false },
							{ Keys.CanMediaShowhide,        false },

							{ Keys.CanUseWebViewer,         false },
							{ Keys.CanUseDesktopViewer,     false }
						}
					};
				}
			}
		}

		private static Record _compiled = null;
		private static bool _valid = false;

		public static bool IsFreeAccount { get { return AccountThreshold == BlrtAccountThreshold.Free; } }
		public static bool IsTrialAccount { get { return AccountThreshold == BlrtAccountThreshold.Trial; } }


		public static event EventHandler OnAccountThresholdChanged;
		public static BlrtAccountThreshold? AccountThreshold { get { return BlrtManager.Permissions?.account_threshold_enum; } }
		public static string AccountName { get { return BlrtManager.Permissions?.account_name; } }


		public static int MaxBlrtDuration { get { return Convert.ToInt32 (_compiled.values [Keys.MaxBlrtDuration]); } }

		public static int MaxConvoSizeKb { get { return Convert.ToInt32 (_compiled.values [Keys.MaxConvoSizeKb]); } }
		public static int MaxBlrtPageCount { get { return Convert.ToInt32 (_compiled.values [Keys.MaxBlrtPageCount]); } }
		public static int MaxBlrtSizeKb { get { return Convert.ToInt32 (_compiled.values [Keys.MaxBlrtSizeKb]); } }

		public static int MaxImageResolution { get { return Convert.ToInt32 (_compiled.values [Keys.MaxImageResolution]); } }

		public static int MaxConversationUsers { get { return Convert.ToInt32 (_compiled.values [Keys.MaxConversationUsers]); } }

		public static int MaxPDFSizeKB { get { return Convert.ToInt32 (_compiled.values [Keys.MaxPdfSizeKb]); } }

		public static int MaxSecondaryEmails { get { return Convert.ToInt32 (_compiled.values [Keys.MaxSecondaryEmails]); } }

		public static bool CanCreateConversation { get { return (bool)_compiled.values [Keys.CanCreateConversation]; } }
		public static bool CanCreateComment { get { return (bool)_compiled.values [Keys.CanCreateComment]; } }
		public static bool CanCreateReblrt { get { return (bool)_compiled.values [Keys.CanCreateReblrt]; } }
		public static bool CanAddUsers { get { return (bool)_compiled.values [Keys.CanAddUsers]; } }

		public static bool CanMediaAddOnReblrt { get { return (bool)_compiled.values [Keys.CanMediaAddOnReblrt]; } }
		public static bool CanMediaReorder { get { return (bool)_compiled.values [Keys.CanMediaReorder]; } }
		public static bool CanMediaShowHide { get { return (bool)_compiled.values [Keys.CanMediaShowhide]; } }

		public static bool CanUseWebViewer { get { return (bool)_compiled.values [Keys.CanUseWebViewer]; } }
		public static bool CanUseDesktopViewer { get { return (bool)_compiled.values [Keys.CanUseDesktopViewer]; } }


		public static bool PermissionValid { get { return _valid && AppPermissionsValid; } }
		public static bool AppPermissionsValid { get { return BlrtSettings.UpdatedAt + BlrtSettings.PermissionsExpiryTime > DateTime.Now; } }


		public int [] account_threshold;
		public BlrtAccountThreshold? account_threshold_enum;

		public string account_name;

		public List<Record> permissions;


		static BlrtPermissions ()
		{
			// Set compiled defaults
			_compiled = Record.Default;
		}

		public BlrtPermissions ()
		{
			account_threshold_enum = null;
			account_name = null;

			permissions = new List<Record> ();
		}


		private IEnumerable<Record> _applied;

		public static void Apply (IEnumerable<Record> additionalRecords, bool valid)
		{
			BlrtManager.Permissions._applied = additionalRecords;

			IEnumerable<Record> records = BlrtManager.Permissions.permissions;

			if (additionalRecords != null)
				records = records.Concat (additionalRecords);

			_valid = valid;
			_compiled = BlrtManager.Permissions.Compile (records);
		}


#if __ANDROID__
		private Record Compile (IEnumerable<Record> records)
		{

			Record compiled = Record.Default;

			var orderedRecords = records.OrderBy ((Record arg) => arg.stackLevel);

			foreach (var record in orderedRecords) {
				foreach (var kvp in record.values) {

					if (kvp.Key.EndsWith ("_args"))
						continue;

					//rewrite the logic of permission to use the relevant permissions in all records when compiled
					if (compiled.values.ContainsKey (kvp.Key)) {
						if (kvp.Key.StartsWith ("can", StringComparison.Ordinal)) {
							var value = Convert.ToBoolean (kvp.Value);
							compiled.values [kvp.Key] = value;
						} else {
							var value = Convert.ToInt32 (kvp.Value);
							compiled.values [kvp.Key] = value;
						}

					} else {
						compiled.values.Add (kvp.Key, kvp.Value);
					}
				}
			}

			return compiled;
		}
#endif
#if __IOS__
		private Record Compile (IEnumerable<Record> records)
		{

			Record compiled = Record.Default;

			foreach (var record in records.OrderBy ((Record arg) => arg.stackLevel)) {
				foreach (var kvp in record.values) {

					if (kvp.Key.EndsWith ("_args"))
						continue;

					bool simpleReplace = true;

					if (record.values.ContainsKey (kvp.Key + "_args")) {

						var args = record.values [kvp.Key + "_args"] as Dictionary<string, object>;

						if (args.ContainsKey ("functionInt")) {

							simpleReplace = false;

							int value = 0;
							int current = 0;

							try {
								value = (int)kvp.Value;
							} catch { }
							try {
								current = (int)compiled.values [kvp.Key];
							} catch { }

							switch (args ["functionInt"].ToString ()) {
							case "add":
								current += value;
								break;
							case "max":
								current = Math.Max (value, current);
								break;
							case "min":
								current = Math.Min (value, current);
								break;
							case "minus":
								current -= value;
								break;
							}
							compiled.values [kvp.Key] = current;
						}
						if (args.ContainsKey ("functionBool")) {

							simpleReplace = false;

							bool value = false;
							bool current = false;


							try {
								value = (bool)kvp.Value;
							} catch { }
							try {
								current = (bool)compiled.values [kvp.Key];
							} catch { }

							switch (args ["functionBool"].ToString ()) {
							case "and":
							case "min":
								current = current && value;
								break;
							case "or":
							case "max":
								current = current || value;
								break;
							}
							compiled.values [kvp.Key] = current;
						}
					}

					if (simpleReplace)
						compiled.values [kvp.Key] = kvp.Value;
				}
			}

			return compiled;
		}
#endif

		public void Unpack ()
		{
			if (account_threshold != null) {
				// If an account_threshold is specified but cannot be mapped to our enum then set it to Unkown

				bool accountThresholdSet = false;
				foreach (int i in account_threshold) {
					if (Enum.IsDefined (typeof (BlrtAccountThreshold), i)) {
						account_threshold_enum = (BlrtAccountThreshold)i;
						accountThresholdSet = true;
						break;
					}
				}

				if (!accountThresholdSet)
					account_threshold_enum = BlrtAccountThreshold.Unknown;
			}

			_valid = true;

			if (_applied != null)
				_compiled = Compile (permissions.Concat (_applied));
			else
				_compiled = Compile (permissions);

			if (OnAccountThresholdChanged != null)
				OnAccountThresholdChanged (this, EventArgs.Empty);
		}


		public enum PermissionEnum
		{
			MaxBlrtDuration,
			MaxImageResolution,
			MaxConversationUsers,
			MaxPDFSizeKB,
			MaxBlrtPageCount,
			MaxConvoSizeKB,
			MaxBlrtSizeKB,

			CanCreateConversation,
			CanCreateComment,
			CanCreateReblrt,
			CanAddUsers,

			CanMediaAddOnReblrt,
			CanMediaReorder,
			CanMediaShowHide,

			CanUseWebViewer,
			CanUseDesktopViewer,

			CanChangeAllowPublic,
			CanChangeAllowAdd
		}
	}
}

