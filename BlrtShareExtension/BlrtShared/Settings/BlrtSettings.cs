using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace BlrtData
{
	public class BlrtSettings
	{
		public static bool CanQuery {get { return !(MaintenanceMode || InvalidVersion);	 	} }

		public static bool MaintenanceMode 					{ get { return BlrtManager.Settings.maintenanceMode;	 		} }
		public static bool InvalidVersion 					{ get { return BlrtManager.Settings.lastInvalidVersion == BlrtManager.Responder.DeviceDetails.AppVersion; } }
		public static bool IsCurrentVersion 				{ get { return BlrtManager.Settings.isCurrentVersion ?? true; 	} }
		public static bool CanCreate						{ get { return BlrtManager.Settings.canCreate; 					} }

		public static DateTime UpdatedAt 					{ get { return BlrtManager.settingManager.UpdatedAt;			} }
			
		public static string MaintenanceMessage 			{ get { return BlrtManager.Settings.maintenanceMessage;			} }
		public static string InvalidVersionMessage			{ get { return BlrtManager.Settings.invalidVersionMessage;		} }




		public static string PublicWebsite 					{ get { return BlrtManager.Settings.public_website;				} }
		public static string ParseWebsite 					{ get { return BlrtManager.Settings.parse_website; 				} }
		public static string AppStoreUrl					{ get { return BlrtManager.Settings.app_store_url; 				} }
		public static string TermOfServiceUrl				{ get { return BlrtManager.Settings.term_of_service_url; 		} }
		public static string PrivacyUrl						{ get { return BlrtManager.Settings.privacy_url; 		} }
		public static string AndroidStatus					{ get { return BlrtManager.Settings.android_status; 		} }
		public static string NewsFeedUrl					{ get { return BlrtManager.Settings.news_feed_url;				} }

		public static string ShareUrl	 					{ get { return BlrtManager.Settings.share_url; 					} }
		public static string ShareTypeFBWall				{ get { return BlrtManager.Settings.share_type_fbwall; 			} }
		public static string ShareTypeTwitter				{ get { return BlrtManager.Settings.share_type_twitter; 		} }
		public static string ShareTypeEmail			 		{ get { return BlrtManager.Settings.share_type_email; 			} }
		public static string ShareTypeSMS				 	{ get { return BlrtManager.Settings.share_type_sms; 			} }
		public static string ShareTypeFBInvite				{ get { return BlrtManager.Settings.share_type_fbinvite;	 	} }


		public static string PremiumFeaturePoints			{ get { return BlrtManager.Settings.premium_feature_points; 			} }
		public static string FreeTrialModalFineprint		{ get { return BlrtManager.Settings.free_trial_modal_fineprint; 		} }
		public static string PremiumModalPullout			{ get { return BlrtManager.Settings.premium_modal_pullout; 				} }

		public static string UpgradeModalJson 				{ get { return BlrtManager.Settings.upgrade_modal_json;			} }



		public static PostBlrtShareState PostBlrtShare		{ get { return BlrtManager.Settings.post_blrt_share_state_enum; 	} }

		public static int RateAppSignificantEvents			{ get { return BlrtManager.Settings.rate_app_significant_events_int; 	} }
		public static int RateAppDays						{ get { return BlrtManager.Settings.rate_app_days_int; 					} }

		public static TimeSpan SettingsUpdateFrequency 		{ get { return TimeSpan.FromMinutes(BlrtManager.Settings.settings_update_frequency); } }
		public static TimeSpan PermissionsUpdateFrequency 	{ get { return TimeSpan.FromMinutes(BlrtManager.Settings.permissions_update_frequency); } }
		public static TimeSpan PermissionsExpiryTime 		{ get { return TimeSpan.FromMinutes(BlrtManager.Settings.permissions_expiry_time); } }

		public static int UploadProgressTimeout				{ get{return BlrtManager.Settings.upload_progress_timeout;} }
		public static int UploadInternetTimeout				{ get{return BlrtManager.Settings.upload_internet_timeout;} }
		public static int EncryptionType					{ get{return BlrtManager.Settings.encryption_type;} }

		public static string ReferralProgramKey					{ get { return BlrtManager.Settings.referral_program_key; 				} }
		public static string ReferralProgramUrl					{ get { return BlrtManager.Settings.referral_program_url; 				} }

		public static Dictionary<string,string> YozioDictionary {get{return BlrtManager.Settings.yozio_dic;}}

		public static string EmbedPreviewLearnMore   { get { return BlrtManager.Settings.embed_preview_learn_more_url; 				} }
		public static string[] PhoneNumberSupportedCountries  { get { return BlrtManager.Settings.phoneNumberSupportedCountries!=null? BlrtManager.Settings.phoneNumberSupportedCountries: new string[]{}; } }
	
		public bool 	maintenanceMode;
		public bool 	invalidVersion;
		public string 	lastInvalidVersion;
		public bool 	canCreate;

		//this is nullable because the default value is true, not false.  See IsCurrentVersion for implementation
		public bool? 	isCurrentVersion;

		public string maintenanceMessage;
		public string invalidVersionMessage;



		public string public_website;
		public string parse_website;
		public string app_store_url;
		public string term_of_service_url;
		public string privacy_url;
		public string android_status;
		public string news_feed_url;

		public string share_url;
		public string share_type_fbwall;
		public string share_type_twitter;
		public string share_type_email;
		public string share_type_sms;
		public string share_type_fbinvite;

		public string premium_feature_points;
		public string free_trial_modal_fineprint;
		public string premium_modal_pullout;

		public string upgrade_modal_json;
		public string[] phoneNumberSupportedCountries;


		public string parse_email_verification;

		public string advertised_premium_values;
		public Dictionary<string, object>  advertised_premium_values_compiled;

		public string post_blrt_share_state;
		public PostBlrtShareState post_blrt_share_state_enum;


		public string rate_app_significant_events;
		public int rate_app_significant_events_int;
		public string rate_app_days;
		public int rate_app_days_int;

		public int settings_update_frequency; // in minutes
		public int permissions_update_frequency; // in minutes
		public int permissions_expiry_time; // in minutes


		public int upload_progress_timeout;
		public int upload_internet_timeout;
		public int encryption_type;

		public string referral_program_key;
		public string referral_program_url;

		public string yozio_obj;
		public Dictionary<string,string> yozio_dic;

		public string embed_preview_learn_more_url;

		public BlrtSettings ()
		{
			public_website 			= ""; 
			parse_website 			= "";
			share_url 				= "";
			term_of_service_url		= "http://m.blrt.co/terms/{0}";
			privacy_url				= "http://m.blrt.co/privacy/{0}";

			invalidVersion = false;
			maintenanceMode = false;
			canCreate = false;


			rate_app_significant_events_int = 100;
			rate_app_days_int = 0;


			settings_update_frequency = 20;
			permissions_update_frequency = 20;
			permissions_expiry_time = 120; // Very low default - should be got from settings..

			upload_internet_timeout = -999;
			upload_progress_timeout = -999;

			yozio_dic = new Dictionary<string, string> ();
			encryption_type = 2;
		}



		public void Unpack ()
		{

			try {
				rate_app_significant_events_int = Convert.ToInt32 (rate_app_significant_events);
				rate_app_days_int = Convert.ToInt32 (rate_app_days);
			} catch {
				rate_app_significant_events_int = rate_app_days_int = 0;
			}

			try {
				advertised_premium_values_compiled = JsonConvert.DeserializeObject<Dictionary<string, object>> (advertised_premium_values);
			} catch {

			}

			try {
				yozio_dic = JsonConvert.DeserializeObject<Dictionary<string, string>> (yozio_obj);
			} catch {

			}

			try {
				post_blrt_share_state_enum = (PostBlrtShareState)Convert.ToInt32 (post_blrt_share_state);
			} catch {
				post_blrt_share_state_enum = PostBlrtShareState.NeverShow;
			}

		}



		public static class AdvertisedPremiumValues
		{
			public static int MaxBlrtDuration {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxBlrtDuration))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxBlrtDuration]);
					return 0;
				}
			}

			public static int MaxImageResolution {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxImageResolution))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxImageResolution]);
					return 0;
				}
			}

			public static int MaxPdfSizeKb {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxPdfSizeKb))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxPdfSizeKb]);
					return 0;
				}
			}

			public static int MaxBlrtPageCount {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxBlrtPageCount))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxBlrtPageCount]);
					return 0;
				}
			}

			public static int MaxSecondaryEmails {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxSecondaryEmails))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxSecondaryEmails]);
					return 0;
				}
			}

			public static int MaxConvoSizeKB {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxConvoSizeKb))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxConvoSizeKb]);
					return 0;
				}
			}

			public static int MaxBlrtSizeKB {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxBlrtSizeKb))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxBlrtSizeKb]);
					return 0;
				}
			}

			public static int MaxPdfFileCount {
				get {
					var dic = BlrtManager.Settings.advertised_premium_values_compiled;

					if (dic.ContainsKey (BlrtPermissions.Keys.MaxBlrtSizeKb))
						return Convert.ToInt32 (dic [BlrtPermissions.Keys.MaxBlrtSizeKb]);
					return 0;
				}
			}
		}


		public enum PostBlrtShareState {

			NeverShow			= 0,
			OnceUntilShare		= 1,
			Once				= 2,
			AlwaysShow			= 3,
		}
	}
}

