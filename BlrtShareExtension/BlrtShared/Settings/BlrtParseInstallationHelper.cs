using System;
using System.Collections.Generic;
using Parse;
using Newtonsoft.Json;

namespace BlrtData
{
	public class BlrtParseInstallation
	{
		private static BlrtParseInstallation _current;

		public static BlrtParseInstallation Current {
			get {
				if (_current == null)
					_current = new BlrtParseInstallation ();
				return _current;
			}
		}

		bool _saving;

		BlrtParseInstallationInstance _instance;




		private BlrtParseInstallation ()
		{
			_instance = new BlrtParseInstallationInstance ();
			_saving = false;
		}


		public void Update ()
		{
			if (_saving)
				return;

			UpdateLocalParseInstallation ();

#if __IOS__

            UpdateIOSLocalParseInstallation (); 

#elif __ANDROID__

			UpdateAndroidLocalParseInstallation ();

#endif

			if (ShouldSave ())
				TrySaveData ();
		}

		public void SetDeviceToken (string deviceToken)
		{
			_instance.DeviceToken = deviceToken;
			Update ();
		}


#if __ANDROID__
		/// <summary>
		/// Updates the android local parse installation.
		/// </summary>
		void UpdateAndroidLocalParseInstallation ()
		{
			var install = ParseInstallation.CurrentInstallation;

			_instance.Badge = null;
			//			_instance.ParseVersion				= install.ParseVersion;
			_instance.AppIdentifier = install.AppIdentifier;
			//			_instance.DeviceTokenLastModified	= install.GetLong("deviceTokenLastModified");
			_instance.PushType = "gcm";
			_instance.DeviceType = install.DeviceType;
			_instance.DeviceToken = install.DeviceToken;
		}
#endif

#if __IOS__
		/// <summary>
		/// Updates the IOSL ocal parse installation.
		/// </summary>
		void UpdateIOSLocalParseInstallation ()
		{
			_instance.Badge = (int)UIKit.UIApplication.SharedApplication.ApplicationIconBadgeNumber;
			_instance.AppIdentifier = Foundation.NSBundle.MainBundle.BundleIdentifier;
		}
#endif

		/// <summary>
		/// Updates the local parse installation.
		/// </summary>
		void UpdateLocalParseInstallation ()
		{
			var install = ParseInstallation.CurrentInstallation;

			_instance.TimeZone = DateTimeOffset.Now.Offset;
			_instance.AppName = BlrtManager.Responder.AppName;
			_instance.AppVersion = BlrtManager.Responder.DeviceDetails.AppVersion;
			_instance.InstallationId = install.InstallationId.ToString ();

			if (ParseUser.CurrentUser != null) {
				_instance.HasDoneTrial = BlrtUserHelper.HasDoneFreeTrial;

				var industry = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.IndustryKey, "");

				if (!string.IsNullOrWhiteSpace (industry)) {
					if (industry.StartsWith (BlrtConstants.IndustriesOther))
						_instance.Industry = BlrtConstants.IndustriesOther;
					else
						_instance.Industry = industry;
				} else {
					_instance.Industry = "";
				}

				_instance.LocationCountry = ParseUser.CurrentUser.Get<string> (BlrtUserHelper.DEVICE_LOCAL, "");
				_instance.AccountType = BlrtPermissions.AccountName;
				_instance.AccountThreshold = ((int)(BlrtPermissions.AccountThreshold ?? 0)).ToString ();
			} else {
				_instance.Industry = "";
				_instance.LocationCountry = "";
				_instance.AccountType = "";
				_instance.AccountThreshold = "";
			}
		}

		private bool ShouldSave()
		{
			if (string.IsNullOrWhiteSpace(_instance.DeviceToken) || _instance.DeviceToken.Length == 0)
				return false;


			return _instance.Dirty;
		}

		private void TrySaveData()
		{
			SaveData ();
		}

		private async void SaveData()
		{
			if (_saving) return;

			_saving = true;
			BlrtUtil.PushDownloadStack ();


			try {

				var param = new Dictionary<string, object>(){
					{ "deviceType",			_instance.DeviceType},
					{ "deviceToken",		_instance.DeviceToken},
					{ "timeZone",			JsonConvert.SerializeObject(_instance.TimeZone)},
					{ "appName",			_instance.AppName},
					{ "appVersion",			_instance.AppVersion},
					{ "parseVersion",		_instance.ParseVersion},
					{ "appIdentifier",		_instance.AppIdentifier},
					{ "doneTrial", 			_instance.HasDoneTrial}
				};

				if(_instance.Badge 						!= null)
					param.Add("badge", 						_instance.Badge.Value);
				if(_instance.DeviceTokenLastModified	!= null)
					param.Add("deviceTokenLastModified",	_instance.DeviceTokenLastModified.Value);
				if(!string.IsNullOrWhiteSpace(				_instance.InstallationId))
					param.Add("installationId", 			_instance.InstallationId);
				if(!string.IsNullOrWhiteSpace(				_instance.PushType))
					param.Add("pushType", 					_instance.PushType);
				if(!string.IsNullOrWhiteSpace(				_instance.Industry))
					param.Add("industry", 					_instance.Industry);
				if(!string.IsNullOrWhiteSpace(				_instance.LocationCountry))
					param.Add("locationCountry", 			_instance.LocationCountry);
				if(!string.IsNullOrWhiteSpace(				_instance.LocationState))
					param.Add("locationState", 				_instance.LocationState);
				if(!string.IsNullOrWhiteSpace(				_instance.AccountType))
					param.Add("accountType", 				_instance.AccountType);
				if(!string.IsNullOrWhiteSpace(				_instance.AccountThreshold))
					param.Add("accountThreshold", 			_instance.AccountThreshold);


				var message = await BlrtUtil.BetterParseCallFunctionAsync<object> (
					"assignInstallation", 
					param, 
					BlrtUtil.CreateTokenWithTimeout (15000)
				);

				LLogger.WriteLine ("Parse installation saved: ", message);

				_instance.Dirty = false;

			} catch (Exception  ex) {
				LLogger.WriteLine ("Parse installation error: ", ex.Message);
			}
			_saving = false;
			BlrtUtil.PopDownloadStack ();

		}



		public class BlrtParseInstallationInstance
		{
			private string 		_deviceType;
			private string 		_deviceToken;
			private long? 		_deviceTokenLastModified;
			private string 		_installationId;
			private string 		_pushType;
			private int? 		_badge;
			private TimeSpan 	_timeZone;
			private string 		_appName;
			private string 		_appVersion;
			private string 		_parseVersion;
			private string 		_appIdentifier;
			private string 		_industry;
			private string 		_locationCountry;
			private string 		_locationState;
			private string 		_accountType;
			private string 		_accountThreshold;
			private bool 		_doneTrial;


			public string DeviceType { 
				get { return _deviceType; } 
				set {
					if (_deviceType != value) {
						_dirty = true;
						_deviceType = value;
					}
				}
			}
			public string DeviceToken { 
				get { return _deviceToken; } 
				set {
					if (_deviceToken!= value) {
						_dirty = true;
						_deviceToken = value;
					}
				}
			}
			public long? DeviceTokenLastModified { 
				get { return _deviceTokenLastModified; } 
				set {
					if (_deviceTokenLastModified!= value) {
						_dirty = true;
						_deviceTokenLastModified = value;
					}
				}
			}
			public string InstallationId { 
				get { return _installationId; } 
				set {
					if (_installationId!= value) {
						_dirty = true;
						_installationId = value;
					}
				}
			}
			public string PushType { 
				get { return _pushType; } 
				set {
					if (_pushType!= value) {
						_dirty = true;
						_pushType = value;
					}
				}
			}
			public int? Badge { 
				get { return _badge; } 
				set {
					if (_badge != value) {
						_dirty = true;
						_badge = value;
					}
				}
			}
			public TimeSpan TimeZone { 
				get { return _timeZone; } 
				set {
					if (_timeZone != value) {
						_dirty = true;
						_timeZone = value;
					}
				}
			}
			public string AppName { 
				get { return _appName; } 
				set {
					if (_appName != value) {
						_dirty = true;
						_appName = value;
					}
				}
			}
			public string AppVersion { 
				get { return _appVersion; } 
				set {
					if (_appVersion != value) {
						_dirty = true;
						_appVersion = value;
					}
				}
			}
			public string ParseVersion { 
				get { return _parseVersion; } 
				set {
					if (_parseVersion != value) {
						_dirty = true;
						_parseVersion = value;
					}
				}
			}
			public string AppIdentifier { 
				get { return _appIdentifier; } 
				set {
					if (_appIdentifier != value) {
						_dirty = true;
						_appIdentifier = value;
					}
				}
			}
			public string Industry { 
				get { return _industry; } 
				set {
					if (_industry != value) {
						_dirty = true;
						_industry = value;
					}
				}
			}
			public string LocationCountry { 
				get { return _locationCountry; } 
				set {
					if (_locationCountry != value) {
						_dirty = true;
						_locationCountry = value;
					}
				}
			}
			public string LocationState { 
				get { return _locationState; } 
				set {
					if (_locationState != value) {
						_dirty = true;
						_locationState = value;
					}
				}
			}
			public string AccountType { 
				get { return _accountType; } 
				set {
					if (_accountType != value) {
						_dirty = true;
						_accountType = value;
					}
				}
			}
			public string AccountThreshold { 
				get { return _accountThreshold; } 
				set {
					if (_accountThreshold != value) {
						_dirty = true;
						_accountThreshold = value;
					}
				}
			}
			public bool HasDoneTrial { 
				get { return _doneTrial; } 
				set {
					if (_doneTrial != value) {
						_dirty = true;
						_doneTrial = value;
					}
				}
			}

			//string channels;
			//?installationId: Unique Id for the device used by Parse (readonly).


			private bool _dirty;
			[JsonIgnore]
			public bool Dirty { get { return _dirty; } set { _dirty = value;} }

			public BlrtParseInstallationInstance()
			{
#if __IOS__
				DeviceType = "ios";
#elif __ANDROID__
				// set in Update()
#endif
			}
		}
	}
}

