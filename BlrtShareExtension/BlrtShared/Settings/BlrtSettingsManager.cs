using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Parse;

namespace BlrtData
{
	public class BlrtSettingsManager
	{
		private static string FileName { get { return "blrt-setting"; } }
		private static string EncryptionKey { get { return "bork_borkd326_fla6tmout3hv" + (ParseUser.CurrentUser != null ? ParseUser.CurrentUser.ObjectId : "waffles"); } }

		[JsonIgnore]
		public bool FromCloud { get; private set; }



		public DateTime UpdatedAt { get; set; }
		public BlrtPermissions Permissions { get; set; }
		public BlrtSettings Settings { get; set; }
		public BlrtIAP IAP { get; set; }


		public BlrtSettingsManager ()
		{
			Permissions = new BlrtPermissions ();
			Settings = new BlrtSettings ();
			IAP = new BlrtIAP ();

			FromCloud = false;
		}



		private BlrtSettingsManager Unpack () 
		{
			try {

				Permissions.Unpack();
				Settings.Unpack();
				IAP.Unpack();

			} catch(Exception e) {
			}

			return this;
		}

		private void Validate (string json) { }


		public static void CompareSettings (BlrtSettingsManager newSettings, BlrtSettingsManager oldSettings)
		{
			if (newSettings == oldSettings)
				return;


			BlrtPermissions.BlrtAccountThreshold? oldThreshold = null;
			List<string> oldAvailableIAPs = null;


			if (oldSettings != null) {
				oldThreshold = oldSettings.Permissions.account_threshold_enum;
				oldAvailableIAPs = oldSettings.IAP.available_iaps;
			}


			if (newSettings.Permissions.account_threshold_enum != oldThreshold)
				BlrtGlobalEventManager.AccountTypeChanged (newSettings, new EventArgs ());

			if (oldAvailableIAPs == null || newSettings.IAP.available_iaps.Count != oldAvailableIAPs.Count || newSettings.IAP.available_iaps.Except (oldAvailableIAPs).Any ())
				BlrtGlobalEventManager.AvailableIAPsChanged (newSettings, new EventArgs ());
		}


		public static bool FromStringOrCache(string json, out BlrtSettingsManager settings)
		{
			try{
				LLogger.WriteEvent("Settings", "Loading", "Trying to open from json file.");
				var output = JsonConvert.DeserializeObject<BlrtSettingsManager>(json);
				output.Validate(json);


				LLogger.WriteEvent("Settings", "Loading", "Openning json was successful, now unpacking results.");
				settings = output.Unpack();

				settings.UpdatedAt = DateTime.Now;

				LLogger.WriteEvent("Settings", "Loading", "Unpacking settings was successful, now encrypting results.");
				json = BlrtUtil.XorString(JsonConvert.SerializeObject(settings), EncryptionKey);

				LLogger.WriteEvent("Settings", "Loading", "Saving encrypting file results to cache.");
				File.WriteAllText(BlrtConstants.Directories.Default.GetDataPath (FileName), json);


				settings.FromCloud = true;

				return true;
			} catch (Exception ex){
				LLogger.WriteEvent("Settings", "Loading", "Loading failed because: " + ex.Message);
				settings = FromCache ();
				return false;
			}
		}

		public static BlrtSettingsManager FromCache()
		{
			try{
				LLogger.WriteEvent("Settings", "Loading", "Loading setting from cache.");
				var json = File.ReadAllText(
					BlrtConstants.Directories.Default.GetDataPath (FileName)
				);

				LLogger.WriteEvent("Settings", "Loading", "Decrypting solution.");
				json = BlrtUtil.XorString(json, EncryptionKey);

				return JsonConvert.DeserializeObject<BlrtSettingsManager>(json).Unpack();

			} catch (Exception ex){
				LLogger.WriteEvent("Settings", "Loading", "Loading setting from cache fails, now loading from defaults.");
				return new BlrtSettingsManager ().Unpack();
			}
		}

	}
}

