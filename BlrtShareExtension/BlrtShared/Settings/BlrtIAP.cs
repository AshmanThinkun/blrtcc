using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BlrtData
{
	public class BlrtIAP
	{
		public static List<string> AvailableIAPs 						{ get { return BlrtManager.IAP.available_iaps; 		} }
		public static Dictionary<string, string> SpecialCases 			{ get { return BlrtManager.IAP.special_cases; 		} }



		public List<string> available_iaps;

		public Dictionary<string, string> special_cases;




		public BlrtIAP ()
		{
			available_iaps = null;

			special_cases = null;
		}



		public void Unpack () {
		}

	}
}

