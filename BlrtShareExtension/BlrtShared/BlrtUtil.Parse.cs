using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Parse;
using System.Threading.Tasks;
using BlrtAPI;

namespace BlrtData
{
	public static partial class BlrtUtil
	{
		public static T Get<T>(this ParseObject obj, string key, T defaultValue)
		{
			T output;
			if ((null != obj) && obj.TryGetValue<T> (key, out output) && (output != null))
				return output;

			return defaultValue;
		}


		public static Task<ParseObject> BetterFetchAsync(this ParseObject obj, CancellationToken token) {
			return HandleTask<ParseObject> (obj.FetchAsync (token),	token);
		}
		public static Task<ParseObject> BetterFetchIfNeededAsync(this ParseObject obj, CancellationToken token) {
			return HandleTask<ParseObject> (obj.FetchIfNeededAsync (token),	token);
		}
		public static Task BetterSaveAsync(this ParseObject obj, CancellationToken token) {
			return HandleTask (obj.SaveAsync (token),	token);
		}

		public static Task BetterSaveAllAsync(this IEnumerable<ParseObject> obj, CancellationToken token) {
			return HandleTask (obj.SaveAllAsync (token),	token);
		}
		public static Task<IEnumerable<ParseObject>> BetterFetchAllAsync(this IEnumerable<ParseObject> obj, CancellationToken token) {
			return HandleTask (obj.FetchAllAsync (token),	token);
		}
		public static Task<IEnumerable<ParseObject>> BetterFetchAllIfNeededAsync(this IEnumerable<ParseObject> obj, CancellationToken token) {
			return HandleTask (obj.FetchAllIfNeededAsync (token),	token);
		}


		public static Task<IEnumerable<ParseObject>> BetterFindAsync(this ParseQuery<ParseObject> obj, CancellationToken token) {
			return HandleTask<IEnumerable<ParseObject>>( obj.FindAsync(token), token );
		}
		public static Task<int> BetterCountAsync(this ParseQuery<ParseObject> obj, CancellationToken token) {
			return HandleTask<int>( obj.CountAsync(token), token );
		}
		public static Task<ParseObject> BetterFirstAsync(this ParseQuery<ParseObject> obj, CancellationToken token) {
			return HandleTask<ParseObject> (obj.FirstAsync (token),	token);
		}


		public static Task<T> BetterParseCallFunctionAsync<T>(string name, IDictionary<string, object> parameters, CancellationToken token) {
			return HandleTask (ParseCloud.CallFunctionAsync<T> (name, parameters, token), token);
		}

		static async Task<T> HandleTask<T> (Task<T> task, CancellationToken token)
		{
			T result;
			try {
				result = await AwaitOrCancelTask (task, token);
			} catch (Exception e) {
				if (BlrtUtil.IsConnectionIssueException (e))
					BlrtUtil.SetConnectionHasProblem (true);

				if (await BlrtManager.HandlePotentialInvalidSessionException (e)) {
					throw new TaskCanceledException ("Cancelled because of session expired");
				}

				throw;
			}

			BlrtUtil.SetConnectionHasProblem (false);
			return result;
		}

		static async Task HandleTask (Task task, CancellationToken token)
		{
			try {
				await AwaitOrCancelTask (task, token);
			} catch (Exception e) {
				if (BlrtUtil.IsConnectionIssueException (e))
					BlrtUtil.SetConnectionHasProblem (true);

				if (await BlrtManager.HandlePotentialInvalidSessionException (e)) {
					throw new TaskCanceledException ("Cancelled because of session expired");
				}

				throw;
			}

			BlrtUtil.SetConnectionHasProblem (false);
		}


		public static async Task<IEnumerable<ParseObject>> FetchAllAsync(IEnumerable<APIResponseObject> objects, CancellationToken token)
		{
			var tasks = (from obj in objects
						 select ParseObject.CreateWithoutData (obj.ClassName, obj.ObjectId).BetterFetchAsync (token));

			return await Task.WhenAll (tasks);
		}
	}
}
