using System;

namespace BlrtData
{
	public static class BlrtArgumentTranslator
	{
		public static string[] GetValues(string[] arguments){
		
			var output = new string[arguments.Length];
		
			for (int i = arguments.Length - 1; i >= 0; --i) {
				output [i] = GetValue (arguments [i]);			
			}
		
			return output;		
		}

		public static bool HasValue(string argument){

			switch (argument ) {
				case "num_days_to_account_expiration":
				case "account_expiration_num_day_or_days":
				case "account_expiration_num_days_left":
				case "account_expiration_num_weeks_left":
					return true;
			}

			return false;
		}

		public static string GetValue(string argument){
		
			if (!HasValue (argument))
				return "";

			switch (argument ) {
				case "num_days_to_account_expiration":		//old, use "account_expiration_num_days_left"
					var tte = BlrtUserHelper.TimeToAccountTypeExpiration ();
					return tte != null ? tte.Value.Days.ToString () : "";
				case "account_expiration_num_day_or_days":	//old, use "account_expiration_num_days_left"
					tte = BlrtUserHelper.TimeToAccountTypeExpiration ();
					return tte != null ? (tte.Value.Days == 1 ? "day" : "days") : "";

				case "account_expiration_num_days_left":
					tte = BlrtUserHelper.TimeToAccountTypeExpiration ();
					if (tte != null)	return tte.Value.Days + (tte.Value.Days == 1 ? " day" : " days");
					break;
				case "account_expiration_num_weeks_left":
					tte = BlrtUserHelper.TimeToAccountTypeExpiration ();
					if (tte != null)	return tte.Value.Days/7 + (tte.Value.Days/7 == 1 ? " week" : " weeks");
					break;
			}
		
			return "";
		}
	}
}

