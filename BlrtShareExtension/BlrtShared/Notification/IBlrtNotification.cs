using System;

namespace BlrtData
{
	public interface IBlrtNotification
	{
		bool IsAlert { get; }
		string Title { get; }
		string Message { get; }
		string Cancel { get; }
		string[] Arguments { get; }



		BlrtNotificationOption[] Options 	{ get; }


	}

	public class BlrtNotificationOption {
		public string label;
		public string type;
		public string arg;
		public BlrtNotificationOption fallback;
	}
}

