using System;
using System.Collections.Generic;
using System.Linq;

namespace BlrtData.Notification
{
	public class BlrtCloudNotification : IBlrtNotification
	{
		public bool IsAlert { get { return true; } }

		string _title;
		string[] _argSet;
		string[] _parsed;
		string _message;
		string _cancel;



		public string Title {
			get {
				return _title;
			}
			set { _title = value; }
		}

		public string Message {
			get {
				return _message;
			}
			set { _message = value; }
		}

		public string Cancel {
			get {
				return _cancel;
			}
			set { _cancel = value; }
		}

		public BlrtNotificationOption[] Options 	{ get; set; }

		public string[] Arguments {
			get {
				if (_argSet == null)
					return new string[0];

				if (_parsed != null)
					return _parsed;

				return _parsed = BlrtArgumentTranslator.GetValues(_argSet);
			}
		}

		public string[] ArgSet {
			get {
				return _argSet;
			}
			set { _argSet = value;
				_parsed = null;
			}
		}

		public BlrtCloudNotification ()
		{
		}
	}
}

