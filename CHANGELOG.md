#0.1.1 (08/11/16)
### Bug: 
* add concurrency control for remove user
## Improvement:
* upgrade parse-server version to 2.2.24

#0.1.2 (14/11/16)
### Fix 
redirect /make to web.blrt.com/make

#0.1.3(15/12/16)
### Fix: 
video conversion server url

#0.1.5
## enable 2.3.2 2.3.3 IOS

#0.2.0
## API for audio and image item

#0.2.1
## enable 2.3.4 IOS

#0.2.2
## support websocket for staging

#0.2.3
## implement the pdf message

#0.2.4
## version number

#0.2.5 (21/04/2017)
## version control the thumbnail generation script
## set SERVER_URL correctly for staging
## change the thumbnail logic 
## update useMasterKey logic